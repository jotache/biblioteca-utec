package com.tecsup.SGA.web.reclutamiento.controller;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.BuscarAreasEstudiosCommand;

public class ProcesosAreaEstudiosFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(ProcesosAreaEstudiosFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request) throws ServletException {
		log.info("formBackingObject:INI");   
		BuscarAreasEstudiosCommand command = new BuscarAreasEstudiosCommand();
		
		List<TipoTablaDetalle> lista  = this.tablaDetalleManager.getAllTablaDetalle("0002", "", "", "", "", "", "", "", "1");
		if (lista!=null)
			command.setLongitud(lista.size());
			
    	//request.getSession().setAttribute("listaDetalle",lista);
		command.setListaAreasEstudios(lista);
        log.info("formBackingObject:FIN");

		return command;
	}
	   
}
