package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.EvaluacionProcesosManager;
import com.tecsup.SGA.service.reclutamiento.CalificacionManager;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.EnviarPostulanteCommand;
import com.tecsup.SGA.modelo.Evaluacion;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.TipoTablaDetalle;


public class EvaluacionesEvaluarPostulanteFormController extends SimpleFormController{
	EvaluacionProcesosManager evaluacionProcesosManager;
	CalificacionManager calificacionManager;
	private ProcesoManager procesoManager;
	private TablaDetalleManager tablaDetalleManager;
	private static Log log = LogFactory.getLog(EvaluacionesEvaluarPostulanteFormController.class);
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		EnviarPostulanteCommand command = new EnviarPostulanteCommand();
		
		command.setCodUsuario(request.getParameter("txhCodUsuario"));
		command.setCodPostSel(request.getParameter("txhPostulante") == null ? "" :  request.getParameter("txhPostulante"));
		command.setNomPostSel(request.getParameter("txhNomPostulante") == null ? "" :  request.getParameter("txhNomPostulante").replace("|", ""));
		command.setCodEvalSel(request.getParameter("txhTipoEvaluacion") == null ? "" :  request.getParameter("txhTipoEvaluacion").replace("|", ""));
		command.setDscEvalSel(request.getParameter("txhDscEvaluacion") == null ? "" :  request.getParameter("txhDscEvaluacion").replace("|", ""));
		command.setCodProceso(request.getParameter("txhProceso") == null ? "" :  request.getParameter("txhProceso"));
		command.setCodEvaluacionActual(request.getParameter("txhEvaluacionAct") == null ? "" :  request.getParameter("txhEvaluacionAct").replace("|", ""));
		command.setCodPostulanteProceso(request.getParameter("txhPostulanteProceso") == null ? "" :  request.getParameter("txhPostulanteProceso").replace("|", ""));
		command.setCodEtapa(request.getParameter("txhCodEtapaProceso") == null ? "" : request.getParameter("txhCodEtapaProceso"));
		command.setDscEtapa(request.getParameter("txhDscEtapaProceso") == null ? "" : request.getParameter("txhDscEtapaProceso"));
		
		if ( !command.getCodEvalSel().trim().equals("") )
			command.setListCalificaciones(
					calificacionManager.getAllCalificacion(
					command.getCodEtapa() //CommonConstants.PROC_ETAPA_REVISION
					, command.getCodEvalSel()));
		if ( !command.getCodEvaluacionActual().equals("") )
		{
			Evaluacion evaluacion = this.evaluacionProcesosManager.getEvaluacionById(command.getCodEvaluacionActual().replace("|", ""));
			if ( evaluacion != null )
			{
				command.setCodCalSel(evaluacion.getCodCalificaion());
				command.setComentario(evaluacion.getComentario());
			}
		}
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
    * Redirect to the successView when the cancel button has been pressed.
    */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	String respuesta;
    	EnviarPostulanteCommand control = (EnviarPostulanteCommand) command;
    	if ( control.getOperacion().trim().equals("GRABAR") )
    	{
    		
    		//Permitir grabar solo si EL postulante seleccionado
    		//tienen su etapa anterior ya CALIFICADA.
    		String validacion = "";
    		
    		if (control.getCodEtapa().equals(CommonConstants.PROC_ETAPA_REVISION)){
    			validacion="1"; //en Revision no hace falta preguntar por este proceso pues solo hay una Evaluaci�n.
    		}else{
    			validacion = validarEtapaAnterior(control.getCodProceso(),control.getCodPostSel(),control.getCodEvalSel());	
    		}
    		    		
    		System.out.println("validacion:"+validacion);
    		
    		if (validacion.equals("1")){
    			//TODO: habilitar Grabar.
    			respuesta = Grabar(control);
    			control.setCodEvaluacionActual(respuesta);
    			//respuesta = "0";
    			if (Long.valueOf(respuesta)>0 && control.getCodEtapa().equals(CommonConstants.PROC_ETAPA_SELECCION)){
    				//TODO: Enviar Mail
    				enviarMail(control.getCodProceso(),control.getCodEvalSel(),control);
    			}
    			
    		}else{
    			respuesta = "-2";
    		}
    		
    		if ( respuesta != null )
    			if (Long.valueOf(respuesta)>0 ) {
    				control.setMsg("OK");    							
    				control.setDatosRRHH1(control.getCodPostSel());
    				control.setDatosRRHH2(control.getNomPostSel());
    				control.setDatosRRHH3(control.getCodEvalSel());
    				//control.setDatosRRHH4(""); ya se carg� previamente en EnviarMail
    				control.setDatosRRHH5(control.getCodProceso());
    				
    				System.out.println("getCodEvaluacionActual:" + control.getCodEvaluacionActual());
    				
    				control.setDatosRRHH6(control.getCodEvaluacionActual());
    				control.setDatosRRHH7(control.getCodPostulanteProceso());
    			}else if (respuesta.trim().equals("-1") )
    				control.setMsg("ERROR");
    			else if (respuesta.trim().equals("-2") )
    				control.setMsg("EVAL_ANT_NO_CALIF");
    			else control.setMsg("ERROR");
    		else control.setMsg("ERROR");
    	}
	    return new ModelAndView("/reclutamiento/evaluaciones/eva_evaluar_postulante_rev","control",control);	    
    }
    
    private String validarEtapaAnterior(String codProceso0,String codPostulante0,String codEvalActual){
    	//1= El postulante OK
    	//0= El postulante NO esta calificado en su etapa anterior
    	String validacion="1";
    	/*    
	    codUsuario value="152544"/>		
		codPostSel value="25|"/>
		nomPostSel value="V�squez Silva, Claudia"/>	
		codProceso value="13|"/>
		*/
    	String codProceso="";
    	StringTokenizer codProceso1 = new StringTokenizer(codProceso0,"|");
    	while ( codProceso1.hasMoreTokens())
    		codProceso = codProceso1.nextToken();
    	
    	String codPostulante="";
    	StringTokenizer codPostulante1 = new StringTokenizer(codPostulante0,"|");
    	while ( codPostulante1.hasMoreTokens())
    		codPostulante = codPostulante1.nextToken();
    	    	
    	
    	List<EvaluacionesByEvaluadorBean> evaluaciones = this.evaluacionProcesosManager.getEvaluacionesSeleccionPorPostulante(codProceso, codPostulante);
    	
    	System.out.println("validarEtapaAnterior#codProceso"+codProceso);
    	System.out.println("validarEtapaAnterior#codPostulante"+codPostulante);
    	System.out.println("validarEtapaAnterior#codEvalActual"+codEvalActual);
    	
    	if (evaluaciones!=null){
    		if (evaluaciones.size()>0){
    			//obtener nro de orden de tipo de evaluacion ACTUAL...
    			String orden="";
    			String codEvalAnterior="";
    			for (EvaluacionesByEvaluadorBean evals : evaluaciones){
    				if (evals.getCodTipoEvaluacion().equals(codEvalActual)){
    					orden=evals.getOrden(); //orden obtenido
    					break;
    				}
    				codEvalAnterior = evals.getCodTipoEvaluacion();
    			}
    			
    			//Si codEvalAnterior="" se trata de la primera evaluaci�n
    			if (!codEvalAnterior.equals("")){
					//Evaluacion Anterior obtenida.. validar que ya est� CALIFICADA por el encargado asignado
    				String calificacion="";
    				for (EvaluacionesByEvaluadorBean evals : evaluaciones){        					
    					if (evals.getCodTipoEvaluacion().equals(codEvalAnterior) ){    						
    						calificacion = (evals.getDscCalificacionJefeDpto_Revision()==null?"":evals.getDscCalificacionJefeDpto_Revision());	    						
    						break;    						
    					}
    				}
    				
    				if (calificacion.equals(""))
    					validacion="0";    				
				}    			
    			
    		}
    	}
    		    	
    	return validacion;
    }
    
    private void enviarMail(String codProceso0,String codEvalActual,EnviarPostulanteCommand control){
    	System.out.println("enviarMail#codProceso0:"+codProceso0);
    	System.out.println("enviarMail#codEvalActual:"+codEvalActual);
    	//validar que todos los postulantes de la evaluacion actual esten CALIFICADOS y al menos existe uno con calificaci�n "APTO".
    	String codProceso="";
    	StringTokenizer codProceso1 = new StringTokenizer(codProceso0,"|");
    	while ( codProceso1.hasMoreTokens())
    		codProceso = codProceso1.nextToken();
    	
    	List<EvaluacionesByEvaluadorBean> evaluacionesTodos = this.evaluacionProcesosManager.getEvaluacionesPostulantesSeleccionPorTipoEvaluacion(codProceso,codEvalActual);
    	boolean postSinCalificar=false;
    	boolean conCalifApto = false;
    	if (evaluacionesTodos!=null){
    		if (evaluacionesTodos.size()>0){
    			String calificacion="";    			
    			for (EvaluacionesByEvaluadorBean evals : evaluacionesTodos){
    				calificacion = (evals.getDscCalificacionJefeDpto_Revision()==null?"":evals.getDscCalificacionJefeDpto_Revision());
    				if (calificacion.equals("")){
    					postSinCalificar=true;    	//hallando al menos una evaluacion Sin calificar
    					break;
    				}
    			}

    			//verificando que al menos una evaluacion sea calificada como APTO.
				for (EvaluacionesByEvaluadorBean evalsApto : evaluacionesTodos){
					calificacion = (evalsApto.getDscCalificacionJefeDpto_Revision()==null?"":evalsApto.getDscCalificacionJefeDpto_Revision());
					System.out.println("::" + evalsApto.getCodTipoEvaluacion()+"-"+evalsApto.getDscCalificacionJefeDpto_Revision());
					if (calificacion.equals(CommonConstants.REC_CODIGO_APTO)){
						conCalifApto=true;
						break;
					}
				}
    		}
    	}
    	System.out.println("enviarMail#postSinCalificar:"+postSinCalificar);
    	System.out.println("enviarMail#conCalifApto:"+conCalifApto);
    	
    	java.util.ResourceBundle prop = java.util.ResourceBundle.getBundle("db");
    	String entorno = prop.getString("db.environment");
    	
    	//en caso que todos esten calificados y al menos uno es APTO, enviar mensaje al evaluador de la etapa siguiente...
    	if (postSinCalificar==false && conCalifApto==true ){
    		//obtener datos del evaluador de la siguiente etapa...
    		String codTipoEvalSgte="";
    		List<Evaluador> tiposEvaluaciones = this.procesoManager.getEvaluadoresAsignadosXProceso(codProceso); //esta ordenado
    		boolean aux = false;
    		if (tiposEvaluaciones!=null){
    			if (tiposEvaluaciones.size()>0){
    				for (Evaluador evals : tiposEvaluaciones){
    					System.out.println("codigoEvaluador: "+evals.getCodEvaluador()+" codTipoEval:" + evals.getCodTipoEvaluacion());
    					if (aux==true){
    						if (!evals.getCodEvaluador().equals("0")){//que la siguiente etapa se APLIQUE es decir tenga asignado un evaluador.
    							codTipoEvalSgte=evals.getCodTipoEvaluacion();    							
    							break;
    						}else{
    							codEvalActual = evals.getCodTipoEvaluacion();
    							aux=false;
    						}
    					}
    					if (evals.getCodTipoEvaluacion().equals(codEvalActual)){
    						control.setDatosRRHH4(evals.getDscTipoEvaluacion());
    						aux=true;
    					}
    				}
    			}
    		}
    		System.out.println("enviarMail#codTipoEvalSgte:"+codTipoEvalSgte);
    		System.out.println("enviarMail#aux:"+aux);
    		if (!(aux==true && !codTipoEvalSgte.equals(""))){
    			codTipoEvalSgte=""; //la evaluaci�n es la �ltima y no hay siguiente evaluacion
    		}
    		
    		if (!codTipoEvalSgte.equals("") && entorno.equals("produccion")){ //proceder con el envio de mail...    				
    				Evaluador evaluador = this.procesoManager.getEvaluador(codProceso, codTipoEvalSgte, CommonConstants.PROC_ETAPA_SELECCION);
    				
    				    			
    				/*Sr(a). NOM_EVALUADOR:
					Se le informa que ya Ud. puede ejecutar la evaluaci�n NOM_EVALUACION perteneciente al proceso de NOM_PROCESO.					
					Puede ingresar al sistema y dirijirse a la opci�n "Evaluar Postulante" 					
					Saludos Coordiales,					
					TECSUP.*/
    				
    				//TECSUP SGA Reclutamiento - Ejecuci�n de Evaluaci�n (Selecci�n: NOMBRE_PROCESO)
    				List listaDatosMail = this.tablaDetalleManager.getAllTablaDetalle(
							CommonConstants.TIPT_TIPO_RECLUTAMIENTO_EMAIL, "0005" , "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    				String asunto="";
    				String mensaje="";
    				Proceso proceso = this.procesoManager.getProceso(codProceso);
    				if (listaDatosMail!=null && proceso!=null){
    					TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
    					tipoTablaDetalle = (TipoTablaDetalle) listaDatosMail.get(0);
    					asunto = tipoTablaDetalle.getDscValor1();
    					asunto = asunto.replace("NOMBRE_PROCESO",proceso.getDscProceso());
    					
    					mensaje = tipoTablaDetalle.getDescripcion();
    					mensaje = mensaje.replace("NOM_EVALUADOR", evaluador.getDscEvaluador());
    					mensaje = mensaje.replace("NOM_EVALUACION", evaluador.getDscTipoEvaluacion());    					    					    				
    					mensaje = mensaje.replace("NOM_PROCESO", proceso.getDscProceso());
    					
    					//enviar mensaje al evaluador de la siguiente Evaluaci�n
    					System.out.println("evaluador.getCorreo():" + evaluador.getCorreo());
			    		System.out.println("CommonConstantsEmail.TECSUP_EMAIL:" + CommonConstantsEmail.TECSUP_EMAIL);
			    		System.out.println("evaluador.getDscEvaluador():" + evaluador.getDscEvaluador());
			    		System.out.println("From - Nombre:" + tipoTablaDetalle.getDscValor2());    	    		
			    		System.out.println("Texto a enviar en el Correo: \n"+ asunto + "\n" + mensaje);
			    		
			    		//TODO: Habilitar Envio de Correo (YA ESTA HABILITADO)
			    		
			    		try {
			    			
			    			/*
			    			 * EnviarCorreo(
			    			 * String correoDestino, String correoOrigen, 
			    			 * String nombreDestino, String nombreOrigen,
    							String asunto, String cuerpoMensaje) */
			    			
			    			ServletEnvioCorreo envioCorreo = new ServletEnvioCorreo();
			    			envioCorreo.EnviarCorreo(evaluador.getCorreo(), CommonConstantsEmail.TECSUP_EMAIL, 
			    					evaluador.getDscEvaluador(), tipoTablaDetalle.getDscValor2(), 
			        	    		asunto, mensaje);
			    		 }catch(Exception ex){
				    	    	ex.printStackTrace();
				    	    	log.error(ex.getMessage(), ex);
			    		}
    				}
    				
    		}
    		
    		
    	}
    	
    }
    
    private String Grabar(EnviarPostulanteCommand control)
    {
    	String codSigEstado = "";
    	if ( control.getCodEtapa().equals(CommonConstants.PROC_ETAPA_REVISION) )
    		codSigEstado = CommonConstants.EST_REV_REV_X_JEFE_DPTO;
    	else
    		codSigEstado = CommonConstants.EST_SEL_EVALUADO;
    	try
    	{
    		if ( control.getCodEvaluacionActual().trim().equals("") )
    			return this.evaluacionProcesosManager.insertEvaluacionPostulante(
    				 control.getCodPostSel(), control.getCodProceso(), control.getCodEtapa()
    				 , control.getCodEvalSel(), codSigEstado 
    				 , control.getComentario(), control.getCodCalSel(), "", ""
    				 , CommonConstants.EVAL_NO_ACT_ESTADO, control.getCodUsuario());
    		else
    			return this.evaluacionProcesosManager.updateEvaluacionPostulante(
    					control.getCodPostulanteProceso(), control.getCodEvaluacionActual()
    					, codSigEstado, control.getComentario(), control.getCodCalSel()
    					, "", "", CommonConstants.EVAL_NO_ACT_ESTADO, control.getCodUsuario());
    	
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return null;
    }
    
	public void setEvaluacionProcesosManager(
			EvaluacionProcesosManager evaluacionProcesosManager) {
		this.evaluacionProcesosManager = evaluacionProcesosManager;
	}

	public void setCalificacionManager(CalificacionManager calificacionManager) {
		this.calificacionManager = calificacionManager;
	}
	
}
