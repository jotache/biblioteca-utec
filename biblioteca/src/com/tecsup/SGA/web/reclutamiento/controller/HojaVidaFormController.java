package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.HojaVidaDatosBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.reclutamiento.ReclutaManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.DatosPersonalesCommand;
import com.tecsup.SGA.web.reclutamiento.command.HojaVidaCommand;

public class HojaVidaFormController extends SimpleFormController{
	private TablaDetalleManager tablaDetalleManager;
	private ReclutaManager reclutaManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	HojaVidaCommand command = new HojaVidaCommand();    	
    	/*Recogiendo valores*/
    	command.setIdRec((String)request.getParameter("txhCodPostulante"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	command.setProcedencia((String)request.getParameter("txhProcedencia"));
    	/*Informacion*/
    	inicializaCombos(command);
    	//modificado Napa
    	inicializaAreaInteres(command);
    	inicializaEstudios(command);
    	inicializaExperiencia(command);
    	//Llenado Tabla Interes - Session
    	//request.getSession().setAttribute("listaAreaInteresH", command.getListaAreaInteresH());
    	inicializaDatos(command, command.getIdRec());
   	
    	
    	//Obteniendo estudios superiores
    	//command.setListaEstudiosSuperiores(
    				//this.reclutaManager.getReclutaSuperior(command.getIdRec())
    										//);
    	//
    	HojaVidaDatosBean datosEstudios;
    	Recluta recluta ;
    	command.setListaEstudiosSuperiores(new ArrayList<HojaVidaDatosBean>());
    	List estudiosSuperiores = this.reclutaManager.getReclutaSuperior(command.getIdRec());
    	
    	for (int i = 0; i < estudiosSuperiores.size(); i++){
    		datosEstudios = new HojaVidaDatosBean();
    		recluta = (Recluta) estudiosSuperiores.get(i);
    		datosEstudios.setCodProfesion(recluta.getAreaEstudio()==null?"":recluta.getAreaEstudio() );
    		datosEstudios.setCodInstitucion(recluta.getInstitucion()==null?"":recluta.getInstitucion());
    		datosEstudios.setCodGradoAcademico(recluta.getGradoAcad()==null?"":recluta.getGradoAcad());
    		datosEstudios.setCodMerito(recluta.getMerito()==null?"":recluta.getMerito());
    		datosEstudios.setOtraInstitucion( recluta.getOtraInstitucion()==null?"":recluta.getOtraInstitucion() );
    		datosEstudios.setCiclosEstudiados(recluta.getCiclo()==null?"":recluta.getCiclo());
    		datosEstudios.setInicio(recluta.getInicio()==null?"":recluta.getInicio());
    		datosEstudios.setTermino(recluta.getTermino()==null?"":recluta.getTermino());
    		datosEstudios.setListaProfesiones(command.getListaAreaEstudio1());
    		datosEstudios.setListaMeritos(command.getListaMerito1());
    		datosEstudios.setListaInsituciones(command.getListaInstitucion1());
    		datosEstudios.setListaGradosAcademicos(command.getListaGradoAcad1());
    		command.getListaEstudiosSuperiores().add(datosEstudios);
    	}
    		
    	
    	HojaVidaDatosBean datosExpercianLab;
    	command.setListaExperienciasLaborales(new ArrayList<HojaVidaDatosBean>());
    	List experienciaLab = this.reclutaManager.getReclutaExpLaboral(command.getIdRec());
    	//Obteniendo experiencias laborales
    	//command.setListaExperienciasLaborales(
    				//this.reclutaManager.getReclutaExpLaboral(command.getIdRec()));
    	for (int i = 0; i < experienciaLab.size(); i++){
    		datosExpercianLab = new HojaVidaDatosBean();
    		recluta = (Recluta) experienciaLab.get(i);    	
    		datosExpercianLab.setEmpresa(recluta.getOrganizacion()==null?"":recluta.getOrganizacion());
    		datosExpercianLab.setCodPuesto(recluta.getPuesto()==null?"":recluta.getPuesto());
    		datosExpercianLab.setOtroPuesto(recluta.getOtroPuesto()==null?"":recluta.getOtroPuesto());
    		datosExpercianLab.setInicio(recluta.getFecIni()==null?"":recluta.getFecIni());
    		datosExpercianLab.setTermino(recluta.getFecFin()==null?"":recluta.getFecFin());
    		datosExpercianLab.setReferencia(recluta.getReferencia()==null?"":recluta.getReferencia());
    		datosExpercianLab.setTelefonoContacto(recluta.getTelefRef()==null?"":recluta.getTelefRef());
    		datosExpercianLab.setDescripcionActividades(recluta.getFunciones()==null?"":recluta.getFunciones());
    		datosExpercianLab.setListaPuestos(command.getListaPuesto1());
    		command.getListaExperienciasLaborales().add(datosExpercianLab);
    	}

    	System.out.println("command.getListaEstudiosSuperiores:"+ command.getListaEstudiosSuperiores().size());
    	System.out.println("command.getListaExperienciasLaborales:"+ command.getListaExperienciasLaborales().size());
    	
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	HojaVidaCommand control = (HojaVidaCommand) command;
    	
    	if (control.getOperacion().trim().equals("VER_DATOS")){
    		request.getSession().setAttribute("txhCodPostulanteVD", control.getMessage());
    		request.getSession().setAttribute("txhCodUsuarioVD", control.getCodUsuario());
    		control.setMessage("VER_DATOS");
    	}
    	
	    return new ModelAndView("/reclutamiento/procesos/rec_hojavida","control",control);
	    
    }
    
    /*Metodos*/
    //RNapa
    private void inicializaAreaInteres(HojaVidaCommand command){
    	List lista = this.reclutaManager.getReclutaAreaInteres(command.getIdRec());
    	List resultado=new ArrayList();
    	Recluta recluta=new Recluta();
    	String codPostulante="";
    	try {
    		for(int j=0; j < lista.size(); j++){
    			recluta=(Recluta)lista.get(j);
    			codPostulante=recluta.getCodTTDInteres();
    			if(codPostulante!=null && !codPostulante.equals("")){
    				System.out.println(">>"+recluta.getCodTTDInteres()+"<<");
    				Recluta obj=new Recluta();
    				obj.setCodTTDInteres(recluta.getCodTTDInteres());
    				obj.setDescInteres(recluta.getDescInteres());
    				resultado.add(obj);
    			}
        	}
		}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    	command.setListaAreaInteresByPost(resultado);
    }
    private void inicializaEstudios(HojaVidaCommand control){
    	ArrayList lista;
		Recluta lObject;
		int i;
		lista = (ArrayList)this.reclutaManager.getReclutaSuperior(control.getIdRec());
		
		if(lista!=null){
			if(lista.size()>0){
				lObject = (Recluta) lista.get(0);
				control.setGradoAcad1(lObject.getGradoAcad());				
				control.setAreaEstudio1(lObject.getAreaEstudio());
				control.setInstitucion1(lObject.getInstitucion());
				control.setOtraInstitucion1(lObject.getOtraInstitucion());
				control.setCiclo1(lObject.getCiclo());
				control.setMerito1(lObject.getMerito());
				control.setInicio1(lObject.getInicio());
				control.setTermino1(lObject.getTermino());				
			}
		}
    }
    private void inicializaExperiencia(HojaVidaCommand control){
    	ArrayList lista;
		Recluta lObject;
		int i;		
		lista = (ArrayList)this.reclutaManager.getReclutaExpLaboral(control.getIdRec());
		
		if(lista!=null){
			if(lista.size()>0){
				lObject = (Recluta) lista.get(0);
				control.setOrganizacion1(lObject.getOrganizacion());
				control.setPuesto1(lObject.getPuesto());
				control.setOtroPuesto1(lObject.getOtroPuesto());
				control.setFechaInicio1(lObject.getFecIni());
				control.setFechaFin1(lObject.getFecFin());
				control.setPersonaRef1(lObject.getReferencia());
				control.setTelefonoRef1(lObject.getTelefRef());
				control.setDesPuesto1(lObject.getFunciones());						
			}
		}
    }
	private void inicializaCombos(HojaVidaCommand command){
		command.setListaEstadoCivil(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTADO_CIVIL
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
		
    	/*command.setListaDepartamento(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTADO_CIVIL
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));*/
		command.setListaDepartamento(this.reclutaManager.getAllDepartamento());
    	
    	//command.setListaProvincia(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTADO_CIVIL
			//	, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
		//command.setListaProvincia(this.reclutaManager.getAllProvincia(departamento))
		
    	//command.setListaDistrito(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTADO_CIVIL
				//, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
    	command.setListaMoneda(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_MONEDA
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	
    	/*command.setListaAreaInteres(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT1
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	//Tabla Interes
    	command.setListaAreaInteresH(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT2
				, "0001", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));*/

    	command.setListaSedePrefTrabajo(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_SEDE_PREFERENTE
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaInteresEn(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_INTERES_EN
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaDedicacion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_DEDICACION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaDispoTrabajar(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_DISPONIBILIDAD
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	//COMBOS PARA ESTUDIOS
    	command.setListaGradoAcad1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_ACADEMICO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));    	
    	command.setListaAreaEstudio1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PROFESION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
    	command.setListaInstitucion1(this.tablaDetalleManager.getAllInstitucion(CommonConstants.GSTR_INSTI_EDUCATIVA));
    	
    	command.setListaMerito1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_MERITO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	//COMBO EXPERIENCIA LABORAL
    	command.setListaPuesto1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_CARGO_PUESTO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
	}
	
	public void inicializaDatos(HojaVidaCommand command, String codPersona)
	{
		//command.setIdRec(codPersona);
		Recluta lObject = this.reclutaManager.getAllDatosPersonaRecluta(codPersona);
		if ( lObject != null )
		{
	    	command.setNombres(lObject.getNombre());
			command.setApepat(lObject.getApepat());
			command.setApemat(lObject.getApemat());
			if (lObject.getFoto()!= null){
				//String ruta = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_POSTULANTE_FOTO;
				
				String ruta = CommonConstants.GSTR_HTTP+
		    	CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+
		    	CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_FOTO;
				
				command.setFoto( ruta + lObject.getFoto() );
				
			}
			command.setFecnac(lObject.getFecnac());
			command.setFecreg(lObject.getFecreg());
			command.setClave(lObject.getClave());
			command.setEmail(lObject.getEmail());
			command.setEmail1(lObject.getEmailAlterno());
			command.setSexo(lObject.getSexo());
			command.setNacionalidad(lObject.getNacionalidad());
			command.setDni(lObject.getDni());
			command.setRuc(lObject.getRuc());
			command.setEstadoCivil(lObject.getEstadoCivil());
			command.setAnioExpLaboral(lObject.getAnioExpLaboral());
			command.setPerfil(lObject.getPerfil());
			command.setDireccion(lObject.getDireccion());
			command.setDepartamento(lObject.getDepartamento());
			
			command.setListaProvincia(this.reclutaManager.getAllProvincia(lObject.getDepartamento()));
			
			command.setProvincia(lObject.getProvincia());
			command.setDistrito(lObject.getDistrito());
			
			command.setListaDistrito(this.reclutaManager.getAllDistrito(lObject.getProvincia()));
			
			command.setPais(lObject.getPais());
			command.setTelef(lObject.getTelef());
			command.setTelefAdicional(lObject.getTelefAdicional());
			command.setTelefMovil(lObject.getTelefMovil());
			command.setCodPostal(lObject.getCodPostal());
			
			command.setPostuladoAntes(lObject.getPostuladoAntes());
			command.setTrabajadoAntes(lObject.getTrabajadoAntes());
			command.setFamiliaTecsup(lObject.getFamiliaTecsup());
			command.setFamiliaNombres(lObject.getFamiliaNombres());
			command.setExpDocente(lObject.getExpDocente());
			command.setExpDocenteAnios(lObject.getExpDocenteAnios());
			command.setDispoViaje(lObject.getDispoViaje());
			command.setSedePrefTrabajo(lObject.getSedePrefTrabajo());
			command.setInteresEn(lObject.getInteresEn());
			command.setMoneda(lObject.getMoneda());
			command.setPretensionEconomica(lObject.getPretensionEconomica());
			command.setIndPago(lObject.getTipoPago());
			command.setDedicacion(lObject.getDedicacion());
			command.setDispoTrabajar(lObject.getDispoTrabajar());
		}
	}
}
