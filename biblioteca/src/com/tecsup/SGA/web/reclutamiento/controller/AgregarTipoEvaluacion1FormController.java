package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class AgregarTipoEvaluacion1FormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AgregarTipoEvaluacion1FormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	
    	AgregarTipoEvaluacion1Command command = new AgregarTipoEvaluacion1Command();
    	command.setCodEval((String)request.getParameter("txhCodEva"));
    	System.out.println(command.getCodEval());
    	String cantidad1 = (String)request.getParameter("txhCantidad1");
    	String codDetalle = request.getParameter("txhCodDetalle");
    	request.setAttribute("cantidad1",cantidad1);
    	command.setCodDetalle("");
    	command.setCantidad1(cantidad1);
    	if ( codDetalle != null ) if ( !codDetalle.trim().equals("") ) cargaDetalle(command, codDetalle);
    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    String resultado ="";
    	AgregarTipoEvaluacion1Command control = (AgregarTipoEvaluacion1Command) command;
    	request.setAttribute("cantidad1",control.getCantidad1()
    			);
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		resultado = InsertTablaDetalle(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if ( resultado.equals("-2")) 
    		{
    			control.setMsg("DOBLE");
    		}
    		else control.setMsg("OK");
    	
    	}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_agregar_tipo_evaluacion_1","control",control);		
    }
    
    private String InsertTablaDetalle(AgregarTipoEvaluacion1Command control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_EVA);
    		obj.setDescripcion(control.getDescripcion());
    		obj.setDscValor1(CommonConstants.PROC_ETAPA_SELECCION);
    		obj.setDscValor2(control.getDscValor2());
	
    		if (!control.getDescripcion().trim().equals("") )
    		obj.setCodTipoTablaDetalle(this.tablaDetalleManager.InsertarTablaDetalle(obj, control.getCodEval()));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(AgregarTipoEvaluacion1Command command, String codDetalle)
    {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
		
		obj = (TipoTablaDetalle)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
				,"", "", codDetalle,"", "", "", "", CommonConstants.TIPO_ORDEN_DSC).get(0);
		
		command.setDescripcion(obj.getDescripcion());		
		command.setCodDetalle(codDetalle);
    }
    
}
