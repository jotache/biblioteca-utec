package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.service.reclutamiento.*;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.common.CommonMessage;

public class ListaOfertasFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ListaOfertasFormController.class);
	
	private ReclutaManager reclutaManager;
	
	/*SETTER*/
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ListaOfertasCommand command = new ListaOfertasCommand();

    	/*Recogiendo valores*/
    	command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	ArrayList Ofertas = (ArrayList)this.reclutaManager.getAllOfertas();

    	if(Ofertas == null || Ofertas.size() == 0){
    		command.setMessage(CommonMessage.NO_OFERTAS);
			command.setTypeMessage("0");
		}
    	request.setAttribute("Ofertas", Ofertas);
    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	ListaOfertasCommand control = (ListaOfertasCommand) command;
		
    	if ("POST".equals(control.getOperacion())){
    		String rpta;
        	
    		rpta = postularOferta(control, control.getIdRec(), control.getCodOferta());
       
        	if ("0".equals(rpta)){
        		control.setMessage(CommonMessage.OFERTA_OK);
        		control.setTypeMessage("OK");
        	}
        	else if ("1".equals(rpta)){
        		control.setMessage(CommonMessage.OFERTA_ANTES);
        		control.setTypeMessage("1");
        	}
        	else{
        		control.setMessage(CommonMessage.RECLUTA_ERROR);
        		control.setTypeMessage("ERROR");
        	}
        	control.setOperacion("ACCESO");
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_listado_ofertas","control",control);		
    }
    
    private String postularOferta(ListaOfertasCommand command, String idRec, String codOferta){
		try{
			return this.reclutaManager.insertPostularOferta(idRec, codOferta);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}    
}
