package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ConstantesCuerpoMensajes;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Postulante;
import com.tecsup.SGA.service.reclutamiento.*;


public class OlvidoClaveFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(OlvidoClaveFormController.class);

	private ReclutaManager reclutaManager;
	private PostulanteManager postulanteManager;
	
	public void setPostulanteManager(PostulanteManager postulanteManager) {
		this.postulanteManager = postulanteManager;
	}

	/*SETTER*/
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	OlvidoClaveCommand command = new OlvidoClaveCommand();
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	OlvidoClaveCommand control = (OlvidoClaveCommand) command;
		
		if("ENVIAR".equals(control.getOperacion())){ //Ver Datos
    		String rpta="NO";
    		
        	//rpta = BuscarPostulanteByEmail(control);
        	
        	List lista= new ArrayList();
        	
        	String mensaje="";
        	ServletEnvioCorreo servletEnvioCorreo= new ServletEnvioCorreo();
        	ConstantesCuerpoMensajes cuerpo= new ConstantesCuerpoMensajes();
        	
        	lista=this.postulanteManager.GetPostulanteByEmail(control.getUsuario());
        	Postulante postulante= new Postulante();
        	if(lista!=null)
        	{ if(lista.size()>0)
        	  { postulante=(Postulante)lista.get(0);
        	  
        	    mensaje=cuerpo.CuerpoOlvidoContrasena(postulante.getNombres(), postulante.getEmail(), 
    	    		  postulante.getClave());
        	   
        	   try{
        		           		   
        	   servletEnvioCorreo.EnviarCorreo(postulante.getEmail(), CommonConstantsEmail.TECSUP_EMAIL, 
        	    		postulante.getNombres(), CommonConstantsEmail.TECSUP_EMAIL_ORIGEN, 
        	    		CommonConstantsEmail.TECSUP_ASUNTO_OLVIDO_PASS, mensaje);
        
        	    System.out.println("Texto a enviar en el Correo: \n"+mensaje);
        	    rpta="SI";
        	    log.info("Envio clave postulante satisfactoriamente");
        	   }
       	    	catch(Exception ex){
       	    	    rpta="ERROR";
       	    	    log.error(ex.getMessage());
       	       }
        	 }
        	}
        	System.out.println("Respuesta: "+rpta);
        	/*this.reclutaManager.getAllRecluta(control.getUsuario(), "");*/

        	if (rpta.equals("NO")){ //Postulante no existe
        		control.setMessage(CommonMessage.RECLUTA_NO_EXISTE);
        		control.setTypeMessage("ERROR");
        	}
        	else if (rpta.equals("SI")){ //Existe cuenta
        		control.setMessage(CommonMessage.ENVIO_EMAIL);
        		control.setTypeMessage("OK");
        	}else if(rpta.equals("ERROR")){
        		control.setMessage("No se pudo enviar los Datos al Correo Indicado");
        		control.setTypeMessage("ERROR");
        	}
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_olvido_clave","control",control);		
    }
      
    
}

