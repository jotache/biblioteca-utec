package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.ConsultasyReportesCommand;

public class ConsultasyReportesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultasyReportesFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
   
    	ConsultasyReportesCommand command = new ConsultasyReportesCommand();
    	command.setUsuario((String)request.getParameter("txhCodUsuario"));
    	
    	command.setListReportes(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_REPORTES_RECLUTAMIENTO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
    	command.setListProcesos(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTAPA_PROCESO
    				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
    	command.setListUniFuncional(this.tablaDetalleManager.getAllUnidadFuncional());
        
        
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	ConsultasyReportesCommand control = (ConsultasyReportesCommand) command;
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_agregar_calificaciones","control",control);		
    }
}
