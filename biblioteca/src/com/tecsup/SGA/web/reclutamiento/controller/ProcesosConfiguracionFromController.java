package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.ProcesoCommand;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.modelo.*;
import com.tecsup.SGA.common.*;

//nuvea factura correo solicitando.
public class ProcesosConfiguracionFromController extends SimpleFormController {

	private static Log log = LogFactory.getLog(ProcesosConfiguracionFromController.class);
	private ProcesoManager procesoManager;
	private TablaDetalleManager tablaDetalleManager;

	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ProcesoCommand command = new ProcesoCommand();
    	/*Recuperando al usuario*/
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	/*Llenando los combos.*/
    	inicializaCombos(command);
    	/*Veo si es un editar*/
    	String codProceso = (String)request.getParameter("txhCodProceso");
    	command.setFlagEtapa((String)request.getParameter("txhCodTipoEtapa"));
    	if ( codProceso != null ) 
    		if ( codProceso.trim() != "" )
    		{
    	    	command.setListProcRevAsociado(
    	    				this.procesoManager.getAllProcesosParaRevision(
    	    					CommonConstants.EST_REV_PRESELECCIONADO, codProceso));
    			cargaProceso(codProceso, command);
    		}
        log.info("formBackingObject:FIN");
    	//request.getSession().setAttribute("listaAreasInteres",command.getListAreasInteres());
    	//request.getSession().setAttribute("listaAreasEstudios", command.getListAreaEstudio());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	log.info("onSubmit:INI");
    	String resultado;
    	
    	ProcesoCommand control = (ProcesoCommand) command;
    	log.info("Operación:"+control.getOperacion());
    	
    	if ( control.getOperacion().trim().equals("GRABAR"))
    	{
        	control.setOperacion("");
    		resultado = grabarProceso(control);
    		log.info("resultado:"+resultado);
    		if ( resultado != null)
    			if ( resultado.trim().equals("") || resultado.trim().equals("-1") )
    			{
    				control.setMessage(CommonMessage.GRABAR_ERROR);
    			}
    			else if ( resultado.trim().equals("-2") )
    			{
    				control.setMessage(CommonMessage.PROCESO_DUPLICADO);
    			}
    			else{
    				//control.setCodProceso(resultado);
    				cargaProceso(resultado.trim(), control);
    				control.setMessage(CommonMessage.GRABAR_EXITO);
    				control.setOperacion("OK");
    			} 
    		else control.setMessage(CommonMessage.GRABAR_ERROR);
    	}
    	request.getSession().setAttribute("listaAreasInteres",control.getListAreasInteres());
 
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/procesos/rec_procesosconfig","control",control);		
    }

	private String grabarProceso(ProcesoCommand command)
	{
		StringTokenizer st; 
		String resultado = "";
		Proceso proceso = new Proceso();

		//log.info("command.getFlagEtapa().trim():"+command.getFlagEtapa().trim());
		//log.info("command.getCodAreasIntSeleccionadas():"+command.getCodAreasIntSeleccionadas());
		
		try
		{
			proceso.setCodProceso(command.getCodProceso());
			proceso.setDscProceso(command.getDscProceso());
			proceso.setCodUnidadFuncional(command.getCodUnidadFuncional());
			proceso.setFlagEtapa(command.getFlagEtapa());
			proceso.setUsuario(command.getCodUsuario());
			
			if (command.getFlagEtapa().trim().equals("1")) 
				proceso.setCodEtapa(CommonConstants.PROC_ETAPA_REVISION);
			else 
				proceso.setCodEtapa(CommonConstants.PROC_ETAPA_SELECCION); 
			
			if (command.getFlagEtapa().trim().equals("1"))
			{
				//////////// Falta el listado de area de interes.
				proceso.setCodAreasInteres(command.getCodAreasIntSeleccionadas());
				proceso.setCodAreasEstudios(command.getCodAreasEstSeleccionadas());
				proceso.setCodAreasNivelEstudios(command.getCodAreasNivEstSeleccionadas());
				proceso.setCodInsEdu(command.getCodInstEduSeleccionadas());
				System.out.println("proceso.getCodAreasInteres():" + proceso.getCodAreasInteres());
				System.out.println("proceso.getCodAreasEstudios():" + proceso.getCodAreasEstudios());
				System.out.println("proceso.getCodAreasNivelEstudios():" + proceso.getCodAreasNivelEstudios());
				System.out.println("proceso.getCodInsEdu():" + proceso.getCodInsEdu());
				
				proceso.setCodSexo(command.getCodSexo());
				proceso.setCodAreaEstudio(command.getCodAreaEstudio());
				proceso.setCodGradoAcedemico(command.getCodGradoAcedemico());
				proceso.setCodInstitucionAcademica(command.getCodInstitucionAcademica());
				proceso.setSalarioIni(command.getSalarioIni());
				proceso.setSalarioFin(command.getSalarioFin());
				proceso.setCodTipoEdad(command.getCodTipoEdad());
				proceso.setEdad(command.getEdad());
				proceso.setCodTipoEdad1(command.getCodTipoEdad1());
				proceso.setEdad1(command.getEdad1());
				proceso.setCodDedicacion(command.getCodDedicacion());
				proceso.setAnhosExperienciaDocencia(command.getAnhosExperienciaDocencia());
				proceso.setCodInteresado(command.getCodInteresado());
				
				proceso.setFlagDisponibilidadViajar(command.getFlagDisponibilidadViajar()
						+ "|" + command.getFlagDisponibilidadViajar1());
				
				proceso.setPuestoPostula(command.getPuestoPostulaDoc()+"|"+command.getPuestoPostulaAdm());
				
				proceso.setAnhosExperiencia(command.getAnhosExperiencia());
				proceso.setCodTipoMoneda(command.getCodTipoMoneda());
			}
			else
			{
				st = new StringTokenizer(command.getCodProcRevAsociado(),"|");
				if (st.countTokens() > 0)
				{
					proceso.setCodProcRevAsociado(st.nextToken());
				}
			}
				
			log.info("proceso.getCodAreasIntSeleccionadas():"+proceso.getCodAreasInteres());
			
			resultado = this.procesoManager.grabarProceso(proceso);
			//resultado = "10";
			
			log.info("grabarProceso.resultado 1:" + resultado);
			
			if (command.getFlagEtapa().trim().equals("1")){
				//si es etapa revision... actualizamos los postulantes que cumplen los criterios de seleccion de este proceso:
				if (Integer.valueOf(resultado)>=0 ){ // no hay error
					resultado = this.procesoManager.cargarPostulantesXProceso(proceso.getCodProceso(), proceso.getUsuario());					
				}
			}			
			
			return resultado;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	private void inicializaCombos(ProcesoCommand command)
	{
    	command.setListUnidadFuncional(tablaDetalleManager.getAllUnidadFuncional());
    	command.setListSexo(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_SEXO    			
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	/*command.setListAreaEstudio(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PROFESION
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));*/
    	command.setListGradoAcedemico(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_ACADEMICO
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListInstitucionAcademica(tablaDetalleManager.getAllInstitucion(CommonConstants.GSTR_INSTI_EDUCATIVA));
    	command.setListTipoEdad(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_SIMBOLO
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListDedicacion(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_DEDICACION
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListInteresado(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_INTERES_EN
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
    	command.setListProcRevAsociado(this.procesoManager.getAllProcesosParaRevision(CommonConstants.EST_REV_PRESELECCIONADO
    										, command.getCodProceso()));
    	command.setListaTipoMoneda(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_MONEDA
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
	}
	
	private void cargaProceso(String codProceso, ProcesoCommand command)
	{   
		System.out.println("cargaProceso:codProceso: "+codProceso);
		Proceso proceso = this.procesoManager.getProceso(codProceso);
		String strFlagIndViaje = "";
		String strFlagPuestoPostula = "";
		StringTokenizer st;
		
		Proceso procesoAux = null;
		StringTokenizer stProcAsoc;
		if ( proceso != null )
		{
			command.setCodProceso(codProceso);
			command.setDscProceso(proceso.getDscProceso());
			command.setCodUnidadFuncional(proceso.getCodUnidadFuncional());
			command.setCodSexo(proceso.getCodSexo());
			command.setCodAreaEstudio(proceso.getCodAreaEstudio());
			command.setCodGradoAcedemico(proceso.getCodGradoAcedemico());
			command.setCodInstitucionAcademica(proceso.getCodInstitucionAcademica());
			command.setSalarioIni(proceso.getSalarioIni());
			command.setSalarioFin(proceso.getSalarioFin());
			command.setCodTipoEdad(proceso.getCodTipoEdad());
			command.setEdad(proceso.getEdad());
			command.setCodTipoEdad1(proceso.getCodTipoEdad1());
			command.setEdad1(proceso.getEdad1());
			command.setCodDedicacion(proceso.getCodDedicacion());
			command.setAnhosExperiencia(proceso.getAnhosExperiencia());
			command.setAnhosExperienciaDocencia(proceso.getAnhosExperienciaDocencia());
			command.setCodInteresado(proceso.getCodInteresado());
			command.setCodTipoMoneda(proceso.getCodTipoMoneda());
			//System.out.println("CodTipoMoneda: "+command.getCodTipoMoneda());
			if (proceso.getCodEtapa().trim().equals(CommonConstants.PROC_ETAPA_REVISION)) 
				command.setFlagEtapa("1");
			else{
				command.setFlagEtapa("2");
				/*Setenado el proceso relacionado*/
				if ( command.getListProcRevAsociado() != null )
					for ( int x=0; x < command.getListProcRevAsociado().size(); x++)
					{
						procesoAux = (Proceso)command.getListProcRevAsociado().get(x);
						stProcAsoc = new StringTokenizer(procesoAux.getCodProceso(),"|");
						if (stProcAsoc.countTokens() > 0)
							if ( proceso.getCodProcRevAsociado().trim().equals(stProcAsoc.nextToken()) )
								command.setCodProcRevAsociado(procesoAux.getCodProceso());
					}
				//command.setCodProcRevAsociado(proceso.getCodProcRevAsociado());
			}
			
			strFlagIndViaje = proceso.getFlagDisponibilidadViajar();			
			
			st = new StringTokenizer(strFlagIndViaje,"|");
			if ( st.hasMoreTokens() )
			{
				command.setFlagDisponibilidadViajar(st.nextToken());
				command.setFlagDisponibilidadViajar1(st.nextToken());
			}
			
			strFlagPuestoPostula = proceso.getPuestoPostula();
			st = new StringTokenizer(strFlagPuestoPostula,"|");
			if ( st.hasMoreTokens() ){
				command.setPuestoPostulaDoc(st.nextToken());
				command.setPuestoPostulaAdm(st.nextToken());
			}
							
			cargaListaAreasSeleccionadas(command, proceso);
			cargaListaAreasEstudiosSeleccionadas(command, proceso);
			cargaListaAreasNIvelEstudiosSeleccionadas(command, proceso);
			cargaListaInstEduSeleccionadas(command, proceso);
		}
	}
	
	private void cargaListaAreasSeleccionadas(ProcesoCommand command, Proceso proceso)
	{
		int i;
		ArrayList listAreas = null;
		command.setCodAreasIntSeleccionadas("");
		TipoTablaDetalle tipoTablaDetalle = null;
		if ( proceso != null )
		{
			listAreas = (ArrayList) proceso.getListAreasInteres();
			command.setListAreasInteres(listAreas);
			if ( listAreas != null )
				for( i = 0; i < listAreas.size() ; i++)
				{
					tipoTablaDetalle = (TipoTablaDetalle)listAreas.get(i);
					command.setCodAreasIntSeleccionadas(command.getCodAreasIntSeleccionadas() 
							+  tipoTablaDetalle.getCodTipoTablaDetalle()
							+ "|");
				}
		}
	}
	
	private void cargaListaAreasEstudiosSeleccionadas(ProcesoCommand command, Proceso proceso){
		int i;
		ArrayList listaAreasEstudios = null;
		command.setCodAreasEstSeleccionadas("");
		TipoTablaDetalle procesoAreEst = null;
		if (proceso!=null){
			listaAreasEstudios = (ArrayList) proceso.getListAreasEstudios();
			command.setListAreaEstudio(listaAreasEstudios);
			if (listaAreasEstudios!=null){
				for(i=0;i<listaAreasEstudios.size();i++){
					procesoAreEst = (TipoTablaDetalle) listaAreasEstudios.get(i);
					command.setCodAreasEstSeleccionadas(command.getCodAreasEstSeleccionadas()
							+ procesoAreEst.getCodTipoTablaDetalle()
							+ "|");
				}
			}
		}
	}
	
	private void cargaListaAreasNIvelEstudiosSeleccionadas(ProcesoCommand command, Proceso proceso){
		int i;
		ArrayList listaAreasEstudios = null;
		command.setCodAreasNivEstSeleccionadas("");		
		TipoTablaDetalle procesoAreEst = null;
		if (proceso!=null){
			listaAreasEstudios = (ArrayList) proceso.getListAreasNivelEstudios();
			command.setListAreasNivelEstudios(listaAreasEstudios);
			if (listaAreasEstudios!=null){
				for(i=0;i<listaAreasEstudios.size();i++){
					procesoAreEst = (TipoTablaDetalle) listaAreasEstudios.get(i);
					command.setCodAreasNivEstSeleccionadas(command.getCodAreasNivEstSeleccionadas()
							+ procesoAreEst.getCodTipoTablaDetalle()
							+ "|");					
				}
			}
		}
	}
	
	private void cargaListaInstEduSeleccionadas(ProcesoCommand command, Proceso proceso){
		int i;
		ArrayList listaAreasEstudios = null;
		command.setCodInstEduSeleccionadas("");		
		TipoTablaDetalle procesoAreEst = null;
		if (proceso!=null){
			listaAreasEstudios = (ArrayList) proceso.getListlistInstitucionesEdu();
			command.setListInstEdu(listaAreasEstudios);
			if (listaAreasEstudios!=null){
				for(i=0;i<listaAreasEstudios.size();i++){
					procesoAreEst = (TipoTablaDetalle) listaAreasEstudios.get(i);
					command.setCodInstEduSeleccionadas(command.getCodInstEduSeleccionadas()
							+ procesoAreEst.getCodTipoTablaDetalle()
							+ "|");
					
				}
			}
		}
	}
}
