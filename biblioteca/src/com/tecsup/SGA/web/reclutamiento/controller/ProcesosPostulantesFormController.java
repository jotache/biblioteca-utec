package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.EvaluacionesProcesoCommand;
import com.tecsup.SGA.web.reclutamiento.command.ProcesosPostulantesCommand;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.EvaluacionProcesosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Postulante;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;

public class ProcesosPostulantesFormController  extends SimpleFormController{
	private static Log log = LogFactory.getLog(ProcesosPostulantesFormController.class);
	private ProcesoManager procesoManager;
	private EvaluacionProcesosManager evaluacionProcesosManager;
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}
	public void setEvaluacionProcesosManager(
			EvaluacionProcesosManager evaluacionProcesosManager) {
		this.evaluacionProcesosManager = evaluacionProcesosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ProcesosPostulantesCommand command = new ProcesosPostulantesCommand();
    	
    	String codProceso = (String)request.getParameter("txhCodProceso");
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	//AGREGADO NAPA
    	command.setTipoEtapa(request.getParameter("txhTipoEtapa") == null ? "" : (String)request.getParameter("txhTipoEtapa") );
    	command.setCodUniFun(request.getParameter("txhCodUniFun") == null ? "" : (String)request.getParameter("txhCodUniFun") );
    	//fechas del la bandeja para que cuando se regrese a la bandeja permanescan seleccionadoas las fechas
    	command.setFecIni(request.getParameter("txhFecIni") == null ? "" : (String)request.getParameter("txhFecIni") );
    	command.setFecFin(request.getParameter("txhFecFin") == null ? "" : (String)request.getParameter("txhFecFin") );
    	
    	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
		String fecha1 =  sdf.format(Fecha.getSumaFechaDias("-90", Fecha.getCurrentDate())).toString();
    	String fecha2 = sdf.format(Fecha.getCurrentDate()).toString();	
    	command.setFecIniPost(fecha1);
    	command.setFecFinPost(fecha2);
    	command.setTipoVistaFecha1("1");
    	command.setTipoVistaFecha2("0");
    	
    	//*************
    	//String ruta = CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_IP + CommonConstants.STR_RUTA_POSTULANTE;
    	
    	String ruta = CommonConstants.GSTR_HTTP+
				    	CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+
				    	CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE;
    	
		request.setAttribute("msgRutaServer",ruta);
		
    	if ( codProceso != null )
    		if ( !codProceso.trim().equals("") )
	    	{
	    		cargaDatosProceso(command, codProceso);
	    		listaTodosPostulantes(command,"NOM");
	    		//command.setListPostulantes(this.procesoManager.getAllPostulantes(codProceso
	    			//				, command.getCodEtapa(), "", ""));
	    	}    	
    	request.getSession().setAttribute("listaPostulantes", command.getListPostulantes());
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	String respuesta;
    	ProcesosPostulantesCommand control = (ProcesosPostulantesCommand) command;
    	
    	if ( control.getOperacion().trim().equals("BUSCAR"))
    	{
    		//control.setListPostulantes(this.procesoManager.getAllPostulantes(control.getCodProceso()
				//	, control.getCodEtapa(), control.getNombres(), control.getApellidos()));
    		String orden = request.getParameter("ordenConsulta");
    		if(orden==null) orden="vacio";
    		log.info("Orden busqueda:" + orden);
    		listaTodosPostulantes(control,orden);
    	}
    	else if ( control.getOperacion().trim().equals("VER_CV") )
    	{
    		listaTodosPostulantes(control,"NOM");
    		//control.setListPostulantes(this.procesoManager.getAllPostulantes(control.getCodProceso()
					//, control.getCodEtapa(), control.getNombres(), control.getApellidos()));
    	}
    	else if ( control.getOperacion().trim().equals("ELIMINAR") )
    	{
    		respuesta = this.procesoManager.deletePostulanteByProceso(control.getCodPostulantesSel(),
    				control.getCodProceso(), "", control.getCodUsuario());
    		
    		if ( respuesta == null ) control.setMessage("ERROR");    	
    		else if ( respuesta.trim().equals("-1") ) control.setMessage("ERROR");
    		else if ( respuesta.trim().equals("0") ) 
    		{
    			control.setMessage("OK");
    			listaTodosPostulantes(control,"NOM");
        		//control.setListPostulantes(this.procesoManager.getAllPostulantes(control.getCodProceso()
    				//	, CommonConstants.PROC_ETAPA_REVISION, control.getNombres(), control.getApellidos()));
    		}    		
    	}
    	else if ( control.getOperacion().trim().equals("EVALUAR") )
    	{

    		respuesta = this.evaluacionProcesosManager.insertEvaluacionPostulante(
    				control.getCodPostulantesSel(), 
    				control.getCodProceso(), 
    				CommonConstants.PROC_ETAPA_SELECCION, 
    				"", CommonConstants.EST_SEL_ENVIADO_EVAL, 
    				"", "", "", "", CommonConstants.EVAL_ACT_ESTADO, 
    				control.getCodUsuario());
    				
    		//respuesta="0";
    		enviarMail(control);
    		
    		if ( respuesta == null ) control.setMessage("EVA_ERROR");    	
    		else if ( respuesta.trim().equals("-1") ) control.setMessage("EVA_ERROR");
    		else if ( Long.valueOf(respuesta)>0 ) 
    		{
    			control.setMessage("EVA_OK");
    			listaTodosPostulantes(control,"NOM");
        		//control.setListPostulantes(this.procesoManager.getAllPostulantes(control.getCodProceso()
    				//	, control.getCodEtapa(), control.getNombres(), control.getApellidos()));
    		}  
    		
    	}else if (control.getOperacion().trim().equals("ENVIARJEFE")){
    		//System.out.println("control.getCodPostulantesSel():"+control.getCodPostulantesSel());
    		respuesta = enviarJefe(control);
    		if ( respuesta == null ) control.setMessage("ENVJEFE_ERROR");
    		else if (respuesta.trim().equals("-1")) control.setMessage("ENVJEFE_ERROR");
    		else if (respuesta.trim().equals("0"))
    		{
    			control.setMessage("EVA_OK");
    			listaTodosPostulantes(control,"NOM");
        		//control.setListPostulantes(this.procesoManager.getAllPostulantes(control.getCodProceso()
    				//	, control.getCodEtapa(), control.getNombres(), control.getApellidos()));
    		}    		
    	}
    	
    	
    	control.setOperacion("");
    	control.setCodPostulantesSel("");
    	control.setNomPostulantesSel("");
    	control.setEstPostulantesSel("");
    	control.setCodPostulanteCV("");
    	
    	//request.getSession().setAttribute("listaPostulantes", control.getListPostulantes());
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/procesos/rec_procesospostulantes","control",control);		
    }
    
    public String enviarJefe(ProcesosPostulantesCommand control){
    	try
    	{
    		StringTokenizer stPostulantes = new StringTokenizer(control.getCodPostulantesSel(),"|");
    		String cadenaProceso = "";
    		String respuesta="";
    		
    		//ENVIAR MAIL AL EVALUADOR...evaluando calificación de los postulantes seleccionados para enviarlos al evaluador
    		
    		for( int i = 0; i < stPostulantes.countTokens(); i++)
        		cadenaProceso = cadenaProceso +  control.getCodProceso() + "|";
    		
    		System.out.println("control.getCodPostulantesSel():"+control.getCodPostulantesSel()); 
    		//respuesta = "0";
			respuesta = this.evaluacionProcesosManager.updateEvaluacionPostulanteEnvio(
					control.getCodPostulantesSel(), 							
					cadenaProceso, 
					CommonConstants.PROC_ETAPA_REVISION, 
					CommonConstants.EST_REV_REV_X_RRHH, 
					control.getCodUsuario());
			
			//enviar mail a evaluador si al menos uno de los postulantes es APTO.
			//control.getCodPostulantesSel()
    		
    		enviarMail(control);    		    		    		
    		
			return respuesta;
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return null;
    }
    
    private void enviarMail(ProcesosPostulantesCommand control){
		List<Postulante> postulantes = new ArrayList<Postulante>();
		postulantes = evaluacionProcesosManager.getPostulantesEnviadosJefeDpto(control.getCodPostulantesSel(), control.getCodProceso(),control.getCodEtapa());

		
		//verificando el estado de los postulantes seleccionados: si al menos uno tiene el estado 'APTO' entonces enviar mail al Jefe de Departamento:
		boolean enviar = false;
    	java.util.ResourceBundle prop = java.util.ResourceBundle.getBundle("db");
    	String entorno = prop.getString("db.environment");
    	
    	if (entorno.equals("produccion")){
    		String calificacion="";
    		if (postulantes!=null){
    			for (Postulante postulante : postulantes){
    				calificacion = postulante.getUltEstadoPostulRevision();    				    				
    				if (!calificacion.equals(""))
	    				if (calificacion.substring(0, 4).equals(CommonConstants.REC_CODIGO_APTO))
	    					enviar=true;	    			
    					    				
    			}			
    		}
    		
    		if (control.getCodEtapa().equals("0001"))
    			enviar=true; //POR DEFINIR:Siempre enviar a pesar de que no hay aptos dentro de los postulantes enviados.
    		else
    			enviar=true; //SIEMPRE. //en etapa de seleccion cuando se usa "ENVIAR A EVALUADOR" los postulantes no tienen calificación.
    		
    		if (enviar=true){
    			//enviar mensaje al Jefe Dpto: (Revision) - Etapa Revisar Curriculum Vitae.
    			//listar los evaluadores y tipo de evaluaciones del proceso:
    			//List tiposEvaluacion = procesoManager.getEvaluadorsByProceso(control.getCodProceso());
    			List<Evaluador> tiposEvaluacion = procesoManager.getEvaluadoresAsignadosXProceso(control.getCodProceso()) ;
    			
    			if (tiposEvaluacion!=null){
    				if (tiposEvaluacion.size()>0){
    					boolean bEnviado=false; 
    					for (Evaluador tipoEvaluacion:tiposEvaluacion){
    						//solo se envia al mensaje al primer
    						if (bEnviado==false){
	    						if (!tipoEvaluacion.getCodEvaluador().equals("0")){
	    							bEnviado=true;
	    							//Solo enviar mensaje al evaluador si estamos en la primera evaluacion(no necesariamente puede ser la nro 1)... por que al resto se le envía mensaje solo cuando cada etapa este con todos los postulantes revisados y haya al menos un apto.
	    							//Como la lista esta ordenada solo enviar mensaje en el primer loop que tenga codigoevaluador<>0 
	    							
		    						Evaluador evaluador = procesoManager.getEvaluador(control.getCodProceso(), tipoEvaluacion.getCodTipoEvaluacion(),
		    								control.getCodEtapa()
		    								);
		    						String mensaje , asunto = "";
		    						List listaDatosMail = this.tablaDetalleManager.getAllTablaDetalle(
		    								CommonConstants.TIPT_TIPO_RECLUTAMIENTO_EMAIL, 	    								
		    								( control.getCodEtapa().equals("0002")?"0007":"0003")	    								
		    								, "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
		    						if (listaDatosMail!=null){
		    							//armando el mensaje...
		    							TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
		    							tipoTablaDetalle = (TipoTablaDetalle) listaDatosMail.get(0);
		    							
		    							asunto = tipoTablaDetalle.getDscValor1();
		    							asunto = asunto.replace("NOM_PROCESO", control.getDscProceso());
		    							
		    							mensaje = tipoTablaDetalle.getDescripcion();
		    							mensaje = mensaje.replace("NOM_EVALUADOR", evaluador.getDscEvaluador());
		    							mensaje = mensaje.replace("NOM_EVALUACION", tipoEvaluacion.getDscTipoEvaluacion());
		    							mensaje = mensaje.replace("NOM_PROCESO", control.getDscProceso());
		    							
		    							System.out.println("evaluador.getCorreo():" + evaluador.getCorreo());
		    				    		System.out.println("CommonConstantsEmail.TECSUP_EMAIL:" + CommonConstantsEmail.TECSUP_EMAIL);
		    				    		System.out.println("evaluador.getDscEvaluador():" + evaluador.getDscEvaluador());
		    				    		System.out.println("From - Nombre:" + tipoTablaDetalle.getDscValor2());    	    		
		    				    		System.out.println("Texto a enviar en el Correo: \n"+ asunto + "\n" + mensaje);
		    				    		
		    				    		//TODO: Habilitar Envio de Correo (YA ESTA HABILITADO)
		    				    		
		    				    		try {
		    				    			
		    				    			
		    				    			
		    				    			ServletEnvioCorreo envioCorreo = new ServletEnvioCorreo();
		    				    			envioCorreo.EnviarCorreo(evaluador.getCorreo(), CommonConstantsEmail.TECSUP_EMAIL, 
		    				    					evaluador.getDscEvaluador(), tipoTablaDetalle.getDscValor2(), 
		    				        	    		asunto, mensaje);
		    				    		 }catch(Exception ex){
		    					    	    ex.printStackTrace();
		    					    	    log.error(ex.getMessage(), ex);
		    				    		}
		    						}
	    						
	    						}
    						
    						}else{
    							//no mover
    							break;
    						}
    					}
    				}
    			}
    		}
    	}

		
    }
    
    private void cargaDatosProceso(ProcesosPostulantesCommand command, String codProceso)
    {
    	Proceso proceso = new Proceso();
    	proceso = this.procesoManager.getProceso(codProceso);
    	
    	command.setCodProceso(codProceso);
    	if ( proceso != null )
    	{
    		command.setCodProceso(proceso.getCodProceso());
    		command.setDscProceso(proceso.getDscProceso());
    		command.setCodOperta(proceso.getCodOfertaAsociada());
    		command.setCodEtapa(proceso.getCodEtapa());
    		command.setDscEtapa(proceso.getDscEtapa());
    		command.setCodProcesoRel(proceso.getCodProcRevAsociado());
    		command.setDscOferta(proceso.getDscOfertaAsociada());
    		command.setNroEvaluadores(proceso.getListaEvaluadores().size());
    		System.out.println("NroEvaluadores:" + command.getNroEvaluadores());
    	}
    	
    	
    }
    
    private void listaTodosPostulantes(ProcesosPostulantesCommand control,String orden){
    	List lista = new ArrayList();
    	lista = this.procesoManager.getAllPostulantes(control.getCodProceso()
				, control.getCodEtapa(), control.getNombres(), control.getApellidos(),orden);
    	Postulante postulante;
    	Postulante postLista;
    	if (lista!=null){
    		control.setListPostulantes(new ArrayList<Postulante>());
    		for (int i=0; i < lista.size() ; i++){
    			postulante = new Postulante();
    			postLista =(Postulante) lista.get(i);
    			//postulante.set
    			postulante.setCodPostulante(postLista.getCodPostulante());
    			postulante.setNombres(postLista.getNombres());
    			postulante.setPerfil(postLista.getPerfil());
    			postulante.setPretension_economica(postLista.getPretension_economica());
    			postulante.setProfesion(postLista.getProfesion());
    			postulante.setGradoInstruccion(postLista.getGradoInstruccion());
    			postulante.setCodEstado(postLista.getCodEstado());
    			postulante.setDscEstado(postLista.getDscEstado());
    			postulante.setUltEstadoPostulRevision(postLista.getUltEstadoPostulRevision());
    			postulante.setHistorico(postLista.getHistorico());
    			postulante.setFecreg(postLista.getFecreg());
    			postulante.setFechaModificacion(postLista.getFechaModificacion());
    			//System.out.println("control.getCodEtapa():" + control.getCodEtapa());
    			//System.out.println("control.getCodProceso():" + control.getCodProceso());
    			//System.out.println("postLista.getCodPostulante():" + postLista.getCodPostulante());    			
    			if (control.getCodEtapa().equals("0002")){
    				postulante.setEvaluaciones(this.procesoManager.getTrazaEvaluaciones(control.getCodProceso(), postLista.getCodPostulante()));
    				if (postulante.getEvaluaciones()!=null){
    					//System.out.println("size:"  + postulante.getEvaluaciones().size());
    				}
    			}
    			//System.out.println("11:");    			
    			control.getListPostulantes().add(postulante);
    			//System.out.println("12:");
    		}
    	}
   
    	/*control.setListPostulantes(this.procesoManager.getAllPostulantes(control.getCodProceso()
				, control.getCodEtapa(), control.getNombres(), control.getApellidos()));    */	
    }
}
