package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;

import com.tecsup.SGA.service.reclutamiento.*;
import com.tecsup.SGA.modelo.*;

public class DatosPersonalesAreasInteresFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(DatosPersonalesAreasInteresFormController.class);

	private TablaDetalleManager tablaDetalleManager;
	private ReclutaManager reclutaManager;

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
 	
    	DatosPersonalesCommand command = new DatosPersonalesCommand();
    	
    	command.setIdRec((String)request.getParameter("txhIdRec"));    	  	

    	inicializaAreaInteres(command);
    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
   

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	DatosPersonalesCommand control = (DatosPersonalesCommand) command;    	
    	//log.info("<"+control.getOperacion()+">");
    	/*if("AGREGAR_INTERES".equals(control.getOperacion())){
    		
    		control.setListaAreaInteresByPost(this.reclutaManager.getReclutaAreaInteresEspecial(control.getIdRec(), 
    				control.getCadenaAI()));
    		
    	}*/
    	
		//log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_datos_personales_areas_interes","control",control);		
    }
    
    private void inicializaAreaInteres(DatosPersonalesCommand command){
    	
    	
    	System.out.println("Cod Rec>>"+command.getIdRec()+"<<");
    	//log.info("1>>"+command.getIdRec()+">>");
    	//log.info("2>>"+command.getAreaInteres()+">>");
    	
		/*command.setListaAreaInteres(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT1
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
		//Tabla Interes
    	command.setListaAreaInteresByPost(this.reclutaManager.getReclutaAreaInteresEspecial(command.getIdRec(), 
    										command.getAreaInteres()));*/
    	command.setListaAreaInteresByPost(this.reclutaManager.getReclutaAreaInteres(command.getIdRec()));
    	
    	log.info("TAMA�O>>"+command.getListaAreaInteresByPost().size()+">>");
    	
	}

    /*SETTER*/
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
    
}
