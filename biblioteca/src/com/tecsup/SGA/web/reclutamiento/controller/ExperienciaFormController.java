package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.reclutamiento.*;

import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.exception.FormatoInvalidoException;

public class ExperienciaFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(ExperienciaFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	private ReclutaManager reclutaManager;
	
	/*SETTER*/
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	ExperienciaCommand command = new ExperienciaCommand();
    	
    	/*Llenado de los combos */
    	inicializaCombos(command);
    	
    	/*Fecha*/
    	command.setFecha(Fecha.getFechaActual());
    	
    	/*Recogiendo valores*/
    	command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	
    	if("ACCESO".equals(command.getOperacion())){ //Ver Datos
    		inicializaDatosExpLab(command, command.getIdRec());
    	}
    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	ExperienciaCommand control = (ExperienciaCommand) command;
		
    	if("GRABAR".equals(control.getOperacion())){
    		
    		if (control.getCodUsuario().equals("")) control.setCodUsuario(control.getIdRec());
    		
    		String rpta = "";
    		String [] strExpLab = control.getCadenaExp().split("_");
    		String cadena;
    		String valor1, valor2, valor3, valor4, valor5, valor6, valor7, valor8, valor9;
    		int i;
    		
    		for (i = 0 ; i < strExpLab.length ; i++){
    			cadena = strExpLab[i];
    			
    			String [] stkCadena = cadena.split("\\$");
    			valor1 = stkCadena[0];
				valor2 = stkCadena[1];
				valor3 = stkCadena[2];
				valor4 = stkCadena[3];
				valor5 = stkCadena[4];
				valor6 = stkCadena[5];
				valor7 = stkCadena[6];
				valor8 = stkCadena[7];
				valor9 = stkCadena[8]; //codExpLab

    			if(!("".equals(valor9))){
    				rpta = actualizarExperiencia(control.getIdRec(), valor9, valor1, valor2, valor3, valor4, valor5, valor6, valor7, valor8,control.getCodUsuario());
    			}
    			else{
    				rpta = insertarExperiencia(control.getIdRec(), valor1, valor2, valor3, valor4, valor5, valor6, valor7, valor8,control.getCodUsuario());
    			}
    			
    			if ("-1".equals(rpta)){
        			control.setMessage(CommonMessage.RECLUTA_ERROR);
        			control.setTypeMessage("ERROR");
        			return null;
            	}
        		else{
        			control.setOperacion("ACCESO");
            		control.setMessage(CommonMessage.GRABAR_EXITO);
            		control.setTypeMessage("OK");    	
        		}
    		}
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_experiencia","control",control);		
    }
	
	/*Metodos*/
	private void inicializaCombos(ExperienciaCommand command){
		command.setListaPuesto1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_CARGO_PUESTO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaPuesto2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_CARGO_PUESTO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaPuesto3(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_CARGO_PUESTO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
	}
	
	private void inicializaDatosExpLab(ExperienciaCommand command, String codPersona){
		ArrayList lista;
		Recluta lObject;
		int i;
		
		lista = (ArrayList)this.reclutaManager.getReclutaExpLaboral(codPersona);
		if (lista.size()>0){
			for( i = 0; i < lista.size() ; i++){
				lObject = (Recluta) lista.get(i);
				
				switch (i) {
					case 0:
						command.setOrganizacion1(lObject.getOrganizacion());
						command.setPuesto1(lObject.getPuesto());
						command.setOtroPuesto1(lObject.getOtroPuesto());
						command.setFechaInicio1(lObject.getFecIni());
						command.setFechaFin1(lObject.getFecFin());
						command.setPersonaRef1(lObject.getReferencia());
						command.setTelefonoRef1(lObject.getTelefRef());
						command.setDesPuesto1(lObject.getFunciones());
						command.setCodExpLab1(lObject.getCodExpLaboral());
						break;
					case 1:
						command.setOrganizacion2(lObject.getOrganizacion());
						command.setPuesto2(lObject.getPuesto());
						command.setOtroPuesto2(lObject.getOtroPuesto());
						command.setFechaInicio2(lObject.getFecIni());
						command.setFechaFin2(lObject.getFecFin());
						command.setPersonaRef2(lObject.getReferencia());
						command.setTelefonoRef2(lObject.getTelefRef());
						command.setDesPuesto2(lObject.getFunciones());
						command.setCodExpLab2(lObject.getCodExpLaboral());
						break;
					case 2:
						command.setOrganizacion3(lObject.getOrganizacion());
						command.setPuesto3(lObject.getPuesto());
						command.setOtroPuesto3(lObject.getOtroPuesto());
						command.setFechaInicio3(lObject.getFecIni());
						command.setFechaFin3(lObject.getFecFin());
						command.setPersonaRef3(lObject.getReferencia());
						command.setTelefonoRef3(lObject.getTelefRef());
						command.setDesPuesto3(lObject.getFunciones());
						command.setCodExpLab3(lObject.getCodExpLaboral());
						break;
				}
			}
		}
	}
	
	private String insertarExperiencia(String idRec, String valor1, String valor2, String valor3, 
			String valor4, String valor5, String valor6, String valor7, String valor8,String codUsuario){
		try{
		return this.reclutaManager.insertReclutaExpLaboral(idRec, valor1, valor2, valor3, valor4, valor5, valor6, 
									valor7, valor8,codUsuario);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	private String actualizarExperiencia(String idRec, String codExpLab, String valor1, String valor2, 
			String valor3, String valor4, String valor5, String valor6, String valor7, String valor8,String codUsuario){

		try{
		return this.reclutaManager.updateReclutaExpLaboral(idRec, codExpLab, valor1, valor2, valor3, valor4, 
									valor5, valor6, valor7, valor8,codUsuario);
		}
		catch(Exception ex){
			ex.printStackTrace();				
		}
		return null;
	}
}
