package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.modelo.*;

public class ConfInicioFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConfInicioFormController.class);
    //private ConfInicioManager confInicioManager;
    
    public ConfInicioFormController() {    	
        super();        
        setCommandClass(ConfInicioCommand.class);        
    }

	/*public void setConfInicioManager(ConfInicioManager confInicioManager) {
		this.confInicioManager = confInicioManager;
	}*/

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ConfInicioCommand command = new ConfInicioCommand();
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	ConfInicioCommand control = (ConfInicioCommand) command;
			//if(control.getOperacion().equals("irRegistrar")){
    	/*ConfInicio ConfInicio = confInicioManager.GetAllConfInicio("");
			control.SetIdOrden(ConfInicio.GetIdOrden());
			control.SetDescripcion(ConfInicio.GetDescripcion());*/
			//control.Clear();
		//}		
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_mto_conf_inicio","control",control);		
    }
	
}
