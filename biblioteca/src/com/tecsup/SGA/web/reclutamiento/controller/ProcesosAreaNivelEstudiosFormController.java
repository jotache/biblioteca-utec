package com.tecsup.SGA.web.reclutamiento.controller;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.BuscarAreasEstudiosCommand;

public class ProcesosAreaNivelEstudiosFormController extends SimpleFormController {
	private TablaDetalleManager tablaDetalleManager;

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ServletException {		   
		BuscarAreasEstudiosCommand command = new BuscarAreasEstudiosCommand();		
		List<TipoTablaDetalle> lista  = this.tablaDetalleManager.getAllTablaDetalle("0003", "", "", "", "", "", "", "", "1");
		if (lista!=null)
			command.setLongitud(lista.size());
			    	
		command.setListaAreasNivelEstudios(lista);
		return command;
	}
}
