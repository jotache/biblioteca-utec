package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.*;

public class ModificarAreaInteresFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ModificarAreaInteresFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	
    	ModificarAreaInteresCommand command = new ModificarAreaInteresCommand();
    
    	String codDetalle = (String)request.getParameter("txhCodDetalle");
    	String dscDetalle = (String)request.getParameter("txhDscDetalle");
    	command.setCodEval((String)request.getParameter("txhCodEva"));
    	System.out.println(command.getCodEval());
    	
    	command.setCodDetalle("");
    	
    	if ( codDetalle != null ) if ( !codDetalle.trim().equals("") ) cargaDetalle(command, codDetalle);
    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado = "";
    	ModificarAreaInteresCommand control = (ModificarAreaInteresCommand) command;

    	if (control.getOperacion().trim().equals("MODIFICAR"))
    	{
    		resultado = UpdateTablaDetalle(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if ( resultado.equals("-2")) 
    		{
    			control.setMsg("DOBLE");
    		}
    		else control.setMsg("OK");
    	}
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_modificar_area_interes","control",control);		
    }
    
    private String UpdateTablaDetalle(ModificarAreaInteresCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
 
    		obj.setCodTipoTabla(CommonConstants.TIPT_AREA_INT1);
    		obj.setDescripcion(control.getDescripcion());
    		obj.setCodDetalle(control.getCodDetalle());
    		
    		System.out.println(CommonConstants.TIPT_AREA_INT1);
    		System.out.println(control.getDescripcion());
    		System.out.println(control.getCodDetalle());
    		System.out.println("eval: "+control.getCodEval());
    		if ( !control.getCodDetalle().trim().equals("") )
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.UpdateTablaDetalle(obj, control.getCodEval()));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(ModificarAreaInteresCommand command, String codDetalle)
    {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
		
		obj = (TipoTablaDetalle)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT1
				, codDetalle, "", "","", "", "", "", CommonConstants.TIPO_ORDEN_DSC).get(0);
		
		command.setDescripcion(obj.getDescripcion());		
		command.setCodDetalle(codDetalle);
    }
    
    
}
