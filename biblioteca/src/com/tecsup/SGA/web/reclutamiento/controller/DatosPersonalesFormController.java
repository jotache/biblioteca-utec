package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ConstantesCuerpoMensajes;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;

import com.tecsup.SGA.service.reclutamiento.*;
import com.tecsup.SGA.modelo.*;

public class DatosPersonalesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(DatosPersonalesFormController.class);

	private TablaDetalleManager tablaDetalleManager;
	private ReclutaManager reclutaManager;

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
 	
    	DatosPersonalesCommand command = new DatosPersonalesCommand();

    	/*Recogiendo valores*/
    	command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario")); 
    	
    	/*Informacion*/
    	inicializaCombos(command, request);
    	inicializaAreaInteres(command);

    	//Llenado Tabla Interes - Session
    	//request.getSession().setAttribute("listaAreaInteresH", command.getListaAreaInteresH());
    	
    	if("ACCESO".equals(command.getOperacion())){ //Ver Datos
    		inicializaDatos(command, command.getIdRec(), request);
    	}
    	else{ //Postulante nuevo
    		//se cambio el valor por defecto ya es radio sino caja de texto
    		//command.setNacionalidad(CommonConstants.GSTR_NAC_PERUANO); //por defecto peruano 0
    	}
    	if (command.getCodUsuario()==null)
    		command.setCodUsuario(command.getIdRec());
    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	DatosPersonalesCommand control = (DatosPersonalesCommand) command;
    	
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta;
    		
    		log.info("Cadena Areas 1:"+control.getCodArea());
    		
    		//log.info("Cadena Areas 2:"+control.getAreaInteres());
    		
    		if("".equals(control.getIdRec())){ //Recluta no existe - Insertar    			
	        	rpta = insertarDatos(control);	        	
	        	//grabaAreaInteres(control, control.getIdRec(), control.getCodArea(), control.getNroRegAreaInt());
	        	request.setAttribute("usuNuevo", "1");
    		}
    		else{ //Recluta existe - Actualizar	
    			rpta = actualizarDatos(control);
    			//grabaAreaInteres(control, control.getIdRec(), control.getCodArea(), control.getNroRegAreaInt());
    		}
    		log.info("rpta:"+rpta);
    		if ("-1".equals(rpta)){ //Error Operación
    			control.setMessage(CommonMessage.RECLUTA_ERROR);
    			control.setTypeMessage(rpta);
        	}
        	else if ("-2".equals(rpta)){ //Cuenta correo ya existe
        		control.setMessage(CommonMessage.RECLUTA_EXISTE);
        		control.setTypeMessage(rpta);
        	}
        	else if ("-3".equals(rpta)){ //Documento y fecha ya existe
        		control.setMessage(CommonMessage.DOC_FECHA_EXIST);
        		control.setTypeMessage(rpta);
        	}
        	else if ("-4".equals(rpta)){ //Documento ya existe
        		control.setMessage(CommonMessage.DOC_EXIST);
        		control.setTypeMessage(rpta);
        	}
        	else{ //OK
        		//control.setIdRec(rpta);
        		//JHPR 2008-09-30 Comentado 
        		//JCMU 2008-10-29 reactivado 
        		if ("".equals(control.getIdRec())){ //En caso Recluta nuevo
        			control.setIdRec(rpta);
        			ConstantesCuerpoMensajes cuerpo= new ConstantesCuerpoMensajes();
        			String mensaje="";
        			mensaje=cuerpo.RegistroPostulante(control.getNombres()+" "+control.getApepat()+" "+control.getApemat(), 
        				                      control.getEmail(), control.getClave());
        			
        			try{
        			ServletEnvioCorreo servletEnvioCorreo= new ServletEnvioCorreo();
        			servletEnvioCorreo.EnviarCorreo(control.getEmail(), CommonConstantsEmail.TECSUP_EMAIL,
        					control.getNombres()+" "+control.getApepat()+" "+control.getApemat(),
        					CommonConstantsEmail.TECSUP_EMAIL_ORIGEN, 
        					CommonConstantsEmail.TECSUP_ASUNTO_INSCRIPCION_POSTULANTE, mensaje);
        		   System.out.println("Texto a enviar en el Correo: \n"+mensaje);
        			}
        			catch(Exception ex){
        				System.out.println(ex.getMessage());
        			}
        		}
        		
        		grabaAreaInteres(control, control.getIdRec(), control.getCodArea(), control.getNroRegAreaInt());
        		control.setOperacion("ACCESO");
        		
        		control.setMessage(CommonMessage.GRABAR_EXITO);
        		control.setTypeMessage("OK");
        		inicializaDatos(control, control.getIdRec(), request);
        	}
    	}
    	
    	else if("AGREGAR_INTERES".equals(control.getOperacion())){
    		control.setListaAreaInteresByPost(this.reclutaManager.getReclutaAreaInteresEspecial(control.getIdRec(), 
    				control.getCadenaAI()));
    		
    		//inicializaDatos(control, control.getIdRec(), request);
    		/*
    		control.setListaAreaInteresH(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT2
    				, "", "", control.getAreaInteres(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    		*/
    	}
    	
    	else if("PROV".equals(control.getOperacion())){
    		control.setListaProvincia(this.reclutaManager.getAllProvincia(control.getDepartamento()));
    		request.getSession().setAttribute("listaProvincia", control.getListaProvincia());
    		control.setProvincia(control.getProvincia());
    	}
    	else if("DIST".equals(control.getOperacion())){
    		control.setListaProvincia(this.reclutaManager.getAllProvincia(control.getDepartamento()));
    		control.setListaDistrito(this.reclutaManager.getAllDistrito(control.getProvincia()));
    		request.getSession().setAttribute("listaProvincia", control.getListaProvincia());
    		request.getSession().setAttribute("listaDistrito", control.getListaDistrito());
    		
    		control.setProvincia(control.getProvincia());
    		control.setDistrito(control.getDistrito());
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_datos_personales","control",control);		
    }

    /*SETTER*/
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
    
	/*Metodos*/
	private void inicializaAreaInteres(DatosPersonalesCommand command){
		command.setListaAreaInteres(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT1
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	log.info("1>>"+command.getIdRec()+">>");
    	log.info("2>>"+command.getAreaInteres()+">>");
		//Tabla Interes
    	//command.setListaAreaInteresByPost(this.reclutaManager.getReclutaAreaInteresEspecial(command.getIdRec(), 
    		//								command.getAreaInteres()));
    	
    	//command.setListaAreaInteresByPost(this.reclutaManager.getReclutaAreaInteres(command.getIdRec()));
	}
	
	private void inicializaCombos(DatosPersonalesCommand command, HttpServletRequest request){
		command.setListaEstadoCivil(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTADO_CIVIL
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaSedePrefTrabajo(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_SEDE_PREFERENTE
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaInteresEn(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_INTERES_EN
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaDedicacion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_DEDICACION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaDispoTrabajar(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_DISPONIBILIDAD
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	
    	command.setListaMoneda(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_MONEDA
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	
    	/*Departamento*/
    	command.setListaDepartamento(this.reclutaManager.getAllDepartamento());
    	request.getSession().setAttribute("listaDepartamento", command.getListaDepartamento());
	
	}
	
	public void inicializaDatos(DatosPersonalesCommand command, String codPersona, HttpServletRequest request)
	{
		Recluta lObject = this.reclutaManager.getAllDatosPersonaRecluta(codPersona);
		if ( lObject != null )
		{
			System.out.println("puestoPostula:" + lObject.getPuestoPostula());
			
	    	command.setNombres(lObject.getNombre());
			command.setApepat(lObject.getApepat());
			command.setApemat(lObject.getApemat());
			if (lObject.getFoto()!= null){
				//String ruta = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_POSTULANTE_FOTO;
		    	
				String ruta = CommonConstants.GSTR_HTTP+
		    	CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+
		    	CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_FOTO;
		    	
				command.setFoto( ruta + lObject.getFoto());
				
			}
			command.setFecnac(lObject.getFecnac());
			command.setFecreg(lObject.getFecreg());
			command.setClave(lObject.getClave());
			command.setEmail(lObject.getEmail());
			command.setEmail1(lObject.getEmailAlterno());
			command.setSexo(lObject.getSexo());
			command.setNacionalidad(lObject.getNacionalidad());
			command.setDni(lObject.getDni());
			command.setRuc(lObject.getRuc());
			command.setEstadoCivil(lObject.getEstadoCivil());
			command.setAnioExpLaboral(lObject.getAnioExpLaboral());
		
			command.setDireccion(lObject.getDireccion());
			command.setDepartamento(lObject.getDepartamento());
			
			if (lObject.getProvincia()!= null){
				command.setListaProvincia(this.reclutaManager.getAllProvincia(command.getDepartamento()));
	    		request.getSession().setAttribute("listaProvincia", command.getListaProvincia());
	    		command.setProvincia(lObject.getProvincia());
			}
			
			if (lObject.getDistrito()!= null){
				command.setListaProvincia(this.reclutaManager.getAllProvincia(command.getDepartamento()));
				command.setListaDistrito(this.reclutaManager.getAllDistrito(command.getProvincia()));
				request.getSession().setAttribute("listaProvincia", command.getListaProvincia());
	    		request.getSession().setAttribute("listaDistrito", command.getListaDistrito());
	    		
	    		command.setProvincia(lObject.getProvincia());
	    		command.setDistrito(lObject.getDistrito());
			}

			command.setPais(lObject.getPais());
			command.setTelef(lObject.getTelef());
			command.setTelefAdicional(lObject.getTelefAdicional());
			command.setTelefMovil(lObject.getTelefMovil());
			command.setCodPostal(lObject.getCodPostal());
			
			command.setPostuladoAntes(lObject.getPostuladoAntes());
			command.setTrabajadoAntes(lObject.getTrabajadoAntes());
			command.setFamiliaTecsup(lObject.getFamiliaTecsup());
			command.setFamiliaNombres(lObject.getFamiliaNombres());
			command.setExpDocente(lObject.getExpDocente());
			command.setExpDocenteAnios(lObject.getExpDocenteAnios());
			command.setDispoViaje(lObject.getDispoViaje());
			command.setSedePrefTrabajo(lObject.getSedePrefTrabajo());
			command.setInteresEn(lObject.getInteresEn());
			command.setMoneda(lObject.getMoneda());
			command.setPretensionEconomica(lObject.getPretensionEconomica());
			command.setIndPago(lObject.getTipoPago());
			command.setDedicacion(lObject.getDedicacion());
			command.setDispoTrabajar(lObject.getDispoTrabajar());
			command.setPerfil(lObject.getPerfil());
			command.setPuestoPostula(lObject.getPuestoPostula());
		}
	}
	
	private String insertarDatos(DatosPersonalesCommand control){
		try{
			
			if (control.getPerfil()!=null){
				if (control.getPerfil().length()>999){
					control.setPerfil(control.getPerfil().substring(0,999));
				}
			}
			
			return this.reclutaManager.insertReclutaDatos(control.getNombres(), control.getApepat(), 
				control.getApemat(), control.getClave(), control.getFecnac(), control.getDni(), 
				control.getRuc(), control.getSexo(), control.getEstadoCivil(), control.getNacionalidad(), 
				control.getEmail(), control.getEmail1(),control.getAnioExpLaboral(), control.getDireccion(), 
				control.getDepartamento(), control.getProvincia(), control.getDistrito(), control.getPais(), 
				control.getCodPostal(), control.getTelef(), control.getTelefAdicional(), control.getTelefMovil(), 
				control.getPostuladoAntes(), control.getTrabajadoAntes(), control.getExpDocente(), 
				control.getExpDocenteAnios(), control.getFamiliaTecsup(), control.getFamiliaNombres(), control.getDispoViaje(), 
				control.getSedePrefTrabajo(), control.getInteresEn(), control.getPretensionEconomica(), 
				control.getDedicacion(), control.getDispoTrabajar(), control.getPerfil(), control.getAreaInteres(), 
				control.getNroRegAreaInt(), control.getMoneda(), control.getIndPago(),control.getPuestoPostula());
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	private String actualizarDatos(DatosPersonalesCommand control){
		try{
			
		/*log.warn("control.getCodUsuario():"+control.getCodUsuario());*/
		//System.out.println("actualizarDatos:control.getIdRec():"+control.getIdRec());
		//System.out.println("actualizarDatos:control.getCodUsuario():"+control.getCodUsuario());	
		if (control.getCodUsuario()==null){
			control.setCodUsuario(control.getIdRec());
		}
		
		if (control.getCodUsuario().equals("null")){
			control.setCodUsuario(control.getIdRec());
		}
		
		if (control.getCodUsuario().equals(""))
			control.setCodUsuario(control.getIdRec());
		
		//System.out.println("actualizarDatos:control.getCodUsuario():"+control.getCodUsuario());
		if (control.getPerfil()!=null){
			if (control.getPerfil().length()>999){
				control.setPerfil(control.getPerfil().substring(0,999));
			}
		}
			
		
		return this.reclutaManager.updateReclutaDatos(control.getIdRec(), control.getNombres(), control.getApepat(), 
				control.getApemat(), control.getFecnac(), control.getDni(), 
				control.getRuc(), control.getSexo(), control.getEstadoCivil(), control.getNacionalidad(), 
				control.getEmail(), control.getEmail1(), control.getAnioExpLaboral(), control.getDireccion(), 
				control.getDepartamento(), control.getProvincia(), control.getDistrito(), control.getPais(), 
				control.getCodPostal(), control.getTelef(), control.getTelefAdicional(), control.getTelefMovil(), 
				control.getPostuladoAntes(), control.getTrabajadoAntes(), control.getExpDocente(), 
				control.getExpDocenteAnios(), control.getFamiliaTecsup(), control.getFamiliaNombres(), 
				control.getDispoViaje(), control.getSedePrefTrabajo(), control.getInteresEn(), 
				control.getPretensionEconomica(), control.getDedicacion(), control.getDispoTrabajar(), 
				control.getPerfil(), control.getMoneda(), control.getIndPago(),control.getCodArea(),control.getPuestoPostula(),control.getCodUsuario());
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	private void grabaAreaInteres(DatosPersonalesCommand control, String idRec, String cadena, String nroReg){
		String rpta;
		rpta = this.reclutaManager.insertReclutaAreaInteres(idRec, cadena, nroReg);	
	}
}
