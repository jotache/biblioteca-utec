package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.*;


public class TipoEvaluacionFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(TipoEvaluacionFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	TipoEvaluacionCommand command = new TipoEvaluacionCommand();
    	command.setCodEval((String)request.getParameter("txhCodEva"));
    	command.setEta((String)request.getParameter("txhEta"));
    	
    	if(!(command.getCodEval()==null)){
          	if(!command.getCodEval().trim().equals("")){
    		command.setEta("1");
    		command.setEtapa("0001");
    	}
    	}
    	System.out.println(command.getCodEval());
    	String cantidad;
    	String cantidad1;
    	command.setListTipoEvaluacionRevision(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
				, "", "",CommonConstants.PROC_ETAPA_REVISION, "", "", "","", CommonConstants.E_C_TIPO));    	
    	request.getSession().setAttribute("listTipoEvaluacionRevision", command.getListTipoEvaluacionRevision());
       
    	command.setListTipoEvaluacionSeleccion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
				, "", "",CommonConstants.PROC_ETAPA_SELECCION, "", "", "", "", CommonConstants.E_C_TIPO));    	
    	request.getSession().setAttribute("listTipoEvaluacionSeleccion", command.getListTipoEvaluacionSeleccion());
    	System.out.println(command.getListTipoEvaluacionRevision().size());
    	System.out.println(command.getListTipoEvaluacionSeleccion().size());
    	cantidad = ""+command.getListTipoEvaluacionRevision().size();
    	cantidad1 = ""+command.getListTipoEvaluacionSeleccion().size();
    	command.setCantidad(cantidad);
    	command.setCantidad1(cantidad1);
    	
    	System.out.println("etapa: "+command.getEtapa());
    	request.setAttribute("etapa",command.getEtapa());
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	TipoEvaluacionCommand control = (TipoEvaluacionCommand) command;
    	
    	if ( control.getOperacion().trim().equals("SELECCION"))
    	{
    		control.setListConsulta(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
    				, "", "",control.getEtapa(), "", "", "", "", CommonConstants.E_C_TIPO));    	
        	request.getSession().setAttribute("listConsulta", control.getListConsulta());
        	request.setAttribute("etapa",control.getEtapa());
    	}
    	else if(control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		resultado = DeleteTablaDetalle(control);
    		if ( resultado.equals("-1")){ 
    			control.setMsg("ERROR");
    		}
    		else if ( resultado.equals("-2")){ 
    			control.setMsg("ERROR_USADO");
    		}
    		else control.setMsg("OK");
    		control.setListTipoEvaluacionRevision(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
    				, "", "",CommonConstants.PROC_ETAPA_REVISION, "", "", "", "", CommonConstants.E_C_TIPO));    	
        	request.getSession().setAttribute("listTipoEvaluacionRevision", control.getListTipoEvaluacionRevision());
        	request.setAttribute("etapa","0001");   
    	}
       	else if(control.getOperacion().trim().equals("ELIMINAR2"))
    	{
    		resultado = DeleteTablaDetalle2(control);
    		if ( resultado.equals("-1")){ 
    			control.setMsg2("ERROR");
    		}
    		else if ( resultado.equals("-2")){ 
    			control.setMsg("ERROR_USADO");
    		}
    		else control.setMsg2("OK");
    		control.setListTipoEvaluacionRevision(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
    				, "", "",CommonConstants.PROC_ETAPA_SELECCION, "", "", "", "", CommonConstants.E_C_TIPO));    	
        	request.getSession().setAttribute("listTipoEvaluacionSeleccion", control.getListTipoEvaluacionRevision());
        	request.setAttribute("etapa","0002");   
    	}
    	control.setCantidad(""+control.getListTipoEvaluacionRevision().size());
    	control.setCantidad1(""+control.getListTipoEvaluacionSeleccion().size());
    
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_mto_cfg_tipos_de_evaluacion_consulta","control",control);		
    }

    private String DeleteTablaDetalle(TipoEvaluacionCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String cant = ""+control.getCant();
    		obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_EVA);
    		obj.setDscValor1(CommonConstants.PROC_ETAPA_REVISION);
    		obj.setCadCod(control.getCodDetalle());
    		obj.setNroReg(cant);
 	
    		if (!obj.getCadCod().trim().equals("") )
    		{
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.EliminarTablaDetalle(obj, control.getCodEval()));	
    			return obj.getCodTipoTablaDetalle();
    	    		}
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }

    
    
    private String DeleteTablaDetalle2(TipoEvaluacionCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_EVA);
    		obj.setDscValor1(CommonConstants.PROC_ETAPA_SELECCION);
    		obj.setCadCod(control.getCodDetalle());
    		obj.setNroReg("1");
 	
    		if (!obj.getCadCod().trim().equals("") )
    		{
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.EliminarTablaDetalle(obj, control.getCodEval()));	
    			return obj.getCodTipoTablaDetalle();
    	    		}
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    
    
    
    
}
