package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.reclutamiento.*;


public class CambioClaveFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(CambioClaveFormController.class);

	private ReclutaManager reclutaManager;

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	CambioClaveCommand command = new CambioClaveCommand();
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	CambioClaveCommand control = (CambioClaveCommand) command;
		
    	if (control.getOperacion().equals("CHANGE")){
    		String rpta;
        	
        	//Valido Cambio Password
        	rpta = this.reclutaManager.updateReclutaClave(control.getUsuario(), control.getClave(), control.getClaveNueva());
        	
        	if (rpta.equals("1")){ //Pass actual no coincide
        		control.setMessage(CommonMessage.NO_PASS_ACTUAL);
        		control.setTypeMessage("1");
        	}
        	else if (rpta.equals("2")){ //Recluta no existe
        		control.setMessage(CommonMessage.RECLUTA_NO_EXISTE);
        		control.setTypeMessage("2");
        	}
        	else if (rpta.equals("0")){ //Cambio clave exitosamente
        		control.setMessage(CommonMessage.OK_CAMBIO_PASS);
        		control.setTypeMessage("0");
        	}
        	else if (rpta.equals("-1")){ //Error
        		control.setMessage(CommonMessage.RECLUTA_ERROR);
        		control.setTypeMessage("ERROR");
        	}
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_cambio_clave","control",control);		
    }
    
    /*SETTER*/
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
}

