package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;

import com.tecsup.SGA.service.reclutamiento.*;
import com.tecsup.SGA.modelo.*;

public class DatosPersonalesDomicilioFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(DatosPersonalesDomicilioFormController.class);

	private TablaDetalleManager tablaDetalleManager;
	private ReclutaManager reclutaManager;

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
 	
    	DatosPersonalesCommand command = new DatosPersonalesCommand();
    	
    	command.setListaDepartamento(this.reclutaManager.getAllDepartamento());  	

    	if(request.getParameter("departamento")!=null){
    		
    		command.setDepartamento(request.getParameter("departamento"));
    		command.setProvincia(request.getParameter("provincia"));
    		command.setDistrito(request.getParameter("distrito"));
    		
    		iniDatos(command);
    	}
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    private void iniDatos(DatosPersonalesCommand command) {
    	if (command.getProvincia()!= null){
			command.setListaProvincia(this.reclutaManager.getAllProvincia(command.getDepartamento()));
    		//request.getSession().setAttribute("listaProvincia", command.getListaProvincia());
    		//command.setProvincia(lObject.getProvincia());
		}
		
		if (command.getDistrito()!= null){
			//command.setListaProvincia(this.reclutaManager.getAllProvincia(command.getDepartamento()));
			command.setListaDistrito(this.reclutaManager.getAllDistrito(command.getProvincia()));
			//request.getSession().setAttribute("listaProvincia", command.getListaProvincia());
    		//request.getSession().setAttribute("listaDistrito", command.getListaDistrito());
    		
    		//command.setProvincia(lObject.getProvincia());
    		//command.setDistrito(lObject.getDistrito());
    	}		
	}

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	DatosPersonalesCommand control = (DatosPersonalesCommand) command;    	
    	
    	if("PROV".equals(control.getOperacion())){
    		control.setListaProvincia(this.reclutaManager.getAllProvincia(control.getDepartamento()));
    		request.getSession().setAttribute("listaProvincia", control.getListaProvincia());
    		control.setProvincia(control.getProvincia());
    	}
    	else if("DIST".equals(control.getOperacion())){
    		control.setListaProvincia(this.reclutaManager.getAllProvincia(control.getDepartamento()));
    		control.setListaDistrito(this.reclutaManager.getAllDistrito(control.getProvincia()));
    		request.getSession().setAttribute("listaProvincia", control.getListaProvincia());
    		request.getSession().setAttribute("listaDistrito", control.getListaDistrito());
    		
    		control.setProvincia(control.getProvincia());
    		control.setDistrito(control.getDistrito());
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_datos_personales_domicilio","control",control);		
    }

    /*SETTER*/
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
    
}
