package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.ReclutaManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.reclutamiento.EvaluacionProcesosManager;
import com.tecsup.SGA.web.reclutamiento.command.ProcesosPostulantesCommand;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.modelo.Evaluacion;

public class ProcesosCalificarRevFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ProcesosBandejaFormController.class);
	private ProcesoManager procesoManager;
	private TablaDetalleManager tablaDetalleManager;
	private ReclutaManager reclutaManager;
	private EvaluacionProcesosManager evaluacionProcesosManager;
		
	public void setEvaluacionProcesosManager(
			EvaluacionProcesosManager evaluacionProcesosManager) {
		this.evaluacionProcesosManager = evaluacionProcesosManager;
	}

	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}

	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ProcesosPostulantesCommand command = new ProcesosPostulantesCommand();
    	
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setCodProceso(request.getParameter("txhCodProceso") == null ? "" : request.getParameter("txhCodProceso"));
    	command.setCodPostulantesSel(request.getParameter("txhCodPostulante") == null ? "" : request.getParameter("txhCodPostulante"));
    	command.setEstPostulantesSel(request.getParameter("txhCodEstado") == null ? "" : request.getParameter("txhCodEstado"));
    	command.setCodEtapa(request.getParameter("txhCodEtapa") == null ? "" : request.getParameter("txhCodEtapa"));
    	
    	if ( command.getCodEtapa().trim().equals(CommonConstants.PROC_ETAPA_REVISION))
    	{
        	command.setCodEstRevJefe(CommonConstants.EST_REV_REV_X_JEFE_DPTO);
    		command.setListCalificaciones(this.tablaDetalleManager.getAllTablaDetalle(
    			CommonConstants.TIPT_CALIF_FINAL_REV
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	}
    	else
    	{
        	command.setCodEstRevJefe(CommonConstants.EST_SEL_EVALUADO);
    		command.setListCalificaciones(this.tablaDetalleManager.getAllTablaDetalle(
    			CommonConstants.TIPT_SISTEMAS_SGA
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	}
    	
    	if ( !command.getCodProceso().trim().equals("") )
    	{
    		Proceso proceso = this.procesoManager.getProceso(command.getCodProceso().trim());
    		if ( proceso != null )
    		{
    			command.setDscEtapa(proceso.getDscEtapa());
    			command.setDscProceso(proceso.getDscProceso());    			
    		}
    	}
    	
    	if ( !command.getCodPostulantesSel().trim().equals("") )
    	{
    		cargaDatos(command);
    	}
    	
    	/*String ruta = CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_IP + 
		CommonConstants.STR_RUTA_POSTULANTE_INFFIN;*/
    	
    	String ruta = CommonConstants.GSTR_HTTP+
    	CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+
    	CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_INFFIN;
    	
    	
    	request.setAttribute("rutaServidor",ruta );
    	
    	request.getSession().setAttribute("listEvalPostRev", command.getListPostulantes());
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	String respuesta = "";
    	ProcesosPostulantesCommand control = (ProcesosPostulantesCommand) command;
    	
		if ( control.getOperacion().trim().equals("GRABAR"))
		{
			respuesta = grabar(control);
			if ( respuesta == null ) control.setMessage("ERROR");
			else if ( respuesta.trim().equals("-1") ) control.setMessage("ERROR");
			else if ( Long.valueOf(respuesta)>0 ) control.setMessage("OK");
		}
		
		cargaDatos(control);
    	request.getSession().setAttribute("listEvalPostRev", control.getListPostulantes());
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/procesos/rec_procesoscalificar_rev","control",control);		
    }
    
    private String grabar(ProcesosPostulantesCommand control)
    {
    	String estadoFinal = "";
    	String respuesta = "";
    	
    	//control.setCodCalificacion("");
    	
    	if ( control.getCodEtapa().trim().equals(CommonConstants.PROC_ETAPA_REVISION))
    	{
	    	//if (control.getCodCalificacion().trim().equals(CommonConstants.CAL_REV_FIN_PRESELECCIONADO))
	    	estadoFinal = CommonConstants.EST_REV_PRESELECCIONADO;
	    	//else 		    	
	    	if (control.getCodCalificacion().trim().equals(CommonConstants.CAL_REV_FIN_NO_PRESELECCIONADO))
	    		estadoFinal = CommonConstants.EST_REV_NO_PRESELECCIONADO;
    	}
    	else
    	{
	    	if (control.getCodCalificacion().trim().equals(CommonConstants.CAL_SEL_FIN_ACEPTADO))
	    		estadoFinal = CommonConstants.EST_SEL_ACEPTADO;
	    	else //if (control.getCodCalificacion().trim().equals(CommonConstants.CAL_REV_FIN_NO_PRESELECCIONADO))
	    		estadoFinal = CommonConstants.EST_SEL_NO_ACEPTADO;    		
    	}
    	
    	respuesta = this.evaluacionProcesosManager.insertEvaluacionPostulante(
    			control.getCodPostulantesSel(),
    			control.getCodProceso(), 
    			control.getCodEtapa(), "", 
    			estadoFinal, "", "", 
    			control.getCodCalificacion().trim(), 
    			control.getComentario(),
    			CommonConstants.EVAL_ACT_ESTADO, 
    			control.getCodUsuario());
    	
    	if ( respuesta != null ){
    		//Inactivado en revision
	    	if ( control.getCodCalificacion().trim().equals(CommonConstants.CAL_REV_FIN_INACTIVO) 
	    			&& respuesta.trim().equals("0") 
	    			&& control.getCodEtapa().trim().equals(CommonConstants.PROC_ETAPA_REVISION))
	    	{
	    		respuesta = this.procesoManager.deletePostulanteByProceso(control.getCodPostulantesSel()
	    				, control.getCodProceso(), "", control.getCodUsuario());
	    	}
	    	//Inactivado en seleccion
	    	if ( control.getCodCalificacion().trim().equals(CommonConstants.CAL_SEL_FIN_INACTIVO) 
	    			&& respuesta.trim().equals("0") 
	    			&& control.getCodEtapa().trim().equals(CommonConstants.PROC_ETAPA_SELECCION))
	    	{
	    		respuesta = this.procesoManager.deletePostulanteByProceso(control.getCodPostulantesSel()
	    				, control.getCodProceso(), "2", control.getCodUsuario());
	    	}
    	}
    		
    	return respuesta;
    }
    
    private void cargaDatos(ProcesosPostulantesCommand command)
    {
		Recluta recluta = reclutaManager.getAllDatosPersonaRecluta(command.getCodPostulantesSel().trim());
		if ( recluta != null)
		{
			command.setNomPostulantesSel(recluta.getNombre() + " " + 
										 recluta.getApepat() + " " + 
										 recluta.getApemat());
		}
		
		command.setListPostulantes(this.procesoManager.getAllEvaluacionesByPostulante(
				command.getCodEtapa(), command.getCodProceso().trim()
				, command.getCodPostulantesSel().trim()));
		
		Evaluacion evaluacion = this.procesoManager.getCalificacionFinal(
				command.getCodProceso(), command.getCodPostulantesSel());
		if ( evaluacion != null )
		{
			command.setCodCalificacion(evaluacion.getCodCalificacionFinEtapa());
			command.setComentario(evaluacion.getComentarioFinEtapa());
			command.setInfFinal(evaluacion.getCodCalificaion());
			command.setInfFinalGen(evaluacion.getComentario());
		}
    }
}
