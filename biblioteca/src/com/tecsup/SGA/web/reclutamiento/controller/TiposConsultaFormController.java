package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.reclutamiento.CalificacionManager;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Calificacion;
import com.tecsup.SGA.modelo.CatalogoProducto;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.ParametrosEspeciales;

public class TiposConsultaFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(TiposConsultaFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	private CalificacionManager calificacionManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public void setCalificacionManager(CalificacionManager calificacionManager) {
		this.calificacionManager = calificacionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	TiposConsultaCommand command = new TiposConsultaCommand();
    	command.setCodEval((String)request.getParameter("txhCodEva"));
    	System.out.println(command.getCodEval());
    	command.setListConsulta(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
				, "", "",CommonConstants.PROC_ETAPA_REVISION, "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
    	request.getSession().setAttribute("listConsulta", command.getListConsulta());
    	
    	command.setListaDenominacion(this.calificacionManager.getAllCalificacion(command.getEtapa(), command.getCodTipoEva()));
    	request.getSession().setAttribute("listaDenominacion", command.getListaDenominacion());
		
    	command.setListEva(this.calificacionManager.getAllCalificacion(command.getEtapa(), command.getCodTipoEva()));
    	request.getSession().setAttribute("listEva", command.getListEva());
    	
    	command.setListTipoCalificacion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_CALIFICACION
    			, "", "","","","","", "", CommonConstants.TIPO_ORDEN_DSC));
    	request.getSession().setAttribute("listTipoCalificacion",command.getListTipoCalificacion());
    	
    	command.setEtapa("0001");
    	request.setAttribute("etapa","0001");
    	fcRestaLista(command);
    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
		String resultado2 = "";
		int numero ;
    	TiposConsultaCommand control = (TiposConsultaCommand) command;
    	
    	if ( control.getOperacion().trim().equals("DETALLE"))
    	{
    		control.setListConsulta(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
    				, "", "",control.getEtapa(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
        	
        	control.setListTipoCalificacion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_CALIFICACION
        			, "", "","","","","", "", CommonConstants.TIPO_ORDEN_DSC));
        	
        	
        	
        	request.setAttribute("etapa",control.getEtapa());
        	request.getSession().setAttribute("listConsulta", control.getListConsulta());
        	request.getSession().setAttribute("listEva",control.getListEva());
        	request.getSession().setAttribute("listaDenominacion", control.getListaDenominacion());
        	request.getSession().setAttribute("listTipoCalificacion",control.getListTipoCalificacion());
        	fcRestaLista(control);
    	}
    	
    	else if ( control.getOperacion().trim().equals("GRABAR"))
    	{
    		if(control.getSelTipo()>0){
    				resultado2 = DeleteCalificacion(control);
    			resultado = InsertTablaDetalle(control);
        		if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
        		else {
        			control.setMsg("OK");
        		}
        		
        		
        		control.setListConsulta(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
        				, "", "",control.getEtapa(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
            	request.getSession().setAttribute("listConsulta", control.getListConsulta());
            	
            	control.setListEva(this.calificacionManager.getAllCalificacion(control.getEtapa(), control.getCodTipoEva()));
            	request.getSession().setAttribute("listEva",control.getListEva());
            	
            	control.setListTipoCalificacion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_CALIFICACION
            			, "", "","","","","", "", CommonConstants.TIPO_ORDEN_DSC));
            	request.getSession().setAttribute("listTipoCalificacion",control.getListTipoCalificacion());
            	
            	control.setListaDenominacion(this.calificacionManager.getAllCalificacion(control.getEtapa(), control.getCodTipoEva()));
            	request.getSession().setAttribute("listaDenominacion", control.getListaDenominacion());
        
            	numero = control.getListaDenominacion().size();
            	control.setSelTipo(numero);	
            	
            	fcRestaLista(control);
            	
    		}
    		else{
    		resultado = InsertTablaDetalle(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else {
    			control.setMsg("OK");
    		}
    		control.setListConsulta(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
    				, "", "",control.getEtapa(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
        	
        	control.setListEva(this.calificacionManager.getAllCalificacion(control.getEtapa(), control.getCodTipoEva()));
        	List lst  = control.getListEva();
        	if(lst!=null ){
        		if ( lst.size()>0 )
        		{
        			Calificacion calificacion = null;
        			calificacion = (Calificacion)lst.get(0); 
        			control.setCboEvaluacion(calificacion.getCodCalificacionEliminatoria());
        		}
        		}
        	System.out.println("conbo: "+control.getCboEvaluacion());
        	control.setListTipoCalificacion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_CALIFICACION
        			, "", "","","","","", "", CommonConstants.TIPO_ORDEN_DSC));
        	request.getSession().setAttribute("listTipoCalificacion",control.getListTipoCalificacion());
        	
        	control.setListaDenominacion(this.calificacionManager.getAllCalificacion(control.getEtapa(), control.getCodTipoEva()));
        	
        	numero = control.getListaDenominacion().size();
        	control.setSelTipo(numero);	
        	fcRestaLista(control);
    	}
    		request.getSession().setAttribute("listConsulta", control.getListConsulta());
        	request.getSession().setAttribute("listEva",control.getListEva());
        	request.getSession().setAttribute("listaDenominacion", control.getListaDenominacion());
    	}
    	else if(control.getOperacion().trim().equals("LLENAR"))
    	{
    		control.setListConsulta(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
    				, "", "",control.getEtapa(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
        	
        	
        	control.setListEva(this.calificacionManager.getAllCalificacion(control.getEtapa(), control.getCodTipoEva()));
        	List lst  = control.getListEva();
        	if(lst!=null ){
        		if ( lst.size()>0 )
        		{
        			Calificacion calificacion = null;
        			calificacion = (Calificacion)lst.get(0); 
        			control.setCboEvaluacion(calificacion.getCodCalificacionEliminatoria());
        		}
        		}
        	
        	System.out.println("conbo: "+control.getCboEvaluacion());
        	
        	control.setListaDenominacion(this.calificacionManager.getAllCalificacion(control.getEtapa(), control.getCodTipoEva()));
        	control.setListTipoCalificacion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_CALIFICACION
        			, "", "","","","","", "", CommonConstants.TIPO_ORDEN_DSC));
    
        	request.setAttribute("etapa",control.getEtapa());
        	fcRestaLista(control);
        	numero = control.getListaDenominacion().size();
        	control.setSelTipo(numero);
        	
        	request.getSession().setAttribute("listConsulta", control.getListConsulta());
        	request.getSession().setAttribute("listEva",control.getListEva());
        	request.getSession().setAttribute("listaDenominacion", control.getListaDenominacion());
        	request.getSession().setAttribute("listTipoCalificacion",control.getListTipoCalificacion());
    	}
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_mto_cfg_calificacion_tipo_evaluacion_consulta","control",control);		
    }
    
    private String InsertTablaDetalle(TiposConsultaCommand control)
    {
    		Calificacion obj = new Calificacion();
    	
    		StringTokenizer stkEvaluaciones = new StringTokenizer(control.getEvaluacion(),"|");
    		String codEvaluacion;
    	
    		obj.setCodEtapa(control.getEtapa());
    		obj.setCodTipoEvaluacion(control.getCodTipoEva());
    		obj.setCodCalificacionEliminatoria(control.getCodCalifElimi());
    		while ( stkEvaluaciones.hasMoreTokens() )
    		{	codEvaluacion = stkEvaluaciones.nextToken();
    			obj.setCodCalificacionNormal(codEvaluacion);
    	
    			System.out.println("codetapa: "+obj.getCodEtapa());
    			System.out.println("codtipeva: "+obj.getCodTipoEvaluacion());
    			System.out.println("codcalelim: "+obj.getCodCalificacionEliminatoria());
    			System.out.println("codcalif: "+obj.getCodCalificacionNormal());
    			System.out.println("cod eval: "+control.getCodEval());
    			System.out.println("esta reg: "+CommonConstants.E_C_TTDE_EST_REG);
    			obj.setCodCalificacion(this.calificacionManager.InsertCalificacion(obj,control.getCodEval(),CommonConstants.E_C_TTDE_EST_REG));
    		}
    		return obj.getCodCalificacion();
    }
    
    private String DeleteCalificacion(TiposConsultaCommand control){
    	Calificacion obj = new Calificacion();
    	
    	obj.setCodEtapa(control.getEtapa());
    	obj.setCodTipoEvaluacion(control.getCodTipoEva());
    	obj.setCodCalificacion(this.calificacionManager.DeleteCalificaciones(obj, control.getCodEval()));
    	return obj.getCodCalificacion();
    }
    
    
	private void fcRestaLista(TiposConsultaCommand control) {
		List lstCiclos = control.getListTipoCalificacion();
		List lstCiclosEmpresa = control.getListaDenominacion();
		
		List lstResultado = null;
		
		if( lstCiclos!=null && lstCiclos.size()>0 )
		{	
			lstResultado = new ArrayList();
			
			TipoTablaDetalle obj = null;
			System.out.println("el numero de reg es :"+lstCiclos.size());
			for(int i=0;i<lstCiclos.size();i++)
			{
				System.out.println("i :"+i);
				obj = (TipoTablaDetalle)lstCiclos.get(i);				
				Calificacion obj2 = null;
				
				if(lstCiclosEmpresa !=null && lstCiclosEmpresa.size()>0)
				{
					boolean flgEncontrado = false;
					
					for(int j=0;j<lstCiclosEmpresa.size();j++)
					{
						obj2 = (Calificacion)lstCiclosEmpresa.get(j);			
						System.out.println("primer combo :"+obj.getCodTipoTablaDetalle()+"  el segundo combo  :"+ obj2.getCodCalificacion());
						if(obj.getCodTipoTablaDetalle().equalsIgnoreCase(obj2.getCodCalificacion()))
						flgEncontrado = true;					
						
					}
					
					if(!flgEncontrado)
						lstResultado.add(obj);
					
				}else
				{
					lstResultado = lstCiclos;
				}	

			}
			
		}
		control.setListTipoCalificacion(lstResultado);
	

	}
    
    
}
