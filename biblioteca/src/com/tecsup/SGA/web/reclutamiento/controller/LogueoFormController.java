package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.service.reclutamiento.*;
import com.tecsup.SGA.common.CommonMessage;

public class LogueoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(LogueoFormController.class);
	
	private ReclutaManager reclutaManager;
	
	/*SETTER*/
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	LogueoCommand command = new LogueoCommand();
    	
    	if ( request.getSession().getAttribute("txhCodPostulanteVD") != null )
    	{
    		command.setIdRec((String)request.getSession().getAttribute("txhCodPostulanteVD"));
    		command.setOperacion("ACCESO");
    		command.setTypeMessage("OK");
    		command.setCodUsuario((String)request.getSession().getAttribute("txhCodUsuarioVD"));
    		///log.warn("command.getUsuario()"+command.getUsuario()); 
    		//log.info("reemplazando postUsuario");
	    	request.getSession().setAttribute("postUsuario", command);
	    	request.getSession().setAttribute("indRRHH", "RRHH");
	    	request.getSession().removeAttribute("txhCodPostulanteVD");
	    	request.getSession().removeAttribute("txhCodUsuarioVD");
    	}
    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	LogueoCommand control = (LogueoCommand) command;
    	if ("ACCESO".equals(control.getOperacion())){
           	String rpta;
        	
        	//Valido Ingreso
        	rpta = this.reclutaManager.getAllRecluta(control.getUsuario(), control.getClave());

        	if (rpta.equals("0")){ //Postulante no existe
        		control.setMessage(CommonMessage.RECLUTA_NO_EXISTE);
        		control.setTypeMessage("ERROR");
        	}
        	else if (rpta.equals("1")){ //Clave incorrecta
        		control.setMessage(CommonMessage.RECLUTA_NO_PASSWORD);
        		control.setTypeMessage("PASS");
        	}
        	else{ //Recluta existe
        		control.setIdRec(rpta.split("_")[1]);
        		control.setTypeMessage("OK");
        		log.info("postUsuario existe");
            	request.getSession().setAttribute("postUsuario", control);
            	request.getSession().setAttribute("postUsuario2", control);
        	}
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_logueo","control",control);		
    }
}
