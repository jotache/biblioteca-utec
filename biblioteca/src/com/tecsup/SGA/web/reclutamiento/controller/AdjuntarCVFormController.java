package com.tecsup.SGA.web.reclutamiento.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Archivo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.reclutamiento.ReclutaManager;
import com.tecsup.SGA.web.reclutamiento.command.AdjuntarCVCommand;


public class AdjuntarCVFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdjuntarCVFormController.class);
	
	private ReclutaManager reclutaManager;
	
	/*SETTER*/
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
	
	public AdjuntarCVFormController(){
		super();
		setCommandClass(AdjuntarCVCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	//System.out.println("formBackingObject:INI");
    	AdjuntarCVCommand command = new AdjuntarCVCommand();

    	/*Recogiendo valores*/
    	command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	
    	/*Extensiones de los archivos adjuntos*/
//    	request.setAttribute("strExtensionJPG", CommonConstants.GSTR_EXTENSION_JPG);
//    	request.setAttribute("strExtensionGIF", CommonConstants.GSTR_EXTENSION_GIF);
//    	request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
//    	request.setAttribute("strExtensionPDF", CommonConstants.GSTR_EXTENSION_PDF);
    	
    	request.setAttribute("msgRutaServer", "");
    	
    	if("ACCESO".equals(command.getOperacion())){ //Ver Datos
    		inicializaDatos(command, command.getIdRec(), request);
    	}
    	
    	//System.out.println("formBackingObject:FIN");
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AdjuntarCVCommand control = (AdjuntarCVCommand) command;
		    	  
    	String uploadDirCV = CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_TMP;
		String uploadDirFOTO = CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_FOTO_TMP;
		String sep = System.getProperty("file.separator");
		
    	if("GRABAR".equals(control.getOperacion())){
    		    		    		    	
    		String rpta;
    		
    		inicializaDatos(control, control.getIdRec(), request);
    		
//    		MultipartFile multipartFile = control.getTxtCV2();
    		byte[] bytesCV = control.getTxtCV();
    		byte[] bytesFoto = control.getTxtFoto();
    		
    		String filenameCV;
            String filenameIMG;
        	
        	// cast to multipart file so we can get additional information
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            CommonsMultipartFile fileCV = (CommonsMultipartFile) multipartRequest.getFile("txtCV");
            CommonsMultipartFile fileFoto = (CommonsMultipartFile) multipartRequest.getFile("txtFoto");
          
//    		String uploadDirCV = getServletContext().getRealPath("/"+CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE);
//    		String uploadDirFOTO = getServletContext().getRealPath("/"+CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_FOTO);
            
//    		String uploadDirCV = CommonConstants.DIRECTORIO_DATA+CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE;
//    		String uploadDirFOTO = CommonConstants.DIRECTORIO_DATA+CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_FOTO;
    		
    		

    		
    		File dirPath = new File(uploadDirCV);
            if ( !dirPath.exists() )
                  dirPath.mkdirs();
                        
    		File dirPath2 = new File(uploadDirFOTO);
            if ( !dirPath2.exists() ) 
                  dirPath2.mkdirs();
            
	        if (bytesCV.length>0){
	        	 control.setCvOriginal(fileCV.getOriginalFilename()); //Nos da el nombre original
	        	 filenameCV = CommonConstants.FILE_NAME_CV + control.getIdRec() + "." +control.getExtCv().toLowerCase();
//	        	 File uploadedFileCV = new File(uploadDirCV + sep + filenameCV);
//	        	 FileCopyUtils.copy(bytesCV, uploadedFileCV);
	        	 
	        	 FileOutputStream output = null;
	        	 try{
	        		 output = new FileOutputStream(new File(uploadDirCV + sep + filenameCV));
	        		 output.write(bytesCV);
	        		 output.close();	        		 
	        	 }catch(Exception ex){
	        		 log.error("Error subiendo curriculum",ex);
	        	 }	        	 
	        }else{	        
	        	filenameCV = control.getCv();
	        	control.setCvOriginal(control.getCvOriginal());
	        }
	        
	        if (bytesFoto.length>0){
	        	control.setFotoOriginal(fileFoto.getOriginalFilename()); //Nos da el nombre original
	        	
	        	filenameIMG = CommonConstants.FILE_NAME_IMG + control.getIdRec() + "." +control.getExtFoto().toLowerCase();
//	        	File uploadedFileFoto = new File(uploadDirFOTO + sep + filenameIMG);
//	        	FileCopyUtils.copy(bytesFoto, uploadedFileFoto);
	        	FileOutputStream output = null;
	        	 try{
	        		 output = new FileOutputStream(new File(uploadDirFOTO + sep + filenameIMG));
	        		 output.write(bytesFoto);
	        		 output.close();	        		 
	        	 }catch(Exception ex){
	        		 log.error("Error subiendo imagen",ex);
	        	 }	 
	        	
	        }else{	        	
	        	filenameIMG = control.getFoto();
	        	control.setFotoOriginal(control.getFotoOriginal());
	        }
	        
	        if (control.getCodUsuario().equals("")) control.setCodUsuario(control.getIdRec());
	        
    		//Guardando en BD nombre de archivos
    		rpta = this.reclutaManager.updateReclutaArchAdjunto(control.getIdRec(), 
    															filenameCV + "|" + control.getCvOriginal(), 
    															filenameIMG + "|" + control.getFotoOriginal(),control.getCodUsuario());
    		

    		    		
    		if ("-1".equals(rpta)){ //Error Operación
    			control.setMessage(CommonMessage.RECLUTA_ERROR);
    			control.setTypeMessage("ERROR");
        	}
        	else{ //OK
        		control.setOperacion("ACCESO");        		
        		control.setMessage("Proceso terminado satisfactoriamente");        		
        		control.setFotoOriginal("");
        		control.setCvOriginal("");        		
        		control.setTypeMessage("OK");        		
        	}
    		
    		inicializaDatos(control, control.getIdRec(), request);
    		
    	}else if("OPEN_FOTO".equals(control.getOperacion())){
    		   		    		
    		String foto = control.getFoto() + control.getExtFoto();    		    		
    		Archivo.downloadFile(foto, uploadDirFOTO + sep + foto, response);
    		inicializaDatos(control, control.getIdRec(), request);
    		return null;
    		    		    		
    	}else if("OPEN_CV".equals(control.getOperacion())){

    		String cv = control.getCv() + control.getExtCv();    		    		
    		Archivo.downloadFile(cv, uploadDirCV + sep + cv, response);
    		inicializaDatos(control, control.getIdRec(), request);
    		return null;
    	}
    	
    	
    	
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/reclutamiento/postulante/rec_adjuntarCV","control",control);
    }
   
	/*Metodos*/
	public void inicializaDatos(AdjuntarCVCommand command, String codPersona, HttpServletRequest request){
		
		Recluta lObject = this.reclutaManager.getAllDatosPersonaRecluta(codPersona);
		if ( lObject != null ){
			command.setFoto(lObject.getFoto());
			command.setCv(lObject.getCv());
					
			command.setFotoOriginal(lObject.getFotoOriginal());
			command.setCvOriginal(lObject.getCvOriginal());
			
			command.setInfFinal(lObject.getInfFinal());
			command.setInfFinalGen(lObject.getInfFinalGen());
			
			//DEFINE LA RUTA DEL SERVIDOR
	    	/*String uploadDirArchivoCV = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_IP +
	    	CommonConstants.STR_RUTA_POSTULANTE;*/
	    	
//	    	String uploadDirArchivoCV = CommonConstants.GSTR_HTTP+
//	    	CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+
//	    	CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE;
//	    	request.setAttribute("msgRutaServer",uploadDirArchivoCV);
			
		}
	}

	
}
