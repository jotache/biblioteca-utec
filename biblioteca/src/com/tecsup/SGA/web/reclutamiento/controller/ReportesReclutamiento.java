package com.tecsup.SGA.web.reclutamiento.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.reclutamiento.ReportesManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.inicio.command.MenuCommand;
import com.tecsup.SGA.web.reclutamiento.command.LogueoCommand;
import com.tecsup.SGA.web.seguridad.command.SeguridadCommand;

public class ReportesReclutamiento implements Controller{
	
	private static Log log = LogFactory.getLog(ReportesReclutamiento.class);
	private ReportesManager reportesManager;
	private SeguridadManager seguridadManager;
	
	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception {
	 log.info("onSubmit:INI");
    
	 UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
	 if(usuarioSeguridad==null){		 
		InicioCommand command = new InicioCommand();    		
 		return new ModelAndView("/cabeceraReclutamiento","control",command); 		
	 }
	 
     ModelMap model = new ModelMap();
     String pagina="";
     String fechaRep = Fecha.getFechaActual();
	 String horaRep = Fecha.getHoraActual();
     List lista= new ArrayList();
     
     String codSede=request.getParameter("txhCodSede");
     String codTipoProceso=request.getParameter("txhCodTipoPro");
     String codUniFunci=request.getParameter("txhCodIniFun");
     String dscProceso=request.getParameter("txhDscProceso");
     String fecIni=request.getParameter("txhFecIni");
     String fecFin=request.getParameter("txhFecFin");
     String operacion=request.getParameter("txhOperacion");
     String apePaterno=request.getParameter("txhApePaterno");
     String apeMaterno=request.getParameter("txhApeMaterno");
     String nombre=request.getParameter("txhNombre");
     String codUsuario="";
     String dscUsuario="";
     String txhNomSede="";
     
     
    	if ( usuarioSeguridad != null )
    	{
    		codUsuario=usuarioSeguridad.getIdUsuario();
    		dscUsuario=usuarioSeguridad.getNomUsuario();
    		System.out.println("DscUsuario: "+dscUsuario);
    		List listaSede= new ArrayList();
    		listaSede=this.seguridadManager.getAllSedes();
    		if(codSede!=null)
    		{if(listaSede!=null)
    			{  SedeBean sede=new SedeBean();
    				for(int a=0;a<listaSede.size();a++)
    				{ sede=(SedeBean)listaSede.get(a);
    				if(codSede.equals(sede.getCodSede()))
    				{  txhNomSede=sede.getDscSede();
    			      a=listaSede.size();
    			    }
    			}
    			
    			}
    		}
    	}
    	
    		// Consulta v�lida
    		if(operacion.equals("1"))
    	    {   try {lista=reportesManager.GetAllProcesoEstado(codTipoProceso, codUniFunci, dscProceso, fecIni, 
    	    		fecFin);
    		          if(lista!=null && lista.size()>0)
    		          System.out.println("lista: "+lista.size());
    			} catch (Exception e) {
    				
    				System.out.println("Exception");
    			}
    			model.addObject("fechaIni", fecIni);
    		   	model.addObject("fechaFin", fecFin);	  
    			request.getSession().setAttribute("lista_cabecera", lista);
    			pagina = "/reclutamiento/consultasyReportes/rep_Gen_Proceso_Estado";
    	    }
    	    else{ if(operacion.equals("2"))
    		    {   
    	    		log.warn("codTipoProceso:"+codTipoProceso);
    	    		log.warn("codUniFunci:"+codUniFunci);
    	    		log.warn("dscProceso:"+dscProceso);
    	    		log.warn("nombre:"+nombre);
    	    		log.warn("apePaterno:"+apePaterno);
    	    		log.warn("apeMaterno:"+apeMaterno);
    	    		
    	    	try {lista=reportesManager.GetAllProcesoPostulante(codTipoProceso, codUniFunci, dscProceso,
    		    		nombre, apePaterno, apeMaterno,fecIni,fecFin);
    	               if(lista!=null && lista.size()>0)
    	               System.out.println("lista: "+lista.size());
    		    	} catch (Exception e) {
    		    		/*url="?txhOperacion=" + u + 			
    				"&txhCodTipoPro=" + document.getElementById("tipoProceso").value +  
    				"&txhCodIniFun=" + document.getElementById("unidadFuncional").value + 
    				"&txhDscProceso=" + document.getElementById("nomProceso").value +
    				"&txhApePaterno=" + document.getElementById("apePaterno").value +
    				"&txhNombre=" + document.getElementById("nom").value + 
    				"&txhApeMaterno=" + document.getElementById("apeMaterno").value ;*/
    		    		System.out.println("Exception");
    		    	}
    		    	model.addObject("nombre", nombre);
    			   	model.addObject("apePaterno", apePaterno);
    			   	model.addObject("apeMaterno", apeMaterno);
    				request.getSession().setAttribute("lista_cabecera", lista);
    		    	pagina = "/reclutamiento/consultasyReportes/rep_Gen_Proceso_Postulante";
    		    }
    	    	else if(operacion.equals("3"))
    			       {   try {lista=reportesManager.GetAllOferta(codTipoProceso, codUniFunci, dscProceso, fecIni, fecFin);
    			        	if(lista!=null && lista.size()>0)
    			        	System.out.println("lista: "+lista.size());
    			       } catch (Exception e) {
    					
    					System.out.println("Exception");
    				}
    			    model.addObject("fechaIni", fecIni);
    				model.addObject("fechaFin", fecFin);
    				request.getSession().setAttribute("lista_cabecera", lista);
    				pagina = "/reclutamiento/consultasyReportes/rep_Gen_Oferta";
    			       }
    	    }
    		model.addObject("fechaRep", fechaRep);
    		model.addObject("horaRep", horaRep);
    		model.addObject("NOM_SEDE", txhNomSede);
    		model.addObject("usuario", dscUsuario);
    				
    		     		 
    		 log.info("onSubmit:FIN");
    		 return new ModelAndView(pagina,"model",model);    		
    		//Fin Consulta v�lida
    	
    		
    		
    	       
    
}

	public ReportesManager getReportesManager() {
		return reportesManager;
	}

	public void setReportesManager(ReportesManager reportesManager) {
		this.reportesManager = reportesManager;
	}
}
