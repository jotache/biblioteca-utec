package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.*;

public class ModificarTipoEvaluacion1FormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ModificarTipoEvaluacion1FormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	
    	ModificarTipoEvaluacion1Command command = new ModificarTipoEvaluacion1Command();
    	command.setCodEval((String)request.getParameter("txhCodEva"));
    	System.out.println(command.getCodEval());
    	String codDetalle = (String)request.getParameter("txhCodDetalle");
    	String dscDetalle = (String)request.getParameter("txhDescripcion");
    	String dscValor1 = (String)request.getParameter("txhDscValor1");
    	String dscValor2 = (String)request.getParameter("txhDscValor2");
    	String cantidad1 = (String)request.getParameter("txhCantidad1");
    	if(dscValor2==null){
    		dscValor2 = "0";
    	}
    	if(cantidad1==null){
    		cantidad1 = "0";
    	}
    	
    	request.setAttribute("posicion",dscValor2);
    	request.setAttribute("cantidad1",cantidad1);
    	command.setCodDetalle("");
 
    	command.setCantidad1(cantidad1);
	    command.setCodDetalle(codDetalle);	
   		command.setDescripcion(dscDetalle);
    	command.setDscValor1(dscValor1);
    	command.setDscValor2(dscValor2);

    	log.info("formBackingObject:FIN");
        return command;
    } 
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	String resultado = "";
    	ModificarTipoEvaluacion1Command control = (ModificarTipoEvaluacion1Command) command;
    	request.setAttribute("cantidad1",control.getCantidad1());
    	if (control.getOperacion().trim().equals("MODIFICAR"))
    	{
    		resultado = UpdateTablaDetalle(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if ( resultado.equals("-2")) 
    		{
    			control.setMsg("DOBLE");
    		}
    		else control.setMsg("OK");
    	}
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_modificar_tipo_evaluacion_1","control",control);		
    }
    
    private String UpdateTablaDetalle(ModificarTipoEvaluacion1Command control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_EVA);
    		obj.setCodDetalle(control.getCodDetalle());
    		obj.setDescripcion(control.getDescripcion());
    		obj.setDscValor1(CommonConstants.PROC_ETAPA_SELECCION);
    		obj.setDscValor2(control.getDscValor2());
    		obj.setTipo(CommonConstants.E_C_TIPO);
    		
    		if ( !control.getCodDetalle().trim().equals("") )
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.ActualizarTablaDetalle(obj, control.getCodEval()));
    		System.out.println("se actualizo");
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    

    
}
