package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.bean.ProcesoEvaluadorBean;
import com.tecsup.SGA.modelo.Calificacion;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.CalificacionManager;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.ProcesoCommand;
import com.tecsup.SGA.web.reclutamiento.command.ProcesosEvaluadoresCommand;

public class ProcesosEvaluadoresSelFormController extends SimpleFormController{
	private ProcesoManager procesoManager;
	private CalificacionManager calificacionManager;
	private TablaDetalleManager tablaDetalleManager;
	private static Log log = LogFactory.getLog(ProcesosEvaluadoresSelFormController.class);
	
	public void setCalificacionManager(CalificacionManager calificacionManager) {
		this.calificacionManager = calificacionManager;
	}

	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
  	
		ProcesosEvaluadoresCommand command = new ProcesosEvaluadoresCommand();
    	/*Veo si es un editar*/
    	String codProceso = (String)request.getParameter("txhCodProceso");
    	command.setNroEvaluaciones("0");
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	if ( codProceso != null ) if ( codProceso.trim() != "" )
    	{
    		cargaDatosProceso(codProceso, command);
    		request.getSession().setAttribute("listaTipoEvaluaciones",command.getListTiposEvaluacion());
    	}    	        
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	ProcesosEvaluadoresCommand control = (ProcesosEvaluadoresCommand) command;
    	
    	if ( control.getOperacion().trim().equals("GRABAR") )
    	{
    		if ( !grabarEvaluadores(control) )
    			control.setMessage("OK");
    		else control.setMessage("ERROR");    			
    	}
		cargaDatosProceso(control.getCodProceso(), control);
		request.getSession().setAttribute("listaTipoEvaluaciones",control.getListTiposEvaluacion());
	    return new ModelAndView("/reclutamiento/procesos/rec_procesosevaluador_sel","control",control);		
    }
    
    private void cargaDatosProceso(String codProceso, ProcesosEvaluadoresCommand command)
    {
    	String dscCalificacionEliminatoria = null;
		Proceso proceso = this.procesoManager.getProceso(codProceso);
		TipoTablaDetalle tablaDetalle;
		ProcesoEvaluadorBean procesoEvaluadorBean;
		ArrayList listaEvaluaciones;
		ArrayList listaEvaluadores;
		ArrayList<Calificacion> arrCalifiaciones = null;
		ArrayList<TipoTablaDetalle> tablaDetalleArr; 
		TipoTablaDetalle tablaDetalleCal;
		Evaluador evaluador;
		
		
		command.setCodEvaluaciones("");
		command.setCodEvaluadores("");
		
		/*CARGAMOS DESCRIPCION DE CALIFICAION ELIMINATORIA*/
		if ( proceso != null )
		{
			command.setCodProceso(codProceso);
			command.setCodEtapa(proceso.getCodEtapa());
			command.setDscProceso(proceso.getDscProceso());
			command.setDscEtapa(proceso.getDscEtapa());
			
			listaEvaluadores = (ArrayList)this.procesoManager.getEvaluadorsByProceso(codProceso);
			listaEvaluaciones = (ArrayList)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
					, "", "" , command.getCodEtapa(), "", "", "", "", CommonConstants.TIPO_ORDEN_VAL3);			
			command.setListTiposEvaluacion(new ArrayList());
			
			if ( listaEvaluaciones != null )
			{
				command.setNroEvaluaciones(Integer.toString(listaEvaluaciones.size()));
				for ( int i = 0; i < listaEvaluaciones.size() ; i++)
				{
					procesoEvaluadorBean = new ProcesoEvaluadorBean();
					tablaDetalle = (TipoTablaDetalle)listaEvaluaciones.get(i);
					
					evaluador = getCodEvaluadorRelacionado(tablaDetalle.getCodTipoTablaDetalle(), listaEvaluadores);
					if ( evaluador != null )
						if ( !evaluador.getCodEvaluador().trim().equals("") )
						{
							command.setCodEvaluaciones( command.getCodEvaluaciones() + tablaDetalle.getCodTipoTablaDetalle() + "|" );
							command.setCodEvaluadores( command.getCodEvaluadores() + evaluador.getCodEvaluador().trim() + "|" );
						}
					
					/*if ( dscCalificacionEliminatoria == null )
					{*/
						Calificacion obj = new Calificacion();
						arrCalifiaciones = (ArrayList<Calificacion>)calificacionManager.getAllCalificacion(
													CommonConstants.PROC_ETAPA_SELECCION
													, tablaDetalle.getCodTipoTablaDetalle());
						if ( arrCalifiaciones != null )
							if ( arrCalifiaciones.size() > 0)
							{
								obj = arrCalifiaciones.get(0);
								tablaDetalleArr = (ArrayList<TipoTablaDetalle>)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_CALIFICACION
										, obj.getCodCalificacionEliminatoria(), "", "", "", "", "", ""
										, CommonConstants.TIPO_ORDEN_DSC);
								if ( tablaDetalleArr != null )
									if ( tablaDetalleArr.size() > 0 )
									{
										tablaDetalleCal = tablaDetalleArr.get(0);
										dscCalificacionEliminatoria = tablaDetalleCal.getDescripcion(); 
									}
							}
					//}
					
					procesoEvaluadorBean.setCodEvaluadorSel(evaluador != null ? evaluador.getCodEvaluador().trim() : "");
					procesoEvaluadorBean.setCodPadre(tablaDetalle.getCodTipoTabla());
					procesoEvaluadorBean.setCodDetalle(tablaDetalle.getCodTipoTablaDetalle());
					procesoEvaluadorBean.setDescripcion(tablaDetalle.getDescripcion());
					procesoEvaluadorBean.setDscEliminatoria(dscCalificacionEliminatoria);
					procesoEvaluadorBean.setValor1(tablaDetalle.getDscValor1());				
					
					procesoEvaluadorBean.setEvaluadores(this.procesoManager.getUsuarioByTipo(CommonConstants.ROL_JEFE_DEP, ""));
					command.getListTiposEvaluacion().add(procesoEvaluadorBean);
				}
			}
		}
    }
    private Evaluador getCodEvaluadorRelacionado(String codDetalle, ArrayList listaEvaluador)
    {
    	Evaluador evaluador = new Evaluador();
    	if ( listaEvaluador != null )
    	{
	    	for (int i = 0 ; i < listaEvaluador.size() ; i++ )
	    	{
	    		evaluador = (Evaluador)listaEvaluador.get(i);
	    		if ( evaluador.getCodRegEvaluador() == null ) evaluador.setCodRegEvaluador("");
	    		if ( codDetalle.trim().equals(evaluador.getCodTipoEvaluacion()))
	    			return evaluador;
	    	}
    	}
    	return null;
    }
    private boolean grabarEvaluadores(ProcesosEvaluadoresCommand control)
    {
    	
		StringTokenizer stkEvaluaciones = new StringTokenizer(control.getCodEvaluaciones(),"|");
		StringTokenizer stkEvaluadores = new StringTokenizer(control.getCodEvaluadores(),"|");
		
		String codEvaluacion;
		String codEvaluador;
		String strResultado = ""; 
		boolean flagError = false;
		
		Evaluador evaluador = new Evaluador();
		Evaluador evaluador1 = new Evaluador();
		ArrayList listaEvaluadores = (ArrayList)this.procesoManager.getEvaluadorsByProceso(control.getCodProceso().trim());
		
		while ( stkEvaluaciones.hasMoreTokens() )
		{
			codEvaluacion = stkEvaluaciones.nextToken();
			codEvaluador = stkEvaluadores.nextToken();
			
			evaluador = getCodEvaluadorRelacionado(codEvaluacion, listaEvaluadores);
			
			if ( evaluador == null ){
				strResultado = this.procesoManager.insertEvaluadorByProceso(control.getCodProceso()
						, codEvaluacion, codEvaluador, control.getCodUsuario());
				enviarMail(control,codEvaluacion,codEvaluador,true);
			}else if ( evaluador.getCodRegEvaluador().trim().equals("") ){
				strResultado = this.procesoManager.insertEvaluadorByProceso(control.getCodProceso()
					, codEvaluacion, codEvaluador, control.getCodUsuario());
				enviarMail(control,codEvaluacion,codEvaluador,true);
			}else{
				evaluador1 = procesoManager.getEvaluador(control.getCodProceso(), codEvaluacion, CommonConstants.PROC_ETAPA_SELECCION);
				strResultado = this.procesoManager.updateEvaluadorByProceso(evaluador.getCodRegEvaluador().trim()
						, control.getCodProceso(), codEvaluacion, codEvaluador, control.getCodUsuario());
				codEvaluador = evaluador1.getCodEvaluador();
				enviarMail(control,codEvaluacion,codEvaluador,false);
			}
			if ( strResultado == null ) flagError = true;
			else if ( strResultado.trim().equals("-1") ) flagError = true;
			
		}
		
		return flagError;
    }
    
    private void enviarMail(ProcesosEvaluadoresCommand control,String codEvaluacion, String codEvaluador, boolean insert){
    	//Validar que el entorno sea produccion:
    	java.util.ResourceBundle prop = java.util.ResourceBundle.getBundle("db");
    	String entorno = prop.getString("db.environment");
    	System.out.println("enviarMail().entorno:" + entorno);
    	if (entorno.equals("produccion")){
    		
    		Evaluador evaluador = new Evaluador();
    		Proceso proceso = this.procesoManager.getProceso(control.getCodProceso());
    		
			ServletEnvioCorreo envioCorreo = new ServletEnvioCorreo();
			    			
			evaluador = procesoManager.getEvaluador(control.getCodProceso(), codEvaluacion,CommonConstants.PROC_ETAPA_SELECCION);
			
			boolean enviar = false;
			
			if (evaluador == null){
				enviar=false;
			}else if (evaluador.getCodRegEvaluador().trim().equals("")){    				
				enviar=true;    				
			}else{    				
				//System.out.println("evaluador.getCodEvaluador():"+evaluador.getCodEvaluador());
				//System.out.println("codEvaluador:"+codEvaluador);
				if (insert==true || (!evaluador.getCodEvaluador().equals(codEvaluador) && !evaluador.getCodEvaluador().equals("0")) ){					
					enviar=true;
				}
			}
			
			if (enviar==true && evaluador!=null){
				String mensaje , asunto = "";    	
				List listaDatosMail = new ArrayList();
				listaDatosMail = this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_RECLUTAMIENTO_EMAIL 
	  	    			, "0002" , "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
				TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
				tipoTablaDetalle = (TipoTablaDetalle) listaDatosMail.get(0);
				
				asunto = tipoTablaDetalle.getDscValor1();
				asunto = asunto.replace("NOMBRE_PROCESO", proceso.getDscProceso());
	    		mensaje = tipoTablaDetalle.getDescripcion();
	    		mensaje = mensaje.replace("NOM_EVALUADOR",evaluador.getDscEvaluador());
	    		mensaje = mensaje.replace("NOMBRE_PROCESO",proceso.getDscProceso());
	    		mensaje = mensaje.replace("NOMBRE_ETAPA",evaluador.getDscTipoEvaluacion());
	    		
	    		System.out.println("evaluador.getCorreo():" + evaluador.getCorreo());
	    		System.out.println("CommonConstantsEmail.TECSUP_EMAIL:" + CommonConstantsEmail.TECSUP_EMAIL);
	    		System.out.println("evaluador.getDscEvaluador():" + evaluador.getDscEvaluador());
	    		System.out.println("From - Nombre:" + tipoTablaDetalle.getDscValor2());    	    		
	    		System.out.println("Texto a enviar en el Correo: \n"+ asunto + "\n" + mensaje);
	    		
	    		//TODO: Habilitar envio de correo (YA ESTA HABILITADO)
	    		
	    		/*
    			 * EnviarCorreo(
    			 * String correoDestino, String correoOrigen, 
    			 * String nombreDestino, String nombreOrigen,
					String asunto, String cuerpoMensaje) */
	    		
	    		try{
	    			envioCorreo.EnviarCorreo(evaluador.getCorreo(), CommonConstantsEmail.TECSUP_EMAIL, 
	    					evaluador.getDscEvaluador(), tipoTablaDetalle.getDscValor2(), 
	        	    		asunto, mensaje);
	        
	        	   //System.out.println("Texto a enviar en el Correo: \n"+ asunto + "\n" + mensaje); 		        	   
	 		   	 }catch(Exception ex){
	    	    	ex.printStackTrace();
	    	    	log.error(ex.getMessage(), ex);
	    	    }
	    	    
			}
    			    			    		
    	}
    }
    
}
