package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.BuscarAreasInteresCommand;

public class ProcesosAreaInteresFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(ProcesosBandejaFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	BuscarAreasInteresCommand command = new BuscarAreasInteresCommand();
    	/* Modificado by Renzo Anccana 23/04/2008 */
    	/*command.setListAreaIntNiv1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT1 
    			, "", "", "", "", "", "", "",CommonConstants.TIPO_ORDEN_DSC));*/
    	
    	/*
    	List lista=new ArrayList();
    	lista=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT2 
    			, "", "", "", "", "", "", "",CommonConstants.TIPO_ORDEN_DSC);  //null);
    	if(lista!=null)
    	{	command.setListAreaIntNiv2(lista);
    	    if(lista.size()>0)
    	     command.setLongitud(lista.size());	
    	    System.out.println("Size: "+lista.size());
    	}*/
    	
    	
    	/*  Fin */
    	List<TipoTablaDetalle> listin = this.tablaDetalleManager.getAllAreasInteresReclutamiento("0005", "0006", CommonConstants.TIPO_ORDEN_DSC);
    	
    	/*for (TipoTablaDetalle datos:listin){
    		System.out.println("" + datos.getTipo() + " " + datos.getCodTipoTabla()+ " " + datos.getCodTipoTablaDetalle() + " " + datos.getDescripcion());
    	}*/
    	
    	//request.getSession().setAttribute("listaDetalle",command.getListAreaIntNiv2());
    	if(listin!=null) {
    		command.setLongitud(listin.size());	
    	}    	
    	
    	request.getSession().setAttribute("listaDetalle",listin);
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	BuscarAreasInteresCommand control = (BuscarAreasInteresCommand) command;
    	
    	if (control.getOperacion().trim().equals("NIVEL2"))
    	{
    		control.setCodAreaIntNiv2("");
    		control.setListAreaIntNiv2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT2 
        			, "", "", control.getCodAreaIntNiv1().trim(), "", "", "", "",CommonConstants.TIPO_ORDEN_DSC));
    	}
		request.getSession().setAttribute("listaDetalle",control.getListAreaIntNiv2());
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/procesos/rec_procesosareainteres","control",control);		
    }
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
}
