package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.ReclutaManager;


public class RecMenuRegFormController   extends SimpleFormController{
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		RecMenuRegCommand command = new RecMenuRegCommand();
		
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	command.setOpeMenu((String)request.getParameter("txhOpMenu"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + 
    						"&txhOperacion=" + command.getOperacion() +
    						"&txhOpMenu=" + command.getOpeMenu() +
    						"&txhCodUsuario=" + command.getCodUsuario());

    	request.setAttribute("lstrURL", command.getLstrURL()); //ruta
    	request.setAttribute("idRec", command.getIdRec()); //idRecluta
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
    	RecMenuRegCommand control = (RecMenuRegCommand) command;
		
	    return new ModelAndView("/reclutamiento/postulante/rec_menu_registro","control",control);		
    }
	
}
