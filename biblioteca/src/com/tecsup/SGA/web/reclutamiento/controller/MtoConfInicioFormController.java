package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.modelo.*;

public class MtoConfInicioFormController extends SimpleFormController{
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		MtoConfInicioCommand command = new MtoConfInicioCommand();
		
		command.setCodEval((String)request.getParameter("txhCodUsuario"));
    	System.out.println(command.getCodEval());
    	command.setLstrURL("?txhCodUsuario=" + command.getCodEval() );
    	request.setAttribute("lstrURL", command.getLstrURL());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
    	MtoConfInicioCommand control = (MtoConfInicioCommand) command;
		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_mto_conf_inicio","control",control);		
    }

}
