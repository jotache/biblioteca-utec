package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.PostulanteManager;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.BuscarPostulanteCommand;

public class BuscarPostulanteFormController extends SimpleFormController{
	PostulanteManager postulanteManager;
	TablaDetalleManager tablaDetalleManager;
	ProcesoManager procesoManager;		
	
	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setPostulanteManager(PostulanteManager postulanteManager) {
		this.postulanteManager = postulanteManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		BuscarPostulanteCommand command = new BuscarPostulanteCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		
		command.setListEgresado(this.tablaDetalleManager.getAllInstitucion(CommonConstants.GSTR_INSTI_EDUCATIVA));
		command.setListGrado(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_ACADEMICO				
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
		command.setListProfesion(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PROFESION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
		
		String codProceso = request.getParameter("txhCodProceso");
		if( codProceso != null ) if( !codProceso.trim().equals("") )
		{
			command.setCodProceso(codProceso);
			request.getSession().setAttribute("listaBusqPostulantes",null);
		}		
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		String respuesta;
    	BuscarPostulanteCommand control = (BuscarPostulanteCommand) command;
    	
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{
    		control.setListDetalle(
    				this.postulanteManager.getAllPostulante(control.getNombres()
    				, control.getApellidos(), control.getCodAreasInteres(), control.getCodProfesion()
    				, control.getCodEgresado(), control.getCodGrado()
    				, control.getCodProceso(), "")
    				);
    	}
    	else if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		respuesta = grabarPostulante(control);
    		if ( respuesta == null ) control.setMessage("ERROR");
    		else if ( respuesta.trim().equals("-1")) control.setMessage("ERROR");
    		else if ( respuesta.trim().equals("-2")) control.setMessage("EXISTE");
    		else if ( respuesta.trim().equals("-3")) control.setMessage("OKPARCIAL");
    		else if ( respuesta.trim().equals("0")) control.setMessage("OK");
    	}
    	else if (control.getOperacion().trim().equals("VER_DATOS"))
    	{
    		request.getSession().setAttribute("txhCodPostulanteVD", control.getMessage());
    		request.getSession().setAttribute("txhCodUsuarioVD", control.getCodUsuario());
    		//System.out.println("usuario admin reclutamiento:"+control.getCodUsuario());
    		control.setMessage("VER_DATOS");
    		control.setListDetalle(
    				this.postulanteManager.getAllPostulante(control.getNombres()
    				, control.getApellidos(), control.getCodAreasInteres(), control.getCodProfesion()
    				, control.getCodEgresado(), control.getCodGrado()
    				, control.getCodProceso(), "")
    				);
    	}
    	
    	control.setCodPostulantesSeleccioandos("");
		request.getSession().setAttribute("listaBusqPostulantes",control.getListDetalle());
	
	    return new ModelAndView("/reclutamiento/procesos/rec_procesosbuscarpostulante","control",control);		
    }
    
    private String grabarPostulante(BuscarPostulanteCommand control)    
    {
    	
    	try
    	{
    		return this.procesoManager.insertPostulanteByProceso(control.getCodPostulantesSeleccioandos()
						    				, control.getCodProceso(), CommonConstants.INS_POST_BUS
						    				, CommonConstants.PROC_ETAPA_REVISION
						    				, CommonConstants.EST_REV_SIN_REVISION
						    				, control.getCodUsuario());
    	}
    	catch(Exception ex)
    	{
    		
    	}
    	return null;
    }

}
;