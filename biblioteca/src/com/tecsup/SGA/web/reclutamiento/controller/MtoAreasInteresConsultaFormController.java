package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.common.*;


public class MtoAreasInteresConsultaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoAreasInteresConsultaFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MtoAreasInteresConsultaCommand command = new MtoAreasInteresConsultaCommand();
    	command.setCodEval((String)request.getParameter("txhCodEva"));
    	System.out.println(command.getCodEval());
    	
    	command.setListAreasNivelUno(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT1
    				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	request.getSession().setAttribute("listaAreasIntUno", command.getListAreasNivelUno());
   	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	MtoAreasInteresConsultaCommand control = (MtoAreasInteresConsultaCommand) command;
    	
    	if ( control.getOperacion().trim().equals("NIVEL2"))
    	{
    		control.setListAreasNivelDos(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT2
    				, "", "", control.getCodAreasNivelUno().trim(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
    	}
    	else if(control.getOperacion().trim().equals("ELIMINAR")){
    		resultado = DeleteTablaDetalle(control);
    		if ( resultado.equals("-1")){ 
    			control.setMsg("ERROR");
    		}
    		else if( resultado.equals("-2")){
    			control.setMsg("DOBLE");
    		}
    		else control.setMsg("OK");
    		control.setListAreasNivelUno(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT1
    				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	}
    	else if(control.getOperacion().trim().equals("ELIMINAR2")){
    		resultado = DeleteTablaDetalle2(control);
    		if ( resultado.equals("-1")){
    		control.setMsg2("ERROR");	
    		}
    		else if( resultado.equals("-2")){
    			control.setMsg2("DOBLE");
    		}
    		else control.setMsg2("OK");
    		control.setListAreasNivelDos(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AREA_INT2
    				, "", "", control.getCodAreasNivelUno().trim(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	}
    	
    	request.getSession().setAttribute("listaAreasIntDos", control.getListAreasNivelDos());
    	request.getSession().setAttribute("listaAreasIntUno", control.getListAreasNivelUno());
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_mto_cfg_areas_interes_consulta","control",control);		
    }

    private String DeleteTablaDetalle(MtoAreasInteresConsultaCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla(CommonConstants.TIPT_AREA_INT1);
    		obj.setCadCod(control.getCodAreasNivelUno());
    		obj.setNroReg("1");
    	
    		if (!obj.getCadCod().trim().equals("") )
    		{
    			System.out.println("codeval "+control.getCodEval());
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.DeleteTablaDetalle(obj, control.getCodEval()));	
    			return obj.getCodTipoTablaDetalle();
    	   	}
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private String DeleteTablaDetalle2(MtoAreasInteresConsultaCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla(CommonConstants.TIPT_AREA_INT2);
    		obj.setCadCod(control.getCodAreasNivelDos());   		
    		obj.setNroReg("1");	
 
    		if (!obj.getCadCod().trim().equals("") )
    		{
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.DeleteTablaDetalle(obj, control.getCodEval()));	
    			return obj.getCodTipoTablaDetalle();
    		}
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
}
