package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.reclutamiento.*;

public class EstudiosFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(EstudiosFormController.class);

	private TablaDetalleManager tablaDetalleManager;
	private ReclutaManager reclutaManager;

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	//System.out.println("formBackingObject:INI");
    	EstudiosCommand command = new EstudiosCommand();

    	/*Llenado de los combos*/
    	inicializaCombos(command);
    	
    	/*Recogiendo valores*/
    	command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	command.setIndMenu((String)request.getParameter("txhOpMenu"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	
    	if("ACCESO".equals(command.getOperacion())){ //Ver Datos
    		inicializaDatosSecundaria(command, command.getIdRec());
    		inicializaDatosIdiomas(command, command.getIdRec());
    		inicializaDatosSuperior(command, command.getIdRec());
    	}
    	
    	System.out.println("formBackingObject:FIN");
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	EstudiosCommand control = (EstudiosCommand) command;
		
    	if (control.getCodUsuario().equals("")) control.setCodUsuario(control.getIdRec());
    	
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta;
    		
    		
    		
    		rpta = this.reclutaManager.updateReclutaEstSec(control.getIdRec(), control.getColegio1(), 
    				control.getColegio2(), control.getAnioInicio1(), control.getAnioFin1(), 
    				control.getAnioInicio2(), control.getAnioFin2(),control.getCodUsuario());
    		
    		if ("-1".equals(rpta)){ //Error Operación
    			control.setMessage(CommonMessage.RECLUTA_ERROR);
    			control.setTypeMessage("ERROR");
        	}
        	else{
        		rpta="";
        		rpta = this.reclutaManager.updateReclutaIdiomas(control.getIdRec(), control.getCadenaIdioma(), control.getNroRegIdioma(),control.getCodUsuario());
        		
        		if ("-1".equals(rpta)){ //Error Operación
        			control.setMessage(CommonMessage.RECLUTA_ERROR);
        			control.setTypeMessage("ERROR");
            	}
        		else{
        			rpta="";
        			rpta = this.reclutaManager.updateReclutaEstSup(control.getIdRec(), control.getCadenaEstSup(), control.getNroRegEstSup(),control.getCodUsuario());
        			
        			if ("-1".equals(rpta)){ //Error Operación
            			control.setMessage(CommonMessage.RECLUTA_ERROR);
            			control.setTypeMessage("ERROR");
                	}
            		else{
            			control.setOperacion("ACCESO");
                		control.setMessage(CommonMessage.GRABAR_EXITO);
                		control.setTypeMessage("OK");    	
            		}		
        		}
        	}
    		inicializaDatosSecundaria(control, control.getIdRec());
    		inicializaDatosIdiomas(control, control.getIdRec());
    		inicializaDatosSuperior(control, control.getIdRec());
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/rec_estudios","control",control);		
    }

	/*SETTER*/
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}

	/*Metodos*/
	private void inicializaCombos(EstudiosCommand command){
		//Idioma
		command.setListaIdioma1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_IDIOMAS
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaIdioma2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_IDIOMAS
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaIdioma3(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_IDIOMAS
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	//Nivel
    	command.setListaNivel1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_NIVEL_IDIOMAS
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaNivel2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_NIVEL_IDIOMAS
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaNivel3(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_NIVEL_IDIOMAS
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	//Grado Comprensión
    	command.setListaGrado1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_COMPRENSION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaGrado2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_COMPRENSION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaGrado3(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_COMPRENSION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	//Grado Académico
    	command.setListaGradoAcad1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_ACADEMICO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_VAL2));
    	command.setListaGradoAcad2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_ACADEMICO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_VAL2));
    	command.setListaGradoAcad3(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_ACADEMICO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_VAL2));
    	//Area Estudio
    	command.setListaAreaEstudio1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PROFESION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaAreaEstudio2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PROFESION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaAreaEstudio3(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PROFESION
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	//Institucion
    	command.setListaInstitucion1(this.tablaDetalleManager.getAllInstitucion(CommonConstants.GSTR_INSTI_EDUCATIVA));
    	command.setListaInstitucion2(this.tablaDetalleManager.getAllInstitucion(CommonConstants.GSTR_INSTI_EDUCATIVA));
    	command.setListaInstitucion3(this.tablaDetalleManager.getAllInstitucion(CommonConstants.GSTR_INSTI_EDUCATIVA));
    	//Merito
    	command.setListaMerito1(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_MERITO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaMerito2(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_MERITO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	command.setListaMerito3(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_MERITO
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
	}
	
	public void inicializaDatosIdiomas(EstudiosCommand command, String codPersona){
		ArrayList lista;
		Recluta lObject;
		int i;
		
		lista = (ArrayList)this.reclutaManager.getReclutaIdiomas(codPersona);
		if (lista.size()>0){
			for( i = 0; i < lista.size() ; i++){
				lObject = (Recluta) lista.get(i);
				switch (i) {
					case 0:
						command.setIdioma1(lObject.getIdioma());
						command.setNivel1(lObject.getNivel());
						command.setGrado1(lObject.getGrado());
						command.setCodEstudioIdioma1(lObject.getCodEstudioIdioma());
						break;
					case 1:
						command.setIdioma2(lObject.getIdioma());
						command.setNivel2(lObject.getNivel());
						command.setGrado2(lObject.getGrado());
						command.setCodEstudioIdioma2(lObject.getCodEstudioIdioma());
						break;
					case 2:
						command.setIdioma3(lObject.getIdioma());
						command.setNivel3(lObject.getNivel());
						command.setGrado3(lObject.getGrado());
						command.setCodEstudioIdioma3(lObject.getCodEstudioIdioma());
						break;
				}
			}
		}
	}
	
	public void inicializaDatosSecundaria(EstudiosCommand command, String codPersona)
	{
		//command.setIdRec(codPersona);
		Recluta lObject = this.reclutaManager.getAllDatosPersonaRecluta(codPersona);
		if ( lObject != null )
		{
			command.setColegio1(lObject.getColegio1());
			command.setColegio2(lObject.getColegio2());
			command.setAnioInicio1(lObject.getAnioInicio1());
			command.setAnioInicio2(lObject.getAnioInicio2());
			command.setAnioFin1(lObject.getAnioFin1());
			command.setAnioFin2(lObject.getAnioFin2());
		}
	}
	
	public void inicializaDatosSuperior(EstudiosCommand command, String codPersona){
		ArrayList lista;
		Recluta lObject;
		int i;
		lista = (ArrayList)this.reclutaManager.getReclutaSuperior(codPersona);
		if (lista.size()>0){
			for( i = 0; i < lista.size() ; i++){
				lObject = (Recluta) lista.get(i);
				switch (i) {
					case 0:
						command.setGradoAcad1(lObject.getGradoAcad());
						command.setAreaEstudio1(lObject.getAreaEstudio());
						command.setInstitucion1(lObject.getInstitucion());
						command.setOtraInstitucion1(lObject.getOtraInstitucion());
						command.setCiclo1(lObject.getCiclo());
						command.setMerito1(lObject.getMerito());
						command.setInicio1(lObject.getInicio());
						command.setTermino1(lObject.getTermino());
						command.setCodEstudioSup1(lObject.getCodEstudioSup());
						break;
					case 1:
						command.setGradoAcad2(lObject.getGradoAcad());
						command.setAreaEstudio2(lObject.getAreaEstudio());
						command.setInstitucion2(lObject.getInstitucion());
						command.setOtraInstitucion2(lObject.getOtraInstitucion());
						command.setCiclo2(lObject.getCiclo());
						command.setMerito2(lObject.getMerito());
						command.setInicio2(lObject.getInicio());
						command.setTermino2(lObject.getTermino());
						command.setCodEstudioSup2(lObject.getCodEstudioSup());
						break;
					case 2:
						command.setGradoAcad3(lObject.getGradoAcad());
						command.setAreaEstudio3(lObject.getAreaEstudio());
						command.setInstitucion3(lObject.getInstitucion());
						command.setOtraInstitucion3(lObject.getOtraInstitucion());
						command.setCiclo3(lObject.getCiclo());
						command.setMerito3(lObject.getMerito());
						command.setInicio3(lObject.getInicio());
						command.setTermino3(lObject.getTermino());
						command.setCodEstudioSup3(lObject.getCodEstudioSup());
						break;
				}
			}
		}
	}
	
	
}
