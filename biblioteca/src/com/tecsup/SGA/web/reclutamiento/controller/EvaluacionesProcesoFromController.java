package com.tecsup.SGA.web.reclutamiento.controller;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Evaluacion;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.EvaluacionProcesosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.reclutamiento.command.EvaluacionesProcesoCommand;

public class EvaluacionesProcesoFromController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(EvaluacionesProcesoFromController.class);
	private EvaluacionProcesosManager evaluacionProcesosManager;
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		EvaluacionesProcesoCommand command = new EvaluacionesProcesoCommand();
		
		command.setCodUsuario(request.getParameter("txhCodUsuario"));
		command.setIndEtapa("0");
		command.setListRevision(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluador(
				CommonConstants.PROC_ETAPA_REVISION
				, CommonConstants.EST_REV_REV_X_RRHH
				, CommonConstants.EST_REV_REV_X_RRHH
				, command.getCodUsuario(),command.getCodProceso()));
		
		String ruta = CommonConstants.GSTR_HTTP+
    	CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+
    	CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE;
		
		request.setAttribute("msgRutaServer",ruta);
    	/*
		List lista = new ArrayList();
		if (command.getListRevision()!=null){
			for(int i=0;i<command.getListRevision().size(); i++) {
				EvaluacionesByEvaluadorBean proceso = (EvaluacionesByEvaluadorBean) command.getListRevision().get(i);				
				if (existeItem(lista, proceso.getCodProceso())==false){
					lista.add(proceso);
				}
			}
		}
		*/
		
		command.setListaProcesos(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluadorCbo(
				CommonConstants.PROC_ETAPA_REVISION
				, CommonConstants.EST_REV_REV_X_RRHH
				, CommonConstants.EST_REV_REV_X_RRHH
				, command.getCodUsuario()));
		
		if(command.getOperacion()==null){
			log.info("BUSCAR POSTULANTES");
			command.setListPostRevisados(this.evaluacionProcesosManager.getEvaluacionesRevisadas("0", command.getCodUsuario()));
			log.info("Tama�o lista de revisados:" + command.getListPostRevisados().size());
		}
		
		request.getSession().setAttribute("listRevision",command.getListRevision() );
		request.getSession().setAttribute("listSeleccion",command.getListSeleccion() );
		
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String respuesta = "";
    	EvaluacionesProcesoCommand control = (EvaluacionesProcesoCommand) command;
    	if ( control.getOperacion().trim().equals("BUSCAR_REV") )
    	{
    		//System.out.println("control.getCodProceso():" + control.getCodProceso());
    		
    		control.setListRevision(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluador(
    				CommonConstants.PROC_ETAPA_REVISION
    				, CommonConstants.EST_REV_REV_X_RRHH //CommonConstants.EST_REV_REV_X_RRHH
    				, CommonConstants.EST_REV_REV_X_RRHH //CommonConstants.EST_REV_REV_X_RRHH
    				, control.getCodUsuario(),control.getCodProceso()));
    		control.setListSeleccion(null);
    		
        	control.setListaProcesos(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluadorCbo(
    				CommonConstants.PROC_ETAPA_REVISION
    				, CommonConstants.EST_REV_REV_X_RRHH
    				, CommonConstants.EST_REV_REV_X_RRHH
    				, control.getCodUsuario()));    	
        	
        	control.setListPostRevisados(this.evaluacionProcesosManager.getEvaluacionesRevisadas(control.getCodProceso(), control.getCodUsuario()));
        	
    	}
    	else if ( control.getOperacion().trim().equals("BUSCAR_SEL") )
    	{
    		    		    		
    		control.setListSeleccion(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluador(
    				CommonConstants.PROC_ETAPA_SELECCION
    				, CommonConstants.EST_SEL_ENVIADO_EVAL
    				, ""
    				, control.getCodUsuario(),control.getCodProceso()));
    		control.setListRevision(null);
    		
        	control.setListaProcesos(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluadorCbo(
    				CommonConstants.PROC_ETAPA_SELECCION
    				, CommonConstants.EST_REV_REV_X_RRHH
    				, CommonConstants.EST_REV_REV_X_RRHH
    				, control.getCodUsuario()));
        	
        	control.setListPostRevisados(null);
    	}    	
    	else if ( control.getOperacion().trim().equals("ENVIAR_RRHH") ) {
    		
    		//TODO: Habilitar envio a RRHH (YA ESTA HABILITADO)
    		respuesta = enviarRRHH(control);
    		//respuesta = "0";
    		
    		System.out.println("respuesta ENVIAR_RRHH:" + respuesta);
    		
    		//Envio de Mails a RRHH... 
    		//enviarMail(control.getIndEtapa(),control.getCodEvaEnProcs());
    		
    		//System.out.println("control.getIndEtapa():"+control.getIndEtapa());
    		
       		if ( respuesta == null ) control.setMsg("ERROR");
    		else if (respuesta.trim().equals("-1")) control.setMsg("ERROR");
    		else if (Long.valueOf(respuesta)>0)
    		{
    			if ( control.getIndEtapa().equals("1") )
    			{
    	    		control.setListSeleccion(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluador(
    	    				CommonConstants.PROC_ETAPA_SELECCION
    	    				, CommonConstants.EST_SEL_ENVIADO_EVAL
    	    				, ""
    	    				, control.getCodUsuario(),control.getCodProceso() ));
    	    		control.setListRevision(null);
    	    		control.setListPostRevisados(null);
    	        	control.setListaProcesos(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluadorCbo(
    	    				CommonConstants.PROC_ETAPA_SELECCION
    	    				, CommonConstants.EST_REV_REV_X_RRHH
    	    				, CommonConstants.EST_REV_REV_X_RRHH
    	    				, control.getCodUsuario()));     	    		
    			}
    			else
    			{
    	    		control.setListRevision(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluador(
    	    				CommonConstants.PROC_ETAPA_REVISION
    	    				, CommonConstants.EST_REV_REV_X_RRHH
    	    				, CommonConstants.EST_REV_REV_X_RRHH
    	    				, control.getCodUsuario(),control.getCodProceso()));
    	    		control.setListPostRevisados(this.evaluacionProcesosManager.getEvaluacionesRevisadas(control.getCodProceso(), control.getCodUsuario()));
    	        	control.setListaProcesos(this.evaluacionProcesosManager.getAllEvaluacionesByEvaluadorCbo(
    	    				CommonConstants.PROC_ETAPA_REVISION
    	    				, CommonConstants.EST_REV_REV_X_RRHH
    	    				, CommonConstants.EST_REV_REV_X_RRHH
    	    				, control.getCodUsuario())); 
    	    		
    	    		control.setListSeleccion(null);
    			}
    			control.setMsg("OK");
    		}
    	}
    	

    	
    	control.setOperacion("");
    	control.setCodPostulantes("");
    	control.setNomPostulantes("");
    	control.setCodTipoEvals("");
    	control.setDscTipoEvals("");
    	control.setCodProcesos("");
    	control.setCodEvaEnProcs("");
    	control.setCodPostulanteProceso("");
		request.getSession().setAttribute("listRevision",control.getListRevision());		
		request.getSession().setAttribute("listSeleccion",control.getListSeleccion());
		
	    return new ModelAndView("/reclutamiento/evaluaciones/eva_bandejaevaluaciones","control",control);
	    
    }
    
    private String enviarRRHH(EvaluacionesProcesoCommand control){    
    	try{
    	
			if ( control.getIndEtapa().equals("1") ){			
				return this.evaluacionProcesosManager.updateEvaluacionPostulante(control.getCodPostulanteProceso()
	    				, control.getCodEvaEnProcs(), CommonConstants.EST_SEL_EVALUADO
	    				, "", "", "", "", CommonConstants.EVAL_ACT_ESTADO, control.getCodUsuario());
			}else{
				return this.evaluacionProcesosManager.updateEvaluacionPostulante(control.getCodPostulanteProceso()
    				, control.getCodEvaEnProcs(), CommonConstants.EST_REV_REV_X_JEFE_DPTO
    				, "", "", "", "", CommonConstants.EVAL_ACT_ESTADO, control.getCodUsuario());
			}
    	}
    	catch(Exception ex){    	
    		ex.printStackTrace();
    	}
    	return null;
    }
    
    private void enviarMail(String etapa,String codsEvaluaciones){
    	System.out.println("enviarMail#etapa:"+etapa);
    	System.out.println("enviarMail#codsEvaluaciones:"+codsEvaluaciones);
    	//System.out.println("enviarMail#codEvaluador:"+codEvaluador);
    	
    	java.util.ResourceBundle prop = java.util.ResourceBundle.getBundle("db");
    	String entorno = prop.getString("db.environment");
    	if (entorno.equals("produccion")){
    		
    		//if (etapa.equals("0")){
    			
    			//Etapa de Revisi�n
        		int nroEvaluacion=0;
        		StringTokenizer evaluaciones = new StringTokenizer(codsEvaluaciones,"|");
        		String codEvaluacion = "";
    			String datos[][];
    			datos = new String[100][5]; //C�digo Proceso | Nombre proceso |Cantidad |Descripci�n tipo de Evaluacion |Nombre evaluador 
        		
        		while (evaluaciones.hasMoreTokens()){
        			nroEvaluacion=nroEvaluacion+1;
        			codEvaluacion = evaluaciones.nextToken();
        			System.out.println("enviarMail#codEvaluacion:"+codEvaluacion);
        			
        			Evaluacion evaluacion = this.evaluacionProcesosManager.getEvaluacionById(codEvaluacion);
        			
        			if (evaluacion!=null){    				
        				//evaluar agregar elemento
        				boolean encuentraProceso=false;
        				int nroFilaProceso=0;
        				int nroPostProceso=0;
        				int nroTotalElem=0;
        				for (int i=0;i<datos.length;i++){
        					if (datos[i][0]!=null){
        						if (datos[i][0].equals(evaluacion.getCodProceso())){
        							encuentraProceso = true;
        							nroFilaProceso = i;
        							nroPostProceso = Integer.valueOf(datos[i][2]).intValue();
        						}
        					}    					
        				}
        				
        				//obtener cantidad de filas
    					for(int i=0;i<datos.length;i++)
    						if (datos[i][0]!=null) nroTotalElem = nroTotalElem + 1;
        					//for(int col=0;col<datos[i].length;col++)
        						//if (datos[i][col]!=null) nroTotalElem = nroTotalElem + 1;
        									
        				if (encuentraProceso==true){
        					//acumulando
        					datos[nroFilaProceso][2] =String.valueOf(Integer.valueOf(datos[nroFilaProceso][2]).intValue() + 1);    					
        				}else{
        					//agregando un nuevo elemento
        					//System.out.println("nroTotalElem:"+nroTotalElem);
        					datos[nroTotalElem][0] = evaluacion.getCodProceso();
        					datos[nroTotalElem][1] = evaluacion.getNombreProceso();
        					datos[nroTotalElem][2] = String.valueOf(nroPostProceso+1).toString();
        					datos[nroTotalElem][3] = evaluacion.getDesTipoEvaluacion();
        					datos[nroTotalElem][4] = evaluacion.getNombreEvaluador();
        				}
        				    				
        			}
        			
        		}//end while
        		
    			//ver resultados:
    			//obtener cantidad de elementos
        		
    			for(int i=0;i<datos.length;i++){				
    				if (datos[i][0]!=null){
    					//Obteniendo datos del mensaje seg�n la etapa: REVISION=>tipt_id='0004' SELECCION=>tipt_id='0006'
    					List listaDatosMail = new ArrayList();
    					listaDatosMail = this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_RECLUTAMIENTO_EMAIL,
    							(etapa.equals("0")?"0004":"0006") ,
    							"","","","","","",CommonConstants.TIPO_ORDEN_DSC);
    					TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
    					tipoTablaDetalle = (TipoTablaDetalle) listaDatosMail.get(0);						
    					
    					String mensaje , asunto = "";
    					asunto = tipoTablaDetalle.getDscValor1();
    					asunto = asunto.replace("NOM_PROCESO", datos[i][1]);											
    					
    					
    					mensaje = tipoTablaDetalle.getDescripcion();
    					mensaje = mensaje.replace("NOM_PROCESO", datos[i][1]);
    					mensaje = mensaje.replace("NRO_ENVIADOS", datos[i][2]);
    					
    					if (etapa.equals("0"))
    						mensaje = mensaje.replace("NOM_EVALUACION", datos[i][3]);
    					
    					mensaje = mensaje.replace("NOM_EVALUADOR", datos[i][4]);						
    					
        				 System.out.println("Correo Origen:" + CommonConstantsEmail.TECSUP_EMAIL);
        				 System.out.println("From - Nombre:" + tipoTablaDetalle.getDscValor2());    	    		
        				 System.out.println("Texto a enviar en el Correo: \n"+ asunto + "\n" + mensaje);
        				
    		    		//TODO: Habilitar Envio de Correo (YA ESTA HABILITADO)
    		    		
    		    		try {
    		    			ServletEnvioCorreo envioCorreo = new ServletEnvioCorreo();
    		    			String origen[][]; 
    		    			origen  = new String[1][2]; // 1 origen
    		    			origen[0][0] = CommonConstantsEmail.TECSUP_EMAIL;
    						origen[0][1] = tipoTablaDetalle.getDscValor2();
    						
    						/*
    						 * enviarEmail(String asunto, 
    						 * String destino[][], 
    						 * String origen[][], 
    						 * String cuerpo)
    						 * */
    						
    						envioCorreo.enviarEmail(asunto, CommonConstantsEmail.DESTINOS_RRHH, origen, mensaje);
    						
    		    		 }catch(Exception ex){
    			    	    	ex.printStackTrace();
    			    	    	log.error(ex.getMessage(), ex);
    		    		}			    					
    				}				
    			}//end for    			
    		
    	}//fin entorno
    	
    }
    
    
    
	public void setEvaluacionProcesosManager(
			EvaluacionProcesosManager evaluacionProcesosManager) {
		this.evaluacionProcesosManager = evaluacionProcesosManager;
	}	
	
	private boolean existeItem(List lista, String item){
		String dato = "";
		if (lista!=null){
			for(int i=0; i<lista.size(); i++) {			
				dato = ((EvaluacionesByEvaluadorBean) lista.get(i)).getCodPostProceso();
				if (item.equalsIgnoreCase(item))
					return true;
			}
		}
		return false;
	}
}
