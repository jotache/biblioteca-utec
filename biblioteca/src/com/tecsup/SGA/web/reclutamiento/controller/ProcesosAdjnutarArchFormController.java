package com.tecsup.SGA.web.reclutamiento.controller;

import java.io.File;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.web.reclutamiento.command.AdjuntarCVCommand;

public class ProcesosAdjnutarArchFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ProcesosBandejaFormController.class);
	private ProcesoManager procesoManager;
	
	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
		
	}

	public ProcesosAdjnutarArchFormController(){
		super();
		setCommandClass(AdjuntarCVCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AdjuntarCVCommand command = new AdjuntarCVCommand();
    	
    	command.setIdRec(request.getParameter("txhCodPostulante"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	
    	request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
    	request.setAttribute("strExtensionPDF", CommonConstants.GSTR_EXTENSION_PDF);
    	request.setAttribute("strExtensionXLS", CommonConstants.GSTR_EXTENSION_XLS);
    	request.setAttribute("strExtensionDOCX", CommonConstants.GSTR_EXTENSION_DOCX);
    	request.setAttribute("strExtensionXLSX", CommonConstants.GSTR_EXTENSION_XLSX);
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class, nf, true));
    	binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }

    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");		
    	AdjuntarCVCommand control = (AdjuntarCVCommand) command;
    	
    	String respuesta = "";
    	if ( control.getOperacion().trim().equals("GRABAR"))
    	{
    		byte[] bytesInf = control.getTxtCV();
    		
    		String filenameInf;

//            String uploadDirInf = getServletContext().getRealPath("/"+CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_INFFIN);
            String uploadDirInf = CommonConstants.DIRECTORIO_DATA+CommonConstants.DIRECTORIO_STR_RUTA_POSTULANTE_INFFIN;
    		File dirPath = new File(uploadDirInf);
            if ( !dirPath.exists() )
                  dirPath.mkdirs();
            
            
            String sep = System.getProperty("file.separator");
            
            if (bytesInf.length>0){
            	
        	 filenameInf = CommonConstants.FILE_NAME_INF + control.getIdRec().trim() + "." +control.getExtCv();
       	 	 File uploadedFileInf = new File(uploadDirInf + sep + filenameInf);
       	 	 FileCopyUtils.copy(bytesInf, uploadedFileInf);
           	 	
           	 respuesta = this.procesoManager.setInformeFinalPostulante(control.getIdRec()
           	 			, control.getExtFoto(), filenameInf, control.getCodUsuario());
           	 if ( respuesta == null ) respuesta = "ERROR";
           	 else if ( respuesta.trim().equals("0")) respuesta = "OK";
           	 else respuesta = "ERROR";

           }
           else respuesta = "NO_EXI";
    	}
    	control.setMessage(respuesta);
	    return new ModelAndView("/reclutamiento/procesos/rec_adjuntar_docfinal","control",control);
	}
}
