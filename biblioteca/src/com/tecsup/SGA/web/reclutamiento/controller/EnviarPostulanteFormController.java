package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.NumberFormat;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.bean.ProcesoEvaluadorBean;
import com.tecsup.SGA.web.reclutamiento.command.EnviarPostulanteCommand;
import com.tecsup.SGA.modelo.Postulante;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.reclutamiento.EvaluacionProcesosManager;
import com.tecsup.SGA.service.reclutamiento.CalificacionManager;

public class EnviarPostulanteFormController extends SimpleFormController{
	TablaDetalleManager tablaDetalleManager;
	ProcesoManager procesoManager;
	EvaluacionProcesosManager evaluacionProcesosManager;
	CalificacionManager calificacionManager;
		
	public void setCalificacionManager(CalificacionManager calificacionManager) {
		this.calificacionManager = calificacionManager;
	}

	public void setEvaluacionProcesosManager(
			EvaluacionProcesosManager evaluacionProcesosManager) {
		this.evaluacionProcesosManager = evaluacionProcesosManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		EnviarPostulanteCommand command = new EnviarPostulanteCommand();
		
		String codProceso = request.getParameter("txhCodProceso");
		String codPostSele = request.getParameter("txhCodPostSele");
		String nomPostSele = request.getParameter("txhNomPostSele");
		String dscEtapa = request.getParameter("txhDscEtapa");
		command.setCodUsuario(request.getParameter("txhCodUsuario"));
		
		System.out.println("codProceso:" + codProceso);
		System.out.println("codPostSele:" + codPostSele);
		System.out.println("nomPostSele:" + nomPostSele);
		System.out.println("dscEtapa:" + dscEtapa);
		
		if( codProceso != null && codPostSele!= null && nomPostSele != null ) 
			if( !codProceso.trim().equals("") ) 
			{
				command.setCodPostSel(codPostSele);
				command.setNomPostSel(nomPostSele);
				command.setCodProceso(codProceso);
				command.setDscEtapa(dscEtapa);
				cargaData(command);
			}
		request.getSession().setAttribute("listaEvaluaciones", command.getListEvaluaciones());		
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {

		EnviarPostulanteCommand control = (EnviarPostulanteCommand) command;
		if ( control.getOperacion().trim().equals("GRABAR"))
		{
			if ( grabar(control) ) control.setMsg("OK");
			else control.setMsg("ERROR");
		}
	    return new ModelAndView("/reclutamiento/procesos/rec_enviopostulante_rev","control",control);		
    }
    
    private boolean grabar(EnviarPostulanteCommand control)
    {
    	boolean flag = true;
    	String respuesta = "";
    	String cadenaProceso = ""; 

    	/*Cadena de procesos, como en este envio solo se hace por un proceso
    	  se armara una cadena de "n" codigos de procesos.*/
    	
    	System.out.println("*** control.getCodEvalSel():"+control.getCodEvalSel());
    	
    	System.out.println("control.getComentario():" + control.getComentario());
    	
    	StringTokenizer stEvaluaciones = new StringTokenizer(control.getCodEvalSel(),"|");
    	StringTokenizer stCalificaciones = new StringTokenizer(control.getCodCalificaciones(),"|");
    	StringTokenizer stPostulantes = new StringTokenizer(control.getCodPostSel(),"|");
    	StringTokenizer stComentarios = new StringTokenizer(control.getComentario(),"|");
    	
    	System.out.println("stEvaluaciones:" + control.getCodEvalSel());
    	System.out.println("stCalificaciones:" + control.getCodCalificaciones());
    	System.out.println("stPostulantes:" + control.getCodPostSel());
    	System.out.println("stComentarios:" + control.getComentario());
    	
    	System.out.println("grabar:1");
    	for( int i = 0; i < stPostulantes.countTokens(); i++)
    		cadenaProceso = cadenaProceso +  control.getCodProceso() + "|";
    	
    	/*String valor1 = cadenaProceso;
    	String valor2 = stEvaluaciones.nextToken();
    	String valor3 = stComentarios.nextToken();
    	String valor4 = stCalificaciones.nextToken();*/
    	
    	/*System.out.println("cadenaProceso:"+valor1);
    	System.out.println("stEvaluaciones:"+valor2);
    	System.out.println("stComentarios:"+valor3);
    	System.out.println("stCalificaciones:"+valor4);*/
    	//System.out.println("cadenaProceso:"+ cadenaProceso);
    	System.out.println("grabar:2");
    	while ( stEvaluaciones.hasMoreTokens() )
    	{
    		 /*System.out.println("grabar:3");
    		 System.out.println("control.getCodPostSel():"+control.getCodPostSel());
    		 System.out.println("cadenaProceso:"+ cadenaProceso);
    		 System.out.println("CommonConstants.PROC_ETAPA_REVISION:"+CommonConstants.PROC_ETAPA_REVISION);
    		 System.out.println("stEvaluaciones.nextToken():" + stEvaluaciones.nextToken());
    		 System.out.println("CommonConstants.EST_REV_REVISADO_NO_ENVIADO:"+CommonConstants.EST_REV_REVISADO_NO_ENVIADO);
    		 System.out.println("stComentarios.nextToken():" + stComentarios.nextToken());
    		 System.out.println("stCalificaciones.nextToken():" + stCalificaciones.nextToken());
    		 System.out.println("CommonConstants.EVAL_ACT_ESTADO:" + CommonConstants.EVAL_ACT_ESTADO);
    		 System.out.println("control.getCodUsuario():"+control.getCodUsuario());*/
    		 
    		respuesta = this.evaluacionProcesosManager.insertEvaluacionPostulante(
    						control.getCodPostSel()
    						, cadenaProceso
    						, CommonConstants.PROC_ETAPA_REVISION
    						, stEvaluaciones.nextToken()
    						, CommonConstants.EST_REV_REVISADO_NO_ENVIADO // CommonConstants.EST_REV_REV_X_RRHH
    						, stComentarios.nextToken()
    						, stCalificaciones.nextToken()
    						, "", ""
    						, CommonConstants.EVAL_ACT_ESTADO
    						, control.getCodUsuario());
    		 //respuesta="0";
    		 System.out.println("grabar:4:" + respuesta);
    		if ( respuesta == null ) flag = false;
    		else if ( respuesta.trim().equals("-1") ) flag = false;
    		
    	}
    	return flag;
    }
    private void cargaData(EnviarPostulanteCommand control)
    {
    	/*Cargamos combo de postulantes.*/
    	StringTokenizer stCodPostulantes = new StringTokenizer(control.getCodPostSel(),"|");
    	StringTokenizer stNomPostulantes = new StringTokenizer(control.getNomPostSel(),"|");
    	Postulante postulante;
    	ArrayList<Postulante> postulantes = new ArrayList<Postulante>();    	
    	while ( stCodPostulantes.hasMoreTokens())
    	{
    		postulante = new Postulante();
    		postulante.setCodPostulante(stCodPostulantes.nextToken()); 
    		postulante.setNombres(stNomPostulantes.nextToken());    		
    		postulantes.add(postulante);
    	}
    	control.setListPostulantes(postulantes);
    	
    	/*Lista de tipos de evaluaciones*/
    	ArrayList<TipoTablaDetalle> listTipoEvaluaciones = (ArrayList)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_EVA
    			, "", "", CommonConstants.PROC_ETAPA_REVISION, "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	control.setNumEvaluaciones(Integer.toString(listTipoEvaluaciones.size()));
    	
    	System.out.println("listTipoEvaluaciones.size():"+listTipoEvaluaciones.size());
    	
    	/*Cargamos el Bean con el cual llenaremos la grilla de tipos de evaluacion*/
    	/*Reutilizaremos el Bean ProcesoEvaluadorBean, pues contiene los campos necesarios*/
    	ProcesoEvaluadorBean procesoEvaluadorBean;
    	control.setListEvaluaciones(new ArrayList<ProcesoEvaluadorBean>());
    	for (int i = 0; i < listTipoEvaluaciones.size() ; i++)    		
    	{    		
    		procesoEvaluadorBean = new ProcesoEvaluadorBean();
    		procesoEvaluadorBean.setCodPadre(listTipoEvaluaciones.get(i).getCodTipoTabla());
    		procesoEvaluadorBean.setCodDetalle(listTipoEvaluaciones.get(i).getCodTipoTablaDetalle());
    		procesoEvaluadorBean.setDescripcion(listTipoEvaluaciones.get(i).getDescripcion());
        	/*Lista de calificaciones*/
    		procesoEvaluadorBean.setEvaluadores(
    					this.calificacionManager.getAllCalificacion(CommonConstants.PROC_ETAPA_REVISION
    							, listTipoEvaluaciones.get(i).getCodTipoTablaDetalle().trim()));
    		control.getListEvaluaciones().add(procesoEvaluadorBean);
    	}
    }

}
