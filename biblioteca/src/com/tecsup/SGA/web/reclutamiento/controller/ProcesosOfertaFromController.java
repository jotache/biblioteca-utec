package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.OfertaCommand;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Oferta;
import com.tecsup.SGA.modelo.Proceso;

public class ProcesosOfertaFromController extends SimpleFormController {
	private ProcesoManager procesoManager;	
	
	private static Log log = LogFactory.getLog(ProcesosOfertaFromController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	OfertaCommand command = new OfertaCommand();

    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	String codProceso = (String)request.getParameter("txhCodProceso");
    	String indEdicicion = (String)request.getParameter("txhIndEdicicion");
    	
    	if ( codProceso != null) if ( !codProceso.trim().equals("") ) cargarOferta(command, codProceso);
    	command.setIndEdicicion("");
    	if ( indEdicicion != null ) command.setIndEdicicion(indEdicicion);
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String respuesta;
    	OfertaCommand control = (OfertaCommand) command;
    	
    	if ( control.getOperacion().trim().equals("GRABAR"))
    	{
    		respuesta = grabarOferta(control);
    		if ( respuesta != null)
    			if ( !respuesta.trim().equals("") && !respuesta.trim().equals("-1") )
    			{
    				cargarOferta(control, control.getCodProceso());
    				control.setMessage(CommonMessage.GRABAR_EXITO);
    				control.setOperacion("OK");
    			}
    			else control.setMessage(CommonMessage.GRABAR_ERROR);
    		else control.setMessage(CommonMessage.GRABAR_ERROR);
    	}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/procesos/rec_procesosoferta","control",control);		
    }
	
    private void cargarOferta(OfertaCommand command, String codProceso)
    {
    	Proceso proceso = this.procesoManager.getProceso(codProceso);
    	Oferta oferta = null;
    	command.setCodProceso(codProceso);
    	if ( proceso != null )
    	{
    		command.setDscProceso(proceso.getDscProceso());
    		command.setDscUnidadFuncional(proceso.getDscUnidadFuncional());
    		
    		oferta = this.procesoManager.getOferta(proceso.getCodOfertaAsociada());
    		if ( oferta != null )
    		{
    			command.setCodOferta(oferta.getCodOferta());
    			command.setDscOferta(oferta.getDenominacion());
    			command.setTextoPublicar(oferta.getDescripcion());
    			command.setFecIniPublicacion(oferta.getFecIniPub());
    			command.setNroVacantes(oferta.getVacantes());
    			command.setFecFinPublicacion(oferta.getFecFinPub());
    		}    		
    	}
    }
    private String grabarOferta(OfertaCommand command)
    {
    	try
    	{
	    	String respuesta = null;
	    	Oferta oferta = new Oferta();
	    	
	    	oferta.setCodOferta(command.getCodOferta());
	    	oferta.setCodProceso(command.getCodProceso());
	    	oferta.setDenominacion(command.getDscOferta());
	    	oferta.setDescripcion(command.getTextoPublicar());
	    	oferta.setVacantes(command.getNroVacantes());
	    	oferta.setFecFinPub(command.getFecFinPublicacion());
	    	oferta.setFecIniPub(command.getFecIniPublicacion());
	    	
	    	respuesta = this.procesoManager.grabarOferta(oferta,command.getCodUsuario());
	    	
	    	return respuesta;
    	}
    	catch (Exception ex){ex.printStackTrace();}
    	return null;
    	
    }
	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}
}
