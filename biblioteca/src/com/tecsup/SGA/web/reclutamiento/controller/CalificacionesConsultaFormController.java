package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.modelo.TipoDetalle;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaManager;
import com.tecsup.SGA.common.*;


public class CalificacionesConsultaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CalificacionesConsultaFormController.class);
	private TablaManager tablaManager;
	private TablaDetalleManager tablaDetalleManager;
	List listaTexto= new ArrayList();
	
	public void setTablaManager(TablaManager tablaManager) {
		this.tablaManager = tablaManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager){
		this.tablaDetalleManager = tablaDetalleManager;		
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	CalificacionesConsultaCommand command = new CalificacionesConsultaCommand();
    	command.setCodEval((String)request.getParameter("txhCodEva"));
    	System.out.println(command.getCodEval());
    	inicializaCombos(command);
    	
    	command.setListTipos(this.tablaManager.getAllTabla("M","0001")); 
    	listaTexto=command.getListTipos();
    	request.getSession().setAttribute("listTipos", command.getListTipos());
    	request.getSession().setAttribute("listaCalificaciones", null);    	
  
    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado = "";
    	CalificacionesConsultaCommand control = (CalificacionesConsultaCommand) command;
       	request.getSession().setAttribute("listaCalificaciones", null);
       	System.out.println("hola");
       	if(control.getOperacion().trim().equals("DETALLE")){
       		System.out.println("cod: "+control.getCodSelTipo());
       		buscar(control);
      		control.setListCalificaciones(this.tablaDetalleManager.getAllTablaDetalle(control.getCodSelTipo()
      				, "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));    	
          	request.getSession().setAttribute("listaCalificaciones", control.getListCalificaciones());
          	System.out.println("tot: "+control.getListCalificaciones().size());
    	}
    
    	else if(control.getOperacion().trim().equals("ELIMINAR")){
    		resultado = DeleteTablaDetalle(control);
    		if ( resultado.equals("-1"))control.setMsg("ERROR");
    		else if( resultado.equals("-2"))control.setMsg("NO");
    		else control.setMsg("OK");
    		
    	}
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/mantenimiento/rec_mto_cfg_calificaciones_consulta","control",control);		
    }
    
    
    private void inicializaCombos(CalificacionesConsultaCommand command)
	{
    	command.setListTipos(this.tablaManager.getAllTabla("M","0001"));
    }
    
    private String DeleteTablaDetalle(CalificacionesConsultaCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    
    		obj.setCodTipoTabla(control.getCodSelTipo());
    		obj.setCadCod(control.getCodDetalle());   		
    		obj.setNroReg("1");

    		if (!obj.getCadCod().trim().equals("") )
    		{
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.DeleteTablaDetalle(obj, control.getCodEval()));	
    			return obj.getCodTipoTablaDetalle();
    		}
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    public void buscar(CalificacionesConsultaCommand control){
    	TipoDetalle obj= new TipoDetalle();
    	for(int i=0;i<listaTexto.size();i++)
    	{obj=(TipoDetalle)listaTexto.get(i);
    	  if(control.getCodSelTipo().equals(obj.getCodTipoTipo()))
    	  {  control.setTexto(obj.getDenominacionTipo());
    	     i=listaTexto.size();
    		  
    	  }
    		
    	}
    	System.out.println("Texto encontrado: "+control.getTexto());
    }
}
