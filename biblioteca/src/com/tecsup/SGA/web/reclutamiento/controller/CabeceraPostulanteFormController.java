package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.service.reclutamiento.*;
import com.tecsup.SGA.common.CommonMessage;

public class CabeceraPostulanteFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(CabeceraPostulanteFormController.class);
	
	private ReclutaManager reclutaManager;
	
	/*SETTER*/
	public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	LogueoCommand command = new LogueoCommand();
    	
    	LogueoCommand logueoCommand = (LogueoCommand)request.getSession().getAttribute("postUsuario");
    	    	    	    	
    	LogueoCommand logueoCommand2 = (LogueoCommand)request.getSession().getAttribute("postUsuario2");
    	//log.info("Valor Jody:" + logueoCommand.getIdRec());
    	
    	if ( logueoCommand2 != null )
    	{
    		log.info("Cabecera IdRec:" + logueoCommand2.getIdRec());
	    	command.setIdRec(logueoCommand2.getIdRec());
	    	command.setOperacion(logueoCommand2.getOperacion());
	    	//log.error("command.getUsuario: " + command.getUsuario());
	    	command.setCodUsuario(logueoCommand2.getCodUsuario());
	    	//log.error("command.getUsuario: " + command.getUsuario());
    	}    	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	LogueoCommand control = (LogueoCommand) command;		
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/postulante/cabeceraReclutamientoPost","control",control);		
    }
}
