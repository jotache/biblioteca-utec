package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.*;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.exception.FormatoInvalidoException;

public class ProcesosBandejaFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(ProcesosBandejaFormController.class);
	private ProcesoManager procesoManager;
	private TablaDetalleManager tablaDetalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException,FormatoInvalidoException, ParseException {
    	log.info("formBackingObject:INI");   	
    	ProcesosBandejaCommand command = new ProcesosBandejaCommand();
    	String etapa = CommonConstants.PROC_ETAPA_REVISION;
    	
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? 
    							"" : (String)request.getParameter("txhCodUsuario") );
    	command.setCodPeriodo(request.getParameter("txhCodPeriodo") == null ? 
								"" : (String)request.getParameter("txhCodPeriodo") );
    	/*SETEAMOS FECHAS POR DEFECTO*/
    	//String fecha = Fecha.getFechaActual();
    	//String[] arrFecha = fecha.split("/");
    	
    	//command.setFecIni("01/" + arrFecha[1] + "/" + arrFecha[2]);
    	//command.setFecFin(Fecha.getDiasPorMes(arrFecha[1], arrFecha[2]) + "/" + arrFecha[1] + "/" + arrFecha[2]);
    	    	
    	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
		String fecha1 =  sdf.format(Fecha.getSumaFechaDias("-60", Fecha.getCurrentDate())).toString();
    	String fecha2 = sdf.format(Fecha.getCurrentDate()).toString();			
    	command.setFecIni(fecha1);
    	//command.setFecFin(arrFecha[01]+ "/" + arrFecha[1] + "/" + arrFecha[2]);
    	command.setFecFin(fecha2);
    	
    	command.setListaUnidadFuncional(this.tablaDetalleManager.getAllUnidadFuncional());
    	
    	String opcion=request.getParameter("txhOpcion") == null ?"" : (String)request.getParameter("txhOpcion");
    	//System.out.println(">>"+opcion+"<<");
    	if(opcion.equals("0")){    		
    		command.setFlagEtapa(request.getParameter("txhTipoEtapa"));
    		command.setUnidadFuncional(request.getParameter("txhCodUniFun"));
    		command.setFecIni(request.getParameter("txhFecIni"));
    		command.setFecFin(request.getParameter("txhFecFin"));
    		
    		if ( command.getFlagEtapa().trim().equals("2") )
    			etapa = CommonConstants.PROC_ETAPA_SELECCION;
    	}
    	//Seleccion = txhTipoEtapa = 2
    	command.setListaProcesos(this.procesoManager.getAllProcesos(etapa, ""
    			,command.getFecIni(),command.getFecFin()));    	
    	request.getSession().setAttribute("listaProcesos", command.getListaProcesos());
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
		
    	ProcesosBandejaCommand control = (ProcesosBandejaCommand) command;
    	String retorno = "";
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{
    		buscar(control);
    	}
    	else if (control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		retorno = eliminarProceso(control.getCodProcesosSeleccionados(), control.getCodUsuario());
    		if ( retorno != null )
    		{
    			if ( retorno.trim().equals("0"))
    				control.setMsg("ELI");
    			else 
    			{
    				if ( retorno.trim().equals("-2"))
    					control.setMsg("EN_PROC");
        			else
        				control.setMsg("NOELI");
    			}

    			buscar(control);
    		}
    	}
    	
    	control.setOperacion("");
		request.getSession().setAttribute("listaProcesos", control.getListaProcesos());
		
		//log.info("onSubmit:FIN");		
	    return new ModelAndView("/reclutamiento/procesos/rec_procesosbandeja","control",control);		
    }
    public void buscar(ProcesosBandejaCommand control)
    {
		control.setCodProcesosSeleccionados("");
    	if (control.getFlagEtapa().trim().equals("1") || 
    		control.getFlagEtapa().trim().equals("") )
    		control.setListaProcesos(this.procesoManager.getAllProcesos(CommonConstants.PROC_ETAPA_REVISION
    							, control.getUnidadFuncional(), control.getFecIni() , control.getFecFin()));
    	else
    		control.setListaProcesos(this.procesoManager.getAllProcesos(CommonConstants.PROC_ETAPA_SELECCION
					, control.getUnidadFuncional(), control.getFecIni() , control.getFecFin()));
    }
    public String eliminarProceso(String codProceso, String codUsuario)
    {
    	String repuesta;
    	repuesta = this.procesoManager.deleteProceso(codProceso.replace("|", " ")
    			, codUsuario);
    	return repuesta;
    }
	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
}
