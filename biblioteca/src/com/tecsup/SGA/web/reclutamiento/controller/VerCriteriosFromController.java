package com.tecsup.SGA.web.reclutamiento.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.reclutamiento.command.ProcesoCommand;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.modelo.*;
import com.tecsup.SGA.common.*;

//nuvea factura correo solicitando.
public class VerCriteriosFromController extends SimpleFormController {

	private static Log log = LogFactory.getLog(VerCriteriosFromController.class);
	private ProcesoManager procesoManager;
	private TablaDetalleManager tablaDetalleManager;

	public void setProcesoManager(ProcesoManager procesoManager) {
		this.procesoManager = procesoManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    	throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ProcesoCommand command = new ProcesoCommand();
    	/*Llenando los combos.*/
    	inicializaCombos(command);
    	/*Veo si es un editar*/
    	String codProceso = (String)request.getParameter("txhCodProceso");
    	if ( codProceso != null ) 
    		if ( codProceso.trim() != "" ) 
    			cargaProceso(codProceso, command);
    	
        log.info("formBackingObject:FIN");
    	//request.getSession().setAttribute("listaAreasInteres",command.getListAreasInteres());        
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	log.info("onSubmit:INI");
    	ProcesoCommand control = (ProcesoCommand) command;
    	request.getSession().setAttribute("listaAreasInteres",control.getListAreasInteres());
		log.info("onSubmit:FIN");
	    return new ModelAndView("/reclutamiento/procesos/rec_procesovercriterios","control",control);		
    }

	private void inicializaCombos(ProcesoCommand command)
	{
    	command.setListUnidadFuncional(tablaDetalleManager.getAllUnidadFuncional());
    	command.setListSexo(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_SEXO    			
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	//command.setListAreaEstudio(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PROFESION , "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
    	//command.setListGradoAcedemico(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRADO_ACADEMICO  , "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	//command.setListInstitucionAcademica(tablaDetalleManager.getAllInstitucion(CommonConstants.GSTR_INSTI_EDUCATIVA));
    	command.setListTipoEdad(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_SIMBOLO
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListDedicacion(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_DEDICACION
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListInteresado(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_INTERES_EN
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListaTipoMoneda(tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_MONEDA
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
	}
	
	private void cargaProceso(String codProceso, ProcesoCommand command)
	{
		Proceso proceso = this.procesoManager.getProceso(codProceso);
		String strFlagIndViaje = "";
		String strFlagPuestoPostula = "";
		StringTokenizer st;
		
		if ( proceso != null )
		{   System.out.println("CodProceso: "+codProceso);
			command.setCodProceso(codProceso);
			command.setDscProceso(proceso.getDscProceso());
			command.setCodUnidadFuncional(proceso.getCodUnidadFuncional());
			command.setCodSexo(proceso.getCodSexo());
			command.setCodAreaEstudio(proceso.getCodAreaEstudio());
			command.setCodGradoAcedemico(proceso.getCodGradoAcedemico());
			command.setCodInstitucionAcademica(proceso.getCodInstitucionAcademica());
			command.setSalarioIni(proceso.getSalarioIni());
			command.setSalarioFin(proceso.getSalarioFin());
			command.setCodTipoEdad(proceso.getCodTipoEdad());
			command.setEdad(proceso.getEdad());
			command.setCodTipoEdad1(proceso.getCodTipoEdad1());
			command.setEdad1(proceso.getEdad1());
			command.setCodDedicacion(proceso.getCodDedicacion());
			command.setAnhosExperiencia(proceso.getAnhosExperiencia());
			command.setAnhosExperienciaDocencia(proceso.getAnhosExperienciaDocencia());
			command.setCodInteresado(proceso.getCodInteresado());
			command.setCodTipoMoneda(proceso.getCodTipoMoneda());
			//JCMU 13/11/2008 - Falta incluir PostulaPuestoDoc y PostulaPuestoAdm 
			//command.setPuestoPostulaAdm(proceso.getIndPostulaAdm());
			//command.setPuestoPostulaDoc(proceso.getIndPostulaDoc());
			System.out.println("CodTipoMoneda: "+command.getCodTipoMoneda());
			strFlagIndViaje = proceso.getFlagDisponibilidadViajar();
			
			st = new StringTokenizer(strFlagIndViaje,"|");
			if ( st.hasMoreTokens() )
			{
				command.setFlagDisponibilidadViajar(st.nextToken());
				command.setFlagDisponibilidadViajar1(st.nextToken());
			}
			
			//command.setFlagDisponibilidadViajar(proceso.getFlagDisponibilidadViajar());
			strFlagPuestoPostula = proceso.getPuestoPostula();
			st = new StringTokenizer(strFlagPuestoPostula,"|");
			if ( st.hasMoreTokens() ){
				command.setPuestoPostulaDoc(st.nextToken());
				command.setPuestoPostulaAdm(st.nextToken());
			}
			
			cargaListaAreasSeleccionadas(command, proceso);
			cargaListaAreasEstudiosSeleccionadas(command, proceso);
			cargaListaAreasNIvelEstudiosSeleccionadas(command, proceso);
			cargaListaInstEduSeleccionadas(command, proceso);
		}
	}
	
	private void cargaListaAreasSeleccionadas(ProcesoCommand command, Proceso proceso)
	{
		int i;
		ArrayList listAreas = null;
		command.setCodAreasIntSeleccionadas("");
		TipoTablaDetalle tipoTablaDetalle = null;
		if ( proceso != null )
		{
			listAreas = (ArrayList) proceso.getListAreasInteres();
			command.setListAreasInteres(listAreas);
			if ( listAreas != null )
				for( i = 0; i < listAreas.size() ; i++)
				{
					tipoTablaDetalle = (TipoTablaDetalle)listAreas.get(i);
					command.setCodAreasIntSeleccionadas(command.getCodAreasIntSeleccionadas() 
							+  tipoTablaDetalle.getCodTipoTablaDetalle()
							+ "|");
				}
		}
	}

	
	private void cargaListaAreasEstudiosSeleccionadas(ProcesoCommand command, Proceso proceso){
		int i;
		ArrayList listaAreasEstudios = null;
		command.setCodAreasEstSeleccionadas("");
		TipoTablaDetalle procesoAreEst = null;
		if (proceso!=null){
			listaAreasEstudios = (ArrayList) proceso.getListAreasEstudios();
			command.setListAreaEstudio(listaAreasEstudios);
			if (listaAreasEstudios!=null){
				for(i=0;i<listaAreasEstudios.size();i++){
					procesoAreEst = (TipoTablaDetalle) listaAreasEstudios.get(i);
					command.setCodAreasEstSeleccionadas(command.getCodAreasEstSeleccionadas()
							+ procesoAreEst.getCodTipoTablaDetalle()
							+ "|");
				}
			}
		}
	}
	
	private void cargaListaAreasNIvelEstudiosSeleccionadas(ProcesoCommand command, Proceso proceso){
		int i;
		ArrayList listaAreasEstudios = null;
		command.setCodAreasNivEstSeleccionadas("");		
		TipoTablaDetalle procesoAreEst = null;
		if (proceso!=null){
			listaAreasEstudios = (ArrayList) proceso.getListAreasNivelEstudios();
			command.setListAreasNivelEstudios(listaAreasEstudios);
			if (listaAreasEstudios!=null){
				for(i=0;i<listaAreasEstudios.size();i++){
					procesoAreEst = (TipoTablaDetalle) listaAreasEstudios.get(i);
					command.setCodAreasNivEstSeleccionadas(command.getCodAreasNivEstSeleccionadas()
							+ procesoAreEst.getCodTipoTablaDetalle()
							+ "|");
					
				}
			}
		}
	}
	
	private void cargaListaInstEduSeleccionadas(ProcesoCommand command, Proceso proceso){
		int i;
		ArrayList listaAreasEstudios = null;
		command.setCodInstEduSeleccionadas("");		
		TipoTablaDetalle procesoAreEst = null;
		if (proceso!=null){
			listaAreasEstudios = (ArrayList) proceso.getListlistInstitucionesEdu();
			command.setListInstEdu(listaAreasEstudios);
			if (listaAreasEstudios!=null){
				for(i=0;i<listaAreasEstudios.size();i++){
					procesoAreEst = (TipoTablaDetalle) listaAreasEstudios.get(i);
					command.setCodInstEduSeleccionadas(command.getCodInstEduSeleccionadas()
							+ procesoAreEst.getCodTipoTablaDetalle()
							+ "|");
					
				}
			}
		}
	}
	
}
