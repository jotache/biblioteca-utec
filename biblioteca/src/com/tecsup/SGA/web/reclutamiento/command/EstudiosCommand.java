package com.tecsup.SGA.web.reclutamiento.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class EstudiosCommand {
	
	private String operacion;
	private String typeMessage;
	private String message;
	private String idRec;
	private String indMenu; 
	
	//Estudios Secundarios
	private String colegio1;
	private String colegio2;
	private String anioInicio1;
	private String anioInicio2;
	private String anioFin1;
	private String anioFin2;
	
	//Idiomas
	private String cadenaIdioma;
	private String nroRegIdioma;
	
	private String codEstudioIdioma1;
	private String codEstudioIdioma2;
	private String codEstudioIdioma3;
	
	private String idioma1;
	private List listaIdioma1;
	private String idioma2;
	private List listaIdioma2;
	private String idioma3;
	private List listaIdioma3;
	
	private String nivel1; //conocimiento
	private List listaNivel1;
	private String nivel2;
	private List listaNivel2;
	private String nivel3;
	private List listaNivel3;
	
	private String grado1; //comprensión
	private List listaGrado1;
	private String grado2;
	private List listaGrado2;
	private String grado3;
	private List listaGrado3;
	
	//Estudios Superiores
	private String cadenaEstSup;
	private String nroRegEstSup;
	
	private String codEstudioSup1;
	private String codEstudioSup2;
	private String codEstudioSup3;
		
	private String gradoAcad1;
	private List listaGradoAcad1;
	private String gradoAcad2;
	private List listaGradoAcad2;
	private String gradoAcad3;
	private List listaGradoAcad3;
	
	private String areaEstudio1;
	private List listaAreaEstudio1;
	private String areaEstudio2;
	private List listaAreaEstudio2;
	private String areaEstudio3;
	private List listaAreaEstudio3;
	
	private String institucion1;
	private List listaInstitucion1;
	private String institucion2;
	private List listaInstitucion2;
	private String institucion3;
	private List listaInstitucion3;

	private String otraInstitucion1;
	private String otraInstitucion2;
	private String otraInstitucion3;
	
	private String ciclo1;
	private String ciclo2;
	private String ciclo3;
	
	private String merito1;
	private List listaMerito1;
	private String merito2;
	private List listaMerito2;
	private String merito3;
	private List listaMerito3;

	private String inicio1; //(mm/aaaa)
	private String inicio2;
	private String inicio3;
	
	private String termino1; //(mm/aaaa)
	private String termino2;
	private String termino3;
	
	private String codUsuario;
	
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getColegio1() {
		return colegio1;
	}
	public void setColegio1(String colegio1) {
		this.colegio1 = colegio1;
	}
	public String getColegio2() {
		return colegio2;
	}
	public void setColegio2(String colegio2) {
		this.colegio2 = colegio2;
	}
	public String getAnioInicio1() {
		return anioInicio1;
	}
	public void setAnioInicio1(String anioInicio1) {
		this.anioInicio1 = anioInicio1;
	}
	public String getAnioInicio2() {
		return anioInicio2;
	}
	public void setAnioInicio2(String anioInicio2) {
		this.anioInicio2 = anioInicio2;
	}
	public String getAnioFin1() {
		return anioFin1;
	}
	public void setAnioFin1(String anioFin1) {
		this.anioFin1 = anioFin1;
	}
	public String getAnioFin2() {
		return anioFin2;
	}
	public void setAnioFin2(String anioFin2) {
		this.anioFin2 = anioFin2;
	}
	public String getIdioma1() {
		return idioma1;
	}
	public void setIdioma1(String idioma1) {
		this.idioma1 = idioma1;
	}
	public String getIdioma2() {
		return idioma2;
	}
	public void setIdioma2(String idioma2) {
		this.idioma2 = idioma2;
	}
	public String getIdioma3() {
		return idioma3;
	}
	public void setIdioma3(String idioma3) {
		this.idioma3 = idioma3;
	}
	public String getNivel1() {
		return nivel1;
	}
	public void setNivel1(String nivel1) {
		this.nivel1 = nivel1;
	}
	public String getNivel2() {
		return nivel2;
	}
	public void setNivel2(String nivel2) {
		this.nivel2 = nivel2;
	}
	public String getNivel3() {
		return nivel3;
	}
	public void setNivel3(String nivel3) {
		this.nivel3 = nivel3;
	}
	public String getGrado1() {
		return grado1;
	}
	public void setGrado1(String grado1) {
		this.grado1 = grado1;
	}
	public String getGrado2() {
		return grado2;
	}
	public void setGrado2(String grado2) {
		this.grado2 = grado2;
	}
	public String getGrado3() {
		return grado3;
	}
	public void setGrado3(String grado3) {
		this.grado3 = grado3;
	}
	public String getAreaEstudio1() {
		return areaEstudio1;
	}
	public void setAreaEstudio1(String areaEstudio1) {
		this.areaEstudio1 = areaEstudio1;
	}
	public String getAreaEstudio2() {
		return areaEstudio2;
	}
	public void setAreaEstudio2(String areaEstudio2) {
		this.areaEstudio2 = areaEstudio2;
	}
	public String getAreaEstudio3() {
		return areaEstudio3;
	}
	public void setAreaEstudio3(String areaEstudio3) {
		this.areaEstudio3 = areaEstudio3;
	}
	public String getInstitucion1() {
		return institucion1;
	}
	public void setInstitucion1(String institucion1) {
		this.institucion1 = institucion1;
	}
	public String getInstitucion2() {
		return institucion2;
	}
	public void setInstitucion2(String institucion2) {
		this.institucion2 = institucion2;
	}
	public String getInstitucion3() {
		return institucion3;
	}
	public void setInstitucion3(String institucion3) {
		this.institucion3 = institucion3;
	}
	public String getOtraInstitucion1() {
		return otraInstitucion1;
	}
	public void setOtraInstitucion1(String otraInstitucion1) {
		this.otraInstitucion1 = otraInstitucion1;
	}
	public String getOtraInstitucion2() {
		return otraInstitucion2;
	}
	public void setOtraInstitucion2(String otraInstitucion2) {
		this.otraInstitucion2 = otraInstitucion2;
	}
	public String getOtraInstitucion3() {
		return otraInstitucion3;
	}
	public void setOtraInstitucion3(String otraInstitucion3) {
		this.otraInstitucion3 = otraInstitucion3;
	}
	public String getCiclo1() {
		return ciclo1;
	}
	public void setCiclo1(String ciclo1) {
		this.ciclo1 = ciclo1;
	}
	public String getCiclo2() {
		return ciclo2;
	}
	public void setCiclo2(String ciclo2) {
		this.ciclo2 = ciclo2;
	}
	public String getCiclo3() {
		return ciclo3;
	}
	public void setCiclo3(String ciclo3) {
		this.ciclo3 = ciclo3;
	}
	public String getInicio1() {
		return inicio1;
	}
	public void setInicio1(String inicio1) {
		this.inicio1 = inicio1;
	}
	public String getInicio2() {
		return inicio2;
	}
	public void setInicio2(String inicio2) {
		this.inicio2 = inicio2;
	}
	public String getInicio3() {
		return inicio3;
	}
	public void setInicio3(String inicio3) {
		this.inicio3 = inicio3;
	}
	public String getTermino1() {
		return termino1;
	}
	public void setTermino1(String termino1) {
		this.termino1 = termino1;
	}
	public String getTermino2() {
		return termino2;
	}
	public void setTermino2(String termino2) {
		this.termino2 = termino2;
	}
	public String getTermino3() {
		return termino3;
	}
	public void setTermino3(String termino3) {
		this.termino3 = termino3;
	}
	public List getListaIdioma1() {
		return listaIdioma1;
	}
	public void setListaIdioma1(List listaIdioma1) {
		this.listaIdioma1 = listaIdioma1;
	}
	public List getListaIdioma2() {
		return listaIdioma2;
	}
	public void setListaIdioma2(List listaIdioma2) {
		this.listaIdioma2 = listaIdioma2;
	}
	public List getListaIdioma3() {
		return listaIdioma3;
	}
	public void setListaIdioma3(List listaIdioma3) {
		this.listaIdioma3 = listaIdioma3;
	}
	public List getListaNivel1() {
		return listaNivel1;
	}
	public void setListaNivel1(List listaNivel1) {
		this.listaNivel1 = listaNivel1;
	}
	public List getListaNivel2() {
		return listaNivel2;
	}
	public void setListaNivel2(List listaNivel2) {
		this.listaNivel2 = listaNivel2;
	}
	public List getListaNivel3() {
		return listaNivel3;
	}
	public void setListaNivel3(List listaNivel3) {
		this.listaNivel3 = listaNivel3;
	}
	public List getListaGrado1() {
		return listaGrado1;
	}
	public void setListaGrado1(List listaGrado1) {
		this.listaGrado1 = listaGrado1;
	}
	public List getListaGrado2() {
		return listaGrado2;
	}
	public void setListaGrado2(List listaGrado2) {
		this.listaGrado2 = listaGrado2;
	}
	public List getListaGrado3() {
		return listaGrado3;
	}
	public void setListaGrado3(List listaGrado3) {
		this.listaGrado3 = listaGrado3;
	}
	public String getGradoAcad1() {
		return gradoAcad1;
	}
	public void setGradoAcad1(String gradoAcad1) {
		this.gradoAcad1 = gradoAcad1;
	}
	public List getListaGradoAcad1() {
		return listaGradoAcad1;
	}
	public void setListaGradoAcad1(List listaGradoAcad1) {
		this.listaGradoAcad1 = listaGradoAcad1;
	}
	public String getGradoAcad2() {
		return gradoAcad2;
	}
	public void setGradoAcad2(String gradoAcad2) {
		this.gradoAcad2 = gradoAcad2;
	}
	public List getListaGradoAcad2() {
		return listaGradoAcad2;
	}
	public void setListaGradoAcad2(List listaGradoAcad2) {
		this.listaGradoAcad2 = listaGradoAcad2;
	}
	public String getGradoAcad3() {
		return gradoAcad3;
	}
	public void setGradoAcad3(String gradoAcad3) {
		this.gradoAcad3 = gradoAcad3;
	}
	public List getListaGradoAcad3() {
		return listaGradoAcad3;
	}
	public void setListaGradoAcad3(List listaGradoAcad3) {
		this.listaGradoAcad3 = listaGradoAcad3;
	}
	public List getListaAreaEstudio1() {
		return listaAreaEstudio1;
	}
	public void setListaAreaEstudio1(List listaAreaEstudio1) {
		this.listaAreaEstudio1 = listaAreaEstudio1;
	}
	public List getListaAreaEstudio2() {
		return listaAreaEstudio2;
	}
	public void setListaAreaEstudio2(List listaAreaEstudio2) {
		this.listaAreaEstudio2 = listaAreaEstudio2;
	}
	public List getListaAreaEstudio3() {
		return listaAreaEstudio3;
	}
	public void setListaAreaEstudio3(List listaAreaEstudio3) {
		this.listaAreaEstudio3 = listaAreaEstudio3;
	}
	public List getListaInstitucion1() {
		return listaInstitucion1;
	}
	public void setListaInstitucion1(List listaInstitucion1) {
		this.listaInstitucion1 = listaInstitucion1;
	}
	public List getListaInstitucion2() {
		return listaInstitucion2;
	}
	public void setListaInstitucion2(List listaInstitucion2) {
		this.listaInstitucion2 = listaInstitucion2;
	}
	public List getListaInstitucion3() {
		return listaInstitucion3;
	}
	public void setListaInstitucion3(List listaInstitucion3) {
		this.listaInstitucion3 = listaInstitucion3;
	}
	public String getMerito1() {
		return merito1;
	}
	public void setMerito1(String merito1) {
		this.merito1 = merito1;
	}
	public List getListaMerito1() {
		return listaMerito1;
	}
	public void setListaMerito1(List listaMerito1) {
		this.listaMerito1 = listaMerito1;
	}
	public String getMerito2() {
		return merito2;
	}
	public void setMerito2(String merito2) {
		this.merito2 = merito2;
	}
	public List getListaMerito2() {
		return listaMerito2;
	}
	public void setListaMerito2(List listaMerito2) {
		this.listaMerito2 = listaMerito2;
	}
	public String getMerito3() {
		return merito3;
	}
	public void setMerito3(String merito3) {
		this.merito3 = merito3;
	}
	public List getListaMerito3() {
		return listaMerito3;
	}
	public void setListaMerito3(List listaMerito3) {
		this.listaMerito3 = listaMerito3;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCodEstudioIdioma1() {
		return codEstudioIdioma1;
	}
	public void setCodEstudioIdioma1(String codEstudioIdioma1) {
		this.codEstudioIdioma1 = codEstudioIdioma1;
	}
	public String getCodEstudioIdioma2() {
		return codEstudioIdioma2;
	}
	public void setCodEstudioIdioma2(String codEstudioIdioma2) {
		this.codEstudioIdioma2 = codEstudioIdioma2;
	}
	public String getCodEstudioIdioma3() {
		return codEstudioIdioma3;
	}
	public void setCodEstudioIdioma3(String codEstudioIdioma3) {
		this.codEstudioIdioma3 = codEstudioIdioma3;
	}
	public String getCadenaIdioma() {
		return cadenaIdioma;
	}
	public void setCadenaIdioma(String cadenaIdioma) {
		this.cadenaIdioma = cadenaIdioma;
	}
	public String getNroRegIdioma() {
		return nroRegIdioma;
	}
	public void setNroRegIdioma(String nroRegIdioma) {
		this.nroRegIdioma = nroRegIdioma;
	}
	public String getCadenaEstSup() {
		return cadenaEstSup;
	}
	public void setCadenaEstSup(String cadenaEstSup) {
		this.cadenaEstSup = cadenaEstSup;
	}
	public String getNroRegEstSup() {
		return nroRegEstSup;
	}
	public void setNroRegEstSup(String nroRegEstSup) {
		this.nroRegEstSup = nroRegEstSup;
	}
	public String getCodEstudioSup1() {
		return codEstudioSup1;
	}
	public void setCodEstudioSup1(String codEstudioSup1) {
		this.codEstudioSup1 = codEstudioSup1;
	}
	public String getCodEstudioSup2() {
		return codEstudioSup2;
	}
	public void setCodEstudioSup2(String codEstudioSup2) {
		this.codEstudioSup2 = codEstudioSup2;
	}
	public String getCodEstudioSup3() {
		return codEstudioSup3;
	}
	public void setCodEstudioSup3(String codEstudioSup3) {
		this.codEstudioSup3 = codEstudioSup3;
	}
	public String getIndMenu() {
		return indMenu;
	}
	public void setIndMenu(String indMenu) {
		this.indMenu = indMenu;
	}
	
}
