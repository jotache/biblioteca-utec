package com.tecsup.SGA.web.reclutamiento.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class MtoAreasInteresConsultaCommand {
	
	private String ListaNivel;
	private String operacion;
	private String codAreasNivelUno;
	private List listAreasNivelUno;
	private List listAreasNivelDos; 
	private String codAreasNivelDos;
	private String dscNivelDos;
	private String codDetalle;
	private String cadCod;
	private String nroReg;
	private String msg;
	private String msg2;
	private String descripcion;
	private String codEval;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCadCod() {
		return cadCod;
	}

	public void setCadCod(String cadCod) {
		this.cadCod = cadCod;
	}

	public String getNroReg() {
		return nroReg;
	}

	public void setNroReg(String nroReg) {
		this.nroReg = nroReg;
	}

	public String getCodDetalle() {
		return codDetalle;
	}

	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}

	public String getDscNivelDos() {
		return dscNivelDos;
	}

	public void setDscNivelDos(String dscNivelDos) {
		this.dscNivelDos = dscNivelDos;
	}

	public String getCodAreasNivelDos() {
		return codAreasNivelDos;
	}

	public void setCodAreasNivelDos(String codAreasNivelDos) {
		this.codAreasNivelDos = codAreasNivelDos;
	}

	public String getListaNivel() {
		return ListaNivel;
	}

	public void setListaNivel(String listaNivel) {
		ListaNivel = listaNivel;
	}

	public List getListAreasNivelUno() {
		return listAreasNivelUno;
	}

	public void setListAreasNivelUno(List listAreasNivelUno) {
		this.listAreasNivelUno = listAreasNivelUno;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodAreasNivelUno() {
		return codAreasNivelUno;
	}

	public void setCodAreasNivelUno(String codAreasNivelUno) {
		this.codAreasNivelUno = codAreasNivelUno;
	}

	public List getListAreasNivelDos() {
		return listAreasNivelDos;
	}

	public void setListAreasNivelDos(List listAreasNivelDos) {
		this.listAreasNivelDos = listAreasNivelDos;
	}

	public String getMsg2() {
		return msg2;
	}

	public void setMsg2(String msg2) {
		this.msg2 = msg2;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodEval() {
		return codEval;
	}

	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}

	
}
