package com.tecsup.SGA.web.reclutamiento.command;

import java.util.*;

import com.tecsup.SGA.modelo.Evaluador;
public class ProcesosEvaluadoresCommand {
	/*ATRIBUTOS*/
	private String codUsuario;
	private String operacion;
	private String message;
	
	private String codProceso;
	private String codEtapa;
	private String dscProceso;
	private String dscEtapa;
	
	private String nroEvaluaciones;
	private List listTiposEvaluacion;
	
	private String codEvaluaciones;
	private String codEvaluadores;
	
	
	/*GETTERS Y SETTERS*/	
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getDscProceso() {
		return dscProceso;
	}
	public void setDscProceso(String dscProceso) {
		this.dscProceso = dscProceso;
	}
	public String getDscEtapa() {
		return dscEtapa;
	}
	public void setDscEtapa(String dscEtapa) {
		this.dscEtapa = dscEtapa;
	}
	public List getListTiposEvaluacion() {
		return listTiposEvaluacion;
	}
	public void setListTiposEvaluacion(List listTiposEvaluacion) {
		this.listTiposEvaluacion = listTiposEvaluacion;
	}
	public String getCodEvaluaciones() {
		return codEvaluaciones;
	}
	public void setCodEvaluaciones(String codEvaluaciones) {
		this.codEvaluaciones = codEvaluaciones;
	}
	public String getCodEvaluadores() {
		return codEvaluadores;
	}
	public void setCodEvaluadores(String codEvaluadores) {
		this.codEvaluadores = codEvaluadores;
	}
	public String getNroEvaluaciones() {
		return nroEvaluaciones;
	}
	public void setNroEvaluaciones(String nroEvaluaciones) {
		this.nroEvaluaciones = nroEvaluaciones;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}	
}
