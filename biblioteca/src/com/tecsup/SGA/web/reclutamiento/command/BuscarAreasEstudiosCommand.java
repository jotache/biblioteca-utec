package com.tecsup.SGA.web.reclutamiento.command;

import java.util.List;

public class BuscarAreasEstudiosCommand {
	
	private String operacion;
	private String codAreaEstNiv2;
	private String dscAreaEstNiv2;

	/*private String codAreaEstNiv1;
	private List listAreaEstNiv1;
	private List listAreaEstNiv2;*/
	private List listaAreasEstudios;
	private List listaAreasNivelEstudios;
	private List listaInstitucionEducativa;
	
	private int longitud;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public int getLongitud() {
		return longitud;
	}
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	public List getListaAreasEstudios() {
		return listaAreasEstudios;
	}
	public void setListaAreasEstudios(List listaAreasEstudios) {
		this.listaAreasEstudios = listaAreasEstudios;
	}
	public String getCodAreaEstNiv2() {
		return codAreaEstNiv2;
	}
	public void setCodAreaEstNiv2(String codAreaEstNiv2) {
		this.codAreaEstNiv2 = codAreaEstNiv2;
	}
	public String getDscAreaEstNiv2() {
		return dscAreaEstNiv2;
	}
	public void setDscAreaEstNiv2(String dscAreaEstNiv2) {
		this.dscAreaEstNiv2 = dscAreaEstNiv2;
	}
	public List getListaAreasNivelEstudios() {
		return listaAreasNivelEstudios;
	}
	public void setListaAreasNivelEstudios(List listaAreasNivelEstudios) {
		this.listaAreasNivelEstudios = listaAreasNivelEstudios;
	}
	public List getListaInstitucionEducativa() {
		return listaInstitucionEducativa;
	}
	public void setListaInstitucionEducativa(List listaInstitucionEducativa) {
		this.listaInstitucionEducativa = listaInstitucionEducativa;
	}
		
}
