package com.tecsup.SGA.web.reclutamiento.command;

import java.util.List;

import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
public class EvaluacionesProcesoCommand {
	private String codUsuario;
	private String operacion;
	private String msg;
	private String codEtapa;
	private String indEtapa;
	private List listRevision;
	private List<EvaluacionesByEvaluadorBean> listPostRevisados;
	private List listSeleccion;
	
	private String codProcesos;
	private String codPostulantes;
	private String nomPostulantes;
	private String codEvaEnProcs;
	private String codTipoEvals;
	private String dscTipoEvals;
	private String codPostulanteProceso;
	private List listaProcesos;
	private String codProceso;
	
	public String getIndEtapa() {
		return indEtapa;
	}
	public void setIndEtapa(String indEtapa) {
		this.indEtapa = indEtapa;
	}
	public String getCodProcesos() {
		return codProcesos;
	}
	public void setCodProcesos(String codProcesos) {
		this.codProcesos = codProcesos;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public List getListRevision() {
		return listRevision;
	}
	public void setListRevision(List listRevision) {
		this.listRevision = listRevision;
	}
	public List getListSeleccion() {
		return listSeleccion;
	}
	public void setListSeleccion(List listSeleccion) {
		this.listSeleccion = listSeleccion;
	}

	public String getCodPostulantes() {
		return codPostulantes;
	}
	public void setCodPostulantes(String codPostulantes) {
		this.codPostulantes = codPostulantes;
	}
	public String getCodEvaEnProcs() {
		return codEvaEnProcs;
	}
	public void setCodEvaEnProcs(String codEvaEnProcs) {
		this.codEvaEnProcs = codEvaEnProcs;
	}
	public String getCodTipoEvals() {
		return codTipoEvals;
	}
	public void setCodTipoEvals(String codTipoEvals) {
		this.codTipoEvals = codTipoEvals;
	}
	public String getNomPostulantes() {
		return nomPostulantes;
	}
	public void setNomPostulantes(String nomPostulantes) {
		this.nomPostulantes = nomPostulantes;
	}
	public String getDscTipoEvals() {
		return dscTipoEvals;
	}
	public void setDscTipoEvals(String dscTipoEvals) {
		this.dscTipoEvals = dscTipoEvals;
	}
	public String getCodPostulanteProceso() {
		return codPostulanteProceso;
	}
	public void setCodPostulanteProceso(String codPostulanteProceso) {
		this.codPostulanteProceso = codPostulanteProceso;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public List getListaProcesos() {
		return listaProcesos;
	}
	public void setListaProcesos(List listaProcesos) {
		this.listaProcesos = listaProcesos;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public List<EvaluacionesByEvaluadorBean> getListPostRevisados() {
		return listPostRevisados;
	}
	public void setListPostRevisados(
			List<EvaluacionesByEvaluadorBean> listPostRevisados) {
		this.listPostRevisados = listPostRevisados;
	}
	
}
