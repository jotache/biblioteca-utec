package com.tecsup.SGA.web.reclutamiento.command;


import java.util.*;

public class CalificacionesConsultaCommand {

	private String operacion;
	private List listCalificaciones;
	
	private String codTipo;
	private List listTipos;
	
	private String codSelTipo;
	private String seltipo;
	
	private String descripcion;
	private String codDetalle;
	private String txhCodAreasNivelUno;
	private String cadCod;
	private String nroReg;
	private String msg;
	
	private String codTablaDetalle;
	private String codPeriodo;
	private String codProducto;
	private String valor;
	private String codEval;
	private int can;
	private String texto;
	
	
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public int getCan() {
		return can;
	}
	public void setCan(int can) {
		this.can = can;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getCadCod() {
		return cadCod;
	}
	public void setCadCod(String cadCod) {
		this.cadCod = cadCod;
	}
	public String getNroReg() {
		return nroReg;
	}
	public void setNroReg(String nroReg) {
		this.nroReg = nroReg;
	}
	public String getTxhCodAreasNivelUno() {
		return txhCodAreasNivelUno;
	}
	public void setTxhCodAreasNivelUno(String txhCodAreasNivelUno) {
		this.txhCodAreasNivelUno = txhCodAreasNivelUno;
	}
	public String getSeltipo() {
		return seltipo;
	}
	public void setSeltipo(String seltipo) {
		this.seltipo = seltipo;
	}
	public String getCodSelTipo() {
		return codSelTipo;
	}
	public void setCodSelTipo(String codSelTipo) {
		this.codSelTipo = codSelTipo;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public List getListTipos() {
		return listTipos;
	}
	public void setListTipos(List listTipos) {
		this.listTipos = listTipos;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListCalificaciones() {
		return listCalificaciones;
	}
	public void setListCalificaciones(List listCalificaciones) {
		this.listCalificaciones = listCalificaciones;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodTablaDetalle() {
		return codTablaDetalle;
	}
	public void setCodTablaDetalle(String codTablaDetalle) {
		this.codTablaDetalle = codTablaDetalle;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}

	
}
