package com.tecsup.SGA.web.reclutamiento.command;

public class ListaOfertasCommand {
	private String operacion;
	private String typeMessage;
	private String message;

	private String idRec;
	
	private String codOferta;
	private String oferta;
	private String area;
	private String fechaPubli;
	private String fechaCierre;
	private String nroVacantes;
	private String funcionesOferta;
	
	
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	public String getOferta() {
		return oferta;
	}
	public void setOferta(String oferta) {
		this.oferta = oferta;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getFechaPubli() {
		return fechaPubli;
	}
	public void setFechaPubli(String fechaPubli) {
		this.fechaPubli = fechaPubli;
	}
	public String getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public String getNroVacantes() {
		return nroVacantes;
	}
	public void setNroVacantes(String nroVacantes) {
		this.nroVacantes = nroVacantes;
	}
	public String getFuncionesOferta() {
		return funcionesOferta;
	}
	public void setFuncionesOferta(String funcionesOferta) {
		this.funcionesOferta = funcionesOferta;
	}
	public String getCodOferta() {
		return codOferta;
	}
	public void setCodOferta(String codOferta) {
		this.codOferta = codOferta;
	}
}
