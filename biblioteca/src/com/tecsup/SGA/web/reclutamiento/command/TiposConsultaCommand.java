package com.tecsup.SGA.web.reclutamiento.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class TiposConsultaCommand {

	private List ListaDenominacion;
	private String descripcion;
	private String operacion;
	private String tiposEvaluacion;
	private String codSelRevision;
	private String codSelSeleccion;
	private List listTipoEvaluacionRevision;
	private List listTipoEvaluacionSeleccion;
	private List listTipos;
	private String codEvaluacion;
	private List listTipoCalificacion;
	private int selTipo;
	private String  codSelCalificacion;
	private String codSelTipo;
	private String etapa;
	private List listConsulta;
	private List listEva;
	private String codEva;
	private String codEtapa;
	private String codCombo;
	private String msg;
	private String evaluacion;
	private String codTipoEva;
	private String codCalifElimi; 
	private String codCaliNor;
	private String cboTagsSeleccionados;
	private String cboEvaluacion;
	private int rdb;
	private String codEval;
	
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getCodCalifElimi() {
		return codCalifElimi;
	}
	public void setCodCalifElimi(String codCalifElimi) {
		this.codCalifElimi = codCalifElimi;
	}
	public String getCodTipoEva() {
		return codTipoEva;
	}
	public void setCodTipoEva(String codTipoEva) {
		this.codTipoEva = codTipoEva;
	}
	public String getEvaluacion() {
		return evaluacion;
	}
	public void setEvaluacion(String evaluacion) {
		this.evaluacion = evaluacion;
	}
	public List getListEva() {
		return listEva;
	}
	public void setListEva(List listEva) {
		this.listEva = listEva;
	}
	public String getCodEva() {
		return codEva;
	}
	public void setCodEva(String codEva) {
		this.codEva = codEva;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	
	public int getSelTipo() {
		return selTipo;
	}
	public void setSelTipo(int selTipo) {
		this.selTipo = selTipo;
	}
	public List getListTipoCalificacion() {
		return listTipoCalificacion;
	}
	public void setListTipoCalificacion(List listTipoCalificacion) {
		this.listTipoCalificacion = listTipoCalificacion;
	}
	
	public List getListaDenominacion() {
		return ListaDenominacion;
	}
	public void setListaDenominacion(List listaDenominacion) {
		ListaDenominacion = listaDenominacion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTiposEvaluacion() {
		return tiposEvaluacion;
	}
	public void setTiposEvaluacion(String tiposEvaluacion) {
		this.tiposEvaluacion = tiposEvaluacion;
	}
	public List getListTipoEvaluacionRevision() {
		return listTipoEvaluacionRevision;
	}
	public void setListTipoEvaluacionRevision(List listTipoEvaluacionRevision) {
		this.listTipoEvaluacionRevision = listTipoEvaluacionRevision;
	}
	public List getListTipoEvaluacionSeleccion() {
		return listTipoEvaluacionSeleccion;
	}
	public void setListTipoEvaluacionSeleccion(List listTipoEvaluacionSeleccion) {
		this.listTipoEvaluacionSeleccion = listTipoEvaluacionSeleccion;
	}
	public List getListTipos() {
		return listTipos;
	}
	public void setListTipos(List listTipos) {
		this.listTipos = listTipos;
	}
	public String getCodSelRevision() {
		return codSelRevision;
	}
	public void setCodSelRevision(String codSelRevision) {
		this.codSelRevision = codSelRevision;
	}
	public String getCodSelSeleccion() {
		return codSelSeleccion;
	}
	public void setCodSelSeleccion(String codSelSeleccion) {
		this.codSelSeleccion = codSelSeleccion;
	}
	public String getCodEvaluacion() {
		return codEvaluacion;
	}
	public void setCodEvaluacion(String codEvaluacion) {
		this.codEvaluacion = codEvaluacion;
	}
	public String getCodSelCalificacion() {
		return codSelCalificacion;
	}
	public void setCodSelCalificacion(String codSelCalificacion) {
		this.codSelCalificacion = codSelCalificacion;
	}
	public String getCodSelTipo() {
		return codSelTipo;
	}
	public void setCodSelTipo(String codSelTipo) {
		this.codSelTipo = codSelTipo;
	}
	public String getEtapa() {
		return etapa;
	}
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
	public List getListConsulta() {
		return listConsulta;
	}
	public void setListConsulta(List listConsulta) {
		this.listConsulta = listConsulta;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodCombo() {
		return codCombo;
	}
	public void setCodCombo(String codCombo) {
		this.codCombo = codCombo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodCaliNor() {
		return codCaliNor;
	}
	public void setCodCaliNor(String codCaliNor) {
		this.codCaliNor = codCaliNor;
	}
	public String getCboTagsSeleccionados() {
		return cboTagsSeleccionados;
	}
	public void setCboTagsSeleccionados(String cboTagsSeleccionados) {
		this.cboTagsSeleccionados = cboTagsSeleccionados;
	}
	public String getCboEvaluacion() {
		return cboEvaluacion;
	}
	public void setCboEvaluacion(String cboEvaluacion) {
		this.cboEvaluacion = cboEvaluacion;
	}
	public int getRdb() {
		return rdb;
	}
	public void setRdb(int rdb) {
		this.rdb = rdb;
	}
	
}
