package com.tecsup.SGA.web.reclutamiento.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class MtoMenuCommand {

	/****************	ATRIBUTOS	*******************/
	private long idOrden;
	private String descripcion;
	private String operacion;
	private String cboOrden;
	private List MtoMenuList;
	private String codEval;

	/****************	SETTER	*******************/
	public long GetIdOrden()
	{
		return this.idOrden;
	}
	public String GetDescripcion()
	{
		return this.descripcion;
	}
	public String getOperacion()
	{
		return this.operacion;
	}
	public String getCboOrden()
	{
		return this.cboOrden;
	}
	public List GetMtoMenuList()
	{
		return MtoMenuList;
	}
	
	/****************	GETTER	*******************/
	public void SetIdOrden(long idOrden)
	{
		this.idOrden =idOrden; 
	}
	public void SetDescripcion(String descripcion)
	{
		this.descripcion = 	descripcion;
	}
	public void SetOperacion(String operacion)
	{
		this.operacion = operacion;
	}
	public void SetCboOrden(String idOc)
	{
		this.cboOrden = idOc;
	}
	public void SetMtoMenuList(List MtoMenuList)
	{
		this.MtoMenuList = MtoMenuList;
	}
	
	public void Clear()
	{
		this.idOrden = 0;
		this.descripcion = "";
		this.operacion = "";
		this.MtoMenuList.clear();
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	
}
