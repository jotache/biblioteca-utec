package com.tecsup.SGA.web.reclutamiento.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

import org.springframework.web.multipart.MultipartFile;

public class AdjuntarCVCommand {
	
	MultipartFile txtCV2;
	public MultipartFile getTxtCV2() {
		return txtCV2;
	}

	public void setTxtCV2(MultipartFile file) {
		this.txtCV2 = file;
	}
	
	private String operacion;
	private String typeMessage;
	private String message;
	private String idRec;
	private String codUsuario; //Usado para el caso de adjuntar informe final de postulante;
	
	private String fotoOriginal;
	private String cvOriginal;
	private String foto; //valor para obtener nombre
	private String cv;
	
	private byte[] txtCV; //archivo a subir
	private byte[] txtFoto;
	
	private String extFoto; //extension
	private String extCv;
	
	private String infFinal;
	private String infFinalGen;
	
	public String getExtFoto() {
		return extFoto;
	}
	public void setExtFoto(String extFoto) {
		this.extFoto = extFoto;
	}
	public String getExtCv() {
		return extCv;
	}
	public void setExtCv(String extCv) {
		this.extCv = extCv;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	public byte[] getTxtCV() {
		return txtCV;
	}
	public void setTxtCV(byte[] txtCV) {
		this.txtCV = txtCV;
	}
	public byte[] getTxtFoto() {
		return txtFoto;
	}
	public void setTxtFoto(byte[] txtFoto) {
		this.txtFoto = txtFoto;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getFotoOriginal() {
		return fotoOriginal;
	}
	public void setFotoOriginal(String fotoOriginal) {
		this.fotoOriginal = fotoOriginal;
	}
	public String getCvOriginal() {
		return cvOriginal;
	}
	public void setCvOriginal(String cvOriginal) {
		this.cvOriginal = cvOriginal;
	}
	public String getInfFinal() {
		return infFinal;
	}
	public void setInfFinal(String infFinal) {
		this.infFinal = infFinal;
	}
	public String getInfFinalGen() {
		return infFinalGen;
	}
	public void setInfFinalGen(String infFinalGen) {
		this.infFinalGen = infFinalGen;
	}
	
	
}
