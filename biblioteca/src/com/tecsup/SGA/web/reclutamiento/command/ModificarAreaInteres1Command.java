package com.tecsup.SGA.web.reclutamiento.command;
import com.tecsup.SGA.modelo.*;

import java.util.*;
public class ModificarAreaInteres1Command {
	private String operacion;
	private String descripcion;
	private String codDetalle;
	private String usucrea;
	private String usuModi;
	private String CodProceso;
	private String DscProceso;
	private String Valor1;
	private String msg;
	private String codEval;
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getUsucrea() {
		return usucrea;
	}
	public void setUsucrea(String usucrea) {
		this.usucrea = usucrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getCodProceso() {
		return CodProceso;
	}
	public void setCodProceso(String codProceso) {
		CodProceso = codProceso;
	}
	public String getDscProceso() {
		return DscProceso;
	}
	public void setDscProceso(String dscProceso) {
		DscProceso = dscProceso;
	}
	public String getValor1() {
		return Valor1;
	}
	public void setValor1(String valor1) {
		Valor1 = valor1;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
