package com.tecsup.SGA.web.reclutamiento.command;

import java.util.*;
public class ProcesoCommand {

	private String codUsuario;
	private String operacion;
	private String message;
	private String codAreasIntSeleccionadas;
	private String codAreasEstSeleccionadas;
	private String codAreasNivEstSeleccionadas;
	private String codInstEduSeleccionadas;
	
	private List listAreasNivelEstudios;
	private List listAreasInteres;
	private List listInstEdu;
	
	private String codProceso;
	private String dscProceso;
	
	private String codUnidadFuncional;
	private List listUnidadFuncional;
	
	private String flagEtapa;
	
	private String codSexo;
	private List listSexo;
	
	private String codAreaEstudio;
	private List listAreaEstudio;
	
	private String codGradoAcedemico;
	private List listGradoAcedemico;
	
	private String codInstitucionAcademica;
	private List listInstitucionAcademica;
	
	private String salarioIni;
	private String salarioFin;
	
	private String codTipoEdad;
	private String codTipoEdad1;
	private List listTipoEdad;
	private String edad;
	private String edad1;
	
	private String codDedicacion;
	private List listDedicacion;
	
	private String codInteresado;
	private List listInteresado;
	
	private String flagDisponibilidadViajar;
	private String flagDisponibilidadViajar1;
	private String anhosExperienciaDocencia;
	private String anhosExperiencia;
	
	private String codProcRevAsociado;
	private List listProcRevAsociado;
	
	private String codTipoMoneda;
	private List listaTipoMoneda;
	
	/**
	 * @return the puestoPostulaDoc
	 */
	public String getPuestoPostulaDoc() {
		return puestoPostulaDoc;
	}
	/**
	 * @param puestoPostulaDoc the puestoPostulaDoc to set
	 */
	public void setPuestoPostulaDoc(String puestoPostulaDoc) {
		this.puestoPostulaDoc = puestoPostulaDoc;
	}
	/**
	 * @return the puestoPostulaAdm
	 */
	public String getPuestoPostulaAdm() {
		return puestoPostulaAdm;
	}
	/**
	 * @param puestoPostulaAdm the puestoPostulaAdm to set
	 */
	public void setPuestoPostulaAdm(String puestoPostulaAdm) {
		this.puestoPostulaAdm = puestoPostulaAdm;
	}
	private String puestoPostulaDoc;
	private String puestoPostulaAdm;
	
	public String getCodAreasIntSeleccionadas() {
		return codAreasIntSeleccionadas;
	}
	public void setCodAreasIntSeleccionadas(String codAreasIntSeleccionadas) {
		this.codAreasIntSeleccionadas = codAreasIntSeleccionadas;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getDscProceso() {
		return dscProceso;
	}
	public void setDscProceso(String dscProceso) {
		this.dscProceso = dscProceso;
	}
	public String getCodUnidadFuncional() {
		return codUnidadFuncional;
	}
	public void setCodUnidadFuncional(String codUnidadFuncional) {
		this.codUnidadFuncional = codUnidadFuncional;
	}
	public List getListUnidadFuncional() {
		return listUnidadFuncional;
	}
	public void setListUnidadFuncional(List listUnidadFuncional) {
		this.listUnidadFuncional = listUnidadFuncional;
	}
	public String getFlagEtapa() {
		return flagEtapa;
	}
	public void setFlagEtapa(String flagEtapa) {
		this.flagEtapa = flagEtapa;
	}
	public List getListAreasInteres() {
		return listAreasInteres;
	}
	public void setListAreasInteres(List listAreasInteres) {
		this.listAreasInteres = listAreasInteres;
	}
	public String getCodSexo() {
		return codSexo;
	}
	public void setCodSexo(String codSexo) {
		this.codSexo = codSexo;
	}
	public List getListSexo() {
		return listSexo;
	}
	public void setListSexo(List listSexo) {
		this.listSexo = listSexo;
	}
	public String getCodAreaEstudio() {
		return codAreaEstudio;
	}
	public void setCodAreaEstudio(String codAreaEstudio) {
		this.codAreaEstudio = codAreaEstudio;
	}
	public List getListAreaEstudio() {
		return listAreaEstudio;
	}
	public void setListAreaEstudio(List listAreaEstudio) {
		this.listAreaEstudio = listAreaEstudio;
	}
	public String getCodGradoAcedemico() {
		return codGradoAcedemico;
	}
	public void setCodGradoAcedemico(String codGradoAcedemico) {
		this.codGradoAcedemico = codGradoAcedemico;
	}
	public List getListGradoAcedemico() {
		return listGradoAcedemico;
	}
	public void setListGradoAcedemico(List listGradoAcedemico) {
		this.listGradoAcedemico = listGradoAcedemico;
	}
	public String getCodInstitucionAcademica() {
		return codInstitucionAcademica;
	}
	public void setCodInstitucionAcademica(String codInstitucionAcademica) {
		this.codInstitucionAcademica = codInstitucionAcademica;
	}
	public List getListInstitucionAcademica() {
		return listInstitucionAcademica;
	}
	public void setListInstitucionAcademica(List listInstitucionAcademica) {
		this.listInstitucionAcademica = listInstitucionAcademica;
	}
	public String getSalarioIni() {
		return salarioIni;
	}
	public void setSalarioIni(String salarioIni) {
		this.salarioIni = salarioIni;
	}
	public String getSalarioFin() {
		return salarioFin;
	}
	public void setSalarioFin(String salarioFin) {
		this.salarioFin = salarioFin;
	}
	public String getCodTipoEdad() {
		return codTipoEdad;
	}
	public void setCodTipoEdad(String codTipoEdad) {
		this.codTipoEdad = codTipoEdad;
	}
	public List getListTipoEdad() {
		return listTipoEdad;
	}
	public void setListTipoEdad(List listTipoEdad) {
		this.listTipoEdad = listTipoEdad;
	}
	public String getCodDedicacion() {
		return codDedicacion;
	}
	public void setCodDedicacion(String codDedicacion) {
		this.codDedicacion = codDedicacion;
	}
	public List getListDedicacion() {
		return listDedicacion;
	}
	public void setListDedicacion(List listDedicacion) {
		this.listDedicacion = listDedicacion;
	}
	public String getCodInteresado() {
		return codInteresado;
	}
	public void setCodInteresado(String codInteresado) {
		this.codInteresado = codInteresado;
	}
	public List getListInteresado() {
		return listInteresado;
	}
	public void setListInteresado(List listInteresado) {
		this.listInteresado = listInteresado;
	}
	public String getFlagDisponibilidadViajar() {
		return flagDisponibilidadViajar;
	}
	public void setFlagDisponibilidadViajar(String flagDisponibilidadViajar) {
		this.flagDisponibilidadViajar = flagDisponibilidadViajar;
	}
	public String getAnhosExperiencia() {
		return anhosExperiencia;
	}
	public void setAnhosExperiencia(String anhosExperiencia) {
		this.anhosExperiencia = anhosExperiencia;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getAnhosExperienciaDocencia() {
		return anhosExperienciaDocencia;
	}
	public void setAnhosExperienciaDocencia(String anhosExperienciaDocencia) {
		this.anhosExperienciaDocencia = anhosExperienciaDocencia;
	}
	public String getCodProcRevAsociado() {
		return codProcRevAsociado;
	}
	public void setCodProcRevAsociado(String codProcRevAsociado) {
		this.codProcRevAsociado = codProcRevAsociado;
	}
	public List getListProcRevAsociado() {
		return listProcRevAsociado;
	}
	public void setListProcRevAsociado(List listProcRevAsociado) {
		this.listProcRevAsociado = listProcRevAsociado;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getFlagDisponibilidadViajar1() {
		return flagDisponibilidadViajar1;
	}
	public void setFlagDisponibilidadViajar1(String flagDisponibilidadViajar1) {
		this.flagDisponibilidadViajar1 = flagDisponibilidadViajar1;
	}	
	
	public String getCodTipoEdad1() {
		return codTipoEdad1;
	}
	public void setCodTipoEdad1(String codTipoEdad1) {
		this.codTipoEdad1 = codTipoEdad1;
	}
	public String getEdad1() {
		return edad1;
	}
	public void setEdad1(String edad1) {
		this.edad1 = edad1;
	}
	public ProcesoCommand ()
	{
		this.operacion = "";
		this.message = "";
		this.codProceso = "";
		this.dscProceso = "";
		this.codUnidadFuncional = "";
		this.listUnidadFuncional = null;		
		this.flagEtapa = "1";
		this.listAreasInteres = null;
		this.codSexo = "";
		this.listSexo = null;		
		this.codAreaEstudio = "";
		this.codAreasNivEstSeleccionadas="";
		this.listAreaEstudio = null;		
		this.codGradoAcedemico = "";
		this.listGradoAcedemico = null;		
		this.codInstitucionAcademica = "";
		this.listInstitucionAcademica = null;		
		this.salarioIni = "";
		this.salarioFin = "";		
		this.codTipoEdad = "";
		this.codTipoEdad1 = "";	
		this.listTipoEdad = null;
		this.edad = "";		
		this.edad1 = "";
		this.codDedicacion = "";
		this.listDedicacion = null;		
		this.codInteresado = "";
		this.listInteresado = null;		
		this.flagDisponibilidadViajar = "";
		this.anhosExperienciaDocencia = "";
		this.anhosExperiencia = "";		
		this.codProcRevAsociado = "";
		this.listProcRevAsociado = null;
	}
	public String getCodTipoMoneda() {
		return codTipoMoneda;
	}
	public void setCodTipoMoneda(String codTipoMoneda) {
		this.codTipoMoneda = codTipoMoneda;
	}
	public List getListaTipoMoneda() {
		return listaTipoMoneda;
	}
	public void setListaTipoMoneda(List listaTipoMoneda) {
		this.listaTipoMoneda = listaTipoMoneda;
	}
	
	public String getCodAreasEstSeleccionadas() {
		return codAreasEstSeleccionadas;
	}
	public void setCodAreasEstSeleccionadas(String codAreasEstSeleccionadas) {
		this.codAreasEstSeleccionadas = codAreasEstSeleccionadas;
	}
	public String getCodAreasNivEstSeleccionadas() {
		return codAreasNivEstSeleccionadas;
	}
	public void setCodAreasNivEstSeleccionadas(String codAreasNivEstSeleccionadas) {
		this.codAreasNivEstSeleccionadas = codAreasNivEstSeleccionadas;
	}
	public List getListAreasNivelEstudios() {
		return listAreasNivelEstudios;
	}
	public void setListAreasNivelEstudios(List listAreasNivelEstudios) {
		this.listAreasNivelEstudios = listAreasNivelEstudios;
	}
	public String getCodInstEduSeleccionadas() {
		return codInstEduSeleccionadas;
	}
	public void setCodInstEduSeleccionadas(String codInstEduSeleccionadas) {
		this.codInstEduSeleccionadas = codInstEduSeleccionadas;
	}
	public List getListInstEdu() {
		return listInstEdu;
	}
	public void setListInstEdu(List listInstEdu) {
		this.listInstEdu = listInstEdu;
	}
	
	
}
