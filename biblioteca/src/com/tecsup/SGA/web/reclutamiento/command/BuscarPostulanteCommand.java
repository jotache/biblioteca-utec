package com.tecsup.SGA.web.reclutamiento.command;

import java.util.List;
public class BuscarPostulanteCommand {
	/*	ATRIBUTOS	*/
	private String codUsuario;
	private String operacion;
	private String message;
	
	private String codProceso;
	private String codPostulantesSeleccioandos;
	private String nombres;
	private String apellidos;	
	
	private String codAreasInteres;
	private String strAreaInteres;
	private List listAreasInteres;
	
	private String codProfesion;
	private List listProfesion;
	
	private String codEgresado;
	private List listEgresado;
	
	private String codGrado;
	private List listGrado;
	
	private List listDetalle;
		
	/*	GETTERS Y SETTERS	*/
	
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCodAreasInteres() {
		return codAreasInteres;
	}
	public void setCodAreasInteres(String codAreasInteres) {
		this.codAreasInteres = codAreasInteres;
	}
	public String getCodProfesion() {
		return codProfesion;
	}
	public void setCodProfesion(String codProfesion) {
		this.codProfesion = codProfesion;
	}
	public List getListProfesion() {
		return listProfesion;
	}
	public void setListProfesion(List listProfesion) {
		this.listProfesion = listProfesion;
	}
	public String getCodEgresado() {
		return codEgresado;
	}
	public void setCodEgresado(String codEgresado) {
		this.codEgresado = codEgresado;
	}
	public List getListEgresado() {
		return listEgresado;
	}
	public void setListEgresado(List listEgresado) {
		this.listEgresado = listEgresado;
	}
	public String getCodGrado() {
		return codGrado;
	}
	public void setCodGrado(String codGrado) {
		this.codGrado = codGrado;
	}
	public List getListGrado() {
		return listGrado;
	}
	public void setListGrado(List listGrado) {
		this.listGrado = listGrado;
	}
	public List getListDetalle() {
		return listDetalle;
	}
	public void setListDetalle(List listDetalle) {
		this.listDetalle = listDetalle;
	}
	public String getStrAreaInteres() {
		return strAreaInteres;
	}
	public void setStrAreaInteres(String strAreaInteres) {
		this.strAreaInteres = strAreaInteres;
	}
	public List getListAreasInteres() {
		return listAreasInteres;
	}
	public void setListAreasInteres(List listAreasInteres) {
		this.listAreasInteres = listAreasInteres;
	}
	public String getCodPostulantesSeleccioandos() {
		return codPostulantesSeleccioandos;
	}
	public void setCodPostulantesSeleccioandos(String codPostulantesSeleccioandos) {
		this.codPostulantesSeleccioandos = codPostulantesSeleccioandos;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
	
}
