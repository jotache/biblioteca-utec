package com.tecsup.SGA.web.reclutamiento.command;

public class OlvidoClaveCommand {
	private String operacion;
	private String message;
	private String typeMessage;
	
	private String usuario;
	
	public String getUsuario() {
		return usuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}