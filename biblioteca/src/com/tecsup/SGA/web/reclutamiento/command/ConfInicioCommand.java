package com.tecsup.SGA.web.reclutamiento.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class ConfInicioCommand {

	/****************	ATRIBUTOS	*******************/
	private long idOrden;
	private String descripcion;
	private String operacion;
	private String cboOrden;
	private List ConfInicioList;

	/****************	SETTER	*******************/
	public long GetIdOrden()
	{
		return this.idOrden;
	}
	public String GetDescripcion()
	{
		return this.descripcion;
	}
	public String getOperacion()
	{
		return this.operacion;
	}
	public String getCboOrden()
	{
		return this.cboOrden;
	}
	public List GetConfInicioList()
	{
		return ConfInicioList;
	}
	
	/****************	GETTER	*******************/
	public void SetIdOrden(long idOrden)
	{
		this.idOrden =idOrden; 
	}
	public void SetDescripcion(String descripcion)
	{
		this.descripcion = 	descripcion;
	}
	public void SetOperacion(String operacion)
	{
		this.operacion = operacion;
	}
	public void SetCboOrden(String idOc)
	{
		this.cboOrden = idOc;
	}
	public void SetConfInicioList(List ConfInicioList)
	{
		this.ConfInicioList = ConfInicioList;
	}
	
	public void Clear()
	{
		this.idOrden = 0;
		this.descripcion = "";
		this.operacion = "";
		this.ConfInicioList.clear();
	}
	
	
}
