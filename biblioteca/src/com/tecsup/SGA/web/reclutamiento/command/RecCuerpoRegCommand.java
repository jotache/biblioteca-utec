package com.tecsup.SGA.web.reclutamiento.command;

public class RecCuerpoRegCommand {
	
	private String operacion;
	private String idRec;
	private String indMenu;
	private String lstrURL;
	private String codUsuario;
	
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getIndMenu() {
		return indMenu;
	}
	public void setIndMenu(String indMenu) {
		this.indMenu = indMenu;
	}

	
}
