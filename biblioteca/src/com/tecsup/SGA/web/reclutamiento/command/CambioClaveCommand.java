package com.tecsup.SGA.web.reclutamiento.command;

public class CambioClaveCommand {
	private String operacion;
	private String message;
	private String typeMessage;
	
	private String usuario;
	private String clave; //clave anterior
	private String claveNueva; //clave nueva
	private String claveNueva1; //clave nueva confirmada
	
	
	public String getUsuario() {
		return usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getClaveNueva() {
		return claveNueva;
	}
	public void setClaveNueva(String claveNueva) {
		this.claveNueva = claveNueva;
	}
	public String getClaveNueva1() {
		return claveNueva1;
	}
	public void setClaveNueva1(String claveNueva1) {
		this.claveNueva1 = claveNueva1;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	
}