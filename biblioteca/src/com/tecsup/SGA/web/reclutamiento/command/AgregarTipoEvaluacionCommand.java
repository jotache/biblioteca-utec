package com.tecsup.SGA.web.reclutamiento.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class AgregarTipoEvaluacionCommand {
	private String operacion;
	private String descripcion;
	private String codDetalle;
	private String usucrea;
	private String usuModi;
	private String msg;
	private String cantidad;
	private String dscValor2;
	private String codEval;
	private String eta;
	
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getUsucrea() {
		return usucrea;
	}
	public void setUsucrea(String usucrea) {
		this.usucrea = usucrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getDscValor2() {
		return dscValor2;
	}
	public void setDscValor2(String dscValor2) {
		this.dscValor2 = dscValor2;
	}
	
	
}
