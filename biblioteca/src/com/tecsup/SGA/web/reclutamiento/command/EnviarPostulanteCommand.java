package com.tecsup.SGA.web.reclutamiento.command;

import java.util.List;

public class EnviarPostulanteCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	private String msgAux;
	private String datosRRHH1; //Codigos Postulantes
	private String datosRRHH2; //Nombres Postulantes
	private String datosRRHH3; //Codigo Tipo Evaluaciones
	private String datosRRHH4; //DscTipoEvaluacion
	private String datosRRHH5; //Codigos Procesos
	private String datosRRHH6; //Codigos Evaluaciones en Proceso
	private String datosRRHH7; //Codigos Postulantes en Proceso
	private String codProceso;
	private String codPostSel;
	private String nomPostSel;
	private String codCalificaciones;
	
	/*Codigo del de post_proceso y codigo de rec_evaluaciones*/
	private String codPostulanteProceso;
	private String codEvaluacionActual;
	
	private String codPostulantes;
	private List listPostulantes;
	
	private String codEtapa;
	private String dscEtapa;
	
	private String numEvaluaciones;
	private String codEvalSel;
	private String dscEvalSel;
	private List ListEvaluaciones;
	
	private String codCalSel;
	private List listCalificaciones;
	
	private String comentario;
	
	
	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodProceso() {
		return codProceso;
	}

	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}

	public String getCodPostSel() {
		return codPostSel;
	}

	public void setCodPostSel(String codPostSel) {
		this.codPostSel = codPostSel;
	}

	public String getNomPostSel() {
		return nomPostSel;
	}

	public void setNomPostSel(String nomPostSel) {
		this.nomPostSel = nomPostSel;
	}

	public List getListPostulantes() {
		return listPostulantes;
	}

	public void setListPostulantes(List listPostulantes) {
		this.listPostulantes = listPostulantes;
	}

	public String getCodEtapa() {
		return codEtapa;
	}

	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}

	public String getDscEtapa() {
		return dscEtapa;
	}

	public void setDscEtapa(String dscEtapa) {
		this.dscEtapa = dscEtapa;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getCodPostulantes() {
		return codPostulantes;
	}

	public void setCodPostulantes(String codPostulantes) {
		this.codPostulantes = codPostulantes;
	}

	public String getCodEvalSel() {
		return codEvalSel;
	}

	public void setCodEvalSel(String codEvalSel) {
		this.codEvalSel = codEvalSel;
	}

	public List getListEvaluaciones() {
		return ListEvaluaciones;
	}

	public void setListEvaluaciones(List listEvaluaciones) {
		ListEvaluaciones = listEvaluaciones;
	}

	public String getCodCalSel() {
		return codCalSel;
	}

	public void setCodCalSel(String codCalSel) {
		this.codCalSel = codCalSel;
	}

	public List getListCalificaciones() {
		return listCalificaciones;
	}

	public void setListCalificaciones(List listCalificaciones) {
		this.listCalificaciones = listCalificaciones;
	}

	public String getNumEvaluaciones() {
		return numEvaluaciones;
	}

	public void setNumEvaluaciones(String numEvaluaciones) {
		this.numEvaluaciones = numEvaluaciones;
	}

	public String getCodCalificaciones() {
		return codCalificaciones;
	}

	public void setCodCalificaciones(String codCalificaciones) {
		this.codCalificaciones = codCalificaciones;
	}

	public String getDscEvalSel() {
		return dscEvalSel;
	}

	public void setDscEvalSel(String dscEvalSel) {
		this.dscEvalSel = dscEvalSel;
	}

	public String getCodPostulanteProceso() {
		return codPostulanteProceso;
	}

	public void setCodPostulanteProceso(String codPostulanteProceso) {
		this.codPostulanteProceso = codPostulanteProceso;
	}

	public String getCodEvaluacionActual() {
		return codEvaluacionActual;
	}

	public void setCodEvaluacionActual(String codEvaluacionActual) {
		this.codEvaluacionActual = codEvaluacionActual;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getMsgAux() {
		return msgAux;
	}

	public void setMsgAux(String msgAux) {
		this.msgAux = msgAux;
	}

	public String getDatosRRHH1() {
		return datosRRHH1;
	}

	public void setDatosRRHH1(String datosRRHH1) {
		this.datosRRHH1 = datosRRHH1;
	}

	public String getDatosRRHH2() {
		return datosRRHH2;
	}

	public void setDatosRRHH2(String datosRRHH2) {
		this.datosRRHH2 = datosRRHH2;
	}

	public String getDatosRRHH3() {
		return datosRRHH3;
	}

	public void setDatosRRHH3(String datosRRHH3) {
		this.datosRRHH3 = datosRRHH3;
	}

	public String getDatosRRHH4() {
		return datosRRHH4;
	}

	public void setDatosRRHH4(String datosRRHH4) {
		this.datosRRHH4 = datosRRHH4;
	}

	public String getDatosRRHH5() {
		return datosRRHH5;
	}

	public void setDatosRRHH5(String datosRRHH5) {
		this.datosRRHH5 = datosRRHH5;
	}

	public String getDatosRRHH6() {
		return datosRRHH6;
	}

	public void setDatosRRHH6(String datosRRHH6) {
		this.datosRRHH6 = datosRRHH6;
	}

	public String getDatosRRHH7() {
		return datosRRHH7;
	}

	public void setDatosRRHH7(String datosRRHH7) {
		this.datosRRHH7 = datosRRHH7;
	}

	
}
