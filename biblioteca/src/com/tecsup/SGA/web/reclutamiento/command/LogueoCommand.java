package com.tecsup.SGA.web.reclutamiento.command;

public class LogueoCommand {
	private String operacion;
	private String typeMessage;
	private String message;
	private String usuario;
	private String clave;
	private String idRec;
	private String codUsuario; // usuario ADM de RRHH que modifica los datos de un postulante
	
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
