package com.tecsup.SGA.web.reclutamiento.command;

import java.util.*;
import com.tecsup.SGA.modelo.*;
import com.tecsup.SGA.common.CommonConstants;

public class ProcesosPostulantesCommand {
	private String codUsuario;
	private String operacion;
	private String message;
	private String nombres;
	private String apellidos;
		
	private String codPostulantesSel;
	private String nomPostulantesSel;
	private String estPostulantesSel;
	
	private String codProceso;
	private String codProcesoRel;
	private String codEtapa;
	private String dscEtapa;
	private String dscProceso;
	private String codOperta;
	private String dscOferta;
	private List<Postulante> listPostulantes;
	private String codPostulanteCV;
	
	private List listCalificaciones;
	private String codCalificacion;
	
	private String codEstSinRevision;
	private String codEstRevisadoNoEnviado;
	private String codEstRevJefe;
	private String codEstSelSinEvaluar;
	private String codProcSelEnviadoAEvaluadores;
	
	
	private String comentario;
	
	private String infFinal;
	private String infFinalGen;
	//AGREGAOD RNAPA
	private String tipoEtapa;
	private String codUniFun;
	private String fecIni;
	private String fecFin;
	
	private String fecIniPost;
	private String fecFinPost;
	private String tipoVistaFecha1 ; //para filtrar postulnades por un rango de fechas (fecha de registro).
	private String tipoVistaFecha2 ;
	
	private int nroEvaluadores;	
	
	public String getCodUniFun() {
		return codUniFun;
	}
	public void setCodUniFun(String codUniFun) {
		this.codUniFun = codUniFun;
	}
	public String getFecIni() {
		return fecIni;
	}
	public void setFecIni(String fecIni) {
		this.fecIni = fecIni;
	}
	public String getFecFin() {
		return fecFin;
	}
	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}
	public String getCodEstSelSinEvaluar() {
		return codEstSelSinEvaluar;
	}
	public void setCodEstSelSinEvaluar(String codEstSelSinEvaluar) {
		this.codEstSelSinEvaluar = codEstSelSinEvaluar;
	}
	public String getCodProcesoRel() {
		return codProcesoRel;
	}
	public void setCodProcesoRel(String codProcesoRel) {
		this.codProcesoRel = codProcesoRel;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getCodEstRevJefe() {
		return codEstRevJefe;
	}
	public void setCodEstRevJefe(String codEstRevJefe) {
		this.codEstRevJefe = codEstRevJefe;
	}
	public List getListCalificaciones() {
		return listCalificaciones;
	}
	public void setListCalificaciones(List listCalificaciones) {
		this.listCalificaciones = listCalificaciones;
	}
	public String getCodCalificacion() {
		return codCalificacion;
	}
	public void setCodCalificacion(String codCalificacion) {
		this.codCalificacion = codCalificacion;
	}
	public String getNomPostulantesSel() {
		return nomPostulantesSel;
	}
	public void setNomPostulantesSel(String nomPostulantesSel) {
		this.nomPostulantesSel = nomPostulantesSel;
	}
	public String getCodPostulantesSel() {
		return codPostulantesSel;
	}
	public void setCodPostulantesSel(String codPostulantesSel) {
		this.codPostulantesSel = codPostulantesSel;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getDscEtapa() {
		return dscEtapa;
	}
	public void setDscEtapa(String dscEtapa) {
		this.dscEtapa = dscEtapa;
	}
	public String getDscProceso() {
		return dscProceso;
	}
	public void setDscProceso(String dscProceso) {
		this.dscProceso = dscProceso;
	}
	public String getCodOperta() {
		return codOperta;
	}
	public void setCodOperta(String codOperta) {
		this.codOperta = codOperta;
	}
	public String getCodPostulanteCV() {
		return codPostulanteCV;
	}
	public void setCodPostulanteCV(String codPostulanteCV) {
		this.codPostulanteCV = codPostulanteCV;
	}
	public String getEstPostulantesSel() {
		return estPostulantesSel;
	}
	public void setEstPostulantesSel(String estPostulantesSel) {
		this.estPostulantesSel = estPostulantesSel;
	}	
	
	public String getCodEstSinRevision() {
		return codEstSinRevision;
	}
	public void setCodEstSinRevision(String codEstSinRevision) {
		this.codEstSinRevision = codEstSinRevision;
	}
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public ProcesosPostulantesCommand()
	{
		this.codEstSinRevision = CommonConstants.EST_REV_SIN_REVISION;
		this.codEstSelSinEvaluar = CommonConstants.EST_SEL_SIN_EVALUAR;
		this.codEstRevJefe = CommonConstants.EST_REV_REV_X_JEFE_DPTO;
		this.codEstRevisadoNoEnviado = CommonConstants.EST_REV_REVISADO_NO_ENVIADO;
		this.codProcSelEnviadoAEvaluadores = CommonConstants.EST_SEL_ENVIADO_EVAL;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getInfFinal() {
		return infFinal;
	}
	public void setInfFinal(String infFinal) {
		this.infFinal = infFinal;
	}
	public String getInfFinalGen() {
		return infFinalGen;
	}
	public void setInfFinalGen(String infFinalGen) {
		this.infFinalGen = infFinalGen;
	}
	public String getDscOferta() {
		return dscOferta;
	}
	public void setDscOferta(String dscOferta) {
		this.dscOferta = dscOferta;
	}
	public String getTipoEtapa() {
		return tipoEtapa;
	}
	public void setTipoEtapa(String tipoEtapa) {
		this.tipoEtapa = tipoEtapa;
	}
	public String getCodEstRevisadoNoEnviado() {
		return codEstRevisadoNoEnviado;
	}
	public void setCodEstRevisadoNoEnviado(String codEstRevisadoNoEnviado) {
		this.codEstRevisadoNoEnviado = codEstRevisadoNoEnviado;
	}
	public List<Postulante> getListPostulantes() {
		return listPostulantes;
	}
	public void setListPostulantes(List<Postulante> listPostulantes) {
		this.listPostulantes = listPostulantes;
	}
	public String getCodProcSelEnviadoAEvaluadores() {
		return codProcSelEnviadoAEvaluadores;
	}
	public void setCodProcSelEnviadoAEvaluadores(
			String codProcSelEnviadoAEvaluadores) {
		this.codProcSelEnviadoAEvaluadores = codProcSelEnviadoAEvaluadores;
	}
	public int getNroEvaluadores() {
		return nroEvaluadores;
	}
	public void setNroEvaluadores(int nroEvaluadores) {
		this.nroEvaluadores = nroEvaluadores;
	}
	public String getFecIniPost() {
		return fecIniPost;
	}
	public void setFecIniPost(String fecIniPost) {
		this.fecIniPost = fecIniPost;
	}
	public String getFecFinPost() {
		return fecFinPost;
	}
	public void setFecFinPost(String fecFinPost) {
		this.fecFinPost = fecFinPost;
	}
	public String getTipoVistaFecha1() {
		return tipoVistaFecha1;
	}
	public void setTipoVistaFecha1(String tipoVistaFecha1) {
		this.tipoVistaFecha1 = tipoVistaFecha1;
	}
	public String getTipoVistaFecha2() {
		return tipoVistaFecha2;
	}
	public void setTipoVistaFecha2(String tipoVistaFecha2) {
		this.tipoVistaFecha2 = tipoVistaFecha2;
	}
	
}
