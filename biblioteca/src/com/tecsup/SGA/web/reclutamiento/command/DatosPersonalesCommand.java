package com.tecsup.SGA.web.reclutamiento.command;

import java.util.*;


public class DatosPersonalesCommand {
	
	private String operacion;
	private String typeMessage;
	private String message;
	
	private String nroRegAreaInt; //cantidad de codigos (area inter�s) a grabar
	private String codArea; //codigos concatenados (area inter�s) a grabar
	
	private String idRec;
	
	/*Datos Personales*/
	private String nombres;
	private String apepat;
	private String apemat;
	private String foto;
	private String fecnac;
	private String fecreg;
	private String sexo;
	private String nacionalidad;
	private String ruc;
	private String anioExpLaboral;
	
	private String estadoCivil;
	private List listaEstadoCivil;
	
	private String dni;
	private String email; //Email principal
	private String email1; //Email
	private String clave; //clave
	private String clave1; //clave confirmaci�n
	
	/*Domicilio*/
	private String direccion;
	
	private String departamento;
	private List listaDepartamento;
	
	private String provincia;
	private List listaProvincia;
	
	private String distrito;
	private List listaDistrito;
	
	private String pais; //pais de residencia
	private String telef; //telef domicilio
	private String telefAdicional;
	private String telefMovil;
	private String codPostal;
	
	/*Informaci�n Laboral*/
	private String areaInteres;
	private List listaAreaInteresH; //Tabla areas de interes hijos
	private List listaAreaInteres;
	private List listaAreaInteresByPost;
	private String cadenaAI; //cadena area de interes para la b�squeda
	
	
	private String postuladoAntes; //char 1 (Si-No)
	private String trabajadoAntes; //char 1 (Si-No)
	private String familiaTecsup; //char 1 (Si-No)
	private String familiaNombres;
	private String expDocente; //char 1 (Si-No)
	private String indPago; //char 1 (Mes-Horas)
	private String expDocenteAnios;
	private String dispoViaje; //char 1 (Si-No)}
	
	private String sedePrefTrabajo;
	private List listaSedePrefTrabajo;
	
	private String interesEn;
	private List listaInteresEn;
	
	private String pretensionEconomica;
	
	private String dispoTrabajar;
	private List listaDispoTrabajar;
	
	private String dedicacion;
	private List listaDedicacion;
	
	/*Perfil Profesional*/
	private String perfil;
	//modificado RNAPA
	private List listaMoneda;
	private String moneda;
	private String puestoPostula;
	private String codUsuario;//usuario que modifica puede ser el mismo postulante como tambi�n un empleado con accesos de ADM en RRHH.
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}

	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	/**
	 * @return the puestoPostula
	 */
	public String getPuestoPostula() {
		return puestoPostula;
	}

	/**
	 * @param puestoPostula the puestoPostula to set
	 */
	public void setPuestoPostula(String puestoPostula) {
		this.puestoPostula = puestoPostula;
	}

	public List getListaMoneda() {
		return listaMoneda;
	}

	public void setListaMoneda(List listaMoneda) {
		this.listaMoneda = listaMoneda;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApepat() {
		return apepat;
	}

	public void setApepat(String apepat) {
		this.apepat = apepat;
	}

	public String getApemat() {
		return apemat;
	}

	public void setApemat(String apemat) {
		this.apemat = apemat;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getFecreg() {
		return fecreg;
	}

	public void setFecreg(String fecreg) {
		this.fecreg = fecreg;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getClave1() {
		return clave1;
	}

	public String getTelef() {
		return telef;
	}

	public void setTelef(String telef) {
		this.telef = telef;
	}

	public String getTelefAdicional() {
		return telefAdicional;
	}

	public void setTelefAdicional(String telefAdicional) {
		this.telefAdicional = telefAdicional;
	}

	public String getAnioExpLaboral() {
		return anioExpLaboral;
	}

	public void setAnioExpLaboral(String anioExpLaboral) {
		this.anioExpLaboral = anioExpLaboral;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public List getListaEstadoCivil() {
		return listaEstadoCivil;
	}

	public void setListaEstadoCivil(List listaEstadoCivil) {
		this.listaEstadoCivil = listaEstadoCivil;
	}

	public List getListaDepartamento() {
		return listaDepartamento;
	}

	public void setListaDepartamento(List listaDepartamento) {
		this.listaDepartamento = listaDepartamento;
	}

	public List getListaProvincia() {
		return listaProvincia;
	}

	public void setListaProvincia(List listaProvincia) {
		this.listaProvincia = listaProvincia;
	}

	public List getListaDistrito() {
		return listaDistrito;
	}

	public void setListaDistrito(List listaDistrito) {
		this.listaDistrito = listaDistrito;
	}

	public String getTelefMovil() {
		return telefMovil;
	}

	public void setTelefMovil(String telefMovil) {
		this.telefMovil = telefMovil;
	}

	public String getCodPostal() {
		return codPostal;
	}

	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	public String getAreaInteres() {
		return areaInteres;
	}

	public void setAreaInteres(String areaInteres) {
		this.areaInteres = areaInteres;
	}

	public List getListaAreaInteres() {
		return listaAreaInteres;
	}

	public void setListaAreaInteres(List listaAreaInteres) {
		this.listaAreaInteres = listaAreaInteres;
	}

	public String getPostuladoAntes() {
		return postuladoAntes;
	}

	public void setPostuladoAntes(String postuladoAntes) {
		this.postuladoAntes = postuladoAntes;
	}

	public String getTrabajadoAntes() {
		return trabajadoAntes;
	}

	public void setTrabajadoAntes(String trabajadoAntes) {
		this.trabajadoAntes = trabajadoAntes;
	}

	public String getFamiliaTecsup() {
		return familiaTecsup;
	}

	public void setFamiliaTecsup(String familiaTecsup) {
		this.familiaTecsup = familiaTecsup;
	}

	public String getFamiliaNombres() {
		return familiaNombres;
	}

	public void setFamiliaNombres(String familiaNombres) {
		this.familiaNombres = familiaNombres;
	}

	public String getExpDocente() {
		return expDocente;
	}

	public void setExpDocente(String expDocente) {
		this.expDocente = expDocente;
	}

	public String getExpDocenteAnios() {
		return expDocenteAnios;
	}

	public void setExpDocenteAnios(String expDocenteAnios) {
		this.expDocenteAnios = expDocenteAnios;
	}

	public String getDispoViaje() {
		return dispoViaje;
	}

	public void setDispoViaje(String dispoViaje) {
		this.dispoViaje = dispoViaje;
	}

	public String getSedePrefTrabajo() {
		return sedePrefTrabajo;
	}

	public void setSedePrefTrabajo(String sedePrefTrabajo) {
		this.sedePrefTrabajo = sedePrefTrabajo;
	}

	public List getListaSedePrefTrabajo() {
		return listaSedePrefTrabajo;
	}

	public void setListaSedePrefTrabajo(List listaSedePrefTrabajo) {
		this.listaSedePrefTrabajo = listaSedePrefTrabajo;
	}

	public String getInteresEn() {
		return interesEn;
	}

	public void setInteresEn(String interesEn) {
		this.interesEn = interesEn;
	}

	public List getListaInteresEn() {
		return listaInteresEn;
	}

	public void setListaInteresEn(List listaInteresEn) {
		this.listaInteresEn = listaInteresEn;
	}

	public String getDispoTrabajar() {
		return dispoTrabajar;
	}

	public void setDispoTrabajar(String dispoTrabajar) {
		this.dispoTrabajar = dispoTrabajar;
	}

	public List getListaDispoTrabajar() {
		return listaDispoTrabajar;
	}

	public void setListaDispoTrabajar(List listaDispoTrabajar) {
		this.listaDispoTrabajar = listaDispoTrabajar;
	}

	public String getDedicacion() {
		return dedicacion;
	}

	public void setDedicacion(String dedicacion) {
		this.dedicacion = dedicacion;
	}

	public List getListaDedicacion() {
		return listaDedicacion;
	}

	public void setListaDedicacion(List listaDedicacion) {
		this.listaDedicacion = listaDedicacion;
	}

	public String getFecnac() {
		return fecnac;
	}

	public void setFecnac(String fecnac) {
		this.fecnac = fecnac;
	}

	public String getIdRec() {
		return idRec;
	}

	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}

	public List getListaAreaInteresH() {
		return listaAreaInteresH;
	}

	public void setListaAreaInteresH(List listaAreaInteresH) {
		this.listaAreaInteresH = listaAreaInteresH;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setClave1(String clave1) {
		this.clave1 = clave1;
	}

	public String getNroRegAreaInt() {
		return nroRegAreaInt;
	}

	public void setNroRegAreaInt(String nroRegAreaInt) {
		this.nroRegAreaInt = nroRegAreaInt;
	}

	public String getPretensionEconomica() {
		return pretensionEconomica;
	}

	public void setPretensionEconomica(String pretensionEconomica) {
		this.pretensionEconomica = pretensionEconomica;
	}

	public String getTypeMessage() {
		return typeMessage;
	}

	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}

	public String getCodArea() {
		return codArea;
	}

	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}

	public List getListaAreaInteresByPost() {
		return listaAreaInteresByPost;
	}

	public void setListaAreaInteresByPost(List listaAreaInteresByPost) {
		this.listaAreaInteresByPost = listaAreaInteresByPost;
	}

	public String getCadenaAI() {
		return cadenaAI;
	}

	public void setCadenaAI(String cadenaAI) {
		this.cadenaAI = cadenaAI;
	}

	public String getIndPago() {
		return indPago;
	}

	public void setIndPago(String indPago) {
		this.indPago = indPago;
	}
}
