package com.tecsup.SGA.web.reclutamiento.command;

public class OfertaCommand {
	private String operacion;
	private String codUsuario;
	private String indEdicicion;
	
	private String codOferta;
	private String codProceso;
	
	private String message;
	private String dscProceso;
	private String dscUnidadFuncional;
	
	private String dscOferta;
	private String nroVacantes;
	private String textoPublicar;
	private String fecIniPublicacion;
	private String fecFinPublicacion;
	
	
	public String getIndEdicicion() {
		return indEdicicion;
	}
	public void setIndEdicicion(String indEdicicion) {
		this.indEdicicion = indEdicicion;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCodOferta() {
		return codOferta;
	}
	public void setCodOferta(String codOferta) {
		this.codOferta = codOferta;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getDscProceso() {
		return dscProceso;
	}
	public void setDscProceso(String dscProceso) {
		this.dscProceso = dscProceso;
	}
	public String getDscUnidadFuncional() {
		return dscUnidadFuncional;
	}
	public void setDscUnidadFuncional(String dscUnidadFuncional) {
		this.dscUnidadFuncional = dscUnidadFuncional;
	}
	public String getDscOferta() {
		return dscOferta;
	}
	public void setDscOferta(String dscOferta) {
		this.dscOferta = dscOferta;
	}
	public String getNroVacantes() {
		return nroVacantes;
	}
	public void setNroVacantes(String nroVacantes) {
		this.nroVacantes = nroVacantes;
	}
	public String getTextoPublicar() {
		return textoPublicar;
	}
	public void setTextoPublicar(String textoPublicar) {
		this.textoPublicar = textoPublicar;
	}
	public String getFecIniPublicacion() {
		return fecIniPublicacion;
	}
	public void setFecIniPublicacion(String fecIniPublicacion) {
		this.fecIniPublicacion = fecIniPublicacion;
	}
	public String getFecFinPublicacion() {
		return fecFinPublicacion;
	}
	public void setFecFinPublicacion(String fecFinPublicacion) {
		this.fecFinPublicacion = fecFinPublicacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
	
}
