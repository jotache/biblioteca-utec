package com.tecsup.SGA.web.reclutamiento.command;

import java.util.*;
public class BuscarAreasInteresCommand {
	
	private String operacion;
	private String codAreaIntNiv1;
	private List listAreaIntNiv1;
	private String codAreaIntNiv2;
	private String dscAreaIntNiv2;
	private List listAreaIntNiv2;
	
	private int longitud;
		
	public int getLongitud() {
		return longitud;
	}
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	public String getDscAreaIntNiv2() {
		return dscAreaIntNiv2;
	}
	public void setDscAreaIntNiv2(String dscAreaIntNiv2) {
		this.dscAreaIntNiv2 = dscAreaIntNiv2;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodAreaIntNiv1() {
		return codAreaIntNiv1;
	}
	public void setCodAreaIntNiv1(String codAreaIntNiv1) {
		this.codAreaIntNiv1 = codAreaIntNiv1;
	}
	public List getListAreaIntNiv1() {
		return listAreaIntNiv1;
	}
	public void setListAreaIntNiv1(List listAreaIntNiv1) {
		this.listAreaIntNiv1 = listAreaIntNiv1;
	}
	public String getCodAreaIntNiv2() {
		return codAreaIntNiv2;
	}
	public void setCodAreaIntNiv2(String codAreaIntNiv2) {
		this.codAreaIntNiv2 = codAreaIntNiv2;
	}
	public List getListAreaIntNiv2() {
		return listAreaIntNiv2;
	}
	public void setListAreaIntNiv2(List listAreaIntNiv2) {
		this.listAreaIntNiv2 = listAreaIntNiv2;
	}	

	
	
}