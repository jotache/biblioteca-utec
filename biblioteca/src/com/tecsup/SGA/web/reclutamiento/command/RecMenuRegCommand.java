package com.tecsup.SGA.web.reclutamiento.command;

public class RecMenuRegCommand {
	
	private String operacion;
	private String idRec;
	private String lstrURL;
	private String nombre;
	private String opeMenu;
	private String codUsuario;//usuario de RRHH que modifica datos de un postulante 
	
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getOpeMenu() {
		return opeMenu;
	}
	public void setOpeMenu(String opeMenu) {
		this.opeMenu = opeMenu;
	}
	
}
