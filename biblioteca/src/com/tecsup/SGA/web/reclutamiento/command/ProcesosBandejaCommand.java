package com.tecsup.SGA.web.reclutamiento.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class ProcesosBandejaCommand {
	private String operacion;
	private String flagEtapa;
	private String codPeriodo;
	private String codUsuario;
	private String msg;
	private String unidadFuncional;
	private List listaUnidadFuncional;
	private String fecIni;
	private String fecFin;
	private List listaProcesos;
	private String codProcesosSeleccionados;
	
	
	public String getFlagEtapa() {
		return flagEtapa;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public void setFlagEtapa(String flagEtapa) {
		this.flagEtapa = flagEtapa;
	}
	public List getListaUnidadFuncional() {
		return listaUnidadFuncional;
	}
	public void setListaUnidadFuncional(List listaUnidadFuncional) {
		this.listaUnidadFuncional = listaUnidadFuncional;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUnidadFuncional() {
		return unidadFuncional;
	}
	public void setUnidadFuncional(String unidadFuncional) {
		this.unidadFuncional = unidadFuncional;
	}
	public String getFecIni() {
		return fecIni;
	}
	public void setFecIni(String fecIni) {
		this.fecIni = fecIni;
	}
	public String getFecFin() {
		return fecFin;
	}
	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}
	public List getListaProcesos() {
		return listaProcesos;
	}
	public void setListaProcesos(List listaProcesos) {
		this.listaProcesos = listaProcesos;
	}
	public String getCodProcesosSeleccionados() {
		return codProcesosSeleccionados;
	}
	public void setCodProcesosSeleccionados(String codProcesosSeleccionados) {
		this.codProcesosSeleccionados = codProcesosSeleccionados;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
	
}
