package com.tecsup.SGA.web.reclutamiento.command;

import java.util.List;

public class ConsultasyReportesCommand {
	private String operacion;
	private String usuario;
	private String tipoProceso;
	private List listProcesos;
	private List listUniFuncional;
	private String unidadFuncional;
	private String nomProceso;
	private String fechaInicio;
	private String fechaFin;
	private List listReportes;
	private String codReporte;
	private String nom;
	private String apePaterno;
	private String apeMaterno; 
	private String tipoReporte;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getApePaterno() {
		return apePaterno;
	}
	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}
	public String getApeMaterno() {
		return apeMaterno;
	}
	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}
	public List getListReportes() {
		return listReportes;
	}
	public void setListReportes(List listReportes) {
		this.listReportes = listReportes;
	}
	public String getCodReporte() {
		return codReporte;
	}
	public void setCodReporte(String codReporte) {
		this.codReporte = codReporte;
	}
	public String getTipoProceso() {
		return tipoProceso;
	}
	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}
	public List getListProcesos() {
		return listProcesos;
	}
	public void setListProcesos(List listProcesos) {
		this.listProcesos = listProcesos;
	}
	public List getListUniFuncional() {
		return listUniFuncional;
	}
	public void setListUniFuncional(List listUniFuncional) {
		this.listUniFuncional = listUniFuncional;
	}
	public String getUnidadFuncional() {
		return unidadFuncional;
	}
	public void setUnidadFuncional(String unidadFuncional) {
		this.unidadFuncional = unidadFuncional;
	}
	public String getNomProceso() {
		return nomProceso;
	}
	public void setNomProceso(String nomProceso) {
		this.nomProceso = nomProceso;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTipoReporte() {
		return tipoReporte;
	}
	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
}
