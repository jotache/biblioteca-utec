package com.tecsup.SGA.web.reclutamiento.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class ExperienciaCommand {
	
	private String operacion;
	private String typeMessage;
	private String message;
	private String idRec;
	
	/*Fecha*/
	private String fecha;
	
	private String cadenaExp;
	
	private String codExpLab1;
	private String codExpLab2;
	private String codExpLab3;
	
	private String organizacion1;
	private String organizacion2;
	private String organizacion3;
	
	private String puesto1;
	private List listaPuesto1;
	private String puesto2;
	private List listaPuesto2;
	private String puesto3;
	private List listaPuesto3;
	
	private String otroPuesto1;
	private String otroPuesto2;
	private String otroPuesto3;
	
	private String fechaInicio1;
	private String fechaInicio2;
	private String fechaInicio3;
	
	private String fechaFin1;
	private String fechaFin2;
	private String fechaFin3;
	
	private String personaRef1;
	private String personaRef2;
	private String personaRef3;
	
	private String telefonoRef1;
	private String telefonoRef2;
	private String telefonoRef3;
	
	private String desPuesto1; //descripcion actividades del puesto
	private String desPuesto2;
	private String desPuesto3;
	private String codUsuario;
	
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getOrganizacion1() {
		return organizacion1;
	}
	public void setOrganizacion1(String organizacion1) {
		this.organizacion1 = organizacion1;
	}
	public String getOrganizacion2() {
		return organizacion2;
	}
	public void setOrganizacion2(String organizacion2) {
		this.organizacion2 = organizacion2;
	}
	public String getOrganizacion3() {
		return organizacion3;
	}
	public void setOrganizacion3(String organizacion3) {
		this.organizacion3 = organizacion3;
	}
	public String getPuesto1() {
		return puesto1;
	}
	public void setPuesto1(String puesto1) {
		this.puesto1 = puesto1;
	}
	public List getListaPuesto1() {
		return listaPuesto1;
	}
	public void setListaPuesto1(List listaPuesto1) {
		this.listaPuesto1 = listaPuesto1;
	}
	public String getPuesto2() {
		return puesto2;
	}
	public void setPuesto2(String puesto2) {
		this.puesto2 = puesto2;
	}
	public List getListaPuesto2() {
		return listaPuesto2;
	}
	public void setListaPuesto2(List listaPuesto2) {
		this.listaPuesto2 = listaPuesto2;
	}
	public String getPuesto3() {
		return puesto3;
	}
	public void setPuesto3(String puesto3) {
		this.puesto3 = puesto3;
	}
	public List getListaPuesto3() {
		return listaPuesto3;
	}
	public void setListaPuesto3(List listaPuesto3) {
		this.listaPuesto3 = listaPuesto3;
	}
	public String getOtroPuesto1() {
		return otroPuesto1;
	}
	public void setOtroPuesto1(String otroPuesto1) {
		this.otroPuesto1 = otroPuesto1;
	}
	public String getOtroPuesto2() {
		return otroPuesto2;
	}
	public void setOtroPuesto2(String otroPuesto2) {
		this.otroPuesto2 = otroPuesto2;
	}
	public String getOtroPuesto3() {
		return otroPuesto3;
	}
	public void setOtroPuesto3(String otroPuesto3) {
		this.otroPuesto3 = otroPuesto3;
	}
	public String getFechaInicio1() {
		return fechaInicio1;
	}
	public void setFechaInicio1(String fechaInicio1) {
		this.fechaInicio1 = fechaInicio1;
	}
	public String getFechaInicio2() {
		return fechaInicio2;
	}
	public void setFechaInicio2(String fechaInicio2) {
		this.fechaInicio2 = fechaInicio2;
	}
	public String getFechaInicio3() {
		return fechaInicio3;
	}
	public void setFechaInicio3(String fechaInicio3) {
		this.fechaInicio3 = fechaInicio3;
	}
	public String getFechaFin1() {
		return fechaFin1;
	}
	public void setFechaFin1(String fechaFin1) {
		this.fechaFin1 = fechaFin1;
	}
	public String getFechaFin2() {
		return fechaFin2;
	}
	public void setFechaFin2(String fechaFin2) {
		this.fechaFin2 = fechaFin2;
	}
	public String getFechaFin3() {
		return fechaFin3;
	}
	public void setFechaFin3(String fechaFin3) {
		this.fechaFin3 = fechaFin3;
	}
	public String getPersonaRef1() {
		return personaRef1;
	}
	public void setPersonaRef1(String personaRef1) {
		this.personaRef1 = personaRef1;
	}
	public String getPersonaRef2() {
		return personaRef2;
	}
	public void setPersonaRef2(String personaRef2) {
		this.personaRef2 = personaRef2;
	}
	public String getPersonaRef3() {
		return personaRef3;
	}
	public void setPersonaRef3(String personaRef3) {
		this.personaRef3 = personaRef3;
	}
	public String getTelefonoRef1() {
		return telefonoRef1;
	}
	public void setTelefonoRef1(String telefonoRef1) {
		this.telefonoRef1 = telefonoRef1;
	}
	public String getTelefonoRef2() {
		return telefonoRef2;
	}
	public void setTelefonoRef2(String telefonoRef2) {
		this.telefonoRef2 = telefonoRef2;
	}
	public String getTelefonoRef3() {
		return telefonoRef3;
	}
	public void setTelefonoRef3(String telefonoRef3) {
		this.telefonoRef3 = telefonoRef3;
	}
	public String getDesPuesto1() {
		return desPuesto1;
	}
	public void setDesPuesto1(String desPuesto1) {
		this.desPuesto1 = desPuesto1;
	}
	public String getDesPuesto2() {
		return desPuesto2;
	}
	public void setDesPuesto2(String desPuesto2) {
		this.desPuesto2 = desPuesto2;
	}
	public String getDesPuesto3() {
		return desPuesto3;
	}
	public void setDesPuesto3(String desPuesto3) {
		this.desPuesto3 = desPuesto3;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getCodExpLab1() {
		return codExpLab1;
	}
	public void setCodExpLab1(String codExpLab1) {
		this.codExpLab1 = codExpLab1;
	}
	public String getCodExpLab2() {
		return codExpLab2;
	}
	public void setCodExpLab2(String codExpLab2) {
		this.codExpLab2 = codExpLab2;
	}
	public String getCodExpLab3() {
		return codExpLab3;
	}
	public void setCodExpLab3(String codExpLab3) {
		this.codExpLab3 = codExpLab3;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCadenaExp() {
		return cadenaExp;
	}
	public void setCadenaExp(String cadenaExp) {
		this.cadenaExp = cadenaExp;
	}
}
