package com.tecsup.SGA.web.reclutamiento.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class MtoConfInicioCommand {

	/****************	ATRIBUTOS	*******************/
	private long idOrden;
	private String descripcion;
	private String operacion;
	private String cboOrden;
	private List MtoConfInicioList;
	private String IdRec;
	private String lstrURL;
	private String codEval;
	private String codPeriodo;

public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
public long getIdOrden() {
	return idOrden;
}
public void setIdOrden(long idOrden) {
	this.idOrden = idOrden;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getOperacion() {
	return operacion;
}
public void setOperacion(String operacion) {
	this.operacion = operacion;
}
public String getCboOrden() {
	return cboOrden;
}
public void setCboOrden(String cboOrden) {
	this.cboOrden = cboOrden;
}
public List getMtoConfInicioList() {
	return MtoConfInicioList;
}
public void setMtoConfInicioList(List mtoConfInicioList) {
	MtoConfInicioList = mtoConfInicioList;
}
public String getIdRec() {
	return IdRec;
}
public void setIdRec(String idRec) {
	IdRec = idRec;
}
public String getLstrURL() {
	return lstrURL;
}
public void setLstrURL(String lstrURL) {
	this.lstrURL = lstrURL;
} 

	
}
