package com.tecsup.SGA.web.reclutamiento.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class TipoEvaluacionCommand {
	
	private String ListaDenominacion;
	private String operacion;
	private String tiposEvaluacion;
	private List listTipoEvaluacionRevision;
	private List listTipoEvaluacionSeleccion;
	private List listTipos;
	private String etapa;
	private String posicion;
	private String descripcion;
	private String dscValor1;
	private String dscValor2;
	private List listConsulta;
	private String codSelTipo;
	private String cantidad;
	private String cantidad1;
	private String codDetalle;
	private String msg;
	private String msg2;
	private int cant;
	private String codEval;
	private int can;
	private String eta;
	
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public int getCant() {
		return cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	public String getDscValor1() {
		return dscValor1;
	}
	public void setDscValor1(String dscValor1) {
		this.dscValor1 = dscValor1;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public List getListTipos() {
		return listTipos;
	}
	public void setListTipos(List listTipos) {
		this.listTipos = listTipos;
	}
	public String getTiposEvaluacion() {
		return tiposEvaluacion;
	}
	public void setTiposEvaluacion(String tiposEvaluacion) {
		this.tiposEvaluacion = tiposEvaluacion;
	}
	public String getListaDenominacion() {
		return ListaDenominacion;
	}
	public void setListaDenominacion(String listaDenominacion) {
		ListaDenominacion = listaDenominacion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	

	
	public List getListTipoEvaluacionRevision() {
		return listTipoEvaluacionRevision;
	}
	public void setListTipoEvaluacionRevision(List listTipoEvaluacionRevision) {
		this.listTipoEvaluacionRevision = listTipoEvaluacionRevision;
	}
	public List getListTipoEvaluacionSeleccion() {
		return listTipoEvaluacionSeleccion;
	}
	public void setListTipoEvaluacionSeleccion(List listTipoEvaluacionSeleccion) {
		this.listTipoEvaluacionSeleccion = listTipoEvaluacionSeleccion;
	}
	public String getEtapa() {
		return etapa;
	}
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDscValor2() {
		return dscValor2;
	}
	public void setDscValor2(String dscValor2) {
		this.dscValor2 = dscValor2;
	}
	public List getListConsulta() {
		return listConsulta;
	}
	public void setListConsulta(List listConsulta) {
		this.listConsulta = listConsulta;
	}
	public String getCodSelTipo() {
		return codSelTipo;
	}
	public void setCodSelTipo(String codSelTipo) {
		this.codSelTipo = codSelTipo;
	}
	public String getCantidad1() {
		return cantidad1;
	}
	public void setCantidad1(String cantidad1) {
		this.cantidad1 = cantidad1;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getMsg2() {
		return msg2;
	}
	public void setMsg2(String msg2) {
		this.msg2 = msg2;
	}
	public int getCan() {
		return can;
	}
	public void setCan(int can) {
		this.can = can;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	
	 


	
}
