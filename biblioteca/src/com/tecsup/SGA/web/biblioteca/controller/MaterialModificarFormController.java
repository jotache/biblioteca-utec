package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.DescriptoresModificarCommand;


public class MaterialModificarFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(MaterialModificarFormController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	   	
    	DescriptoresModificarCommand command = new DescriptoresModificarCommand();
    
    	String codDetalle = (String)request.getParameter("codigo");
    	String dscDetalle = (String)request.getParameter("descripcion");
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	DescriptoresModificarCommand control = (DescriptoresModificarCommand) command;

    	
    	control.setOperacion("");
				
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_descriptores_modificar","control",control);		
    }
    
   
}
