package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialRegistrarIngresoCommand;


public class AdminRegistrarMaterialRegistrarIngresoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialRegistrarIngresoFormController.class);
	
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
    public AdminRegistrarMaterialRegistrarIngresoFormController() {
        super();        
        setCommandClass(AdminRegistrarMaterialRegistrarIngresoCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)	
    throws ServletException{
    	//log.info("formBackingObject:INI"); 
    	AdminRegistrarMaterialRegistrarIngresoCommand control = new AdminRegistrarMaterialRegistrarIngresoCommand();
		if(control.getOperacion()==null){
			control.setUsuario(request.getParameter("txhCodUsuario"));
			control.setTxhCodigoMaterial(request.getParameter("txhCodUnico"));
			control.setTxhCodigoUnico(request.getParameter("txhCodigoIngreso"));
			control = ini(control);
			control.setCboEstado("0001");//ACTIVO POR DEFECTO
			
			if(request.getParameter("flgModificarIngreso")!=null){
				control = getIngreso(control);
				control.setFlgUpdate("true");
			}
		}
        //log.info("formBackingObject:FIN");
        return control;
   }
	
	private AdminRegistrarMaterialRegistrarIngresoCommand getIngreso(
			AdminRegistrarMaterialRegistrarIngresoCommand control) {
		//log.info("getIngreso:INI");
		control = biblioGestionMaterialManager.getIngresoxMaterial(control);
		//log.info("getIngreso:FIN");
		return control;
	}

	private AdminRegistrarMaterialRegistrarIngresoCommand ini(
			AdminRegistrarMaterialRegistrarIngresoCommand control) {
		control = iniciaProcedencia(control);
		control = iniciaMoneda(control);
		return control;
	}

	private AdminRegistrarMaterialRegistrarIngresoCommand iniciaMoneda(
			AdminRegistrarMaterialRegistrarIngresoCommand control) {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MONEDA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstMoneda(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		return control;
	}

	private AdminRegistrarMaterialRegistrarIngresoCommand iniciaProcedencia(
			AdminRegistrarMaterialRegistrarIngresoCommand control) {

    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_PROCEDENCIA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstProcedencia(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		return control;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	String resp = "";

    	AdminRegistrarMaterialRegistrarIngresoCommand control = (AdminRegistrarMaterialRegistrarIngresoCommand) command;
   	

    	
    	log.info("OPERACION:"+control.getOperacion());
    	if("irConsultar".equalsIgnoreCase(control.getOperacion())){
    		//control = irConsultar(control);    		
    	}else if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
    		/*log.info("getTxhCodigoMaterial()|"+control.getTxhCodigoMaterial()+"|");
    		log.info("control.getTxtFecIngreso()|"+control.getTxtFecIngreso()+"|");
    		//log.info("control.getTxtFecBaja()|"+control.getTxtFecBaja()+"|");
    		log.info("control.getNroVolumen()|"+control.getNroVolumen()+"|");    		
    		log.info("control.getCboProcedencia()|"+control.getCboProcedencia()+"|");
    		log.info("control.getCboMoneda()|"+control.getCboMoneda()+"|");
    		log.info("control.getTxtPrecio()|"+control.getTxtPrecio()+"|");
    		log.info("control.getCboEstado()|"+control.getCboEstado()+"|");
    		log.info("control.getTxtObservacion()|"+control.getTxtObservacion()+"|");
    		log.info("control.getUsuario()|"+control.getUsuario()+"|");
    		*/
    		resp = biblioGestionMaterialManager.insertIngresosxMaterial(
    		control.getTxhCodigoMaterial(),
    		control.getTxtFecIngreso(),
    		//control.getTxtFecBaja(),
    		control.getNroVolumen(),
    		control.getCboProcedencia(),
    		control.getCboMoneda(),
    		control.getTxtPrecio(),
    		control.getCboEstado(),
    		control.getTxtObservacion(),
    		control.getUsuario()
    		);
    		
    		log.info("RESP:"+resp);
    		
    		if("0".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
    			request.setAttribute("refresh","1");
    		}else{
    			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    		}
    	}else if("irActualizar".equalsIgnoreCase(control.getOperacion())){
    		//log.info("PARAMETROS PARA EL ACTUALIZAR");
    		//log.info("control.getTxhCodigoUnico()|"+control.getTxhCodigoUnico()+"|");
    		//log.info("control.getTxtFecIngreso()|"+control.getTxtFecIngreso()+"|");
    		//log.info("control.getTxtFecBaja()|"+control.getTxtFecBaja()+"|");
    		//log.info("control.getNroVolumen()|"+control.getNroVolumen()+"|");
    		
    		
    		/*log.info("control.getCboProcedencia()|"+control.getCboProcedencia()+"|");
    		log.info("control.getCboMoneda()|"+control.getCboMoneda()+"|");
    		log.info("control.getTxtPrecio()|"+control.getTxtPrecio()+"|");
    		log.info("control.getCboEstado()|"+control.getCboEstado()+"|");
    		log.info("control.getTxtObservacion()|"+control.getTxtObservacion()+"|");
    		log.info("control.getUsuario()|"+control.getUsuario()+"|");
    		*/
    		resp = biblioGestionMaterialManager.updateIngresosxMaterial(
    				control.getTxhCodigoUnico(),
    				control.getTxtFecIngreso(),
    				//control.getTxtFecBaja(),
    				control.getNroVolumen(),
    				control.getCboProcedencia(),
    				control.getCboMoneda(),
    				control.getTxtPrecio(),
    				control.getCboEstado(),
    				control.getTxtObservacion(),
    				control.getUsuario()
    			);
    		log.info("RPTA:"+resp);
    		if("0".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
    			request.setAttribute("refresh","1");
    		}else{
    			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    		}
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_registrar_ingreso","control",control);		
    }

}
