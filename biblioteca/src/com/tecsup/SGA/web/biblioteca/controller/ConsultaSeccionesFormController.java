package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaSeccionesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaSeccionesFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {		
		
		ConsultaSeccionesCommand command = new ConsultaSeccionesCommand();
		command.setCodNovedades(CommonConstants.COD_SECCION_NOVEDADES);
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());
    	
    	
		cargaCabecera(command);	
		if(command.getOperacion()==null){
			
			request.getSession().removeAttribute("listaBandejaSeccion");
			
		}
				
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	ConsultaSeccionesCommand control = (ConsultaSeccionesCommand) command;    	
    	String resultado="";    
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR")){    		
    		cargaBandeja(control, request);
    	}
    	else
		if (control.getOperacion().trim().equals("QUITAR")){			
    		resultado = DeleteSeccion(control);    		
    		log.info("RPTA:" + resultado);
    		cargaBandeja(control,request);
    	}
    	
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_secciones","control",control);		
    }
    public void cargaBandeja(ConsultaSeccionesCommand control, HttpServletRequest request){
    	
    	Secciones secciones= new Secciones();
    	secciones.setCodUnico("");
    	    	
    	secciones.setCodSeccion(control.getCodSeccion());
    	secciones.setCodGrupo(control.getCodGrupo());
    	secciones.setCodTipo(control.getOpcion());    	

    	List busqueda= new ArrayList();
    	busqueda=this.biblioMantenimientoManager.getSeccionesByFiltros(secciones);   	
    	
    	request.getSession().setAttribute("listaBandejaSeccion", busqueda);
    	request.getSession().setAttribute("codSeccion", control.getCodSeccion());
    	
    }
    public void cargaCabecera(ConsultaSeccionesCommand control){
    	cargaListaSeccion(control);
    	cargaListaGrupos(control);    	
    }
    public String DeleteSeccion(ConsultaSeccionesCommand control){
    	Secciones seccion = new Secciones();
		
		seccion.setCodUnico(control.getCodUnico());
		seccion.setCodSeccion("");
		seccion.setTipoMaterial("");
		seccion.setCodGrupo("");
		seccion.setTema("");
		seccion.setDescripcionCorta("");
		seccion.setDescripcionLarga("");
		seccion.setUrl("");
		seccion.setArchivo("");
		seccion.setFechaPublicacion("");
		seccion.setFechaVigencia("");
		seccion.setFlag("");
		seccion.setNomAdjunto("");
		
		String resultado="";
		//campo usuario 8 / campo tipo vacio "insertar" "modificar"
		resultado=this.biblioMantenimientoManager.insertSecciones(seccion, control.getCodUsuario(), "1");
		
		return resultado;    	
    }
    private void cargaListaSeccion(ConsultaSeccionesCommand control){
	    try{
	    	TipoTablaDetalle obj = new TipoTablaDetalle();
	    	
	    	obj.setDscValor3("");
	    	obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SECCION);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_COD);
	    	
	    	control.setListaSeccion(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
		}
    }
	private void cargaListaGrupos(ConsultaSeccionesCommand control){
		try{
    		TipoTablaDetalle obj1= new TipoTablaDetalle();
    		
   		    obj1.setDscValor3("");
   		    obj1.setCodTipoTabla("");
   		    obj1.setCodTipoTablaDetalle(CommonConstants.TIPT_GRUPO_SECCION);
   			obj1.setCodDetalle("");
   			obj1.setDescripcion("");
   			obj1.setTipo(CommonConstants.TIPO_ORDEN_DSC);   			
   			
   			control.setListaGrupo(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj1));
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
	}
}
