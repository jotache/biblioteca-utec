package com.tecsup.SGA.web.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.bean.FechaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.web.biblioteca.command.AtencionCommand;

public class AtencionFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(AtencionFormController.class);
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private ComunManager comunManager;
	
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	public void setBiblioMantenimientoManager(BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	public void setPortalWebBibliotecaManager(PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		
		AtencionCommand control = new AtencionCommand();
		
		//log.info("formBackingObject Start");
		//log.info("Accion:" + control.getAccion());
		//listar los recursos (pcs) segun sede
		String sSede = (String)request.getParameter("sede");
				
		List<Sala> listaHoras = new ArrayList<Sala>();
		
		listaHoras = portalWebBibliotecaManager.ListarHorasAtencionPorSede(sSede);
		
		String fechas=Fecha.getFechaActual();
		
		FechaBean fechaActual = comunManager.getFecha();
		control.setHoraActual(fechaActual.getHora());
		control.setMinutoActual(fechaActual.getMinuto());
		control.setSegundoActual(fechaActual.getSegundo());
		
		control.setFechaAtencion(fechas);
		control.setSede(sSede);
		
		request.setAttribute("listaHoras", listaHoras);
		
		if (control.getAccion().equals("inicio")){
			//log.info("Buscando en formBacking:"+control.getAccion());
			control.setCodTipoSala("0");
			listarRecursos(control);
		}
		
		listarComputadoras(control);
		
		ComboConsultaTipoSalas(control);
		
		//log.info("formBackingObject End");
		
		return control;
	}

	
	protected ModelAndView onSubmit(HttpServletRequest request,	HttpServletResponse response, Object command, BindException errors) throws Exception {
		
		AtencionCommand control = (AtencionCommand) command;
		
		log.info("OPERACION:"+ control.getAccion());
		
		if (control.getAccion().equals("consultar")){
			
			List<Sala> listaRecursos = new ArrayList<Sala>();
			
			//log.info("fecha:" + control.getFechaAtencion());
			//log.info("sede:" + control.getSede());
			
			listaRecursos = portalWebBibliotecaManager.ListarRecursosAtencion(control.getFechaAtencion(), control.getSede(), control.getCodTipoSala());
			
			//log.info("listaRecursos tama�o:" + listaRecursos.size());
			
			control.setListaRecursos(listaRecursos);
			
			muestraRangos(control);
			//request.setAttribute("listaRecursos", listaRecursos);
			//request.getSession().setAttribute("listaRecursos", listaRecursos);
		}else if(control.getAccion().equals("listarTipoSala")){
			
			List<Sala> listaRecursos = new ArrayList<Sala>();
			listaRecursos = portalWebBibliotecaManager.ListarRecursosAtencion(control.getFechaAtencion(), control.getSede(), control.getCodTipoSala());
			control.setListaRecursos(listaRecursos);
			muestraRangos(control);
			
			
		}else if (control.getAccion().equals("buscarUsuario")){
			
			AlumnoXCodigoBean usuario = this.portalWebBibliotecaManager.obtenerUsuarioAtencion(control.getUsuario());
			
			if (usuario!=null){
				
				control.setNomUsuario(usuario.getNombre());
				control.setCodUsuario(usuario.getCodUsuario());
				
				if (control.getCodUsuario()==null)
					request.setAttribute("mensaje", "Usuario no existe. Intente nuevamente");				
				
			}else
				request.setAttribute("mensaje", "Usuario no existe. Intente nuevamente");
			
			//log.info("control.getNomUsuario():" + control.getNomUsuario());
			
			/*List<Sala> listaRecursos = new ArrayList<Sala>();
			listaRecursos = portalWebBibliotecaManager.ListarRecursosAtencion(control.getFechaAtencion(), control.getSede());
			control.setListaRecursos(listaRecursos);
			*/
			
			listarRecursos(control);
						
			//log.info("control lista recursos :" + control.getListaRecursos().size());
			
		}else if (control.getAccion().equals("grabar")){
						
			if(request.getSession().getAttribute("usuarioSeguridad")!=null){
				
				String codRecurso = control.getCodPc();			
				String codUsuarioActualiza = "";
				UsuarioSeguridad user = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
				codUsuarioActualiza = user.getIdUsuario(); //Usuario que registra
				
				String codUsuarioAtencion = control.getCodUsuario();//Usuario de atenci�n
				String fecha = control.getFechaAtencion();
				String horaIni = "";//hora inicial generada en la BD (hora actual HH:mm) //Fecha.getHoraActualHH();
				 
				String horaFin = control.getHoraFin();
				
				//log.info("codRecurso:" + codRecurso);
				//log.info("codUsuarioAct:" + codUsuarioActualiza);
				//log.info("codUsuario:" + codUsuarioAtencion);
				//log.info("fecha:" + fecha);
				//log.info("horaIni:" + horaIni);
				//log.info("horaFin:" + horaFin);
				
				String resp = portalWebBibliotecaManager.insertarAtencion(codRecurso, codUsuarioAtencion, fecha, horaIni, horaFin,codUsuarioActualiza,control.getSede());
				log.info("RPTA:" + resp);
				if (resp.equals("0")){
					control.setTipoMensaje("OK");
					control.setMensaje("Atenci�n registrada satisfactoriamente");
					listarRecursos(control);
				}else if(resp.equals("1")){
					control.setTipoMensaje("ERROR");
					control.setMensaje("El usuario tiene una atenci�n activa");
				}else if(resp.equals("2")){
					control.setTipoMensaje("ERROR");
					control.setMensaje("Existe un cruce de horas con alguna Reserva");
				}else if(resp.equals("3")){
					control.setTipoMensaje("ERROR");
					control.setMensaje("El Recurso est� ocupado");
				}
			}else{
				request.setAttribute("mensaje","Debe Iniciar Sesi�n.");
			}
			
		}
		
		//log.info("finaliza onSubmit");
		
		return new ModelAndView("/biblioteca/userAdmin/atencion","control",control);
	}
	
	private void listarRecursos(AtencionCommand control){
		
		//log.info("Listando recursos");
		
		List<Sala> listaRecursos = new ArrayList<Sala>();
		
		//log.info("fecha:" + control.getFechaAtencion());
		//log.info("sede:" + control.getSede());
		
		listaRecursos = portalWebBibliotecaManager.ListarRecursosAtencion(control.getFechaAtencion(), control.getSede(), control.getCodTipoSala());
		
		//log.info("listaRecursos tama�o:" + listaRecursos.size());
		
		control.setListaRecursos(listaRecursos);
		
		muestraRangos(control);
	}

	private void muestraRangos(AtencionCommand control){
		
		List<Sala> listaAtencionesPorDia = new ArrayList<Sala>();
		
		listaAtencionesPorDia = this.portalWebBibliotecaManager.listarAtenciones(control.getFechaAtencion(), control.getSede());
		
		if (listaAtencionesPorDia.size()>0){
			String rango = "";
			for (Sala atencion : listaAtencionesPorDia)
				rango += atencion.getCodSala() + "|" + atencion.getIdHoraIni() + "|" + 
					atencion.getIdHoraFin() + "|" + atencion.getCodEstado() + "|," ;
			
			control.setRangos(rango);
			//log.info("Rangos obtenidos:" + rango);
		}else
			control.setRangos("");
		
		//control.setRangos("0007|08|10|,0007|09|11|,0009|08|12|,0010|08|XX|,0012|10|13|,");
	}
	private void listarComputadoras(AtencionCommand control){
		
		TipoTablaDetalle objTablas = new TipoTablaDetalle();
		
		objTablas.setDscValor3(CommonConstants.TIPT_TIPO_SALA);    	
		//objTablas.setCodTipoTabla("0002");
		objTablas.setCodTipoTabla("");//Ahora muestro pc's y salas
		objTablas.setCodTipoTablaDetalle(CommonConstants.TIPT_SALA);
		objTablas.setCodDetalle("");
		objTablas.setDescripcion("");
		objTablas.setDscValor6(control.getSede());
		objTablas.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		
		control.setListaComputadoras(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(objTablas));
	}
	
	private void ComboConsultaTipoSalas(AtencionCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();    	
    	obj.setDscValor3("");
    	obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SALA);
		obj.setCodDetalle("");
		obj.setDescripcion("");		
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);    	
    	control.setCodListaTipoSala(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));    	
    }
}
