package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaCiudadFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaCiudadFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		
		ConsultaCiudadCommand command = new ConsultaCiudadCommand();							
		//******************************************************************
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));    	
    	//System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
		//******************************************************************
    	if(command.getOperacion()==null){		
			request.getSession().removeAttribute("listaCiudad");			
		}
		cargaCabecera(command);
		
		//log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	ConsultaCiudadCommand control = (ConsultaCiudadCommand) command;
    	
    	log.info("OPERACION:"+control.getOperacion()); 
    	if (control.getOperacion().trim().equals("BUSCAR")){
    		
    		cargaBandeja(control);
    		request.getSession().setAttribute("listaCiudad", control.getListaCiudad());
    	}
    	
    	//log.info("onSubmit:INI");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_ciudad","control",control);		
    }
    public void cargaBandeja(ConsultaCiudadCommand control){    	
    	Ciudad obj = new Ciudad();
    	
    	if(control.getPais()!=""){
    		
    		System.out.println(">>"+control.getPais()+"<<");
    		obj.setPaisId(control.getPais());
    		obj.setCiudadCodigo("");
    		obj.setCiudadDescripcion(control.getDscCiudad());
    		
    		control.setListaCiudad(this.biblioMantenimientoManager.getCiudadByPais(obj, CommonConstants.TIPO_ORDEN_DSC));
    	}
    }
    public void cargaCabecera(ConsultaCiudadCommand control){
    	Pais obj = new Pais();
    	
    	obj.setPaisCodigo("");
    	obj.setPaisDescripcion("");
    	obj.setPaisId("");
    	
    	control.setListaPais(this.biblioMantenimientoManager.getAllPaises(obj, CommonConstants.TIPO_ORDEN_DSC));
    }    
}
