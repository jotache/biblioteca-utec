package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarPrestamoFlotanteCommand;

public class AdminRegistrarPrestamoFlotanteFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AdminRegistrarPrestamoFlotanteFormController.class);
	
private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI"); 
    	
    	AdminRegistrarPrestamoFlotanteCommand command= new AdminRegistrarPrestamoFlotanteCommand();
    	
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));        
        command.setDscUsuario((String)request.getParameter("txhDscUsuario"));
        command.setMatNroIngreso(request.getParameter("txhNroIngreso"));
        command.setMatTitulo(request.getParameter("txhTitulo"));
        command.setMatTitulo(request.getParameter("txhTitulo"));
        command.setUsuarioLogin(request.getParameter("txhUsuarioLogin"));
    	
        //command.setSedeSel(request.getParameter("txhSedeSel"));
        //command.setSedeUsuario(request.getParameter("txhSedeUsuario"));
        
        command.setFechaBD(Fecha.getFechaActual());
    	BuscarBandeja(command, request);
    	
        //log.info("formBackingObject:FIN");
        return command;
   }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	
    	AdminRegistrarPrestamoFlotanteCommand control= (AdminRegistrarPrestamoFlotanteCommand) command;
    	    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_prestamo_flotante","control",control);		
    }
    
    public void BuscarBandeja(AdminRegistrarPrestamoFlotanteCommand control, HttpServletRequest request){
    	
    	List lista= new ArrayList();
    	//log.info("control.getCodUsuario()>>"+control.getCodUsuario()+">>");
    	//log.info("control.getMatNroIngreso()>>"+control.getMatNroIngreso()+">>");
    	
    	lista=this.biblioGestionMaterialManager.
    	GetAllReservaByAlumno(control.getCodUsuario(),control.getMatNroIngreso());
    	if(lista!=null)
    	request.getSession().setAttribute("listaBandeja", lista);
    	else request.getSession().setAttribute("listaBandeja", new ArrayList());
    }
}
