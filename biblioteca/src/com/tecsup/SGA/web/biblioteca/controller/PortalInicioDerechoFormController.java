package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.PortalInicioDerechoCommand;

public class PortalInicioDerechoFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PortalInicioDerechoFormController.class);
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
		
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {		
		
		PortalInicioDerechoCommand command = new PortalInicioDerechoCommand();
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
		VerHoraAtencion(command);
		VerEnlacesInteres(command, request);
		VerBibliotecas(command, request);
		VerOtrasFuentes(command, request);
		VerReglamento(command, request);
		
    	return command;
	}
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {    	
    	
    	PortalInicioDerechoCommand control=(PortalInicioDerechoCommand) command;
    	    	
    	return new ModelAndView("/biblioteca/userAdmin/buzonSugerencias/bib_bandeja_buzon_sugerencias","control",control);
    	
    }
    
    public void VerHoraAtencion(PortalInicioDerechoCommand control){
    	Secciones seccion= new Secciones();
    	seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_HORARIO_ATENCION);
    	TipoTablaDetalle obj= new TipoTablaDetalle();
    	obj.setCodTipoTablaDetalle("");
    	    	
		List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
		Secciones seccion1= new Secciones();
		if(lista!=null){
			if(lista.size()>0){	
				seccion1=(Secciones)lista.get(0);	
				control.setHoraAtencion(seccion1.getDescripcionCorta());
				control.setHorario("HORARIO DE ATENCI�N");
				
				}
			else{ control.setHoraAtencion("");
				  control.setHorario(" ");}
			}
		else
			{lista=new ArrayList();
		    control.setHoraAtencion("");}
    	
    }
		
   public void VerEnlacesInteres(PortalInicioDerechoCommand control, HttpServletRequest request) 
   {
	   Secciones seccion= new Secciones();
   		seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_ENLACES_INTERES);
   		TipoTablaDetalle obj= new TipoTablaDetalle();
   			
   		obj.setCodTipoTablaDetalle("");   
   		
   		List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
   		if(lista!=null)
   			control.setListaEnlaces(lista);
   		else
   			control.setListaEnlaces(new ArrayList());
   		if(lista.size()>0)
   			control.setEnlacesInteres("1");
   		else control.setEnlacesInteres("0");
		request.getSession().setAttribute("listaEnlaces", control.getListaEnlaces());
	   
   }
   public void VerBibliotecas(PortalInicioDerechoCommand control, HttpServletRequest request)
   {
	   Secciones seccion= new Secciones();
	   seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_BIBLIOTECAS);
	   TipoTablaDetalle obj= new TipoTablaDetalle();
	   obj.setCodTipoTablaDetalle("");
	   List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
	   if(lista!=null)
		   control.setListaBibliotecas(lista);
	   else control.setListaBibliotecas(new ArrayList());
	   
	   if(lista.size()>0)
		   control.setBibliotecas("1");
	   else control.setBibliotecas("0");
	   
		request.getSession().setAttribute("listaBibliotecas", control.getListaBibliotecas());
	   
   }
   public void VerOtrasFuentes(PortalInicioDerechoCommand control, HttpServletRequest request)
   {
	   Secciones seccion= new Secciones();
	   seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_OTRAS_FUENTES_INFORMACION);
	   TipoTablaDetalle obj= new TipoTablaDetalle();
	   obj.setCodTipoTablaDetalle("");
	   List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
	   if(lista!=null)
	   control.setListaOtrasFuentes(lista);
	   else control.setListaOtrasFuentes(new ArrayList());
	   
	   if(lista.size()>0)
		   control.setOtrasFuentes("OTRAS FUENTES DE INFORMACION");
	   else control.setOtrasFuentes(" ");
		request.getSession().setAttribute("listaOtrasFuentesInformacion", control.getListaOtrasFuentes());
	   
   }
   
   public void VerReglamento(PortalInicioDerechoCommand control, HttpServletRequest request){
	   
	   Secciones seccion= new Secciones();
	   seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_REGLAMENTOS);
	   TipoTablaDetalle obj= new TipoTablaDetalle();
   	   obj.setCodTipoTablaDetalle("");
   	       
	   List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
	   if(lista!=null)
	    {  if(lista.size()>0) 
	    	control.setReglamento("REGLAMENTO");
	       else control.setReglamento(" ");
	     
	    }
	      
   }
}
