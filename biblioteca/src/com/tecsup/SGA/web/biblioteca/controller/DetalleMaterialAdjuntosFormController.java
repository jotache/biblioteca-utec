package com.tecsup.SGA.web.biblioteca.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;

public class DetalleMaterialAdjuntosFormController  implements Controller  {
	
	private static Log log = LogFactory.getLog(DetalleMaterialAdjuntosFormController.class);
	
	BiblioGestionMaterialManager biblioGestionMaterialManager;
	
    public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	log.info("handleRequest:INI");		    	    	
    	ModelMap model = new ModelMap();   	
    	
    	String txhCodigoUnico = request.getParameter("txhCodigoUnico");
    	
    	String uploadDirArchivo = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_DOCUMENTOS;
    	
    	request.setAttribute("msgRutaServer",uploadDirArchivo);
    	    	
    	log.info("CODIGO UNICO:"+txhCodigoUnico+":");
    	model.addObject("txhCodigoUnico", txhCodigoUnico);
    	model.addObject("lstDocumentosRelacionados",
    			biblioGestionMaterialManager.getAllArchAdjuntoxMaterial(txhCodigoUnico));
    	
	   	log.info("handleRequest:FIN");
		return new ModelAndView("/biblioteca/userWeb/bib_material_adjuntos", "model", model);
	}

}
