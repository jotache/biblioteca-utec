package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaCiudadPaginadoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaCiudadPaginadoFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		
		ConsultaCiudadPaginadoCommand command = new ConsultaCiudadPaginadoCommand();
		//******************************************************************
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));    	
    	//System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
		//******************************************************************
		//log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	ConsultaCiudadPaginadoCommand control = (ConsultaCiudadPaginadoCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	String resultado="";    	
		if (control.getOperacion().trim().equals("QUITAR")){
			//System.out.println("entra para quitar en paginado");
    		resultado = DeleteCiudad(control);
    		log.info("RPTA:"+resultado);
    		//System.out.println("este es el resultado de quitar->"+resultado);
    		if(resultado.equals("0")){
    			control.setMsg("OK");
    		}
    	}
		
		//log.info("onSubmit:INI");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_ciudad_paginado","control",control);		
    }    
    public String DeleteCiudad(ConsultaCiudadPaginadoCommand control){    	
    	try{
    		Ciudad obj = new Ciudad();
    		
    		String resultado="";
    		
    		obj.setPaisId("");
    		obj.setCiudadId(control.getCodigoSec());
    		obj.setCiudadDescripcion("");
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuCrea(control.getCodUsuario());
    		
    		/*System.out.println(">>codigo secuencial--cod usuario<<");
    		System.out.println(">>"+control.getCodigoSec()+"<<");
    		System.out.println(">>"+control.getCodUsuario()+"<<");*/
    		
    		resultado=this.biblioMantenimientoManager.updateDeleteCiudadbyPais(obj);    		
    		return resultado;
    		
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
}
