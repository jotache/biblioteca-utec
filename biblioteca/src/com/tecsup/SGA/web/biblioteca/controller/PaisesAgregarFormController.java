package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.EditorialesAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.PaisesAgregarCommand;


public class PaisesAgregarFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PaisesAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {    	   
    	
    	PaisesAgregarCommand command = new PaisesAgregarCommand();
    	
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	command.setCodDetalle((String)request.getParameter("txhCodigo"));
    	command.setCodigoPais("");
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	PaisesAgregarCommand control = (PaisesAgregarCommand) command;
    	
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if(control.getOperacion().trim().equals("GRABAR"))
    	{   
    		resultado = InsertPais(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    		      else
    		      control.setMsg("OK");
    		    }
    	}
    	control.setOperacion("");
			
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_paises_agregar","control",control);		
    }
    
    private String InsertPais(PaisesAgregarCommand control)
    {   	
    	try
    	{   
    		Pais pais= new Pais();
    		 String resultado="";   		
    		pais.setPaisDescripcion(control.getDescripcionPais().trim());
    		String usuario=control.getCodEvaluador();
    	    
    		resultado=this.biblioMantenimientoManager.InsertPaises(pais, usuario);
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
 }
