package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.IdiomasAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.VideosAgregarCommand;


public class VideosAgregarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(VideosAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    		
    	
    	VideosAgregarCommand command = new VideosAgregarCommand();		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());
    	    	
    	String operacion=((String)request.getParameter("txhOperacion")!=null?(String)request.getParameter("txhOperacion"):"");
		if(operacion.equalsIgnoreCase("MODIFICAR")){
			command.setCodigoSec((String)request.getParameter("txhCodigoSec"));
	    	command.setCodigo((String)request.getParameter("txhCodigoVideo"));
	    	command.setDscVideo((String)request.getParameter("txhDescripcion"));
	    	command.setOperacion(operacion);
	    		    	
		}
		else{
			command.setOperacion(operacion);
			
		}    	
        
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	VideosAgregarCommand control = (VideosAgregarCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("AGREGAR"))
    	{
    		resultado = InsertVideo(control);
    		log.info("RPTA:" + resultado);
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}
    	else
		if (control.getOperacion().trim().equals("MODIFICAR"))
    	{	
    		resultado = UpdateVideo(control);    		
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}   	
				
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_tipo_video_agregar","control",control);		
    }
    
    private String InsertVideo(VideosAgregarCommand control)
    {
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_VIDEO);
    		obj.setDescripcion(control.getDscVideo());
    		obj.setUsuario(control.getCodUsuario());
    		
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		
    		resultado=this.biblioMantenimientoManager.InsertTablaDetalleBiblioteca(obj);
    		return resultado;			 		
    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String UpdateVideo(VideosAgregarCommand control)
    {
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_VIDEO);
    		obj.setDscValor1(control.getCodigoSec());
    		obj.setDscValor2(control.getCodigo());
    		obj.setDescripcion(control.getDscVideo());
    		obj.setEstReg(CommonConstants.UPDATE_MTO_BIB);
    		obj.setUsuario(control.getCodUsuario());
    		
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		return resultado;			 		
    		
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }    
}
