package com.tecsup.SGA.web.biblioteca.controller;

import java.io.File;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.evaluaciones.NotaExternaManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialAdjuntarArchivoCommand;
//

public class AdminRegistrarMaterialAdjuntarDocumentosFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminRegistrarMaterialAdjuntarDocumentosFormController.class);	
//	private NotaExternaManager notaExternaManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

//	public void setNotaExternaManager(NotaExternaManager notaExternaManager) {
//		this.notaExternaManager = notaExternaManager;
//	}
	
	public AdminRegistrarMaterialAdjuntarDocumentosFormController(){
		super();
		setCommandClass(AdminRegistrarMaterialAdjuntarArchivoCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI");
    	
    	AdminRegistrarMaterialAdjuntarArchivoCommand command = new AdminRegistrarMaterialAdjuntarArchivoCommand();
    	
    	request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
    	request.setAttribute("strExtensionPDF", CommonConstants.GSTR_EXTENSION_PDF);
    	
    	//************************************
    	String codPeriodo =(String)request.getParameter("txhCodPeriodo");
    	String codProducto =(String)request.getParameter("codProducto");
    	String codCiclo =(String)request.getParameter("codCiclo");
    	String codEvaluador =(String)request.getParameter("txhCodEvaluador");
    	if(command.getOperacion()==null){
    		command.setUsuario(request.getParameter("txhCodUsuario"));
    		command.setTxhCodigoUnico(request.getParameter("txhCodUnico"));
    	}
    	command.setCodPeriodo(codPeriodo);
    	command.setCodProducto(codProducto);
    	command.setCodCiclo(codCiclo);
    	command.setCodEvaluador(codEvaluador);
    	//log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");    	
    	AdminRegistrarMaterialAdjuntarArchivoCommand control = (AdminRegistrarMaterialAdjuntarArchivoCommand)command;
    	log.info("OPERACION: " + control.getOperacion());
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta;    		
    		byte[] bytesArchivo = control.getTxtArchivo();
    		
    		if(bytesArchivo.length>5242880)//5MB
    		{
    			log.info("El tama�o del archivo es muy grande!");
    			control.setMsg("ERROR_TAMANIO");
    		}else{
        		String fileNameArchivo="";
        		
//        		String uploadDirArchivo = getServletContext().getRealPath("/"+CommonConstants.DIRECTORIO_DOCUMENTOS);
        		String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA+CommonConstants.DIRECTORIO_DOCUMENTOS;
        		
        		File dirPath = new File(uploadDirArchivo);
                if (!dirPath.exists()) {                                   
                      dirPath.mkdirs();
                }
        		
        		
                String sep = System.getProperty("file.separator");
                
    	        if (bytesArchivo.length>0){
    	        	 //fileNameArchivo = CommonConstants.FILE_NAME_DOC+control.getCodPeriodo()+control.getCodProducto()+control.getCodCiclo()+"."+control.getExtArchivo();
    	        	 fileNameArchivo = control.getNomArchivo();
    	        	 File uploadedFileArchivo = new File(uploadDirArchivo + sep + fileNameArchivo);
    	        	 FileCopyUtils.copy(bytesArchivo, uploadedFileArchivo);
    	        	 control.setNomNuevoArchivo(fileNameArchivo);
    	        	 
    	        	 rpta=InsertArchivoAdjuntoxMaterial(control);	        	 
    	        	 log.info("RPTA: " + rpta);
    	        	 if("-1".equals(rpta)){
    	        			control.setMsg("ERROR");
    	        	 }else if("-2".equals(rpta)){
    	        		 	control.setMsg("ERROR2");
    	        		 	log.info("ARCHIVO REPETIDO");
    	        	 }else{
    	        			control.setMsg("OK");
    	        	 }
    	        }
    	        else{

    	        }    			
    			
    		}

    	}
		//log.info("onSubmit:FIN");
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_adjuntar_documentos","control",control);	    
    }
    //SP_INS_ARCH_ADJ_X_MATERIAL
    private String InsertArchivoAdjuntoxMaterial(AdminRegistrarMaterialAdjuntarArchivoCommand control){
    	try{
    		/*log.info("PARAMETROS PARA EL STORED InsertArchivoAdjuntoxMaterial");
    		log.info("CodigoUnico|"+control.getTxhCodigoUnico()+"|");
    		log.info("Titulo|"+control.getTxtTitulo()+"|");
    		log.info("NomArchivo|"+control.getNomArchivo()+"|");
    		log.info("Usuario|"+control.getUsuario()+"|");*/
    		
    		String resp = biblioGestionMaterialManager
    		.InsertArchivoAdjuntoxMaterial(
    				control.getTxhCodigoUnico(),
    				control.getTxtTitulo(),
    				control.getNomArchivo(),
    				control.getUsuario());
    		return resp;
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }   
}
