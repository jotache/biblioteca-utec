package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.web.biblioteca.command.ParametrosGeneralesCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.modelo.ParametrosReservados;

public class ParametrosGeneralesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ParametrosGeneralesFormController.class);
	BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)  throws ServletException {
		ParametrosGeneralesCommand command = new ParametrosGeneralesCommand();		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		
		List<ParametrosReservados> lista = new ArrayList<ParametrosReservados>();
		lista = biblioMantenimientoManager.GetListParametrosGenerales();
		
		request.setAttribute("listaParametrosUsuario", lista);
		
		command.setTamanioLista(String.valueOf(lista.size()));
		
		return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	ParametrosGeneralesCommand control = (ParametrosGeneralesCommand)command;    	    	
		String resultado = "";
		log.info("OPERACION:" + control.getOperacion());
		
		if (control.getOperacion().trim().equals("GRABAR"))
    	{
			resultado = graba(control);
			log.info("RPTA:" + resultado);
			if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else {
    			List<ParametrosReservados> lista = new ArrayList<ParametrosReservados>();
    			lista = biblioMantenimientoManager.GetListParametrosGenerales();
    			
    			request.setAttribute("listaParametrosUsuario", lista);
    			
    			control.setTamanioLista(String.valueOf(lista.size()));
    			control.setMsg("OK");
    		}
    	}
		
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_Parametros_Generales","control",control);		
    }
    
   private String graba(ParametrosGeneralesCommand control){
	   
//	   control.setGraba(this.biblioMantenimientoManager.InsertParametrosReservados(CommonConstants.TIPT_PARAMETROS_RESERVA
//			   , control.getEsmadi(), control.getEsmito(), control.getEsdipe()
//			   , control.getInmadi(), control.getInmito(), control.getIndipe()
//			   , control.getBinrodipe(),control.getVisualizar(),"", control.getCodUsuario()));	   
	   	  
	   return this.biblioMantenimientoManager.UpdateParametrosGenerales(
			   	control.getCadenaDatos(), control.getCodUsuario(), control.getTamanioLista());
   }
    
//   private String cargaDatos(ParametrosGeneralesCommand control){
//	   ParametrosReservados obj = this.biblioMantenimientoManager.GetAllParametrosGenerales(CommonConstants.TIPT_PARAMETROS_RESERVA);
//	   if(obj==null){
//		   control.setBinrodipe("");
//		   control.setEsdipe("");
//		   control.setEsmadi("");
//		   control.setEsmito("");
//		   control.setIndipe("");
//		   control.setInmadi("");
//		   control.setInmito("");
//		   control.setVisualizar("");
//	   }
//	   else{
//		   control.setEsmadi(obj.getEsMaDi());
//		   control.setEsmito(obj.getEsMiTo());
//		   control.setEsdipe(obj.getEsDiPe());
//		   control.setInmadi(obj.getInMaDi());
//		   control.setInmito(obj.getInMiTo());
//		   control.setIndipe(obj.getInDiPe());
//		   control.setBinrodipe(obj.getBiNroDiPe());
//		   control.setVisualizar(obj.getCantMostrarWeb());
//	   }
//	   return control.getEsmadi() + control.getEsmito() + control.getEsdipe() + control.getInmadi() 
//	          + control.getInmadi() + control.getInmadi() + control.getBinrodipe();
//   }
    
}
