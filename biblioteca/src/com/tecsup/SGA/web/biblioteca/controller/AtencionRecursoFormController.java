package com.tecsup.SGA.web.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.AtencionCommand;

public class AtencionRecursoFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(AtencionRecursoFormController.class);
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
		throws Exception {
		//log.info("formBackingObject Start");
		AtencionCommand control = new AtencionCommand();
		
		
		List<Sala> listaAtencionesPC = new ArrayList<Sala>();
		//log.info("control.getAccion():"+control.getAccion());
		if(control.getAccion().equals("inicio")){		
			String sSede = (String)request.getParameter("sede");
			String sCodRecurso = (String)request.getParameter("codrecurso");
			String sFecha = (String) request.getParameter("fecha");
			String sPC = (String) request.getParameter("pc");
			control.setSede(sSede);
			control.setCodPc(sCodRecurso);
			control.setFechaAtencion(sFecha);
			control.setNombrePC(sPC);
			listaAtencionesPC = this.portalWebBibliotecaManager.listarAtencionesPorPC(sFecha, sSede, sCodRecurso);	
		}
		
		control.setListaAtencionesPorPc(listaAtencionesPC);
		control.setPestania("1");
		//listar atencios de una pc en una fecha indicada
		//log.info("listaAtencionesPC.size():"+listaAtencionesPC.size());
		//log.info("formBackingObject End");
		return control;		
	}
	protected ModelAndView onSubmit(HttpServletRequest request,	HttpServletResponse response, Object command, BindException errors) throws Exception {
		AtencionCommand control = (AtencionCommand) command;
		
		log.info("OPERACION:" + control.getAccion());
		control.setPestania("1");
		if (control.getAccion().equals("finalizar")){
			
			//log.info("finalizar id atención:" + control.getIdAtencion());

			String codUsuarioActualiza = "";
			UsuarioSeguridad user = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
			codUsuarioActualiza = user.getIdUsuario(); //Usuario que registra
			
			String resp = this.portalWebBibliotecaManager.actualizarAtencion(control.getIdAtencion(), "T", codUsuarioActualiza);
			log.info("RPTA:" + resp);
			if (resp.equals("0")){
				control.setTipoMensaje("OK");
				control.setMensaje("Se ha registrado la finalización");
				List<Sala> listaAtencionesPC = new ArrayList<Sala>();
				listaAtencionesPC = this.portalWebBibliotecaManager.listarAtencionesPorPC(control.getFechaAtencion(), control.getSede(), control.getCodPc());
				control.setListaAtencionesPorPc(listaAtencionesPC);
			}else if(resp.equals("-1")){
				control.setTipoMensaje("ERROR");
				control.setMensaje("No se pudo finalizar la atención. Comuniquese con el Administrador");			
			}
			

		}else if (control.getAccion().equals("historico")){
			List<Sala> listaAtencionesPCHistorico = new ArrayList<Sala>();
			listaAtencionesPCHistorico = this.portalWebBibliotecaManager.listarAtencionesPorPCHistorico(control.getSede(), control.getCodPc());
			control.setListaAtencionesPorPcHistorico(listaAtencionesPCHistorico);
			control.setPestania("2");
			
			List<Sala> listaAtencionesPC = new ArrayList<Sala>();
			listaAtencionesPC = this.portalWebBibliotecaManager.listarAtencionesPorPC(control.getFechaAtencion(), control.getSede(), control.getCodPc());
			control.setListaAtencionesPorPc(listaAtencionesPC);

		}
		
		//log.info("finaliza onSubmit");
		return new ModelAndView("/biblioteca/userAdmin/atencion_recurso","control",control);
	}
}
