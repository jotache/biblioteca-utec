package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.EnlacesDeInteresCommand;

public class EnlacesDeInteresFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(EnlacesDeInteresFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		EnlacesDeInteresCommand command = new EnlacesDeInteresCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
    	LlenarComboTipoMaterial(command, request);
    	LlenarDatos(command, request);
    	
        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	EnlacesDeInteresCommand control = (EnlacesDeInteresCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	if(control.getOperacion().equals("TIPOMATERIAL")){
    		LlenarDatos(control, request);
    	}
    	else{ if(control.getOperacion().equals("LIMPIAR"))
    		  {	List lista=new ArrayList();
    		 	request.getSession().setAttribute("listaSecciones", lista);
    		  }
    	} 
    	control.setOperacion("");
        return new ModelAndView("/biblioteca/userWeb/bib_enlaces_de_interes","control",control);		
    }
    
    public void LlenarDatos(EnlacesDeInteresCommand control, HttpServletRequest request){
    	
    	Secciones seccion= new Secciones();
    	seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_ENLACES_INTERES);
    	TipoTablaDetalle obj= new TipoTablaDetalle();
    	if(control.getCodTipoMaterial()!=null)
    		{  if(control.getCodTipoMaterial().equals("-1")) obj.setCodTipoTablaDetalle("");
    		   else
    		   obj.setCodTipoTablaDetalle(control.getCodTipoMaterial());}
    	else  	
    		obj.setCodTipoTablaDetalle("");   
    	
		control.setListaSecciones(this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj));
		request.getSession().setAttribute("listaSecciones", control.getListaSecciones());
    	
    }
    
    public void LlenarComboTipoMaterial(EnlacesDeInteresCommand control, HttpServletRequest request){
    	
    	 TipoTablaDetalle obj= new TipoTablaDetalle();
 	        obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			
			control.setCodListaTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
    }
	
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
}
