package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.inicio.command.BusquedaAlumnoCommand;
import com.tecsup.SGA.web.inicio.controller.BusquedaAlumnoController;

public class BusquedaUsuarioController extends SimpleFormController {

	private static Log log = LogFactory.getLog(BusquedaAlumnoController.class);
	private PortalWebBibliotecaManager portalWebBibliotecaManager ;
	/**
	 * @param portalWebBibliotecaManager the portalWebBibliotecaManager to set
	 */
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	
	public BusquedaUsuarioController(){
		super();
		setCommandClass(BusquedaAlumnoCommand.class);
	}
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		BusquedaAlumnoCommand command = new BusquedaAlumnoCommand();
		//log.info("formBackingObject:inicio");
		command.setTipoRetorno(request.getParameter("tipoRetorno"));
		command.setVentanaOrigen(request.getParameter("ventanaOrigen"));
		command.setCtrlUSE(request.getParameter("ctrlUSE"));
		command.setCtrlCOD(request.getParameter("ctrlCOD"));
		command.setCtrlNOM(request.getParameter("ctrlNOM"));				
		//log.info("formBackingObject:fin");			
		return command;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		BusquedaAlumnoCommand control = (BusquedaAlumnoCommand) command;
		log.info("OPERACION:" + control.getOperacion());
		if (control.getOperacion().equals("BUSCAR")){
			//realizar consulta...
			String apellPaterno = (String) control.getApellPaterno();
			String apellMaterno = (String) control.getApellMaterno();
			String nombre1 = (String) control.getNombre();
			String codCarnet = (String) control.getCodCarnet();									
			control.setListaAlumnos(this.portalWebBibliotecaManager.getUsuarios(apellPaterno, apellMaterno, nombre1, codCarnet));
			
		}
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/buscarUsuarioBib","control",control);
	}
}
