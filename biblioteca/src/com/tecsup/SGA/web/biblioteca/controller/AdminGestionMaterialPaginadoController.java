package com.tecsup.SGA.web.biblioteca.controller;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonSeguridad;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminGestionMaterialCommand;

//public class AdminGestionMaterialPaginadoController  implements Controller  {
public class AdminGestionMaterialPaginadoController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AdminGestionMaterialPaginadoController.class);
	//private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
    /*public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}*/
	protected Object formBackingObject(HttpServletRequest request) throws ServletException {			
    	AdminGestionMaterialCommand control = new AdminGestionMaterialCommand();
    	control.setSedeSel(request.getParameter("txhSedeSel"));
    	control.setSedeUsuario(request.getParameter("txhSedeUsuario"));
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");    	
    	String tipoUsuario="";    	
    	if (usuarioSeguridad!= null){    		    		
			
    		tipoUsuario = usuarioSeguridad.getTipoUsuario();
    		
    		//Esto se realiza en el login
//    		List<UsuarioPerfil> listaRoles = usuarioSeguridad.getRoles();			
//			if (listaRoles!=null){				
//				for (UsuarioPerfil up : listaRoles){									
//					if (up.getNombrePerfil().trim().equals(CommonSeguridad.BIB_PERFIL_ADMINISTRADOR) || up.getNombrePerfil().trim().equals(CommonSeguridad.BIB_PERFIL_ADMINISTRADOR_TOTAL)){
//						tipoUsuario="A";
//					}
//				}
//			} 
			
    	}else{
    		//System.out.println("usuarioSeguridad NULOO");
    	}    	
    	control.setTipoUsuario(tipoUsuario);    	
		return control;		
	}
	
	//public ModelAndView handleRequest(HttpServletRequest request,HttpServletResponse response) throws Exception {
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("handleRequest:INI");
		//comment    	    	
    	ModelMap model = new ModelMap();    
    	AdminGestionMaterialCommand control = new AdminGestionMaterialCommand();
    	String operacion = request.getParameter("operacion");    	    	
    	log.info("OPERACION: "+operacion);
    	
    	if("irEliminar".equalsIgnoreCase(operacion)){
    		String txhCodigoUnico = request.getParameter("txhCodigoUnico");
    		//log.info("CODIGO A ELIMINAR|"+txhCodigoUnico+"|");
    		//String resp  = biblioGestionMaterialManager.deleteMaterial(txhCodigoUnico);
    	}
	   	
	   	//log.info("handleRequest:FIN");
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_gestion_material_paginado", "control", control);
	}



}
