package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaSalasFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaSalasFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request) throws ServletException {
		
		ConsultaSalasCommand command = new ConsultaSalasCommand();		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));    	    	
		command.setSedeSel((String)request.getParameter("txhSedeSelec"));
		command.setSedeUsuario((String)request.getParameter("txhSedeUsuario"));
		
    	if(command.getOperacion()==null){		
			request.getSession().removeAttribute("listaSala");			
		}
    	cargaCabecera(command);
    			
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	    	
    	ConsultaSalasCommand control = (ConsultaSalasCommand) command;
    	
    	 
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR")){
    		//System.out.println("entra para buscar");
    		cargaBandeja(control);
    		request.getSession().setAttribute("listaSala", control.getListaSala());
    	}
    	
    	
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_salas","control",control);		
    }
    public void cargaBandeja(ConsultaSalasCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();    	
    	if(control.getTipoSala()!=""){
    		//System.out.println("cod tipo sala--cod descripcion");
    		//System.out.println(control.getTipoSala()+"<->"+control.getDscSala());
    		obj.setDscValor3(CommonConstants.TIPT_TIPO_SALA);    	
			obj.setCodTipoTabla(control.getTipoSala());
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SALA);
			obj.setCodDetalle("");
			obj.setDescripcion(control.getDscSala());
			obj.setDscValor6(control.getSedeSel()); 
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			obj.setTipoLista("0");
			control.setListaSala(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    	}
    }
    public void cargaCabecera(ConsultaSalasCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
    	obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SALA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	control.setListaTipoSala(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    }
}