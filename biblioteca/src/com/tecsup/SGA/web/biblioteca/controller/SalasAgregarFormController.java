package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.ConsultaSalasCommand;
import com.tecsup.SGA.web.biblioteca.command.IdiomasAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.SalasAgregarCommand;


public class SalasAgregarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(SalasAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	   	    	
    	SalasAgregarCommand command = new SalasAgregarCommand();    	
    	cargaCabecera(command);
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));    	    		
    	String operacion=((String)request.getParameter("txhOperacion")!=null?(String)request.getParameter("txhOperacion"):"");
    	command.setSede((String)request.getParameter("sede"));
		
    	if(operacion.equalsIgnoreCase("MODIFICAR")){
			
    		command.setCodigoSec((String)request.getParameter("txhCodigoSec"));
	    	command.setCodigo((String)request.getParameter("txhCodigoSala"));
	    		    		    
	    	//obtener datos de la sala...
	     	TipoTablaDetalle detalleSala = biblioMantenimientoManager.GetTipoTablaDetalle("0046", command.getCodigoSec());
	    	if (detalleSala!=null){
	    			    	
	    		command.setDscSala(detalleSala.getDescripcion());
	    		command.setTipoSala(detalleSala.getDscValor1());
	    		command.setCapacidad(detalleSala.getDscValor3());
	    		command.setCodEstado(detalleSala.getEstReg());
	    		command.setFechaInicio(detalleSala.getDscValor4());
	    		command.setFechaFin(detalleSala.getDscValor5());
	    		
	    		log.info("detalleSala.getDscValor7():" + detalleSala.getDscValor7());
	    		log.info("detalleSala.getDscValor8():" + detalleSala.getDscValor8());
	    		
	    		if(detalleSala.getDscValor8().length()>0)
	    			command.setCodHoraInicio1(detalleSala.getDscValor8().substring(0, 2));
	    		
	    		if(detalleSala.getDscValor9().length()>0)
	    			command.setCodHoraInicio2(detalleSala.getDscValor9().substring(0, 2));
//	    		command.setCodHoraInicio2(detalleSala.getDscValor8());
	    		log.info("C�digo Hr.Inicio:" + command.getCodHoraInicio1());
	    		log.info("C�digo Hr.Fin:" + command.getCodHoraInicio2());
	    		
	    	}
	    	command.setOperacion(operacion);
	    	
		}
		else{
			command.setOperacion(operacion);
			
		}    	
      
		
		//listar horas
		command.setListaHoras(
				this.portalWebBibliotecaManager.ListarHorasAtencionPorSede(command.getSede()==null?"L":command.getSede())
				);
		
		if(command.getListaHoras().size()>0){
//			Sala horaI = command.getListaHoras().get(0);
//			log.info("Hora 1: " + horaI);
//			command.setCodHoraInicio1(horaI.getHoraIni());
//			Sala horaF = command.getListaHoras().get(command.getListaHoras().size()-1);
//			log.info("Hora 2: " + horaI);
//			command.setCodHoraInicio2(horaF.getHoraIni());
		}
		
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	
    	SalasAgregarCommand control = (SalasAgregarCommand) command;    	
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("AGREGAR"))
    	{
    		resultado = InsertSala(control);
    		log.info("RPTA:" + resultado);
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}
    	else
		if (control.getOperacion().trim().equals("MODIFICAR"))
    	{	
    		resultado = UpdateSala(control);
    		log.info("RPTA:" + resultado);
    		
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}   	
				
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_salas_agregar","control",control);		
    }
    public void cargaCabecera(SalasAgregarCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
    	obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SALA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	
    	control.setListaTipoSala(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    }
    private String InsertSala(SalasAgregarCommand control)
    {
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTabla(control.getTipoSala());
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SALA);
    		obj.setDescripcion(control.getDscSala());
    		obj.setUsuario(control.getCodUsuario());
    		
    		obj.setDscValor3(control.getCapacidad());
    		obj.setDscValor4(control.getFechaInicio());
    		obj.setDscValor5(control.getFechaFin());
    		obj.setDscValor6(control.getSede());
    		
    		resultado=this.biblioMantenimientoManager.InsertTablaDetalleBiblioteca(obj);
    		return resultado;			 		
    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String UpdateSala(SalasAgregarCommand control)
    {
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTabla(control.getTipoSala());
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SALA);
    		obj.setDscValor1(control.getCodigoSec());
    		obj.setDscValor2(control.getCodigo());
    		obj.setDescripcion(control.getDscSala());
    		if (control.getCodEstado().equals("A"))
    			obj.setEstReg(CommonConstants.UPDATE_MTO_BIB);
    		else
    			obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		//obj.setEstReg(CommonConstants.UPDATE_MTO_BIB);
    		obj.setUsuario(control.getCodUsuario());
    		
    		    		
    		String fechaHoraIni = "";    		
    		String fechaHoraFin = "";
    		
    		if(!(control.getFechaInicio().trim().equals(control.getFechaFin().trim()))){
//    			fechaHoraIni=control.getFechaFin().trim();
//    			fechaHoraFin=control.getFechaInicio().trim();
    			fechaHoraIni=control.getFechaInicio().trim();
    			fechaHoraFin=control.getFechaFin().trim();    			
    		}else{
    			if(!control.getFechaFin().trim().equals("") && !control.getFechaInicio().trim().equals("")){
    				if(!control.getCodHoraInicio1().trim().equals("") && !control.getCodHoraInicio2().trim().equals("")){
    					fechaHoraIni=control.getFechaFin().trim()+"|"+control.getCodHoraInicio1().trim();
        				fechaHoraFin=control.getFechaInicio().trim()+"|"+control.getCodHoraInicio2().trim();
    				}else{
//    					fechaHoraIni=control.getFechaFin().trim();
//            			fechaHoraFin=control.getFechaInicio().trim();
    					fechaHoraIni=control.getFechaInicio().trim();
    					fechaHoraFin=control.getFechaFin().trim();
    				}
    					
    			}else{
//    				fechaHoraIni=control.getFechaFin().trim();
//        			fechaHoraFin=control.getFechaInicio().trim();
    				fechaHoraIni=control.getFechaInicio().trim();
    				fechaHoraFin=control.getFechaFin().trim();
    			}
    		}
    		    		    	
    		obj.setDscValor3(control.getCapacidad());
    		obj.setDscValor4(fechaHoraIni);
    		obj.setDscValor5(fechaHoraFin);
    		obj.setDscValor6(control.getSede());
    		
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		return resultado;			 		
    		
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }

	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}    
}
