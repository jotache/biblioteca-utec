package com.tecsup.SGA.web.biblioteca.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;


public class AdminGestionMaterialHistorialProrrogasFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminGestionMaterialHistorialProrrogasFormController.class);
	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		log.info("AdminGestionMaterialHistorialProrrogasFormController:formBackingObject:INI"); 
		String txhCodUsuario = request.getParameter("txhCodUsuario");
		String txhNroIngreso = request.getParameter("txhNroIngreso");
		String txhSede = request.getParameter("txhSede");
    	ModelMap model = new ModelMap();
    	
    	List lista = biblioGestionMaterialManager.getAllHistorialProrrogaxMaterial(txhNroIngreso, txhCodUsuario, txhSede);
    	
//    	request.getSession().
//    	setAttribute("SS_HISTORIAL",lista);
    	
    	model.addObject("lista", lista);
    	model.addObject("txhCodUsuario", txhCodUsuario);
    	model.addObject("txhNroIngreso", txhNroIngreso);
    	model.addObject("txhNomUsuario", request.getParameter("txhNomUsuario"));
    	model.addObject("txhNomIngreso", request.getParameter("txhNomIngreso"));
    	
    	
	   	//log.info("handleRequest:FIN");
		return new ModelAndView(this.getSuccessView(), "model", model);
	}
	
    
}
