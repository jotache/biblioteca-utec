package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialDeweyCommand;


public class AdminRegistrarMaterialDeweyFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialDeweyFormController.class);
	
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
    public AdminRegistrarMaterialDeweyFormController() {
        super();        
        setCommandClass(AdminRegistrarMaterialDeweyCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)	
    throws ServletException{
    	//log.info("formBackingObject:INI"); 
    	AdminRegistrarMaterialDeweyCommand control = new AdminRegistrarMaterialDeweyCommand();
    	control.setTxhCodUsuario(request.getParameter("txhCodUsuario"));
    	
		if(control.getOperacion()==null){
			control = iniCategoria(control);
		}
        //log.info("formBackingObject:FIN");
        return control;
   }
	
    private AdminRegistrarMaterialDeweyCommand iniCategoria(
			AdminRegistrarMaterialDeweyCommand control) {
   	 TipoTablaDetalle obj2= new TipoTablaDetalle();
	 obj2.setDscValor3("");
	 obj2.setCodTipoTabla("");
	 obj2.setCodTipoTablaDetalle(CommonConstants.TIPT_CATEGORIA_DEWEY);
     obj2.setCodDetalle("");
	 obj2.setDescripcion("");
	 obj2.setTipo(CommonConstants.TIPO_ORDEN_DSC);
     control.setLstCategoria(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj2));
     return control;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	
    	AdminRegistrarMaterialDeweyCommand control = (AdminRegistrarMaterialDeweyCommand) command;
    	
    	log.info("OPERACION:"+control.getOperacion());
    	
    	if("irConsultar".equalsIgnoreCase(control.getOperacion())){
    		control = irConsultar(control);    		
    	}
    	
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_dewey","control",control);		
    }

	private AdminRegistrarMaterialDeweyCommand irConsultar(
			AdminRegistrarMaterialDeweyCommand control) {
    	
	    TipoTablaDetalle obj1= new TipoTablaDetalle();
	    //log.info("getCodCategoria: "+control.getCboCategoria());
	    obj1.setDscValor3("");
	    obj1.setCodTipoTabla(control.getCboCategoria());
	    obj1.setCodTipoTablaDetalle(CommonConstants.TIPT_CLASIFICACION_DEWEY);
		obj1.setCodDetalle("");
		obj1.setDescripcion(control.getTxtDescripcion());
		//JHPR 2008-06-20
		//obj1.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		obj1.setTipo(CommonConstants.TIPO_ORDEN_COD);
		
		log.info("getCodTipoTabla: "+obj1.getCodTipoTabla());
		log.info("getCodTipoTablaDetalle: "+obj1.getCodTipoTablaDetalle());
		log.info("getCodDetalle: "+obj1.getCodDetalle());
		log.info("getDescripcion: "+obj1.getDescripcion());
		control.setLstResultado(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj1));
		
		return control;
	}
}
