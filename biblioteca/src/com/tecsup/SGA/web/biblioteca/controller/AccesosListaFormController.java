package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.AccesosCommand;
import com.tecsup.SGA.web.biblioteca.command.ParametrosGeneralesCommand;
import com.tecsup.SGA.bean.OpcionesApoyo;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.AccesoIP;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.UsuarioPM;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.modelo.ParametrosReservados;

public class AccesosListaFormController extends SimpleFormController{
    private static Log log = LogFactory.getLog(AccesosListaFormController.class);
    BiblioMantenimientoManager biblioMantenimientoManager;

    public void setBiblioMantenimientoManager(BiblioMantenimientoManager biblioMantenimientoManager) {
        this.biblioMantenimientoManager = biblioMantenimientoManager;
    }


    protected Object formBackingObject(HttpServletRequest request) throws ServletException {
        log.info("formBackingObject");
        AccesosCommand command = new AccesosCommand();

        if(request.getParameter("txhCodUsuario") != null){
            command.setCodUsuario(request.getParameter("txhCodUsuario"));
            command.setCodSujeto(Integer.parseInt(request.getParameter("txhCodSujeto")));
            command.setNomUsuario(request.getParameter("txhNomUsuario"));
            cargaDatos(command);
        }

        return command;
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        log.info("initBinder");
        NumberFormat nf = NumberFormat.getNumberInstance();
        binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class, nf, true));
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        log.info("processFormSubmission");
        return super.processFormSubmission(request, response, command, errors);
    }

    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        log.info("onSubmit");
        AccesosCommand control = (AccesosCommand)command;
        log.info("OPERACION:" + control.getOperacion());

        if ("GRABAR".equals(control.getOperacion())){

            UsuarioSeguridad usuarioActual = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
            control.setUsuCreacion(usuarioActual.getIdUsuario());

            if (!guardar(control)) control.setMsg("ERROR");
            else control.setMsg("OK");

        }else if ("ELIMINAR".equals(control.getOperacion())){

            if (!eliminar(control)) control.setMsg("ERROR");
            else control.setMsg("OK");

        }else if ("GUARDAR_PERMISOS".equals(control.getOperacion())){
            UsuarioSeguridad usuario = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
            control.setUsuCreacion(usuario.getIdUsuario());
            String rpta = this.biblioMantenimientoManager.updateOpcionesApoyo(control.getCodSujeto().toString(), control.getCadenaDatos(), String.valueOf(control.getTamanoLstOpciones()), control.getUsuCreacion());
            if(rpta.equals("0"))
                control.setMsg("OK");
            else
                control.setMsg("ERROR");
        }

        cargaDatos(control);

        return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_AccesosLista","control",control);
    }

   private boolean guardar(AccesosCommand control){
       try{

           this.biblioMantenimientoManager.InsertAccesoIP(control.getCodSujeto(), control.getNumIP(), control.getUsuCreacion());

           return true;
       }catch (Exception e) {
           log.error(e);
           return false;
       }
   }

   private boolean eliminar(AccesosCommand control){
       try{

           this.biblioMantenimientoManager.DeleteAccesoIP(control.getCodSujeto(), control.getNumIP());

           return true;
       }catch (Exception e) {
           log.error(e);
           return false;
       }
   }

    private void cargaDatos(AccesosCommand control) {
        if(control.getCodUsuario() != null){
            UsuarioPM usuario = new UsuarioPM();
            usuario.setCodUsuario(control.getCodUsuario());
            usuario.setCodSujeto(control.getCodSujeto());
            usuario.setNomUsuario(control.getNomUsuario());
            usuario.setListaAcceso(this.biblioMantenimientoManager.GetAllAccesosLista(control.getCodSujeto()));
            usuario.setListaOpciones(this.biblioMantenimientoManager.listaDeOpcionesPorApoyo(control.getCodSujeto().toString()));
            control.setTamanoLstOpciones(usuario.getListaOpciones().size());
            System.out.println(usuario);
            control.setUsuario(usuario);
        }else{
             control.setMsg("ERROR");
        }
    }




}
