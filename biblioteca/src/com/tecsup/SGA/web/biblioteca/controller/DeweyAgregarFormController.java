package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.DeweyAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.PaisesAgregarCommand;


public class DeweyAgregarFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(DeweyAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {    	 
    	
    	DeweyAgregarCommand command = new DeweyAgregarCommand();
    	
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	command.setCodDetalle((String)request.getParameter("txhCodigo"));
    	command.setCodCategoria((String)request.getParameter("txhCodCategoria"));//
    	command.setCodigoDewey("");
    	TipoTablaDetalle obj2= new TipoTablaDetalle();
		 obj2.setDscValor3("");
		 obj2.setCodTipoTabla("");
		 obj2.setCodTipoTablaDetalle(CommonConstants.TIPT_CATEGORIA_DEWEY);
	     obj2.setCodDetalle("");
		 obj2.setDescripcion("");
		 obj2.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		 command.setCodListaCategoria(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj2));
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	DeweyAgregarCommand control = (DeweyAgregarCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	if(control.getOperacion().trim().equals("GRABAR"))
    	{   
    		resultado = InsertDewey(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    		      else
    		      control.setMsg("OK");
    		    }
    	}
    	control.setOperacion("");
				
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_dewey_agregar","control",control);		
    }
    
    private String InsertDewey(DeweyAgregarCommand control)
    {   
    	try
    	{   
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla(control.getCodCategoria().trim());
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_CLASIFICACION_DEWEY);
    		obj.setDescripcion(control.getDescripcionDewey().trim());
    		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    		obj.setCodDetalle("");
    		obj.setUsuario("1");//control.getCodEvaluador());//obj.setUsuario(control.getCodEvaluador());
    		obj.setDscValor3(control.getCodigoDewey());
    		obj.setDscValor4("");
    		obj.setDscValor5("");
                      
    		obj.setCodTipoTablaDetalle(this.biblioMantenimientoManager.InsertTablaDetalleBiblioteca(obj));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
 }
