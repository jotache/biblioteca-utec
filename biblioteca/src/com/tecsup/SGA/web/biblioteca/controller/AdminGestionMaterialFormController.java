package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.CommonSeguridad;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.AdminGestionMaterialCommand;
import com.tecsup.SGA.web.evaluaciones.command.ConsultarEvaluacionesCommand;

public class AdminGestionMaterialFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminGestionMaterialFormController.class);
	
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI"); 
    	AdminGestionMaterialCommand control = new AdminGestionMaterialCommand();    	  
    	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");    	
    	String tipoUsuario="";    	
    	if (usuarioSeguridad!= null){    		    		
			List<UsuarioPerfil> listaRoles = usuarioSeguridad.getRoles();			
			if (listaRoles!=null){				
				for (UsuarioPerfil up : listaRoles){									
					if (up.getNombrePerfil().trim().equals(CommonSeguridad.BIB_PERFIL_ADMINISTRADOR) || up.getNombrePerfil().trim().equals(CommonSeguridad.BIB_PERFIL_ADMINISTRADOR_TOTAL)){
						tipoUsuario="A";
					}
				}
			}     	
    	}    	
    	control.setTipoUsuario(tipoUsuario);
    	control.setSedeSel(request.getParameter("txhCodSede"));
    	control.setSedeUsuario(request.getParameter("txhSedeUsuario"));
    	//comment	
    		if(control.getOperacion()==null){
    			control.setUsuario(request.getParameter("txhCodUsuario"));
    			control = ini(control);
    			control.setPathImagen("../images/default.gif");
    			    			
    			if(request.getParameter("irRegresar")==null)
    			request.getSession().removeAttribute("SS_RESULTADO_MATERIAL_FILTRO");
    			
    			if(request.getParameter("irRegresar")!=null){
    				control = cargaParametrosDeBusqueda(control,request);

    				/*System.out.println("---Parametros de busqueda---");
    				//System.out.println("prmTxtCodigo:"+control.getTxtCodigo()+"*");
    				//System.out.println("prmTxtNroIngreso:"+control.getTxtNroIngreso()+"*");
    				//System.out.println("prmCboTipoMaterial:"+control.getCboTipoMaterial()+"*");
    				//System.out.println("prmCboBuscarPor:"+control.getCboBuscarPor()+"*");
    				//System.out.println("prmTxtTitulo:"+control.getTxtTitulo()+"*");
    				//System.out.println("prmCboIdioma:"+control.getCboIdioma()+"*");
    				//System.out.println("prmCboAnioIni:"+control.getCboAnioIni()+"*");
    				//System.out.println("prmCboAnioFin:"+control.getCboAnioFin()+"*");
    				//System.out.println("prmTxtFechaReservaIni:"+control.getTxtFechaReservaIni()+"*");
    				//System.out.println("prmTxtFechaReservaFin:"+control.getTxtFechaReservaFin()+"*");		
    				System.out.println("---");*/    				
    				
    				if (!(control.getTxtCodigo().trim().equals("") || control.getTxtNroIngreso().trim().equals("") || control.getCboTipoMaterial().trim().equals("") || control.getCboBuscarPor().trim().equals("") || control.getTxtTitulo().trim().equals("") || control.getCboIdioma().trim().equals("") || control.getCboAnioIni().trim().equals("") || control.getCboAnioFin().trim().equals("") || control.getTxtFechaReservaIni().trim().equals("") || control.getTxtFechaReservaFin().trim().equals("")))    				
    					request.getSession().setAttribute("SS_RESULTADO_MATERIAL_FILTRO",irConsultar(control));
    			}
    			
    		}
        //log.info("formBackingObject:FIN");
        return control;
   }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	
    	AdminGestionMaterialCommand control = (AdminGestionMaterialCommand) command;    	

    	log.info("OPERACION: " + control.getOperacion());
    	if("irConsultar".equals(control.getOperacion())){    		
    		request.getSession().setAttribute("SS_RESULTADO_MATERIAL_FILTRO",irConsultar(control));
    	}else if("irEliminar".equals(control.getOperacion())){
    		String resp = irEliminar(control);
    		log.info("RPTA: "+resp);
    		if("0".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO); 			
    		}else {
    			
    			if("-2".equalsIgnoreCase(resp)){
    				request.setAttribute("mensaje",CommonMessage.MAT_NO_ELIM);
    			}
    			else { //-1
    				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    			}
    		}
    		
    		request.getSession().setAttribute("SS_RESULTADO_MATERIAL_FILTRO",irConsultar(control));
    	}else if("irExportar".equals(control.getOperacion())){    		
    		request.getSession().setAttribute("SS_RESULTADO_MATERIAL_FILTRO",irConsultar(control));
    		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_gestion_material_paginado_xls","control",control);		
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_gestion_material","control",control);		
    }	

	private AdminGestionMaterialCommand cargaParametrosDeBusqueda(
			AdminGestionMaterialCommand control, HttpServletRequest request) {
			
		control.setTxtCodigo(request.getParameter("prmTxtCodigo"));
		control.setTxtNroIngreso(request.getParameter("prmTxtNroIngreso"));
		control.setCboTipoMaterial(request.getParameter("prmCboTipoMaterial"));
		control.setCboBuscarPor(request.getParameter("prmCboBuscarPor"));
		control.setTxtTitulo(request.getParameter("prmTxtTitulo"));
		control.setCboIdioma(request.getParameter("prmCboIdioma"));
		control.setCboAnioIni(request.getParameter("prmCboAnioIni"));
		control.setCboAnioFin(request.getParameter("prmCboAnioFin"));
		control.setTxtFechaReservaIni(request.getParameter("prmTxtFechaReservaIni"));
		control.setTxtFechaReservaFin(request.getParameter("prmTxtFechaReservaFin"));
	
		return control;
	}
	private AdminGestionMaterialCommand ini(AdminGestionMaterialCommand control){
		//log.info("ini:INI");
		control = iniciaMaterial(control);
		control = iniciaBuscarPor(control);
		control = iniciaIdioma(control);		
		control.setLstAnio(Anios.getAniosTodos(1990));		
		//log.info("ini:FIN");
		return control;
	}
	
	private AdminGestionMaterialCommand iniciaIdioma(
			AdminGestionMaterialCommand control) {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	obj.setDscValor3("");    	
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_IDIOMAS_BIBLIOTECA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstIdioma(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		return control;
	}

	private AdminGestionMaterialCommand iniciaBuscarPor(
			AdminGestionMaterialCommand control) {
	    TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_BUSQUEDA_Y_DEWEY); //antes era TIPT_TIPO_BUSQUEDA
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_COD);
		control.setLstBuscarPor(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		return control;
	}

	private AdminGestionMaterialCommand iniciaMaterial(
			AdminGestionMaterialCommand control) {		
	    TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		return control;
	}

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }   


	private String irEliminar(
			AdminGestionMaterialCommand control) { 
		return this.biblioGestionMaterialManager.deleteMaterial(control.getIdEliminacion(),control.getUsuario());
	}
	private List irConsultar(AdminGestionMaterialCommand control) {
		/*log.info("irConsultar:INI");
		
		log.info("control.getTxhCodigoUnico(): " + control.getTxhCodigoUnico());
		log.info("control.getTxtCodigo(): " + control.getTxtCodigo());
		log.info("control.getTxtNroIngreso(): " + control.getTxtNroIngreso());
		log.info("control.getCboTipoMaterial(): " + control.getCboTipoMaterial());
		log.info("control.getCboBuscarPor(): " + control.getCboBuscarPor());
		log.info("control.getTxtTitulo(): " + control.getTxtTitulo());
		log.info("control.getCboIdioma(): " + control.getCboIdioma());
		log.info("control.getCboAnioIni(): " + control.getCboAnioIni());
		log.info("control.getCboAnioFin(): " + control.getCboAnioFin());
		log.info("control.getTxtFechaReservaIni(): " + control.getTxtFechaReservaIni());
		log.info("control.getTxtFechaReservaFin(): " + control.getTxtFechaReservaFin());

		log.info("irConsultar:FIN" );*/
		
			return  this.biblioGestionMaterialManager.getAllMaterialesxFiltro(
			control.getTxhCodigoUnico(),		
			control.getTxtCodigo(),
			control.getTxtNroIngreso(),
			control.getCboTipoMaterial(),
			control.getCboBuscarPor(),
			control.getTxtTitulo(),
			control.getCboIdioma(),
			control.getCboAnioIni(),
			control.getCboAnioFin(),
			control.getTxtFechaReservaIni(),
			control.getTxtFechaReservaFin(),
			control.getSedeSel());

	}
}
