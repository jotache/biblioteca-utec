package com.tecsup.SGA.web.biblioteca.controller;

import java.io.File;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.SeccionesAgregarCommand;


public class SeccionesAgregarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(SeccionesAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    	throws ServletException {
    	   	    	
    	SeccionesAgregarCommand command = new SeccionesAgregarCommand();
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){
    		command.setCodEvaluador(usuarioSeguridad.getIdUsuario());	
    	}
    	
    	String operacion=((String)request.getParameter("txhOperacion")!=null?(String)request.getParameter("txhOperacion"):"");
    	
    	String codSeccion=((String)request.getParameter("txhCodSeccion")!=null?(String)request.getParameter("txhCodSeccion"):"");
    	
		if(operacion.equalsIgnoreCase("MODIFICAR")){
			inicializaModificar(command, request);
			command.setOperacion(operacion);			
		}
		if(operacion.equalsIgnoreCase("AGREGAR")){
			command.setOperacion(operacion);			
			request.setAttribute("codSeccion", codSeccion);
		}    	
        cargaCabecera(command);        
        //se inicializa las extensiones de los archivos
        request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
        request.setAttribute("strExtensionDOCX", CommonConstants.GSTR_EXTENSION_DOCX);
    	request.setAttribute("strExtensionXLS", CommonConstants.GSTR_EXTENSION_XLS);
    	request.setAttribute("strExtensionXLSX", CommonConstants.GSTR_EXTENSION_XLSX);
    	request.setAttribute("strExtensionPDF", CommonConstants.GSTR_EXTENSION_PDF);
    	request.setAttribute("strExtensionGIF", CommonConstants.GSTR_EXTENSION_GIF);
    	request.setAttribute("strExtensionJPG", CommonConstants.GSTR_EXTENSION_JPG);
    	//se inicializan los codigos de las secciones
    	request.setAttribute("codNOV", CommonConstants.COD_SEC_NOV);
    	request.setAttribute("codENLINT", CommonConstants.COD_SEC_ENL_INT);
    	request.setAttribute("codBIB", CommonConstants.COD_SEC_BIB);
    	request.setAttribute("codFUEINF", CommonConstants.COD_SEC_FUE_INF);
    	request.setAttribute("codREG", CommonConstants.COD_SEC_REG);
    	request.setAttribute("codHOR", CommonConstants.COD_SEC_HOR);    	
        //*********************************************
    	//carga fecha actual de la BD
    	try {
    		command.setFechaActual(Fecha.getFechaActual());
		} catch (Exception e) {
			e.printStackTrace(); 
		}           
        return command;
    } 
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {        
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	    	
    	SeccionesAgregarCommand control = (SeccionesAgregarCommand) command;
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("AGREGAR")){
    		
	        resultado = insertSeccion(control, request);
	        log.info("RPTA:" + resultado);
    		
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}
    	else
		if (control.getOperacion().trim().equals("MODIFICAR")){
			
			resultado = insertSeccion(control, request);
			log.info("RPTA:" + resultado);
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}   
    	//carga fecha actual de la BD
    	try {
    		control.setFechaActual(Fecha.getFechaActual());
		} catch (Exception e) {
			e.printStackTrace(); 
		}   
				
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_secciones_agregar","control",control);		
    }
    
    private void cargaCabecera(SeccionesAgregarCommand control){
    	cargaListaSeccion(control);
    	cargaListaGrupos(control);
    	cargaListaTipoMaterial(control);
    }
    private void cargaListaSeccion(SeccionesAgregarCommand control){
	    try{
	    	TipoTablaDetalle obj = new TipoTablaDetalle();
	    	
	    	obj.setDscValor3("");
	    	obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SECCION);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_COD);
	    	
	    	control.setListaSeccion(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
		}
    }
	private void cargaListaGrupos(SeccionesAgregarCommand control){
		try{
    		TipoTablaDetalle obj1= new TipoTablaDetalle();
    		
   		    obj1.setDscValor3("");
   		    obj1.setCodTipoTabla("");
   		    obj1.setCodTipoTablaDetalle(CommonConstants.TIPT_GRUPO_SECCION);
   			obj1.setCodDetalle("");
   			obj1.setDescripcion("");
   			obj1.setTipo(CommonConstants.TIPO_ORDEN_DSC);   			
   			
   			control.setListaGrupo(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj1));
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
	}
	private void cargaListaTipoMaterial(SeccionesAgregarCommand control){
		try{
    		TipoTablaDetalle obj= new TipoTablaDetalle();
    		
   		    obj.setDscValor3("");
   		    obj.setCodTipoTabla("");
   		    obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
   			obj.setCodDetalle("");
   			obj.setDescripcion("");
   			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);   			
   			
   			control.setListaTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));   			
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}		
	}
    private String insertSeccion(SeccionesAgregarCommand control, HttpServletRequest request){
    	try{
    		
    		
    		if(!control.getDscArchivo().equals("")){
    			
    			adjuntarArchivo(control, request);
    		}

    		Secciones seccion = new Secciones();
    		
    		seccion.setCodUnico(control.getCodUnico());
    		seccion.setCodSeccion(control.getCodSeccion());
    		seccion.setTipoMaterial(control.getCodTipoMaterial());
    		seccion.setCodGrupo(control.getCodGrupo());
    		seccion.setTema(control.getDscTema());
    		seccion.setDescripcionCorta(control.getDscCorta());
    		seccion.setDescripcionLarga(control.getDscLarga());
    		seccion.setUrl(control.getDscUrl());
    		seccion.setArchivo(control.getDscArchivo());
    		seccion.setFechaPublicacion(control.getFechaPublicacion());
    		seccion.setFechaVigencia(control.getFechaVigencia());
    		seccion.setFlag(control.getFlag());
    		seccion.setNomAdjunto("");
    		
    		if(!control.getDscDocumNov().equals("")){
    			
    			adjuntarArchivoDocNov(control, request);
    			seccion.setNomAdjunto(control.getDscDocumNov());
    			seccion.setUrl(control.getUrlNov());
    		}
    		
    		
    		String resultado="";
    		//campo usuario 8 / campo tipo vacio "insertar" "modificar"
    		
    		resultado=this.biblioMantenimientoManager.insertSecciones(seccion, control.getCodEvaluador(), "");
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private void inicializaModificar(SeccionesAgregarCommand control, HttpServletRequest request){
    	String codUnico=(String)request.getParameter("txhCodUnico");
    	
    	Secciones secciones= new Secciones();    	
    	secciones.setCodUnico(codUnico);    	
    	secciones.setCodSeccion("");
    	secciones.setCodGrupo("");
    	secciones.setCodTipo("");    	

    	Secciones seccion=(Secciones)this.biblioMantenimientoManager.getSeccionesByFiltros(secciones).get(0);    	
    	
    	
    	inicializaDataModificar(control, seccion);    	
    }
    private void inicializaDataModificar(SeccionesAgregarCommand control, Secciones seccion){    	
    	int tipo=0;    	
    	//se inicializa estos valores que ya no seran modificados
    	control.setCodUnico(seccion.getCodUnico());
    	control.setCodSeccion(seccion.getCodSeccion());
    	//****************************************************
    	if(seccion.getCodSeccion().equals(CommonConstants.COD_SEC_NOV))tipo=1;
    	if(seccion.getCodSeccion().equals(CommonConstants.COD_SEC_ENL_INT))tipo=2;
    	if(seccion.getCodSeccion().equals(CommonConstants.COD_SEC_BIB))tipo=3;
    	if(seccion.getCodSeccion().equals(CommonConstants.COD_SEC_FUE_INF))tipo=4;
    	if(seccion.getCodSeccion().equals(CommonConstants.COD_SEC_REG))tipo=5;
    	if(seccion.getCodSeccion().equals(CommonConstants.COD_SEC_HOR))tipo=6;
    	switch (tipo) {
			case 1:				
				control.setTipoGrupo(seccion.getCodGrupo());
				control.setTema(seccion.getTema());
				control.setDscCortaNov(seccion.getDescripcionCorta());
				control.setDscLargaNov(seccion.getDescripcionLarga());
				control.setDscArchivo(seccion.getArchivo());
				control.setDscDocumNov(seccion.getDocumentoNov());
				if (seccion.getDocumentoNov()!=null && seccion.getDocumentoNov()!=""){
					String [] stkCadena = seccion.getDocumentoNov().split("\\|");
					control.setDscDocumNov(stkCadena[1]);
				}
				control.setUrlNov(seccion.getUrl());
				
				//********************************
				control.setPathImagen(CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_SECCIONES+control.getDscArchivo());
				//control.setPathImagen("\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_DOCUMENTOS_SECCIONES+control.getDscArchivo());
				
				break;
			case 2:				
				control.setNombre(seccion.getTema());
				control.setTipoMaterial(seccion.getTipoMaterial());
				control.setUrl(seccion.getUrl());
				control.setDscArchivo(seccion.getArchivo());				
				break;
			case 3:
			case 4:				
				control.setNombre(seccion.getTema());
				control.setUrl(seccion.getUrl());
				control.setDscArchivo(seccion.getArchivo());				
				break;
			case 5:				
				control.setReglamento(seccion.getDescripcionLarga());				
				break;
			case 6:				
				control.setHorarioAtencion(seccion.getDescripcionCorta());				
				break;
		}
    	//la fecha es el mismo juego para todos las secciones
    	control.setTipoSeccion(seccion.getCodSeccion());
    	control.setFlag(seccion.getFlag());
    	control.setFechaInicio(seccion.getFechaPublicacion());
		control.setFechaFin(seccion.getFechaVigencia());
    }
    private void adjuntarArchivo(SeccionesAgregarCommand control, HttpServletRequest request)throws Exception{   	
		byte[] bytesArchivo;
    	if(control.getCodSeccion().equals(CommonConstants.COD_SEC_NOV)){
			bytesArchivo = control.getArchivoImagen();	
		}
		else{
			bytesArchivo = control.getArchivoDocumento();	
		}
    	
		String fileNameArchivo="";
		
//		String uploadDirArchivo = getServletContext().getRealPath("/"+CommonConstants.DIRECTORIO_SECCIONES);
		String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA + CommonConstants.DIRECTORIO_SECCIONES;
		
		//SI NO EXISTE LO CREO
		File dirPath = new File(uploadDirArchivo);
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }
        
        String sep = System.getProperty("file.separator");
        control.setCodArchivo(this.biblioMantenimientoManager.getNombreArchivoSeccion());
        

        if (bytesArchivo.length>0){
        	 fileNameArchivo = CommonConstants.FILE_NAME_DOC+control.getCodSeccion()+control.getCodArchivo()+"."+control.getExtArchivo();
        	 File uploadedFileArchivo = new File(uploadDirArchivo + sep + fileNameArchivo);
        	 FileCopyUtils.copy(bytesArchivo, uploadedFileArchivo);
        	 control.setDscArchivo(fileNameArchivo);        	    	 
        }
    }
    
    private void adjuntarArchivoDocNov(SeccionesAgregarCommand control, HttpServletRequest request)throws Exception{   	
		byte[] bytesDocumNov;
    	
		bytesDocumNov = control.getArchivoDocumentoNov();

		String fileNameArchivo="";
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		
//		String uploadDirArchivo = getServletContext().getRealPath("/"+CommonConstants.DIRECTORIO_SECCIONES);
		String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA + CommonConstants.DIRECTORIO_SECCIONES;
		
        String sep = System.getProperty("file.separator");
        control.setCodArchivo(this.biblioMantenimientoManager.getNombreArchivoSeccion());
        control.setDscDocumNov("");
        
        if(bytesDocumNov.length>0){
        	
        	CommonsMultipartFile fileDocNov = (CommonsMultipartFile) multipartRequest.getFile("archivoDocumentoNov");
        	control.setArchNovOriginalName(fileDocNov.getOriginalFilename());
        	
        	fileNameArchivo = CommonConstants.FILE_NAME_DOC+control.getCodSeccion()+control.getCodArchivo()+ "_doc" +"."+control.getExtArchivoNov();
       	 	File uploadedFileArchivo = new File(uploadDirArchivo + sep + fileNameArchivo);
       	 	FileCopyUtils.copy(bytesDocumNov, uploadedFileArchivo);
       	 	control.setDscDocumNov(fileNameArchivo + "|" + control.getArchNovOriginalName());
        }
        
    }
}