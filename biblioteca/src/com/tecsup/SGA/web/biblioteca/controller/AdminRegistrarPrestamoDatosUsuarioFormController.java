package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioBean;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarPrestamoDatosUsuarioCommand;

public class AdminRegistrarPrestamoDatosUsuarioFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AdminRegistrarPrestamoDatosUsuarioFormController.class);
	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI"); 
    	AdminRegistrarPrestamoDatosUsuarioCommand control = new AdminRegistrarPrestamoDatosUsuarioCommand();
    	
    	if(control.getOperacion()==null)
    	{    		
    		control.setTxhCodUsuario(request.getParameter("txhCodUsuario"));
    		control = irGetDatosUsuario(control);

    	}	
        //log.info("formBackingObject:FIN");
        return control;
   }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	AdminRegistrarPrestamoDatosUsuarioCommand control = (AdminRegistrarPrestamoDatosUsuarioCommand) command;
    	
    	log.info("OPERACION:"+control.getOperacion());
    	
    	if("irGetMaterial".equalsIgnoreCase(control.getOperacion())){
    		control = irGetDatosUsuario(control); 
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_prestamo","control",control);		
    }

	private AdminRegistrarPrestamoDatosUsuarioCommand irGetDatosUsuario(
			AdminRegistrarPrestamoDatosUsuarioCommand control) {
		//log.info("usuario>>"+control.getTxhCodUsuario()+">");
		List lst = biblioGestionMaterialManager.getAllDatosUsuarioBib(control.getTxhCodUsuario());
		
		if(lst!=null && lst.size()>0){
			UsuarioBean usu = (UsuarioBean)lst.get(0);
			control.setTxtCargo(usu.getNomCargoPersonal());
			control.setTxtCiclo(usu.getNomCiclo());
			control.setTxtEspecialidad(usu.getNomEspecialidad());
			control.setTxtNombres(usu.getNomUsuario());
			control.setTxtTipoUsuario(usu.getNomTipoUsuario());
			control.setTxhIndTipoUsuario(usu.getIndTipoUsuario());
			control.setTxhCodTipoUsuario(usu.getCodTipoUsuario());
			
		}else{
			control.setTxtCargo("");
			control.setTxtCiclo("");
			control.setTxtEspecialidad("");
			control.setTxtNombres("");
			control.setTxtTipoUsuario("");
			control.setTxhIndTipoUsuario("");
			control.setTxhCodTipoUsuario("");
		}
		
		return control;
	}
    
}
