package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.IdiomasAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.TipoProcedenciaAgregarCommand;


public class IdiomasAgregarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(IdiomasAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	   	
    	IdiomasAgregarCommand command = new IdiomasAgregarCommand();    	
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());
    	
    	String operacion=((String)request.getParameter("txhOperacion")!=null?(String)request.getParameter("txhOperacion"):"");
		if(operacion.equalsIgnoreCase("MODIFICAR")){
			command.setCodigoSec((String)request.getParameter("txhCodigoSec"));
	    	command.setCodigo((String)request.getParameter("txhCodigoIdioma"));
	    	command.setDscIdioma((String)request.getParameter("txhDescripcion"));
	    	command.setOperacion(operacion);
	    		    	
		}
		else{
			command.setOperacion(operacion);
			
		}    	
        
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	IdiomasAgregarCommand control = (IdiomasAgregarCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("AGREGAR")){
    		resultado = InsertIdioma(control);
    		log.info("RPTA:" + resultado);
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}
    	else
		if (control.getOperacion().trim().equals("MODIFICAR"))
    	{	
    		resultado = UpdateIdioma(control);
    		log.info("RPTA:" + resultado);
    		
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}   	
				
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_idiomas_agregar","control",control);		
    }
    
    private String InsertIdioma(IdiomasAgregarCommand control)
    {
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_IDIOMAS_BIBLIOTECA);
    		obj.setDescripcion(control.getDscIdioma());
    		obj.setUsuario(control.getCodUsuario());
    		
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		
    		resultado=this.biblioMantenimientoManager.InsertTablaDetalleBiblioteca(obj);
    		return resultado;			 		
    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String UpdateIdioma(IdiomasAgregarCommand control)
    {
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_IDIOMAS_BIBLIOTECA);
    		obj.setDscValor1(control.getCodigoSec());
    		obj.setDscValor2(control.getCodigo());
    		obj.setDescripcion(control.getDscIdioma());
    		obj.setEstReg(CommonConstants.UPDATE_MTO_BIB);
    		obj.setUsuario(control.getCodUsuario());
    		
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		return resultado;			 		
    		
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }    
}
