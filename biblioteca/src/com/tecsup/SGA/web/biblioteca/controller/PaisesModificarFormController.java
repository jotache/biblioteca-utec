package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.EditorialesModificarCommand;
import com.tecsup.SGA.web.biblioteca.command.PaisesModificarCommand;


public class PaisesModificarFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(PaisesModificarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	   	
    	PaisesModificarCommand command = new PaisesModificarCommand();
    
    	command.setCodigoPais((String)request.getParameter("txhCodValor"));
    	command.setDescripcionPais((String)request.getParameter("txhDescripcion"));
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	command.setCodDetalle((String)request.getParameter("txhCodigo"));
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	PaisesModificarCommand control = (PaisesModificarCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("MODIFICAR"))
    	{   
    		resultado = UpdatePaisBiblioteca(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    		      else
    			  control.setMsg("OK");}
    	}
    	control.setOperacion("");
			
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_paises_modificar","control",control);		
    }
    
    private String UpdatePaisBiblioteca(PaisesModificarCommand control)
    {   
    	try
    	{   String resultado="";
    		
    		Pais pais= new Pais();
    		pais.setPaisId(control.getCodDetalle().trim());
    		pais.setPaisDescripcion(control.getDescripcionPais().trim());
    		pais.setPaisCodigo(control.getCodigoPais().trim());
    		pais.setFlag(CommonConstants.UPDATE_MTO_BIB);
    		pais.setUsuCrea(control.getCodEvaluador().trim());//txhCodUsuario
    		resultado=this.biblioMantenimientoManager.updateDeletePais(pais);
    		
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
}
