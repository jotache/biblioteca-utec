package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.AutoresModificarCommand;


public class AutoresModificarFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AutoresModificarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;

	
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI");   	
    	AutoresModificarCommand command = new AutoresModificarCommand();
    
    	command.setCodigo((String)request.getParameter("txhCodValor"));
    	command.setDescripcion((String)request.getParameter("txhDescripcion"));
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	command.setCodDetalle((String)request.getParameter("txhCodigo"));
    	command.setCodTipo((String)request.getParameter("txhCodTipo"));    	
    	//ACTUALIZADO NAPA
    	command.setCodPersona(CommonConstants.COD_PERSONA);
    	command.setCodEmpresa(CommonConstants.COD_EMPRESA);
        //log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	String resultado = "";
    	AutoresModificarCommand control = (AutoresModificarCommand) command;
    	log.info("OPERACION:"+control.getOperacion());
    	if (control.getOperacion().trim().equals("MODIFICAR")){    		
    		resultado = UpdateTablaDetalleBiblioteca(control);
    		log.info("RPTA:"+resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    		      else
    			  control.setMsg("OK");}
    	}
    	control.setOperacion("");
		//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_autores_modificar","control",control);		
    }
    
    private String UpdateTablaDetalleBiblioteca(AutoresModificarCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla("");
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_AUTOR);
    		obj.setDscValor2(control.getCodigo());
    		obj.setDscValor1(control.getCodDetalle());
    		obj.setDescripcion(control.getDescripcion());
    		obj.setEstReg("0");
    		obj.setUsuario(control.getCodEvaluador());
    		obj.setDscValor3(control.getCodTipo());
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		 	
    		obj.setCodTipoTablaDetalle(this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj));
    		
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
 
}
