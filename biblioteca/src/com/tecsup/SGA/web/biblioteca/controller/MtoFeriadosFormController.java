package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.web.evaluaciones.controller.RelacionarProductoHibridoFormController;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.common.Anios;

public class MtoFeriadosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoFeriadosFormController.class);
	
	BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		MtoFeriadosCommand command = new MtoFeriadosCommand();				
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	command.setSede((String)request.getParameter("txhSedeSelec"));
    	    	
    	if(!(command.getCodUsuario()==null)){
          	if(!command.getCodUsuario().trim().equals("")){
          		Anios anios= new Anios();          		
		    	
          		command.setFecha(this.biblioMantenimientoManager.GetAllFeriados(
          				(command.getSede().equals("U")?CommonConstants.TIPT_FERIADO_UTEC:CommonConstants.TIPT_FERIADO)
						, command.getMes(), CommonConstants.TIPT_PERIODO_ACTUAL));
		    	request.getSession().setAttribute("Fecha", command.getFecha());
		    	
		    	command.setCodListaMeses(anios.getMeses());
		    	command.setCodListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
          		}
          	}    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	MtoFeriadosCommand control = (MtoFeriadosCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{		Anios anios= new Anios();
    		
    		control.setFecha(this.biblioMantenimientoManager.GetAllFeriados(
    				(control.getSede().equals("U")?CommonConstants.TIPT_FERIADO_UTEC:CommonConstants.TIPT_FERIADO)
    				, control.getMeseses(), control.getAniosos()));
    		
    		request.getSession().setAttribute("Fecha", control.getFecha());
    		control.setCodListaMeses(anios.getMeses());
    		control.setCodListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
          
    	}
    	
    	if (control.getOperacion().trim().equals("ELIMINAR"))
    	{	Anios anios= new Anios();
    		resultado = eliminar(control);
    		log.info("RPTA:" + resultado);
    		control.setFecha(this.biblioMantenimientoManager.GetAllFeriados(
    				(control.getSede().equals("U")?CommonConstants.TIPT_FERIADO_UTEC:CommonConstants.TIPT_FERIADO)
    				, control.getMeseses(), control.getAniosos()));
    		request.getSession().setAttribute("Fecha", control.getFecha());
    		control.setCodListaMeses(anios.getMeses());
    		control.setCodListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
          
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else control.setMsg("OK");
    	}

    	if (control.getOperacion().trim().equals(""))
    	{
    		Anios anios= new Anios();	
    		control.setFecha(this.biblioMantenimientoManager.GetAllFeriados(
    				(control.getSede().equals("U")?CommonConstants.TIPT_FERIADO_UTEC:CommonConstants.TIPT_FERIADO)
    				, control.getMeseses(), control.getAniosos()));
    		request.getSession().setAttribute("Fecha", control.getFecha());
    		control.setCodListaMeses(anios.getMeses());
    		control.setCodListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
        
    	}
    	
    	
    	return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_mto_feriados","control",control);		
    }
    
    private String eliminar(MtoFeriadosCommand control){
    	
    	control.setEliminar(this.biblioMantenimientoManager.ActualizarFeriado(
    			(control.getSede().equals("U")?CommonConstants.TIPT_FERIADO_UTEC:CommonConstants.TIPT_FERIADO)
    			, control.getCod(), control.getDia(), control.getMess(), control.getAnio()
    			, control.getDescripcion(), control.getCodUsuario(), CommonConstants.DELETE_MTO_BIB));
    	return control.getEliminar();
    }
    
    private String meses(MtoFeriadosCommand control){
    	return control.getMess();
    }
    
}
