package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
//import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.AdminGestionMaterialHistorialPrestamosCommand;


public class AdminGestionMaterialHistorialPrestamosFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminGestionMaterialHistorialPrestamosFormController.class);
	
	//private BiblioMantenimientoManager biblioMantenimientoManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	/*public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}*/
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI"); 
    	AdminGestionMaterialHistorialPrestamosCommand control = new AdminGestionMaterialHistorialPrestamosCommand();
    		if(control.getOperacion()==null){
    				control.setTxhCodigoUnico(request.getParameter("codigoUnico"));
    				control.setTxtCodigo(request.getParameter("codigo"));
    				control.setTxtTipoMaterial(request.getParameter("tipoMaterial"));
    				control.setTxtTitulo(request.getParameter("titulo"));
    				control.setLstResultado(biblioGestionMaterialManager.getAllHistorialPrestamoxMaterial(control.getTxhCodigoUnico()));
    		}
        //log.info("formBackingObject:FIN");
        return control;
   }


	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	    	
    	AdminGestionMaterialHistorialPrestamosCommand control = (AdminGestionMaterialHistorialPrestamosCommand) command;    	
    	log.info("OPERACION: " + control.getOperacion());
    	
    	if("irConsultar".equals(control.getOperacion())){
    		control = irConsultar(request, response, errors, control);
    	}
    		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_gestion_material_historial","control",control);		
    }

	private AdminGestionMaterialHistorialPrestamosCommand irConsultar(HttpServletRequest request,
			HttpServletResponse response, BindException errors,
			AdminGestionMaterialHistorialPrestamosCommand control) {
			
		return control;
	}
}
