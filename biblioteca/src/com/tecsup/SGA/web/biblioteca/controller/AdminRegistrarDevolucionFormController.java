package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarDevolucionCommand;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarPrestamoCommand;

public class AdminRegistrarDevolucionFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AdminRegistrarDevolucionFormController.class);
	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	private BiblioProcesoSancionesManager biblioProcesoSancionesManager;
	
	public void setBiblioProcesoSancionesManager(   
			BiblioProcesoSancionesManager biblioProcesoSancionesManager) {
		this.biblioProcesoSancionesManager = biblioProcesoSancionesManager;
	}	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI"); 
        
        AdminRegistrarDevolucionCommand control = new AdminRegistrarDevolucionCommand();
        
        control.setSedeSel(request.getParameter("txhSedeSel"));
    	control.setSedeUsuario(request.getParameter("txhSedeUsuario"));
    	
    	if(control.getOperacion()==null)
    	{
    		control.setUsuario(request.getParameter("txhCodUsuario"));
    		control = cargaParametrosDeBusqueda(control,request);
    		control.setTxhFecHoy(Fecha.getFechaActual());
    	}   	
    	control.setTxhFlgSancion("");
    	control.setTipoUsuario(request.getParameter("txhTipoUsuario"));
    	//log.info("formBackingObject:FIN");
        return control;
   } 
	private AdminRegistrarDevolucionCommand cargaParametrosDeBusqueda(
			AdminRegistrarDevolucionCommand control, HttpServletRequest request) {
		
		control.setPrmTxtCodigo(request.getParameter("prmTxtCodigo"));
		control.setPrmTxtNroIngreso(request.getParameter("prmTxtNroIngreso"));
		control.setPrmCboTipoMaterial(request.getParameter("prmCboTipoMaterial"));
		control.setPrmCboBuscarPor(request.getParameter("prmCboBuscarPor"));
		control.setPrmTxtTitulo(request.getParameter("prmTxtTitulo"));
		control.setPrmCboIdioma(request.getParameter("prmCboIdioma"));
		control.setPrmCboAnioIni(request.getParameter("prmCboAnioIni"));
		control.setPrmCboAnioFin(request.getParameter("prmCboAnioFin"));
		control.setPrmTxtFechaReservaIni(request.getParameter("prmTxtFechaReservaIni"));
		control.setPrmTxtFechaReservaFin(request.getParameter("prmTxtFechaReservaFin"));
		
		return control;
	}

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	//log.info("onSubmit:INI");
    	AdminRegistrarDevolucionCommand control = (AdminRegistrarDevolucionCommand) command;
    	
    	control.setTxhFlgSancion("0");
    	control.setTxtSancionado("");
    	
    	log.info("OPERACION: " + control.getOperacion());
    	
    	if("irGetMaterial".equalsIgnoreCase(control.getOperacion()))
    	{    		
    		
    		//Es una devolucion individual o Grupal?    		
    		control = irGetMaterial(control,request,response);
    		
    		//En este punto ya se identific� si el prestamo del libro es individual o es m�ltiple.     		
    		if(control.getCodPrestamoMultiple()!= null && !control.getCodPrestamoMultiple().equals("")){    			
    			List<MaterialxIngresoBean> librosBloque = new ArrayList<MaterialxIngresoBean>();
    			librosBloque = biblioProcesoSancionesManager.listaBloqueLibrosPrestados(control.getCodPrestamoMultiple());
    			log.info("CODIGO MULTIPLE:" + control.getCodPrestamoMultiple());
    			log.info("CANTIDAD LIBROS PRESTADOS:" + librosBloque.size());
    			request.setAttribute("bloquePrestamo", librosBloque); 
    			
    			//si es devolucion individual... Limpiar campos de la p�gina de devoluci�n individual...
    			control.setTxtNroIngreso("");
    			control.setTxtTipomaterial("");
				control.setTxtTitulo("");
				control.setTxhCodMaterial("");
				control.setTxtFecPrestamo("");
				control.setTxtFecDevolucionProrroga("");				
				control.setTxtUsuarioNombre("");
				control.setTxtFecDevolucion("");
				control.setTxhCodUnico("");//CODIGO UNICO DEL LA DEVOLUCION						
				control.setTxhFlgPrestado("");
				control.setTxtCodDewey("");
				control.setChkIndPrestamo("");//ULTIMA VALIDACION
    		}
    		
    	}else if("irRegistrar".equalsIgnoreCase(control.getOperacion()))
    	{
    		String str = irRegistrar(control);
    		log.info("RPTA:" + str);
    		if( "0".equalsIgnoreCase(str) || "1".equalsIgnoreCase(str) )
    		{    		
    			clearForm(control);
    			request.setAttribute("mensaje", CommonMessage.GRABAR_EXITO);
    			
    		}else if("-1".equalsIgnoreCase(str))
    		{    		
    			request.setAttribute("mensaje", CommonMessage.GRABAR_ERROR);
    		}else 
    		{    		
    			tokenizer(control,str);
    			clearForm(control);
    			request.setAttribute("mensaje", control.getTxtSancionado());
    		}
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_devolucion","control",control);		
    }
    
	private void tokenizer(AdminRegistrarDevolucionCommand control, String str) {
		StringTokenizer tkn = new StringTokenizer(str,"|");
		String msg1 = "";
		String msg2 = "";
		String msg3 = "";
		String msg4 = "";
		
		if(tkn.hasMoreTokens())
			msg1 = tkn.nextToken();//2|
		if(tkn.hasMoreTokens())
			msg2 = tkn.nextToken();//NRO DE OCURRENCIAS ENCONTRADAS
		if(tkn.hasMoreTokens())
			msg3 = tkn.nextToken();//NRO DE DIAS DE PENALIDAD
		if(tkn.hasMoreTokens())
			msg4 = tkn.nextToken();	//FECHA FIN DE LA SANCION
		
		String mensaje = "Al usuario se le penalizara con "+msg3+" dias y su fecha fin de sancion es "+msg4;
		control.setTxhFlgSancion("1");
		control.setTxtSancionado(mensaje);
		
	}
	
	private String irRegistrar(AdminRegistrarDevolucionCommand control) {
				
		String str = "";
		
		str =  this.biblioProcesoSancionesManager.
				insertSancion(
				control.getTxtNroIngreso(),//.getTxhCodUnico(),
				control.getTxtFecDevolucion(),
				control.getUsuario(),
				control.getSedeSel(),"1"				
				);
		
		log.info("control.getChkIndProrroga(): " +  control.getChkIndProrroga());
		log.info("str=> " + str);
		
		if( ("1".equalsIgnoreCase(control.getChkIndProrroga())) && ("0".equalsIgnoreCase(str) || "1".equalsIgnoreCase(str)) )
		{
			
			String respPrestamo =  (String)this.biblioGestionMaterialManager.
			insertPrestamoxAlumno
			(
					control.getTxhCodMaterial(),
					control.getTxtNroIngreso(),
					control.getTxhCodUsuarioLogin().trim(),
					(control.getChkIndProrroga()==null?"":control.getChkIndProrroga()),
					control.getUsuario(),
					control.getChkIndPrestamo()==null?"1":control.getChkIndPrestamo(),// Prestamo en sala
					control.getTxhFlgReserva(),
					control.getTxtFecPrestamo(),
					null
			);
						
		}
		
		return str;
		
	}
	
	private AdminRegistrarDevolucionCommand irGetMaterial(
			AdminRegistrarDevolucionCommand control,HttpServletRequest request, HttpServletResponse response) {
		
		//log.info("PARAMETROS PARA EL SP SP_SEL_MATERIAL_BIB_X_INGRESO ");
		//log.info("E_V_CODIGO:|"+control.getTxtNroIngreso()+"|");
		//log.info("E_V_TIPO:|"+CommonConstants.FLG_TIPO_DEVOLUCION+"|");
		log.info("Sede Sel:" + control.getSedeSel());
		log.info("Nro.Ingreso Ingresado A: "  + control.getTxtNroIngreso());
		
		Long numIngresoReal = null;
		if(control.getSedeSel().equals("A")){
			//Lectura con pistola
//			numIngresoReal = Long.valueOf(control.getTxtNroIngreso()) + 2000000;
			numIngresoReal = Long.valueOf(control.getTxtNroIngreso());
		}else if(control.getSedeSel().equals("T")){
			//Lee sin pistola
			numIngresoReal = Long.valueOf(control.getTxtNroIngreso()) + 3000000;
		}else if(control.getSedeSel().equals("U")){
			//Lima lee sin pistola
			numIngresoReal = Long.valueOf(control.getTxtNroIngreso()) + 5000000;
		}else
			numIngresoReal = Long.valueOf(control.getTxtNroIngreso());
		control.setTxtNroIngreso(numIngresoReal.toString());
		log.info("Nro.Ingreso Ingresado B: "  + control.getTxtNroIngreso());
		
		String estadoIngreso = this.biblioGestionMaterialManager.getEstadoIngreso(numIngresoReal.toString());
		if(estadoIngreso.equals("I")){
			request.setAttribute("mensaje","El libro esta dado de Baja.");
			clearForm(control);
			return control;
		}else if (estadoIngreso.equals("")){
			request.setAttribute("mensaje","Nro de secuencia No existe.");
			clearForm(control);
			return control;
		}
			
		
		MaterialxIngresoBean materialxIngresoBean = 
			biblioGestionMaterialManager.getMaterialxIngreso(
					control.getTxtNroIngreso(),
					CommonConstants.FLG_TIPO_DEVOLUCION,
					control.getSedeSel(), control.getCodUsuario()
					);
		
		if(materialxIngresoBean!=null)
		{
			
			//HA SIDO PRESTADO
			if(materialxIngresoBean.getFlgPrestado().equalsIgnoreCase("1"))
			{								
				control.setCodPrestamoMultiple(materialxIngresoBean.getCodPrestamoMultiple());
				control.setTxtTipomaterial(materialxIngresoBean.getDesTipoMaterial());
				control.setTxtTitulo(materialxIngresoBean.getTitulo());
				control.setTxhCodMaterial(materialxIngresoBean.getCodMaterialBib());
				control.setTxtFecPrestamo(materialxIngresoBean.getFecPrestamo());
				control.setTxtFecDevolucionProrroga(materialxIngresoBean.getFecDevolucionProg());
				
				control.setTxtUsuarioNombre(materialxIngresoBean.getNombreUsuario());
				control.setTxtFecDevolucion(materialxIngresoBean.getFecDevolucion());
				//control.setTxtSancionado(materialxIngresoBean.getObservacion());
				control.setTxhCodUnico(materialxIngresoBean.getCodUnico());//CODIGO UNICO DEL LA DEVOLUCION
				//control.setChkIndProrroga(materialxIngresoBean.getIndProrroga());			
				control.setTxtMensaje(
						materialxIngresoBean.getFlgSancion().equalsIgnoreCase("1")?CommonMessage.LIBRO_SANCIONADO:CommonMessage.LIBRO_PERMITIDO);
				//control.setTxhFlgSancion(materialxIngresoBean.getFlgSancion());
				control.setTxhFlgPrestado(materialxIngresoBean.getFlgPrestado());
				control.setTxtCodDewey(materialxIngresoBean.getCodDewey());
				control.setChkIndPrestamo(materialxIngresoBean.getIndTipoPrestamo());//ULTIMA VALIDACION
				//control.setTxtUsuariologin(materialxIngresoBean.getCodUsuario());
				control.setTxtUsuariologin(materialxIngresoBean.getNomLoginUsuPrestamo());
				control.setTxhCodUsuarioLogin(materialxIngresoBean.getCodUsuario());	
				control.setTxhSedeMat(materialxIngresoBean.getSede());
				
				//Calculando total de Prorrogas
				int totalProrrogas = 0;
				List prorrogas = biblioGestionMaterialManager.getAllHistorialProrrogaxMaterial(control.getTxtNroIngreso(), control.getTxhCodUsuarioLogin(),control.getSedeSel());
				if(prorrogas != null)
					totalProrrogas = prorrogas.size() - 1;
				
				control.setTotalProrroga(String.valueOf(totalProrrogas));
				
				if(biblioGestionMaterialManager.getPermisoParaProrroga(control.getTxhCodUsuarioLogin(), totalProrrogas))
					control.setFlagProrroga("1");
				else
					control.setFlagProrroga("0");
				
				
				if (!control.getTxhSedeMat().equals(control.getSedeSel())){
					request.setAttribute("mensaje","El libro prestado pertenece a una sede diferente.");
					clearForm(control);
				}
				
			}else{
				control.setTxhFlgPrestado("0");
				request.setAttribute("mensaje",CommonMessage.LIBRO_NO_PRESTADO);
			}

			
		}else{
			control.setTxhFlgPrestado("0");
			request.setAttribute("mensaje",CommonMessage.LIBRO_NO_PRESTADO);
			//request.setAttribute("mensaje", "El nro. de ingreso digitado no se encuentra registrado en el sistema");
			clearForm(control);
		}
		return control;
	}

	private void clearForm(AdminRegistrarDevolucionCommand control) {
		control.setTxtNroIngreso(null);
		control.setTxtTipomaterial(null);
		control.setTxtTitulo(null);
		control.setTxhCodMaterial(null);
		control.setTxtFecPrestamo(null);
		control.setTxtFecDevolucionProrroga(null);		
		control.setTxtUsuarioNombre(null);
		control.setTxtFecDevolucion(null);
		//control.setTxtSancionado(null);
		control.setTxhCodUnico(null);
		//control.setChkIndProrroga(null);
		control.setTxtMensaje(null);
		//control.setTxhFlgSancion("0");
		control.setTxhFlgPrestado(null);
		control.setTxtCodDewey(null);
		control.setTxhFlgReserva(null);
		control.setChkIndPrestamo(null);
		control.setChkIndProrroga(null);
		control.setTxtUsuariologin(null);
		control.setTxhCodUsuarioLogin(null);
	}

}