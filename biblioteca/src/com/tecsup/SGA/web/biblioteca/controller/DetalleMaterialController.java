package com.tecsup.SGA.web.biblioteca.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialCommand;

public class DetalleMaterialController  implements Controller  {
	
	private static Log log = LogFactory.getLog(DetalleMaterialController.class);
	
	BiblioGestionMaterialManager biblioGestionMaterialManager;
	
    public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    			    	    	
    	ModelMap model = new ModelMap();
    	
    	String txhCodigoUnico = request.getParameter("txhCodigoUnico");    	
    	String flag = request.getParameter("flgView");
    	String strUrl = request.getParameter("strUrl");
    	String strUrlAntes = "";    	
    	String resp = "";    	
    	if(request.getParameter("txhCodigoUnico")!=null){
    		    		
    		AdminRegistrarMaterialCommand control = new AdminRegistrarMaterialCommand();
    		
    		control.setTxhCodUnico(txhCodigoUnico);    		
        	//flgView = 1 --> viene de busq avanzada flotante 
        	request.setAttribute("flg", flag);
        	
        	if (strUrl!=null){
        		strUrlAntes = strUrl;
        		strUrl = strUrl.replace("|", "&");
        	}
        	request.setAttribute("strUrlNuevo", strUrl);
        	request.setAttribute("strUrl", strUrlAntes);
    		/**/
    		
    		request.setAttribute("OBJResultado", biblioGestionMaterialManager.getAllDetalleMaterialFichaBibliografica(txhCodigoUnico));
    		model.addObject("OBJControl",control);
    		control.getTxtTitulo();
    		
    		if("0".equalsIgnoreCase(resp))
    		{
    			request.setAttribute("mensaje","OK");
    		}else{
    			request.setAttribute("mensaje","ERROR");
    		}	
    	}
    	
    	
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_gestion_material_detalle_material", "model", model);
	}



}
