package com.tecsup.SGA.web.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarDevolucionCommand;

public class AdminSupensionServiciosFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(AdminSupensionServiciosFormController.class);
	private BiblioProcesoSancionesManager biblioProcesoSancionesManager;
	private TablaDetalleManager tablaDetalleManager;
	/**
	 * @param tablaDetalleManager the tablaDetalleManager to set
	 */
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	/**
	 * @param biblioProcesoSancionesManager the biblioProcesoSancionesManager to set
	 */
	public void setBiblioProcesoSancionesManager(
			BiblioProcesoSancionesManager biblioProcesoSancionesManager) {
		this.biblioProcesoSancionesManager = biblioProcesoSancionesManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ServletException {
		AdminRegistrarDevolucionCommand control = new AdminRegistrarDevolucionCommand();
						
    	if(control.getOperacion()==null){
    		control.setTxtUsuariologin(request.getParameter("txhIdUsuario"));
    		control.setTxtUsuarioNombre(request.getParameter("txhNomUsuario"));
    		control.setUsuario(request.getParameter("txhCodUsuarioAcc")); 
    		control.setCodUsuario(request.getParameter("txhCodUsuario"));    		
    	}   
    	    	
    	List suspensiones=this.tablaDetalleManager.getAllTablaDetalle("0129", "", 
    			"", "M",	"", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(suspensiones!=null)
    		control.setListaSuspenciones(suspensiones);
    	else
    		control.setListaSuspenciones(new ArrayList());
    	
    	request.setAttribute("listaSuspenciones", control.getListaSuspenciones());
		return control;
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		AdminRegistrarDevolucionCommand control = (AdminRegistrarDevolucionCommand) command;
		
		log.info("OPERACION:" + control.getOperacion());
		
		if("SUSPENDER".equalsIgnoreCase(control.getOperacion())){
			System.out.println("Cadena Sanciones:" + control.getCodSancionesSeleccionados());
			System.out.println("Cadena getNroSelec:" + control.getNroSelec());
			System.out.println("codUsuario:" + control.getCodUsuario());
			System.out.println("codUsuario Acc:" + control.getUsuario());
			System.out.println("fecIniInhabi:" + control.getFecIniInhabi());
			System.out.println("fecFinInhabi:" + control.getFecFinInhabi());
			System.out.println("obsInhabi:" + control.getObsInhabi());
			
			String rpta = this.biblioProcesoSancionesManager.insertarInhabilitaciones(control.getCodUsuario(),
					control.getCodSancionesSeleccionados(), control.getNroSelec(), 
					control.getFecIniInhabi(), control.getFecFinInhabi(), control.getObsInhabi(),control.getUsuario());
			
			if ( rpta.equals("-1")) request.setAttribute("Msg", "ERROR");
			else  
				request.setAttribute("Msg", "OK");
				
			control.setOperacion("");
			
		}
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_suspensiones","control",control);
	}
		
		
}
