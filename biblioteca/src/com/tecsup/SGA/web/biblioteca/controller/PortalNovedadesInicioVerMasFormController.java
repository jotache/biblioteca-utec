package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.PortalNovedadesInicioVerMasCommand;

public class PortalNovedadesInicioVerMasFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PortalNovedadesInicioVerMasFormController.class);
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		PortalNovedadesInicioVerMasCommand command= new PortalNovedadesInicioVerMasCommand();
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	String codigo=(String)request.getParameter("txhCodigo");
    	LlenarDatos(command, codigo);

    	return command;
	}
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }

    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	PortalNovedadesInicioVerMasCommand control= (PortalNovedadesInicioVerMasCommand) command;
    	return new ModelAndView("/biblioteca/userAdmin/buzonSugerencias/bib_portal_novedades_inicio","control",control);
    }
    
    public void LlenarDatos(PortalNovedadesInicioVerMasCommand control, String codigo){
    	
    	Secciones seccion= new Secciones();
    	seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_NOVEDADES);
    	TipoTablaDetalle obj= new TipoTablaDetalle();
    	obj.setCodTipoTablaDetalle("");
		List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
    	if(lista!=null)
    	{
    		if(lista.size()>0)
    		{	for(int i=0;i<lista.size();i++)
		    	{ seccion=(Secciones)lista.get(i);
		    	  if(codigo.equals(seccion.getCodUnico()))
		    	 i=lista.size();
		    	}
		    	control.setTema(seccion.getTema());
		    	control.setDescripcionGrupo(seccion.getDescripcionGrupo());
		    	control.setDescripcionLarga(seccion.getDescripcionLarga());
		    	control.setImagen(seccion.getArchivo());
		    	control.setUrl(seccion.getUrl());
		    	
		    	if (control.getUrl()!=null && "&nbsp;".equals(control.getUrl())){
		    		control.setUrl(null);
		    	}
		    	
		    	control.setDocumentoNov(seccion.getDocumentoNov());
		    	
		    	if((seccion.getDocumentoNov()!=null)&&(!seccion.getDocumentoNov().equals("")))
		    	{	
		    		String [] stkCadena = seccion.getDocumentoNov().split("\\|");
					control.setNombreFile(stkCadena[0]);
					control.setNombreOriginal(stkCadena[1]);
		    	}
		    	
		    	if((seccion.getExtDocNov()!=null)&&(!seccion.getExtDocNov().equals("")))
		    	{	
		    		if(seccion.getExtDocNov().equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_DOC) || seccion.getExtDocNov().equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_DOCX)) control.setExtDocNov("doc.gif");
				    else{ if(seccion.getExtDocNov().equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_XLS) || seccion.getExtDocNov().equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_XLSX)) control.setExtDocNov("excel.gif");
				    	  else if(seccion.getExtDocNov().equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_PDF)) control.setExtDocNov("pdf.gif");
				    	}
		    	}
		    	
		    	if((seccion.getArchivo()!=null)&&(!seccion.getArchivo().equals("")))
		    	{	
		    		/*String uploadDirArchivo ="\\\\"+CommonConstants.SERVIDOR_NAME +
		    		CommonConstants.STR_RUTA_SECCIONES+seccion.getArchivo();*/
		    		String uploadDirArchivo = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+
		    			CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_NOVEDADES+seccion.getArchivo();
		    		
		    				    		
		    		control.setArchivo(uploadDirArchivo);
		    	}
		    	
		    	//String rutaArchivos = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_SECCIONES;
		    	String rutaArchivos = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+
    				CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_NOVEDADES;
		    			    	
		    	control.setRutaArchivos(rutaArchivos);
    		}
    	}
    	 	
    }
}
