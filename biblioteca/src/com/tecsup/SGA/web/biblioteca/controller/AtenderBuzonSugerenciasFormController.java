package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.web.evaluaciones.controller.RelacionarProductoHibridoFormController;
import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ConstantesCuerpoMensajes;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.BuzonSugerencia;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.AdminBuzonSugerenciasManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;

public class AtenderBuzonSugerenciasFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AtenderBuzonSugerenciasFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private AdminBuzonSugerenciasManager adminBuzonSugerenciasManager;
	private TablaDetalleManager tablaDetalleManager;
	
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public AdminBuzonSugerenciasManager getAdminBuzonSugerenciasManager() {
		return adminBuzonSugerenciasManager;
	}

	public void setAdminBuzonSugerenciasManager(
			AdminBuzonSugerenciasManager adminBuzonSugerenciasManager) {
		this.adminBuzonSugerenciasManager = adminBuzonSugerenciasManager;
	}

	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		
		AtenderBuzonSugerenciasCommand command = new AtenderBuzonSugerenciasCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
		command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));				
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion()); 
    	

		command.setCodSelec((String)request.getParameter("txhCodSelec"));
		command.setFechaRegistro((String)request.getParameter("txhFechaRegistro"));				
		command.setTipoSugerencia((String)request.getParameter("txhTipoSugerencia"));
		command.setComentario((String)request.getParameter("txhComentario"));
		command.setCalificacionRespuesta((String)request.getParameter("txhCalRespuesta"));				
		command.setTipoMaterial((String)request.getParameter("txhTipoMaterial"));
		command.setUsuario((String)request.getParameter("txhUsuario"));
		command.setEmail((String)request.getParameter("txhEmail"));
		
		//System.out.println("Load correo del usuario:"+command.getEmail());
		
		command.setConsteBuzonConsulta(CommonConstants.TIPT_COD_CONSULTA);
		command.setConsteBuzonComentario(CommonConstants.TIPT_COD_COMENTARIO);
		command.setConsteBuzonSugerencia(CommonConstants.TIPT_COD_SUGERENCIA);
		command.setConsteBuzonReclamo(CommonConstants.TIPT_COD_RECLAMO);
    	
    	request.setAttribute("lstrURL", command.getLstrURL());
    	//log.info("txhCodSelec: "+command.getCodSelec());
    	//log.info("txhFechaRegistro: "+command.getFechaRegistro());
    	//log.info("txhTipoSugerencia: "+command.getTipoSugerencia());
    	
    	BuzonSugerencia buzon = adminBuzonSugerenciasManager.obtenerSugerencia(command.getCodSelec());
    	
    	//JHPR 2008-7-30
    	//log.info("txhComentario: "+command.getComentario());
    	//log.info("txhComentario: "+buzon.getDescripcion());
    	
    	command.setComentario(buzon.getDescripcion());
    	
    	//log.info("txhCalRespuesta: "+command.getCalificacionRespuesta());
    	//log.info("txhTipoMaterial: "+command.getTipoMaterial());
    	//log.info("txhUsuario: "+command.getUsuario());
    	LlenarComboCalificacion(command);
    	LlenarComboTipoMaterial(command);
    	
    	if(command.getTipoSugerencia()!=null)
    		buscarTipoSugerencia(command);
    	
    	//log.info("formBackingObject:FIN getCodListaCalificacion: "+command.getCodListaCalificacion().size()+
    	//		"getCodListaTipoMaterial: "+command.getCodListaTipoMaterial());
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	
    	AtenderBuzonSugerenciasCommand control = (AtenderBuzonSugerenciasCommand)command;  
    	log.info("Operacion: "+control.getOperacion());
    	
		if(control.getOperacion().equals("ENVIAR"))
		{  //System.out.println("Dentro del EnviarRespuesta");
		   String resultado="";
		   //System.out.println("Correo del usuario:"+control.getEmail());
		   resultado=EnviarRespuesta(control);
		   log.info("RPTA:"+resultado);
		   control.setMsg(resultado);
		   if(resultado.trim().equals("0")) 
		   {  control.setMsg("OK");
			  ServletEnvioCorreo servletEnvioCorreo= new ServletEnvioCorreo();
		   	  String mensaje="";
		   	  String asunto="";
		   	  List lista1= new ArrayList();
		   	  lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_BIBLIOTECA_EMAIL 
	    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
		   	  if(lista1!=null)
		   	  { if(lista1.size()>1)
		   	  	{ TipoTablaDetalle TTDetalle1= new TipoTablaDetalle();
		   	      TipoTablaDetalle TTDetalle2= new TipoTablaDetalle();
		   		  /*if(lista1.size()>1) 
		   		  {*/ //System.out.println("Dentro dela lista");
		   			TTDetalle1=(TipoTablaDetalle)lista1.get(0);
		   		    asunto=TTDetalle1.getDescripcion().trim();
		   		    TTDetalle2=(TipoTablaDetalle)lista1.get(1);
		   		    mensaje=TTDetalle2.getDescripcion().trim();
		   		    mensaje=mensaje.replace("<NOMBRE>", control.getUsuario().trim()+"\n");
		   		    mensaje=mensaje.replace("<TIPO_SUGENCIA>", control.getTipoSugerencia().trim());
		   		    mensaje=mensaje.replace("<FEC_REGISTRO>", control.getFechaRegistro().trim()); 
		   		    mensaje=mensaje.replace("<ATENCION>", "\n"+control.getTextoRespuesta().trim()+"\n"+"\n");
		   		    mensaje=mensaje.replace("servirle mejor.", "servirle mejor."+"\n");
		   		    mensaje=mensaje.replace("Atentamente,", "Atentamente \n");
		   		    /*log.info(">>getEmail<<"+control.getEmail()+">>TECSUP_EMAIL<<"+
		   		    		CommonConstantsEmail.TECSUP_EMAIL_BIBLIOTECA+">>getUsuario<<"+ 
	    	    			control.getUsuario()+">>TECSUP_BIBLIOTECA_EMAIL_BUZON<<"+
	    	    			CommonConstantsEmail.TECSUP_BIBLIOTECA_EMAIL_BUZON+">>asunto<<"+ 
	        	    		asunto+">>mensaje<<"+mensaje);*/
		   		    //System.out.println("Asunto:"+"\n"+asunto);
		   		    try{
		    	    	servletEnvioCorreo.EnviarCorreo(control.getEmail(), CommonConstantsEmail.TECSUP_EMAIL_BIBLIOTECA, 
		    	    			control.getUsuario(), CommonConstantsEmail.TECSUP_BIBLIOTECA_EMAIL_BUZON, 
		        	    		asunto, mensaje);
		        
		    	    	//log.info("Texto a enviar en el Correo(TryCatch): \n"+mensaje);
		        	   
				   	  }
				   	  catch(Exception ex){
		    	    }
		   	  	}  
		   		  	  
		   	  	//}
		   	  }
		   	 //System.out.println("Texto a enviar en el Correo: \n"+mensaje);
		   	  /*try{
    	    	servletEnvioCorreo.EnviarCorreo(control.getEmail(), CommonConstantsEmail.TECSUP_EMAIL, 
    	    			control.getUsuario(), CommonConstantsEmail.TECSUP_BIBLIOTECA_EMAIL_BUZON, 
        	    		asunto, mensaje);
        
        	  System.out.println("Texto a enviar en el Correo: \n"+mensaje);
        	   
		   	  }
		   	  catch(Exception ex){
    	    	
    	    }*/
		   }   
		}	
    	
    	//log.info("onSubmit:FIN");
    	return new ModelAndView("/biblioteca/userAdmin/buzonSugerencias/bib_atender_buzon_sugerencias","control",control);		
    }
    
    public void LlenarComboCalificacion(AtenderBuzonSugerenciasCommand control){
   	 
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	        obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_CALIFICACION_SUGERENCIA);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setCodListaCalificacion(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
			TipoTablaDetalle mad= new TipoTablaDetalle();
			for(int i=0;i<control.getCodListaCalificacion().size();i++)
            {  mad=(TipoTablaDetalle)control.getCodListaCalificacion().get(i);
               if(mad.getDescripcion().equals(control.getCalificacionRespuesta())){
            	   control.setCalificacionRespuesta(mad.getCodTipoTablaDetalle());
            	   i=control.getCodListaCalificacion().size();
               }
            }
    }//setCalificacionRespuesta
    
    public void LlenarComboTipoMaterial(AtenderBuzonSugerenciasCommand control){
   	 
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	        obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setCodListaTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
			TipoTablaDetalle mad= new TipoTablaDetalle();
			for(int i=0;i<control.getCodListaTipoMaterial().size();i++)
            {  mad=(TipoTablaDetalle)control.getCodListaTipoMaterial().get(i);
               if(mad.getDescripcion().equals(control.getTipoMaterial())){
            	   control.setTipoMaterial(mad.getCodTipoTablaDetalle());
            	   i=control.getCodListaTipoMaterial().size();
               }
            }
    }
   
   public String EnviarRespuesta(AtenderBuzonSugerenciasCommand control){
	   //UpdateAtenderBuzonSugerencia(obj.getCodSugerencia(),
		//obj.getCodCalificacion(), obj.getCodigoMaterial(), obj.getRespuesta());
	   String resultado="";
	   BuzonSugerencia buzonSugerencia = new BuzonSugerencia();
	   buzonSugerencia.setCodSugerencia(control.getCodSelec());
	   buzonSugerencia.setCodCalificacion(control.getCalificacionRespuesta());
	   buzonSugerencia.setCodigoMaterial(control.getTipoMaterial());
	   
	   String rpta="";
	   if (control.getTextoRespuesta().trim().length()>1000)
		   rpta = control.getTextoRespuesta().trim().substring(0, 1000);
	   else
		   rpta = control.getTextoRespuesta().trim();
	   
	   //System.out.println("rpta:"+rpta);
	   buzonSugerencia.setRespuesta(rpta);
	   resultado=this.adminBuzonSugerenciasManager.UpdateAtenderBuzonSugerencia(buzonSugerencia);
	  
	   //log.info("El VALOR de Enviar la respuesta es: "+resultado);
	   return resultado;
   } 
    
   public void buscarTipoSugerencia(AtenderBuzonSugerenciasCommand control){
	   
	    	TipoTablaDetalle obj= new TipoTablaDetalle();
		    
	    	obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SUGERENCIA);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			List listatipoSugerencia=this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj);
			if((listatipoSugerencia!=null)&& (listatipoSugerencia.size()>0))
			{   TipoTablaDetalle sk= new TipoTablaDetalle();
				for(int a=0;a<listatipoSugerencia.size();a++)
				{ sk=(TipoTablaDetalle)listatipoSugerencia.get(a);
				  if(sk.getDescripcion().equals(control.getTipoSugerencia()))
				   {  control.setCodSelTipoSugerencia(sk.getCodTipoTablaDetalle());
				      a=listatipoSugerencia.size();}
				}
			}	
	    } 
	   
	   
   
}
