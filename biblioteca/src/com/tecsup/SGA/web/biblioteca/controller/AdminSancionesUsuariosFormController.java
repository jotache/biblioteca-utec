package com.tecsup.SGA.web.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Sancion;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarDevolucionCommand;

public class AdminSancionesUsuariosFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(AdminSancionesUsuariosFormController.class);
	private BiblioProcesoSancionesManager biblioProcesoSancionesManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public void setBiblioProcesoSancionesManager(BiblioProcesoSancionesManager biblioProcesoSancionesManager) {
		this.biblioProcesoSancionesManager = biblioProcesoSancionesManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ServletException {
		AdminRegistrarDevolucionCommand control = new AdminRegistrarDevolucionCommand();
		control.setSedeSel(request.getParameter("txhSedeSel"));
    	control.setSedeUsuario(request.getParameter("txhSedeUsuario"));
    	if(control.getOperacion()==null){
    		control.setUsuario(request.getParameter("txhCodUsuario"));
    		control = cargaParametrosDeBusqueda(control,request);    		
    	}   
		return control;
	}
	private AdminRegistrarDevolucionCommand cargaParametrosDeBusqueda(AdminRegistrarDevolucionCommand control, HttpServletRequest request) {		
		control.setPrmTxtCodigo(request.getParameter("prmTxtCodigo"));
		control.setPrmTxtNroIngreso(request.getParameter("prmTxtNroIngreso"));
		control.setPrmCboTipoMaterial(request.getParameter("prmCboTipoMaterial"));
		control.setPrmCboBuscarPor(request.getParameter("prmCboBuscarPor"));
		control.setPrmTxtTitulo(request.getParameter("prmTxtTitulo"));
		control.setPrmCboIdioma(request.getParameter("prmCboIdioma"));
		control.setPrmCboAnioIni(request.getParameter("prmCboAnioIni"));
		control.setPrmCboAnioFin(request.getParameter("prmCboAnioFin"));
		control.setPrmTxtFechaReservaIni(request.getParameter("prmTxtFechaReservaIni"));
		control.setPrmTxtFechaReservaFin(request.getParameter("prmTxtFechaReservaFin"));		
		return control;
	}	  
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		//log.info("onSubmit:INI");
		AdminRegistrarDevolucionCommand control = (AdminRegistrarDevolucionCommand) command;
		log.info("OPERACION:"+ control.getOperacion());
		if("BUSCARSANCIONES".equalsIgnoreCase(control.getOperacion())){
			String codUsuario = control.getCodUsuario();
			control.setListaSancionesUsuario(obtenerSanciones(codUsuario));
			request.setAttribute("listaSanciones", control.getListaSancionesUsuario());			
			
		}else if("LEVANTARSANCION".equalsIgnoreCase(control.getOperacion())){
			String codUsuario = control.getCodUsuario();
			String rptaBD = this.biblioProcesoSancionesManager.updateSancionUsuario(codUsuario, control.getIdSancion(), control.getUsuario());
			
			if( "0".equalsIgnoreCase(rptaBD))
    			request.setAttribute("mensaje", "La sanci�n ha sido levantada");    			
    		else if("-1".equalsIgnoreCase(rptaBD))     		
    			request.setAttribute("mensaje", CommonMessage.GRABAR_ERROR);
			
			control.setListaSancionesUsuario(obtenerSanciones(codUsuario));
			request.setAttribute("listaSanciones", control.getListaSancionesUsuario());
			
		}else if("BUSCARUSUARIO".equalsIgnoreCase(control.getOperacion())){
			String usuario = control.getTxtUsuariologin();			
			//Ultimo parametro "tipoBusquedaCodUsu": '0'=El cogido enviado es codigo oracle, '1'=El codigo es enviado es el username
			AlumnoXCodigoBean alumnoXCodigoBean = biblioGestionMaterialManager.getAlumnoXCodigo(usuario, "0","1",control.getSedeSel(),"");
			if (alumnoXCodigoBean!=null){
				control.setCodUsuario(alumnoXCodigoBean.getCodUsuario());
				control.setTxtUsuarioNombre(alumnoXCodigoBean.getNombre());
				control.setTxhCodUsuarioLogin(alumnoXCodigoBean.getUsuario());
			}else{
				control.setCodUsuario("");
				control.setTxtUsuarioNombre("");
				control.setTxhCodUsuarioLogin("");
				control.setIdSancion("");
				request.setAttribute("mensaje", "Usuario no encontrado");
			}
				
		}
		
		//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_sanciones","control",control);
	}
	    
    private List<Sancion> obtenerSanciones(String codUsuario){
    	List<Sancion> listaSancionesUsuario = new ArrayList<Sancion>();
    	listaSancionesUsuario = biblioProcesoSancionesManager.getSancionesUsuario(codUsuario);
    	return listaSancionesUsuario;
    }
}

