package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.ReservasByMaterial;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminConsultarReservasMaterialCommand;

public class AdminConsultarReservasMaterialFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AdminConsultarReservasMaterialFormController.class);
	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI"); 
    	
    	AdminConsultarReservasMaterialCommand command= new AdminConsultarReservasMaterialCommand();
    	
    	command.setUsuario((String)request.getParameter("codUsuario"));
    	command.setCodMaterial((String)request.getParameter("codMaterial"));
    	
    	if (command.getCodMaterial() != null){
    		BuscarBandeja(command, request, command.getCodMaterial());
    	}
    	
        //log.info("formBackingObject:FIN");
        return command;
   }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	
    	AdminConsultarReservasMaterialCommand control= (AdminConsultarReservasMaterialCommand) command;
    	log.info("OPERACION: "+control.getOperacion()+"||");
    	String rpta ="";
    	
    	if("ELIMINAR".equals(control.getOperacion())){
    		rpta = EliminarReserva(control, control.getCadena(), control.getUsuario());
    		log.info("RPTA:" + rpta);
    		if("-1".equalsIgnoreCase(rpta)){    			
    			request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);
    		}else if("0".equalsIgnoreCase(rpta)){
    			request.setAttribute("mensaje",CommonMessage.ELIMINAR_EXITO);
    			BuscarBandeja(control, request, control.getCodMaterial());
    		}
        }    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/bib_admin_consultar_reservas_material","control",control);		
    }
    
    public void BuscarBandeja(AdminConsultarReservasMaterialCommand control, HttpServletRequest request, 
    			String codMaterial){
    	
    	List lista= new ArrayList();
    	lista = this.biblioGestionMaterialManager.getAllReservaByMaterial(codMaterial);
    	
    	request.getSession().setAttribute("listaBandeja", lista);
    	
    	if(lista!=null){
    		if (lista.size()>0){
    			ReservasByMaterial reserva;
    			reserva = (ReservasByMaterial)lista.get(0);
    			
    			control.setCodigo(reserva.getCodigo());
    			control.setTipoMaterial(reserva.getTipoMaterial());
    			control.setTitulo(reserva.getTitulo());
    		}
    	}
    }
    
    public String EliminarReserva(AdminConsultarReservasMaterialCommand control, String cadena, String usuario){
    	return this.biblioGestionMaterialManager.deleteReservasMaterial(cadena, usuario);
    }
    
}
