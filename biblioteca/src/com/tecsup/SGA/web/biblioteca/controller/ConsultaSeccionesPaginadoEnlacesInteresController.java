package com.tecsup.SGA.web.biblioteca.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.UsuarioSeguridad;

public class ConsultaSeccionesPaginadoEnlacesInteresController  implements Controller  {
	
	private static Log log = LogFactory.getLog(ConsultaSeccionesPaginadoEnlacesInteresController.class);
	
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	//log.info("handleRequest:INI");
    	String codUsuario = "";
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){
    		//command.setCodEvaluador(usuarioSeguridad.getIdUsuario());
    		codUsuario = usuarioSeguridad.getIdUsuario();
    	}
    	request.setAttribute("codusuario_consultasecciones", codUsuario);
    	//codUsuario = request.getParameter("codusuario");
    	//System.out.println("ConsultaSeccionesPaginadoEnlacesInteresController:"+codUsuario); 	    	
    	ModelMap model = new ModelMap();    	
	   	
	   //	log.info("handleRequest:FIN");
		return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_secciones_paginado_enlaces_interes", "model", model);
	}
}
