package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.web.biblioteca.command.PortalRevistaIiCommand;

public class PortalRevistaIiFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(PortalRevistaIiFormController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    	throws ServletException {			
		PortalRevistaIiCommand command= new PortalRevistaIiCommand();    			
    	return command;
	}
}
