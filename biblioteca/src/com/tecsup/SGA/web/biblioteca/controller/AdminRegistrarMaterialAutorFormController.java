package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialAutorCommand;


public class AdminRegistrarMaterialAutorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialAutorFormController.class);
	
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
    public AdminRegistrarMaterialAutorFormController() {
        super();        
        setCommandClass(AdminRegistrarMaterialAutorCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)	
    throws ServletException{
    	//log.info("formBackingObject:INI"); 
    	AdminRegistrarMaterialAutorCommand control = new AdminRegistrarMaterialAutorCommand();
    	control.setTxhCodUsuario(request.getParameter("txhCodUsuario"));
    	
		if(control.getOperacion()==null){
			
		}
		
		control.setCodPersona(CommonConstants.COD_PERSONA);
		control.setCodEmpresa(CommonConstants.COD_EMPRESA);
        //log.info("formBackingObject:FIN");
        return control;
   }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AdminRegistrarMaterialAutorCommand control = (AdminRegistrarMaterialAutorCommand) command;
    	
    	if("irConsultar".equalsIgnoreCase(control.getOperacion())){
    		control = irConsultar(control);    		
    	}
    	
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_autor","control",control);		
    }

	private AdminRegistrarMaterialAutorCommand irConsultar(AdminRegistrarMaterialAutorCommand control){
		log.info("INPUT|"+control.getTxtCodigo()+"|");
		log.info("INPUT|"+control.getTxtAutores()+"|");
		
		TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_AUTOR);
		obj.setCodDetalle(control.getTxtCodigo().trim());
		obj.setDescripcion(control.getTxtAutores().trim());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		
		control.setLstResultado(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AUTOR, "", 
				control.getTxtAutores(), "", control.getTxtCodigo(), control.getCodTipo(),
				"", "", CommonConstants.TIPO_ORDEN_DSC) );

		return control;
	}
}
