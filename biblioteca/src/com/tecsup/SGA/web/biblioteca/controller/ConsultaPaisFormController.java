package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaPaisFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConsultaPaisFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		ConsultaPaisCommand command = new ConsultaPaisCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());
    	
    	LlenarDatos(command, request);
    	
    	request.setAttribute("lstrURL", command.getLstrURL());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
    	ConsultaPaisCommand control = (ConsultaPaisCommand) command;
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{   LlenarDatos(control, request);
    		
    	}
    	else{ if (control.getOperacion().trim().equals("ELIMINAR"))
    	      {resultado = DeletePais(control);
    	      log.info("RPTA:" + resultado);
	    		if ( resultado.equals("-1")) control.setMsg("ERROR");
	    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
	    		      else
	    			  {control.setMsg("OK");
	    			    LlenarDatos(control, request);
	    			   }
    		
    		}
    		
    	      }
    	}
    	
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_pais","control",control);		
    }
    public String DeletePais(ConsultaPaisCommand control){
    	try
    	{   String resultado="";
    		
    		Pais pais= new Pais();
    		pais.setPaisId(control.getCodSelec());
    		pais.setPaisDescripcion(control.getDescripcionPaises());
    		pais.setPaisCodigo(control.getCodValor());
    		pais.setFlag(CommonConstants.DELETE_MTO_BIB);
    		pais.setUsuCrea(control.getCodEvaluador()); 
    		resultado=this.biblioMantenimientoManager.updateDeletePais(pais);
    		//log.info("Resultado: "+resultado);	
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    public void LlenarDatos(ConsultaPaisCommand control, HttpServletRequest request){
    	
    	Pais pais= new Pais();
		control.setCodSelec("");
		if(control.getCodigoPais()==null)
			control.setCodigoPais("");
		if(control.getDescripcionPaises()==null)
			control.setDescripcionPaises("");
		pais.setPaisCodigo(control.getCodigoPais());
		pais.setPaisDescripcion(control.getDescripcionPaises());
		//log.info(control.getCodigoPais()+">><<"+control.getDescripcionPaises());
		control.setListaPaises(this.biblioMantenimientoManager.getAllPaises(pais, 
				CommonConstants.TIPO_ORDEN_DSC));
		request.getSession().setAttribute("listaPaises", control.getListaPaises());
    }
		
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
    
}
