package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.bean.UsuarioBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarPrestamoCommand;

public class AdminRegistrarPrestamoFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(AdminRegistrarPrestamoFormController.class);
	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI"); 
    	AdminRegistrarPrestamoCommand control = new AdminRegistrarPrestamoCommand();
    	
    	control.setSedeSel(request.getParameter("txhSedeSel"));
    	control.setSedeUsuario(request.getParameter("txhSedeUsuario"));
    	
    	if(control.getOperacion()==null)
    	{
    		control.setUsuario(request.getParameter("txhCodUsuario"));
    		control = cargaParametrosDeBusqueda(control,request);
    	}	
        //log.info("formBackingObject:FIN");
        control.setTxhFlgSancion("");
        return control;
   }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	//log.info("onSubmit:INI");
    	AdminRegistrarPrestamoCommand control = (AdminRegistrarPrestamoCommand) command;
    	
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if("irGetMaterial".equalsIgnoreCase(control.getOperacion()))
    	{
    		control = irGetMaterial(control,request,response);
    	}else if("irGetMateriales".equalsIgnoreCase(control.getOperacion())){    		
    		int nro =Integer.parseInt(control.getTxtNroMat());
    		control = irGetMateriales(nro, control,request,response);    	    	
    	}else if("irGetAlumno".equalsIgnoreCase(control.getOperacion())){    	
    		control = irGetAlumno(control,request,response);    		
    	}else if("irGetAlumno2".equalsIgnoreCase(control.getOperacion())){    	
    		control = irGetAlumnoOraCod(control,request,response);
    	}else if("irRegistrarMultiple".equalsIgnoreCase(control.getOperacion())){
    		//segun titulo del material...
    		String sRpta = "";
    		String str2 = "";
    		String men = "";
    		
    		//obtener codigo para identificar prestamos grupales
    		Long codigo = biblioGestionMaterialManager.obtenerCodigoPrestamoMultiple();
    		control.setCodPrestamoMultiple(codigo);
    		
    		if(!control.getTxtTitulo1().equals("")){
    			sRpta = irRegistrarByNro(control, 1);
    			str2 = sRpta.substring(0, 1);  
    			if("0".equalsIgnoreCase(str2)){    				
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.GRABAR_EXITO_PRESTAMO + "�";
        		}else if("2".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.LIBRO_PRESTADO + "�";
        		}else if("3".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.MAX_PRESTADOS + "�";
        		}else if("4".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.LIBRO_RETIRADO + "�";
        		}else if("5".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.LIBRO_RESERVADO_OTRO_USU + "�";
        		}else if("6".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.USUARIO_SANCIONADO_LIBRO + "�";
        		}else if("7".equalsIgnoreCase(str2)){ 
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.USUARIO_INHABILITADO + "�";
        		}else if("8".equalsIgnoreCase(str2)){        			//
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.LIBRO_PRESTADO_RECIEN + "�";
        		}else if("9".equalsIgnoreCase(str2)){     			
        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO + "�";
        			//enviar lista de libros que adeuda entregar
        			List<MaterialxIngresoBean> lista = this.biblioGestionMaterialManager.getListaLibrosDeudaXUsuario(control.getTxhCodUsuario());
        			log.info("tama�o libros adeuda:" + lista.size());
        			request.setAttribute("listaLibrosAdeuda", lista);
        		}else{
        			men += "Nro. Ing.: " + control.getNroIngresoReal1() + " " + CommonMessage.GRABAR_ERROR + "�";
        		}
    		}
    		
    		if(!control.getTxtTitulo2().equals("")){
    			sRpta = irRegistrarByNro(control, 2);
    			str2 = sRpta.substring(0, 1);  
    			if("0".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.GRABAR_EXITO_PRESTAMO + "�";
        		}else if("2".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.LIBRO_PRESTADO + "�";
        		}else if("3".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.MAX_PRESTADOS  + "�";
        		}else if("4".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.LIBRO_RETIRADO + "�";
        		}else if("5".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.LIBRO_RESERVADO_OTRO_USU + "�";
        		}else if("6".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.USUARIO_SANCIONADO_LIBRO + "�";
        		}else if("7".equalsIgnoreCase(str2)){ 
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.USUARIO_INHABILITADO + "�";
        		}else if("8".equalsIgnoreCase(str2)){        			//
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.LIBRO_PRESTADO_RECIEN + "�";
        		}else if("9".equalsIgnoreCase(str2)){     			
        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO + "�";
        			//enviar lista de libros que adeuda entregar
        			List<MaterialxIngresoBean> lista = this.biblioGestionMaterialManager.getListaLibrosDeudaXUsuario(control.getTxhCodUsuario());
        			log.info("tama�o libros adeuda:" + lista.size());
        			request.setAttribute("listaLibrosAdeuda", lista);
        		}else{
        			men += "Nro. Ing.: " + control.getNroIngresoReal2() + " " + CommonMessage.GRABAR_ERROR + "�";
        		}
    		}
    		
    		if(!control.getTxtTitulo3().equals("")){
    			sRpta = irRegistrarByNro(control, 3);
    			str2 = sRpta.substring(0, 1);  
    			if("0".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.GRABAR_EXITO_PRESTAMO + "�";
        		}else if("2".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.LIBRO_PRESTADO + "�";
        		}else if("3".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.MAX_PRESTADOS + "�";
        		}else if("4".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.LIBRO_RETIRADO + "�";
        		}else if("5".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.LIBRO_RESERVADO_OTRO_USU + "�";
        		}else if("6".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.USUARIO_SANCIONADO_LIBRO + "�";
        		}else if("7".equalsIgnoreCase(str2)){ 
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.USUARIO_INHABILITADO + "�";
        		}else if("8".equalsIgnoreCase(str2)){        			//
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.LIBRO_PRESTADO_RECIEN + "�";
        		}else if("9".equalsIgnoreCase(str2)){     			
        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO + "�";
        			//enviar lista de libros que adeuda entregar
        			List<MaterialxIngresoBean> lista = this.biblioGestionMaterialManager.getListaLibrosDeudaXUsuario(control.getTxhCodUsuario());
        			log.info("tama�o libros adeuda:" + lista.size());
        			request.setAttribute("listaLibrosAdeuda", lista);
        		}else{
        			men += "Nro. Ing.: " + control.getNroIngresoReal3() + " " + CommonMessage.GRABAR_ERROR + "�";
        		}
    		}
    		
    		if(!control.getTxtTitulo4().equals("")){
    			sRpta = irRegistrarByNro(control, 4);
    			str2 = sRpta.substring(0, 1);  
    			if("0".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.GRABAR_EXITO_PRESTAMO + "�";
        		}else if("2".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.LIBRO_PRESTADO + "�";
        		}else if("3".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.MAX_PRESTADOS + "�";
        		}else if("4".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.LIBRO_RETIRADO + "�";
        		}else if("5".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.LIBRO_RESERVADO_OTRO_USU + "�";
        		}else if("6".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.USUARIO_SANCIONADO_LIBRO + "�";
        		}else if("7".equalsIgnoreCase(str2)){ 
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.USUARIO_INHABILITADO + "�";
        		}else if("8".equalsIgnoreCase(str2)){        			//
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.LIBRO_PRESTADO_RECIEN + "�";
        		}else if("9".equalsIgnoreCase(str2)){     			
        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO + "�";
        			//enviar lista de libros que adeuda entregar
        			List<MaterialxIngresoBean> lista = this.biblioGestionMaterialManager.getListaLibrosDeudaXUsuario(control.getTxhCodUsuario());
        			log.info("tama�o libros adeuda:" + lista.size());
        			request.setAttribute("listaLibrosAdeuda", lista);
        		}else{
        			men += "Nro. Ing.: " + control.getNroIngresoReal4() + " " + CommonMessage.GRABAR_ERROR + "�";
        		}
    		}
    		
    		if(!control.getTxtTitulo5().equals("")){
    			sRpta = irRegistrarByNro(control, 5);
    			str2 = sRpta.substring(0, 1);  
    			if("0".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.GRABAR_EXITO_PRESTAMO + "�";
        		}else if("2".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.LIBRO_PRESTADO + "�";
        		}else if("3".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.MAX_PRESTADOS + "�";
        		}else if("4".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.LIBRO_RETIRADO + "�";
        		}else if("5".equalsIgnoreCase(str2)){        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.LIBRO_RESERVADO_OTRO_USU + "�";
        		}else if("6".equalsIgnoreCase(str2)){
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.USUARIO_SANCIONADO_LIBRO + "�";
        		}else if("7".equalsIgnoreCase(str2)){ 
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.USUARIO_INHABILITADO + "�";
        		}else if("8".equalsIgnoreCase(str2)){        			//
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.LIBRO_PRESTADO_RECIEN + "�";
        		}else if("9".equalsIgnoreCase(str2)){     			
        			
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO + "�";
        			//enviar lista de libros que adeuda entregar
        			List<MaterialxIngresoBean> lista = this.biblioGestionMaterialManager.getListaLibrosDeudaXUsuario(control.getTxhCodUsuario());
        			log.info("tama�o libros adeuda:" + lista.size());
        			request.setAttribute("listaLibrosAdeuda", lista);
        		
        		}else{
        			men += "Nro. Ing.: " + control.getNroIngresoReal5() + " " + CommonMessage.GRABAR_ERROR + "�";
        		}
    		}
    		
    		request.setAttribute("mensaje", men);
    		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_prestamo","control",control);
    		
    	}else if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
    	    		
    		String str = irRegistrar(control);
    		log.info("RPTA:"+str);    		
    		
    		String str2 =str.substring(0, 1); 
    		
    		if("0".equalsIgnoreCase(str2)){
    			request.setAttribute("mensaje", CommonMessage.GRABAR_EXITO_PRESTAMO);
    			clearMaterial(control);
    			clearAlumno(control);
    		}else if("2".equalsIgnoreCase(str2)){
    			request.setAttribute("mensaje", CommonMessage.LIBRO_PRESTADO);
    		}else if("3".equalsIgnoreCase(str2)){
    			request.setAttribute("mensaje", CommonMessage.MAX_PRESTADOS);
    		}else if("4".equalsIgnoreCase(str2)){
    			request.setAttribute("mensaje", CommonMessage.LIBRO_RETIRADO);
    		}else if("5".equalsIgnoreCase(str2)){
    			request.setAttribute("mensaje",CommonMessage.LIBRO_RESERVADO_OTRO_USU);
    		}else if("6".equalsIgnoreCase(str2)){ //JHPR 2008-06-27
    			request.setAttribute("mensaje",CommonMessage.USUARIO_SANCIONADO_LIBRO);    			
    		}else if("7".equalsIgnoreCase(str2)){ 
    			request.setAttribute("mensaje",CommonMessage.USUARIO_INHABILITADO);    			
    		}else if("8".equalsIgnoreCase(str2)){ 
    			request.setAttribute("mensaje",CommonMessage.LIBRO_PRESTADO_RECIEN);
    		}else if("9".equalsIgnoreCase(str2)){     			
    			request.setAttribute("mensaje",CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO);
    			//enviar lista de libros que adeuda entregar
    			List<MaterialxIngresoBean> lista = this.biblioGestionMaterialManager.getListaLibrosDeudaXUsuario(control.getTxhCodUsuario());
    			log.info("tama�o libros adeuda:" + lista.size());
    			request.setAttribute("listaLibrosAdeuda", lista);
    					
    		}else{
    			request.setAttribute("mensaje", CommonMessage.GRABAR_ERROR);
    		}
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_prestamo","control",control);		
    }
    
    private String finSancion(String str) {
		StringTokenizer tkn = new StringTokenizer(str,"|");
		String msg1 = "";		
		String msg2 = "";
		if(tkn.hasMoreTokens())
			msg1 = tkn.nextToken();//2|
		if(tkn.hasMoreTokens())
			msg2 = tkn.nextToken();//NRO DE OCURRENCIAS ENCONTRADAS		
		
		String mensaje = "\nTermino de Sanci�n:" + msg2;
		return mensaje;
	}
    
	private String irRegistrar(AdminRegistrarPrestamoCommand control){			
		
		if(control.getChkIndPrestamo()==null)
			control.setChkIndPrestamo("1"); //1 - prestamo domicilio / 0 - prestamos sala			
			
		return (String)this.biblioGestionMaterialManager.
		insertPrestamoxAlumno
		(
				control.getTxhCodMaterial(),//.getTxhCodUnico(),
				//control.getTxhNroIngreso(),
				control.getNroIngresoReal(),
				control.getTxhCodUsuario(),//control.getTxtUsuariologin()
				"",//control.getChkIndProrroga()
				control.getUsuario(),
				control.getChkIndPrestamo(),//para prestamo en sala
				control.getTxhFlgReserva(), //ULTIMO CAMBIO
				control.getTxtFecPrestamo(),
				null
		);
	}

	private String irRegistrarByNro(AdminRegistrarPrestamoCommand control,int Nro){			
		if(control.getChkIndPrestamo()==null)
			control.setChkIndPrestamo("1"); //1 - prestamo domicilio / 0 - prestamos sala			
	
		String codMaterial="";
		String nroIngreReal="";
		switch(Nro)
		{ 
			case 1: 
				codMaterial = control.getTxhCodMaterial1();
				nroIngreReal = control.getNroIngresoReal1();
				break;			
			case 2: 
				codMaterial = control.getTxhCodMaterial2();
				nroIngreReal = control.getNroIngresoReal2();
				break;			
			case 3: 
				codMaterial = control.getTxhCodMaterial3();
				nroIngreReal = control.getNroIngresoReal3();
				break;			
			case 4: 
				codMaterial = control.getTxhCodMaterial4();
				nroIngreReal = control.getNroIngresoReal4();
				break;		 
			case 5: 
				codMaterial = control.getTxhCodMaterial5();
				nroIngreReal = control.getNroIngresoReal5();
				break;			
		}; 
		
				
		return (String)this.biblioGestionMaterialManager.insertPrestamoxAlumno
		(
				codMaterial,				
				nroIngreReal,
				control.getTxhCodUsuario(),
				"",
				control.getUsuario(),
				control.getChkIndPrestamo(),//para prestamo en sala
				control.getTxhFlgReserva(), //ULTIMO CAMBIO
				control.getTxtFecPrestamo(), control.getCodPrestamoMultiple()
		);
	}
	
	private AdminRegistrarPrestamoCommand irGetAlumnoOraCod(AdminRegistrarPrestamoCommand control,HttpServletRequest request, HttpServletResponse response){
		String inPrestamo = control.getChkIndPrestamo()==null?"1":control.getChkIndPrestamo();
		//'0'=El cogido enviado es codigo oracle, '1'=El codigo es enviado es el username
		AlumnoXCodigoBean alumnoXCodigoBean = biblioGestionMaterialManager.getAlumnoXCodigo(control.getTxtUsuariologin(),inPrestamo,"0",control.getSedeSel(), control.getTxhCodUsuario());
		if(alumnoXCodigoBean!=null){	
			System.out.println("alumno encontrado");
			control.setTxtUsuarioNombre(alumnoXCodigoBean.getNombre());
			control.setTxtFecPrestamo(alumnoXCodigoBean.getFecActual());
			control.setTxtFecDevolucionProrroga(alumnoXCodigoBean.getFecProgDev());
			control.setTxhFlgPermitido(alumnoXCodigoBean.getFlgPermitido());//PERMITIDO DOMICILIO
			control.setTxhFlgPermitidoSala(alumnoXCodigoBean.getFlgPermitidoSala());//PERMITIDO DOMICILIO			
			control.setTxhSedeUsu(alumnoXCodigoBean.getSede().trim());						
			control.setTxhCodUsuario(alumnoXCodigoBean.getCodUsuario());
			//System.out.println("Estado Permitido Domicilio:"+alumnoXCodigoBean.getFlgPermitido());
			if(!("1".equalsIgnoreCase(alumnoXCodigoBean.getFlgPermitido())))
				control.setTxtMensaje("No Permitido");	
				
			if (alumnoXCodigoBean.getFlgSancion().trim().equals("1")){
				//System.out.println("Estado Sancion:" + alumnoXCodigoBean.getFlgSancion());
				//control.setTxtMensaje(alumnoXCodigoBean.getFlgSancion().equalsIgnoreCase("1")?"Sancionado":"Permitido");				//
				control.setTxhFlgSancion(alumnoXCodigoBean.getFlgSancion());
				control.setFechaFinSancion(alumnoXCodigoBean.getFechaFinSancion());				
			}else{
				control.setTxhFlgSancion("0");
				control.setFechaFinSancion("");
			}
			
			
			if (alumnoXCodigoBean.getFlgInhabilitado().trim().equals("1")){
				control.setTxhFlgInhabilitado(alumnoXCodigoBean.getFlgInhabilitado());
				control.setFechaFinInhabilitado(alumnoXCodigoBean.getFechaFinInhabilitado());
			}else{
				control.setTxhFlgInhabilitado("0");
				control.setFechaFinInhabilitado("");
			}
			
		}else{					
			clearAlumno(control);
			control.setFlgBusqXCodOracle("0");
			request.setAttribute("mensaje",CommonMessage.USUARIO_NO_REGISTRADO);
			request.setAttribute("flgUsuario","1");
		}
		
		return control;
	}
	
	private AdminRegistrarPrestamoCommand irGetAlumno(
			AdminRegistrarPrestamoCommand control,HttpServletRequest request, HttpServletResponse response) {
						
		String inPrestamo = control.getChkIndPrestamo()==null?"1":control.getChkIndPrestamo();
				
		List lista = new ArrayList();
		lista = biblioGestionMaterialManager.getUsuariosXCodigo(control.getTxtUsuariologin(), control.getTipousuario());
				
		if (!(lista==null)) {
			//System.out.println("lista.size():" + lista.size());
			if (lista.size()==1){
				UsuarioBean usuario = (UsuarioBean) lista.get(0);		
				//System.out.println("control.usuario.getCodTipoUsuario():"+ getCodTipoUsuario());
				//Ultimo parametro "tipoBusquedaCodUsu": '0'=El cogido enviado es c�digo oracle, '1'=El codigo enviado es el username
				AlumnoXCodigoBean alumnoXCodigoBean = biblioGestionMaterialManager.getAlumnoXCodigo(usuario.getCodTipoUsuario(),inPrestamo,"0",control.getSedeSel(), control.getNroIngresoReal()); 
				//AlumnoXCodigoBean alumnoXCodigoBean = biblioGestionMaterialManager.getAlumnoXCodigo(control.getTxtUsuariologin(),inPrestamo);
				if(alumnoXCodigoBean!=null){					
					control.setTxtUsuarioNombre(alumnoXCodigoBean.getNombre());
					control.setTxtFecPrestamo(alumnoXCodigoBean.getFecActual());
					control.setTxtFecDevolucionProrroga(alumnoXCodigoBean.getFecProgDev());
					control.setTxtFecDevolucionProrroga2(alumnoXCodigoBean.getFecProgDev());
					control.setTxhFlgPermitido(alumnoXCodigoBean.getFlgPermitido());//PERMITIDO DOMICILIO
					control.setTxhFlgPermitidoSala(alumnoXCodigoBean.getFlgPermitidoSala());//PERMITIDO DOMICILIO			
					control.setTxhSedeUsu(alumnoXCodigoBean.getSede().trim());				
					control.setTxhCodUsuario(alumnoXCodigoBean.getCodUsuario());					
					control.setEstadoUsuario(alumnoXCodigoBean.getEstado());	
					control.setTxtNroPrestamos(alumnoXCodigoBean.getNroPrestamos());
					control.setTxtLimitePrestamos(alumnoXCodigoBean.getLimitePrestamos());
					
					if (control.getEstadoUsuario().equals("N")){
						clearAlumno(control);
						control.setFlgBusqXCodOracle("0");
						request.setAttribute("mensaje",CommonMessage.USUARIO_INHABILITADO);
						request.setAttribute("flgUsuario","1");
						return control;
					}
						
					if(!("1".equalsIgnoreCase(alumnoXCodigoBean.getFlgPermitido())))
						control.setTxtMensaje("No Permitido");	
						
					if (alumnoXCodigoBean.getFlgSancion().trim().equals("1")){
						//System.out.println("Estado Sancion:" + alumnoXCodigoBean.getFlgSancion());						
						control.setTxhFlgSancion(alumnoXCodigoBean.getFlgSancion());
						control.setFechaFinSancion(alumnoXCodigoBean.getFechaFinSancion());				
					}else{
						control.setTxhFlgSancion("0");
						control.setFechaFinSancion("");
					}
					
					if (alumnoXCodigoBean.getFlgInhabilitado().trim().equals("1")){
						control.setTxhFlgInhabilitado(alumnoXCodigoBean.getFlgInhabilitado());
						control.setFechaFinInhabilitado(alumnoXCodigoBean.getFechaFinInhabilitado());
					}else{
						control.setTxhFlgInhabilitado("0");
						control.setFechaFinInhabilitado("");
					}
					
				}else{					
					clearAlumno(control);
					control.setFlgBusqXCodOracle("0");
					request.setAttribute("mensaje",CommonMessage.USUARIO_NO_REGISTRADO);
					request.setAttribute("flgUsuario","1");
				}				
			}else if (lista.size()==0){
				clearAlumno(control);
				control.setFlgBusqXCodOracle("0");
				request.setAttribute("mensaje",CommonMessage.USUARIO_NO_REGISTRADO);
				request.setAttribute("flgUsuario","1");			
			}else if (lista.size()>1){// en caso que exista similitud entre codigo carnet y codigo oracle.				
				//clearAlumno(control);
				control.setTxtUsuarioNombre(null);				
				request.setAttribute("busca_codigo",control.getTxtUsuariologin());
				request.setAttribute("busca_tipo",control.getTipousuario());
			}
		}else{
			clearAlumno(control);		
			request.setAttribute("mensaje",CommonMessage.USUARIO_NO_REGISTRADO);
			request.setAttribute("flgUsuario","1");			
		}
		return control;
	}
	
	
	private void clearAlumno(AdminRegistrarPrestamoCommand control) {
		control.setTxtUsuarioNombre(null);
		control.setTxtFecPrestamo(null);
		control.setTxtFecDevolucionProrroga(null);
		control.setTxhFlgPermitido(null);
		control.setTxtMensaje(null);	
		control.setTxtUsuariologin(null);
		control.setTxhCodUsuario(null);
	}

	private AdminRegistrarPrestamoCommand irGetMateriales(
			int nNumero,AdminRegistrarPrestamoCommand control,HttpServletRequest request, HttpServletResponse response) {
				
		String sNumIngreso = "";
		switch(nNumero)
		{ 
			case 1: sNumIngreso = control.getTxtNroIngreso1();break;			
			case 2: sNumIngreso = control.getTxtNroIngreso2();break;			
			case 3: sNumIngreso = control.getTxtNroIngreso3();break;			
			case 4: sNumIngreso = control.getTxtNroIngreso4();break;		 
			case 5: sNumIngreso = control.getTxtNroIngreso5();break;			
		}; 
		
		MaterialxIngresoBean materialxIngresoBean = 
			biblioGestionMaterialManager.getMaterialxIngreso(sNumIngreso,CommonConstants.FLG_TIPO_PRESTAMO,control.getSedeSel(), control.getTxhCodUsuario());
		
		if(materialxIngresoBean!=null){
			
			switch(nNumero)
			{ 
				case 1: control.setTxtTipomaterial1(materialxIngresoBean.getDesTipoMaterial());break;		
				case 2: control.setTxtTipomaterial2(materialxIngresoBean.getDesTipoMaterial());break;			
				case 3: control.setTxtTipomaterial3(materialxIngresoBean.getDesTipoMaterial());break;			
				case 4: control.setTxtTipomaterial4(materialxIngresoBean.getDesTipoMaterial());break;			 
				case 5: control.setTxtTipomaterial5(materialxIngresoBean.getDesTipoMaterial());break;				
			};
			
			switch(nNumero)
			{ 
				case 1: control.setTxtTitulo1(materialxIngresoBean.getTitulo());break;			
				case 2: control.setTxtTitulo2(materialxIngresoBean.getTitulo());break;			
				case 3: control.setTxtTitulo3(materialxIngresoBean.getTitulo());break;			
				case 4: control.setTxtTitulo4(materialxIngresoBean.getTitulo());break;			 
				case 5: control.setTxtTitulo5(materialxIngresoBean.getTitulo());break;				
			};
					
			switch(nNumero)
			{ 
				case 1: control.setTxhCodMaterial1(materialxIngresoBean.getCodMaterialBib());break;			
				case 2: control.setTxhCodMaterial2(materialxIngresoBean.getCodMaterialBib());break;			
				case 3: control.setTxhCodMaterial3(materialxIngresoBean.getCodMaterialBib());break;			
				case 4: control.setTxhCodMaterial4(materialxIngresoBean.getCodMaterialBib());break;			 
				case 5: control.setTxhCodMaterial5(materialxIngresoBean.getCodMaterialBib());break;				
			};
			
			switch(nNumero)
			{ 
				case 1: control.setTxhNroIngreso1(materialxIngresoBean.getNroIngreso());break;			
				case 2: control.setTxhNroIngreso2(materialxIngresoBean.getNroIngreso());break;			
				case 3: control.setTxhNroIngreso3(materialxIngresoBean.getNroIngreso());break;			
				case 4: control.setTxhNroIngreso4(materialxIngresoBean.getNroIngreso());break;			 
				case 5: control.setTxhNroIngreso5(materialxIngresoBean.getNroIngreso());break;				
			};
			
			switch(nNumero)
			{ 
				case 1: control.setTxhCodUnico1(materialxIngresoBean.getCodUnico());break;			
				case 2: control.setTxhCodUnico2(materialxIngresoBean.getCodUnico());break;			
				case 3: control.setTxhCodUnico3(materialxIngresoBean.getCodUnico());break;			
				case 4: control.setTxhCodUnico4(materialxIngresoBean.getCodUnico());break;			 
				case 5: control.setTxhCodUnico5(materialxIngresoBean.getCodUnico());break;				
			};
			
			switch(nNumero)
			{ 
				case 1: control.setTxhFlgPrestado1(materialxIngresoBean.getFlgPrestado());break;			
				case 2: control.setTxhFlgPrestado2(materialxIngresoBean.getFlgPrestado());break;			
				case 3: control.setTxhFlgPrestado3(materialxIngresoBean.getFlgPrestado());break;			
				case 4: control.setTxhFlgPrestado4(materialxIngresoBean.getFlgPrestado());break;			 
				case 5: control.setTxhFlgPrestado5(materialxIngresoBean.getFlgPrestado());break;				
			};
			
			switch(nNumero)
			{ 
				case 1: control.setTxhFlgReserva1(materialxIngresoBean.getCodReserva());break;			
				case 2: control.setTxhFlgReserva2(materialxIngresoBean.getCodReserva());break;			
				case 3: control.setTxhFlgReserva3(materialxIngresoBean.getCodReserva());break;			
				case 4: control.setTxhFlgReserva4(materialxIngresoBean.getCodReserva());break;			 
				case 5: control.setTxhFlgReserva5(materialxIngresoBean.getCodReserva());break;				
			};
			
			switch(nNumero)
			{ 
				case 1: control.setTxtCodUsuarioReserva1(materialxIngresoBean.getUsuarioReserva());break;		
				case 2: control.setTxtCodUsuarioReserva2(materialxIngresoBean.getUsuarioReserva());break;		
				case 3: control.setTxtCodUsuarioReserva3(materialxIngresoBean.getUsuarioReserva());break;		
				case 4: control.setTxtCodUsuarioReserva4(materialxIngresoBean.getUsuarioReserva());break;		 
				case 5: control.setTxtCodUsuarioReserva5(materialxIngresoBean.getUsuarioReserva());break;			
			};
											
			switch(nNumero)
			{ 
				case 1: control.setTxhFlgMatIndCasa1(materialxIngresoBean.getFlgMatIndCasa());break;		
				case 2: control.setTxhFlgMatIndCasa2(materialxIngresoBean.getFlgMatIndCasa());break;	
				case 3: control.setTxhFlgMatIndCasa3(materialxIngresoBean.getFlgMatIndCasa());break;	
				case 4: control.setTxhFlgMatIndCasa4(materialxIngresoBean.getFlgMatIndCasa());break;	 
				case 5: control.setTxhFlgMatIndCasa5(materialxIngresoBean.getFlgMatIndCasa());break;		
			};
						
			switch(nNumero)
			{ 
				case 1: control.setTxhFlgMatIndSala1(materialxIngresoBean.getFlgMatIndSala());break;		
				case 2: control.setTxhFlgMatIndSala2(materialxIngresoBean.getFlgMatIndSala());break;	
				case 3: control.setTxhFlgMatIndSala3(materialxIngresoBean.getFlgMatIndSala());break;	
				case 4: control.setTxhFlgMatIndSala4(materialxIngresoBean.getFlgMatIndSala());break;	 
				case 5: control.setTxhFlgMatIndSala5(materialxIngresoBean.getFlgMatIndSala());break;		
			};
						
			switch(nNumero)
			{ 
				case 1: control.setTxhFlgLibroRetirado1(materialxIngresoBean.getFlgLibroRetirado());break;
				case 2: control.setTxhFlgLibroRetirado2(materialxIngresoBean.getFlgLibroRetirado());break;	
				case 3: control.setTxhFlgLibroRetirado3(materialxIngresoBean.getFlgLibroRetirado());break;	
				case 4: control.setTxhFlgLibroRetirado4(materialxIngresoBean.getFlgLibroRetirado());break;	 
				case 5: control.setTxhFlgLibroRetirado5(materialxIngresoBean.getFlgLibroRetirado());break;		
			};
						
			switch(nNumero)
			{ 
				case 1: control.setTxhSedeMat1(materialxIngresoBean.getSede().trim());break;
				case 2: control.setTxhSedeMat2(materialxIngresoBean.getSede().trim());break;	
				case 3: control.setTxhSedeMat3(materialxIngresoBean.getSede().trim());break;	
				case 4: control.setTxhSedeMat4(materialxIngresoBean.getSede().trim());break;	 
				case 5: control.setTxhSedeMat5(materialxIngresoBean.getSede().trim());break;		
			};
			
			
			switch(nNumero)
			{ 
				case 1: control.setTxhCodDewey1(materialxIngresoBean.getCodDewey());break;
				case 2: control.setTxhCodDewey2(materialxIngresoBean.getCodDewey());break;	
				case 3: control.setTxhCodDewey3(materialxIngresoBean.getCodDewey());break;	
				case 4: control.setTxhCodDewey4(materialxIngresoBean.getCodDewey());break;	 
				case 5: control.setTxhCodDewey5(materialxIngresoBean.getCodDewey());break;		
			};
						
			
			switch(nNumero)
			{ 
				case 1: control.setTxhFlgSancion1("2");break;			
				case 2: control.setTxhFlgSancion2("2");break;	
				case 3: control.setTxhFlgSancion3("2");break;
				case 4: control.setTxhFlgSancion4("2");break;	 
				case 5: control.setTxhFlgSancion5("2");break;		
			};
						
			switch(nNumero)
			{ 
				case 1: control.setTxhFlgInhabilitado1("2");break;		
				case 2: control.setTxhFlgInhabilitado2("2");break;	
				case 3: control.setTxhFlgInhabilitado3("2");break;	
				case 4: control.setTxhFlgInhabilitado4("2");break;	 
				case 5: control.setTxhFlgInhabilitado5("2");break;		
			};
						
			switch(nNumero)
			{ 
				case 1: control.setNroIngresoReal1(materialxIngresoBean.getNroIngresoReal());break;		
				case 2: control.setNroIngresoReal2(materialxIngresoBean.getNroIngresoReal());break;	
				case 3: control.setNroIngresoReal3(materialxIngresoBean.getNroIngresoReal());break;	
				case 4: control.setNroIngresoReal4(materialxIngresoBean.getNroIngresoReal());break;	 
				case 5: control.setNroIngresoReal5(materialxIngresoBean.getNroIngresoReal());break;		
			};
			
			/*
			switch(nNumero)
			{ 
				case 1: sNumIngreso = control.getTxtNroIngreso1();break;
				case 2: sNumIngreso = control.getTxtNroIngreso2();break;	
				case 3: sNumIngreso = control.getTxtNroIngreso3();break;	
				case 4: sNumIngreso = control.getTxtNroIngreso4();break;	 
				case 5: sNumIngreso = control.getTxtNroIngreso5();break;		
			};
			*/
			
			if(materialxIngresoBean.getFlgLibroRetirado().equalsIgnoreCase("1"))
				request.setAttribute("mensaje","El c�digo de libro seleccionado est� retirado de la biblioteca.");
			
			if(materialxIngresoBean.getFlgPrestado().equalsIgnoreCase("1"))
				request.setAttribute("mensaje","El libro ha sido prestado.");
			
			if(!materialxIngresoBean.getFlgReserva().equalsIgnoreCase("0"))
				request.setAttribute("mensaje","El libro ha sido reservado.");
			
			if (!(materialxIngresoBean.getSede().equalsIgnoreCase(control.getSedeUsuario())))
				request.setAttribute("mensaje","El libro pertenece a una Sede diferente.");			
			
			if(materialxIngresoBean.getFlgMatIndCasa().equalsIgnoreCase("0") &&
				materialxIngresoBean.getFlgMatIndSala().equalsIgnoreCase("0")){
				request.setAttribute("mensaje","El libro seleccionado no est� configurado para pr�stamo.");
			}
			else{
				if(materialxIngresoBean.getFlgMatIndCasa().equalsIgnoreCase("0"))
					request.setAttribute("mensaje","El libro seleccionado no est� configurado para pr�stamo a Domicilio.");
					
				if(materialxIngresoBean.getFlgMatIndSala().equalsIgnoreCase("0"))
					request.setAttribute("mensaje","El libro seleccionado no est� configurado para pr�stamo en Sala.");
			}
						
		}else{
			clearMaterial(control);
			request.setAttribute("nNumero",String.valueOf(nNumero));
			request.setAttribute("mensaje",CommonMessage.LIBRO_NO_REGISTRADO);
			request.setAttribute("flgMaterial","1");			
		}
		
		request.setAttribute("item_numero", String.valueOf(nNumero)); //item
		return control;
		
	}
	
	private AdminRegistrarPrestamoCommand irGetMaterial(
			AdminRegistrarPrestamoCommand control,HttpServletRequest request, HttpServletResponse response) {
			
		MaterialxIngresoBean materialxIngresoBean = 
			biblioGestionMaterialManager.getMaterialxIngreso(control.getTxtNroIngreso(),CommonConstants.FLG_TIPO_PRESTAMO,control.getSedeSel(), control.getTxhCodUsuario());
		
		if(materialxIngresoBean!=null){
			//FecDevolucionProg
			control.setTxtFecDevolucionProrroga(materialxIngresoBean.getFecDevolucionProg());
			control.setTxtTipomaterial(materialxIngresoBean.getDesTipoMaterial());
			control.setTxtTitulo(materialxIngresoBean.getTitulo());
			control.setTxhCodMaterial(materialxIngresoBean.getCodMaterialBib());
			control.setTxhNroIngreso(materialxIngresoBean.getNroIngreso());
			control.setTxhCodUnico(materialxIngresoBean.getCodUnico());
			control.setTxhFlgPrestado(materialxIngresoBean.getFlgPrestado());			
			control.setTxhFlgReserva(materialxIngresoBean.getCodReserva());
			control.setTxtCodUsuarioReserva(materialxIngresoBean.getUsuarioReserva());			
			control.setTxhFlgMatIndCasa(materialxIngresoBean.getFlgMatIndCasa());
			control.setTxhFlgMatIndSala(materialxIngresoBean.getFlgMatIndSala());			
			control.setTxhFlgLibroRetirado(materialxIngresoBean.getFlgLibroRetirado());
			control.setTxhSedeMat(materialxIngresoBean.getSede().trim());
			control.setTxhCodDewey(materialxIngresoBean.getCodDewey());			
			control.setTxhFlgSancion("2");
			control.setTxhFlgInhabilitado("2");
			control.setNroIngresoReal(materialxIngresoBean.getNroIngresoReal());
			if(materialxIngresoBean.getFlgLibroRetirado().equalsIgnoreCase("1"))
				request.setAttribute("mensaje","El c�digo de libro seleccionado est� retirado de la biblioteca.");
			
			if(materialxIngresoBean.getFlgPrestado().equalsIgnoreCase("1"))
				request.setAttribute("mensaje","El libro ha sido prestado.");
			
			if(!materialxIngresoBean.getFlgReserva().equalsIgnoreCase("0"))
				request.setAttribute("mensaje","El libro ha sido reservado.");
			
			if (!(materialxIngresoBean.getSede().equalsIgnoreCase(control.getSedeUsuario())))
				request.setAttribute("mensaje","El libro pertenece a una Sede diferente.");			
			
			if(materialxIngresoBean.getFlgMatIndCasa().equalsIgnoreCase("0") &&
				materialxIngresoBean.getFlgMatIndSala().equalsIgnoreCase("0")){
				request.setAttribute("mensaje","El libro seleccionado no est� configurado para pr�stamo.");
			}
			else{
				if(materialxIngresoBean.getFlgMatIndCasa().equalsIgnoreCase("0"))
					request.setAttribute("mensaje","El libro seleccionado no est� configurado para pr�stamo a Domicilio.");
					
				if(materialxIngresoBean.getFlgMatIndSala().equalsIgnoreCase("0"))
					request.setAttribute("mensaje","El libro seleccionado no est� configurado para pr�stamo en Sala.");
			}
						
		}else{
			clearMaterial(control);
			request.setAttribute("nNumero","0");
			request.setAttribute("mensaje",CommonMessage.LIBRO_NO_REGISTRADO);
			request.setAttribute("flgMaterial","1");
		}
		return control;
	}
	
	private void clearMaterial(AdminRegistrarPrestamoCommand control) {
		control.setTxtTipomaterial(null);
		control.setTxtTitulo(null);
		control.setTxhCodMaterial(null);
		control.setTxhNroIngreso(null);
		control.setTxhCodUnico(null);
		control.setTxhFlgPrestado(null);
		
		control.setTxtNroIngreso(null);
		control.setChkIndPrestamo(null);
		control.setTxhCodDewey(null);
		control.setTxhFlgMatIndCasa(null);
		control.setTxhFlgMatIndSala(null);
	}

	private AdminRegistrarPrestamoCommand cargaParametrosDeBusqueda(
			AdminRegistrarPrestamoCommand control, HttpServletRequest request) {
		
		control.setPrmTxtCodigo(request.getParameter("prmTxtCodigo"));
		control.setPrmTxtNroIngreso(request.getParameter("prmTxtNroIngreso"));
		control.setPrmCboTipoMaterial(request.getParameter("prmCboTipoMaterial"));
		control.setPrmCboBuscarPor(request.getParameter("prmCboBuscarPor"));
		control.setPrmTxtTitulo(request.getParameter("prmTxtTitulo"));
		control.setPrmCboIdioma(request.getParameter("prmCboIdioma"));
		control.setPrmCboAnioIni(request.getParameter("prmCboAnioIni"));
		control.setPrmCboAnioFin(request.getParameter("prmCboAnioFin"));
		control.setPrmTxtFechaReservaIni(request.getParameter("prmTxtFechaReservaIni"));
		control.setPrmTxtFechaReservaFin(request.getParameter("prmTxtFechaReservaFin"));
		
		return control;
	}
}

