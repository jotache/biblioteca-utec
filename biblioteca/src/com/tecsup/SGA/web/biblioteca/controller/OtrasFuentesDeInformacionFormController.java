package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.OtrasFuentesDeInformacionCommand;

public class OtrasFuentesDeInformacionFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(OtrasFuentesDeInformacionFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		OtrasFuentesDeInformacionCommand command = new OtrasFuentesDeInformacionCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
		
    	LlenarDatos(command, request);
    	
        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	OtrasFuentesDeInformacionCommand control = (OtrasFuentesDeInformacionCommand) command;
    	
        return new ModelAndView("/biblioteca/userWeb/bib_otras_fuentes_de_informacion","control",control);		
    }
    
    public void LlenarDatos(OtrasFuentesDeInformacionCommand control, HttpServletRequest request){
    	
    	Secciones seccion= new Secciones();
    	seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_OTRAS_FUENTES_INFORMACION);
    	TipoTablaDetalle obj= new TipoTablaDetalle();
    	obj.setCodTipoTablaDetalle("");
    	    	
		control.setListaOtrasFuentesInformacion(this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj));
		request.getSession().setAttribute("listaOtrasFuentesInformacion", control.getListaOtrasFuentesInformacion());
    	
    }
 
}
