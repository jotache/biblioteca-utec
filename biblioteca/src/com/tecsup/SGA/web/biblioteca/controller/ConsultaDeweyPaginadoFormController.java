package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.ConsultaDeweyPaginadoCommand;

public class ConsultaDeweyPaginadoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConsultaDeweyPaginadoFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		
		ConsultaDeweyPaginadoCommand command= new ConsultaDeweyPaginadoCommand();
		
		command.setCodCategoria((String)request.getParameter("txhCodCategoria"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		//System.out.println("Categoria: "+command.getCodCategoria()+" CodUsuario: "+command.getCodEvaluador());
	 	//log.info("formBackingObject:FIN");
    	//if(command.getCodCategoria().equals("-1"))
    	//	command.setListaDewey(new ArrayList());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	
    	ConsultaDeweyPaginadoCommand control= (ConsultaDeweyPaginadoCommand) command;
    	String resultado="";
    	log.info("OPERACION:"+ control.getOperacion());
    	if (control.getOperacion().trim().equals("ELIMINAR"))
	      { //log.info("Descripcion: "+control.getDescripcionDewey());
	      control.setDescripcionDewey("");
		    resultado = DeleteDewey(control);
  		if ( resultado.equals("-1")) control.setMsg("ERROR");
  		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
  		      else
  			  {control.setMsg("OK");
  			   
  			  }
		
		  }
  		}
    	//log.info("onSubmit:FIN");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_dewey_paginado","control",control);		
    
   }
    
    public String DeleteDewey(ConsultaDeweyPaginadoCommand control){
    	try
    	{   //log.info("DeleteDescriptor");
    		String resultado="";
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla(control.getCodCategoria());
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_CLASIFICACION_DEWEY);
    		obj.setDscValor1(control.getCodSelec());
    		obj.setDscValor2(control.getCodValor());
    		obj.setDescripcion(control.getDescripSelec());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodEvaluador());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		 	
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		//log.info("Resultado: "+resultado);	
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
}
