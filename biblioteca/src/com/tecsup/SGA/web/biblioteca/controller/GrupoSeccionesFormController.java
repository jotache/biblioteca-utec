package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.GrupoSeccionesCommand;
import com.tecsup.SGA.web.biblioteca.command.IdiomasAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.SeccionesAgregarCommand;


public class GrupoSeccionesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(GrupoSeccionesFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
		
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	    	
    	GrupoSeccionesCommand command = new GrupoSeccionesCommand();
    	command.setCodUsuario(request.getParameter("codusuario"));
    	cargaBandeja(command);
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	    	
    	GrupoSeccionesCommand control = (GrupoSeccionesCommand) command;
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = UpdateGrupo(control);
    		log.info("RPTA:" + resultado);
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			control.setMsg("OK");    			
    		}
    		cargaBandeja(control);
    	}
    	else if (control.getOperacion().trim().equals("LOAD")){
    		cargaBandeja(control);
    	}

	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_grupo_secciones","control",control);		
    }
    private void cargaBandeja(GrupoSeccionesCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	obj.setDscValor3("");    	
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_GRUPO_SECCION);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setListaGrupos(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    	
    }
    private String UpdateGrupo(GrupoSeccionesCommand control){
    	String resultado;
    	resultado=this.biblioMantenimientoManager.UpdateGrupos(CommonConstants.TIPT_GRUPO_SECCION, control.getCadena(), control.getNroRegistros(), control.getCodUsuario());
    	return resultado;
    }    
}