package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.TipoMaterialModificarCommand;


public class TipoMaterialModificarFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(TipoMaterialModificarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	 	
    	TipoMaterialModificarCommand command = new TipoMaterialModificarCommand();
    
    	command.setCodDetalle((String)request.getParameter("txhCodigo"));
        command.setDescripcion((String)request.getParameter("txhDescripcion"));
        command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	
    
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	TipoMaterialModificarCommand control = (TipoMaterialModificarCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("MODIFICAR"))
    	{    		
    		resultado = UpdateTablaDetalle(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    		      else
    			  control.setMsg("OK");}
    	}
    	control.setOperacion("");
			
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_tipo_material_modificar","control",control);		
    }
    
    private String UpdateTablaDetalle(TipoMaterialModificarCommand control)
    {
    	try
    	{   String resultado="";
    		TipoTablaDetalle obj = new TipoTablaDetalle();    		
    		obj.setCodTipoTabla("");
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
    		obj.setDscValor1(control.getCodDetalle());
    		obj.setDscValor2("");
    		obj.setDescripcion(control.getDescripcion());
    		obj.setEstReg("0");
    		obj.setUsuario("1");//control.getCodEvaluador());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		//log.info(obj.);
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    			
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
  
}
