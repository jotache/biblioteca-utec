package com.tecsup.SGA.web.biblioteca.controller;

import java.io.File;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.service.evaluaciones.NotaExternaManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialAdjuntarArchivoCommand;

public class AdminRegistrarMaterialAdjuntarFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminRegistrarMaterialAdjuntarFormController.class);	
//	private NotaExternaManager notaExternaManager;
	
//	public void setNotaExternaManager(NotaExternaManager notaExternaManager) {
//		this.notaExternaManager = notaExternaManager;
//	}
	
	public AdminRegistrarMaterialAdjuntarFormController(){
		super();
		setCommandClass(AdminRegistrarMaterialAdjuntarArchivoCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI");
    	
    	AdminRegistrarMaterialAdjuntarArchivoCommand command = new AdminRegistrarMaterialAdjuntarArchivoCommand();

    	//request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
    	request.setAttribute("strExtensionGIF", CommonConstants.GSTR_EXTENSION_GIF);
    	//************************************
    	String codPeriodo =(String)request.getParameter("txhCodPeriodo");
    	String codProducto =(String)request.getParameter("codProducto");
    	String codCiclo =(String)request.getParameter("codCiclo");
    	String codEvaluador =(String)request.getParameter("txhCodEvaluador");
    	
    	if(command.getOperacion()==null){
    		command.setUsuario(request.getParameter("txhCodUsuario"));
    	}
    	
    	command.setCodPeriodo(codPeriodo);
    	command.setCodProducto(codProducto);
    	command.setCodCiclo(codCiclo);
    	command.setCodEvaluador(codEvaluador);
    	//log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");    	
    	AdminRegistrarMaterialAdjuntarArchivoCommand control = (AdminRegistrarMaterialAdjuntarArchivoCommand)command;

    	log.info("OPERACION: " + control.getOperacion());
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta;    		
    		byte[] bytesArchivo = control.getTxtArchivo();
    		
    		String fileNameArchivo="";
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		
    		//***********************************CAMBIO************************
//   	      	String uploadDirArchivo = getServletContext().getRealPath("/"+CommonConstants.DIRECTORIO_IMAGENES);   
   	      	String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA + CommonConstants.DIRECTORIO_IMAGENES;
   	      	//si no existe lo creo.
            File dirPath = new File(uploadDirArchivo);
            if (!dirPath.exists()) {
                log.info("crenado..el directorio");             
                dirPath.mkdirs();
            }
            //***********************************CAMBIO************************
    		
    		//String uploadDirArchivo = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_MATERIAL_BIB_IMAGENES;            
            String sep = System.getProperty("file.separator");
            
            log.info("NUEVO ARCHIVO EN ..<"+uploadDirArchivo + sep + fileNameArchivo+">");
	        if (bytesArchivo.length>0){
	        	 //fileNameArchivo = CommonConstants.FILE_NAME_DOC+control.getCodPeriodo()+control.getCodProducto()+control.getCodCiclo()+"."+control.getExtArchivo();
	        	 fileNameArchivo = control.getNomArchivo();
	        	 File uploadedFileArchivo = new File(uploadDirArchivo + sep + fileNameArchivo);
	        	 FileCopyUtils.copy(bytesArchivo, uploadedFileArchivo);
	        	 
	        	 log.info("fileNameArchivo>>"+fileNameArchivo+">>");
	        	 control.setNomNuevoArchivo(fileNameArchivo);
	        	 
	        	 rpta=InsertAdjuntoCasoCat(control);	        	 
	        	 log.info("RPTA: "+ rpta);
	        	 if("-1".equals(rpta)){
	        			control.setMsg("ERROR");
	        	 }
	        		else{
	        			control.setMsg("OK");
	        	 }
	        }
	        else{	        	

	        }
    	}
		//log.info("onSubmit:FIN");
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_adjuntar_archivo_material","control",control);	    
    }
    private String InsertAdjuntoCasoCat(AdminRegistrarMaterialAdjuntarArchivoCommand control){
    	try{
    		Archivo obj= new Archivo();
    		
    		obj.setPeriodo(control.getCodPeriodo());
    		obj.setProducto(control.getCodProducto());
    		obj.setCiclo(control.getCodCiclo());
    		obj.setNombreNuevo(control.getNomNuevoArchivo());
    		obj.setNombre(control.getNomArchivo());
    		/*log.info("control.getCodPeriodo()|"+control.getCodPeriodo()+"|");
    		log.info("control.getCodProducto()|"+control.getCodProducto()+"|");
    		log.info("control.getCodCiclo()|"+control.getCodCiclo()+"|");
    		log.info("control.getNomNuevoArchivo()|"+control.getNomNuevoArchivo()+"|");
    		log.info("control.getNomArchivo()|"+control.getNomArchivo()+"|");    		
    		*/
    		//obj.setEstado(this.notaExternaManager.InsertAdjuntoCasoCat(obj, control.getCodEvaluador()));
    		return obj.getEstado();
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }   
}
