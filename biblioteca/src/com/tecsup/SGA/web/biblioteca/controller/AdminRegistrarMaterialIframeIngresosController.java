package com.tecsup.SGA.web.biblioteca.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;

public class AdminRegistrarMaterialIframeIngresosController  implements Controller  {
	
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialIframeIngresosController.class);
	
	BiblioGestionMaterialManager biblioGestionMaterialManager;
	
    public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	//log.info("handleRequest:INI");		    	    	
    	ModelMap model = new ModelMap();
    	
    	//CODIGO UNICO DEL MATERIAL
    	String txhCodigoUnico = request.getParameter("txhCodigoUnico");    	
    	
    	String resp = "";
    	String usuario = request.getParameter("usuario");
    	
    	log.info("OPERACION:" +  request.getParameter("operacion"));
    	
    	if("irEliminar".equalsIgnoreCase(request.getParameter("operacion"))){
    		//CODIGO SELECCIONADO 
    		String txhCodigoIngreso=  request.getParameter("txhCodigoIngreso");    		
    		
    		resp = biblioGestionMaterialManager.deleteIngresosxMaterial(txhCodigoIngreso, usuario);    		
    		log.info("RPTA:"+ resp);
    		if("0".equalsIgnoreCase(resp))
    		{
    			request.setAttribute("mensaje","OK");
    		}else{
    			request.setAttribute("mensaje","ERROR");
    		}	
    	}
    	
    	model.addObject("txhCodigoUnico", txhCodigoUnico);
    	model.addObject("lstRegistroIngreso",
    	biblioGestionMaterialManager.getAllIngresosxMaterial(txhCodigoUnico,null));   	
    	
	   	//log.info("handleRequest:FIN");
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_iframe_ingreso", "model", model);
	}



}
