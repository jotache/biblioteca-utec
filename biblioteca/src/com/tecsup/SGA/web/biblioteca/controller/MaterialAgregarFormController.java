package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.MtoAgregarTipoPerfilCommand;
import com.tecsup.SGA.web.reclutamiento.command.ProcesoCommand;

public class MaterialAgregarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MaterialAgregarFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    		
    	
    	MtoAgregarTipoPerfilCommand command = new MtoAgregarTipoPerfilCommand();
    	
    	String codDetalle = request.getParameter("txhCodDetalle");
    	 	
    	if ( codDetalle != null ) if ( !codDetalle.trim().equals("") ) cargaDetalle(command, codDetalle);
    	        
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	MtoAgregarTipoPerfilCommand control = (MtoAgregarTipoPerfilCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    	
    		resultado = InsertTablaDetalle(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else control.setMsg("OK");
    	}
    	control.setOperacion("");
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_agregar_tipo_perfil","control",control);		
    }
    
    private String InsertTablaDetalle(MtoAgregarTipoPerfilCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla(CommonConstants.TIPT_PERFIL);
    		obj.setDescripcion(control.getDescripcion().trim());
    		obj.setUsuario(control.getCodEvaluador());//(CommonConstants.e_v_ttde_usu_crea);
    
    		obj.setCodTipoTablaDetalle(this.tablaDetalleManager.InsertTablaDetalle(obj
    				, control.getCodEvaluador()));//CommonConstants.e_v_ttde_usu_crea));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(MtoAgregarTipoPerfilCommand command, String codDetalle)
    {   
    	TipoTablaDetalle obj = new TipoTablaDetalle();
		obj = (TipoTablaDetalle)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PERFIL
				, codDetalle, "", "","", "", "", "", CommonConstants.TIPO_ORDEN_DSC).get(0);
		command.setDescripcion(obj.getDescripcion().trim());		
		command.setCodDetalle(codDetalle);
    }
    
 }
