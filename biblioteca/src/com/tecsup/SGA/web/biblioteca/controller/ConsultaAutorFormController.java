package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;

public class ConsultaAutorFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConsultaAutorFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;	 
	private TablaDetalleManager tablaDetalleManager;
	
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		ConsultaAutorCommand command = new ConsultaAutorCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());
    	log.info("formBackingObject LLenarDatos");
    	log.info((String)request.getParameter("codSelTipo"));
    	command.setCodTipo((String)request.getParameter("codSelTipo"));
    	LlenarDatos(command, request);
    	
    	request.setAttribute("lstrURL", command.getLstrURL()); 
    	//ACTUALIZADO NAPA
    	command.setCodPersona(CommonConstants.COD_PERSONA);
    	command.setCodEmpresa(CommonConstants.COD_EMPRESA);
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
    	ConsultaAutorCommand control = (ConsultaAutorCommand) command;
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR")){
    		LlenarDatos(control, request);
    	}
    	else{ if (control.getOperacion().trim().equals("ELIMINAR"))
    	      {resultado = DeleteMaterial(control);
    	      log.info("RPTA:" +resultado);
	    		if ( resultado.equals("-1")) control.setMsg("ERROR");
	    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
	    		      else
	    			  {control.setMsg("OK");
	    			   LlenarDatos(control, request);
	    			  }
    		  }
    		
    	      }
    	}
    	control.setOperacion("");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_autor","control",control);		
    }

    public String DeleteMaterial(ConsultaAutorCommand control){
    	try
    	{   String resultado="";
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla("");
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_AUTOR);
    		obj.setDscValor1(control.getCodSelec());
    		obj.setDscValor2(control.getCodValor());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    	
    		obj.setDescripcion(control.getDescripSelec());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodEvaluador());
    		 	
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		//log.info("Resultado: "+resultado);	
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    } 
    
    public void LlenarDatos(ConsultaAutorCommand control, HttpServletRequest request){
    	
    	log.info("LLenarDatos:control.getCodTipo():"+control.getCodTipo());
    	
    	String descripcionAutor=(String) request.getParameter("descripcionAutor");
    	String codigoAutor=(String) request.getParameter("codigoAutor");
    	
    	if (descripcionAutor!=null){
    		if (!(descripcionAutor.equals("")))
    			control.setDescripcionAutor(descripcionAutor);
    	}		
    	if (codigoAutor!=null){
    		if (!(codigoAutor.equals("")))
    			control.setCodigoAutor(codigoAutor);
    	}
    	
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_AUTOR);
		obj.setCodDetalle(control.getCodigoAutor());
		obj.setDescripcion(control.getDescripcionAutor());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		
		control.setListaAutores(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_AUTOR, "", 
				control.getDescripcionAutor(), "", control.getCodigoAutor(), control.getCodTipo(),
				"", "", CommonConstants.TIPO_ORDEN_DSC) );
		
		request.getSession().setAttribute("listaAutores", control.getListaAutores());
    }
    
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
    
}
