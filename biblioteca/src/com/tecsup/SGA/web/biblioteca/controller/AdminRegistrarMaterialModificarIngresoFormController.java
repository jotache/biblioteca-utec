package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialModificarIngresoCommand;


public class AdminRegistrarMaterialModificarIngresoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialModificarIngresoFormController.class);
	
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
    public AdminRegistrarMaterialModificarIngresoFormController() {
        super();        
        setCommandClass(AdminRegistrarMaterialModificarIngresoCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)	
    throws ServletException{
    	//log.info("formBackingObject:INI"); 
    	AdminRegistrarMaterialModificarIngresoCommand control = new AdminRegistrarMaterialModificarIngresoCommand();
		if(control.getOperacion()==null){
			control.setTxhCodigoUnico(request.getParameter("txhCodUnico"));
			control = ini(control);
		}
        //log.info("formBackingObject:FIN");
        return control;
   }
	
	private AdminRegistrarMaterialModificarIngresoCommand ini(
			AdminRegistrarMaterialModificarIngresoCommand control) {
		control = iniciaProcedencia(control);
		control = iniciaMoneda(control);
		return control;
	}

	private AdminRegistrarMaterialModificarIngresoCommand iniciaMoneda(
			AdminRegistrarMaterialModificarIngresoCommand control) {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MONEDA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstMoneda(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		return control;
	}

	private AdminRegistrarMaterialModificarIngresoCommand iniciaProcedencia(
			AdminRegistrarMaterialModificarIngresoCommand control) {

    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_PROCEDENCIA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstProcedencia(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		return control;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	String resp = "";

    	AdminRegistrarMaterialModificarIngresoCommand control = (AdminRegistrarMaterialModificarIngresoCommand) command;
    	
    	UsuarioSeguridad usr = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	control.setUsuario(usr.getIdUsuario());
    	log.info("OPERACION:"+control.getOperacion()+"|");
    	if("irConsultar".equalsIgnoreCase(control.getOperacion())){
    		//control = irConsultar(control);    		
    	}else if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
    		/*log.info("control.getTxhCodigoUnico()|"+control.getTxhCodigoUnico()+"|");
    		log.info("control.getTxtFecIngreso()|"+control.getTxtFecIngreso()+"|");
    		log.info("control.getTxtFecBaja()|"+control.getTxtFecBaja()+"|");
    		log.info("control.getCboProcedencia()|"+control.getCboProcedencia()+"|");
    		log.info("control.getCboMoneda()|"+control.getCboMoneda()+"|");
    		log.info("control.getTxtPrecio()|"+control.getTxtPrecio()+"|");
    		log.info("control.getCboEstado()|"+control.getCboEstado()+"|");
    		log.info("control.getTxtObservacion()|"+control.getTxtObservacion()+"|");
    		log.info("control.getUsuario()|"+control.getUsuario()+"|");*/
    		resp = biblioGestionMaterialManager.insertIngresosxMaterial(
    		control.getTxhCodigoUnico(),
    		control.getTxtFecIngreso(),
    		control.getTxtFecBaja(),
    		control.getCboProcedencia(),
    		control.getCboMoneda(),
    		control.getTxtPrecio(),
    		control.getCboEstado(),
    		control.getTxtObservacion(),
    		control.getUsuario()
    		);
    		log.info("RPTA: "+resp);
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_registrar_ingreso","control",control);		
    }

}
