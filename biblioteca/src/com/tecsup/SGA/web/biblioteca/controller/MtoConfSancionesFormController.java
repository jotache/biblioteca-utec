package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;

public class MtoConfSancionesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoConfSancionesFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		MtoConfSancionesCommand command = new MtoConfSancionesCommand();		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
		cargaCabecera(command);
		cargaBandeja(command);
		cargaNroOcurrencia(command);
		
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	MtoConfSancionesCommand control = (MtoConfSancionesCommand)command;
    	log.info("OPERACION:" + control.getOperacion());


    	if (control.getOperacion().trim().equals("GRABAR")) {
    		String resultado=""; 
			resultado=UpdateSanciones(control);
	    	log.info("RPTA:" + resultado);
			cargaCabecera(control);
			cargaBandeja(control);
			cargaNroOcurrencia(control);
			if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{    		
    			control.setMsg("OK");
    		}
    	}		
    	    	
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_mto_conf_sanciones","control",control);		
    }
    public void cargaCabecera(MtoConfSancionesCommand control){    	
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
    	obj.setCodTipoTabla("");
    	obj.setCodTipoTablaDetalle(CommonConstants.TIPT_RANGO_SANCIONES);
    	obj.setCodDetalle("");
    	obj.setDescripcion("");
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	
    	control.setListaRangoDias(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    }
    public void cargaBandeja(MtoConfSancionesCommand control){
    	control.setListaSanciones(this.biblioMantenimientoManager.Sanciones());
    	
    }
    public void cargaNroOcurrencia(MtoConfSancionesCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
    	obj.setCodTipoTabla("");
    	obj.setCodTipoTablaDetalle(CommonConstants.TIPT_NRO_OCURRENCIAS);
    	obj.setCodDetalle("");
    	obj.setDescripcion("");
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	
    	List x=this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj);
    	
    	if(x.size()>0){
    		TipoTablaDetalle obj2 = new TipoTablaDetalle();
    		obj2=(TipoTablaDetalle)x.get(0);
    		control.setNumeroOcurrencias(obj2.getDscValor1());
    	}
    	else{
    		control.setNumeroOcurrencias("");
    	}
    }
    public String UpdateSanciones(MtoConfSancionesCommand control){
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
 
    		obj.setDscValor1(control.getCadena());
    		obj.setDscValor2(control.getNumeroRegistros());
    		obj.setDscValor3(control.getNumeroOcurrencias());
    		obj.setUsuario(control.getCodUsuario());
    		    		    		    	
    		resultado=this.biblioMantenimientoManager.UpdateSanciones(obj);    			    			
    			
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
}
