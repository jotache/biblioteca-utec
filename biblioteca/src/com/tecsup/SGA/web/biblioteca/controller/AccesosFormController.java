package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.AccesosCommand;

import com.tecsup.SGA.modelo.UsuarioPM;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class AccesosFormController extends SimpleFormController{
    private static Log log = LogFactory.getLog(AccesosFormController.class);

    BiblioMantenimientoManager biblioMantenimientoManager;

    public void setBiblioMantenimientoManager(BiblioMantenimientoManager biblioMantenimientoManager) {
        this.biblioMantenimientoManager = biblioMantenimientoManager;
    }

    protected Object formBackingObject(HttpServletRequest request) throws ServletException {
        AccesosCommand command = new AccesosCommand();

        log.info("formBackingObject:INI");

        command.setUsuCreacion(ServletRequestUtils.getStringParameter(request,"txhCodUsuario"));
        cargaDatos(command);

        log.info("formBackingObject:FIN");
        return command;
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        log.info("initBinder:INI");

        NumberFormat nf = NumberFormat.getNumberInstance();
        binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class, nf, true));

        log.info("initBinder:FIN");
    }

    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors) throws Exception {
        log.info("processFormSubmission:INI");
        log.info("processFormSubmission:FIN");
        return super.processFormSubmission(request, response, command, errors);
    }

    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        AccesosCommand control = (AccesosCommand)command;

        log.info("onSubmit:INI");
        log.info("OPERACION:" + control.getOperacion());

//        if ("GUARDAR".equals(control.getOperacion())){
//
//            if (false) control.setMsg("ERROR");
//            else control.setMsg("OK");
//
//        }

        if ("ELIMINAR".equals(control.getOperacion())){

            if (this.eliminarUsuarioApoyo(control)){
                //Cargo nuevamente la lista de los usuarios.
                cargaDatos(control);
                control.setMsg("OK_ELIMINAR");
            }else{
                control.setMsg("ERROR_ELIMINAR");
            }
        }

        log.info("onSubmit:FIN");
        return new ModelAndView(this.getFormView(),"control",control);
    }

   private void guardar(AccesosCommand control){

//       control.setGraba(this.biblioMantenimientoManager.InsertParametrosReservados(CommonConstants.TIPT_PARAMETROS_RESERVA
//               , control.getEsmadi(), control.getEsmito(), control.getEsdipe()
//               , control.getInmadi(), control.getInmito(), control.getIndipe()
//               , control.getBinrodipe(),control.getVisualizar(),"", control.getCodUsuario()));
   }

    private void cargaDatos(AccesosCommand control) {
        List<UsuarioPM> list = this.biblioMantenimientoManager.GetAllUsuariosAcceso();

        for (UsuarioPM usuarioPM : list) {
            System.out.println(usuarioPM.getNomUsuario());
        }

        control.setUsuarios(list);
    }

    private boolean eliminarUsuarioApoyo(AccesosCommand control){
        try{
            this.biblioMantenimientoManager.DeleteAccesosIP(control.getCodSujeto());
            this.biblioMantenimientoManager.DeleteOpcionesApoyo(control.getCodSujeto());
            this.biblioMantenimientoManager.DeleteRolesApoyo(control.getCodUsuario());
        }catch(Exception ex){
            ex.printStackTrace();
            log.error(ex);
            return false;
        }
        return true;
    }

}
