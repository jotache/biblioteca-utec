package com.tecsup.SGA.web.biblioteca.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;

public class AdminRegistrarMaterialIframeDocumentosRelacionadosController  implements Controller  {
	
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialIframeDocumentosRelacionadosController.class);
	
	BiblioGestionMaterialManager biblioGestionMaterialManager;
	
    public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	//log.info("handleRequest:INI");		    	    	
    	ModelMap model = new ModelMap();   	
    	
    	String txhCodigoUnico = request.getParameter("txhCodigoUnico");
    	
    	/*String uploadDirArchivo = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_IP +
    	CommonConstants.STR_RUTA_MATERIAL_BIB_DOCUMENTOS;*/
    	
    	String uploadDirArchivo = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_DOCUMENTOS;
    	
    	request.setAttribute("msgRutaServer",uploadDirArchivo);
    	
    	String resp = "";
    	String usuario = request.getParameter("usuario");
    	//log.info("ID ELIMINACION|"+usuario+"|");
    	log.info("OPERACION:" +  request.getParameter("operacion"));
    	
    	if("irEliminar".equalsIgnoreCase(request.getParameter("operacion"))){
    		//CODIGO SELECCIONADO 
    		String txhCodigoUnicoDocumento =  request.getParameter("txhCodigoUnicoDocumento");
    		//log.info("txhCodigoUnicoDocumento>"+txhCodigoUnicoDocumento+">");
    		resp = biblioGestionMaterialManager.deleteArchAdjuntoxMaterial(txhCodigoUnicoDocumento,usuario);
    		log.info("RPTA:" + resp);
    		if("0".equalsIgnoreCase(resp))
    		{
    			request.setAttribute("mensaje","OK");
    		}else{
    			request.setAttribute("mensaje","ERROR");
    		}	
    	}
    	
    	//log.info("CODIGO UNICO:"+txhCodigoUnico+":");
    	model.addObject("txhCodigoUnico", txhCodigoUnico);
    	model.addObject("lstDocumentosRelacionados",
    			biblioGestionMaterialManager.getAllArchAdjuntoxMaterial(txhCodigoUnico));
    	
	   	//log.info("handleRequest:FIN");
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_iframe_documentos_relacionados", "model", model);
	}

}
