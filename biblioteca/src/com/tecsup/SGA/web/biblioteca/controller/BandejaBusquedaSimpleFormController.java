package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.BandejaBusquedaSimpleCommand;


public class BandejaBusquedaSimpleFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(BandejaBusquedaSimpleFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	public PortalWebBibliotecaManager getPortalWebBibliotecaManager() {
		return portalWebBibliotecaManager;
	}
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		
		BandejaBusquedaSimpleCommand command= new BandejaBusquedaSimpleCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		
		LlenarComboTipoMaterial(command);
    	LlenarOrdenarAndBuscarBy(command);
    	LLenarComboSede(command);
    	//***********************************************************************
    	
    	//***********************************************************************
		
		//log.info("formBackingObject:FIN");
        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
	
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
	throws Exception {
	return super.processFormSubmission(request, response, command, errors);
			}
			
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
		BindException errors)
		throws Exception {
		//log.info("onSubmit:INI");
		
		BandejaBusquedaSimpleCommand control= (BandejaBusquedaSimpleCommand) command;
				
		//log.info("onSubmit:FIN");	
		return new ModelAndView("/biblioteca/userWeb/bandejaBusquedaSimple","control",control);		
	}
	
	public void LlenarComboTipoMaterial(BandejaBusquedaSimpleCommand control){
    	
	   	 TipoTablaDetalle obj= new TipoTablaDetalle();
		        obj.setDscValor3("");
				obj.setCodTipoTabla("");
				obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
				obj.setCodDetalle("");
				obj.setDescripcion("");
				obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
				control.setCodListaTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
					   		   	
	}
	
	public void LlenarOrdenarAndBuscarBy(BandejaBusquedaSimpleCommand control){
		TipoTablaDetalle obj = new TipoTablaDetalle();
		obj.setDscValor3("");    	
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_BUSQUEDA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setCodListaOrdenarBy(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));		
       control.setCodListaBuscarBy(control.getCodListaOrdenarBy());
	}
	public void LLenarComboSede(BandejaBusquedaSimpleCommand control){
		TipoTablaDetalle obj = new TipoTablaDetalle();
		obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SEDE_PREFERENTE);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setCodListaSede(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
	}
}
