package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.PortalAcercaDeCommand;

public class PortalAcercaFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(PortalAcercaFormController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    									throws ServletException {
		
		PortalAcercaDeCommand cmd = new PortalAcercaDeCommand();
				
		return cmd;
	}
}
