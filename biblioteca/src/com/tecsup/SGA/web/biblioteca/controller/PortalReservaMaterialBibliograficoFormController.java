package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.service.biblioteca.Impl.BiblioProcesoSancionesManagerImpl;
import com.tecsup.SGA.web.biblioteca.command.PortalReservaMaterialBibliograficoCommand;

public class PortalReservaMaterialBibliograficoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(PortalReservaMaterialBibliograficoFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	private BiblioProcesoSancionesManager biblioProcesoSancionesManager;

	public void setBiblioProcesoSancionesManager(
			BiblioProcesoSancionesManager biblioProcesoSancionesManager) {
		this.biblioProcesoSancionesManager = biblioProcesoSancionesManager;
	}
	
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		PortalReservaMaterialBibliograficoCommand command= new PortalReservaMaterialBibliograficoCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	command.setTxtFechaBD((String)request.getParameter("txhFechaBD"));
    	command.setCodTipoMaterial((String)request.getParameter("txhCodigoUnico"));
    	    	    
	     return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	PortalReservaMaterialBibliograficoCommand control=(PortalReservaMaterialBibliograficoCommand) command;
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if(control.getOperacion().equals("GUARDAR"))
    	{   
    		    		
    		resultado = this.biblioProcesoSancionesManager.insertReservaMaterial(
    				control.getCodTipoMaterial(),
    				control.getTxtFecha(),
    				control.getCodEvaluador(),
    				control.getCodEvaluador()
    				);
    		
    		log.info("RPTA:" + resultado);
    	   	
    	   	String cabMensaje = resultado.substring(0,1);    	   	

    	   	control.setMsg(cabMensaje);
    	   	
    	   if(cabMensaje.equalsIgnoreCase("2")){
    	   		
        	   	StringTokenizer tkn = new StringTokenizer(resultado,"|");
        		String msg1 = "";
        		String msg2 = "";
        		String msg3 = "";
        		
        		if(tkn.hasMoreTokens())
        			msg1 = tkn.nextToken();
        		if(tkn.hasMoreTokens())
        			msg2 = tkn.nextToken();
        		
        		request.setAttribute("msg1",msg1);
    	   		request.setAttribute("msg2",msg2);   	   		
    	   		
    	   	}else if(cabMensaje.equalsIgnoreCase("5")){
    	   		
    	 	   	StringTokenizer tkn = new StringTokenizer(resultado,"|");
        		String msg1 = "";
        		String msg2 = "";
        		String msg3 = "";        		
        		
        		if(tkn.hasMoreTokens())
        			msg1 = tkn.nextToken();
        		if(tkn.hasMoreTokens())
        			msg2 = tkn.nextToken();
        		if(tkn.hasMoreTokens())
        			msg3 = tkn.nextToken();
        		
    	   		request.setAttribute("msg1",msg1);
    	   		request.setAttribute("msg2",msg2);
    	   		request.setAttribute("msg3",msg3);    	   		
    	   	}
    	   
    	}	
    	
        return new ModelAndView("/biblioteca/userWeb/bib_reserva_material_bibliografico","control",control);		
    }
	
}
