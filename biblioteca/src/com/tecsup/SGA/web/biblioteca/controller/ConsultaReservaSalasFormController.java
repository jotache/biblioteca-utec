package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.web.biblioteca.command.ConsultaReservaSalasCommand;

public class ConsultaReservaSalasFormController extends SimpleFormController {
  
  private static Log log = LogFactory
      .getLog(ConsultaReservaSalasFormController.class);
  private BiblioMantenimientoManager biblioMantenimientoManager;
  private PortalWebBibliotecaManager portalWebBibliotecaManager;
  private BiblioProcesoSancionesManager biblioProcesoSancionesManager;
  private ComunManager comunManager;
  
  public void setComunManager(ComunManager comunManager) {
    this.comunManager = comunManager;
  }
  
  public void setBiblioProcesoSancionesManager(
      BiblioProcesoSancionesManager biblioProcesoSancionesManager) {
    this.biblioProcesoSancionesManager = biblioProcesoSancionesManager;
  }
  
  public void setPortalWebBibliotecaManager(
      PortalWebBibliotecaManager portalWebBibliotecaManager) {
    this.portalWebBibliotecaManager = portalWebBibliotecaManager;
  }
  
  public void setBiblioMantenimientoManager(
      BiblioMantenimientoManager biblioMantenimientoManager) {
    this.biblioMantenimientoManager = biblioMantenimientoManager;
  }
  
  protected Object formBackingObject(HttpServletRequest request)
      throws ServletException {
    boolean flagLabQuimica = false;
    UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad) request.getSession()
        .getAttribute("usuarioSeguridad");
    
    ConsultaReservaSalasCommand command = new ConsultaReservaSalasCommand();
    Anios anios = new Anios();
    
    if (command.getOperacion() == null)
    	command.setSedeSel(request.getParameter("txhCodSede"));
    
	    command.setSedeUsuario(request.getParameter("txhSedeUsuario"));
	    command.setCodPeriodo((String) request.getParameter("txhCodPeriodo"));
	    command.setCodEvaluador((String) request.getParameter("txhCodUsuario"));
	    command.setEstado((String) request.getParameter("txhEstado"));
	    command.setFlgUsuAdmin(request.getParameter("flgUsuAdmin"));
    
	    if (usuarioSeguridad != null && usuarioSeguridad.getCiclo() != null
	        && usuarioSeguridad.getCodigoEspecialidad() != null) {
	      flagLabQuimica = (usuarioSeguridad.getCiclo().equals("3")
	          || usuarioSeguridad.getCiclo().equals("4")
	          || usuarioSeguridad.getCiclo().equals("5") 
	          || usuarioSeguridad.getCiclo().equals("6"))
	          && usuarioSeguridad.getCodigoEspecialidad().equals("11");
	    }
    
	    command.setFlgLabQuimica(flagLabQuimica ? "1" : "0");
	    
	    command.setListaHoras(this.portalWebBibliotecaManager
	        .ListarHorasAtencionPorSede(command.getSedeSel() == null ? "L"
	            : command.getSedeSel()));
	    
	    command.setConsteDisponible(CommonConstants.FLAG_DISPONIBLE);

	    log.info("formBackingObject - Operaci�n:" + command.getOperacion());
	    if (command.getOperacion() == null)
	    	ComboConsultaTipoSalas(command);
	    
	    command.setFechaBD(comunManager.getFecha().getFecha());
	    
    
    return command;
  }
  
  protected void initBinder(HttpServletRequest request,
      ServletRequestDataBinder binder) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class,
        nf, true));
  }
  
  public ModelAndView processFormSubmission(HttpServletRequest request,
      HttpServletResponse response, Object command, BindException errors)
      throws Exception {
    return super.processFormSubmission(request, response, command, errors);
  }
  
  public ModelAndView onSubmit(HttpServletRequest request,
      HttpServletResponse response, Object command, BindException errors)
      throws Exception {
    
    String resultado = "";
    ConsultaReservaSalasCommand control = (ConsultaReservaSalasCommand) command;
    log.info("onSubmit()");    
    
    control.setListaHoras(this.portalWebBibliotecaManager
        .ListarHorasAtencionPorSede(control.getSedeSel() == null ? "L"
            : control.getSedeSel()));
    
    log.info("OPERACION:" + control.getOperacion());
    if (control.getOperacion().equals("BUSCAR")) {
      BuscarSalas(control);
      ComboConsultaTipoSalas(control);
      CapacidadMaxima(control);
    }
    // modificado RNapa
    else if (control.getOperacion().equals("APLICAR")) {
      resultado = Aplicar(control);
      log.info("RPTA:" + resultado);
      String cabMensaje = resultado.substring(0, 1);
      if ("1".equalsIgnoreCase(cabMensaje)) {
        control.setMsg("OK_APLICAR");
      } else if ("0".equalsIgnoreCase(cabMensaje)) {
        control.setMsg("USU_NO_CONFIG");
      } else if ("3".equalsIgnoreCase(cabMensaje)) {
        control.setMsg("FECHA_HORA_INVALIDA");
      } else if ("4".equalsIgnoreCase(cabMensaje)) {
        control.setMsg("RECURSO_YA_NO_ESTA_RESERVADO");
      } else if ("2".equalsIgnoreCase(cabMensaje)) {
        String msg1 = "";
        String msg2 = "";
        String msg3 = "";
        String msg4 = "";
        
        StringTokenizer tkn = new StringTokenizer(resultado, "|");
        if (tkn.hasMoreTokens())
          msg1 = tkn.nextToken();
        if (tkn.hasMoreTokens())
          msg2 = tkn.nextToken();
        if (tkn.hasMoreTokens())
          msg3 = tkn.nextToken();
        if (tkn.hasMoreTokens())
          msg4 = tkn.nextToken();
        
        request.setAttribute("msg1", msg1);
        request.setAttribute("msg2", msg2);
        request.setAttribute("msg3", msg3);
        request.setAttribute("msg4", msg4);
        
        control.setMsg("APL_SANCION");
        
      } else {
        control.setMsg("ERROR_APLICAR");
      }
      
      BuscarSalas(control);
    }
    // ***********************************************
    else if (control.getOperacion().equals("NOMBRE_SALA")) {
      CapacidadMaxima(control);
      ComboConsultaTipoSalas(control);
      ComboConsultaNombreSalas(control);
    }
    // ***********************************************
    else if (control.getOperacion().equals("ELIMINAR")) {
      resultado = DeteleSalaReserva(control);
      log.info("RPTA:" + resultado);
      if (resultado.equals("-1"))
        control.setMsg("ERROR_ELIMINAR");
      else {
        if (resultado.equals("-2"))
          control.setMsg("DUPLICADO");
        else {
          control.setMsg("OK_ELIMINAR");
          BuscarSalas(control);
        }
      }
    }
    // ***********************************************
    else if (control.getOperacion().equals("TIPO_SALA")) {        
    	ComboConsultaTipoSalas(control);
    	ComboConsultaNombreSalas(control);
    }
    // ***********************************************
    else if (control.getOperacion().equals("LIMPIAR_NOMBRE_SALA")) {
      control.setCodListaNombreSala(new ArrayList());
    }
    // ***********************************************
    return new ModelAndView("/biblioteca/userWeb/bib_consulta_reserva_salas",
        "control", control);
  }
  
  public void ComboConsultaTipoSalas(ConsultaReservaSalasCommand control) {
    TipoTablaDetalle obj = new TipoTablaDetalle();
    obj.setDscValor3("");
    if(control.getSedeUsuario()!=null && control.getSedeUsuario().equals("U"))
    	obj.setDscValor4("U"); // se marca con �U� los tipos de Salas que usa Utec
    else
    	obj.setDscValor4("T");
        
    obj.setDscValor5(""); //se marca con �1� a las salas que solo podr�n ser seleccionadas por personal administrativo    
    obj.setDscValor6("");
    obj.setCodTipoTabla("");
    obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SALA);
    obj.setCodDetalle("");
    obj.setDescripcion("");
    obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    control.setCodListaTipoSala(this.biblioMantenimientoManager
        .getAllTablaDetalleBiblioteca(obj));
  }
  
  public void ComboConsultaNombreSalas(ConsultaReservaSalasCommand control) {
    TipoTablaDetalle obj = new TipoTablaDetalle();
    
    obj.setDscValor3(CommonConstants.TIPT_TIPO_SALA);
    obj.setCodTipoTabla(control.getCodTipoSala());
    obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SALA);
    obj.setCodDetalle("");
    obj.setDescripcion("");
    obj.setDscValor6(control.getSedeSel());
    obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    control.setCodListaNombreSala(this.biblioMantenimientoManager
        .getAllTablaDetalleBiblioteca(obj));
  }
  
  public void BuscarSalas(ConsultaReservaSalasCommand control) {
    
    Sala sala = new Sala();
    if (control.getCodNombreSalas().equals("-1"))
      control.setCodNombreSalas("");
    if (control.getTxtFechaInicio().equals("-1"))
      control.setTxtFechaInicio("");
    if (control.getTxtFechaFin().equals("-1"))
      control.setTxtFechaFin("");
    if (control.getCodHoraInicio().equals("-1"))
      control.setCodHoraInicio("");
    if (control.getCodHoraFin().equals("-1"))
      control.setCodHoraFin("");
    
    sala.setCodTipoSala(control.getCodTipoSala().trim());
    sala.setNombreSala(control.getCodNombreSalas().trim());
    sala.setFechaInicio(control.getTxtFechaInicio().trim());
    sala.setFechaFin(control.getTxtFechaFin().trim());
    sala.setHoraIni(control.getCodHoraInicio().trim());
    sala.setHoraFin(control.getCodHoraFin().trim());
    sala.setFlagDisponibilidad(control.getDisponible().trim());
    sala.setCodUsuario(control.getCodigo());
    
    sala.setEstadoPendiente(control.getEstadoPendiente());
    sala.setEstadoReservado(control.getEstadoReservado());
    sala.setEstadoEjecutado(control.getEstadoEjecutado());
    
    sala.setSede(control.getSedeSel());
    
    if (control.getDisponible().trim().equals("0")) {
      control.setListaConsultaReservaSala(this.portalWebBibliotecaManager
          .getReservaSalas(sala));
      control.setTamListaReservaSala01(""
          + control.getListaConsultaReservaSala().size());
    } else {
      sala.setFechaFin("");
      sala.setCodUsuario("");
      control.setListaConsultaReservaSala02(this.portalWebBibliotecaManager
          .getReservaSalas(sala));
      control.setTamListaReservaSala02(""
          + control.getListaConsultaReservaSala02().size());
    }
    
    ComboConsultaNombreSalas(control);
  }
  
  public void CapacidadMaxima(ConsultaReservaSalasCommand control) {
    
    Sala sala = new Sala();
    sala.setCodTipoSala(control.getCodNombreSalas());
    
    List atila = new ArrayList();
    atila = this.portalWebBibliotecaManager.getMaximaCapacidadSalas(sala);
    
    Sala sal = new Sala();
    
    if (atila.size() > 0) {
      sal = (Sala) atila.get(0);
      control.setTxtCapacidadMaxima(sal.getCapacidadMax());
      control.setFechaBD(sal.getFechaBD());
      control.setTxtVacantes(sal.getVacantes());
    }
  }
  
  public String DeteleSalaReserva(ConsultaReservaSalasCommand control) {
    try {
      String nroRegistros = String.valueOf(control
          .getCodComponentesSeleccionados().length() / 2);
      String resultado = "";
      
      String usuCrea = control.getCodEvaluador();
      resultado = this.portalWebBibliotecaManager.DeleteReservaSalas(
          control.getCodComponentesSeleccionados(), nroRegistros, usuCrea);
      
      return resultado;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
  
  private String Aplicar(ConsultaReservaSalasCommand control) {
    try {
      
      String resultado = biblioProcesoSancionesManager
          .insertSancionReservaSalas(control.getUsuarioReserva(),
              control.getCodSala(), control.getCodReserva(),
              control.getCodEvaluador(), "0002");
      
      return resultado;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
