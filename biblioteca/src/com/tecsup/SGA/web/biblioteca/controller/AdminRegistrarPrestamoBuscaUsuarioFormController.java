package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarPrestamoBuscaUsuarioCommand;

public class AdminRegistrarPrestamoBuscaUsuarioFormController extends
		SimpleFormController {

	private static Log log = LogFactory.getLog(AdminRegistrarPrestamoBuscaUsuarioFormController.class);	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public AdminRegistrarPrestamoBuscaUsuarioFormController(){
		super();
		setCommandClass(AdminRegistrarPrestamoBuscaUsuarioCommand.class);
	}

	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)	
    	throws ServletException{
		AdminRegistrarPrestamoBuscaUsuarioCommand control = new AdminRegistrarPrestamoBuscaUsuarioCommand();
		control.setTxhCodUsuario(request.getParameter("txhCodUsuario"));
		control.setTxhTipo(request.getParameter("txhTipo"));
		if (control.getOperacion()==null){
			consultar(control);
		}
		return control;
	}
	
	private AdminRegistrarPrestamoBuscaUsuarioCommand consultar(AdminRegistrarPrestamoBuscaUsuarioCommand control){
		control.setLstResultado(this.biblioGestionMaterialManager.getUsuariosXCodigo(control.getTxhCodUsuario(), control.getTxhTipo()));
		return control;
	}

}
