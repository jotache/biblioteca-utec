package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaEditorialFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConsultaEditorialFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		ConsultaEditorialCommand command = new ConsultaEditorialCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());
    	if (command.getOperacion()==null)
    		LlenarDatos(command, request);
    	    	
    	request.setAttribute("lstrURL", command.getLstrURL());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
    	ConsultaEditorialCommand control = (ConsultaEditorialCommand) command;
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	//System.out.println("onSubmit:getDescripcionEditorial:"+control.getDescripcionEditorial());
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{   LlenarDatos(control, request);
    		    		
    	}
    	else{ if (control.getOperacion().trim().equals("ELIMINAR"))
    	      {resultado = DeleteEditorial(control);
    	      log.info("RPTA:" + resultado);
	    		if ( resultado.equals("-1")) control.setMsg("ERROR");
	    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
	    		      else
	    			  {control.setMsg("OK");
	    			   LlenarDatos(control, request);
	    			   
	    			   }
    		
    		}
    		
    	      }
    	}
    	control.setOperacion("");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_editorial","control",control);		
    }
    public String DeleteEditorial(ConsultaEditorialCommand control){
    	try
    	{   String resultado="";
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla("");
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_EDITORIAL);
    		obj.setDscValor1(control.getCodSelec());
    		obj.setDscValor2(control.getCodValor());
    		obj.setDescripcion(control.getDescripSelec());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodEvaluador());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		 	
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		//log.info("Resultado: "+resultado);	
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }

    public void LlenarDatos(ConsultaEditorialCommand control, HttpServletRequest request){
    	
    	String descEditorial=(String) request.getParameter("descripcionEditorial");
    	String codEditorial=(String) request.getParameter("codigoEditorial");
    	//System.out.println("Descripcion paginado:"+dato);
    	if (descEditorial!=null){
    		if (!(descEditorial.equals("")))
    			control.setDescripcionEditorial(descEditorial);
    	}		
    	if (codEditorial!=null){
    		if (!(codEditorial.equals("")))
    			control.setCodigoEditorial(codEditorial);
    	}
    	
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodDetalle(codEditorial);
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_EDITORIAL);
		obj.setCodDetalle(control.getCodigoEditorial());
		//obj.setDescripcion(control.getDescripcionEditorial());
		obj.setDescripcion(descEditorial);
		//System.out.println("control.getDescripcionEditorial():"+control.getDescripcionEditorial());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setListaEditoriales(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		request.getSession().setAttribute("listaEditorial", control.getListaEditoriales());
    	
    }
    
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
    
}
