package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.CiudadesModificarCommand;


public class CiudadesModificarFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(CiudadesModificarFormController.class);
	//private TablaDetalleManager tablaDetalleManager;

	/*public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}*/
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI");   	
    	CiudadesModificarCommand command = new CiudadesModificarCommand();
    
    	String codDetalle = (String)request.getParameter("codigo");
    	String dscDetalle = (String)request.getParameter("descripcion");
    	//log.info(codDetalle+" y "+dscDetalle);
    	command.setCodDetalle("");    	
    	//if ( codDetalle != null ) if ( !codDetalle.trim().equals("") ) cargaDetalle(command, codDetalle);    	
        //log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	String resultado = "";
    	CiudadesModificarCommand control = (CiudadesModificarCommand) command;

    	/*if (control.getOperacion().trim().equals("MODIFICAR"))
    	{
    		
    		resultado = UpdateTablaDetalle(control);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else control.setMsg("OK");
    	}*/
    	control.setOperacion("");
		//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_ciudades_modificar","control",control);		
    }
    
    /*private String UpdateTablaDetalle(MtoModificarTipoPerfilCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
 
    		obj.setCodTipoTabla(CommonConstants.TIPT_PERFIL);
    		obj.setDescripcion(control.getDescripcion());
    		obj.setUsuario(CommonConstants.e_v_ttde_usu_crea);
    		obj.setCodDetalle(control.getCodDetalle());
    		
    		if ( !control.getCodDetalle().trim().equals("") )
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.
    			UpdateTablaDetalle(obj, CommonConstants.e_v_ttde_usu_crea));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }*/
    
   /* private void cargaDetalle(MtoModificarTipoPerfilCommand command, String codDetalle)
    {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
		
		obj = (TipoTablaDetalle)this.tablaDetalleManager.getAllTablaDetalle(
				CommonConstants.TIPT_PERFIL, codDetalle, "", "","", "", "", 
				"", CommonConstants.TIPO_ORDEN_DSC).get(0);
		System.out.print(" descripcion del display:table seleccionado: "+ 
		obj.getDescripcion()+ "y el codigo es: "+ codDetalle);
		command.setDescripcion(obj.getDescripcion());		
		command.setCodDetalle(codDetalle);
    }*/
}
