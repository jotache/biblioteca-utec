package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.BibliotecaReglamentoCommand;

public class BibliotecaReglamentoFormController  extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(BibliotecaReglamentoFormController.class);
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public PortalWebBibliotecaManager getPortalWebBibliotecaManager() {
		return portalWebBibliotecaManager;
	}

	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		
		BibliotecaReglamentoCommand command= new BibliotecaReglamentoCommand();
		VerReglamento(command, request);
		
		//log.info("formBackingObject:FIN: TamaņoListaCuidades");//+command.getListaCiudades().size());
    	return command;
	}
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	
    	BibliotecaReglamentoCommand control= (BibliotecaReglamentoCommand) command;
    	
    	//log.info("onSubmit:FIN");
    	return new ModelAndView("/biblioteca/userWeb/bib_reglamento","control",control);
    	
    }

    public void VerReglamento(BibliotecaReglamentoCommand control, HttpServletRequest request){
    	Secciones seccion= new Secciones();
    	seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_REGLAMENTOS);
    	TipoTablaDetalle obj= new TipoTablaDetalle();
    	obj.setCodTipoTablaDetalle("");
    	//log.info("Seccion: "+seccion.getCodTipo());    	
		List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
		request.getSession().setAttribute("listaReglamento", lista);
		
    }
    
}
