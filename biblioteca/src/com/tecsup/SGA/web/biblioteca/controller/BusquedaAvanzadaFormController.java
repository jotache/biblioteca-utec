package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Busqueda;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.BusquedaAvanzadaCommand;

public class BusquedaAvanzadaFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(BusquedaAvanzadaFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public PortalWebBibliotecaManager getPortalWebBibliotecaManager() {
		return portalWebBibliotecaManager;
	}
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		BusquedaAvanzadaCommand command = new BusquedaAvanzadaCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
		//command.setOperacion((String)request.getParameter("txhOperacion"));
    	//command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	//command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());
    	
//    	command.setRuta( CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_IP+CommonConstants.STR_RUTA_MATERIAL_BIB_IMAGENES);
    	command.setRuta(CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_PUERTO_WEB + CommonConstants.DIRECTORIO_PROY + CommonConstants.STR_RUTA_MATERIAL_BIB_IMAGENES);
    	
    	Anios anios= new Anios();
    	//LlenarDatos(command, request);
    	LlenarComboTipoMaterial(command, request);
    	LlenarComboIdiomas(command);
    	LlenarOrdenarAndBuscarBy(command);
    	LlenarComboCondiciones(command);
    	LLenarComboSede(command);
    	command.setConsteSala(CommonConstants.TIPT_FLAG_SALA);
    	command.setConsteDomicilio(CommonConstants.TIPT_FLAG_DOMICILIO);
    	command.setFlagReserva(CommonConstants.FLAG_DISPONIBLE);
    	command.setImagen("&nbsp;");
    	
    	command.setCodListaFechaAnioInicio(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO_BUZON_BIBLIOTECA));
    	command.setCodListaFechaAnioFin(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO_BUZON_BIBLIOTECA));
    	
    	//log.info(command.getCodListaTipoMaterial().size());
    	//request.setAttribute("lstrURL", command.getLstrURL());
        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
	
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
          Object command,
          BindException errors)
	throws Exception {
	return super.processFormSubmission(request, response, command, errors);
			}
			
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
		BindException errors)
		throws Exception {
			
		BusquedaAvanzadaCommand control = (BusquedaAvanzadaCommand) command;
		log.info("OPERACION:" + control.getOperacion());
		if(control.getOperacion().equals("BUSCAR"))
		{   //log.info("BUSCAR");
			BusquedaAvanzada(control, request);
		}
		//log.info("onSubmit:FIN");
		return new ModelAndView("/biblioteca/userWeb/bib_busqueda_avanzada","control",control);		
	}
	
	public void LlenarComboTipoMaterial(BusquedaAvanzadaCommand control, HttpServletRequest request){
    	
   	 TipoTablaDetalle obj= new TipoTablaDetalle();
	        obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			List lista= new ArrayList();
			lista=this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj);
			if(lista!=null)
			control.setCodListaTipoMaterial(lista);
			//request.getSession().setAttribute("listaMateriales", control.getCodListaTipoMaterial());
   	   	
   }

	
	 
	 public void LlenarComboIdiomas(BusquedaAvanzadaCommand control){
	    	TipoTablaDetalle obj = new TipoTablaDetalle();
	    	obj.setDscValor3("");    	
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_IDIOMAS_BIBLIOTECA);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			List lista= new ArrayList();
			lista=this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj);
			if(lista!=null)
			control.setCodListaIdioma(lista);		
	    }
	 
	 public void LlenarOrdenarAndBuscarBy(BusquedaAvanzadaCommand control){
	    	TipoTablaDetalle obj = new TipoTablaDetalle();
	    	obj.setDscValor3("");    	
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_BUSQUEDA);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			List lista= new ArrayList();
			lista=this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj);
			if(lista!=null)
			control.setCodListaOrdenarBy(lista);		
	        
	 }
	 
	 public void LlenarComboCondiciones(BusquedaAvanzadaCommand control){
	    	TipoTablaDetalle obj = new TipoTablaDetalle();
	    	obj.setDscValor3("");    	
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_CONDICIONES_BUSQUEDA);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			List lista= new ArrayList();
			lista=this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj);
			if(lista!=null)
			{control.setCodListaCondiciones(lista);		
		        for(int k=0;k<control.getCodListaCondiciones().size();k++)
		        {   TipoTablaDetalle val = new TipoTablaDetalle();
		            val=(TipoTablaDetalle)control.getCodListaCondiciones().get(k);
		        	//log.info("ID: "+val.getCodTipoTablaDetalle());
		        	//log.info("DESCRIPCION: "+val.getDescripcion());
		        }	
			}
	 }
	 
	 public void LLenarComboSede(BusquedaAvanzadaCommand control){
			TipoTablaDetalle obj = new TipoTablaDetalle();
			obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SEDE_PREFERENTE);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setCodListaSede(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
	}
	 
	public void BusquedaAvanzada(BusquedaAvanzadaCommand control, HttpServletRequest request){
		Busqueda obj= new Busqueda();
		String sala=control.getSelSala();
		String domicilio=control.getSelDomicilio();
		/*GetBusquedaAvanzada(obj.getTipoMaterial(), obj.getCodTipoIdioma(),
				obj.getCodAnioInicio(), obj.getCodAnioFin(), obj.getFlagDisponibilidadSala(), obj.getFlagDisponibilidadDomicilio(),
				obj.getBuscar1(), obj.getTxtTexto1(), obj.getCondicion1(), obj.getBuscar2(), obj.getTxtTexto2(),
				obj.getCondicion2(), obj.getBuscar3(), obj.getTxtTexto3(), obj.getOrderBy())*/
		//log.info("Sala: "+sala+ ">>Domicilio: "+domicilio);
		if(control.getTxtTexto1()==null) control.setTxtTexto1("");
		if(control.getTxtTexto2()==null) control.setTxtTexto2("");
		if(control.getTxtTexto3()==null) control.setTxtTexto3("");
		
		if(control.getCodTipoMaterial().equals("-1")) control.setCodTipoMaterial("");
		if(control.getCodIdioma().equals("-1")) control.setCodIdioma("");
		if(control.getCodFechaAnioInicio().equals("")) control.setCodFechaAnioInicio("");
		if(control.getCodFechaAnioFin().equals("")) control.setCodFechaAnioFin("");
		if(control.getCodFechaAnioFin().equals("")) control.setCodFechaAnioFin("");
		//if(control.getSelSala().equals("-1")) control.setSelSala("");
		//if(control.getSelDomicilio().equals("-1")) control.setSelDomicilio("");
		if(control.getCodSeleccion1().equals("-1")) control.setCodSeleccion1("0");
		
		if(control.getCodCondicion1().equals("-1")) control.setCodCondicion1("0");
		if(control.getCodSeleccion2().equals("-1")) control.setCodSeleccion2("0");
		
		if(control.getCodCondicion2().equals("-1")) control.setCodCondicion2("0");
		if(control.getCodSeleccion3().equals("-1")) control.setCodSeleccion3("0");
		
		if(control.getCodSeleccion4().equals("-1")) control.setCodSeleccion4("0");
			
		obj.setTipoMaterial(control.getCodTipoMaterial());
		obj.setCodTipoIdioma(control.getCodIdioma());
		obj.setCodAnioInicio(control.getCodFechaAnioInicio());
		obj.setCodAnioFin(control.getCodFechaAnioFin());
		obj.setFlagDisponibilidadSala(control.getSelSala());
		obj.setFlagDisponibilidadDomicilio(control.getSelDomicilio());
		obj.setBuscar1(Integer.valueOf(control.getCodSeleccion1()));
		obj.setTxtTexto1(control.getTxtTexto1());
		obj.setCondicion1(Integer.valueOf(control.getCodCondicion1()));
		obj.setBuscar2(Integer.valueOf(control.getCodSeleccion2()));
		obj.setTxtTexto2(control.getTxtTexto2());
		obj.setCondicion2(Integer.valueOf(control.getCodCondicion2()));
		obj.setBuscar3(Integer.valueOf(control.getCodSeleccion3()));
		obj.setTxtTexto3(control.getTxtTexto3());
		obj.setOrderBy(Integer.valueOf(Integer.valueOf(control.getCodSeleccion4())));
		
		/*System.out.println("getTipoMaterial: "+obj.getTipoMaterial());
		System.out.println("getCodTipoIdioma: "+obj.getCodTipoIdioma());
		System.out.println("getCodTipoIdioma: "+obj.getCodAnioInicio());
		System.out.println("getCodAnioFin: "+obj.getCodAnioFin());
		System.out.println("getFlagDisponibilidadSala: "+obj.getFlagDisponibilidadSala());
		System.out.println("getFlagDisponibilidadDomicilio: "+obj.getFlagDisponibilidadDomicilio());
		System.out.println("getBuscar1: "+obj.getBuscar1());
		System.out.println("getTxtTexto1: "+obj.getTxtTexto1());
		System.out.println("getCondicion1: "+obj.getCondicion1());
		System.out.println("getBuscar2: "+obj.getBuscar2());
		System.out.println("getTxtTexto2: "+obj.getTxtTexto2());
		System.out.println("getCondicion2: "+obj.getCondicion2());
		System.out.println("getBuscar3: "+obj.getBuscar3());
		System.out.println("getTxtTexto3: "+obj.getTxtTexto3());
		System.out.println("getOrderBy: "+obj.getOrderBy());*/
		
		control.setSelDomicilio(domicilio);
		control.setSelSala(sala);
		
		List listaBusquedaAvanzada= this.portalWebBibliotecaManager.GetBusquedaAvanzada(obj);
		
		if(listaBusquedaAvanzada==null)
			listaBusquedaAvanzada= new ArrayList();
	
		control.setListaBusquedaAvanzada(listaBusquedaAvanzada);
		request.getSession().setAttribute("listaBusquedaAvanzada", listaBusquedaAvanzada);
		//System.out.println("Tama�o de la Lista :D "+listaBusquedaAvanzada.size());
	} 
}
