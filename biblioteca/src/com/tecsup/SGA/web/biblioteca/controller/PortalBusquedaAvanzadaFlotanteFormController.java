package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Busqueda;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.BusquedaAvanzadaCommand;
import com.tecsup.SGA.web.biblioteca.command.PortalBusquedaAvanzadaFlotanteCommand;

public class PortalBusquedaAvanzadaFlotanteFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(PortalBusquedaAvanzadaFlotanteFormController.class);
	
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
		
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
				
		PortalBusquedaAvanzadaFlotanteCommand command= new PortalBusquedaAvanzadaFlotanteCommand();		
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
		
		if ( usuarioSeguridad != null )
    	{
			command.setCodUsuario(usuarioSeguridad.getIdUsuario());    		
    	}		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
		
		command.setCodTipoMaterial((String)request.getParameter("txhCodTipoMaterial"));
		
		command.setCodSeleccion1((String)request.getParameter("txhCodSeleccion1"));
		command.setTxtTexto1((String)request.getParameter("txhTxtTexto1"));
		command.setCodCondicion1((String)request.getParameter("txhCodCondicion1"));
		
		command.setCodSeleccion2((String)request.getParameter("txhCodSeleccion2"));
		command.setTxtTexto2((String)request.getParameter("txhTxtTexto2"));
		command.setCodCondicion2((String)request.getParameter("txhCodCondicion2"));
		
		command.setCodSeleccion3((String)request.getParameter("txhCodSeleccion3"));
		command.setTxtTexto3((String)request.getParameter("txhTxtTexto3"));
		
		command.setCodIdioma((String)request.getParameter("txhCodIdioma"));
		command.setCodFechaAnioInicio((String)request.getParameter("txhCodFechaAnioInicio"));
		command.setCodFechaAnioFin((String)request.getParameter("txhCodFechaAnioFin"));
		
		command.setOrdenarBy((String)request.getParameter("txhOrdenarBy"));
		command.setSelSala((String)request.getParameter("txhSelSala"));
		command.setSelDomicilio((String)request.getParameter("txhSelDomicilio"));
		command.setCodSede((String)request.getParameter("txhSelSede"));
		if(command.getCodTipoMaterial()!=null)
    	{
			request.setAttribute("strUrl",(String)request.getParameter("strUrl"));
			
			BusquedaAvanzada(command, request);
    	}
				
	    return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
	
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
          Object command,
          BindException errors)
	throws Exception {
	return super.processFormSubmission(request, response, command, errors);
			}
			
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
		BindException errors)
		throws Exception {
				
		PortalBusquedaAvanzadaFlotanteCommand control= (PortalBusquedaAvanzadaFlotanteCommand) command;
		log.info("OPERACION:" + control.getOperacion());
		if(control.getOperacion().equals("BUSCAR"))
		{ 
		  
		   BusquedaAvanzada(control, request);
		}	
		
		return new ModelAndView("/biblioteca/userWeb/bib_busqueda_avanzada_flotante","control",control);		
	}
	
	public void BusquedaAvanzada(PortalBusquedaAvanzadaFlotanteCommand control, HttpServletRequest request){
		Busqueda obj= new Busqueda();
		
		if(control.getSelSala()==null) 
			control.setSelSala("0");
		if(control.getSelDomicilio()==null) 
			control.setSelDomicilio("0");
		
		String sala=control.getSelSala();
		String domicilio=control.getSelDomicilio();
		
		if(control.getCodTipoMaterial()==null) 
			control.setCodTipoMaterial("");
		if(control.getCodSede()==null) 
			control.setCodSede("");		
		if(control.getCodIdioma()==null) 
			control.setCodIdioma("");
		if(control.getCodFechaAnioInicio()==null) 
			control.setCodFechaAnioInicio("");
		if(control.getCodFechaAnioFin()==null) 
			control.setCodFechaAnioFin("");
		if(control.getCodSeleccion1()==null) 
			control.setCodSeleccion1("0");
		if(control.getTxtTexto1()==null) 
			control.setTxtTexto1("");
		if(control.getCodCondicion1()==null) 
			control.setCodCondicion1("0");
		if(control.getCodSeleccion2()==null) 
			control.setCodSeleccion2("0");
		if(control.getTxtTexto2()==null) 
			control.setTxtTexto2("");
		if(control.getCodCondicion2()==null) 
			control.setCodCondicion2("0");
		if(control.getCodSeleccion3()==null) 
			control.setCodSeleccion3("0");
		if(control.getTxtTexto3()==null) 
			control.setTxtTexto3("");
		if(control.getOrdenarBy()==null) 
			control.setOrdenarBy("0");
		

		if(control.getCodTipoMaterial().equals("-1")) control.setCodTipoMaterial("");
		if(control.getCodSede().equals("-1")) control.setCodSede("");
		if(control.getCodIdioma().equals("-1")) control.setCodIdioma("");
		if(control.getCodFechaAnioInicio().equals("")) control.setCodFechaAnioInicio("");			
		if(control.getCodFechaAnioFin().equals("")) control.setCodFechaAnioFin("");
		
		if(control.getCodFechaAnioInicio().equals("-1")) control.setCodFechaAnioInicio("");
		if(control.getCodFechaAnioFin().equals("-1")) control.setCodFechaAnioFin("");
		
		if(control.getCodSeleccion1().equals("-1")) control.setCodSeleccion1("0");
		if(control.getTxtTexto1()==null) control.setTxtTexto1("");
		if(control.getCodCondicion1().equals("-1")) control.setCodCondicion1("0");
		if(control.getCodSeleccion2().equals("-1")) control.setCodSeleccion2("0");
		if(control.getTxtTexto2()==null) control.setTxtTexto2("");
		if(control.getCodCondicion2().equals("-1")) control.setCodCondicion2("0");
		if(control.getCodSeleccion3().equals("-1")) control.setCodSeleccion3("0");
		if(control.getTxtTexto3()==null) control.setTxtTexto3("");
		if(control.getOrdenarBy().equals("-1")) control.setOrdenarBy("0");
		
		obj.setTipoMaterial(control.getCodTipoMaterial());
		obj.setCodTipoIdioma(control.getCodIdioma());
		obj.setCodAnioInicio(control.getCodFechaAnioInicio());
		obj.setCodAnioFin(control.getCodFechaAnioFin());
		obj.setFlagDisponibilidadSala(control.getSelSala());
		obj.setFlagDisponibilidadDomicilio(control.getSelDomicilio());
		obj.setBuscar1(Integer.valueOf(control.getCodSeleccion1()));
		obj.setTxtTexto1(control.getTxtTexto1());
		obj.setCondicion1(Integer.valueOf(control.getCodCondicion1()));
		obj.setBuscar2(Integer.valueOf(control.getCodSeleccion2()));
		obj.setTxtTexto2(control.getTxtTexto2());
		obj.setCondicion2(Integer.valueOf(control.getCodCondicion2()));
		obj.setBuscar3(Integer.valueOf(control.getCodSeleccion3()));
		obj.setTxtTexto3(control.getTxtTexto3());
		obj.setOrderBy(Integer.valueOf(Integer.valueOf(control.getOrdenarBy())));
		obj.setCodSede(control.getCodSede());
		
		
		control.setSelDomicilio(domicilio);
		control.setSelSala(sala);
		
		List listaBusquedaAvanzada= this.portalWebBibliotecaManager.GetBusquedaAvanzada(obj);
		
		if(listaBusquedaAvanzada==null)
			listaBusquedaAvanzada= new ArrayList();
	
		control.setListaBusquedaAvanzada(listaBusquedaAvanzada);
		request.getSession().setAttribute("listaBusquedaAvanzada", listaBusquedaAvanzada);		
	} 
}
