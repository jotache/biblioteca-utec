package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminGestionMaterialHistorialPrestamosCommand;

public class AdminGestionUsuarioPrestamosFormController extends
		SimpleFormController {

	private static Log log = LogFactory.getLog(AdminGestionUsuarioPrestamosFormController.class);
	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
		    throws ServletException {
		AdminGestionMaterialHistorialPrestamosCommand control = new AdminGestionMaterialHistorialPrestamosCommand();
		if(control.getOperacion()==null){
						
			control.setTxtCodUsuario(request.getParameter("codUsuario"));
			control.setLstResultado(biblioGestionMaterialManager.getListaLibrosPrestadosXUsuario(control.getTxtCodUsuario()));
			request.setAttribute("LibrosPrestadosXAlumno", control.getLstResultado());
		}		
		return control;
	}
}
