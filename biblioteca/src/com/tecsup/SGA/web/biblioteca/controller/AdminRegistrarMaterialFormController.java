package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

//import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
//import com.tecsup.SGA.web.biblioteca.command.AdminGestionMaterialCommand;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialCommand;

public class AdminRegistrarMaterialFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialFormController.class);
	  
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)	
    throws ServletException {
    	//log.info("formBackingObject:INI");
    	
    	//System.out.println("AdminRegistrarMaterial:0");
    	
    	AdminRegistrarMaterialCommand control = new AdminRegistrarMaterialCommand();
		if(control.getOperacion()==null){
			//System.out.println("AdminRegistrarMaterial:1");
			control.setSedeSel(request.getParameter("txhSedeSel")); 
			control.setSedeUsuario(request.getParameter("txhSedeUsuario"));
			//System.out.println("AdminRegistrarMaterial:sede:" + control.getSedeSel());
			control = cargaParametrosDeBusqueda(control,request);			
			control.setUsuario(request.getParameter("txhCodUsuario"));
			control.setTxhCodUnico(request.getParameter("txhCodigoUnico"));
			control.setTxtCodigo(request.getParameter("txhCodigoGenerado"));
			control = ini(control);
			control = verificaInsertOrUpdate(control,request);
			control = getLstRegistroIngreso(control);
			control = getLstDescriptores(control);
			control = getLstDocumentosRelacionados(control);	
			
		}
		//System.out.println("AdminRegistrarMaterial:2");

		control.setLstAnio(Anios.getAniosTodos(1900));
		
        //log.info("formBackingObject:FIN");
        return control;
   }
	

	private AdminRegistrarMaterialCommand cargaParametrosDeBusqueda(
			AdminRegistrarMaterialCommand control, HttpServletRequest request) {
		
		control.setPrmTxtCodigo(request.getParameter("prmTxtCodigo"));
		control.setPrmTxtNroIngreso(request.getParameter("prmTxtNroIngreso"));
		control.setPrmCboTipoMaterial(request.getParameter("prmCboTipoMaterial"));
		control.setPrmCboBuscarPor(request.getParameter("prmCboBuscarPor"));
		control.setPrmTxtTitulo(request.getParameter("prmTxtTitulo"));
		control.setPrmCboIdioma(request.getParameter("prmCboIdioma"));
		control.setPrmCboAnioIni(request.getParameter("prmCboAnioIni"));
		control.setPrmCboAnioFin(request.getParameter("prmCboAnioFin"));
		control.setPrmTxtFechaReservaIni(request.getParameter("prmTxtFechaReservaIni"));
		control.setPrmTxtFechaReservaFin(request.getParameter("prmTxtFechaReservaFin"));
		
		return control;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	String resp ="";
    	AdminRegistrarMaterialCommand control = (AdminRegistrarMaterialCommand) command;
    	log.info("OPERACION:"+control.getOperacion());
    	    	
    	if("irCambiaPais".equalsIgnoreCase(control.getOperacion())){    		
    		control = cambiaPais(control);
    		control.setCboSede(control.getSedeSel());
    	}else if("verificaCategoria".equalsIgnoreCase(control.getOperacion())){
    		//System.out.println("Verifica Categoria");
    		//System.out.println("TxhCodUnico: " + control.getTxhCodUnico());
    		System.out.println("Ciudad: " + control.getCboCiudad());
    		control = cambiaPais(control);
    		control.setCboCiudad(control.getCboCiudad());
    		boolean catDupli = this.biblioGestionMaterialManager.categoriaDuplicada(control.getTxhCodUnico(), control.getTxtIdDewey()); 
    		if (catDupli==true){
    			request.setAttribute("mensaje",CommonMessage.CATEGORIA_DUPLICADO);
    			return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material","control",control);
    		}
    	}else if ("irActualizaImg".equalsIgnoreCase(control.getOperacion())){    		
    		//control.setPathImagen("\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_MATERIAL_BIB_IMAGENES+control.getNomImagen());
    		//control = getAllMaterialDetalle(control);
    		control.setPathImagen(CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_IMAGENES+control.getNomImagen());
    		//control = iniciaSede(control);
    		control = cambiaPais(control);    	
    	}else if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
    		    	
    		control.setCboSede(control.getSedeSel());
    		//Si es la primera vez (No ha aceptado grabar), validar ISBN por sede...    		
    		if (control.getIndISBN().equals("") && (!control.getTxtIsbn().equals("")) ){
    			String ciudad = control.getCboCiudad();
        		if (this.biblioGestionMaterialManager.getCantISBNPorSede(control.getTxtIsbn(), control.getCboSede(),null)>0){
        			request.setAttribute("misbn", "El ISBN ya existe para esta Sede");
        			cambiaPais(control);
        			control.setCboCiudad(ciudad);
        			//request.setAttribute("mensaje","ISBN del material duplicado para la sede: " + control.getCboSede());
        			return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material","control",control);
        		}    			
    		}
    		
    		//System.out.println("control.getCboSede():" + control.getCboSede()); 
    		resp = irRegistrar(control);    		
    		
    		control.setTxhCodUnico(null);
    		
    		/*if("0".equalsIgnoreCase(resp)){
    			control.setTxhCodUnico(resp);
        		control = getAllMaterialDetalle(control);
    			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);    			
    		}else*/ 
    		if("-1".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    		}else if("-2".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.TITULO_DUPLICADO);
    		}else if("-3".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.ISBN_DUPLICADO);
    		}else if("-4".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.CATEGORIA_DUPLICADO);
    		}else{
    			control.setTxhCodUnico(resp);
        		control = getAllMaterialDetalle(control);
    			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
    		}
    		
    	}else if("irActualizar".equalsIgnoreCase(control.getOperacion())){
    		control.setCboSede(control.getSedeSel());
    		//Si es la primera vez (No ha aceptado grabar), validar ISBN por sede...    		
    		if (control.getIndISBN().equals("") && (!control.getTxtIsbn().equals("")) ){
    			String ciudad = control.getCboCiudad();
        		if (this.biblioGestionMaterialManager.getCantISBNPorSede(control.getTxtIsbn(), control.getCboSede(),Long.valueOf(control.getTxhCodUnico()))>0){
        			request.setAttribute("misbn", "El ISBN ya existe para esta Sede");
        			cambiaPais(control);
        			control.setCboCiudad(ciudad);
        			//request.setAttribute("mensaje","ISBN del material duplicado para la sede: " + control.getCboSede());
        			return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material","control",control);
        		}    			
    		}
    		
    		resp = irActualizar(control);
    		control = cambiaPais(control);
    		//log.info("RPUESTA:"+resp+":");
    		if("-1".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    		}else if("-2".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.TITULO_DUPLICADO);
    		}else if("-3".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.ISBN_DUPLICADO);
    		}else if("-4".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.CATEGORIA_DUPLICADO);	
    		}else {
    			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
    			control = getAllMaterialDetalle(control);
    		}
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material","control",control);		
    }
	
    private String irActualizar(AdminRegistrarMaterialCommand control) {
    	String str = "";
    	
    	str = biblioGestionMaterialManager.
    	updateMaterial(
    			control.getTxhCodUnico(),
    			control.getCboTipoMaterial(),
    			control.getTxtTitulo(),
    			control.getTxhCodigoAutor(),
    			control.getTxhCodigoDewey(),
    			control.getTxtNroPag(),
    			control.getCboEditorial(),
    			control.getTxtNroEdicion(),
    			control.getCboPais(),
    			control.getCboCiudad(),
    			control.getTxtFecPublicacion(),
    			control.getTxtUrlArchivoDig(),
    			control.getCboIdioma(),
    			control.getTxtIsbn(),
    			control.getTxtFormato(),
    			control.getCboTipoVideo(),
    			control.getChkTipoPrestamoSala(),
    			control.getChkTipoPrestamoDomicilio(),
    			control.getRdoAplicaReserva(),    			
    			control.getNomImagen(),
    			control.getUsuario(),
    			control.getTxtUbicacionFisica(),
    			control.getCboSede(),
    			control.getTxtIdDewey(),
    			control.getTxtPrefijo(),
    			control.getTxtSufijo(),
    			control.getTxtFrecuencia(),
    			control.getMencionSerie(),
    			control.getTxtContenido()
    			);
		return str;
	}

	private String irRegistrar(
			AdminRegistrarMaterialCommand control){
    	String str = "";

    	str = biblioGestionMaterialManager.
    	insertMaterial(
    			control.getCboTipoMaterial(),
    			control.getTxtTitulo(),
    			control.getTxhCodigoAutor(),
    			control.getTxhCodigoDewey(),
    			control.getTxtNroPag(),
    			control.getCboEditorial(),
    			control.getTxtNroEdicion(),
    			control.getCboPais(),
    			control.getCboCiudad(),
    			control.getTxtFecPublicacion(),
    			control.getTxtUrlArchivoDig(),
    			control.getCboIdioma(),
    			control.getTxtIsbn(),
    			control.getTxtFormato(),
    			control.getCboTipoVideo(),
    			control.getCboProcedencia(),
    			control.getCboPrecio(),
    			control.getTxtPrecio(),
    			control.getChkTipoPrestamoSala(),
    			control.getChkTipoPrestamoDomicilio(),
    			control.getRdoAplicaReserva(),
    			control.getTxtObservaciones(),
    			control.getTxhImagen(),
    			control.getUsuario(),
    			control.getTxtUbicacionFisica(),
    			control.getCboSede(),
    			control.getTxtIdDewey(),
    			control.getTxtPrefijo(),
    			control.getTxtSufijo(),
    			control.getTxtFrecuencia(),
    			control.getMencionSerie(),
    			control.getTxtContenido()
    			);
		return str;
	}

	private AdminRegistrarMaterialCommand getLstDocumentosRelacionados(
			AdminRegistrarMaterialCommand control) {
		control.setLstDocumentosRelacionados(biblioGestionMaterialManager.getAllArchAdjuntoxMaterial(control.getTxhCodUnico()));
		return control;
	}

	private AdminRegistrarMaterialCommand getLstDescriptores(
			AdminRegistrarMaterialCommand control) {
		control.setLstDescriptores(biblioGestionMaterialManager.getAllDescriptorxMaterial(control.getTxhCodUnico()));
		return control;
	}

	private AdminRegistrarMaterialCommand getLstRegistroIngreso(
			AdminRegistrarMaterialCommand control) {
		//SE LE ENVIA COMO CODIGOMATERIAL EL CODIGO UNICO
		control.setLstRegistroIngreso(biblioGestionMaterialManager.getAllIngresosxMaterial(control.getTxhCodUnico(),null));
		return control;
	}

	private AdminRegistrarMaterialCommand verificaInsertOrUpdate(
			AdminRegistrarMaterialCommand control,HttpServletRequest request) {
    	
		if(request.getParameter("parModifica")!=null)
			control.setAccion("irActualizar");
		else
			control.setAccion("irRegistrar");
		
		if(control.getAccion().equalsIgnoreCase("irActualizar"))
			control = getAllMaterialDetalle(control);
		
		return control;
	}

	private AdminRegistrarMaterialCommand getAllMaterialDetalle(
			AdminRegistrarMaterialCommand control) {
		//log.info("getDatosMaterial:INI");

		control = biblioGestionMaterialManager.getAllMaterialDetalle(control);
		control = cambiaPais(control);
		
		
		//log.info("getDatosMaterial:FIN");
		return control;
	}

	
	private AdminRegistrarMaterialCommand ini(AdminRegistrarMaterialCommand control){
		//log.info("ini:INI");
		
		control = iniciaMaterial(control);
		control = iniciaEditorial(control);
		control = iniciaPais(control);
		control = iniciaIdioma(control);
		control = iniciaVideos(control);
		control = iniciaProcedencia(control);
		control = iniciaMoneda(control);
		control = iniciaSede(control);
		
		//log.info("ini:FIN");
		return control;
	}
	private AdminRegistrarMaterialCommand iniciaSede(
			AdminRegistrarMaterialCommand control) {
		TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SEDE);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			
		control.setLstSede(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		//log.warn("iniciaSede:" + control.getSedeSel());
		if (control.getSedeSel()!=null)
			control.setCboSede(control.getSedeSel());			
		
		return control;
	}

	private AdminRegistrarMaterialCommand iniciaMoneda(
			AdminRegistrarMaterialCommand control) {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MONEDA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstPrecio(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		return control;
	}

	private AdminRegistrarMaterialCommand iniciaProcedencia(
			AdminRegistrarMaterialCommand control) {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_PROCEDENCIA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstProcedencia(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		return control;
	}

	private AdminRegistrarMaterialCommand iniciaVideos(
			AdminRegistrarMaterialCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_VIDEO);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstTipoVideo(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		return control;
	}

	private AdminRegistrarMaterialCommand iniciaIdioma(
			AdminRegistrarMaterialCommand control) {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	obj.setDscValor3("");    	
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_IDIOMAS_BIBLIOTECA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstIdioma(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		return control;
	}

	private AdminRegistrarMaterialCommand iniciaPais(
			AdminRegistrarMaterialCommand control) {    	
    	Pais pais= new Pais();		
		pais.setPaisCodigo("");
		pais.setPaisDescripcion("");		
		control.setLstPais(this.biblioMantenimientoManager.getAllPaises(pais,CommonConstants.TIPO_ORDEN_DSC));

		return control;
	}

	private AdminRegistrarMaterialCommand iniciaEditorial(
			AdminRegistrarMaterialCommand control) {
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_EDITORIAL);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstEditorial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));		
		return control;
	}

	private AdminRegistrarMaterialCommand iniciaMaterial(
			AdminRegistrarMaterialCommand control) {
	    TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		return control;
	}

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    


	private AdminRegistrarMaterialCommand cambiaPais(
			AdminRegistrarMaterialCommand control) {
		
    		Ciudad obj = new Ciudad();    		
    		obj.setPaisId(control.getCboPais());
    		obj.setCiudadCodigo("");
    		obj.setCiudadDescripcion("");    		
    		control.setLstCiudad(this.biblioMantenimientoManager.getCiudadByPais(obj, CommonConstants.TIPO_ORDEN_DSC));
    	
    		return control;
	}
}
