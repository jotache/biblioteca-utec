package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.web.evaluaciones.controller.RelacionarProductoHibridoFormController;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;


public class MtoAgregarFeriadoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoAgregarFeriadoFormController.class);
	BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		MtoAgregarFeriadoCommand command = new MtoAgregarFeriadoCommand();
		command.setCodUsuario((String)request.getParameter("txhcodEva"));
		command.setCod((String)request.getParameter("txhCod"));
		command.setAno((String)request.getParameter("txhAnio"));
		command.setDescripcion((String)request.getParameter("txhDes"));
		command.setDia((String)request.getParameter("txhDia"));
		command.setMes((String)request.getParameter("txhmess"));
		command.setFlag((String)request.getParameter("txhFlag"));
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());    	
    	request.setAttribute("lstrURL", command.getLstrURL());
    	Anios anios= new Anios();
    	command.setCodListaMeses(anios.getMeses());
    	command.setCodListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
    	command.setSede((String)request.getParameter("txhSede"));
    	
    	if (command.getCod() != null){
    		getFecha(command);
    	}
    	
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	MtoAgregarFeriadoCommand control = (MtoAgregarFeriadoCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	

    	if (control.getOperacion().trim().equals("GRABAR"))
    	{	
    		String [] v = control.getTxtFecha().split("/");
    		String dia = v[0];String mes =v[1];String anio =v[2];
    		
    		control.setDia(dia);
    		control.setMes(mes);
    		control.setAno(anio);
    		
    		if(control.getFlag().trim().equals("0")){
    			resultado = modifica(control, dia, mes, anio);
    		}
	    	else {
	    		resultado = graba(control, dia, mes, anio);
	    	}
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else control.setMsg("OK");
    	}
    	
    	return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_mto_agregar_feriado","control",control);		
    }
    
    private String graba(MtoAgregarFeriadoCommand control, String dia, String mes, String anio){
    	control.setGraba(this.biblioMantenimientoManager.InsertFeriados(
    			(control.getSede().equals("U")?CommonConstants.TIPT_FERIADO_UTEC:CommonConstants.TIPT_FERIADO)
    			, dia, mes, anio, control.getDescripcion(), control.getCodUsuario()));
    	return control.getGraba();  
    }
    
    private String modifica(MtoAgregarFeriadoCommand control, String dia, String mes, String anio){
    	control.setGraba(this.biblioMantenimientoManager.ActualizarFeriado(
    			(control.getSede().equals("U")?CommonConstants.TIPT_FERIADO_UTEC:CommonConstants.TIPT_FERIADO)
    			, control.getCod(), dia, mes, anio 
    			, control.getDescripcion(), control.getCodUsuario(), control.getFlag()));
    	return control.getGraba();
    }
    
    private void getFecha(MtoAgregarFeriadoCommand control){    	
    	String dia = "";
    	String mes = "";
    	if (control.getMes().trim().length() == 1)	mes = "0" + control.getMes().trim();
    	else mes = control.getMes().trim();
    	
    	if (control.getDia().trim().length() == 1)	dia = "0" + control.getDia().trim(); 
    	else dia = control.getDia().trim();
    	
		String fecha = dia + "/" + mes + "/" + control.getAno().trim();
		
    	control.setTxtFecha(fecha);
    }
    
}
