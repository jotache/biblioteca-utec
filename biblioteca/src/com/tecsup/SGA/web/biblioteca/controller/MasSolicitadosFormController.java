package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.MasSolicitadosCommand;

public class MasSolicitadosFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(MasSolicitadosFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    	throws ServletException {
		MasSolicitadosCommand command = new MasSolicitadosCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
		
    	LlenarDatos(command, request);
    	LlenarComboTipoMaterial(command, request);
    	
        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	MasSolicitadosCommand control = (MasSolicitadosCommand) command;
    	  	
        return new ModelAndView("/biblioteca/userWeb/bib_los_mas_solicitados","control",control);		
    }
    
    public void LlenarDatos(MasSolicitadosCommand control, HttpServletRequest request){
    	
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_EDITORIAL);
		obj.setCodDetalle("");//control.getCodigoEditorial());
		obj.setDescripcion("");//control.getDescripcionEditorial());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setListaTipoMateriales(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		request.getSession().setAttribute("listaTipoMaterial", control.getListaTipoMateriales());
    	
    }
    
    public void LlenarComboTipoMaterial(MasSolicitadosCommand control, HttpServletRequest request){
    	
    	 TipoTablaDetalle obj= new TipoTablaDetalle();
 	        obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setCodListaTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
			request.getSession().setAttribute("listaMateriales", control.getCodListaTipoMaterial());
    	
    	
    }
    
}
