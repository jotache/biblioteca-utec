package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialAgregarDescriptoresCommand;


public class AdminRegistrarMaterialAgregarDescriptoresFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AdminRegistrarMaterialAgregarDescriptoresFormController.class);
	
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
    public AdminRegistrarMaterialAgregarDescriptoresFormController() {
        super();        
        setCommandClass(AdminRegistrarMaterialAgregarDescriptoresCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)	
    throws ServletException{
    	//log.info("formBackingObject:INI"); 
    	AdminRegistrarMaterialAgregarDescriptoresCommand control = new AdminRegistrarMaterialAgregarDescriptoresCommand();
		if(control.getOperacion()==null){
			//CODIGO DEL MATRIAL DE LA VENTANA PADRE
			control.setTxhCodMaterial(request.getParameter("txhCodUnico"));
			control.setUsuario(request.getParameter("txhCodUsuario"));
		}
        //log.info("formBackingObject:FIN");
        return control;
   }
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	String resp ="";
    	AdminRegistrarMaterialAgregarDescriptoresCommand control = (AdminRegistrarMaterialAgregarDescriptoresCommand) command;

    	
    	log.info("OPERACION: "+control.getOperacion());
    	
    	if("irConsultar".equalsIgnoreCase(control.getOperacion())){
    		control = irConsultar(control);    		
    	}else if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
    		resp = irRegistrar(control);
    		log.info("RPTA:"+resp+":");
    		if("0".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
    			request.setAttribute("refresh","1");
    		}else if("-2".equalsIgnoreCase(resp)){
    			request.setAttribute("mensaje",CommonMessage.ALGUNOS_YA_REGISTRADOS);
    			request.setAttribute("refresh","1");
    		}else{
    			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    		}    		
    		control = irConsultar(control);
    	}
    	
    	//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_material_agregar_descriptores","control",control);		
    }

	private String irRegistrar(
			AdminRegistrarMaterialAgregarDescriptoresCommand control) {
			/*log.info("irRegistrar:INI");
			log.info("PARAMETROS PARA EL STORED insertDescriptorxMaterial");
			log.info("TOKEN A REGISTRAR<"+control.getTxhCodMaterial()+">");
			log.info("TOKEN A REGISTRAR<"+control.getTxhToken()+">");
			log.info("TOKEN A REGISTRAR<"+control.getTxhTamanioSeleccionado()+">");
			log.info("TOKEN A REGISTRAR<"+control.getUsuario()+">");*/
			//String codMaterial,String cadena, String nroRegistros, String usuario
			String resp = biblioGestionMaterialManager.
			insertDescriptorxMaterial(
					control.getTxhCodMaterial(),
					control.getTxhToken(),
					control.getTxhTamanioSeleccionado(),
					control.getUsuario()
					);
			control.setTxhTamanioSeleccionado("0");
		//log.info("irRegistrar:FIN");
		return resp;
	}

	private AdminRegistrarMaterialAgregarDescriptoresCommand irConsultar(
			AdminRegistrarMaterialAgregarDescriptoresCommand control) {
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_DESCRIPTOR);
		obj.setCodDetalle("");//control.getCodigoDescriptor()
		obj.setDescripcion(control.getTxtDescriptores().trim());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setLstResultado(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));		
		control.setTxhtamanio(control.getLstResultado()==null?"0":""+control.getLstResultado().size());
		
		return control;
	}

	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

}
