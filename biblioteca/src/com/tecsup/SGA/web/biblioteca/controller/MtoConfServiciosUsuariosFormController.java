package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.ServiciosUsuarios;

public class MtoConfServiciosUsuariosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoConfServiciosUsuariosFormController.class);
	BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		MtoConfServiciosUsuariosCommand command = new MtoConfServiciosUsuariosCommand();		
		TipoTablaDetalle obj = new TipoTablaDetalle();
		command.setCodEval((String)request.getParameter("txhCodUsuario"));
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));  
    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());    	
    	request.setAttribute("lstrURL", command.getLstrURL());
    	obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_USUARIO_BIBLIOTECA);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	command.setTipoAlumnos(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    	request.getSession().setAttribute("tipoAlumnos",command.getTipoAlumnos());
    	request.setAttribute("apliSancion","0");
    	request.setAttribute("resaReser","0");
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		MtoConfServiciosUsuariosCommand control = (MtoConfServiciosUsuariosCommand)command;    	    	
		String resultado = "";
		log.info("OPERACION:" + control.getOperacion());
		
		if (control.getOperacion().trim().equals("BUSCAR"))
    	{
			buscar(control);			
			request.setAttribute("apliSancion",control.getApSanciones());
	    	request.setAttribute("resaReser",control.getReaRe());
	    	request.setAttribute("presSalas",control.getPresSala());
	    	request.setAttribute("presDomicilio",control.getPresDomi());
	    	
	    	List<TipoTablaDetalle> lista = control.getListaMaxDiasPrestamo();
	    	request.setAttribute("listaDiasPrestamo", lista);
	    	control.setDatosTamano(String.valueOf(lista.size()));
	    	
    	}
		if (control.getOperacion().trim().equals("GRABAR"))
    	{
			resultado = graba(control);
			log.info("RPTA:" + resultado);
			buscar(control);
			request.setAttribute("apliSancion",control.getApSanciones());
	    	request.setAttribute("resaReser",control.getReaRe());
	    	request.setAttribute("presSalas",control.getPresSala());
	    	request.setAttribute("presDomicilio",control.getPresDomi());
	    		    		    
			if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else {
    			
    			List<TipoTablaDetalle> lista = control.getListaMaxDiasPrestamo();
    	    	request.setAttribute("listaDiasPrestamo", lista);
    	    	control.setDatosTamano(String.valueOf(lista.size()));
    			
    			control.setMsg("OK");
    		}
    	}
		
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_mto_conf_servicios_usuarios","control",control);		
    }
    
    private String buscar(MtoConfServiciosUsuariosCommand control){
    
    	ServiciosUsuarios obj = this.biblioMantenimientoManager.GetAllServicioUsuario(CommonConstants.TIPT_SERVICIO_USUARIO, control.getCodAlumno());
    	
    	if(obj==null){
    		
    		control.setCodId("");
        	control.setPresSala("0");
        	control.setPresDomi("0");
        	control.setPrestPen("");
        	control.setDiasPrest("");
        	control.setNumDias("");
        	control.setApSanciones("0");
        	control.setReaRe("0");
        	control.setNroProrrogas("");
        	control.setListaMaxDiasPrestamo(null);
       
    	}else{
    	
	    	control.setCodId(obj.getCodSecuencial());
	    	control.setPresSala(obj.getPrestamoSala());
	    	control.setPresDomi(obj.getPrestamoDomicilio());
	    	control.setPrestPen(obj.getNroPrestamoSala());
	    	control.setDiasPrest(obj.getNroPrestamoInternet());
	    	control.setApSanciones(obj.getSancion());
	    	control.setReaRe(obj.getReserva());
	    	control.setNroProrrogas(obj.getNroProrrogas());
	    	
	    	List<TipoTablaDetalle> lista = biblioMantenimientoManager.listaMaximoDiasPrestamos(control.getCodAlumno());
	    	log.info("lista.size():"+lista.size());
	    	control.setListaMaxDiasPrestamo(lista);
    	       
    	}
    
    	return control.getCodId() + control.getPresSala() + control.getPresDomi() + control.getPrestPen() +
    	control.getDiasPrest() + control.getApSanciones() + control.getReaRe();
    }
    
    private String graba(MtoConfServiciosUsuariosCommand control){
    	
    	if(control.getPresSala().equals("")){
    		control.setPresSala("0");
    	}
    	if(control.getPresDomi().equals("")){
    		control.setPresDomi("0");
    	}
    	control.setCodId(this.biblioMantenimientoManager.InsertServicioUsuario(CommonConstants.TIPT_SERVICIO_USUARIO
    			, control.getCodAlumno(), control.getPresSala(), control.getPresDomi(), control.getPrestPen(), control.getDiasPrest()
    			, control.getApSanciones(), control.getReaRe(),"","",control.getNroProrrogas(), control.getCodEval(),control.getDatosCadena(), control.getDatosTamano())
    			);
    	return control.getCodId();
    }
    
}
