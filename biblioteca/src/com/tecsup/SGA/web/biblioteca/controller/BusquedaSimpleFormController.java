package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Busqueda;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.BusquedaSimpleCommand;

public class BusquedaSimpleFormController extends SimpleFormController {

	private static Log log = LogFactory
			.getLog(BusquedaSimpleFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	String CodTipoMaterial = "";
	String TxtTexto = "";
	String CodBusquedaBy = "";
	String CodOrdenarBy = "";
	String SelSala = "";
	String SelDomicilio = "";
	String SelSede="";
	String strUrl = "";

	public PortalWebBibliotecaManager getPortalWebBibliotecaManager() {
		return portalWebBibliotecaManager;
	}

	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}

	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		//log.info("formBackingObject:INI");
		BusquedaSimpleCommand command = new BusquedaSimpleCommand();
		// se establece el cod del usuario
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad) request
				.getSession().getAttribute("usuarioSeguridad");
		if (usuarioSeguridad != null) {
			command.setCodEvaluador(usuarioSeguridad.getIdUsuario());
		}
		// *******************************
		CodTipoMaterial = (String) request.getParameter("txhCodTipoMaterial");
		TxtTexto = (String) request.getParameter("txhTexto");
		CodBusquedaBy = (String) request.getParameter("txhCodBuscarBy");
		CodOrdenarBy = (String) request.getParameter("txhCodOrdenarBy");
		SelSala = (String) request.getParameter("txhSelSala");
		SelDomicilio = (String) request.getParameter("txhSelDomicilio");
		SelSede = (String) request.getParameter("txhSelSede");
		strUrl = (String) request.getParameter("strUrl");
		request.setAttribute("strUrl", (String) request.getParameter("strUrl"));
		// se asigna los valores a las variables para la busqueda
		if (CodTipoMaterial != null) {
			command.setCodSede(SelSede);
			command.setCodTipoMaterial(CodTipoMaterial);
			command.setTxtTexto(TxtTexto);
			command.setCodBusquedaBy(CodBusquedaBy);
			command.setCodOrdenarBy(CodOrdenarBy);
			if (SelSala.equals(""))
				SelSala = "0";
			command.setSelSala(SelSala);
			if (SelDomicilio.equals(""))
				SelDomicilio = "0";
			command.setSelDomicilio(SelDomicilio);
			// buscar por filtro			
			BusquedaSimple(command, request);
		}
		// ********************************************************
		command.setConsteSala(CommonConstants.TIPT_FLAG_SALA);
		command.setConsteDomicilio(CommonConstants.TIPT_FLAG_DOMICILIO);
		
		LlenarComboTipoMaterial(command, request);
		LlenarOrdenarAndBuscarBy(command);

		//log.info("formBackingObject:FIN");
		return command;
	}
	
	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		
		BusquedaSimpleCommand control = (BusquedaSimpleCommand) command;
		log.info("OPERACION:"+control.getOperacion());
		if (control.getOperacion().equals("BUSCAR")) {			
			BusquedaSimple(control, request);
		}
		//log.info("onSubmit:FIN");
		return new ModelAndView("/biblioteca/userWeb/bib_busqueda_simple","control", control);
	}

	public void LlenarComboTipoMaterial(BusquedaSimpleCommand control,
			HttpServletRequest request) {

		TipoTablaDetalle obj = new TipoTablaDetalle();
		obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setCodListaTipoMaterial(this.biblioMantenimientoManager
				.getAllTablaDetalleBiblioteca(obj));
		request.getSession().setAttribute("listaMateriales",
				control.getCodListaTipoMaterial());

	}

	public void LlenarDatos(BusquedaSimpleCommand control,
			HttpServletRequest request) {

		TipoTablaDetalle obj = new TipoTablaDetalle();
		obj.setDscValor3("");

		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_EDITORIAL);
		obj.setCodDetalle("");// control.getCodigoEditorial());
		obj.setDescripcion("");// control.getDescripcionEditorial());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setListaTipoMaterial(this.biblioMantenimientoManager
				.getAllTablaDetalleBiblioteca(obj));
		request.getSession().setAttribute("listaTipoMaterial",
				control.getListaTipoMaterial());

	}

	public void LlenarOrdenarAndBuscarBy(BusquedaSimpleCommand control) {
		TipoTablaDetalle obj = new TipoTablaDetalle();
		obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_BUSQUEDA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setCodListaOrdenarBy(this.biblioMantenimientoManager
				.getAllTablaDetalleBiblioteca(obj));
		control.setCodListaBusquedaBy(control.getCodListaOrdenarBy());
	}

	public void BusquedaSimple(BusquedaSimpleCommand control,
			HttpServletRequest request) {
		Busqueda obj = new Busqueda();
		
		if (control.getCodTipoMaterial() == null)
			control.setCodTipoMaterial("");
		if (control.getCodBusquedaBy() == null)
			control.setCodBusquedaBy("");
		if (control.getCodOrdenarBy() == null)
			control.setCodOrdenarBy("");
		if (control.getSelSala() == null)
			control.setSelSala("");
		if (control.getSelDomicilio() == null)
			control.setSelDomicilio("");
		if (control.getCodTipoMaterial().equals("-1"))
			control.setCodTipoMaterial("");
		if (control.getCodSede().equals("-1"))
			control.setCodSede("");
		
		obj.setTipoMaterial(control.getCodTipoMaterial());
		obj.setCodSede(control.getCodSede());
		if (control.getTxtTexto() == null)
			control.setTxtTexto("");
		else
			obj.setTexto(control.getTxtTexto());
		if (control.getCodBusquedaBy().equals("-1"))
			control.setCodBusquedaBy("");
		
		obj.setBuscarBy(control.getCodBusquedaBy());
		
		if (control.getCodOrdenarBy().equals("-1"))
			control.setCodOrdenarBy("");
		
		obj.setOrdenarBy(control.getCodOrdenarBy());
		
		if (control.getSelSala().equals("0"))
			obj.setPrestaSala("0");
		else
			obj.setPrestaSala(control.getSelSala());
		if (control.getSelDomicilio().equals("0"))
			obj.setPrestaDomicilio("0");
		else
			obj.setPrestaDomicilio(control.getSelDomicilio());
		
		List listaBusqueda = this.portalWebBibliotecaManager.GetBusquedaSimple(obj);
		
		Busqueda obj1 = new Busqueda();
		if (listaBusqueda != null) {
			if (listaBusqueda.size() > 0) {
				for (int a = 0; a < listaBusqueda.size(); a++) {
					obj1 = (Busqueda) listaBusqueda.get(a);					
				}
			}
			request.getSession().setAttribute("listaBusquedaSimple",listaBusqueda);
		}
		else {
			listaBusqueda = new ArrayList();
		}		
	}	
}
