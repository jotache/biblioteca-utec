package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.TipoMaterialAgregarCommand;


public class TipoMaterialAgregarFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(TipoMaterialAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	TipoMaterialAgregarCommand command = new TipoMaterialAgregarCommand();
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	
    	String codDetalle = (String)request.getParameter("txhCodigo");
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	TipoMaterialAgregarCommand control = (TipoMaterialAgregarCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		resultado = InsertTablaDetalleBiblioteca(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    		      else
    		      control.setMsg("OK");
    		    
    		     }
    	}
    	control.setOperacion("");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_tipo_material_agregar","control",control);		
    }
    
    private String InsertTablaDetalleBiblioteca(TipoMaterialAgregarCommand control)
    {     
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		    		
    		obj.setCodTipoTabla("");
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
    		obj.setDescripcion(control.getDescripcion().trim());
    		obj.setUsuario("1");//control.getCodEvaluador());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");            
    		obj.setCodTipoTablaDetalle(this.biblioMantenimientoManager.InsertTablaDetalleBiblioteca(obj));

    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
 }
