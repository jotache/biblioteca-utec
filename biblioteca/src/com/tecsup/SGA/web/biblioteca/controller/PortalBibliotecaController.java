package com.tecsup.SGA.web.biblioteca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.seguridad.SeguridadManager;


public class PortalBibliotecaController  implements Controller  {
	
	private static Log log = LogFactory.getLog(PortalBibliotecaController.class);

	private SeguridadManager seguridadManager;	
	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    		
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){    	
    		request.setAttribute("txhCodUsuario", usuarioSeguridad.getCodUsuario());
    		request.setAttribute("nombre",usuarioSeguridad.getCodUsuario()+" - "+usuarioSeguridad.getNomUsuario() +" " + usuarioSeguridad.getApeUsuario() );
    	}  
    	
    	ModelMap model = new ModelMap();

    	String usuario = (String)request.getParameter("txhCodUsuario");    	
    	String operacion = (String)request.getParameter("txhOperacion");    	
    	    
    	String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
    	log.info("OPERACION:" + operacion);
    	
    	if ("CERRAR_SESION".equals(operacion)){    	
    		usuario = usuarioSeguridad.getCodUsuario();
    		request.getSession().removeAttribute("usuarioSeguridad");
    		seguridadManager.RegistrarSesion(false,estacioncliente, usuario.trim(),CommonConstants.SGA_BIB);    		    	
    		response.sendRedirect("/biblioteca/biblioteca/index.html");
    	}
    	    	    
    	return new ModelAndView("/biblioteca/portal", "model", model);
	}
	
	
}

