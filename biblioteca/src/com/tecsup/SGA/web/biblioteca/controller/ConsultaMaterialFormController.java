package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.web.evaluaciones.controller.MtoModificarTipoConceptoFormController;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;

public class ConsultaMaterialFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConsultaMaterialFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		ConsultaMaterialCommand command = new ConsultaMaterialCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	LlenarDatos(command, request);
    	//request.getSession().setAttribute("listaMateriales", command.getListaMateriales());
    	//request.getSession().setAttribute("listaAreasIntUno", command.getListAreasNivelUno());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
    	ConsultaMaterialCommand control = (ConsultaMaterialCommand) command;
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("ELIMINAR_MATERIAL")) {

    		resultado = DeleteMaterial(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    		      else
    			  {control.setMsg("OK");
    			  LlenarDatos(control, request);
    			   }
    		
    		}
    	}
    	control.setOperacion("");
    	//log.info("onSubmit:FIN");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_material","control",control);		
    }
    
    public String DeleteMaterial(ConsultaMaterialCommand control){
    	try
    	{   String resultado="";
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla("");
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
    		obj.setDscValor1(control.getCodSelec());
    		obj.setDscValor2("");
    		obj.setDescripcion(control.getDescripSelec());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodEvaluador());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		 	
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    			
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    } 
     public void LlenarDatos(ConsultaMaterialCommand control, HttpServletRequest request){
    	 
    	    TipoTablaDetalle obj= new TipoTablaDetalle();
    	    obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setListaMateriales(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
			request.getSession().setAttribute("listaMateriales", control.getListaMateriales());
     }
    

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
    
}
