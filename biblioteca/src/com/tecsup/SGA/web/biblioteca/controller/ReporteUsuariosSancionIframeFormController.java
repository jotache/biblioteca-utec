package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.service.biblioteca.BiblioReportesManager;

public class ReporteUsuariosSancionIframeFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ReporteUsuariosSancionIframeFormController.class);
	
	private BiblioReportesManager biblioReportesManager;
	
	/*SETTER*/
	public void setBiblioReportesManager(BiblioReportesManager biblioReportesManager) {
		this.biblioReportesManager = biblioReportesManager;
	}
	
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		ReporteUsuariosSancionIframeCommand command = new ReporteUsuariosSancionIframeCommand();
				
		List consulta;
    	String pagina = "";    	    	
    	String periodo1 = request.getParameter("periodo1");
    	String periodo2 = request.getParameter("periodo2");
    	String tipoUsuario = request.getParameter("tipousuario");
    	String sede = request.getParameter("sede");
    	String subOpcion = request.getParameter("subOpcion");
    	    	    	
		consulta = biblioReportesManager.getReporteUsuariosSancion(periodo1,periodo2, subOpcion,sede,tipoUsuario);
    	request.getSession().setAttribute("consulta", consulta);

        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	ReporteUsuariosSancionIframeCommand control = (ReporteUsuariosSancionIframeCommand) command;
    	
	    return new ModelAndView("/biblioteca/userAdmin/consultaReportes/bib_reporte_usuario_sancion_iframe","control",control);
    }
}
