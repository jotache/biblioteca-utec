package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaDescriptoresFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(ConsultaDescriptoresFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	} 

	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		ConsultaDescriptoresCommand command = new ConsultaDescriptoresCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());
        LlenarDatos(command, request);
    	//log.info("formBackingObject");
    	request.setAttribute("lstrURL", command.getLstrURL());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
    	ConsultaDescriptoresCommand control = (ConsultaDescriptoresCommand) command;
    	String resultado="";
    	log.info("OPERACION:"+control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{   LlenarDatos(control, request);
    		    		
    	}
    	else{ if (control.getOperacion().trim().equals("ELIMINAR"))    		
    	      {    		
    		resultado = DeleteDescriptor(control);
    		log.info("RPTA:"+resultado);
	    		if ( resultado.equals("-1")) control.setMsg("ERROR");
	    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
	    		      else
	    			  {control.setMsg("OK");
	    			   LlenarDatos(control, request);
	    			  } 
    		          
    		}
    		
    	      }
    	}
    	control.setOperacion("");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_descriptores","control",control);		
    }
    public String DeleteDescriptor(ConsultaDescriptoresCommand control){
    	try
    	{   String resultado="";
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla("");
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_DESCRIPTOR);
    		obj.setDscValor1(control.getCodSelec());
    		obj.setDscValor2(control.getCodValor());
    		obj.setDescripcion(control.getDescripSelec());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodEvaluador());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		 	
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		//log.info("Resultado: "+resultado);	
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }

    public void LlenarDatos(ConsultaDescriptoresCommand control, HttpServletRequest request){
    	//log.info("LlenarDatos");
    	
    	String descDescriptor=(String) request.getParameter("descripcionDescriptor");
    	String codDescriptor=(String) request.getParameter("codigoDescriptor");
    	//System.out.println("Descripcion paginado:"+dato);
    	
    	if (descDescriptor!=null){
    		if (!(descDescriptor.equals("")))
    			control.setDescripcionDescriptor(descDescriptor);
    	}		
    	if (codDescriptor!=null){
    		if (!(codDescriptor.equals("")))
    			control.setCodigoDescriptor(codDescriptor);
    	}
    	
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodDetalle(control.getCodigoDescriptor());
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_DESCRIPTOR);
		obj.setCodDetalle(control.getCodigoDescriptor());
		obj.setDescripcion(control.getDescripcionDescriptor());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setListaDescriptores(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		request.getSession().setAttribute("listaDescriptores", control.getListaDescriptores());
    }
    

    
}
