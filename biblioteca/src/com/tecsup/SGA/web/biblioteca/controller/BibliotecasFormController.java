package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.BibliotecasCommand;


public class BibliotecasFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(BibliotecasFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public PortalWebBibliotecaManager getPortalWebBibliotecaManager() {
		return portalWebBibliotecaManager;
	}
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		BibliotecasCommand command = new BibliotecasCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
		//command.setOperacion((String)request.getParameter("txhOperacion"));
    	//command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	//command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());
    	
    	LlenarDatos(command, request);
    	//LlenarComboTipoMaterial(command, request);
    	//log.info("INI");
    	//request.setAttribute("lstrURL", command.getLstrURL());
        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	BibliotecasCommand control = (BibliotecasCommand) command;
    	  	
        return new ModelAndView("/biblioteca/userWeb/bib_bibliotecas","control",control);		
    }
    
    public void LlenarDatos(BibliotecasCommand control, HttpServletRequest request){
    	    	
    	Secciones seccion= new Secciones();
    	seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_BIBLIOTECAS);
    	TipoTablaDetalle obj= new TipoTablaDetalle();
    	obj.setCodTipoTablaDetalle("");
    	    	
		control.setListaBibliotecas(this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj));
		//log.info("Tama�o: "+control.getListaBibliotecas().size());
		request.getSession().setAttribute("listaBibliotecas", control.getListaBibliotecas());
    	
    }
    
 
}
