package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.PortalNovedadesInicioCommand;

public class PortalNovedadesInicioFormController  extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PortalNovedadesInicioFormController.class);
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {		
		
		PortalNovedadesInicioCommand command= new PortalNovedadesInicioCommand();
		//command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	//command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
    	
		VerNovedades(command, request);
		
    	return command;
	}
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    /*
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	PortalNovedadesInicioCommand control= (PortalNovedadesInicioCommand) command;
    	//return new ModelAndView("/biblioteca/userAdmin/buzonSugerencias/bib_portal_novedades_inicio","control",control);
    	return new ModelAndView("/biblioteca/userWeb/bib_portal_novedades_inicio","control",control);
    }
    */
    
    public void VerNovedades(PortalNovedadesInicioCommand control, HttpServletRequest request){
    	
    	Secciones seccion= new Secciones();
    	seccion.setCodTipo(CommonConstants.TIPT_BIBLIO_PORTAL_NOVEDADES);
    	TipoTablaDetalle obj= new TipoTablaDetalle();
    	obj.setCodTipoTablaDetalle("");
    	    	
		List lista=this.portalWebBibliotecaManager.GetSeccionesPortalWeb(seccion, obj);
		if(lista!=null)
		{    if(lista.size()>0)
			   {
			    Secciones sk= new Secciones();
			     for(int k=0;k<lista.size();k++)
			     { sk=(Secciones)lista.get(k);
 
			     }
			   }
		}
		else lista=new ArrayList();
		request.getSession().setAttribute("listaNovedades", lista);
    }
}
