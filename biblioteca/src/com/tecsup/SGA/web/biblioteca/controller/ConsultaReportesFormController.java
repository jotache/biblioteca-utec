package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.BiblioReportesManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.common.Anios;

public class ConsultaReportesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaReportesFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private BiblioReportesManager biblioReportesManager;

	
	/*SETTER*/
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	public void setBiblioReportesManager(BiblioReportesManager biblioReportesManager) {
		this.biblioReportesManager = biblioReportesManager;
	}
	
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		ConsultaReportesCommand command = new ConsultaReportesCommand();
		command.setSedeSel((String)request.getParameter("txhCodSede"));
		//bandeja siempre se muestra cada vez que inicia la pagina
		//******************************************************************
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());
    	//System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
		//******************************************************************
		cargaBandeja(command);
		//request.getSession().setAttribute("listaIdioma", command.getListaIdioma());
		
    	TipoTablaDetalle obj = new TipoTablaDetalle();    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_PROCEDENCIA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		command.setLstProcedencia(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));

		
		
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	ConsultaReportesCommand control = (ConsultaReportesCommand) command;
    	
    	
    	if (control.getOperacion().trim().equals("BUSCAR")){    		
    		
    	}
    	    
	    return new ModelAndView("/biblioteca/userAdmin/consultaReportes/bib_consulta_reportes","control",control);
    }
    public void cargaBandeja(ConsultaReportesCommand control){
    	control.setListaReportes(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REPORTES
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	
    	Anios anios= new Anios();
    	
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_COD);
		control.setListaMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    	
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_MAT_BIB);
		
		control.setListaTipoRepMatBib(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_MOV_PRES_DEV_DIA);
		
		control.setListaTipoRepMovPresDev(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_EST_CICLO_ESP);
		
		control.setListaTipoRepCicloEsp(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_TIPO_USUARIOS);
		control.setLstTipoResultado(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		//control.setListaTipoUsuarios(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_USUARIO_BIBLIOTECA);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	control.setListaTipoUsuarios(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		//Tipos de Movimientos de reservas:
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_MOV_RESERV_DEV_DIA);
		control.setListaTipoRepMovReserDev(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SEDE);
		control.setListaSedeRep(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
    	control.setTxhReporte1(CommonConstants.REPORTE_1);
    	control.setTxhReporte2(CommonConstants.REPORTE_2);
    	control.setTxhReporte3(CommonConstants.REPORTE_3);
    	control.setTxhReporte4(CommonConstants.REPORTE_4);
    	control.setTxhReporte5(CommonConstants.REPORTE_5);
    	control.setTxhReporte6(CommonConstants.REPORTE_BIB_6);
    	control.setTxhReporte7(CommonConstants.REPORTE_BIB_7);
    	control.setTxhReporte8(CommonConstants.REPORTE_BIB_8);
    	
    	control.setTxhRep2A(CommonConstants.REPORTE_2A);
    	control.setTxhRep2B(CommonConstants.REPORTE_2B);
    	control.setTxhRep2C(CommonConstants.REPORTE_2C);
    	control.setTxhRep2D(CommonConstants.REPORTE_2D);
    	control.setTxhRep2E(CommonConstants.REPORTE_2E);
    	control.setTxhRep2F(CommonConstants.REPORTE_2F);
    	control.setTxhRep2G(CommonConstants.REPORTE_2G);
    	
    	control.setListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
    	control.setListaMeses(anios.getMeses02());
    	
    	control.setCboAnioSancion(CommonConstants.TIPT_PERIODO_ACTUAL);
    	control.setCboAnioBuzon(CommonConstants.TIPT_PERIODO_ACTUAL);
    	
    }
    
}
