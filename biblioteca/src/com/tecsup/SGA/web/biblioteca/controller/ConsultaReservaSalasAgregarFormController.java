package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.web.biblioteca.command.ConsultaReservaSalasAgregarCommand;

public class ConsultaReservaSalasAgregarFormController extends
    SimpleFormController {
  
  private static Log log = LogFactory
      .getLog(ConsultaReservaSalasAgregarFormController.class);
  
  private BiblioMantenimientoManager biblioMantenimientoManager;
  private PortalWebBibliotecaManager portalWebBibliotecaManager;
  private BiblioProcesoSancionesManager biblioProcesoSancionesManager;
  
  public void setBiblioProcesoSancionesManager(
      BiblioProcesoSancionesManager biblioProcesoSancionesManager) {
    this.biblioProcesoSancionesManager = biblioProcesoSancionesManager;
  }
  
  public void setPortalWebBibliotecaManager(
      PortalWebBibliotecaManager portalWebBibliotecaManager) {
    this.portalWebBibliotecaManager = portalWebBibliotecaManager;
  }
  
  public void setBiblioMantenimientoManager(
      BiblioMantenimientoManager biblioMantenimientoManager) {
    this.biblioMantenimientoManager = biblioMantenimientoManager;
  }
  
  protected Object formBackingObject(HttpServletRequest request)
      throws ServletException {
    
    ConsultaReservaSalasAgregarCommand command = new ConsultaReservaSalasAgregarCommand();
    Anios anios = new Anios();
    boolean flagLabQuimica = false;
    command.setAccion((String) request.getParameter("txhAccion"));
    command.setCodSecuencia((String) request.getParameter("txhCodSeqRecurso")); // Cod
                                                                                // secuencia
                                                                                // del
                                                                                // recurso.
    command.setCodRecurso((String) request.getParameter("txhCodRecurso"));// CodRecurso.
    command.setSedeSel((String) request.getParameter("txhSedeSel"));
    
    UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad) request.getSession()
        .getAttribute("usuarioSeguridad");
    
    if (usuarioSeguridad != null) {
      command.setCodEvaluador(usuarioSeguridad.getIdUsuario());
      command.setTxtUserReserva(usuarioSeguridad.getCodUsuario());
    }
    
    if (usuarioSeguridad != null && usuarioSeguridad.getCiclo() != null
        && usuarioSeguridad.getCodigoEspecialidad() != null) {
      flagLabQuimica = (usuarioSeguridad.getCiclo().equals("3")
          || usuarioSeguridad.getCiclo().equals("4")
          || usuarioSeguridad.getCiclo().equals("5") || usuarioSeguridad
          .getCiclo().equals("6"))
          && usuarioSeguridad.getCodigoEspecialidad().equals("11");
    }
    command.setFlgLabQuimica(flagLabQuimica ? "1" : "0");
    
    command.setCodPeriodo((String) request.getParameter("txhCodPeriodo"));
    command.setCodSalas((String) request.getParameter("txhCodigoSala"));
    
    command
        .setTxtCapacidadMaxima((String) request.getParameter("txhCapMaxima"));
    command.setFechaBD((String) request.getParameter("txhFechaBD"));
    command.setTxtVacantes((String) request.getParameter("txhVacantes"));
    command.setEstado((String) request.getParameter("txhEstado"));
    log.info("TIPO ESTADO:" + command.getEstado());
    if (command.getEstado() == null)
      command.setEstado(request.getParameter("estado"));
    // CAMBIO RBN
    command.setCodTipoSala(request.getParameter("txhCodTipoSala"));
    
    if ("A".equalsIgnoreCase(command.getEstado()))
      request.setAttribute("pagina", "/biblioteca/logeoBiblioteca.html");
    else
      request.setAttribute("pagina", "/biblioteca/biblioteca/index.html");
    
    List<Sala> listaHoras = new ArrayList<Sala>();
    listaHoras = this.portalWebBibliotecaManager
        .ListarHorasAtencionPorSede((command.getSedeSel() == null ? "L"
            : command.getSedeSel()));
    
    command.setListaHoras(listaHoras);
    ComboConsultaSalas(command);
    
    if (command.getAccion() != null) {
      if (command.getAccion().equals("TERMINAR")
          || command.getAccion().equals("CANCELAR_EJECUTAR")) {
        
        Sala sala = portalWebBibliotecaManager.getReservaSalaById(
            command.getCodRecurso(), command.getCodSecuencia());
        command.setTxtCodUserReserva(sala.getCodUsuario());
        command.setTxtUserReserva(sala.getLoginUsuario().trim());
        command.setTxtCapacidadMaxima(sala.getCapacidadMax());
        command.setTxtFechaInicio(sala.getFechaReserva());
        command.setCodHoraInicio(sala.getHoraIni().substring(0, 2));
        command.setCodHoraFin(sala.getHoraFin().substring(0, 2));
        
      } else if (command.getAccion().equals("RESERVAR")) {
        command.setTxtCapacidadMaxima((String) request
            .getParameter("txhCapacidad"));
        command.setTxtFechaInicio((String) request.getParameter("txhFechaRec"));
        command.setCodHoraInicio(((String) request.getParameter("txhHoraIni"))
            .substring(0, 2));
        command.setCodHoraFin(((String) request.getParameter("txhHoraFin"))
            .substring(0, 2));
        
        command.setFechaReservSel((String) request.getParameter("txhFechaRec"));
        command.setHoraIniSel(((String) request.getParameter("txhHoraIni"))
            .substring(0, 2));
        command.setHoraFinSel(((String) request.getParameter("txhHoraFin"))
            .substring(0, 2));
        command.setCapacidadSel((String) request.getParameter("txhCapacidad"));
      }
      
    }
    
    return command;
  }
  
  protected void initBinder(HttpServletRequest request,
      ServletRequestDataBinder binder) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class,
        nf, true));
    
  }
  
  public ModelAndView processFormSubmission(HttpServletRequest request,
      HttpServletResponse response, Object command, BindException errors)
      throws Exception {
    return super.processFormSubmission(request, response, command, errors);
  }
  
  public ModelAndView onSubmit(HttpServletRequest request,
      HttpServletResponse response, Object command, BindException errors)
      throws Exception {
    
    String resultado = "";
    String cabMensaje = "";
    ConsultaReservaSalasAgregarCommand control = (ConsultaReservaSalasAgregarCommand) command;
    log.info("OPERACION:" + control.getOperacion());
    if (control.getOperacion().equals("TIPOSALA")) {
      log.info("Tipo estado:" + control.getEstado());
      List<Sala> listaHoras = new ArrayList<Sala>();
      listaHoras = this.portalWebBibliotecaManager
          .ListarHorasAtencionPorSede((control.getSedeSel() == null ? "L"
              : control.getSedeSel()));
      control.setListaHoras(listaHoras);
      
      ComboConsultaSalas(control);
      CapacidadMaxima(control, request);
    } else {
      // System.out.println("getAccion-Guardar Previo:"+ control.getAccion());
      // System.out.println("txtFechaInicio-Guardar Previo:"+
      // control.getTxtFechaInicio());
      if (control.getOperacion().equals("GUARDAR")) {
        resultado = InsertReservaSala(control);
        log.info("RPTA:" + resultado);
        
        /*
         * -- S_V_RETVAL:= '1'; --SE REALIZO LA RESERVA -- S_V_RETVAL:= '2';
         * --NO SE PUEDE REALIZAR LA RESERVA. EL USUARIO NO TIENE PERMISOS PARA
         * REGISTRAR RESERVAS -- S_V_RETVAL:= '3'; --NO SE PUEDE REALIZAR LA
         * RESERVA. FECHA � HORA YA TRANSCURRIDAS. -- S_V_RETVAL:= '4'; --NO SE
         * PUEDE REALIZAR LA RESERVA. EL USUARIO YA TIENE REGISTRADO UNA RESERVA
         * EN LA FECHA Y HORAS. -- S_V_RETVAL:= '5'; --NO SE PUEDE REALIZAR LA
         * RESERVA. EL USUARIO TIENE SANCIONES VIGENTES -- S_V_RETVAL:= '6';
         * --NO SE PUEDE REALIZAR LA RESERVA. LA SALA NO TIENE DISPONIBILIDAD --
         * S_V_RETVAL:= '7'; --NO SE PUEDE REALIZAR LA RESERVA. SE EXCEDIO EL
         * NRO DE HORAS PERMITIDAS A LA SEMANA
         */
        if (resultado.equals("-1"))
          control.setMsg("ERROR");
        else if (resultado.equals("1"))
          control.setMsg("OK");
        
        else {
          if (resultado.equals("2"))
            control.setMsg("NOT_ALLOW");
          else {
            if (resultado.equals("3"))
              control.setMsg("FECHA_HORA");
            else {
              if (resultado.equals("4"))
                control.setMsg("YA_EXISTE");
              else {
                if (resultado.equals("5"))
                  control.setMsg("SANCION");
                else {
                  if (resultado.equals("6"))
                    control.setMsg("NO_DISPONIBLE");
                  else {
                    if (resultado.equals("7"))
                      control.setMsg("EXCESO");
                    else {
                      if (resultado.equals("8"))
                        control.setMsg("ERROR_USUARIO");
                      else {
                        if (resultado.equals("9"))
                          control.setMsg("USUARIO_INHABILITADO");
                        else {
                          if (resultado.equals("10"))
                            control.setMsg("CRUCE_ATENCION"); // se detect� un
                                                              // cruce de horas
                                                              // con las
                                                              // atenciones de
                                                              // PC's
                          else if (resultado.equals("11")) {
                            control.setMsg("RESERVA_NO_PERMITIDA"); // usuario
                                                                    // de tecsup
                                                                    // o utec
                                                                    // solo
                                                                    // pueden
                                                                    // reservar
                                                                    // las que
                                                                    // les
                                                                    // correspondan.
                          } else if (resultado.equals("12")) {
                            control.setMsg("SALA_INHABILITADA"); // fecha de
                                                                 // reserva cae
                                                                 // en el rango
                                                                 // de sala
                                                                 // inhabilitada
                          } else if (resultado.equals("13")) {
                            control.setMsg("SALA_INHABILITADA_HORAS"); // fecha
                                                                       // y hora
                                                                       // de
                                                                       // reserva
                                                                       // cae en
                                                                       // el
                                                                       // rango
                                                                       // de
                                                                       // sala
                                                                       // inhabilitada.
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        
      } else if (control.getOperacion().equals("TERMINAR")) {
        resultado = TerminarReservaSala(control);
        log.info("RPTA:" + resultado);
        if (resultado.equals("-1"))
          control.setMsg("ERROR_TERMINAR");
        else if (resultado.equals("2"))
          control.setMsg("RECURSO_YA_NO_ESTA_EJECUTANDOSE");
        else if (resultado.equals("1"))
          control.setMsg("OK_TERMINAR");
      } else if (control.getOperacion().equals("CANCELARESERVA")) {
        resultado = cancelarReserva(control.getTxtCodUserReserva(),
            control.getCodRecurso(), control.getCodSecuencia(),
            control.getCodEvaluador());
        log.info("RPTA:" + resultado);
        cabMensaje = resultado.substring(0, 1);
        if ("1".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("OK_CANCELAR");
        } else if ("0".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("USU_NO_CONFIG_CANCELAR");
        } else if ("4".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("RECURSO_YA_NO_ESTA_RESERVADO_2");
        } else if ("2".equalsIgnoreCase(cabMensaje)) {
          String msg1 = "";
          String msg2 = "";
          String msg3 = "";
          String msg4 = "";
          StringTokenizer tkn = new StringTokenizer(resultado, "|");
          if (tkn.hasMoreTokens())
            msg1 = tkn.nextToken();
          if (tkn.hasMoreTokens())
            msg2 = tkn.nextToken();
          if (tkn.hasMoreTokens())
            msg3 = tkn.nextToken();
          if (tkn.hasMoreTokens())
            msg4 = tkn.nextToken();
          request.setAttribute("msg1", msg1);
          request.setAttribute("msg2", msg2);
          request.setAttribute("msg3", msg3);
          request.setAttribute("msg4", msg4);
          control.setMsg("APL_SANCION_CANCELAR");
        } else if ("9".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("ERROR_CANCELAR");
        }
        
      } else if (control.getOperacion().equals("APLICARESERVA")) {
        resultado = aplicarReserva(control.getTxtCodUserReserva(),
            control.getCodRecurso(), control.getCodSecuencia(),
            control.getCodEvaluador());
        log.info("RPTA:" + resultado);
        cabMensaje = resultado.substring(0, 1);
        if ("1".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("OK_APLICAR");
        } else if ("0".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("USU_NO_CONFIG");
        } else if ("3".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("FECHA_HORA_INVALIDA");
        } else if ("4".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("RECURSO_YA_NO_ESTA_RESERVADO");
        } else if ("2".equalsIgnoreCase(cabMensaje)) {
          String msg1 = "";
          String msg2 = "";
          String msg3 = "";
          String msg4 = "";
          StringTokenizer tkn = new StringTokenizer(resultado, "|");
          if (tkn.hasMoreTokens())
            msg1 = tkn.nextToken();
          if (tkn.hasMoreTokens())
            msg2 = tkn.nextToken();
          if (tkn.hasMoreTokens())
            msg3 = tkn.nextToken();
          if (tkn.hasMoreTokens())
            msg4 = tkn.nextToken();
          request.setAttribute("msg1", msg1);
          request.setAttribute("msg2", msg2);
          request.setAttribute("msg3", msg3);
          request.setAttribute("msg4", msg4);
          control.setMsg("APL_SANCION");
        } else if ("9".equalsIgnoreCase(cabMensaje)) {
          control.setMsg("ERROR_APLICAR");
        }
      }
    }
    
    // System.out.println("getAccion-Guardar Post:"+ control.getAccion());
    
    // control.setOperacion("");
    
    return new ModelAndView(
        "/biblioteca/userWeb/bib_consulta_reserva_salas_agregar", "control",
        control);
  }
  
  public String aplicarReserva(String usuarioReserva, String codRecurso,
      String codSecuencia, String codUsuarioSistema) {
    try {
      String res = "";
      res = this.biblioProcesoSancionesManager.insertSancionReservaSalas(
          usuarioReserva, codRecurso, codSecuencia, codUsuarioSistema, "0002");
      return res;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
  
  public String cancelarReserva(String usuarioReserva, String codRecurso,
      String codSecuencia, String codUsuarioSistema) {
    try {
      String res = "";
      res = this.biblioProcesoSancionesManager.insertSancionReservaSalas(
          usuarioReserva, codRecurso, codSecuencia, codUsuarioSistema, "0003");
      return res;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
  
  public void ComboConsultaSalas(ConsultaReservaSalasAgregarCommand control) {
    TipoTablaDetalle obj = new TipoTablaDetalle();
    
    obj.setDscValor3(CommonConstants.TIPT_TIPO_SALA);
    obj.setCodTipoTabla("-1".equalsIgnoreCase(control.getCodTipoSala()) ? ""
        : control.getCodTipoSala());
    obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SALA);
    obj.setCodDetalle("");
    obj.setDescripcion("");
    obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    obj.setDscValor6(control.getSedeSel());
    if (control.getCodRecurso() != null) {
      if (control.getCodRecurso() != "")
        control.setCodSalas(control.getCodRecurso());
    }
    if (control.getEstado().equals("A"))
      control.setCodListaSalas(this.biblioMantenimientoManager
          .getAllTablaDetalleBiblioteca(obj));
    else {
      // si es web... no mostrar los ambientes de Polideportivo
      List lista2 = new ArrayList();
      Iterator iter = this.biblioMantenimientoManager
          .getAllTablaDetalleBiblioteca(obj).iterator();
      while (iter.hasNext()) {
        TipoTablaDetalle obj1 = (TipoTablaDetalle) iter.next();
        if (!obj1.getDscValor1().trim().equals("0004")) {
          if (obj1.getDscValor1().trim().equals("0006")) {
            if (control.getFlgLabQuimica() == "1") {
              lista2.add(obj1);
            }
          } else {
            lista2.add(obj1);
          }
        }
      }
      control.setCodListaSalas(lista2);
    }
    
  }
  
  public void CapacidadMaxima(ConsultaReservaSalasAgregarCommand control,
      HttpServletRequest request) {
    control.setAccion(control.getAccion());
    Sala sala = new Sala();
    sala.setCodTipoSala(control.getCodSalas());
    
    List atila = new ArrayList();
    atila = this.portalWebBibliotecaManager.getMaximaCapacidadSalas(sala);
    
    Sala sal = new Sala();
    if (atila.size() > 0) {
      sal = (Sala) atila.get(0);
      control.setTxtCapacidadMaxima(sal.getCapacidadMax());
      control.setFechaBD(sal.getFechaBD());
      control.setTxtVacantes(sal.getVacantes());
      request.setAttribute("txtNombre", control.getTxtUserReserva());
    }
    
  }
  
  public String InsertReservaSala(ConsultaReservaSalasAgregarCommand control) {
    try {
      
      Sala sala = new Sala();
      String resultado = "";
      sala.setCodTipoSala(control.getCodSalas());
      sala.setFechaReserva(control.getTxtFechaInicio());
      sala.setHoraIni(control.getCodHoraInicio());
      sala.setHoraFin(control.getCodHoraFin());
      sala.setUsuReserva(control.getTxtUserReserva());
      sala.setUsuCrea(control.getCodEvaluador());
      
      if (control.getAccion().equals("RESERVAR")) {
        sala.setCodTipoSala(control.getCodRecurso());
        sala.setFechaReserva(control.getFechaReservSel());
        sala.setHoraIni(control.getHoraIniSel());
        sala.setHoraFin(control.getHoraFinSel());
      }
      
      resultado = this.portalWebBibliotecaManager.insertReservaSalas(sala);
      return resultado;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
    
  }
  
  public String TerminarReservaSala(ConsultaReservaSalasAgregarCommand control) {
    try {
      String resultado = "";
      resultado = this.portalWebBibliotecaManager.terminarReservaSalas(
          control.getCodRecurso(), control.getCodSecuencia(),
          control.getCodEvaluador());
      return resultado;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
}
