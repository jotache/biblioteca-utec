package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.biblioteca.command.SalasModificarCommand;


public class SalasModificarFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(SalasModificarFormController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	   	
    	SalasModificarCommand command = new SalasModificarCommand();
    
    	String codDetalle = (String)request.getParameter("codigo");
    	String dscDetalle = (String)request.getParameter("descripcion");
    	
    	command.setCodDetalle("");
    	
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	SalasModificarCommand control = (SalasModificarCommand) command;

    	
    	control.setOperacion("");
				
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_salas_modificar","control",control);		
    }
    
    
}
