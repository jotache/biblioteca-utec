package com.tecsup.SGA.web.biblioteca.controller;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.modelo.UsuarioSeguriBib;
import com.tecsup.SGA.service.biblioteca.BiblioReportesManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.inicio.command.MenuCommand;

public class ReporteResultadoFormController implements Controller{
	private static Log log = LogFactory.getLog(ReporteResultadoFormController.class);

	private BiblioReportesManager biblioReportesManager;
	private ComunManager comunManager;
	
    public void setBiblioReportesManager(BiblioReportesManager biblioReportesManager) {
		this.biblioReportesManager = biblioReportesManager;
	}

	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}
	
	public ReporteResultadoFormController(){
		
	}
    
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception {
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if(usuarioSeguridad==null){
    		InicioCommand command = new InicioCommand();
    		return new ModelAndView("/cabeceraBiblioteca","control",command);
    	}    	
    	
    	log.info("onSubmit:INI");
    	String opcion = request.getParameter("opcion");
    	System.out.println("ReporteResultadoFormController:opcion:"+opcion);
    	List consulta;
    	String subOpcion = "";
    	String dscSubOpcion = "";
    	String sede = "";
    	
    	String pagina = "";
    	String fecIni = "";
    	String fecFin = "";
    	String nroMax = "";
    	
    	String codUsuario = ""; //JHPR 2008-7-3
    	String tipoBusqueda = "";
    	String username = "";
    	
    	String fechaRep = Fecha.getFechaActual();
		String horaRep = Fecha.getHoraActual();
    	
		String nroIngresoIni = "";
		String nroIngresoFin = "";
		
    	ModelMap model = new ModelMap();
    	
    	model.addObject("fechaRep", fechaRep);
		model.addObject("horaRep", horaRep);
    	
    	//Usuarios deudores y sancionados
    	if(opcion.equals(CommonConstants.REPORTE_1)){
    		 
    		subOpcion = request.getParameter("subOpcion"); //0001 sancionado / 0002 deudor
    		//String periodo = request.getParameter("periodo");
    		String periodo1 = request.getParameter("periodo1");
    		String periodo2 = request.getParameter("periodo2");
    		sede = request.getParameter("sede");
    		String tipoUsuario = request.getParameter("tipousuario");
    		//String dscPeriodo = request.getParameter("dscPeriodo");
    		dscSubOpcion = request.getParameter("dscSubOpcion");
    		
    		//model.addObject("dscPeriodo", dscPeriodo);
    		model.addObject("dscPeriodo", periodo1 + " - " + periodo2);
    		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
    		model.addObject("dscSubOpcion", dscSubOpcion);
    		
    		consulta = biblioReportesManager.getReporteUsuariosSancion(periodo1,periodo2, subOpcion,sede,tipoUsuario);
        	request.getSession().setAttribute("consulta", consulta);
        	
        	if(consulta.size()>0){
        		pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_usuario_sancion";
    		}
    		else
    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
    		
    	}
    	//Material Bibliográfico
    	else if(opcion.equals(CommonConstants.REPORTE_2)){
    		
    		subOpcion = request.getParameter("subOpcion");
    		sede = request.getParameter("sede");
    		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
    		
    		if(subOpcion.equals(CommonConstants.REPORTE_2A)){
    			consulta = biblioReportesManager.getReporteCatalogoFichas(sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0){
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_catalogo_ficha";
        		}
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";

    		}
    		else if(subOpcion.equals(CommonConstants.REPORTE_2B)){
    			fecIni = request.getParameter("fecIni");
    			fecFin = request.getParameter("fecFin");
    			
    			model.addObject("fecIni", fecIni);
        		model.addObject("fecFin", fecFin);
    			
    			consulta = biblioReportesManager.getReporteMatFaltante(fecIni, fecFin,sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0){
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_mat_faltante";
        		}
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
    			
    		}
    		else if(subOpcion.equals(CommonConstants.REPORTE_2C)){
    			fecIni = request.getParameter("fecIni");
    			fecFin = request.getParameter("fecFin");
    			String tipoCambio = (String)request.getParameter("tipoCambio").trim();
    			String procedencia = (String)request.getParameter("procedencia").trim();
    			model.addObject("fecIni", fecIni);
        		model.addObject("fecFin", fecFin);
        		model.addObject("tipoCambio", tipoCambio);
    			System.out.println("procedencia:"+procedencia);
    			consulta = biblioReportesManager.getReporteMatAdquiridos(fecIni, fecFin, tipoCambio,sede,procedencia);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0){
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_mat_adquiridos";
        		}
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
    			
    		}
    		else if(subOpcion.equals(CommonConstants.REPORTE_2D)){
    			fecIni = request.getParameter("fecIni");
    			fecFin = request.getParameter("fecFin");
    			
    			model.addObject("fecIni", fecIni);
        		model.addObject("fecFin", fecFin);
        		
    			consulta = biblioReportesManager.getReporteMatRetirados(fecIni, fecFin,sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0){
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_mat_retirados";
        		}
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
    			
    		}
    		else if(subOpcion.equals(CommonConstants.REPORTE_2E)){
    			fecIni = request.getParameter("fecIni");
    			fecFin = request.getParameter("fecFin");
    			nroMax = request.getParameter("nroMax");
    			
    			model.addObject("fecIni", fecIni);
        		model.addObject("fecFin", fecFin);
        		model.addObject("nroMax", nroMax);
    			
    			consulta = biblioReportesManager.getReporteMatMasSolicitado(fecIni, fecFin, nroMax,sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0){
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_mat_solicitados";
        		}
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
    			
    		}else if(CommonConstants.REPORTE_2G.equals(request.getParameter("subOpcion"))){  
        		subOpcion = request.getParameter("subOpcion");    		
        		fecIni = request.getParameter("fecIni");
    			fecFin = request.getParameter("fecFin");	
    			sede = request.getParameter("sede");
    			
    			nroIngresoIni = request.getParameter("nroIng1");
    			nroIngresoFin = request.getParameter("nroIng2");
    			if(nroIngresoIni==null) nroIngresoIni="";
    			if(nroIngresoFin==null) nroIngresoFin="";
    			String tipoRep = (nroIngresoIni.equals("")?"1":"2"); //opcion x Fecha o x Rando de Nros. Ingreso
    			
        		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
    			model.addObject("fecIni", fecIni);
        		model.addObject("fecFin", fecFin);
        		model.addObject("tipo_rep", tipoRep);
        		model.addObject("ing_ini", nroIngresoIni);
        		model.addObject("ing_fin", nroIngresoFin);
        		
        		log.info(fecIni+"-"+fecFin+"-"+sede);
    			consulta = biblioReportesManager.getReporteEtiquetas(fecIni, fecFin,sede, tipoRep, nroIngresoIni, nroIngresoFin);
        		request.getSession().setAttribute("consulta", consulta);   
        		response.setHeader( "Content-Disposition", "attachment; filename=\"Etiquetas.pdf\"" );
        		
        		System.out.println("Lista: "+consulta.size());
//        		Iterator u = consulta.iterator();
//        		while(u.hasNext()){
//        			Reporte reporte = (Reporte)u.next();
//        			System.out.println(reporte.getCodigo());
//        		}
        		
        		if(consulta.size()>0){
        			
        			try{
        				Document doc = new Document(PageSize.A4,20,40,40,0);
        				
        				doc.addTitle("Reporte de Material");
        				doc.addAuthor("Erick Benites");
        				doc.addSubject("Reporte de Material Bibliográfico");
        				doc.addKeywords("Tecsup Virtual");
        				doc.addCreator("iText");
        				
        				PdfWriter writer = PdfWriter.getInstance(doc, response.getOutputStream());
        				//PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream("C:/demo.pdf"));
        				writer.setCloseStream(true);
        				doc.open();        				
        				
        				//Tabla
        				float[] widths = {30,30,30,30,30,30,30}; //Ancho
        				PdfPTable table = new PdfPTable(widths);
        				table.setWidthPercentage(100);
        				table.getDefaultCell().setPadding(3);
        				table.getDefaultCell().setIndent(10);
        				table.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
        				table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        				table.getDefaultCell().setBorder(Rectangle.BOX);
        				table.getDefaultCell().setBorderColor(Color.BLACK);
        				table.getDefaultCell().setBorderWidth(1);
        				table.getDefaultCell().setMinimumHeight(70); //Alto
        				
        				Iterator x = consulta.iterator();
        				int i = 0;
        				int ancho = 7; //Ancho
        				String[] vals = new String[ancho]; //---
        	    		while(x.hasNext()){
        	    			i++;
        	    			
        	    			Reporte reporte = (Reporte)x.next();
        					PdfPCell cell = new PdfPCell(table.getDefaultCell());
        					cell.setPhrase(new Phrase(reporte.getCodigo(), FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0x00, 0x00, 0x00))));
        					table.addCell(cell);
        					
        					vals[(i-1)%ancho] = reporte.getCiclo(); //---
        					
        					if(i==consulta.size() ){
            					for(int n=0; n<ancho-(i%ancho);n++){
            						PdfPCell cellFill = new PdfPCell(table.getDefaultCell());
            						cellFill.setPhrase(new Phrase(""));
                					table.addCell(cellFill);
            					}
        					}
        					
        					if(i%ancho == 0 || i==consulta.size()){
        						for (int m = 0; m < ancho; m++) {
        							PdfPCell cell2 = new PdfPCell();
        							cell2.setPadding(1);
        							cell2.setVerticalAlignment(Element.ALIGN_TOP);
        							cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        							cell2.setBorder(Rectangle.NO_BORDER);
        							cell2.setMinimumHeight(10);
        							cell2.setPhrase(new Phrase((vals[m]==null)?"":vals[m], FontFactory.getFont(FontFactory.HELVETICA, 6)));
        							table.addCell(cell2);
        						}
        						
        						vals = new String[ancho]; //---
        						
        					}
        					
        	    		}
        				
        				doc.add(table);
        				
        				//FIn
        								
//        				log.info(doc);
        				
        				doc.close();
        				writer.close();
        				        				
        			} catch (Exception e) {
        				log.error(e);
        			}
        			
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_etiquetas";
        			//return new ModelAndView();
        		}
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";    		
        		
        	}

    	}
    	//Ciclo y especialidad
    	else if(opcion.equals(CommonConstants.REPORTE_3)){
    		subOpcion = request.getParameter("subOpcion");
    		sede = request.getParameter("sede");
    		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
    		fecIni = request.getParameter("fecIni");
			fecFin = request.getParameter("fecFin");
    		
			model.addObject("fecIni", fecIni);
    		model.addObject("fecFin", fecFin);
			
			if(subOpcion.equals(CommonConstants.REPORTE_3A)){
				consulta = biblioReportesManager.getReporteEstadTipoUsuario(fecIni, fecFin,sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0)
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_estad_tipo_usuario";        		
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";								
			}
			else if(subOpcion.equals(CommonConstants.REPORTE_3B)){
				consulta = biblioReportesManager.getReporteEstadCicloEsp(fecIni, fecFin,sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0)
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_estad_ciclo_espec";        		
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
				
				
			}
    	}
    	//Movimiento del día
    	else if(opcion.equals(CommonConstants.REPORTE_4)){
    		
    		subOpcion = request.getParameter("subOpcion");    		
    		fecIni = request.getParameter("fecIni");
			fecFin = request.getParameter("fecFin");
			sede = request.getParameter("sede");
    		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
			model.addObject("fecIni", fecIni);
    		model.addObject("fecFin", fecFin);		
    		
    		if(subOpcion.equals(CommonConstants.REPORTE_4A)){    			
    			consulta = biblioReportesManager.getReporteMovPrestDevDiaHra(fecIni, fecFin,sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0)
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_estad_mov_prest_dev_dia";        		
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
        		
    		}
    		else if(subOpcion.equals(CommonConstants.REPORTE_4B)){    			
    			//consulta = biblioReportesManager.getReporteMovPrestDevDiaHra(fecIni, fecFin); //getReporteResumenMovDia
    			consulta = biblioReportesManager.getReporteResumenMovDia(fecIni, fecFin,sede);
        		request.getSession().setAttribute("consulta", consulta);
        		
        		if(consulta.size()>0)        		        		
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_estad_resumen_mov_dia";        		
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
        		
    		}
    		
    	}
    	//Buzon de Sugerencias
    	else if(opcion.equals(CommonConstants.REPORTE_5)){
			String periodo = request.getParameter("periodo");
			String periodoDesc = request.getParameter("periodoDesc");
			String codTipoMaterial = request.getParameter("codTipoMaterial");
			String descMaterial = request.getParameter("descMaterial");
			
			model.addObject("periodo", periodoDesc);
			model.addObject("descMaterial", descMaterial);
			
			consulta = biblioReportesManager.getReporteBuzonSugerencia(periodo, codTipoMaterial);
    		request.getSession().setAttribute("consulta", consulta);
    		
    		if(consulta.size()>0){
    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_buzon_sugerencias";
    		}
    		else
    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
			
    	}else if(opcion.equals(CommonConstants.REPORTE_BIB_6)){
			//JHPR 2008-7-3
			tipoBusqueda = request.getParameter("tipobusqueda");
			codUsuario = request.getParameter("codusuario");
	    	username = request.getParameter("username");
	    	username = username.toLowerCase();
	    	
	    	sede = request.getParameter("sede");
    		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
	    	
	    	UsuarioSeguridad usuarioRep = this.comunManager.getUsuarioById(
	    				(codUsuario.trim().equals("")?"USUARIO":"CODIGO"), 
	    				(codUsuario.trim().equals("")?username:codUsuario)
	    				);
	    	
	    	if (usuarioRep!=null) {
	    		
	    		/*System.out.println("usuarioRep.getCodUsuario():"+usuarioRep.getCodUsuario());
	    		System.out.println("usuarioRep.getNomUsuario():"+usuarioRep.getNomUsuario());
	    		System.out.println("tipoBusqueda:"+tipoBusqueda);*/
	    		
    	    	model.addObject("codusuariorep", usuarioRep.getCodUsuario());
	    		//model.addObject("codusuariorep", username);
    	    	model.addObject("nombreusuariorep", usuarioRep.getNomUsuario());    	    	
    	    	if (!(codUsuario.trim().equals(""))){
    	    		consulta = biblioReportesManager.getReporteUsuarioIndividual(codUsuario, null,sede);	
    	    	}else{
    	    		consulta = biblioReportesManager.getReporteUsuarioIndividual(null, username,sede);
    	    	}
    			
        		request.getSession().setAttribute("consulta", consulta);            		
        		if(consulta.size()>0){
        			//JHPR 2008-08-27        			
        			Reporte fec  = (Reporte) consulta.get(0);        			
        			model.addObject("fechafinsancion",fec.getFechaFinSancion());
        			//ESBC was here
        			model.addObject("fechafinsuspension",fec.getFechaFinSuspension());
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_usuario_individual";            		
        		}else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
	    		
	    	} else {
	    		pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
	    	}  
	    	
	    	System.out.println("pagina:"+pagina);
	    	
		}else if (opcion.equals(CommonConstants.REPORTE_BIB_7)){
			subOpcion = request.getParameter("subOpcion");    		
    		fecIni = request.getParameter("fecIni");
			fecFin = request.getParameter("fecFin");    	
			sede = request.getParameter("sede");
    		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
			model.addObject("fecIni", fecIni);
    		model.addObject("fecFin", fecFin);			
			
			consulta = biblioReportesManager.getReporteReservaCicloEsp(fecIni, fecFin, sede);
    		request.getSession().setAttribute("consulta", consulta);        		
    		if(consulta.size()>0){
    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_reserv_ciclo_espec";        															 
    		}
    		else
    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
		
		}else if(opcion.equals(CommonConstants.REPORTE_8)){    		
    		subOpcion = request.getParameter("subOpcion");    		
    		fecIni = request.getParameter("fecIni");
			fecFin = request.getParameter("fecFin");	
			sede = request.getParameter("sede");
    		model.addObject("dscSede", sede.equals("A")?"Arequipa":(sede.equals("T")?"Trujillo":"Lima") );
			model.addObject("fecIni", fecIni);
    		model.addObject("fecFin", fecFin);			    		    		
    			//consulta = biblioReportesManager.getReporteMovPrestDevDiaHra(fecIni, fecFin); //getReporteResumenMovDia
    			consulta = biblioReportesManager.getReporteResumenResDia(fecIni, fecFin,sede);
        		request.getSession().setAttribute("consulta", consulta);      		
        		if(consulta.size()>0){
        			//pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_estad_mov_prest_dev_dia"; //bib_reporte_estad_resumen_mov_dia        			
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_reporte_estad_resumen_res_dia";
        		}
        		else
        			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";    		
    		
		}
    	
    	
    	//System.out.println("pagina:"+pagina);
    	    
    	return new ModelAndView(pagina,"model",model);
    	
    }


}
