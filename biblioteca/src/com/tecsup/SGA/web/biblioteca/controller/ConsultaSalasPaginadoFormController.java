package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaSalasPaginadoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaSalasPaginadoFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		ConsultaSalasPaginadoCommand command = new ConsultaSalasPaginadoCommand();				
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));    	
    	command.setSedeSel((String)request.getParameter("sede"));
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	ConsultaSalasPaginadoCommand control = (ConsultaSalasPaginadoCommand) command;
    	
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
		if (control.getOperacion().trim().equals("QUITAR")){			
    		resultado = DeleteSala(control);    		
    		if(resultado.equals("0")){
    			control.setMsg("OK");
    		}
    	}

	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_salas_paginado","control",control);		
    }    
    public String DeleteSala(ConsultaSalasPaginadoCommand control){
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SALA);
    		obj.setDscValor1(control.getCodigoSec());
    		obj.setDscValor2(control.getCodigoSala());
    		obj.setDescripcion(control.getDescripcion());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodUsuario());
    		
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}
