package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.CiudadesAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.ConsultaSalasCommand;
import com.tecsup.SGA.web.biblioteca.command.IdiomasAgregarCommand;
import com.tecsup.SGA.web.biblioteca.command.SalasAgregarCommand;


public class CiudadesAgregarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CiudadesAgregarFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI");   	
    	
    	CiudadesAgregarCommand command = new CiudadesAgregarCommand();    	
    	cargaCabecera(command);
		//******************************************************************
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));    	
    	//System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
		//******************************************************************
    	String operacion=((String)request.getParameter("txhOperacion")!=null?(String)request.getParameter("txhOperacion"):"");
		if(operacion.equalsIgnoreCase("MODIFICAR")){
			command.setCodigoSec((String)request.getParameter("txhCodigoSec"));
	    	command.setCodigo((String)request.getParameter("txhCodigoCiudad"));
	    	command.setDscCiudad((String)request.getParameter("txhDescripcion"));
	    	command.setPais((String)request.getParameter("txhPais"));
	    	command.setOperacion(operacion);
	    	/*System.out.println(">>"+command.getCodigoSec()+"<<");
	    	System.out.println(">>"+command.getCodigo()+"<<");	    	
	    	System.out.println(">>"+command.getDscCiudad()+"<<");
	    	System.out.println(">>"+command.getPais()+"<<");	    	
	    	System.out.println(">>"+command.getOperacion()+"<<");*/
		}
		else{
			command.setOperacion(operacion);
			//System.out.println(">>"+command.getOperacion()+"<<");
		}    	
        //log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	
    	CiudadesAgregarCommand control = (CiudadesAgregarCommand) command;    	
    	log.info("OPERACION:"+control.getOperacion());
    	if (control.getOperacion().trim().equals("AGREGAR"))
    	{
    		resultado = InsertCiudad(control);
    		log.info("RPTA:" + resultado);
    		//System.out.println("resultado de grabar-->"+resultado);
    		//******************************************
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}
    	else
		if (control.getOperacion().trim().equals("MODIFICAR"))
    	{	//System.out.println("entra para modificar");
    		resultado = UpdateCiudad(control);
    		//System.out.println("resultado de modificar-->"+resultado);
    		//******************************************
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}   	
		//log.info("onSubmit:FIN");		
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_ciudades_agregar","control",control);		
    }
    public void cargaCabecera(CiudadesAgregarCommand control){    	
    	Pais obj = new Pais();
    	
    	obj.setPaisCodigo("");
    	obj.setPaisDescripcion("");
    	obj.setPaisId("");
    	
    	control.setListaPais(this.biblioMantenimientoManager.getAllPaises(obj, CommonConstants.TIPO_ORDEN_DSC));
    }
    private String InsertCiudad(CiudadesAgregarCommand control){    	
    	try{
    		Ciudad obj = new Ciudad();
    		String resultado="";
    		
    		
    		obj.setPaisId(control.getPais());
    		obj.setCiudadDescripcion(control.getDscCiudad());
    		obj.setUsuCrea(control.getCodUsuario());
    		
    		/*System.out.println(">>"+control.getPais()+"<<");    		
    		System.out.println(">>"+control.getDscCiudad()+"<<");    		
    		System.out.println(">>"+control.getCodUsuario()+"<<");*/
    		
    		resultado=this.biblioMantenimientoManager.insertCiudadByPais(obj);
    		
    		return resultado;
    	}
    	catch (Exception e) {
    		e.printStackTrace(); 
		}
    	return null;
    }
    private String UpdateCiudad(CiudadesAgregarCommand control)
    {	
    	try{
    		Ciudad obj = new Ciudad();
    		
    		String resultado="";
    		
    		obj.setPaisId(control.getPais());
    		obj.setCiudadId(control.getCodigoSec());
    		obj.setCiudadDescripcion(control.getDscCiudad());
    		obj.setEstReg(CommonConstants.UPDATE_MTO_BIB);
    		obj.setUsuCrea(control.getCodUsuario());
    		
    		/*System.out.println(">>"+control.getPais()+"<<");
    		System.out.println(">>"+control.getCodigoSec()+"<<");
    		System.out.println(">>"+control.getDscCiudad()+"<<");
    		System.out.println(">>"+CommonConstants.UPDATE_MTO_BIB+"<<");
    		System.out.println(">>"+control.getCodUsuario()+"<<");*/
    		
    		resultado=this.biblioMantenimientoManager.updateDeleteCiudadbyPais(obj);
    		
    		return resultado;	
    		
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;    	
    }    
}
