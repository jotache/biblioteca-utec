package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaTipoVideoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaTipoVideoFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		
		ConsultaTipoVideoCommand command = new ConsultaTipoVideoCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());
    	
    	if(command.getCodUsuario()!=null){
    		if(!command.getCodUsuario().equals("")){
    			cargaBandeja(command);
    			request.getSession().setAttribute("listaVideo", command.getListaVideo());
    		}
    	}
		
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	ConsultaTipoVideoCommand control = (ConsultaTipoVideoCommand) command;
    	String resultado=""; 
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR")){
    		
    		cargaBandeja(control);
    		request.getSession().setAttribute("listaVideo", control.getListaVideo());
    	}
    	else
		if (control.getOperacion().trim().equals("QUITAR")){
			log.info("RPTA:" + resultado);
    		resultado = DeleteIdioma(control);
    		cargaBandeja(control);
    		request.getSession().setAttribute("listaVideo", control.getListaVideo());
    		control.setOperacion("");
    	}
    	
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_tipo_video","control",control);		
    }
    public void cargaBandeja(ConsultaTipoVideoCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    	obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_VIDEO);
		obj.setCodDetalle(control.getCodigo());
		obj.setDescripcion(control.getDscVideo());
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setListaVideo(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    }
    public String DeleteIdioma(ConsultaTipoVideoCommand control){
    	try{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado="";
    		
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_VIDEO);
    		obj.setDscValor1(control.getCodigoSec());
    		obj.setDscValor2(control.getCodigoVideo());
    		obj.setDescripcion(control.getDescripcion());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodUsuario());
    		
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}
