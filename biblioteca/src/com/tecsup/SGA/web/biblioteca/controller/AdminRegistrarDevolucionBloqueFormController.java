package com.tecsup.SGA.web.biblioteca.controller;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Sancion;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarDevolucionCommand;

public class AdminRegistrarDevolucionBloqueFormController extends
		SimpleFormController {

	private static Log log = LogFactory.getLog(AdminRegistrarDevolucionBloqueFormController.class);
	
	private BiblioGestionMaterialManager biblioGestionMaterialManager;
	private BiblioProcesoSancionesManager biblioProcesoSancionesManager;
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}
	public void setBiblioProcesoSancionesManager(
			BiblioProcesoSancionesManager biblioProcesoSancionesManager) {
		this.biblioProcesoSancionesManager = biblioProcesoSancionesManager;
	}
	
	
	protected Object formBackingObject(HttpServletRequest request)
		    throws ServletException {
		
		AdminRegistrarDevolucionCommand control = new AdminRegistrarDevolucionCommand();
		
		control.setUsuario(request.getParameter("txhCodUsuario"));
		control.setTipoUsuario(request.getParameter("txhTipoUsuario"));
		control.setSedeSel(request.getParameter("txhSedeSel"));
    	control.setSedeUsuario(request.getParameter("txhSedeUsuario"));		
		control.setCodPrestamoMultiple(request.getParameter("idBloque"));	
		control.setTxhCodUsuarioLogin(request.getParameter("txhCodUsuarioLogin"));
			
//		log.info("control.getUsuario():"+control.getUsuario());
//		log.info("control.getTipoUsuario()"+control.getTipoUsuario());
//		log.info("control.getSedeSel():"+control.getSedeSel());
//		log.info("control.getSedeUsuario():"+control.getSedeUsuario());
//		log.info("control.getCodPrestamoMultiple():"+control.getCodPrestamoMultiple());
//		log.info("control.getTxhCodUsuarioLogin():"+control.getTxhCodUsuarioLogin());
		
		String fecActual = Fecha.getFechaActual();
		String fecMinDate = "";
		log.info("fecActual 1: " + fecActual);  
		
		
		//Listar libros prestados...
		if(control.getCodPrestamoMultiple()!=null){
			List<MaterialxIngresoBean> librosBloque = new ArrayList<MaterialxIngresoBean>();
			librosBloque = biblioProcesoSancionesManager.listaBloqueLibrosPrestados(control.getCodPrestamoMultiple());		
			fecMinDate = librosBloque.get(0).getFecPrestamo(); //La fecha de Devolucion es la fecha minima limite que se puede seleccionar en la interfaz
			log.info("fecActual 2: " + fecMinDate);
			request.setAttribute("bloquePrestamo", librosBloque);	
		}
		 		
		request.setAttribute("fecActual", fecActual);
		request.setAttribute("fecMinDate", fecMinDate);
		
		//Mostrar sancion vigente x prestamo de libro (si tuviera)
		List<Sancion> sancionesPrestMat = new ArrayList<Sancion>();
		sancionesPrestMat = biblioProcesoSancionesManager.getSancionesUsuarioPorPrestamoMaterial(control.getTxhCodUsuarioLogin());
		if(sancionesPrestMat!=null && !sancionesPrestMat.isEmpty()){
			Sancion oSancion = sancionesPrestMat.get(0);
			
			log.info(oSancion);
			request.setAttribute("sancion", oSancion);
		}
		
		return control;
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
		
		AdminRegistrarDevolucionCommand control = (AdminRegistrarDevolucionCommand) command;
		
		log.info("OPERACION: " + control.getOperacion());		
		
		if(control.getOperacion().equals("DEVOLUCION_BLOQUE")){
			log.info("DEVOLUCION_BLOQUE");
			try{
				String result = biblioProcesoSancionesManager.actualizarPrestamoBloque(
						control.getTxtCadenaDatos(), control.getTxtTamanioLista(), control.getTxhCodUsuarioLogin(), 
						control.getUsuario());
				
				log.info("RESULT:"+result);
				request.setAttribute("result", result);	
			}catch(Exception e){
				log.info(e);
				request.setAttribute("result", "-1");
			}
			
		}
		
		//Listar libros prestados...
		List<MaterialxIngresoBean> librosBloque = new ArrayList<MaterialxIngresoBean>();
		librosBloque = biblioProcesoSancionesManager.listaBloqueLibrosPrestados(control.getCodPrestamoMultiple());			
		request.setAttribute("bloquePrestamo", librosBloque); 

		String fecMinDate = librosBloque.get(0).getFecPrestamo();
		request.setAttribute("fecMinDate", fecMinDate);
		
		//Mostrar sancion vigente x prestamo de libro (si tuviera)
		List<Sancion> sancionesPrestMat = new ArrayList<Sancion>();
		log.info("Sanciones para el Usuario:"+control.getUsuario());
		sancionesPrestMat = biblioProcesoSancionesManager.getSancionesUsuarioPorPrestamoMaterial(control.getTxhCodUsuarioLogin());
		log.info("sancionesPrestMat.size():" + sancionesPrestMat.size());
		if(sancionesPrestMat!=null && !sancionesPrestMat.isEmpty()){
			Sancion oSancion = sancionesPrestMat.get(0);
			log.info(oSancion);
			request.setAttribute("sancion", oSancion);
		}
		
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_registrar_devolucion_bloque","control",control);
	}
	
}
