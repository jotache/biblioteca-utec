package com.tecsup.SGA.web.biblioteca.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.MtoModificarTipoPerfilCommand;


public class ConsultaMaterialModificarFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConsultaMaterialModificarFormController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI");   	
    	MtoModificarTipoPerfilCommand command = new MtoModificarTipoPerfilCommand();
    
    	String codDetalle = (String)request.getParameter("txhCodDetalle");
    	String dscDetalle = (String)request.getParameter("txhDscDetalle");
    	//log.info(codDetalle+" y "+dscDetalle);
    	command.setCodDetalle("");
    	
    	//if ( codDetalle != null ) if ( !codDetalle.trim().equals("") ) cargaDetalle(command, codDetalle);
    	
        //log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	String resultado = "";
    	MtoModificarTipoPerfilCommand control = (MtoModificarTipoPerfilCommand) command;

    	
    	control.setOperacion("");
		//log.info("onSubmit:FIN");		
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_concepto_perfil_modificacion","control",control);		
    }
    
   
}
