package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class ConsultaDeweyFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConsultaDeweyFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		ConsultaDeweyCommand command = new ConsultaDeweyCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));
    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());
    	//command.setCodCategoria("-1");
    	LlenarDatos(command, request);
    	
    	request.setAttribute("lstrURL", command.getLstrURL());
    	if(command.getOperacion()==null){		
			request.getSession().removeAttribute("listaDewey");			
		}
    	//log.info("formBackingObject:FIN");
    	//if(command.getCodCategoria().equals("-1"))
    	//	command.setListaDewey(new ArrayList());
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	ConsultaDeweyCommand control = (ConsultaDeweyCommand) command;
    	String resultado="";
    	log.info("Operacion: "+control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{   LlenarDatos(control, request);
         	LlenarBandeja(control, request);
    		
    	}
    	else{ if (control.getOperacion().trim().equals("ELIMINAR"))
    	      { 
    	      control.setDescripcionDewey("");
    		    resultado = DeleteDewey(control);
    		    log.info("RPTA: "+resultado);
	    		if ( resultado.equals("-1")) control.setMsg("ERROR");
	    		else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
	    		      else
	    			  {control.setMsg("OK");
	    			    LlenarDatos(control, request);
	    			    LlenarBandeja(control, request);
	    			  }
    		
    		}
    		
    	      }
    	   else{ if(control.getOperacion().trim().equals("CATEGORIAS")){
    		   
    		    LlenarDatos(control, request);
    		    LlenarBandeja(control, request);
    	                }
    	   		else{if(control.getOperacion().trim().equals("LIMPIAR")){
    	   			   List lista= new ArrayList();
    	   			   //control.setListaDewey(lista);
    	   			  request.getSession().setAttribute("listaDewey", lista);
    	   		      }
    	   		 	else{ if(control.getOperacion().trim().equals("")){
    	   		 		   LlenarDatos(control, request);
    	   		 		   //log.info("En el OnSubmit donde Operacion es vacio"+ control.getOperacion()+control.getDescripcionDewey());
    	   		 		  
    	   		 	          }
    	   		 		
    	   		 	    }
    	   		} 
    	       }
    	}
    	control.setOperacion("");
    	//log.info("onSubmit:FIN");
	    return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_consulta_dewey","control",control);		
    }

    public String DeleteDewey(ConsultaDeweyCommand control){
    	try
    	{   //log.info("DeleteDescriptor");
    		String resultado="";
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		obj.setCodTipoTabla(control.getCodCategoria());
    		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_CLASIFICACION_DEWEY);
    		obj.setDscValor1(control.getCodSelec());
    		obj.setDscValor2(control.getCodValor());
    		obj.setDescripcion(control.getDescripSelec());
    		obj.setEstReg(CommonConstants.DELETE_MTO_BIB);
    		obj.setUsuario(control.getCodEvaluador());
    		obj.setDscValor3("");
    		obj.setDscValor4("");
    		obj.setDscValor5("");
    		 	
    		resultado=this.biblioMantenimientoManager.UpdateAndDeleteTablaDetalleBiblioteca(obj);
    		//log.info("Resultado: "+resultado);	
    		return resultado;
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    public void LlenarDatos(ConsultaDeweyCommand control, HttpServletRequest request){
    	
    	 TipoTablaDetalle obj2= new TipoTablaDetalle();
		 obj2.setDscValor3("");
		 obj2.setCodTipoTabla("");
		 obj2.setCodTipoTablaDetalle(CommonConstants.TIPT_CATEGORIA_DEWEY);
	     obj2.setCodDetalle("");
		 obj2.setDescripcion("");
		 obj2.setTipo(CommonConstants.TIPO_ORDEN_DSC);
         control.setCodListaCategoria(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj2));
   	
    }
    
    public void LlenarBandeja(ConsultaDeweyCommand control, HttpServletRequest request)
    {  
    	TipoTablaDetalle obj1= new TipoTablaDetalle();
	   
    	obj1.setDscValor3("");
    	obj1.setCodTipoTabla(control.getCodCategoria());
    	obj1.setCodTipoTablaDetalle(CommonConstants.TIPT_CLASIFICACION_DEWEY);
    	obj1.setCodDetalle("");
    	obj1.setDescripcion(control.getDescripcionDewey());
    	obj1.setTipo(CommonConstants.TIPO_ORDEN_COD);
	
    	control.setListaDewey(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj1));
    	request.getSession().setAttribute("listaDewey", control.getListaDewey());
    	
    }
    
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
    
}
