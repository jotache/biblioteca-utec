package com.tecsup.SGA.web.biblioteca.controller;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
//import java.util.ArrayList;
import java.util.List;




import java.util.Map;


//import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.BuzonSugerencia;
//import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.AdminBuzonSugerenciasManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.biblioteca.command.PortalBusquedaInicioCommand;

import edu.utecsup.main.Sender;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.google.api.services.plus.model.Person.Emails;
import com.google.gson.Gson;

public class PortalBusquedaInicioFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PortalBusquedaInicioFormController.class);
 	
	private BiblioMantenimientoManager biblioMantenimientoManager;
 	private PortalWebBibliotecaManager portalWebBibliotecaManager;
 	private AdminBuzonSugerenciasManager adminBuzonSugerenciasManager;
 	private SeguridadManager seguridadManager;
 	
 	private String code;
	
 	private String object;
	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public void setAdminBuzonSugerenciasManager(
			AdminBuzonSugerenciasManager adminBuzonSugerenciasManager) {
		this.adminBuzonSugerenciasManager = adminBuzonSugerenciasManager;
	}
	
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		PortalBusquedaInicioCommand command = new PortalBusquedaInicioCommand();
		
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
		
		if ( usuarioSeguridad != null )
    	{			
			command.setCodEvaluador(usuarioSeguridad.getIdUsuario());
    		//command.setPerfil(usuarioSeguridad.getPerfiles());
    	}
				
    	LlenarComboTipoMaterial(command);
    	LlenarOrdenarAndBuscarBy(command);    	
    	LlenarComboTipoSugerencia(command);    	

        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
	
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
          Object command,
          BindException errors)
		throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}
	
	
	private HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private JsonFactory JSON_FACTORY = new JacksonFactory();
	
	
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
		BindException errors)
		throws Exception {

		ModelMap model = new ModelMap();
		
		PortalBusquedaInicioCommand control = (PortalBusquedaInicioCommand) command;
		//***************************************************
		log.info("OPERACION:" + control.getOperacion());		

		String resultado="";
		if(control.getOperacion().trim().equals("GOOGLE_CONNECT")){
						
			log.info("onSubmit: Google_Connect()");
			log.info("gingreso() code:" + (String)request.getParameter("code"));
			this.code = (String)request.getParameter("code");
			
			try{
								
				GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(HTTP_TRANSPORT, JSON_FACTORY, CommonConstants.GOOGLEPLUS_CLIENT_ID, CommonConstants.GOOGLEPLUS_CLIENT_SECRET, code, "postmessage").execute();
				
				log.info("Token: " + tokenResponse.toString());
			    
			    // Crea una representación credencial de los datos del token.
			    GoogleCredential credential = new GoogleCredential.Builder()
			        .setJsonFactory(JSON_FACTORY)
			        .setTransport(HTTP_TRANSPORT)
			        .setClientSecrets(CommonConstants.GOOGLEPLUS_CLIENT_ID, CommonConstants.GOOGLEPLUS_CLIENT_SECRET).build()
			        .setFromTokenResponse(tokenResponse);
			    
			    Plus plus = new Plus.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).build();
			    
			    Person profile = plus.people().get("me").execute();
			    
			    log.info("ID: " + profile.getId());
			    log.info("Name: " + profile.getDisplayName());
			    log.info("Image URL: " + profile.getImage().getUrl());
			    log.info("Profile URL: " + profile.getUrl());
			    
			    String principalEmail = null;
				for (Emails email : profile.getEmails()) {
					log.info("Email: " +email.getValue() + " - " + email.toPrettyString());
					principalEmail = email.getValue();
				}
				
				String usuario = principalEmail.substring(0, principalEmail.indexOf("@")).toLowerCase();
				String dominio = principalEmail.substring(principalEmail.indexOf("@")+1).toLowerCase();
				
				log.info("dominio: " + dominio);
				
				String[] allowDomains = {"utec.edu.pe"};
				if (Arrays.asList(allowDomains).contains(dominio)){
					log.info("Cuenta " + usuario + " pertenece a la lista blanca " + Arrays.toString(allowDomains));
					
					List lstResultado = null;
					String verificarUsuario="";
					UsuarioSeguridad usuarioSeguridad = null;
//					if(dominio.equals("utec.edu.pe")){
//						lstResultado = seguridadManager.getOpcionesCeditec(usuario, CommonConstants.SGA_BIB);
//						if(lstResultado.size()>0){
//							usuarioSeguridad = this.seguridadManager.getDatosUsuarioBiblioUTEC(usuario);	
//						}
//					
//					}else{ 
						lstResultado = seguridadManager.getOpciones(usuario, CommonConstants.SGA_BIB);
						if(lstResultado.size()>0){
							usuarioSeguridad = this.seguridadManager.getDatosUsuario(usuario);
						}
//					}
											
					if (usuarioSeguridad==null) usuarioSeguridad = new UsuarioSeguridad();
    				
    				usuarioSeguridad.setOpciones(convierte(lstResultado));
    				usuarioSeguridad.setCodUsuario(usuario.toLowerCase());
    				
    				String sedeUsuarioBiblio = portalWebBibliotecaManager.sedeUsuario(usuarioSeguridad.getCodUsuario());    				
    				if (sedeUsuarioBiblio==null) sedeUsuarioBiblio="";    				
    				usuarioSeguridad.setSede(sedeUsuarioBiblio);
    				
    				verificarUsuario = portalWebBibliotecaManager.GetVerificarReservaUsuario(usuarioSeguridad.getCodUsuario());
    				
    				usuarioSeguridad.setUsuarioSeguriBib(
    						this.seguridadManager.obtenerUsuarioSeguriBiblio(
    								usuarioSeguridad.getIdUsuario(),
    								usuarioSeguridad.getCodUsuario()));
    				
    				log.info("Usuario Bib:"+usuarioSeguridad.getUsuarioSeguriBib());
    				if(usuarioSeguridad.getUsuarioSeguriBib()!=null){
    					request.getSession().setAttribute("verificarUsuario", verificarUsuario);
            			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);            			
            			request.getSession().setAttribute("usuarioClave",usuario.toLowerCase());
            			request.getSession().removeAttribute("reingreso");
        				request.getSession().removeAttribute("contextoIngreso");
        				request.getSession().setAttribute("laboratoria601",false);	
    				}
    				    				
    				//registrar log
					String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
					seguridadManager.RegistrarSesion(true,estacioncliente, usuario.trim(),CommonConstants.SGA_BIB);
					
					this.object = "Ceditec";
										
					
				}else{
					log.info("Cuenta " + usuario + " NO pertenece a la lista blanca " + Arrays.toString(allowDomains));
					//send error message
				}
							
				
			}catch (Throwable e) {
				log.error(e);
				throw new Exception(e);
			}
			
			
			
		}else if (control.getOperacion().trim().equals("ENVIAR")){
    		resultado = EnviarSugerencia(control);
    		log.info("RPTA:" + resultado);
			//resultado = "0";    		
    		//******************************************
    		if (resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{    			
    			control.setMsg("OK");
    		}    
    	}else if (control.getOperacion().equals("INGRESAR")){
    		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    		String usuario = control.getUsuario();
    		String clave = control.getClave();
    		String verificarUsuario="";
    		String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
    		List lstResultado = null;
    		if (usuario!=null && clave!= null){
    			usuarioSeguridad = null;    			
    			int i = seguridadManager.getValidaUsuario(usuario, clave);
    			
//    			if( SeguridadBean.esAdministrativo(request.getRemoteAddr(),control.getClave()) ) i = 0; //-- Acceso libre
    			
    			log.info("RPTA:" + i);   		
        		if(i==0){
        			        			
//        			lstResultado = seguridadManager.getOpcionesCeditec(usuario, CommonConstants.SGA_BIB);        					
        			lstResultado = seguridadManager.getOpciones(usuario, CommonConstants.SGA_BIB);
    		        seguridadManager.RegistrarSesion(true,estacioncliente, usuario.trim(),CommonConstants.SGA_BIB);        			
        			if(lstResultado.size()>0){
        				//Obtenmos datos del usuario
//        				usuarioSeguridad = this.seguridadManager.getDatosUsuarioBiblioUTEC(usuario);
        				usuarioSeguridad = this.seguridadManager.getDatosUsuarioBiblio(usuario);
        				
        				if (usuarioSeguridad==null) usuarioSeguridad = new UsuarioSeguridad();
        				
        				usuarioSeguridad.setOpciones(convierte(lstResultado));
        				usuarioSeguridad.setCodUsuario(usuario.toLowerCase());
        				        				        				
        				String sedeUsuarioBiblio = portalWebBibliotecaManager.sedeUsuario(usuarioSeguridad.getCodUsuario());
        				
        				if (sedeUsuarioBiblio==null) sedeUsuarioBiblio="";
        				
        				usuarioSeguridad.setSede(sedeUsuarioBiblio);
        				        				
        				//lista=portalWebBibliotecaManager.GetVerificarReservaUsuario(usuarioSeguridad.getIdUsuario());
        				verificarUsuario = portalWebBibliotecaManager.GetVerificarReservaUsuario(usuarioSeguridad.getCodUsuario()); 
        				        				
        				//JHPR 2008-06-08 Añadir objeto para ingreso a biblioteca virtual.
        				usuarioSeguridad.setUsuarioSeguriBib(
        						this.seguridadManager.obtenerUsuarioSeguriBiblio(
        								usuarioSeguridad.getIdUsuario(),
        								usuarioSeguridad.getCodUsuario()));
        				boolean laboratoria601 = false;
        				
        				log.info("Usuario Bib:"+usuarioSeguridad.getUsuarioSeguriBib());
        				
        				if(usuarioSeguridad.getUsuarioSeguriBib()!=null){
        					if (usuarioSeguridad.getCiclo() !=null && usuarioSeguridad.getCodigoEspecialidad() != null ){
            					laboratoria601 = (usuarioSeguridad.getCiclo().equals("3") 
            										|| usuarioSeguridad.getCiclo().equals("4") 
            										|| usuarioSeguridad.getCiclo().equals("5") 
            										|| usuarioSeguridad.getCiclo().equals("6")) 
            									&& usuarioSeguridad.getCodigoEspecialidad().equals("11");
            				}
            				request.getSession().setAttribute("verificarUsuario", verificarUsuario);
                			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);            			
                			request.getSession().setAttribute("usuarioClave",usuario.toLowerCase());
                			request.getSession().removeAttribute("reingreso");
            				request.getSession().removeAttribute("contextoIngreso");
            				request.getSession().setAttribute("laboratoria601",laboratoria601 );
                			        			
            				request.setAttribute("mensaje","OK");
            				control.setLog("1");    
        				}else{
        					request.setAttribute("mensaje","-8");        					
        				}
        				    				
        				
        			} else {
        				request.setAttribute("mensaje",i);
        				control.setLog("0");
        			}
        		}else{
        			if (i==-6) {
        				//request.setAttribute("usuarioClave",usuario.toLowerCase());
        				request.getSession().setAttribute("usuarioClave", usuario.toLowerCase());
    					request.getSession().setAttribute("contextoIngreso", "Si");
    					request.setAttribute("mensaje","-6");
    					    					
//        			}else if (i>0){
//        				lstResultado = seguridadManager.getOpciones(usuario, CommonConstants.SGA_BIB);        					
//        		        seguridadManager.RegistrarSesion(true,estacioncliente, usuario.trim(),CommonConstants.SGA_BIB);            			
//            			
        			}else{
        				log.info("Enviando mensaje:"+i);
        				request.setAttribute("mensaje",i);
        			}
        		}
        		
        		return new ModelAndView("/biblioteca/userWeb/bib_busqueda_inicio", "control", control);
    		}
    	}
				
		//return new ModelAndView("/biblioteca/userWeb/bib_sugerencia","control",control);
		return new ModelAndView("/biblioteca/userWeb/bib_busqueda_inicio", "control", control);
	}
	
	public void LlenarComboTipoMaterial(PortalBusquedaInicioCommand control){
    	
	   	 TipoTablaDetalle obj= new TipoTablaDetalle();
		        obj.setDscValor3("");
				obj.setCodTipoTabla("");
				obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
				obj.setCodDetalle("");
				obj.setDescripcion("");
				obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
				control.setCodListaTipoMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
					   		   	
	}
	
	public void LlenarOrdenarAndBuscarBy(PortalBusquedaInicioCommand control){
    	TipoTablaDetalle obj = new TipoTablaDetalle();
    	obj.setDscValor3("");    	
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_BUSQUEDA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setCodListaOrdenarBy(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));		
        control.setCodListaBuscarBy(control.getCodListaOrdenarBy());
	}
	
	public void LlenarComboTipoSugerencia(PortalBusquedaInicioCommand control){
    	
	   	TipoTablaDetalle obj= new TipoTablaDetalle();
	   	
        obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SUGERENCIA);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
		control.setCodListaSugerencia(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		//INICIALIZO LAS CONTANTES DEL TIPO DE SUGERENCIA
		control.setCodTipoComentario(CommonConstants.COD_COM);
		control.setCodTipoConsulta(CommonConstants.COD_CON);
		control.setCodTipoSugerencia(CommonConstants.COD_SUG);
		control.setCodTipoReclamo(CommonConstants.COD_REC);
		//************************************************
	}
	
	private String EnviarSugerencia(PortalBusquedaInicioCommand control){
		try{
    		BuzonSugerencia obj = new BuzonSugerencia();			
    		String resultado="";
    		
    		obj.setCodTipoSugerencia(control.getCodSugerencia());
    		
    		String rpta="";
    		
    		   if (control.getTxtSugerencia().trim().length()>1000)
    			   rpta = control.getTxtSugerencia().trim().substring(0, 1000);
    		   else
    			   rpta = control.getTxtSugerencia().trim();    		
    		
    		//obj.setDescripcion(control.getTxtSugerencia());    		   
    		obj.setDescripcion(rpta);
    		obj.setEmail(control.getTxtMail());    		
    		obj.setUsuario((control.getCodEvaluador()==null || control.getCodEvaluador().equalsIgnoreCase(""))?"999999999":control.getCodEvaluador());
    		
    		resultado=this.adminBuzonSugerenciasManager.InsertSugerencia(obj);
    		
    		//Enviar la sugerencia por correo electrónico. biblioteca@tecsup.edu.pe
    		Sender senderMail = new Sender();
    		String asunto = "[SGA CEDITEC] Alerta de Registro de Sugerencia";
    		String contenido = "";
    		String tipoSugerencia = "";
    		StringBuilder bodyHTML = new StringBuilder();
    		
//    		Tipo:
//    			0001 	0075	Consulta
//    			0002 	0075	Comentario
//    			0003 	0075	Sugerencia
//    			0004 	0075	Reclamo
    		
    		if(obj.getCodTipoSugerencia().equals("0001")) tipoSugerencia = "Consulta";
    		if(obj.getCodTipoSugerencia().equals("0002")) tipoSugerencia = "Comentario";
    		if(obj.getCodTipoSugerencia().equals("0003")) tipoSugerencia = "Sugerencia";
    		if(obj.getCodTipoSugerencia().equals("0004")) tipoSugerencia = "Reclamo";
    		
    		bodyHTML.append("<table width=650 border=0 cellpadding=14 align=center>");
    		bodyHTML.append("<tr><td style='background-color:#000; color:#FFF; font-weight:bold' colspan='2'>Biblioteca - Tecsup</td></tr>");
    		bodyHTML.append("<tr><td colspan='2'><p>Se ha registrado la siguiente sugerencia:</p>");
    		bodyHTML.append("<p>" + obj.getDescripcion() + "</p>");
    		bodyHTML.append("</td></tr>");
    		bodyHTML.append("<tr><td style='width:15%'>Tipo:&nbsp;</td>");
    		bodyHTML.append("<td style='width:85%'>" + tipoSugerencia + "</td></tr>");
//    		bodyHTML.append("<tr><td>Usuario:&nbsp;</td><td>USUARIO</td></tr>");
    		bodyHTML.append("<tr><td>Correo:&nbsp;</td><td>" + (obj.getEmail()==null?"(Sin correo)":obj.getEmail()) + "</td></tr></table>");
    		  		
    		
    		contenido = bodyHTML.toString();
    		
    		senderMail.sendMail(true, "biblioteca@tecsup.edu.pe", "Biblioteca Tecsup", null, null, asunto, 
    				contenido, null, null);
    		    		
    		return resultado;			 		
    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
	}

	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}
}
