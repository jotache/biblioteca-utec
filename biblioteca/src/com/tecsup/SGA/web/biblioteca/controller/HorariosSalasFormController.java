package com.tecsup.SGA.web.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

//import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;
//import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.web.biblioteca.command.HorarioSalasCommand;

public class HorariosSalasFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(HorariosSalasFormController.class);
	BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
			
	protected Object formBackingObject(HttpServletRequest request)  throws ServletException {
		HorarioSalasCommand command = new HorarioSalasCommand();
		if(request.getParameter("txhCodUsuario")!=null)
			command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		
		if(request.getParameter("txhSedeSelec")!=null)
			command.setSede((String)request.getParameter("txhSedeSelec"));
		
		log.info(command.getOperacion());
		if (command.getOperacion()==null && command.getSede()!=null){	
			List<Sala> listaTipoSalas = new ArrayList<Sala>();
			//fill lista
			listaTipoSalas = biblioMantenimientoManager.listaHorarioSalas(command.getSede().equals("U")?"U":"T");
			request.setAttribute("listaTipoSalas", listaTipoSalas);
			command.setTamanioLista(String.valueOf(listaTipoSalas.size()));
					
			request.setAttribute("listaHoras",biblioMantenimientoManager.listaHorasBiblioteca(command.getSede()));
		}
		return command;
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, 
			Object command,
            BindException errors)
    		throws Exception {
		
		HorarioSalasCommand control = (HorarioSalasCommand) command;
		log.info("OPERACION:" + control.getOperacion());
		if (control.getOperacion().trim().equals("GRABAR")){
			String resultado = graba(control);
			log.info("Rpta: " + resultado);
			
			if ( resultado.equals("-1"))
    			control.setMsg("ERROR");    		
    		else {
    			
    			List<Sala> listaTipoSalas = new ArrayList<Sala>();    			
    			listaTipoSalas = biblioMantenimientoManager.listaHorarioSalas(control.getSede().equals("U")?"U":"T");
    			request.setAttribute("listaTipoSalas", listaTipoSalas);
    			control.setTamanioLista(String.valueOf(listaTipoSalas.size()));    				
    			request.setAttribute("listaHoras",biblioMantenimientoManager.listaHorasBiblioteca(control.getSede()));
    			control.setMsg("OK");
    		}
						
		}
		
		return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_horario_salas", "control", control);
	}
	
	private String graba(HorarioSalasCommand control){
		
		return this.biblioMantenimientoManager.UpdateHorarioSalas(
				control.getCadenaDatos(), 
				control.getCodUsuario(), 
				control.getTamanioLista());
	}
}
