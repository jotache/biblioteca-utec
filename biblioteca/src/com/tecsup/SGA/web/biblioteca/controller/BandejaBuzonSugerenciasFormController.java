package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.*;
import com.tecsup.SGA.web.evaluaciones.controller.RelacionarProductoHibridoFormController;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Item;
import com.tecsup.SGA.modelo.BuzonSugerencia;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Meses;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.biblioteca.AdminBuzonSugerenciasManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class BandejaBuzonSugerenciasFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(BandejaBuzonSugerenciasFormController.class);
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private AdminBuzonSugerenciasManager adminBuzonSugerenciasManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		//log.info("formBackingObject:INI");
		
		BandejaBuzonSugerenciasCommand command = new BandejaBuzonSugerenciasCommand();						
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodUsuario"));
		command.setOperacion((String)request.getParameter("txhOperacion"));
    	command.setIdRec((String)request.getParameter("txhIdRec"));    	
    	command.setLstrURL("?txhIdRec=" + command.getIdRec() + "&txhOperacion=" + command.getOperacion());    	
    	request.setAttribute("lstrURL", command.getLstrURL());
    	
    	LlenarComboEstado(command);
    	LlenarComboTipoSugerencia(command);
    	
    	Anios anios= new Anios();
    	
    	command.setCodListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
    	command.setCodListaMeses(anios.getMeses());
    	command.setPeriodoActual(CommonConstants.TIPT_PERIODO_ACTUAL);
    	
    	//log.info("formBackingObject:FIN: TamaņoListaCuidades");//+command.getListaCiudades().size());
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	     	    	
    	BandejaBuzonSugerenciasCommand control = (BandejaBuzonSugerenciasCommand)command;    		    	    	
    	log.info("OPERACION:"+control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{   LlenarSugerencias(control, request);
    		//log.info("BUSCAR");
    	}
    	else{if(control.getOperacion().trim().equals("LIMPIAR")){
    		    LimpiarLista(control, request); 
    	       }
    		
    	}
    	//control.setOperacion("");
    	//log.info("onSubmit:FIN");
    	return new ModelAndView("/biblioteca/userAdmin/buzonSugerencias/bib_bandeja_buzon_sugerencias","control",control);		
    }
    
    public void LlenarSugerencias(BandejaBuzonSugerenciasCommand control ,HttpServletRequest request){
    	
    	BuzonSugerencia buzonSugerencia= new BuzonSugerencia();
    	if(control.getCodTipoSugerencia().equals("-1"))
    		control.setCodTipoSugerencia("");
    	if(control.getCodEstado().equals("-1"))
    		control.setCodEstado("");
    	
    	buzonSugerencia.setAnio(control.getAnio());
    	buzonSugerencia.setMes(control.getMes());
    	buzonSugerencia.setCodTipoSugerencia(control.getCodTipoSugerencia());
    	buzonSugerencia.setEstadoSugerencia(control.getCodEstado());
    	//log.info("Aņo: "+control.getAnio());
    	//log.info("Mes: "+control.getMes());
    	//log.info("Codigo Tipo Sugerencia: "+control.getCodTipoSugerencia());
    	//log.info("Codigo de Estado: "+control.getCodEstado());
    	//log.info(control.getAnio());
    	control.setListaSugerencia(this.adminBuzonSugerenciasManager.GetAllSugerencias(buzonSugerencia));
    	request.getSession().setAttribute("listaSugerencias", control.getListaSugerencia());
    }
    
    
    public void LlenarCiudad(BandejaBuzonSugerenciasCommand control ,HttpServletRequest request){
    	
    	Ciudad ciudad= new Ciudad();
    	ciudad.setPaisId("1");
    	//ciudad.setEstReg("1");
    	ciudad.setCiudadDescripcion("");
    	ciudad.setCiudadCodigo(""); 
    	control.setListaCiudades(this.biblioMantenimientoManager.getCiudadByPais(ciudad, CommonConstants.TIPO_ORDEN_DSC));
    	//control.setListaTipoMateriales(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		request.getSession().setAttribute("listaCiudades", control.getListaCiudades());
    	//obj.getCiudadCodigo(),		obj.getCiudadDescripcion()
    }

    public void LlenarComboTipoSugerencia(BandejaBuzonSugerenciasCommand control){
    	 
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	        obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_SUGERENCIA);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setCodListaTipoSugerencia(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    }
    public void LlenarComboEstado(BandejaBuzonSugerenciasCommand control){
    	 
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	        obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_ESTADO_SUGERENCIA);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setCodListaEstado(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    }
    
    public void LimpiarLista(BandejaBuzonSugerenciasCommand control, HttpServletRequest request){
    	List listaVacia= new ArrayList();
    	//log.info("Dentro del Limpiar Lista xD");
    	request.getSession().setAttribute("listaSugerencias", listaVacia);
    }
    
	public BiblioMantenimientoManager getBiblioMantenimientoManager() {
		return biblioMantenimientoManager;
	}

	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}

	public AdminBuzonSugerenciasManager getAdminBuzonSugerenciasManager() {
		return adminBuzonSugerenciasManager;
	}

	public void setAdminBuzonSugerenciasManager(
			AdminBuzonSugerenciasManager adminBuzonSugerenciasManager) {
		this.adminBuzonSugerenciasManager = adminBuzonSugerenciasManager;
	}
}
