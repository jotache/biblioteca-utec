package com.tecsup.SGA.web.biblioteca.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;

public class BandejaUsuarioWebController  implements Controller  {
	
	private static Log log = LogFactory.getLog(BandejaUsuarioWebController.class);


	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	//log.info("handleRequest:INI");		    	    	
    	ModelMap model = new ModelMap();   	
    	
    	
	   	//log.info("handleRequest:FIN");
		return new ModelAndView("/biblioteca/userWeb/bandejaUsuario", "model", model);
	}



}
