package com.tecsup.SGA.web.biblioteca.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;

public class AdminGestionMaterialHistorialPrestamosIframe  implements Controller  {
	
	private static Log log = LogFactory.getLog(AdminGestionMaterialHistorialPrestamosIframe.class);
	
	BiblioGestionMaterialManager biblioGestionMaterialManager;
	
    public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	//log.info("handleRequest:INI");		    	    	
    	ModelMap model = new ModelMap();
    	
    	String txhCodigoUnico = request.getParameter("txhCodigoUnico");
    	//log.info("txhCodigoUnico>"+txhCodigoUnico+">");
    	
    	request.getSession().
    	setAttribute("SS_HISTORIAL",biblioGestionMaterialManager.getAllHistorialPrestamoxMaterial(txhCodigoUnico));
    	
    	model.addObject("txhCodigoUnico", txhCodigoUnico);   	
    	
	   	//log.info("handleRequest:FIN");
		return new ModelAndView("/biblioteca/userAdmin/gestionMaterial/biblio_admin_gestion_material_historial_iframe", "model", model);
	}



}
