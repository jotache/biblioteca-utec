package com.tecsup.SGA.web.biblioteca.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;



import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.biblioteca.command.SeccionesAdjuntarImagenCommand;
import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.web.reclutamiento.command.AdjuntarCVCommand;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota.InsertAdjuntoCasoCat;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.evaluaciones.*;

//
import org.springframework.util.FileCopyUtils;
import java.io.File;
import java.io.FilenameFilter;

public class SeccionesAdjuntarImagenFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(SeccionesAdjuntarImagenFormController.class);	
	
	
	public SeccionesAdjuntarImagenFormController(){
		super();
		setCommandClass(SeccionesAdjuntarImagenCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	    	
    	SeccionesAdjuntarImagenCommand command = new SeccionesAdjuntarImagenCommand();
    	
    	request.setAttribute("strExtensionGIF", CommonConstants.GSTR_EXTENSION_GIF);
    	
    	String codPeriodo =(String)request.getParameter("txhCodPeriodo");
    	String codProducto =(String)request.getParameter("codProducto");
    	String codCiclo =(String)request.getParameter("codCiclo");
    	String codEvaluador =(String)request.getParameter("txhCodEvaluador");
    	
    	command.setCodPeriodo(codPeriodo);
    	command.setCodProducto(codProducto);
    	command.setCodCiclo(codCiclo);
    	command.setCodEvaluador(codEvaluador);
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {        
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	    	
    	SeccionesAdjuntarImagenCommand control = (SeccionesAdjuntarImagenCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta;    		
    		byte[] bytesArchivo = control.getTxtArchivo();
    		
    		String fileNameArchivo="";
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		
    		String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA  + CommonConstants.STR_RUTA_MATERIAL_BIB;
    		//String uploadDirArchivo = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_MATERIAL_BIB;            
            String sep = System.getProperty("file.separator");
            
	        if (bytesArchivo.length>0){
	        	 //fileNameArchivo = CommonConstants.FILE_NAME_DOC+control.getCodPeriodo()+control.getCodProducto()+control.getCodCiclo()+"."+control.getExtArchivo();
	        	 fileNameArchivo = control.getNomArchivo();
	        	 File uploadedFileArchivo = new File(uploadDirArchivo + sep + fileNameArchivo);
	        	 FileCopyUtils.copy(bytesArchivo, uploadedFileArchivo);
	        	 control.setNomNuevoArchivo(fileNameArchivo);
	        	 
	        	 rpta=InsertAdjuntoCasoCat(control);	        	 
	        	 log.info("RPTA:" + rpta);
	        	 if("-1".equals(rpta)){
	        			control.setMsg("ERROR");
	        	 }
	        		else{
	        			control.setMsg("OK");
	        	 }
	        }
	        else{	        	

	        }
    	}
		
		return new ModelAndView("/biblioteca/userAdmin/mantenimiento/bib_secciones_adjuntar_imagen","control",control);	    
    }
    private String InsertAdjuntoCasoCat(SeccionesAdjuntarImagenCommand control){
    	try{
    		Archivo obj= new Archivo();
    		
    		obj.setPeriodo(control.getCodPeriodo());
    		obj.setProducto(control.getCodProducto());
    		obj.setCiclo(control.getCodCiclo());
    		obj.setNombreNuevo(control.getNomNuevoArchivo());
    		obj.setNombre(control.getNomArchivo());
    		
    		return obj.getEstado();
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }   
}
