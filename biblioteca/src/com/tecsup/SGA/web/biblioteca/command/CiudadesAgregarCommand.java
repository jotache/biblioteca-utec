package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class CiudadesAgregarCommand {
	private String codUsuario;
	private String operacion;	
    private String msg;
    private String codigo;
    private String dscCiudad;
    //**************************
    private String codigoSec;
    
    private String pais;
    private List listaPais;

	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDscCiudad() {
		return dscCiudad;
	}
	public void setDscCiudad(String dscCiudad) {
		this.dscCiudad = dscCiudad;
	}
	public List getListaPais() {
		return listaPais;
	}
	public void setListaPais(List listaPais) {
		this.listaPais = listaPais;
	}
	public String getCodigoSec() {
		return codigoSec;
	}
	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
}