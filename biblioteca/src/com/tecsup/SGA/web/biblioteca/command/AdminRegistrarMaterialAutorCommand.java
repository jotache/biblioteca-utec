package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminRegistrarMaterialAutorCommand {
	private String operacion;
	private String txtCodigo;
	private String txtAutores;
	private List lstResultado;
	
	private String txhCodUsuario;
	
	private String codPersona;
	private String codEmpresa;	
	private String codTipo;
	
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}	
	public String getCodPersona() {
		return codPersona;
	}
	public void setCodPersona(String codPersona) {
		this.codPersona = codPersona;
	}
	public String getCodEmpresa() {
		return codEmpresa;
	}
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTxtCodigo() {
		return txtCodigo;
	}
	public void setTxtCodigo(String txtCodigo) {
		this.txtCodigo = txtCodigo;
	}
	public String getTxtAutores() {
		return txtAutores;
	}
	public void setTxtAutores(String txtAutores) {
		this.txtAutores = txtAutores;
	}
	
	public String getTxhCodUsuario() {
		return txhCodUsuario;
	}
	public void setTxhCodUsuario(String txhCodUsuario) {
		this.txhCodUsuario = txhCodUsuario;
	}
 
	
	
}
