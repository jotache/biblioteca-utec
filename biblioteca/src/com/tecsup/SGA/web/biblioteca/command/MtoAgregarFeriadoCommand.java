package com.tecsup.SGA.web.biblioteca.command;
import java.util.List;
public class MtoAgregarFeriadoCommand {

	private String operacion;
	private String msg;
	private String idRec;
	private String lstrURL;
	private String descripcion;
	private String ano;
	private String mes;
	private String dia;
	private String graba;
	private String codEval;
	private String cod;
	private String flag;
	private List codListaMeses;
	private List codListaAnios;
	private String codUsuario;
	private String sede;
	
	private String txtFecha;
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public List getCodListaMeses() {
		return codListaMeses;
	}
	public void setCodListaMeses(List codListaMeses) {
		this.codListaMeses = codListaMeses;
	}
	public List getCodListaAnios() {
		return codListaAnios;
	}
	public void setCodListaAnios(List codListaAnios) {
		this.codListaAnios = codListaAnios;
	}
	
	public String getTxtFecha() {
		return txtFecha;
	}
	public void setTxtFecha(String txtFecha) {
		this.txtFecha = txtFecha;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}

}
