package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminRegistrarMaterialCommand {
	
private String accion;//accion : registrar o actualizar material	
private String operacion;
private String txtCodigo;
private String cboTipoMaterial;
private List lstTipoMaterial;
private String txtTitulo;
private String txtAutor;
private String txtDewey;
private String txtNroPag;
private String txtNroVolumen;
private String txtTotalVolumen;
private String cboEditorial;
private List lstEditorial;
private String txtNroEdicion;
private String cboPais;
private List lstPais;
private String cboCiudad;
private List lstCiudad;
private String txtFecPublicacion;
private String txtUrlArchivoDig;
private String cboIdioma;
private List lstIdioma;
private String txtIsbn;
private String txtFormato;
private String cboTipoVideo;
private List lstTipoVideo;
private String cboProcedencia;
private List lstProcedencia;
private String cboPrecio;
private List lstPrecio;
private String txtPrecio;
private String chkTipoPrestamoSala;
private String chkTipoPrestamoDomicilio;
private String rdoAplicaReserva;
private String txtObservaciones;
private String chk1;
private String chk2;
private String nomImagen;
private String pathImagen;
private String txhCodigoAutor;
private String txhCodigoDewey;
private String txhCodUnico;
private List lstRegistroIngreso;
private List lstDescriptores;
private List lstDocumentosRelacionados;
private String txhImagen;
private String usuario;
private String txtUbicacionFisica;
private String txtContenido;

private String txhDesEditorial;
private String txhDesIdioma;
private String txhDesPais;
private String txhDesCiudad;

private String cboSede;
private List lstSede;

private String txtPrefijo;
private String txtSufijo;
private String txtIdDewey;
private String txtFrecuencia;

private String txhCodGeneradoDewey;
private String mencionSerie;
private List lstAnio;


//PARAEMTROS DE BUSQUEDA DE LA PANTALLA CONUSLTA DE PMATERIAL BIBLIOGRAFICO
private String prmTxtCodigo;
private String prmTxtNroIngreso;
private String prmCboTipoMaterial;
private String prmCboBuscarPor;
private String prmTxtTitulo;
private String prmCboIdioma;
private String prmCboAnioIni;
private String prmCboAnioFin;
private String prmTxtFechaReservaIni;
private String prmTxtFechaReservaFin;

private String sedeSel;
private String sedeUsuario;

/*Flag para ver la ficha detalle material / saber de qu� p�g es invocada*/
private String flgView;
private String indISBN;

public String getTxtContenido() {
	return txtContenido;
}
public void setTxtContenido(String txtContenido) {
	this.txtContenido = txtContenido;
}
public String getTxtUbicacionFisica() {
	return txtUbicacionFisica;
}
public void setTxtUbicacionFisica(String txtUbicacionFisica) {
	this.txtUbicacionFisica = txtUbicacionFisica;
}
public String getTxhImagen() {
	return txhImagen;
}
public void setTxhImagen(String txhImagen) {
	this.txhImagen = txhImagen;
}
public List getLstDocumentosRelacionados() {
	return lstDocumentosRelacionados;
}
public void setLstDocumentosRelacionados(List lstDocumentosRelacionados) {
	this.lstDocumentosRelacionados = lstDocumentosRelacionados;
}
public List getLstDescriptores() {
	return lstDescriptores;
}
public void setLstDescriptores(List lstDescriptores) {
	this.lstDescriptores = lstDescriptores;
}
public List getLstRegistroIngreso() {
	return lstRegistroIngreso;
}
public void setLstRegistroIngreso(List lstRegistroIngreso) {
	this.lstRegistroIngreso = lstRegistroIngreso;
}
public String getTxhCodigoDewey() {
	return txhCodigoDewey;
}
public void setTxhCodigoDewey(String txhCodigoDewey) {
	this.txhCodigoDewey = txhCodigoDewey;
}
public String getTxhCodigoAutor() {
	return txhCodigoAutor;
}
public void setTxhCodigoAutor(String txhCodigoAutor) {
	this.txhCodigoAutor = txhCodigoAutor;
}
public String getNomImagen() {
	return nomImagen;
}
public void setNomImagen(String nomImagen) {
	this.nomImagen = nomImagen;
}
public String getChk1() {
	return chk1;
}
public void setChk1(String chk1) {
	this.chk1 = chk1;
}
public String getChk2() {
	return chk2;
}
public void setChk2(String chk2) {
	this.chk2 = chk2;
}
public String getOperacion() {
	return operacion;
}
public void setOperacion(String operacion) {
	this.operacion = operacion;
}
public String getTxtCodigo() {
	return txtCodigo;
}
public void setTxtCodigo(String txtCodigo) {
	this.txtCodigo = txtCodigo;
}
public String getCboTipoMaterial() {
	return cboTipoMaterial;
}
public void setCboTipoMaterial(String cboTipoMaterial) {
	this.cboTipoMaterial = cboTipoMaterial;
}
public List getLstTipoMaterial() {
	return lstTipoMaterial;
}
public void setLstTipoMaterial(List lstTipoMaterial) {
	this.lstTipoMaterial = lstTipoMaterial;
}
public String getTxtTitulo() {
	return txtTitulo;
}
public void setTxtTitulo(String txtTitulo) {
	this.txtTitulo = txtTitulo;
}
public String getTxtAutor() {
	return txtAutor;
}
public void setTxtAutor(String txtAutor) {
	this.txtAutor = txtAutor;
}
public String getTxtDewey() {
	return txtDewey;
}
public void setTxtDewey(String txtDewey) {
	this.txtDewey = txtDewey;
}
public String getTxtNroPag() {
	return txtNroPag;
}
public void setTxtNroPag(String txtNroPag) {
	this.txtNroPag = txtNroPag;
}
public String getTxtNroVolumen() {
	return txtNroVolumen;
}
public void setTxtNroVolumen(String txtNroVolumen) {
	this.txtNroVolumen = txtNroVolumen;
}
public String getTxtTotalVolumen() {
	return txtTotalVolumen;
}
public void setTxtTotalVolumen(String txtTotalVolumen) {
	this.txtTotalVolumen = txtTotalVolumen;
}
public String getCboEditorial() {
	return cboEditorial;
}
public void setCboEditorial(String cboEditorial) {
	this.cboEditorial = cboEditorial;
}
public List getLstEditorial() {
	return lstEditorial;
}
public void setLstEditorial(List lstEditorial) {
	this.lstEditorial = lstEditorial;
}
public String getTxtNroEdicion() {
	return txtNroEdicion;
}
public void setTxtNroEdicion(String txtNroEdicion) {
	this.txtNroEdicion = txtNroEdicion;
}
public String getCboPais() {
	return cboPais;
}
public void setCboPais(String cboPais) {
	this.cboPais = cboPais;
}
public List getLstPais() {
	return lstPais;
}
public void setLstPais(List lstPais) {
	this.lstPais = lstPais;
}
public String getCboCiudad() {
	return cboCiudad;
}
public void setCboCiudad(String cboCiudad) {
	this.cboCiudad = cboCiudad;
}
public List getLstCiudad() {
	return lstCiudad;
}
public void setLstCiudad(List lstCiudad) {
	this.lstCiudad = lstCiudad;
}
public String getTxtFecPublicacion() {
	return txtFecPublicacion;
}
public void setTxtFecPublicacion(String txtFecPublicacion) {
	this.txtFecPublicacion = txtFecPublicacion;
}
public String getTxtUrlArchivoDig() {
	return txtUrlArchivoDig;
}
public void setTxtUrlArchivoDig(String txtUrlArchivoDig) {
	this.txtUrlArchivoDig = txtUrlArchivoDig;
}
public String getCboIdioma() {
	return cboIdioma;
}
public void setCboIdioma(String cboIdioma) {
	this.cboIdioma = cboIdioma;
}
public List getLstIdioma() {
	return lstIdioma;
}
public void setLstIdioma(List lstIdioma) {
	this.lstIdioma = lstIdioma;
}
public String getTxtIsbn() {
	return txtIsbn;
}
public void setTxtIsbn(String txtIsbn) {
	this.txtIsbn = txtIsbn;
}
public String getTxtFormato() {
	return txtFormato;
}
public void setTxtFormato(String txtFormato) {
	this.txtFormato = txtFormato;
}
public String getCboTipoVideo() {
	return cboTipoVideo;
}
public void setCboTipoVideo(String cboTipoVideo) {
	this.cboTipoVideo = cboTipoVideo;
}
public List getLstTipoVideo() {
	return lstTipoVideo;
}
public void setLstTipoVideo(List lstTipoVideo) {
	this.lstTipoVideo = lstTipoVideo;
}
public String getCboProcedencia() {
	return cboProcedencia;
}
public void setCboProcedencia(String cboProcedencia) {
	this.cboProcedencia = cboProcedencia;
}
public List getLstProcedencia() {
	return lstProcedencia;
}
public void setLstProcedencia(List lstProcedencia) {
	this.lstProcedencia = lstProcedencia;
}
public String getCboPrecio() {
	return cboPrecio;
}
public void setCboPrecio(String cboPrecio) {
	this.cboPrecio = cboPrecio;
}
public List getLstPrecio() {
	return lstPrecio;
}
public void setLstPrecio(List lstPrecio) {
	this.lstPrecio = lstPrecio;
}
public String getTxtPrecio() {
	return txtPrecio;
}
public void setTxtPrecio(String txtPrecio) {
	this.txtPrecio = txtPrecio;
}
public String getRdoAplicaReserva() {
	return rdoAplicaReserva;
}
public void setRdoAplicaReserva(String rdoAplicaReserva) {
	this.rdoAplicaReserva = rdoAplicaReserva;
}
public String getTxtObservaciones() {
	return txtObservaciones;
}
public void setTxtObservaciones(String txtObservaciones) {
	this.txtObservaciones = txtObservaciones;
}
public String getChkTipoPrestamoSala() {
	return chkTipoPrestamoSala;
}
public void setChkTipoPrestamoSala(String chkTipoPrestamoSala) {
	this.chkTipoPrestamoSala = chkTipoPrestamoSala;
}
public String getChkTipoPrestamoDomicilio() {
	return chkTipoPrestamoDomicilio;
}
public void setChkTipoPrestamoDomicilio(String chkTipoPrestamoDomicilio) {
	this.chkTipoPrestamoDomicilio = chkTipoPrestamoDomicilio;
}
public String getPathImagen() {
	return pathImagen;
}
public void setPathImagen(String pathImagen) {
	this.pathImagen = pathImagen;
}
public String getAccion() {
	return accion;
}
public void setAccion(String accion) {
	this.accion = accion;
}
public String getTxhCodUnico() {
	return txhCodUnico;
}
public void setTxhCodUnico(String txhCodUnico) {
	this.txhCodUnico = txhCodUnico;
}
public String getUsuario() {
	return usuario;
}
public void setUsuario(String usuario) {
	this.usuario = usuario;
}
public String getPrmTxtCodigo() {
	return prmTxtCodigo;
}
public void setPrmTxtCodigo(String prmTxtCodigo) {
	this.prmTxtCodigo = prmTxtCodigo;
}
public String getPrmTxtNroIngreso() {
	return prmTxtNroIngreso;
}
public void setPrmTxtNroIngreso(String prmTxtNroIngreso) {
	this.prmTxtNroIngreso = prmTxtNroIngreso;
}
public String getPrmCboTipoMaterial() {
	return prmCboTipoMaterial;
}
public void setPrmCboTipoMaterial(String prmCboTipoMaterial) {
	this.prmCboTipoMaterial = prmCboTipoMaterial;
}
public String getPrmCboBuscarPor() {
	return prmCboBuscarPor;
}
public void setPrmCboBuscarPor(String prmCboBuscarPor) {
	this.prmCboBuscarPor = prmCboBuscarPor;
}
public String getPrmTxtTitulo() {
	return prmTxtTitulo;
}
public void setPrmTxtTitulo(String prmTxtTitulo) {
	this.prmTxtTitulo = prmTxtTitulo;
}
public String getPrmCboIdioma() {
	return prmCboIdioma;
}
public void setPrmCboIdioma(String prmCboIdioma) {
	this.prmCboIdioma = prmCboIdioma;
}
public String getPrmCboAnioIni() {
	return prmCboAnioIni;
}
public void setPrmCboAnioIni(String prmCboAnioIni) {
	this.prmCboAnioIni = prmCboAnioIni;
}
public String getPrmCboAnioFin() {
	return prmCboAnioFin;
}
public void setPrmCboAnioFin(String prmCboAnioFin) {
	this.prmCboAnioFin = prmCboAnioFin;
}
public String getPrmTxtFechaReservaIni() {
	return prmTxtFechaReservaIni;
}
public void setPrmTxtFechaReservaIni(String prmTxtFechaReservaIni) {
	this.prmTxtFechaReservaIni = prmTxtFechaReservaIni;
}
public String getPrmTxtFechaReservaFin() {
	return prmTxtFechaReservaFin;
}
public void setPrmTxtFechaReservaFin(String prmTxtFechaReservaFin) {
	this.prmTxtFechaReservaFin = prmTxtFechaReservaFin;
}
public String getTxhDesEditorial() {
	return txhDesEditorial;
}
public void setTxhDesEditorial(String txhDesEditorial) {
	this.txhDesEditorial = txhDesEditorial;
}
public String getTxhDesIdioma() {
	return txhDesIdioma;
}
public void setTxhDesIdioma(String txhDesIdioma) {
	this.txhDesIdioma = txhDesIdioma;
}
public String getTxhDesPais() {
	return txhDesPais;
}
public void setTxhDesPais(String txhDesPais) {
	this.txhDesPais = txhDesPais;
}
public String getTxhDesCiudad() {
	return txhDesCiudad;
}
public void setTxhDesCiudad(String txhDesCiudad) {
	this.txhDesCiudad = txhDesCiudad;
}
public String getCboSede() {
	return cboSede;
}
public void setCboSede(String cboSede) {
	this.cboSede = cboSede;
}
public List getLstSede() {
	return lstSede;
}
public void setLstSede(List lstSede) {
	this.lstSede = lstSede;
}
public String getTxtPrefijo() {
	return txtPrefijo;
}
public void setTxtPrefijo(String txtPrefijo) {
	this.txtPrefijo = txtPrefijo;
}
public String getTxtSufijo() {
	return txtSufijo;
}
public void setTxtSufijo(String txtSufijo) {
	this.txtSufijo = txtSufijo;
}
public String getTxtIdDewey() {
	return txtIdDewey;
}
public void setTxtIdDewey(String txtIdDewey) {
	this.txtIdDewey = txtIdDewey;
}
public String getTxtFrecuencia() {
	return txtFrecuencia;
}
public void setTxtFrecuencia(String txtFrecuencia) {
	this.txtFrecuencia = txtFrecuencia;
}
public String getTxhCodGeneradoDewey() {
	return txhCodGeneradoDewey;
}
public void setTxhCodGeneradoDewey(String txhCodGeneradoDewey) {
	this.txhCodGeneradoDewey = txhCodGeneradoDewey;
}
public String getMencionSerie() {
	return mencionSerie;
}
public void setMencionSerie(String mencionSerie) {
	this.mencionSerie = mencionSerie;
}
public List getLstAnio() {
	return lstAnio;
}
public void setLstAnio(List lstAnio) {
	this.lstAnio = lstAnio;
}

public String getFlgView() {
	return flgView;
}
public void setFlgView(String flgView) {
	this.flgView = flgView;
}
/**
 * @return the sedeSel
 */
public String getSedeSel() {
	return sedeSel;
}
/**
 * @param sedeSel the sedeSel to set
 */
public void setSedeSel(String sedeSel) {
	this.sedeSel = sedeSel;
}
/**
 * @return the sedeUsuario
 */
public String getSedeUsuario() {
	return sedeUsuario;
}
/**
 * @param sedeUsuario the sedeUsuario to set
 */
public void setSedeUsuario(String sedeUsuario) {
	this.sedeUsuario = sedeUsuario;
}
public String getIndISBN() {
	return indISBN;
}
public void setIndISBN(String indISBN) {
	this.indISBN = indISBN;
}


}
