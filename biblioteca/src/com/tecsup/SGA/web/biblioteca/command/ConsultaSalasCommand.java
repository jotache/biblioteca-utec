package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaSalasCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	private String tipoSala;
	private String dscSala;
	//*****************************
	private List listaTipoSala;
	private List listaSala;
	
	private String sedeSel;
	private String sedeUsuario;
	
	public String getSedeSel() {
		return sedeSel;
	}
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}
	public String getSedeUsuario() {
		return sedeUsuario;
	}
	public void setSedeUsuario(String sedeUsuario) {
		this.sedeUsuario = sedeUsuario;
	}
	
	public List getListaSala() {
		return listaSala;
	}
	public void setListaSala(List listaSala) {
		this.listaSala = listaSala;
	}
	//*****************************
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTipoSala() {
		return tipoSala;
	}
	public void setTipoSala(String tipoSala) {
		this.tipoSala = tipoSala;
	}
	public String getDscSala() {
		return dscSala;
	}
	public void setDscSala(String dscSala) {
		this.dscSala = dscSala;
	}
	public List getListaTipoSala() {
		return listaTipoSala;
	}
	public void setListaTipoSala(List listaTipoSala) {
		this.listaTipoSala = listaTipoSala;
	}		
}
