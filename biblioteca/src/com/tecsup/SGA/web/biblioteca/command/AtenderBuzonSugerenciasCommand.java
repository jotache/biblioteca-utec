package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AtenderBuzonSugerenciasCommand {

	private String operacion;
	private String msg;
	private String idRec;
	private String lstrURL;
	private String codPeriodo;
	private String codEvaluador;
	/*********************/
	private String fechaRegistro;//
	private String comentario;//
	private String usuario;//
	private String calificacionRespuesta;//
	private String tipoMaterial;//
	private String codSelec;//
	private String tipoSugerencia;//
	private String textoRespuesta;//
	/*********************/
	private List codListaTipoMaterial;
	private List codListaCalificacion;
	
	private String consteBuzonConsulta;
	private String consteBuzonComentario;
	private String consteBuzonSugerencia;
	private String consteBuzonReclamo;
	
	private String consteConsulta;
	private String consteComentario;
	private String consteSugerencia;
	private String consteReclamo;
	
	private String codSelTipoSugerencia;
	
	private String respuesta;
	private String email;
	
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCalificacionRespuesta() {
		return calificacionRespuesta;
	}
	public void setCalificacionRespuesta(String calificacionRespuesta) {
		this.calificacionRespuesta = calificacionRespuesta;
	}
	public String getTipoMaterial() {
		return tipoMaterial;
	}
	public void setTipoMaterial(String tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getTipoSugerencia() {
		return tipoSugerencia;
	}
	public void setTipoSugerencia(String tipoSugerencia) {
		this.tipoSugerencia = tipoSugerencia;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	
	public String getTextoRespuesta() {
		return textoRespuesta;
	}
	public void setTextoRespuesta(String textoRespuesta) {
		this.textoRespuesta = textoRespuesta;
	}
	
	public List getCodListaTipoMaterial() {
		return codListaTipoMaterial;
	}
	public void setCodListaTipoMaterial(List codListaTipoMaterial) {
		this.codListaTipoMaterial = codListaTipoMaterial;
	}
	public List getCodListaCalificacion() {
		return codListaCalificacion;
	}
	public void setCodListaCalificacion(List codListaCalificacion) {
		this.codListaCalificacion = codListaCalificacion;
	}
	public String getConsteBuzonConsulta() {
		return consteBuzonConsulta;
	}
	public void setConsteBuzonConsulta(String consteBuzonConsulta) {
		this.consteBuzonConsulta = consteBuzonConsulta;
	}
	public String getConsteBuzonComentario() {
		return consteBuzonComentario;
	}
	public void setConsteBuzonComentario(String consteBuzonComentario) {
		this.consteBuzonComentario = consteBuzonComentario;
	}
	public String getConsteBuzonSugerencia() {
		return consteBuzonSugerencia;
	}
	public void setConsteBuzonSugerencia(String consteBuzonSugerencia) {
		this.consteBuzonSugerencia = consteBuzonSugerencia;
	}
	public String getConsteBuzonReclamo() {
		return consteBuzonReclamo;
	}
	public void setConsteBuzonReclamo(String consteBuzonReclamo) {
		this.consteBuzonReclamo = consteBuzonReclamo;
	}
	public String getConsteConsulta() {
		return consteConsulta;
	}
	public void setConsteConsulta(String consteConsulta) {
		this.consteConsulta = consteConsulta;
	}
	public String getConsteComentario() {
		return consteComentario;
	}
	public void setConsteComentario(String consteComentario) {
		this.consteComentario = consteComentario;
	}
	public String getConsteSugerencia() {
		return consteSugerencia;
	}
	public void setConsteSugerencia(String consteSugerencia) {
		this.consteSugerencia = consteSugerencia;
	}
	public String getConsteReclamo() {
		return consteReclamo;
	}
	public void setConsteReclamo(String consteReclamo) {
		this.consteReclamo = consteReclamo;
	}
	public String getCodSelTipoSugerencia() {
		return codSelTipoSugerencia;
	}
	public void setCodSelTipoSugerencia(String codSelTipoSugerencia) {
		this.codSelTipoSugerencia = codSelTipoSugerencia;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
