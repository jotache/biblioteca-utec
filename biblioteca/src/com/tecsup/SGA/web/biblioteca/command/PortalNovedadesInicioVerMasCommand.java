package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class PortalNovedadesInicioVerMasCommand {
	
	private String CodEvaluador;
	private String CodPeriodo;
	private List listaNovedadesInicioVerMas;
	private String descripcionLarga;
	private String descripcionGrupo;
	private String tema;
	private String imagen;
	private String txtDescripcionLarga;
	private String archivo;
	
	public String url;
	public String documentoNov; //novedades
	
	public String nombreOriginal; //nombre original
	public String nombreFile; //guardado en carpeta
	
	public String rutaArchivos;
	public String extDocNov;
	
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public String getTxtDescripcionLarga() {
		return txtDescripcionLarga;
	}
	public void setTxtDescripcionLarga(String txtDescripcionLarga) {
		this.txtDescripcionLarga = txtDescripcionLarga;
	}
	public String getDescripcionLarga() {
		return descripcionLarga;
	}
	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}
	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}
	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getCodEvaluador() {
		return CodEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		CodEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return CodPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		CodPeriodo = codPeriodo;
	}
	public List getListaNovedadesInicioVerMas() {
		return listaNovedadesInicioVerMas;
	}
	public void setListaNovedadesInicioVerMas(List listaNovedadesInicioVerMas) {
		this.listaNovedadesInicioVerMas = listaNovedadesInicioVerMas;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDocumentoNov() {
		return documentoNov;
	}
	public void setDocumentoNov(String documentoNov) {
		this.documentoNov = documentoNov;
	}
	public String getNombreOriginal() {
		return nombreOriginal;
	}
	public void setNombreOriginal(String nombreOriginal) {
		this.nombreOriginal = nombreOriginal;
	}
	public String getNombreFile() {
		return nombreFile;
	}
	public void setNombreFile(String nombreFile) {
		this.nombreFile = nombreFile;
	}
	public String getRutaArchivos() {
		return rutaArchivos;
	}
	public void setRutaArchivos(String rutaArchivos) {
		this.rutaArchivos = rutaArchivos;
	}
	public String getExtDocNov() {
		return extDocNov;
	}
	public void setExtDocNov(String extDocNov) {
		this.extDocNov = extDocNov;
	}
	
}
