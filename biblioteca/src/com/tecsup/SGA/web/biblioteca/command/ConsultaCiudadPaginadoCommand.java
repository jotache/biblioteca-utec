package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaCiudadPaginadoCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	//*****************************
	private String pais;
	private String dscCiudad;
	private List listaPais;
	private List listaCiudad;
	private String tamListaCiudad;
	//*****************************
	private String codigoSec;
	private String codigoCiudad;
	private String descripcion;
	//*****************************
	private String seleccion;
	
	private String seleccionPais;

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getDscCiudad() {
		return dscCiudad;
	}

	public void setDscCiudad(String dscCiudad) {
		this.dscCiudad = dscCiudad;
	}

		
	public List getListaCiudad() {
		return listaCiudad;
	}

	public void setListaCiudad(List listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	public String getTamListaCiudad() {
		return tamListaCiudad;
	}

	public void setTamListaCiudad(String tamListaCiudad) {
		this.tamListaCiudad = tamListaCiudad;
	}

	public String getCodigoSec() {
		return codigoSec;
	}

	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}

	public String getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}

	public List getListaPais() {
		return listaPais;
	}

	public void setListaPais(List listaPais) {
		this.listaPais = listaPais;
	}

	public String getSeleccionPais() {
		return seleccionPais;
	}

	public void setSeleccionPais(String seleccionPais) {
		this.seleccionPais = seleccionPais;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

}
