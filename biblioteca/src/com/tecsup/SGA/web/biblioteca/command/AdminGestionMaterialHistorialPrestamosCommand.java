package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminGestionMaterialHistorialPrestamosCommand {
	private String operacion;
	private String txtCodigo;
	private String txtTipoMaterial;
	private String txtTitulo;
	private String txhCodigoUnico;
	private String txtCodUsuario;
	private List lstResultado;

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getTxtCodigo() {
		return txtCodigo;
	}

	public void setTxtCodigo(String txtCodigo) {
		this.txtCodigo = txtCodigo;
	}

	public String getTxtTipoMaterial() {
		return txtTipoMaterial;
	}

	public void setTxtTipoMaterial(String txtTipoMaterial) {
		this.txtTipoMaterial = txtTipoMaterial;
	}

	public String getTxtTitulo() {
		return txtTitulo;
	}

	public void setTxtTitulo(String txtTitulo) {
		this.txtTitulo = txtTitulo;
	}

	public List getLstResultado() {
		return lstResultado;
	}

	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}

	public String getTxhCodigoUnico() {
		return txhCodigoUnico;
	}

	public void setTxhCodigoUnico(String txhCodigoUnico) {
		this.txhCodigoUnico = txhCodigoUnico;
	}

	public String getTxtCodUsuario() {
		return txtCodUsuario;
	}

	public void setTxtCodUsuario(String txtCodUsuario) {
		this.txtCodUsuario = txtCodUsuario;
	}
}
