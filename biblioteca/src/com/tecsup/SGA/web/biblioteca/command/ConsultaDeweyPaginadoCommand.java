package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaDeweyPaginadoCommand {

	private String operacion; //
	private String idRec;
	private String lstrURL;
	private List listaDewey;//
	
	private List codListaCategoria;//
	private String codCategoria;
	private String descripcionDewey;//
	private String codSelec; //
	private String descripSelec;//
	private String codEvaluador;
	private String codPeriodo;
	private String msg;
	private String codValor;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public List getListaDewey() {
		return listaDewey;
	}
	public void setListaDewey(List listaDewey) {
		this.listaDewey = listaDewey;
	}
	public List getCodListaCategoria() {
		return codListaCategoria;
	}
	public void setCodListaCategoria(List codListaCategoria) {
		this.codListaCategoria = codListaCategoria;
	}
	public String getCodCategoria() {
		return codCategoria;
	}
	public void setCodCategoria(String codCategoria) {
		this.codCategoria = codCategoria;
	}
	public String getDescripcionDewey() {
		return descripcionDewey;
	}
	public void setDescripcionDewey(String descripcionDewey) {
		this.descripcionDewey = descripcionDewey;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
}
