package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminRegistrarPrestamoBuscaUsuarioCommand {

	private String operacion;
	private List lstResultado;
	private String txhTipo;
	private String txhCodUsuario;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getTxhTipo() {
		return txhTipo;
	}
	public void setTxhTipo(String txhTipo) {
		this.txhTipo = txhTipo;
	}
	public String getTxhCodUsuario() {
		return txhCodUsuario;
	}
	public void setTxhCodUsuario(String txhCodUsuario) {
		this.txhCodUsuario = txhCodUsuario;
	}
}
