package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminGestionMaterialHistorialProrrogasCommand {
	private String operacion;
	private String txhNroIngreso;
	private String txhCodUsuario;
	private List lstResultado;

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getTxhNroIngreso() {
		return txhNroIngreso;
	}

	public void setTxhNroIngreso(String txhNroIngreso) {
		this.txhNroIngreso = txhNroIngreso;
	}

	public String getTxhCodUsuario() {
		return txhCodUsuario;
	}

	public void setTxhCodUsuario(String txhCodUsuario) {
		this.txhCodUsuario = txhCodUsuario;
	}

	public List getLstResultado() {
		return lstResultado;
	}

	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}

}
