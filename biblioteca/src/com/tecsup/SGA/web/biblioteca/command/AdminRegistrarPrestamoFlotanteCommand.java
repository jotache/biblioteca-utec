package com.tecsup.SGA.web.biblioteca.command;

public class AdminRegistrarPrestamoFlotanteCommand {

	public String operacion;
	public String codMaterial;
	public String tipoMaterial;
	public String codUsuario;
	public String codUnico;
	public String nroIngreso;
	public String dscUsuario;
	public String fechaBD;
	public String fechaReserva;
	public String titulo;
	public String usuarioLogin;
	public String dewey;
	
	public String matNroIngreso;
	public String matTitulo;
	//public String sedeSel;
	//public String sedeUsuario;
	
	
	public String getMatNroIngreso() {
		return matNroIngreso;
	}
	public void setMatNroIngreso(String matNroIngreso) {
		this.matNroIngreso = matNroIngreso;
	}
	public String getMatTitulo() {
		return matTitulo;
	}
	public void setMatTitulo(String matTitulo) {
		this.matTitulo = matTitulo;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodMaterial() {
		return codMaterial;
	}
	public void setCodMaterial(String codMaterial) {
		this.codMaterial = codMaterial;
	}
	public String getTipoMaterial() {
		return tipoMaterial;
	}
	public void setTipoMaterial(String tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getNroIngreso() {
		return nroIngreso;
	}
	public void setNroIngreso(String nroIngreso) {
		this.nroIngreso = nroIngreso;
	}
	public String getFechaBD() {
		return fechaBD;
	}
	public void setFechaBD(String fechaBD) {
		this.fechaBD = fechaBD;
	}
	public String getFechaReserva() {
		return fechaReserva;
	}
	public void setFechaReserva(String fechaReserva) {
		this.fechaReserva = fechaReserva;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getUsuarioLogin() {
		return usuarioLogin;
	}
	public void setUsuarioLogin(String usuarioLogin) {
		this.usuarioLogin = usuarioLogin;
	}
	public String getDewey() {
		return dewey;
	}
	public void setDewey(String dewey) {
		this.dewey = dewey;
	}
	
	
}

