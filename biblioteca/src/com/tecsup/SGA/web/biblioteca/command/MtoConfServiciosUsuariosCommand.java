package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class MtoConfServiciosUsuariosCommand {

	private String operacion;
	private String msg;
	private String idRec;
	private String lstrURL;
	private List tipoAlumnos;
	private String codAlumno;
	private String presSala;
	private String presDomi;
	private String sala;
	private String inter;
	private String maBi;
	private String numDias;
	private String apSanciones;
	private String reaRe;
	private String cantOcu;
	private String codId;
	private String codEval;
	private String prestPen;
	private String diasPrest;
	private String nroProrrogas;
	private List<TipoTablaDetalle> listaMaxDiasPrestamo;
	private String datosCadena;
	private String datosTamano;
	
	public String getNroProrrogas() {
		return nroProrrogas;
	}
	public void setNroProrrogas(String nroProrrogas) {
		this.nroProrrogas = nroProrrogas;
	}
	public String getPrestPen() {
		return prestPen;
	}
	public void setPrestPen(String prestPen) {
		this.prestPen = prestPen;
	}
	public String getDiasPrest() {
		return diasPrest;
	}
	public void setDiasPrest(String diasPrest) {
		this.diasPrest = diasPrest;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public List getTipoAlumnos() {
		return tipoAlumnos;
	}
	public void setTipoAlumnos(List tipoAlumnos) {
		this.tipoAlumnos = tipoAlumnos;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPresSala() {
		return presSala;
	}
	public void setPresSala(String presSala) {
		this.presSala = presSala;
	}
	public String getPresDomi() {
		return presDomi;
	}
	public void setPresDomi(String presDomi) {
		this.presDomi = presDomi;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	public String getInter() {
		return inter;
	}
	public void setInter(String inter) {
		this.inter = inter;
	}
	public String getMaBi() {
		return maBi;
	}
	public void setMaBi(String maBi) {
		this.maBi = maBi;
	}
	
	public String getNumDias() {
		return numDias;
	}
	public void setNumDias(String numDias) {
		this.numDias = numDias;
	}
	public String getApSanciones() {
		return apSanciones;
	}
	public void setApSanciones(String apSanciones) {
		this.apSanciones = apSanciones;
	}
	public String getReaRe() {
		return reaRe;
	}
	public void setReaRe(String reaRe) {
		this.reaRe = reaRe;
	}
	public String getCantOcu() {
		return cantOcu;
	}
	public void setCantOcu(String cantOcu) {
		this.cantOcu = cantOcu;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public List<TipoTablaDetalle> getListaMaxDiasPrestamo() {
		return listaMaxDiasPrestamo;
	}
	public void setListaMaxDiasPrestamo(List<TipoTablaDetalle> listaMaxDiasPrestamo) {
		this.listaMaxDiasPrestamo = listaMaxDiasPrestamo;
	}
	public String getDatosCadena() {
		return datosCadena;
	}
	public void setDatosCadena(String datosCadena) {
		this.datosCadena = datosCadena;
	}
	public String getDatosTamano() {
		return datosTamano;
	}
	public void setDatosTamano(String datosTamano) {
		this.datosTamano = datosTamano;
	}

}
