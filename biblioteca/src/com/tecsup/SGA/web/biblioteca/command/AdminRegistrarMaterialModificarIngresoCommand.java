package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminRegistrarMaterialModificarIngresoCommand {
	private String operacion;
	private String txtIngreso;
	private String txtNroIngreso;
	private String txtFecIngreso;
	private String txtFecBaja;
	private String cboProcedencia;
	private List lstProcedencia;
	private String cboMoneda;
	private List lstMoneda;
	private String txtPrecio;
	private String cboEstado;
	private List lstEstado;
	private String txtObservacion;
	private String txhCodigoUnico;
	private String usuario;

public String getOperacion() {
	return operacion;
}

public void setOperacion(String operacion) {
	this.operacion = operacion;
}

public String getTxtIngreso() {
	return txtIngreso;
}

public void setTxtIngreso(String txtIngreso) {
	this.txtIngreso = txtIngreso;
}

public String getTxtNroIngreso() {
	return txtNroIngreso;
}

public void setTxtNroIngreso(String txtNroIngreso) {
	this.txtNroIngreso = txtNroIngreso;
}

public String getTxtFecIngreso() {
	return txtFecIngreso;
}

public void setTxtFecIngreso(String txtFecIngreso) {
	this.txtFecIngreso = txtFecIngreso;
}

public String getCboProcedencia() {
	return cboProcedencia;
}

public void setCboProcedencia(String cboProcedencia) {
	this.cboProcedencia = cboProcedencia;
}

public List getLstProcedencia() {
	return lstProcedencia;
}

public void setLstProcedencia(List lstProcedencia) {
	this.lstProcedencia = lstProcedencia;
}

public String getCboMoneda() {
	return cboMoneda;
}

public void setCboMoneda(String cboMoneda) {
	this.cboMoneda = cboMoneda;
}

public List getLstMoneda() {
	return lstMoneda;
}

public void setLstMoneda(List lstMoneda) {
	this.lstMoneda = lstMoneda;
}

public String getTxtPrecio() {
	return txtPrecio;
}

public void setTxtPrecio(String txtPrecio) {
	this.txtPrecio = txtPrecio;
}

public String getCboEstado() {
	return cboEstado;
}

public void setCboEstado(String cboEstado) {
	this.cboEstado = cboEstado;
}

public List getLstEstado() {
	return lstEstado;
}

public void setLstEstado(List lstEstado) {
	this.lstEstado = lstEstado;
}

public String getTxtObservacion() {
	return txtObservacion;
}

public void setTxtObservacion(String txtObservacion) {
	this.txtObservacion = txtObservacion;
}

public String getTxtFecBaja() {
	return txtFecBaja;
}

public void setTxtFecBaja(String txtFecBaja) {
	this.txtFecBaja = txtFecBaja;
}

public String getTxhCodigoUnico() {
	return txhCodigoUnico;
}

public void setTxhCodigoUnico(String txhCodigoUnico) {
	this.txhCodigoUnico = txhCodigoUnico;
}

public String getUsuario() {
	return usuario;
}

public void setUsuario(String usuario) {
	this.usuario = usuario;
}
}
