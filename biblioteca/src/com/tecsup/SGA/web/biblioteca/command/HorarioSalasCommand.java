package com.tecsup.SGA.web.biblioteca.command;

public class HorarioSalasCommand {

	private String operacion;
	private String tamanioLista;
	private String codUsuario;
	private String msg;
	private String sede;
	private String cadenaDatos;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTamanioLista() {
		return tamanioLista;
	}
	public void setTamanioLista(String tamanioLista) {
		this.tamanioLista = tamanioLista;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getCadenaDatos() {
		return cadenaDatos;
	}
	public void setCadenaDatos(String cadenaDatos) {
		this.cadenaDatos = cadenaDatos;
	}
}
