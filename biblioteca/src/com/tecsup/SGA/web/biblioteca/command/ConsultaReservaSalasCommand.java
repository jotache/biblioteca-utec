package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

import com.tecsup.SGA.modelo.Sala;

public class ConsultaReservaSalasCommand {
  
  private String operacion;
  private String codSalas;
  private List codListaSalas;
  private String codHoraInicio;
  // private List codListaHoraInicio;
  private String codHoraFin;
  // private List codListaHoraFin;
  private String txtFechaInicio;
  private String txtFechaFin;
  private String txtCapacidadMaxima;
  
  private String codPeriodo;
  private String codEvaluador;
  
  private List listaConsultaReservaSala;
  private List listaConsultaReservaSala02;
  private String msg;
  private String nroComponentes;
  private String codComponentesSeleccionados;
  private String fechaBD;
  private String txtVacantes;
  private String usuarioReserva;
  
  private String codTipoSala;
  private List codListaTipoSala;
  
  private String codNombreSalas;
  private List codListaNombreSala;
  
  private String consteDisponible;
  private String disponible;
  private String estado;
  // modificado RNapa
  private String codigo;// bandeja de busqueja
  private String tamListaReservaSala01;
  private String tamListaReservaSala02;
  private String codSala;
  private String codReserva;
  private String flgUsuAdmin;
  private String flgLabQuimica;
  
  private String estadoPendiente = "1";
  private String estadoReservado = "1";
  private String estadoEjecutado = "1";
  private String sedeSel;
  private String sedeUsuario;
  private List<Sala> listaHoras;
  
  /**
   * @return the estadoPendiente
   */
  public String getEstadoPendiente() {
    return estadoPendiente;
  }
  
  /**
   * @param estadoPendiente
   *          the estadoPendiente to set
   */
  public void setEstadoPendiente(String estadoPendiente) {
    this.estadoPendiente = estadoPendiente;
  }
  
  /**
   * @return the estadoReservado
   */
  public String getEstadoReservado() {
    return estadoReservado;
  }
  
  /**
   * @param estadoReservado
   *          the estadoReservado to set
   */
  public void setEstadoReservado(String estadoReservado) {
    this.estadoReservado = estadoReservado;
  }
  
  /**
   * @return the estadoEjecutado
   */
  public String getEstadoEjecutado() {
    return estadoEjecutado;
  }
  
  /**
   * @param estadoEjecutado
   *          the estadoEjecutado to set
   */
  public void setEstadoEjecutado(String estadoEjecutado) {
    this.estadoEjecutado = estadoEjecutado;
  }
  
  public String getCodSala() {
    return codSala;
  }
  
  public void setCodSala(String codSala) {
    this.codSala = codSala;
  }
  
  public String getCodReserva() {
    return codReserva;
  }
  
  public void setCodReserva(String codReserva) {
    this.codReserva = codReserva;
  }
  
  public String getTamListaReservaSala02() {
    return tamListaReservaSala02;
  }
  
  public void setTamListaReservaSala02(String tamListaReservaSala02) {
    this.tamListaReservaSala02 = tamListaReservaSala02;
  }
  
  public String getTamListaReservaSala01() {
    return tamListaReservaSala01;
  }
  
  public void setTamListaReservaSala01(String tamListaReservaSala01) {
    this.tamListaReservaSala01 = tamListaReservaSala01;
  }
  
  public String getCodigo() {
    return codigo;
  }
  
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }
  
  public String getEstado() {
    return estado;
  }
  
  public void setEstado(String estado) {
    this.estado = estado;
  }
  
  public String getConsteDisponible() {
    return consteDisponible;
  }
  
  public void setConsteDisponible(String consteDisponible) {
    this.consteDisponible = consteDisponible;
  }
  
  public String getCodTipoSala() {
    return codTipoSala;
  }
  
  public void setCodTipoSala(String codTipoSala) {
    this.codTipoSala = codTipoSala;
  }
  
  public List getCodListaTipoSala() {
    return codListaTipoSala;
  }
  
  public void setCodListaTipoSala(List codListaTipoSala) {
    this.codListaTipoSala = codListaTipoSala;
  }
  
  public String getTxtVacantes() {
    return txtVacantes;
  }
  
  public void setTxtVacantes(String txtVacantes) {
    this.txtVacantes = txtVacantes;
  }
  
  public String getFechaBD() {
    return fechaBD;
  }
  
  public void setFechaBD(String fechaBD) {
    this.fechaBD = fechaBD;
  }
  
  public String getNroComponentes() {
    return nroComponentes;
  }
  
  public void setNroComponentes(String nroComponentes) {
    this.nroComponentes = nroComponentes;
  }
  
  public String getCodComponentesSeleccionados() {
    return codComponentesSeleccionados;
  }
  
  public void setCodComponentesSeleccionados(String codComponentesSeleccionados) {
    this.codComponentesSeleccionados = codComponentesSeleccionados;
  }
  
  public String getMsg() {
    return msg;
  }
  
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  public List getListaConsultaReservaSala() {
    return listaConsultaReservaSala;
  }
  
  public void setListaConsultaReservaSala(List listaConsultaReservaSala) {
    this.listaConsultaReservaSala = listaConsultaReservaSala;
  }
  
  public String getOperacion() {
    return operacion;
  }
  
  public void setOperacion(String operacion) {
    this.operacion = operacion;
  }
  
  public String getCodSalas() {
    return codSalas;
  }
  
  public void setCodSalas(String codSalas) {
    this.codSalas = codSalas;
  }
  
  public List getCodListaSalas() {
    return codListaSalas;
  }
  
  public void setCodListaSalas(List codListaSalas) {
    this.codListaSalas = codListaSalas;
  }
  
  public String getCodHoraInicio() {
    return codHoraInicio;
  }
  
  public void setCodHoraInicio(String codHoraInicio) {
    this.codHoraInicio = codHoraInicio;
  }
  
  /*
   * public List getCodListaHoraInicio() { return codListaHoraInicio; } public
   * void setCodListaHoraInicio(List codListaHoraInicio) {
   * this.codListaHoraInicio = codListaHoraInicio; }
   */
  public String getCodHoraFin() {
    return codHoraFin;
  }
  
  public void setCodHoraFin(String codHoraFin) {
    this.codHoraFin = codHoraFin;
  }
  
  /*
   * public List getCodListaHoraFin() { return codListaHoraFin; } public void
   * setCodListaHoraFin(List codListaHoraFin) { this.codListaHoraFin =
   * codListaHoraFin; }
   */
  public String getTxtFechaInicio() {
    return txtFechaInicio;
  }
  
  public void setTxtFechaInicio(String txtFechaInicio) {
    this.txtFechaInicio = txtFechaInicio;
  }
  
  public String getTxtFechaFin() {
    return txtFechaFin;
  }
  
  public void setTxtFechaFin(String txtFechaFin) {
    this.txtFechaFin = txtFechaFin;
  }
  
  public String getTxtCapacidadMaxima() {
    return txtCapacidadMaxima;
  }
  
  public void setTxtCapacidadMaxima(String txtCapacidadMaxima) {
    this.txtCapacidadMaxima = txtCapacidadMaxima;
  }
  
  public String getCodPeriodo() {
    return codPeriodo;
  }
  
  public void setCodPeriodo(String codPeriodo) {
    this.codPeriodo = codPeriodo;
  }
  
  public String getCodEvaluador() {
    return codEvaluador;
  }
  
  public void setCodEvaluador(String codEvaluador) {
    this.codEvaluador = codEvaluador;
  }
  
  public String getCodNombreSalas() {
    return codNombreSalas;
  }
  
  public void setCodNombreSalas(String codNombreSalas) {
    this.codNombreSalas = codNombreSalas;
  }
  
  public List getCodListaNombreSala() {
    return codListaNombreSala;
  }
  
  public void setCodListaNombreSala(List codListaNombreSala) {
    this.codListaNombreSala = codListaNombreSala;
  }
  
  public String getDisponible() {
    return disponible;
  }
  
  public void setDisponible(String disponible) {
    this.disponible = disponible;
  }
  
  public String getUsuarioReserva() {
    return usuarioReserva;
  }
  
  public void setUsuarioReserva(String usuarioReserva) {
    this.usuarioReserva = usuarioReserva;
  }
  
  public List getListaConsultaReservaSala02() {
    return listaConsultaReservaSala02;
  }
  
  public void setListaConsultaReservaSala02(List listaConsultaReservaSala02) {
    this.listaConsultaReservaSala02 = listaConsultaReservaSala02;
  }
  
  public String getFlgUsuAdmin() {
    return flgUsuAdmin;
  }
  
  public void setFlgUsuAdmin(String flgUsuAdmin) {
    this.flgUsuAdmin = flgUsuAdmin;
  }
  
  /**
   * @return the sedeSel
   */
  public String getSedeSel() {
    return sedeSel;
  }
  
  /**
   * @param sedeSel
   *          the sedeSel to set
   */
  public void setSedeSel(String sedeSel) {
    this.sedeSel = sedeSel;
  }
  
  /**
   * @return the sedeUsuario
   */
  public String getSedeUsuario() {
    return sedeUsuario;
  }
  
  /**
   * @param sedeUsuario
   *          the sedeUsuario to set
   */
  public void setSedeUsuario(String sedeUsuario) {
    this.sedeUsuario = sedeUsuario;
  }
  
  public List<Sala> getListaHoras() {
    return listaHoras;
  }
  
  public void setListaHoras(List<Sala> listaHoras) {
    this.listaHoras = listaHoras;
  }
  
  public String getFlgLabQuimica() {
    return flgLabQuimica;
  }
  
  public void setFlgLabQuimica(String flgLabQuimica) {
    this.flgLabQuimica = flgLabQuimica;
  }

@Override
public String toString() {
	return "ConsultaReservaSalasCommand [operacion=" + operacion
			+ ", codSalas=" + codSalas + ", codListaSalas=" + codListaSalas
			+ ", codHoraInicio=" + codHoraInicio + ", codHoraFin=" + codHoraFin
			+ ", txtFechaInicio=" + txtFechaInicio + ", txtFechaFin="
			+ txtFechaFin + ", txtCapacidadMaxima=" + txtCapacidadMaxima
			+ ", codPeriodo=" + codPeriodo + ", codEvaluador=" + codEvaluador
			+ ", listaConsultaReservaSala=" + listaConsultaReservaSala
			+ ", listaConsultaReservaSala02=" + listaConsultaReservaSala02
			+ ", msg=" + msg + ", nroComponentes=" + nroComponentes
			+ ", codComponentesSeleccionados=" + codComponentesSeleccionados
			+ ", fechaBD=" + fechaBD + ", txtVacantes=" + txtVacantes
			+ ", usuarioReserva=" + usuarioReserva + ", codTipoSala="
			+ codTipoSala + ", codListaTipoSala=" + codListaTipoSala
			+ ", codNombreSalas=" + codNombreSalas + ", codListaNombreSala="
			+ codListaNombreSala + ", consteDisponible=" + consteDisponible
			+ ", disponible=" + disponible + ", estado=" + estado + ", codigo="
			+ codigo + ", tamListaReservaSala01=" + tamListaReservaSala01
			+ ", tamListaReservaSala02=" + tamListaReservaSala02 + ", codSala="
			+ codSala + ", codReserva=" + codReserva + ", flgUsuAdmin="
			+ flgUsuAdmin + ", flgLabQuimica=" + flgLabQuimica
			+ ", estadoPendiente=" + estadoPendiente + ", estadoReservado="
			+ estadoReservado + ", estadoEjecutado=" + estadoEjecutado
			+ ", sedeSel=" + sedeSel + ", sedeUsuario=" + sedeUsuario
			+ ", listaHoras=" + listaHoras + "]";
}
  
}
