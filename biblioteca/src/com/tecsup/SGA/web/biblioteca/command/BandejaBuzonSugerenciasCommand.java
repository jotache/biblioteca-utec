package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class BandejaBuzonSugerenciasCommand {

	private String operacion;
	private String msg;
	private String idRec;
	private String lstrURL;
	private String codPeriodo;
	private String codEvaluador;
	//*********************
	private String mes;
	private String anio;
	private String tipoSugerencia;
	private String estado;
	//*********************	
	private List listaCiudades;
	private List listaSugerencia;
	private String codTipoSugerencia;
	private List codListaTipoSugerencia;
	private String codEstado;
	private List codListaEstado;
	private String fechaRegistro;
	//private String tipoSugerencia;
	private String comentario;
	private String usuario;
	private String calRespuesta;
	private String tipoMaterial;
	private String codSelec;
	
	//***********************
	private String codAnios;
	private List codListaAnios;
	private String codMeses;
	private List codListaMeses;
	private String periodoActual;
	private String email;
	//*********************
	
	/*private String mesActual;
	private String anioActual;*/
	
	public String getPeriodoActual() {
		return periodoActual;
	}
	public void setPeriodoActual(String periodoActual) {
		this.periodoActual = periodoActual;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getCodTipoSugerencia() {
		return codTipoSugerencia;
	}
	public void setCodTipoSugerencia(String codTipoSugerencia) {
		this.codTipoSugerencia = codTipoSugerencia;
	}
	public List getCodListaTipoSugerencia() {
		return codListaTipoSugerencia;
	}
	public void setCodListaTipoSugerencia(List codListaTipoSugerencia) {
		this.codListaTipoSugerencia = codListaTipoSugerencia;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public List getCodListaEstado() {
		return codListaEstado;
	}
	public void setCodListaEstado(List codListaEstado) {
		this.codListaEstado = codListaEstado;
	}
	public List getListaCiudades() {
		return listaCiudades;
	}
	public void setListaCiudades(List listaCiudades) {
		this.listaCiudades = listaCiudades;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getTipoSugerencia() {
		return tipoSugerencia;
	}
	public void setTipoSugerencia(String tipoSugerencia) {
		this.tipoSugerencia = tipoSugerencia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public List getListaSugerencia() {
		return listaSugerencia;
	}
	public void setListaSugerencia(List listaSugerencia) {
		this.listaSugerencia = listaSugerencia;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCalRespuesta() {
		return calRespuesta;
	}
	public void setCalRespuesta(String calRespuesta) {
		this.calRespuesta = calRespuesta;
	}
	public String getTipoMaterial() {
		return tipoMaterial;
	}
	public void setTipoMaterial(String tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodAnios() {
		return codAnios;
	}
	public void setCodAnios(String codAnios) {
		this.codAnios = codAnios;
	}
	public List getCodListaAnios() {
		return codListaAnios;
	}
	public void setCodListaAnios(List codListaAnios) {
		this.codListaAnios = codListaAnios;
	}
	public String getCodMeses() {
		return codMeses;
	}
	public void setCodMeses(String codMeses) {
		this.codMeses = codMeses;
	}
	public List getCodListaMeses() {
		return codListaMeses;
	}
	public void setCodListaMeses(List codListaMeses) {
		this.codListaMeses = codListaMeses;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
