package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class BusquedaSimpleCommand {

	private String operacion;
	private String codPeriodo;
	private String codEvaluador;
	private String codTipoMaterial;
	private String codSede;
	private List codListaTipoMaterial;
	private List listaTipoMaterial;
	private String codBusquedaBy;
	private List codListaBusquedaBy;
	private String codOrdenarBy;
	private List codListaOrdenarBy;
	private String txtTexto;
	private String selSala;
	private String selDomicilio;
	private String consteSala;
	private String consteDomicilio;
	
	private String codComponentesSeleccionados;
	private String codTipoSala;
	private List codListaTipoSala;
	
	public String getCodTipoSala() {
		return codTipoSala;
	}
	public void setCodTipoSala(String codTipoSala) {
		this.codTipoSala = codTipoSala;
	}
	public List getCodListaTipoSala() {
		return codListaTipoSala;
	}
	public void setCodListaTipoSala(List codListaTipoSala) {
		this.codListaTipoSala = codListaTipoSala;
	}
	public String getCodComponentesSeleccionados() {
		return codComponentesSeleccionados;
	}
	public void setCodComponentesSeleccionados(String codComponentesSeleccionados) {
		this.codComponentesSeleccionados = codComponentesSeleccionados;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public List getCodListaTipoMaterial() {
		return codListaTipoMaterial;
	}
	public void setCodListaTipoMaterial(List codListaTipoMaterial) {
		this.codListaTipoMaterial = codListaTipoMaterial;
	}
	public List getListaTipoMaterial() {
		return listaTipoMaterial;
	}
	public void setListaTipoMaterial(List listaTipoMaterial) {
		this.listaTipoMaterial = listaTipoMaterial;
	}
	public String getCodBusquedaBy() {
		return codBusquedaBy;
	}
	public void setCodBusquedaBy(String codBusquedaBy) {
		this.codBusquedaBy = codBusquedaBy;
	}
	public List getCodListaBusquedaBy() {
		return codListaBusquedaBy;
	}
	public void setCodListaBusquedaBy(List codListaBusquedaBy) {
		this.codListaBusquedaBy = codListaBusquedaBy;
	}
	public String getCodOrdenarBy() {
		return codOrdenarBy;
	}
	public void setCodOrdenarBy(String codOrdenarBy) {
		this.codOrdenarBy = codOrdenarBy;
	}
	public List getCodListaOrdenarBy() {
		return codListaOrdenarBy;
	}
	public void setCodListaOrdenarBy(List codListaOrdenarBy) {
		this.codListaOrdenarBy = codListaOrdenarBy;
	}
	public String getTxtTexto() {
		return txtTexto;
	}
	public void setTxtTexto(String txtTexto) {
		this.txtTexto = txtTexto;
	}
	public String getSelSala() {
		return selSala;
	}
	public void setSelSala(String selSala) {
		this.selSala = selSala;
	}
	public String getSelDomicilio() {
		return selDomicilio;
	}
	public void setSelDomicilio(String selDomicilio) {
		this.selDomicilio = selDomicilio;
	}
	public String getConsteSala() {
		return consteSala;
	}
	public void setConsteSala(String consteSala) {
		this.consteSala = consteSala;
	}
	public String getConsteDomicilio() {
		return consteDomicilio;
	}
	public void setConsteDomicilio(String consteDomicilio) {
		this.consteDomicilio = consteDomicilio;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	
}
