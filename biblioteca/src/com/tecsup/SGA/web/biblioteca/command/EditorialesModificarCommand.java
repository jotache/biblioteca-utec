package com.tecsup.SGA.web.biblioteca.command;

public class EditorialesModificarCommand {
	private String operacion;
	private String descripcionEditorial;
	private String codDetalle;
	private String usucrea;
	private String usuModi;
	private String CodProceso;
	private String DscProceso;
	private String msg;
	private String codigoEditorial;
    private String codEvaluador;
    private String codPeriodo;
    private String descripDetalle;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getUsucrea() {
		return usucrea;
	}
	public void setUsucrea(String usucrea) {
		this.usucrea = usucrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getCodProceso() {
		return CodProceso;
	}
	public void setCodProceso(String codProceso) {
		CodProceso = codProceso;
	}
	public String getDscProceso() {
		return DscProceso;
	}
	public void setDscProceso(String dscProceso) {
		DscProceso = dscProceso;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDescripcionEditorial() {
		return descripcionEditorial;
	}
	public void setDescripcionEditorial(String descripcionEditorial) {
		this.descripcionEditorial = descripcionEditorial;
	}
	public String getCodigoEditorial() {
		return codigoEditorial;
	}
	public void setCodigoEditorial(String codigoEditorial) {
		this.codigoEditorial = codigoEditorial;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getDescripDetalle() {
		return descripDetalle;
	}
	public void setDescripDetalle(String descripDetalle) {
		this.descripDetalle = descripDetalle;
	}
	
}
