package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class BandejaBusquedaSimpleCommand {
 
	private String operacion;
	private String codUsuario;
	private String codPeriodo;
	private String selSala;
	private String selDomicilio;
	private String codTipoMaterial;
	private String txtTexto;
	private String codBuscarBy;
	private String codOrdenarBy;
	private String codSede;
	private List codListaTipoMaterial;
	private List codListaOrdenarBy;
	private List codListaBuscarBy;
	private List codListaSede;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getSelSala() {
		return selSala;
	}
	public void setSelSala(String selSala) {
		this.selSala = selSala;
	}
	public String getSelDomicilio() {
		return selDomicilio;
	}
	public void setSelDomicilio(String selDomicilio) {
		this.selDomicilio = selDomicilio;
	}
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public String getTxtTexto() {
		return txtTexto;
	}
	public void setTxtTexto(String txtTexto) {
		this.txtTexto = txtTexto;
	}
	public String getCodBuscarBy() {
		return codBuscarBy;
	}
	public void setCodBuscarBy(String codBuscarBy) {
		this.codBuscarBy = codBuscarBy;
	}
	public String getCodOrdenarBy() {
		return codOrdenarBy;
	}
	public void setCodOrdenarBy(String codOrdenarBy) {
		this.codOrdenarBy = codOrdenarBy;
	}
	public List getCodListaTipoMaterial() {
		return codListaTipoMaterial;
	}
	public void setCodListaTipoMaterial(List codListaTipoMaterial) {
		this.codListaTipoMaterial = codListaTipoMaterial;
	}
	public List getCodListaOrdenarBy() {
		return codListaOrdenarBy;
	}
	public void setCodListaOrdenarBy(List codListaOrdenarBy) {
		this.codListaOrdenarBy = codListaOrdenarBy;
	}
	public List getCodListaBuscarBy() {
		return codListaBuscarBy;
	}
	public void setCodListaBuscarBy(List codListaBuscarBy) {
		this.codListaBuscarBy = codListaBuscarBy;
	}
	public List getCodListaSede() {
		return codListaSede;
	}
	public void setCodListaSede(List codListaSede) {
		this.codListaSede = codListaSede;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	
}
