package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaAutorCommand {

	private String operacion;
	private String idRec;
	private String lstrURL;
	private String descripcion;
	private String codigo;
	private List listaAutores;
	private String codSelec;
	private String descripSelec;
	private String descripcionAutor;
	private String codigoAutor;
	private String codPeriodo;
	private String codEvaluador;
	private String codValor;
	private String msg;
	
	private String codPersona;
	private String codEmpresa;
	private String codTipo;
	private String codTipoAutor;
	
	public String getCodTipoAutor() {
		return codTipoAutor;
	}
	public void setCodTipoAutor(String codTipoAutor) {
		this.codTipoAutor = codTipoAutor;
	}
	public String getCodPersona() {
		return codPersona;
	}
	public void setCodPersona(String codPersona) {
		this.codPersona = codPersona;
	}
	public String getCodEmpresa() {
		return codEmpresa;
	}
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
	public String getDescripcionAutor() {
		return descripcionAutor;
	}
	public void setDescripcionAutor(String descripcionAutor) {
		this.descripcionAutor = descripcionAutor;
	}
	public String getCodigoAutor() {
		return codigoAutor;
	}
	public void setCodigoAutor(String codigoAutor) {
		this.codigoAutor = codigoAutor;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public List getListaAutores() {
		return listaAutores;
	}
	public void setListaAutores(List listaAutores) {
		this.listaAutores = listaAutores;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}

}
