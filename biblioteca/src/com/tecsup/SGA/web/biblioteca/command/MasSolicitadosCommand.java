package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class MasSolicitadosCommand {
	
	private List listaTipoMateriales;
	private List codListaTipoMaterial;
	private String codTipoMaterial;
	private String codEvaluador;
	private String codPeriodo;
	
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public List getListaTipoMateriales() {
		return listaTipoMateriales;
	}
	public void setListaTipoMateriales(List listaTipoMateriales) {
		this.listaTipoMateriales = listaTipoMateriales;
	}
	public List getCodListaTipoMaterial() {
		return codListaTipoMaterial;
	}
	public void setCodListaTipoMaterial(List codListaTipoMaterial) {
		this.codListaTipoMaterial = codListaTipoMaterial;
	}
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
}
