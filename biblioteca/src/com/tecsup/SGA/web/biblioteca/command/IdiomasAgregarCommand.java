package com.tecsup.SGA.web.biblioteca.command;

public class IdiomasAgregarCommand {
	private String codUsuario;
	private String operacion;	
    private String msg;
    private String codigo;
    private String dscIdioma;
    //**************************
    private String codigoSec;
    
	public String getDscIdioma() {
		return dscIdioma;
	}
	public void setDscIdioma(String dscIdioma) {
		this.dscIdioma = dscIdioma;
	}
	public String getCodigoSec() {
		return codigoSec;
	}
	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}	
}