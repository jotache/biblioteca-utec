package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaTipoProcedenciaCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	private String codigo;
	private String dscProcedencia;
	//*****************************
	private List listaProcedencia;
	private String tamListaProcedencia;
	//*****************************
	private String codigoSec;
	private String codigoProcedencia;
	private String descripcion;
	//*****************************
	private String seleccion;
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDscProcedencia() {
		return dscProcedencia;
	}
	public void setDscProcedencia(String dscProcedencia) {
		this.dscProcedencia = dscProcedencia;
	}
	public List getListaProcedencia() {
		return listaProcedencia;
	}
	public void setListaProcedencia(List listaProcedencia) {
		this.listaProcedencia = listaProcedencia;
	}
	public String getTamListaProcedencia() {
		return tamListaProcedencia;
	}
	public void setTamListaProcedencia(String tamListaProcedencia) {
		this.tamListaProcedencia = tamListaProcedencia;
	}
	public String getCodigoSec() {
		return codigoSec;
	}
	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}
	public String getCodigoProcedencia() {
		return codigoProcedencia;
	}
	public void setCodigoProcedencia(String codigoProcedencia) {
		this.codigoProcedencia = codigoProcedencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
	
}
