package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminGestionMaterialCommand {
	private String operacion;
	private String usuario;
	
	//pagina
	private String txtCodigo;
	private String txtNroIngreso;
	private String cboTipoMaterial;
	private String cboBuscarPor;
	private String txtTitulo;
	private String cboIdioma;
	private String cboAnioIni;
	private String cboAnioFin;
	private String txtFechaReservaIni;
	private String txtFechaReservaFin;
	private List lstResultado;
	private List lstTipoMaterial;
	private List lstBuscarPor;
	private List lstIdioma;
	private List lstAnio; 
	private String nomImagen;
	private String pathImagen;
	private String txhCodigoUnico;
	private String idEliminacion;
	private String tipoUsuario;
	
	/*************************/
	private String chkConsultaReserva;
	private String sedeSel;
	private String sedeUsuario;
	
	public String getIdEliminacion() {
		return idEliminacion;
	}

	public void setIdEliminacion(String idEliminacion) {
		this.idEliminacion = idEliminacion;
	}

	public String getTxhCodigoUnico() {
		return txhCodigoUnico;
	}

	public void setTxhCodigoUnico(String txhCodigoUnico) {
		this.txhCodigoUnico = txhCodigoUnico;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTxtCodigo() {
		return txtCodigo;
	}

	public void setTxtCodigo(String txtCodigo) {
		this.txtCodigo = txtCodigo;
	}

	public String getTxtNroIngreso() {
		return txtNroIngreso;
	}

	public void setTxtNroIngreso(String txtNroIngreso) {
		this.txtNroIngreso = txtNroIngreso;
	}

	public String getCboTipoMaterial() {
		return cboTipoMaterial;
	}

	public void setCboTipoMaterial(String cboTipoMaterial) {
		this.cboTipoMaterial = cboTipoMaterial;
	}

	public String getCboBuscarPor() {
		return cboBuscarPor;
	}

	public void setCboBuscarPor(String cboBuscarPor) {
		this.cboBuscarPor = cboBuscarPor;
	}

	public String getTxtTitulo() {
		return txtTitulo;
	}

	public void setTxtTitulo(String txtTitulo) {
		this.txtTitulo = txtTitulo;
	}

	public String getCboIdioma() {
		return cboIdioma;
	}

	public void setCboIdioma(String cboIdioma) {
		this.cboIdioma = cboIdioma;
	}

	public String getCboAnioIni() {
		return cboAnioIni;
	}

	public void setCboAnioIni(String cboAnioIni) {
		this.cboAnioIni = cboAnioIni;
	}

	public String getCboAnioFin() {
		return cboAnioFin;
	}

	public void setCboAnioFin(String cboAnioFin) {
		this.cboAnioFin = cboAnioFin;
	}

	public String getTxtFechaReservaIni() {
		return txtFechaReservaIni;
	}

	public void setTxtFechaReservaIni(String txtFechaReservaIni) {
		this.txtFechaReservaIni = txtFechaReservaIni;
	}

	public String getTxtFechaReservaFin() {
		return txtFechaReservaFin;
	}

	public void setTxtFechaReservaFin(String txtFechaReservaFin) {
		this.txtFechaReservaFin = txtFechaReservaFin;
	}

	public List getLstResultado() {
		return lstResultado;
	}

	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}

	public List getLstTipoMaterial() {
		return lstTipoMaterial;
	}

	public void setLstTipoMaterial(List lstTipoMaterial) {
		this.lstTipoMaterial = lstTipoMaterial;
	}

	public List getLstBuscarPor() {
		return lstBuscarPor;
	}

	public void setLstBuscarPor(List lstBuscarPor) {
		this.lstBuscarPor = lstBuscarPor;
	}

	public List getLstIdioma() {
		return lstIdioma;
	}

	public void setLstIdioma(List lstIdioma) {
		this.lstIdioma = lstIdioma;
	}

	public List getLstAnio() {
		return lstAnio;
	}

	public void setLstAnio(List lstAnio) {
		this.lstAnio = lstAnio;
	}

	public String getNomImagen() {
		return nomImagen;
	}

	public void setNomImagen(String nomImagen) {
		this.nomImagen = nomImagen;
	}

	public String getPathImagen() {
		return pathImagen;
	}

	public void setPathImagen(String pathImagen) {
		this.pathImagen = pathImagen;
	}

	public String getChkConsultaReserva() {
		return chkConsultaReserva;
	}

	public void setChkConsultaReserva(String chkConsultaReserva) {
		this.chkConsultaReserva = chkConsultaReserva;
	}

	/**
	 * @return the tipoUsuario
	 */
	public String getTipoUsuario() {
		return tipoUsuario;
	}

	/**
	 * @param tipoUsuario the tipoUsuario to set
	 */
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	/**
	 * @return the sedeSel
	 */
	public String getSedeSel() {
		return sedeSel;
	}

	/**
	 * @param sedeSel the sedeSel to set
	 */
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}

	/**
	 * @return the sedeUsuario
	 */
	public String getSedeUsuario() {
		return sedeUsuario;
	}

	/**
	 * @param sedeUsuario the sedeUsuario to set
	 */
	public void setSedeUsuario(String sedeUsuario) {
		this.sedeUsuario = sedeUsuario;
	}


	

}
