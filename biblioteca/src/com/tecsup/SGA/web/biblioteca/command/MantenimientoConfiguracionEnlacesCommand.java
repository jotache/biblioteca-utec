package com.tecsup.SGA.web.biblioteca.command;

public class MantenimientoConfiguracionEnlacesCommand {

	private String operacion;
	private String codUsuario;
	private String sedeSel;
	private String sedeUsuario;
	
	public String getSedeSel() {
		return sedeSel;
	}
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}
	public String getSedeUsuario() {
		return sedeUsuario;
	}
	public void setSedeUsuario(String sedeUsuario) {
		this.sedeUsuario = sedeUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
}
