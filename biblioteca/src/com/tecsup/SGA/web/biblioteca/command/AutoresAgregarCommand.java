package com.tecsup.SGA.web.biblioteca.command;

public class AutoresAgregarCommand {
	private String operacion;
	private String descripcion;
	private String codDetalle;
	private String usucrea;
	private String usuModi;
    private String msg;
    private String codigo;
    private String codPeriodo;
    private String codEvaluador;
    
    private String codPersona;
	private String codEmpresa;
	private String codTipo;
    
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public String getCodPersona() {
		return codPersona;
	}
	public void setCodPersona(String codPersona) {
		this.codPersona = codPersona;
	}
	public String getCodEmpresa() {
		return codEmpresa;
	}
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getUsucrea() {
		return usucrea;
	}
	public void setUsucrea(String usucrea) {
		this.usucrea = usucrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
}
