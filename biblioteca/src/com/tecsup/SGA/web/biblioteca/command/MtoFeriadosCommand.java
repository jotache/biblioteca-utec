package com.tecsup.SGA.web.biblioteca.command;
import java.util.List;
public class MtoFeriadosCommand {

	private String operacion;
	private String msg;
	private String idRec;
	private String lstrURL;
	private String dia;
	private String mes;
	private String ano;
	private List fecha;
	private String codEval;
	private String mess;
	private String anio;
	private String cod;
	private String descripcion;
	private String eliminar;
	private List Meses;
	private List anios;
	private String meseses;
	private String aniosos;
	private List codListaMeses;
	private List codListaAnios;
	private String codUsuario;
	private String sede;
	
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public List getFecha() {
		return fecha;
	}
	public void setFecha(List fecha) {
		this.fecha = fecha;
	}
	public String getMess() {
		return mess;
	}
	public void setMess(String mess) {
		this.mess = mess;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEliminar() {
		return eliminar;
	}
	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}
	public List getMeses() {
		return Meses;
	}
	public void setMeses(List meses) {
		Meses = meses;
	}
	public List getAnios() {
		return anios;
	}
	public void setAnios(List anios) {
		this.anios = anios;
	}
	public String getMeseses() {
		return meseses;
	}
	public void setMeseses(String meseses) {
		this.meseses = meseses;
	}
	public String getAniosos() {
		return aniosos;
	}
	public void setAniosos(String aniosos) {
		this.aniosos = aniosos;
	}
	public List getCodListaMeses() {
		return codListaMeses;
	}
	public void setCodListaMeses(List codListaMeses) {
		this.codListaMeses = codListaMeses;
	}
	public List getCodListaAnios() {
		return codListaAnios;
	}
	public void setCodListaAnios(List codListaAnios) {
		this.codListaAnios = codListaAnios;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}

}
