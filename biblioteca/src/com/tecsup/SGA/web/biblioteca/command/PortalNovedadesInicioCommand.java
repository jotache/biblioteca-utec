package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class PortalNovedadesInicioCommand {
	
	private String CodEvaluador;
	private String CodPeriodo;
	private List listaNovedades;

	public List getListaNovedades() {
		return listaNovedades;
	}

	public void setListaNovedades(List listaNovedades) {
		this.listaNovedades = listaNovedades;
	}

	public String getCodEvaluador() {
		return CodEvaluador;
	}

	public void setCodEvaluador(String codEvaluador) {
		CodEvaluador = codEvaluador;
	}

	public String getCodPeriodo() {
		return CodPeriodo;
	}

	public void setCodPeriodo(String codPeriodo) {
		CodPeriodo = codPeriodo;
	}

}
