package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class DeweyAgregarCommand {
	private String operacion;
	private String descripcionDewey;
	private String codDetalle;
	private String usucrea;
	private String usuModi;
    private String msg;
    private String codigo;
    private List codListaCategoria;
    private String codCategoria;
    private String codigoDewey;
    private String codPeriodo;
    private String codEvaluador;
    
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getUsucrea() {
		return usucrea;
	}
	public void setUsucrea(String usucrea) {
		this.usucrea = usucrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDescripcionDewey() {
		return descripcionDewey;
	}
	public void setDescripcionDewey(String descripcionDewey) {
		this.descripcionDewey = descripcionDewey;
	}
	public List getCodListaCategoria() {
		return codListaCategoria;
	}
	public void setCodListaCategoria(List codListaCategoria) {
		this.codListaCategoria = codListaCategoria;
	}
	public String getCodCategoria() {
		return codCategoria;
	}
	public void setCodCategoria(String codCategoria) {
		this.codCategoria = codCategoria;
	}
	public String getCodigoDewey() {
		return codigoDewey;
	}
	public void setCodigoDewey(String codigoDewey) {
		this.codigoDewey = codigoDewey;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	
}
