package com.tecsup.SGA.web.biblioteca.command;

public class AdminRegistrarPrestamoDatosUsuarioCommand {
	private String operacion;
	private String usuario;
	private String txhCodUsuario;
	private String txtNombres;
	private String txtTipoUsuario;
	private String txtEspecialidad;
	private String txtCiclo;
	private String txtCargo;
	
	private String txhIndTipoUsuario;
	private String txhCodTipoUsuario;
	
	public String getTxhIndTipoUsuario() {
		return txhIndTipoUsuario;
	}
	public void setTxhIndTipoUsuario(String txhIndTipoUsuario) {
		this.txhIndTipoUsuario = txhIndTipoUsuario;
	}
	public String getTxhCodTipoUsuario() {
		return txhCodTipoUsuario;
	}
	public void setTxhCodTipoUsuario(String txhCodTipoUsuario) {
		this.txhCodTipoUsuario = txhCodTipoUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTxhCodUsuario() {
		return txhCodUsuario;
	}
	public void setTxhCodUsuario(String txhCodUsuario) {
		this.txhCodUsuario = txhCodUsuario;
	}
	public String getTxtNombres() {
		return txtNombres;
	}
	public void setTxtNombres(String txtNombres) {
		this.txtNombres = txtNombres;
	}
	public String getTxtTipoUsuario() {
		return txtTipoUsuario;
	}
	public void setTxtTipoUsuario(String txtTipoUsuario) {
		this.txtTipoUsuario = txtTipoUsuario;
	}
	public String getTxtEspecialidad() {
		return txtEspecialidad;
	}
	public void setTxtEspecialidad(String txtEspecialidad) {
		this.txtEspecialidad = txtEspecialidad;
	}
	public String getTxtCiclo() {
		return txtCiclo;
	}
	public void setTxtCiclo(String txtCiclo) {
		this.txtCiclo = txtCiclo;
	}
	public String getTxtCargo() {
		return txtCargo;
	}
	public void setTxtCargo(String txtCargo) {
		this.txtCargo = txtCargo;
	}
	
}
