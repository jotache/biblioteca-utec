package com.tecsup.SGA.web.biblioteca.command;

public class DescriptoresModificarCommand {
	private String operacion;
	private String usucrea;
	private String usuModi;
	private String msg;
	private String codigoDescriptor;
	private String descripcionDescriptor;
	private String codEvaluador;
	private String codPeriodo;
	private String codValor;
	private String descripSelec;
	private String codSelec;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public String getUsucrea() {
		return usucrea;
	}
	public void setUsucrea(String usucrea) {
		this.usucrea = usucrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodigoDescriptor() {
		return codigoDescriptor;
	}
	public void setCodigoDescriptor(String codigoDescriptor) {
		this.codigoDescriptor = codigoDescriptor;
	}
	public String getDescripcionDescriptor() {
		return descripcionDescriptor;
	}
	public void setDescripcionDescriptor(String descripcionDescriptor) {
		this.descripcionDescriptor = descripcionDescriptor;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	
}
