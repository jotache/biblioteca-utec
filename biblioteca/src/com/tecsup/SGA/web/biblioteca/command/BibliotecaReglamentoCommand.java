package com.tecsup.SGA.web.biblioteca.command;

public class BibliotecaReglamentoCommand {

	private String descripReglamento;

	public String getDescripReglamento() {
		return descripReglamento;
	}

	public void setDescripReglamento(String descripReglamento) {
		this.descripReglamento = descripReglamento;
	}
}
