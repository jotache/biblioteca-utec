package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaSeccionesCommand {

	private String operacion;
	private String msg;
	private String seleccion;
	private String opcion;
	private String codNovedades;
	private String codUsuario;
	//*****************************	
	private List listaSeccion;
	private String codSeccion;
	private List listaGrupo;
	private String codGrupo;
	//*****************************
	private String codUnico;
	//*************************************
	private String codEvaluador;
	private String codPeriodo;
	//*************************************
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getCodNovedades() {
		return codNovedades;
	}
	public void setCodNovedades(String codNovedades) {
		this.codNovedades = codNovedades;
	}
	public List getListaSeccion() {
		return listaSeccion;
	}
	public void setListaSeccion(List listaSeccion) {
		this.listaSeccion = listaSeccion;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public List getListaGrupo() {
		return listaGrupo;
	}
	public void setListaGrupo(List listaGrupo) {
		this.listaGrupo = listaGrupo;
	}
	public String getCodGrupo() {
		return codGrupo;
	}
	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	 
}
