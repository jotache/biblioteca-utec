package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaEditorialCommand {

	private String operacion;
	private String idRec;
	private String lstrURL;
	private List listaEditoriales;
	private String codSelec;
	private String descripSelec;
	private String codValor;
	private String codEvaluador;
	private String codPeriodo;
	private String msg;
	private String codigoEditorial;
	private String descripcionEditorial;
	
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List getListaEditoriales() {
		return listaEditoriales;
	}
	public void setListaEditoriales(List listaEditoriales) {
		this.listaEditoriales = listaEditoriales;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getCodigoEditorial() {
		return codigoEditorial;
	}
	public void setCodigoEditorial(String codigoEditorial) {
		this.codigoEditorial = codigoEditorial;
	}
	public String getDescripcionEditorial() {
		return descripcionEditorial;
	}
	public void setDescripcionEditorial(String descripcionEditorial) {
		this.descripcionEditorial = descripcionEditorial;
	}

}
