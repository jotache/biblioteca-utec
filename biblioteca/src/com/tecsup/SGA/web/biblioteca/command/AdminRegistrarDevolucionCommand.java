package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

import com.tecsup.SGA.modelo.Sancion;

public class AdminRegistrarDevolucionCommand {

	
	private String operacion;
	private String txtNroIngreso;
	private String txtTipomaterial;
	private String txtTitulo;
	private String txtFecPrestamo;
	private String txtFecDevolucionProrroga;
	private String chkIndicaProrroga;
	private String txtUsuariologin;
	private String txtUsuarioNombre;
	private String txtFecDevolucion;
	private String txtSancionado;
	private String txtCodSancion;
	
	private String usuario;
	private String chkIndProrroga;
	private String txhCodMaterial;
	private String txhTipo;
	private String txhFlgDevolucion;
	private String txhCodUnico;
	private String txtMensaje;
	private String txhFlgSancion;
	private String txhFlgPrestado;
	private String txtCodDewey;
	private String txhFlgReserva;
	private String chkIndPrestamo;//RBN ULTIMO
	private String txhCodUsuarioLogin;
	private String codPrestamoMultiple;
	
	//PARAEMTROS DE BUSQUEDA DE LA PANTALLA CONUSLTA DE PMATERIAL BIBLIOGRAFICO
	private String prmTxtCodigo;
	private String prmTxtNroIngreso;
	private String prmCboTipoMaterial;
	private String prmCboBuscarPor;
	private String prmTxtTitulo;
	private String prmCboIdioma;
	private String prmCboAnioIni;
	private String prmCboAnioFin;
	private String prmTxtFechaReservaIni;
	private String prmTxtFechaReservaFin;
	private String txhFecHoy;
	
	private String tipoUsuario;

	private String sedeSel;
	private String sedeUsuario;
	private String txhSedeMat;
	private List<Sancion> listaSancionesUsuario;
	private String codUsuario;
	private String idSancion;
	private List listaSuspenciones;
	private String codSancionesSeleccionados;
	private String nroSelec;
	private String fecIniInhabi;
	private String fecFinInhabi;
	private String obsInhabi;
	
	private String totalProrroga;
	private String flagProrroga;
	
	private String txtCadenaDatos;
	private String txtTamanioLista;
	
	public String getFlagProrroga() {
		return flagProrroga;
	}
	public void setFlagProrroga(String flagProrroga) {
		this.flagProrroga = flagProrroga;
	}
	public String getTotalProrroga() {
		return totalProrroga;
	}
	public void setTotalProrroga(String totalProrroga) {
		this.totalProrroga = totalProrroga;
	}
	public String getObsInhabi() {
		return obsInhabi;
	}
	public void setObsInhabi(String obsInhabi) {
		this.obsInhabi = obsInhabi;
	}
	public String getTxhFecHoy() {
		return txhFecHoy;
	}
	public void setTxhFecHoy(String txhFecHoy) {
		this.txhFecHoy = txhFecHoy;
	}
	public String getPrmTxtCodigo() {
		return prmTxtCodigo;
	}
	public void setPrmTxtCodigo(String prmTxtCodigo) {
		this.prmTxtCodigo = prmTxtCodigo;
	}
	public String getPrmTxtNroIngreso() {
		return prmTxtNroIngreso;
	}
	public void setPrmTxtNroIngreso(String prmTxtNroIngreso) {
		this.prmTxtNroIngreso = prmTxtNroIngreso;
	}
	public String getPrmCboTipoMaterial() {
		return prmCboTipoMaterial;
	}
	public void setPrmCboTipoMaterial(String prmCboTipoMaterial) {
		this.prmCboTipoMaterial = prmCboTipoMaterial;
	}
	public String getPrmCboBuscarPor() {
		return prmCboBuscarPor;
	}
	public void setPrmCboBuscarPor(String prmCboBuscarPor) {
		this.prmCboBuscarPor = prmCboBuscarPor;
	}
	public String getPrmTxtTitulo() {
		return prmTxtTitulo;
	}
	public void setPrmTxtTitulo(String prmTxtTitulo) {
		this.prmTxtTitulo = prmTxtTitulo;
	}
	public String getPrmCboIdioma() {
		return prmCboIdioma;
	}
	public void setPrmCboIdioma(String prmCboIdioma) {
		this.prmCboIdioma = prmCboIdioma;
	}
	public String getPrmCboAnioIni() {
		return prmCboAnioIni;
	}
	public void setPrmCboAnioIni(String prmCboAnioIni) {
		this.prmCboAnioIni = prmCboAnioIni;
	}
	public String getPrmCboAnioFin() {
		return prmCboAnioFin;
	}
	public void setPrmCboAnioFin(String prmCboAnioFin) {
		this.prmCboAnioFin = prmCboAnioFin;
	}
	public String getPrmTxtFechaReservaIni() {
		return prmTxtFechaReservaIni;
	}
	public void setPrmTxtFechaReservaIni(String prmTxtFechaReservaIni) {
		this.prmTxtFechaReservaIni = prmTxtFechaReservaIni;
	}
	public String getPrmTxtFechaReservaFin() {
		return prmTxtFechaReservaFin;
	}
	public void setPrmTxtFechaReservaFin(String prmTxtFechaReservaFin) {
		this.prmTxtFechaReservaFin = prmTxtFechaReservaFin;
	}
	public String getTxhFlgPrestado() {
		return txhFlgPrestado;
	}
	public void setTxhFlgPrestado(String txhFlgPrestado) {
		this.txhFlgPrestado = txhFlgPrestado;
	}
	public String getTxhFlgSancion() {
		return txhFlgSancion;
	}
	public void setTxhFlgSancion(String txhFlgSancion) {
		this.txhFlgSancion = txhFlgSancion;
	}
	public String getTxtMensaje() {
		return txtMensaje;
	}
	public void setTxtMensaje(String txtMensaje) {
		this.txtMensaje = txtMensaje;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTxtNroIngreso() {
		return txtNroIngreso;
	}
	public void setTxtNroIngreso(String txtNroIngreso) {
		this.txtNroIngreso = txtNroIngreso;
	}
	public String getTxtTipomaterial() {
		return txtTipomaterial;
	}
	public void setTxtTipomaterial(String txtTipomaterial) {
		this.txtTipomaterial = txtTipomaterial;
	}
	public String getTxtTitulo() {
		return txtTitulo;
	}
	public void setTxtTitulo(String txtTitulo) {
		this.txtTitulo = txtTitulo;
	}
	public String getTxtFecPrestamo() {
		return txtFecPrestamo;
	}
	public void setTxtFecPrestamo(String txtFecPrestamo) {
		this.txtFecPrestamo = txtFecPrestamo;
	}
	public String getTxtFecDevolucionProrroga() {
		return txtFecDevolucionProrroga;
	}
	public void setTxtFecDevolucionProrroga(String txtFecDevolucionProrroga) {
		this.txtFecDevolucionProrroga = txtFecDevolucionProrroga;
	}
	public String getChkIndicaProrroga() {
		return chkIndicaProrroga;
	}
	public void setChkIndicaProrroga(String chkIndicaProrroga) {
		this.chkIndicaProrroga = chkIndicaProrroga;
	}
	public String getTxtUsuariologin() {
		return txtUsuariologin;
	}
	public void setTxtUsuariologin(String txtUsuariologin) {
		this.txtUsuariologin = txtUsuariologin;
	}
	public String getTxtUsuarioNombre() {
		return txtUsuarioNombre;
	}
	public void setTxtUsuarioNombre(String txtUsuarioNombre) {
		this.txtUsuarioNombre = txtUsuarioNombre;
	}
	public String getTxtFecDevolucion() {
		return txtFecDevolucion;
	}
	public void setTxtFecDevolucion(String txtFecDevolucion) {
		this.txtFecDevolucion = txtFecDevolucion;
	}
	public String getTxtSancionado() {
		return txtSancionado;
	}
	public void setTxtSancionado(String txtSancionado) {
		this.txtSancionado = txtSancionado;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getChkIndProrroga() {
		return chkIndProrroga;
	}
	public void setChkIndProrroga(String chkIndProrroga) {
		this.chkIndProrroga = chkIndProrroga;
	}
	public String getTxhCodMaterial() {
		return txhCodMaterial;
	}
	public void setTxhCodMaterial(String txhCodMaterial) {
		this.txhCodMaterial = txhCodMaterial;
	}
	public String getTxhTipo() {
		return txhTipo;
	}
	public void setTxhTipo(String txhTipo) {
		this.txhTipo = txhTipo;
	}
	public String getTxhFlgDevolucion() {
		return txhFlgDevolucion;
	}
	public void setTxhFlgDevolucion(String txhFlgDevolucion) {
		this.txhFlgDevolucion = txhFlgDevolucion;
	}
	public String getTxhCodUnico() {
		return txhCodUnico;
	}
	public void setTxhCodUnico(String txhCodUnico) {
		this.txhCodUnico = txhCodUnico;
	}
	public String getTxtCodDewey() {
		return txtCodDewey;
	}
	public void setTxtCodDewey(String txtCodDewey) {
		this.txtCodDewey = txtCodDewey;
	}
	public String getTxhFlgReserva() {
		return txhFlgReserva;
	}
	public void setTxhFlgReserva(String txhFlgReserva) {
		this.txhFlgReserva = txhFlgReserva;
	}
	public String getChkIndPrestamo() {
		return chkIndPrestamo;
	}
	public void setChkIndPrestamo(String chkIndPrestamo) {
		this.chkIndPrestamo = chkIndPrestamo;
	}
	public String getTxhCodUsuarioLogin() {
		return txhCodUsuarioLogin;
	}
	public void setTxhCodUsuarioLogin(String txhCodUsuarioLogin) {
		this.txhCodUsuarioLogin = txhCodUsuarioLogin;
	}
	/**
	 * @return the tipoUsuario
	 */
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	/**
	 * @param tipoUsuario the tipoUsuario to set
	 */
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	/**
	 * @return the sedeSel
	 */
	public String getSedeSel() {
		return sedeSel;
	}
	/**
	 * @param sedeSel the sedeSel to set
	 */
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}
	/**
	 * @return the sedeUsuario
	 */
	public String getSedeUsuario() {
		return sedeUsuario;
	}
	/**
	 * @param sedeUsuario the sedeUsuario to set
	 */
	public void setSedeUsuario(String sedeUsuario) {
		this.sedeUsuario = sedeUsuario;
	}
	/**
	 * @return the txhSedeMat
	 */
	public String getTxhSedeMat() {
		return txhSedeMat;
	}
	/**
	 * @param txhSedeMat the txhSedeMat to set
	 */
	public void setTxhSedeMat(String txhSedeMat) {
		this.txhSedeMat = txhSedeMat;
	}
	/**
	 * @return the listaSancionesUsuario
	 */
	public List<Sancion> getListaSancionesUsuario() {
		return listaSancionesUsuario;
	}
	/**
	 * @param listaSancionesUsuario the listaSancionesUsuario to set
	 */
	public void setListaSancionesUsuario(List<Sancion> listaSancionesUsuario) {
		this.listaSancionesUsuario = listaSancionesUsuario;
	}
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	/**
	 * @return the idSancion
	 */
	public String getIdSancion() {
		return idSancion;
	}
	/**
	 * @param idSancion the idSancion to set
	 */
	public void setIdSancion(String idSancion) {
		this.idSancion = idSancion;
	}
	/**
	 * @return the listaSuspenciones
	 */
	public List getListaSuspenciones() {
		return listaSuspenciones;
	}
	/**
	 * @param listaSuspenciones the listaSuspenciones to set
	 */
	public void setListaSuspenciones(List listaSuspenciones) {
		this.listaSuspenciones = listaSuspenciones;
	}
	/**
	 * @return the codSancionesSeleccionados
	 */
	public String getCodSancionesSeleccionados() {
		return codSancionesSeleccionados;
	}
	/**
	 * @param codSancionesSeleccionados the codSancionesSeleccionados to set
	 */
	public void setCodSancionesSeleccionados(String codSancionesSeleccionados) {
		this.codSancionesSeleccionados = codSancionesSeleccionados;
	}
	/**
	 * @return the nroSelec
	 */
	public String getNroSelec() {
		return nroSelec;
	}
	/**
	 * @param nroSelec the nroSelec to set
	 */
	public void setNroSelec(String nroSelec) {
		this.nroSelec = nroSelec;
	}
	/**
	 * @return the fecIniInhabi
	 */
	public String getFecIniInhabi() {
		return fecIniInhabi;
	}
	/**
	 * @param fecIniInhabi the fecIniInhabi to set
	 */
	public void setFecIniInhabi(String fecIniInhabi) {
		this.fecIniInhabi = fecIniInhabi;
	}
	/**
	 * @return the fecFinInhabi
	 */
	public String getFecFinInhabi() {
		return fecFinInhabi;
	}
	/**
	 * @param fecFinInhabi the fecFinInhabi to set
	 */
	public void setFecFinInhabi(String fecFinInhabi) {
		this.fecFinInhabi = fecFinInhabi;
	}
	public String getCodPrestamoMultiple() {
		return codPrestamoMultiple;
	}
	public void setCodPrestamoMultiple(String codPrestamoMultiple) {
		this.codPrestamoMultiple = codPrestamoMultiple;
	}
	public String getTxtCodSancion() {
		return txtCodSancion;
	}
	public void setTxtCodSancion(String txtCodSancion) {
		this.txtCodSancion = txtCodSancion;
	}
	public String getTxtCadenaDatos() {
		return txtCadenaDatos;
	}
	public void setTxtCadenaDatos(String txtCadenaDatos) {
		this.txtCadenaDatos = txtCadenaDatos;
	}
	public String getTxtTamanioLista() {
		return txtTamanioLista;
	}
	public void setTxtTamanioLista(String txtTamanioLista) {
		this.txtTamanioLista = txtTamanioLista;
	}

}
