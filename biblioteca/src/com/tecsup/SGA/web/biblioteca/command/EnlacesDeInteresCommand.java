package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class EnlacesDeInteresCommand {
	
	 private String codEvaluador;
	 private String codPeriodo;
	 private String codTipoMaterial;
	 private List codListaTipoMaterial;
	 private List listaSecciones;
	 private String operacion;
	 
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public List getCodListaTipoMaterial() {
		return codListaTipoMaterial;
	}
	public void setCodListaTipoMaterial(List codListaTipoMaterial) {
		this.codListaTipoMaterial = codListaTipoMaterial;
	}
	public List getListaSecciones() {
		return listaSecciones;
	}
	public void setListaSecciones(List listaSecciones) {
		this.listaSecciones = listaSecciones;
	}
	
}
