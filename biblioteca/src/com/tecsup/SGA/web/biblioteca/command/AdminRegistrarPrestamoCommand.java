package com.tecsup.SGA.web.biblioteca.command;

public class AdminRegistrarPrestamoCommand {
	private String operacion;
	private String txtNroMat;
	private String usuario;
	private String txtNroIngreso;
	private String txtNroIngreso1;
	private String txtNroIngreso2;
	private String txtNroIngreso3;
	private String txtNroIngreso4;
	private String txtNroIngreso5;
	
	private String txtTipomaterial;
	
	private String txtTipomaterial1;
	private String txtTipomaterial2;
	private String txtTipomaterial3;
	private String txtTipomaterial4;
	private String txtTipomaterial5;
	
	private String txtTitulo;
	private String txtTitulo1;
	private String txtTitulo2;
	private String txtTitulo3;
	private String txtTitulo4;
	private String txtTitulo5;
	
	private String txtFecPrestamo;
	private String txtFecDevolucionProrroga;
	private String txtFecDevolucionProrroga2;
	//private String chkIndicaProrroga;
	private String txtUsuariologin;
	private String txtUsuarioNombre;
	private String txtFecDevolucion;
	private String chkIndProrroga;
	private String txhCodMaterial;
	private String txhCodMaterial1;
	private String txhCodMaterial2;
	private String txhCodMaterial3;
	private String txhCodMaterial4;
	private String txhCodMaterial5;
	
	private String txhTipo;
	//private String txhFlgPrestamo;	
	private String txhCodUnico;
	private String txhCodUnico1;
	private String txhCodUnico2;
	private String txhCodUnico3;
	private String txhCodUnico4;
	private String txhCodUnico5;
	private String txtMensaje;
	private String txhFlgSancion;
	private String txhFlgSancion1;
	private String txhFlgSancion2;
	private String txhFlgSancion3;
	private String txhFlgSancion4;
	private String txhFlgSancion5;
	private String txhFlgPrestado;
	private String txhFlgPrestado1;
	private String txhFlgPrestado2;
	private String txhFlgPrestado3;
	private String txhFlgPrestado4;
	private String txhFlgPrestado5;
	
	private String txhFlgPermitido;     //flag usuario tiene permiso prestamo a domicilio
	private String txhFlgPermitidoSala; //flag usuario tiene permiso prestamo en sala
	private String txtSancionado;	
	private String txhNroIngreso;
	private String txhNroIngreso1;
	private String txhNroIngreso2;
	private String txhNroIngreso3;
	private String txhNroIngreso4;
	private String txhNroIngreso5;
	private String chkIndPrestamo;	
	//PARAEMTROS DE BUSQUEDA DE LA PANTALLA CONUSLTA DE PMATERIAL BIBLIOGRAFICO
	private String prmTxtCodigo;
	private String prmTxtNroIngreso;
	private String prmCboTipoMaterial;
	private String prmCboBuscarPor;
	private String prmTxtTitulo;
	private String prmCboIdioma;
	private String prmCboAnioIni;
	private String prmCboAnioFin;
	private String prmTxtFechaReservaIni;
	private String prmTxtFechaReservaFin;	
	private String txhFlgReserva; //recibe el cod de la reserva en caso el material est� reservado
	private String txhFlgReserva1;
	private String txhFlgReserva2;
	private String txhFlgReserva3;
	private String txhFlgReserva4;
	private String txhFlgReserva5;
	private String txtCodUsuarioReserva;//codigo del usuario que reserva
	private String txtCodUsuarioReserva1;
	private String txtCodUsuarioReserva2;
	private String txtCodUsuarioReserva3;
	private String txtCodUsuarioReserva4;
	private String txtCodUsuarioReserva5;
	private String txhFlgMatIndCasa;
	private String txhFlgMatIndCasa1;
	private String txhFlgMatIndCasa2;
	private String txhFlgMatIndCasa3;
	private String txhFlgMatIndCasa4;
	private String txhFlgMatIndCasa5;
	private String txhFlgMatIndSala;
	private String txhFlgMatIndSala1;
	private String txhFlgMatIndSala2;
	private String txhFlgMatIndSala3;
	private String txhFlgMatIndSala4;
	private String txhFlgMatIndSala5;
	private String txhFlgLibroRetirado;
	private String txhFlgLibroRetirado1;
	private String txhFlgLibroRetirado2;
	private String txhFlgLibroRetirado3;
	private String txhFlgLibroRetirado4;
	private String txhFlgLibroRetirado5;
	private String txhSedeMat;
	private String txhSedeMat1;
	private String txhSedeMat2;
	private String txhSedeMat3;
	private String txhSedeMat4;
	private String txhSedeMat5;
	private String txhSedeUsu;	
	private String txhCodUsuario;
	private String txhCodDewey;
	private String txhCodDewey1;
	private String txhCodDewey2;
	private String txhCodDewey3;
	private String txhCodDewey4;
	private String txhCodDewey5;
	private String tipousuario="0"; //jhpr 2008-08-15
	private String flgBusqXCodOracle ; //flag para indicar que la busqueda se realizar� por c�digo oracle.
	private String fechaFinSancion;	
	private String sedeSel;
	private String sedeUsuario;	
	private String txhFlgInhabilitado;
	private String txhFlgInhabilitado1;
	private String txhFlgInhabilitado2;
	private String txhFlgInhabilitado3;
	private String txhFlgInhabilitado4;
	private String txhFlgInhabilitado5;
	private String fechaFinInhabilitado;	
	private String nroIngresoReal;
	private String nroIngresoReal1;
	private String nroIngresoReal2;
	private String nroIngresoReal3;
	private String nroIngresoReal4;
	private String nroIngresoReal5;
	private String estadoUsuario;
	private Long codPrestamoMultiple;
	private String txtNroPrestamos; 
	private String txtLimitePrestamos;
	
	public String getEstadoUsuario() {
		return estadoUsuario;
	}

	public void setEstadoUsuario(String estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}

	public String getFlgBusqXCodOracle() {
		return flgBusqXCodOracle;
	}

	public void setFlgBusqXCodOracle(String flgBusqXCodOracle) {
		this.flgBusqXCodOracle = flgBusqXCodOracle;
	}

	public String getTipousuario() {
		return tipousuario;
	}

	public void setTipousuario(String tipousuario) {
		this.tipousuario = tipousuario;
	}

	public String getTxhCodDewey() {
		return txhCodDewey;
	}

	public void setTxhCodDewey(String txhCodDewey) {
		this.txhCodDewey = txhCodDewey;
	}

	public String getTxhTipo() {
		return txhTipo;
	}

	public void setTxhTipo(String txhTipo) {
		this.txhTipo = txhTipo;
	}

	public String getTxhCodMaterial() {
		return txhCodMaterial;
	}

	public void setTxhCodMaterial(String txhCodMaterial) {
		this.txhCodMaterial = txhCodMaterial;
	}

	public String getChkIndProrroga() {
		return chkIndProrroga;
	}

	public void setChkIndProrroga(String chkIndProrroga) {
		this.chkIndProrroga = chkIndProrroga;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getTxtNroIngreso() {
		return txtNroIngreso;
	}

	public void setTxtNroIngreso(String txtNroIngreso) {
		this.txtNroIngreso = txtNroIngreso;
	}

	public String getTxtTipomaterial() {
		return txtTipomaterial;
	}

	public void setTxtTipomaterial(String txtTipomaterial) {
		this.txtTipomaterial = txtTipomaterial;
	}

	public String getTxtTitulo() {
		return txtTitulo;
	}

	public void setTxtTitulo(String txtTitulo) {
		this.txtTitulo = txtTitulo;
	}

	public String getTxtFecPrestamo() {
		return txtFecPrestamo;
	}

	public void setTxtFecPrestamo(String txtFecPrestamo) {
		this.txtFecPrestamo = txtFecPrestamo;
	}

	public String getTxtFecDevolucionProrroga() {
		return txtFecDevolucionProrroga;
	}

	public void setTxtFecDevolucionProrroga(String txtFecDevolucionProrroga) {
		this.txtFecDevolucionProrroga = txtFecDevolucionProrroga;
	}
	public String getTxtUsuariologin() {
		return txtUsuariologin;
	}

	public void setTxtUsuariologin(String txtUsuariologin) {
		this.txtUsuariologin = txtUsuariologin;
	}

	public String getTxtUsuarioNombre() {
		return txtUsuarioNombre;
	}

	public void setTxtUsuarioNombre(String txtUsuarioNombre) {
		this.txtUsuarioNombre = txtUsuarioNombre;
	}

	public String getTxtFecDevolucion() {
		return txtFecDevolucion;
	}

	public void setTxtFecDevolucion(String txtFecDevolucion) {
		this.txtFecDevolucion = txtFecDevolucion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTxhCodUnico() {
		return txhCodUnico;
	}

	public void setTxhCodUnico(String txhCodUnico) {
		this.txhCodUnico = txhCodUnico;
	}

	public String getTxtMensaje() {
		return txtMensaje;
	}

	public void setTxtMensaje(String txtMensaje) {
		this.txtMensaje = txtMensaje;
	}

	public String getTxhFlgSancion() {
		return txhFlgSancion;
	}

	public void setTxhFlgSancion(String txhFlgSancion) {
		this.txhFlgSancion = txhFlgSancion;
	}

	public String getTxhFlgPrestado() {
		return txhFlgPrestado;
	}

	public void setTxhFlgPrestado(String txhFlgPrestado) {
		this.txhFlgPrestado = txhFlgPrestado;
	}

	public String getTxhNroIngreso() {
		return txhNroIngreso;
	}

	public void setTxhNroIngreso(String txhNroIngreso) {
		this.txhNroIngreso = txhNroIngreso;
	}

	public String getTxhFlgPermitido() {
		return txhFlgPermitido;
	}

	public void setTxhFlgPermitido(String txhFlgPermitido) {
		this.txhFlgPermitido = txhFlgPermitido;
	}

	public String getTxtSancionado() {
		return txtSancionado;
	}

	public void setTxtSancionado(String txtSancionado) {
		this.txtSancionado = txtSancionado;
	}

	public String getPrmTxtCodigo() {
		return prmTxtCodigo;
	}

	public void setPrmTxtCodigo(String prmTxtCodigo) {
		this.prmTxtCodigo = prmTxtCodigo;
	}

	public String getPrmTxtNroIngreso() {
		return prmTxtNroIngreso;
	}

	public void setPrmTxtNroIngreso(String prmTxtNroIngreso) {
		this.prmTxtNroIngreso = prmTxtNroIngreso;
	}

	public String getPrmCboTipoMaterial() {
		return prmCboTipoMaterial;
	}

	public void setPrmCboTipoMaterial(String prmCboTipoMaterial) {
		this.prmCboTipoMaterial = prmCboTipoMaterial;
	}

	public String getPrmCboBuscarPor() {
		return prmCboBuscarPor;
	}

	public void setPrmCboBuscarPor(String prmCboBuscarPor) {
		this.prmCboBuscarPor = prmCboBuscarPor;
	}

	public String getPrmTxtTitulo() {
		return prmTxtTitulo;
	}

	public void setPrmTxtTitulo(String prmTxtTitulo) {
		this.prmTxtTitulo = prmTxtTitulo;
	}

	public String getPrmCboIdioma() {
		return prmCboIdioma;
	}

	public void setPrmCboIdioma(String prmCboIdioma) {
		this.prmCboIdioma = prmCboIdioma;
	}

	public String getPrmCboAnioIni() {
		return prmCboAnioIni;
	}

	public void setPrmCboAnioIni(String prmCboAnioIni) {
		this.prmCboAnioIni = prmCboAnioIni;
	}

	public String getPrmCboAnioFin() {
		return prmCboAnioFin;
	}

	public void setPrmCboAnioFin(String prmCboAnioFin) {
		this.prmCboAnioFin = prmCboAnioFin;
	}

	public String getPrmTxtFechaReservaIni() {
		return prmTxtFechaReservaIni;
	}

	public void setPrmTxtFechaReservaIni(String prmTxtFechaReservaIni) {
		this.prmTxtFechaReservaIni = prmTxtFechaReservaIni;
	}

	public String getPrmTxtFechaReservaFin() {
		return prmTxtFechaReservaFin;
	}

	public void setPrmTxtFechaReservaFin(String prmTxtFechaReservaFin) {
		this.prmTxtFechaReservaFin = prmTxtFechaReservaFin;
	}

	public String getChkIndPrestamo() {
		return chkIndPrestamo;
	}

	public void setChkIndPrestamo(String chkIndPrestamo) {
		this.chkIndPrestamo = chkIndPrestamo;
	}

	public String getTxhFlgReserva() {
		return txhFlgReserva;
	}

	public void setTxhFlgReserva(String txhFlgReserva) {
		this.txhFlgReserva = txhFlgReserva;
	}

	public String getTxtCodUsuarioReserva() {
		return txtCodUsuarioReserva;
	}

	public void setTxtCodUsuarioReserva(String txtCodUsuarioReserva) {
		this.txtCodUsuarioReserva = txtCodUsuarioReserva;
	}

	
	public String getTxhFlgMatIndCasa() {
		return txhFlgMatIndCasa;
	}

	public void setTxhFlgMatIndCasa(String txhFlgMatIndCasa) {
		this.txhFlgMatIndCasa = txhFlgMatIndCasa;
	}

	public String getTxhFlgMatIndSala() {
		return txhFlgMatIndSala;
	}

	public void setTxhFlgMatIndSala(String txhFlgMatIndSala) {
		this.txhFlgMatIndSala = txhFlgMatIndSala;
	}

	public String getTxhFlgPermitidoSala() {
		return txhFlgPermitidoSala;
	}

	
	public void setTxhFlgPermitidoSala(String txhFlgPermitidoSala) {
		this.txhFlgPermitidoSala = txhFlgPermitidoSala;
	}

	public String getTxhFlgLibroRetirado() {
		return txhFlgLibroRetirado;
	}

	public void setTxhFlgLibroRetirado(String txhFlgLibroRetirado) {
		this.txhFlgLibroRetirado = txhFlgLibroRetirado;
	}

	public String getTxhSedeMat() {
		return txhSedeMat;
	}

	public void setTxhSedeMat(String txhSedeMat) {
		this.txhSedeMat = txhSedeMat;
	}

	public String getTxhSedeUsu() {
		return txhSedeUsu;
	}

	public void setTxhSedeUsu(String txhSedeUsu) {
		this.txhSedeUsu = txhSedeUsu;
	}

	public String getTxhCodUsuario() {
		return txhCodUsuario;
	}

	public void setTxhCodUsuario(String txhCodUsuario) {
		this.txhCodUsuario = txhCodUsuario;
	}

	public String getFechaFinSancion() {
		return fechaFinSancion;
	}

	public void setFechaFinSancion(String fechaFinSancion) {
		this.fechaFinSancion = fechaFinSancion;
	}

	/**
	 * @return the sedeSel
	 */
	public String getSedeSel() {
		return sedeSel;
	}

	/**
	 * @param sedeSel the sedeSel to set
	 */
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}

	/**
	 * @return the sedeUsuario
	 */
	public String getSedeUsuario() {
		return sedeUsuario;
	}

	/**
	 * @param sedeUsuario the sedeUsuario to set
	 */
	public void setSedeUsuario(String sedeUsuario) {
		this.sedeUsuario = sedeUsuario;
	}

	public String getTxhFlgInhabilitado() {
		return txhFlgInhabilitado;
	}

	public void setTxhFlgInhabilitado(String txhFlgInhabilitado) {
		this.txhFlgInhabilitado = txhFlgInhabilitado;
	}

	public String getFechaFinInhabilitado() {
		return fechaFinInhabilitado;
	}

	public void setFechaFinInhabilitado(String fechaFinInhabilitado) {
		this.fechaFinInhabilitado = fechaFinInhabilitado;
	}

	public String getNroIngresoReal() {
		return nroIngresoReal;
	}

	public void setNroIngresoReal(String nroIngresoReal) {
		this.nroIngresoReal = nroIngresoReal;
	}
	
	public String getTxtFecDevolucionProrroga2() {
		return txtFecDevolucionProrroga2;
	}

	public void setTxtFecDevolucionProrroga2(String txtFecDevolucionProrroga2) {
		this.txtFecDevolucionProrroga2 = txtFecDevolucionProrroga2;
	}

	public String getTxtTipomaterial1() {
		return txtTipomaterial1;
	}

	public void setTxtTipomaterial1(String txtTipomaterial1) {
		this.txtTipomaterial1 = txtTipomaterial1;
	}

	public String getTxtTipomaterial2() {
		return txtTipomaterial2;
	}

	public void setTxtTipomaterial2(String txtTipomaterial2) {
		this.txtTipomaterial2 = txtTipomaterial2;
	}

	public String getTxtTipomaterial3() {
		return txtTipomaterial3;
	}

	public void setTxtTipomaterial3(String txtTipomaterial3) {
		this.txtTipomaterial3 = txtTipomaterial3;
	}

	public String getTxtTipomaterial4() {
		return txtTipomaterial4;
	}

	public void setTxtTipomaterial4(String txtTipomaterial4) {
		this.txtTipomaterial4 = txtTipomaterial4;
	}

	public String getTxtTipomaterial5() {
		return txtTipomaterial5;
	}

	public void setTxtTipomaterial5(String txtTipomaterial5) {
		this.txtTipomaterial5 = txtTipomaterial5;
	}

	public String getTxtTitulo1() {
		return txtTitulo1;
	}

	public void setTxtTitulo1(String txtTitulo1) {
		this.txtTitulo1 = txtTitulo1;
	}

	public String getTxtTitulo2() {
		return txtTitulo2;
	}

	public void setTxtTitulo2(String txtTitulo2) {
		this.txtTitulo2 = txtTitulo2;
	}

	public String getTxtTitulo3() {
		return txtTitulo3;
	}

	public void setTxtTitulo3(String txtTitulo3) {
		this.txtTitulo3 = txtTitulo3;
	}

	public String getTxtTitulo4() {
		return txtTitulo4;
	}

	public void setTxtTitulo4(String txtTitulo4) {
		this.txtTitulo4 = txtTitulo4;
	}

	public String getTxtTitulo5() {
		return txtTitulo5;
	}

	public void setTxtTitulo5(String txtTitulo5) {
		this.txtTitulo5 = txtTitulo5;
	}

	public String getTxhCodMaterial1() {
		return txhCodMaterial1;
	}

	public void setTxhCodMaterial1(String txhCodMaterial1) {
		this.txhCodMaterial1 = txhCodMaterial1;
	}

	public String getTxhCodMaterial2() {
		return txhCodMaterial2;
	}

	public void setTxhCodMaterial2(String txhCodMaterial2) {
		this.txhCodMaterial2 = txhCodMaterial2;
	}

	public String getTxhCodMaterial3() {
		return txhCodMaterial3;
	}

	public void setTxhCodMaterial3(String txhCodMaterial3) {
		this.txhCodMaterial3 = txhCodMaterial3;
	}

	public String getTxhCodMaterial4() {
		return txhCodMaterial4;
	}

	public void setTxhCodMaterial4(String txhCodMaterial4) {
		this.txhCodMaterial4 = txhCodMaterial4;
	}

	public String getTxhCodMaterial5() {
		return txhCodMaterial5;
	}

	public void setTxhCodMaterial5(String txhCodMaterial5) {
		this.txhCodMaterial5 = txhCodMaterial5;
	}

	public String getTxhNroIngreso1() {
		return txhNroIngreso1;
	}

	public void setTxhNroIngreso1(String txhNroIngreso1) {
		this.txhNroIngreso1 = txhNroIngreso1;
	}

	public String getTxhNroIngreso2() {
		return txhNroIngreso2;
	}

	public void setTxhNroIngreso2(String txhNroIngreso2) {
		this.txhNroIngreso2 = txhNroIngreso2;
	}

	public String getTxhNroIngreso3() {
		return txhNroIngreso3;
	}

	public void setTxhNroIngreso3(String txhNroIngreso3) {
		this.txhNroIngreso3 = txhNroIngreso3;
	}

	public String getTxhNroIngreso4() {
		return txhNroIngreso4;
	}

	public void setTxhNroIngreso4(String txhNroIngreso4) {
		this.txhNroIngreso4 = txhNroIngreso4;
	}

	public String getTxhNroIngreso5() {
		return txhNroIngreso5;
	}

	public void setTxhNroIngreso5(String txhNroIngreso5) {
		this.txhNroIngreso5 = txhNroIngreso5;
	}

	public String getTxhCodUnico1() {
		return txhCodUnico1;
	}

	public void setTxhCodUnico1(String txhCodUnico1) {
		this.txhCodUnico1 = txhCodUnico1;
	}

	public String getTxhCodUnico2() {
		return txhCodUnico2;
	}

	public void setTxhCodUnico2(String txhCodUnico2) {
		this.txhCodUnico2 = txhCodUnico2;
	}

	public String getTxhCodUnico3() {
		return txhCodUnico3;
	}

	public void setTxhCodUnico3(String txhCodUnico3) {
		this.txhCodUnico3 = txhCodUnico3;
	}

	public String getTxhCodUnico4() {
		return txhCodUnico4;
	}

	public void setTxhCodUnico4(String txhCodUnico4) {
		this.txhCodUnico4 = txhCodUnico4;
	}

	public String getTxhCodUnico5() {
		return txhCodUnico5;
	}

	public void setTxhCodUnico5(String txhCodUnico5) {
		this.txhCodUnico5 = txhCodUnico5;
	}

	public String getTxhFlgPrestado1() {
		return txhFlgPrestado1;
	}

	public void setTxhFlgPrestado1(String txhFlgPrestado1) {
		this.txhFlgPrestado1 = txhFlgPrestado1;
	}

	public String getTxhFlgPrestado2() {
		return txhFlgPrestado2;
	}

	public void setTxhFlgPrestado2(String txhFlgPrestado2) {
		this.txhFlgPrestado2 = txhFlgPrestado2;
	}

	public String getTxhFlgPrestado3() {
		return txhFlgPrestado3;
	}

	public void setTxhFlgPrestado3(String txhFlgPrestado3) {
		this.txhFlgPrestado3 = txhFlgPrestado3;
	}

	public String getTxhFlgPrestado4() {
		return txhFlgPrestado4;
	}

	public void setTxhFlgPrestado4(String txhFlgPrestado4) {
		this.txhFlgPrestado4 = txhFlgPrestado4;
	}

	public String getTxhFlgPrestado5() {
		return txhFlgPrestado5;
	}

	public void setTxhFlgPrestado5(String txhFlgPrestado5) {
		this.txhFlgPrestado5 = txhFlgPrestado5;
	}

	public String getTxhFlgReserva1() {
		return txhFlgReserva1;
	}

	public void setTxhFlgReserva1(String txhFlgReserva1) {
		this.txhFlgReserva1 = txhFlgReserva1;
	}

	public String getTxhFlgReserva2() {
		return txhFlgReserva2;
	}

	public void setTxhFlgReserva2(String txhFlgReserva2) {
		this.txhFlgReserva2 = txhFlgReserva2;
	}

	public String getTxhFlgReserva3() {
		return txhFlgReserva3;
	}

	public void setTxhFlgReserva3(String txhFlgReserva3) {
		this.txhFlgReserva3 = txhFlgReserva3;
	}

	public String getTxhFlgReserva4() {
		return txhFlgReserva4;
	}

	public void setTxhFlgReserva4(String txhFlgReserva4) {
		this.txhFlgReserva4 = txhFlgReserva4;
	}

	public String getTxhFlgReserva5() {
		return txhFlgReserva5;
	}

	public void setTxhFlgReserva5(String txhFlgReserva5) {
		this.txhFlgReserva5 = txhFlgReserva5;
	}

	public String getTxtCodUsuarioReserva1() {
		return txtCodUsuarioReserva1;
	}

	public void setTxtCodUsuarioReserva1(String txtCodUsuarioReserva1) {
		this.txtCodUsuarioReserva1 = txtCodUsuarioReserva1;
	}

	public String getTxtCodUsuarioReserva2() {
		return txtCodUsuarioReserva2;
	}

	public void setTxtCodUsuarioReserva2(String txtCodUsuarioReserva2) {
		this.txtCodUsuarioReserva2 = txtCodUsuarioReserva2;
	}

	public String getTxtCodUsuarioReserva3() {
		return txtCodUsuarioReserva3;
	}

	public void setTxtCodUsuarioReserva3(String txtCodUsuarioReserva3) {
		this.txtCodUsuarioReserva3 = txtCodUsuarioReserva3;
	}

	public String getTxtCodUsuarioReserva4() {
		return txtCodUsuarioReserva4;
	}

	public void setTxtCodUsuarioReserva4(String txtCodUsuarioReserva4) {
		this.txtCodUsuarioReserva4 = txtCodUsuarioReserva4;
	}

	public String getTxtCodUsuarioReserva5() {
		return txtCodUsuarioReserva5;
	}

	public void setTxtCodUsuarioReserva5(String txtCodUsuarioReserva5) {
		this.txtCodUsuarioReserva5 = txtCodUsuarioReserva5;
	}

	public String getTxhFlgMatIndCasa1() {
		return txhFlgMatIndCasa1;
	}

	public void setTxhFlgMatIndCasa1(String txhFlgMatIndCasa1) {
		this.txhFlgMatIndCasa1 = txhFlgMatIndCasa1;
	}

	public String getTxhFlgMatIndCasa2() {
		return txhFlgMatIndCasa2;
	}

	public void setTxhFlgMatIndCasa2(String txhFlgMatIndCasa2) {
		this.txhFlgMatIndCasa2 = txhFlgMatIndCasa2;
	}

	public String getTxhFlgMatIndCasa3() {
		return txhFlgMatIndCasa3;
	}

	public void setTxhFlgMatIndCasa3(String txhFlgMatIndCasa3) {
		this.txhFlgMatIndCasa3 = txhFlgMatIndCasa3;
	}

	public String getTxhFlgMatIndCasa4() {
		return txhFlgMatIndCasa4;
	}

	public void setTxhFlgMatIndCasa4(String txhFlgMatIndCasa4) {
		this.txhFlgMatIndCasa4 = txhFlgMatIndCasa4;
	}

	public String getTxhFlgMatIndCasa5() {
		return txhFlgMatIndCasa5;
	}

	public void setTxhFlgMatIndCasa5(String txhFlgMatIndCasa5) {
		this.txhFlgMatIndCasa5 = txhFlgMatIndCasa5;
	}

	public String getTxhFlgMatIndSala1() {
		return txhFlgMatIndSala1;
	}

	public void setTxhFlgMatIndSala1(String txhFlgMatIndSala1) {
		this.txhFlgMatIndSala1 = txhFlgMatIndSala1;
	}

	public String getTxhFlgMatIndSala2() {
		return txhFlgMatIndSala2;
	}

	public void setTxhFlgMatIndSala2(String txhFlgMatIndSala2) {
		this.txhFlgMatIndSala2 = txhFlgMatIndSala2;
	}

	public String getTxhFlgMatIndSala3() {
		return txhFlgMatIndSala3;
	}

	public void setTxhFlgMatIndSala3(String txhFlgMatIndSala3) {
		this.txhFlgMatIndSala3 = txhFlgMatIndSala3;
	}

	public String getTxhFlgMatIndSala4() {
		return txhFlgMatIndSala4;
	}

	public void setTxhFlgMatIndSala4(String txhFlgMatIndSala4) {
		this.txhFlgMatIndSala4 = txhFlgMatIndSala4;
	}

	public String getTxhFlgMatIndSala5() {
		return txhFlgMatIndSala5;
	}

	public void setTxhFlgMatIndSala5(String txhFlgMatIndSala5) {
		this.txhFlgMatIndSala5 = txhFlgMatIndSala5;
	}

	public String getTxhFlgLibroRetirado1() {
		return txhFlgLibroRetirado1;
	}

	public void setTxhFlgLibroRetirado1(String txhFlgLibroRetirado1) {
		this.txhFlgLibroRetirado1 = txhFlgLibroRetirado1;
	}

	public String getTxhFlgLibroRetirado2() {
		return txhFlgLibroRetirado2;
	}

	public void setTxhFlgLibroRetirado2(String txhFlgLibroRetirado2) {
		this.txhFlgLibroRetirado2 = txhFlgLibroRetirado2;
	}

	public String getTxhFlgLibroRetirado3() {
		return txhFlgLibroRetirado3;
	}

	public void setTxhFlgLibroRetirado3(String txhFlgLibroRetirado3) {
		this.txhFlgLibroRetirado3 = txhFlgLibroRetirado3;
	}

	public String getTxhFlgLibroRetirado4() {
		return txhFlgLibroRetirado4;
	}

	public void setTxhFlgLibroRetirado4(String txhFlgLibroRetirado4) {
		this.txhFlgLibroRetirado4 = txhFlgLibroRetirado4;
	}

	public String getTxhFlgLibroRetirado5() {
		return txhFlgLibroRetirado5;
	}

	public void setTxhFlgLibroRetirado5(String txhFlgLibroRetirado5) {
		this.txhFlgLibroRetirado5 = txhFlgLibroRetirado5;
	}

	public String getTxhSedeMat1() {
		return txhSedeMat1;
	}

	public void setTxhSedeMat1(String txhSedeMat1) {
		this.txhSedeMat1 = txhSedeMat1;
	}

	public String getTxhSedeMat2() {
		return txhSedeMat2;
	}

	public void setTxhSedeMat2(String txhSedeMat2) {
		this.txhSedeMat2 = txhSedeMat2;
	}

	public String getTxhSedeMat3() {
		return txhSedeMat3;
	}

	public void setTxhSedeMat3(String txhSedeMat3) {
		this.txhSedeMat3 = txhSedeMat3;
	}

	public String getTxhSedeMat4() {
		return txhSedeMat4;
	}

	public void setTxhSedeMat4(String txhSedeMat4) {
		this.txhSedeMat4 = txhSedeMat4;
	}

	public String getTxhSedeMat5() {
		return txhSedeMat5;
	}

	public void setTxhSedeMat5(String txhSedeMat5) {
		this.txhSedeMat5 = txhSedeMat5;
	}

	public String getTxhCodDewey1() {
		return txhCodDewey1;
	}

	public void setTxhCodDewey1(String txhCodDewey1) {
		this.txhCodDewey1 = txhCodDewey1;
	}

	public String getTxhCodDewey2() {
		return txhCodDewey2;
	}

	public void setTxhCodDewey2(String txhCodDewey2) {
		this.txhCodDewey2 = txhCodDewey2;
	}

	public String getTxhCodDewey3() {
		return txhCodDewey3;
	}

	public void setTxhCodDewey3(String txhCodDewey3) {
		this.txhCodDewey3 = txhCodDewey3;
	}

	public String getTxhCodDewey4() {
		return txhCodDewey4;
	}

	public void setTxhCodDewey4(String txhCodDewey4) {
		this.txhCodDewey4 = txhCodDewey4;
	}

	public String getTxhCodDewey5() {
		return txhCodDewey5;
	}

	public void setTxhCodDewey5(String txhCodDewey5) {
		this.txhCodDewey5 = txhCodDewey5;
	}

	public String getTxhFlgSancion1() {
		return txhFlgSancion1;
	}

	public void setTxhFlgSancion1(String txhFlgSancion1) {
		this.txhFlgSancion1 = txhFlgSancion1;
	}

	public String getTxhFlgSancion2() {
		return txhFlgSancion2;
	}

	public void setTxhFlgSancion2(String txhFlgSancion2) {
		this.txhFlgSancion2 = txhFlgSancion2;
	}

	public String getTxhFlgSancion3() {
		return txhFlgSancion3;
	}

	public void setTxhFlgSancion3(String txhFlgSancion3) {
		this.txhFlgSancion3 = txhFlgSancion3;
	}

	public String getTxhFlgSancion4() {
		return txhFlgSancion4;
	}

	public void setTxhFlgSancion4(String txhFlgSancion4) {
		this.txhFlgSancion4 = txhFlgSancion4;
	}

	public String getTxhFlgSancion5() {
		return txhFlgSancion5;
	}

	public void setTxhFlgSancion5(String txhFlgSancion5) {
		this.txhFlgSancion5 = txhFlgSancion5;
	}

	public String getTxhFlgInhabilitado1() {
		return txhFlgInhabilitado1;
	}

	public void setTxhFlgInhabilitado1(String txhFlgInhabilitado1) {
		this.txhFlgInhabilitado1 = txhFlgInhabilitado1;
	}

	public String getTxhFlgInhabilitado2() {
		return txhFlgInhabilitado2;
	}

	public void setTxhFlgInhabilitado2(String txhFlgInhabilitado2) {
		this.txhFlgInhabilitado2 = txhFlgInhabilitado2;
	}

	public String getTxhFlgInhabilitado3() {
		return txhFlgInhabilitado3;
	}

	public void setTxhFlgInhabilitado3(String txhFlgInhabilitado3) {
		this.txhFlgInhabilitado3 = txhFlgInhabilitado3;
	}

	public String getTxhFlgInhabilitado4() {
		return txhFlgInhabilitado4;
	}

	public void setTxhFlgInhabilitado4(String txhFlgInhabilitado4) {
		this.txhFlgInhabilitado4 = txhFlgInhabilitado4;
	}

	public String getTxhFlgInhabilitado5() {
		return txhFlgInhabilitado5;
	}

	public void setTxhFlgInhabilitado5(String txhFlgInhabilitado5) {
		this.txhFlgInhabilitado5 = txhFlgInhabilitado5;
	}

	public String getNroIngresoReal1() {
		return nroIngresoReal1;
	}

	public void setNroIngresoReal1(String nroIngresoReal1) {
		this.nroIngresoReal1 = nroIngresoReal1;
	}

	public String getNroIngresoReal2() {
		return nroIngresoReal2;
	}

	public void setNroIngresoReal2(String nroIngresoReal2) {
		this.nroIngresoReal2 = nroIngresoReal2;
	}

	public String getNroIngresoReal3() {
		return nroIngresoReal3;
	}

	public void setNroIngresoReal3(String nroIngresoReal3) {
		this.nroIngresoReal3 = nroIngresoReal3;
	}

	public String getNroIngresoReal4() {
		return nroIngresoReal4;
	}

	public void setNroIngresoReal4(String nroIngresoReal4) {
		this.nroIngresoReal4 = nroIngresoReal4;
	}

	public String getNroIngresoReal5() {
		return nroIngresoReal5;
	}

	public void setNroIngresoReal5(String nroIngresoReal5) {
		this.nroIngresoReal5 = nroIngresoReal5;
	}

	public String getTxtNroIngreso1() {
		return txtNroIngreso1;
	}

	public void setTxtNroIngreso1(String txtNroIngreso1) {
		this.txtNroIngreso1 = txtNroIngreso1;
	}

	public String getTxtNroIngreso2() {
		return txtNroIngreso2;
	}

	public void setTxtNroIngreso2(String txtNroIngreso2) {
		this.txtNroIngreso2 = txtNroIngreso2;
	}

	public String getTxtNroIngreso3() {
		return txtNroIngreso3;
	}

	public void setTxtNroIngreso3(String txtNroIngreso3) {
		this.txtNroIngreso3 = txtNroIngreso3;
	}

	public String getTxtNroIngreso4() {
		return txtNroIngreso4;
	}

	public void setTxtNroIngreso4(String txtNroIngreso4) {
		this.txtNroIngreso4 = txtNroIngreso4;
	}

	public String getTxtNroIngreso5() {
		return txtNroIngreso5;
	}

	public void setTxtNroIngreso5(String txtNroIngreso5) {
		this.txtNroIngreso5 = txtNroIngreso5;
	}

	public String getTxtNroMat() {
		return txtNroMat;
	}

	public void setTxtNroMat(String txtNroMat) {
		this.txtNroMat = txtNroMat;
	}

	public Long getCodPrestamoMultiple() {
		return codPrestamoMultiple;
	}

	public void setCodPrestamoMultiple(Long codPrestamoMultiple) {
		this.codPrestamoMultiple = codPrestamoMultiple;
	}

	public String getTxtNroPrestamos() {
		return txtNroPrestamos;
	}

	public void setTxtNroPrestamos(String txtNroPrestamos) {
		this.txtNroPrestamos = txtNroPrestamos;
	}

	public String getTxtLimitePrestamos() {
		return txtLimitePrestamos;
	}

	public void setTxtLimitePrestamos(String txtLimitePrestamos) {
		this.txtLimitePrestamos = txtLimitePrestamos;
	}
	
		
}
