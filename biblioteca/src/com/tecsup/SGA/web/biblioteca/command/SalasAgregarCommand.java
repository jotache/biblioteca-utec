package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

import com.tecsup.SGA.modelo.Sala;

public class SalasAgregarCommand {
	private String codUsuario;
	private String operacion;	
    private String msg;
    private String codigo;
    private String dscSala;
    private String capacidad;
    //**************************
    private String codigoSec;
    
    private String tipoSala;
    private List listaTipoSala;
    private String codEstado;
    private String fechaInicio;
    private String fechaFin;
    private String sede;

    private String codHoraInicio1;    
    private String codHoraInicio2;
    
    private List<Sala> listaHoras;
    
	public String getTipoSala() {
		return tipoSala;
	}
	public void setTipoSala(String tipoSala) {
		this.tipoSala = tipoSala;
	}
	public List getListaTipoSala() {
		return listaTipoSala;
	}
	public void setListaTipoSala(List listaTipoSala) {
		this.listaTipoSala = listaTipoSala;
	}
	public String getDscSala() {
		return dscSala;
	}
	public void setDscSala(String dscSala) {
		this.dscSala = dscSala;
	}
	public String getCodigoSec() {
		return codigoSec;
	}
	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getCodHoraInicio1() {
		return codHoraInicio1;
	}
	public void setCodHoraInicio1(String codHoraInicio1) {
		this.codHoraInicio1 = codHoraInicio1;
	}
	
	public String getCodHoraInicio2() {
		return codHoraInicio2;
	}
	public void setCodHoraInicio2(String codHoraInicio2) {
		this.codHoraInicio2 = codHoraInicio2;
	}
	
	public List<Sala> getListaHoras() {
		return listaHoras;
	}
	public void setListaHoras(List<Sala> listaHoras) {
		this.listaHoras = listaHoras;
	}	
}