package com.tecsup.SGA.web.biblioteca.command;

public class PortalReservaMaterialBibliograficoCommand {

	private String txtFecha;
	private String codEvaluador;
	private String codPeriodo;
	private String txtFechaBD;
	private String msg;
	private String codTipoMaterial;
	private String operacion;
	
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTxtFecha() {
		return txtFecha;
	}
	public void setTxtFecha(String txtFecha) {
		this.txtFecha = txtFecha;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getTxtFechaBD() {
		return txtFechaBD;
	}
	public void setTxtFechaBD(String txtFechaBD) {
		this.txtFechaBD = txtFechaBD;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
}
