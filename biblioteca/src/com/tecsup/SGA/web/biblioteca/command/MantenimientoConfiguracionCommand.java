package com.tecsup.SGA.web.biblioteca.command;

public class MantenimientoConfiguracionCommand {

	private String operacion;	
	private String codUsuario;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
}
