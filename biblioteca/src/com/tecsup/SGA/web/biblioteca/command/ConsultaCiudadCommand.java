package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaCiudadCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	//*****************************
	private String pais;
	private String dscCiudad;
	private List listaPais;
	private List listaCiudad;
	//*****************************
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDscCiudad() {
		return dscCiudad;
	}
	public void setDscCiudad(String dscCiudad) {
		this.dscCiudad = dscCiudad;
	}
	public List getListaPais() {
		return listaPais;
	}
	public void setListaPais(List listaPais) {
		this.listaPais = listaPais;
	}
	public List getListaCiudad() {
		return listaCiudad;
	}
	public void setListaCiudad(List listaCiudad) {
		this.listaCiudad = listaCiudad;
	}
}
