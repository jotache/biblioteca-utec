package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

import com.tecsup.SGA.modelo.Sala;

public class AtencionCommand {
	private String accion = "inicio";
	private String fechaAtencion;
	private String sede;
	private String horaIni;
	private String horaFin;
	private String usuario;
	private String nomUsuario;
	private String codUsuario;
	private List<Sala> listaRecursos;
	private List listaComputadoras;
	private String codPc;
	private String rangos;
	private String tipoMensaje;
	private String mensaje;
	
	private List<Sala> listaAtencionesPorPc;
	private List<Sala> listaAtencionesPorPcHistorico;
	private String nombrePC;
	private String idAtencion;
	private String pestania;
	
	private String HoraActual;
	private String minutoActual;
	private String segundoActual;
	
	private String codTipoSala;
	private List codListaTipoSala;
	
	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(String fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public String getHoraIni() {
		return horaIni;
	}

	public void setHoraIni(String horaIni) {
		this.horaIni = horaIni;
	}

	public String getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNomUsuario() {
		return nomUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public List<Sala> getListaRecursos() {
		return listaRecursos;
	}

	public void setListaRecursos(List<Sala> listaRecursos) {
		this.listaRecursos = listaRecursos;
	}

	public List getListaComputadoras() {
		return listaComputadoras;
	}

	public void setListaComputadoras(List listaComputadoras) {
		this.listaComputadoras = listaComputadoras;
	}

	public String getCodPc() {
		return codPc;
	}

	public void setCodPc(String codPc) {
		this.codPc = codPc;
	}

	public String getRangos() {
		return rangos;
	}

	public void setRangos(String rangos) {
		this.rangos = rangos;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<Sala> getListaAtencionesPorPc() {
		return listaAtencionesPorPc;
	}

	public void setListaAtencionesPorPc(List<Sala> listaAtencionesPorPc) {
		this.listaAtencionesPorPc = listaAtencionesPorPc;
	}

	public String getNombrePC() {
		return nombrePC;
	}

	public void setNombrePC(String nombrePC) {
		this.nombrePC = nombrePC;
	}

	public String getIdAtencion() {
		return idAtencion;
	}

	public void setIdAtencion(String idAtencion) {
		this.idAtencion = idAtencion;
	}

	public List<Sala> getListaAtencionesPorPcHistorico() {
		return listaAtencionesPorPcHistorico;
	}

	public void setListaAtencionesPorPcHistorico(
			List<Sala> listaAtencionesPorPcHistorico) {
		this.listaAtencionesPorPcHistorico = listaAtencionesPorPcHistorico;
	}

	public String getPestania() {
		return pestania;
	}

	public void setPestania(String pestania) {
		this.pestania = pestania;
	}

	public String getHoraActual() {
		return HoraActual;
	}

	public void setHoraActual(String horaActual) {
		HoraActual = horaActual;
	}

	public String getMinutoActual() {
		return minutoActual;
	}

	public void setMinutoActual(String minutoActual) {
		this.minutoActual = minutoActual;
	}

	public String getSegundoActual() {
		return segundoActual;
	}

	public void setSegundoActual(String segundoActual) {
		this.segundoActual = segundoActual;
	}

	public String getCodTipoSala() {
		return codTipoSala;
	}

	public void setCodTipoSala(String codTipoSala) {
		this.codTipoSala = codTipoSala;
	}

	public List getCodListaTipoSala() {
		return codListaTipoSala;
	}

	public void setCodListaTipoSala(List codListaTipoSala) {
		this.codListaTipoSala = codListaTipoSala;
	}

	


}
