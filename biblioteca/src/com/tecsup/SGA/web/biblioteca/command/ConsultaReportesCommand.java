package com.tecsup.SGA.web.biblioteca.command;

import java.util.*;

public class ConsultaReportesCommand {
	private String codUsuario;
	private String operacion;
	private String msg;
	//*****************************
	private String cboReportes;
	private List listaReportes;
	
	private String cboMaterial;
	private List listaMaterial;
	//*****************************
	private String cbotipoRepMatBib;
	private List listaTipoRepMatBib;
	//*****************************
	private String cboTipoRepMovPresDev;
	private List listaTipoRepMovPresDev;
	//*****************************
	private String cboTipoRepMovReserDev;
	private List listaTipoRepMovReserDev;
	
	//*****************************
	private String cboTipoRepCicloEsp;
	private List listaTipoRepCicloEsp;
	//*****************************	
	private String cboTipoUsuarios;
	private List listaTipoUsuarios;
	//*****************************
	private List listaMeses;
	private List listaAnios;
	
	private String cboSedeRep;
	private List listaSedeRep;
	
	private String cboPeriodoSancion;
	private String cboAnioSancion;
	
	private String cboPeriodoBuzon;
	private String cboAnioBuzon;
	
	//*****************************
	
	private String txhReporte1;
	private String txhReporte2;
	private String txhReporte3;
	private String txhReporte4;
	private String txhReporte5;
	private String txhReporte6;//JHPR 2008-7-3
	private String txhReporte7;
	private String txhReporte8;
	

	private String txhRep2A;
	private String txhRep2B;
	private String txhRep2C;
	private String txhRep2D;
	private String txhRep2E;
	private String txhRep2F;
	private String txhRep2G;
	
	private String txhCodUsuarioRep;
	private String txhUsernameRep;
	private String sedeSel;
	private List lstProcedencia;
	private String cboProcedencia;
	private List lstTipoResultado;
	private String codTipoResultado;
	
	private String nroIngresoIni;
	private String nroIngresoFin;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCboReportes() {
		return cboReportes;
	}
	public void setCboReportes(String cboReportes) {
		this.cboReportes = cboReportes;
	}
	public List getListaReportes() {
		return listaReportes;
	}
	public void setListaReportes(List listaReportes) {
		this.listaReportes = listaReportes;
	}
	public String getTxhReporte1() {
		return txhReporte1;
	}
	public void setTxhReporte1(String txhReporte1) {
		this.txhReporte1 = txhReporte1;
	}
	public String getTxhReporte2() {
		return txhReporte2;
	}
	public void setTxhReporte2(String txhReporte2) {
		this.txhReporte2 = txhReporte2;
	}
	public String getTxhReporte3() {
		return txhReporte3;
	}
	public String getTxhRep2G() {
		return txhRep2G;
	}
	public void setTxhRep2G(String txhRep2G) {
		this.txhRep2G = txhRep2G;
	}
	public void setTxhReporte3(String txhReporte3) {
		this.txhReporte3 = txhReporte3;
	}
	public String getTxhReporte4() {
		return txhReporte4;
	}
	public void setTxhReporte4(String txhReporte4) {
		this.txhReporte4 = txhReporte4;
	}
	public String getTxhReporte5() {
		return txhReporte5;
	}
	public void setTxhReporte5(String txhReporte5) {
		this.txhReporte5 = txhReporte5;
	}
	public String getCboMaterial() {
		return cboMaterial;
	}
	public void setCboMaterial(String cboMaterial) {
		this.cboMaterial = cboMaterial;
	}
	public List getListaMaterial() {
		return listaMaterial;
	}
	public void setListaMaterial(List listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	public String getCbotipoRepMatBib() {
		return cbotipoRepMatBib;
	}
	public void setCbotipoRepMatBib(String cbotipoRepMatBib) {
		this.cbotipoRepMatBib = cbotipoRepMatBib;
	}
	public List getListaTipoRepMatBib() {
		return listaTipoRepMatBib;
	}
	public void setListaTipoRepMatBib(List listaTipoRepMatBib) {
		this.listaTipoRepMatBib = listaTipoRepMatBib;
	}
	public String getTxhRep2A() {
		return txhRep2A;
	}
	public void setTxhRep2A(String txhRep2A) {
		this.txhRep2A = txhRep2A;
	}
	public String getTxhRep2B() {
		return txhRep2B;
	}
	public void setTxhRep2B(String txhRep2B) {
		this.txhRep2B = txhRep2B;
	}
	public String getTxhRep2C() {
		return txhRep2C;
	}
	public void setTxhRep2C(String txhRep2C) {
		this.txhRep2C = txhRep2C;
	}
	public String getTxhRep2D() {
		return txhRep2D;
	}
	public void setTxhRep2D(String txhRep2D) {
		this.txhRep2D = txhRep2D;
	}
	public String getTxhRep2E() {
		return txhRep2E;
	}
	public void setTxhRep2E(String txhRep2E) {
		this.txhRep2E = txhRep2E;
	}
	public String getCboTipoRepMovPresDev() {
		return cboTipoRepMovPresDev;
	}
	public void setCboTipoRepMovPresDev(String cboTipoRepMovPresDev) {
		this.cboTipoRepMovPresDev = cboTipoRepMovPresDev;
	}
	public List getListaTipoRepMovPresDev() {
		return listaTipoRepMovPresDev;
	}
	public void setListaTipoRepMovPresDev(List listaTipoRepMovPresDev) {
		this.listaTipoRepMovPresDev = listaTipoRepMovPresDev;
	}
	public String getCboTipoRepCicloEsp() {
		return cboTipoRepCicloEsp;
	}
	public void setCboTipoRepCicloEsp(String cboTipoRepCicloEsp) {
		this.cboTipoRepCicloEsp = cboTipoRepCicloEsp;
	}
	public List getListaTipoRepCicloEsp() {
		return listaTipoRepCicloEsp;
	}
	public void setListaTipoRepCicloEsp(List listaTipoRepCicloEsp) {
		this.listaTipoRepCicloEsp = listaTipoRepCicloEsp;
	}
	public String getCboTipoUsuarios() {
		return cboTipoUsuarios;
	}
	public void setCboTipoUsuarios(String cboTipoUsuarios) {
		this.cboTipoUsuarios = cboTipoUsuarios;
	}
	public List getListaTipoUsuarios() {
		return listaTipoUsuarios;
	}
	public void setListaTipoUsuarios(List listaTipoUsuarios) {
		this.listaTipoUsuarios = listaTipoUsuarios;
	}
	public List getListaMeses() {
		return listaMeses;
	}
	public void setListaMeses(List listaMeses) {
		this.listaMeses = listaMeses;
	}
	public List getListaAnios() {
		return listaAnios;
	}
	public void setListaAnios(List listaAnios) {
		this.listaAnios = listaAnios;
	}
	public String getCboPeriodoSancion() {
		return cboPeriodoSancion;
	}
	public void setCboPeriodoSancion(String cboPeriodoSancion) {
		this.cboPeriodoSancion = cboPeriodoSancion;
	}
	public String getCboAnioSancion() {
		return cboAnioSancion;
	}
	public void setCboAnioSancion(String cboAnioSancion) {
		this.cboAnioSancion = cboAnioSancion;
	}
	public String getCboPeriodoBuzon() {
		return cboPeriodoBuzon;
	}
	public void setCboPeriodoBuzon(String cboPeriodoBuzon) {
		this.cboPeriodoBuzon = cboPeriodoBuzon;
	}
	public String getCboAnioBuzon() {
		return cboAnioBuzon;
	}
	public void setCboAnioBuzon(String cboAnioBuzon) {
		this.cboAnioBuzon = cboAnioBuzon;
	}
	public String getTxhReporte6() {
		return txhReporte6;
	}
	public void setTxhReporte6(String txhReporte6) {
		this.txhReporte6 = txhReporte6;
	}
	public String getTxhRep2F() {
		return txhRep2F;
	}
	public void setTxhRep2F(String txhRep2F) {
		this.txhRep2F = txhRep2F;
	}
	public String getTxhCodUsuarioRep() {
		return txhCodUsuarioRep;
	}
	public void setTxhCodUsuarioRep(String txhCodUsuarioRep) {
		this.txhCodUsuarioRep = txhCodUsuarioRep;
	}
	public String getTxhUsernameRep() {
		return txhUsernameRep;
	}
	public void setTxhUsernameRep(String txhUsernameRep) {
		this.txhUsernameRep = txhUsernameRep;
	}

	/**
	 * @return the txhReporte7
	 */
	public String getTxhReporte7() {
		return txhReporte7;
	}
	/**
	 * @param txhReporte7 the txhReporte7 to set
	 */
	public void setTxhReporte7(String txhReporte7) {
		this.txhReporte7 = txhReporte7;
	}
	
	/**
	 * @return the cboTipoRepMovReserDev
	 */
	public String getCboTipoRepMovReserDev() {
		return cboTipoRepMovReserDev;
	}
	/**
	 * @param cboTipoRepMovReserDev the cboTipoRepMovReserDev to set
	 */
	public void setCboTipoRepMovReserDev(String cboTipoRepMovReserDev) {
		this.cboTipoRepMovReserDev = cboTipoRepMovReserDev;
	}
	/**
	 * @return the listaTipoRepMovReserDev
	 */
	public List getListaTipoRepMovReserDev() {
		return listaTipoRepMovReserDev;
	}
	/**
	 * @param listaTipoRepMovReserDev the listaTipoRepMovReserDev to set
	 */
	public void setListaTipoRepMovReserDev(List listaTipoRepMovReserDev) {
		this.listaTipoRepMovReserDev = listaTipoRepMovReserDev;
	}
	/**
	 * @return the txhReporte8
	 */
	public String getTxhReporte8() {
		return txhReporte8;
	}
	/**
	 * @param txhReporte8 the txhReporte8 to set
	 */
	public void setTxhReporte8(String txhReporte8) {
		this.txhReporte8 = txhReporte8;
	}
	/**
	 * @return the cboSedeRep
	 */
	public String getCboSedeRep() {
		return cboSedeRep;
	}
	/**
	 * @param cboSedeRep the cboSedeRep to set
	 */
	public void setCboSedeRep(String cboSedeRep) {
		this.cboSedeRep = cboSedeRep;
	}
	/**
	 * @return the listaSedeRep
	 */
	public List getListaSedeRep() {
		return listaSedeRep;
	}
	/**
	 * @param listaSedeRep the listaSedeRep to set
	 */
	public void setListaSedeRep(List listaSedeRep) {
		this.listaSedeRep = listaSedeRep;
	}
	/**
	 * @return the sedeSel
	 */
	public String getSedeSel() {
		return sedeSel;
	}
	/**
	 * @param sedeSel the sedeSel to set
	 */
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}
	public List getLstProcedencia() {
		return lstProcedencia;
	}
	public void setLstProcedencia(List lstProcedencia) {
		this.lstProcedencia = lstProcedencia;
	}
	public String getCboProcedencia() {
		return cboProcedencia;
	}
	public void setCboProcedencia(String cboProcedencia) {
		this.cboProcedencia = cboProcedencia;
	}
	public List getLstTipoResultado() {
		return lstTipoResultado;
	}
	public void setLstTipoResultado(List lstTipoResultado) {
		this.lstTipoResultado = lstTipoResultado;
	}
	public String getCodTipoResultado() {
		return codTipoResultado;
	}
	public void setCodTipoResultado(String codTipoResultado) {
		this.codTipoResultado = codTipoResultado;
	}
	public String getNroIngresoIni() {
		return nroIngresoIni;
	}
	public void setNroIngresoIni(String nroIngresoIni) {
		this.nroIngresoIni = nroIngresoIni;
	}
	public String getNroIngresoFin() {
		return nroIngresoFin;
	}
	public void setNroIngresoFin(String nroIngresoFin) {
		this.nroIngresoFin = nroIngresoFin;
	}
	
}
