package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class MtoConfSancionesCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	//**********************************
	private String numeroOcurrencias;
	private String cadena;
	private String numeroRegistros;
	//**********************************
	private List listaRangoDias;
	private List listaSanciones;
	//*********************************
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNumeroOcurrencias() {
		return numeroOcurrencias;
	}
	public void setNumeroOcurrencias(String numeroOcurrencias) {
		this.numeroOcurrencias = numeroOcurrencias;
	}
	public List getListaRangoDias() {
		return listaRangoDias;
	}
	public void setListaRangoDias(List listaRangoDias) {
		this.listaRangoDias = listaRangoDias;
	}
	public List getListaSanciones() {
		return listaSanciones;
	}
	public void setListaSanciones(List listaSanciones) {
		this.listaSanciones = listaSanciones;
	}
	public String getCadena() {
		return cadena;
	}
	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
	public String getNumeroRegistros() {
		return numeroRegistros;
	}
	public void setNumeroRegistros(String numeroRegistros) {
		this.numeroRegistros = numeroRegistros;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
}
