package com.tecsup.SGA.web.biblioteca.command;

public class ParametrosGeneralesCommand {
	private String operacion;
	private String esmadi;
	private String esdipe;
	private String esmito;
	private String inmadi;
	private String indipe;
	private String inmito;
	private String binrodipe;
	private String graba;
	private String msg;
	private String codEval;
	private String codUsuario;
	private String visualizar;
	private String tamanioLista;
	private String cadenaDatos;
	
	public String getVisualizar() {
		return visualizar;
	}
	public void setVisualizar(String visualizar) {
		this.visualizar = visualizar;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getEsmadi() {
		return esmadi;
	}
	public void setEsmadi(String esmadi) {
		this.esmadi = esmadi;
	}
	public String getEsdipe() {
		return esdipe;
	}
	public void setEsdipe(String esdipe) {
		this.esdipe = esdipe;
	}
	public String getEsmito() {
		return esmito;
	}
	public void setEsmito(String esmito) {
		this.esmito = esmito;
	}
	public String getInmadi() {
		return inmadi;
	}
	public void setInmadi(String inmadi) {
		this.inmadi = inmadi;
	}
	public String getIndipe() {
		return indipe;
	}
	public void setIndipe(String indipe) {
		this.indipe = indipe;
	}
	public String getInmito() {
		return inmito;
	}
	public void setInmito(String inmito) {
		this.inmito = inmito;
	}
	public String getBinrodipe() {
		return binrodipe;
	}
	public void setBinrodipe(String binrodipe) {
		this.binrodipe = binrodipe;
	}
	public String getTamanioLista() {
		return tamanioLista;
	}
	public void setTamanioLista(String tamanioLista) {
		this.tamanioLista = tamanioLista;
	}
	public String getCadenaDatos() {
		return cadenaDatos;
	}
	public void setCadenaDatos(String cadenaDatos) {
		this.cadenaDatos = cadenaDatos;
	}
	
	
	
}
