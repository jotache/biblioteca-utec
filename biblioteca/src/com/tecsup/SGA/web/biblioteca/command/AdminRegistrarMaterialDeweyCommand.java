package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminRegistrarMaterialDeweyCommand {
	private String operacion;
	private String txtDescripcion;
	private String cboCategoria;
	private List lstCategoria;
	private List lstResultado;
	
	private String txhCodUsuario;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTxtDescripcion() {
		return txtDescripcion;
	}
	public void setTxtDescripcion(String txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}
	public String getCboCategoria() {
		return cboCategoria;
	}
	public void setCboCategoria(String cboCategoria) {
		this.cboCategoria = cboCategoria;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public List getLstCategoria() {
		return lstCategoria;
	}
	public void setLstCategoria(List lstCategoria) {
		this.lstCategoria = lstCategoria;
	}
	
	
	public String getTxhCodUsuario() {
		return txhCodUsuario;
	}
	public void setTxhCodUsuario(String txhCodUsuario) {
		this.txhCodUsuario = txhCodUsuario;
	}

	
	
}
