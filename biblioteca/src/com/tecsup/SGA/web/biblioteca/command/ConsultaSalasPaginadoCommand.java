package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaSalasPaginadoCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	private String tipoSala;
	private String dscSala;
	//*****************************
	private List listaTipoSala;
	private List listaSala;
	private String tamListaSala;
	//*****************************
	private String codigoSec;
	private String codigoSala;
	private String descripcion;
	private String seleccionTipoSala;
	private String capacidad;
	//*****************************
	private String seleccion;
	private String sedeSel;
	
	
	public String getSeleccionTipoSala() {
		return seleccionTipoSala;
	}
	public void setSeleccionTipoSala(String seleccionTipoSala) {
		this.seleccionTipoSala = seleccionTipoSala;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}	
	public String getDscSala() {
		return dscSala;
	}
	public void setDscSala(String dscSala) {
		this.dscSala = dscSala;
	}
	public List getListaSala() {
		return listaSala;
	}
	public void setListaSala(List listaSala) {
		this.listaSala = listaSala;
	}
	public String getTamListaSala() {
		return tamListaSala;
	}
	public void setTamListaSala(String tamListaSala) {
		this.tamListaSala = tamListaSala;
	}
	public String getCodigoSec() {
		return codigoSec;
	}
	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}
	public String getCodigoSala() {
		return codigoSala;
	}
	public void setCodigoSala(String codigoSala) {
		this.codigoSala = codigoSala;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getTipoSala() {
		return tipoSala;
	}
	public void setTipoSala(String tipoSala) {
		this.tipoSala = tipoSala;
	}
	public List getListaTipoSala() {
		return listaTipoSala;
	}
	public void setListaTipoSala(List listaTipoSala) {
		this.listaTipoSala = listaTipoSala;
	}	
	public String getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getSedeSel() {
		return sedeSel;
	}
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}
}
