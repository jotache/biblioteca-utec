package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaDescriptoresCommand {

	private String operacion;
	private String idRec;
	private String lstrURL;
	private String descripcion;
	private String codigo;
	private List listaDescriptores;
	private String codSelec;
	private String descripSelec;
	private String codigoDescriptor;
	private String descripcionDescriptor;
	private String codValor;
	private String codEvaluador;
	private String codPeriodo;
	private String msg;
	
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodigoDescriptor() {
		return codigoDescriptor;
	}
	public void setCodigoDescriptor(String codigoDescriptor) {
		this.codigoDescriptor = codigoDescriptor;
	}
	public String getDescripcionDescriptor() {
		return descripcionDescriptor;
	}
	public void setDescripcionDescriptor(String descripcionDescriptor) {
		this.descripcionDescriptor = descripcionDescriptor;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public List getListaDescriptores() {
		return listaDescriptores;
	}
	public void setListaDescriptores(List listaDescriptores) {
		this.listaDescriptores = listaDescriptores;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}

}
