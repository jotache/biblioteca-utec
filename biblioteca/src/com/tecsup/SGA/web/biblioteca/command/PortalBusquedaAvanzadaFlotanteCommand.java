package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class PortalBusquedaAvanzadaFlotanteCommand {
	
	private String operacion;
	private String ruta;
	private String archivo;
	private String flagReserva;
	private String peso;
	private List listaBusquedaAvanzada;
	private String imagen;
	
	/**********/
	private String selSala;
	private String selDomicilio;
	private String codTipoMaterial;
	private String codSede;
	private String codIdioma;
	private String codSeleccion1;
	private String codSeleccion2;
	private String codSeleccion3;
	private String codSeleccion4;
	private String codFechaAnioInicio;
	private String codFechaAnioFin;
	private String txtTexto1;
	private String txtTexto2;
	private String txtTexto3;
	
	private String codCondicion1;
	private String codCondicion2;
	private String ordenarBy;
	
	/*******************/
	private String codUsuario;
	private String codPeriodo;
	private List listaBusqueda;
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public List getListaBusqueda() {
		return listaBusqueda;
	}
	public void setListaBusqueda(List listaBusqueda) {
		this.listaBusqueda = listaBusqueda;
	}
	public String getOrdenarBy() {
		return ordenarBy;
	}
	public void setOrdenarBy(String ordenarBy) {
		this.ordenarBy = ordenarBy;
	}
	public String getSelSala() {
		return selSala;
	}
	public void setSelSala(String selSala) {
		this.selSala = selSala;
	}
	public String getSelDomicilio() {
		return selDomicilio;
	}
	public void setSelDomicilio(String selDomicilio) {
		this.selDomicilio = selDomicilio;
	}
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public String getCodIdioma() {
		return codIdioma;
	}
	public void setCodIdioma(String codIdioma) {
		this.codIdioma = codIdioma;
	}
	public String getCodSeleccion1() {
		return codSeleccion1;
	}
	public void setCodSeleccion1(String codSeleccion1) {
		this.codSeleccion1 = codSeleccion1;
	}
	public String getCodSeleccion2() {
		return codSeleccion2;
	}
	public void setCodSeleccion2(String codSeleccion2) {
		this.codSeleccion2 = codSeleccion2;
	}
	public String getCodSeleccion3() {
		return codSeleccion3;
	}
	public void setCodSeleccion3(String codSeleccion3) {
		this.codSeleccion3 = codSeleccion3;
	}
	public String getCodSeleccion4() {
		return codSeleccion4;
	}
	public void setCodSeleccion4(String codSeleccion4) {
		this.codSeleccion4 = codSeleccion4;
	}
	public String getCodFechaAnioInicio() {
		return codFechaAnioInicio;
	}
	public void setCodFechaAnioInicio(String codFechaAnioInicio) {
		this.codFechaAnioInicio = codFechaAnioInicio;
	}
	public String getCodFechaAnioFin() {
		return codFechaAnioFin;
	}
	public void setCodFechaAnioFin(String codFechaAnioFin) {
		this.codFechaAnioFin = codFechaAnioFin;
	}
	public String getTxtTexto1() {
		return txtTexto1;
	}
	public void setTxtTexto1(String txtTexto1) {
		this.txtTexto1 = txtTexto1;
	}
	public String getTxtTexto2() {
		return txtTexto2;
	}
	public void setTxtTexto2(String txtTexto2) {
		this.txtTexto2 = txtTexto2;
	}
	public String getTxtTexto3() {
		return txtTexto3;
	}
	public void setTxtTexto3(String txtTexto3) {
		this.txtTexto3 = txtTexto3;
	}
	public String getCodCondicion1() {
		return codCondicion1;
	}
	public void setCodCondicion1(String codCondicion1) {
		this.codCondicion1 = codCondicion1;
	}
	public String getCodCondicion2() {
		return codCondicion2;
	}
	public void setCodCondicion2(String codCondicion2) {
		this.codCondicion2 = codCondicion2;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public String getFlagReserva() {
		return flagReserva;
	}
	public void setFlagReserva(String flagReserva) {
		this.flagReserva = flagReserva;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	public List getListaBusquedaAvanzada() {
		return listaBusquedaAvanzada;
	}
	public void setListaBusquedaAvanzada(List listaBusquedaAvanzada) {
		this.listaBusquedaAvanzada = listaBusquedaAvanzada;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
		
}
