package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class DeweyModificarCommand {
	private String operacion;
	private String descripcionDewey;
	private String codDetalle;
	private String usucrea;
	private String usuModi;
	private String msg;
	private String codigoDewey;
	private List codListaCategoria;
	private String codPeriodo;
	private String codEvaluador;
	private String codValor;
	private String descripSelec;
	private String codSelec;
    private String codCategoria;
  
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public String getDescripcionDewey() {
		return descripcionDewey;
	}
	public void setDescripcionDewey(String descripcionDewey) {
		this.descripcionDewey = descripcionDewey;
	}
	public String getCodigoDewey() {
		return codigoDewey;
	}
	public void setCodigoDewey(String codigoDewey) {
		this.codigoDewey = codigoDewey;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getUsucrea() {
		return usucrea;
	}
	public void setUsucrea(String usucrea) {
		this.usucrea = usucrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List getCodListaCategoria() {
		return codListaCategoria;
	}
	public void setCodListaCategoria(List codListaCategoria) {
		this.codListaCategoria = codListaCategoria;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getCodCategoria() {
		return codCategoria;
	}
	public void setCodCategoria(String codCategoria) {
		this.codCategoria = codCategoria;
	}
	
}
