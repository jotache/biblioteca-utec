package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class AdminRegistrarMaterialAgregarDescriptoresCommand {
	private String operacion;
	private String txtDescriptores;
	private List lstResultado;
	private String txhtamanio;
	private String txhToken;
	private String txhCodMaterial;
	private String usuario;
	private String txhTamanioSeleccionado;
	

	public String getTxhTamanioSeleccionado() {
		return txhTamanioSeleccionado;
	}

	public void setTxhTamanioSeleccionado(String txhTamanioSeleccionado) {
		this.txhTamanioSeleccionado = txhTamanioSeleccionado;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTxhCodMaterial() {
		return txhCodMaterial;
	}

	public void setTxhCodMaterial(String txhCodMaterial) {
		this.txhCodMaterial = txhCodMaterial;
	}

	public String getTxhToken() {
		return txhToken;
	}

	public void setTxhToken(String txhToken) {
		this.txhToken = txhToken;
	}

	public String getTxtDescriptores() {
		return txtDescriptores;
	}

	public void setTxtDescriptores(String txtDescriptores) {
		this.txtDescriptores = txtDescriptores;
	}

	public List getLstResultado() {
		return lstResultado;
	}

	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getTxhtamanio() {
		return txhtamanio;
	}

	public void setTxhtamanio(String txhtamanio) {
		this.txhtamanio = txhtamanio;
	}
}
