package com.tecsup.SGA.web.biblioteca.command;

public class AdminRegistrarMaterialAdjuntarArchivoCommand {
	private String operacion;
	private String msg;	
	private byte[] txtArchivo;//a subir
	private String extArchivo;//extension
	//*************************************
	private String codPeriodo;
	private String codProducto;
	private String codCiclo;
	private String nomNuevoArchivo;
	private String nomArchivo;
	private String nomImagen;
	private String txtTitulo;	
	private String codEvaluador;
	private String txhCodigoUnico;
	private String usuario;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getNomNuevoArchivo() {
		return nomNuevoArchivo;
	}
	public void setNomNuevoArchivo(String nomNuevoArchivo) {
		this.nomNuevoArchivo = nomNuevoArchivo;
	}
	public String getNomArchivo() {
		return nomArchivo;
	}
	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}	
	public String getExtArchivo() {
		return extArchivo;
	}
	public void setExtArchivo(String extArchivo) {
		this.extArchivo = extArchivo;
	}
	public byte[] getTxtArchivo() {
		return txtArchivo;
	}
	public void setTxtArchivo(byte[] txtArchivo) {
		this.txtArchivo = txtArchivo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNomImagen() {
		return nomImagen;
	}
	public void setNomImagen(String nomImagen) {
		this.nomImagen = nomImagen;
	}
	public String getTxtTitulo() {
		return txtTitulo;
	}
	public void setTxtTitulo(String txtTitulo) {
		this.txtTitulo = txtTitulo;
	}
	public String getTxhCodigoUnico() {
		return txhCodigoUnico;
	}
	public void setTxhCodigoUnico(String txhCodigoUnico) {
		this.txhCodigoUnico = txhCodigoUnico;
	}
	
}
