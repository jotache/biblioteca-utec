package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaIdiomaCommand {
	private String codUsuario;
	private String operacion;
	private String msg;
	private String codigo;
	private String dscIdioma;
	//*****************************
	private List listaIdioma;
	private String tamListaIdioma;
	//*****************************
	private String codigoSec;
	private String codigoIdioma;
	private String descripcion;
	//*****************************
	private String seleccion;
	
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDscIdioma() {
		return dscIdioma;
	}
	public void setDscIdioma(String dscIdioma) {
		this.dscIdioma = dscIdioma;
	}
	public List getListaIdioma() {
		return listaIdioma;
	}
	public void setListaIdioma(List listaIdioma) {
		this.listaIdioma = listaIdioma;
	}
	public String getTamListaIdioma() {
		return tamListaIdioma;
	}
	public void setTamListaIdioma(String tamListaIdioma) {
		this.tamListaIdioma = tamListaIdioma;
	}
	public String getCodigoSec() {
		return codigoSec;
	}
	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}
	public String getCodigoIdioma() {
		return codigoIdioma;
	}
	public void setCodigoIdioma(String codigoIdioma) {
		this.codigoIdioma = codigoIdioma;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

}
