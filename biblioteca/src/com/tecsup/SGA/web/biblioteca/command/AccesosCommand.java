package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

import com.tecsup.SGA.modelo.UsuarioPM;

public class AccesosCommand {
    private String operacion;
    private String msg;
    private String codUsuario;
    private Integer codSujeto;
    private String nomUsuario;
    private String numIP;
    private String usuCreacion;
    private UsuarioPM usuario;
    private List<UsuarioPM> usuarios;
    private int tamanoLstOpciones;
    private String cadenaDatos;

    public UsuarioPM getUsuario() {
        return usuario;
    }
    public void setUsuario(UsuarioPM usuario) {
        this.usuario = usuario;
    }
    public String getNumIP() {
        return numIP;
    }
    public void setNumIP(String numIP) {
        this.numIP = numIP;
    }
    public String getNomUsuario() {
        return nomUsuario;
    }
    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }
    public String getUsuCreacion() {
        return usuCreacion;
    }
    public void setUsuCreacion(String usuCreacion) {
        this.usuCreacion = usuCreacion;
    }
    public String getCodUsuario() {
        return codUsuario;
    }
    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }
    public Integer getCodSujeto() {
        return codSujeto;
    }
    public void setCodSujeto(Integer codSujeto) {
        this.codSujeto = codSujeto;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getOperacion() {
        return operacion;
    }
    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }
    public List<UsuarioPM> getUsuarios() {
        return usuarios;
    }
    public void setUsuarios(List<UsuarioPM> usuarios) {
        this.usuarios = usuarios;
    }
    @Override
    public String toString() {
        return "OPERACION:"+operacion+"-CODUSUARIO:"+codUsuario+"-NUMIP:"+numIP+"-USUCREACION:"+usuCreacion;
    }
    public int getTamanoLstOpciones() {
        return tamanoLstOpciones;
    }
    public void setTamanoLstOpciones(int tamanoLstOpciones) {
        this.tamanoLstOpciones = tamanoLstOpciones;
    }
    public String getCadenaDatos() {
        return cadenaDatos;
    }
    public void setCadenaDatos(String cadenaDatos) {
        this.cadenaDatos = cadenaDatos;
    }



}
