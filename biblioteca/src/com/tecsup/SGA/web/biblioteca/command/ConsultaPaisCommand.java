package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaPaisCommand {

	private String operacion;//
	private String idRec;
	private String lstrURL;
	private String codigoPais;
	private List listaPaises;
	private String codSelec;//
	private String descripSelec;//
	private String descripcionPaises;
	private String codPeriodo;
	private String codEvaluador;
	private String msg;
	private String codValor;
	
	public String getDescripcionPaises() {
		return descripcionPaises;
	}
	public void setDescripcionPaises(String descripcionPaises) {
		this.descripcionPaises = descripcionPaises;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	
	public List getListaPaises() {
		return listaPaises;
	}
	public void setListaPaises(List listaPaises) {
		this.listaPaises = listaPaises;
	}
	
	public String getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodValor() {
		return codValor;
	}
	public void setCodValor(String codValor) {
		this.codValor = codValor;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}

}
