package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class ConsultaTipoVideoCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	private String codigo;
	private String dscVideo;
	//*****************************
	private List listaVideo;
	private String tamListaVideo;
	//*****************************
	private String codigoSec;
	private String codigoVideo;
	private String descripcion;
	//*****************************
	private String seleccion;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDscVideo() {
		return dscVideo;
	}
	public void setDscVideo(String dscVideo) {
		this.dscVideo = dscVideo;
	}
	public List getListaVideo() {
		return listaVideo;
	}
	public void setListaVideo(List listaVideo) {
		this.listaVideo = listaVideo;
	}
	public String getTamListaVideo() {
		return tamListaVideo;
	}
	public void setTamListaVideo(String tamListaVideo) {
		this.tamListaVideo = tamListaVideo;
	}
	public String getCodigoSec() {
		return codigoSec;
	}
	public void setCodigoSec(String codigoSec) {
		this.codigoSec = codigoSec;
	}
	public String getCodigoVideo() {
		return codigoVideo;
	}
	public void setCodigoVideo(String codigoVideo) {
		this.codigoVideo = codigoVideo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
}
