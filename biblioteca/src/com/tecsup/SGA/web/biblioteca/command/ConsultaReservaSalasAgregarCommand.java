package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

import com.tecsup.SGA.modelo.Sala;

public class ConsultaReservaSalasAgregarCommand {
  
  private String operacion;
  private String codSalas;
  private List codListaSalas;
  private String codHoraInicio;
  private String codHoraFin;
  
  // private List codListaHoraInicio;
  private List<Sala> ListaHoras;
  
  // private List codListaHoraFin;
  private String txtFechaInicio;
  private String txtCapacidadMaxima;
  
  private String codPeriodo;
  private String codEvaluador;
  private String codSeleccionado;
  private String msg;
  private String fechaBD;
  private String txtVacantes;
  
  private String txtUserReserva;
  private String txtCodUserReserva;
  private String estado; // para saber si es web o admin
  private String sedeSel;
  private String flgLabQuimica;
  
  /**
   * @return the txtCodUserReserva
   */
  public String getTxtCodUserReserva() {
    return txtCodUserReserva;
  }
  
  /**
   * @param txtCodUserReserva
   *          the txtCodUserReserva to set
   */
  public void setTxtCodUserReserva(String txtCodUserReserva) {
    this.txtCodUserReserva = txtCodUserReserva;
  }
  
  private String codTipoSala;
  private String flgUsuAdmin;
  private String accion;
  private String codRecurso;
  
  private String fechaReservSel;
  private String horaIniSel;
  private String horaFinSel;
  private String capacidadSel;
  
  /**
   * @return the fechaReservSel
   */
  public String getFechaReservSel() {
    return fechaReservSel;
  }
  
  /**
   * @param fechaReservSel
   *          the fechaReservSel to set
   */
  public void setFechaReservSel(String fechaReservSel) {
    this.fechaReservSel = fechaReservSel;
  }
  
  /**
   * @return the horaIniSel
   */
  public String getHoraIniSel() {
    return horaIniSel;
  }
  
  /**
   * @param horaIniSel
   *          the horaIniSel to set
   */
  public void setHoraIniSel(String horaIniSel) {
    this.horaIniSel = horaIniSel;
  }
  
  /**
   * @return the horaFinSel
   */
  public String getHoraFinSel() {
    return horaFinSel;
  }
  
  /**
   * @param horaFinSel
   *          the horaFinSel to set
   */
  public void setHoraFinSel(String horaFinSel) {
    this.horaFinSel = horaFinSel;
  }
  
  /**
   * @return the capacidadSel
   */
  public String getCapacidadSel() {
    return capacidadSel;
  }
  
  /**
   * @param capacidadSel
   *          the capacidadSel to set
   */
  public void setCapacidadSel(String capacidadSel) {
    this.capacidadSel = capacidadSel;
  }
  
  /**
   * @return the codRecurso
   */
  public String getCodRecurso() {
    return codRecurso;
  }
  
  /**
   * @param codRecurso
   *          the codRecurso to set
   */
  public void setCodRecurso(String codRecurso) {
    this.codRecurso = codRecurso;
  }
  
  private String codSecuencia;
  
  /**
   * @return the codSecuencia
   */
  public String getCodSecuencia() {
    return codSecuencia;
  }
  
  /**
   * @param codSecuencia
   *          the codSecuencia to set
   */
  public void setCodSecuencia(String codSecuencia) {
    this.codSecuencia = codSecuencia;
  }
  
  /**
   * @return the accion
   */
  public String getAccion() {
    return accion;
  }
  
  /**
   * @param accion
   *          the accion to set
   */
  public void setAccion(String accion) {
    this.accion = accion;
  }
  
  /**
   * @return the flgUsuAdmin
   */
  public String getFlgUsuAdmin() {
    return flgUsuAdmin;
  }
  
  /**
   * @param flgUsuAdmin
   *          the flgUsuAdmin to set
   */
  public void setFlgUsuAdmin(String flgUsuAdmin) {
    this.flgUsuAdmin = flgUsuAdmin;
  }
  
  public String getCodTipoSala() {
    return codTipoSala;
  }
  
  public void setCodTipoSala(String codTipoSala) {
    this.codTipoSala = codTipoSala;
  }
  
  public String getTxtVacantes() {
    return txtVacantes;
  }
  
  public void setTxtVacantes(String txtVacantes) {
    this.txtVacantes = txtVacantes;
  }
  
  public String getFechaBD() {
    return fechaBD;
  }
  
  public void setFechaBD(String fechaBD) {
    this.fechaBD = fechaBD;
  }
  
  public String getMsg() {
    return msg;
  }
  
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  public String getOperacion() {
    return operacion;
  }
  
  public void setOperacion(String operacion) {
    this.operacion = operacion;
  }
  
  public String getCodSalas() {
    return codSalas;
  }
  
  public void setCodSalas(String codSalas) {
    this.codSalas = codSalas;
  }
  
  public List getCodListaSalas() {
    return codListaSalas;
  }
  
  public void setCodListaSalas(List codListaSalas) {
    this.codListaSalas = codListaSalas;
  }
  
  public String getCodHoraInicio() {
    return codHoraInicio;
  }
  
  public void setCodHoraInicio(String codHoraInicio) {
    this.codHoraInicio = codHoraInicio;
  }
  
  /*
   * public List getCodListaHoraInicio() { return codListaHoraInicio; } public
   * void setCodListaHoraInicio(List codListaHoraInicio) {
   * this.codListaHoraInicio = codListaHoraInicio; }
   */
  public String getTxtFechaInicio() {
    return txtFechaInicio;
  }
  
  public void setTxtFechaInicio(String txtFechaInicio) {
    this.txtFechaInicio = txtFechaInicio;
  }
  
  public String getTxtCapacidadMaxima() {
    return txtCapacidadMaxima;
  }
  
  public void setTxtCapacidadMaxima(String txtCapacidadMaxima) {
    this.txtCapacidadMaxima = txtCapacidadMaxima;
  }
  
  public String getCodPeriodo() {
    return codPeriodo;
  }
  
  public void setCodPeriodo(String codPeriodo) {
    this.codPeriodo = codPeriodo;
  }
  
  public String getCodEvaluador() {
    return codEvaluador;
  }
  
  public void setCodEvaluador(String codEvaluador) {
    this.codEvaluador = codEvaluador;
  }
  
  public String getCodSeleccionado() {
    return codSeleccionado;
  }
  
  public void setCodSeleccionado(String codSeleccionado) {
    this.codSeleccionado = codSeleccionado;
  }
  
  public String getCodHoraFin() {
    return codHoraFin;
  }
  
  public void setCodHoraFin(String codHoraFin) {
    this.codHoraFin = codHoraFin;
  }
  
  /*
   * public List getCodListaHoraFin() { return codListaHoraFin; } public void
   * setCodListaHoraFin(List codListaHoraFin) { this.codListaHoraFin =
   * codListaHoraFin; }
   */
  public String getTxtUserReserva() {
    return txtUserReserva;
  }
  
  public void setTxtUserReserva(String txtUserReserva) {
    this.txtUserReserva = txtUserReserva;
  }
  
  public String getEstado() {
    return estado;
  }
  
  public void setEstado(String estado) {
    this.estado = estado;
  }
  
  /**
   * @return the sedeSel
   */
  public String getSedeSel() {
    return sedeSel;
  }
  
  /**
   * @param sedeSel
   *          the sedeSel to set
   */
  public void setSedeSel(String sedeSel) {
    this.sedeSel = sedeSel;
  }
  
  public List<Sala> getListaHoras() {
    return ListaHoras;
  }
  
  public void setListaHoras(List<Sala> listaHoras) {
    ListaHoras = listaHoras;
  }
  
  public String getFlgLabQuimica() {
    return flgLabQuimica;
  }
  
  public void setFlgLabQuimica(String flgLabQuimica) {
    this.flgLabQuimica = flgLabQuimica;
  }
  
}
