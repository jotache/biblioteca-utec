package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class PortalInicioDerechoCommand {

	private String horaAtencion;
	private String codEvaluador;
	private String codPeriodo;
	private List listaEnlaces;
	private List listaBibliotecas;
	private List listaOtrasFuentes;

	private String ingComoUser;
	private String reservaSala;
	private String enlacesInteres;
	private String bibliotecas;
	private String otrasFuentes;
	private String horario;
	private String reglamento;
	
	private String txtUsuario;
	private String txtClave;
	
	public String getReglamento() {
		return reglamento;
	}

	public void setReglamento(String reglamento) {
		this.reglamento = reglamento;
	}

	public List getListaEnlaces() {
		return listaEnlaces;
	}

	public void setListaEnlaces(List listaEnlaces) {
		this.listaEnlaces = listaEnlaces;
	}

	public List getListaBibliotecas() {
		return listaBibliotecas;
	}

	public void setListaBibliotecas(List listaBibliotecas) {
		this.listaBibliotecas = listaBibliotecas;
	}

	public List getListaOtrasFuentes() {
		return listaOtrasFuentes;
	}

	public void setListaOtrasFuentes(List listaOtrasFuentes) {
		this.listaOtrasFuentes = listaOtrasFuentes;
	}

	public String getHoraAtencion() {
		return horaAtencion;
	}

	public void setHoraAtencion(String horaAtencion) {
		this.horaAtencion = horaAtencion;
	}

	public String getCodEvaluador() {
		return codEvaluador;
	}

	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}

	public String getCodPeriodo() {
		return codPeriodo;
	}

	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}

	public String getIngComoUser() {
		return ingComoUser;
	}

	public void setIngComoUser(String ingComoUser) {
		this.ingComoUser = ingComoUser;
	}

	public String getReservaSala() {
		return reservaSala;
	}

	public void setReservaSala(String reservaSala) {
		this.reservaSala = reservaSala;
	}

	public String getEnlacesInteres() {
		return enlacesInteres;
	}

	public void setEnlacesInteres(String enlacesInteres) {
		this.enlacesInteres = enlacesInteres;
	}

	public String getBibliotecas() {
		return bibliotecas;
	}

	public void setBibliotecas(String bibliotecas) {
		this.bibliotecas = bibliotecas;
	}

	public String getOtrasFuentes() {
		return otrasFuentes;
	}

	public void setOtrasFuentes(String otrasFuentes) {
		this.otrasFuentes = otrasFuentes;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(String txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public String getTxtClave() {
		return txtClave;
	}

	public void setTxtClave(String txtClave) {
		this.txtClave = txtClave;
	}
	
	
}
