package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class PortalBusquedaInicioCommand {
	
	private String codEvaluador;
	private String codPeriodo;
	private String codTipoMaterial;
	private String txtTexto;
	private String codBuscarBy;
	private List codListaTipoMaterial;
	private List  codListaBuscarBy;
	private String codOrdenarBy;
	private List  codListaOrdenarBy;
	private String codSugerencia;
	private List  codListaSugerencia;
	private String selDomicilio;
	private String selSala;
	//napa sugerencias
	//********************************
	private String msg;
	private String operacion;
	private String codTipoConsulta;
	private String codTipoComentario;
	private String codTipoSugerencia;
	private String codTipoReclamo;
	private String txtSugerencia;
	private String txtMail;
	
	private String usuario;
	private String clave;
	private String log; //1,0 indica si esta logueado o no (1=Si,0=No)
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	//********************************
	public String getTxtSugerencia() {
		return txtSugerencia;
	}
	public void setTxtSugerencia(String txtSugerencia) {
		this.txtSugerencia = txtSugerencia;
	}
	//******************************
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public String getTxtTexto() {
		return txtTexto;
	}
	public void setTxtTexto(String txtTexto) {
		this.txtTexto = txtTexto;
	}
	public String getCodBuscarBy() {
		return codBuscarBy;
	}
	public void setCodBuscarBy(String codBuscarBy) {
		this.codBuscarBy = codBuscarBy;
	}
	public List getCodListaTipoMaterial() {
		return codListaTipoMaterial;
	}
	public void setCodListaTipoMaterial(List codListaTipoMaterial) {
		this.codListaTipoMaterial = codListaTipoMaterial;
	}
	public List getCodListaBuscarBy() {
		return codListaBuscarBy;
	}
	public void setCodListaBuscarBy(List codListaBuscarBy) {
		this.codListaBuscarBy = codListaBuscarBy;
	}
	public String getCodOrdenarBy() {
		return codOrdenarBy;
	}
	public void setCodOrdenarBy(String codOrdenarBy) {
		this.codOrdenarBy = codOrdenarBy;
	}
	public List getCodListaOrdenarBy() {
		return codListaOrdenarBy;
	}
	public void setCodListaOrdenarBy(List codListaOrdenarBy) {
		this.codListaOrdenarBy = codListaOrdenarBy;
	}
	public String getCodSugerencia() {
		return codSugerencia;
	}
	public void setCodSugerencia(String codSugerencia) {
		this.codSugerencia = codSugerencia;
	}
	public List getCodListaSugerencia() {
		return codListaSugerencia;
	}
	public void setCodListaSugerencia(List codListaSugerencia) {
		this.codListaSugerencia = codListaSugerencia;
	}
	public String getSelDomicilio() {
		return selDomicilio;
	}
	public void setSelDomicilio(String selDomicilio) {
		this.selDomicilio = selDomicilio;
	}
	public String getSelSala() {
		return selSala;
	}
	public void setSelSala(String selSala) {
		this.selSala = selSala;
	}
	public String getCodTipoConsulta() {
		return codTipoConsulta;
	}
	public void setCodTipoConsulta(String codTipoConsulta) {
		this.codTipoConsulta = codTipoConsulta;
	}
	public String getCodTipoComentario() {
		return codTipoComentario;
	}
	public void setCodTipoComentario(String codTipoComentario) {
		this.codTipoComentario = codTipoComentario;
	}
	public String getCodTipoSugerencia() {
		return codTipoSugerencia;
	}
	public void setCodTipoSugerencia(String codTipoSugerencia) {
		this.codTipoSugerencia = codTipoSugerencia;
	}
	public String getCodTipoReclamo() {
		return codTipoReclamo;
	}
	public void setCodTipoReclamo(String codTipoReclamo) {
		this.codTipoReclamo = codTipoReclamo;
	}
	public String getTxtMail() {
		return txtMail;
	}
	public void setTxtMail(String txtMail) {
		this.txtMail = txtMail;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}

}
