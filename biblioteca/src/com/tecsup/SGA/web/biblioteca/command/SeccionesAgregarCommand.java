package com.tecsup.SGA.web.biblioteca.command;

import java.util.List;

public class SeccionesAgregarCommand {
	private String operacion;
    private String msg;	
	private String codPeriodo;
	private String codEvaluador;
	//atributos para realizar las operaciones de insert y update
	private String codUnico;
	private String codSeccion;
	private String codTipoMaterial;
	private String codGrupo;
	private String dscTema;
	private String dscCorta;
	private String dscLarga;
	private String dscUrl;
	private String dscArchivo;	
	private String fechaPublicacion;
	private String fechaVigencia;
	private String fechaActual;
	private String flag;
	//**************************************************************	
	//para mostrar en pantalla //seccion mismo para todas las vistas
	private List listaSeccion;
	private String tipoSeccion;
	//areas de interes & biblioteca & otras fuentes de informacion 
	private String nombre;
	private List listaTipoMaterial;//solo areas de interes
	private String tipoMaterial;//solo areas de interes
	private String url;		
	//novedades
	private List listaGrupo;
	private String tipoGrupo;
	private String tema;
	private String dscCortaNov;
	private String dscLargaNov;		
	
	private String urlNov;
	private String dscDocumNov;
	private byte[] archivoDocumentoNov;
	private String pathDocumentoNov;
	private String archNovOriginalName;
	private String extArchivoNov;//extension
	
	//reglamento
	private String reglamento;
	//horario de atencion
	private String horarioAtencion;
	//las fechas son las mismas para todas las secciones
	private String fechaInicio;
	private String fechaFin;
	//para el manejo de archivos
	private byte[] txtArchivo;//a subir
	private String extArchivo;//extension
	//***************************
	private byte[] archivoImagen;
	private byte[] archivoDocumento;
	private String pathImagen;
	private String pathDocumento;
	//***************************
	private String codNov;
	private String codAreInt;
	private String codBib;
	private String codInf;
	private String codReg;
	private String codHor;
	//***************************
	private String codArchivo;
	
	public String getCodArchivo() {
		return codArchivo;
	}
	public void setCodArchivo(String codArchivo) {
		this.codArchivo = codArchivo;
	}
	public String getCodNov() {
		return codNov;
	}
	public void setCodNov(String codNov) {
		this.codNov = codNov;
	}
	public String getCodAreInt() {
		return codAreInt;
	}
	public void setCodAreInt(String codAreInt) {
		this.codAreInt = codAreInt;
	}
	public String getCodBib() {
		return codBib;
	}
	public void setCodBib(String codBib) {
		this.codBib = codBib;
	}
	public String getCodInf() {
		return codInf;
	}
	public void setCodInf(String codInf) {
		this.codInf = codInf;
	}
	public String getCodReg() {
		return codReg;
	}
	public void setCodReg(String codReg) {
		this.codReg = codReg;
	}
	public String getCodHor() {
		return codHor;
	}
	public void setCodHor(String codHor) {
		this.codHor = codHor;
	}
	public String getPathDocumento() {
		return pathDocumento;
	}
	public void setPathDocumento(String pathDocumento) {
		this.pathDocumento = pathDocumento;
	}
	public String getPathImagen() {
		return pathImagen;
	}
	public void setPathImagen(String pathImagen) {
		this.pathImagen = pathImagen;
	}	
	public void setExtArchivo(String extArchivo) {
		this.extArchivo = extArchivo;
	}
	//**************************************************************	
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	public String getDscCortaNov() {
		return dscCortaNov;
	}
	public void setDscCortaNov(String dscCortaNov) {
		this.dscCortaNov = dscCortaNov;
	}
	public String getDscLargaNov() {
		return dscLargaNov;
	}
	public void setDscLargaNov(String dscLargaNov) {
		this.dscLargaNov = dscLargaNov;
	}
	public String getUrlNov() {
		return urlNov;
	}
	public void setUrlNov(String urlNov) {
		this.urlNov = urlNov;
	}
	public String getDscDocumNov() {
		return dscDocumNov;
	}
	public void setDscDocumNov(String dscDocumNov) {
		this.dscDocumNov = dscDocumNov;
	}
	public byte[] getArchivoDocumentoNov() {
		return archivoDocumentoNov;
	}
	public void setArchivoDocumentoNov(byte[] archivoDocumentoNov) {
		this.archivoDocumentoNov = archivoDocumentoNov;
	}
	public String getPathDocumentoNov() {
		return pathDocumentoNov;
	}
	public void setPathDocumentoNov(String pathDocumentoNov) {
		this.pathDocumentoNov = pathDocumentoNov;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public String getCodGrupo() {
		return codGrupo;
	}
	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}	
	public String getDscCorta() {
		return dscCorta;
	}
	public void setDscCorta(String dscCorta) {
		this.dscCorta = dscCorta;
	}
	public String getDscLarga() {
		return dscLarga;
	}
	public void setDscLarga(String dscLarga) {
		this.dscLarga = dscLarga;
	}	
	public String getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public String getFechaVigencia() {
		return fechaVigencia;
	}
	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getTipoSeccion() {
		return tipoSeccion;
	}
	public void setTipoSeccion(String tipoSeccion) {
		this.tipoSeccion = tipoSeccion;
	}
	public String getTipoGrupo() {
		return tipoGrupo;
	}
	public void setTipoGrupo(String tipoGrupo) {
		this.tipoGrupo = tipoGrupo;
	}	
	public String getDscTema() {
		return dscTema;
	}
	public void setDscTema(String dscTema) {
		this.dscTema = dscTema;
	}
	public String getDscUrl() {
		return dscUrl;
	}
	public void setDscUrl(String dscUrl) {
		this.dscUrl = dscUrl;
	}
	public String getDscArchivo() {
		return dscArchivo;
	}
	public void setDscArchivo(String dscArchivo) {
		this.dscArchivo = dscArchivo;
	}
	public List getListaSeccion() {
		return listaSeccion;
	}
	public void setListaSeccion(List listaSeccion) {
		this.listaSeccion = listaSeccion;
	}
	public List getListaGrupo() {
		return listaGrupo;
	}
	public void setListaGrupo(List listaGrupo) {
		this.listaGrupo = listaGrupo;
	}
	public List getListaTipoMaterial() {
		return listaTipoMaterial;
	}
	public void setListaTipoMaterial(List listaTipoMaterial) {
		this.listaTipoMaterial = listaTipoMaterial;
	}
	public String getTipoMaterial() {
		return tipoMaterial;
	}
	public void setTipoMaterial(String tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getReglamento() {
		return reglamento;
	}
	public void setReglamento(String reglamento) {
		this.reglamento = reglamento;
	}
	public String getHorarioAtencion() {
		return horarioAtencion;
	}
	public void setHorarioAtencion(String horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}	
	public byte[] getTxtArchivo() {
		return txtArchivo;
	}
	public void setTxtArchivo(byte[] txtArchivo) {
		this.txtArchivo = txtArchivo;
	}
	public String getExtArchivo() {
		return extArchivo;
	}
	public byte[] getArchivoImagen() {
		return archivoImagen;
	}
	public void setArchivoImagen(byte[] archivoImagen) {
		this.archivoImagen = archivoImagen;
	}
	public byte[] getArchivoDocumento() {
		return archivoDocumento;
	}
	public void setArchivoDocumento(byte[] archivoDocumento) {
		this.archivoDocumento = archivoDocumento;
	}
	public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	
	
	public String getArchNovOriginalName() {
		return archNovOriginalName;
	}
	public void setArchNovOriginalName(String archNovOriginalName) {
		this.archNovOriginalName = archNovOriginalName;
	}
	public String getExtArchivoNov() {
		return extArchivoNov;
	}
	public void setExtArchivoNov(String extArchivoNov) {
		this.extArchivoNov = extArchivoNov;
	}		
	
	
}