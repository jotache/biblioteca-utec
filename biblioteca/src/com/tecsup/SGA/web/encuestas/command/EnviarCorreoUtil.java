package com.tecsup.SGA.web.encuestas.command;

import java.io.Serializable;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;


public class EnviarCorreoUtil 
implements Serializable {

	private static final long serialVersionUID = 1L;
	private Message mensaje;

	public EnviarCorreoUtil() {
		
		Properties propiedades = new Properties();
		
		propiedades.put("mail.transport.protocol", "smtp");
		propiedades.put("mail.smtp.host", "localhost");
		propiedades.put("mail.smtp.port", "25");
		mensaje = new MimeMessage(Session.getInstance(propiedades));
	}
	

	   public void envioBuzon(String to, String contenido) throws Exception{
			
		   try {
				this.mensaje.setFrom(new InternetAddress("ecaycho@tecsup.edu.pe"));
				this.mensaje.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
				this.mensaje.setRecipients(Message.RecipientType.BCC, InternetAddress.parse("ecaycho_06@hotmail.com"));
				this.mensaje.setSentDate(new Date());
				
				this.mensaje.setSubject("SISTEMA DE ENCUESTAS: ENLACE GENERADO");
				this.mensaje.setDataHandler(new DataHandler(new ByteArrayDataSource(contenido, "text/html")));
				Transport.send(this.mensaje);
				
				System.out.println("M: "+mensaje);
				System.out.println("M: "+to);
				System.out.println("M: "+contenido);
				
				System.out.println("Correo Enviado a: " + to);
			
			} catch (Exception e){
				e.printStackTrace();
				System.out.println(e);
				System.out.println("Error al enviar el correo a: "+to ) ;
				throw e;
			}
		}
	   	
}