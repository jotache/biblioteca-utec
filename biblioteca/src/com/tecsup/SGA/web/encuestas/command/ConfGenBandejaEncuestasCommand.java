package com.tecsup.SGA.web.encuestas.command;

import java.util.List;

public class ConfGenBandejaEncuestasCommand {

	private String operacion;
	private String codSelec;
	private String codUsuario;
	private String consteTipoPrograma;
	private String consteTipoServicios;
	private String msg;
	private String codTipoAplicacion;
	private String codTipoEncuesta;
	private String codTipoServicio;
	private String codTipoEstado;
	
	private List listaTipoAplicacion;
	private List listaTipoEncuesta;
	private List listaTipoServicio;
	private List listaTipoEstado;
	
	private String consteEstadoPendiente;
	private String consteEstadoAplicacion;
	
	private String consteAplicacionPrograma;
	private String consteAplicacionServicio;
	private String consteEncuestaGenerada;
	private String consteEncuestaPendiente;
	private String consteEncuestaAplicada;	
	private String valTipoEncuesta;
	private String bandera;
	
	private String tamListaBandeja;
	private String dscMensaje;
	
	private String nomUsuario;
	private String perfilUsuario;
	private String codResponsable;
	private String txtNombre;
	private String txhTipo;	
	private String txhTipoBusqueda;
	
	
	public String getTxhTipoBusqueda() {
		return txhTipoBusqueda;
	}
	public void setTxhTipoBusqueda(String txhTipoBusqueda) {
		this.txhTipoBusqueda = txhTipoBusqueda;
	}
	public String getTxhTipo() {
		return txhTipo;
	}
	public void setTxhTipo(String txhTipo) {
		this.txhTipo = txhTipo;
	}
	public String getTxtNombre() {
		return txtNombre;
	}
	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getPerfilUsuario() {
		return perfilUsuario;
	}
	public void setPerfilUsuario(String perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
	public String getDscMensaje() {
		return dscMensaje;
	}
	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}
	public String getTamListaBandeja() {
		return tamListaBandeja;
	}
	public void setTamListaBandeja(String tamListaBandeja) {
		this.tamListaBandeja = tamListaBandeja;
	}
	public String getBandera() {
		return bandera;
	}
	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
	public String getConsteEncuestaGenerada() {
		return consteEncuestaGenerada;
	}
	public void setConsteEncuestaGenerada(String consteEncuestaGenerada) {
		this.consteEncuestaGenerada = consteEncuestaGenerada;
	}
	public String getValTipoEncuesta() {
		return valTipoEncuesta;
	}
	public void setValTipoEncuesta(String valTipoEncuesta) {
		this.valTipoEncuesta = valTipoEncuesta;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getConsteTipoPrograma() {
		return consteTipoPrograma;
	}
	public void setConsteTipoPrograma(String consteTipoPrograma) {
		this.consteTipoPrograma = consteTipoPrograma;
	}
	public String getConsteTipoServicios() {
		return consteTipoServicios;
	}
	public void setConsteTipoServicios(String consteTipoServicios) {
		this.consteTipoServicios = consteTipoServicios;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodTipoAplicacion() {
		return codTipoAplicacion;
	}
	public void setCodTipoAplicacion(String codTipoAplicacion) {
		this.codTipoAplicacion = codTipoAplicacion;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public String getCodTipoEstado() {
		return codTipoEstado;
	}
	public void setCodTipoEstado(String codTipoEstado) {
		this.codTipoEstado = codTipoEstado;
	}
	public List getListaTipoAplicacion() {
		return listaTipoAplicacion;
	}
	public void setListaTipoAplicacion(List listaTipoAplicacion) {
		this.listaTipoAplicacion = listaTipoAplicacion;
	}
	public List getListaTipoEncuesta() {
		return listaTipoEncuesta;
	}
	public void setListaTipoEncuesta(List listaTipoEncuesta) {
		this.listaTipoEncuesta = listaTipoEncuesta;
	}
	public List getListaTipoServicio() {
		return listaTipoServicio;
	}
	public void setListaTipoServicio(List listaTipoServicio) {
		this.listaTipoServicio = listaTipoServicio;
	}
	public List getListaTipoEstado() {
		return listaTipoEstado;
	}
	public void setListaTipoEstado(List listaTipoEstado) {
		this.listaTipoEstado = listaTipoEstado;
	}
	public String getConsteEstadoPendiente() {
		return consteEstadoPendiente;
	}
	public void setConsteEstadoPendiente(String consteEstadoPendiente) {
		this.consteEstadoPendiente = consteEstadoPendiente;
	}
	public String getConsteEstadoAplicacion() {
		return consteEstadoAplicacion;
	}
	public void setConsteEstadoAplicacion(String consteEstadoAplicacion) {
		this.consteEstadoAplicacion = consteEstadoAplicacion;
	}
	public String getConsteAplicacionPrograma() {
		return consteAplicacionPrograma;
	}
	public void setConsteAplicacionPrograma(String consteAplicacionPrograma) {
		this.consteAplicacionPrograma = consteAplicacionPrograma;
	}
	public String getConsteAplicacionServicio() {
		return consteAplicacionServicio;
	}
	public void setConsteAplicacionServicio(String consteAplicacionServicio) {
		this.consteAplicacionServicio = consteAplicacionServicio;
	}
	public String getConsteEncuestaPendiente() {
		return consteEncuestaPendiente;
	}
	public void setConsteEncuestaPendiente(String consteEncuestaPendiente) {
		this.consteEncuestaPendiente = consteEncuestaPendiente;
	}
	public String getConsteEncuestaAplicada() {
		return consteEncuestaAplicada;
	}
	public void setConsteEncuestaAplicada(String consteEncuestaAplicada) {
		this.consteEncuestaAplicada = consteEncuestaAplicada;
	}	
}
