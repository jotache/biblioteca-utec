package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class AplicacionDatosGeneralesAlternativasCommand {
	private String 	operacion;
	private String 	usuario;
	private String 	txtAlternativa;
	private String 	txtPeso;
	private String 	txtDescripcion;
	private List	lstResultado;
	private String 	txhCodigo;
	private String 	txhCodEstado;
	
	public String getTxhCodEstado() {
		return txhCodEstado;
	}
	public void setTxhCodEstado(String txhCodEstado) {
		this.txhCodEstado = txhCodEstado;
	}
	public String getTxhCodigo() {
		return txhCodigo;
	}
	public void setTxhCodigo(String txhCodigo) {
		this.txhCodigo = txhCodigo;
	}
	public String getTxtAlternativa() {
		return txtAlternativa;
	}
	public void setTxtAlternativa(String txtAlternativa) {
		this.txtAlternativa = txtAlternativa;
	}
	public String getTxtPeso() {
		return txtPeso;
	}
	public void setTxtPeso(String txtPeso) {
		this.txtPeso = txtPeso;
	}
	public String getTxtDescripcion() {
		return txtDescripcion;
	}
	public void setTxtDescripcion(String txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
