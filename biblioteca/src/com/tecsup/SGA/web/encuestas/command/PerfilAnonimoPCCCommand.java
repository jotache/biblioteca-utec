package com.tecsup.SGA.web.encuestas.command;

public class PerfilAnonimoPCCCommand {
	
	private String enlace;
	
	private String codEncuesta;
	private String codPerfilEncuesta; //0000 anonimo_PCC
	private String codTipoEncuesta;
	private String tipoEncuesta; // Estandar=0001 - Mixta=0002
	
	private String codTipoAplicacion;
	
	private String dscMensaje;
	private String totalEncuestados;
	private String dscEncuestados;
	
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codSede;
	
	private String perfilPPCAnonimo;
	
	public String getPerfilPPCAnonimo() {
		return perfilPPCAnonimo;
	}

	public void setPerfilPPCAnonimo(String perfilPPCAnonimo) {
		this.perfilPPCAnonimo = perfilPPCAnonimo;
	}

	public String getTipoEncuesta() {
		return tipoEncuesta;
	}

	public void setTipoEncuesta(String tipoEncuesta) {
		this.tipoEncuesta = tipoEncuesta;
	}

	public String getCodEncuesta() {
		return codEncuesta;
	}

	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}

	public String getCodPerfilEncuesta() {
		return codPerfilEncuesta;
	}

	public void setCodPerfilEncuesta(String codPerfilEncuesta) {
		this.codPerfilEncuesta = codPerfilEncuesta;
	}

	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}

	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}

	public String getCodTipoAplicacion() {
		return codTipoAplicacion;
	}

	public void setCodTipoAplicacion(String codTipoAplicacion) {
		this.codTipoAplicacion = codTipoAplicacion;
	}

	public String getDscMensaje() {
		return dscMensaje;
	}

	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}

	public String getTotalEncuestados() {
		return totalEncuestados;
	}

	public void setTotalEncuestados(String totalEncuestados) {
		this.totalEncuestados = totalEncuestados;
	}

	public String getDscEncuestados() {
		return dscEncuestados;
	}

	public void setDscEncuestados(String dscEncuestados) {
		this.dscEncuestados = dscEncuestados;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getEnlace() {
		return enlace;
	}

	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}
	
	
}
