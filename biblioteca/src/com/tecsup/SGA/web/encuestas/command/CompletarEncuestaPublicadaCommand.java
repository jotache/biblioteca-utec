package com.tecsup.SGA.web.encuestas.command;

import java.util.List;

public class CompletarEncuestaPublicadaCommand {

	private String operacion;	
	private String codUsuario;
	private String msg;
	
	private List listaEncPublicadas;	
	
	private String codTipoEstandar;
	private String codTipoMixta;
	
	private String tamListaEncPublicadas;
	
	public List getListaEncPublicadas() {
		return listaEncPublicadas;
	}
	public void setListaEncPublicadas(List listaEncPublicadas) {
		this.listaEncPublicadas = listaEncPublicadas;
	}
	public String getTamListaEncPublicadas() {
		return tamListaEncPublicadas;
	}
	public void setTamListaEncPublicadas(String tamListaEncPublicadas) {
		this.tamListaEncPublicadas = tamListaEncPublicadas;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodTipoEstandar() {
		return codTipoEstandar;
	}
	public void setCodTipoEstandar(String codTipoEstandar) {
		this.codTipoEstandar = codTipoEstandar;
	}
	public String getCodTipoMixta() {
		return codTipoMixta;
	}
	public void setCodTipoMixta(String codTipoMixta) {
		this.codTipoMixta = codTipoMixta;
	}
}
