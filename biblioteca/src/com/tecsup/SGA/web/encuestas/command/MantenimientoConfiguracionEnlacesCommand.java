package com.tecsup.SGA.web.encuestas.command;

public class MantenimientoConfiguracionEnlacesCommand {

	private String codUsuario;

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
}
