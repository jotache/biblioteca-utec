package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class AplicacionDatosGeneralesFormatoCommand {
	private String 	operacion;
	private String 	usuario;
	private String 	txtFormato;
	private List	lstResultado;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTxtFormato() {
		return txtFormato;
	}
	public void setTxtFormato(String txtFormato) {
		this.txtFormato = txtFormato;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}

	
}
