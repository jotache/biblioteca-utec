package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class AplicacionDatosGeneralesMixtaAlternativasCommand {
	
	private String 	operacion;
	private String 	usuario;
	private String 	txtAlternativa;
	private String 	txtPeso;
	private String 	txtDescripcion;
	private List	lstResultado;
	private String 	txhCodigo;
	private String 	txhCodGrupo;
	private String 	txhVieneDePreguntasMixtas;
	private String 	txhCodEstado;	
	private String 	txhCodPregunta;//PARA ENCUESTA MIXTA
	
	private String tamanioBandeja;

	
	public String getTamanioBandeja() {
		return tamanioBandeja;
	}
	public void setTamanioBandeja(String tamanioBandeja) {
		this.tamanioBandeja = tamanioBandeja;
	}
	public String getTxhCodPregunta() {
		return txhCodPregunta;
	}
	public void setTxhCodPregunta(String txhCodPregunta) {
		this.txhCodPregunta = txhCodPregunta;
	}
	public String getTxtAlternativa() {
		return txtAlternativa;
	}
	public void setTxtAlternativa(String txtAlternativa) {
		this.txtAlternativa = txtAlternativa;
	}
	public String getTxtPeso() {
		return txtPeso;
	}
	public void setTxtPeso(String txtPeso) {
		this.txtPeso = txtPeso;
	}
	public String getTxtDescripcion() {
		return txtDescripcion;
	}
	public void setTxtDescripcion(String txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTxhCodigo() {
		return txhCodigo;
	}
	public void setTxhCodigo(String txhCodigo) {
		this.txhCodigo = txhCodigo;
	}
	public String getTxhCodGrupo() {
		return txhCodGrupo;
	}
	public void setTxhCodGrupo(String txhCodGrupo) {
		this.txhCodGrupo = txhCodGrupo;
	}
	public String getTxhVieneDePreguntasMixtas() {
		return txhVieneDePreguntasMixtas;
	}
	public void setTxhVieneDePreguntasMixtas(String txhVieneDePreguntasMixtas) {
		this.txhVieneDePreguntasMixtas = txhVieneDePreguntasMixtas;
	}
	public String getTxhCodEstado() {
		return txhCodEstado;
	}
	public void setTxhCodEstado(String txhCodEstado) {
		this.txhCodEstado = txhCodEstado;
	}
}
