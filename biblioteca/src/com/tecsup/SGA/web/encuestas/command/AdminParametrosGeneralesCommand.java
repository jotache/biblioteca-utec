package com.tecsup.SGA.web.encuestas.command;

public class AdminParametrosGeneralesCommand {

	public String operacion;	
	public String msg;
	public String codUsuario;
	public String nroDias;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getNroDias() {
		return nroDias;
	}
	public void setNroDias(String nroDias) {
		this.nroDias = nroDias;
	}
}
