package com.tecsup.SGA.web.encuestas.command;

import java.util.List;

public class AplicacionSeguimientoBandejaCommand {

	private String operacion;
	private String msg;
	private String codUsuario;
	private String constePrograma;
	private String consteServicio;
	private String codSeleccion;
	private String nroEncuesta;
	private String responsable;
	private String codResponsable;
	
	private String codTipoAplicacion;
	private List listaTipoAplicacion;
	
	private String codTipoEncuesta;
	private List listaTipoEncuesta;
	
	private String codTipoServicio;
	private List listaTipoServicio;
	
	private String codTipoEstado;
	private List listaTipoEstado;
	
	private List listaBandeja;
	private String tamListaBandeja;
	
	private String consteAplicacionPrograma;
	private String consteAplicacionServicio;
	
	private String codEncuesta;
	private String indEncuesta;
	private String parBandera;
	private String consCodEncEnAplicacion;
	
	private String nomUsuario;
	private String perfilUsuario;	
	
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getPerfilUsuario() {
		return perfilUsuario;
	}
	public void setPerfilUsuario(String perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
	public String getConsCodEncEnAplicacion() {
		return consCodEncEnAplicacion;
	}
	public void setConsCodEncEnAplicacion(String consCodEncEnAplicacion) {
		this.consCodEncEnAplicacion = consCodEncEnAplicacion;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getConsteAplicacionPrograma() {
		return consteAplicacionPrograma;
	}
	public void setConsteAplicacionPrograma(String consteAplicacionPrograma) {
		this.consteAplicacionPrograma = consteAplicacionPrograma;
	}
	public String getConsteAplicacionServicio() {
		return consteAplicacionServicio;
	}
	public void setConsteAplicacionServicio(String consteAplicacionServicio) {
		this.consteAplicacionServicio = consteAplicacionServicio;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getConstePrograma() {
		return constePrograma;
	}
	public void setConstePrograma(String constePrograma) {
		this.constePrograma = constePrograma;
	}
	public String getConsteServicio() {
		return consteServicio;
	}
	public void setConsteServicio(String consteServicio) {
		this.consteServicio = consteServicio;
	}
	public String getCodSeleccion() {
		return codSeleccion;
	}
	public void setCodSeleccion(String codSeleccion) {
		this.codSeleccion = codSeleccion;
	}	
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getCodTipoAplicacion() {
		return codTipoAplicacion;
	}
	public void setCodTipoAplicacion(String codTipoAplicacion) {
		this.codTipoAplicacion = codTipoAplicacion;
	}
	public List getListaTipoAplicacion() {
		return listaTipoAplicacion;
	}
	public void setListaTipoAplicacion(List listaTipoAplicacion) {
		this.listaTipoAplicacion = listaTipoAplicacion;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}
	public List getListaTipoEncuesta() {
		return listaTipoEncuesta;
	}
	public void setListaTipoEncuesta(List listaTipoEncuesta) {
		this.listaTipoEncuesta = listaTipoEncuesta;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public List getListaTipoServicio() {
		return listaTipoServicio;
	}
	public void setListaTipoServicio(List listaTipoServicio) {
		this.listaTipoServicio = listaTipoServicio;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getNroEncuesta() {
		return nroEncuesta;
	}
	public void setNroEncuesta(String nroEncuesta) {
		this.nroEncuesta = nroEncuesta;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getTamListaBandeja() {
		return tamListaBandeja;
	}
	public void setTamListaBandeja(String tamListaBandeja) {
		this.tamListaBandeja = tamListaBandeja;
	}
	public String getParBandera() {
		return parBandera;
	}
	public void setParBandera(String parBandera) {
		this.parBandera = parBandera;
	}
	public String getIndEncuesta() {
		return indEncuesta;
	}
	public void setIndEncuesta(String indEncuesta) {
		this.indEncuesta = indEncuesta;
	}
	public String getCodTipoEstado() {
		return codTipoEstado;
	}
	public void setCodTipoEstado(String codTipoEstado) {
		this.codTipoEstado = codTipoEstado;
	}
	public List getListaTipoEstado() {
		return listaTipoEstado;
	}
	public void setListaTipoEstado(List listaTipoEstado) {
		this.listaTipoEstado = listaTipoEstado;
	}
	
	
}
