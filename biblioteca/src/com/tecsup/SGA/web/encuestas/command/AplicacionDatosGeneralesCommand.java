package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class AplicacionDatosGeneralesCommand {
	
	private String 	operacion;
	private String 	usuario;
	private String 	cboTipoEncuesta;
	private List 	lstTipoEncuesta;
	private String 	cboSede;
	private List 	lstSede;
	private String 	txtCodigo;
	private String 	txhCodigo;//EXISTE O NO LA ENCUESTA
	private String 	txtNombre;
	private String 	txtDuracion;
	private String 	cboTipoServicio;
	private List 	lstTipoServicio;
	private String 	txaObservacion;
	private String 	cboTipoAplicacion;
	private List 	lstTipoAplicacion;
	private String 	txhCodPerfil;
	private String 	txhCodEncuestado;
	private String 	txhCodProfesor;
	private String 	parCodTipoAplicacion;
	private String 	parCodTipoEncuesta;
	private String 	parCodTipoServicio;
	private String 	parCodTipoEstado;
	private String 	parBandera;
	private String 	txhCodEstado;//estado de la encuesta
	private String 	txhNumPerfil;
	
	private List lstResultado;
	
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getCboTipoAplicacion() {
		return cboTipoAplicacion;
	}
	public void setCboTipoAplicacion(String cboTipoAplicacion) {
		this.cboTipoAplicacion = cboTipoAplicacion;
	}
	public List getLstTipoAplicacion() {
		return lstTipoAplicacion;
	}
	public void setLstTipoAplicacion(List lstTipoAplicacion) {
		this.lstTipoAplicacion = lstTipoAplicacion;
	}
	public String getCboTipoEncuesta() {
		return cboTipoEncuesta;
	}
	public void setCboTipoEncuesta(String cboTipoEncuesta) {
		this.cboTipoEncuesta = cboTipoEncuesta;
	}
	public List getLstTipoEncuesta() {
		return lstTipoEncuesta;
	}
	public void setLstTipoEncuesta(List lstTipoEncuesta) {
		this.lstTipoEncuesta = lstTipoEncuesta;
	}
	public String getCboSede() {
		return cboSede;
	}
	public void setCboSede(String cboSede) {
		this.cboSede = cboSede;
	}
	public List getLstSede() {
		return lstSede;
	}
	public void setLstSede(List lstSede) {
		this.lstSede = lstSede;
	}
	public String getTxtCodigo() {
		return txtCodigo;
	}
	public void setTxtCodigo(String txtCodigo) {
		this.txtCodigo = txtCodigo;
	}
	public String getTxtNombre() {
		return txtNombre;
	}
	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}
	public String getTxtDuracion() {
		return txtDuracion;
	}
	public void setTxtDuracion(String txtDuracion) {
		this.txtDuracion = txtDuracion;
	}
	public String getCboTipoServicio() {
		return cboTipoServicio;
	}
	public void setCboTipoServicio(String cboTipoServicio) {
		this.cboTipoServicio = cboTipoServicio;
	}
	public List getLstTipoServicio() {
		return lstTipoServicio;
	}
	public void setLstTipoServicio(List lstTipoServicio) {
		this.lstTipoServicio = lstTipoServicio;
	}
	public String getTxaObservacion() {
		return txaObservacion;
	}
	public void setTxaObservacion(String txaObservacion) {
		this.txaObservacion = txaObservacion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTxhCodigo() {
		return txhCodigo;
	}
	public void setTxhCodigo(String txhCodigo) {
		this.txhCodigo = txhCodigo;
	}
	public String getTxhCodPerfil() {
		return txhCodPerfil;
	}
	public void setTxhCodPerfil(String txhCodPerfil) {
		this.txhCodPerfil = txhCodPerfil;
	}
	public String getTxhCodEncuestado() {
		return txhCodEncuestado;
	}
	public void setTxhCodEncuestado(String txhCodEncuestado) {
		this.txhCodEncuestado = txhCodEncuestado;
	}
	public String getTxhCodProfesor() {
		return txhCodProfesor;
	}
	public void setTxhCodProfesor(String txhCodProfesor) {
		this.txhCodProfesor = txhCodProfesor;
	}
	public String getParCodTipoAplicacion() {
		return parCodTipoAplicacion;
	}
	public void setParCodTipoAplicacion(String parCodTipoAplicacion) {
		this.parCodTipoAplicacion = parCodTipoAplicacion;
	}
	public String getParCodTipoEncuesta() {
		return parCodTipoEncuesta;
	}
	public void setParCodTipoEncuesta(String parCodTipoEncuesta) {
		this.parCodTipoEncuesta = parCodTipoEncuesta;
	}
	public String getParCodTipoServicio() {
		return parCodTipoServicio;
	}
	public void setParCodTipoServicio(String parCodTipoServicio) {
		this.parCodTipoServicio = parCodTipoServicio;
	}
	public String getParCodTipoEstado() {
		return parCodTipoEstado;
	}
	public void setParCodTipoEstado(String parCodTipoEstado) {
		this.parCodTipoEstado = parCodTipoEstado;
	}
	public String getParBandera() {
		return parBandera;
	}
	public void setParBandera(String parBandera) {
		this.parBandera = parBandera;
	}
	public String getTxhCodEstado() {
		return txhCodEstado;
	}
	public void setTxhCodEstado(String txhCodEstado) {
		this.txhCodEstado = txhCodEstado;
	}
	public String getTxhNumPerfil() {
		return txhNumPerfil;
	}
	public void setTxhNumPerfil(String txhNumPerfil) {
		this.txhNumPerfil = txhNumPerfil;
	}
}
