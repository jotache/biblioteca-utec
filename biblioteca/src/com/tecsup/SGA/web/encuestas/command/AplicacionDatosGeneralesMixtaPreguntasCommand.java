package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class AplicacionDatosGeneralesMixtaPreguntasCommand {

	private String 	cboAlternativa;
	
	private String 	operacion;
	private String 	usuario;
	private String 	txtPregunta;
	private String 	chkObligatorio;
	private String 	cboTipo;
	private List	lstTipo;
	private List	lstResultado;
	private String 	txhCodigo;
	private String 	txtPeso;
	private String 	txhCodAlternativa;
	private String 	txhCodEstado;
	
	private String 	txhCodGrupo;
	
	public String getCboAlternativa() {
		return cboAlternativa;
	}
	public void setCboAlternativa(String cboAlternativa) {
		this.cboAlternativa = cboAlternativa;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTxtPregunta() {
		return txtPregunta;
	}
	public void setTxtPregunta(String txtPregunta) {
		this.txtPregunta = txtPregunta;
	}
	public String getChkObligatorio() {
		return chkObligatorio;
	}
	public void setChkObligatorio(String chkObligatorio) {
		this.chkObligatorio = chkObligatorio;
	}
	public String getCboTipo() {
		return cboTipo;
	}
	public void setCboTipo(String cboTipo) {
		this.cboTipo = cboTipo;
	}
	public List getLstTipo() {
		return lstTipo;
	}
	public void setLstTipo(List lstTipo) {
		this.lstTipo = lstTipo;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getTxhCodigo() {
		return txhCodigo;
	}
	public void setTxhCodigo(String txhCodigo) {
		this.txhCodigo = txhCodigo;
	}
	public String getTxhCodGrupo() {
		return txhCodGrupo;
	}
	public void setTxhCodGrupo(String txhCodGrupo) {
		this.txhCodGrupo = txhCodGrupo;
	}
	public String getTxtPeso() {
		return txtPeso;
	}
	public void setTxtPeso(String txtPeso) {
		this.txtPeso = txtPeso;
	}
	public String getTxhCodAlternativa() {
		return txhCodAlternativa;
	}
	public void setTxhCodAlternativa(String txhCodAlternativa) {
		this.txhCodAlternativa = txhCodAlternativa;
	}
	public String getTxhCodEstado() {
		return txhCodEstado;
	}
	public void setTxhCodEstado(String txhCodEstado) {
		this.txhCodEstado = txhCodEstado;
	}
	
}
