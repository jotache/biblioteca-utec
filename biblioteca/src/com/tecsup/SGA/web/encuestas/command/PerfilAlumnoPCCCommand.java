package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class PerfilAlumnoPCCCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codSede;
	
	private String codEncuesta;
	private String codPerfilEncuesta;
	private String codTipoEncuesta;
	
	private String cadenaCodDepartamentos;	
	private String cadenaCodCursos;
	
	private String indPrograma;	
	
	private List listaProducto;
	private String codProducto;
	
	private List listaPrograma;
	private String codPrograma;
	
	private List listaDepartamento;
	private String tamListaDepartamento;
	
	
	private List listaCurso;
	private String codCurso;
	
	private List listaCursoSel;
	private String codCursoSel;
	
	//constantes
	private String encuestaPrograma;
	private String encuestaSeccion;
	private String perfilPCC;
	
	private String dscMensaje;
	private String totalEncuestados;
	private String dscEncuestados;
	
	public String getTotalEncuestados() {
		return totalEncuestados;
	}
	public void setTotalEncuestados(String totalEncuestados) {
		this.totalEncuestados = totalEncuestados;
	}
	public String getDscEncuestados() {
		return dscEncuestados;
	}
	public void setDscEncuestados(String dscEncuestados) {
		this.dscEncuestados = dscEncuestados;
	}
	public String getDscMensaje() {
		return dscMensaje;
	}
	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}
	public String getEncuestaPrograma() {
		return encuestaPrograma;
	}
	public void setEncuestaPrograma(String encuestaPrograma) {
		this.encuestaPrograma = encuestaPrograma;
	}
	public String getEncuestaSeccion() {
		return encuestaSeccion;
	}
	public void setEncuestaSeccion(String encuestaSeccion) {
		this.encuestaSeccion = encuestaSeccion;
	}
	public String getPerfilPCC() {
		return perfilPCC;
	}
	public void setPerfilPCC(String perfilPCC) {
		this.perfilPCC = perfilPCC;
	}
	public List getListaCursoSel() {
		return listaCursoSel;
	}
	public void setListaCursoSel(List listaCursoSel) {
		this.listaCursoSel = listaCursoSel;
	}
	public String getCodCursoSel() {
		return codCursoSel;
	}
	public void setCodCursoSel(String codCursoSel) {
		this.codCursoSel = codCursoSel;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListaPrograma() {
		return listaPrograma;
	}
	public void setListaPrograma(List listaPrograma) {
		this.listaPrograma = listaPrograma;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}		
	public List getListaDepartamento() {
		return listaDepartamento;
	}
	public void setListaDepartamento(List listaDepartamento) {
		this.listaDepartamento = listaDepartamento;
	}
	public String getTamListaDepartamento() {
		return tamListaDepartamento;
	}
	public void setTamListaDepartamento(String tamListaDepartamento) {
		this.tamListaDepartamento = tamListaDepartamento;
	}	
	public List getListaCurso() {
		return listaCurso;
	}
	public void setListaCurso(List listaCurso) {
		this.listaCurso = listaCurso;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getCadenaCodCursos() {
		return cadenaCodCursos;
	}
	public void setCadenaCodCursos(String cadenaCodCursos) {
		this.cadenaCodCursos = cadenaCodCursos;
	}
	public String getCadenaCodDepartamentos() {
		return cadenaCodDepartamentos;
	}
	public void setCadenaCodDepartamentos(String cadenaCodDepartamentos) {
		this.cadenaCodDepartamentos = cadenaCodDepartamentos;
	}	
	public List getListaProducto() {
		return listaProducto;
	}
	public void setListaProducto(List listaProducto) {
		this.listaProducto = listaProducto;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getIndPrograma() {
		return indPrograma;
	}
	public void setIndPrograma(String indPrograma) {
		this.indPrograma = indPrograma;
	}
	public String getCodPerfilEncuesta() {
		return codPerfilEncuesta;
	}
	public void setCodPerfilEncuesta(String codPerfilEncuesta) {
		this.codPerfilEncuesta = codPerfilEncuesta;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}
	
	
}
