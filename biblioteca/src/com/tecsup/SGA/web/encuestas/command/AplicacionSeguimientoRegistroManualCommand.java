package com.tecsup.SGA.web.encuestas.command;


public class AplicacionSeguimientoRegistroManualCommand {
	private String 	operacion;
	private String 	txhUsuario;
	private String 	txhCodigo;
	private String 	flagMultiple;
	private String 	flagObligatorio;
	
	private String 	parCodTipoAplicacion;
	private String 	parCodTipoEncuesta;
	private String 	parCodTipoServicio;
	private String parCodTipoEstado;
	private String 	parCodEncuesta;
	private String 	parResponsable;
	private String 	parBandera;
	private String  parCodResponsable;
	
	public String getFlagMultiple() {
		return flagMultiple;
	}
	public void setFlagMultiple(String flagMultiple) {
		this.flagMultiple = flagMultiple;
	}
	public String getFlagObligatorio() {
		return flagObligatorio;
	}
	public void setFlagObligatorio(String flagObligatorio) {
		this.flagObligatorio = flagObligatorio;
	}
	public String getTxhCodigo() {
		return txhCodigo;
	}
	public void setTxhCodigo(String txhCodigo) {
		this.txhCodigo = txhCodigo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	
	public String getTxhUsuario() {
		return txhUsuario;
	}
	public void setTxhUsuario(String txhUsuario) {
		this.txhUsuario = txhUsuario;
	}
	public String getParCodTipoAplicacion() {
		return parCodTipoAplicacion;
	}
	public void setParCodTipoAplicacion(String parCodTipoAplicacion) {
		this.parCodTipoAplicacion = parCodTipoAplicacion;
	}
	public String getParCodTipoEncuesta() {
		return parCodTipoEncuesta;
	}
	public void setParCodTipoEncuesta(String parCodTipoEncuesta) {
		this.parCodTipoEncuesta = parCodTipoEncuesta;
	}
	public String getParCodTipoServicio() {
		return parCodTipoServicio;
	}
	public void setParCodTipoServicio(String parCodTipoServicio) {
		this.parCodTipoServicio = parCodTipoServicio;
	}
	public String getParCodEncuesta() {
		return parCodEncuesta;
	}
	public void setParCodEncuesta(String parCodEncuesta) {
		this.parCodEncuesta = parCodEncuesta;
	}
	public String getParResponsable() {
		return parResponsable;
	}
	public void setParResponsable(String parResponsable) {
		this.parResponsable = parResponsable;
	}
	public String getParBandera() {
		return parBandera;
	}
	public void setParBandera(String parBandera) {
		this.parBandera = parBandera;
	}
	public String getParCodResponsable() {
		return parCodResponsable;
	}
	public void setParCodResponsable(String parCodResponsable) {
		this.parCodResponsable = parCodResponsable;
	}
	public String getParCodTipoEstado() {
		return parCodTipoEstado;
	}
	public void setParCodTipoEstado(String parCodTipoEstado) {
		this.parCodTipoEstado = parCodTipoEstado;
	}
	
}
