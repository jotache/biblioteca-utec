package com.tecsup.SGA.web.encuestas.command;

public class ActConsultaEmpresaCommand {

	private String operacion;
	private String codSelec;
	private String descripSelec;
	private String msg;
	private String codUsuario;
	private String nombre;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
