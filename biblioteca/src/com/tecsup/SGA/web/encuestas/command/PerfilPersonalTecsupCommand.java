package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class PerfilPersonalTecsupCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codSede;
	
	private String codEncuesta;
	private String codPerfilEncuesta;
	private String codTipoEncuesta;
	
	private String cadPersonalSel;
	private String cadDocenteSel;	
	
	private String codPersonalDisponibles;
	private List listaPersonalDisponibles;
	
	private String codDocentesDisponibles;
	private List listaDocentesDisponibles;
	
	private String codPersonalSeleccionados;
	private List listaPersonalSeleccionado;
	
	private String codDocentesSeleccionados; 
	private List listaDocentesSeleccionado;
	//constantes
	private String encuestaPrograma;
	private String encuestaSeccion;
	private String perfilPERSONAL;
	
	private String dscMensaje;
	private String totalEncuestados;
	private String dscEncuestados;
	
	public String getTotalEncuestados() {
		return totalEncuestados;
	}
	public void setTotalEncuestados(String totalEncuestados) {
		this.totalEncuestados = totalEncuestados;
	}
	public String getDscEncuestados() {
		return dscEncuestados;
	}
	public void setDscEncuestados(String dscEncuestados) {
		this.dscEncuestados = dscEncuestados;
	}
	public String getDscMensaje() {
		return dscMensaje;
	}
	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}
	public String getEncuestaPrograma() {
		return encuestaPrograma;
	}
	public void setEncuestaPrograma(String encuestaPrograma) {
		this.encuestaPrograma = encuestaPrograma;
	}
	public String getEncuestaSeccion() {
		return encuestaSeccion;
	}
	public void setEncuestaSeccion(String encuestaSeccion) {
		this.encuestaSeccion = encuestaSeccion;
	}
	public String getPerfilPERSONAL() {
		return perfilPERSONAL;
	}
	public void setPerfilPERSONAL(String perfilPERSONAL) {
		this.perfilPERSONAL = perfilPERSONAL;
	}
	public String getCodDocentesSeleccionados() {
		return codDocentesSeleccionados;
	}
	public void setCodDocentesSeleccionados(String codDocentesSeleccionados) {
		this.codDocentesSeleccionados = codDocentesSeleccionados;
	}
	public List getListaDocentesSeleccionado() {
		return listaDocentesSeleccionado;
	}
	public void setListaDocentesSeleccionado(List listaDocentesSeleccionado) {
		this.listaDocentesSeleccionado = listaDocentesSeleccionado;
	}
	public String getCodPersonalSeleccionados() {
		return codPersonalSeleccionados;
	}
	public void setCodPersonalSeleccionados(String codPersonalSeleccionados) {
		this.codPersonalSeleccionados = codPersonalSeleccionados;
	}
	public List getListaPersonalSeleccionado() {
		return listaPersonalSeleccionado;
	}
	public void setListaPersonalSeleccionado(List listaPersonalSeleccionado) {
		this.listaPersonalSeleccionado = listaPersonalSeleccionado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getCodPersonalDisponibles() {
		return codPersonalDisponibles;
	}
	public void setCodPersonalDisponibles(String codPersonalDisponibles) {
		this.codPersonalDisponibles = codPersonalDisponibles;
	}
	public List getListaPersonalDisponibles() {
		return listaPersonalDisponibles;
	}
	public void setListaPersonalDisponibles(List listaPersonalDisponibles) {
		this.listaPersonalDisponibles = listaPersonalDisponibles;
	}
	public String getCodDocentesDisponibles() {
		return codDocentesDisponibles;
	}
	public void setCodDocentesDisponibles(String codDocentesDisponibles) {
		this.codDocentesDisponibles = codDocentesDisponibles;
	}
	public List getListaDocentesDisponibles() {
		return listaDocentesDisponibles;
	}
	public void setListaDocentesDisponibles(List listaDocentesDisponibles) {
		this.listaDocentesDisponibles = listaDocentesDisponibles;
	}	
	public String getCadPersonalSel() {
		return cadPersonalSel;
	}
	public void setCadPersonalSel(String cadPersonalSel) {
		this.cadPersonalSel = cadPersonalSel;
	}
	public String getCadDocenteSel() {
		return cadDocenteSel;
	}
	public void setCadDocenteSel(String cadDocenteSel) {
		this.cadDocenteSel = cadDocenteSel;
	}
	public String getCodPerfilEncuesta() {
		return codPerfilEncuesta;
	}
	public void setCodPerfilEncuesta(String codPerfilEncuesta) {
		this.codPerfilEncuesta = codPerfilEncuesta;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}	
}
