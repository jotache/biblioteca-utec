package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class AplicacionDatosGeneralesGruposCommand {
	private String 	operacion;
	private String 	usuario;	
	private String 	txtPeso;	
	private List	lstResultado;	
	private String 	txtGrupo;	
	private String 	cboFormato;
	private List 	lstFormato;	
	private String 	txhCodigo;
	private String 	txhCodEstado;
	
	public String getTxhCodEstado() {
		return txhCodEstado;
	}
	public void setTxhCodEstado(String txhCodEstado) {
		this.txhCodEstado = txhCodEstado;
	}
	public String getTxtPeso() {
		return txtPeso;
	}
	public void setTxtPeso(String txtPeso) {
		this.txtPeso = txtPeso;
	}	
	public String getTxtGrupo() {
		return txtGrupo;
	}
	public void setTxtGrupo(String txtGrupo) {
		this.txtGrupo = txtGrupo;
	}
	public String getCboFormato() {
		return cboFormato;
	}
	public void setCboFormato(String cboFormato) {
		this.cboFormato = cboFormato;
	}
	public List getLstFormato() {
		return lstFormato;
	}
	public void setLstFormato(List lstFormato) {
		this.lstFormato = lstFormato;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTxhCodigo() {
		return txhCodigo;
	}
	public void setTxhCodigo(String txhCodigo) {
		this.txhCodigo = txhCodigo;
	}
}
