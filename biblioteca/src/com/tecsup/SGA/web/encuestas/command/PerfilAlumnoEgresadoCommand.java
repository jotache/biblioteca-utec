package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class PerfilAlumnoEgresadoCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codSede;
	
	private String codEncuesta;
	private String codPerfilEncuesta;
	private String codTipoEncuesta;
	
	private String codProgramaEgresado;
	private List listaProgramaEgresado;
	
	private String añoIngreso;
	private String añoEgreso;
	//constantes
	private String encuestaPrograma;
	private String encuestaSeccion;
	private String perfilEGRESADO;
	
	private String dscMensaje;
	private String totalEncuestados;
	private String dscEncuestados;
	
	public String getTotalEncuestados() {
		return totalEncuestados;
	}
	public void setTotalEncuestados(String totalEncuestados) {
		this.totalEncuestados = totalEncuestados;
	}
	public String getDscEncuestados() {
		return dscEncuestados;
	}
	public void setDscEncuestados(String dscEncuestados) {
		this.dscEncuestados = dscEncuestados;
	}
	public String getDscMensaje() {
		return dscMensaje;
	}
	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}
	public String getEncuestaPrograma() {
		return encuestaPrograma;
	}
	public void setEncuestaPrograma(String encuestaPrograma) {
		this.encuestaPrograma = encuestaPrograma;
	}
	public String getEncuestaSeccion() {
		return encuestaSeccion;
	}
	public void setEncuestaSeccion(String encuestaSeccion) {
		this.encuestaSeccion = encuestaSeccion;
	}
	public String getPerfilEGRESADO() {
		return perfilEGRESADO;
	}
	public void setPerfilEGRESADO(String perfilEGRESADO) {
		this.perfilEGRESADO = perfilEGRESADO;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodProgramaEgresado() {
		return codProgramaEgresado;
	}
	public void setCodProgramaEgresado(String codProgramaEgresado) {
		this.codProgramaEgresado = codProgramaEgresado;
	}
	public List getListaProgramaEgresado() {
		return listaProgramaEgresado;
	}
	public void setListaProgramaEgresado(List listaProgramaEgresado) {
		this.listaProgramaEgresado = listaProgramaEgresado;
	}
	public String getAñoIngreso() {
		return añoIngreso;
	}
	public void setAñoIngreso(String añoIngreso) {
		this.añoIngreso = añoIngreso;
	}
	public String getAñoEgreso() {
		return añoEgreso;
	}
	public void setAñoEgreso(String añoEgreso) {
		this.añoEgreso = añoEgreso;
	}
	public String getCodPerfilEncuesta() {
		return codPerfilEncuesta;
	}
	public void setCodPerfilEncuesta(String codPerfilEncuesta) {
		this.codPerfilEncuesta = codPerfilEncuesta;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}
		
}
