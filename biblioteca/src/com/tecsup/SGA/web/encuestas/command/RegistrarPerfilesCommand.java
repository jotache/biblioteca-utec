package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class RegistrarPerfilesCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codSede;
	
	private String tipoEncuesta;// 0->Programas 1->Servicios/Otros 
	private String tipoPerfil;// 1->PFR 2->PCC 3->EGRESADOS 4->PERSONAL
	
	private String codDepartamento;
	
	private List listaPrograma;
	private String codPrograma;	
	
	private List listaPaquete;
	private String codPaquete;
	
	private List listaDepartamento;
	private String tamListaDepartamento;
	
	private List listaCiclo;
	private String tamListaCiclo;
	
	private List listaCursoDepartamento;
	private String codCursoDepartamento;
	
	private List listaProgramaEgresado;
	private String codProgramaEgresado;
	
	private List listaPersonalDisponibles;
	private String codPersonalDisponibles;
	
	private List listaDocentesDisponibles;
	private String codDocentesDisponibles;
	
	public List getListaPersonalDisponibles() {
		return listaPersonalDisponibles;
	}
	public void setListaPersonalDisponibles(List listaPersonalDisponibles) {
		this.listaPersonalDisponibles = listaPersonalDisponibles;
	}
	public String getCodPersonalDisponibles() {
		return codPersonalDisponibles;
	}
	public void setCodPersonalDisponibles(String codPersonalDisponibles) {
		this.codPersonalDisponibles = codPersonalDisponibles;
	}
	public List getListaDocentesDisponibles() {
		return listaDocentesDisponibles;
	}
	public void setListaDocentesDisponibles(List listaDocentesDisponibles) {
		this.listaDocentesDisponibles = listaDocentesDisponibles;
	}
	public String getCodDocentesDisponibles() {
		return codDocentesDisponibles;
	}
	public void setCodDocentesDisponibles(String codDocentesDisponibles) {
		this.codDocentesDisponibles = codDocentesDisponibles;
	}
	public List getListaProgramaEgresado() {
		return listaProgramaEgresado;
	}
	public void setListaProgramaEgresado(List listaProgramaEgresado) {
		this.listaProgramaEgresado = listaProgramaEgresado;
	}
	public String getCodProgramaEgresado() {
		return codProgramaEgresado;
	}
	public void setCodProgramaEgresado(String codProgramaEgresado) {
		this.codProgramaEgresado = codProgramaEgresado;
	}
	public List getListaCiclo() {
		return listaCiclo;
	}
	public void setListaCiclo(List listaCiclo) {
		this.listaCiclo = listaCiclo;
	}
	public String getTamListaCiclo() {
		return tamListaCiclo;
	}
	public void setTamListaCiclo(String tamListaCiclo) {
		this.tamListaCiclo = tamListaCiclo;
	}
	public String getTamListaDepartamento() {
		return tamListaDepartamento;
	}
	public void setTamListaDepartamento(String tamListaDepartamento) {
		this.tamListaDepartamento = tamListaDepartamento;
	}
	public List getListaDepartamento() {
		return listaDepartamento;
	}
	public void setListaDepartamento(List listaDepartamento) {
		this.listaDepartamento = listaDepartamento;
	}	
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodPaquete() {
		return codPaquete;
	}
	public void setCodPaquete(String codPaquete) {
		this.codPaquete = codPaquete;
	}
	public List getListaPrograma() {
		return listaPrograma;
	}
	public void setListaPrograma(List listaPrograma) {
		this.listaPrograma = listaPrograma;
	}
	public List getListaPaquete() {
		return listaPaquete;
	}
	public void setListaPaquete(List listaPaquete) {
		this.listaPaquete = listaPaquete;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getTipoEncuesta() {
		return tipoEncuesta;
	}
	public void setTipoEncuesta(String tipoEncuesta) {
		this.tipoEncuesta = tipoEncuesta;
	}
	public String getTipoPerfil() {
		return tipoPerfil;
	}
	public void setTipoPerfil(String tipoPerfil) {
		this.tipoPerfil = tipoPerfil;
	}
	public List getListaCursoDepartamento() {
		return listaCursoDepartamento;
	}
	public void setListaCursoDepartamento(List listaCursoDepartamento) {
		this.listaCursoDepartamento = listaCursoDepartamento;
	}
	public String getCodCursoDepartamento() {
		return codCursoDepartamento;
	}
	public void setCodCursoDepartamento(String codCursoDepartamento) {
		this.codCursoDepartamento = codCursoDepartamento;
	}
	public String getCodDepartamento() {
		return codDepartamento;
	}
	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}
}
