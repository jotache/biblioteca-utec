package com.tecsup.SGA.web.encuestas.command;

import java.util.*;

public class ConsultaReportesEncuestaCommand {
	private String codUsuario;
	private String operacion;
	private String msg;
	//*****************************
	private String codReportes;
	private List listaReportes;
	
	private String nomUsuario;
	private String perfilUsuario;
	
	private String responsable;
	
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getCodReportes() {
		return codReportes;
	}
	public void setCodReportes(String codReportes) {
		this.codReportes = codReportes;
	}	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public void setListaReportes(List listaReportes) {
		this.listaReportes = listaReportes;
	}
	public List getListaReportes() {
		return listaReportes;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getPerfilUsuario() {
		return perfilUsuario;
	}
	public void setPerfilUsuario(String perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
}
