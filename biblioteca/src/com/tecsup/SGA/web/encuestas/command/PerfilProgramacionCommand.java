package com.tecsup.SGA.web.encuestas.command;

import java.util.Map;



public class PerfilProgramacionCommand {
	private String operacion;
	private String msg;
	private String dscMensaje;
	private String codUsuario;
	private String codSede;
	//campos de la cabecera
	private String codTipoAplicacion;
	private String tipoAplicacion;
	private String sede;
	private String tipoEncuesta;
	private String codigo;
	private String nombre;
	private String duracion;
	private String responsable;
	private String tipoServicio;
	private String totalEncuestado;	
	//para manejar la BD	
	private String codEncuesta;
	private String codPerfilEncuesta;
	//parea grabar la cabecera	
	private String codResponsable;
	private String indicadorManual;
	
	private String dscEncuestados;
	private String codEstado;	
	//constantes
	private String estadoGenerada;
	private String estadoEnAplicacion;
	private String constPerfilAnonimo_PCC;
	private String constPerfilPFC;
	private String constPerfilPCC;
	private String constPerfilEGRESADO;
	private String constPerfilPERSONAL;
	private String constOrientado;	
	//parametros a devolver a la bandeja 
	private String parCodTipoAplicacion;
	private String parCodTipoEncuesta;
	private String parCodTipoServicio;
	private String parCodTipoEstado;
	private String parBandera;
	
	public String getConstPerfilAnonimo_PCC() {
		return constPerfilAnonimo_PCC;
	}
	public void setConstPerfilAnonimo_PCC(String constPerfilAnonimo_PCC) {
		this.constPerfilAnonimo_PCC = constPerfilAnonimo_PCC;
	}
	public String getEstadoGenerada() {
		return estadoGenerada;
	}
	public void setEstadoGenerada(String estadoGenerada) {
		this.estadoGenerada = estadoGenerada;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	
	public String getParCodTipoAplicacion() {
		return parCodTipoAplicacion;
	}
	public void setParCodTipoAplicacion(String parCodTipoAplicacion) {
		this.parCodTipoAplicacion = parCodTipoAplicacion;
	}
	public String getParCodTipoEncuesta() {
		return parCodTipoEncuesta;
	}
	public void setParCodTipoEncuesta(String parCodTipoEncuesta) {
		this.parCodTipoEncuesta = parCodTipoEncuesta;
	}
	public String getParCodTipoServicio() {
		return parCodTipoServicio;
	}
	public void setParCodTipoServicio(String parCodTipoServicio) {
		this.parCodTipoServicio = parCodTipoServicio;
	}
	public String getParCodTipoEstado() {
		return parCodTipoEstado;
	}
	public void setParCodTipoEstado(String parCodTipoEstado) {
		this.parCodTipoEstado = parCodTipoEstado;
	}
	public String getParBandera() {
		return parBandera;
	}
	public void setParBandera(String parBandera) {
		this.parBandera = parBandera;
	}
	public String getDscEncuestados() {
		return dscEncuestados;
	}
	public void setDscEncuestados(String dscEncuestados) {
		this.dscEncuestados = dscEncuestados;
	}
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getIndicadorManual() {
		return indicadorManual;
	}
	public void setIndicadorManual(String indicadorManual) {
		this.indicadorManual = indicadorManual;
	}	
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getTipoAplicacion() {
		return tipoAplicacion;
	}
	public void setTipoAplicacion(String tipoAplicacion) {
		this.tipoAplicacion = tipoAplicacion;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getTipoEncuesta() {
		return tipoEncuesta;
	}
	public void setTipoEncuesta(String tipoEncuesta) {
		this.tipoEncuesta = tipoEncuesta;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getTotalEncuestado() {
		return totalEncuestado;
	}
	public void setTotalEncuestado(String totalEncuestado) {
		this.totalEncuestado = totalEncuestado;
	}
	public String getCodPerfilEncuesta() {
		return codPerfilEncuesta;
	}
	public void setCodPerfilEncuesta(String codPerfilEncuesta) {
		this.codPerfilEncuesta = codPerfilEncuesta;
	}
	public String getCodTipoAplicacion() {
		return codTipoAplicacion;
	}
	public void setCodTipoAplicacion(String codTipoAplicacion) {
		this.codTipoAplicacion = codTipoAplicacion;
	}
	public String getConstPerfilPFC() {
		return constPerfilPFC;
	}
	public void setConstPerfilPFC(String constPerfilPFC) {
		this.constPerfilPFC = constPerfilPFC;
	}
	public String getConstPerfilPCC() {
		return constPerfilPCC;
	}
	public void setConstPerfilPCC(String constPerfilPCC) {
		this.constPerfilPCC = constPerfilPCC;
	}
	public String getConstPerfilEGRESADO() {
		return constPerfilEGRESADO;
	}
	public void setConstPerfilEGRESADO(String constPerfilEGRESADO) {
		this.constPerfilEGRESADO = constPerfilEGRESADO;
	}
	public String getConstPerfilPERSONAL() {
		return constPerfilPERSONAL;
	}
	public void setConstPerfilPERSONAL(String constPerfilPERSONAL) {
		this.constPerfilPERSONAL = constPerfilPERSONAL;
	}
	public String getConstOrientado() {
		return constOrientado;
	}
	public void setConstOrientado(String constOrientado) {
		this.constOrientado = constOrientado;
	}
	public String getEstadoEnAplicacion() {
		return estadoEnAplicacion;
	}
	public void setEstadoEnAplicacion(String estadoEnAplicacion) {
		this.estadoEnAplicacion = estadoEnAplicacion;
	}
	public String getDscMensaje() {
		return dscMensaje;
	}
	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}
}
