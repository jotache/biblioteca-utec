package com.tecsup.SGA.web.encuestas.command;

public class AdminTipoServicioAgregarCommand {

	private String operacion;
	private String codUsuario;
	private String codSeleccion;
	private String codOpcion;
	private String msg;
	private String tipoServicio;
	private String tipoBandera;
	
	public String getTipoBandera() {
		return tipoBandera;
	}
	public void setTipoBandera(String tipoBandera) {
		this.tipoBandera = tipoBandera;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSeleccion() {
		return codSeleccion;
	}
	public void setCodSeleccion(String codSeleccion) {
		this.codSeleccion = codSeleccion;
	}
	public String getCodOpcion() {
		return codOpcion;
	}
	public void setCodOpcion(String codOpcion) {
		this.codOpcion = codOpcion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	
}
