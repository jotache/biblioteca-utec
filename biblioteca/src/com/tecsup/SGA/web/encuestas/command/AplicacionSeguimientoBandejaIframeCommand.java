package com.tecsup.SGA.web.encuestas.command;

import java.util.List;

public class AplicacionSeguimientoBandejaIframeCommand {
	
	private String operacion;
	private String codSeleccion;
	private String codUsuario;
	private String tamListaBandeja;
	private List listaBandeja;
	private String parCodTipoAplicacion;
	private String parCodTipoEncuesta;
	private String parCodTipoServicio;
	private String parCodEncuesta;
	private String parResponsable;
	private String parBandera;
	private String parCodTipoEstado;
	
	private String nomUsuario;
	private String perfilUsuario;
	private String codResponsable;
	
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getPerfilUsuario() {
		return perfilUsuario;
	}
	public void setPerfilUsuario(String perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getParCodTipoEstado() {
		return parCodTipoEstado;
	}
	public void setParCodTipoEstado(String parCodTipoEstado) {
		this.parCodTipoEstado = parCodTipoEstado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSeleccion() {
		return codSeleccion;
	}
	public void setCodSeleccion(String codSeleccion) {
		this.codSeleccion = codSeleccion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getTamListaBandeja() {
		return tamListaBandeja;
	}
	public void setTamListaBandeja(String tamListaBandeja) {
		this.tamListaBandeja = tamListaBandeja;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getParCodTipoAplicacion() {
		return parCodTipoAplicacion;
	}
	public void setParCodTipoAplicacion(String parCodTipoAplicacion) {
		this.parCodTipoAplicacion = parCodTipoAplicacion;
	}
	public String getParCodTipoEncuesta() {
		return parCodTipoEncuesta;
	}
	public void setParCodTipoEncuesta(String parCodTipoEncuesta) {
		this.parCodTipoEncuesta = parCodTipoEncuesta;
	}
	public String getParCodTipoServicio() {
		return parCodTipoServicio;
	}
	public void setParCodTipoServicio(String parCodTipoServicio) {
		this.parCodTipoServicio = parCodTipoServicio;
	}
	public String getParCodEncuesta() {
		return parCodEncuesta;
	}
	public void setParCodEncuesta(String parCodEncuesta) {
		this.parCodEncuesta = parCodEncuesta;
	}
	public String getParResponsable() {
		return parResponsable;
	}
	public void setParResponsable(String parResponsable) {
		this.parResponsable = parResponsable;
	}
	public String getParBandera() {
		return parBandera;
	}
	public void setParBandera(String parBandera) {
		this.parBandera = parBandera;
	}
}
