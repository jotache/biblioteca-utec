package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class OrientacionPerfilCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codSede;	
	
	private String codEncuesta;
	private String codPerfilEncuesta;
	private String codPerfilSeleccion;
	
	private List listaProfesoresPFR;
	private String tamListaProfesoresPFR;
	
	private List listaProfesoresPCC;
	private String tamListaProfesoresPCC;
	
	private List listaGeneral;
	private String tamListaGeneral;
	
	private String tamMostrar;//es el tama�o de registros que se muestra cada vez que se cambia de perfil
	//a�adido para eliminar registro	
	private String codProfesor;
	private String codProducto;
	private String codPrograma;
	private String codCentroCosto;
	private String codCurso;
	private String codCiclo;
	private String codSeccion;	
	
	private String totalEncuestados;
	private String dscEncuestados;
	private String dscMensaje;
	
	public String getDscMensaje() {
		return dscMensaje;
	}
	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}
	public String getTotalEncuestados() {
		return totalEncuestados;
	}
	public void setTotalEncuestados(String totalEncuestados) {
		this.totalEncuestados = totalEncuestados;
	}
	public String getDscEncuestados() {
		return dscEncuestados;
	}
	public void setDscEncuestados(String dscEncuestados) {
		this.dscEncuestados = dscEncuestados;
	}
	public String getCodProfesor() {
		return codProfesor;
	}
	public void setCodProfesor(String codProfesor) {
		this.codProfesor = codProfesor;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodCentroCosto() {
		return codCentroCosto;
	}
	public void setCodCentroCosto(String codCentroCosto) {
		this.codCentroCosto = codCentroCosto;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getTamMostrar() {
		return tamMostrar;
	}
	public void setTamMostrar(String tamMostrar) {
		this.tamMostrar = tamMostrar;
	}
	public List getListaGeneral() {
		return listaGeneral;
	}
	public void setListaGeneral(List listaGeneral) {
		this.listaGeneral = listaGeneral;
	}
	public String getTamListaGeneral() {
		return tamListaGeneral;
	}
	public void setTamListaGeneral(String tamListaGeneral) {
		this.tamListaGeneral = tamListaGeneral;
	}
	public List getListaProfesoresPCC() {
		return listaProfesoresPCC;
	}
	public void setListaProfesoresPCC(List listaProfesoresPCC) {
		this.listaProfesoresPCC = listaProfesoresPCC;
	}
	public String getTamListaProfesoresPCC() {
		return tamListaProfesoresPCC;
	}
	public void setTamListaProfesoresPCC(String tamListaProfesoresPCC) {
		this.tamListaProfesoresPCC = tamListaProfesoresPCC;
	}
	public List getListaProfesoresPFR() {
		return listaProfesoresPFR;
	}
	public void setListaProfesoresPFR(List listaProfesoresPFR) {
		this.listaProfesoresPFR = listaProfesoresPFR;
	}
	public String getTamListaProfesoresPFR() {
		return tamListaProfesoresPFR;
	}
	public void setTamListaProfesoresPFR(String tamListaProfesoresPFR) {
		this.tamListaProfesoresPFR = tamListaProfesoresPFR;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}	
	public String getCodPerfilEncuesta() {
		return codPerfilEncuesta;
	}
	public void setCodPerfilEncuesta(String codPerfilEncuesta) {
		this.codPerfilEncuesta = codPerfilEncuesta;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getCodPerfilSeleccion() {
		return codPerfilSeleccion;
	}
	public void setCodPerfilSeleccion(String codPerfilSeleccion) {
		this.codPerfilSeleccion = codPerfilSeleccion;
	}
}
