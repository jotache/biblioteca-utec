package com.tecsup.SGA.web.encuestas.command;

public class ConGenTipoFormatoCommand {

	private String operacion;
	private String codSelec;
	private String descripSelec;
	private String msg;
	private String codUsuario;
	private String cadCodSelec;
	private String cadCodDescripSelec;
	private String dscTipoFormato;
	private String codFormatoEncuesta;
	private String nroTotalRegistros;
	
	public String getNroTotalRegistros() {
		return nroTotalRegistros;
	}
	public void setNroTotalRegistros(String nroTotalRegistros) {
		this.nroTotalRegistros = nroTotalRegistros;
	}
	public String getCodFormatoEncuesta() {
		return codFormatoEncuesta;
	}
	public void setCodFormatoEncuesta(String codFormatoEncuesta) {
		this.codFormatoEncuesta = codFormatoEncuesta;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCadCodSelec() {
		return cadCodSelec;
	}
	public void setCadCodSelec(String cadCodSelec) {
		this.cadCodSelec = cadCodSelec;
	}
	public String getCadCodDescripSelec() {
		return cadCodDescripSelec;
	}
	public void setCadCodDescripSelec(String cadCodDescripSelec) {
		this.cadCodDescripSelec = cadCodDescripSelec;
	}
	public String getDscTipoFormato() {
		return dscTipoFormato;
	}
	public void setDscTipoFormato(String dscTipoFormato) {
		this.dscTipoFormato = dscTipoFormato;
	}
}
