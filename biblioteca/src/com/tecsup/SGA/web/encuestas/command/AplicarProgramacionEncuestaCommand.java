package com.tecsup.SGA.web.encuestas.command;

import java.util.List;



public class AplicarProgramacionEncuestaCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codSede;
	private String codEncuesta;
	//campos de la cabecera	
	private String fechaVigenciaIni;
	private String horaVigenciaIni;
	private String fechaVigenciaFin;
	private String horaVigenciaFin;
	
	private List listaHoraInicio;
	private List listaHoraFin;
	
	private String fechaActual;
	private String horaActual;
	
	private String dscMensaje;
	
	public String getDscMensaje() {
		return dscMensaje;
	}
	public void setDscMensaje(String dscMensaje) {
		this.dscMensaje = dscMensaje;
	}
	public String getHoraActual() {
		return horaActual;
	}
	public void setHoraActual(String horaActual) {
		this.horaActual = horaActual;
	}
	public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	public List getListaHoraInicio() {
		return listaHoraInicio;
	}
	public void setListaHoraInicio(List listaHoraInicio) {
		this.listaHoraInicio = listaHoraInicio;
	}
	public List getListaHoraFin() {
		return listaHoraFin;
	}
	public void setListaHoraFin(List listaHoraFin) {
		this.listaHoraFin = listaHoraFin;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getFechaVigenciaIni() {
		return fechaVigenciaIni;
	}
	public void setFechaVigenciaIni(String fechaVigenciaIni) {
		this.fechaVigenciaIni = fechaVigenciaIni;
	}
	public String getHoraVigenciaIni() {
		return horaVigenciaIni;
	}
	public void setHoraVigenciaIni(String horaVigenciaIni) {
		this.horaVigenciaIni = horaVigenciaIni;
	}
	public String getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}
	public void setFechaVigenciaFin(String fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}
	public String getHoraVigenciaFin() {
		return horaVigenciaFin;
	}
	public void setHoraVigenciaFin(String horaVigenciaFin) {
		this.horaVigenciaFin = horaVigenciaFin;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
}
