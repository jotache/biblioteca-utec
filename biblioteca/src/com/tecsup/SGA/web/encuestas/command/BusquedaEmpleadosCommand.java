package com.tecsup.SGA.web.encuestas.command;

import java.util.List;

public class BusquedaEmpleadosCommand {

	private String operacion;
	private String codSelec;
	private String descripSelec;
	private String msg;
	private String codUsuario;
	private String codTipoPersonal;
	private List listaTipoPersonal;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String codPersonal;
	private String codBDTipoPersonal;
	private String nombrePersonal;
	private String flag;
	
	private String tipoPersonal;
	private String tipoDocente;
	private String usuPersonal;	
	
	public String getUsuPersonal() {
		return usuPersonal;
	}
	public void setUsuPersonal(String usuPersonal) {
		this.usuPersonal = usuPersonal;
	}
	public String getTipoDocente() {
		return tipoDocente;
	}
	public void setTipoDocente(String tipoDocente) {
		this.tipoDocente = tipoDocente;
	}
	public String getTipoPersonal() {
		return tipoPersonal;
	}
	public void setTipoPersonal(String tipoPersonal) {
		this.tipoPersonal = tipoPersonal;
	}
	public String getNombrePersonal() {
		return nombrePersonal;
	}
	public void setNombrePersonal(String nombrePersonal) {
		this.nombrePersonal = nombrePersonal;
	}
	public String getCodPersonal() {
		return codPersonal;
	}
	public void setCodPersonal(String codPersonal) {
		this.codPersonal = codPersonal;
	}
	public String getCodBDTipoPersonal() {
		return codBDTipoPersonal;
	}
	public void setCodBDTipoPersonal(String codBDTipoPersonal) {
		this.codBDTipoPersonal = codBDTipoPersonal;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodTipoPersonal() {
		return codTipoPersonal;
	}
	public void setCodTipoPersonal(String codTipoPersonal) {
		this.codTipoPersonal = codTipoPersonal;
	}
	public List getListaTipoPersonal() {
		return listaTipoPersonal;
	}
	public void setListaTipoPersonal(List listaTipoPersonal) {
		this.listaTipoPersonal = listaTipoPersonal;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
}
