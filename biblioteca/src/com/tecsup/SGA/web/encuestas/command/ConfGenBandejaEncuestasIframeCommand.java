package com.tecsup.SGA.web.encuestas.command;

public class ConfGenBandejaEncuestasIframeCommand {
	
	private String operacion;
	private String codSelec;
	private String codUsuario;
	private String consteTipoPrograma;
	private String consteTipoServicios;
	private String consteEstadoPendiente;
	private String consteEstadoAplicacion;
	private String consteAplicacionPrograma;
	private String consteAplicacionServicio;
	private String consteEncuestaGenerada;
	private String consteEncuestaPendiente;
	private String consteEncuestaAplicada;
	private String valTipoEncuesta;
	private String msg;
	
	private String parCodTipoAplicacion;
	private String parCodTipoEncuesta;
	private String parCodTipoEstado;
	private String parCodTipoServicio;
	private String parBandera;
	private String tamListaBandeja;
	
	private String nomUsuario;
	private String perfilUsuario;
	private String codResponsable;
	private String txtBusqueda;
	
	public String getTxtBusqueda() {
		return txtBusqueda;
	}
	public void setTxtBusqueda(String txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getPerfilUsuario() {
		return perfilUsuario;
	}
	public void setPerfilUsuario(String perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getTamListaBandeja() {
		return tamListaBandeja;
	}
	public void setTamListaBandeja(String tamListaBandeja) {
		this.tamListaBandeja = tamListaBandeja;
	}
	public String getParCodTipoAplicacion() {
		return parCodTipoAplicacion;
	}
	public void setParCodTipoAplicacion(String parCodTipoAplicacion) {
		this.parCodTipoAplicacion = parCodTipoAplicacion;
	}
	public String getParCodTipoEncuesta() {
		return parCodTipoEncuesta;
	}
	public void setParCodTipoEncuesta(String parCodTipoEncuesta) {
		this.parCodTipoEncuesta = parCodTipoEncuesta;
	}
	public String getParCodTipoEstado() {
		return parCodTipoEstado;
	}
	public void setParCodTipoEstado(String parCodTipoEstado) {
		this.parCodTipoEstado = parCodTipoEstado;
	}
	public String getParCodTipoServicio() {
		return parCodTipoServicio;
	}
	public void setParCodTipoServicio(String parCodTipoServicio) {
		this.parCodTipoServicio = parCodTipoServicio;
	}
	public String getParBandera() {
		return parBandera;
	}
	public void setParBandera(String parBandera) {
		this.parBandera = parBandera;
	}
	public String getConsteAplicacionPrograma() {
		return consteAplicacionPrograma;
	}
	public void setConsteAplicacionPrograma(String consteAplicacionPrograma) {
		this.consteAplicacionPrograma = consteAplicacionPrograma;
	}
	public String getConsteAplicacionServicio() {
		return consteAplicacionServicio;
	}
	public void setConsteAplicacionServicio(String consteAplicacionServicio) {
		this.consteAplicacionServicio = consteAplicacionServicio;
	}
	public String getConsteEncuestaGenerada() {
		return consteEncuestaGenerada;
	}
	public void setConsteEncuestaGenerada(String consteEncuestaGenerada) {
		this.consteEncuestaGenerada = consteEncuestaGenerada;
	}
	public String getConsteEncuestaPendiente() {
		return consteEncuestaPendiente;
	}
	public void setConsteEncuestaPendiente(String consteEncuestaPendiente) {
		this.consteEncuestaPendiente = consteEncuestaPendiente;
	}
	public String getConsteEncuestaAplicada() {
		return consteEncuestaAplicada;
	}
	public void setConsteEncuestaAplicada(String consteEncuestaAplicada) {
		this.consteEncuestaAplicada = consteEncuestaAplicada;
	}
	public String getValTipoEncuesta() {
		return valTipoEncuesta;
	}
	public void setValTipoEncuesta(String valTipoEncuesta) {
		this.valTipoEncuesta = valTipoEncuesta;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getConsteTipoPrograma() {
		return consteTipoPrograma;
	}
	public void setConsteTipoPrograma(String consteTipoPrograma) {
		this.consteTipoPrograma = consteTipoPrograma;
	}
	public String getConsteTipoServicios() {
		return consteTipoServicios;
	}
	public void setConsteTipoServicios(String consteTipoServicios) {
		this.consteTipoServicios = consteTipoServicios;
	}
	public String getConsteEstadoPendiente() {
		return consteEstadoPendiente;
	}
	public void setConsteEstadoPendiente(String consteEstadoPendiente) {
		this.consteEstadoPendiente = consteEstadoPendiente;
	}
	public String getConsteEstadoAplicacion() {
		return consteEstadoAplicacion;
	}
	public void setConsteEstadoAplicacion(String consteEstadoAplicacion) {
		this.consteEstadoAplicacion = consteEstadoAplicacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
