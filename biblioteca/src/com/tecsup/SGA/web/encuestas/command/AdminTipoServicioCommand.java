package com.tecsup.SGA.web.encuestas.command;

public class AdminTipoServicioCommand {

	public String operacion;
	public String codSelec;
	public String descripSelec;
	public String msg;
	public String codUsuario;
	public String dscTipoServicio;
	//public String operacion;
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSelec() {
		return codSelec;
	}
	public void setCodSelec(String codSelec) {
		this.codSelec = codSelec;
	}
	public String getDescripSelec() {
		return descripSelec;
	}
	public void setDescripSelec(String descripSelec) {
		this.descripSelec = descripSelec;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getDscTipoServicio() {
		return dscTipoServicio;
	}
	public void setDscTipoServicio(String dscTipoServicio) {
		this.dscTipoServicio = dscTipoServicio;
	}
	
}
