package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.BusquedaEmpleadosCommand;

public class BusquedaEmpleadosFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(BusquedaEmpleadosFormController.class);
	
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private TablaDetalleManager tablaDetalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");

		BusquedaEmpleadosCommand command= new BusquedaEmpleadosCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
    	List lista1= new ArrayList();
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_PERSONAL
  				, "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista1!=null)
    		command.setListaTipoPersonal(lista1);
    	//******************************************************************
    	//SE ASIGNA TIPO PERSONAL IGUAL A DOCENTE CUANDO EL PERFIL DE LOGEO ES ADMINISTRADOR
    	command.setTipoPersonal((String)request.getParameter("txhTipoPersonal"));
    	command.setTipoDocente(CommonConstants.COD_TIPO_DOCENTE);
    	if(command.getTipoPersonal()!=null){
    		if(command.getTipoPersonal().equals("1")){
    			command.setCodTipoPersonal(command.getTipoDocente());
    		}
    	}
    	if(request.getParameter("txhCodUsuario")!=null)
        	command.setFlag("OK");
    	//******************************************************************
    	log.info("formBackingObject:FIN");
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	BusquedaEmpleadosCommand control= (BusquedaEmpleadosCommand) command;
    	String resultado="";
    	
    	if(control.getOperacion().equals("BUSCAR"))
    		BuscarBandeja(control, request);
    	else{ if(control.getOperacion().equals("ACEPTAR"))
    		    resultado=Guardar(control); 
    	}
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/configuracionGeneracion/enc_busqueda_empleados","control",control);		
    }
    
    public void BuscarBandeja(BusquedaEmpleadosCommand control, HttpServletRequest request){
    	List lista= new ArrayList();
    	if(control.getCodTipoPersonal().equals("-1"))
    		control.setCodTipoPersonal("");
    	lista=this.configuracionGeneracionManager.GetAllDatosPersonal(control.getNombre(),"", control.getApellidoPaterno(), 
    			control.getApellidoMaterno(), control.getCodTipoPersonal());
    	if(lista!=null)
    		request.getSession().setAttribute("listaBandejadatosPersonal", lista);
    	else request.getSession().setAttribute("listaBandejadatosPersonal", new ArrayList());
    }
    
    public String Guardar(BusquedaEmpleadosCommand control){
    	String resultado="";
    	
    	return resultado;
    }

	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
    
}
