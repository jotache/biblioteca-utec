package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.web.encuestas.command.RegistrarPerfilesCommand;



public class RegistrarPerfilesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RegistrarPerfilesFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	RegistrarPerfilesCommand command = new RegistrarPerfilesCommand();

    	command.setTipoEncuesta(request.getParameter("txhTipoEncuesta") == null ? "" : request.getParameter("txhTipoEncuesta"));
    	System.out.println("tipoEncuesta="+command.getTipoEncuesta()+"<<");
    	
    	CargarData(command);    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	RegistrarPerfilesCommand control = (RegistrarPerfilesCommand) command;		
    	String resultado = "";
		
    	if (control.getOperacion().trim().equals("PROGRAMA")){
    		CargarPaquete(control);
    	}
    	else
    	if (control.getOperacion().trim().equals("PAQUETE")){
    		CargarPaquete(control);
    		CargarCiclo(control);
    	}
    	else
    	if (control.getOperacion().trim().equals("DEPARTAMENTO")){
    		CargarPaquete(control);
    		CargarCiclo(control);
    		CargarCursos(control);
    	}
    		
    	CargarData(control);
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_registrar_perfiles","control",control);		
    }
    private void CargarData(RegistrarPerfilesCommand control){
	   CargarCombos(control);
	   CargarBandejas(control);
    }    
    private void CargarCombos(RegistrarPerfilesCommand control){
    	//carga lista del combo programa / especializacion
    	control.setListaPrograma(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9000", ""));
    	//carga lista de la bandeja de departamento 
    	control.setListaDepartamento(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9002", ""));
    	control.setTamListaDepartamento(""+control.getListaDepartamento().size());
    	//carga lista del combo programa egresado
    	control.setListaProgramaEgresado(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9005", ""));
    	//carga lista de personal disponibles
    	control.setListaPersonalDisponibles(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9006", ""));
    	//carga lista de docentes disponibles
    	control.setListaDocentesDisponibles(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9007", ""));
    }
    private void CargarPaquete(RegistrarPerfilesCommand control){    	
    	//carga lista del combo programa especializacion
    	System.out.println("codPrograma>>"+control.getCodPrograma()+"<<");
    	control.setListaPaquete(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9001", control.getCodPrograma()));    	
    }
    private void CargarCiclo(RegistrarPerfilesCommand control){    	
    	//carga lista del combo programa especializacion
    	System.out.println("codPaquete>>"+control.getCodPaquete()+"<<");
    	control.setListaCiclo(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9003", control.getCodPaquete()));
    	control.setTamListaCiclo(""+control.getListaCiclo().size());
    }
    private void CargarCursos(RegistrarPerfilesCommand control){
    	System.out.println("codDepartamento>>"+control.getCodDepartamento()+"<<");
    	//carga lista de cursos por departamento
    	control.setListaCursoDepartamento(this.configuracionGeneracionManager.GetAllConfiguracionPerfil("9004", control.getCodDepartamento()));
    }
    private void CargarBandejas(RegistrarPerfilesCommand control){
    	
    }
		
}
