package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionSeguimientoBandejaCommand;
import com.tecsup.SGA.web.encuestas.command.ConfGenBandejaEncuestasCommand;

public class AplicacionSeguimientoBandejaFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(AplicacionSeguimientoBandejaFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}         

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}  

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		AplicacionSeguimientoBandejaCommand command= new AplicacionSeguimientoBandejaCommand();
		//******************************************************************
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){
    		
    		System.out.println("usuarioSeguridad codUsuario--->>"+usuarioSeguridad.getCodUsuario()+"<<");
    		System.out.println("usuarioSeguridad nomUsuario--->>"+usuarioSeguridad.getNomUsuario()+"<<");
    		System.out.println("usuarioSeguridad getPerfiles--->>"+usuarioSeguridad.getPerfiles()+"<<");
    		command.setNomUsuario(usuarioSeguridad.getNomUsuario());
    		command.setPerfilUsuario(usuarioSeguridad.getPerfiles());
    	}
		//*******************************************************************
		command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" :
		    request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
    	if( command.getCodUsuario()!=null)
    	{   System.out.println("Dentro del CodUsuario");
    		command.setNroEncuesta(request.getParameter("txhParCodEncuesta") == null ? "" :
		    request.getParameter("txhParCodEncuesta"));
    		command.setCodTipoAplicacion(request.getParameter("txhParCodTipoAplicacion") == null ? "" :
			    	    request.getParameter("txhParCodTipoAplicacion"));
    		command.setCodTipoEncuesta(request.getParameter("txhParCodTipoEncuesta") == null ? "" :
			    	    request.getParameter("txhParCodTipoEncuesta"));
    		command.setCodTipoServicio(request.getParameter("txhParCodTipoServicio") == null ? "" :
					    request.getParameter("txhParCodTipoServicio"));
    		command.setCodTipoEstado(request.getParameter("txhParCodTipoEstado") == null ? "" :
			    		request.getParameter("txhParCodTipoEstado"));
    		command.setParBandera(request.getParameter("txhParBandera") == null ? "" :
					    request.getParameter("txhParBandera"));
    		command.setResponsable(request.getParameter("txhParResponsable") == null ? "" :
					    request.getParameter("txhParResponsable"));
    		command.setCodResponsable(request.getParameter("txhParCodResponsable") == null ? "" : 
			    request.getParameter("txhParCodResponsable"));
    		
    		System.out.println("ParCodTipoAplicacion: "+command.getCodTipoAplicacion()+
    				">>ParCodTipoEncuesta: "+command.getCodTipoEncuesta()+
    				">>ParCodEncuesta: "+command.getNroEncuesta()+
    				">>ParCodTipoServicio: "+command.getCodTipoServicio()+
    				">>ParResponsable: "+command.getResponsable()+
    				">>ParBandera: "+command.getParBandera()+
    				">>ParCodResponsable: "+command.getCodResponsable());
    		
    		if(command.getParBandera().equals("1"))
    		   request.setAttribute("iFrameBandeja", "OK");
    	}
    	
    	command.setConsteAplicacionPrograma(CommonConstants.COD_TIPO_APLICACION_PROGRAMAS);
    	command.setConsteAplicacionServicio(CommonConstants.COD_TIPO_APLICACION_SERVICIO_OTROS);
    	
    	List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();
    	List lista4= new ArrayList();
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_APLICACION
  				, "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista1!=null)
    		command.setListaTipoAplicacion(lista1);
    	
    	lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ENCUESTA
  				, "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista2!=null)
    		command.setListaTipoEncuesta(lista2);
    	
    	lista3=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ENCUESTA_SERVICIO
  				, "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista3!=null)
    		command.setListaTipoServicio(lista3);
    	
    	lista4 = this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ESTADO_ENCUESTA, "", "", "", "", "",
				"", "", CommonConstants.TIPO_ORDEN_COD);
    	
    	TipoTablaDetalle obj=new TipoTablaDetalle();
    	if (lista4 != null){			
			for(int i=0;i<lista4.size();i++){
				obj=(TipoTablaDetalle)lista4.get(i);				
				if(obj.getCodTipoTablaDetalle().equals(CommonConstants.COD_ENCUESTA_GENERADA)){					
					lista4.remove(i);
				}
			}
			for(int i=0;i<lista4.size();i++){
				obj=(TipoTablaDetalle)lista4.get(i);				
				if(obj.getCodTipoTablaDetalle().equals(CommonConstants.COD_ENCUESTA_PENDIENTE)){					
					lista4.remove(i);
				}
			}
			command.setListaTipoEstado(lista4);			
		}
    	if( command.getCodUsuario()!=null){
    		command.setCodTipoEstado(CommonConstants.COD_ENCUESTA_EN_APLICACION);
    	}
    	command.setConsCodEncEnAplicacion(CommonConstants.COD_ENCUESTA_EN_APLICACION);
    	log.info("formBackingObject:FIN");
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AplicacionSeguimientoBandejaCommand control=(AplicacionSeguimientoBandejaCommand) command;
    	
    	if(control.getOperacion().equals("BUSCAR")){
    		BuscarEncuestas(control);
    	}
    	else
    	if(control.getOperacion().equalsIgnoreCase("TERMINAR")){
    		String resp = terminarEncuesta(control);
    		if(resp==null || resp.equalsIgnoreCase("")){
    			request.setAttribute("mensaje",CommonMessage.ERROR_TRANSACCION);
    		}else if( Integer.parseInt(resp)>=0 ){
				request.setAttribute("mensaje",CommonMessage.EXITO_TRANSACCION);				
			}else if("-1".equalsIgnoreCase(resp)){
				request.setAttribute("mensaje",CommonMessage.ERROR_TRANSACCION);				
			}else if("-2".equalsIgnoreCase(resp)){
				request.setAttribute("mensaje",CommonMessage.ERROR_ESTADO);				
			}else if("-3".equalsIgnoreCase(resp)) {
				request.setAttribute("mensaje",CommonMessage.ERROR_TERMINANDO);				
			}else {
				request.setAttribute("mensaje",CommonMessage.ERROR_TRANSACCION);
			}
    		request.setAttribute("iFrameBandeja","OK");
    		BuscarEncuestas(control);
    	}
    	
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/aplicacionSeguimiento/enc_bandeja_aplicacion_seguimiento","control",control);		
    }
    
    private String terminarEncuesta(AplicacionSeguimientoBandejaCommand control){
    	
    	log.info("terminarEncuesta");
    	log.info("control.getCodEncuesta()>>"+control.getCodEncuesta()+">>");
    	log.info("control.getCodUsuario()>>"+control.getCodUsuario()+">>");
    	String resp = configuracionPerfilManager.updateTerminarEncuesta(control.getCodEncuesta(),control.getCodUsuario());
    	log.info("resp>"+resp+">");
    	
		return resp;
		
	}

	private void BuscarEncuestas(AplicacionSeguimientoBandejaCommand control) {    	
		List lista = new ArrayList();
		
		System.out.println(">>BuscarEncuestas<<");
		System.out.println("NroEncuesta>>"+control.getNroEncuesta()+"<<");
		System.out.println("CodResponsable>>"+control.getCodResponsable()+"<<");
		System.out.println("CodTipoAplicacion>>"+control.getCodTipoAplicacion()+"<<");
		System.out.println("CodTipoEncuesta>>"+control.getCodTipoEncuesta()+"<<");
		System.out.println("CodTipoServicio>>"+control.getCodTipoServicio()+"<<");				
		
		lista = this.configuracionGeneracionManager.getAllFormato(control.getCodTipoAplicacion(),
				control.getCodTipoServicio(), control.getCodTipoEncuesta(), "0003,0004", 
				"", control.getCodResponsable(), control.getNroEncuesta());
		
		if (lista != null){
			control.setListaBandeja(lista);
			control.setTamListaBandeja(""+lista.size());
		}	
		else{
			control.setListaBandeja(null);
			control.setTamListaBandeja("0");
		}
	}
}
