package com.tecsup.SGA.web.encuestas.controller;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.EncuestaManualBean;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;

public class AplicacionSeguimientoRegistroManualGrillaFormController  implements Controller {

	 
	private static Log log = LogFactory.getLog(AplicacionSeguimientoRegistroManualGrillaFormController.class);
    
	ConfiguracionGeneracionManager configuracionGeneracionManager;

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	log.info("handleRequest:INI");
  
    	String pagina = "/encuestas/aplicacionSeguimiento/enc_registro_manual_grilla";
    	ModelMap model = new ModelMap();   	
    	
    	log.info("txhCodigo>>"+request.getParameter("txhCodigo")+">");
    	log.info("txhOperacion>>"+request.getParameter("txhOperacion")+">");
    	
    	//List lstResultado = configuracionGeneracionManager.getAllEncuestaManual(request.getParameter("txhCodigo"));
    	if("ELIMINAR".equalsIgnoreCase(request.getParameter("txhOperacion"))){
    		
    		log.info("codEncuesta>>"+request.getParameter("txhCodigo")+">>");
    		log.info("0000"+"0000"+">>");
    		log.info("correlativo>>"+request.getParameter("txhCodigoCorrelativo")+">>");
    		log.info("usuario>>"+request.getParameter("txhUsuario")+">>");
    		
    		String resp = configuracionGeneracionManager.deleteEncuestaManual(
    				request.getParameter("txhCodigo"),
    				"0000",
    				request.getParameter("txhCodigoCorrelativo"),
    				request.getParameter("txhUsuario")
    				);
    		
			log.info("resp>>"+resp+">>");
			
			if( "0".equalsIgnoreCase(resp) ){
				request.setAttribute("mensaje",CommonMessage.ELIMINAR_EXITO);    		
			}else if( "-1".equalsIgnoreCase(resp) ) {
				request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);
			}else{
				StringTokenizer tkn = new StringTokenizer(resp,"|");
				String msg1 = ""; String msg2 = "";
				msg1 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
				msg2 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
				log.info("msg1>"+msg1+">");log.info("msg2>"+msg2+">");
				request.setAttribute("mensaje",msg2);
			}		
    		
    	}
    	
    	List lstResultado  = configuracionGeneracionManager.getPreparaGrillaRespuestaxEncuestado(request.getParameter("txhCodigo"));
    	
    	log.info(">>"+(lstResultado==null?"0":lstResultado.size())+">>");
    	String tamanioPreguntas = "0";
    	
    	if(lstResultado!=null && lstResultado.get(0)!=null ){
    		EncuestaManualBean bean = (EncuestaManualBean)lstResultado.get(0);
    		List lstPreguntas = bean.getLstPreguntas();
    		if(lstPreguntas!=null)
    			tamanioPreguntas = ""+lstPreguntas.size();
    	}
    	
    	
    	request.setAttribute("txhTamanio",tamanioPreguntas);
    	model.addObject("txhTamanio",tamanioPreguntas);
    	model.addObject("lstResultado",lstResultado);
    	model.addObject("txhCodigo",request.getParameter("txhCodigo"));
    	model.addObject("txhUsuario",request.getParameter("txhUsuario"));
    	
    	
    	log.info("handleRequest:FIN");
		return new ModelAndView(pagina, "model", model);
	}

}
