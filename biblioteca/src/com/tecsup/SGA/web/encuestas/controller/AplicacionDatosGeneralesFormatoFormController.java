package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesFormatoCommand;





public class AplicacionDatosGeneralesFormatoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicacionDatosGeneralesFormatoFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public AplicacionDatosGeneralesFormatoFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesFormatoCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesFormatoCommand control = new AplicacionDatosGeneralesFormatoCommand();    	
    	
    	if(request.getAttribute("prmCodEncuesta")!=null )
    	{
    		cargaDatosEncuesta(control);
    	}else{
    		//ini(control);
    	}	
        log.info("formBackingObject:FIN");
        return control;
    }
	
    private void cargaDatosEncuesta(AplicacionDatosGeneralesFormatoCommand control) {
		
		
	}


	

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	AplicacionDatosGeneralesFormatoCommand control = (AplicacionDatosGeneralesFormatoCommand) command;		
		log.info("operacion|"+control.getOperacion()+"|");
		
		if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
			
		}
		
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_datos_generales_formato","control",control);		
    }
		
}
