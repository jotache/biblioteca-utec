package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.web.encuestas.command.AdminTipoFormatoCommand;
import com.tecsup.SGA.web.encuestas.command.ConGenTipoFormatoCommand;

public class ConGenTipoFormatoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ConGenTipoFormatoFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		ConGenTipoFormatoCommand command= new ConGenTipoFormatoCommand();
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		command.setCodFormatoEncuesta((String)request.getParameter("txhCodFormatoEncuesta"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
    	
    	BuscarBandeja(command, request);
    	
    	log.info("formBackingObject:FIN");
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	ConGenTipoFormatoCommand control= (ConGenTipoFormatoCommand) command;
    	String resultado="";
    	
    	if(control.getOperacion().equals("BUSCAR"))
    	{
    		BuscarBandeja(control, request);
    	}
    	else{ if(control.getOperacion().equals("ACEPTAR"))
    			{   resultado=GuardarDatos(control);
    			    if(resultado.equals("0"))
    			    {	control.setMsg("OK");
    			    	BuscarBandeja(control, request);
    			    }
    			    else{ if(resultado.equals("-2"))
    			    		{ control.setMsg("ERROR_MODIFICAR");
    			    		  BuscarBandeja(control, request);
    			    		}
    			    	 else{
    			    		  control.setMsg("ERROR");	
    			    		  BuscarBandeja(control, request);
    			    	 }
    			    	} 
    			}
    	}
    	
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/configuracionGeneracion/enc_datos_generales_tipo_formato","control",control);		
    }
    
    public void BuscarBandeja(ConGenTipoFormatoCommand control, HttpServletRequest request)
    {   System.out.println("BuscarBandeja");
    	List lista= new ArrayList();
        lista=this.configuracionGeneracionManager.GetAllEncuestaByFormato( control.getCodFormatoEncuesta());
        String cadena="";
        if(lista!=null)
        {	System.out.println("Size: "+lista.size());
         if(lista.size()>0)
          { FormatoEncuesta formatoEncuesta= new FormatoEncuesta();
        	for(int k=0; k<lista.size();k++)
        	  { formatoEncuesta=(FormatoEncuesta)lista.get(k);
        	    if(!formatoEncuesta.getCodSeccionAsignado().equals(""))
        	     { if(cadena.equals(""))
        	    	 cadena=formatoEncuesta.getCodFormato()+"|";
        	       else cadena=cadena+formatoEncuesta.getCodFormato()+"|";
        	     }
        	  }
           control.setCadCodDescripSelec(cadena);
          } 
        }
        System.out.println("Cadena Final: "+cadena);
    	request.getSession().setAttribute("listaBandejaFormato", lista);
    }
    
    public String GuardarDatos(ConGenTipoFormatoCommand control)
    {  String resultado="";
       int tam=control.getCadCodSelec().trim().length();
       if(tam>0)
       control.setCadCodSelec(control.getCadCodSelec().substring(0, tam-1));
       else control.setCadCodSelec("|");
       System.out.println(">>CodFormatoEncuesta<<"+control.getCodFormatoEncuesta()+">>CadCodSelec<<"+ 
     		  control.getCadCodSelec()+">>NroTotalRegistros<<"+control.getNroTotalRegistros()+">>CodUsuario<<"+
     		  control.getCodUsuario());
       resultado=this.configuracionGeneracionManager.UpdateEncuestaByFormato(control.getCodFormatoEncuesta(), 
    		  control.getCadCodSelec(), control.getNroTotalRegistros(), control.getCodUsuario());
       System.out.println("Resultado: "+resultado);
       return resultado;
    }
}
