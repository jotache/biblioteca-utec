package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;

import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilProgramacionCommand;



public class AplicacionDatosGeneralesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicacionDatosGeneralesFormController.class);
	
	
	private BiblioMantenimientoManager biblioMantenimientoManager;
	private TablaDetalleManager tablaDetalleManager;
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;

	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public void setBiblioMantenimientoManager(
			BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}


	public AplicacionDatosGeneralesFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesCommand control = new AplicacionDatosGeneralesCommand();    	
    	
    	control = ini(control);
    	
    	control.setUsuario(request.getParameter("prmUsuario"));
    	control.setCboSede(CommonMessage.SEDE_LIMA_COD);
    	
    	if(control.getOperacion()==null)
    		control.setCboSede(CommonMessage.SEDE_LIMA_COD);
    	
    	if(request.getParameter("prmCodEncuesta")!=null )
    	{	//CARGAR LOS DATOS DE LA ENCUESTA
    		control.setTxhCodigo(request.getParameter("prmCodEncuesta"));    		
    		cargaDatosEncuesta(control);    		
    		CargarDatosBandeja(control, request);
    		
    	}else{
    		//NUEVA ENCUESTA
    		if(request.getParameter("prmUsuario")!=null)
    		   CargarDatosBandeja(control, request);
    		
    		control.setTxhNumPerfil("0");
    	}
    	
    	log.info("NumPerfil>>"+control.getTxhNumPerfil()+">>");
        log.info("formBackingObject:FIN");
        return control;
    }
	

	private String numeroDePerfiles(String txhCodigo) {
		List lst = configuracionPerfilManager.GetAllPerfilesByEncuesta(txhCodigo);		
		if(lst!=null )
			return ""+lst.size();
		else
			return "0";		
	}
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	String resp = "";
    	
    	AplicacionDatosGeneralesCommand control = (AplicacionDatosGeneralesCommand) command;		
		log.info("operacion|"+control.getOperacion()+"|");
		
		if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
			
			if(control.getTxhCodigo()!=null && !control.getTxhCodigo().trim().equalsIgnoreCase("") )
			{	log.info("irActualizar");
				resp = irActualizar(control);				
			
				log.info("RPUESTA:"+resp+":");
				if( "0".equalsIgnoreCase(resp) ){
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
				}else if( "-1".equalsIgnoreCase(resp) ){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}else if( "-2".equalsIgnoreCase(resp) ){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_COD_FORMATO_YA_EXISTE);
				}else if( "-3".equalsIgnoreCase(resp) ){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);
				}else{
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}
				
				cargaDatosEncuesta(control);
			}
			else 
			{	log.info("irRegistrar");
				resp = irRegistrar(control);    		
				log.info("RPUESTA:"+resp+":");
				
				if(Integer.parseInt(resp)>0){
					control.setTxhCodigo(resp);					
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
					cargaDatosEncuesta(control);
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
					control.setTxhCodigo(null);
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_COD_FORMATO_YA_EXISTE);
					control.setTxhCodigo(null);
				}else {
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
					control.setTxhCodigo(null);
				}
				
    		
			}
			
				
		}else if("irGenerar".equalsIgnoreCase(control.getOperacion())){
			
			resp = irGenerar(control);
    		
			log.info("RPUESTA:"+resp+":");
			 
			if("0".equalsIgnoreCase(resp)){				
				request.setAttribute("mensajeGenerar","TRUE");
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);    		
			}else if("-1".equalsIgnoreCase(resp)){
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			}else{
				StringTokenizer token = new StringTokenizer(resp,"|");
				String msg1 = "";
				String msg2 = "";
				
				if(token.hasMoreTokens())
					msg1 = token.nextToken();
				if(token.hasMoreTokens())
					msg2 = token.nextToken();
				
				log.info("msg1:>"+msg1+">");
				log.info("ms21:>"+msg2+">");
				
				request.setAttribute("mensaje",msg2);
				
			}
			cargaDatosEncuesta(control);
		}
		
		log.info("onSubmit:FIN");
		
		control.setOperacion("");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_datos_generales","control",control);		
    }

		private String irGenerar(AplicacionDatosGeneralesCommand control) {
			String str = "";
			
			log.info("irGenerar");
			log.info("1.>"+control.getTxhCodigo()+">");
			log.info("2.>"+control.getUsuario()+">");
			str = configuracionGeneracionManager.updateGeneraEncuesta(control.getTxhCodigo(), control.getUsuario());
			return str;
		}

		private String irActualizar(AplicacionDatosGeneralesCommand control) {
			String str = "";
			/*log.info("1.>"+control.getTxhCodigo()+">");
			log.info("2.>"+control.getCboTipoAplicacion()+">");
			log.info("3.>"+control.getCboTipoServicio()+">");
			log.info("4.>"+control.getCboTipoEncuesta()+">");
			log.info("5.>"+control.getTxtCodigo()+">");
			log.info("6.>"+control.getTxtNombre()+">");
			log.info("7.>"+control.getTxtDuracion()+">");			
			log.info("8.>"+control.getTxaObservacion()+">");
			log.info("9.>"+control.getCboSede()+">");
			log.info("10.>"+control.getUsuario()+">");*/
			
			str = configuracionGeneracionManager.updateFormato(
					control.getTxhCodigo(),
					control.getCboTipoAplicacion(),
					control.getCboTipoServicio(),
					control.getCboTipoEncuesta(),
					control.getTxtCodigo(),
					control.getTxtNombre(),
					control.getTxtDuracion(),
					control.getTxaObservacion(),
					control.getCboSede(),
					control.getUsuario()
					);
			return str;
		}

		private String irRegistrar(AplicacionDatosGeneralesCommand control) {
			String str = "";
			
			/*log.info("1>"+control.getCboTipoAplicacion()+">");
			log.info("2>"+control.getCboTipoServicio()+">");
			log.info("3>"+control.getCboTipoEncuesta()+">");
			log.info("4>"+control.getTxtCodigo()+">");
			log.info("5>"+control.getTxtNombre()+">");
			log.info("6>"+control.getTxtDuracion()+">");
			log.info("7>"+control.getTxaObservacion()+">");
			log.info("8>"+control.getCboSede()+">");
			log.info("9>"+control.getUsuario()+">");*/
			
			str = configuracionGeneracionManager.insertFormato(
					control.getCboTipoAplicacion(),
					control.getCboTipoServicio(),
					control.getCboTipoEncuesta(),
					control.getTxtCodigo(),//numero
					control.getTxtNombre(),
					control.getTxtDuracion(),
					control.getTxaObservacion(),
					control.getCboSede(),
					control.getUsuario()
					);
			
			return str;
		}

		private void cargaCombos(AplicacionDatosGeneralesCommand control) {
			//control.setLstSede(null);
			
		      List lstTipoEncuesta=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ENCUESTA
		      , "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
		      
		      /*List lstTipoServicio=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO
		    	      , "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);*/
		      
		      List lstTipoAplicacion=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_APLICACION
		    	      , "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
		      
		    control.setLstTipoAplicacion(lstTipoAplicacion);
			control.setLstTipoEncuesta(lstTipoEncuesta);
			
			
			List lista3 = this.tablaDetalleManager.getAllTablaDetalle(
					CommonConstants.TIPT_TIPO_ENCUESTA_SERVICIO, "", "", "", "",
					"", "", "", CommonConstants.TIPO_ORDEN_COD);
			if (lista3 != null)				
			control.setLstTipoServicio(lista3);
		}

	    private void cargaDatosEncuesta(AplicacionDatosGeneralesCommand control) {	    	
	    	control = configuracionGeneracionManager.getFormato(control);
	    	control.setTxhNumPerfil(numeroDePerfiles(control.getTxhCodigo()));
		}

		private AplicacionDatosGeneralesCommand ini(AplicacionDatosGeneralesCommand control) {
			control = iniciaSede(control);		
			cargaCombos(control);
			return control;
		}

		private AplicacionDatosGeneralesCommand iniciaSede(
				AplicacionDatosGeneralesCommand control) {
			TipoTablaDetalle obj = new TipoTablaDetalle();
	    	
	    	obj.setDscValor3("");
			obj.setCodTipoTabla("");
			obj.setCodTipoTablaDetalle(CommonConstants.TIPT_SEDE);
			obj.setCodDetalle("");
			obj.setDescripcion("");
			obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
			control.setLstSede(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
			
			return control;
		}

		private void CargarDatosBandeja(AplicacionDatosGeneralesCommand command, HttpServletRequest request){
	    	
	    	command.setParCodTipoAplicacion(request.getParameter("txhParCodTipoAplicacion") == null ? "" : 
	            request.getParameter("txhParCodTipoAplicacion"));
	    	command.setParCodTipoEncuesta(request.getParameter("txhParCodTipoEncuesta") == null ? "" : 
				request.getParameter("txhParCodTipoEncuesta"));
	    	command.setParCodTipoServicio(request.getParameter("txhParCodTipoServicio") == null ? "" : 
				request.getParameter("txhParCodTipoServicio"));
	    	command.setParCodTipoEstado(request.getParameter("txhParCodTipoEstado") == null ? "" : 
				request.getParameter("txhParCodTipoEstado"));
	    	command.setParBandera(request.getParameter("txhParBandera") == null ? "" : 
				request.getParameter("txhParBandera"));
	    	System.out.println("ParCodTipoAplicacion: "+command.getParCodTipoAplicacion()+
	    			" ParCodTipoEncuesta: "+command.getParCodTipoEncuesta()+
	    			" ParCodTipoServicio: "+command.getParCodTipoServicio()+
	    			" ParCodTipoEstado: "+command.getParCodTipoEstado()+
	    			" ParBandera: "+command.getParBandera());
	    }
		
		
}
