package com.tecsup.SGA.web.encuestas.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.bean.ValidaReporteBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.service.encuestas.ConsultasReportesEncuestasManager;

public class ReporteEncuestasController implements Controller{
	private static Log log = LogFactory.getLog(ReporteEncuestasController.class);
	
	private ConsultasReportesEncuestasManager consultasReportesEncuestasManager;
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;

	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	public void setConsultasReportesEncuestasManager(
			ConsultasReportesEncuestasManager consultasReportesEncuestasManager) {
		this.consultasReportesEncuestasManager = consultasReportesEncuestasManager;
	}

	public ReporteEncuestasController(){
		
	}
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception {
    	
    	log.info("onSubmit:INICIO - ReporteEncuestasController() ");
    	
    	ModelMap model = new ModelMap();
    	
    	String codTipoReporte=request.getParameter("txhCodTipoReporte");
    	String codEncuesta=request.getParameter("txhCodEncuesta");
    	String codTipoResultado=request.getParameter("txhCodTipoResultado");
    	String codProfesor=request.getParameter("txhCodResponsable");
    	String codUsuario=request.getParameter("txhCodUsuario");
    	
    	
    	log.info("codTipoReporte " +codTipoReporte);
    	log.info("codEncuesta " +codEncuesta);
    	log.info("codTipoResultado " +codTipoResultado);
    	log.info("codProfesor " +codProfesor);
    	log.info("codUsuario " +codUsuario);
    	
    	
    	String pagina="";
    	String codPerfil="";
    	String codTipoAplicacion="";
    	String codTipoEncuesta="";
    	List listaPregCer=null;
    	List listaPregAbi=null;
    	List listaLeyenda=null;
    	
		String fechaRep = Fecha.getFechaActual();
		String horaRep = Fecha.getHoraActual();
		model.addObject("FECHA_ACTUAL", fechaRep);
		model.addObject("HORA_ACTUAL", horaRep);
		
		log.info("fechaRep: " + fechaRep);
		log.info("horaRep: " + horaRep);
		
    	
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
		/*PRIMERA INSTANCIA LLAMANDO A LA VALIDACION*/
		//String  codEnc = valida(codTipoReporte,codEncuesta,request,response,model);
		String codEnvio = "";
		
		//SI ES PERFIL ADM ENCUESTAS
		if(CommonConstants.PERFIL_ADM_ENCUESTA.equalsIgnoreCase(usuarioSeguridad.getPerfiles()))
		codEnvio = 	codUsuario;
		
		ValidaReporteBean bean = valida(codTipoReporte,codEncuesta,codEnvio,request,response);
		//if( !"-1".equalsIgnoreCase(codEnc) ){
		
		log.info("Valor obtenido BEAN() " + bean.getCodResultado()+ " >>"  +bean.getDesResultado());
		
		String codEnc  = bean.getIdEncuesta();
		log.info("COD ENCUESTA SACADA DEL SP VALIDA>>"+codEnc+">>");				
		
		/*List lista = this.configuracionGeneracionManager.getAllFormato("", 
				"", "", "", codEnc, "", "");*/
		
		log.info("OBTENIENDO DATOS DE LA CABECERA..");
		log.info("1>> bean.getIdEncuesta(): "+bean.getIdEncuesta()+">>");
		log.info("2>> codProfesor: "+codProfesor+">>");
		log.info("3>> bean.getCodProducto(): "+bean.getCodProducto()+">>");
		log.info("4>> bean.getCodPrograma(): "+bean.getCodPrograma()+">>");
		
		/*DatosCabeceraBean obj = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(
				bean.getIdEncuesta(),
				"",
				bean.getCodProducto(),
				bean.getCodPrograma()
				);*/
		
		DatosCabeceraBean obj = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(
				bean.getIdEncuesta(),
				codProfesor);
		//CAMBIO DE SP
		
		pagina="/biblioteca/userAdmin/consultaReportes/bib_close";
		
		log.info("Nombre Nro. Encuesta : "+ obj.getNroNomEncuesta() + " -  Total Encuestados= " + obj.getTotEncuestados());
		
		if(obj!=null){
			
			///FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);
 		
			/*model.addObject("COD_SEDE", obj.getCodSede());
			model.addObject("NOM_SEDE", obj.getNomSede());
			model.addObject("COD_ENCUESTA", obj.getNroEncuesta());
			model.addObject("NOM_ENCUESTA", obj.getNombreEncuesta());		
			model.addObject("COD_PROFESOR", obj.getCodResponsable());
			model.addObject("NOM_PROFESOR", obj.getNomResponsable());		
			model.addObject("NOM_PROGRAMA", "");
			model.addObject("COD_PROGRAMA", "");
			model.addObject("COD_TIPOAPLICACION", obj.getCodTipoAplicacion());
			model.addObject("NOM_TIPOAPLICACION", obj.getNomTipoAplicacion());
			model.addObject("NOM_PERFIL", obj.getDscEncuestados());
			model.addObject("NOM_PRODUCTO", "");
			model.addObject("NOM_ANIOS_EGRESO", "");
			model.addObject("NOM_TIPOSERVICIO", obj.getNomTipoServicio());		    		
			codTipoAplicacion=obj.getCodTipoAplicacion();
			codTipoEncuesta=obj.getCodTipoEncuesta();*/
		
			model.addObject("COD_SEDE", obj.getCodSede());
			model.addObject("NOM_SEDE", obj.getNomSede());
			model.addObject("COD_ENCUESTA", obj.getNroNomEncuesta());
			model.addObject("NOM_ENCUESTA", obj.getNomEncuesta());		
			 //model.addObject("COD_PROFESOR", obj.getcodp.GET.getCodResponsable());
			model.addObject("NOM_PROFESOR", obj.getNomProfesor());		
			model.addObject("NOM_PROGRAMA", obj.getNomPrograma());//""
			model.addObject("COD_PROGRAMA", obj.getCodPrograma());//""
			model.addObject("COD_TIPOAPLICACION", obj.getCodTipoAplicacion());//obj.getCodTipoAplicacion()
			model.addObject("NOM_TIPOAPLICACION", obj.getNomTipoAplicacion());//obj.getNomTipoAplicacion()
			model.addObject("NOM_PERFIL", obj.getDetEncuestados());//obj.getDscEncuestados()
			model.addObject("NOM_PRODUCTO", obj.getNomProducto());//obj.getNomProducto()
			model.addObject("NOM_ANIOS_EGRESO", obj.getAnioIniEgresado()+" - "+obj.getAnioFinEgresado());//""
			model.addObject("NOM_TIPOSERVICIO", obj.getNomServicio());//obj.getNomTipoServicio()		    		
			codTipoAplicacion=obj.getCodTipoAplicacion();
			codTipoEncuesta=obj.getCodTipoEncuesta();
			
			
		//}
		//OBTENEMOS EL CODIGO DE PERFIL DE LA ENCUESTA
		codPerfil=getPerfilEncuesta(codEnc);
		
		System.out.println("codPerfil>>"+codPerfil+"<<");
		//REPORTE CONSULTA DE EVANCE DE ENCUESTAS A PROFESORES
		if(codTipoReporte.equals("0001")){
			//Solo aplica para encuestas con tipo de aplicación: PROGRAMA y tipo encuesta: ESTANDAR 
			if(codTipoAplicacion.equals(CommonConstants.ENCUESTA_PROGRAMA) && codTipoEncuesta.equals(CommonConstants.TIPO_ENCUESTA_ESTANDAR)){
				//CONSOLIDADO
				if(codTipoResultado.equals("0")){							
					if(codPerfil.equals(CommonConstants.PERFIL_PFR)){
						
						List lst = consultasReportesEncuestasManager.getAllProfConsolidadaPFR(codEnc, codProfesor);
						
						
						if(lst!=null && lst.size()>0){
							
							request.setAttribute("LST_RESULTADO",lst);
							
							if(request.getParameter("txhPagina")!=null)
								pagina = "/encuestas/consultaReportes/enc_rep_prof_consolidada_pfr_page";
							else	
								pagina = "/encuestas/consultaReportes/enc_rep_prof_consolidada_pfr";
						}else{
							request.setAttribute("mensaje",CommonMessage.NO_HAY_RESPUESTA);
							request.setAttribute("NotClose","OK");
							pagina = "/encuestas/consultaReportes/mensajes";
						}
						
					}
					else
					if(codPerfil.equals(CommonConstants.PERFIL_PCC)){
						
						List lst = consultasReportesEncuestasManager.getAllProfConsolidadaPCC(codEnc, codProfesor);
						
						
						
						if(lst!=null && lst.size()>0){
							request.setAttribute("LST_RESULTADO",lst);
							
							if(request.getParameter("txhPagina")!=null)
								pagina = "/encuestas/consultaReportes/enc_rep_prof_consolidada_pcc_page";
							else
								pagina = "/encuestas/consultaReportes/enc_rep_prof_consolidada_pcc";
							
						}else{
							request.setAttribute("mensaje",CommonMessage.NO_HAY_RESPUESTA);
							request.setAttribute("NotClose","OK");
							pagina = "/encuestas/consultaReportes/mensajes";
						}

					}
				}
				else
				//DETALLE
				if(codTipoResultado.equals("1")){
					if(codPerfil.equals(CommonConstants.PERFIL_PFR)){
						listaPregCer=this.consultasReportesEncuestasManager.GetAllConsultaByProfesorDetPRFCer(codEnc, codProfesor);
				    	listaPregAbi=this.consultasReportesEncuestasManager.GetAllConsultaByProfesorDetPRFAbi(codEnc, codProfesor);
				    	listaLeyenda = this.configuracionGeneracionManager.getAllVerEncuesta(codEnc);						    	
						request.setAttribute("LST_RESULTADO", listaPregCer);									
						request.setAttribute("LST_RESULTADO_2", listaPregAbi);
						request.setAttribute("LST_LEYENDA", listaLeyenda);
						if(listaPregCer!=null && listaPregAbi!=null){
							System.out.println("listaPregCer.size>>"+listaPregCer.size());
							System.out.println("listaPregAbi.size>>"+listaPregAbi.size());
							if(listaPregCer.size()>0 || listaPregAbi.size()>0){
								if(request.getParameter("txhPagina")!=null)
									pagina = "/encuestas/consultaReportes/enc_rep_avanceEncuestaDetalladoPFR_page";
								else
									pagina = "/encuestas/consultaReportes/enc_rep_avanceEncuestaDetalladoPFR";
							}
							else{
								request.setAttribute("mensaje", "No se completó ninguna respuesta para la encuesta.");
								pagina = "/encuestas/consultaReportes/mensajes";
							}	
						}
					}
					else
					if(codPerfil.equals(CommonConstants.PERFIL_PCC)){
						listaPregCer=this.consultasReportesEncuestasManager.GetAllConsultaByProfesorDetPCCCer(codEnc, codProfesor);
						listaPregAbi=this.consultasReportesEncuestasManager.GetAllConsultaByProfesorDetPCCAbi(codEnc, codProfesor);
						listaLeyenda = this.configuracionGeneracionManager.getAllVerEncuesta(codEnc);						    	
						request.setAttribute("LST_RESULTADO", listaPregCer);									
						request.setAttribute("LST_RESULTADO_2", listaPregAbi);
						request.setAttribute("LST_LEYENDA", listaLeyenda);
						if(listaPregCer!=null && listaPregAbi!=null){
							System.out.println("listaPregCer.size>>"+listaPregCer.size());
							System.out.println("listaPregAbi.size>>"+listaPregAbi.size());
							if(listaPregCer.size()>0 || listaPregAbi.size()>0){
								if(request.getParameter("txhPagina")!=null)
									pagina = "/encuestas/consultaReportes/enc_rep_avanceEncuestaDetalladoPCC_page";
								else
									pagina = "/encuestas/consultaReportes/enc_rep_avanceEncuestaDetalladoPCC";
							}
							else{
								request.setAttribute("mensaje", "No se completó ninguna respuesta para la encuesta.");
								pagina = "/encuestas/consultaReportes/mensajes";
							}
						}								
					}
				}
			}
		}
		else
		//REPORTE DE ENCUESTAS ESTANDARES
		if(codTipoReporte.equals("0002")){
			//Solo tipo encuesta: ESTANDAR 
			if(codTipoEncuesta.equals(CommonConstants.TIPO_ENCUESTA_ESTANDAR)){
				//CONSOLIDADO
				if(codTipoResultado.equals("0")){
					//codPerfil=(String)request.getParameter("txhCodPerfil");
					//solo encuestas con tipo apliacion programa
					if(codTipoAplicacion.equals(CommonConstants.ENCUESTA_PROGRAMA)){							
						if(codPerfil.equals(CommonConstants.PERFIL_PFR) || codPerfil.equals(CommonConstants.PERFIL_PCC)){
							request.setAttribute("LST_RESULTADO",consultasReportesEncuestasManager.getAllEstandarConsAlumno(codEnc));
							pagina = "/encuestas/consultaReportes/enc_rep_estandar_consolidada_alumno";			
						}
						else
						if(codPerfil.equals(CommonConstants.PERFIL_EGRESADO)){
							request.setAttribute("LST_RESULTADO",consultasReportesEncuestasManager.getAllEstandarConsEgresado(codEnc));
							pagina = "/encuestas/consultaReportes/enc_rep_estandar_consolidada_egresado";			
						}
						else
						if(codPerfil.equals(CommonConstants.PERFIL_PERSONAL)){
							request.setAttribute("LST_RESULTADO",consultasReportesEncuestasManager.getAllEstandarConsPersonal(codEnc));
							pagina = "/encuestas/consultaReportes/enc_rep_estandar_consolidada_personal";			
						}
					}
					else
					//solo encuetstas servicios
					if(codTipoAplicacion.equals(CommonConstants.ENCUESTA_SECCION)){
						request.setAttribute("LST_RESULTADO",consultasReportesEncuestasManager.getAllEstandarConsServicio(codEnc));
						pagina = "/encuestas/consultaReportes/enc_rep_estandar_consolidada_servicio";
					}
				}
				else
				//DETALLE
				if(codTipoResultado.equals("1")){					
					//solo encuestas con tipo apliacion programa
					if(codTipoAplicacion.equals(CommonConstants.ENCUESTA_PROGRAMA)){								
						if(codPerfil.equals(CommonConstants.PERFIL_PFR) || codPerfil.equals(CommonConstants.PERFIL_PCC)){
							listaPregCer=this.consultasReportesEncuestasManager.GetAllEstandarDetalleAlumnoCer(codEnc);
							listaPregAbi=this.consultasReportesEncuestasManager.GetAllEstandarDetalleAlumnoAbi(codEnc);
							listaLeyenda = this.configuracionGeneracionManager.getAllVerEncuesta(codEnc);						    	
							request.setAttribute("LST_RESULTADO", listaPregCer);									
							request.setAttribute("LST_RESULTADO_2", listaPregAbi);
							request.setAttribute("LST_LEYENDA", listaLeyenda);
							request.setAttribute("COD_PERFIL", codPerfil);
							if(listaPregCer!=null && listaPregAbi!=null){
								System.out.println("listaPregCer.size>>"+listaPregCer.size());
								System.out.println("listaPregAbi.size>>"+listaPregAbi.size());
								if(listaPregCer.size()>0 || listaPregAbi.size()>0){
									pagina = "/encuestas/consultaReportes/enc_rep_estandar_detallado_alumno";
								}
								else{
									request.setAttribute("mensaje", "No se completó ninguna respuesta para la encuesta.");
									pagina = "/encuestas/consultaReportes/mensajes";
								}
							}
						}
						else
						if(codPerfil.equals(CommonConstants.PERFIL_EGRESADO)){
							listaPregCer=this.consultasReportesEncuestasManager.GetAllEstandarDetalleEgresadoCer(codEnc);
							listaPregAbi=this.consultasReportesEncuestasManager.GetAllEstandarDetalleEgresadoAbi(codEnc);
							listaLeyenda = this.configuracionGeneracionManager.getAllVerEncuesta(codEnc);						    	
							request.setAttribute("LST_RESULTADO", listaPregCer);									
							request.setAttribute("LST_RESULTADO_2", listaPregAbi);
							request.setAttribute("LST_LEYENDA", listaLeyenda);
							if(listaPregCer!=null && listaPregAbi!=null){
								System.out.println("listaPregCer.size>>"+listaPregCer.size());
								System.out.println("listaPregAbi.size>>"+listaPregAbi.size());
								if(listaPregCer.size()>0 || listaPregAbi.size()>0){
									pagina = "/encuestas/consultaReportes/enc_rep_estandar_detallado_egresado";
								}
								else{
									request.setAttribute("mensaje", "No se completó ninguna respuesta para la encuesta.");
									pagina = "/encuestas/consultaReportes/mensajes";
								}
							}
						}
						else
						if(codPerfil.equals(CommonConstants.PERFIL_PERSONAL)){
							listaPregCer=this.consultasReportesEncuestasManager.GetAllEstandarDetallePersonalCer(codEnc);
							listaPregAbi=this.consultasReportesEncuestasManager.GetAllEstandarDetallePersonalAbi(codEnc);
							listaLeyenda = this.configuracionGeneracionManager.getAllVerEncuesta(codEnc);						    	
							request.setAttribute("LST_RESULTADO", listaPregCer);									
							request.setAttribute("LST_RESULTADO_2", listaPregAbi);
							request.setAttribute("LST_LEYENDA", listaLeyenda);
							if(listaPregCer!=null && listaPregAbi!=null){
								System.out.println("listaPregCer.size>>"+listaPregCer.size());
								System.out.println("listaPregAbi.size>>"+listaPregAbi.size());
								if(listaPregCer.size()>0 || listaPregAbi.size()>0){
									pagina = "/encuestas/consultaReportes/enc_rep_estandar_detallado_personal";
								}
								else{
									request.setAttribute("mensaje", "No se completó ninguna respuesta para la encuesta.");
									pagina = "/encuestas/consultaReportes/mensajes";
								}
							}									
						}
					}
					else
					//solo encuetstas servicios
					if(codTipoAplicacion.equals(CommonConstants.ENCUESTA_SECCION)){
						listaPregCer=this.consultasReportesEncuestasManager.GetAllEstandarDetalleServicioCer(codEnc);
						listaPregAbi=this.consultasReportesEncuestasManager.GetAllEstandarDetalleServicioAbi(codEnc);
						listaLeyenda = this.configuracionGeneracionManager.getAllVerEncuesta(codEnc);						    	
						request.setAttribute("LST_RESULTADO", listaPregCer);									
						request.setAttribute("LST_RESULTADO_2", listaPregAbi);
						request.setAttribute("LST_LEYENDA", listaLeyenda);
						if(listaPregCer!=null && listaPregAbi!=null){
							System.out.println("listaPregCer.size>>"+listaPregCer.size());
							System.out.println("listaPregAbi.size>>"+listaPregAbi.size());
							if(listaPregCer.size()>0 || listaPregAbi.size()>0){
								pagina = "/encuestas/consultaReportes/enc_rep_estandar_detallado_servicio";
							}
							else{
								request.setAttribute("mensaje", "No se completó ninguna respuesta para la encuesta.");
								pagina = "/encuestas/consultaReportes/mensajes";
							}
						}
					}
				}
			}
		}
		else
		//REPORTE DE ENCUETAS MIXTAS
		if(codTipoReporte.equals("0003")){
			//if(codTipoEncuesta.equals(CommonConstants.TIPO_ENCUESTA_MIXTA)){
			
			log.info("REPORTE DE ENCUESTAS MIXTAS 0003" + " codEnc= " + codEnc);
			
			listaPregCer=this.consultasReportesEncuestasManager.GetAllMixtaDetalleCerrada(codEnc);
			listaPregAbi=this.consultasReportesEncuestasManager.GetAllMixtaDetalleAbierta(codEnc);
			
			request.setAttribute("LST_RESULTADO", listaPregCer);
			request.setAttribute("LST_RESULTADO_2", listaPregAbi);
			if(listaPregCer!=null && listaPregAbi!=null){
				System.out.println("listaPregCer.size>>"+listaPregCer.size());
				System.out.println("listaPregAbi.size>>"+listaPregAbi.size());
				if(listaPregCer.size()>0 || listaPregAbi.size()>0){
					pagina = "/encuestas/consultaReportes/enc_rep_mixta";
				}
				else{
					request.setAttribute("mensaje", "No se completó ninguna respuesta para la encuesta.");
					pagina = "/encuestas/consultaReportes/mensajes";
				}
			}
			//}
		}
		
		}
    	//}
    	log.info("PAGE REDIRECT:"+pagina+":");    	
    	log.info("onSubmit:FIN");
	    return new ModelAndView(pagina,"model",model);
    }

	private ValidaReporteBean valida(String operacion, String txhCodEncuesta,String codResponsable,HttpServletRequest request,
            HttpServletResponse response) {

		boolean permiso = false;
		
		log.info(">>valida>> ValidaReporteBean() ");
		
		log.info("operacion>>"+operacion+">>");
		log.info("txhCodEncuesta>>"+txhCodEncuesta+">>");
		log.info("codResponsable>>"+codResponsable+">>");
		
		
		List lst = consultasReportesEncuestasManager.getAllValidaRptaReporte(operacion, txhCodEncuesta, codResponsable);
		
		if( lst!=null && lst.size()>0 && lst.get(0)!=null )
		{
			ValidaReporteBean bean = (ValidaReporteBean) lst.get(0);
			String resp = bean.getCodResultado();
			String idEnc = bean.getIdEncuesta();
			
			log.info("ID RETORNADO >>"+idEnc+">>" + " >>Respuesta: " + resp);
			 
			if( resp==null || resp.equalsIgnoreCase(""))
			{				
				request.setAttribute("mensaje", "Error Interno");
				//return "-1";
				return null;
			}else if(Integer.parseInt(resp)>=0){
				permiso = true;				
				//return idEnc;
				return bean;
				
			}else{
				request.setAttribute("mensaje", bean.getDesResultado());
				//return "-1";
				return null;
			}
		}
		
		//return "-1";
		return null;

	}
	
	private String getPerfilEncuesta(String codEncuesta){
		List lista = this.configuracionPerfilManager.GetAllPerfilesByEncuesta(codEncuesta.trim());
		PerfilEncuesta obj = new PerfilEncuesta();
    	if(lista!=null && lista.size() == 1){
			obj = (PerfilEncuesta)lista.get(0);
    	}
    	return obj.getCodPerfil();
	}
}

