package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;

import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;



public class ConfGenVerEncuestasMixtasFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConfGenVerEncuestasMixtasFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	
	public ConfGenVerEncuestasMixtasFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesCommand control = new AplicacionDatosGeneralesCommand();    	
    	
    	if(request.getParameter("txhCodEncuesta")!=null )
    	{	
    		control.setTxhCodigo(request.getParameter("txhCodEncuesta"));
    		control.setUsuario(request.getParameter("txhUsuario"));
    		control.setTxhCodPerfil(request.getParameter("txhCodPerfil"));//codPerfil,
			control.setTxhCodEncuestado(request.getParameter("txhCodEncuestado"));//epdecId,
			control.setTxhCodProfesor(request.getParameter("txhCodProfesor"));

	    	
	    	//DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor(),"","");
			DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor());			
	    	request.setAttribute("OBJ_CABECERA",bean);
    	}    	
    	
    	request.setAttribute("LST_RESULTADO",VerEncuesta(control.getTxhCodigo()));
    	
        log.info("formBackingObject:FIN");
        return control;
    }
	



	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	String resp = "";    	
    	AplicacionDatosGeneralesCommand control = (AplicacionDatosGeneralesCommand) command;	
		
		if("GRABAR".equalsIgnoreCase(control.getOperacion())){
			
			/*log.info("0>>"+control.getTxhCodigo()+">>");
			log.info("1>>"+control.getTxhCodPerfil()+">>");
			log.info("2>>"+control.getTxhCodEncuestado()+">>");
			log.info("3>>"+request.getParameter("tknCodPreguntaCerrada")+">>");
			log.info("4>>"+request.getParameter("tknCodRespuestaCerrada")+">>");
			log.info("5>>"+request.getParameter("tknNumPregCerrada")+">>");
			log.info("6>>"+request.getParameter("tknCodPreguntaAbierta")+">>");
			log.info("7>>"+request.getParameter("tknCodRespuestaAbierta")+">>");
			log.info("8>>"+request.getParameter("tknNumPregAbierta")+">>");
			log.info("9>>"+control.getUsuario()+">>");*/
		
			resp = 	configuracionGeneracionManager.insertRespuestaByEncuestado(
				control.getTxhCodigo(),
				control.getTxhCodPerfil(),
				control.getTxhCodEncuestado(),
				request.getParameter("tknCodPreguntaCerrada"),
				request.getParameter("tknCodRespuestaCerrada"),
				request.getParameter("tknNumPregCerrada"),
				request.getParameter("tknCodPreguntaAbierta"),
				request.getParameter("tknCodRespuestaAbierta"),
				request.getParameter("tknNumPregAbierta"),
				control.getUsuario()
				);
		
		
			log.info("resp>>"+resp+">>");
			request.setAttribute("refresh","TRUE");		
	
			if("0".equalsIgnoreCase(resp)){
				request.setAttribute("refresh","TRUE");
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);    		
			}else if("-1".equalsIgnoreCase(resp)){
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				
				List lst = VerEncuesta(control.getTxhCodigo());
		    	request.setAttribute("LST_RESULTADO",lst);				
			}else{
				StringTokenizer tkn = new StringTokenizer(resp,"|");
				String msg1 = ""; String msg2 = "";
				msg1 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
				msg2 = tkn.hasMoreTokens()==true?tkn.nextToken():"";				
				request.setAttribute("mensaje",msg2);
				
				List lst = VerEncuesta(control.getTxhCodigo());
		    	request.setAttribute("LST_RESULTADO",lst);				
			}
		

			
		}
    	
    	//DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor(),"","");    	
		DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor());		
    	request.setAttribute("OBJ_CABECERA",bean);
		
		log.info("onSubmit:FIN");		
		control.setOperacion("");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_ver_encuesta_mixta","control",control);		
    }

	private List VerEncuesta(String txhCodigo){						
	
		return configuracionGeneracionManager.getAllVerEncuesta(txhCodigo);			
	}			
		
}
