package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Empleados;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoEgresadoCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilPersonalTecsupCommand;
import com.tecsup.SGA.web.encuestas.command.RegistrarPerfilesCommand;



public class PerfilPersonalTecsupFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(PerfilPersonalTecsupFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	PerfilPersonalTecsupCommand command = new PerfilPersonalTecsupCommand();
    	//************************************************************    	
    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
    	command.setCodTipoEncuesta(request.getParameter("txhCodTipoAplicacion") == null ? "" : request.getParameter("txhCodTipoAplicacion"));
    	System.out.println("CodEncuesta="+command.getCodEncuesta()+"<<");
    	System.out.println("CodUsuario="+command.getCodUsuario()+"<<");
    	System.out.println("CodTipoEncuesta="+command.getCodTipoEncuesta()+"<<");
    	//************************************************************
    	//ASIGNACION DE CONSTANTES
    	command.setEncuestaPrograma(CommonConstants.ENCUESTA_PROGRAMA);
    	command.setEncuestaSeccion(CommonConstants.ENCUESTA_SECCION);
    	command.setPerfilPERSONAL(CommonConstants.PERFIL_PERSONAL);
    	//************************************************************
    	CargarListas(command);
    	//SE EVALUA QUE LA ENCUESTA TENGA UN PERFIL
    	String codPerfil=request.getParameter("txhCodPerfilEncuesta") == null ? "" : request.getParameter("txhCodPerfilEncuesta");
    	//ENTRA CUANDO LA ENCUESTA YA TIENE UNO O VARIOS PERFILES GUARDADOS
    	if(!codPerfil.equals("")){
    		System.out.println("codPerfil="+codPerfil+"<<");
    		StringTokenizer tkm = new StringTokenizer(codPerfil,"|");
    		String codigoPerfil="";
    		while(tkm.hasMoreTokens()){
    			codigoPerfil=tkm.nextToken();
    			if(codigoPerfil.equals(CommonConstants.PERFIL_PERSONAL)){
    				System.out.println("entra cuando contiene el cod del perfil");
    				System.out.println("codigoPerfil>>"+codigoPerfil+"<<");
    				GetPerfilEncuesta(command,codigoPerfil);
    			}    			
    		}
    	}
    	//****************************************************
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	PerfilPersonalTecsupCommand control = (PerfilPersonalTecsupCommand) command;		
    	String resultado = "";
    	String mensaje="";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado=InsertPerfilEncuesta(control);
    		System.out.println("Resultado de grabar>>"+resultado+"<<");
    		
    		if(resultado.equals("0")){
    			control.setMsg("OK_GRABAR");
    			cargaPerfiles(control);
    			getTotalEncuestados(control);
    		}
    		else
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
    		CargarListas(control);
    		ObtenerListas(control.getListaPersonalDisponibles(), control.getCadPersonalSel(), control,"1");
    		ObtenerListas(control.getListaDocentesDisponibles(), control.getCadDocenteSel(), control, "2");
    	}
    	
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_perfil_personal_tecsup","control",control);		
    }    
    private void CargarListas(PerfilPersonalTecsupCommand control){
    	control.setListaPersonalDisponibles(this.configuracionPerfilManager.GetAllTipoPersonal("", ""));
    	control.setListaDocentesDisponibles(this.configuracionPerfilManager.GetAllTipoDocente("", "", ""));
    }    
    private void GetPerfilEncuesta(PerfilPersonalTecsupCommand control, String codPerfil){
    	List lista=this.configuracionPerfilManager.GetAllPerfilEncuesta(control.getCodEncuesta(), codPerfil);
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			obj = (PerfilEncuesta)lista.get(0);
    			
    			control.setCadPersonalSel(obj.getCodTipoPersonal());
    			control.setCadDocenteSel(obj.getCodTipoDocente());
    			ObtenerListas(control.getListaPersonalDisponibles(), control.getCadPersonalSel(), control, "1");
    			ObtenerListas(control.getListaDocentesDisponibles(), control.getCadDocenteSel(), control, "2");    			    			    			
    		}
    	}
    }    
    private String InsertPerfilEncuesta(PerfilPersonalTecsupCommand control){
    	try {
			PerfilEncuesta perfil = new PerfilEncuesta();
			
			perfil.setCodTipoEncuesta(control.getCodTipoEncuesta());
			perfil.setCodEncuesta(control.getCodEncuesta());
			perfil.setCodPerfil(CommonConstants.PERFIL_PERSONAL);
			perfil.setCodProducto(CommonConstants.COD_PRODUCTO_PFR);
			perfil.setCodPrograma("");
			perfil.setCodCurso("");
			perfil.setCodDepartamento("");
			perfil.setCodCiclo("");
			perfil.setCodTipoPersonal(control.getCadPersonalSel());
			perfil.setCodTipoDocente(control.getCadDocenteSel());
			perfil.setAñoIniEgresado("");
			perfil.setAñoFinEgresado("");
			
			String resultado = this.configuracionPerfilManager.InsertPerfilEncuesta(perfil, control.getCodUsuario());
			return resultado;
		}
    	catch (Exception e) {
			e.printStackTrace();			
		}
    	return null;
    }    
    private void ObtenerListas(List listaPersonalDis, String cadenaCodSel, PerfilPersonalTecsupCommand control, String indicador){
    	List listaDisponibles = null;
    	List listaSeleccionados = null;    	
    	StringTokenizer srt=new StringTokenizer(cadenaCodSel,"|");
    	String codigoSel="";
    	listaSeleccionados = new ArrayList();
    	while(srt.hasMoreTokens()){
    		codigoSel=srt.nextToken();    		
    		Empleados emp1 = new Empleados();
    		
    		for(int i=0; i<listaPersonalDis.size(); i++){
    			
    			emp1 = (Empleados)listaPersonalDis.get(i);
    			String cod=emp1.getCodigo();
    			
    			if(cod.equals(codigoSel)){
    			/*	Empleados emp2 = new Empleados();
    				emp2=(Empleados)listaPersonalDis.get(i);*/
    				listaSeleccionados.add(emp1);
    				listaPersonalDis.remove(i);
    				i=listaPersonalDis.size();
    			}
    		}
    	}
    	if(listaSeleccionados!=null){
    		if(indicador.equals("1")){
    			control.setListaPersonalSeleccionado(listaSeleccionados);
        		control.setListaPersonalDisponibles(listaPersonalDis);
    		}
    		else{
    			control.setListaDocentesSeleccionado(listaSeleccionados);
    			control.setListaDocentesDisponibles(listaPersonalDis);    			
    		}
    	}
    }
    private void getTotalEncuestados(PerfilPersonalTecsupCommand control){
    	List lista = this.configuracionGeneracionManager.getAllFormato("", 
    			"", "", "", control.getCodEncuesta(), "", "");
    	if(lista != null && lista.size()>0){
			FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);			
			
			control.setTotalEncuestados(obj.getTotalEncuestados());
			control.setDscEncuestados(obj.getDscEncuestados());
		}    	
    }
    private void cargaPerfiles(PerfilPersonalTecsupCommand control){
    	List lista = this.configuracionPerfilManager.GetAllPerfilesByEncuesta(control.getCodEncuesta());
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			String cadenaPerfiles="";
    			for(int i=0;i<lista.size();i++){
    				obj = (PerfilEncuesta)lista.get(i);
    				if(i==0){
    					cadenaPerfiles = obj.getCodPerfil() + "|";
    				}
    				else{
    					cadenaPerfiles = cadenaPerfiles + obj.getCodPerfil() + "|";
    				}    				
    			}
    			control.setCodPerfilEncuesta(cadenaPerfiles);    			
    		}
    	}
    }
    private void AsignarMensaje(PerfilPersonalTecsupCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_GRABAR");
		}		
    }
}
