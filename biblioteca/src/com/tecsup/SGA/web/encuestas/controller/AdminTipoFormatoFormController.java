package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AdminTipoFormatoCommand;

public class AdminTipoFormatoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminTipoFormatoFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		AdminTipoFormatoCommand command= new AdminTipoFormatoCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
    	List lista= new ArrayList();
    	lista=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_FORMATO_ENCUESTA_01, "", 
    			"", "",	"", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	request.getSession().setAttribute("listaBandejaFormato", lista);
    	log.info("formBackingObject:FIN");
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AdminTipoFormatoCommand control= (AdminTipoFormatoCommand) command;
    	String resultado="";
    	
    	if(control.getOperacion().equals("BUSCAR"))
    	{
    		BuscarBandeja(control, request);
    	}	
    	else{ if(control.getOperacion().equals("ELIMINAR"))
    			{
    			  resultado=Eliminar(control);
    			  System.out.println("Resultado Eliminar: "+resultado);
    			  if(resultado.equals("-1")) control.setMsg("ERROR");
    			  else if(resultado.equals("0")) control.setMsg("OK");
    			}
    	}
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/administracionSistema/enc_tipo_formato","control",control);		
    }
    
    public void BuscarBandeja(AdminTipoFormatoCommand control, HttpServletRequest request)
    {  
    	List lista= new ArrayList();
    	lista=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_FORMATO_ENCUESTA_01, "", 
    			control.getDscTipoFormato(), "","", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	request.getSession().setAttribute("listaBandejaFormato", lista);
    	
    }
    
    private String Eliminar(AdminTipoFormatoCommand control) {
    	try {
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    	   	obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_FORMATO_ENCUESTA_01);
    		obj.setCadCod(control.getCodSelec()+"|");
    		obj.setNroReg("1");
    		if (!obj.getCadCod().trim().equals("")) {
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager
    					.DeleteTablaDetalle(obj, control.getCodUsuario()));
    			log.info("CODIGO: "+obj.getCodTipoTablaDetalle());
    			return obj.getCodTipoTablaDetalle();
    		}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	return null;
    	}
    
}
