package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Empleados;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPCCCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilPersonalTecsupCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilProgramacionCommand;
import com.tecsup.SGA.web.encuestas.command.RegistrarPerfilesCommand;



public class PerfilAlumnoPCCFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(PerfilAlumnoPCCFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}
	
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	PerfilAlumnoPCCCommand command = new PerfilAlumnoPCCCommand();
    	//************************************************************
    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
    	command.setCodTipoEncuesta(request.getParameter("txhCodTipoAplicacion") == null ? "" : request.getParameter("txhCodTipoAplicacion"));    	
    	//************************************************************
    	//ASIGNACION DE CONSTANTES
    	command.setEncuestaPrograma(CommonConstants.ENCUESTA_PROGRAMA);
    	command.setEncuestaSeccion(CommonConstants.ENCUESTA_SECCION);
    	command.setPerfilPCC(CommonConstants.PERFIL_PCC);
    	//************************************************************
    	CargarData(command);
    	//SE EVALUA QUE LA ENCUESTA TENGA UN PERFIL
    	String codPerfil=request.getParameter("txhCodPerfilEncuesta") == null ? "" : request.getParameter("txhCodPerfilEncuesta");
    	//ENTRA CUANDO LA ENCUESTA YA TIENE UNO O VARIOS PERFILES GUARDADOS
    	if(!codPerfil.equals("")){    		
    		StringTokenizer tkm = new StringTokenizer(codPerfil,"|");
    		String codigoPerfil="";
    		while(tkm.hasMoreTokens()){
    			codigoPerfil=tkm.nextToken();
    			if(codigoPerfil.equals(CommonConstants.PERFIL_PCC)){    				
    				GetPerfilEncuesta(command,codigoPerfil);
    			}    			
    		}
    	}
    	//****************************************************
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	PerfilAlumnoPCCCommand control = (PerfilAlumnoPCCCommand) command;		
    	String resultado = "";
    	String mensaje = "";
		
    	if (control.getOperacion().trim().equals("PRODUCTO")){
			CargarPrograma(control);    		
    	}    	
    	else
    	if (control.getOperacion().trim().equals("BUSCAR_CURSOS")){
    		CargarCursos(control);    		
    		CargarPrograma(control);
    		control.setListaCursoSel(null);
    	}
    	else
    	if (control.getOperacion().trim().equals("GRABAR")){
    		CargarCursos(control);    		
    		CargarPrograma(control);
    		resultado=InsertPerfilEncuesta(control);    		
    		
    		if(resultado.equals("0")){
    			control.setMsg("OK_GRABAR");
    			cargaPerfiles(control);
    			getTotalEncuestados(control);
    		}
    		else
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
    		ObtenerListas(control);
    	}    	
    		
    	CargarData(control);
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_perfil_alumno_PCC","control",control);		
    }
    
        
    private void CargarData(PerfilAlumnoPCCCommand control){
	   CargarCombos(control);	   
    }    
    private void CargarCombos(PerfilAlumnoPCCCommand control){
    	//lista producto
    	control.setListaProducto(this.configuracionPerfilManager.GetAllProductos("", CommonConstants.COD_PRODUCTO_PFR));    			
    	//carga lista de la bandeja de departamento 
    	control.setListaDepartamento(this.configuracionPerfilManager.GetAllDepartamentos("", ""));    			
    	control.setTamListaDepartamento(""+control.getListaDepartamento().size());
    }
    
    private void CargarCursos(PerfilAlumnoPCCCommand control){    	    	
    	control.setListaCurso(this.configuracionPerfilManager.GetAllCursos("", "", control.getCodProducto(), control.getCodPrograma(), control.getCadenaCodDepartamentos(), ""));
    }
    private void CargarPrograma(PerfilAlumnoPCCCommand control){    	
    	control.setListaPrograma(this.configuracionPerfilManager.GetAllProgramas(control.getCodProducto(), "", ""));    	
    }
    private String InsertPerfilEncuesta(PerfilAlumnoPCCCommand control){
    	try {
			PerfilEncuesta perfil = new PerfilEncuesta();
			
			perfil.setCodTipoEncuesta(control.getCodTipoEncuesta());
			perfil.setCodEncuesta(control.getCodEncuesta());
			perfil.setCodPerfil(CommonConstants.PERFIL_PCC);
			perfil.setCodProducto(control.getCodProducto());
			perfil.setCodPrograma(control.getCodPrograma());
			perfil.setCodCurso(control.getCadenaCodCursos());
			perfil.setCodDepartamento(control.getCadenaCodDepartamentos());
			perfil.setCodCiclo("");
			perfil.setCodTipoPersonal("");
			perfil.setCodTipoDocente("");
			perfil.setAñoIniEgresado("");
			perfil.setAñoFinEgresado("");
			
			String resultado = this.configuracionPerfilManager.InsertPerfilEncuesta(perfil, control.getCodUsuario());
			return resultado;
		}
    	catch (Exception e) {
			e.printStackTrace();			
		}
    	return null;
    }
    private void GetPerfilEncuesta(PerfilAlumnoPCCCommand control, String codPerfil){
    	List lista=this.configuracionPerfilManager.GetAllPerfilEncuesta(control.getCodEncuesta(), codPerfil);
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			obj = (PerfilEncuesta)lista.get(0);
    			
    			control.setCodProducto(obj.getCodProducto());
    			control.setCodPrograma(obj.getCodPrograma());
    			control.setCadenaCodDepartamentos(obj.getCodDepartamento());
    			control.setCadenaCodCursos(obj.getCodCurso());
    			//SE CARGAN LOS CICLOS DEACUERDO AL CODPROGRAMA OBTENIDO
    			CargarPrograma(control);    			    			
        		CargarCursos(control);    			
    			ObtenerListas(control);
    			if(!control.getCodPrograma().equals("")){
    				control.setIndPrograma("1");
    			}    			
    		}
    	}
    }
    private void ObtenerListas(PerfilAlumnoPCCCommand control){
    	List listaPersonalDis = control.getListaCurso();
    	String cadenaCodSel = control.getCadenaCodCursos();
    	
    	List listaDisponibles = null;
    	List listaSeleccionados = null;
    	
    	System.out.println(">>"+cadenaCodSel+"<<");
    	StringTokenizer srt=new StringTokenizer(cadenaCodSel,"|");
    	String codigoSel="";
    	listaSeleccionados = new ArrayList();
    	while(srt.hasMoreTokens()){
    		
    		codigoSel=srt.nextToken();
    		System.out.println(">>"+codigoSel+"<<");
    		Curso emp1 = new Curso();
    		
    		for(int i=0; i<listaPersonalDis.size(); i++){
    			
    			emp1 = (Curso)listaPersonalDis.get(i);
    			String cod=emp1.getCodCurso();    			
    			
    			if(cod.equals(codigoSel)){    			
    				listaSeleccionados.add(emp1);
    				listaPersonalDis.remove(i);
    				i=listaPersonalDis.size();
    			}
    		}
    	}
    	if(listaSeleccionados!=null){    		
			control.setListaCurso(listaPersonalDis);
			control.setListaCursoSel(listaSeleccionados);
    		//control.setListaPersonalDisponibles(listaPersonalDis);    		
    	}
    }
    private void cargaPerfiles(PerfilAlumnoPCCCommand control){
    	List lista = this.configuracionPerfilManager.GetAllPerfilesByEncuesta(control.getCodEncuesta());
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			String cadenaPerfiles="";
    			for(int i=0;i<lista.size();i++){
    				obj = (PerfilEncuesta)lista.get(i);
    				if(i==0){
    					cadenaPerfiles = obj.getCodPerfil() + "|";
    				}
    				else{
    					cadenaPerfiles = cadenaPerfiles + obj.getCodPerfil() + "|";
    				}    				
    			}
    			control.setCodPerfilEncuesta(cadenaPerfiles);    			
    		}
    	}
    }
    private void getTotalEncuestados(PerfilAlumnoPCCCommand control){
    	List lista = this.configuracionGeneracionManager.getAllFormato("", 
    			"", "", "", control.getCodEncuesta(), "", "");
    	if(lista != null && lista.size()>0){
			FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);			
			
			control.setTotalEncuestados(obj.getTotalEncuestados());
			control.setDscEncuestados(obj.getDscEncuestados());
		}    	
    }
    private void AsignarMensaje(PerfilAlumnoPCCCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_GRABAR");
		}		
    }
}
