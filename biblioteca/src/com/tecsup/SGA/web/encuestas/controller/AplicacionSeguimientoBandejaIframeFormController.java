package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionSeguimientoBandejaIframeCommand;

public class AplicacionSeguimientoBandejaIframeFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AplicacionSeguimientoBandejaIframeFormController.class);
	
	private TablaDetalleManager tablaDetalleManager;
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		AplicacionSeguimientoBandejaIframeCommand command= new AplicacionSeguimientoBandejaIframeCommand();
		
		//******************************************************************
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){
    		
    		System.out.println("usuarioSeguridad codUsuario--->>"+usuarioSeguridad.getCodUsuario()+"<<");
    		System.out.println("usuarioSeguridad nomUsuario--->>"+usuarioSeguridad.getNomUsuario()+"<<");
    		System.out.println("usuarioSeguridad getPerfiles--->>"+usuarioSeguridad.getPerfiles()+"<<");
    		command.setNomUsuario(usuarioSeguridad.getNomUsuario());
    		command.setPerfilUsuario(usuarioSeguridad.getPerfiles());
    	}
		//*******************************************************************
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		
		if (request.getParameter("txhCodUsuario") != null) {
			
			command.setParCodTipoAplicacion(request.getParameter("txhParCodTipoAplicacion") == null ? "" :
			    request.getParameter("txhParCodTipoAplicacion"));
		    command.setParCodTipoEncuesta(request.getParameter("txhParCodTipoEncuesta") == null ? "" :
		        request.getParameter("txhParCodTipoEncuesta"));
		    command.setParCodEncuesta(request.getParameter("txhParCodEncuesta") == null ? "" :
	            request.getParameter("txhParCodEncuesta"));
		    command.setParCodTipoServicio(request.getParameter("txhParCodTipoServicio") == null ? "" :
		    	request.getParameter("txhParCodTipoServicio"));
		    command.setParCodTipoEstado(request.getParameter("txhParCodTipoEstado") == null ? "" :
		    	request.getParameter("txhParCodTipoEstado"));
		    command.setParResponsable(request.getParameter("txhParResponsable") == null ? "" :
	            request.getParameter("txhParResponsable"));
		    command.setParBandera(request.getParameter("txhParBandera") == null ? "" : 
			    request.getParameter("txhParBandera"));
		    
		    BuscarEncuestas(command, request);
		    
		System.out.println("CommandBandera:"+command.getParBandera());
		}
		
		log.info("formBackingObject:FIN");
		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	/**
	 * Redirect to the successView when the cancel button has been pressed.
	 */
	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		log.info("onSubmit:INI");
		
		AplicacionSeguimientoBandejaIframeCommand control=(AplicacionSeguimientoBandejaIframeCommand) command;
		
		log.info("onSubmit:FIN");
		
		return new ModelAndView("/encuestas/aplicacionSeguimiento/enc_bandeja_aplicacion_seguimiento_iframe","control", control);
	}
	
	private void BuscarEncuestas(AplicacionSeguimientoBandejaIframeCommand control, HttpServletRequest request) {    	
		List lista = new ArrayList();		
		
		if(control.getParCodTipoEstado().equals("")){
			control.setParCodTipoEstado(CommonConstants.COD_ENCUESTA_EN_APLICACION+","+CommonConstants.COD_ENCUESTA_APLICADA);
		}
		System.out.println("------------------------------------------- >>BuscarEncuestas<< -------------------------------");
		System.out.println("NroEncuesta>>"+control.getParCodEncuesta()+"<<");
		System.out.println("CodResponsable>>"+control.getParResponsable()+"<<");
		System.out.println("CodTipoAplicacion>>"+control.getParCodTipoAplicacion()+"<<");
		System.out.println("CodTipoEncuesta>>"+control.getParCodTipoEncuesta()+"<<");
		System.out.println("CodTipoServicio>>"+control.getParCodTipoServicio()+"<<");
		System.out.println("CodTipoEstado>>"+control.getParCodTipoEstado()+"<<");
		
		lista = this.configuracionGeneracionManager.getAllFormato(control.getParCodTipoAplicacion(),
				control.getParCodTipoServicio(), control.getParCodTipoEncuesta(), control.getParCodTipoEstado(), 
				"", control.getParResponsable(), control.getParCodEncuesta());
		
		if (lista != null){
			control.setListaBandeja(lista);
			control.setTamListaBandeja(""+lista.size());
			
			log.info("setTamListaBandeja() " + lista.size());
			
		}	
		else{
			control.setListaBandeja(null);
			control.setTamListaBandeja("0");
		}
		request.getSession().setAttribute("listaBandeja", lista);
	}
}
