package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.EnviarCorreoUtil;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilAnonimoPCCCommand;


		public class PerfilAnonimoPCCFormController extends SimpleFormController{
		private static Log log = LogFactory.getLog(PerfilAnonimoPCCFormController.class);
	
		private ConfiguracionGeneracionManager configuracionGeneracionManager;
		private ConfiguracionPerfilManager configuracionPerfilManager;
		
		public void setConfiguracionPerfilManager(
				ConfiguracionPerfilManager configuracionPerfilManager) {
			this.configuracionPerfilManager = configuracionPerfilManager;
		}
		
		public void setConfiguracionGeneracionManager(
				ConfiguracionGeneracionManager configuracionGeneracionManager) {
			this.configuracionGeneracionManager = configuracionGeneracionManager;
		}

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject: PerfilAnonimoPCCFormController: INICIO");   
	    	
	    	PerfilAnonimoPCCCommand command = new PerfilAnonimoPCCCommand();
	    	
	    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
	    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
	    	command.setCodTipoAplicacion(request.getParameter("txhCodTipoAplicacion") == null ? "" : request.getParameter("txhCodTipoAplicacion"));
	    	command.setCodTipoEncuesta(request.getParameter("txhCodTipoEncuesta") == null ? "" : request.getParameter("txhCodTipoEncuesta"));
	    	
	    	//ASIGNACION DE CONSTANTES
	    	command.setPerfilPPCAnonimo(CommonConstants.PERFIL_ANONIMO_PCC);
	    	
	    	String codPerfil=request.getParameter("txhCodPerfilEncuesta") == null ? "" : request.getParameter("txhCodPerfilEncuesta");
	    	if(!codPerfil.equals("")){    		
	    		StringTokenizer tkm = new StringTokenizer(codPerfil,"|");
	    		String codigoPerfil="";
	    		while(tkm.hasMoreTokens()){
	    			codigoPerfil=tkm.nextToken();
	    			if(codigoPerfil.equals(CommonConstants.PERFIL_ANONIMO_PCC)){    				
	    				//GetPerfilEncuesta(command,codigoPerfil);
	    			}
	    		}
	    	}
	    	
	    	command.setPerfilPPCAnonimo(CommonConstants.PERFIL_ANONIMO_PCC);
	    	command.setTipoEncuesta(request.getParameter("txhCodTipoEncuesta") == null ? "" : request.getParameter("txhCodTipoEncuesta"));
	    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
	    	//String tipo_encuesta = request.getParameter("txtTipoEncuesta") == null ? "" : request.getParameter("txtTipoEncuesta");
	    	
	    	log.info("Perfil_encuesta(pcc_anonimo)= "+CommonConstants.PERFIL_ANONIMO_PCC);
	    	log.info("txhCodEncuesta= " +request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
	    	log.info("txhCodUsuario= " +request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
	    	log.info("txhCodTipoAplicacion= " +request.getParameter("txhCodTipoAplicacion") == null ? "" : request.getParameter("txhCodTipoAplicacion"));
	    	log.info("txtTipoEncuesta= " +request.getParameter("txtTipoEncuesta") == null ? "" : request.getParameter("txtTipoEncuesta"));
	    	log.info("Codigo Tipo Encuesta= " + request.getParameter("txhCodTipoEncuesta"));
	    	
	        log.info("formBackingObject: PerfilAnonimoPCCFormController: FIN --- ");
	        
	        return command;
	    }
		
	    protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));
	    }
	    
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
        
    	//----
	    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit: - PerfilAnonimoPCCFormController(submit) - INI()");

    	PerfilAnonimoPCCCommand control = (PerfilAnonimoPCCCommand) command;
    	
    	String resultado = "";
    	log.info("Obteniendo valor getOperacion() "  + control.getOperacion());
    	log.info("Obteniendo valor codTipoEncuesta() "  + control.getTipoEncuesta());
    	
    	log.info("CODIGO_ENCUESTA " + control.getCodEncuesta());
    	
    	if (control.getOperacion().trim().equals("GRABAR")){    		
    		
    		resultado=InsertPerfilEncuesta(control);
    		if(resultado.equals("0")){
    			
    			log.info("obteniendo resultado { metodo guardar } -> " + resultado);
    			control.setMsg("OK_GRABAR");
    			
    			try {
					
    				log.info("codUsuario obtenido: " + control.getCodUsuario());
    		    	String datos_obtenidos = this.configuracionPerfilManager.obtener_correo(control.getCodUsuario());
    		    	
    		    	String nombre_encargado="";
    		    	String correo_encargado="";
    		    	
    		    	StringTokenizer str=new StringTokenizer(datos_obtenidos,"|");
    		    	if(str.hasMoreTokens()){
    		    		correo_encargado=str.nextToken();
    		    		nombre_encargado=str.nextToken();
    					
    					log.info("nombre_encargado: " + nombre_encargado + " | " + "correo_encargado: " +correo_encargado);
    				}
    		    	
    		    	if( nombre_encargado == null || correo_encargado == null ){
    		    		nombre_encargado="Erick";
    		    		correo_encargado="ecaycho@tecsup.edu.pe";
    		    	}
    		    	
    				String asunto = " ";
    		    	String saludo = "Estimada(o): "+nombre_encargado;
    		    	String mensaje2="Este es el siguiente enlace generado para el envio de tus participantes.";
    		    	String evento =" ";
    		    	String mensaje3=" Copiar el siguiente enlace:";
    		    	String despedida = "Saludos cordiales.";
    		    	String firma = "Robot UDS";
    		    	String cargo = "Unidad de Desarrollo de Sistemas";
    		    	
    		    	String enlace="";
    		    	
    		    	if(control.getTipoEncuesta().trim().equalsIgnoreCase("0001")){
    		    		
    		    		enlace = "http://www.tecsup.edu.pe/SGA/encuesta_anonimo/verEncuestas.html?txhCodEncuesta="+control.getCodEncuesta()+"&txhCodPerfil=" +CommonConstants.PERFIL_ANONIMO_PCC;
    		    		
    		    	}else{
    		    		
    		    		enlace = "http://www.tecsup.edu.pe/SGA/encuesta_anonimo/verEncuestasMixtas.html?txhCodEncuesta="+control.getCodEncuesta()+"&txhCodPerfil=" +CommonConstants.PERFIL_ANONIMO_PCC;
    		    		
    		    	}

    		    	StringBuilder out = new StringBuilder();
    		    	
    		    	out.append("<html>");
    				out.append("<body>");
    				out.append("<table align=\"center\" width=\"800\" height=\"450\"style=\"border: thin solid black;\" background=\"http://www.tecsup.edu.pe/REPORTES/image/mensajes2.jpg\">");
    				out.append("<tr><td align=\"center\" height=\"66\"><b>"+ asunto+ "</b></td></tr>");
    				out.append("<tr><td height=\"25\"><br><br><br>"+saludo +"</td></tr>");
    				out.append("<tr><td height=\"60\">"+mensaje2+"<b>"+evento+"</b>"+mensaje3+"<br>");
    				out.append("<a href=\""+enlace+"\" target=\"_blank\">"+enlace +"</a><br><br><b></b><br><br>" +despedida+ "<br><br>" +firma+"<br>"+ cargo+"<br><br></td></tr>");
    				out.append("</table>");
    				
    				out.append("</body>");
    				out.append("</html>");
    		    	
    				EnviarCorreoUtil send = new EnviarCorreoUtil();
					send.envioBuzon(correo_encargado, out.toString());
					
					log.info("Correo usuario: "+correo_encargado+" | Nombre usuario: " +nombre_encargado  +" - Correo enviado con éxito." );
    				
				} catch (Exception e) {
					log.error(e);
				}
    		}
    		
    		else
    		if(resultado.equals("-1")){
    			
    			log.info("obteniendo resultado -> " + resultado);
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
    	}  
    	
		log.info("onSubmit: PerfilAnonimoPCCFormController(submit) - FIN()");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_perfil_anonimo_PCC","control",control);		
    	
    	}
	    
	    private void AsignarMensaje(PerfilAnonimoPCCCommand control, String resultado){
	    	StringTokenizer str=new StringTokenizer(resultado,"|");
			if(str.hasMoreTokens()){
				control.setMsg(str.nextToken());
				control.setDscMensaje(str.nextToken());
			}
			else{
				control.setMsg("ERROR_GRABAR");
			}		
	    }
	    
	    private String InsertPerfilEncuesta(PerfilAnonimoPCCCommand control){
	    	try {
				PerfilEncuesta perfil = new PerfilEncuesta();
				
				perfil.setCodTipoEncuesta(control.getTipoEncuesta());
				perfil.setCodEncuesta(control.getCodEncuesta());
				perfil.setCodPerfil(CommonConstants.PERFIL_ANONIMO_PCC);
				perfil.setCodProducto("");
				perfil.setCodPrograma("");
				perfil.setCodCurso("");
				perfil.setCodDepartamento("");
				perfil.setCodCiclo("");
				perfil.setCodTipoPersonal("");
				perfil.setCodTipoDocente("");
				perfil.setAñoIniEgresado("");
				perfil.setAñoFinEgresado("");
				System.out.println("CodTipoEncuesta>> "+control.getTipoEncuesta()+" <<");
				System.out.println("CodEncuesta>> "+control.getCodEncuesta()+"<<");
				System.out.println("CodPerfil>>"+CommonConstants.PERFIL_ANONIMO_PCC+"<<");
				System.out.println("CodPrograma>> NOT DATA <<");
				System.out.println("CadenaCodCursos>>  NOT DATA  <<");
				System.out.println("CadenaCodDepartamentos>>  NOT DATA  <<");
				System.out.println("CadenaCodCiclos>>  NOT DATA  <<");			
				System.out.println("getCodUsuario>>"+control.getCodUsuario()+"<<");
				
				String resultado = this.configuracionPerfilManager.InsertPerfilEncuesta(perfil, control.getCodUsuario());
				
				log.info("InsertPerfilEncuesta --> " + resultado);
				
				return resultado;
			}
	    	catch (Exception e) {
				e.printStackTrace();			
			}
	    	return null;
	    }
	    
	    
	    
}
