package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.bean.FechaBean;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.AplicarProgramacionEncuestaCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilProgramacionCommand;



public class AplicarProgramacionEncuestaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicarProgramacionEncuestaFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;	
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicarProgramacionEncuestaCommand command = new AplicarProgramacionEncuestaCommand();
    	
    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));    	
    	
    	CargarListaHoras(command);
    	CargarFechaHoraActual(command);
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	AplicarProgramacionEncuestaCommand control = (AplicarProgramacionEncuestaCommand) command;		
		String resultado="";
		
		if(control.getOperacion().equals("GRABAR")){
			resultado=AplicarProgramacionEncuesta(control);			
    		System.out.println(">>"+resultado+"<<");
    		if(resultado.equals("0")){
    			control.setMsg("OK_GRABAR");    			
    		}
    		else
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
		}
		
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_aplicar_programacion_encuesta","control",control);		
    }
    private void CargarListaHoras(AplicarProgramacionEncuestaCommand control){    	
    	control.setListaHoraInicio(Anios.getHoras(CommonConstants.HORA_INICIAL_DIA, 
				CommonConstants.HORA_FINAL_DIA));
		control.setListaHoraFin(Anios.getHoras(CommonConstants.HORA_INICIAL_DIA, 
				CommonConstants.HORA_FINAL_DIA));
    }
    private void CargarFechaHoraActual(AplicarProgramacionEncuestaCommand control){
    	List lista=this.configuracionPerfilManager.GetFechaHoraSistema();
    	if(lista!=null && lista.size()==1){
    		FechaBean fecha=(FechaBean)lista.get(0);
        	control.setFechaActual(fecha.getFecha());
        	control.setHoraActual(fecha.getHora());
    	}    	
    }
    private String AplicarProgramacionEncuesta(AplicarProgramacionEncuestaCommand control){
    	try {
    		System.out.println(">>"+control.getCodEncuesta()+"<<");
    		System.out.println(">>"+control.getFechaVigenciaIni()+"<<");
    		System.out.println(">>"+control.getHoraVigenciaIni()+"<<");
    		System.out.println(">>"+control.getFechaVigenciaFin()+"<<");
    		System.out.println(">>"+control.getHoraVigenciaFin()+"<<");
    		System.out.println(">>"+control.getCodUsuario()+"<<");
			String resultado=this.configuracionPerfilManager.AplicarEncuesta(control.getCodEncuesta(), control.getFechaVigenciaIni(), control.getHoraVigenciaIni(), 
					control.getFechaVigenciaFin(), control.getHoraVigenciaFin(), control.getCodUsuario());
			return resultado;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    private void AsignarMensaje(AplicarProgramacionEncuestaCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_GRABAR");
		}
    }	
}
