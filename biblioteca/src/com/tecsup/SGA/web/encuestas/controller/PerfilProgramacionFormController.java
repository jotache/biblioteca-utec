package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilProgramacionCommand;

public class PerfilProgramacionFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(PerfilProgramacionFormController.class);	
	private ConfiguracionGeneracionManager configuracionGeneracionManager;	
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject: PerfilProgramacionFormController -INI");   	
    	PerfilProgramacionCommand command = new PerfilProgramacionCommand();
    	
    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
    	
    	log.info("Codigo Encuesta= " + request.getParameter("txhCodEncuesta"));
    	log.info("Codigo Usuario= " + request.getParameter("txhCodUsuario"));
    	
    	if(command.getCodEncuesta()!=null){
    		if(!command.getCodEncuesta().equals("")){
    			cargaCabecera(command);
    			cargaPerfiles(command);    			
    			CargarDatosBandeja(command, request);    			
    		}
    	}
    	CargarConstantes(command);
        log.info("formBackingObject: - PerfilProgramacionFormController - FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit: - PerfilProgramacionFormController INI");
    	PerfilProgramacionCommand control = (PerfilProgramacionCommand) command;
    	String resultado = "";
		
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = ActualizarConfiguracionEncuesta(control);    		
    		if (resultado.equals("0")){
    			control.setMsg("OK_GRABAR");
    		}
    		else
			if (resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
			}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
    	}
    	
    	log.info("getNomSede= " + control.getCodSede());
    	log.info("getNomSede= " + control.getCodUsuario());
    	
    	cargaCabecera(control);
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_perfil_programacion","control",control);		
    }
    private void cargaCabecera(PerfilProgramacionCommand control){
    	List lista = this.configuracionGeneracionManager.getAllFormato("", 
    			"", "", "", control.getCodEncuesta(), "", "");
    	
    	if(lista != null && lista.size()>0){
			FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);
			
			control.setCodTipoAplicacion(obj.getCodTipoAplicacion());
			control.setTipoAplicacion(obj.getNomTipoAplicacion());
			control.setTipoEncuesta(obj.getNomTipoEncuesta());
			control.setSede(obj.getNomSede());
			control.setCodigo(obj.getNroEncuesta());
			control.setNombre(obj.getNombreEncuesta());
			control.setDuracion(obj.getDuracion());
			control.setCodResponsable(obj.getCodResponsable());
			control.setResponsable(obj.getNomResponsable());
			control.setTipoServicio(obj.getNomTipoServicio());			
			control.setTotalEncuestado(obj.getTotalEncuestadosProg());
			control.setIndicadorManual(obj.getIndEncuestaManual());
			control.setCodEstado(obj.getCodEstado());
			control.setTotalEncuestado(obj.getTotalEncuestados());
			control.setDscEncuestados(obj.getDscEncuestados());
			
			log.info("CodTipoAplicacion= " + obj.getCodTipoAplicacion());
			log.info("NomTipoAplicacion= " + obj.getNomTipoAplicacion());
			log.info("NomTipoEncuesta= " + obj.getNomTipoEncuesta());
			log.info("getNomSede= " + obj.getNomSede());
			log.info("getNroEncuesta= " + obj.getNroEncuesta());
			log.info("getNombreEncuesta= " + obj.getNombreEncuesta());
			log.info("getDuracion= " + obj.getDuracion());
			log.info("getCodResponsable= " + obj.getCodResponsable());
			
			log.info("getNomResponsable= " + obj.getNomResponsable());
			log.info("getNomTipoServicio= " + obj.getNomTipoServicio());
			log.info("getTotalEncuestadosProg= " + obj.getTotalEncuestadosProg());
			
			log.info("getIndEncuestaManual= " + obj.getIndEncuestaManual());
			log.info("getTotalEncuestadosProg= " + obj.getTotalEncuestadosProg());
			log.info("getCodEstado= " + obj.getCodEstado());
			log.info("getTotalEncuestados= " + obj.getTotalEncuestados());
			log.info("getDscEncuestados= " + obj.getDscEncuestados());
			
		}    	
    }
    private void cargaPerfiles(PerfilProgramacionCommand control){
    	List lista = this.configuracionPerfilManager.GetAllPerfilesByEncuesta(control.getCodEncuesta());
    	
    	if(lista!=null){
    		
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			String cadenaPerfiles="";
    			for(int i=0;i<lista.size();i++){
    				obj = (PerfilEncuesta)lista.get(i);
    				if(i==0){
    					cadenaPerfiles = obj.getCodPerfil() + "|";
    					log.info("COD_PERFIL_ENCUESTA_1= " + cadenaPerfiles);
    				}
    				else{
    					cadenaPerfiles = cadenaPerfiles + obj.getCodPerfil() + "|";
    					log.info("COD_PERFIL_ENCUESTA_2= " + cadenaPerfiles);
    				}    				
    			}
    			control.setCodPerfilEncuesta(cadenaPerfiles);
    			
    			log.info("codPerfilEncuesta: " + cadenaPerfiles);
    		}
    	}
    }    
    private String ActualizarConfiguracionEncuesta(PerfilProgramacionCommand control){
    	try{
    		return this.configuracionGeneracionManager.UpdateConfiguracionPerfil(control.getCodEncuesta(),
    				control.getCodResponsable(), control.getIndicadorManual(), control.getCodUsuario());    		
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    private void CargarConstantes(PerfilProgramacionCommand control){
    	//nuevoPerfil-Anonimo-PCC (Erick_Caycho_Ponce)
    	
    	control.setConstPerfilAnonimo_PCC(CommonConstants.PERFIL_ANONIMO_PCC);
    	control.setConstPerfilPFC(CommonConstants.PERFIL_PFR);
    	control.setConstPerfilPCC(CommonConstants.PERFIL_PCC);
    	control.setConstPerfilEGRESADO(CommonConstants.PERFIL_EGRESADO);
    	control.setConstPerfilPERSONAL(CommonConstants.PERFIL_PERSONAL);
    	
    	control.setConstOrientado(CommonConstants.ORIENTADO);
    	//CONSTANTE ESTADO
    	control.setEstadoGenerada(CommonConstants.COD_ENCUESTA_GENERADA);
    	control.setEstadoEnAplicacion(CommonConstants.COD_ENCUESTA_EN_APLICACION);
    }
    private void CargarDatosBandeja(PerfilProgramacionCommand command, HttpServletRequest request){
    	
    	command.setParCodTipoAplicacion(request.getParameter("txhParCodTipoAplicacion") == null ? "" : 
            request.getParameter("txhParCodTipoAplicacion"));
    	command.setParCodTipoEncuesta(request.getParameter("txhParCodTipoEncuesta") == null ? "" : 
			request.getParameter("txhParCodTipoEncuesta"));
    	command.setParCodTipoServicio(request.getParameter("txhParCodTipoServicio") == null ? "" : 
			request.getParameter("txhParCodTipoServicio"));
    	command.setParCodTipoEstado(request.getParameter("txhParCodTipoEstado") == null ? "" : 
			request.getParameter("txhParCodTipoEstado"));
    	command.setParBandera(request.getParameter("txhParBandera") == null ? "" : 
			request.getParameter("txhParBandera"));    	
    }
    private void AsignarMensaje(PerfilProgramacionCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_GRABAR");
		}		
    }
}