package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;
import com.tecsup.SGA.web.encuestas.command.EnviarCorreoUtil;

		public class CargarDatosFichaMixtaAnonimoController extends SimpleFormController{
		private static Log log = LogFactory.getLog(CargarDatosFichaMixtaAnonimoController.class);
	
		private ConfiguracionGeneracionManager configuracionGeneracionManager;
		private ConfiguracionPerfilManager configuracionPerfilManager;
		
		public void setConfiguracionPerfilManager(
				ConfiguracionPerfilManager configuracionPerfilManager) {
			this.configuracionPerfilManager = configuracionPerfilManager;
		}
		
		public void setConfiguracionGeneracionManager(
				ConfiguracionGeneracionManager configuracionGeneracionManager) {
			this.configuracionGeneracionManager = configuracionGeneracionManager;
		}

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
			
	    	log.info("formBackingObject: PerfilAnonimoPCCFormController: CargarDatosFichaMixtaAnonimoController - INICIO() --- ");   	
	    	AplicacionDatosGeneralesCommand control = new AplicacionDatosGeneralesCommand();
	    	
	    	String codEncuestado = configuracionGeneracionManager.obtenerCodEstadoEncuesta(request.getParameter("txhCodEncuesta")); 
	    	control.setTxhCodEstado(codEncuestado);
	    	log.info("codEncuestado " + codEncuestado);
	    	log.info("control.getTxhCodEstado() " + control.getTxhCodEstado());
	    	
	    	
	    	if(!control.getTxhCodEstado().equalsIgnoreCase("0004")){
	    		 
	    		if(request.getParameter("txhCodEncuesta")!=null )
 
		    	{	
		      		
		      		log.info("txhCodEncuesta="+ request.getParameter("txhCodEncuesta"));
		      		log.info("txhUsuario="+ request.getParameter("txhUsuario"));
		      		log.info("txhCodPerfil="+ request.getParameter("txhCodPerfil"));
		      		log.info("txhCodEncuestado="+ request.getParameter("txhCodEncuestado"));
		      		log.info("txhCodProfesor="+ request.getParameter("txhCodProfesor"));
		      		
		      		
		    		control.setTxhCodigo(request.getParameter("txhCodEncuesta"));
		    		control.setUsuario(request.getParameter("txhUsuario"));
		    		control.setTxhCodPerfil(request.getParameter("txhCodPerfil"));//codPerfil,
					control.setTxhCodEncuestado(request.getParameter("txhCodEncuestado"));//epdecId,
					control.setTxhCodProfesor(request.getParameter("txhCodProfesor"));
			    	
			    	DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor());
			    	request.setAttribute("OBJ_CABECERA",bean);
			    	
		    	} 
	    	}
	    	
	    /*  	if(request.getParameter("txhCodEncuesta")!=null )
	    	{	
	      		
	      		log.info("txhCodEncuesta="+ request.getParameter("txhCodEncuesta"));
	      		log.info("txhUsuario="+ request.getParameter("txhUsuario"));
	      		log.info("txhCodPerfil="+ request.getParameter("txhCodPerfil"));
	      		log.info("txhCodEncuestado="+ request.getParameter("txhCodEncuestado"));
	      		log.info("txhCodProfesor="+ request.getParameter("txhCodProfesor"));
	      		
	      		
	    		control.setTxhCodigo(request.getParameter("txhCodEncuesta"));
	    		control.setUsuario(request.getParameter("txhUsuario"));
	    		control.setTxhCodPerfil(request.getParameter("txhCodPerfil"));//codPerfil,
				control.setTxhCodEncuestado(request.getParameter("txhCodEncuestado"));//epdecId,
				control.setTxhCodProfesor(request.getParameter("txhCodProfesor"));
		    	
		    	DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor());
		    	request.setAttribute("OBJ_CABECERA",bean);
		    	
	    	}  */
	    	
	      	request.setAttribute("LST_RESULTADO",VerEncuesta(control.getTxhCodigo()));
	      	
	        log.info("formBackingObject: PerfilAnonimoPCCFormController: CargarDatosFichaMixtaAnonimoController - FIN() ---");
	        
	        return control;
	    }
	
	    protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));
	    }
	    
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
        
	    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
	    log.info("onSubmit:INI - ModelAndView --> CargarDatosFichaMixtaAnonimoController() ");

    	String resp = "";
    	String codEncuestado="";
    	AplicacionDatosGeneralesCommand control = (AplicacionDatosGeneralesCommand) command;
    	
    	log.info("Obtener valor - Encuesta Mixta: ---> "  + control.getOperacion());
    	
    	
    	codEncuestado = configuracionGeneracionManager.obtenertxhCodEncuestadoByEncuestado(control.getTxhCodigo());
    	
    	
		if("0".equalsIgnoreCase(codEncuestado)){
			log.error("No se obtiene ningun valor, revisar el SP:sp_obt_cod_encuestado_anonimo");
		}else{
			control.setTxhCodEncuestado(codEncuestado);
		}
    	
		if("GRABAR".equalsIgnoreCase(control.getOperacion())){
			
			log.info("0 - TxhCodigo >>"+control.getTxhCodigo()+">>");
			log.info("1 - TxhCodPerfil >>"+control.getTxhCodPerfil()+">>");
			log.info("2 - TxhCodEncuestado >>"+control.getTxhCodEncuestado()+">>");
			log.info("3 - tknCodPreguntaCerrada >>"+request.getParameter("tknCodPreguntaCerrada")+">>");
			log.info("4 - tknCodRespuestaCerrada >>"+request.getParameter("tknCodRespuestaCerrada")+">>");
			log.info("5 - tknNumPregCerrada >>"+request.getParameter("tknNumPregCerrada")+">>");
			log.info("6 - tknCodPreguntaAbierta >>"+request.getParameter("tknCodPreguntaAbierta")+">>");
			log.info("7 - tknCodRespuestaAbierta>>"+request.getParameter("tknCodRespuestaAbierta")+">>");
			log.info("8 -tknNumPregAbierta >>"+request.getParameter("tknNumPregAbierta")+">>");
			log.info("9- getUsuario() >>"+control.getUsuario()+">>");
		
		
		log.info("Codigo_encuestado_encuesta (Encuesta Mixta)= " + control.getTxhCodEncuestado());
		
		resp = 	configuracionGeneracionManager.insertRespuestaByEncuestado(
				control.getTxhCodigo(),
				control.getTxhCodPerfil(),
				control.getTxhCodEncuestado(),
				request.getParameter("tknCodPreguntaCerrada"),
				request.getParameter("tknCodRespuestaCerrada"),
				request.getParameter("tknNumPregCerrada"),
				request.getParameter("tknCodPreguntaAbierta"),
				request.getParameter("tknCodRespuestaAbierta"),
				request.getParameter("tknNumPregAbierta"),
				control.getUsuario()
				);		
		
			log.info("resp>>"+resp+">>");
			request.setAttribute("refresh","TRUE");			
			
			if("0".equalsIgnoreCase(resp)){
				
				
				log.info("Update: identifica respuesta abierta => Genera un identificador para el reporte final");
				log.info("getCodigoEncuesta: "+control.getTxhCodigo());
				log.info("getCodPerfilEncuesta: " + control.getTxhCodPerfil());
				log.info("getCodPreguntaAbierta: "+request.getParameter("tknCodPreguntaAbierta")+">>");
				
				//Validamos si la encuesta cuenta con el campo de respuestas abiertas
				//if(request.getParameter("tknCodPreguntaAbierta") != null){
				if(request.getParameter("tknCodPreguntaAbierta").trim() != ""){
				
				String respuesta = configuracionGeneracionManager.marcarRespuestasAbiertas
						(control.getTxhCodigo(), control.getTxhCodPerfil(), request.getParameter("tknCodPreguntaAbierta"));
				
				log.info("Respuesta: Etiqueta para respuestas abiertas --> Resultado: "+ respuesta);
				
				if(respuesta.equalsIgnoreCase("0")){
					
					try {
						
	    				String asunto = " ";
	    		    	String saludo = "Estimado Admin: ";
	    		    	String mensaje2="Existe un error al asignarle una etiqueta a las respuestas abiertas.";
	    		    	String mensaje3=" SOLUCIONARLO CUANTO ANTES.";
	    		    	String despedida = "Saludos cordiales.";
	    		    	String firma = "Robot UDS";
	    		    	String cargo = "Unidad de Desarrollo de Sistemas";

	    		    	StringBuilder out = new StringBuilder();
	    		    	
	    		    	out.append("<html>");
	    				out.append("<body>");
	    				out.append("<table align=\"center\" width=\"800\" height=\"450\"style=\"border: thin solid black;\" background=\"http://www.tecsup.edu.pe/REPORTES/image/mensajes2.jpg\">");
	    				out.append("<tr><td align=\"center\" height=\"66\"><b>"+ asunto+ "</b></td></tr>");
	    				out.append("<tr><td height=\"25\"><br><br><br>"+saludo +"</td></tr>");
	    				out.append("<tr><td height=\"60\">"+mensaje2+"<br>"+mensaje3+"<br>");
	    				out.append("<br><br>" +despedida+ "<br><br>" +firma+"<br>"+ cargo+"<br><br></td></tr>");
	    				out.append("</table>");
	    				
	    				out.append("</body>");
	    				out.append("</html>");
	    		    	
	    				String correo_encargado="ecaycho@tecsup.edu.pe";
	    				EnviarCorreoUtil send = new EnviarCorreoUtil();
						send.envioBuzon(correo_encargado, out.toString());
						log.info("Usuario :" + correo_encargado +" - Correo enviado con �xito." );
	    				
					} catch (Exception e) {
						log.error(e);
					}
				}

				}
				
				request.setAttribute("refresh","TRUE");
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
//				
				List lst = VerEncuesta(control.getTxhCodigo());
				request.setAttribute("LST_RESULTADO",lst);
				return new ModelAndView("redirect:/encuesta_anonimo/finEncuesta.html");
//				
			}else if("-1".equalsIgnoreCase(resp)){
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				
				log.info("Obteniendo codigo para generar lista: " +control.getTxhCodigo());
				List lst = VerEncuesta(control.getTxhCodigo());
				log.info("T: "+ lst.size());
		    	request.setAttribute("LST_RESULTADO",lst);
		    	
			}else{
				
				StringTokenizer tkn = new StringTokenizer(resp,"|");
				String msg1 = ""; String msg2 = "";
				msg1 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
				msg2 = tkn.hasMoreTokens()==true?tkn.nextToken():"";				
				
				log.info("LLave error: " + msg1);
				
				if("-2".equalsIgnoreCase(msg1)){
					msg2="N�mero de encuesta incorrecta, accede otra vez al correo para responder a la encuesta.";
					request.setAttribute("mensaje",msg2);
					
					return new ModelAndView("redirect:/encuesta_anonimo/verEncuestasMixtas.html");
				}else{
					request.setAttribute("mensaje",msg2);
				}
				
				log.info("Respuesta ante otro tipo de error: " + msg2);
				
				
				List lst = VerEncuesta(control.getTxhCodigo());
				request.setAttribute("LST_RESULTADO",lst);
							
			}
		}
		
		DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor());
		request.setAttribute("OBJ_CABECERA",bean);
		//return new ModelAndView("/encuesta_anonimo/enc_anonima_formato_mixta","control",control);

		log.info("onSubmit:FIN - ModelAndView --> CargarDatosFichaMixtaAnonimoController");
		control.setOperacion("");
		
		//return new ModelAndView("/encuesta_anonimo/enc_anonima_formato_mixta","control",control);	
		return new ModelAndView("redirect:/encuesta_anonimo/finEncuesta.html");
	   }
	    
		private List VerEncuesta(String txhCodigo) {
			return configuracionGeneracionManager.getAllVerEncuesta(txhCodigo);
		}

}
