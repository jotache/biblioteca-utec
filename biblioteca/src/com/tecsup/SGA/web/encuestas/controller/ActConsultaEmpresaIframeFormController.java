package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.encuestas.ActualizacionDatosManager;
import com.tecsup.SGA.web.encuestas.command.ActConsultaEmpresaIframeCommand;

public class ActConsultaEmpresaIframeFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ActConsultaEmpresaIframeFormController.class);
	private ActualizacionDatosManager actualizacionDatosManager;
	
	public ActualizacionDatosManager getActualizacionDatosManager() {		
		return actualizacionDatosManager;
	}
	public void setActualizacionDatosManager(
			ActualizacionDatosManager actualizacionDatosManager) {
		this.actualizacionDatosManager = actualizacionDatosManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
	log.info("formBackingObject:INI");
	ActConsultaEmpresaIframeCommand command= new ActConsultaEmpresaIframeCommand();
	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
	command.setNombre((String)request.getParameter("txhNombre"));
	request.setAttribute("codUsuario", command.getCodUsuario());    	
	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
	BuscarBandeja(command, request);	
	log.info("formBackingObject:FIN");
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	ActConsultaEmpresaIframeCommand control=(ActConsultaEmpresaIframeCommand) command;
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/actualizacionDatos/enc_act_consulta_empresas_iframe","control",control);		
    }
    public void BuscarBandeja(ActConsultaEmpresaIframeCommand control, HttpServletRequest request){
    	List lista= new ArrayList();
    	lista=this.actualizacionDatosManager.GetAllEncuestaEmpresa(control.getNombre());
    	request.getSession().setAttribute("listaBandejaEmpresa", lista);
    }
}
