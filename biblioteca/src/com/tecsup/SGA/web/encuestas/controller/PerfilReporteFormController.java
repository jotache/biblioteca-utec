package com.tecsup.SGA.web.encuestas.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;

public class PerfilReporteFormController implements Controller{
	private static Log log = LogFactory.getLog(PerfilReporteFormController.class);	
    
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	public PerfilReporteFormController(){
		
	}
    
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception {
    	log.info("onSubmit:INI");
    	String codTipoEncuesta = request.getParameter("txhCodTipoEncuesta");
    	String codTipoPerfil = request.getParameter("txhCodTipoPerfil");
    	String codEncuesta = request.getParameter("txhCodEncuesta");
    	System.out.println("codTipoEncuesta>>"+codTipoEncuesta);
    	System.out.println("codTipoPerfil>>"+codTipoPerfil);
    	System.out.println("codEncuesta>>"+codEncuesta);
    	
    	String fechaRep = Fecha.getFechaActual();
		String horaRep = Fecha.getHoraActual();
    	
    	ModelMap model = new ModelMap();
    	
    	model.addObject("fechaRep", fechaRep);
		model.addObject("horaRep", horaRep);
		
		List consulta=null;
		String pagina="";
		String titulo="";		
		String codTipoAplicacion = "";
		String indManual = "";
		List lista = this.configuracionGeneracionManager.getAllFormato("", 
    			"", "", "", codEncuesta, "", "");
    	if(lista != null && lista.size() == 1){
			FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);
			
			model.addObject("tipoAplicacion", obj.getNomTipoAplicacion());
			model.addObject("tipoEncuesta", obj.getNomTipoEncuesta());
			model.addObject("sede", obj.getNomSede());
			model.addObject("nroEncuesta", obj.getNroEncuesta());
			model.addObject("nomEncuesta", obj.getNombreEncuesta());
			model.addObject("duracion", obj.getDuracion());
			model.addObject("tipoServicio", obj.getNomTipoServicio());
			codTipoAplicacion = obj.getCodTipoAplicacion();
			indManual = obj.getIndEncuestaManual();			
		}   
		
		if(codTipoEncuesta.equals(CommonConstants.ENCUESTA_PROGRAMA)){			
			if(codTipoPerfil.equals(CommonConstants.PERFIL_PFR)){
				//PFR
				titulo="Listado de Participantes por Configuracion del Perfil: PFR";
				model.addObject("titulo", titulo);
				consulta=this.configuracionPerfilManager.GetAllProfesorPFR(codEncuesta, codTipoPerfil);
				request.getSession().setAttribute("consulta", consulta);
				request.getSession().setAttribute("indManual", indManual);
				if(consulta.size()>0){
	        		pagina = "/encuestas/configuracionGeneracion/enc_perfil_reporte_PFR";
	    		}
	    		else
	    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
			}
			else
			if(codTipoPerfil.equals(CommonConstants.PERFIL_PCC)){
				titulo="Listado de Participantes por Configuracion del Perfil: PCC";
				model.addObject("titulo", titulo);
				consulta=this.configuracionPerfilManager.GetAllProfesorPCC(codEncuesta, codTipoPerfil);				
				request.getSession().setAttribute("consulta", consulta);
				request.getSession().setAttribute("indManual", indManual);
				if(consulta.size()>0){
	        		pagina = "/encuestas/configuracionGeneracion/enc_perfil_reporte_PCC";
	    		}
	    		else
	    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
			}
			else
			if(codTipoPerfil.equals(CommonConstants.PERFIL_EGRESADO) || codTipoPerfil.equals(CommonConstants.PERFIL_PERSONAL)){
				if(codTipoPerfil.equals(CommonConstants.PERFIL_EGRESADO))titulo="Listado de Participantes por Configuracion del Perfil: Alumnos Egresados";
				if(codTipoPerfil.equals(CommonConstants.PERFIL_PERSONAL))titulo="Listado de Participantes por Configuracion del Perfil: Personal Tecsup";
				model.addObject("titulo", titulo);
				consulta=this.configuracionPerfilManager.GetAllAmbitoGeneral(codEncuesta, codTipoPerfil);								
				request.getSession().setAttribute("consulta", consulta);
				request.getSession().setAttribute("indManual", indManual);
				if(consulta.size()>0){
	        		pagina = "/encuestas/configuracionGeneracion/enc_perfil_reporte_general";
	    		}
	    		else
	    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
			}
		}
		else
		if(codTipoEncuesta.equals(CommonConstants.ENCUESTA_SECCION)){
			if(codTipoPerfil.equals(CommonConstants.PERFIL_PFR))titulo="Listado de Participantes por Configuracion del Perfil: PFR";
			if(codTipoPerfil.equals(CommonConstants.PERFIL_PCC))titulo="Listado de Participantes por Configuracion del Perfil: PCC";
			if(codTipoPerfil.equals(CommonConstants.PERFIL_EGRESADO))titulo="Listado de Participantes por Configuracion del Perfil: Alumnos Egresados";
			if(codTipoPerfil.equals(CommonConstants.PERFIL_PERSONAL))titulo="Listado de Participantes por Configuracion del Perfil: Personal Tecsup";			
			model.addObject("titulo", titulo);
			consulta=this.configuracionPerfilManager.GetAllAmbitoGeneral(codEncuesta, codTipoPerfil);				
			request.getSession().setAttribute("consulta", consulta);
			request.getSession().setAttribute("codTipoAplicacion", codTipoAplicacion);
			request.getSession().setAttribute("indManual", indManual);
			if(consulta.size()>0){
        		pagina = "/encuestas/configuracionGeneracion/enc_perfil_reporte_general";
    		}
    		else
    			pagina = "/biblioteca/userAdmin/consultaReportes/bib_close";
		}    	

    	log.info("onSubmit:FIN");
	    return new ModelAndView(pagina,"model",model);
    }
}
