package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionSeguimientoRegistroManualCommand;


public class AplicacionSeguimientoRegistroManualFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicacionSeguimientoRegistroManualFormController.class);
	
	ConfiguracionGeneracionManager configuracionGeneracionManager;

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	public AplicacionSeguimientoRegistroManualFormController() {    	
        super();        
        setCommandClass(AplicacionSeguimientoRegistroManualCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionSeguimientoRegistroManualCommand control = new AplicacionSeguimientoRegistroManualCommand();    	
    	
    	control.setFlagMultiple(request.getParameter("flagMultiple"));
    	control.setFlagObligatorio(request.getParameter("flagObligatorio"));
    	System.out.println("txhCodigo: "+request.getParameter("txhCodigo"));
    	if(request.getParameter("txhCodigo")!=null )
    	{   System.out.println("Dentro del txhCodigo");
    		control.setTxhCodigo(request.getParameter("txhCodigo").trim());
    		control.setTxhUsuario(request.getParameter("txhUsuario"));
    		
    		control.setParCodTipoAplicacion(request.getParameter("txhParCodTipoAplicacion") == null ? "" :
			    request.getParameter("txhParCodTipoAplicacion"));
    		control.setParCodTipoEncuesta(request.getParameter("txhParCodTipoEncuesta") == null ? "" :
		        request.getParameter("txhParCodTipoEncuesta"));
    		control.setParCodEncuesta(request.getParameter("txhParCodEncuesta") == null ? "" :
	            request.getParameter("txhParCodEncuesta"));
    		control.setParCodTipoServicio(request.getParameter("txhParCodTipoServicio") == null ? "" :
    			request.getParameter("txhParCodTipoServicio"));
    		control.setParCodTipoEstado(request.getParameter("txhParCodTipoEstado") == null ? "" :
    			request.getParameter("txhParCodTipoEstado"));
    		control.setParResponsable(request.getParameter("txhParResponsable") == null ? "" :
	            request.getParameter("txhParResponsable"));
    		control.setParBandera(request.getParameter("txhParBandera") == null ? "" : 
			    request.getParameter("txhParBandera"));
    		control.setParCodResponsable(request.getParameter("txhParCodResponsable") == null ? "" : 
			    request.getParameter("txhParCodResponsable"));
    		
    		System.out.println("ParCodTipoAplicacion: "+control.getParCodTipoAplicacion()+
    				">>ParCodTipoEncuesta: "+control.getParCodTipoEncuesta()+
    				">>ParCodEncuesta: "+control.getParCodEncuesta()+
    				">>ParCodTipoServicio: "+control.getParCodTipoServicio()+
    				">>ParResponsable: "+control.getParResponsable()+
    				">>ParBandera: "+control.getParBandera()+
    				">>ParCodResponsable: "+control.getParCodResponsable());
    				
    				//DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),null,"","");	    	
    				DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),null);    				
	    			request.setAttribute("OBJ_CABECERA",bean);
    	}
    	
        log.info("formBackingObject:FIN");
        return control;
    }

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	AplicacionSeguimientoRegistroManualCommand control = (AplicacionSeguimientoRegistroManualCommand) command;		
			
		
	    return new ModelAndView("/encuestas/aplicacionSeguimiento/enc_registro_manual","control",control);		
    }


		
}
