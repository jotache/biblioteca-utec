package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.ConfGenBandejaEncuestasIframeCommand;

public class ReporteBusquedaEncuestasIframeFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ReporteBusquedaEncuestasIframeFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private TablaDetalleManager tablaDetalleManager;
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
	throws ServletException {
			log.info("formBackingObject:INI");
			
			ConfGenBandejaEncuestasIframeCommand command= new ConfGenBandejaEncuestasIframeCommand();
			//******************************************************************
			UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
	    	if ( usuarioSeguridad != null ){
	    		
	    		System.out.println("usuarioSeguridad codUsuario--->>"+usuarioSeguridad.getCodUsuario()+"<<");
	    		
	    		System.out.println("usuarioSeguridad codigo Persona Usuario--->>"+usuarioSeguridad.getIdUsuario()+"<<");
	    		
	    		System.out.println("usuarioSeguridad nomUsuario--->>"+usuarioSeguridad.getNomUsuario()+"<<");
	    		System.out.println("usuarioSeguridad getPerfiles--->>"+usuarioSeguridad.getPerfiles()+"<<");
	    		command.setNomUsuario(usuarioSeguridad.getNomUsuario());
	    		command.setPerfilUsuario(usuarioSeguridad.getPerfiles());
	    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
	    	}
			//*******************************************************************
	    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
			if (request.getParameter("txhCodUsuario") != null) {
				command.setParBandera(request.getParameter("txhParBandera") == null ? "" : 
				    request.getParameter("txhParBandera"));
			    command.setParCodTipoAplicacion(request.getParameter("txhParCodTipoAplicacion") == null ? "" :
				    request.getParameter("txhParCodTipoAplicacion"));
			    command.setParCodTipoEncuesta(request.getParameter("txhParCodTipoEncuesta") == null ? "" :
			        request.getParameter("txhParCodTipoEncuesta"));
			    command.setParCodTipoEstado(request.getParameter("txhParCodTipoEstado") == null ? "" :
		            request.getParameter("txhParCodTipoEstado"));
			    command.setParCodTipoServicio(request.getParameter("txhParCodTipoServicio") == null ? "" :
	            request.getParameter("txhParCodTipoServicio"));
			    command.setTxtBusqueda(request.getParameter("txhTxtBusqueda"));
			    
			    BuscarBandeja(command, request);
			System.out.println("CommandBandera:"+command.getParBandera());
			}
			
			/*List lista = new ArrayList();
			if (command.getCodUsuario() != null){
				request.getSession().setAttribute("listaBandejaConfiguracion",
						lista);
				command.setTamListaBandeja("0");
			}*/
			log.info("formBackingObject:FIN");
			return command;
		}

		protected void initBinder(HttpServletRequest request,
				ServletRequestDataBinder binder) {
			NumberFormat nf = NumberFormat.getNumberInstance();
			binder.registerCustomEditor(Long.class, new CustomNumberEditor(
					Long.class, nf, true));
		}

		/**
		 * Redirect to the successView when the cancel button has been pressed.
		 */
		public ModelAndView processFormSubmission(HttpServletRequest request,
				HttpServletResponse response, Object command, BindException errors)
				throws Exception {
			return super.processFormSubmission(request, response, command, errors);
		}

		public ModelAndView onSubmit(HttpServletRequest request,
				HttpServletResponse response, Object command, BindException errors)
				throws Exception {
			log.info("onSubmit:INI");
			
			ConfGenBandejaEncuestasIframeCommand control=(ConfGenBandejaEncuestasIframeCommand) command;
			
			log.info("onSubmit:FIN");
			return new ModelAndView(
					"/encuestas/consultaReportes/enc_consulta_busquedaEncuesta_iframe",
					"control", control);
		}
		
		public void BuscarBandeja(ConfGenBandejaEncuestasIframeCommand control,
				HttpServletRequest request) {
			
			log.info("Obteniendo Cod usuario para el filtro de encuestas por Usuario/Responsable  --->");
			UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
			control.setCodUsuario(usuarioSeguridad.getIdUsuario());
			
			
			List lista = new ArrayList();
			if (control.getParCodTipoAplicacion().equals("-1"))
				control.setParCodTipoAplicacion("");
			if (control.getParCodTipoServicio().equals("-1"))
				control.setParCodTipoServicio("");
			if (control.getParCodTipoEncuesta().equals("-1"))
				control.setParCodTipoEncuesta("");
			/*if (control.getParCodTipoEstado().equals("-1"))
				control.setParCodTipoEstado(CommonConstants.COD_ENCUESTA_PENDIENTE+","+CommonConstants.COD_ENCUESTA_GENERADA);*/
			if (control.getParCodTipoEstado().equals("-1"))
				control.setParCodTipoEstado(CommonConstants.COD_ENCUESTA_EN_APLICACION+","+CommonConstants.COD_ENCUESTA_APLICADA);			
			
			
			System.out.println("control.getPerfilUsuario()>>"+control.getPerfilUsuario()+"<<");
			System.out.println("control.getCodUsuario()>>"+control.getCodUsuario()+"<<");
			System.out.println("control.getNomUsuario()>>"+control.getNomUsuario()+"<<");
			
			if("0".equals(control.getPerfilUsuario())){
				control.setCodResponsable("");
			}
			else{
				control.setCodResponsable(control.getCodUsuario());
			}
			System.out.println("getAllFormato");
			System.out.println("1>>"+control.getParCodTipoAplicacion()+">");
			System.out.println("2>>"+control.getParCodTipoServicio()+">");
			System.out.println("3>>"+control.getParCodTipoEncuesta()+">");
			System.out.println("4>>"+control.getParCodTipoEstado()+">");
			System.out.println("5>>>>");
		
			System.out.println("6>>"+control.getCodUsuario()+"<<"); // Cod responsable de la encuesta 2013-12-20
			System.out.println("7>>>>");
			System.out.println("8>>"+control.getTxtBusqueda()+">>");
			
			lista = this.configuracionGeneracionManager.getAllEncFormatoBusqueda(control
					.getParCodTipoAplicacion(), control.getParCodTipoServicio(), control
					.getParCodTipoEncuesta(), control.getParCodTipoEstado(), "", control.getCodResponsable(), ""
					,control.getTxtBusqueda());
			if (lista != null){
				//System.out.println("Size: " + lista.size());
				control.setTamListaBandeja(""+lista.size());
			}	
			else{
				//System.out.println("Lista Null");
				control.setTamListaBandeja("0");
			}
			request.getSession().setAttribute("listaBandejaConfiguracion", lista);
		}
}
