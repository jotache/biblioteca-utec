package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoEgresadoCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.RegistrarPerfilesCommand;



public class PerfilAlumnoEgresadoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(PerfilAlumnoEgresadoFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	PerfilAlumnoEgresadoCommand command = new PerfilAlumnoEgresadoCommand();
    	//************************************************************    	
    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
    	command.setCodTipoEncuesta(request.getParameter("txhCodTipoAplicacion") == null ? "" : request.getParameter("txhCodTipoAplicacion"));    	
    	//************************************************************
    	//ASIGNACION DE CONSTANTES
    	command.setEncuestaPrograma(CommonConstants.ENCUESTA_PROGRAMA);
    	command.setEncuestaSeccion(CommonConstants.ENCUESTA_SECCION);
    	command.setPerfilEGRESADO(CommonConstants.PERFIL_EGRESADO);
    	//************************************************************
    	CargarCombos(command);
    	//SE EVALUA QUE LA ENCUESTA TENGA UN PERFIL
    	String codPerfil=request.getParameter("txhCodPerfilEncuesta") == null ? "" : request.getParameter("txhCodPerfilEncuesta");
    	//ENTRA CUANDO LA ENCUESTA YA TIENE UNO O VARIOS PERFILES GUARDADOS
    	if(!codPerfil.equals("")){    		
    		StringTokenizer tkm = new StringTokenizer(codPerfil,"|");
    		String codigoPerfil="";
    		while(tkm.hasMoreTokens()){
    			codigoPerfil=tkm.nextToken();
    			if(codigoPerfil.equals(CommonConstants.PERFIL_EGRESADO)){    				
    				GetPerfilEncuesta(command,codigoPerfil);
    			}    			
    		}
    	}
    	//****************************************************
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	PerfilAlumnoEgresadoCommand control = (PerfilAlumnoEgresadoCommand) command;		
    	String resultado = "";
    	String mensaje = "";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado=InsertPerfilEncuesta(control);    		
    		
    		if(resultado.equals("0")){
    			control.setMsg("OK_GRABAR");
    			cargaPerfiles(control);
    			getTotalEncuestados(control);
    		}
    		else
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
    	}    	
    		
    	CargarCombos(control);
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_perfil_alumno_egresado","control",control);		
    }    
    private void CargarCombos(PerfilAlumnoEgresadoCommand control){
    	//lista programa
    	control.setListaProgramaEgresado(this.configuracionPerfilManager.GetAllProgramas(CommonConstants.COD_PRODUCTO_PFR, "", ""));
    }
    private void GetPerfilEncuesta(PerfilAlumnoEgresadoCommand control, String codPerfil){
    	List lista=this.configuracionPerfilManager.GetAllPerfilEncuesta(control.getCodEncuesta(), codPerfil);
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			obj = (PerfilEncuesta)lista.get(0);
    			
    			control.setCodProgramaEgresado(obj.getCodPrograma());
    			control.setAñoIngreso(obj.getAñoIniEgresado());
    			control.setAñoEgreso(obj.getAñoFinEgresado());    			    			
    		}
    	}    	
    }
    private String InsertPerfilEncuesta(PerfilAlumnoEgresadoCommand control){
    	try {
			PerfilEncuesta perfil = new PerfilEncuesta();
			
			perfil.setCodTipoEncuesta(control.getCodTipoEncuesta());
			perfil.setCodEncuesta(control.getCodEncuesta());
			perfil.setCodPerfil(CommonConstants.PERFIL_EGRESADO);
			perfil.setCodProducto(CommonConstants.COD_PRODUCTO_PFR);
			perfil.setCodPrograma(control.getCodProgramaEgresado());
			perfil.setCodCurso("");
			perfil.setCodDepartamento("");
			perfil.setCodCiclo("");
			perfil.setCodTipoPersonal("");
			perfil.setCodTipoDocente("");
			perfil.setAñoIniEgresado(control.getAñoIngreso());
			perfil.setAñoFinEgresado(control.getAñoEgreso());
			
			String resultado = this.configuracionPerfilManager.InsertPerfilEncuesta(perfil, control.getCodUsuario());
			return resultado;
		}
    	catch (Exception e) {
			e.printStackTrace();			
		}
    	return null;
    }
    private void getTotalEncuestados(PerfilAlumnoEgresadoCommand control){
    	List lista = this.configuracionGeneracionManager.getAllFormato("", 
    			"", "", "", control.getCodEncuesta(), "", "");
    	if(lista != null && lista.size()>0){
			FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);			
			
			control.setTotalEncuestados(obj.getTotalEncuestados());
			control.setDscEncuestados(obj.getDscEncuestados());
		}    	
    }
    private void cargaPerfiles(PerfilAlumnoEgresadoCommand control){
    	List lista = this.configuracionPerfilManager.GetAllPerfilesByEncuesta(control.getCodEncuesta());
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			String cadenaPerfiles="";
    			for(int i=0;i<lista.size();i++){
    				obj = (PerfilEncuesta)lista.get(i);
    				if(i==0){
    					cadenaPerfiles = obj.getCodPerfil() + "|";
    				}
    				else{
    					cadenaPerfiles = cadenaPerfiles + obj.getCodPerfil() + "|";
    				}    				
    			}
    			control.setCodPerfilEncuesta(cadenaPerfiles);    			
    		}
    	}
    }
    private void AsignarMensaje(PerfilAlumnoEgresadoCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_GRABAR");
		}		
    }
}
