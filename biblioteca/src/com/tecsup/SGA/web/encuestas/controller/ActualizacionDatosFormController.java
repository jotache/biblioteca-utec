package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.EncuestaEmpresa;
import com.tecsup.SGA.service.encuestas.ActualizacionDatosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.ActConsultaEmpresaCommand;
import com.tecsup.SGA.web.encuestas.command.ActualizacionDatosCommand;
/***********************************/
import java.security.Security;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/***********************************/

public class ActualizacionDatosFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(ActualizacionDatosFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	private ActualizacionDatosManager actualizacionDatosManager;
	
	public ActualizacionDatosManager getActualizacionDatosManager() {
		return actualizacionDatosManager;
	}

	public void setActualizacionDatosManager(
			ActualizacionDatosManager actualizacionDatosManager) {
		this.actualizacionDatosManager = actualizacionDatosManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		ActualizacionDatosCommand command= new ActualizacionDatosCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
    	if(command.getCodUsuario()!=null)
    	{ CargarDatosAlumno(command);
    	}
    	
    	List lista= new ArrayList();
    	lista=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_DOCUMENTOS
  				, "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista!=null)
    		command.setListaTipoDocumento(lista);
    	
    	log.info("formBackingObject:FIN");
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	ActualizacionDatosCommand control= (ActualizacionDatosCommand) command;
    	String resultado="";
    	
    	if(control.getOperacion().equals("GUARDAR"))
    	{
    		resultado=Guardar(control);
    		if(!resultado.equals("0")) control.setMsg("ERROR");
    		else{ control.setMsg("OK");
    			  
    		}
    	}
    	    	
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/actualizacionDatos/enc_actualizacion_datos","control",control);		
    }
    public String Guardar(ActualizacionDatosCommand control){
    	String resultado="";
    	EncuestaAlumno encuestaAlumno= new EncuestaAlumno();
    	    			
    	encuestaAlumno.setPrimerNombre(control.getPrimerNombre());
    	encuestaAlumno.setSegundoNombre(control.getSegundoNombre());
    	encuestaAlumno.setApellidoMaterno(control.getApellidoMaterno());
    	encuestaAlumno.setApellidoPaterno(control.getApellidoPaterno());
    	encuestaAlumno.setCodTipoDoc(control.getCodTipoDocumento());
    	encuestaAlumno.setNroTipoDoc(control.getNroDocumento());
    	encuestaAlumno.setEmail(control.getEmail());
    	encuestaAlumno.setDireccion(control.getDomicilio());
    	encuestaAlumno.setTelfCasa(control.getTelefonoDomicilio());
    	encuestaAlumno.setTelfCelular(control.getTelefonoMovil());
    	encuestaAlumno.setCodEmpresa(control.getCodEmpresa());
    	encuestaAlumno.setNombreArea(control.getArea());
    	encuestaAlumno.setNombreCargo(control.getCargo());
    	encuestaAlumno.setCorreoEmpresa(control.getEmailLaboral());
    	encuestaAlumno.setNombreContacto("Heyddy");
    	encuestaAlumno.setTelfContacto(control.getTelefonoContacto());
    	
    	resultado=this.actualizacionDatosManager.UpdateEncuestaAlumno(control.getCodUsuario(), 
    			encuestaAlumno, control.getCodUsuario());
    	System.out.println("Resultado: "+resultado);
    	return resultado;
    }
    
   public void CargarDatosAlumno(ActualizacionDatosCommand control){
	   List alumno= new ArrayList();
	   System.out.println("CodUsuario: "+control.getCodUsuario());
   	   alumno=this.actualizacionDatosManager.GetAllEncuestaAlumno(control.getCodUsuario());
   	   if(alumno!=null)
   	   { if(alumno.size()>0)
   	   	  {  EncuestaAlumno encuestaAlumno=new EncuestaAlumno();
   	         encuestaAlumno=(EncuestaAlumno)alumno.get(0);
	   		 control.setPrimerNombre(encuestaAlumno.getPrimerNombre());
	   		 control.setSegundoNombre(encuestaAlumno.getSegundoNombre());
	   		 control.setApellidoMaterno(encuestaAlumno.getApellidoMaterno());
	   		 control.setApellidoPaterno(encuestaAlumno.getApellidoPaterno());
	   		 control.setCodTipoDocumento(encuestaAlumno.getCodTipoDoc());
	   		 control.setNroDocumento(encuestaAlumno.getNroTipoDoc());
	   		 control.setEmail(encuestaAlumno.getEmail());
	   		 control.setDomicilio(encuestaAlumno.getDireccion());
	   		 control.setTelefonoDomicilio(encuestaAlumno.getTelfCasa());
	   		 control.setTelefonoMovil(encuestaAlumno.getTelfCelular());
	   		 control.setCodEmpresa(encuestaAlumno.getCodEmpresa());
	   		 control.setArea(encuestaAlumno.getNombreArea());
	   		 control.setCargo(encuestaAlumno.getNombreCargo());
	   		 control.setEmailLaboral(encuestaAlumno.getCorreoEmpresa());
	   		 control.setTelefonoContacto(encuestaAlumno.getTelfContacto());
	   		 BuscarBandeja(control);
   	   	  }
   	   }
   }
   
   	public void BuscarBandeja(ActualizacionDatosCommand control){
    	
   		List lista= new ArrayList();
    	lista=this.actualizacionDatosManager.GetAllEncuestaEmpresa("");
    	EncuestaEmpresa encuestaEmpresa= new EncuestaEmpresa();
    	if(lista!=null)
    	{ if(lista.size()>0)
    		{ for(int i=0;i<lista.size();i++)
    		  {encuestaEmpresa=(EncuestaEmpresa)lista.get(i);
    		   if(encuestaEmpresa.getCodEmpresa().equals(control.getCodEmpresa()))
    		   	  { control.setEmpresa(encuestaEmpresa.getNombreEmpresa());
    			    i=lista.size();
    		   	  }
    		  }
    		}
    	}
    	
    }
	   
   
   
}
