package com.tecsup.SGA.web.encuestas.controller;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;

public class AplicacionSeguimientoRegistroManualOpcionesFormController  implements Controller {

	 
	private static Log log = LogFactory.getLog(AplicacionSeguimientoRegistroManualOpcionesFormController.class);
	
	ConfiguracionGeneracionManager configuracionGeneracionManager;

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	log.info("handleRequest:INI");
  
    	String pagina = "/encuestas/aplicacionSeguimiento/enc_registro_manual_opciones";
    	ModelMap model = new ModelMap();
    	String resp = "";   	
    	
    	if("GRABAR".equalsIgnoreCase(request.getParameter("operacion"))){   			
    			
    			log.info("0>>"+request.getParameter("txhCodigo")+">>");    			
    			log.info("1>>"+request.getParameter("tknCodPreguntaCerrada")+">>");
    			log.info("2>>"+request.getParameter("tknCodRespuestaCerrada")+">>");
    			log.info("3>>"+request.getParameter("tknNumPregCerrada")+">>");
    			log.info("4>>"+request.getParameter("tknCodPreguntaAbierta")+">>");
    			log.info("5>>"+request.getParameter("tknCodRespuestaAbierta")+">>");
    			log.info("6>>"+request.getParameter("tknNumPregAbierta")+">>");
    			log.info("7>>"+request.getParameter("txhUsuario")+">>");
    			
    			resp = 	configuracionGeneracionManager.insertAgregarFormatoManual(
    				request.getParameter("txhCodigo"),
    				request.getParameter("tknCodPreguntaCerrada"),
    				request.getParameter("tknCodRespuestaCerrada"),
    				request.getParameter("tknNumPregCerrada"),
    				request.getParameter("tknCodPreguntaAbierta"),
    				request.getParameter("tknCodRespuestaAbierta"),
    				request.getParameter("tknNumPregAbierta"),
    				request.getParameter("txhUsuario"),
    				"");
    		
    				log.info("resp>>"+resp+">>");
    				request.setAttribute("refresh","true");			
		
    				if( "0".equalsIgnoreCase(resp) ){
    					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);    		
    				}else if( "-1".equalsIgnoreCase(resp) ){
    					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    				}else{    					
    					StringTokenizer tkn = new StringTokenizer(resp,"|");
    					String msg1 = ""; String msg2 = "";
    					msg1 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
    					msg2 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
    					log.info("msg1>"+msg1+">");log.info("msg2>"+msg2+">");
    					request.setAttribute("mensaje",msg2);
    				}			
    			
    	}else if("ACTUALIZAR".equalsIgnoreCase(request.getParameter("operacion"))){
    		
			log.info("0>>"+request.getParameter("txhCodigo")+">>");    			
			log.info("1>>"+request.getParameter("tknCodPreguntaCerrada")+">>");
			log.info("2>>"+request.getParameter("tknCodRespuestaCerrada")+">>");
			log.info("3>>"+request.getParameter("tknNumPregCerrada")+">>");
			log.info("4>>"+request.getParameter("tknCodPreguntaAbierta")+">>");
			log.info("5>>"+request.getParameter("tknCodRespuestaAbierta")+">>");
			log.info("6>>"+request.getParameter("tknNumPregAbierta")+">>");
			log.info("7>>"+request.getParameter("txhUsuario")+">>");
			log.info("8>>"+request.getParameter("txhCodEncuestado")+">>");
			
			
			resp = 	configuracionGeneracionManager.insertAgregarFormatoManual(
				request.getParameter("txhCodigo"),
				request.getParameter("tknCodPreguntaCerrada"),
				request.getParameter("tknCodRespuestaCerrada"),
				request.getParameter("tknNumPregCerrada"),
				request.getParameter("tknCodPreguntaAbierta"),
				request.getParameter("tknCodRespuestaAbierta"),
				request.getParameter("tknNumPregAbierta"),
				request.getParameter("txhUsuario"),
				request.getParameter("txhCodEncuestado")
				);log.info("resp>>"+resp+">>");
					
			request.setAttribute("refresh","true");
			
			if( "0".equalsIgnoreCase(resp) ){
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);    		
			}else if( "-1".equalsIgnoreCase(resp) ){
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			}else{    					
				StringTokenizer tkn = new StringTokenizer(resp,"|");
				String msg1 = ""; String msg2 = "";
				msg1 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
				msg2 = tkn.hasMoreTokens()==true?tkn.nextToken():"";
				log.info("msg1>"+msg1+">");log.info("msg2>"+msg2+">");
				request.setAttribute("mensaje",msg2);
			}		
		
    	}
    	
    	model.addObject("flagMultiple",request.getParameter("flagMultiple"));
    	model.addObject("flagObligatorio",request.getParameter("flagObligatorio"));
    	model.addObject("txhCodigo",request.getParameter("txhCodigo"));
    	model.addObject("txhUsuario",request.getParameter("txhUsuario"));
    	
    	List lstResultado = configuracionGeneracionManager.cargaFormularioEncuesta(request.getParameter("txhCodigo"));
    	model.addObject("lstResultado",lstResultado);
    	
    	log.info("handleRequest:FIN");
		return new ModelAndView(pagina, "model", model);
	}

}
