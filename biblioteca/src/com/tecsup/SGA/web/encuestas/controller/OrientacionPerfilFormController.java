package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.OrientacionPerfilCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.RegistrarPerfilesCommand;



public class OrientacionPerfilFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(OrientacionPerfilFormController.class);
	
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}

	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	
    	OrientacionPerfilCommand command = new OrientacionPerfilCommand();
    	
    	String codEncuesta = request.getParameter("txhCodEncuesta");
    	String setCodUsuario = request.getParameter("txhCodUsuario");
    	String setCodPerfilEncuesta = request.getParameter("txhCodPerfilEncuesta");
    	String codTipoAplicacion_1 = request.getParameter("txhCodTipoAplicacion");

//TEST    	
    	log.info( "codEncuesta= "+codEncuesta );
    	log.info( "setCodUsuario= "+setCodUsuario );
    	log.info( "setCodPerfilEncuesta= "+setCodPerfilEncuesta );
    	log.info( "codTipoAplicacion_1= "+codTipoAplicacion_1 );
//    	
    	
    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
    	command.setCodPerfilEncuesta(request.getParameter("txhCodPerfilEncuesta") == null ? "" : request.getParameter("txhCodPerfilEncuesta"));    	
    	String codTipoAplicacion=request.getParameter("txhCodTipoAplicacion") == null ? "" : request.getParameter("txhCodTipoAplicacion");
    	
    	log.info("[ codTipoAplicacion ] " + codTipoAplicacion);
    	
    	if(codTipoAplicacion.equals(CommonConstants.ENCUESTA_PROGRAMA)){    		
    		cargarBandeja(command);
    	}else{
    		log.info("No es una encuesta tipo Programa !, es de tipo servicio:  " + codTipoAplicacion);
    	}
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	OrientacionPerfilCommand control = (OrientacionPerfilCommand) command;
    	String resultado="";
		
    	if(control.getOperacion().equals("SECCION")){    		
    		CargarListaAmbitoGeneral(control,control.getCodPerfilSeleccion());
    	}
    	else
    	if(control.getOperacion().equals("QUITAR")){
    		resultado=eliminarRegistro(control);
    		if(resultado.equals("0")){    			
    			control.setMsg("OK_QUITAR");    			
    			getTotalEncuestados(control);
    		}
    		else
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_QUITAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
    		cargarBandeja(control);
    	}
		
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_orientacion_perfil","control",control);		
    }
    private void cargarBandeja(OrientacionPerfilCommand control){
    	String codPerfil="";
    	StringTokenizer str = new StringTokenizer(control.getCodPerfilEncuesta(),"|");
    	
    	codPerfil=str.nextToken();
    	log.info("Nuevo valor codPerfil = "+codPerfil);
    	
    	if(codPerfil.equals(CommonConstants.PERFIL_PFR)){
    		List lista = this.configuracionPerfilManager.GetAllProfesorPFR(control.getCodEncuesta(), codPerfil);
        	if(lista!=null){        		
        		if(lista.size()>0){
        			control.setListaProfesoresPFR(lista);
        			control.setTamListaProfesoresPFR(""+lista.size());
        		}
        		else{
        			control.setListaProfesoresPFR(null);
        			control.setTamListaProfesoresPFR("0");
        			
        			log.info("::: NO TENGO LISTA PARA MOSTRAR, LO SIENTO !  - PFR (0):::: ");
        			
        		}
        		control.setTamMostrar(control.getTamListaProfesoresPFR());
        	}
        	else{
        		log.info("::: PFR (1) :::: ");
        	}
    	}
    	else
    	if(codPerfil.equals(CommonConstants.PERFIL_PCC)){
    		List lista = this.configuracionPerfilManager.GetAllProfesorPCC(control.getCodEncuesta(), codPerfil);
        	if(lista!=null){        		
        		if(lista.size()>0){
        			control.setListaProfesoresPCC(lista);
        			control.setTamListaProfesoresPCC(""+lista.size());
        		}
        		else{
        			control.setListaProfesoresPCC(null);
        			control.setTamListaProfesoresPCC("0");
        		}
        		control.setTamMostrar(control.getTamListaProfesoresPCC());
        	}
        	else{
        		log.info("::: NO TENGO LISTA PARA MOSTRAR, LO SIENTO !  - PCC :::: ");
        	}
    	}
    	else{
    		CargarListaAmbitoGeneral(control,codPerfil);
    	}
    }
    private void CargarListaAmbitoGeneral(OrientacionPerfilCommand control, String codPerfil){    	
    	List lista = this.configuracionPerfilManager.GetAllAmbitoGeneral(control.getCodEncuesta(), codPerfil);
    	if(lista!=null){    		
    		if(lista.size()>0){
    			control.setListaGeneral(lista);
    			control.setTamListaGeneral(""+lista.size());
    		}
    		else{
    			control.setListaGeneral(null);
    			control.setTamListaGeneral("0");
    		}
    		control.setTamMostrar(control.getTamListaGeneral());
    	}
    }
    private String eliminarRegistro(OrientacionPerfilCommand control){
    	try{
    		String resultado="";
    		
	    	String codPerfil="";
	    	StringTokenizer str = new StringTokenizer(control.getCodPerfilEncuesta(),"|");
	    	codPerfil=str.nextToken();
	    	System.out.println("codEncuesta>>"+control.getCodEncuesta()+"<<");
	    	System.out.println("codPerfil>>"+codPerfil+"<<");
	    	System.out.println("codProfesor>>"+control.getCodProfesor()+"<<");
	    	System.out.println("codProducto>>"+control.getCodProducto()+"<<");
	    	System.out.println("codPrograma>>"+control.getCodPrograma()+"<<");
	    	System.out.println("codCentroCosto>>"+control.getCodCentroCosto()+"<<");
	    	System.out.println("codCurso>>"+control.getCodCurso()+"<<");
	    	System.out.println("codCiclo>>"+control.getCodCiclo()+"<<");
	    	System.out.println("codSeccion>>"+control.getCodSeccion()+"<<");
	    	System.out.println("codUsuario>>"+control.getCodUsuario()+"<<");
	    	
	    	resultado = this.configuracionPerfilManager.deleteAmbitoProfesor(control.getCodEncuesta(), codPerfil, 
	    			control.getCodProfesor(), control.getCodProducto(), control.getCodPrograma(), 
	    			control.getCodCentroCosto(), control.getCodCurso(), control.getCodCiclo(), 
	    			control.getCodSeccion(), control.getCodUsuario());    	
	    	
	    	return resultado;
	    }
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
    }
    private void getTotalEncuestados(OrientacionPerfilCommand control){
    	List lista = this.configuracionGeneracionManager.getAllFormato("", 
    			"", "", "", control.getCodEncuesta(), "", "");
    	if(lista != null && lista.size()>0){
			FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);
			control.setTotalEncuestados(obj.getTotalEncuestados());
			control.setDscEncuestados(obj.getDscEncuestados());
		}    	
    }
    private void AsignarMensaje(OrientacionPerfilCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_QUITAR");
		}		
    }
}