package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.web.encuestas.command.BusquedaEmpleadosIframeCommand;

public class BusquedaEmpleadosIframeFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(BusquedaEmpleadosIframeFormController.class);
	
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
		
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request) throws ServletException {
		log.info("formBackingObject:INI");
	log.info("formBackingObject:FIN");
    
	BusquedaEmpleadosIframeCommand command= new BusquedaEmpleadosIframeCommand();
	
	command.setCodTipoPersonal((String)request.getParameter("txhCodTipoPersonal"));
	command.setNombre((String)request.getParameter("txhNombre"));
	command.setApellidoPaterno((String)request.getParameter("txhApellidoPaterno"));
	command.setApellidoMaterno((String)request.getParameter("txhApellidoMaterno"));
	
	if(request.getParameter("txhCodTipoPersonal")!=null)
	BuscarBandeja(command, request);
	else request.getSession().setAttribute("listaBandejadatosPersonal", new ArrayList());
    return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	BusquedaEmpleadosIframeCommand control= (BusquedaEmpleadosIframeCommand) command;
    	
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/configuracionGeneracion/enc_busqueda_empleados_iframe","control",control);		
    }
    
    public void BuscarBandeja(BusquedaEmpleadosIframeCommand control, HttpServletRequest request){
    	List lista= new ArrayList();
    	if(control.getCodTipoPersonal().equals("-1"))
    		control.setCodTipoPersonal("");
    	lista=this.configuracionGeneracionManager.GetAllDatosPersonal(control.getNombre(),"", control.getApellidoPaterno(), 
    			control.getApellidoMaterno(), control.getCodTipoPersonal());
    	if(lista!=null)
    		request.getSession().setAttribute("listaBandejadatosPersonal", lista);
    	else request.getSession().setAttribute("listaBandejadatosPersonal", new ArrayList());
    }
}
