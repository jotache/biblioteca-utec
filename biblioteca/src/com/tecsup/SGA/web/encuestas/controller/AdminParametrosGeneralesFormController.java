package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AdminParametrosGeneralesCommand;
import com.tecsup.SGA.web.encuestas.command.AdminTipoFormatoAgregarCommand;
import com.tecsup.SGA.web.encuestas.command.AdminTipoFormatoCommand;

public class AdminParametrosGeneralesFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminParametrosGeneralesFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		AdminParametrosGeneralesCommand command= new AdminParametrosGeneralesCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
    	List lista= new ArrayList();
    	lista=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ENCUESTA_PARAM_GENERALES, "", 
    			"", "",	"", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista!=null){
    		TipoTablaDetalle obj;
    		if(lista.size()==1){
				obj=(TipoTablaDetalle)lista.get(0);
    			command.setNroDias(obj.getDscValor1());
    		}
    	}
		
    	log.info("formBackingObject:FIN");
    	
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AdminParametrosGeneralesCommand control= (AdminParametrosGeneralesCommand) command;
    	String resultado="";
    	
    	if(control.getOperacion().equals("GRABAR")){
    		resultado = UpdateTablaDetalle(control);
    		if(!resultado.equals("")){
    			control.setMsg("OK_GRABAR");
    		}
    		else{
    			control.setMsg("ERROR_GRABAR");
    		}
    	}
    	log.info("onSubmit:FIN");
        return new ModelAndView("/encuestas/administracionSistema/enc_parametros_generales","control",control);		
    }    
    
    private String UpdateTablaDetalle(AdminParametrosGeneralesCommand control){
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		String resultado;
    		
    		obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_ENCUESTA_PARAM_GENERALES);
    		obj.setDscValor1(control.getNroDias());    				
    		obj.setCodDetalle(CommonConstants.COD_DETALLE_PARAM_GEN_ENC);
    		//System.out.println("-->"+obj.getDscValor1());
    		resultado=this.tablaDetalleManager.UpdateTablaDetalle(obj, control.getCodUsuario());
    		//resultado=this.tablaDetalleManager.UpdateTablaDetalle(obj, control.getCodUsuario());
    		
    		return resultado;
    	}
    	catch(Exception ex){
			ex.printStackTrace();
		}
    	return null;
    }
}
