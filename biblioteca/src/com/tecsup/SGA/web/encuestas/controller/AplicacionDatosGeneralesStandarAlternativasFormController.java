package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.MetodosConstants;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesAlternativasCommand;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;




public class AplicacionDatosGeneralesStandarAlternativasFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicacionDatosGeneralesStandarAlternativasFormController.class);
	//CotBienesYServiciosManager cotBienesYServiciosManager;
	ConfiguracionGeneracionManager configuracionGeneracionManager;
	

	public AplicacionDatosGeneralesStandarAlternativasFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesAlternativasCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesAlternativasCommand control = new AplicacionDatosGeneralesAlternativasCommand();    	
    	
    	control = ini(control);
    	
    	if(request.getParameter("prmCodEncuesta")!=null )
    	{	control.setTxhCodigo(request.getParameter("prmCodEncuesta"));    		
    		control.setUsuario(request.getParameter("prmUsuario"));
    		control.setTxhCodEstado(request.getParameter("prmCodEstado"));
    		control = irConsultar(control); 
    		//control.setTxtPeso("1");
    	}
    	
        log.info("formBackingObject:FIN");
        return control;
    }

	private AplicacionDatosGeneralesAlternativasCommand ini(AplicacionDatosGeneralesAlternativasCommand control) {				
		
		control = irConsultar(control);
		return control;
	}
	

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	String resp = "";
    	
    	AplicacionDatosGeneralesAlternativasCommand control = (AplicacionDatosGeneralesAlternativasCommand) command;		
		log.info("operacion|"+control.getOperacion()+"|");
		
		if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
			 
			if(request.getParameter("txhCodigoUpdate")!=null && !request.getParameter("txhCodigoUpdate").trim().equalsIgnoreCase("") )
			{
				resp = irActualizar(control,request.getParameter("txhCodigoUpdate"));			
				log.info("RPUESTA:"+resp+":");
				if("0".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO+ MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()) );    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_EXISTE_ALTER_O_DESCRIPCION);    		
				}else if("-3".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		
				}else{
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}
			}
			else 
			{	
				resp = irRegistrar(control);    		
				log.info("RPUESTA:"+resp+":");
				
				if(resp==null || resp.equals("")) {
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}else if(Integer.parseInt(resp)>0){
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO+ MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()) );    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_EXISTE_ALTER_O_DESCRIPCION);    		
				}else if("-3".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		
				}else{
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}
    		
			}
			
		}else if("irEliminar".equalsIgnoreCase(control.getOperacion())){
			
				resp = irEliminar(control,request.getParameter("txhCodigoSel"));
				
				if("0".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ELIMINAR_EXITO+ MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()) );    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		
				}else {
					request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);
				}
		}
			
		control = irConsultar(control);
		clearForm(control);
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_datos_generales_standar_alternativas","control",control);		
    }

		private void clearForm(
				AplicacionDatosGeneralesAlternativasCommand control) {
			control.setTxtAlternativa("");
			control.setTxtDescripcion("");
			control.setTxtPeso("");
			
		}

		private String irEliminar(
				AplicacionDatosGeneralesAlternativasCommand control,
				String idEliminacion) {
			String str = ""; 
			
			log.info("1.>"+control.getTxhCodigo()+">");
			log.info("2.>"+idEliminacion+">");
			log.info("3.>"+control.getUsuario()+">");
			
			str = configuracionGeneracionManager.deleteAlternativa(
					control.getTxhCodigo(),
					idEliminacion,
					control.getUsuario());
			return str;
		}

		private String irRegistrar(AplicacionDatosGeneralesAlternativasCommand control){
			String str = "";
				/*log.info("1.>"+control.getTxhCodigo()+">");
				log.info("2.>null>");
				log.info("3.>null>");
				log.info("4.>"+control.getTxtAlternativa()+">");
				log.info("5.>"+control.getTxtDescripcion()+">");
				log.info("6.>"+control.getTxtPeso()+">");
				log.info("7.>"+control.getUsuario()+">");*/
				
			str = configuracionGeneracionManager.insertAlternativa(
					control.getTxhCodigo(),
					null,
					null,
					control.getTxtAlternativa(),
					control.getTxtDescripcion(),
					control.getTxtPeso(),
					control.getUsuario()
					);
			return str;
		}

		private String irActualizar(AplicacionDatosGeneralesAlternativasCommand control, String codigoUpdate){
			String str = "";
				/*log.info("1.>"+control.getTxhCodigo()+">");
				log.info("2.>"+codigoUpdate+">");
				log.info("3.>"+">");
				log.info("4.>"+">");
				log.info("5.>"+control.getTxtAlternativa()+">");
				log.info("6.>"+control.getTxtDescripcion()+">");
				log.info("7.>"+control.getTxtPeso()+">");
				log.info("8.>"+control.getUsuario()+">");*/
			
			str = configuracionGeneracionManager.updateAlternativa(
					control.getTxhCodigo(),
					codigoUpdate,
					"",
					"",
					control.getTxtAlternativa(),
					control.getTxtDescripcion(),
					control.getTxtPeso(),
					control.getUsuario()
					);
			
			
			return str;
			
		}

		public void setConfiguracionGeneracionManager(
				ConfiguracionGeneracionManager configuracionGeneracionManager) {
			this.configuracionGeneracionManager = configuracionGeneracionManager;
		}
		
		private AplicacionDatosGeneralesAlternativasCommand irConsultar(
				AplicacionDatosGeneralesAlternativasCommand control) {
			
			log.info("BUSQUEDA DE ALTERNATIVAS CON CODIGO>>"+control.getTxhCodigo()+">>");
			control.setLstResultado(configuracionGeneracionManager.getAllAlternativa(control.getTxhCodigo(),null,null,null));			
			return control;
		}

		
}
