package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Empleados;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.PerfilAlumnoPFRCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilPersonalTecsupCommand;
import com.tecsup.SGA.web.encuestas.command.PerfilProgramacionCommand;
import com.tecsup.SGA.web.encuestas.command.RegistrarPerfilesCommand;



public class PerfilAlumnoPFRFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(PerfilAlumnoPFRFormController.class);
	
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}
	
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	PerfilAlumnoPFRCommand command = new PerfilAlumnoPFRCommand();
    	//************************************************************
    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));
    	command.setCodTipoEncuesta(request.getParameter("txhCodTipoAplicacion") == null ? "" : request.getParameter("txhCodTipoAplicacion"));
    	//************************************************************
    	//ASIGNACION DE CONSTANTES
    	command.setEncuestaPrograma(CommonConstants.ENCUESTA_PROGRAMA);
    	command.setEncuestaSeccion(CommonConstants.ENCUESTA_SECCION);
    	command.setPerfilPFR(CommonConstants.PERFIL_PFR);
    	//************************************************************
    	CargarData(command);
    	//SE EVALUA QUE LA ENCUESTA TENGA UN PERFIL
    	String codPerfil=request.getParameter("txhCodPerfilEncuesta") == null ? "" : request.getParameter("txhCodPerfilEncuesta");
    	//ENTRA CUANDO LA ENCUESTA YA TIENE UNO O VARIOS PERFILES GUARDADOS
    	if(!codPerfil.equals("")){    		
    		StringTokenizer tkm = new StringTokenizer(codPerfil,"|");
    		String codigoPerfil="";
    		while(tkm.hasMoreTokens()){
    			codigoPerfil=tkm.nextToken();
    			if(codigoPerfil.equals(CommonConstants.PERFIL_PFR)){    				
    				GetPerfilEncuesta(command,codigoPerfil);
    			}
    		}
    	}
    	//****************************************************
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	PerfilAlumnoPFRCommand control = (PerfilAlumnoPFRCommand) command;		
    	String resultado = "";
    	
    	if (control.getOperacion().trim().equals("PROGRAMA")){
    		CargarCiclo(control);
    	}
    	else
    	if (control.getOperacion().trim().equals("BUSCAR_CURSOS")){
    		CargarCiclo(control);
    		CargarCursos(control);
    		control.setListaCursoSel(null);
    	}
    	else
    	if (control.getOperacion().trim().equals("GRABAR")){    		
    		CargarCiclo(control);
    		CargarCursos(control);
    		resultado=InsertPerfilEncuesta(control);
    		if(resultado.equals("0")){
    			control.setMsg("OK_GRABAR");
    			cargaPerfiles(control);
    			getTotalEncuestados(control);
    		}
    		else
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}
    		ObtenerListas(control);
    	}    	
    		
    	CargarData(control);
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_perfil_alumno_PFR","control",control);		
    }
    private void CargarData(PerfilAlumnoPFRCommand control){
	   CargarCombos(control);	   
    }    
    private void CargarCombos(PerfilAlumnoPFRCommand control){
    	//lista programa
    	control.setListaPrograma(this.configuracionPerfilManager.GetAllProgramas(CommonConstants.COD_PRODUCTO_PFR, "", ""));    			
    	//carga lista de la bandeja de departamento 
    	control.setListaDepartamento(this.configuracionPerfilManager.GetAllDepartamentos("", ""));    			
    	control.setTamListaDepartamento(""+control.getListaDepartamento().size());
    }    
    private void CargarCiclo(PerfilAlumnoPFRCommand control){    	
    	control.setListaCiclo(this.configuracionPerfilManager.GetAllCiclos(control.getCodPrograma(), ""));
    	control.setTamListaCiclo(""+control.getListaCiclo().size());
    }
    private void CargarCursos(PerfilAlumnoPFRCommand control){
    	control.setListaCurso(this.configuracionPerfilManager.GetAllCursos("", "", CommonConstants.COD_PRODUCTO_PFR, control.getCodPrograma(), control.getCadenaCodDepartamentos(), control.getCadenaCodCiclos()));
    }    
    private String InsertPerfilEncuesta(PerfilAlumnoPFRCommand control){
    	try {
			PerfilEncuesta perfil = new PerfilEncuesta();
			
			perfil.setCodTipoEncuesta(control.getCodTipoEncuesta());
			
			perfil.setCodEncuesta(control.getCodEncuesta());
			//perfil.setCodEncuesta("83");
			
			perfil.setCodPerfil(CommonConstants.PERFIL_PFR);
			perfil.setCodProducto(CommonConstants.COD_PRODUCTO_PFR);
			perfil.setCodPrograma(control.getCodPrograma());
			perfil.setCodCurso(control.getCadenaCodCursos());
			perfil.setCodDepartamento(control.getCadenaCodDepartamentos());
			perfil.setCodCiclo(control.getCadenaCodCiclos());
			perfil.setCodTipoPersonal("");
			perfil.setCodTipoDocente("");
			perfil.setAñoIniEgresado("");
			perfil.setAñoFinEgresado("");
			System.out.println("CodTipoEncuesta>>"+control.getCodTipoEncuesta()+"<<");
			System.out.println("CodEncuesta>>"+control.getCodEncuesta()+"<<");
			System.out.println("CodPerfil>>"+CommonConstants.PERFIL_PFR+"<<");
			System.out.println("CodProducto>>"+CommonConstants.COD_PRODUCTO_PFR+"<<");
			System.out.println("CodPrograma>>"+control.getCodPrograma()+"<<");
			System.out.println("CadenaCodCursos>>"+control.getCadenaCodCursos()+"<<");
			System.out.println("CadenaCodDepartamentos>>"+control.getCadenaCodDepartamentos()+"<<");
			System.out.println("CadenaCodCiclos>>"+control.getCadenaCodCiclos()+"<<");			
			System.out.println("getCodUsuario>>"+control.getCodUsuario()+"<<");
			
			String resultado = this.configuracionPerfilManager.InsertPerfilEncuesta(perfil, control.getCodUsuario());
			return resultado;
		}
    	catch (Exception e) {
			e.printStackTrace();			
		}
    	return null;
    }
    private void GetPerfilEncuesta(PerfilAlumnoPFRCommand control, String codPerfil){
    	List lista=this.configuracionPerfilManager.GetAllPerfilEncuesta(control.getCodEncuesta(), codPerfil);
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			obj = (PerfilEncuesta)lista.get(0);
    			
    			control.setCodPrograma(obj.getCodPrograma());    			
    			control.setCadenaCodCiclos(obj.getCodCiclo());
    			control.setCadenaCodDepartamentos(obj.getCodDepartamento());
    			control.setCadenaCodCursos(obj.getCodCurso());
    			//SE CARGAN LOS CICLOS DEACUERDO AL CODPROGRAMA OBTENIDO
    			CargarCiclo(control);    			
        		CargarCursos(control);
    			control.setIndCiclo("1");
    			ObtenerListas(control);
    		}
    	}
    }
    private void ObtenerListas(PerfilAlumnoPFRCommand control){
    	List listaPersonalDis = control.getListaCurso();
    	String cadenaCodSel = control.getCadenaCodCursos();
    	
    	List listaDisponibles = null;
    	List listaSeleccionados = null;
    	
    	StringTokenizer srt=new StringTokenizer(cadenaCodSel,"|");
    	String codigoSel="";
    	listaSeleccionados = new ArrayList();
    	while(srt.hasMoreTokens()){
    		
    		codigoSel=srt.nextToken();
    		System.out.println(">>"+codigoSel+"<<");
    		Curso emp1 = new Curso();
    		
    		for(int i=0; i<listaPersonalDis.size(); i++){
    			
    			emp1 = (Curso)listaPersonalDis.get(i);
    			String cod=emp1.getCodCurso();    			
    			
    			if(cod.equals(codigoSel)){    			
    				listaSeleccionados.add(emp1);
    				listaPersonalDis.remove(i);
    				i=listaPersonalDis.size();
    			}
    		}
    	}
    	if(listaSeleccionados!=null){    		
			control.setListaCurso(listaPersonalDis);
			control.setListaCursoSel(listaSeleccionados);    		
    	}
    }
    private void getTotalEncuestados(PerfilAlumnoPFRCommand control){
    	List lista = this.configuracionGeneracionManager.getAllFormato("", 
    			"", "", "", control.getCodEncuesta(), "", "");
    	if(lista != null && lista.size()>0){
			FormatoEncuesta obj = (FormatoEncuesta)lista.get(0);			
			
			control.setTotalEncuestados(obj.getTotalEncuestados());
			control.setDscEncuestados(obj.getDscEncuestados());
		}    	
    }
    private void cargaPerfiles(PerfilAlumnoPFRCommand control){
    	List lista = this.configuracionPerfilManager.GetAllPerfilesByEncuesta(control.getCodEncuesta());
    	if(lista!=null){
    		if(lista.size()>0){
    			PerfilEncuesta obj = new PerfilEncuesta();
    			String cadenaPerfiles="";
    			for(int i=0;i<lista.size();i++){
    				obj = (PerfilEncuesta)lista.get(i);
    				if(i==0){
    					cadenaPerfiles = obj.getCodPerfil() + "|";
    				}
    				else{
    					cadenaPerfiles = cadenaPerfiles + obj.getCodPerfil() + "|";
    				}    				
    			}
    			control.setCodPerfilEncuesta(cadenaPerfiles);    			
    		}
    	}
    }
    private void AsignarMensaje(PerfilAlumnoPFRCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_GRABAR");
		}		
    }
}
