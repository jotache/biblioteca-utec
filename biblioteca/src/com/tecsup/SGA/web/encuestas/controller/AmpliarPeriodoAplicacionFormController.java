package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AmpliarPeriodoAplicacionCommand;
import com.tecsup.SGA.web.encuestas.command.AplicarProgramacionEncuestaCommand;

public class AmpliarPeriodoAplicacionFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AmpliarPeriodoAplicacionFormController.class);
	private ConfiguracionPerfilManager configuracionPerfilManager;
	
	public void setConfiguracionPerfilManager(
			ConfiguracionPerfilManager configuracionPerfilManager) {
		this.configuracionPerfilManager = configuracionPerfilManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		AmpliarPeriodoAplicacionCommand command= new AmpliarPeriodoAplicacionCommand();
		
		command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));    	
    	
    	CargarListaHoras(command);
    	CargarFechaHoraInicial(command);
    	
		log.info("formBackingObject:FIN");
    	
		return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AmpliarPeriodoAplicacionCommand control= (AmpliarPeriodoAplicacionCommand) command;
    	String resultado;
    	
    	if(control.getOperacion().equals("GRABAR")){
    		resultado=AmpliarProgramacionEncuesta(control);
    		System.out.println("Resultado de grabar>>"+resultado+"<<");
    		
    		if(resultado.equals("0")){
    			control.setMsg("OK_GRABAR");    			
    		}
    		else
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{    			
    			AsignarMensaje(control,resultado);
    		}	
    	}
    	
    	log.info("onSubmit:FIN");
        
    	return new ModelAndView("/encuestas/aplicacionSeguimiento/enc_ampliar_periodo_aplicacion","control",control);		
    }
    private void CargarListaHoras(AmpliarPeriodoAplicacionCommand control){
		control.setListaHoraFin(Anios.getHoras(CommonConstants.HORA_INICIAL_DIA, 
				CommonConstants.HORA_FINAL_DIA));		
    }
    private void CargarFechaHoraInicial(AmpliarPeriodoAplicacionCommand control){		
    	List lista=this.configuracionPerfilManager.GetAmpliarProgByEncuesta(control.getCodEncuesta());
    	if(lista!=null){
    		if(lista.size()==1){
    			FormatoEncuesta obj=(FormatoEncuesta)lista.get(0);
    			
    			control.setFechaVigenciaIni(obj.getFecIniAplicacion());
    			control.setHoraVigenciaIni(obj.getHoraIniAplicacion());
    			control.setFechaVigenciaFin(obj.getFecFinAplicacion());
    			
    			String[] valHoraFin=obj.getHoraFinAplicacion().split(":");
    			control.setHoraVigenciaFin(valHoraFin[0]);
    		}
    	}
    }
    private String AmpliarProgramacionEncuesta(AmpliarPeriodoAplicacionCommand control){
    	try {    		
    		System.out.println(">>"+control.getCodEncuesta()+"<<");
    		System.out.println(">>"+control.getFechaVigenciaIni()+"<<");
    		System.out.println(">>"+control.getHoraVigenciaIni()+"<<");
    		System.out.println(">>"+control.getFechaVigenciaFin()+"<<");
    		System.out.println(">>"+control.getHoraVigenciaFin()+":00"+"<<");
    		System.out.println(">>"+control.getCodUsuario()+"<<");    		
    		String resultado=this.configuracionPerfilManager.AmpliarProgramacionEncuesta(control.getCodEncuesta(), 
    				control.getFechaVigenciaIni(), control.getHoraVigenciaIni(), control.getFechaVigenciaFin(), 
    				control.getHoraVigenciaFin()+":00", control.getCodUsuario());
    		
    		return resultado;
		}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    	return null;
    }
    private void AsignarMensaje(AmpliarPeriodoAplicacionCommand control, String resultado){
    	StringTokenizer str=new StringTokenizer(resultado,"|");
		if(str.hasMoreTokens()){
			control.setMsg(str.nextToken());
			control.setDscMensaje(str.nextToken());
		}
		else{
			control.setMsg("ERROR_GRABAR");
		}
    }
}
