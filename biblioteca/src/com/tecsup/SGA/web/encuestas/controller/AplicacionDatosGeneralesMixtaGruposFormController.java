package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.MetodosConstants;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesMixtaGruposCommand;






public class AplicacionDatosGeneralesMixtaGruposFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicacionDatosGeneralesMixtaGruposFormController.class);

	private ConfiguracionGeneracionManager configuracionGeneracionManager;

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	public AplicacionDatosGeneralesMixtaGruposFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesMixtaGruposCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesMixtaGruposCommand control = new AplicacionDatosGeneralesMixtaGruposCommand();    	
    	
    	if(request.getParameter("prmCodEncuesta")!=null )
    	{	control.setTxhCodigo(request.getParameter("prmCodEncuesta"));
    		control.setUsuario(request.getParameter("prmUsuario"));
    		control.setTxhCodEstado(request.getParameter("prmCodEstado"));    		
    		//cargaDatosEncuesta(control);
    		control = ini(control);
    	}
    	
    	
    	cargaCombos(control);

        log.info("formBackingObject:FIN");
        return control;
    }
	
    private void clearForm(AplicacionDatosGeneralesMixtaGruposCommand control) {
    	control.setTxtGrupo("");
    	control.setTxtPeso("");
    	control.setCboFormato("");
    	control.setCboAlternativa("");
		
	}

	private String irEliminar(
			AplicacionDatosGeneralesMixtaGruposCommand control, String idEliminacion) {
    	String str = ""; 
		
		log.info("1.>"+control.getTxhCodigo()+">");
		log.info("2.>"+idEliminacion+">");
		log.info("3.>"+control.getUsuario()+">");
		str = configuracionGeneracionManager.deleteGruposByEncuesta(
				control.getTxhCodigo(),
				idEliminacion,
				control.getUsuario());
		
		return str;
	}

	private String irRegistrar(
			AplicacionDatosGeneralesMixtaGruposCommand control) {
    	String str = "";
		log.info("1.>"+control.getTxhCodigo()+">");
		log.info("2.>"+control.getCboFormato()+">");
		log.info("3.>"+control.getTxtGrupo()+">");
		log.info("4.>"+control.getTxtPeso()+">");
		log.info("5.>"+control.getCboAlternativa()+">");
		log.info("6.>"+control.getUsuario()+">");

	str = configuracionGeneracionManager.insertGruposByEncuesta(
			control.getTxhCodigo(),
			control.getCboFormato(),
			control.getTxtGrupo(), 
			control.getTxtPeso(), 
			control.getCboAlternativa(),//para mixta
			control.getUsuario());
	
	return str;
	}

	private String irActualizar(
			AplicacionDatosGeneralesMixtaGruposCommand control, String codigoUpdate) {
    	String str = "";
		log.info("1.>"+control.getTxhCodigo()+">");
		log.info("2.>"+control.getCboFormato()+">");
		log.info("3.>"+codigoUpdate+">");
		log.info("4.>"+control.getTxtGrupo()+">");
		log.info("5.>"+control.getTxtPeso()+">");
		log.info("6.>"+control.getCboAlternativa()+">");
		log.info("7.>"+control.getUsuario()+">");
		
	str = configuracionGeneracionManager.updateGruposByEncuesta(
			control.getTxhCodigo(),
			control.getCboFormato(),
			codigoUpdate,
			control.getTxtGrupo(),
			control.getTxtPeso(),
			control.getCboAlternativa(),//MIXTA
			control.getUsuario()
			);
	
	return str;
	}

	private AplicacionDatosGeneralesMixtaGruposCommand ini(
			AplicacionDatosGeneralesMixtaGruposCommand control) {
    	control = irConsultar(control);
		return control;
	}

	private AplicacionDatosGeneralesMixtaGruposCommand irConsultar(
			AplicacionDatosGeneralesMixtaGruposCommand control) {
		
		log.info("1.>>"+control.getTxhCodigo()+">>");
		log.info("2.>>"+">>");
		
		control.setLstResultado(
				configuracionGeneracionManager.getAllGruposByEncuesta(
						control.getTxhCodigo(),
						null)
				);
		
		return control;
	}

	private void cargaCombos(AplicacionDatosGeneralesMixtaGruposCommand control) {
    	control.setLstFormato(configuracionGeneracionManager.getAllFormatoEncuesta(control.getTxhCodigo()));		
	}

	private void cargaDatosEncuesta(AplicacionDatosGeneralesMixtaGruposCommand control) {
		
		
	}


	

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	AplicacionDatosGeneralesMixtaGruposCommand control = (AplicacionDatosGeneralesMixtaGruposCommand) command;		
    	String resp = "";    	
		
		log.info("operacion|"+control.getOperacion()+"|");
		
		if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
			 
			if(request.getParameter("txhCodigoUpdate")!=null && !request.getParameter("txhCodigoUpdate").trim().equalsIgnoreCase("") )
			{	log.info("irActualizar");
				log.info("txhCodigoUpdate>"+request.getParameter("txhCodigoUpdate")+">");
				resp = irActualizar(control,request.getParameter("txhCodigoUpdate"));				
			
				log.info("RPUESTA:"+resp+":");
				if("0".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO+MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()));    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_EXISTE_GRUPO_O_DESCRIPCION);    		
				}else if("-3".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		    		
				}else{
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}
				
			}
			else 
			{	log.info("irRegistrar");
				resp = irRegistrar(control);    		
				log.info("RPUESTA:"+resp+":");

				if(resp==null || resp.equals("")) {
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}else if(Integer.parseInt(resp)>0){
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO+MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()));    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_EXISTE_GRUPO_O_DESCRIPCION);    		
				}else if("-3".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		    		
				}else{
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}
				
				
    		
			}
			
		}else if("irEliminar".equalsIgnoreCase(control.getOperacion())){

				//ID A ELIMINAR		
				log.info("txhCodigoSel>"+request.getParameter("txhCodigoSel")+">");
				
				resp = irEliminar(control,request.getParameter("txhCodigoSel"));
				
				if("0".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ELIMINAR_EXITO+MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()));    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		
				}else {
					request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);
				}
		}
			
		control = irConsultar(control);		
    	cargaCombos(control);
    	clearForm(control);
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_datos_generales_mixta_grupos","control",control);		
    }
		
}
