package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.encuestas.command.MantenimientoConfiguracionEnlacesCommand;

public class MantenimientoConfiguracionEnlacesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoConfiguracionEnlacesFormController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("onSubmit:INI");
		MantenimientoConfiguracionEnlacesCommand command= new MantenimientoConfiguracionEnlacesCommand();
		
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
		log.info("onSubmit:FIN");
        return command;
    }
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	MantenimientoConfiguracionEnlacesCommand control= (MantenimientoConfiguracionEnlacesCommand) command;
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/encuesta/administracionSistema/enc_mantenimiento_configuracion_enlaces.html","control",control);		
    }
}
