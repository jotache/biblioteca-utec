package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.ConfGenBandejaEncuestasCommand;
import com.tecsup.SGA.web.encuestas.command.ConfGenBandejaEncuestasIframeCommand;

public class ReporteBusquedaEncuestasFormController extends
		SimpleFormController {

	private static Log log = LogFactory
			.getLog(ReporteBusquedaEncuestasFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	private TablaDetalleManager tablaDetalleManager;

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		log.info("formBackingObject:INI");

		ConfGenBandejaEncuestasCommand command = new ConfGenBandejaEncuestasCommand();
		// ******************************************************************
		
		if(request.getParameter("prmTipo")!=null)
		command.setTxhTipo(request.getParameter("prmTipo"));
		
		if(request.getParameter("prmInicio")!=null)
			request.getSession().removeAttribute("listaBandejaConfiguracion2");
			
		
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad) request
				.getSession().getAttribute("usuarioSeguridad");
		
		log.info(">>>>>tipoBusqueda>"+request.getParameter("prmTipo")+">");
		
		if(request.getParameter("prmTipo")!=null)
		{
			command.setTxhTipoBusqueda(request.getParameter("prmTipo"));
		}
		
		if (usuarioSeguridad != null) {
			System.out.println("usuarioSeguridad codUsuario--->>"
					+ usuarioSeguridad.getCodUsuario() + "<<");
			System.out.println("usuarioSeguridad nomUsuario--->>"
					+ usuarioSeguridad.getNomUsuario() + "<<");
			System.out.println("usuarioSeguridad getPerfiles--->>"
					+ usuarioSeguridad.getPerfiles() + "<<");
			command.setNomUsuario(usuarioSeguridad.getNomUsuario());
			command.setPerfilUsuario(usuarioSeguridad.getPerfiles());
		}
		// ******************************************************************
		if (request.getParameter("txhCodUsuario") != null) {
			command.setCodUsuario((String) request
					.getParameter("txhCodUsuario"));
			request.setAttribute("codUsuario", command.getCodUsuario());
			System.out.println("ParBandera: "
					+ request.getParameter("txhParBandera"));
			if (request.getParameter("txhParBandera") != null) {
				command
						.setBandera(request.getParameter("txhParBandera") == null ? ""
								: request.getParameter("txhParBandera"));
				command.setCodTipoAplicacion(request
						.getParameter("txhParCodTipoAplicacion") == null ? ""
						: request.getParameter("txhParCodTipoAplicacion"));
				command.setCodTipoEncuesta(request
						.getParameter("txhParCodTipoEncuesta") == null ? ""
						: request.getParameter("txhParCodTipoEncuesta"));
				command.setCodTipoEstado(request
						.getParameter("txhParCodTipoEstado") == null ? ""
						: request.getParameter("txhParCodTipoEstado"));
				command.setCodTipoServicio(request
						.getParameter("txhParCodTipoServicio") == null ? ""
						: request.getParameter("txhParCodTipoServicio"));
				// BuscarBandeja(command, request);

				if (command.getBandera().equals("1")) {
					System.out.println("MaDFroG: " + command.getBandera()
							+ ">>codUsuario: " + command.getCodUsuario());
					request.setAttribute("iFrameBandeja", "OK");
				}

				System.out.println("CommandBandera:" + command.getBandera());
				/*
				 * if(command.getBandera().trim().equals("1"))
				 * BuscarBandeja(command, request);
				 */
			}
		}

		command.setCodUsuario((String) request.getParameter("txhCodUsuario"));
		System.out.println("codUsuario--->>" + command.getCodUsuario() + "<<");
		log.info("CODIGO_FINAL= codUsuario--->>" + command.getCodUsuario() +  " - " + command.getCodResponsable()+"  -  " +command.getNomUsuario()+ "<<");
		
		
		command
				.setConsteAplicacionPrograma(CommonConstants.COD_TIPO_APLICACION_PROGRAMAS);
		command
				.setConsteAplicacionServicio(CommonConstants.COD_TIPO_APLICACION_SERVICIO_OTROS);

		command
				.setConsteEncuestaGenerada(CommonConstants.COD_ENCUESTA_GENERADA);
		command
				.setConsteEncuestaAplicada(CommonConstants.COD_ENCUESTA_APLICADA);
		command
				.setConsteEncuestaPendiente(CommonConstants.COD_ENCUESTA_PENDIENTE);

		List lista = new ArrayList();
		List lista1 = new ArrayList();
		List lista2 = new ArrayList();
		List lista3 = new ArrayList();
		List lista4 = new ArrayList();

		lista1 = this.tablaDetalleManager.getAllTablaDetalle(
				CommonConstants.TIPT_TIPO_APLICACION, "", "", "", "", "", "",
				"", CommonConstants.TIPO_ORDEN_COD);
		if (lista1 != null)
			command.setListaTipoAplicacion(lista1);

		lista2 = this.tablaDetalleManager.getAllTablaDetalle(
				CommonConstants.TIPT_TIPO_ENCUESTA, "", "", "", "", "", "", "",
				CommonConstants.TIPO_ORDEN_COD);
		
		//SE LISTA EL COMBO TIPO ENCUESTA
		if (lista2 != null)
			command.setListaTipoEncuesta(lista2);

		lista3 = this.tablaDetalleManager.getAllTablaDetalle(
				CommonConstants.TIPT_TIPO_ENCUESTA_SERVICIO, "", "", "", "",
				"", "", "", CommonConstants.TIPO_ORDEN_COD);
		if (lista3 != null)
			command.setListaTipoServicio(lista3);
		// ********************************************************************
		lista4 = this.tablaDetalleManager.getAllTablaDetalle(
				CommonConstants.TIPT_TIPO_ESTADO_ENCUESTA, "", "", "", "", "",
				"", "", CommonConstants.TIPO_ORDEN_COD);
		
		
		//SI EL ESTADO ES APLICADA LA ELIMINAMOS DE LA LISTA
		TipoTablaDetalle obj = new TipoTablaDetalle();
		if (lista4 != null) {
			
			String tipoBus = command.getTxhTipoBusqueda();
			
			for (int i = 0; i < lista4.size(); i++) {
				obj = (TipoTablaDetalle) lista4.get(i);
				log.info("BD>>"+obj.getCodTipoTablaDetalle()+">>");
				log.info("tipoBus>>"+tipoBus+">>");
				if(tipoBus.equalsIgnoreCase("1")){
					if (
						!obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_EN_APLICACION) &&	
						!obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_APLICADA)					
						
					) {
						lista4.remove(i);
						i--;
					}
				}else if(tipoBus.equalsIgnoreCase("2")){
					if (
						obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_PENDIENTE) ||
						obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_GENERADA) ||
						obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_EN_APLICACION)
						) {
						lista4.remove(i);
						i--;
					}					
				}else if(tipoBus.equalsIgnoreCase("3")){
					if (
							obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_PENDIENTE) ||
							obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_GENERADA) ||
							obj.getCodTipoTablaDetalle().equalsIgnoreCase(CommonConstants.COD_ENCUESTA_EN_APLICACION)
						) {
						lista4.remove(i);
						i--;
					}
				}
			}
			command.setListaTipoEstado(lista4);
		}
		
		
		// ***********************************************************************
		if (command.getCodUsuario() != null) {
			request.getSession().setAttribute("listaBandejaConfiguracion2",lista);
			command.setTamListaBandeja("0");
		}

		// ***********************************************************************

		log.info("formBackingObject:FIN");

		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	/**
	 * Redirect to the successView when the cancel button has been pressed.
	 */
	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		log.info("onSubmit:INI");

		ConfGenBandejaEncuestasCommand control = (ConfGenBandejaEncuestasCommand) command;
		String resultado = "";
		
		log.info("OPERACION>>"+control.getOperacion()+">>");
		if (control.getOperacion().equals("BUSCAR")) {
			BuscarBandeja(control, request);
		} 

		log.info("onSubmit:FIN");
		return new ModelAndView("/encuestas/consultaReportes/enc_consulta_busquedaEncuesta","control", control);
	}

	public void BuscarBandeja(ConfGenBandejaEncuestasCommand control,
			HttpServletRequest request) {
		List lista = new ArrayList();
		if (control.getCodTipoAplicacion().equals("-1"))
			control.setCodTipoAplicacion("");
		if (control.getCodTipoServicio().equals("-1"))
			control.setCodTipoServicio("");
		if (control.getCodTipoEncuesta().equals("-1"))
			control.setCodTipoEncuesta("");
		if (control.getCodTipoEstado().equals("-1"))
			control.setCodTipoEstado(CommonConstants.COD_ENCUESTA_PENDIENTE
					+ "," + CommonConstants.COD_ENCUESTA_GENERADA + ","
					+ CommonConstants.COD_ENCUESTA_EN_APLICACION);
		log.info("getAllFormato");
		
		log.info("1>>" + control.getCodTipoAplicacion() +">>");
		log.info("2>>"+ control.getCodTipoServicio() + ">>");
		log.info("3>>"+ control.getCodTipoEncuesta() + ">>");
		log.info("4>>"+ control.getCodTipoEstado() + ">>");
		log.info("5>>"+">>");
		log.info("6>>"+ control.getCodResponsable() + ">>");				
		log.info("7>>"+control.getTxtNombre()+">>");
		/*lista = this.configuracionGeneracionManager.getAllFormato(control
				.getCodTipoAplicacion(), control.getCodTipoServicio(), control
				.getCodTipoEncuesta(), control.getCodTipoEstado(), "", control
				.getCodResponsable(), "");*/

		lista = this.configuracionGeneracionManager.getAllEncFormatoBusqueda(
				control.getCodTipoAplicacion(),
				control.getCodTipoServicio(),
				control.getCodTipoEncuesta(),
				control.getCodTipoEstado(),
				"",
				control.getCodResponsable(), "",
				control.getTxtNombre()
				);

		if (lista != null) {
			// System.out.println("Size: " + lista.size());
			control.setTamListaBandeja("" + lista.size());
		} else {
			// System.out.println("Lista Null");
			control.setTamListaBandeja("0");
		}
		request.getSession().setAttribute("listaBandejaConfiguracion2", lista);
	}

	private String DeleteEncuestaFormato(ConfGenBandejaEncuestasCommand control) {
		try {
			System.out.println(">>DeleteEncuestaFormato<<");
			System.out.println("CodSelec>>" + control.getCodSelec() + "<<");
			System.out.println("CodUsuario>>" + control.getCodUsuario() + "<<");
			String resultado = this.configuracionGeneracionManager
					.DeleteEncuestaFormato(control.getCodSelec(), control
							.getCodUsuario());
			return resultado;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String CopiarEncuestaFormato(ConfGenBandejaEncuestasCommand control) {
		try {
			System.out.println(">>CopiarEncuestaFormato<<");
			System.out
					.println(">>getCodSelec>>" + control.getCodSelec() + "<<");
			String resultado = this.configuracionGeneracionManager
					.InsertCopiaFormatoEncuesta(control.getCodSelec());
			return resultado;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void AsignarMensajesQuitar(ConfGenBandejaEncuestasCommand control,
			String resultado) {
		if (resultado.equals("0")) {
			control.setMsg("OK_QUITAR");
		} else if (resultado.equals("-1")) {
			control.setMsg("ERROR_QUITAR");
		} else {
			StringTokenizer str = new StringTokenizer(resultado, "|");
			if (str.hasMoreTokens()) {
				String a = str.nextToken();
				control.setMsg("ESTADO_INCORRECTO");
				control.setDscMensaje(str.nextToken());
			} else {
				control.setMsg("ERROR_QUITAR");
			}
			System.out.println("msg>>" + control.getMsg());
			System.out.println("dscMensaje>>" + control.getDscMensaje());
		}
	}

	private void AsignarMensajesCopiar(ConfGenBandejaEncuestasCommand control,
			String resultado) {
		if (resultado.equals("0")) {
			control.setMsg("OK_COPIAR");
		} else if (resultado.equals("-1")) {
			control.setMsg("ERROR_COPIAR");
		} else {
			StringTokenizer str = new StringTokenizer(resultado, "|");
			if (str.hasMoreTokens()) {
				String a = str.nextToken();
				control.setMsg("ERROR_GENERAR_COPIA");
				control.setDscMensaje(str.nextToken());
			} else {
				control.setMsg("ERROR_COPIAR");
			}
			System.out.println("msg>>" + control.getMsg());
			System.out.println("dscMensaje>>" + control.getDscMensaje());
		}
	}
}
