package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.web.encuestas.command.CompletarEncuestaPublicadaCommand;


public class CompletarEncuestaPublicadaFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(CompletarEncuestaPublicadaFormController.class);
	
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	public ConfiguracionGeneracionManager getConfiguracionGeneracionManager() {
		return configuracionGeneracionManager;
	}

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		log.info("formBackingObject:INI");

		CompletarEncuestaPublicadaCommand command = new CompletarEncuestaPublicadaCommand();
		
		command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "" : request.getParameter("txhCodUsuario"));    	
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");
    	//constantes tipo de encuesta
    	command.setCodTipoEstandar(CommonConstants.TIPO_ENCUESTA_ESTANDAR);
    	command.setCodTipoMixta(CommonConstants.TIPO_ENCUESTA_MIXTA);
    	//***************************
		EncuestasPublicadas(command);

		log.info("formBackingObject:FIN");

		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	/**
	 * Redirect to the successView when the cancel button has been pressed.
	 */
	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		log.info("onSubmit:INI");

		CompletarEncuestaPublicadaCommand control = (CompletarEncuestaPublicadaCommand) command;
		EncuestasPublicadas(control);
		
		log.info("onSubmit:FIN");
		return new ModelAndView("/encuestas/completarEncuesta/enc_completar_encuesta_publicada","control", control);
	}
	private void EncuestasPublicadas(CompletarEncuestaPublicadaCommand control){		
		List lista=this.configuracionGeneracionManager.GetAllEncuestasPublicadas(control.getCodUsuario());		
		if(lista!=null){
			if(lista.size()>0){
				control.setListaEncPublicadas(lista);
				control.setTamListaEncPublicadas(""+lista.size());
			}
			else{
				control.setTamListaEncPublicadas("0");
			}
		}
		else{
			control.setListaEncPublicadas(null);
			control.setTamListaEncPublicadas("0");
		}
	}
}
