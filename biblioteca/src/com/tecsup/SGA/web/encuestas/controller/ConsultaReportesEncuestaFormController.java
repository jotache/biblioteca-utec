package com.tecsup.SGA.web.encuestas.controller;

import java.text.*;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.ConsultaReportesEncuestaCommand;


public class ConsultaReportesEncuestaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaReportesEncuestaFormController.class);	
	private TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		ConsultaReportesEncuestaCommand command = new ConsultaReportesEncuestaCommand();
		//******************************************************************
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){
    		System.out.println("usuarioSeguridad codUsuario--->>"+usuarioSeguridad.getCodUsuario()+"<<");
    		System.out.println("usuarioSeguridad nomUsuario--->>"+usuarioSeguridad.getNomUsuario()+"<<");
    		System.out.println("usuarioSeguridad getPerfiles--->>"+usuarioSeguridad.getPerfiles()+"<<");
    		command.setNomUsuario(usuarioSeguridad.getNomUsuario());
    		command.setPerfilUsuario(usuarioSeguridad.getPerfiles());
    	}
		//bandeja siempre se muestra cada vez que inicia la pagina
		//******************************************************************
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	request.setAttribute("codUsuario", command.getCodUsuario());
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
		//******************************************************************
    	request.setAttribute("listaReportesTotal", this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REPORTES_ENCUESTA
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
		log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	ConsultaReportesEncuestaCommand control = (ConsultaReportesEncuestaCommand) command;
    	if (control.getOperacion().trim().equals("BUSCAR")){
    		System.out.println("entra para buscar");
    	}
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/encuestas/consultaReportes/enc_consulta_reportes","control",control);
    }
    /*
    public void cargaBandeja(ConsultaReportesCommand control){
    	control.setListaReportes(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REPORTES
				, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD));
    	
    	Anios anios= new Anios();
    	
    	TipoTablaDetalle obj= new TipoTablaDetalle();
	    obj.setDscValor3("");
		obj.setCodTipoTabla("");
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_TIPO_MATERIAL);
		obj.setCodDetalle("");
		obj.setDescripcion("");
		obj.setTipo(CommonConstants.TIPO_ORDEN_COD);
		control.setListaMaterial(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
    	
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_MAT_BIB);
		
		control.setListaTipoRepMatBib(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_MOV_PRES_DEV_DIA);
		
		control.setListaTipoRepMovPresDev(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_EST_CICLO_ESP);
		
		control.setListaTipoRepCicloEsp(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));
		
		obj.setCodTipoTablaDetalle(CommonConstants.TIPT_REPORTES_TIPO_USUARIOS);
		
		control.setListaTipoUsuarios(this.biblioMantenimientoManager.getAllTablaDetalleBiblioteca(obj));

    	control.setTxhReporte1(CommonConstants.REPORTE_1);
    	control.setTxhReporte2(CommonConstants.REPORTE_2);
    	control.setTxhReporte3(CommonConstants.REPORTE_3);
    	control.setTxhReporte4(CommonConstants.REPORTE_4);
    	control.setTxhReporte5(CommonConstants.REPORTE_5);
    	
    	control.setTxhRep2A(CommonConstants.REPORTE_2A);
    	control.setTxhRep2B(CommonConstants.REPORTE_2B);
    	control.setTxhRep2C(CommonConstants.REPORTE_2C);
    	control.setTxhRep2D(CommonConstants.REPORTE_2D);
    	control.setTxhRep2E(CommonConstants.REPORTE_2E);
    	
    	control.setListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
    	control.setListaMeses(anios.getMeses02());
    	
    	control.setCboAnioSancion(CommonConstants.TIPT_PERIODO_ACTUAL);
    	control.setCboAnioBuzon(CommonConstants.TIPT_PERIODO_ACTUAL);
    	
    }*/
}
