package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.PerfilAnonimoPCCCommand;


		public class RespuestaAnonimoPCCFormController extends SimpleFormController{
		private static Log log = LogFactory.getLog(RespuestaAnonimoPCCFormController.class);
	
		private ConfiguracionGeneracionManager configuracionGeneracionManager;
		private ConfiguracionPerfilManager configuracionPerfilManager;
		
		public void setConfiguracionPerfilManager(
				ConfiguracionPerfilManager configuracionPerfilManager) {
			this.configuracionPerfilManager = configuracionPerfilManager;
		}
		
		public void setConfiguracionGeneracionManager(
				ConfiguracionGeneracionManager configuracionGeneracionManager) {
			this.configuracionGeneracionManager = configuracionGeneracionManager;
		}

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject: PerfilAnonimoPCCFormController: INICIO");   
   	
	    	PerfilAnonimoPCCCommand command = new PerfilAnonimoPCCCommand();
	  	
	    	command.setCodEncuesta(request.getParameter("txhCodEncuesta") == null ? "" : request.getParameter("txhCodEncuesta"));
	    	
	        log.info("formBackingObject: PerfilAnonimoPCCFormController: FIN --- ");
	        
	        return command;
	    }
		
	    protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));
	    }
	    
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
        
    	//----
	    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
	    	
    	log.info("onSubmit: - PerfilAnonimoPCCFormController(submit) - INI()");

    	PerfilAnonimoPCCCommand control = (PerfilAnonimoPCCCommand) command;
    	String resultado = "";
    	
		log.info("onSubmit: PerfilAnonimoPCCFormController(submit) - FIN()");
	    return new ModelAndView("","control",control);		
    	
    	}
	    
}
