package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesMixtaGruposCommand;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesMixtaVerGruposCommand;







public class AplicacionDatosGeneralesMixtaVerGruposFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicacionDatosGeneralesMixtaVerGruposFormController.class);
	
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}


	public AplicacionDatosGeneralesMixtaVerGruposFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesMixtaVerGruposCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesMixtaVerGruposCommand control = new AplicacionDatosGeneralesMixtaVerGruposCommand();    	
    	
    	if(request.getParameter("prmCodEncuesta")!=null )
    	{	control.setTxhCodigo(request.getParameter("prmCodEncuesta"));
    		control.setUsuario(request.getParameter("prmUsuario"));
    		control.setTxhCodEstado(request.getParameter("prmCodEstado"));    		
    		//cargaDatosEncuesta(control);
    		control = irConsultar(control);
    	}
    	
        log.info("formBackingObject:FIN");
        return control;
    }
	

	private AplicacionDatosGeneralesMixtaVerGruposCommand irConsultar(
			AplicacionDatosGeneralesMixtaVerGruposCommand control) {
		
		log.info("1.>>"+control.getTxhCodigo()+">>");
		log.info("2.>>"+">>");
		
		control.setLstResultado(
				configuracionGeneracionManager.getAllGruposByEncuesta(
						control.getTxhCodigo(),
						null)
				);
		
		return control;
	}


	

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	AplicacionDatosGeneralesMixtaVerGruposCommand control = (AplicacionDatosGeneralesMixtaVerGruposCommand) command;		
		log.info("operacion|"+control.getOperacion()+"|");
		
		if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
			
		}
		
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_datos_generales_mixta_ver_grupos","control",control);		
    }
		
}
