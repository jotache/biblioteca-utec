package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.MetodosConstants;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesAlternativasCommand;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesPreguntasCommand;


public class AplicacionDatosGeneralesStandarPreguntasFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AplicacionDatosGeneralesStandarPreguntasFormController.class);

	private TablaDetalleManager tablaDetalleManager;
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}

	public AplicacionDatosGeneralesStandarPreguntasFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesPreguntasCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesPreguntasCommand control = new AplicacionDatosGeneralesPreguntasCommand();    	
    	
    	ini(control);
    	
    	if(request.getParameter("prmCodEncuesta")!=null )
    	{	
    		control.setTxhCodigo(request.getParameter("prmCodEncuesta"));
    		control.setTxhCodGrupo(request.getParameter("prmCodGrupo"));
    		control.setUsuario(request.getParameter("prmUsuario"));
    		control.setTxhCodEstado(request.getParameter("prmCodEstado"));    		
    		control = irConsultar(control);
    	}
        log.info("formBackingObject:FIN");
        return control;
    }
	
    private void ini(AplicacionDatosGeneralesPreguntasCommand control) {
		
	      List lstTipoEncuesta=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_PREGUNTA
			      , "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
	      
	      control.setLstTipo(lstTipoEncuesta);
		
	}	

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resp = "";

    	AplicacionDatosGeneralesPreguntasCommand control = (AplicacionDatosGeneralesPreguntasCommand) command;		
    	log.info("operacion|"+control.getOperacion()+"|");
		
		if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
			 
			if(request.getParameter("txhCodigoUpdate")!=null && !request.getParameter("txhCodigoUpdate").trim().equalsIgnoreCase("") )
			{	log.info("irActualizar");
				log.info("txhCodigoUpdate>"+request.getParameter("txhCodigoUpdate")+">");
				resp = irActualizar(control,request.getParameter("txhCodigoUpdate"),request.getParameter("radio"));
				
			
				log.info("RPUESTA:"+resp+":");
				if("0".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO+MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()));    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_EXISTE_PREGUNTA );    		
				}else if("-3".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);										    		
				}else{
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}
			}
			else 
			{	log.info("irRegistrar");
				resp = irRegistrar(control,request.getParameter("radio"));    		
				log.info("RPUESTA:"+resp+":");
				
				if(resp==null || resp.equals("")) {
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}else if(Integer.parseInt(resp)>0){
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO+MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()));    		
				}else if("-1".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		
				}else if("-3".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_EXISTE_PREGUNTA );
				}else{
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				}
    		
			}
			
		}else if("irEliminar".equalsIgnoreCase(control.getOperacion())){

				//ID A ELIMINAR		
				log.info("txhCodigoSel>"+request.getParameter("txhCodigoSel")+">");
				
				resp = irEliminar(control,request.getParameter("txhCodigoSel"));
				
				if("0".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ELIMINAR_EXITO+MetodosConstants.getMensajeEncuesta(control.getTxhCodEstado()));    		
				}else if("-2".equalsIgnoreCase(resp)){
					request.setAttribute("mensaje",CommonMessage.ENCUESTA_NO_SE_PUDE_MODIFICAR);    		
				}else{
					request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);
				}
		}
			
		control = irConsultar(control);
    	clearForm(control);

		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_datos_generales_standar_preguntas","control",control);		
    }
        
        private void clearForm(AplicacionDatosGeneralesPreguntasCommand control) {
			control.setTxtPregunta("");
			control.setCboTipo("");
			control.setChkObligatorio(null);
			control.setOperacion("");
		}

		private String irEliminar(
        		AplicacionDatosGeneralesPreguntasCommand control,
				String idEliminacion) {
			String str = "";
			
			log.info("1.>"+control.getTxhCodigo()+">");
			log.info("2.>"+idEliminacion+">");
			log.info("3.>"+control.getUsuario()+">");
			
			str = configuracionGeneracionManager.deletePreguntasByGrupo(
					control.getTxhCodigo(),
					idEliminacion,
					control.getUsuario()
					);
			
			return str;
		}

		private String irRegistrar(AplicacionDatosGeneralesPreguntasCommand control,String radio){
			String str = "";
			
			/*String indAlt = "0"; 
			if("0001".equalsIgnoreCase(control.getCboTipo()))
				indAlt = "0";
			else
				indAlt = "1";*/
			
				log.info("1.>"+control.getTxhCodigo()+">");
				log.info("2.>"+control.getTxhCodGrupo()+">");
				log.info("3.>"+control.getTxtPregunta()+">");
				log.info("4.>"+control.getCboTipo()+">");
				log.info("5.>"+control.getTxhCodGrupo()+">");
				log.info("6.radio>"+radio+">");
				log.info("7.>0>");
				log.info("8.>"+"0"+">");//indAlt
				log.info("9.>"+control.getUsuario()+">");
				
			str = configuracionGeneracionManager.insertPreguntasByGrupo(
					control.getTxhCodigo(),
					control.getTxhCodGrupo(),
					control.getTxtPregunta(),
					control.getCboTipo(),
					"1".equalsIgnoreCase(control.getChkObligatorio())?"1":"0",
					radio,
					"0",
					"0",//indAlt, //"0"					
					control.getUsuario()
					);
			return str;
		}

		private String irActualizar(AplicacionDatosGeneralesPreguntasCommand control, String codigoUpdate,String radio){
			String str = "";
			
			/*String indAlt = "0"; 
			if("0001".equalsIgnoreCase(control.getCboTipo()))
				indAlt = "0";
			else
				indAlt = "1";*/
			
			log.info("1.>"+control.getTxhCodigo()+">");
			log.info("2.>"+control.getTxhCodGrupo()+">");
			log.info("3.>"+codigoUpdate+">");
			log.info("3.>"+control.getTxtPregunta()+">");
			log.info("4.>"+control.getCboTipo()+">");
			log.info("5.>"+("1".equalsIgnoreCase(control.getChkObligatorio())?"1":"0")+">");
			log.info("6.radio>"+radio+">");
			log.info("7.>0>");
			log.info("8.>"+"0"+">");
			log.info("9.>"+control.getUsuario()+">");
			
		str = configuracionGeneracionManager.updatePreguntasByGrupo(
				control.getTxhCodigo(),
				control.getTxhCodGrupo(),
				codigoUpdate,
				control.getTxtPregunta(),
				control.getCboTipo(),
				"1".equalsIgnoreCase(control.getChkObligatorio())?"1":"0",
				radio,
				"0",
				"0",//indAlt,//"0"
				control.getUsuario()
				);
			
			
			
			return str;
			
		}
	
		
		private AplicacionDatosGeneralesPreguntasCommand irConsultar(
				AplicacionDatosGeneralesPreguntasCommand control) {
			
			log.info("1.>"+control.getTxhCodigo()+">");
			log.info("2.>"+control.getTxhCodGrupo()+">");
			log.info("3.>"+">");
			
			control.setLstResultado(
					configuracionGeneracionManager.getAllPreguntasByGrupo(
					control.getTxhCodigo(),
					control.getTxhCodGrupo(),
					""
					));			
			return control;
		}

		public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
			this.tablaDetalleManager = tablaDetalleManager;
		}
		
}
