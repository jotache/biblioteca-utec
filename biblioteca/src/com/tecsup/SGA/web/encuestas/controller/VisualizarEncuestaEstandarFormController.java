package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;

import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;



public class VisualizarEncuestaEstandarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(VisualizarEncuestaEstandarFormController.class);
	private ConfiguracionGeneracionManager configuracionGeneracionManager;

	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	
	public VisualizarEncuestaEstandarFormController() {    	
        super();        
        setCommandClass(AplicacionDatosGeneralesCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AplicacionDatosGeneralesCommand control = new AplicacionDatosGeneralesCommand();    	
    	
    	if(request.getParameter("txhCodEncuesta")!=null )
    	{	
    		control.setTxhCodigo(request.getParameter("txhCodEncuesta"));
    		control.setUsuario(request.getParameter("txhUsuario"));
    		control.setTxhCodPerfil(request.getParameter("txhCodPerfil"));//codPerfil,
			control.setTxhCodEncuestado(request.getParameter("txhCodEncuestado"));//epdecId,
			control.setTxhCodProfesor(request.getParameter("txhCodProfesor"));
	    	
	    	//DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),control.getTxhCodProfesor(),"","");
			System.out.println("control.setTxhCodigo>>"+control.getTxhCodigo()+"<<");
			System.out.println("control.setTxhCodProfesor>>"+control.getTxhCodProfesor()+"<<");
			DatosCabeceraBean bean = configuracionGeneracionManager.getAllDatosCabeceraEncuesta(control.getTxhCodigo(),"");			
	    	request.setAttribute("OBJ_CABECERA",bean);
    	}   	
    	
    	request.setAttribute("LST_RESULTADO",VerEncuesta(control.getTxhCodigo()));   	
    	
        log.info("formBackingObject:FIN");
        return control;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
	
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	String resp = "";
    	
    	AplicacionDatosGeneralesCommand control = (AplicacionDatosGeneralesCommand) command;
		
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/encuestas/configuracionGeneracion/enc_visualizar_encuesta_estandar","control",control);		
    }

	private List VerEncuesta(String txhCodigo) {
					
		log.info("VerEncuesta");
		log.info("1.>"+txhCodigo+">");			
		
		List lst = configuracionGeneracionManager.getAllVerEncuesta(txhCodigo);
		return lst;		
	}		
}
