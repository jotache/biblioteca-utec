package com.tecsup.SGA.web.encuestas.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.encuestas.command.AdminTipoFormatoAgregarCommand;
import com.tecsup.SGA.web.encuestas.command.AdminTipoServicioAgregarCommand;

public class AdminTipoServicioAgregarFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdminTipoServicioAgregarFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		AdminTipoServicioAgregarCommand command= new AdminTipoServicioAgregarCommand();
		command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
		command.setCodSeleccion((String)request.getParameter("txhCodSelec"));
    	request.setAttribute("codUsuario", command.getCodUsuario());    	
    	System.out.println("codUsuario--->>"+command.getCodUsuario()+"<<");
    	command.setTipoBandera((String)request.getParameter("txhTipoBandera"));
    	if(command.getTipoBandera()!=null)
    	{ if(command.getTipoBandera().equals("2"))
    		 { 
    		   command.setTipoServicio((String)request.getParameter("txhDescripSelec")); 
    		   System.out.println("TipoFormato: "+command.getTipoServicio());
    		 }
    	  System.out.println("TipoBandera: "+command.getTipoBandera());
    	}
    	log.info("formBackingObject:FIN");
    	
		return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	AdminTipoServicioAgregarCommand control= (AdminTipoServicioAgregarCommand) command;
    	String resultado="";
    	System.out.println("TipoBandera: "+control.getTipoBandera()+">>Operacion<<"+control.getOperacion());
    	
    	if(control.getTipoBandera().equals("1"))
    	{	if(control.getOperacion().equals("GUARDAR"))
	    	{	resultado=InsertTablaDetalle(control);
	    	    System.out.println("Resultado: "+resultado);
	    	     if(resultado.equals("-1")) control.setMsg("ERROR");
	    	     else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
	    	     	   else{ control.setMsg("OK");
	    	     	   }
	    	     }
	    	    System.out.println("Msg: "+control.getMsg());
	    	}
    	}
    	else{if(control.getTipoBandera().equals("2"))
    		{	if(control.getOperacion().equals("GUARDAR"))
	    		{	resultado=UpdateTablaDetalle(control);
	    			System.out.println("Resultado: "+resultado);
	    			if(resultado.equals("-1")) control.setMsg("ERROR");
	    			else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
    	     	          else{ control.setMsg("OK");
    	     	        }
	    			}
	    			System.out.println("Msg: "+control.getMsg());
	    		}
    		}
    		
    	}
    	
    	log.info("onSubmit:FIN");
        
    	return new ModelAndView("/encuestas/administracionSistema/enc_tipo_servicio_agregar","control",control);		
    }

    private String InsertTablaDetalle(AdminTipoServicioAgregarCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_ENCUESTA_SERVICIO);
    		obj.setDescripcion(control.getTipoServicio().trim());
    		obj.setUsuario(control.getCodUsuario());
    
    		obj.setCodTipoTablaDetalle(this.tablaDetalleManager.InsertTablaDetalle(obj, control.getCodUsuario()));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private String UpdateTablaDetalle(AdminTipoServicioAgregarCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
 
    		obj.setCodTipoTabla(CommonConstants.TIPT_TIPO_ENCUESTA_SERVICIO);
    		obj.setDescripcion(control.getTipoServicio());
    		obj.setUsuario(control.getCodUsuario());
    		obj.setCodDetalle(control.getCodSeleccion());
    		
    		if ( !control.getCodSeleccion().trim().equals("") )
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.
    			UpdateTablaDetalle(obj, control.getCodUsuario()));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
}
