package com.tecsup.SGA.web.seguridad.command;

import java.util.List;

public class SedeEvaluadorCommand {

	/*Para seleccionar la Sede*/
	private String accion;
	private String codSede;
	private List listaSedes;
	private String codPeriodo;
	private String codEval;
	
	
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		accion = accion;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListaSedes() {
		return listaSedes;
	}
	public void setListaSedes(List listaSedes) {
		this.listaSedes = listaSedes;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}

	
}
