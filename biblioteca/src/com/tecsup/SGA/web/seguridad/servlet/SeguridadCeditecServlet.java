package com.tecsup.SGA.web.seguridad.servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.google.api.services.plus.model.Person.Emails;
import com.google.gson.Gson;
import com.tecsup.SGA.DAO.jdbc.SeguridadDAOJdbc;
import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.service.seguridad.impl.SeguridadManagerImpl;


/**
 * Servlet implementation class SeguridadCeditecServlet
 */
public class SeguridadCeditecServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static Log log = LogFactory.getLog(SeguridadCeditecServlet.class);
	
	private String code;	
 	private String object;
 	
    public SeguridadCeditecServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		System.out.println("get si funciona");
	}

	
	private HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private JsonFactory JSON_FACTORY = new JacksonFactory();
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("Post");
		Map<String, Object> map = new HashMap<String, Object>();
		boolean isValid = false;
		String operacion = request.getParameter("operacion");
		if(operacion==null) operacion="";
		log.info("operación:" + operacion);
		if(operacion.equals("GOOGLE_CONNECT")){
			
			log.info("doPost: Google_Connect");
			
			try{
				
				SeguridadDAOJdbc seguridadManager = new SeguridadDAOJdbc();
				
				this.code = (String)request.getParameter("code");
				log.info("gingreso() code:" + (String)request.getParameter("code"));
				
				GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(HTTP_TRANSPORT, JSON_FACTORY, CommonConstants.GOOGLEPLUS_CLIENT_ID, CommonConstants.GOOGLEPLUS_CLIENT_SECRET, code, "postmessage").execute();
				log.info("Token: " + tokenResponse.toString());
				
				// Crea una representación credencial de los datos del token.
			    GoogleCredential credential = new GoogleCredential.Builder()
			        .setJsonFactory(JSON_FACTORY)
			        .setTransport(HTTP_TRANSPORT)
			        .setClientSecrets(CommonConstants.GOOGLEPLUS_CLIENT_ID, CommonConstants.GOOGLEPLUS_CLIENT_SECRET).build()
			        .setFromTokenResponse(tokenResponse);
			    
			    Plus plus = new Plus.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).build();
			    
			    Person profile = plus.people().get("me").execute();
			    
			    log.info("ID: " + profile.getId());
			    log.info("Name: " + profile.getDisplayName());
			    log.info("Image URL: " + profile.getImage().getUrl());
			    log.info("Profile URL: " + profile.getUrl());
			    
			    String principalEmail = null;
				for (Emails email : profile.getEmails()) {
					log.info("Email: " +email.getValue() + " - " + email.toPrettyString());
					principalEmail = email.getValue();
				}
				
				String usuario = principalEmail.substring(0, principalEmail.indexOf("@")).toLowerCase();
				String dominio = principalEmail.substring(principalEmail.indexOf("@")+1).toLowerCase();
				
				log.info("dominio: " + dominio);
				
				String[] allowDomains = {"tecsup.edu.pe","utec.edu.pe"};
				
				if (Arrays.asList(allowDomains).contains(dominio)){
					log.info("Cuenta " + usuario + " pertenece a la lista blanca " + Arrays.toString(allowDomains));
					
					List lstResultado = null;
					String verificarUsuario="";
					boolean tieneOpciones = false;
					UsuarioSeguridad usuarioSeguridad = null;
					
					if(dominio.equals("utec.edu.pe")){						
						lstResultado = seguridadManager.getOpcionesUtec(usuario, CommonConstants.SGA_BIB); 
						if(lstResultado.size()>0){
							usuarioSeguridad = seguridadManager.getDatosUsuarioCeditecUtec(usuario);
							tieneOpciones = true;
							log.info(usuarioSeguridad);
						}						
					}else{						
						lstResultado = seguridadManager.getOpcionesTecsup(usuario, CommonConstants.SGA_BIB);
						if(lstResultado.size()>0){
							usuarioSeguridad = seguridadManager.getDatosUsuarioCeditecTecsup(usuario);
							tieneOpciones = true;
						}
					}
					
					if (tieneOpciones){
					
						if (usuarioSeguridad==null) usuarioSeguridad = new UsuarioSeguridad();
	    				
	    				usuarioSeguridad.setOpciones(convierte(lstResultado));
	    				usuarioSeguridad.setCodUsuario(usuario.toLowerCase());
	    				
	    				String sedeUsuarioBiblio = seguridadManager.sedeUsuarioCeditec(usuarioSeguridad.getCodUsuario());    				
	    				if (sedeUsuarioBiblio==null) sedeUsuarioBiblio="";    				
	    				usuarioSeguridad.setSede(sedeUsuarioBiblio);
	    				
	    				verificarUsuario = seguridadManager.GetVerificarReservaUsuario(usuarioSeguridad.getCodUsuario());
	    				log.info("verificarUsuario: " + verificarUsuario);
						
	    				usuarioSeguridad.setUsuarioSeguriBib(
	    						seguridadManager.obtenerUsuarioSeguriBiblio2(
	    								usuarioSeguridad.getIdUsuario(),
	    								usuarioSeguridad.getCodUsuario()));
	    				
	    				log.info("Usuario Bib:"+usuarioSeguridad.getUsuarioSeguriBib());
	    				if(usuarioSeguridad.getUsuarioSeguriBib()!=null){
	    					request.getSession().setAttribute("verificarUsuario", verificarUsuario);
	            			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);            			
	            			request.getSession().setAttribute("usuarioClave",usuario.toLowerCase());
	            			request.getSession().removeAttribute("reingreso");
	        				request.getSession().removeAttribute("contextoIngreso");
	        				request.getSession().setAttribute("laboratoria601",false);	
	        				request.setAttribute("mensaje","OK");
	        				
	        				map.put("codusuario", usuarioSeguridad.getIdUsuario());
	        				
	        				isValid = true;
	        				
	    				}
						
					}else{
						map.put("errors", "No tiene permisos para ingresar a CEDITEC.");
					}
					
    				
				}else{
					log.info("Cuenta " + usuario + " NO pertenece a la lista blanca " + Arrays.toString(allowDomains));
					map.put("errors", "No es posible utilizar esta cuenta de google.");
				}
			    
			}catch (Throwable e) {
				log.error(e);	
				map.put("errors","Error");
			}
			
			
//			map.put("error", "");			
		}else{
			map.put("errors", "Operación cancelada.");
		}
		
		map.put("isValid", isValid);
		write(response, map);
	}

	private void write(HttpServletResponse response, Map<String, Object> map) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));		
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public void setCode(String code) {
		this.code = code;
	}

	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}

}
