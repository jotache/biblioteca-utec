package com.tecsup.SGA.web.seguridad.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.seguridad.command.SedeEvaluadorCommand;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.service.evaluaciones.ComunManager;

public class SedeEvaluadorController extends SimpleFormController{
	private static Log log = LogFactory.getLog(SedeEvaluadorController.class);
	private ComunManager comunManager;
	
	
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	SedeEvaluadorCommand command = new SedeEvaluadorCommand();  	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodEval(usuarioSeguridad.getIdUsuario());
    		command.setCodPeriodo(usuarioSeguridad.getPeriodo());
    		llenarSedes(command);
    	}    	
    	
    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	SedeEvaluadorCommand control = (SedeEvaluadorCommand) command;  	
    	if ( control.getAccion().trim().equals("ENTRAR"))
    	{
        	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
        	usuarioSeguridad.setSede(control.getCodSede());
        	request.getSession().setAttribute("usuarioSeguridad",usuarioSeguridad);
        	
        	response.sendRedirect("/biblioteca/Evaluaciones.html");
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/seguridad/eva_sede.jsp","control",control);		
    }
    
    private void llenarSedes(SedeEvaluadorCommand command)
    {
    	command.setListaSedes(this.comunManager.GetSedesByEvaluador(command.getCodEval()));
    }
}