/**
 * 
 */
package com.tecsup.SGA.web.seguridad.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.google.api.services.plus.model.Person.Emails;
import com.tecsup.SGA.common.CommonConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;

/**
 * @author jpalomino
 *
 */
public class SeguridadCeditecController implements Controller {

	private static Log log = LogFactory.getLog(SeguridadCeditecController.class);
	/**
	 * 
	 */
	private String code;
	
//	private HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
//	private JsonFactory JSON_FACTORY = new JacksonFactory();
	
	
	public SeguridadCeditecController() {
		// TODO Auto-generated constructor stub
		log.info("SeguridadCeditecController()");
	}

//	@Override
//	protected Object formBackingObject(HttpServletRequest request) throws Exception {	
		
//	}
	
	
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.Controller#handleRequest(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	//ModelAndView
	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse arg1) throws Exception {
	
//		log.info("handleRequest()");
//		log.info("gingreso() code:" + (String)request.getParameter("code"));
//		this.code = (String)request.getParameter("code");
//		
//		try{
//			
//			GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(HTTP_TRANSPORT, JSON_FACTORY, CommonConstants.GOOGLEPLUS_CLIENT_ID, CommonConstants.GOOGLEPLUS_CLIENT_SECRET, code, "postmessage").execute();
//			
//			log.info("Token: " + tokenResponse.toString());
//		    
//		    // Crea una representación credencial de los datos del token.
//		    GoogleCredential credential = new GoogleCredential.Builder()
//		        .setJsonFactory(JSON_FACTORY)
//		        .setTransport(HTTP_TRANSPORT)
//		        .setClientSecrets(CommonConstants.GOOGLEPLUS_CLIENT_ID, CommonConstants.GOOGLEPLUS_CLIENT_SECRET).build()
//		        .setFromTokenResponse(tokenResponse);
//		    
//		    Plus plus = new Plus.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).build();
//		    
//		    Person profile = plus.people().get("me").execute();
//		    
//		    log.info("ID: " + profile.getId());
//		    log.info("Name: " + profile.getDisplayName());
//		    log.info("Image URL: " + profile.getImage().getUrl());
//		    log.info("Profile URL: " + profile.getUrl());
//		    
//		    String principalEmail = null;
//			for (Emails email : profile.getEmails()) {
//				log.info("Email: " +email.getValue() + " - " + email.toPrettyString());
//				principalEmail = email.getValue();
//			}
//			
//			String usuario = principalEmail.substring(0, principalEmail.indexOf("@")).toLowerCase();
//			String dominio = principalEmail.substring(principalEmail.indexOf("@")+1).toLowerCase();
//			
//			String[] allowDomains = {"tecsup.edu.pe","utec.edu.pe"};
//			if (Arrays.asList(allowDomains).contains(dominio)){
//				log.info("Cuenta " + usuario + " pertenece a la lista blanca " + Arrays.toString(allowDomains));
//			}else{
//				log.info("Cuenta " + usuario + " NO pertenece a la lista blanca " + Arrays.toString(allowDomains));
//				
//			}
//		    
//			
//		}catch (Throwable e) {
//			log.error(e);
//			throw new Exception(e);
//		}
			   
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		log.info("main()");
	}

	public void setCode(String code) {
		this.code = code;
	}

}
