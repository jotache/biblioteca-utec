package com.tecsup.SGA.web.seguridad.controller;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonSeguridad;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.seguridad.command.SeguridadCommand;

public class SeguridadLogiFormController   extends SimpleFormController{

	private static Log log = LogFactory.getLog(SeguridadLogiFormController.class);
	
	private SeguridadManager seguridadManager;
	
	/*SETTER*/	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
	public SeguridadLogiFormController(){
		super();
		setCommandClass(SeguridadCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	SeguridadCommand command = new SeguridadCommand();
    	   	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	SeguridadCommand control = (SeguridadCommand) command;
    	UsuarioSeguridad usuarioSeguridad = null;
    	List lstResultado=null;
    	ArrayList<UsuarioPerfil> lstPerfiles=null;
    	
    	log.info("OPERACION>"+control.getOperacion());
    	
    	if(control.getOperacion().equalsIgnoreCase("ACCESO"))
    	{	
    		//PARA CAMBIAR EN TECSUP
    		int i = seguridadManager.getValidaUsuario(control.getUsuario(),control.getClave());
    		
    		if( SeguridadBean.esAdministrativo(request.getRemoteAddr(),control.getClave()) ) i = 0; //-- Acceso libre
    		log.info("info::>"+i+">");
    		
    		if(i==0)
    		{ 			
    			lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_LOG);
    			lstPerfiles = (ArrayList<UsuarioPerfil>)seguridadManager.getPerfiles(control.getUsuario(),CommonConstants.SGA_LOG);
    			//log.info("TAMA�O DE LA LST DE OPCIONES>"+lstResultado.size());
    			
    			if(lstResultado.size()>0){
    				//Obtenmos datos del usuario
    				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
    				if ( usuarioSeguridad == null ) usuarioSeguridad = new UsuarioSeguridad();
    				
    				usuarioSeguridad.setOpciones(convierte(lstResultado));
    				usuarioSeguridad.setRoles(lstPerfiles);
    				usuarioSeguridad.setCodUsuario(control.getUsuario());    				
    				usuarioSeguridad.setPerfiles(conviertePerfiles(lstPerfiles));
    				
    				//usuarioSeguridad.setSede("L");
    				
    				String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
    				seguridadManager.RegistrarSesion(true, estacioncliente, control.getUsuario(), CommonConstants.SGA_LOG);    				
    				usuarioSeguridad = seguridadManager.ObtenerEstadoUsuario(usuarioSeguridad);
        			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
        			request.getSession().setAttribute("usuarioClave", control.getUsuario());    				        			
    				request.setAttribute("mensaje","OK");    				
    				request.getSession().removeAttribute("reingreso");
    				request.getSession().removeAttribute("contextoIngreso");
    			}
    			else
    				request.setAttribute("mensaje","-9");    			
    		} else{
				if (i==-6) {
					request.getSession().setAttribute("usuarioClave", control.getUsuario());
					request.getSession().setAttribute("contextoIngreso", "Si");
					request.setAttribute("mensaje",i);
				}else if (i>0){
					lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_LOG);
	    			lstPerfiles = (ArrayList<UsuarioPerfil>)seguridadManager.getPerfiles(control.getUsuario(),CommonConstants.SGA_LOG);
	    			if(lstResultado.size()>0){ 
	    				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
	    				if ( usuarioSeguridad == null ) usuarioSeguridad = new UsuarioSeguridad();
	    				
	    				usuarioSeguridad.setOpciones(convierte(lstResultado));
	    				usuarioSeguridad.setRoles(lstPerfiles);
	    				usuarioSeguridad.setCodUsuario(control.getUsuario());    				
	    				usuarioSeguridad.setPerfiles(conviertePerfiles(lstPerfiles));    				
	    				
	    				String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
	    				seguridadManager.RegistrarSesion(true, estacioncliente, control.getUsuario(), CommonConstants.SGA_LOG);    				
	    				usuarioSeguridad = seguridadManager.ObtenerEstadoUsuario(usuarioSeguridad);
	        			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
	        			request.getSession().setAttribute("usuarioClave", control.getUsuario());    				        			
	        			request.setAttribute("mensaje",i);    				
	    				request.getSession().removeAttribute("reingreso");
	    				request.getSession().removeAttribute("contextoIngreso");
	    			}
				}
				request.setAttribute("mensaje",i);  
			}				
    	}    	    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/seguridad/logi_logeo","control",control);
    }

	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}
	
	private String conviertePerfiles(ArrayList<UsuarioPerfil> lstPerfiles) {
		String cad="";
		for(int i=0;i<lstPerfiles.size();i++)
			cad = cad + lstPerfiles.get(i).getDscPerfil()+"|";				
		return cad;
	}
}
