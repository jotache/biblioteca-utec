package com.tecsup.SGA.web.seguridad.controller;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.OpcionesApoyo;
import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonSeguridad;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.AccesoIP;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.service.reclutamiento.ReclutaManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.reclutamiento.command.AdjuntarCVCommand;
import com.tecsup.SGA.web.reclutamiento.controller.AdjuntarCVFormController;
import com.tecsup.SGA.web.seguridad.command.SeguridadCommand;

public class SeguridadBibFormController   extends SimpleFormController{

	private static Log log = LogFactory.getLog(SeguridadBibFormController.class);
	
	private SeguridadManager seguridadManager;
	private PortalWebBibliotecaManager portalWebBibliotecaManager;
	private BiblioMantenimientoManager biblioMantenimientoManager;
	
	public void setBiblioMantenimientoManager(BiblioMantenimientoManager biblioMantenimientoManager) {
		this.biblioMantenimientoManager = biblioMantenimientoManager;
	}
	/**
	 * @param portalWebBibliotecaManager the portalWebBibliotecaManager to set
	 */
	public void setPortalWebBibliotecaManager(
			PortalWebBibliotecaManager portalWebBibliotecaManager) {
		this.portalWebBibliotecaManager = portalWebBibliotecaManager;
	}

	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	
	/*SETTER*/	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
	public SeguridadBibFormController(){
		super();
		setCommandClass(SeguridadCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	SeguridadCommand command = new SeguridadCommand();
    	   	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	SeguridadCommand control = (SeguridadCommand) command;
    	String cadena=""; 	String pagina ="/seguridad/eva_logeo";    	
    	UsuarioSeguridad usuarioSeguridad = null;
    	Periodo periodo;
    	List lstResultado=null;
    	ArrayList<UsuarioPerfil> lstPerfiles=null;
    	log.info("OPERACION>"+control.getOperacion());
    	
    	if(control.getOperacion().equalsIgnoreCase("ACCESO"))
    	{	
    		//PARA CAMBIAR EN TECSUP
    		int i = seguridadManager.getValidaUsuario(control.getUsuario(),control.getClave());
    		
//    		if( SeguridadBean.esAdministrativo(request.getRemoteAddr(),control.getClave()) ) i = 0; //-- Acceso libre
    		log.info("info::>"+i+">");
    		
    		ArrayList<Periodo> arrPeriodos;
    		if(i==0)
    		{ 	
    			
    			lstResultado = seguridadManager.getOpciones(control.getUsuario(), CommonConstants.SGA_BIB);
    			lstPerfiles = (ArrayList<UsuarioPerfil>)seguridadManager.getPerfiles(control.getUsuario(),CommonConstants.SGA_BIB);
    			
    			//log.info("TAMA�O DE LA LST DE OPCIONES>"+lstResultado.size());
    			
    			if(lstResultado.size()>0)
    			{
    				//Obtenmos datos del usuario
    				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
    				if ( usuarioSeguridad == null )
    					usuarioSeguridad = new UsuarioSeguridad();
    				//usuarioSeguridad.setOpciones("BIB_MAT|BIB_BUZ|BIB_SAL|BIB_REP|BIB_ADM");
    				String usuOpciones = convierte(lstResultado);
    				usuarioSeguridad.setOpciones(usuOpciones);
    				usuarioSeguridad.setPerfiles(conviertePerfiles(lstPerfiles));
    				usuarioSeguridad.setRoles(lstPerfiles);
    				
    				//Identificando Usuarios Administradores
    				usuarioSeguridad.setTipoUsuario("");
    				if (lstPerfiles!=null){				
    					for (UsuarioPerfil up : lstPerfiles){									
    						if (up.getNombrePerfil().trim().equals(CommonSeguridad.BIB_PERFIL_ADMINISTRADOR) 
    									|| up.getNombrePerfil().trim().equals(CommonSeguridad.BIB_PERFIL_ADMINISTRADOR_TOTAL)){
    							usuarioSeguridad.setTipoUsuario("A");
    						}
    					}
    				}
    				
    				//*** Validar Accesos desde Host ***
    				boolean permiso = true;
    				boolean isApoyo = false;
        			for (UsuarioPerfil perfil : lstPerfiles) {
        				if(		"260".equals(perfil.getCodPerfil()) || 	//Apoyo Ceditec
        						"261".equals(perfil.getCodPerfil()) || 	//Apoyo PCs
        						"300".equals(perfil.getCodPerfil())){	//Trujillo
        					isApoyo = true;
        					break;
        				}
    				}	
        			
    				if(isApoyo){
    					try{
    						permiso=false;
    						String ip = request.getRemoteAddr();
	    					List<AccesoIP> lista = this.biblioMantenimientoManager.GetAllAccesosLista(Integer.parseInt(usuarioSeguridad.getIdUsuario())); //NFException
	    					
	    					for (AccesoIP accesoIP : lista) {
	    						log.info("Comparando: "+ip+"="+accesoIP.getNumIP());
								if(ipMatch(ip, accesoIP.getNumIP())){
									log.info("Si tiene permiso");
									permiso = true;
									break;
								}
							}
    					}catch (Exception e) {
							log.error(e);
							throw e;
						}
        			}
    				//**********************************
    				
    				if(permiso){
    					
	    				//definir sede del usuario que ingresa al sistema.
	    				//String sedeUsuarioBiblio = portalWebBibliotecaManager.sedeUsuario(control.getUsuario());
	    				//String sedeUsuarioBiblio = portalWebBibliotecaManager.sedeUsuario(usuarioSeguridad.getIdUsuario());
	    				//System.out.println("usuarioSeguridad.getCodUsuario():"+control.getUsuario());
	    				String sedeUsuarioBiblio = portalWebBibliotecaManager.sedeUsuario(control.getUsuario());
	    				if (sedeUsuarioBiblio==null) sedeUsuarioBiblio="";
	    				
	    				usuarioSeguridad.setSede(sedeUsuarioBiblio);
	    				    				
	    				if (lstResultado.size()==1 && usuOpciones.contains("BIB_WEB")){
	    					request.setAttribute("mensaje","NA"); 
	    				}else{
	        				usuarioSeguridad.setCodUsuario(control.getUsuario());
	        				
	        				List<OpcionesApoyo> listaOpciones = biblioMantenimientoManager.listaDeOpcionesPorApoyo(usuarioSeguridad.getIdUsuario());
	        				String cadenaOpciones="";
	        				for(OpcionesApoyo opc : listaOpciones){
	        					if(opc.getSeleccionado().equals("S"))
	        						cadenaOpciones += opc.getNomCorto() + "|";
	        				}
	        				usuarioSeguridad.setOpcionesApoyo(cadenaOpciones);
	        				log.info("OPCIONES APOYO:" + usuarioSeguridad.getOpcionesApoyo());
	        				
	        		        String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
	        		        seguridadManager.RegistrarSesion(true,estacioncliente, control.getUsuario(),CommonConstants.SGA_BIB);    				    				
	        				//Obtenemos Periodo Actual.
	        				arrPeriodos = (ArrayList<Periodo>)mtoParametrosGeneralesManager.getPeriodos();
	        				if ( arrPeriodos.size() > 0 ){    				
	        					usuarioSeguridad.setPeriodo("");
	        					for( int z = 0; z < arrPeriodos.size(); z++) {    					
	        						periodo = (Periodo)arrPeriodos.get(z);
	        						if( periodo.getTipo().equals("A"))
	        							usuarioSeguridad.setPeriodo(periodo.getCodigo());
	        					}
	        				}    				
	            			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
	            			request.getSession().setAttribute("usuarioClave", control.getUsuario());
	        				request.setAttribute("mensaje","OK");            				
	        				request.getSession().removeAttribute("reingreso");
	        				request.getSession().removeAttribute("contextoIngreso");
	    				}
	    				
    				}else
    					request.setAttribute("mensaje","NH"); //No tiene accesos 2010-07-15    		

    			}else
    				request.setAttribute("mensaje","NA"); //No tiene accesos 2008-08-25    			    			
    			
    		}
			else{
				request.setAttribute("mensaje",i);
				if (i==-6) {
					request.getSession().setAttribute("usuarioClave", control.getUsuario());
					request.getSession().setAttribute("contextoIngreso", "Si");
					
				}else if (i>0){
					lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_BIB);
					lstPerfiles = (ArrayList<UsuarioPerfil>)seguridadManager.getPerfiles(control.getUsuario(),CommonConstants.SGA_BIB);
	    			if(lstResultado.size()>0){ 
	    				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
	    				if ( usuarioSeguridad == null ) usuarioSeguridad = new UsuarioSeguridad();
	    				String usuOpciones = convierte(lstResultado);
	    				usuarioSeguridad.setOpciones(usuOpciones);
	    				usuarioSeguridad.setPerfiles(conviertePerfiles(lstPerfiles));
	    				usuarioSeguridad.setRoles(lstPerfiles); 
	    				
	    				//String sedeUsuarioBiblio2 = portalWebBibliotecaManager.sedeUsuario(control.getUsuario());
	    				String sedeUsuarioBiblio2 = portalWebBibliotecaManager.sedeUsuario(control.getUsuario());
	    				if (sedeUsuarioBiblio2==null) sedeUsuarioBiblio2="";
	    				usuarioSeguridad.setSede(sedeUsuarioBiblio2);
	    				
	    				if (lstResultado.size()==1 && usuOpciones.contains("BIB_WEB")){
	    					request.setAttribute("mensaje","NA");
	    				}else{
	    					usuarioSeguridad.setCodUsuario(control.getUsuario());
		    		        String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
		    		        seguridadManager.RegistrarSesion(true,estacioncliente, control.getUsuario(),CommonConstants.SGA_BIB);
		    		        arrPeriodos = (ArrayList<Periodo>)mtoParametrosGeneralesManager.getPeriodos();
	        				if ( arrPeriodos.size() > 0 ){    				
	        					usuarioSeguridad.setPeriodo("");
	        					for( int z = 0; z < arrPeriodos.size(); z++) {    					
	        						periodo = (Periodo)arrPeriodos.get(z);
	        						if( periodo.getTipo().equals("A"))
	        							usuarioSeguridad.setPeriodo(periodo.getCodigo());
	        					}
	        				}  
		        			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
		        			request.getSession().setAttribute("usuarioClave", control.getUsuario());        			
		        			request.setAttribute("mensaje",i);
		        			request.getSession().removeAttribute("reingreso");
		    				request.getSession().removeAttribute("contextoIngreso");
	    				}	    				
	    				
	    			}
				}
				
			}
    	}    	
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/seguridad/bib_logeo","control",control);
    }

	private boolean ipMatch(String ip, String patron) {
		try{
			
			if(ip.equals(patron))return true;
			
		}catch (Exception e) {
			log.error(e);
		}
		return false;
	}
	
	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}

	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}  

	private String conviertePerfiles(ArrayList<UsuarioPerfil> lstPerfiles) {
		String cad="";
		for(int i=0;i<lstPerfiles.size();i++)
			cad = cad + lstPerfiles.get(i).getDscPerfil()+"|";				
		return cad;
	}	
}
