package com.tecsup.SGA.web.seguridad.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;

import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.reclutamiento.controller.AdjuntarCVFormController;
import com.tecsup.SGA.web.seguridad.command.SeguridadCommand;

public class SeguridadRecFormController   extends SimpleFormController{

	private static Log log = LogFactory.getLog(SeguridadRecFormController.class);
	
	private SeguridadManager seguridadManager;
	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	
	/*SETTER*/	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}	
	public SeguridadRecFormController(){
		super();
		setCommandClass(SeguridadCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	SeguridadCommand command = new SeguridadCommand();
    	   	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	SeguridadCommand control = (SeguridadCommand) command;
    	String cadena=""; 	UsuarioSeguridad usuarioSeguridad = null;
    	List lstResultado=null;
    	List<UsuarioPerfil> lstPerfiles=null;
    	//log.info("OPERACION|"+control.getOperacion()+"|");
    	
    	if(control.getOperacion().equalsIgnoreCase("ACCESO"))
    	{	
    		//PARA CAMBIAR EN TECSUP
    		int i = seguridadManager.getValidaUsuario(control.getUsuario(),control.getClave());
    		
    		if( SeguridadBean.esAdministrativo(request.getRemoteAddr(),control.getClave()) ) i = 0; //-- Acceso libre
    		log.info("info::>"+i+">");

    		ArrayList<Periodo> arrPeriodos;
    		if(i==0){
    			
    			lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_REC);    			
    			lstPerfiles = seguridadManager.getPerfiles(control.getUsuario(), CommonConstants.SGA_REC);
    			//log.info("Opciones" + lstResultado.size() + "|" );
    			if(lstResultado.size()>0)
    			{	
					//Obtenmos datos del usuario
					usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
					if ( usuarioSeguridad == null ) usuarioSeguridad = new UsuarioSeguridad();
    				usuarioSeguridad.setOpciones(convierte(lstResultado));
    				usuarioSeguridad.setCodUsuario(control.getUsuario());
    				usuarioSeguridad.setRoles(lstPerfiles);
    				String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
    				seguridadManager.RegistrarSesion(true, estacioncliente, control.getUsuario(), CommonConstants.SGA_REC);    				
    				    				
        			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
        			request.getSession().setAttribute("usuarioClave", control.getUsuario());
        			request.setAttribute("mensaje","OK");
        			request.getSession().removeAttribute("reingreso");
        			request.getSession().removeAttribute("contextoIngreso");
    			}
    			else    			
    				request.setAttribute("mensaje",-9);    			
    		} else {
    			if (i==-6) {
    				request.getSession().setAttribute("usuarioClave", control.getUsuario());
    				request.getSession().setAttribute("contextoIngreso", "Si");
    				request.setAttribute("mensaje",i);
    			}else if (i>0){
    				lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_REC);    			
        			lstPerfiles = seguridadManager.getPerfiles(control.getUsuario(), CommonConstants.SGA_REC);
        			if(lstResultado.size()>0){ 
        				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
        				if ( usuarioSeguridad == null ) usuarioSeguridad = new UsuarioSeguridad();
        				usuarioSeguridad.setOpciones(convierte(lstResultado));
        				usuarioSeguridad.setCodUsuario(control.getUsuario());
        				usuarioSeguridad.setRoles(lstPerfiles);
        				String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
        				seguridadManager.RegistrarSesion(true, estacioncliente, control.getUsuario(), CommonConstants.SGA_REC);
        				request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
            			request.getSession().setAttribute("usuarioClave", control.getUsuario());
            			request.setAttribute("mensaje",i);
            			request.getSession().removeAttribute("reingreso");
            			request.getSession().removeAttribute("contextoIngreso");
        			}
    			}
    			
    			request.setAttribute("mensaje",i);
    			
    		}    		
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/seguridad/rec_logeo","control",control);
    }

	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}

	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}  

	
}
