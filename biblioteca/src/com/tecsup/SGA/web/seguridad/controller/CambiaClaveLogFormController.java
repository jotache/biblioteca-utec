package com.tecsup.SGA.web.seguridad.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.seguridad.command.ChangePasswordCommand;

public class CambiaClaveLogFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(CambiaClaveLogFormController.class);
	private SeguridadManager seguridadManager;
	/**
	 * @param seguridadManager the seguridadManager to set
	 */
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.AbstractFormController#formBackingObject(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ChangePasswordCommand changePasswordCommand = new ChangePasswordCommand();
		String msg = (String) request.getParameter("msg");
		if (msg==null) msg="";
		if (!(request.getSession().getAttribute("contextoIngreso")==null) && msg.equals("1")){
			request.setAttribute("mensaje", "1");
		} else {				
			request.setAttribute("mensaje", "0");
		}
		return changePasswordCommand;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.SimpleFormController#onSubmit(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.validation.BindException)
	 */
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		ChangePasswordCommand changePasswordCommand = (ChangePasswordCommand) command;
		
		if (changePasswordCommand.getOperacion().equals("SALIR")) {
			
			request.getSession().removeAttribute("usuarioSeguridad");
    		response.sendRedirect("/SGA/logeoLogistica.html");
    		
		} else if (changePasswordCommand.getOperacion().equals("CAMBIAR_CLAVE")) {
			
			UsuarioSeguridad usuarioSeguridad = null;
	    	usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
						
	    	String username = null;
	    	String existe = null;
	    	if (usuarioSeguridad != null) {
	    		existe = "1";
	    		username = usuarioSeguridad.getCodUsuario();
	    	} else {
	    		existe ="0";
	    		String usuarioclave = (String) request.getSession().getAttribute("usuarioClave");
	    		username = usuarioclave;
	    	}
	    			    			
			String claveActual = changePasswordCommand.getPasswordOld();
			String claveNueva = changePasswordCommand.getPasswordNew();			
			
	    	int validacion=0;
	    	if (existe.equals("1")) 
	    		validacion=seguridadManager.getValidaUsuario(username, claveActual);
	    	else
	    		validacion=1;
	    	
	    	System.out.println("validacion:" + validacion);
	    	
			if (validacion>=0) {
				//procesar cambio de clave...
				int result = seguridadManager.GrabarFirma(username, claveNueva);
				System.out.println("result:"+result);
				if (result >= 0) {
					//si todo esta ok "validar usuario?" y enviar a ${ctx}/Evaluaciones.html
					seguridadManager.getValidaUsuario(username, claveNueva);
					if (existe.equals("0")){
						usuarioSeguridad = new UsuarioSeguridad();
					}												
					List lstResultado=null;
					lstResultado = seguridadManager.getOpciones(username,CommonConstants.SGA_LOG);
					if(lstResultado.size()>0) {
						usuarioSeguridad.setOpciones(convierte(lstResultado));
						usuarioSeguridad.setCodUsuario(username);
					}		
					request.getSession().removeAttribute("contextoIngreso");
					request.getSession().removeAttribute("usuarioSeguridad");
					System.out.println("removido usuarioSeguridad");
					request.getSession().setAttribute("reingreso", "Si");
					response.sendRedirect("/SGA/Logistica.html");
				} else if (result == -1) {				
					request.setAttribute("mens", result);
				} else if (result == -2) {
					request.setAttribute("mens", result);				
				} else if (result == -3) {
					request.setAttribute("mens", result);				
				} else if (result == -4) {
					request.setAttribute("mens", result);
				}	
			} else {
				request.setAttribute("mens", -5);
			}			
		}		
		return new ModelAndView("/seguridad/log_cambia_clave","changePasswordCommand",changePasswordCommand);
	}
	
	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}
	
}
