package com.tecsup.SGA.web.seguridad.controller;
        
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.seguridad.command.SeguridadCommand;

public class SeguridadEvaFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(SeguridadEvaFormController.class);
	
	private SeguridadManager seguridadManager;
	
	
	/*SETTER*/	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;		
	}
	
	public SeguridadEvaFormController(){
		super();
		setCommandClass(SeguridadCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	 	
    	SeguridadCommand command = new SeguridadCommand();
    	   	       
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);    	
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {    	    	
    	SeguridadCommand control = (SeguridadCommand) command;
    	//String cadena=""; 	String pagina ="/seguridad/eva_logeo";    	
    	UsuarioSeguridad usuarioSeguridad = null;
    	Periodo periodo;
    	List lstResultado=null;    	
    	List<UsuarioPerfil> lstPerfiles=null;
    	log.info("OPERACION:"+control.getOperacion());
    	    	
    	if(control.getOperacion().equalsIgnoreCase("ACCESO"))
    	{	
    		//PARA CAMBIAR EN TECSUP    		
    		control.setUsuario(control.getUsuario().toLowerCase());
    		int i = seguridadManager.getValidaUsuario(control.getUsuario(),control.getClave());
    		log.error(request.getRemoteAddr());
    		if( SeguridadBean.esAdministrativo(request.getRemoteAddr(),control.getClave())) i = 0; //-- Acceso libre
    		log.info("info::>"+i+">");
    		
    		
    		
    		ArrayList<Periodo> arrPeriodos;
    		if(i==0)
    		{ 		    			    			
    			lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_EVA);
    			lstPerfiles = seguridadManager.getPerfiles(control.getUsuario(), CommonConstants.SGA_EVA);
    			
    			//log.info("TAMA�O DE LA LST DE OPCIONES>"+lstResultado.size());
    			if(lstResultado.size()>0){    				
    				//Obtenmos datos del usuario
    				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
    				if ( usuarioSeguridad == null ) usuarioSeguridad = new UsuarioSeguridad();
    				
    				usuarioSeguridad.setOpciones(convierte(lstResultado));
    				usuarioSeguridad.setRoles(lstPerfiles);    				
    				usuarioSeguridad.setCodUsuario(control.getUsuario());
    				
    				//JHPR: 2008-04-11 registrar ingreso de sesion en "seguridad.seg_log"    				    		        
    		        String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
    		        seguridadManager.RegistrarSesion(true,estacioncliente, control.getUsuario(),CommonConstants.SGA_EVA);
    		        
        			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
        			request.getSession().setAttribute("usuarioClave", control.getUsuario());        			
    				request.setAttribute("mensaje","OK");    				
    				//JHPR: 2008-04-10 Permtir Cambio de clave.
    				request.getSession().removeAttribute("reingreso");
    				request.getSession().removeAttribute("contextoIngreso");
    			} else
    				request.setAttribute("mensaje","NA"); //No tiene accesos 2008-08-25
    		} else {
				//JHPR: 2008-04-10 Permtir Cambio de clave.
				if (i==-6) {
					//usuarioSeguridad.setCodUsuario(control.getUsuario());
					request.getSession().setAttribute("usuarioClave", control.getUsuario());
					request.getSession().setAttribute("contextoIngreso", "Si");
					request.setAttribute("mensaje",i);
				}else if (i>0){
	    			lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_EVA);
	    			lstPerfiles = seguridadManager.getPerfiles(control.getUsuario(), CommonConstants.SGA_EVA);
	    			if(lstResultado.size()>0){ 
	    				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
	    				if ( usuarioSeguridad == null ) usuarioSeguridad = new UsuarioSeguridad();
	    				usuarioSeguridad.setOpciones(convierte(lstResultado));
	    				usuarioSeguridad.setRoles(lstPerfiles);    				
	    				usuarioSeguridad.setCodUsuario(control.getUsuario());
	    		        String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();
	    		        seguridadManager.RegistrarSesion(true,estacioncliente, control.getUsuario(),CommonConstants.SGA_EVA);
	        			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
	        			request.getSession().setAttribute("usuarioClave", control.getUsuario());        			
	        			request.setAttribute("mensaje",i);
	        			request.getSession().removeAttribute("reingreso");
	    				request.getSession().removeAttribute("contextoIngreso");
	    			}
					
				}
				
				request.setAttribute("mensaje",i);
				
			}
    	}
		
	    return new ModelAndView("/seguridad/eva_logeo","control",control);
    }

	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}
	
}
