package com.tecsup.SGA.web.seguridad.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.service.encuestas.ActualizacionDatosManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.seguridad.command.SeguridadCommand;

public class SeguridadEncFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(SeguridadEncFormController.class);
	
	private SeguridadManager seguridadManager;
	private ActualizacionDatosManager actualizacionDatosManager;

	public ActualizacionDatosManager getActualizacionDatosManager() {
		return actualizacionDatosManager;
	}

	public void setActualizacionDatosManager(
			ActualizacionDatosManager actualizacionDatosManager) {
		this.actualizacionDatosManager = actualizacionDatosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	SeguridadCommand command = new SeguridadCommand();
    	  log.info("formBackingObject:FIN");
          return command;
      } 
  	
      protected void initBinder(HttpServletRequest request,
              ServletRequestDataBinder binder) {
      	NumberFormat nf = NumberFormat.getNumberInstance();
      	binder.registerCustomEditor(Long.class,
  	                  new CustomNumberEditor(Long.class, nf, true));
      	
      	binder.registerCustomEditor(byte[].class,
                  new ByteArrayMultipartFileEditor());
      }
      /**
       * Redirect to the successView when the cancel button has been pressed.
       */
      public ModelAndView processFormSubmission(HttpServletRequest request,
                                                HttpServletResponse response,
                                                Object command,
                                                BindException errors)
      throws Exception {
          //this.onSubmit(request, response, command, errors);
      	return super.processFormSubmission(request, response, command, errors);
      }
      
      public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
              BindException errors)
      		throws Exception {
      	log.info("onSubmit:INI");
      	
      	SeguridadCommand control = (SeguridadCommand) command;
    	String cadena=""; 	//String pagina ="/seguridad/eva_logeo";    	
    	UsuarioSeguridad usuarioSeguridad = null;
    	Periodo periodo;
    	List lstResultado=null;
    	ArrayList<UsuarioPerfil> lstPerfiles=null;
    	
    	log.info("OPERACION>"+control.getOperacion());
    	
    	if(control.getOperacion().equalsIgnoreCase("ACCESO"))
    	{	
    		//PARA CAMBIAR EN TECSUP 
    		int i = seguridadManager.getValidaUsuario(control.getUsuario(),control.getClave());
    		
    		if( SeguridadBean.esAdministrativo(request.getRemoteAddr(),control.getClave()) ) i = 0; //-- Acceso libre
    		i = 0; //-- Acceso libre
    		
    		log.info("info>"+i+">");
    		ArrayList<Periodo> arrPeriodos;
    		if(i==0)
    		{ 			
    			lstResultado = seguridadManager.getOpciones(control.getUsuario(),CommonConstants.SGA_ENC);
    			lstPerfiles = (ArrayList<UsuarioPerfil>)seguridadManager.getPerfiles(control.getUsuario(),CommonConstants.SGA_ENC);
    			log.info("TAMA�O DE LA LST DE OPCIONES>"+lstResultado.size());
    			
    			if(lstResultado.size()>0)
    			{	
    				//Obtenmos datos del usuario
    				usuarioSeguridad = this.seguridadManager.getDatosUsuario(control.getUsuario());
    				if ( usuarioSeguridad == null )
    					usuarioSeguridad = new UsuarioSeguridad();
    				
    				usuarioSeguridad.setOpciones(convierte(lstResultado));
    				usuarioSeguridad.setPerfiles(conviertePerfiles(lstPerfiles));
    				//usuarioSeguridad.setOpciones("ENC_CONGEN|ENC_APLSEG|ENC_CONREP|ENC_ADMSIS|ENC_ACTDAT|ENC_PUBLI");
    				usuarioSeguridad.setCodUsuario(control.getUsuario());
    				String verificarAlumno="1";
    				EncuestaAlumno encuestaAlumno= new EncuestaAlumno();
    				List lista= new ArrayList();
    				lista=this.actualizacionDatosManager.GetVerificarAlumno(usuarioSeguridad.getIdUsuario());
    				if(lista!=null)
    				{ if(lista.size()>0)
    					{ encuestaAlumno=(EncuestaAlumno)lista.get(0);
    					  verificarAlumno=encuestaAlumno.getCodPerfil();
    					}
    					
    				}
    				//************************************************************
    				String verTieneEncuesta="1";
    				Usuario usuario=new Usuario();
    				List lista2= new ArrayList();
    				lista2=this.actualizacionDatosManager.GetVerificarTieneEncuesta(usuarioSeguridad.getIdUsuario());
    				if(lista2!=null){
    					if(lista2.size()>0){
    						usuario=(Usuario)lista2.get(0);
    						verTieneEncuesta=usuario.getTieneEncuesta();
    					}
    				}
    				//************************************************************
    				
    				//*******VALIDA LOS PERFILES EN DURO COMENTAR EN TECSUP
    				/*String perfil = request.getParameter("cboPerfil");
    				log.info("EL PREFIL DEL USUARIO ES>>"+perfil+">>");
    				if("0".equalsIgnoreCase(perfil)){
    					//ADM SISTEMA
    					usuarioSeguridad.setOpciones("ENC_CONGEN|ENC_APLSEG|ENC_CONREP_3|ENC_CONREP_4|ENC_CONREP_5|ENC_ADMSIS|ENC_ACTDAT");
    					//usuarioSeguridad.setOpciones("ENC_ADMSIS");
    				}else if("1".equalsIgnoreCase(perfil)){
    					//ADM ENCUESTAS
    					usuarioSeguridad.setOpciones("ENC_CONGEN|ENC_APLSEG|ENC_CONREP_4|ENC_CONREP_5");    					
    				}else if("2".equalsIgnoreCase(perfil)){
    					//PROFESOR
    					usuarioSeguridad.setOpciones("ENC_CONREP_3");
    				}else if("3".equalsIgnoreCase(perfil)){
    					//ALUMNO
    					usuarioSeguridad.setOpciones("ENC_ACTDAT");
    				}*/
    				//*******FIN VALIDA LOS PERFILES EN DURO COMENTAR EN TECSUP
    				
    				log.info("LISTA DE OPCIONES>>"+usuarioSeguridad.getOpciones()+">>");
    				
    				request.getSession().setAttribute("verificarTieneEncuesta", verTieneEncuesta);
    				request.getSession().setAttribute("verificarAlumno", verificarAlumno);
        			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
    				request.setAttribute("mensaje","OK");
    			}
    			else{
    				request.setAttribute("mensaje","-9");
    			}
    		}
			else
				request.setAttribute("mensaje",i);
    	 
    	}    	
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/seguridad/enc_logeo","control",control);
    }

	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}
	private String conviertePerfiles(ArrayList<UsuarioPerfil> lstPerfiles) {
		String cad="";
		for(int i=0;i<lstPerfiles.size();i++)
			cad = cad + lstPerfiles.get(i).getDscPerfil()+"|";				
		return cad;
	}
	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
}
