package com.tecsup.SGA.web.seguridad.controller;

import java.io.File;
import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.reclutamiento.ReclutaManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.reclutamiento.command.AdjuntarCVCommand;
import com.tecsup.SGA.web.reclutamiento.controller.AdjuntarCVFormController;
import com.tecsup.SGA.web.seguridad.command.SeguridadCommand;

public class SeguridadFormController   extends SimpleFormController{

	private static Log log = LogFactory.getLog(SeguridadFormController.class);
	
	private SeguridadManager seguridadManager;
	
	/*SETTER*/
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
	public SeguridadFormController(){
		super();
		setCommandClass(SeguridadCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	SeguridadCommand command = new SeguridadCommand();
    	   	
        log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	System.out.println("onSubmit:INI");
    	SeguridadCommand control = (SeguridadCommand) command;
    	String cadena="";
    	UsuarioSeguridad usuarioSeguridad = null; 
    	log.info("ope>"+control.getOperacion());
    	if(control.getOperacion().equalsIgnoreCase("ACCESO"))
    	{	
    		int i = seguridadManager.getValidaUsuario(control.getUsuario(),control.getClave());
    	
    		if(request.getRemoteAddr().startsWith("192.168.116."))
        	i = 0; //-- Acceso libre
    	
    		log.info("info>"+i+">");
    		//request.setAttribute("mensaje","");
    		List lstResultado=null;
    		if(i==0)
    		{	lstResultado = seguridadManager.getOpciones(control.getUsuario(), "seva");
    			
    			
    			if(lstResultado.size()>0)
    			{	usuarioSeguridad = new UsuarioSeguridad();
    				usuarioSeguridad.setOpciones(convierte(lstResultado));
    				//usuarioSeguridad.setCodUsuario(control.getUsuario());
    				usuarioSeguridad.setCodUsuario("jmoran");
    			}
    			
    			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
    			log.info("tam"+lstResultado.size());
    		}
    	
    	}
    	
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/seguridad/seg_logeo","control",control);
    }

	private String convierte(List lstResultado) {
		String cad="";
		for(int i=0;i<lstResultado.size();i++)
			cad = cad+((SeguridadBean)lstResultado.get(i)).getOpcion()+"|";				
		return cad;
	}  

	
}
