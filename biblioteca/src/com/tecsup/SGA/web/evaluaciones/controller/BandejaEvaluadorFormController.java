package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.BandejaEvaluadorCommand;
import com.tecsup.SGA.web.evaluaciones.command.GestAdministrativaBandejaCommand;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonSeguridad;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.Recluta;

public class BandejaEvaluadorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(BandejaEvaluadorFormController.class);
	
	private OperDocenteManager operDocenteManager;
	private ComunManager comunManager;
	
	/*SETTER*/	
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public BandejaEvaluadorFormController(){
		super();
		setCommandClass(BandejaEvaluadorCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	    	
    	BandejaEvaluadorCommand command = new BandejaEvaluadorCommand();
    	UsuarioSeguridad usuarioSeguridad = null;    	
    	usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	/*Recogiendo valores - Codigos*/
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodSede((String)request.getParameter("txhCodSede"));
    	
    	
    	if ( command.getCodSede() != null )//Si la sede es nula
    		request.getSession().setAttribute("sedeEvaluador", command.getCodSede());
    	else
    		command.setCodSede((String)request.getSession().getAttribute("sedeEvaluador"));
    	if ( usuarioSeguridad != null ){ 
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
        	command.setCodEval(usuarioSeguridad.getIdUsuario());
        	command.setNomEvaluador(usuarioSeguridad.getNomUsuario());
    	}
    	if ( command.getCodPeriodo() != null )
    	{
    		cargaPeriodo(command);
        	
        	inicializaData(command, request);
    	}


    	//Llenado Tabla Interes - Session
    	request.getSession().setAttribute("listaCursos", command.getListaCursos());
    	
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	BandejaEvaluadorCommand control = (BandejaEvaluadorCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	inicializaData(control, request);
    	if("BUSCAR".equalsIgnoreCase(control.getOperacion())){
    		String tipo = (String)request.getAttribute("tipoUsuario");
    		
    		busquedaByJefe(control, tipo, control.getCodPeriodo());    		
    	}
    	//Llenado Tabla Interes - Session
    	request.getSession().setAttribute("listaCursos", control.getListaCursos());		
	    return new ModelAndView("/evaluaciones/cargaOperativa/eva_carga_bandeja_evaluador","control",control);		
    }
    
    /*Metodos*/
    private void cargaPeriodo(BandejaEvaluadorCommand command){
    	Periodo periodo;
    	ArrayList<Periodo> listPeriodo;
    	try{
    		listPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoById(command.getCodPeriodo());
    		if ( listPeriodo != null )
    			if ( listPeriodo.size() > 0)
    			{
    				periodo =listPeriodo.get(0);
    				command.setPeriodo(periodo.getNombre());
    			}
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
	private void inicializaData(BandejaEvaluadorCommand command, HttpServletRequest request){
		CursoEvaluador lObject;
    	String tipoUsuario = "0";//0 -> Usuario normal,1 -> JefeDepartamenet 2 -> Director 
		
    	log.info("command.getCodOpciones(): " + command.getCodOpciones());
    	
		//Tabla Interes
		if (command.getCodOpciones() != null)
		{
			if ( command.getCodOpciones().indexOf(CommonSeguridad.EVA_JDEP) > 0 )
				tipoUsuario = "1";
			else if ( command.getCodOpciones().indexOf(CommonSeguridad.EVA_DIRA) > 0 )
				tipoUsuario = "2";
		}
		log.info("tipoUsuario:" + tipoUsuario);
		
		
		ArrayList curso = (ArrayList)this.operDocenteManager.getAllCursos(command.getCodPeriodo(), command.getCodEval(),"","",""
				, command.getCboEvaluadores(), command.getTxtCurso());
		
    	request.setAttribute("tipoUsuario",tipoUsuario);
    	
    	if (tipoUsuario!="0"){
    		command.setListaEvaluadores(this.operDocenteManager.getEvaluadoresByJefeDep(command.getCodEval(), 
    						command.getCodSede(), command.getCodPeriodo(), tipoUsuario));
    	}
    	else{
    		    		
    		command.setListaCursos(this.operDocenteManager.getAllCursos(command.getCodPeriodo(), command.getCodEval()
        			,"",command.getCodSede() + "&" +  tipoUsuario
        			,""
        			, ""
        			, ""));//En el parametro de especialidad paso la sede.
    	}
    		
    	if (curso!= null){    	
	    	if (curso.size()>0){
	    		lObject = (CursoEvaluador) curso.get(0);
	    		//command.setPeriodo(lObject.getPeriodo());
	    		command.setEvaluador(lObject.getEvaluador());
	    	}
    	}
    	
	}
	
	private void busquedaByJefe(BandejaEvaluadorCommand command, String tipoUsuario, String periodo){
				
		command.setListaCursos(this.operDocenteManager.getAllCursos(periodo, command.getCodEval()
    			,"",command.getCodSede() + "&" +  tipoUsuario
    			,""
    			, command.getCboEvaluadores()
    			, command.getTxtCurso()));//En el parametro de especialidad paso la sede.
	}
	
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}
    
}
