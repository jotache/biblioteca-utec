package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.common.*;

public class MtoEvaluacionHibridoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoEvaluacionHibridoFormController.class);
	private SistevalHibridoManager sistevalHibridoManager;

	public void setSistevalHibridoManager(SistevalHibridoManager sistevalHibridoManager) {
		this.sistevalHibridoManager = sistevalHibridoManager;
	}   
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	MtoEvaluacionHibridoCommand command = new MtoEvaluacionHibridoCommand();
    	
		//obteniendo los valores de la sesion
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	    	
    	command.setListEvaluaciones(this.sistevalHibridoManager.getAllSistevalHibrido(""));    	
    	request.getSession().setAttribute("listaEvaluaciones",command.getListEvaluaciones());    	    	
   	   	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
			
		MtoEvaluacionHibridoCommand control = (MtoEvaluacionHibridoCommand) command;
		log.info("OPERACION:" + control.getOperacion());
		if(control.getOperacion().trim().equals("ELIMINAR")){
    		DeleteProductoHibrido(control);
    		control.setSeleccion("");
		}    	
    	control.setListEvaluaciones(this.sistevalHibridoManager.getAllSistevalHibrido(""));
    	request.getSession().setAttribute("listaEvaluaciones",control.getListEvaluaciones());
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_evaluacion_hibrido","control",control);		
    }

    private String DeleteProductoHibrido(MtoEvaluacionHibridoCommand control)
    {
    	try
    	{	SistevalHibrido obj=new SistevalHibrido();
    		obj.setCodSec(control.getSeleccion());
    	
    		if (!obj.getCodSec().trim().equals("") ){
    			obj.setCodSec(this.sistevalHibridoManager.DeleteProductoHibrido(obj, control.getCodEvaluador()));    			
    	    }    		
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
        
}
