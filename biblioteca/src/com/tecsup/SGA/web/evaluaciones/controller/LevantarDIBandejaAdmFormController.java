package com.tecsup.SGA.web.evaluaciones.controller;

import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.service.evaluaciones.GestionAdministradtivaManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.IncidenciaBandejaDocCommand;


public class LevantarDIBandejaAdmFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(LevantarDIBandejaAdmFormController.class);
	private OperDocenteManager operDocenteManager;
	private GestionAdministradtivaManager gestionAdministradtivaManager;
	
	public void setGestionAdministradtivaManager(
			GestionAdministradtivaManager gestionAdministradtivaManager) {
		this.gestionAdministradtivaManager = gestionAdministradtivaManager;
	}

	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    				throws ServletException {
		
		IncidenciaBandejaDocCommand command = new IncidenciaBandejaDocCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
		
    	if ( command.getCodCurso() != null )
    	{
	    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEvaluador(), command.getCodCurso());
	    	inicializaSeccion(command, command.getCodCurso());
	    	//Seteamos el primer valor del combo.
	    	ArrayList<CursoEvaluador> listSeccion = (ArrayList<CursoEvaluador>)command.getListaSeccion();
	    	if ( listSeccion != null )
		    	if ( listSeccion.size() > 0 )
		    		command.setCodSeccion(listSeccion.get(0).getCodSeccion());
	    	//LLenamos lista de alumnos.
	    	buscarAlumnos(command);
    	}
    	//Llenado Tabla Interes - Session
    	request.getSession().setAttribute("listaBandejaDIs", command.getListaBandeja());
    	
		return command;
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors) throws Exception {
     				
		IncidenciaBandejaDocCommand control = (IncidenciaBandejaDocCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	inicializaSeccion(control, control.getCodCurso());
    	
    	if ( control.getOperacion().trim().equals("BUSCAR")){
    		buscarAlumnos(control);
    	}else if (control.getOperacion().trim().equals("GRABAR")){
    		
    		//System.out.println("control.getCodSeccion():"+control.getCodSeccion());    		
    		String respuesta = levantarDI(control);
    		log.info("RPTA:"+ respuesta);
    		if ( respuesta == null ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("-1")) control.setMsg("ERROR");
    		else {
    			control.setMsg("OK");
    		}
    		
    		control.setCodAlumno("");
    		//grab�
    		buscarAlumnos(control);
    	}
    	
    	
    	request.getSession().setAttribute("listaBandejaDIs", control.getListaBandeja());
		
	    return new ModelAndView("/evaluaciones/gestionAdministrativo/eva_levantardibandeja_adm","control",control);
	}
	
	 private String levantarDI(IncidenciaBandejaDocCommand control){
		 try{
			 return this.gestionAdministradtivaManager.updateLevantarDI(control.getCodCurso(), control.getCodTipoSesionDefecto(), control.getCodSeccion(), 
					 control.getCodAlumno(), "0001", "1", control.getCodEvaluador());			 
			 
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 return null;
	 }
	
	private void inicializaDataCurso(IncidenciaBandejaDocCommand command, String strPeriodo, String strEval, String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval
				, "", "", strCodCurso, "","");
		
    	if (dataCurso.size()>0){
    		lObject = (CursoEvaluador) dataCurso.get(0);
    		command.setPeriodoVigente(lObject.getPeriodo());
    		command.setCiclo(lObject.getCiclo());
    		command.setProducto(lObject.getProducto());
    		command.setNomEvaluador(lObject.getEvaluador());
    		command.setCurso(lObject.getCurso());
    		command.setEscialidad(lObject.getEspecialidad());
    		command.setSistemaEval(lObject.getSistEval());
    	}
	}
	
	private void buscarAlumnos(IncidenciaBandejaDocCommand control){	
		control.setListaBandeja(
				this.operDocenteManager.listarDesaprobadosXDI(control.getCodCurso(), control.getCodSeccion(), control.getNombreAlumno(), control.getApellidoAlumno()));
		
	}
	
    private void inicializaSeccion(IncidenciaBandejaDocCommand command, String strCodCurso){
    	ArrayList arrTipoSesiones = (ArrayList)this.operDocenteManager.getAllSesionXCurso(strCodCurso); 
    	CursoEvaluador cursoEvaluador;
    	boolean flag = true;
    	if ( arrTipoSesiones != null )
    	{
    		//BUSCO TIPO SESION TEORIA
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TEO))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO LABORATORIO 
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_LAB))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO TALLER
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TAL))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    	}    
    }
    
}
