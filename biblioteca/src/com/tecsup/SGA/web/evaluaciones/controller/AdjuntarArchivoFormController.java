package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.io.File;
import java.io.FilenameFilter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.web.evaluaciones.command.AdjuntarArchivoCommand;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormacionEmpresa;

public class AdjuntarArchivoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdjuntarArchivoFormController.class);
    
	OperDocenteManager operDocenteManager;
	
    public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public AdjuntarArchivoFormController() {    	
        super();        
        setCommandClass(AdjuntarArchivoCommand.class);        
    }

	/*public void setConfInicioManager(ConfInicioManager confInicioManager) {
		this.confInicioManager = confInicioManager;
	}*/

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    		
    	AdjuntarArchivoCommand command = new AdjuntarArchivoCommand();
    	command.setCodAlumno((String)request.getParameter("txhcodAlumno")); 
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEva((String)request.getParameter("txhCodEva"));
    	
     	request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
    	request.setAttribute("strExtensionXLS", CommonConstants.GSTR_EXTENSION_XLS);
    	request.setAttribute("strExtensionDOCX", CommonConstants.GSTR_EXTENSION_DOCX);
    	request.setAttribute("strExtensionXLSX", CommonConstants.GSTR_EXTENSION_XLSX);
    	request.setAttribute("strExtensionPDF", CommonConstants.GSTR_EXTENSION_PDF);
    	    	
        return command;
    }
	
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    

  
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    
    	AdjuntarArchivoCommand control = (AdjuntarArchivoCommand) command;
    	FormacionEmpresa obj = new FormacionEmpresa();
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta;
    		byte[] bytesCV = control.getTxtCV();
    		String filenameCV;
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		 String uploadDirCV = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_CARGA_OPERATIVA;
    		 String sep = System.getProperty("file.separator");
    	
  	        	 filenameCV = CommonConstants.FILE_NAME_EMPRESA + control.getCodAlumno() + "." +control.getExtCv();
  	        	 File uploadedFileCV = new File(uploadDirCV + sep + filenameCV);
  	        	 FileCopyUtils.copy(bytesCV, uploadedFileCV);
  	           
  	           obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
  	           obj.setCodPeriodo(control.getCodPeriodo());
    		   obj.setCodAlumno(control.getCodAlumno());
    		   obj.setCodCurso(CommonConstants.COD_FORMACION_EMPRESA);
    		   obj.setNombreNuevoArchivo(filenameCV);
    		   obj.setNombreArchivo(control.getNomArchivo());
    		   
    		rpta = this.operDocenteManager.AdjuntarArchivo(obj,control.getCodEva());
    		log.info("RPTA:" + rpta);
    		if ( rpta.equals("-1")) {
    			control.setMessage("ERROR");
    		}
    		else  control.setMessage("OK");
    	}
    	
    	
	    return new ModelAndView("/evaluaciones/formacionEmpresa/eva_adjuntar_archivo","control",control);		
    }
	
	
}
