package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarEmpresaCommand;
import com.tecsup.SGA.modelo.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.common.CommonConstants;

public class RegistrarEmpresaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RegistrarEmpresaFormController.class);
 	OperDocenteManager operDocenteManager;
	
	 public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
			this.operDocenteManager = operDocenteManager;
		}
	
    public RegistrarEmpresaFormController() {    	
        super();        
        setCommandClass(RegistrarEmpresaCommand.class);     
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	 	
    	RegistrarEmpresaCommand command = new RegistrarEmpresaCommand();
    	
    	command.setNombreAlumno((String)request.getParameter("txhNombreAlumno"));
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEva((String)request.getParameter("txhCodEva"));
    	
    	command.setTipo(CommonConstants.COD_NOTA_EXTERNO);
     	request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_IP + CommonConstants.STR_RUTA_CARGA_OPERATIVA);
        
    	if(!(command.getCodEspecialidad()==null)){
          	if(!command.getCodEspecialidad().trim().equals("")){
          		numero(command);
          		numero1(command);
          		numero2(command);
          		empresa(command);
          		empre(command);
          		empresas(command);
          		informe(command);
          		request.setAttribute("listaEmpresasPasantia", command.getListEmpresasPasasntia());
          		request.setAttribute("listaEmpresasPractica", command.getListEmpresasPractica());
          		
          		request.setAttribute("inf", command.getInf());		
          		command.setVez("1");
          		 request.setAttribute("Bandera", "OK");
          	}
        		}
    	if(!(command.getNombreAlumno()==null) && !(command.getCodAlumno()==null)){
    		command.setCadena(command.getNombreAlumno()+"("+command.getCodAlumno()+")");
    	}
    	else{
    		command.setCadena("");
    	}
    	command.setFlag(CommonConstants.TIPT_fLAG);
    	command.setCodFlagApto(CommonConstants.TIPT_FLAG_APTO);
    	command.setCodFlagNoApto(CommonConstants.TIPT_FLAG_NO_APTO);
    	command.setSab(CommonConstants.TIPT_FLAG_SALA);
    	command.setDom(CommonConstants.TIPT_ETAPA_TODOS);
    	
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado;
    	String res;
    	resultado = "";
    	res = "";
    	RegistrarEmpresaCommand control = (RegistrarEmpresaCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	informe(control);
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		control.setOperacion("");
    		if(control.getTipo().trim().equals("0")){
    			resultado = grabar(control);
    			log.info("RPTA:" + resultado);
    			numero(control);
          		numero1(control);
          		numero2(control);
          		empresa(control);
          		empre(control);
          		empresas(control);
          		informe(control);
          		request.setAttribute("listaEmpresasPasantia", control.getListEmpresasPasasntia());
          		request.setAttribute("listaEmpresasPractica", control.getListEmpresasPractica());
          		control.setFlag(CommonConstants.TIPT_fLAG);
    		}
    		else if(control.getTipo().trim().equals("1")){
    			if(control.getHorasPasatia()==null){
    				control.setHorasPasatia("0");
    			}
    		int numeroInt = Integer.valueOf(control.getCant());
    		int numeroInt2 =Integer.valueOf(control.getHorasPasatia());
    	//	if(numeroInt <= numeroInt2){
    			resultado = grabar1(control);
    			log.info("RPTA:" + resultado);
    			numero(control);
          		numero1(control);
          		numero2(control);
          		empresa(control);
          		empre(control);
          		empresas(control);
          		informe(control);
          		request.setAttribute("listaEmpresasPasantia", control.getListEmpresasPasasntia());
          		request.setAttribute("listaEmpresasPractica", control.getListEmpresasPractica());
       	//	}
    	//	else{
    		//	control.setMen("NO");
    			//}
    		control.setFlag(2);
    		}
    		else  if(control.getTipo().trim().equals("2")){
    			if(control.getHorasParciales()==null){
    				control.setHorasParciales("0");
    			}
    			int numeroInt = Integer.valueOf(control.getCant1());
        		int numeroInt2 =Integer.valueOf(control.getHorasParciales());
        	//	if(numeroInt <= numeroInt2){
        			resultado = grabar2(control);
        			log.info("RPTA:" + resultado);
        			numero(control);
              		numero1(control);
              		numero2(control);
              		empresa(control);
              		empre(control);
              		empresas(control);
              		informe(control);
              		request.setAttribute("listaEmpresasPasantia", control.getListEmpresasPasasntia());
              		request.setAttribute("listaEmpresasPractica", control.getListEmpresasPractica());
              		
        //			}
        	//	else{
        	//		control.setMen("NO");
        		//	}
       		control.setFlag(3);
    		}
    		
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    			resultado.equals("");
    		}
    			else {
    				control.setMsg("OK");
    				resultado.equals("");
    			}
    		control.setOperacion("");
    	}
    	
    	else if (control.getOperacion().trim().equals("MODIFICA")){
    		control.setOperacion("");
    		res = modificar(control);
    		numero(control);
      		numero1(control);
      		numero2(control);
      		empresa(control);
      		empre(control);
      		empresas(control);
      		informe(control);
      		request.setAttribute("listaEmpresasPasantia", control.getListEmpresasPasasntia());
      		request.setAttribute("listaEmpresasPractica", control.getListEmpresasPractica());
	  	
    		if(res.equals("-1")){
    			control.setMens("ERROR");
    			res.equals("");
    		}
    		else{
    			control.setMens("OK");
    			res.equals("");
    		}
    		control.setOperacion("");
    		if(control.getTipo().equals("0")){
    		control.setFlag(1);
    		}
    		else if(control.getTipo().equals("1")){
        		control.setFlag(2);
        		}
    		else if(control.getTipo().equals("2")){
        		control.setFlag(3);
        		}
    	}
    	else if (control.getOperacion().trim().equals("ACTUALIZA")){
    		numero(control);
      		numero1(control);
      		numero2(control);
      		empresa(control);
      		empre(control);
      		empresas(control);
      		informe(control);
      		
      		if(control.getTipo().equals("0")){
        		control.setFlag(1);
        		}
        		else if(control.getTipo().equals("1")){
            		control.setFlag(2);
            		}
        		else if(control.getTipo().equals("2")){
            		control.setFlag(3);
            		}
      		control.setMen("");
      		control.setMens("");
      		control.setMsg("");
      		request.setAttribute("listaEmpresasPasantia", control.getListEmpresasPasasntia());
      		request.setAttribute("listaEmpresasPractica", control.getListEmpresasPractica());
      		control.setVez("2");
      	 request.setAttribute("Bandera", "OK");
      	
    	}
    	control.setOperacion("");
    	control.setTipo("");
    	request.setAttribute("inf", control.getInf());
    	
	    return new ModelAndView("/evaluaciones/formacionEmpresa/eva_registrar_empresa","control",control);		
    }
        
        private String grabar(RegistrarEmpresaCommand control)
        {
        	FormacionEmpresa obj = new FormacionEmpresa();
    		obj.setCodPeriodo(control.getCodPeriodo());
    		obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
    		obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodCurso(CommonConstants.COD_CURSO);
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setTipoPractica(control.getTipo());
    		obj.setNombreEmpresa(control.getEmpresa());
    		obj.setFechaIni(control.getFechaInicio());
    		obj.setFechaFin(control.getFechaFin());
    		obj.setObservacion(control.getObservacion());
    		obj.setCantidaHoras(control.getCaHoras());
    		obj.setCodGrabar(this.operDocenteManager.InsertEmpresa(obj, control.getCodEva()));
        	return obj.getCodGrabar();
        }
        
        private String grabar1(RegistrarEmpresaCommand control)
        {
        	FormacionEmpresa obj = new FormacionEmpresa();
    		obj.setCodPeriodo(control.getCodPeriodo());
    		obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
    		obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodCurso(CommonConstants.COD_CURSO);
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setTipoPractica(control.getTipo());
    		obj.setNombreEmpresa(control.getEmpresa1());
    		obj.setFechaIni(control.getFechaInicio());
    		obj.setFechaFin(control.getFechaFin());
    		obj.setCantidaHoras(control.getCant());
    		obj.setCodEstado(control.getEstado());
    		obj.setCodGrabar(this.operDocenteManager.InsertEmpresa(obj, control.getCodEva()));
        	return obj.getCodGrabar();
        }
        private String grabar2(RegistrarEmpresaCommand control)
        {
        	FormacionEmpresa obj = new FormacionEmpresa();
        	
        	obj.setCodPeriodo(control.getCodPeriodo());
    		obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
    		obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodCurso(CommonConstants.COD_CURSO);
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setTipoPractica(control.getTipo());
    		obj.setNombreEmpresa(control.getEmpresa2());
    		obj.setFechaIni(control.getFechaInicio1());
    		obj.setFechaFin(control.getFechaFin1());
    		obj.setCantidaHoras(control.getCant1());
    		obj.setHoraIni(control.getHoraIni());
    		obj.setHoraFin(control.getHoraFin());
    		obj.setIndSabado(control.getIndSabado());
    		obj.setIndDomingo(control.getIndDomingo());
    		obj.setNota(control.getNota());
    		obj.setCodGrabar(this.operDocenteManager.InsertEmpresa(obj, control.getCodEva()));
        	return obj.getCodGrabar();
        }
        
        private String numero(RegistrarEmpresaCommand command)
        {
        command.setHoras("");	
        FormacionEmpresa obj = new FormacionEmpresa();
        obj.setCodPeriodo(command.getCodPeriodo());
        obj.setCodEspecialidad(command.getCodEspecialidad());
        obj.setCodAlumno(command.getCodAlumno());
        obj.setTipoPractica("0");
        HorasXalumnos nobj = this.operDocenteManager.HorasPorAlumno(obj);	
        if(!(nobj==null)){
    		command.setHorasParciales(nobj.getCantHoras());
    	}
    	else{
    		command.setHorasParciales("0");
    	}
        	return command.getHoras();
        }
        
        private String  numero1(RegistrarEmpresaCommand command)
        {
        command.setHoras("");	
        FormacionEmpresa obj = new FormacionEmpresa();
        obj.setCodPeriodo(command.getCodPeriodo());
        obj.setCodEspecialidad(command.getCodEspecialidad());
        obj.setCodAlumno(command.getCodAlumno());
        obj.setTipoPractica(CommonConstants.COD_FORMACION_EMPRESA);
        
        HorasXalumnos nobj = this.operDocenteManager.HorasPorAlumno(obj);	
        if(!(nobj==null)){
    		command.setHorasParciales(nobj.getCantHoras());
    	}
    	else{
    		command.setHorasParciales("0");
    	}
        	return command.getHorasPasatia();
        }
        
        private String  numero2(RegistrarEmpresaCommand command)
        {
        command.setHoras("");	
        FormacionEmpresa obj = new FormacionEmpresa();
        obj.setCodPeriodo(command.getCodPeriodo());
        obj.setCodEspecialidad(command.getCodEspecialidad());
        obj.setCodAlumno(command.getCodAlumno());
        obj.setTipoPractica("2");
        
        HorasXalumnos nobj = this.operDocenteManager.HorasPorAlumno(obj);	
        	if(!(nobj==null)){
        		command.setHorasParciales(nobj.getCantHoras());
        	}
        	else{
        		command.setHorasParciales("0");
        	}
        		return command.getHorasParciales();
        }
        
        private String empresa(RegistrarEmpresaCommand command){
        	FormacionEmpresa obj = new FormacionEmpresa();        	         	
        	obj.setCodPeriodo(command.getCodPeriodo());
        	obj.setCodEspecialidad(command.getCodEspecialidad());
        	obj.setCodAlumno(command.getCodAlumno());
        	obj.setTipoPractica("0");
        	command.setListEmpresas(this.operDocenteManager.EmpresaXAlumno(obj));
        	command.setTotemp(command.getListEmpresas().size());
        	List lst  = command.getListEmpresas();
        	if(lst!=null && lst.size()>0){
        		ConsultaEmpresa consultaEmpresa =null;
        		for(int i=0;i<lst.size();i++){
        			consultaEmpresa = (ConsultaEmpresa)lst.get(i);
        			if(i==0){
        				command.setCodId1(consultaEmpresa.getCodId());
        	        	command.setEmp1(consultaEmpresa.getNombreEmpresa());
        	        	command.setFini1(consultaEmpresa.getFechaInicio());
        	        	command.setFfin1(consultaEmpresa.getFechaFin());
        	        	command.setCa1(consultaEmpresa.getCantHoras());
        	        	command.setOb1(consultaEmpresa.getObservacion());
        	       }else if(i==1)
        			{
        	    	   	command.setCodId2(consultaEmpresa.getCodId());
        	        	command.setEmp2(consultaEmpresa.getNombreEmpresa());
        	        	command.setFini2(consultaEmpresa.getFechaInicio());
        	        	command.setFfin2(consultaEmpresa.getFechaFin());
        	        	command.setCa2(consultaEmpresa.getCantHoras());
        	        	command.setOb2(consultaEmpresa.getObservacion());
        			}else if(i==2){
        				command.setCodId3(consultaEmpresa.getCodId());
        				command.setEmp3(consultaEmpresa.getNombreEmpresa());
        	        	command.setFini3(consultaEmpresa.getFechaInicio());
        	        	command.setFfin3(consultaEmpresa.getFechaFin());
        	        	command.setCa3(consultaEmpresa.getCantHoras());
        	        	command.setOb3(consultaEmpresa.getObservacion());
        			}else if(i==3){
        				command.setCodId4(consultaEmpresa.getCodId());
        				command.setEmp4(consultaEmpresa.getNombreEmpresa());
        	        	command.setFini4(consultaEmpresa.getFechaInicio());
        	        	command.setFfin4(consultaEmpresa.getFechaFin());
        	        	command.setCa4(consultaEmpresa.getCantHoras());
        	        	command.setOb4(consultaEmpresa.getObservacion());
        			}
        		}
        	}
        	return command.getEmp1()+ command.getEmp2()+command.getEmp3();
        }
        
        private String modificar(RegistrarEmpresaCommand control)
        {
        	FormacionEmpresa obj = new FormacionEmpresa();
        	
        	obj.setCodId(control.getCodId());
        	obj.setCodPeriodo(control.getCodPeriodo());
        	obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA2);
        	obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodCurso(CommonConstants.COD_CURSO);
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setTipoPractica(control.getTipo());
    		obj.setNombreEmpresa(control.getEmpresa());
    		obj.setFechaIni(control.getFechaInicio());
    		obj.setFechaFin(control.getFechaFin());
    		obj.setCantidaHoras(control.getCantHoras());
    		obj.setCodEstado(control.getEstado());
    	
    		obj.setObservacion(control.getObser());
    		obj.setNota(control.getNot());
    		obj.setHoraIni(control.getHoraInicio());
    		obj.setHoraFin(control.getHorafinaliza());
    		obj.setIndSabado(control.getSelSabado());
    		obj.setIndDomingo(control.getSelDomingo());
    		control.setModificado(this.operDocenteManager.ModificarFormacionEmpresa(obj,control.getCodEva()));
        	return control.getModificado();
        }
        
        
        private List empresas(RegistrarEmpresaCommand command){
        	 FormacionEmpresa obj = new FormacionEmpresa();
         	obj.setCodPeriodo(command.getCodPeriodo());
         	obj.setCodEspecialidad(command.getCodEspecialidad());
         	obj.setCodAlumno(command.getCodAlumno());
         	obj.setTipoPractica("2");
         	command.setListEmpresasPractica(this.operDocenteManager.EmpresaXAlumno(obj));
        	return command.getListEmpresasPractica();
        }
        
        private String informe(RegistrarEmpresaCommand command){
        	FormacionEmpresa fobj = new FormacionEmpresa();
     	 	fobj.setCodPeriodo(command.getCodPeriodo());
        	fobj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
        	fobj.setCodAlumno(command.getCodAlumno());
        	fobj.setCodCurso(CommonConstants.COD_CURSO);
        	command.setListInformes(this.operDocenteManager.GetAllAdjuntoEmpresa(fobj));
        	List lst  = command.getListInformes();
        	if(lst!=null && lst.size()>0){
        		Adjunto adjunto = null;
        		for(int i=0;i<lst.size();i++){
        			adjunto = (Adjunto)lst.get(i);
        			command.setInf(adjunto.getArchivo());
        			command.setVer(adjunto.getNuevoArchivo());
        			}
        		}
       	return command.getInf();
       }
        
        private List empre(RegistrarEmpresaCommand command){
       	 FormacionEmpresa obj = new FormacionEmpresa();
        	obj.setCodPeriodo(command.getCodPeriodo());
        	obj.setCodEspecialidad(command.getCodEspecialidad());
        	obj.setCodAlumno(command.getCodAlumno());
        	obj.setTipoPractica(CommonConstants.COD_FORMACION_EMPRESA);
        	command.setListEmpresasPasasntia(this.operDocenteManager.EmpresaXAlumno(obj));
        	return command.getListEmpresasPasasntia();
        }
        
}
