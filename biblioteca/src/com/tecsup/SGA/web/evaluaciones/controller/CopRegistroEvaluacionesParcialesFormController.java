package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.EvaluacionesParcialesCommand;

public class CopRegistroEvaluacionesParcialesFormController extends SimpleFormController{
	
	private static Log log = LogFactory
	.getLog(CopRegistroEvaluacionesParcialesFormController.class);

	private OperDocenteManager operDocenteManager;
	private MantenimientoManager mantenimientoManager;	

	public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	
	public CopRegistroEvaluacionesParcialesFormController(){
		super();
		setCommandClass(EvaluacionesParcialesCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
	throws ServletException{
	
	EvaluacionesParcialesCommand command = new EvaluacionesParcialesCommand();	

	command.setTipoUsuarioBandeja((String)request.getParameter("txhTipoUsuarioBandeja"));
	command.setCodUsuarioBandeja((String)request.getParameter("txhCodUsuarioBandeja"));
	
	if(command.getOperacion()==null){		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
		command.setCodEval((String)request.getParameter("txhCodEvaluador"));
		command.setCodCurso((String)request.getParameter("txhCodCurso"));
		command.setCodProducto((String)request.getParameter("txhCodProducto"));
		command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
		
		//MODIFICAICON MEJORA
		command.setCboEvaluacionParcial(request.getParameter("cboEvaluacionParcial"));
		command.setCboSeccion(request.getParameter("cboSeccion"));		
	}
	
	
	if(command.getCodPeriodo() != null && command.getCodEval() != null && command.getCodCurso() != null){
   	
		command.setLstEvaluacionParcial(operDocenteManager.getAllTipoPracticaxCursoxEval(
				command.getCodPeriodo(),
				command.getCodCurso(),
				command.getCodEval(),
				command.getCodProducto(),
				command.getCodEspecialidad()
				));
    	
    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEval(), command.getCodCurso());
    	command.setTxtFecha(Fecha.getFechaActual());
    	//command.setCboSeccion((String)request.getParameter("cboSeccion"));
    	
    	command = irConsultar(command);
	}
	
	
		return command;
	}

	protected void initBinder(HttpServletRequest request,
	ServletRequestDataBinder binder) {
		
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
		Long.class, nf, true));
	}


public ModelAndView processFormSubmission(HttpServletRequest request,
	HttpServletResponse response, Object command, BindException errors)
	throws Exception {
return super.processFormSubmission(request, response, command, errors);
}

public ModelAndView onSubmit(HttpServletRequest request,
	HttpServletResponse response, Object command, BindException errors){
	
	EvaluacionesParcialesCommand control = (EvaluacionesParcialesCommand) command;
	log.info("OPERACION:" + control.getOperacion());
	control.setUsuario(control.getCodEval());

	if(control.getOperacion().equals("irConsultar")){
		//control.setCboEvaluacionParcial("1");
		//control.setCboNroEvaluacion("-1");
		control = irConsultar(control);   	
	}else if(control.getOperacion().equals("irActualizar")){
		//control = irActualizar(request, response, errors, control);
		//control = irConsultar(request, response, errors, control);
	}else if(control.getOperacion().equals("irCambiaEvaluacionParcial")){
		control = irListaEvaluacionParcial(control);
		control = irCambiaEvaluacionParcial(control);
		//control = irConsultar(request, response, errors, control);
	}else if(control.getOperacion().equals("irCambiaSeccion")){
		control = irCambiaSeccion(control);
		control = irListaEvaluacionParcial(control);
		control = irCambiaEvaluacionParcial(control);
		control.setTxhNroEvaluacionInner(null);
		control = irConsultar(control);
		control.setCboNroEvaluacion("-1");//Todos
	} else if (control.getOperacion().equals("CERRAR")) {
		
	
		String cierre = this.operDocenteManager.cerrarRegistroNotas(control.getCodCurso(), control.getCboSeccion(), control.getCboEvaluacionParcial(), control.getUsuario(), CommonConstants.ESTADO_REGISTRO_NOTAS_CERRADO);		
		log.info("RPTA:" + cierre);
		if (cierre.equals("0"))
			request.setAttribute("mensaje", "Cierre de Notas satisfactorio.");
		else if (cierre.equals("-1"))
			request.setAttribute("mensaje", "No puede Cerrar mientras tenga registros NO trabajados.");
		else if (cierre.equals("-2"))
			request.setAttribute("mensaje", "No puede Cerrar por que existen notas PENDIENTES.");
		else if (cierre.equals("-3"))
			request.setAttribute("mensaje", "Problemas al Cerrar Registro de Notas. Consulte con el Administrador");
		
		control = irConsultar(control);
				
	}else if (control.getOperacion().equals("ABRIR_REG")) {
		
		String abrir = this.operDocenteManager.abrirRegistroNotas(control.getCodCurso(), control.getCboSeccion(), control.getCboEvaluacionParcial(), control.getCodUsuarioBandeja());
		log.info("RPTA:" + abrir);
		if (abrir.equals("0"))
			request.setAttribute("mensaje", "Registro de Notas abierto satisfactoriamente.");		
		else if (abrir.equals("-3"))
			request.setAttribute("mensaje", "Problemas al Abrir Registro de Notas. Consulte con el Administrador");
		
		control = irConsultar(control);
	}

	
	return new ModelAndView("/evaluaciones/cargaOperativa/eva_carga_registro_evaluaciones",
		"control", control);
}

private EvaluacionesParcialesCommand irCambiaSeccion(
		EvaluacionesParcialesCommand control){
	
	control.setLstNroEvaluacion(operDocenteManager.getAllNroEvaxCursoxEva(
			control.getCodPeriodo(),
			control.getCodCurso(),
			control.getCodEval(),
			control.getCodProducto(),
			control.getCodEspecialidad(),
			control.getCboEvaluacionParcial(),
			control.getCboSeccion()));
	
	return control;
}

private EvaluacionesParcialesCommand irListaEvaluacionParcial(
		EvaluacionesParcialesCommand control) {
	control.setLstEvaluacionParcial(operDocenteManager.getAllTipoPracticaxCursoxEval(
			control.getCodPeriodo(),
			control.getCodCurso(),
			control.getCodEval(),
			control.getCodProducto(),
			control.getCodEspecialidad()
			));
	return control;
}
private EvaluacionesParcialesCommand irCambiaEvaluacionParcial(EvaluacionesParcialesCommand control) {
		
		control.setLstSeccion(operDocenteManager.getAllSeccionesxCursoxEval(
				control.getCodPeriodo(),
				control.getCodCurso(),
				control.getCodEval(),
				control.getCodProducto(),
				control.getCodEspecialidad(),
				control.getCboEvaluacionParcial()));
		
		return control;
	}


private EvaluacionesParcialesCommand irConsultar(EvaluacionesParcialesCommand control){
		
		String nroEval;		
		if (control.getCboNroEvaluacion()==null)
			nroEval="";
		else if (control.getCboNroEvaluacion().equals("-1"))
			nroEval="";
		else
			nroEval=control.getCboNroEvaluacion();
			
		List<Parciales> lista = this.operDocenteManager.obtenerDefinicionesParciales(control.getCodCurso(), control.getCboSeccion(), control.getCboEvaluacionParcial(), nroEval, "A");
				
		int contador=0;
		for (Parciales evaluacion : lista) {
			if (evaluacion.getFlagCerrarNota()!=null){
				if (evaluacion.getFlagCerrarNota().equals("1")) 
					contador ++;
			}
		}
			
		if (lista.size()==contador)
			control.setFlagCerrarNota("1");
		else
			control.setFlagCerrarNota("0");
						
		
		if("-1".equalsIgnoreCase(control.getCboNroEvaluacion()) || control.getCboNroEvaluacion()==null )
			control.setTxhNroEvaluacionInner(null);
			
			control = irCambiaSeccion(control);
			control = irListaEvaluacionParcial(control);
			control = irCambiaEvaluacionParcial(control);
			
		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), control.getCodCurso());
		control.setSeccion(control.getCboSeccion());		
		control = operDocenteManager.setConsulta(control);
		
		return control;
	}

/*private EvaluacionesParcialesCommand irRegistrar(HttpServletRequest request,
	HttpServletResponse response, BindException errors,
	EvaluacionesParcialesCommand control) {
	log.info("irRegistrar:INI");

	try 
	{	String rpta = this.mantenimientoManager.insertEvaluacioneParciales(control);
		if(rpta.equalsIgnoreCase("0"))
			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
		else
			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
	}catch (Exception e) 
	{
		request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR+CommonMessage.CONSULTE_ADMIN);
		e.printStackTrace();
	}
	log.info("irRegistrar:FIN");
	return control;
}*/

/*private EvaluacionesParcialesCommand irActualizar(HttpServletRequest request,
		HttpServletResponse response, BindException errors,
		EvaluacionesParcialesCommand control) {
		log.info("irRegistrar:INI");

		try 
		{	String rpta = this.mantenimientoManager.updateEvaluacioneParciales(control);
			if(rpta.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			
		}catch (Exception e) 
		{
			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR+CommonMessage.CONSULTE_ADMIN);
			e.printStackTrace();
		}
		log.info("irRegistrar:FIN");
		return control;
}*/


	private void inicializaDataCurso(EvaluacionesParcialesCommand command, String strPeriodo, String strEval, 
									String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval,"",""
				, strCodCurso, "","");
	
		if (dataCurso.size()>0){
			lObject = (CursoEvaluador) dataCurso.get(0);
			command.setTxtPeridoVigente(lObject.getPeriodo());
			command.setTxtProfesor(lObject.getEvaluador());
			command.setTxtPrograma(lObject.getProducto());
			command.setTxtCiclo(lObject.getCiclo());
			command.setTxtCurso(lObject.getCurso());
			command.setTxtEspecialidad(lObject.getEspecialidad());
			command.setTxtSistEval(lObject.getSistEval());
		}
	}

	private void inicializaSeccion(EvaluacionesParcialesCommand command, String strCodCurso){
		command.setLstSeccion(this.operDocenteManager.getAllSeccionCurso("", strCodCurso));
	}
	
	private void inicializaDataAlumno(EvaluacionesParcialesCommand command, String codCurso,
									String codProducto, String codEspecialidad, String tipoSesion,
									String codSeccion, String strNombre, String strApellido){
		List lst = this.operDocenteManager.getAllAlumnosCurso(command.getCodPeriodo(), codCurso,
											codProducto, codEspecialidad, tipoSesion, 
											codSeccion, strNombre, strApellido);
		if(lst!=null)
			command.setHidTamanio(""+lst.size());
		else
			command.setHidTamanio("0");
		
		//List lst2 = mantenimientoManager.getTipoPractica();log.info("lsttam>"+lst2.size());

		command.setLstEvaluacionParcial(mantenimientoManager.getTipoPracticaxCurso(command.getCodPeriodo(),command.getCodCurso()));
		
		command.setLstResultado(lst);
	}

}
