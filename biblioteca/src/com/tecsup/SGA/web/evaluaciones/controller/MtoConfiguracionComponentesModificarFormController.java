package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.MtoConfiguracionComponentesModificarCommand;

public class MtoConfiguracionComponentesModificarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoConfiguracionComponentesModificarFormController.class);
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		MtoConfiguracionComponentesModificarCommand command= new MtoConfiguracionComponentesModificarCommand();
    	
        return command;
    } 
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
}
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	MtoConfiguracionComponentesModificarCommand control=(MtoConfiguracionComponentesModificarCommand) command;    		
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_config_componentes_modificar","control",control);		
    }
}
