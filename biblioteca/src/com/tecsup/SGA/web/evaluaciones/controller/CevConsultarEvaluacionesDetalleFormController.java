package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.evaluaciones.ConsultasReportesManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.web.evaluaciones.command.ConsultarEvaluacionesCommand;

public class CevConsultarEvaluacionesDetalleFormController extends
		SimpleFormController {
	private static Log log = LogFactory
			.getLog(CevConsultarEvaluacionesDetalleFormController.class);

	private MantenimientoManager mantenimientoManager;
	private ConsultasReportesManager consultasReportesManager;

	public void setConsultasReportesManager(
			ConsultasReportesManager consultasReportesManager) {
		this.consultasReportesManager = consultasReportesManager;
	}

	public void setMantenimientoManager(
			MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}

	public CevConsultarEvaluacionesDetalleFormController() {
		super();
		setCommandClass(ConsultarEvaluacionesCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		log.info("formBackingObject");
		ConsultarEvaluacionesCommand command = new ConsultarEvaluacionesCommand();

		if (command.getOperacion() == null){
			String codPeriodo = request.getParameter("codPeriodo");
			String codProducto = request.getParameter("codProducto");
			String codAlumno = request.getParameter("codAlumno");
			String codCiclo = request.getParameter("codCiclo");
			String codCurso = request.getParameter("codCurso");
			String codEval = request.getParameter("txhCodEvaluador");
			String codCboPeriodo = request.getParameter("codCboPeriodo");
			
			/*Variables de tutor*/
			command.setIndProcedencia(request.getParameter("txhIndProcTutor"));
			command.setCodPeriodoTutor(request.getParameter("txhCodPeriodoTutor"));
			command.setCodCursoTutor(codCurso);
						
			command.setCodPeriodo(codPeriodo);
			command.setCboProducto(codProducto);
			command.setCodAlumno(codAlumno);
			command.setCboCiclo(codCiclo);
			command.setTxtCurso(codCurso);
			command.setCodEvaluador(codEval);
			command.setCboPeriodo(codCboPeriodo);

			if ( command.getIndProcedencia() == null ) command.setIndProcedencia(""); 
			if ( command.getIndProcedencia().trim().equals("1") )
				request.setAttribute("IndProcedencia","1");
			else request.setAttribute("IndProcedencia","0");
			
			consultasReportesManager.setConsultarEvaluacionesDetalle(command);

		}
		
		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	
	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors) {
		
		ConsultarEvaluacionesCommand control = (ConsultarEvaluacionesCommand) command;

		log.info("OPERACION:" + control.getOperacion());
		control.setUsuario(control.getCodEvaluador());

		if (control.getOperacion().equals("irConsultarPeriodo")) {
			control = irConsultarPeriodo(request, response, errors, control);
		}

		if ( control.getIndProcedencia().trim().equals("1") )
			request.setAttribute("IndProcedencia","1");
		else request.setAttribute("IndProcedencia","0");
		
		
		return new ModelAndView(
				"/evaluaciones/consultarEvaluaciones/eva_con_detalle",
				"control", control);
	}

	private ConsultarEvaluacionesCommand irConsultarPeriodo(
			HttpServletRequest request, HttpServletResponse response,
			BindException errors, ConsultarEvaluacionesCommand command) {
		
		return command;
	}

}
