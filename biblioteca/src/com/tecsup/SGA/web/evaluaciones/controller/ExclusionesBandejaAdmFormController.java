package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.evaluaciones.GestionAdministradtivaManager;
import com.tecsup.SGA.web.evaluaciones.command.IncidenciaBandejaDocCommand;
import com.tecsup.SGA.modelo.CursoEvaluador;

public class ExclusionesBandejaAdmFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(IncidenciaRegistrarDocFormController.class);
	GestionAdministradtivaManager gestionAdministradtivaManager;
	OperDocenteManager operDocenteManager;	
	
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public void setGestionAdministradtivaManager(
			GestionAdministradtivaManager gestionAdministradtivaManager) {
		this.gestionAdministradtivaManager = gestionAdministradtivaManager;
	}

	public ExclusionesBandejaAdmFormController(){
		super();
		setCommandClass(IncidenciaBandejaDocCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	IncidenciaBandejaDocCommand command = new IncidenciaBandejaDocCommand();  	
    	   	
    	/*Recogiendo valores*/
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	
    	//Llenando información
    	if ( command.getCodCurso() != null )
    	{
    		//Inicializa valores.
	    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEvaluador(), command.getCodCurso());
	    	//Se llena la seccion
	    	inicializaSeccion(command, command.getCodCurso());
	    	//Sete el valor del combo en la primera seccion
	    	ArrayList<CursoEvaluador> listSeccion = (ArrayList<CursoEvaluador>)command.getListaSeccion();
	    	if ( listSeccion != null )
	    	{
		    	if ( listSeccion.size() > 0 )
		    		command.setCodSeccion(listSeccion.get(0).getCodSeccion());
	    	}
	    	//LLeno la lista de alumnos
	    	buscarAlumnos(command);	    	
    	}
    	//Llenado Tabla Interes - Session
    	request.getSession().setAttribute("listaBandejaExclusionesAdm", command.getListaBandeja());
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String respuesta = "";
    	IncidenciaBandejaDocCommand control = (IncidenciaBandejaDocCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	inicializaSeccion(control, control.getCodCurso());
    	
    	if ( control.getOperacion().trim().equals("BUSCAR"))
    	{
    		buscarAlumnos(control);
    	}
    	else if ( control.getOperacion().trim().equals("GRABAR"))
    	{
    		respuesta = gragarExclusion(control);
    		log.info("RPTA:" + respuesta);
    		if ( respuesta == null ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("-1") ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("0") )
    		{
    			buscarAlumnos(control);
    			control.setMsg("OK");
    		}
    	} 
    	
    	request.getSession().setAttribute("listaBandejaExclusionesAdm", control.getListaBandeja());
		
	    return new ModelAndView("/evaluaciones/gestionAdministrativo/eva_exclusionprombandeja_adm","control",control);		
    }
    
	private void inicializaDataCurso(IncidenciaBandejaDocCommand command, String strPeriodo, String strEval, String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval,"",""
				, strCodCurso, "","");
		
    	if (dataCurso.size()>0){
    		lObject = (CursoEvaluador) dataCurso.get(0);
    		command.setPeriodoVigente(lObject.getPeriodo());
    		command.setCiclo(lObject.getCiclo());
    		command.setProducto(lObject.getProducto());
    		command.setNomEvaluador(lObject.getEvaluador());
    		command.setCurso(lObject.getCurso());
    		command.setEscialidad(lObject.getEspecialidad());
    		command.setSistemaEval(lObject.getSistEval());
    	}
	}
	
	private void buscarAlumnos(IncidenciaBandejaDocCommand control)
	{
		control.setListaBandeja(
					this.gestionAdministradtivaManager.getAllExclusionesByCurso(
							control.getCodCurso()
							, control.getNomAlumno()
							, control.getApellidoAlumno()
							, control.getCodSeccion()
							, control.getCodPeriodo()));
	}

	private String gragarExclusion(IncidenciaBandejaDocCommand control)
	{
		String cadGrabar = "";
		String codExclusion = ""; 
		StringTokenizer stCadCodExclusiones = new StringTokenizer(control.getCadCodExclusiones(),"|");
		StringTokenizer stCadCodAlumnos = new StringTokenizer(control.getCadCodAlumnos(),"|");
		StringTokenizer stCadIndSemestre = new StringTokenizer(control.getCadIndSemestre(),"|");
		StringTokenizer stCadIndAcumulado = new StringTokenizer(control.getCadIndAcumulado(),"|");
		
		while ( stCadCodAlumnos.hasMoreTokens() )
		{
			codExclusion = stCadCodExclusiones.nextToken();
			if ( codExclusion.trim().equals("0") ) codExclusion = "";
			
			cadGrabar = cadGrabar + codExclusion + "$";
			cadGrabar = cadGrabar + stCadCodAlumnos.nextToken() + "$";
			cadGrabar = cadGrabar + stCadIndSemestre.nextToken() + "$";
			cadGrabar = cadGrabar + stCadIndAcumulado.nextToken() + "$|";
		}
		
		return this.gestionAdministradtivaManager.insertExclusiones(control.getCodCurso()
				, cadGrabar, control.getCodEvaluador(), control.getCodPeriodo(), control.getCodSeccion());
	}
	
    private void inicializaSeccion(IncidenciaBandejaDocCommand command, String strCodCurso){
    	ArrayList arrTipoSesiones = (ArrayList)this.operDocenteManager.getAllSesionXCurso(strCodCurso); 
    	CursoEvaluador cursoEvaluador;
    	
    	boolean flag = true;
    	if ( arrTipoSesiones != null )
    	{
    		//BUSCO TIPO SESION TEORIA
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TEO))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO LABORATORIO 
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_LAB))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO LABORATORIO
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TAL))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    	}
    	//command.setListaSeccion(this.operDocenteManager.getAllSeccionCurso("", strCodCurso));
    }
}
