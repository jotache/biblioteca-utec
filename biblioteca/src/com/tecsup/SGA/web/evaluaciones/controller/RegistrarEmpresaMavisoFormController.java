package com.tecsup.SGA.web.evaluaciones.controller;

//JHPR 2008-06-20
import java.text.NumberFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Adjunto;
import com.tecsup.SGA.modelo.ConsultaEmpresa;
import com.tecsup.SGA.modelo.FormacionEmpresa;
import com.tecsup.SGA.modelo.HorasXalumnos;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarEmpresaCommand;

public class RegistrarEmpresaMavisoFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(RegistrarEmpresaMavisoFormController.class);
	
	OperDocenteManager operDocenteManager;
	AlumnoManager alumnoManager;
	
	public RegistrarEmpresaMavisoFormController() {
		super();
		setCommandClass(RegistrarEmpresaCommand.class);
	}
	
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	public void setAlumnoManager(AlumnoManager alumnoManager) {
		this.alumnoManager = alumnoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)  throws ServletException {
		
		RegistrarEmpresaCommand command = new RegistrarEmpresaCommand();
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEva((String)request.getParameter("txhCodEva"));
    	command.setTipo(CommonConstants.COD_NOTA_EXTERNO);
    	request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_CARGA_OPERATIVA);
    	    	    
     	String[] alumnos = (String[]) request.getParameterValues("aDatosAlumno");
     		
     	if (alumnos!=null){
     		StringTokenizer datos = new StringTokenizer(alumnos[0],",");
     		String alumnosSelec="";
     		for (int x=0; x<alumnos.length; x++)
     			alumnosSelec = alumnosSelec + alumnos[x];   
     		
     		command.setAlumnosSeleccionados(this.alumnoManager.crearListadeAlumnos(alumnosSelec));
     		//String alumnosSelec="";
     		//for (int x=0; x<alumnos.length; x++)
     			//alumnosSelec = alumnosSelec + alumnos[x];   
     		
     		command.setCodAlumnosSelec(alumnosSelec);
     	}
     	 
     	if (!(command.getTipoIngreso().equals(""))) {     		     		
			command.setEmpresa(command.getF1NomEmpresa());
			command.setFechaInicio(command.getF1FechaIni());
			command.setFechaFin(command.getF1FechaFin());
			command.setObservacion(command.getF1Obs());
			command.setCaHoras(command.getF1CantHrs());	
     	}
     	
     	if (!(command.getTipoIngreso2().equals(""))) {     		
     		command.setEmpresa1(command.getF2NomEmpresa());
     		command.setCant(command.getF2Cantidad());
     	}
     	
     	if (!(command.getTipoIngreso3().equals(""))) {
     		command.setEmpresa2(command.getF3NomEmpresa());
     		command.setCant1(command.getF3Cantidad());     		     		
     		command.setFechaInicio1(command.getF3FechaIni());
     		command.setFechaFin1(command.getF3FechaFin());
     		command.setHoraIni(command.getF3HoraIni());
     		command.setHoraFin(command.getF3HoraFin());
     		command.setNota(command.getF3Nota());
     	}
     	
    		
     	command.setTotemp(0);
    	command.setFlag(0); //?
    	command.setCodFlagApto(CommonConstants.TIPT_FLAG_APTO);
    	command.setCodFlagNoApto(CommonConstants.TIPT_FLAG_NO_APTO);
    	command.setSab(CommonConstants.TIPT_FLAG_SALA);//?
    	command.setDom(CommonConstants.TIPT_ETAPA_TODOS);//?
    	
		return command;		
	}
	   
	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {			
		RegistrarEmpresaCommand control = (RegistrarEmpresaCommand) command;
		String resultado="0";
		String id="";
		String listaIds = "";
		log.info("OPERACION:" + control.getOperacion());
		
		if (control.getOperacion().trim().equals("GRABAR")) {
			
			if(control.getTipo().trim().equals("0")){
				//Practica Inicial.		
				if ("".equals(control.getTipoIngreso())) 
					control.setTipoIngreso("I");
				else
					control.setTipoIngreso("A");				
				int j=0;				
				String [] campos = control.getCodAlumnosSelec().split(",");				
				if (control.getTipoIngreso().equals("I")) {
					control.setIds("");
					while(j<campos.length && (!resultado.equals("-1")) ){					
						id = grabarPracticaInicial(control, campos[j]);
						if (id.equals("-1"))
							resultado.equals("-1");
						else
							listaIds = listaIds.toString() + id.toString() + ",";						
						j++;
					}					
					listaIds = listaIds.substring(0, listaIds.length()-1);				
					control.setIds(listaIds);
				} else if (control.getTipoIngreso().equals("A")) {
					String [] ids = control.getIds().split(",");
					while(j<ids.length && (!resultado.equals("-1")) ){						
						resultado = modificar(control,ids[j]);	
						j++;
					}
				}						
				control.setF1NomEmpresa(control.getEmpresa());
				control.setF1FechaIni(control.getFechaInicio());				
				control.setF1FechaFin(control.getFechaFin());
				control.setF1Obs(control.getObservacion());
				control.setF1CantHrs(control.getCaHoras());				
				if (resultado.equals("-1")) {
	    			control.setMsg("ERROR");
	    			resultado="";
	    		} else {
	    			control.setMsg("OK");
	    			resultado="";
	    		}				
				control.setTipoIngreso("A");
				control.setFlag(0);
				
			} else if(control.getTipo().trim().equals("1")){
				//Pasant�a...								
				if ("".equals(control.getTipoIngreso2())) 
					control.setTipoIngreso2("I");
				else
					control.setTipoIngreso2("A");
				
				int j=0;
				String [] campos = control.getCodAlumnosSelec().split(",");								
				if (control.getTipoIngreso2().equals("I")) {
					control.setIds2("");
					while(j<campos.length && (!resultado.equals("-1")) ){
						//System.out.println("grabar campos[j]:"+campos[j]);
						id = grabarPasantia(control, campos[j]);
						
						if (id.equals("-1"))
							resultado.equals("-1");
						else
							listaIds = listaIds + id + ",";
						
						//System.out.println("listaIds antes 0 substring:"+listaIds);
						j++;
					}			
					//System.out.println("listaIds antes substring:"+listaIds);
					listaIds = listaIds.substring(0, listaIds.length()-1);
					//System.out.println("listaIds despues substring:"+listaIds);
					control.setIds2(listaIds);
				} else if (control.getTipoIngreso2().equals("A")) {
					String [] ids = control.getIds2().split(",");
					while(j<ids.length && (!resultado.equals("-1")) ){
						//System.out.println("modificar ids[j]:"+ids[j]);
						resultado = modificar(control,ids[j]);
						j++;
					}
				}
				control.setF2NomEmpresa(control.getEmpresa1());
				control.setF2Cantidad(control.getCant());			
				//control.setF1FechaFin(control.getFechaFin());
				//control.setF1Obs(control.getObservacion());
				//control.setF1CantHrs(control.getCaHoras());				
				if (resultado.equals("-1")) {
	    			control.setMen("ERROR");
	    			resultado="";
	    		} else {
	    			control.setMen("OK");
	    			resultado="";
	    		}				
				control.setTipoIngreso2("A");					
				control.setFlag(1);
			} else if(control.getTipo().trim().equals("2")){
				//Practica PreProfesional
				if ("".equals(control.getTipoIngreso3())) 
					control.setTipoIngreso3("I");
				else
					control.setTipoIngreso3("A");				
				int j=0;
				String [] campos = control.getCodAlumnosSelec().split(",");								
				if (control.getTipoIngreso3().equals("I")) {
					control.setIds3("");
					while(j<campos.length && (!resultado.equals("-1")) ){					
						id = grabarPracticaPreProf(control, campos[j]);
						if (id.equals("-1"))
							resultado.equals("-1");
						else
							listaIds = listaIds.toString() + id.toString() + ",";						
						j++;
					}					
					listaIds = listaIds.substring(0, listaIds.length()-1);				
					control.setIds3(listaIds);
				} else if (control.getTipoIngreso3().equals("A")) {
					String [] ids = control.getIds3().split(",");
					while(j<ids.length && (!resultado.equals("-1")) ){						
						resultado = modificar(control,ids[j]);
						j++;
					}
				}
				control.setF3NomEmpresa(control.getEmpresa2());
				control.setF3Cantidad(control.getCant1());
				control.setF3FechaIni(control.getFechaInicio1());
				control.setF3FechaFin(control.getFechaFin1());
				control.setF3HoraIni(control.getHoraIni());
				control.setF3HoraFin(control.getHoraFin());
				control.setF3Nota(control.getNota());
				
				//control.setF1FechaFin(control.getFechaFin());
				//control.setF1Obs(control.getObservacion());
				//control.setF1CantHrs(control.getCaHoras());			
				if (resultado.equals("-1")) {
	    			control.setMens("ERROR");
	    			resultado="";
	    		} else {
	    			control.setMens("OK");
	    			resultado="";
	    		}				
				control.setTipoIngreso3("A");					
				control.setFlag(2);
			} 
				
			control.setAlumnosSeleccionados(this.alumnoManager.crearListadeAlumnos(control.getCodAlumnosSelec()));
		}
		
		return new ModelAndView("/evaluaciones/formacionEmpresa/eva_registrar_empresa_masivo","control",control);
	}
	
	private String numero(RegistrarEmpresaCommand command)
    {
    command.setHoras("");	
    FormacionEmpresa obj = new FormacionEmpresa();
    obj.setCodPeriodo(command.getCodPeriodo());
    obj.setCodEspecialidad(command.getCodEspecialidad());
    obj.setCodAlumno(command.getCodAlumno());
    obj.setTipoPractica("0");
    HorasXalumnos nobj = this.operDocenteManager.HorasPorAlumno(obj);	
    if(!(nobj==null)){
		command.setHorasParciales(nobj.getCantHoras());
	}
	else{
		command.setHorasParciales("0");
	}
    	return command.getHoras();
    }

    
    private String  numero1(RegistrarEmpresaCommand command)
    {
    command.setHoras("");	
    FormacionEmpresa obj = new FormacionEmpresa();
    obj.setCodPeriodo(command.getCodPeriodo());
    obj.setCodEspecialidad(command.getCodEspecialidad());
    obj.setCodAlumno(command.getCodAlumno());
    obj.setTipoPractica(CommonConstants.COD_FORMACION_EMPRESA);
    
    HorasXalumnos nobj = this.operDocenteManager.HorasPorAlumno(obj);	
    if(!(nobj==null)){
		command.setHorasParciales(nobj.getCantHoras());
	}
	else{
		command.setHorasParciales("0");
	}
    	return command.getHorasPasatia();
    }
    
    
    private String  numero2(RegistrarEmpresaCommand command)
    {
    command.setHoras("");	
    FormacionEmpresa obj = new FormacionEmpresa();
    obj.setCodPeriodo(command.getCodPeriodo());
    obj.setCodEspecialidad(command.getCodEspecialidad());
    obj.setCodAlumno(command.getCodAlumno());
    obj.setTipoPractica("2");
    
    HorasXalumnos nobj = this.operDocenteManager.HorasPorAlumno(obj);	
    	if(!(nobj==null)){
    		command.setHorasParciales(nobj.getCantHoras());
    	}
    	else{
    		command.setHorasParciales("0");
    	}
    		return command.getHorasParciales();
    }
    
   
    private String empresa(RegistrarEmpresaCommand command){
    	FormacionEmpresa obj = new FormacionEmpresa();        	         	
    	obj.setCodPeriodo(command.getCodPeriodo());
    	obj.setCodEspecialidad(command.getCodEspecialidad());
    	obj.setCodAlumno(command.getCodAlumno());
    	obj.setTipoPractica("0");
    	command.setListEmpresas(this.operDocenteManager.EmpresaXAlumno(obj));
    	command.setTotemp(command.getListEmpresas().size());
    	List lst  = command.getListEmpresas();
    	if(lst!=null && lst.size()>0){
    		ConsultaEmpresa consultaEmpresa =null;
    		for(int i=0;i<lst.size();i++){
    			consultaEmpresa = (ConsultaEmpresa)lst.get(i);
    			if(i==0){
    				command.setCodId1(consultaEmpresa.getCodId());
    	        	command.setEmp1(consultaEmpresa.getNombreEmpresa());
    	        	command.setFini1(consultaEmpresa.getFechaInicio());
    	        	command.setFfin1(consultaEmpresa.getFechaFin());
    	        	command.setCa1(consultaEmpresa.getCantHoras());
    	        	command.setOb1(consultaEmpresa.getObservacion());
    	       }else if(i==1)
    			{
    	    	   	command.setCodId2(consultaEmpresa.getCodId());
    	        	command.setEmp2(consultaEmpresa.getNombreEmpresa());
    	        	command.setFini2(consultaEmpresa.getFechaInicio());
    	        	command.setFfin2(consultaEmpresa.getFechaFin());
    	        	command.setCa2(consultaEmpresa.getCantHoras());
    	        	command.setOb2(consultaEmpresa.getObservacion());
    			}else if(i==2){
    				command.setCodId3(consultaEmpresa.getCodId());
    				command.setEmp3(consultaEmpresa.getNombreEmpresa());
    	        	command.setFini3(consultaEmpresa.getFechaInicio());
    	        	command.setFfin3(consultaEmpresa.getFechaFin());
    	        	command.setCa3(consultaEmpresa.getCantHoras());
    	        	command.setOb3(consultaEmpresa.getObservacion());
    			}
    		}
    	}
    	return command.getEmp1()+ command.getEmp2()+command.getEmp3();
    }
    
    
    private List empre(RegistrarEmpresaCommand command){
      	 FormacionEmpresa obj = new FormacionEmpresa();
       	obj.setCodPeriodo(command.getCodPeriodo());
       	obj.setCodEspecialidad(command.getCodEspecialidad());
       	obj.setCodAlumno(command.getCodAlumno());
       	obj.setTipoPractica(CommonConstants.COD_FORMACION_EMPRESA);
       	command.setListEmpresasPasasntia(this.operDocenteManager.EmpresaXAlumno(obj));
       	return command.getListEmpresasPasasntia();
     }
    
    private List empresas(RegistrarEmpresaCommand command){
   	 FormacionEmpresa obj = new FormacionEmpresa();
    	obj.setCodPeriodo(command.getCodPeriodo());
    	obj.setCodEspecialidad(command.getCodEspecialidad());
    	obj.setCodAlumno(command.getCodAlumno());
    	obj.setTipoPractica("2");
    	command.setListEmpresasPractica(this.operDocenteManager.EmpresaXAlumno(obj));
   	return command.getListEmpresasPractica();
   }
   
   private String informe(RegistrarEmpresaCommand command){
   	FormacionEmpresa fobj = new FormacionEmpresa();
	 	fobj.setCodPeriodo(command.getCodPeriodo());
   	fobj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
   	fobj.setCodAlumno(command.getCodAlumno());
   	fobj.setCodCurso(CommonConstants.COD_CURSO);
   	command.setListInformes(this.operDocenteManager.GetAllAdjuntoEmpresa(fobj));
   	List lst  = command.getListInformes();
   	if(lst!=null && lst.size()>0){
   		Adjunto adjunto = null;
   		for(int i=0;i<lst.size();i++){
   			adjunto = (Adjunto)lst.get(i);
   			command.setInf(adjunto.getArchivo());
   			command.setVer(adjunto.getNuevoArchivo());
   			}
   		}
  	return command.getInf();
  }
    
   private String grabarPracticaInicial(RegistrarEmpresaCommand control,String codigoAlumno)
   {
	   	//System.out.println("Tipo Ingreso Pasant�a:"+control.getTipoIngreso());
	   	//System.out.println("8");
   		FormacionEmpresa obj = new FormacionEmpresa();
		obj.setCodPeriodo(control.getCodPeriodo());
		obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
		obj.setCodEspecialidad(control.getCodEspecialidad());
		//System.out.println("9");
		obj.setCodCurso(CommonConstants.COD_CURSO);
		obj.setCodAlumno(codigoAlumno);
		obj.setTipoPractica(control.getTipo());
		//System.out.println("10");
		obj.setNombreEmpresa(control.getEmpresa());
		obj.setFechaIni(control.getFechaInicio());
		obj.setFechaFin(control.getFechaFin());
		//System.out.println("11");
		obj.setObservacion(control.getObservacion());
		obj.setCantidaHoras(control.getCaHoras());
		//System.out.println("12");
		//obtener datos de formacion de empresa pasantia... para no grabar mas de 3 pasantias.
		int nroPasantias = this.alumnoManager.NroPasantiasAlumno(codigoAlumno);
		//System.out.println("Nro Pasantias: "+ codigoAlumno + " : " +nroPasantias);
		if (nroPasantias < CommonConstants.NRO_MAXIMO_PASANTIAS) {			
			obj.setCodGrabar(this.operDocenteManager.InsertEmpresaId(obj, control.getCodEva()));			
		} else {
			obj.setCodGrabar("0");
		}
				
		return obj.getCodGrabar();
   }   
   
   
   //grabar Pasant�a
   private String grabarPasantia(RegistrarEmpresaCommand control,String codigoAlumno)
   {
   		FormacionEmpresa obj = new FormacionEmpresa();
		obj.setCodPeriodo(control.getCodPeriodo());
		obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
		obj.setCodEspecialidad(control.getCodEspecialidad());
		obj.setCodCurso(CommonConstants.COD_CURSO);
		obj.setCodAlumno(codigoAlumno);
		obj.setTipoPractica(control.getTipo());
		obj.setNombreEmpresa(control.getEmpresa1());
		obj.setFechaIni(control.getFechaInicio());
		obj.setFechaFin(control.getFechaFin());
		obj.setCantidaHoras(control.getCant());
		obj.setCodEstado(control.getEstado());
		obj.setCodGrabar(this.operDocenteManager.InsertEmpresaId(obj, control.getCodEva()));
		
		return obj.getCodGrabar();
   }   
   
   private String grabarPracticaPreProf(RegistrarEmpresaCommand control,String codigoAlumno)
   {
   		FormacionEmpresa obj = new FormacionEmpresa();
   		obj.setCodPeriodo(control.getCodPeriodo());
		obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA);
		obj.setCodEspecialidad(control.getCodEspecialidad());
		obj.setCodCurso(CommonConstants.COD_CURSO);
		obj.setCodAlumno(codigoAlumno);
		obj.setTipoPractica(control.getTipo());
		obj.setNombreEmpresa(control.getEmpresa2());
		obj.setFechaIni(control.getFechaInicio1());
		obj.setFechaFin(control.getFechaFin1());
		obj.setCantidaHoras(control.getCant1());
		obj.setHoraIni(control.getHoraIni());
		obj.setHoraFin(control.getHoraFin());
		obj.setIndSabado(control.getIndSabado());
		obj.setIndDomingo(control.getIndDomingo());
		obj.setNota(control.getNota());
		obj.setCodGrabar(this.operDocenteManager.InsertEmpresaId(obj, control.getCodEva()));
		return obj.getCodGrabar();
   }
   
   private String modificar(RegistrarEmpresaCommand control,String codId)
   {
   		FormacionEmpresa obj = new FormacionEmpresa();
	   	obj.setCodId(codId);
	   	obj.setCodPeriodo(control.getCodPeriodo());
	   	obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA2);
	   	obj.setCodEspecialidad(control.getCodEspecialidad());
		obj.setCodCurso(CommonConstants.COD_CURSO);
		obj.setTipoPractica(control.getTipo());
	
		
		if (control.getTipo().equals("0")){
			obj.setNombreEmpresa(control.getEmpresa());			
			obj.setCantidaHoras(control.getCaHoras());
			obj.setFechaIni(control.getFechaInicio());
			obj.setFechaFin(control.getFechaFin());
			
		} else if (control.getTipo().equals("1")){
			obj.setNombreEmpresa(control.getEmpresa1());
			obj.setCantidaHoras(control.getCant());
		} else if (control.getTipo().equals("2")){
			obj.setNombreEmpresa(control.getEmpresa2());
			obj.setCantidaHoras(control.getCant1());
			obj.setFechaIni(control.getFechaInicio1());
			obj.setFechaFin(control.getFechaFin1());
			
		}
		
		obj.setCodEstado(control.getEstado());				
		obj.setObservacion(control.getObservacion());
		
		obj.setCodEstado(control.getEstado());
		
		obj.setHoraIni(control.getHoraIni());
		obj.setHoraFin(control.getHoraFin());
		
		obj.setIndSabado(control.getIndSabado());
		obj.setIndDomingo(control.getIndDomingo());
		obj.setNota(control.getNota());
		
		control.setModificado(this.operDocenteManager.ModificarFormacionEmpresa(obj,control.getCodEva()));
   	return control.getModificado();
   }
}
