package com.tecsup.SGA.web.evaluaciones.controller;

import java.io.File;
import java.text.*;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.modelo.ConfiguracionNotaExterna;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Especialidad;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.Programas;
//import com.tecsup.SGA.modelo.SistevalHibrido;
//import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.service.evaluaciones.NotaExternaManager;
import com.tecsup.SGA.service.evaluaciones.ProductosManager;
//import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
//import com.tecsup.SGA.service.evaluaciones.TipoExamenManager;
//import com.tecsup.SGA.service.evaluaciones.TipoPracticaManager;
//import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.service.evaluaciones.MtoConfiguracionComponentesManager;
import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;

public class NotaCasosEspecialesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(NotaCasosEspecialesFormController.class);
	private ProductosManager productosManager;	
	private MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager;
	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	private AlumnoManager alumnoManager;
	private MantenimientoManager mantenimientoManager;
	private NotaExternaManager notaExternaManager;
	
	//JHPR:2008/04/07 Para mostrar el texto del período vigente.
	private ComunManager comunManager;
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}
	
	public void setNotaExternaManager(NotaExternaManager notaExternaManager) {
		this.notaExternaManager = notaExternaManager;
	}

	public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}

	public void setAlumnoManager(AlumnoManager alumnoManager) {
		this.alumnoManager = alumnoManager;
	}

	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}

	public void setMtoConfiguracionComponentesManager(
			MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager) {
		this.mtoConfiguracionComponentesManager = mtoConfiguracionComponentesManager;
	}

	public void setProductosManager(ProductosManager productosManager) {
		this.productosManager = productosManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	NotaCasosEspecialesCommand command = new NotaCasosEspecialesCommand();
		//obteniendo los valores de la sesion
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	
    	//***********************************
    	cargaPeriodo(command);
    	//para el combo de productos siempres se carga
    	command.setListProductos(this.productosManager.getAllProductos());
    	command.setListCurso(null);
    	command.setListAlumnos(null);
    	command.setListAlumnosCat(null);
    	//***************************************************
    	//copiado de renzo constantes de los productos
    	command.setCodPFR(CommonConstants.TIPT_PRODUCTO_PFR);
    	command.setCodPCC(CommonConstants.TIPT_PRODUCTO_PCC);
    	command.setCodPCC01(CommonConstants.TIPT_PRODUCTO_PCC_CC);
    	command.setCodCAT(CommonConstants.TIPT_PRODUCTO_CAT);
    	command.setCodPAT(CommonConstants.TIPT_PRODUCTO_PAT);
    	command.setCodPIA(CommonConstants.TIPT_PRODUCTO_PIA);
    	command.setCodPE(CommonConstants.TIPT_PRODUCTO_PE);
    	command.setCodHIBRIDO(CommonConstants.TIPT_PRODUCTO_HIBRIDO);
    	command.setCodTecsupVirtual(CommonConstants.TIPT_PRODUCTO_TECSUP_VIRTUAL);
    	command.setCodEtapaProgramaIntegral(CommonConstants.TIPT_ETAPA_PROGRAMA_INTEGRAL);
    	command.setCodEtapaCursoCorto(CommonConstants.TIPT_ETAPA_CURSOS_CORTOS);
    	//***************************************************
    	
    	//JHPR 2008-05-05 Calcular Nota Tecsup.
    	command.setDatosConfigCurso(null);
    	
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }	    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
   	   	NotaCasosEspecialesCommand control = (NotaCasosEspecialesCommand)command;
   	   	log.info("OPERACION:" + control.getOperacion());
   	
   	   	cargaPeriodo(control);
   	   	if(control.getOperacion().trim().equals("BUSCAR")){
	   		
			CargaBandeja(control);
			request.setAttribute("cat","1");
			//request.setAttribute("config", control.getDatosConfigCurso());
			request.setAttribute("config", control.getListaDatosConfigCurso());
   	   	}else if(control.getOperacion().trim().equals("BUSCAR_CURSOS")){
 		   
 		   control.setListCursoPFR(this.alumnoManager.listaCursosExternos(control.getCodPeriodo()));
 		   request.setAttribute("cat","1");
 		   request.setAttribute("config", control.getListaDatosConfigCurso());
 		
 	   	}else if(control.getOperacion().trim().equals("BUSCARCAT")){
   			
   			CargaBandejaCat(control);
   			request.setAttribute("cat","2");
   		}
   		else		
   	   	if(control.getOperacion().trim().equals("PFR")){
   	   		
   	   		PFR(control);
   	   	}
   	   	else
   	   	if(control.getOperacion().trim().equals("PCC")){
   	   		
   	   		PCC(control);
   	   	}
   	   	else
   	   	if(control.getOperacion().trim().equals("CAT")){
   	   		
   	   		CAT(control);
   	   	}
   	   	else
   	   	if(control.getOperacion().trim().equals("PAT")){
   	   		
   	   		PAT(control);
   	   	}
   	   	else
   	   	if(control.getOperacion().trim().equals("PIA")){
   	   		
   	   		PIA(control);
   	   	}
   	   	else
   	   	if(control.getOperacion().trim().equals("ESP")){
   	   		
   	   		ESP(control);
   	   	}
   	   	else
   	   	if(control.getOperacion().trim().equals("HIB")){
   	   		
   	   		HIB(control);
   	   	}
   	   	else   	 
    	if(control.getOperacion().trim().equals("VIR")){
	   		
	   		VIR(control);
	   	}
    	else
   	   	if(control.getOperacion().trim().equals("ESPECIALIDADPFR")){
   	   		
   	   		//ESPECIALIDADPFR(control);
   	   	}
   	   	else
   	   	if(control.getOperacion().trim().equals("CICLOPFR")){
   	   		
   	   		CICLOPFR(control);   	   		
   	   	}
   	   	else
   		if(control.getOperacion().trim().equals("PROGRAMAPCC")){
   			
   			PROGRAMAPCC(control);
   		}
   		else
   		if(control.getOperacion().trim().equals("ETAPA01")){
   			
   			PCC(control);
   		}
   	   	else
   		if(control.getOperacion().trim().equals("ETAPA02")){
   			
   			ETAPA02(control);
   		}
   	   	else
   		if(control.getOperacion().trim().equals("CICLOCAT")){
   			
   			CICLOCAT(control);
   		}
   		else
   		if(control.getOperacion().trim().equals("PROGRAMAESP")){
   			
   			PROGRAMAESP(control);
   		}
   		else
   		if(control.getOperacion().trim().equals("MODULOESP")){
   			
   			MODULOESP(control);
   		}
   		else
   	   	if(control.getOperacion().trim().equals("CICLOCAT02")){
			
			CICLOCAT02(control);
			request.setAttribute("cat","2");
		} else if (control.getOperacion().equals("GRABARNOTAEXTERNA")) {
			
			//COD_NOTA_EXTERNO (Constants)
			
			//String rpta = notaExternaManager.InsertNotaExterno(control.getCodPeriodo(), control.getCodProducto(), control.getCodCiclo(), control.getCodCurso(), control.getCadenaDatos(), CommonConstants.COD_NOTA_EXTERNO, control.getCodEvaluador(), control.getTamListAlumnos());
			String rpta = notaExternaManager.InsertNotaExterno(control.getCodPeriodo(), control.getCodProducto(), control.getCadenaDatos(), CommonConstants.COD_NOTA_EXTERNO, control.getCodEvaluador(), control.getTamListAlumnos());
			log.info("RPTA:" + rpta);
			if(rpta.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			
			CargaBandeja(control);
			request.setAttribute("cat","1");
			//request.setAttribute("config", control.getDatosConfigCurso());
			request.setAttribute("config", control.getListaDatosConfigCurso());

		}
   	   	
			
	    return new ModelAndView("/evaluaciones/registroNota/eva_nota_casos_especiales","control",control);		
    }    
    private void cargaPeriodo(NotaCasosEspecialesCommand command){
    	Periodo periodo;
    	ArrayList<Periodo> listPeriodo;
    	try{
    		
    		listPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoById(command.getCodPeriodo());
    		if ( listPeriodo != null ){
    			if ( listPeriodo.size() > 0)
    			{
    				periodo =listPeriodo.get(0);
    				command.setPeriodo01(periodo.getNombre());
    				command.setPeriodo02(periodo.getNombre());
    			}
    		}
    		command.setListCiclosCat(this.mantenimientoManager.getAllParametrosEspeciales(command.getCodPeriodo(), "0002"));    		
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    private void CargaBandeja(NotaCasosEspecialesCommand command){

    	String nombre = command.getNombreAlumno();
    	String apePat = command.getApePaternoAlumno();
    	String apeMat = command.getApeMaternoAlumno();
    	
    	command.setListCursoPFR(this.alumnoManager.listaCursosExternos(command.getCodPeriodo()));
    	
    	log.info("command.getCodEspecialidad():" + command.getCodEspecialidad());
    	
    	command.setListAlumnos(this.alumnoManager.getAllAlumnoCasoEspecial(command.getCodPeriodo(), command.getCodProducto(), command.getCodEspecialidad(), command.getCodEtapa(), command.getCodPrograma(), command.getCodCiclo(), command.getCodCurso(),nombre,apePat,apeMat));
    	
    	if (command.getListAlumnos()!=null) {    		    		
    		//command.setHidTamanio(String.valueOf(command.getListAlumnos().size()));
    	};
    	
    	command.setTamListAlumnos(""+command.getListAlumnos().size());
    	
    	//JHPR 2008-05-05 Obtener valores de la configuración del Producto (calculo de Nota Tecsup).
    	//notaExternaManager --> retorna obj ConfiguracionNotaExterna.    	
    	//command.setDatosConfigCurso(this.notaExternaManager.configuracionNotaExterna(command.getCodCurso()));
		
    	//obtengo lista de configuraciones de cursos nota externa segun periodo...
    	command.setListaDatosConfigCurso(this.notaExternaManager.listaConfiguracionNotaExterna(command.getCodPeriodo()));
		
    	actualizarCabecera(command);    
    }
    private void CargaBandejaCat(NotaCasosEspecialesCommand command){
    	CICLOCAT02(command);
    	
    	command.setCodProductoCat(CommonConstants.TIPT_PRODUCTO_CAT);    	
    	
    	command.setListAlumnosCat(this.alumnoManager.getAllAlumnoCasoEspecialCat(command.getCodPeriodo(), command.getCodProductoCat(), command.getTipoCicloCat(),command.getTipoCursoCat()));
    	
    	command.setTamListAlumnosCat(""+command.getListAlumnosCat().size());
    }
    
    //*****************************************************************************************************
    //metodos de renzo
    /*public List Cursos(Producto producto, Programas programas,Especialidad especialidad, Curso curso, String codEtapa){
		return this.mtoConfiguracionComponentesManager.getCursoByProductoEspecialidadCiclo(producto, programas,
				especialidad, curso, codEtapa);
	}*/
    public List Cursos(Producto producto, Programas programas,Especialidad especialidad, Curso curso, 
    		String codEtapa, String codPeriodo){
		//return this.mtoConfiguracionComponentesManager.getCursoByProductoEspecialidadCicloExt(producto, programas, especialidad, curso, codEtapa);
    	//return this.mtoConfiguracionComponentesManager.getCursoByProductoEspecialidadCiclo(producto, 
    		//	programas, especialidad, curso, codEtapa, codPeriodo);
    	return this.mtoConfiguracionComponentesManager.getCursoByProductoEspecialidadCicloExt(producto, programas, especialidad, curso, codEtapa, codPeriodo);
	}
	public List CursosSolos(Producto producto, String codPeriodo){
		return this.mtoConfiguracionComponentesManager.getCursosByProducto(producto, codPeriodo);
	}
	public List Ciclos(NotaCasosEspecialesCommand control, Producto producto, Programas programas, 
			Especialidad especialidad, String codPeriodo){    
    	return this.mtoConfiguracionComponentesManager.getCicloByProductoEspecialidadPrograma(producto, 
    			especialidad, programas, codPeriodo);	
    }    
    public List ListaEspecialidad(NotaCasosEspecialesCommand control, Producto producto){
    	return this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(producto);
    }
    public List ListaPrograma(NotaCasosEspecialesCommand control, Producto producto){	 
		return this.mtoConfiguracionComponentesManager.getProgramasByProducto(producto, control.getValorEtapa());	
	}
    //*****************************************************************************************************
    private void PFR(NotaCasosEspecialesCommand control){
    	Producto producto = new Producto();
    	
    	producto.setCodProducto(control.getTipoProducto());
    	//JHPR: 2008-05-20 Eliminar filtro por Especialidad.
    	//control.setListEspecialidadPFR(ListaEspecialidad(control, producto));
    	//Llenar lista de ciclos
    	
    	/*control.setListCicloPFR(this.mtoConfiguracionComponentesManager.
    		getCicloByProductoEspecialidadPrograma2(control.getTipoProducto(), 
    				"0", "", control.getCodPeriodo()));*/
    	
    }
    
    /*private void ESPECIALIDADPFR(NotaCasosEspecialesCommand control){
    	   		
    	Producto producto = new Producto();
   		producto.setCodProducto(control.getTipoProducto());   		   		
   		control.setListEspecialidadPFR(ListaEspecialidad(control, producto));
   		if(control.getTipoEspecialidadPFR()==""){
   			control.setListCicloPFR(null);
   		}
   		else{
   			control.setListCicloPFR(this.mtoConfiguracionComponentesManager.
   			getCicloByProductoEspecialidadPrograma2(control.getTipoProducto(), 
   					control.getTipoEspecialidadPFR(), "", control.getCodPeriodo()));
   		}   	
    }*/
    
    private void CICLOPFR(NotaCasosEspecialesCommand control){
    	Producto producto = new Producto();
   		producto.setCodProducto(control.getTipoProducto());
	   	Curso curso= new Curso();
   	   	curso.setCodCiclo(control.getTipoCicloPFR());
   	   	Programas programas = new Programas();
		programas.setCodPrograma("");
	    Especialidad especialidad = new Especialidad();
	  //JHPR: 2008-05-20 Comentado, no filtrar por especialidad.
	    //especialidad.setCodEspecialidad(control.getTipoEspecialidadPFR());
	    especialidad.setCodEspecialidad("");
	    
   		//control.setListCursoPFR(Cursos(producto, programas, especialidad, curso, "", control.getCodPeriodo()));
   		
		//control.setListCicloPFR(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));   		
		//control.setListEspecialidadPFR(ListaEspecialidad(control, producto));				    	    
		
    }
    private void PCC(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	String codEtapa="1";
		control.setValorEtapa(codEtapa);
		
		control.setListProgramaPCC(ListaPrograma(control, producto));
    }
    private void PROGRAMAPCC(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	Programas programas= new Programas();    	
    	programas.setCodPrograma(control.getTipoProgramaPCC());    			
		Especialidad especialidad=new Especialidad();
		especialidad.setCodEspecialidad("");
		Curso curso=new Curso();
		curso.setCodCiclo("");
		
		String codEtapa="";		
		
		control.setListCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
		control.setListProgramaPCC(ListaPrograma(control, producto));
    }
    private void ETAPA02(NotaCasosEspecialesCommand control){
		Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	Programas programas= new Programas();    	
    	programas.setCodPrograma("");    			
		Especialidad especialidad=new Especialidad();
		especialidad.setCodEspecialidad("");
		Curso curso=new Curso();
		curso.setCodCiclo("");
		
		String codEtapa="";
			
		control.setListCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));		
    }
    private void CAT(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	Programas programas= new Programas();    	
		programas.setCodPrograma("");
		Especialidad especialidad=new Especialidad();
		especialidad.setCodEspecialidad("");
				
		control.setListCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
    }
    private void CICLOCAT(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	Programas programas= new Programas();    	
		programas.setCodPrograma("");
		Especialidad especialidad=new Especialidad();
		especialidad.setCodEspecialidad("");
		Curso curso=new Curso();
		curso.setCodCiclo(control.getTipoCicloCAT());
		String codEtapa="";
				
		control.setListCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
		control.setListCursoCAT(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
    }
    private void PAT(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	
    	control.setListCursoPAT(CursosSolos(producto, control.getCodPeriodo()));
    }
    private void PIA(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	
    	control.setListCursoPIA(CursosSolos(producto, control.getCodPeriodo()));    	
    }
    private void ESP(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	
    	control.setListProgramaESP(ListaPrograma(control, producto));    	
    }
    private void PROGRAMAESP(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	Especialidad especialidad = new Especialidad();
    	especialidad.setCodEspecialidad("");
    	Programas programas=new Programas();
    	programas.setCodPrograma(control.getTipoProgramaESP());
    			
		control.setListProgramaESP(ListaPrograma(control, producto));
		control.setListModuloESP(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo())); 
    }
    private void MODULOESP(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	Especialidad especialidad = new Especialidad();
    	especialidad.setCodEspecialidad("");
    	Programas programas=new Programas();
    	programas.setCodPrograma(control.getTipoProgramaESP());
    	Curso curso=new Curso();
    	curso.setCodCiclo(control.getTipoModuloESP());
    	String codEtapa="";
    	
    	control.setListProgramaESP(ListaPrograma(control, producto));
		control.setListModuloESP(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
		control.setListCursoESP(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
    }
    private void HIB(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	
    	control.setListCursoHIB(CursosSolos(producto, control.getCodPeriodo()));
    }    
    private void VIR(NotaCasosEspecialesCommand control){
    	Producto producto=new Producto();
    	producto.setCodProducto(control.getTipoProducto());
    	
    	control.setListCursoVIR(CursosSolos(producto, control.getCodPeriodo()));
    }
    private void CICLOCAT02(NotaCasosEspecialesCommand control){
    	control.setListCiclosCat(this.mantenimientoManager.getAllParametrosEspeciales(control.getCodPeriodo(), "0002"));
    	control.setListCursosCat(this.mantenimientoManager.getAllCursosCat(control.getCodCAT(), control.getTipoCicloCat()));
    	control.setListAlumnos(null);
    	control.setCodProductoCat(CommonConstants.TIPT_PRODUCTO_CAT);
    	//carga el nombre del archivo si existe
    	
    	List a=this.notaExternaManager.getAdjuntoCasoCatById(control.getCodPeriodo(), control.getCodProductoCat(), control.getTipoCicloCat());
    	if ( a.size() > 0 ){
    		Archivo obj=(Archivo)a.get(0);
    		control.setArchivo(obj.getNombre());
    		//modificacion ver archivos    		
//    		String uploadDirArchivo = CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_IP + CommonConstants.STR_RUTA_DOCUMENTOS;            
    		String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA + CommonConstants.STR_RUTA_DOCUMENTOS;
            String sep = System.getProperty("file.separator");            
    		control.setRutaArchivo(uploadDirArchivo + obj.getNombreNuevo());
    		//*************************
		}
    }
    private void actualizarCabecera(NotaCasosEspecialesCommand command){
    	int tipo=0;    	
    	if(command.getCodProducto().equals(command.getCodPFR()))tipo=1;
    	if(command.getCodProducto().equals(command.getCodPCC()))tipo=2;
    	if(command.getCodProducto().equals(command.getCodCAT()))tipo=3;
    	if(command.getCodProducto().equals(command.getCodPAT()))tipo=4;
    	if(command.getCodProducto().equals(command.getCodPIA()))tipo=5;
    	if(command.getCodProducto().equals(command.getCodPE()))tipo=6;
    	if(command.getCodProducto().equals(command.getCodHIBRIDO()))tipo=7;
    	if(command.getCodProducto().equals(command.getCodTecsupVirtual()))tipo=8;
    	if(command.getCodProducto().equals(command.getCodPCC01()))tipo=9;
    	
    	switch (tipo) {
			case 1:
				
				CICLOPFR(command);
				break;
			case 2:
				PCC(command);
				if(command.getCodCurso()!=""){
					PROGRAMAPCC(command);
				}
							
				break;
			case 3:
				
				CICLOCAT(command);
				break;
			case 4:
				PAT(command);				
				break;
			case 5:
				PIA(command);
				break;
			case 6:
				
				MODULOESP(command);
				break;
			case 7:
				HIB(command);
				break;
			case 8:
				VIR(command);
				break;
			case 9:
				ETAPA02(command);
				break;
			default:
				break;
		}
    }
}
