package com.tecsup.SGA.web.evaluaciones.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.service.evaluaciones.GestionAdministradtivaManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarExamenesCommand;

public class RegistrarExamenFinalFormController extends SimpleFormController {
	private OperDocenteManager operDocenteManager;
	private GestionAdministradtivaManager gestionAdministradtivaManager;
	private static Log log = LogFactory.getLog(RegistrarExamenFinalFormController.class);
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	public void setGestionAdministradtivaManager(GestionAdministradtivaManager gestionAdministradtivaManager) {
		this.gestionAdministradtivaManager = gestionAdministradtivaManager;
	}
	
	@Override
	protected Object formBackingObject(HttpServletRequest request)	throws Exception {
		RegistrarExamenesCommand command = new RegistrarExamenesCommand();
		
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEval((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	command.setModoExamen((String)request.getParameter("txhModo"));
    	
    	if(command.getCodPeriodo() != null && command.getCodEval() != null && command.getCodCurso() != null){
	    	inicializaSeccion(command, command.getCodCurso());
	    	inicializaTipoExams(command, request, command.getCodPeriodo(), command.getCodCurso());
	    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEval(), command.getCodCurso());
	    	command.setSeccion(command.getPrimeraSeccion()); //al comienzo siempre q busque la primera secci�n
	    	inicializaDataAlumno(command, command.getCodCurso(), command.getSeccion(), "", "");
	    	
    		List<Evaluador> lista = operDocenteManager.obtenerEvaluadorSeccion(command.getCodCurso(), command.getSeccion());
    		if (lista!=null){
    			if(lista.size()>0){
    				Evaluador eval = (Evaluador) lista.get(0);
        			command.setNomEvaluadorSeccion(eval.getDscEvaluador());	
    			}else
    				command.setNomEvaluadorSeccion("---");
    		}else{
    			command.setNomEvaluadorSeccion("---");
    		}
    		
	    	request.setAttribute("listaAlumnos", command.getListaAlumnos());
		}
    	    	
    	command = cargaParametrosDeBusqueda(command, request);    	    	    	
        return command;
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,	HttpServletResponse response, Object command, BindException errors) throws Exception {
    	
    	RegistrarExamenesCommand control = (RegistrarExamenesCommand)command;  	
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if ("BUSCAR".equals(control.getOperacion())){
    		inicializaSeccion(control, control.getCodCurso());
    		inicializaTipoExams(control, request, control.getCodPeriodo(), control.getCodCurso());
    		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), control.getCodCurso());
    		inicializaDataAlumno(control, control.getCodCurso(), control.getSeccion(), control.getNombre(), 
    							control.getApellido());
    		control.setSeccion(control.getSeccion());
    		
    		List<Evaluador> lista = operDocenteManager.obtenerEvaluadorSeccion(control.getCodCurso(), control.getSeccion());
    		if (lista!=null){
    			Evaluador eval = (Evaluador) lista.get(0);
    			control.setNomEvaluadorSeccion(eval.getDscEvaluador());
    		}else{
    			control.setNomEvaluadorSeccion("---");
    		}
    		
    		if(control.getListaNotas()==null){
    			control.setMessage(CommonMessage.NO_REGISTROS);
        		control.setTypeMessage("NO");
    		}
    	}
    	else if("GRABAR".equals(control.getOperacion())){
    		
    		
    		
    		String rpta;
    		rpta = grabarNotas(control.getCodPeriodo(),control.getCodCurso(), control.getSeccion(), control.getCadena(),
    				control.getCodEval());
    		log.info("RPTA:" + rpta);
    		if ("-1".equals(rpta)){
        		control.setMessage(CommonMessage.GRABAR_ERROR);
        		control.setTypeMessage("ERROR");
        	}
    		else{
    			inicializaTipoExams(control, request, control.getCodPeriodo(), control.getCodCurso());
    			inicializaDataAlumno(control, control.getCodCurso(), control.getSeccion(), control.getNombre(), 
						control.getApellido());
    			inicializaSeccion(control, control.getCodCurso());
    			control.setSeccion(control.getSeccion());
    	
    			control.setMessage(CommonMessage.GRABAR_EXITO);
        		control.setTypeMessage("OK");
        	}
    	}
    	
    	
	    return new ModelAndView("/evaluaciones/gestionAdministrativo/eva_examenfinal_registro_adm","control",control);		
	}


    private void inicializaSeccion(RegistrarExamenesCommand command, String strCodCurso){
    	CursoEvaluador lObject;    	   
    	command.setListaSeccion(this.operDocenteManager.getAllSeccionCurso("", strCodCurso));		    	
    	ArrayList firstSeccion ;
    	if (command.getModoExamen().equals("0"))
    		firstSeccion = (ArrayList)this.operDocenteManager.getAllSeccionCurso(CommonConstants.TIPO_SESION_TEO, strCodCurso);
    	else
    		firstSeccion = (ArrayList)this.operDocenteManager.getAllSeccionCurso("", strCodCurso);    		
    	    	
    	if (firstSeccion.size() > 0){
    		lObject = (CursoEvaluador) firstSeccion.get(0);
    		command.setPrimeraSeccion(lObject.getCodSeccion());
    	}
    }
    
   
	private void inicializaTipoExams(RegistrarExamenesCommand command, HttpServletRequest request, String codPeriodo, String codCurso){		
		command.setListaTipoExams(this.gestionAdministradtivaManager.getAllTypesOfExams(codPeriodo, codCurso, command.getModoExamen()));
		request.setAttribute("listaTipoExams", command.getListaTipoExams());
	}

	
	private void inicializaDataCurso(RegistrarExamenesCommand command, String strPeriodo, String strEval,String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval,"","", strCodCurso, "","");		
		if (dataCurso.size()>0){
			lObject = (CursoEvaluador) dataCurso.get(0);
			command.setPeriodo(lObject.getPeriodo());
			command.setEvaluador(lObject.getEvaluador());
			command.setPrograma(lObject.getProducto());
			command.setCiclo(lObject.getCiclo());
			command.setCurso(lObject.getCurso());
			command.setEspecialidad(lObject.getEspecialidad());
			command.setSistEval(lObject.getSistEval());
		}
	}
	
	
	private void inicializaDataAlumno(RegistrarExamenesCommand command, String codCurso, String codSeccion,String strNombre, String strApellido){
		command = gestionAdministradtivaManager.setConsulta(command);
	}	

		
	private RegistrarExamenesCommand cargaParametrosDeBusqueda(RegistrarExamenesCommand control, HttpServletRequest request){
		control.setPrmOperacion(request.getParameter("prmOperacion")); 		
		control.setPrmCodSelCicloPfr(request.getParameter("prmCodSelCicloPfr"));
		control.setPrmEspecialidadPfr(request.getParameter("prmEspecialidadPfr"));
		control.setPrmCodProducto(request.getParameter("prmCodProducto"));
		control.setPrmVerCargos(request.getParameter("prmVerCargos"));
		control.setPrmCodPeriodoVig(request.getParameter("prmCodPeriodoVig"));
		return control;
	}
	
	private String grabarNotas(String codPeriodo, String codCurso, String codSeccion, String cadNotas, 
			String codUsuario){		
		return this.gestionAdministradtivaManager.insertNotasExamenes(codPeriodo, codCurso, codSeccion, cadNotas, 
																	codUsuario);
	}	
}
