package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.*;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.modelo.TipoTablaDetalle;


public class MtoConceptoPerfilFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(MtoConceptoPerfilFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
		public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
			this.tablaDetalleManager = tablaDetalleManager;
		}
	String descrip="";
	protected Object formBackingObject(HttpServletRequest request)
		throws ServletException {
	
	MtoConceptoPerfilCommand command = new MtoConceptoPerfilCommand();
	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
	
	List perfil=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PERFIL, "", 
			"", "",	"", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
	if(perfil!=null)
		command.setListAreasNivelUno(perfil);
	else
		command.setListAreasNivelUno(new ArrayList());	
	
	
	request.getSession().setAttribute("listaAreasIntUno", command.getListAreasNivelUno());
	command.setMsg("");
	
	return command;
	}
	
	protected void initBinder(HttpServletRequest request,
		ServletRequestDataBinder binder) {
	NumberFormat nf = NumberFormat.getNumberInstance();
	binder.registerCustomEditor(Long.class, new CustomNumberEditor(
			Long.class, nf, true));
	}
	
	
	public ModelAndView processFormSubmission(HttpServletRequest request,
		HttpServletResponse response, Object command, BindException errors)
		throws Exception {
	return super.processFormSubmission(request, response, command, errors);
	}
	
	public ModelAndView onSubmit(HttpServletRequest request,
		HttpServletResponse response, Object command, BindException errors)
		throws Exception {
	
	String resultado ="";
	MtoConceptoPerfilCommand control = (MtoConceptoPerfilCommand) command;
	log.info("OPERACION:" + control.getOperacion());
	
	
	if (control.getOperacion().trim().equals("NIVEL2"))
	{   List mad=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PERFIL, control.getCodAreasNivelUno().trim(), 
			"", "",	"", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
	    TipoTablaDetalle tipoTablaDetalle1= new TipoTablaDetalle();
	    if(mad!=null)
	    {	log.info("tama�o lista MAD: "+mad.size());
	    	tipoTablaDetalle1=(TipoTablaDetalle)mad.get(0);
	    }
	    log.info("descripcion MAD: "+tipoTablaDetalle1.getDescripcion());
	    List lista1=this.tablaDetalleManager.getAllTablaDetalle(
	    		CommonConstants.TIPT_CONCEPTOS, "", "",	control.getCodAreasNivelUno().trim(),
	    		"", "", "",	"", CommonConstants.TIPO_ORDEN_DSC);
	    if(lista1!=null)
		control.setListAreasNivelDos(lista1);
	    else control.setListAreasNivelDos(new ArrayList());
	    
		descrip=tipoTablaDetalle1.getDescripcion().trim();
		control.setDescripcionNivel1(descrip);
		control.setDscNivelUno(descrip);
		
		request.getSession().setAttribute("listaAreasIntDos", control.getListAreasNivelDos());
		request.setAttribute("Nivel", "OK");
		
	} 
	else if (control.getOperacion().trim().equals("ELIMINAR_PERFIL")) {

		resultado = DeleteTablaDetalle(control);
		log.info("RPTA:" + resultado);
		if ( resultado.equals("-1")) request.setAttribute("Msg1OK", "ERROR");
		else{ 
			request.setAttribute("Msg1OK", "OK");
			List lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PERFIL, "", "", "",
					"", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
			if(lista2!=null)
			control.setListAreasNivelUno(lista2);
			request.getSession().setAttribute("listaAreasIntUno", control.getListAreasNivelUno());
		}
		control.setOperacion("");
	} else if (control.getOperacion().trim().equals("ELIMINAR_CONCEPTO")) {
		resultado = DeleteTablaDetalle2(control);
		log.info("RPTA:" + resultado);
		if ( "-1".equals(resultado)) request.setAttribute("Msg2OK", "ERROR");
			
		else{ 
			request.setAttribute("Msg2OK", "OK");
			
		    control.setListAreasNivelDos(this.tablaDetalleManager.getAllTablaDetalle(
				CommonConstants.TIPT_CONCEPTOS, "", "",	control.getCodAreasNivelUno().trim(),
				"", "", "",	"", CommonConstants.TIPO_ORDEN_DSC));
				
			request.getSession().setAttribute("listaAreasIntDos", control.getListAreasNivelDos());
			
			}
		
		control.setDescripcionNivel1(descrip);
		control.setOperacion("NIVEL2");
		request.setAttribute("Nivel", "OK");
		
	}
	
	
	return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_concepto_perfil", "control", control);
	}
	
	private String DeleteTablaDetalle(MtoConceptoPerfilCommand control) {
	try {
		TipoTablaDetalle obj = new TipoTablaDetalle();
	   	obj.setCodTipoTabla(CommonConstants.TIPT_PERFIL);
		obj.setCadCod(control.getCodProcesosSeleccionados());
		obj.setNroReg(control.getNroSelec());
		if (!obj.getCadCod().trim().equals("")) {
			obj.setCodTipoTablaDetalle(this.tablaDetalleManager
					.DeleteTablaDetalle(obj, control.getCodEvaluador()));
			
			return obj.getCodTipoTablaDetalle();
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	return null;
	}
	
	private String DeleteTablaDetalle2(MtoConceptoPerfilCommand control) {
	try {
		TipoTablaDetalle obj = new TipoTablaDetalle();
	
		obj.setCodTipoTabla(CommonConstants.TIPT_CONCEPTOS);		
		obj.setCadCod(control.getCodProcesosSeleccionados2());		
		obj.setNroReg(control.getNroSelec());
		
		if (!obj.getCadCod().trim().equals("")) {			
			obj.setCodTipoTablaDetalle(this.tablaDetalleManager
					.DeleteTablaDetalle(obj,control.getCodEvaluador()));
			return obj.getCodTipoTablaDetalle();
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	return null;
	}

		}
