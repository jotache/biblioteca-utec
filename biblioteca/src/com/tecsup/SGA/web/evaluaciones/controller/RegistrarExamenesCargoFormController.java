//JHPR 28/3/2008 Para registrar examenes de cargo.
package com.tecsup.SGA.web.evaluaciones.controller;

import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.service.evaluaciones.GestionAdministradtivaManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarExamenesCommand;

public class RegistrarExamenesCargoFormController extends SimpleFormController {

	private static Log log = LogFactory.getLog(RegistrarExamenesCargoFormController.class);
	private OperDocenteManager operDocenteManager;
	private GestionAdministradtivaManager gestionAdministradtivaManager;

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		
		RegistrarExamenesCommand command = new RegistrarExamenesCommand();

    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEval((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	//JHPR 28/3/2008 Para registrar examens de cargo
    	command.setModoExamen((String)request.getParameter("txhModo"));
    	
    	if(command.getCodPeriodo() != null && command.getCodEval() != null && command.getCodCurso() != null){
    		inicializaTipoExams(command, request, command.getCodPeriodo(), command.getCodCurso());
    		inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEval(), command.getCodCurso());
    		command.setSeccion(null); //Examenes de cargo no tienen secci�n.
    		inicializaDataAlumno(command, command.getCodCurso(), command.getSeccion(), "", "");
    	}
    	
    	command = cargaParametrosDeBusqueda(command, request);
    	
		return command;
	}
	
	private void inicializaDataAlumno(
			RegistrarExamenesCommand command, String codCurso, String codSeccion, 
			String strNombre, String strApellido){
			command = gestionAdministradtivaManager.setConsulta(command);
	}
	
	private void inicializaDataCurso(
			RegistrarExamenesCommand command, String strPeriodo, String strEval,String strCodCurso){
		
		
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval,"",""
				, strCodCurso, "","");
		
		if (dataCurso.size()>0){
			lObject = (CursoEvaluador) dataCurso.get(0);
			command.setPeriodo(lObject.getPeriodo());
			command.setEvaluador(lObject.getEvaluador());
			command.setPrograma(lObject.getProducto());
			command.setCiclo(lObject.getCiclo());
			command.setCurso(lObject.getCurso());
			command.setEspecialidad(lObject.getEspecialidad());
			command.setSistEval(lObject.getSistEval());
		}
	}
	
	public RegistrarExamenesCargoFormController() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

	}

	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public void setGestionAdministradtivaManager(
			GestionAdministradtivaManager gestionAdministradtivaManager) {
		this.gestionAdministradtivaManager = gestionAdministradtivaManager;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		RegistrarExamenesCommand control = (RegistrarExamenesCommand)command;
		log.info("OPERACION:" + control.getOperacion());
		
		if ("BUSCAR".equals(control.getOperacion())){
    		//inicializaSeccion(control, control.getCodCurso());
			control.setSeccion(null);
			
    		inicializaTipoExams(control, request, control.getCodPeriodo(), control.getCodCurso());
    		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), control.getCodCurso());
    		inicializaDataAlumno(control, control.getCodCurso(), control.getSeccion(), control.getNombre(), 
    							control.getApellido());
    		//control.setSeccion(control.getSeccion());
    		
    		if(control.getListaNotas()==null){
    			control.setMessage(CommonMessage.NO_REGISTROS);
        		control.setTypeMessage("NO");
    		}
    	}
		else if("GRABAR".equals(control.getOperacion())){
    		    		    		
    		String rpta;
    		rpta = grabarNotas(control.getCodPeriodo(),control.getCodCurso(), control.getCadena(), control.getCodEval());
    		log.info("RPTA:" + rpta);
    		if ("-1".equals(rpta)){
        		control.setMessage(CommonMessage.GRABAR_ERROR);
        		control.setTypeMessage("ERROR");
        	}
    		else{
    			control.setSeccion(null);
    			inicializaTipoExams(control, request, control.getCodPeriodo(), control.getCodCurso());
    			inicializaDataAlumno(control, control.getCodCurso(), control.getSeccion(), control.getNombre(), 
						control.getApellido());
    			//inicializaSeccion(control, control.getCodCurso());    			
    			
    			control.setMessage(CommonMessage.GRABAR_EXITO);
        		control.setTypeMessage("OK");
        	}
    	}
    	
    	
		
		return new ModelAndView("/evaluaciones/gestionAdministrativo/eva_examen_cargo_registro_adm","control",control);
	}
	
	private String grabarNotas(String codPeriodo, String codCurso, String cadNotas, String codUsuario){

		//return this.gestionAdministradtivaManager.insertNotasExamenes(codPeriodo, codCurso, codSeccion, cadNotas,	codUsuario);
		return this.gestionAdministradtivaManager.insertNotasExamenesCargo(codPeriodo, codCurso, cadNotas, codUsuario, "EC");
	}
	private void inicializaTipoExams(RegistrarExamenesCommand command, HttpServletRequest request, String codPeriodo, String codCurso){
		
		command.setListaTipoExams(this.gestionAdministradtivaManager.getAllTypesOfExams(codPeriodo
					, codCurso, command.getModoExamen()));
		request.setAttribute("listaTipoExams", command.getListaTipoExams());
	}

	private RegistrarExamenesCommand cargaParametrosDeBusqueda(RegistrarExamenesCommand control, HttpServletRequest request){
		control.setPrmOperacion(request.getParameter("prmOperacion")); 		
		control.setPrmCodSelCicloPfr(request.getParameter("prmCodSelCicloPfr"));
		control.setPrmEspecialidadPfr(request.getParameter("prmEspecialidadPfr"));
		control.setPrmCodProducto(request.getParameter("prmCodProducto"));
		control.setPrmVerCargos(request.getParameter("prmVerCargos"));
		control.setPrmCodPeriodoVig(request.getParameter("prmCodPeriodoVig"));
		return control;
	}
}
