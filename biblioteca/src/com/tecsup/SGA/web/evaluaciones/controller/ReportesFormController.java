package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonSeguridad;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.service.evaluaciones.MtoConfiguracionComponentesManager;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.ReportesCommand;

public class ReportesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ReportesFormController.class);
 	OperDocenteManager operDocenteManager;
 	private ComunManager comunManager;	
 	private TablaDetalleManager tablaDetalleManager;
 	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
 	MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager;
 	private MantenimientoManager mantenimientoManager;

	public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}

	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
			this.operDocenteManager = operDocenteManager;
		}
	
    public ReportesFormController() {    	
        super();        
        setCommandClass(ReportesCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    		
    	ReportesCommand command = new ReportesCommand();
    	command.setUsuario((String)request.getParameter("txhCodEvaluador"));
    	
    	llenarSedes(command, request); 
    	command.setListreportes(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REPORTES_EVA
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	command.setListProducto(this.mtoParametrosGeneralesManager.getProductos());
    	//command.setListPeriodo(this.mtoParametrosGeneralesManager.getPeriodos());
    	if(command.getListSede()!=null)
    	{ if(command.getListSede().size()>0)
    	  { SedeBean sedeBean = new SedeBean();
    	    sedeBean=(SedeBean)command.getListSede().get(0);
    	    command.setSede(sedeBean.getCodSede());
    	    
    	    LlenarComboPeriodo(command);
    	  }
    	}
    	
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	ReportesCommand control = (ReportesCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	if(control.getOperacion().equals("BUSCAR")){
    		//System.out.println("usuario:onSubmit():" + control.getUsuario());
    		llenarSedes(control, request);
    		llenarEspecialidad(control);
    		LlenarComboPeriodo(control);
    	}
    	else{if(control.getOperacion().equals("PERIODO")){    		
    			llenarSedes(control, request);
    			llenarEspecialidad(control);
    			LlenarComboPeriodo(control);
    		}
    		
    	}
    	control.setOperacion("");
    	
	    return new ModelAndView("/evaluaciones/consultasReportes/reportes","control",control);		
    }
        private void llenarSedes(ReportesCommand command, HttpServletRequest request)
        {
          ArrayList<TipoTablaDetalle> arrSedes;
          ArrayList<SedeBean> listaSede;
          SedeBean sede;
          
      	  if ( CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CARG") == 1
    	  || CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_ADMI") == 1
    	  || CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_JDEP") == 1
    	  || CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1 )
      	  {
        	command.setListSede(this.comunManager.GetSedesByEvaluador(command.getUsuario()));
      	  }
      	  else
      	  {
      		listaSede = new ArrayList<SedeBean>();
      		arrSedes = (ArrayList<TipoTablaDetalle>)this.tablaDetalleManager.getAllTablaDetalle( 
      					CommonConstants.TIPT_SEDE, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
      		if ( arrSedes != null)
      		{
      			if ( arrSedes.size() > 0 )
      			{
      				for (int j = 0; j < arrSedes.size() ; j++)
      				{
      					sede = new SedeBean();
      					sede.setCodSede(arrSedes.get(j).getDscValor1());
      					sede.setDscSede(arrSedes.get(j).getDescripcion());
      					listaSede.add(sede);
      				}
      			}
      		}
      		command.setListSede(listaSede);
      	  }
      	 
        }

        private void llenarEspecialidad(ReportesCommand control){
        	if(!control.getProducto().equals("-1")){
        	Producto obj = new Producto();
        	obj.setCodProducto(control.getProducto());
        	control.setListEspecialidad(this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(obj));
        	}
        	else{
        		Producto obj = new Producto();
            	obj.setCodProducto("0000");
            	control.setListEspecialidad(this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(obj));
        	}
        }
        
        private void LlenarComboPeriodo(ReportesCommand control){
        
	        //control.setProducto("-1");
	        control.setEspecialidad("");
	        List lista= new ArrayList();	
	        lista=this.mantenimientoManager.GetAllPeriodosBySede(control.getSede());
	        if(lista!=null)
	         control.setListPeriodo(lista);
	        else control.setListPeriodo(new ArrayList());
        
        }
        
		public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
			this.tablaDetalleManager = tablaDetalleManager;
		}

		public void setMtoParametrosGeneralesManager(
				MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
			this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
		}

		public void setMtoConfiguracionComponentesManager(
				MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager) {
			this.mtoConfiguracionComponentesManager = mtoConfiguracionComponentesManager;
		}
}
