package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.modelo.CompetenciasPerfil;
import com.tecsup.SGA.web.evaluaciones.command.EvaCompetenciaBandejaCommand;

public class EvalCompetenciasBandejaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(IncidenciaRegistrarDocFormController.class);
	OperDocenteManager operDocenteManager;	
	TablaDetalleManager tablaDetalleManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public EvalCompetenciasBandejaFormController(){
		super();
		setCommandClass(EvaCompetenciaBandejaCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	EvaCompetenciaBandejaCommand command = new EvaCompetenciaBandejaCommand();  	
    	   	
    	/*Recogiendo valores*/
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	
    	//Llenando información
    	if ( command.getCodCurso() != null )
    	{
    		//Inicializa valores.
	    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEvaluador(), command.getCodCurso());
	    	//Se llena la seccion
	    	inicializaSeccion(command, command.getCodCurso());
	    	//Sete el valor del combo en la primera seccion
	    	ArrayList<CursoEvaluador> listSeccion = (ArrayList<CursoEvaluador>)command.getListaSeccion();
	    	if ( listSeccion != null )
	    	{
		    	if ( listSeccion.size() > 0 )
		    		command.setCodSeccion(listSeccion.get(0).getCodSeccion());
	    	}
	    	//Lleno lista de competecnias.
	    	llenaListaCompetencias(command);
    	
	    	//LLeno la lista de alumnos	    	
	    	buscarAlumnos(command);	    	
    	}
    	request.getSession().setAttribute("listaAlumnoComp", command.getListaBandeja());
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	
    	EvaCompetenciaBandejaCommand control = (EvaCompetenciaBandejaCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	inicializaSeccion(control, control.getCodCurso());
    	
    	if ( control.getOperacion().trim().equals("BUSCAR"))
    	{
    		buscarAlumnos(control);
    	}    	
    	//Lleno lista de competecnias.
    	llenaListaCompetencias(control);
    	request.getSession().setAttribute("listaAlumnoComp", control.getListaBandeja());
		
	    return new ModelAndView("/evaluaciones/cargaOperativa/eva_competenciasbandeja","control",control);		
    }
    
	private void inicializaDataCurso(EvaCompetenciaBandejaCommand command, String strPeriodo, String strEval, String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval
				,"","", strCodCurso, "","");
		
    	if (dataCurso.size()>0){
    		lObject = (CursoEvaluador) dataCurso.get(0);
    		command.setPeriodoVigente(lObject.getPeriodo());
    		command.setCiclo(lObject.getCiclo());
    		command.setProducto(lObject.getProducto());
    		command.setNomEvaluador(lObject.getEvaluador());
    		command.setCurso(lObject.getCurso());
    		command.setEscialidad(lObject.getEspecialidad());
    		command.setSistemaEval(lObject.getSistEval());
    	}    	
	}
	
	private void buscarAlumnos(EvaCompetenciaBandejaCommand control)
	{
		control.setListaBandeja(
					this.operDocenteManager.getAllConpetenciasByCurso(
							control.getCodPeriodo()
							, control.getCodCurso()							
							, control.getCodSeccion()
							, control.getNombreAlumno()
							, control.getApellidoAlumno()
							, CommonConstants.COMP_IND_DOC
							, control.getCodTipoSesionDefecto()));
		
	}

    private void inicializaSeccion(EvaCompetenciaBandejaCommand command, String strCodCurso){
    	ArrayList arrTipoSesiones = (ArrayList)this.operDocenteManager.getAllSesionXCurso(strCodCurso); 
    	CursoEvaluador cursoEvaluador;
    	boolean flag = true;
    	if ( arrTipoSesiones != null )
    	{
    		//BUSCO TIPO SESION TEORIA
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TEO))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO LABORATORIO 
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_LAB))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO LABORATORIO
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TAL))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    	}
    	//command.setListaSeccion(this.operDocenteManager.getAllSeccionCurso("", strCodCurso));
    }
    
    private void llenaListaCompetencias(EvaCompetenciaBandejaCommand control)
    {
    	
    	control.setListaCompetencias(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_COMPETENCIAS
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
    	double anchoCol = 150;
    	control.setNroColumna( Integer.toString(control.getListaCompetencias().size() + 1));
    	
    	if ( ( ( 950 - 30 - 80 - 220 ) / (control.getListaCompetencias().size() + 1 ) ) > 150 )
    		anchoCol = ( 950 - 30 - 80 - 220 ) / (control.getListaCompetencias().size() + 1 );
    	
    	control.setAnchoTabla(
    				Double.toString(
    						anchoCol * (control.getListaCompetencias().size() + 1 ) //Ancho por defecto de las columnas * el numero de columnas
    						+ 30 //Ancho de la primera columna 
    						+ 80 //Ancho de la segunda columa
    						+ 220 //Ancho de la tercera columna
    						)); 
    	
    	control.setAnchoColumna(Double.toString(anchoCol));
    	
    }
}
