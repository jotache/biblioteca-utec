package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.NotaExternaManager;
import com.tecsup.SGA.service.evaluaciones.CursoConfigManager;
import com.tecsup.SGA.service.evaluaciones.ProductosManager;
import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.common.*;

public class MtoNotaExternaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoNotaExternaFormController.class);
	private ProductosManager productosManager;
	private CursoConfigManager cursoConfigManager; 
	private NotaExternaManager notaExternaManager;
	
	
	public void setCursoConfigManager(CursoConfigManager cursoConfigManager) {
		this.cursoConfigManager = cursoConfigManager;
	}

	public void setNotaExternaManager(NotaExternaManager notaExternaManager) {
		this.notaExternaManager = notaExternaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	MtoNotaExternaCommand command = new MtoNotaExternaCommand();
    	
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	
    	command.setListCursos(this.notaExternaManager.getAllCursosNotaExterna());
    	establecerSeleccion(command.getListCursos());
    	request.getSession().setAttribute("listCursos",command.getListCursos());    	    	
   	    
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    	throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    			
		MtoNotaExternaCommand control = (MtoNotaExternaCommand) command;		
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_nota_externa","control",control);		
    }

	
	public void setProductosManager(ProductosManager productosManager) {
		this.productosManager = productosManager;
	}
	private void establecerSeleccion(List lstSeleccion){
		Curso curso = null;
		for (int i = 0; i < lstSeleccion.size(); i++) {
			curso = (Curso)lstSeleccion.get(i);
			if(this.cursoConfigManager.getCursoConfigById(curso.getCodCurso()).size()>0){
				curso.setSeleccion("checked");
				lstSeleccion.remove(i);
				lstSeleccion.add(i,curso);
			}
			
		}		
	}	 
}