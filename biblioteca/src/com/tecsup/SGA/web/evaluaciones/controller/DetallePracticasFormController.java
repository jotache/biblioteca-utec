package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.web.evaluaciones.command.ControlAsistenciaCommand;
import com.tecsup.SGA.web.evaluaciones.command.DetallePracticasCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.TipoExamenes;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.NumeroMinimo;
import com.tecsup.SGA.modelo.TipoPracticas;

public class DetallePracticasFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(DetallePracticasFormController.class);
 	OperDocenteManager operDocenteManager;
 	
 	MantenimientoManager mantenimientoManager;
	
    public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	public DetallePracticasFormController() {    	
        super();        
        setCommandClass(DetallePracticasCommand.class);        
    }
	protected Object formBackingObject(HttpServletRequest request) throws ServletException {    	   
    	DetallePracticasCommand command = new DetallePracticasCommand();
    	Parciales obj = new Parciales();    
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	command.setCurso((String)request.getParameter("txhCurso"));
    	command.setCiclo((String)request.getParameter("txhCiclo"));
    	command.setEspecialidad((String)request.getParameter("txhEspecialidad"));
    	command.setEvaluador((String)request.getParameter("txhEval"));
    	command.setPeriodo((String)request.getParameter("txhPeriodo"));
    	command.setProducto((String)request.getParameter("txhProducto"));
    	command.setSistEval((String)request.getParameter("txhSistEval"));
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
    	command.setCod((String)request.getParameter("txhCodEvaluador"));
    	
    	if(!(command.getCodProducto()==null)){
    		if(!command.getCodProducto().trim().equals("")){   		
    			
    			numero(command);      		
    			obj.setCodPeriodo(command.getCodPeriodo());
    			obj.setCodCurso(command.getCodCurso());        	
    			command.setListEvaluaciones(this.mantenimientoManager.getTipoPracticaxCurso(obj.getCodPeriodo(), obj.getCodCurso()));
    			int nro=0;
    			String primeraSecc = "";        	
				for (Iterator it = command.getListEvaluaciones().iterator(); it.hasNext();) {
					TipoPracticas practicas = (TipoPracticas) it.next();
					if (nro==0){					
						//TipoPracticas nPractica = new TipoPracticas();					
						primeraSecc = practicas.getCodigo();// + " " + practicas.getDescripcion();
					}
					nro += 1;
				}        			
				command.setCodigo(primeraSecc);			
				inicializaSeccion(command);						
	      		obj.setCodPro(command.getCodProducto());
	        	obj.setCodEspe(command.getCodEspecialidad());        	
	        	obj.setSeccion(command.getPrimeraSeccion());
	        	obj.setCodEvaluador(command.getEvaluador());
	        	obj.setCodTipoEvaluacion(command.getCodigo());       	
	        	
             
	        	//JHPR 2008-07-01 Evaluar estado de registro de notas        	
	        	evaluarEstadoRegNotas(command);        	
	        	command.setListaCursos(this.operDocenteManager.GetAllEvaluaciones(obj));        	        
	        	command.setCantidad(command.getListaCursos().size());        	
	        	request.getSession().setAttribute("listaCursos", command.getListaCursos());
	        	request.getSession().setAttribute("listaSeccion", command.getListaSeccion());        	        
	  		}
    	}      		
    	
    	return command;
	 }  
        
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
       
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
    		throws Exception {
		String resultado = "";		
    	DetallePracticasCommand control = (DetallePracticasCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals(""))
    	{
    		
    		Parciales dobj = new Parciales();
    		//CargaDetalle(control);
        	
       		control.setMinimo(control.getMin());
       		inicializaSeccion(control);
       		dobj.setCodPeriodo(control.getCodPeriodo());
      		dobj.setCodPro(control.getCodProducto());
        	dobj.setCodEspe(control.getCodEspecialidad());
        	dobj.setCodCurso(control.getCodCurso());
        	dobj.setSeccion(control.getSeccion());
        	dobj.setCodEvaluador(control.getEvaluador());
        	dobj.setCodTipoEvaluacion(control.getCodigo());
        	//numero2(control);
        	numero(control);
        	
        	control.setListEvaluaciones(this.mantenimientoManager.getTipoPracticaxCurso(dobj.getCodPeriodo(), dobj.getCodCurso()));
        	//request.getSession().setAttribute("listEvaluaciones", control.getListEvaluaciones());
        	
        	control.setListaCursos(this.operDocenteManager.GetAllEvaluaciones(dobj));
        	control.setCantidad(control.getListaCursos().size());
        	
        	//JHPR 2008-7-1 EVAlUAR ESTADO REG X CIERRE DE NOTAS.
        	evaluarEstadoRegNotas(control);

        	request.getSession().setAttribute("listaCursos", control.getListaCursos());        	
        	request.getSession().setAttribute("listaSeccion", control.getListaSeccion());
        	
    	}
    	if (control.getOperacion().trim().equals("DETALLE"))
    	{    	
    	   	control.setSeccion("");    	 
    		Parciales dobj = new Parciales();
    		
       		control.setMinimo(control.getMin());
       		inicializaSeccion(control);
       		       		
       		dobj.setCodPeriodo(control.getCodPeriodo());
      		dobj.setCodPro(control.getCodProducto());
        	dobj.setCodEspe(control.getCodEspecialidad());
        	dobj.setCodCurso(control.getCodCurso());
        	dobj.setSeccion(control.getPrimeraSeccion());        	
        	dobj.setCodEvaluador(control.getEvaluador());
        	dobj.setCodTipoEvaluacion(control.getCodigo());
        	//numero2(control);
        	numero(control);
        	
        	control.setListEvaluaciones(this.mantenimientoManager.getTipoPracticaxCurso(dobj.getCodPeriodo(), dobj.getCodCurso()));        	        	
        	control.setListaCursos(this.operDocenteManager.GetAllEvaluaciones(dobj));
        	control.setCantidad(control.getListaCursos().size());
        	        	
        	//JHPR 2008-7-1 EVAUAR ESTADO REG X CIERRE DE NOTAS.
        	evaluarEstadoRegNotas(control);
        	
        	request.getSession().setAttribute("listaCursos", control.getListaCursos());
        	request.getSession().setAttribute("listaSeccion", control.getListaSeccion());        	
        	
    	}
    	if (control.getOperacion().trim().equals("DETALLE1"))
    	{
    	  	
    	  
    		Parciales dobj = new Parciales();
    		//CargaDetalle(control);
    		
       		control.setMinimo(control.getMin());
       		inicializaSeccion(control);
       		dobj.setCodPeriodo(control.getCodPeriodo());
      		dobj.setCodPro(control.getCodProducto());
        	dobj.setCodEspe(control.getCodEspecialidad());
        	dobj.setCodCurso(control.getCodCurso());
        	dobj.setSeccion(control.getSeccion());
        	dobj.setCodEvaluador(control.getEvaluador());
        	dobj.setCodTipoEvaluacion(control.getCodigo());
        	//numero2(control);
        	numero(control);

        	control.setListEvaluaciones(this.mantenimientoManager.getTipoPracticaxCurso(dobj.getCodPeriodo(), dobj.getCodCurso()));
        	
        	control.setListaCursos(this.operDocenteManager.GetAllEvaluaciones(dobj));
        	control.setCantidad(control.getListaCursos().size());
        	
        	//JHPR 2008-7-1 EVAUAR ESTADO REG X CIERRE DE NOTAS.
        	evaluarEstadoRegNotas(control);
        	
        	request.getSession().setAttribute("listaCursos", control.getListaCursos());        	        	
        	
    	}
    	else if(control.getOperacion().trim().equals("ELIMINAR")){
    			resultado = Eliminar(control);
    			log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) {
    			control.setMessage("ERROR");
    			}
    		else {
    			//JHPR: 2008-04-22 Manejando excepciones: Evaluaciones que no se pudieron eliminar por que ya tienen notas ingresadas.
    			if (resultado.equals("0")) {
    				control.setMessage("OK");
    			} else {
    				//Hay evaluaciones que no se han podido eliminar
    				String noEliminados = "";    				
    				control.setMessage2(resultado.substring(1));
    				control.setMessage("NO");	
    			}    			
    			}
    		
    		Parciales dobj = new Parciales();
    		
       	inicializaSeccion(control);
       		dobj.setCodPeriodo(control.getCodPeriodo());
      		dobj.setCodPro(control.getCodProducto());
        	dobj.setCodEspe(control.getCodEspecialidad());
        	dobj.setCodCurso(control.getCodCurso());
        	dobj.setSeccion(control.getSeccion());
        	dobj.setCodEvaluador(control.getEvaluador());
        	dobj.setCodTipoEvaluacion(control.getCodigo());
        	//numero2(control);
        	numero(control);
        	control.setMinimo(control.getMin());
        //	CargaDetalle(control);
        	control.setListEvaluaciones(this.mantenimientoManager.getTipoPracticaxCurso(dobj.getCodPeriodo(), dobj.getCodCurso()));
        	//request.getSession().setAttribute("listEvaluaciones", control.getListEvaluaciones());
        	
        	control.setListaCursos(this.operDocenteManager.GetAllEvaluaciones(dobj));
        	request.getSession().setAttribute("listaCursos", control.getListaCursos());
        	control.setCantidad(control.getListaCursos().size());	
        	request.getSession().setAttribute("listaSeccion", control.getListaSeccion());
        	
		}
    	control.setCant(0);
    	control.setNumero("");
    	control.setPeso("");
    
		//log.info("onSubmit:FIN");
	    return new ModelAndView("/evaluaciones/definirNumero/eva_detalle_practicas","control",control);		
    }
        
        private List CargaDetalle(DetallePracticasCommand control)
        {
        	Parciales obj = new Parciales();
        	obj.setCodCurso(control.getCodCurso());
        	obj.setCodEvaluador(control.getCodEvaluador());
        	obj.setCodTipoEvaluacion(control.getCodigo());
        	control.setListaCursos(this.operDocenteManager.GetAllEvaluaciones(obj));
        	
        	return control.getListaCursos();
        }
        private String Eliminar(DetallePracticasCommand control)
        {
        	Parciales obj = new Parciales();
        	String cant = ""+control.getCant();
        	obj.setCodPeriodo(control.getCodPeriodo());
        	obj.setCodPro(control.getCodProducto());
        	obj.setCodEspe(control.getCodEspecialidad());
        	obj.setCodCurso(control.getCodCurso());
        	obj.setSeccion(control.getSeccion());
        	obj.setCodTipoEvaluacion(control.getCodEvaluacion());
        	obj.setCodEvaluador(control.getNumero());
        	
        	//System.out.println("cod evaluador:"+control.getNumero());
        	//System.out.println("cadena de codigos:"+control.getCodId());
        	
        	//StringTokenizer stkFlag = new StringTokenizer(control.getCodId(),"|");
    		//String Flag;
        	obj.setCodCursoId(this.operDocenteManager.DeleteEvaluaciones(obj,cant, control.getCodEvaluador()));
        	
    		/*while ( stkFlag.hasMoreTokens() )
    		{
    			Flag = stkFlag.nextToken();
    			System.out.println("while flag:" + Flag);
    			obj.setCodId(Flag);
    			obj.setCodCursoId(this.operDocenteManager.DeleteEvaluaciones(obj,cant, control.getCodEvaluador()));
    		}*/
    		
        	return obj.getCodCursoId();
        }
        private String  numero(DetallePracticasCommand command)
        {
        	command.setMinimo("");
        	Curso nobj = new Curso();
        	if(!command.getCodProducto().equals("7")){
        	nobj.setCodProducto(command.getCodProducto());
        	nobj.setCodPeriodo(command.getCodPeriodo());
        		NumeroMinimo obj = this.operDocenteManager.GetAllNumeroMinimo(nobj);	
        		if(obj==null){
        			command.setMinimo("0");
        		}
        		else{
        		command.setMinimo(obj.getValor());
        	        }
        	}
        	else{
        		command.setMinimo("0");
        	}
        	return command.getMinimo();
        }
        /*private String  numero2(DetallePracticasCommand control)
        {
        	control.setMinimo("");
        	Curso nobj = new Curso();
        	if(!control.getCodProducto().equals("7")){
        	nobj.setCodProducto(control.getCodProducto());
        	nobj.setCodPeriodo(control.getCodPeriodo());
        		NumeroMinimo obj = this.operDocenteManager.GetAllNumeroMinimo(nobj);	
        		if(obj==null){
        			control.setMinimo("0");
        		}
        		else{
        		control.setMinimo(obj.getValor());
        	        }
        	}
        	else{
        		control.setMinimo("");
        	}
        	return control.getMinimo();
        }*/
        
        private void inicializaSeccion(DetallePracticasCommand command){
        	CursoEvaluador lObject;        
        	command.setListaSeccion(this.operDocenteManager.getAllSeccionCurso(command.getCodigo(), command.getCodCurso()));        	
        	ArrayList firstSeccion = (ArrayList)command.getListaSeccion();//(ArrayList)this.operDocenteManager.getAllSeccionCurso(command.getCodigo(), command.getCodCurso());
        	if(firstSeccion!=null){
        		if (firstSeccion.size() > 0){
            		lObject = (CursoEvaluador) firstSeccion.get(0);
            		command.setPrimeraSeccion(lObject.getCodSeccion());            		
            	}
        	}
        	else{
        		command.setPrimeraSeccion("0");        	
        	}
        }
        
        private void evaluarEstadoRegNotas(DetallePracticasCommand command){
        	//System.out.println("command.getCodCurso():"+command.getCodCurso());
        	String seccion = "";
        	if (command.getSeccion()==null || command.getSeccion().equals(""))
        		seccion = command.getPrimeraSeccion();
        	else
        		seccion = command.getSeccion();

        	/*System.out.println("command.getCodCurso():"+command.getCodCurso());
        	System.out.println("seccion:"+seccion);
        	System.out.println("tipoEvaluacion:"+command.getCodigo());*/       	        
        	
        	//command.getCodigo() = Tipo de Evaluación
        	List<Parciales> lista = operDocenteManager.obtenerDefinicionesParciales(command.getCodCurso(), seccion, command.getCodigo(), "", "A");
        	int contador=0;
    		for (Parciales evaluacion : lista) {
    			if (evaluacion.getFlagCerrarNota()!=null){
    				if (evaluacion.getFlagCerrarNota().equals("1")) 
    					contador ++;
    			}
    		}
    		if (lista.size()==contador)
    			command.setFlagCerrarNota("1");
    		else
    			command.setFlagCerrarNota("0");    		        	
        }        
	}
