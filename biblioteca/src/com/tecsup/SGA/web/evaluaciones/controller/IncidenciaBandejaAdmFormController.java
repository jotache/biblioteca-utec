package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.IncidenciaBandejaDocCommand;

public class IncidenciaBandejaAdmFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(IncidenciaRegistrarDocFormController.class);
	OperDocenteManager operDocenteManager;
	
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public IncidenciaBandejaAdmFormController(){
		super();
		setCommandClass(IncidenciaBandejaDocCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	IncidenciaBandejaDocCommand command = new IncidenciaBandejaDocCommand();  	
    	   	
    	/*Recogiendo valores*/
    	//command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
    	
    	//Llenando información
    	if ( command.getCodCurso() != null )
    	{
	    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEvaluador(), command.getCodCurso());
	    	inicializaSeccion(command, command.getCodCurso());
	    	//Seteamos el primer valor del combo.
	    	ArrayList<CursoEvaluador> listSeccion = (ArrayList<CursoEvaluador>)command.getListaSeccion();
	    	if ( listSeccion != null )
		    	if ( listSeccion.size() > 0 )
		    		command.setCodSeccion(listSeccion.get(0).getCodSeccion());
	    	//LLenamos lista de alumnos.
	    	buscarAlumnos(command);
    	}
    	//Llenado Tabla Interes - Session
    	request.getSession().setAttribute("listaBandejaIncidenciasDoc", command.getListaBandeja());
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	IncidenciaBandejaDocCommand control = (IncidenciaBandejaDocCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	inicializaSeccion(control, control.getCodCurso());
    	
    	if ( control.getOperacion().trim().equals("BUSCAR"))
    	{
    		buscarAlumnos(control);
    	}
    	control.setCodAlumno("");
    	request.getSession().setAttribute("listaBandejaIncidenciasDoc", control.getListaBandeja());
		
	    return new ModelAndView("/evaluaciones/gestionAdministrativo/eva_incidenciabandeja_adm","control",control);		
    }
    
	private void inicializaDataCurso(IncidenciaBandejaDocCommand command, String strPeriodo, String strEval, String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval
				, "", "", strCodCurso, "","");
		
    	if (dataCurso.size()>0){
    		lObject = (CursoEvaluador) dataCurso.get(0);
    		command.setPeriodoVigente(lObject.getPeriodo());
    		command.setCiclo(lObject.getCiclo());
    		command.setProducto(lObject.getProducto());
    		command.setNomEvaluador(lObject.getEvaluador());
    		command.setCurso(lObject.getCurso());
    		command.setEscialidad(lObject.getEspecialidad());
    		command.setSistemaEval(lObject.getSistEval());
    	}
	}
	
	private void buscarAlumnos(IncidenciaBandejaDocCommand control)
	{
		control.setListaBandeja(
				this.operDocenteManager.getAllIncidenciasByCurso(
				control.getCodPeriodo(), control.getCodCurso(), "",""
				, control.getCodTipoSesionDefecto(), control.getCodSeccion()
				, CommonConstants.INC_TIPO_OPE_ADM, control.getNombreAlumno()
				, control.getApellidoAlumno()));
	}
	
    private void inicializaSeccion(IncidenciaBandejaDocCommand command, String strCodCurso){
    	ArrayList arrTipoSesiones = (ArrayList)this.operDocenteManager.getAllSesionXCurso(strCodCurso); 
    	CursoEvaluador cursoEvaluador;
    	boolean flag = true;
    	if ( arrTipoSesiones != null )
    	{
    		//BUSCO TIPO SESION TEORIA
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TEO))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO LABORATORIO 
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_LAB))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    		//SI NO HAY TIPO SESION TEORIA BUSCO LABORATORIO
    		for (int i = 0; i < arrTipoSesiones.size(); i++)
    		{
    			cursoEvaluador = (CursoEvaluador)arrTipoSesiones.get(i);
    			if ( cursoEvaluador.getCodTipoSesion().equals(CommonConstants.TIPO_SESION_TAL))
    			{
    				command.setCodTipoSesionDefecto(cursoEvaluador.getCodTipoSesion());
    				command.setListaSeccion(
    						this.operDocenteManager.getAllSeccionCurso(command.getCodTipoSesionDefecto()
    						, strCodCurso));
    				return ;
    			}
    		}
    	}
    	//command.setListaSeccion(this.operDocenteManager.getAllSeccionCurso("", strCodCurso));
    }
}
