package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.StringTokenizer; 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.service.evaluaciones.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.MtoParametrosGeneralesCommand;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.*;


public class MtoParametrosGeneralesFormController extends SimpleFormController{
	
	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	private TablaDetalleManager tablaDetalleManager; 
	
	private static Log log = LogFactory.getLog(MtoParametrosGeneralesFormController.class);
	String resultado = "";
	Periodo periodo= new Periodo();
	
	int madfrog=0;
	String perActual="";
	ArrayList<String> listaGuardar= new ArrayList<String>();
	
	protected Object formBackingObject(HttpServletRequest request)
    	throws ServletException {
    	   	    	
    	MtoParametrosGeneralesCommand command = new MtoParametrosGeneralesCommand();
    	request.setAttribute("Inicio", "SI");
    	List listaPeriodos=this.mtoParametrosGeneralesManager.getPeriodos();
    	if(listaPeriodos!=null)
    	{	command.setCodListaPeriodo(listaPeriodos);
    		if(command.getCodListaPeriodo().size()>0)
    			BuscarPeriodoActual(command);
    	}
    	else command.setCodListaPeriodo(new ArrayList());
    	
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	  	    	
    	command.setCodListaProducto(this.mtoParametrosGeneralesManager.getProductos());
    	command.setConstePFR(CommonConstants.TIPT_PRODUCTO_PFR);
    	command.setConsteProductoHibrido(CommonConstants.CONSTE_PRODUCTO_HIBRIDO);
    	command.setConsteProductoVirtual(CommonConstants.CONSTE_PRODUCTO_TECSUP_VIRTUAL);
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    	throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	request.setAttribute("Inicio", "NO");
    	MtoParametrosGeneralesCommand control = (MtoParametrosGeneralesCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if(control.getCodProducto2().equals(CommonConstants.TIPT_PRODUCTO_PFR))
    		 control.setTxtPFR("PFR");
    	else  control.setTxtPFR("");
    		
    	if(control.getOperacion().trim().equals("REGISTRAR") )
    	{ 
    		resultado=Insert_O_UpdateParametrosGenerales(control, request, response);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("ERROR")) request.setAttribute("Msg", "ERROR");
    		else request.setAttribute("Msg", "OK");
    		request.setAttribute("Operacion", "REGISTRAR");
    		
    	}	
    	else{//*****Si selecciono alguna opcion del Combo**********//
	    	if(control.getOperacion().trim().equals("DETALLE"))
	    	{   listaGuardar= new ArrayList<String>();
	    		if(!control.getCodProducto().equals("-1")) 
	    	   {
	    	    
	    	    String ali=control.getCodProducto();
	    	    BuscarPeriodoActualDetalle(control);
	    	    request.setAttribute("estadoPeriodo", control.getCodPeriodoAperturados());//control.getEstadoPeriodo());
	    	    control.setCodListaPeriodo(this.mtoParametrosGeneralesManager.getPeriodos());
	    	    
	    //***********Comparo si el producto es Hibrido**************//	    
	    	    if(control.getCodProducto2().trim().equals(CommonConstants.TIPT_PRODUCTO_HIBRIDO))//productoHibrido.getCodProducto().trim()))
			       {      TipoTablaDetalle tipoTablaDetalle= new TipoTablaDetalle();
			    	      ParametrosGenerales parametrosGenerales= new ParametrosGenerales();
			    	      List DK=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PARAMETROS_GRALES
			    	  				, "", "",CommonConstants.CONSTE_PRODUCTO_HIBRIDO, "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
			    	   			    	      
			    	      control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
			    	    		  control.getCodProducto2().trim(),control.getCodPeriodo2().trim()));
			    	     
			    	     
			    	      request.setAttribute("MsgProducto", control.getCodProducto2().trim());
			    	      request.setAttribute("MsgPeriodo", control.getCodPeriodo2().trim());
			    	   
			    	    if(control.getListaCalificaciones().size()==0)
			    	    {   request.setAttribute("Msg1OK", "MAD");
			    	        request.setAttribute("Operacion", "DETALLE");
			    	        request.setAttribute("Longitud", String.valueOf(DK.size()));
			    	        
			    	        request.setAttribute("Tipo", "NUEVO");
			    	        control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
			    	                CommonConstants.TIPT_PRODUCTO_PFR/*producto.getCodProducto()*/,periodo.getCodigo()));
			    	        madfrog=DK.size();
			    	        
			    	    	request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
			    	    	request.getSession().setAttribute("listaCalificaciones2", DK);
			    	    	PrepararGuardar(control, DK, 1);
			    	    	
			    	    }	
			    	    else  
			    	    {   request.setAttribute("Msg1OK", "OK");  
			    	        request.setAttribute("Operacion", "DETALLE");
			    	        request.setAttribute("Longitud", String.valueOf(control.getListaCalificaciones().size()));
			    	        
			    	        request.setAttribute("Tipo", "EXISTE");
			    	        
			    	    	request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
			    	    	madfrog=control.getListaCalificaciones().size();
			    	    	request.getSession().setAttribute("listaCalificaciones2", DK);
			    	    	PrepararGuardar(control, DK, 2);
			    	       }	
			    	    }
	    	    /**********************Si el Producto es Tecsup Virtual**************/
	    	    else{ if(control.getCodProducto2().trim().equals(CommonConstants.TIPT_PRODUCTO_TECSUP_VIRTUAL))
	    	    	  {
		    	    	TipoTablaDetalle tipoTablaDetalle= new TipoTablaDetalle();  
			    	      ParametrosGenerales parametrosGenerales= new ParametrosGenerales();
			    	       List DK=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PARAMETROS_GRALES
		    	  				, "", "","", CommonConstants.CONSTE_PRODUCTO_TECSUP_VIRTUAL, "", "", "", CommonConstants.TIPO_ORDEN_COD);
			    	     control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
				    	    		  control.getCodProducto2().trim(),control.getCodPeriodo2().trim()));
			    	     
		    	    	 request.setAttribute("MsgProducto", control.getCodProducto2().trim());
			    	     request.setAttribute("MsgPeriodo", control.getCodPeriodo2().trim());
		    	    	
		    	    	 if(control.getListaCalificaciones().size()==0)
		 	    	     {  request.setAttribute("Msg1OK", "MAD");
		 	    	        request.setAttribute("Operacion", "DETALLE");
		 	    	       request.setAttribute("Tipo", "NUEVO");
		 	    	        request.setAttribute("Longitud", String.valueOf(DK.size()));
		 	    	       
		 	    	       madfrog=DK.size();
		 	    	       
		 	    	       control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
		 	    	        		CommonConstants.TIPT_PRODUCTO_PFR/*producto.getCodProducto()*/,periodo.getCodigo()));
		 	    	       		 	    	      
			    	       request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
			    	       request.getSession().setAttribute("listaCalificaciones2", DK);
			    	       PrepararGuardar(control, DK, 1);
			    	      }
		    	    	 else 
		    	    	 {  request.setAttribute("Msg1OK", "OK");
		    	    	    request.setAttribute("Operacion", "DETALLE");
		    	    	    request.setAttribute("Tipo", "EXISTE");
		    	    	    request.setAttribute("Longitud",String.valueOf(control.getListaCalificaciones().size()));
		    	    	    
		    	    	    madfrog=control.getListaCalificaciones().size();
		    	    	    
		    	    	    request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
		    	    	    request.getSession().setAttribute("listaCalificaciones2", DK);
		    	    	    PrepararGuardar(control, DK, 2);
		    	    	   }
	    	    	
	    	    	  }
	    	        /**************************El Producto no es Hibrido ni Tecsup Virtual**********************/
	    	    	 else
	    	    	 { TipoTablaDetalle tipoTablaDetalle= new TipoTablaDetalle();  
			    	      ParametrosGenerales parametrosGenerales= new ParametrosGenerales();
			    	       List DK=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PARAMETROS_GRALES
		    	  				, "", "","", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
			    	     control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
				    	    		  control.getCodProducto2().trim(),control.getCodPeriodo2().trim()));
			    	     
		    	    	 request.setAttribute("MsgProducto", control.getCodProducto2().trim());
			    	     request.setAttribute("MsgPeriodo", control.getCodPeriodo2().trim());
		    	    	
		    	    	 if(control.getListaCalificaciones().size()==0)
		 	    	     {  request.setAttribute("Msg1OK", "MAD");
		 	    	        request.setAttribute("Operacion", "DETALLE");
		 	    	       request.setAttribute("Tipo", "NUEVO");
		 	    	        request.setAttribute("Longitud", String.valueOf(DK.size()));
		 	    	       
		 	    	       madfrog=DK.size();
		 	    	        control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
		 	    	        		CommonConstants.TIPT_PRODUCTO_PFR/*producto.getCodProducto()*/,periodo.getCodigo()));
		 	    	       		 	    	      
			    	       request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
			    	       request.getSession().setAttribute("listaCalificaciones2", DK);
			    	       PrepararGuardar(control, DK, 1);
			    	      }
		    	    	 else 
		    	    	 {  request.setAttribute("Msg1OK", "OK");
		    	    	    request.setAttribute("Operacion", "DETALLE");
		    	    	    request.setAttribute("Tipo", "EXISTE");
		    	    	    request.setAttribute("Longitud",String.valueOf(control.getListaCalificaciones().size()));
		    	    	    
		    	    	    madfrog=control.getListaCalificaciones().size();
		    	    	    
		    	    	    request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
		    	    	    request.getSession().setAttribute("listaCalificaciones2", DK);
		    	    	    PrepararGuardar(control, DK, 2);
		    	    	   }
	    	           }
	    	       }
	    	  } 
	    	}
    	}
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_config_parametros_grales","control",control);		
    }

    public String Insert_O_UpdateParametrosGenerales(MtoParametrosGeneralesCommand control, 
    		HttpServletRequest request, HttpServletResponse response){
    	
    	ParametrosGenerales obj= new ParametrosGenerales();
    	obj.setCodProducto(control.getCodProducto2().trim());
	    obj.setCodPeriodo(control.getCodPeriodo2().trim());
	    obj.setCodTabla(control.getCodTablaDetalle().trim());
	    
	    int rpta=0;
	    int suma=0;
	    
	    StringTokenizer aa= new StringTokenizer(control.getValor0(), "|");
	    String lia="";
	    ArrayList<String> molde= new ArrayList<String>();
	    while(aa.hasMoreTokens())
	    {	lia = aa.nextToken();
	        molde.add(lia);
	    }  
	   
	  
	    for(int p=0;p<listaGuardar.size();p++)
	    {  if(molde.get(p).equals("&&"))
	    	    obj.setValor("");
	       else obj.setValor(molde.get(p));
	       
	       obj.setCodTabla(listaGuardar.get(p));
	      // obj.setValor(molde.get(p));
	       resultado=this.mtoParametrosGeneralesManager.Insert_O_UpdateParametrosGenerales(obj, control.getCodEvaluador());
		   if(resultado.equals("-1")) rpta=0;  	    		  	    	  
	  	   else rpta=1;
	  	   suma=suma+rpta;
	    } 	
	  
	    if((suma==5)||(suma==9)||(suma==7))
	    { if(suma==5)
	    	{ 
	    	
	    	 control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
   	    		  control.getCodProducto2().trim(),control.getCodPeriodo2().trim()));
	    	 request.setAttribute("Tipo", "EXISTE");
	    	 request.setAttribute("Longitud", "5");
	    	 request.setAttribute("WilDATilA", "MAD");
	    	 request.setAttribute("estadoPeriodo", control.getCodPeriodoAperturados());//control.getEstadoPeriodo());
	    	 request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
	    	}
	      else{ if(suma==7)
	      		{ 
	      			
		    	  control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
		          control.getCodProducto2().trim(),control.getCodPeriodo2().trim()));
		    	  request.setAttribute("Tipo", "EXISTE");
		    	  request.setAttribute("Longitud", "7");
		    	  request.setAttribute("WilDATilA", "MAD");
		    	  request.setAttribute("estadoPeriodo", control.getCodPeriodoAperturados());//control.getEstadoPeriodo());
		     	  request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
	    	  
	      		}
	      		else
	      		{  
		           
		    	  control.setListaCalificaciones(this.mtoParametrosGeneralesManager.getAllParametrosGenerales(
		    	  control.getCodProducto2().trim(),control.getCodPeriodo2().trim()));  
		    	  request.setAttribute("Tipo", "EXISTE");
		    	  request.setAttribute("Longitud", "9");
		    	  request.setAttribute("WilDATilA", "MAD");
		    	  request.setAttribute("estadoPeriodo", control.getCodPeriodoAperturados());//control.getEstadoPeriodo());
		          request.getSession().setAttribute("listaCalificaciones", control.getListaCalificaciones());
	         }
	      }
	    	return "OK"; 
	    }
	    
	    else return "ERROR";
	    	
    }
    
	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public void BuscarPeriodoActual(MtoParametrosGeneralesCommand control){
    	Periodo periodoActual=new Periodo();
    	String codPeriodosAperturados="";
    	for(int p=0;p<control.getCodListaPeriodo().size();p++)
    	{  periodoActual=(Periodo)control.getCodListaPeriodo().get(p);
    	  
    	   if(periodoActual.getTipo().equals("A"))//if(periodoActual.getCodigo().equals(command.getCodPeriodo())
    	   {   
    	       perActual=periodoActual.getCodigo();
    	       control.setEstadoPeriodo("A");
    	       if(codPeriodosAperturados.equals(""))
    	       {  codPeriodosAperturados=periodoActual.getCodigo()+"|";
    	          control.setCodigo(periodoActual.getCodigo());
    	       }
    	       else
    	    	   codPeriodosAperturados=codPeriodosAperturados+periodoActual.getCodigo()+"|";
    	       
    	   }
    	   else control.setEstadoPeriodo("P");
    	}
    	
    	control.setCodPeriodoAperturados(codPeriodosAperturados);
    }
    
    public void BuscarPeriodoActualDetalle(MtoParametrosGeneralesCommand control){ 
    	
    	if(perActual.equals(control.getCodigo()))
    		{control.setEstadoPeriodo("A");
    		 
    		}
    	else{	
    		control.setEstadoPeriodo("P");
    		
    	    }
    }
    
    public void PrepararGuardar(MtoParametrosGeneralesCommand control, List lista, int bandera)
    {   TipoTablaDetalle tipoTablaDetalle= new TipoTablaDetalle();
        ParametrosGenerales parametrosGenerales= new ParametrosGenerales();
        if(bandera==1)
        {	for(int full=0; full<lista.size();full++)	
    		{  tipoTablaDetalle=(TipoTablaDetalle)lista.get(full);
    		   listaGuardar.add(tipoTablaDetalle.getCodTipoTablaDetalle());
    		}
        }
        else{ if(bandera==2)
        	  {  for(int wild=0;wild<control.getListaCalificaciones().size();wild++)
  	    		 { parametrosGenerales=(ParametrosGenerales)control.getListaCalificaciones().get(wild);
  	    		   listaGuardar.add(parametrosGenerales.getCodTabla());
  	    		 }
        	
        	  }
        }
    }
}
