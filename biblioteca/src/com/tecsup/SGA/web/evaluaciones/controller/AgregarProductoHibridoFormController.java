package com.tecsup.SGA.web.evaluaciones.controller;
import java.text.*;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.service.evaluaciones.TipoExamenManager;
import com.tecsup.SGA.service.evaluaciones.TipoPracticaManager;
import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;

public class AgregarProductoHibridoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarProductoHibridoFormController.class);	
	private TipoExamenManager tipoExamenManager;	
	private TipoPracticaManager tipoPracticaManager;
	private SistevalHibridoManager sistevalHibridoManager; 
	
	public void setTipoExamenManager(TipoExamenManager tipoExamenManager) {
		this.tipoExamenManager = tipoExamenManager;
	}

	public void setTipoPracticaManager(TipoPracticaManager tipoPracticaManager) {
		this.tipoPracticaManager = tipoPracticaManager;
	}
	
	public void setSistevalHibridoManager(
			SistevalHibridoManager sistevalHibridoManager) {
		this.sistevalHibridoManager = sistevalHibridoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	AgregarProductoHibridoCommand command = new AgregarProductoHibridoCommand();    	
		//obteniendo los valores de la sesion
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	
    	
    	String codDetalle = request.getParameter("txhCodDetalle");    	
    	if ( codDetalle != null ){
    		if ( !codDetalle.trim().equals("") ){
    			cargaDetalle(command, codDetalle);
    			command.setOperacion("MODIFICAR");
    		}
    	}
    	else{
    		command.setOperacion("AGREGAR");
    	}    	
    	command.setListaExamenes(this.tipoExamenManager.getAllTipoExamenes());
    	command.setListaPracticas(this.tipoPracticaManager.getAllTipoPracticas());
    	request.getSession().setAttribute("listaBandeja",command.getListaBandeja());
    	
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }

	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	AgregarProductoHibridoCommand control = (AgregarProductoHibridoCommand) command;    	
    	String resultado="";
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("AGREGAR")){			
    		resultado = InsertProductoHibrido(control);    
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}
    	else{
    		if (control.getOperacion().trim().equals("MODIFICAR")){    			
        		resultado = UpdateProductoHibrido(control); 
        		log.info("RPTA:" + resultado);
        		if ( resultado.equals("-1")){
        			control.setMsg("ERROR");
        		}
        		else{
        			control.setMsg("OK");        	
        		}
        	}    	
    	}
    		
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_agregar_producto_hibrido","control",control);		
    }
      
    private String InsertProductoHibrido(AgregarProductoHibridoCommand control)
    {  	try
    	{	SistevalHibrido obj = new SistevalHibrido();	 
    		obj.setCodSec(control.getCodigo());
    		obj.setDescripcion(control.getDenominacion());
    		obj.setCadEvaluacion(control.getCadEvaluacion());
    		obj.setNroProductos(control.getNroRegistros());    		
    		obj.setCodGenerado(this.sistevalHibridoManager.InsertProductoHibrido(obj, control.getCodEvaluador()));
    		return obj.getCodGenerado();					
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private String UpdateProductoHibrido(AgregarProductoHibridoCommand control)
    {  	try
    	{	SistevalHibrido obj = new SistevalHibrido();	 
    		obj.setCodSec(control.getSeleccion());    		
    		obj.setDescripcion(control.getDenominacion());
    		obj.setCadEvaluacion(control.getCadEvaluacion());
    		obj.setNroProductos(control.getNroRegistros());    		
    		obj.setCodGenerado(this.sistevalHibridoManager.UpdateProductoHibrido(obj, control.getCodEvaluador()));
    		return obj.getCodGenerado();					
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(AgregarProductoHibridoCommand command, String codDetalle)
    {	
    	SistevalHibrido obj=new SistevalHibrido();    	
    	command.setListaBandeja(this.sistevalHibridoManager.getSistevalHibridoById(codDetalle));
    	obj = (SistevalHibrido)this.sistevalHibridoManager.getSistevalHibridoById(codDetalle).get(0);
    	command.setSeleccion(codDetalle);    	
    	command.setCodigo(obj.getCodSec());
    	command.setDenominacion(obj.getDescripcion());
    	command.setEstadoCodigo("DESABILITADO");
    }      
}
