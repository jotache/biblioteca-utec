package com.tecsup.SGA.web.evaluaciones.controller;
import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class AgregarCompetenciaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarCompetenciaFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	AgregarCompetenciaCommand command = new AgregarCompetenciaCommand();
		
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	
    	command.setOperacion("GRABAR");
    	//OBTIENE EL VALOR PARA MOSTRAR EN LA NUEVA PANTALLA CARGADETALLE()
    	String codDetalle = (String)request.getParameter("txhCodDetalle");
    	command.setCodDetalle("");    	
    	if ( codDetalle != null ){
    		if ( !codDetalle.trim().equals("") ){
    			cargaDetalle(command, codDetalle);
    			command.setOperacion("MODIFICAR");
    		}
    	}    	
    	
    	String valorTope = (String)request.getParameter("val");
    	if(valorTope!=null){
    		if(!valorTope.trim().equals("")){
    			command.setValorTope(valorTope);    	 
    		}
    	}    	    	
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	AgregarCompetenciaCommand control = (AgregarCompetenciaCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		resultado = InsertTablaDetalle(control);    		
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
    	}
    	else{
    		if (control.getOperacion().trim().equals("MODIFICAR"))
        	{   		
        		resultado = UpdateTablaDetalle(control);        		
        		if ( resultado.equals("-1")){
        			control.setMsg("ERROR");
        		}
        		else{
        			control.setMsg("OK");
        		}
        	} 		
    	}    	
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_agregar_competencia","control",control);		
    }
    
    private String InsertTablaDetalle(AgregarCompetenciaCommand control)
    {
    	try
    	{	TipoTablaDetalle obj = new TipoTablaDetalle();	 
    		obj.setCodTipoTabla(CommonConstants.TIPT_COMPETENCIAS);
			obj.setDescripcion(control.getDenominacion());
			obj.setDscValor1(control.getPorcentaje());						
			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.InsertTablaDetalle(obj, control.getCodEvaluador()));						
			return obj.getCodTipoTablaDetalle();  		
    		
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private String UpdateTablaDetalle(AgregarCompetenciaCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
 
    		obj.setCodTipoTabla(CommonConstants.TIPT_COMPETENCIAS);
    		obj.setDescripcion(control.getDenominacion());
    		obj.setDscValor1(control.getPorcentaje());    		
    		obj.setCodDetalle(control.getCodDetalle());    		
    		if ( !control.getCodDetalle().trim().equals("") ){    			
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.UpdateTablaDetalle(obj, control.getCodEvaluador()));    			
    		}    			
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(AgregarCompetenciaCommand command, String codDetalle)
    {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
		obj = (TipoTablaDetalle)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_COMPETENCIAS
				, codDetalle, "", "","", "", "", "", CommonConstants.TIPO_ORDEN_DSC).get(0);
		
		command.setDenominacion(obj.getDescripcion());		
		command.setCodDetalle(codDetalle);
		command.setPorcentaje(obj.getDscValor1());
    }
}
