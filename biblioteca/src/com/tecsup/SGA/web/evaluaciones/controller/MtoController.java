package com.tecsup.SGA.web.evaluaciones.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.ModelMap;

/**
 * @author cosapi
 *
 */
public class MtoController implements Controller {

	private static Log log = LogFactory.getLog(MtoController.class);

	public MtoController(){	
		
	}


    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	
    	
    	ModelMap model = new ModelMap();
    	//Recogiendo valores - Codigos*/
    	String codPeriodo=(String)request.getParameter("txhCodPeriodo");
    	String codEvaluador=(String)request.getParameter("txhCodEvaluador");
    	
    	request.setAttribute("codPeriodo", codPeriodo);
    	request.setAttribute("codEvaluador", codEvaluador);
    	
    	String pagina = "/evaluaciones/mantenimiento/eva_mto_bandeja";
    	
    	//REQUEST
    	String operacion = request.getParameter("operacion");
    	String opcion = request.getParameter("opcion");
   	
    	if(opcion != null && "menu".equalsIgnoreCase(opcion)){
    		//model.addObject("clientes", mgr.getClientesCiaSeguro());	
    		pagina = "/evaluaciones/mantenimiento/eva_mto_menu";
    	}else if("cuerpo".equalsIgnoreCase(opcion)){
    		pagina = "/evaluaciones/mantenimiento/eva_mto_cuerpo";
    	}
    	
		return new ModelAndView(pagina, "model", model);
	}
}
