package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.modelo.*;

public class MtoModificarTipoConceptoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(MtoModificarTipoConceptoFormController.class);
	private TablaDetalleManager tablaDetalleManager;

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	   	
    	MtoModificarTipoConceptoCommand command = new MtoModificarTipoConceptoCommand();
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	
    	String codDetalle = (String)request.getParameter("txhCodAreasNivelDos");
    	String dscDetalle = (String)request.getParameter("txhDscNivelDos");
    	String valor1 =(String)request.getParameter("txhCodAreasNivelUno");
    	String descripcion =(String)request.getParameter("txhDscNivelUno");
    	command.setCodDetalle("");
    	command.setValor1(valor1);
    	
    	if ( codDetalle != null ) if ( !codDetalle.trim().equals("") ) cargaDetalle(command, codDetalle, descripcion);
    	        
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    	throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	MtoModificarTipoConceptoCommand control = (MtoModificarTipoConceptoCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("MODIFICAR"))
    	{
    		
    		resultado = UpdateTablaDetalle(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else control.setMsg("OK");
    	}
    	control.setOperacion("");
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_concepto_concepto_modificacion","control",control);		
    }
    
    private String UpdateTablaDetalle(MtoModificarTipoConceptoCommand control)
    {
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    		
    		obj.setCodTipoTabla(CommonConstants.TIPT_CONCEPTOS);
    		obj.setDescripcion(control.getDescripcion());
    		obj.setUsuario(control.getCodEvaluador());
    		obj.setCodDetalle(control.getCodDetalle());
    		obj.setDscValor1(control.getValor1());
 
    		if ( !control.getCodDetalle().trim().equals("") )
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.UpdateTablaDetalle(obj, control.getCodEvaluador()));
    		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(MtoModificarTipoConceptoCommand command, String codDetalle, String descripcion)
    {
    	TipoTablaDetalle obj = new TipoTablaDetalle();
		
		obj = (TipoTablaDetalle)this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_CONCEPTOS
				, codDetalle, "", "","", "", "", "", CommonConstants.TIPO_ORDEN_DSC).get(0);
		command.setDescripcionNivel1(descripcion);
		command.setDescripcion(obj.getDescripcion());		
		command.setCodDetalle(codDetalle);
    }
    
}
