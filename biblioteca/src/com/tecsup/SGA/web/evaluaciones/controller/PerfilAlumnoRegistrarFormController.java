package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.evaluaciones.PerfilAlumnoManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.web.evaluaciones.command.PerfilAlumnoRegistroCommand;
import com.tecsup.SGA.modelo.ParametrosEspeciales;

public class PerfilAlumnoRegistrarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(PerfilAlumnoRegistrarFormController.class);
	
	private PerfilAlumnoManager perfilAlumnoManager;
	private OperDocenteManager operDocenteManager;
	private MantenimientoManager mantenimientoManager;
	
	public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}
	
	public void setPerfilAlumnoManager(PerfilAlumnoManager perfilAlumnoManager) {
		this.perfilAlumnoManager = perfilAlumnoManager;
	}	
	
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	PerfilAlumnoRegistroCommand command = new PerfilAlumnoRegistroCommand();  	
    	command.setCodAlumno(request.getParameter("txhCodAlumno"));
    	command.setCodEvaluador(request.getParameter("txhCodEvaluador"));
    	command.setCodPeriodo(request.getParameter("txhCodPeriodo"));
    	command.setDscAlumno(request.getParameter("txhNomAlumno"));
    	command.setIndTipo(request.getParameter("txhTipoOpe"));
    	
    	if ( command.getCodAlumno() != null )
    	{
    		cargaPerfiles(command);
    	}
    	request.getSession().setAttribute("listPerfilesByAlumno", command.getListaBandeja());
    	
        return command;
    } 
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String respuesta = "";
    	PerfilAlumnoRegistroCommand control = (PerfilAlumnoRegistroCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if ( control.getOperacion().trim().equals("GRABAR"))
    	{
    		respuesta = gragarCompetencias(control);
    		log.info("RPTA:" + respuesta);
    		if ( respuesta == null ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("-1") ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("0") )
    		{
    			control.setMsg("OK");
    		}
    	}
    	cargaPerfiles(control);
    	request.getSession().setAttribute("listPerfilesByAlumno", control.getListaBandeja());
		
	    return new ModelAndView("/evaluaciones/perfilAlumno/per_perfilalumno_registrar","control",control);		
    }
    
    private String gragarCompetencias(PerfilAlumnoRegistroCommand control)
    {
    	StringTokenizer stCompetencias = new StringTokenizer(control.getCadCodCompetencia(),"|");
    	StringTokenizer stEvaluaciones = new StringTokenizer(control.getCadCodRespuesta(),"|");
    	String cadenaDatos = "";
    	String codEvaluacion = "";
    	try
    	{
    		while (stCompetencias.hasMoreTokens())
    		{
    			cadenaDatos = cadenaDatos + stCompetencias.nextToken() + "$";
    			
    			codEvaluacion = stEvaluaciones.nextToken();
    			if ( codEvaluacion.trim().equals("0") ) codEvaluacion = "";
    			
    			if ( control.getIndTipo().trim().equals("1"))
    				cadenaDatos = cadenaDatos + codEvaluacion + "$$";
    			else cadenaDatos = cadenaDatos + "$" + codEvaluacion + "$";
    			
    			cadenaDatos = cadenaDatos + "|";
    		}
    		
    		return this.operDocenteManager.insertCompetenciasByAlumno(
    					control.getCodAlumno(), ""
    					, control.getCodPeriodo(), CommonConstants.COMP_IND_OTR
    					, cadenaDatos, control.getCodEvaluador(),"", "");
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return null;
    }
    
    private void cargaPerfiles(PerfilAlumnoRegistroCommand control)
    {
		ParametrosEspeciales parametrosEspeciales;
		ArrayList arrValorComp = (ArrayList)mantenimientoManager.getAllParametrosEspeciales(control.getCodPeriodo(), "0003");
		if ( arrValorComp == null) control.setMsg("-99");
		else if ( arrValorComp.size() == 0) control.setMsg("-99");
		else
			for ( int j = 0; j < arrValorComp.size(); j++)
			{
				parametrosEspeciales = (ParametrosEspeciales)arrValorComp.get(j);
				if ( parametrosEspeciales.getValor1() == null ) control.setMsg("-99");
				else if ( parametrosEspeciales.getValor1().trim().equals("")) control.setMsg("-99");				
			}
		
    	if (control.getIndTipo().trim().equals("1")) control.setDscCalificacion("Inicio");
    	else control.setDscCalificacion("Fin");
    	
    	control.setListaBandeja(this.perfilAlumnoManager.getDetalleAlumno(control.getCodPeriodo()
    			, control.getCodEvaluador(), control.getCodAlumno(), control.getIndTipo().trim()
				, CommonConstants.COMP_IND_OTR));
    }
}
