package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.ControlAsistenciaCommand;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarAsistenciaCommand;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.*;

public class RegistrarAsistenciaActualFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RegistrarAsistenciaActualFormController.class);
	
	private OperDocenteManager operDocenteManager;
	
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ServletException {
    	
    	RegistrarAsistenciaCommand command = new RegistrarAsistenciaCommand();  	
    	   	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	
    	if(usuarioSeguridad == null) throw new ServletException("UsuarioSeguridad no logueado");
    	
    	log.info("Informacion de curso para: "+usuarioSeguridad.getIdUsuario());
    	// Informacion de curso
//    	CursoEvaluador dataCurso = this.operDocenteManager.getCursoInTime(usuarioSeguridad.getIdUsuario());
//    	if(dataCurso == null) return command;
//    	
//    	
//    	command.setCodEval(usuarioSeguridad.getIdUsuario());
//    	
//    	command.setCodCursoEjec(dataCurso.getCodCursoEjec());
//    	command.setCodPeriodo(dataCurso.getCodPeriodo());
//    	command.setPeriodo(dataCurso.getPeriodo());
//    	command.setCodProducto(dataCurso.getCodProducto());
//    	command.setCodCurso(dataCurso.getCodCurso());
//		command.setCurso(dataCurso.getCurso());
//		command.setCodSeccion(dataCurso.getCodSeccion());
//		command.setCodTipoSesion(dataCurso.getCodTipoSesion());
//		command.setSistEval(dataCurso.getSistEval());
//		command.setNomEval(dataCurso.getEvaluador());
//		command.setTipoSesion(dataCurso.getDscTipoSesion());
//		command.setEspecialidad(dataCurso.getEspecialidad());
//		command.setCiclo(dataCurso.getCiclo());
//		command.setSeccion(dataCurso.getDscSeccion());
//		command.setAmbiente(dataCurso.getAmbiente());
//		command.setTipoSemana(dataCurso.getTipoSemana());
//		command.setFecha(dataCurso.getFecha());
//		command.setHoraInicio(dataCurso.getHoraInicio());
//		command.setHoraFin(dataCurso.getHoraFin());
//		    	
//    	// Lista de alumnos
//		request.setAttribute("listaAsistenciaCurso", this.operDocenteManager.getAllAlumnosCurso(dataCurso.getCodPeriodo(), dataCurso.getCodCurso(), 
//				dataCurso.getCodProducto(), null, dataCurso.getCodTipoSesion(),	dataCurso.getCodSeccion(), null, null));
//    			
//    	/*Fecha actual*/
//    	command.setFechaActual(Fecha.getFechaActual());
//    	request.setAttribute("fechaActual", command.getFechaActual().trim());
    	    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,  ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class, nf, true));	
    }
   
//    public ModelAndView processFormSubmission(HttpServletRequest request,
//                                              HttpServletResponse response,
//                                              Object command,
//                                              BindException errors)
//    throws Exception {
//        return super.processFormSubmission(request, response, command, errors);
//    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	RegistrarAsistenciaCommand control = (RegistrarAsistenciaCommand)command;  	
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if("GRABAR".equals(control.getOperacion().trim())){
    		String rpta = "";
    		
    		String [] strAsistencia = control.getCadena().split("_");
    		log.info("Cadena Principal: "+control.getCadena());
    		String cadena;
    		String valor1, valor2, valor3, valor4 , codseccion;
    		String sesionGrabacion = "";
    		String fechaGrabacion = "";
    		int i;
    		
    		/**/
    		String save = control.getSaveFecha();
    		log.info("Programada: "+save);
    		if ("1".equals(save)){
    			control.setFlgProg("1"); //Programada
    		}
    		else
    			control.setFlgProg("0"); //Ya tomada
    		/**/
    		
			for (i = 0 ; i < strAsistencia.length ; i++){
    			cadena = strAsistencia[i];
    			log.info("Cadena: "+cadena);
    			String [] stkCadena = cadena.split("\\$");
    			valor1 = stkCadena[0];
				valor2 = stkCadena[1];
				valor3 = stkCadena[2];
				valor4 = stkCadena[3];
				codseccion = stkCadena[4];
				log.info("Alumno:"+valor1 + " NrSession:"+valor3);
				sesionGrabacion = valor3;
				fechaGrabacion = valor4;
				
				rpta = insertarAsistencia(control.getCodPeriodo(), "", valor1.trim(), control.getCodCurso().trim(), control.getTipoSesion().trim(),
											codseccion, valor3.trim(), valor4.trim(), 
											valor2.trim(), control.getCodEval().trim()
											, control.getFlgProg()
											);
				log.info("RPTA:" + rpta);
				if ("-1".equals(rpta)){
	        		control.setMessage(CommonMessage.GRABAR_ERROR);
	        		control.setTypeMessage("ERROR");
	        		break;
	        	}
    		}
    		
    		if ("0".equals(rpta)){
    			searchAlumnosByAsistencia(control, "", control.getNombre().trim(), control.getApellido().trim(), 
    									sesionGrabacion.trim(), control.getCodCurso().trim(), 
    									control.getCodProducto(), control.getCodEspecialidad(), control.getTipoSesion(),
    									control.getCodSeccion().trim(), request);
    			
    			control.setNroSesionConsulta(sesionGrabacion.trim());
    			control.setNroSesion(sesionGrabacion.trim());
    			control.setFe(fechaGrabacion.trim());
    			
        		control.setMessage(CommonMessage.GRABAR_EXITO);
        		control.setTypeMessage("OK");
        	}
    	}
    	    	
    	
    	control.setNroSesionEliminar("");
    	control.setFechaEliminar("");
    	control.setSaveFecha("");
    			
	    return new ModelAndView("/evaluaciones/cargaOperativa/eva_carga_registrar_asistencia_actual","control",control);		
    }
   
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /*Metodos*/

	private String insertarAsistencia(String codPeriodo, String codAsistencia, String codAlumno, String codCurso, String tipoSesion, 
									String codSeccion, String nroSesion, String fecha, String tipoAsist, String usuario, String flagProg){
	
		return this.operDocenteManager.insertAsistencia(codPeriodo, codAsistencia, codAlumno, codCurso, tipoSesion, codSeccion, 
														nroSesion, fecha, tipoAsist, usuario, flagProg);
	}
    
	private void searchAlumnosByAsistencia(RegistrarAsistenciaCommand command, String codAsistencia, String nombre, 
											String apellido, String nroSesion, String codCurso, 
											String codProducto, String codEspecialidad, String tipoSesion, 
											String codSeccion, HttpServletRequest request){
		
		command.setListaAlumnos(this.operDocenteManager.getAllAlumnosXasistencia(command.getCodPeriodo(), 
								codAsistencia, nombre, apellido, nroSesion, codCurso, codProducto,
								codEspecialidad, tipoSesion, codSeccion));
		
		request.setAttribute("listaAsistenciaCurso", command.getListaAlumnos());
		
		if(command.getListaAlumnos().size() == 0){
			command.setTypeMessage("REG");
		}
	}
	
}
