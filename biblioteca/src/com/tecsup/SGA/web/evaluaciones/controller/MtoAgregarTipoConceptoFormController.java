package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.MtoAgregarTipoConceptoCommand;


public class MtoAgregarTipoConceptoFormController extends SimpleFormController{

	private static Log log = LogFactory.getLog(MtoAgregarTipoConceptoFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	MtoAgregarTipoConceptoCommand command = new MtoAgregarTipoConceptoCommand();
    	String codDetalle = (String)request.getParameter("txhCodAreasNivelUno");    	
    	String descripcion =(String)request.getParameter("txhDscNivelUno");
    	
    	command.setCodDetalle(""); 
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	command.setValor1(codDetalle);
    	
    	if ( codDetalle != null ) if ( !codDetalle.trim().equals("") ) cargaDetalle(command, codDetalle, descripcion);
    	
       
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	MtoAgregarTipoConceptoCommand control = (MtoAgregarTipoConceptoCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		
    		resultado = InsertTablaDetalle(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) control.setMsg("ERROR");
    		else{if(resultado.equals("-2"))  control.setMsg("DUPLICADO");
    		     else
    		     control.setMsg("OK");}
    	}
    	control.setOperacion("");
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_agregar_tipo_concepto","control",control);		
    }
    
    private String InsertTablaDetalle(MtoAgregarTipoConceptoCommand control)
    {
    	 
    	try
    	{
    		TipoTablaDetalle obj = new TipoTablaDetalle();
    	
    		obj.setCodTipoTabla(CommonConstants.TIPT_CONCEPTOS); 
    		obj.setDescripcion(control.getDescripcion());
    		
    		obj.setDscValor1(control.getValor1());
    		
    		
    			obj.setCodTipoTablaDetalle(this.tablaDetalleManager.InsertTablaDetalle(obj
    					, control.getCodEvaluador()));
        		return obj.getCodTipoTablaDetalle();
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(MtoAgregarTipoConceptoCommand command, String codDetalle, String descripcion)
    {
    	TipoTablaDetalle obj = new TipoTablaDetalle();    	
		command.setDescripcionNivel1(descripcion);
		command.setCodDetalle(codDetalle);
    }
}
