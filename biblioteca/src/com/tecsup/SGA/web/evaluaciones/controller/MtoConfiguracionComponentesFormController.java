package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.Vector;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Especialidad;
import com.tecsup.SGA.modelo.ParametrosGenerales;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.ProgramaComponentes;
import com.tecsup.SGA.modelo.Programas;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.service.evaluaciones.MtoConfiguracionComponentesManager;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.MtoConfiguracionComponentesCommand;


public class MtoConfiguracionComponentesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MtoConfiguracionComponentesFormController.class);
	private MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager;
	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	private ComunManager comunManager;
	private TablaDetalleManager tablaDetalleManager;
	
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}
	
	public void setMtoConfiguracionComponentesManager(
			MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager) {
		this.mtoConfiguracionComponentesManager = mtoConfiguracionComponentesManager;
	}
	
	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}
	String CadenaComponentes="";
	String madfrog="";
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    		
    	Producto producto= new Producto();
    	MtoConfiguracionComponentesCommand command = new MtoConfiguracionComponentesCommand();
    	//command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	List listaProductos=this.mtoParametrosGeneralesManager.getProductos();
    	
    	if(listaProductos!=null)
    	command.setCodListaProducto(this.mtoParametrosGeneralesManager.getProductos());
    	else command.setCodListaProducto(new ArrayList());
    	
    	List lista9=new ArrayList();
    	lista9=this.tablaDetalleManager.getAllTablaDetalle( CommonConstants.TIPT_SEDE, "", "", "", "", "", 
    			"", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista9!=null)
    	{	command.setListaPeriodo(lista9);
    	 
    	}
    	command.setCodPFR(CommonConstants.TIPT_PRODUCTO_PFR);
    	command.setCodPCC_I(CommonConstants.TIPT_PRODUCTO_PCC_I);
    	command.setCodPCC_CC(CommonConstants.TIPT_PRODUCTO_PCC_CC);
    	command.setCodCAT(CommonConstants.TIPT_PRODUCTO_CAT);
    	command.setCodPAT(CommonConstants.TIPT_PRODUCTO_PAT);
    	command.setCodPIA(CommonConstants.TIPT_PRODUCTO_PIA);
    	command.setCodPE(CommonConstants.TIPT_PRODUCTO_PE);
    	command.setCodHIBRIDO(CommonConstants.TIPT_PRODUCTO_HIBRIDO);
    	command.setCodTecsupVirtual(CommonConstants.TIPT_PRODUCTO_TECSUP_VIRTUAL);
    	command.setCodEtapaProgramaIntegral(CommonConstants.TIPT_ETAPA_PROGRAMA_INTEGRAL);
    	command.setCodEtapaCursoCorto(CommonConstants.TIPT_ETAPA_CURSOS_CORTOS);
    	
        return command;
    }
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
	throws Exception {
	return super.processFormSubmission(request, response, command, errors);
     }
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	Producto producto= new Producto();
    	Curso curso= new Curso();
    	Programas programas= new Programas();
    	Especialidad especialidad= new Especialidad();
    	ProgramaComponentes programaComponentes= new ProgramaComponentes();
        MtoConfiguracionComponentesCommand control = (MtoConfiguracionComponentesCommand) command;
        log.info("OPERACION:" + control.getOperacion());
        programaComponentes.setCodProducto(control.getCodProducto().trim());
        programaComponentes.setCodComponente(control.getCodComponentesSeleccionados());
        String codEtapa="";
        /*log.info("<<"+control.getCodPeriodo()+">>OnSubmit<<"+control.getCodEvaluador()+">>");
        log.info("es PRODUCTO "+control.getCodProducto());
        log.info("Componentes Seleccionados: "+control.getCodComponentesSeleccionados());*/
       
    	producto.setCodProducto(control.getCodProducto().trim());
    	///////////////////////////////////Inicio de la Busqueda del Periodo by Sede//////////////////////////////////////
    	ArrayList<Periodo> listaPeriodo;
    	Periodo periodo=new Periodo();
    	
    	listaPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoBySede(control.getCodSede());
		if ( listaPeriodo != null )
		{	if ( listaPeriodo.size() > 0)
			{
				periodo = listaPeriodo.get(0);
				control.setCodPeriodo(periodo.getCodigo());				
			}
		
		}
		
		/////////*******************************Fin de Busqueda del Perido by Sede*************************************/////
    	if(control.getOperacion().equals("PFR")){
    		
    		control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
    		control.setCodEspecialidadPFR("-1");
    		//control.setCodListaCursoPFR(Cursos(control, producto));
    		request.setAttribute("Producto", "producto");
    	}
       else{		
    	//////////////////////////////////////////////////////////////
    	if(control.getOperacion().equals("CAT")){
    		
    		programas.setCodPrograma("");
    		especialidad.setCodEspecialidad("");
    		control.setCodListaCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
    		
    		request.setAttribute("Producto", "cat");
    	}    	
    	else{
		     ////////////////////////////////////////////////////////
		    if(control.getOperacion().equals("PIA")){
		       
		    	control.setCodListaCursoPIA(CursosSolos(producto, control.getCodPeriodo()));
		        request.setAttribute("Producto", "pia");
		       
		    }
			else{
				////////////////////////////////////////////
				if(control.getOperacion().equals("PAT")){				   
				    control.setCodListaCursoPAT(CursosSolos(producto, control.getCodPeriodo()));				    
				    request.setAttribute("Producto", "pat");
				}
				else{
					//////////////////////////////////////////////////////
					if(control.getOperacion().equals("ESPECIALIZACION")){
						control.setValorEtapa("");
						control.setCodListaProgramaPE(ListaPrograma(control, producto));
						
						control.setCodSelProgramaPE("-1");
						//control.setCodListaCursoPE(Cursos(control, producto));     
						request.setAttribute("Producto", "especializacion");
					}
					else{
						///////////////////////////////////////////////////
						if(control.getOperacion().equals("HIBRIDO")){
							
							control.setCodListaCursoHibrido(CursosSolos(producto, control.getCodPeriodo()));
							request.setAttribute("Producto", "hibrido");
						}
						else{
							///////////////////////////////////////////////
							if(control.getOperacion().equals("PCC_I")){
								
								control.setValorEtapa(CommonConstants.TIPT_ETAPA_PROGRAMA_INTEGRAL);
								control.setCodListaProgramaPCC(ListaPrograma(control, producto));
								control.setCodSelProgramaPCC("-1");
								request.setAttribute("Producto", "pcc1");
							}
							else{
								if(control.getOperacion().equals("PCC_CC")){
									
									especialidad.setCodEspecialidad("");
									curso.setCodCiclo("");
									codEtapa="";
									programas.setCodPrograma("");
									
									control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
									
									request.setAttribute("Producto", "pcc2");
								}
								else{
									
										////////////////////////  GRABAR   ///////////////////////////
										if(control.getOperacion().equals("GRABAR")){
									
											programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
											ListaComponentes( control, producto, request, response, programaComponentes);	
											String bb=Guardar(control, request, response, producto, curso);		
											control.setCodComponentesSeleccionados("");
										}
										else{
											///////////////////  LLenando los Combos HERE       ////////////////////////////////
											///////////////////////// PFR /////////////////////////////////////////////
											if(control.getOperacion().equals("CicloPFR")){
											    curso.setCodCiclo(control.getCodSelCicloPFR().trim());
												programas.setCodPrograma("");
											   	control.setCodSelCursoPFR("-1");
											    especialidad.setCodEspecialidad(control.getCodEspecialidadPFR());
											   	control.setCodListaCursoPFR(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
											   	control.setCodListaCicloPFR(Ciclos(control, producto, programas,especialidad, control.getCodPeriodo()));
											    control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
											    request.setAttribute("Producto", "producto");
											}
											else{
												if(control.getOperacion().equals("CursoPFR")){
												    especialidad.setCodEspecialidad(control.getCodEspecialidadPFR());
													programas.setCodPrograma("");
													curso.setCodCiclo(control.getCodSelCicloPFR());
													control.setCodListaCicloPFR(Ciclos(control, producto, programas,especialidad, control.getCodPeriodo()));
													control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
													control.setCodListaCursoPFR(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
													programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
													
													if(((!control.getCodEspecialidadPFR().equals("-1"))&&(!control.getCodSelCursoPFR().equals("-1")))&&
													    		(!control.getCodSelCicloPFR().equals("-1")))
													     ListaComponentes( control, producto, request, response, programaComponentes);
													request.setAttribute("Producto", "producto");
												}
												else{
													 if(control.getOperacion().equals("EspecialidadPFR")){
														 curso.setCodCurso(control.getCodSelCursoPFR().trim());
														 especialidad.setCodEspecialidad(control.getCodEspecialidadPFR());
														 programas.setCodPrograma("");
														 control.setCodSelCicloPFR("-1");
														 control.setCodSelCursoPFR("-1");
														 control.setCodListaCicloPFR(Ciclos(control, producto, programas,especialidad, control.getCodPeriodo()));
														 programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
														 control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
														 //log.info("<<FOV>> "+ control.getCodEspecialidadPFR()+" <<SKY>> "+control.getCodSelCursoPFR()+" <<TOD>> "+control.getCodSelCicloPFR());
														 if(((!control.getCodEspecialidadPFR().equals("-1"))&&(!control.getCodSelCursoPFR().equals("-1")))&&
														    		(!control.getCodSelCicloPFR().equals("-1")))
														    ListaComponentes( control, producto, request, response, programaComponentes);
														 
														 request.setAttribute("Producto", "producto");
													     }
													else{
														//////////////////////// FIN PFR //////////////////////////////////
														/////////////////////////// PE ////////////////////////////////////////
														if(control.getOperacion().equals("ModuloPE")){
															
															curso.setCodCiclo(control.getCodSelModuloPE().trim());
															programas.setCodPrograma(control.getCodSelProgramaPE());
															especialidad.setCodEspecialidad("");
															control.setCodListaModuloPE(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo())); 
															control.setCodListaProgramaPE(ListaPrograma(control, producto));
															control.setCodListaCursoPE(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
															control.setCodSelCursoPE("-1");
															request.setAttribute("Producto", "producto");
														}
														else{
															if(control.getOperacion().equals("CursoPE")){
																especialidad.setCodEspecialidad("");
																curso.setCodCiclo(control.getCodSelModuloPE());
																programas.setCodPrograma(control.getCodSelProgramaPE().trim());
															  	curso.setCodCurso(control.getCodSelCursoPE().trim());
																control.setCodListaModuloPE(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
																control.setCodListaProgramaPE(ListaPrograma(control, producto));
																control.setCodListaCursoPE(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
																programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																
																if(((!control.getCodListaProgramaPE().equals("-1"))&&(!control.getCodSelCursoPE().equals("-1")))&&
															    		(!control.getCodSelModuloPE().equals("-1")))
															       ListaComponentes( control, producto, request, response, programaComponentes);
																request.setAttribute("Producto", "producto");
															}
															else{
																 if(control.getOperacion().equals("ProgramaPE")){
																	
																	 especialidad.setCodEspecialidad("");
																	 programas.setCodPrograma(control.getCodSelProgramaPE());
																	
																	 control.setCodListaModuloPE(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
																	 
																	 control.setCodListaProgramaPE(ListaPrograma(control, producto));
																	 programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																	 if(((!control.getCodSelModuloPE().equals("-1"))&&(!control.getCodSelCursoPE().equals("-1")))&&
																	    		(!control.getCodSelProgramaPE().equals("-1")))
																	   { programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																		 ListaComponentes( control, producto, request, response, programaComponentes);
																		 																						
																	 }
																		request.setAttribute("Producto", "producto");
																   }
																else{
																	///////////////////////// FIN PE ////////////////////////////////////////////
																	//////////////////////// CAT //////////////////////////////////////////////////
																	if(control.getOperacion().equals("CicloCAT")){
																		curso.setCodCiclo(control.getCodSelCicloCAT().trim());
																		especialidad.setCodEspecialidad("");
																		programas.setCodPrograma("");
																		control.setCodListaCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
																		control.setCodListaCursoCAT(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
																		
																		control.setCodSelCursoCAT("-1");
																		request.setAttribute("Producto", "producto");
																	}
																	else{
																		if(control.getOperacion().equals("CursoCAT")){
																			curso.setCodCurso(control.getCodSelCursoCAT().trim());
																			curso.setCodCiclo(control.getCodSelCicloCAT().trim());
																			especialidad.setCodEspecialidad("");
																			programas.setCodPrograma("");
																			control.setCodListaCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
																			control.setCodListaCursoCAT(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
																			programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																			
																			if((!control.getCodSelCursoCAT().equals("-1"))&&(!control.getCodSelCicloCAT().equals("-1")))
																			    ListaComponentes( control, producto, request, response, programaComponentes);
																			request.setAttribute("Producto", "producto");
																		}
																		else{
																			///////////////////////////// FIN CAT ////////////////////////////////////////
																			//////////////////////////// PAT /////////////////////////////////////////////
																			if(control.getOperacion().equals("CursoPAT")){
																				control.setCodListaCursoPAT(CursosSolos(producto, control.getCodPeriodo()));
																				programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																				
																				 if(!control.getCodSelCursoPAT().equals("-1"))
																				     ListaComponentes( control, producto, request, response, programaComponentes);
																				 request.setAttribute("Producto", "producto");
																			}
																			else{	
																			  	///////////////////////// FIN PAT ////////////////////////////////////////////
																			    //////////////////////// PIA ////////////////////////////////////////////////
																				if(control.getOperacion().equals("CursoPIA")){
																					control.setCodListaCursoPIA(CursosSolos(producto, control.getCodPeriodo()));
																					programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																					
																					if(!control.getCodSelCursoPIA().equals("-1"))
																					   ListaComponentes( control, producto, request, response, programaComponentes);
																					request.setAttribute("Producto", "producto");
																					}
																				else{
																					////////////////////////// FIN PIA //////////////////////////////////////////////
																					/////////////////////////// HIBRIDO /////////////////////////////////////////////
																					if(control.getOperacion().equals("CursoHibrido")){
																						control.setCodListaCursoHibrido(CursosSolos(producto, control.getCodPeriodo()));
																						programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																						
																						if(!control.getCodSelCursoHibrido().equals("-1"))
																						    ListaComponentes( control, producto, request, response, programaComponentes);
																						request.setAttribute("Producto", "producto");
																					}
																					else{
																						if(control.getOperacion().equals("ProgramaIntegralPCC")){
																							
																							especialidad.setCodEspecialidad("");
																							programas.setCodPrograma(control.getCodSelProgramaPCC());
																							codEtapa="";
																							curso.setCodCiclo("");
																							
																							if(!control.getCodSelProgramaPCC().equals("-1")){
																								control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
																							}
																							
																							control.setCodListaProgramaPCC(ListaPrograma(control, producto));
																							control.setCodSelCursoPCC("-1");
																							control.setValorEtapa("1");
																							
																							request.setAttribute("Producto", "producto");
																							request.setAttribute("VALORETAPA1", "UNO");
																					       }
																						else{																								 
																							  if(control.getOperacion().equals("TECSUP_VIRTUAL")){
																										   
																										    control.setCodListaCursoTECSUPVIRTUAL(CursosSolos(producto, control.getCodPeriodo()));
																										    request.setAttribute("Producto", "TECSUP_VIRTUAL");
																										   }
																								      	  else{ if(control.getOperacion().equals("CursoTecsupVirtual")){
																												control.setCodListaCursoTECSUPVIRTUAL(CursosSolos(producto, control.getCodPeriodo()));
																												programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																												
																												 if(!control.getCodSelCursoTECSUPVIRTUAL().equals("-1"))
																												     ListaComponentes( control, producto, request, response, programaComponentes);
																												 request.setAttribute("Producto", "producto");
																											   }
																								      	  		else{ if(control.getOperacion().equals("CursoPCC_I")){
																														especialidad.setCodEspecialidad("");
																														
																														codEtapa="";
																														programas.setCodPrograma(control.getCodSelProgramaPCC());
																														programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																														control.setCodListaProgramaPCC(ListaPrograma(control, producto));
																														if((!control.getCodSelCursoPCC().equals("-1"))&&(!control.getCodSelProgramaPCC().equals("-1")))
																															    ListaComponentes( control, producto, request, response, programaComponentes);
																														
																														curso.setCodCiclo("");
																														control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
																														request.setAttribute("Producto", "producto");
																														request.setAttribute("CODIGOETAPA", programaComponentes.getCodEtapa());
																														}
																								      	  			else{ if(control.getOperacion().equals("CursoPCC_CC")){
																														especialidad.setCodEspecialidad("");
																														codEtapa="";
																														programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																														control.setValorEtapa(""); 
																														programas.setCodPrograma("");
																													    if(control.getCodSelCursosCortosPCC()!=null)
																													      {	if(!control.getCodSelCursosCortosPCC().equals("-1"))
																															     ListaComponentes( control, producto, request, response, programaComponentes);
																													      }
																														curso.setCodCiclo("");
																														control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
																														request.setAttribute("Producto", "producto");
																														}
																								      	  				
																								      	  			}
																								      	  			
																								      	  		}
																								      		  
																								      	  }
																							
																						    }
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										
									}
								}
							}
						}
					}
			   }
		  }
	}	
    	///////////////////// FIN PCC1 /////////////////////////////////////////////////////
    	
    ///////////////////////////////////////////////////////////////////////////////////////////////////	
    			
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_config_componentes","control",control);		
    }
	 public String Guardar(MtoConfiguracionComponentesCommand control, HttpServletRequest request, 
			 HttpServletResponse response, Producto producto, Curso curso){
		 String bandera="";
		 ProgramaComponentes programaComponentes= new ProgramaComponentes();
		 Programas programas= new Programas();
		 Especialidad especialidad= new Especialidad();
		 String codEtapa="";
		 programaComponentes.setCodProducto(control.getCodProducto().trim());
		 programaComponentes.setCodComponente(control.getCodComponentesSeleccionados());
		 /*log.info("<<codProducto>> "+programaComponentes.getCodProducto());
		 log.info("<<codComponente>> "+programaComponentes.getCodComponente());
		 log.info("<<tipoProducto>> "+control.getTipoProducto());
		 log.info("<<CursoCiclo y COdCurso>> "+curso.getCodCiclo()+curso.getCodCurso());*/
		 madfrog="";
		 request.setAttribute("Producto", "producto");
		
		 //log.info("<<Componentes Seleccionados>> "+control.getCodComponentesSeleccionados());
		 String cadena=control.getCodComponentesSeleccionados();//programaComponentes.getCodComponente(); //"0003|0005|0006|";
		
		 //log.info("6Lista Componentes "+control.getListaComponentes().size());
		 ProgramaComponentes tk= new ProgramaComponentes();
		 for(int kk=0;kk<control.getListaComponentes().size();kk++)
		 {   tk=(ProgramaComponentes)control.getListaComponentes().get(kk);
		     //log.info("<<"+tk.getCodComponente()+">>||<<"+tk.getFlag()+">>"); 
		     if(tk.getFlag()==null)
		     { //System.out.println("THOOO");	 
			   madfrog=(madfrog+tk.getCodComponente()+"$"+"0"+"|").trim();}
		     else
		     madfrog=(madfrog+tk.getCodComponente()+"$"+tk.getFlag()+"|").trim();
		 }
		 //log.info("La cadena de componentes al inicio"+madfrog);
		 
	     String cadenaFinal=madfrog;
	     String nroRegistros= String.valueOf(madfrog.length()/7);
	     //log.info("# registros "+nroRegistros + " longitud "+(madfrog.length()));
	        int k=cadena.length()/5;
	        for(int p=0;p<k;p++)
	        {String dina=cadena.substring(5*(p),5*(p)+4);
	        //log.info("Valor de la cadena "+dina);
	         int pos=madfrog.indexOf(dina);
	         //log.info("Valor de Dina: "+dina);
	         int longitud=cadenaFinal.length();
	         String subStr=cadenaFinal.substring(pos+6);
	         String subStr2=cadenaFinal.substring(0, pos+5);
	         String subStr3=cadenaFinal.substring(pos+6, longitud);
	         String subStr4=cadenaFinal.substring(pos+5,pos+6);
	         if(subStr4.equals("0"))subStr4="1"; 
	         else subStr4="0";
	         cadenaFinal=(subStr2+subStr4+subStr3).trim();
	         //log.info("Valor de la cadena dentro del for: "+cadenaFinal);
	        }
	      
	        //log.info("La cadena Inicial es: "+madfrog);
	        //log.info("La cadena Final   es: "+cadenaFinal);
	        programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
	        programaComponentes.setCodComponente(cadenaFinal);
	        cadenaFinal="";
	       
	bandera=this.mtoConfiguracionComponentesManager.InsertComponentes(programaComponentes, 
	    			  nroRegistros, control.getCodEvaluador());   
	//System.out.println("Resultado del Grabar: "+bandera);
	if(control.getTipoProducto().equals("PFR")){
		/*log.info("<<dentro del guradar PFR>>");
		log.info("antes del guardar PFR");
		log.info("Valor de la Bandera en el guardar PFR"+bandera);*/
		if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
	    else{
	    request.setAttribute("GUARDAR", "OK");
	    programas.setCodPrograma("");
	    especialidad.setCodEspecialidad(control.getCodEspecialidadPFR());
	    curso.setCodCiclo(programaComponentes.getCodCiclo());
	    curso.setCodCurso(programaComponentes.getCodCurso());
	    ListaComponentes(control, producto, request, response, programaComponentes);
	    control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
	    control.setCodListaCursoPFR(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));   
	    control.setCodListaCicloPFR(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo()));
	    //log.info("Curso Ciclo Guardar "+curso.getCodCiclo()+" Curso Codigo Guardar "+curso.getCodCurso());
	    //log.info("Tama�o de la Lista Ciclos Despues del Guardar "+control.getCodListaCicloPFR().size());
	    	 }
		//log.info("<<valor de la bandera del guardar PFR> "+bandera);
		}
	else{  
		 if(control.getTipoProducto().equals("CAT")){
			if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
	    	else{programas.setCodPrograma("");
		         especialidad.setCodEspecialidad("");
	    		 curso.setCodCiclo(programaComponentes.getCodCiclo());
	    	     curso.setCodCurso(programaComponentes.getCodCurso());
			     control.setCodListaCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo())); 
	      	     ListaComponentes( control, producto, request, response, programaComponentes);
	      	     control.setCodListaCursoCAT(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
	      	     request.setAttribute("GUARDAR", "OK");
	      	    }
		  }
		 else{
			 if(control.getTipoProducto().equals("PIA")){
				 if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
			     else{
			    		 ListaComponentes(control, producto, request, response, programaComponentes);
			    		 control.setCodListaCursoPIA(CursosSolos(producto, control.getCodPeriodo()));
			      	     request.setAttribute("GUARDAR", "OK");
			      	 }
				}
			 else{
				 if(control.getTipoProducto().equals("PAT")){
					  	if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
					    else{	
					    	    ListaComponentes(control, producto, request, response, programaComponentes);
					    		control.setCodListaCursoPAT(CursosSolos(producto, control.getCodPeriodo()));
					    		
					      	    request.setAttribute("GUARDAR", "OK");
					      	}
				  }
				 else{
					 if(control.getTipoProducto().equals("PROGRA_ESPECIALIZADO")){
						 	if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
						    else{  programas.setCodPrograma(control.getCodSelProgramaPE());
						           especialidad.setCodEspecialidad(""); 
						    	   curso.setCodCiclo(programaComponentes.getCodCiclo());
						    	   curso.setCodCurso(programaComponentes.getCodCurso());
						    	   control.setCodListaModuloPE(Ciclos(control, producto, programas, especialidad, control.getCodPeriodo())); 
						     	   ListaComponentes(control, producto, request, response, programaComponentes);
						     	   control.setCodListaProgramaPE(ListaPrograma(control, producto));
						     	   control.setCodListaCursoPE(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
						     	   request.setAttribute("GUARDAR", "OK");
						    }		
					 }
					 else{
						 if(control.getTipoProducto().equals("HIBRIDO")){
							 	if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
							    else{ ListaComponentes(control, producto, request, response, programaComponentes);
						    		  control.setCodListaCursoHibrido(CursosSolos(producto, control.getCodPeriodo()));
						    		  request.setAttribute("GUARDAR", "OK");
							    }
						 }
						 else{
							 if(control.getTipoProducto().equals("PCC_I")){
								 if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
								 else{ programas.setCodPrograma(control.getCodSelProgramaPCC());
								       especialidad.setCodEspecialidad("");
								       codEtapa="";
								       								       
									   ListaComponentes(control, producto, request, response, programaComponentes);
									   curso.setCodCiclo("");
									   control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
								       control.setCodListaProgramaPCC(ListaPrograma(control, producto));
								        request.setAttribute("GUARDAR", "OK");
								      }
							 }
							 else{ if(control.getTipoProducto().equals("TECSUP_VIRTUAL")){
								  	if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
								    else{	
								    	    ListaComponentes(control, producto, request, response, programaComponentes);
								    		control.setCodListaCursoTECSUPVIRTUAL(CursosSolos(producto, control.getCodPeriodo()));
								    		
								      	    request.setAttribute("GUARDAR", "OK");
								      	}
							 		}
							 		else{ if(control.getTipoProducto().equals("PCC_CC")){
										 if(bandera.equals("-1")) request.setAttribute("GUARDAR", "ERROR");
										 else{ programas.setCodPrograma(control.getCodSelProgramaPCC());
										       especialidad.setCodEspecialidad("");
										       codEtapa="";
										       
											   ListaComponentes(control, producto, request, response, programaComponentes);
											   curso.setCodCiclo("");
											   control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodo()));
										       control.setCodListaProgramaPCC(ListaPrograma(control, producto));
										       
										       request.setAttribute("GUARDAR", "OK");
										       
								    		  }
									 }
							 			
							 		}
								 
							 }
						   }
					     }
				     }
				 }	
		     }
		 }	 
		 return bandera;
	 }
	 
	public List ListaPrograma(MtoConfiguracionComponentesCommand control, Producto producto){
	   
		return this.mtoConfiguracionComponentesManager.getProgramasByProducto(producto, control.getValorEtapa());	
	}
	 
    public void ListaComponentes(MtoConfiguracionComponentesCommand control, Producto producto,
    		HttpServletRequest request, HttpServletResponse response, ProgramaComponentes programaComponentes){
    	
    	control.setListaComponentes(this.mtoConfiguracionComponentesManager.getComponentes(programaComponentes));
    	madfrog="";
    	request.getSession().setAttribute("listaComponentes", control.getListaComponentes());
    	
    }
	 
	public List Cursos(Producto producto, Programas programas,Especialidad especialidad, Curso curso, 
			String codEtapa, String codPeriodo){
		return this.mtoConfiguracionComponentesManager.getCursoByProductoEspecialidadCiclo(producto, programas,
				especialidad, curso, codEtapa, codPeriodo);
	}
	public List CursosSolos(Producto producto, String codPeriodo){
		return this.mtoConfiguracionComponentesManager.getCursosByProducto(producto, codPeriodo);
	}
	
    public List Ciclos(MtoConfiguracionComponentesCommand control, Producto producto, Programas programas,
    		Especialidad especialidad, String codPeriodo){
    
    	return this.mtoConfiguracionComponentesManager.getCicloByProductoEspecialidadPrograma(producto, especialidad,
    			programas, codPeriodo);	
    }
    
    public List ListaEspecialidad(MtoConfiguracionComponentesCommand control, Producto producto){
    	
    	return this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(producto);
    }
    public ProgramaComponentes LlenarProgramaByProducto(MtoConfiguracionComponentesCommand control,
    		ProgramaComponentes programaComponentes ){
    	if(control.getTipoProducto().equals("PFR"))
    	{   programaComponentes.setCodEspecialidad(control.getCodEspecialidadPFR().trim());
    		programaComponentes.setCodCiclo(control.getCodSelCicloPFR().trim());
    		programaComponentes.setCodCurso(control.getCodSelCursoPFR().trim());
    		programaComponentes.setCodEtapa("");
    		programaComponentes.setCodPrograma("");
    	}
    	else{
    	    if(control.getTipoProducto().equals("CAT"))
    		{   programaComponentes.setCodPrograma("");
    	        programaComponentes.setCodCiclo(control.getCodSelCicloCAT().trim());
    	        programaComponentes.setCodCurso(control.getCodSelCursoCAT().trim());
    	        programaComponentes.setCodEtapa("");
    	        programaComponentes.setCodEspecialidad("");
    		}
    		else{
    		    if(control.getTipoProducto().equals("PIA"))
    			{   programaComponentes.setCodPrograma("");
    		      	programaComponentes.setCodCiclo("");
    		      	programaComponentes.setCodCurso(control.getCodSelCursoPIA().trim());
    		      	programaComponentes.setCodEtapa("");
    		      	programaComponentes.setCodEspecialidad("");
    			}
    		    else{
    			    if(control.getTipoProducto().equals("PAT"))
    				{  programaComponentes.setCodPrograma("");
    		     	   programaComponentes.setCodCiclo("");
    		     	   programaComponentes.setCodCurso(control.getCodSelCursoPAT().trim());
    		     	   programaComponentes.setCodEtapa("");
    		     	   programaComponentes.setCodEspecialidad("");
    				}
    				else{
    			    	if(control.getTipoProducto().equals("PROGRA_ESPECIALIZADO"))
    					{	programaComponentes.setCodPrograma(control.getCodSelProgramaPE().trim());
    			        	programaComponentes.setCodCiclo(control.getCodSelModuloPE().trim());
    			        	programaComponentes.setCodCurso(control.getCodSelCursoPE().trim());
    			        	programaComponentes.setCodEtapa("");
    			        	programaComponentes.setCodEspecialidad("");
    					}
    					else{
    				    	if(control.getTipoProducto().equals("HIBRIDO"))
    						{  programaComponentes.setCodPrograma("");
    				       	   programaComponentes.setCodCiclo("");
    				       	   programaComponentes.setCodCurso(control.getCodSelCursoHibrido().trim());
    				       	   programaComponentes.setCodEtapa("");
    				       	   programaComponentes.setCodEspecialidad("");
    						}
    						else{
    					    	if(control.getTipoProducto().equals("PCC_I"))
    							  {	programaComponentes.setCodCurso(control.getCodSelCursoPCC());
    								programaComponentes.setCodEspecialidad("");
    								programaComponentes.setCodCiclo("");
    								programaComponentes.setCodPrograma(control.getCodSelProgramaPCC());
    								programaComponentes.setCodEtapa("");
    						      } 
    					    	else{ if(control.getTipoProducto().equals("TECSUP_VIRTUAL"))
        						       {   programaComponentes.setCodPrograma("");
			     				       	   programaComponentes.setCodCiclo("");
			     				       	   programaComponentes.setCodCurso(control.getCodSelCursoTECSUPVIRTUAL().trim());
			     				       	   programaComponentes.setCodEtapa("");
			     				       	   programaComponentes.setCodEspecialidad("");
     						           }
    					    		else{ if(control.getTipoProducto().equals("PCC_CC"))
      							  		  {	programaComponentes.setCodCurso(control.getCodSelCursosCortosPCC());
      							  			programaComponentes.setCodEspecialidad("");
      							  			programaComponentes.setCodCiclo("");
      							  			programaComponentes.setCodPrograma("");
      							  			programaComponentes.setCodEtapa("");
      							  		  }
    					    		    }
    					    		}
    						    }
    						}	  
    					}		  
    				}		  
    	        }
    	    }
    	
     return programaComponentes;	
    }
    

}
