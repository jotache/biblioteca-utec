package com.tecsup.SGA.web.evaluaciones.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.biblioteca.BiblioReportesManager;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.service.evaluaciones.GestionAdministradtivaManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.evaluaciones.ConsultasReportesManager;
import com.tecsup.SGA.service.evaluaciones.PerfilAlumnoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarExamenesCommand;
import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.inicio.command.MenuCommand;
import com.tecsup.SGA.modelo.DefNroEvalParciales;
import com.tecsup.SGA.modelo.Reportes;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class ReporteResultadoEvaFormController implements Controller{
	private static Log log = LogFactory.getLog(ReporteResultadoEvaFormController.class);

	private BiblioReportesManager biblioReportesManager;
	private OperDocenteManager operDocenteManager;
	private ConsultasReportesManager consultasReportesManager;
	private GestionAdministradtivaManager gestionAdmManager;
	private AlumnoManager alumnoManager;	
	private PerfilAlumnoManager perfilAlumnoManager;
	private TablaDetalleManager tablaDetalleManager;
	
    public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setPerfilAlumnoManager(PerfilAlumnoManager perfilAlumnoManager) {
		this.perfilAlumnoManager = perfilAlumnoManager;
	}

	public void setGestionAdmManager(GestionAdministradtivaManager gestionAdmManager) {
		this.gestionAdmManager = gestionAdmManager;
	}

	public void setAlumnoManager(AlumnoManager alumnoManager) {
		this.alumnoManager = alumnoManager;
	}

	public void setBiblioReportesManager(BiblioReportesManager biblioReportesManager) {
		this.biblioReportesManager = biblioReportesManager;
	}

	public ReporteResultadoEvaFormController(){
		
	}
    
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception {
    	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if(usuarioSeguridad==null){
    		InicioCommand command = new InicioCommand();
    		return new ModelAndView("/cabeceraEvaluaciones","control",command);
    	}
    	
    	
    	String opcion = request.getParameter("opcion");    	
    	List consulta;
    	String usuario="";
    	String codPeriodo = "";
    	String codAsistencia = "";
    	String nombre = "";
    	String apellido = "";
    	String nroSesion ="";
    	String codCurso = "";
    	String codProducto = "";
    	String codEspecialidad ="";
    	String tipoSesion = "";
    	String codSeccion = "";
    	String curso = "";
    	String seccion = "";
    	String sistEval = "";
    	String feAis = "";
    	String nroAsis = "";
    	String nomEval ="";
    	String nomEvalSeccion ="";
    	String codAlumno = "";
    	String alumno="";
    	String nroEval="";
    	String evalParcial="";
    	
    	String periodo = "";
    	String especialidad="";
    	String tipEval ="";
    	String ciclo="";
    	String sede = "";
    	String codSede="";
    	String producto="";
    	
    	String fechaRep = Fecha.getFechaActual();
		String horaRep = Fecha.getHoraActual();
		String pagina = "";
    	
		
		//incidencias por alumno
		
		
		ModelMap model = new ModelMap();
    	
    	model.addObject("fechaRep", fechaRep);
		model.addObject("horaRep", horaRep);
		log.info("OPCION:" +opcion);
    	//carga registrar asistencia
    	if(opcion.equals(CommonConstants.REPORTE_1)){
    		 
    		 codPeriodo = request.getParameter("codPeriodo");
    		 nroSesion = request.getParameter("nroSesion");
    		 codCurso = request.getParameter("codCurso");
    		 codProducto = request.getParameter("codProducto");
    		 codEspecialidad = request.getParameter("codEspecialidad");
    		 tipoSesion = request.getParameter("tipoSesion");
    		 codSeccion = request.getParameter("codSeccion");
    		 curso = request.getParameter("curso");
    		 seccion = request.getParameter("seccion");
    		 sistEval = request.getParameter("sisEval");
    		 feAis = request.getParameter("feAis");
    		 nroAsis = (String)request.getParameter("nroAsis");
    		 nomEval = request.getParameter("nomEval");
    		 //List consulta2;
    		 List consulta2 = this.operDocenteManager.getAllAlumnosXasistencia(codPeriodo, 
						codAsistencia, nombre, apellido, nroSesion.trim(), codCurso, codProducto,
						codEspecialidad, tipoSesion, codSeccion);
    		//System.out.println("consulta2 tot: "+consulta2.size());
    		request.getSession().setAttribute("consultaAsistencia", consulta2);
        		
    		pagina = "/evaluaciones/consultasReportes/rep_carga_RegistarAsistencia";
    	} else if(opcion.equals(CommonConstants.REPORTE_EVA_PERFILESALM)){
    		codPeriodo = request.getParameter("codPeriodo");
    		usuario = request.getParameter("usuario"); //Cod Evaluador
    		nombre = request.getParameter("nombre");
    		apellido = request.getParameter("apellido");
    		periodo = request.getParameter("periodo"); 
    		nomEval = request.getParameter("nomEval");
    		consulta = this.perfilAlumnoManager.getBandejaTutor(codPeriodo, usuario, nombre, apellido);
    		List<TipoTablaDetalle> listaCabera = new ArrayList<TipoTablaDetalle>();
    		listaCabera = this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PERFIL, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    		request.getSession().setAttribute("consultaPerfilesAlumnos_Cab", listaCabera);
    		request.getSession().setAttribute("consultaPerfilesAlumnos_Det", consulta);
    		pagina = "/evaluaciones/consultasReportes/rep_perfilesAlumnos";
    		
    	} else if(opcion.equals(CommonConstants.REPORTE_2)){ //retporte de incidencias
    		codPeriodo = request.getParameter("codPeriodo");
    		codAlumno = request.getParameter("codAlumno");
    		codCurso = request.getParameter("codCurso");
    		tipoSesion = request.getParameter("tipoSesion");
    		codSeccion = request.getParameter("codSeccion");
    		alumno = request.getParameter("alumno");
    		nomEval = request.getParameter("nomEval");
    		consulta = this.operDocenteManager.getAllIncidencias(codPeriodo
    				, codAlumno
    				, codCurso
    				, CommonConstants.INC_TIPO_OPE_DOC
    				, tipoSesion
    				, codSeccion);
    		
    		request.getSession().setAttribute("consultaAsistencia", consulta);
    		pagina = "/evaluaciones/consultasReportes/rep_incidenciasxAlumno";
    	}
    	//reporte de evaluaciones parciales
    	 else if(opcion.equals(CommonConstants.REPORTE_3)){
    		 codPeriodo = request.getParameter("codPeriodo");
    		 codCurso = request.getParameter("codCurso");
    		 codSeccion = request.getParameter("codSeccion");
    		 nombre = request.getParameter("nombre");
    		 apellido = request.getParameter("apellido");
    		 nroEval = request.getParameter("nroEval");
    		 evalParcial = request.getParameter("evalParcial");
    		 codProducto = request.getParameter("codProducto");
    		 codEspecialidad = request.getParameter("codEspecialidad");
    		 //para mostar en el reporte...
    		 periodo = request.getParameter("periodo");
    		 nomEval = request.getParameter("nomEval");
    		 curso = request.getParameter("curso");
    		 especialidad = request.getParameter("especialidad");
    		 sistEval = request.getParameter("sistEval");
    		 tipEval = request.getParameter("tipEval");
    		 ciclo  = request.getParameter("ciclo");
    		 seccion = request.getParameter("seccion");
    		 nroAsis = request.getParameter("nroAsis");//n�mero de evaluaciones....display
    		 consulta = this.operDocenteManager.getAllEvaluacionesParcialesxCurso(
    					codPeriodo,
    					codCurso,
    					codSeccion,
    					nombre,
    					apellido,
    					nroEval.trim(),
    					evalParcial,
    					codProducto,
    					codEspecialidad,"");	
    		 
    		 
    			
        		request.getSession().setAttribute("consultaAsistencia", consulta);
        		pagina = "/evaluaciones/consultasReportes/rep_EvaluacionesParciales";
    	    }
    	//reportes de evaluaciones....pagina Reporte
    	 else if(opcion.equals(CommonConstants.REPORTE_4)){
    		 codPeriodo = request.getParameter("codPeriodo");
    		 codProducto = request.getParameter("codProducto");
    		 codSede = request.getParameter("codSede");
    		 
    		 periodo = request.getParameter("periodo");
    		 sede = request.getParameter("sede");
    		 producto = request.getParameter("producto");
    		 
    		 consulta = this.consultasReportesManager.GetAllRepNotasAvance(codPeriodo, codSede, codProducto);
    		
     		request.getSession().setAttribute("consultaAsistencia", consulta);
     		pagina = "/evaluaciones/consultasReportes/rep_NotasAvance";
 	
    	 }
    	 else if(opcion.equals(CommonConstants.REPORTE_5)){
    		 codPeriodo = request.getParameter("codPeriodo");
    		 codProducto = request.getParameter("codProducto");
    		 codSede = request.getParameter("codSede");
    		 //mostrar
    		 periodo = request.getParameter("periodo");
    		 sede = request.getParameter("sede");
    		 producto = request.getParameter("producto");
    		 
    		 consulta = this.consultasReportesManager.GetAllRepNotasFinal(codPeriodo
    				 ,codSede,codProducto);
    	 
    		
     		request.getSession().setAttribute("consultaAsistencia", consulta);
      		pagina = "/evaluaciones/consultasReportes/rep_NotasFinal";
    	 }
    	 else if(opcion.equals(CommonConstants.REPORTE_6)){
    		 codPeriodo = request.getParameter("codPeriodo");
    		 codProducto = request.getParameter("codProducto");
    		 codSede = request.getParameter("codSede");
    		 codEspecialidad = request.getParameter("codEspecialidad");
    		 
    		 periodo = request.getParameter("periodo");
    		 sede = request.getParameter("sede");
    		 producto = request.getParameter("producto");
    		 especialidad = request.getParameter("especialidad");
    		 consulta = this.consultasReportesManager.GetAllRepestExamenes(codPeriodo
    				 , codSede, codProducto, codEspecialidad);
    	 
    		 
     		request.getSession().setAttribute("consultaAsistencia", consulta);
      		pagina = "/evaluaciones/consultasReportes/rep_EstExamenes";
    	 }
    	 else if(opcion.equals(CommonConstants.REPORTE_7)){
    		 codPeriodo = request.getParameter("codPeriodo");
    		 codProducto = request.getParameter("codProducto");
    		 codSede = request.getParameter("codSede");
    		 codEspecialidad = request.getParameter("codEspecialidad");
    		 
    		 periodo = request.getParameter("periodo");
    		 sede = request.getParameter("sede");
    		 producto = request.getParameter("producto");
    		 especialidad = request.getParameter("especialidad");
    		 consulta = this.consultasReportesManager.GetAllEstadoFinalAlumno(codPeriodo
    				 , codSede, codProducto, codEspecialidad);
    	 
    		
     		request.getSession().setAttribute("consultaAsistencia", consulta);
      		pagina = "/evaluaciones/consultasReportes/rep_EstadoFinalAlumno";
    	 }
    	 else if(opcion.equals(CommonConstants.REPORTE_8)){
    		 codPeriodo = request.getParameter("codPeriodo");
    		 codProducto = request.getParameter("codProducto");
    		 codSede = request.getParameter("codSede");
    		 codEspecialidad = request.getParameter("codEspecialidad");
    		 usuario = request.getParameter("usuario");
    		 periodo = request.getParameter("periodo");
    		 sede = request.getParameter("sede");
    		 producto = request.getParameter("producto");
    		 especialidad = request.getParameter("especialidad");
    		 
    		 consulta = this.consultasReportesManager.GetAllCursosAlumno(codPeriodo
    				 , codSede, codProducto, codEspecialidad, usuario);
    		 if ( consulta != null )
    			 if ( consulta.size() > 0 )
    			 {
    				 Reportes reportes = (Reportes)consulta.get(0);
    				 especialidad = reportes.getEspecialidadAlumno();
    				 nomEval = reportes.getNomAlumno();
    				 ciclo = reportes.getNivelAlumno();
    			 }
    		 
     		request.getSession().setAttribute("consultaAsistencia", consulta);
      		pagina = "/evaluaciones/consultasReportes/rep_Cursos_x_Alumno";
    	 } else if (opcion.equals(CommonConstants.REPORTE_7A) || opcion.equals(CommonConstants.REPORTE_EX_CARGO ) 
    			 || opcion.equals(CommonConstants.REPORTE_EX_SUBSANACION) || opcion.equals(CommonConstants.REPORTE_EX_RECUPERACION) ) {
    		 //JHPR: 2008-04-24
    		 //Reporte de Ex�menes
    		 RegistrarExamenesCommand exams = new RegistrarExamenesCommand();    		
    		 //Datos de cabecera...
    		 periodo = request.getParameter("periodo");
    		 nomEval = request.getParameter("nomEval");    		 
    		 nomEvalSeccion = request.getParameter("nomEvalSeccion");
    		 ciclo  = request.getParameter("ciclo");
    		 curso = request.getParameter("curso");
    		 especialidad = request.getParameter("especialidad");
    		 sistEval = request.getParameter("sistEval");
    		 seccion = request.getParameter("seccion");
    		 
    		 //JHPR: 2008-05-02 Mostrar el usuario que emite el reporte de ex�menes.
    		 usuario = request.getParameter("usuario");    		 
    		 
    		 //Datos de filtro...
    		 nombre = request.getParameter("nombre");    		 
    		 apellido = request.getParameter("apellido");
    		 
    		 //Datos para realizar la consulta de notas de alumnos...
    		 exams.setCodPeriodo(request.getParameter("codPeriodo"));    		 
    		 exams.setCodCurso(request.getParameter("codCurso"));    		 
    		 exams.setSeccion(request.getParameter("codSeccion"));    		 
    		 exams.setNombre(nombre);    		 
    		 exams.setApellido(apellido);    		 
    		 exams.setModoExamen(((String)request.getParameter("tipoExamen")).trim());
    		 model.addObject("tipoExamen", exams.getModoExamen());
    		 
    		 
    		 //model.addObject("commandExamen",exams);    		 
    		 exams.setListaTipoExams(this.gestionAdmManager.getAllTypesOfExams(request.getParameter("codPeriodo"), request.getParameter("codCurso"),request.getParameter("tipoExamen")));
    		 //System.out.println("exams.getListaTipoExams().size():"+exams.getListaTipoExams().size());    		 
    		 RegistrarExamenesCommand examRet = this.gestionAdmManager.setConsulta(exams);
    		     		 
    		 //cargando lista de tipos examenes.
    		 examRet.setListaTipoExams(this.gestionAdmManager.getAllTypesOfExams(request.getParameter("codPeriodo"), request.getParameter("codCurso"),request.getParameter("tipoExamen")));    		 
    		 request.getSession().setAttribute("consultaExamenes0", examRet);
    		 pagina = "/evaluaciones/consultasReportes/rep_Examenes";
    	 //} else if (opcion.equals(CommonConstants.REPORTE_EX_CARGO)) {
    		 
    	 } else if (opcion.equals(CommonConstants.REPORTE_7B)) {
    		 //JHPR : 2008-04-28 Reporte de Notas Especiales.
    		 //datos para mostrar en la cabecera...
    		 periodo = request.getParameter("nomperiodo");
    		 producto = request.getParameter("nomproducto");
    		 especialidad = request.getParameter("nomespecialidad");
    		 curso = request.getParameter("nomcurso");
    		 //parametros para el filtro...
    		 codPeriodo = request.getParameter("codperiodo");
    		 codProducto = request.getParameter("codproducto");
    		 codEspecialidad = request.getParameter("codespecialidad");
    		 ciclo = request.getParameter("codciclo");
    		 codCurso = request.getParameter("codcurso");
    		     		 
    		 consulta = this.alumnoManager.getAllAlumnoCasoEspecial(codPeriodo, codProducto, codEspecialidad, "", codEspecialidad, ciclo, codCurso,"","","");
    		 request.getSession().setAttribute("consultaNotasEspeciales", consulta);
    		 //JHPR: 2008-04-28 (P�GINA NUEVA) Reporte de Notas Especiales.
    		 pagina = "/evaluaciones/consultasReportes/rep_NotasEspeciales";
    	 } else if (opcion.equals(CommonConstants.REPORTE_CIERRE_NOTAS)) {
    		//JHPR : 2008-06-09 Reporte de Cierre de Notas.
    		 periodo = request.getParameter("periodo");
    		 producto = request.getParameter("producto");
    		 especialidad = request.getParameter("especialidad"); 			
    		 sede = request.getParameter("sede");
    		 
    		 String codigoSede = (String)request.getParameter("codSede"); 
    		     		 
    		 //parametros para el filtro...
    		 codPeriodo = request.getParameter("codPeriodo");
    		 codProducto = request.getParameter("codProducto");
    		 codEspecialidad = request.getParameter("codEspecialidad");
    		 ciclo = request.getParameter("codciclo");
    		 //codCurso = request.getParameter("codcurso");    		 	
    		 
    		 
    		 List<DefNroEvalParciales> consultaCierre = null;
    		 consultaCierre = this.operDocenteManager.consultarCierreNotas(codigoSede, codPeriodo, codProducto, codEspecialidad);
    		 request.getSession().setAttribute("consultaCierreNotas", consultaCierre);
    		 pagina = "/evaluaciones/consultasReportes/rep_ConsultaCierreNotas";
    	 }else if(opcion.equals(CommonConstants.REPORTE_HIST_ALUMNO)){
    		 
    		 List<Reportes> listaHistorial = new ArrayList<Reportes>();
    		 
    		 return new ModelAndView("ExcelReporteHistorial","historial",listaHistorial);
    	 }
    	
    	
    	
    	model.addObject("periodo", periodo);
    	model.addObject("curso", curso);
    	model.addObject("especialidad", especialidad);
    	model.addObject("tipEval", tipEval);
    	model.addObject("ciclo", ciclo);
    	model.addObject("sede", sede);
    	model.addObject("producto", producto);
    	
    	model.addObject("curso", curso);
    	model.addObject("seccion", seccion);
    	model.addObject("sistEval", sistEval);
    	model.addObject("feAis", feAis);
    	model.addObject("nroAsis", nroAsis); 
    	model.addObject("nomEval", nomEval);
    	model.addObject("nomEvalSeccion", nomEvalSeccion);
    	model.addObject("alumno", alumno);
    	model.addObject("usuario", usuario);
    	
    	log.info("PAGINA REPORTE:" + pagina);
	    return new ModelAndView(pagina,"model",model);
    }
    
   

	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public void setConsultasReportesManager(
			ConsultasReportesManager consultasReportesManager) {
		this.consultasReportesManager = consultasReportesManager;
	}
}
