package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.web.evaluaciones.command.IncidenciaRegistrarDocCommand;
import com.tecsup.SGA.common.CommonConstants;

public class IncidenciaRegistrarDocFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(IncidenciaRegistrarDocFormController.class);
	OperDocenteManager operDocenteManager;
	ComunManager comunManager;
		
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public IncidenciaRegistrarDocFormController(){
		super();
		setCommandClass(IncidenciaRegistrarDocCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	Periodo periodo;
    	ArrayList<Periodo> listPeriodo;
		IncidenciaRegistrarDocCommand command = new IncidenciaRegistrarDocCommand();  	
    	
		String codAlumno = request.getParameter("txhCodAlumno");
		String codCurso = request.getParameter("txhCodCurso");
		String nomAlumno = request.getParameter("txhNomAlumno");
		String codPeriodo = request.getParameter("txhCodPeriodo");
		String codEvaluador = request.getParameter("txhCodEvaluador");
		
		command.setEvaluador((String)request.getParameter("txtEvaluador"));
		command.setCodProducto((String)request.getParameter("txhCodProducto"));
		command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
		command.setCodSeccion((String)request.getParameter("txhCodSeccion"));
		command.setCodTipoSesionDefecto((String)request.getParameter("txhCodTipoSesion"));
		
		command.setCodPeriodoTutor((String)request.getParameter("txhCodPeriodoTutor"));
		command.setIndProcedencia((String)request.getParameter("txhIndProcedencia"));
		command.setCodCiclo((String)request.getParameter("txhCiclo"));
		if ( command.getIndProcedencia() == null ) command.setIndProcedencia(""); 
		if ( command.getIndProcedencia().trim().equals("1") )
			request.setAttribute("IndProcedencia","1");
		else request.setAttribute("IndProcedencia","0");
		
    	//Llenamos la descripcion del periodo
		command.setFechaIniPer("");
		command.setFechaFinPer("");
    	if ( codPeriodo != null )
    	{
    		listPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoById(codPeriodo);
    		if ( listPeriodo != null )
    			if ( listPeriodo.size() > 0)
    			{
    				periodo =listPeriodo.get(0);
    				command.setFechaIniPer(periodo.getFecInicio());
    				command.setFechaFinPer(periodo.getFecFin());
    			}
    	}
		
		if ( codAlumno != null )
		{
			command.setFechaActual(this.comunManager.getFecha().getFecha());
			command.setCodCurso(codCurso);
			command.setCodAlumno(codAlumno);
			command.setNomAlumno(nomAlumno + "  (" + codAlumno.trim() + ")" );
			command.setCodPeriodo(codPeriodo);
			command.setCodEvaluador(codEvaluador);
			command.setListIncidencias(
						this.operDocenteManager.getAllIncidencias(codPeriodo, codAlumno, codCurso
								, CommonConstants.INC_TIPO_OPE_DOC, command.getCodTipoSesionDefecto()
								, command.getCodSeccion()));
		}
		request.getSession().setAttribute("listaIncidenciasDoc", command.getListIncidencias());
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	IncidenciaRegistrarDocCommand control = (IncidenciaRegistrarDocCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	String respuesta = "";
    	if ( control.getOperacion().trim().equals("GRABAR"))
    	{
   			respuesta = agregarIncidencia(control);
   			log.info("RPTA:" + respuesta);
    		if ( respuesta == null ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("-1")) control.setMsg("ERROR");
    		else {
    			control.setMsg("OK");
    		}
    	}
    	else if ( control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		respuesta = eliminarIncidencia(control);
    		log.info("RPTA:" + respuesta);
    		if ( respuesta == null ) control.setMsg("ERROR_ELI");
    		else if ( respuesta.trim().equals("-1")) control.setMsg("ERROR_ELI");
    		else {
    			control.setMsg("OK_ELI");
    		}
    	}
		control.setListIncidencias(this.operDocenteManager.getAllIncidencias(control.getCodPeriodo()
				, control.getCodAlumno()
				, control.getCodCurso()
				, CommonConstants.INC_TIPO_OPE_DOC
				, control.getCodTipoSesionDefecto()
				, control.getCodSeccion()));
		
    	control.setCodIncidencia("");
    	control.setFechaIncidencia("");
    	control.setDscIncidencia("");
    	
		if ( control.getIndProcedencia().trim().equals("1") )
			request.setAttribute("IndProcedencia","1");
		else request.setAttribute("IndProcedencia","0");
		
		request.getSession().setAttribute("listaIncidenciasDoc", control.getListIncidencias());
    	
	    return new ModelAndView("/evaluaciones/cargaOperativa/eva_incidenciaregistrar_doc","control",control);		
    }
    
    private String agregarIncidencia(IncidenciaRegistrarDocCommand control)
    {
    	try
    	{
    		return this.operDocenteManager.insertIncidencia(
    				control.getCodIncidencia(), control.getCodCurso()
    				, control.getCodTipoSesionDefecto(), control.getCodSeccion(), control.getCodAlumno()
    				, control.getFechaIncidencia(), "", ""
    				, control.getDscIncidencia(), "", CommonConstants.INC_TIPO_OPE_DOC
    				, control.getCodEvaluador());
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String eliminarIncidencia(IncidenciaRegistrarDocCommand control)
    {
    	try
    	{
    		return this.operDocenteManager.deleteIncidencia(control.getCodIncidencia()
    														, control.getCodEvaluador());
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return null;
    }
}
