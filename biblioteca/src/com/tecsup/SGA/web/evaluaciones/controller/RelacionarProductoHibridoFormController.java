package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.modelo.CursosHibrido;
import com.tecsup.SGA.modelo.HibridoCurso;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.CursosHibridoManager;
import com.tecsup.SGA.service.evaluaciones.HibridoCursoManager;
import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.common.*;


public class RelacionarProductoHibridoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RelacionarProductoHibridoFormController.class);
	private SistevalHibridoManager sistevalHibridoManager;
	private CursosHibridoManager cursosHibridoManager;
	private HibridoCursoManager hibridoCursoManager;
	

	public void setHibridoCursoManager(HibridoCursoManager hibridoCursoManager) {
		this.hibridoCursoManager = hibridoCursoManager;
	}

	public CursosHibridoManager getCursosHibridoManager() {
		return cursosHibridoManager;
	}

	public void setCursosHibridoManager(CursosHibridoManager cursosHibridoManager) {
		this.cursosHibridoManager = cursosHibridoManager;
	}

	//private TablaDetalleManager tablaDetalleManager;
	public void setSistevalHibridoManager(SistevalHibridoManager sistevalHibridoManager) {
		this.sistevalHibridoManager = sistevalHibridoManager;
	}   
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	RelacionarProductoHibridoCommand command = new RelacionarProductoHibridoCommand();
    	
    	String codigo = request.getParameter("txhCodigo");
    	String descripcion = request.getParameter("txhDescripcion");
    	String codSisteval = request.getParameter("txhCodSisteval");
		//obteniendo los valores de la sesion
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));    	
    	//***********************************
    	String cadena = codSisteval+" - "+descripcion;    	
    	command.setListaCursosHibrido(this.cursosHibridoManager.getAllCursosHibrido());
    	command.setListaCursosHibridoSel(this.hibridoCursoManager.getHibridoCursoById(codigo));
    	/*command.setListaResultado(
    			fcRestaLista(command.getListaCursosHibrido(), command.getListaCursosHibridoSel())
    					);*/
    	command.setCodigo(codigo);
    	command.setDescripcion(descripcion);
    	//cadena para mostrar en el input de la pantalla se concatena
    	command.setCadena(cadena);
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
		String resultado = "";
		RelacionarProductoHibridoCommand control = (RelacionarProductoHibridoCommand) command;	
		log.info("OPERACION:" + control.getOperacion());
		
		if(control.getOperacion().trim().equals("GRABAR")){			
			resultado = InsertHibridoCurso(control);
			log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			control.setMsg("OK");
    		}
		}    	
			
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_relacionar_producto_hibrido","control",control);		
    }
    
    private String InsertHibridoCurso(RelacionarProductoHibridoCommand control){
       	try
    	{	HibridoCurso obj = new HibridoCurso();
    	
    		obj.setCodigo(control.getCodigo());
    		obj.setCadena(control.getTknCiclos());
    		obj.setNroRegistros(control.getNroRegistros());
    		
    		obj.setCodGenerado(this.hibridoCursoManager.UpdateHibridoCurso(obj, control.getCodEvaluador()));
    		return obj.getCodGenerado();
    		
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }

    /*private List fcRestaLista(List lstCiclos, List lstCiclosEmpresa) {		
		List lstResultado = null;
		if( lstCiclos!=null && lstCiclos.size()>0 ){	
			lstResultado = new ArrayList();
			CursosHibrido obj = null;
			for(int i=0;i<lstCiclos.size();i++)
			{	obj = (CursosHibrido)lstCiclos.get(i);				
				CursosHibrido obj2 = null;
				if(lstCiclosEmpresa !=null && lstCiclosEmpresa.size()>0)
				{	boolean flgEncontrado = false;					
					for(int j=0;j<lstCiclosEmpresa.size();j++)
					{
						obj2 = (CursosHibrido)lstCiclosEmpresa.get(j);
						int x=Integer.valueOf(obj.getCodCurso().trim());
						int y=Integer.valueOf(obj2.getCodCurso());
						if(x==y){							
							flgEncontrado = true;
						}
																	
					}
					if(!flgEncontrado)
						lstResultado.add(obj);
					
				}
				else
				{	lstResultado = lstCiclos;
				}
			}			
		}		
		return lstResultado;
	} */      
}
