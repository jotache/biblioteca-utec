package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.modelo.CursosHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.common.*;

public class MantenimientoCompetenciasFormController extends
		SimpleFormController {
	private static Log log = LogFactory
			.getLog(MantenimientoCompetenciasFormController.class);
	private TablaDetalleManager tablaDetalleManager;

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		
		MantenimientoCompetenciasCommand command = new MantenimientoCompetenciasCommand();
		//obteniendo los valores de la sesion
		command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
		command.setListAreasNivelUno(this.tablaDetalleManager
				.getAllTablaDetalle(CommonConstants.TIPT_COMPETENCIAS, "", "",
						"", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
		request.getSession().setAttribute("listaAreasIntUno",
				command.getListAreasNivelUno());
		int valorTope = obtenerValorTope(command.getListAreasNivelUno());
		command.setValorTope("" + valorTope);

		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	
	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		MantenimientoCompetenciasCommand control = (MantenimientoCompetenciasCommand) command;
		log.info("OPERACION:" + control.getOperacion());
		
		String resultado="";
		if (control.getOperacion().trim().equals("ELIMINAR")) {
			resultado=DeleteTablaDetalle(control);	
			log.info("RPTA:" + resultado);
			control.setOperacion("");
			control.setListAreasNivelUno(this.tablaDetalleManager
					.getAllTablaDetalle(CommonConstants.TIPT_COMPETENCIAS, "",
							"", "", "", "", "", "",
							CommonConstants.TIPO_ORDEN_DSC));
			int valorTope = obtenerValorTope(control.getListAreasNivelUno());
			control.setValorTope("" + valorTope);
			if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			if( resultado.equals("-2")){
    				control.setMsg("IGUAL");
    			}
    			else{
    				control.setMsg("OK");
    			}
    		}
		}
		control.setSeleccion("");
		int valorTope = obtenerValorTope(control.getListAreasNivelUno());
		control.setValorTope("" + valorTope);
		request.getSession().setAttribute("listaAreasIntUno",
				control.getListAreasNivelUno());
		
		return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_competencias", "control",control);
	}

	private String DeleteTablaDetalle(MantenimientoCompetenciasCommand control) {
		try {
			TipoTablaDetalle obj = new TipoTablaDetalle();

			obj.setCodTipoTabla(CommonConstants.TIPT_COMPETENCIAS);
			obj.setCadCod(control.getSeleccion());
			obj.setNroReg("1");
			control.setSeleccion("");

			if (!obj.getCadCod().trim().equals("")) {				
				obj.setCodTipoTablaDetalle(this.tablaDetalleManager.DeleteTablaDetalle(obj,control.getCodEvaluador()));
				return obj.getCodTipoTablaDetalle();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private int obtenerValorTope(List lstCompetencias) {
		int tope = 0;
		List lstResultado = null;
		if (lstCompetencias != null && lstCompetencias.size() > 0) {
			lstResultado = new ArrayList();
			TipoTablaDetalle tabla = null;
			for (int i = 0; i < lstCompetencias.size(); i++) {
				tabla = (TipoTablaDetalle) lstCompetencias.get(i);
				tope = tope + Integer.valueOf(tabla.getDscValor1());
			}
		}
		return tope;
	}
}
