package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.AgregarPesoCommand;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Parciales;

public class AgregarPesoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarPesoFormController.class);
    	
	OperDocenteManager operDocenteManager;
	
    public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
    public AgregarPesoFormController() {    	
        super();        
        setCommandClass(AgregarPesoCommand.class);        
    }


	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	   	
    	AgregarPesoCommand command = new AgregarPesoCommand();
    	String fla= "";
    	CursoEvaluador obj = new CursoEvaluador();
    	command.setCurso((String)request.getParameter("txhCodCurso"));
    	command.setCodCurso((String)request.getParameter("txhCodigo"));
    	command.setSeccion((String)request.getParameter("txhSec"));
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodId((String)request.getParameter("txhCodId"));
    	command.setDescripcion((String)request.getParameter("txhPeso"));
    	command.setCodEvaluador((String)request.getParameter("txhCod"));
    	command.setFlag((String)request.getParameter("txhFlagEliminar"));
    	command.setCodEval((String)request.getParameter("txhCodEval"));
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
    	obj.setCodCurso(command.getCurso());
    	
    	command.setListEvaluadores(this.operDocenteManager.GetAllEvaluadores(obj,command.getCodPeriodo()));
    	if(!(command.getCodPeriodo()==null)){
	    	if(!(command.getFlag()==null)){
	    		if(command.getFlag().equals("0")){
	    			command.setFlag("");
	    	  }
	    	 }
    	}
    	fla = command.getFlag();
    	
    	request.setAttribute("flag",fla);
        
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
		String resultado = "";
    	AgregarPesoCommand control = (AgregarPesoCommand) command;
    	CursoEvaluador obj = new CursoEvaluador();
    	obj.setCodCurso(control.getCurso());
    	log.info("OPERACION:" + control.getOperacion());
    	
    	control.setListEvaluadores(this.operDocenteManager.GetAllEvaluadores(obj,control.getCodPeriodo()));
    	
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		obj.setCodCurso(control.getCurso());
    		//JHPR 2008-06-30
        	control.setListEvaluadores(this.operDocenteManager.GetAllEvaluadores(obj,control.getCodPeriodo()));
    		resultado = InsertTablaDetalle(control);
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")) {
    			control.setMessage("ERROR");
    		}
    		else if ( resultado.equals("-2")) 
    		{
    			control.setMessage("DOBLE");
    		}
    		else control.setMessage("OK");
    	}
    	control.setFlag("");    	
	    return new ModelAndView("/evaluaciones/definirNumero/eva_agregar_peso","control",control);		
    }
        
        private String InsertTablaDetalle(AgregarPesoCommand control)
        {
        		Parciales obj = new Parciales();
        		if(control.getFlag().equals("")){
        			control.setFlag("0");
        		}
        	        		
        		obj.setCodPeriodo(control.getCodPeriodo());
        		obj.setCodId(control.getCodId());
        		obj.setCodCurso(control.getCurso());
        		obj.setCodPro(control.getCodProducto());
        		obj.setCodEspe(control.getCodEspecialidad());
        		obj.setSeccion(control.getSeccion());
        		obj.setCodTipoEvaluacion(control.getCodCurso());
        		obj.setPeso(control.getDescripcion());
        		obj.setCodEvaluador(control.getCodEvaluador());
        		obj.setFlag(control.getFlag());
        		
        		obj.setCodCursoId(this.operDocenteManager.InsertPeso(obj,control.getCodEval()));
        	return obj.getCodCursoId();
        	
        }
        
}
