package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.ControlAsistenciaCommand;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarAsistenciaCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.*;

public class RegistrarAsistenciaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RegistrarAsistenciaFormController.class);
	
	private OperDocenteManager operDocenteManager;

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	RegistrarAsistenciaCommand command = new RegistrarAsistenciaCommand();  	
    	   	
    	/*Recogiendo valores*/
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEval((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
    	command.setTipoSesion((String)request.getParameter("txhTipoSesion"));
    	command.setNomEval((String)request.getParameter("txhNomEval"));
    	command.setCodSeccion((String)request.getParameter("txhCodSeccion")); //codigo seccion
    	command.setSeccion((String)request.getParameter("txhSeccion")); //dsc seccion
    	
    	if(command.getCodPeriodo() != null && command.getCodEval() != null && 
    		command.getCodCurso() != null && command.getCodSeccion() != null){
    		
    		inicializaDataCurso(command, command.getCodPeriodo().trim(), command.getCodEval().trim(), 
    							command.getCodProducto(), command.getCodEspecialidad(), 
    							command.getCodCurso().trim());
    		
	    	inicializaDataAlumno(command, command.getCodCurso().trim(),
	    						command.getCodProducto(), command.getCodEspecialidad(), command.getTipoSesion(),
	    						command.getCodSeccion().trim(), "", "", 
	    						request);
	    	
	    	/*Busqueda de las fechas asistencia registradas*/
	    	inicializaDataFecha(command, command.getCodCurso().trim(), 
	    			command.getCodProducto().trim(), command.getCodEspecialidad().trim(), command.getTipoSesion().trim(),
	    			command.getCodSeccion().trim(), request);
		}

    	/*Fecha actual*/
    	command.setFechaActual(Fecha.getFechaActual());
    	request.setAttribute("fechaActual", command.getFechaActual().trim());
    	    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	RegistrarAsistenciaCommand control = (RegistrarAsistenciaCommand)command;  	
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if("GRABAR".equals(control.getOperacion().trim())){
    		String rpta = "";
    		
    		String [] strAsistencia = control.getCadena().split("_");
    		log.info("Cadena Principal: "+control.getCadena());
    		String cadena;
    		String valor1, valor2, valor3, valor4 , codseccion;
    		String sesionGrabacion = "";
    		String fechaGrabacion = "";
    		int i;
    		
    		/**/
    		String save = control.getSaveFecha().trim();
    		log.info("Programada: "+save);
    		if ("1".equals(save)){
    			control.setFlgProg("1"); //Programada
    		}
    		else
    			control.setFlgProg("0"); //Ya tomada
    		/**/
    		
			for (i = 0 ; i < strAsistencia.length ; i++){
    			cadena = strAsistencia[i];
    			log.info("Cadena: "+cadena);
    			String [] stkCadena = cadena.split("\\$");
    			valor1 = stkCadena[0];
				valor2 = stkCadena[1];
				valor3 = stkCadena[2];
				valor4 = stkCadena[3];
				codseccion = stkCadena[4];
				log.info("Alumno:"+valor1 + " NrSession:"+valor3);
				sesionGrabacion = valor3;
				fechaGrabacion = valor4;
				
				rpta = insertarAsistencia(control.getCodPeriodo(), "", valor1.trim(), control.getCodCurso().trim(), control.getTipoSesion().trim(),
											codseccion, valor3.trim(), valor4.trim(), 
											valor2.trim(), control.getCodEval().trim()
											, control.getFlgProg()
											);
				log.info("RPTA:" + rpta);
				if ("-1".equals(rpta)){
	        		control.setMessage(CommonMessage.GRABAR_ERROR);
	        		control.setTypeMessage("ERROR");
	        		break;
	        	}
    		}
    		
    		if ("0".equals(rpta)){
    			searchAlumnosByAsistencia(control, "", control.getNombre().trim(), control.getApellido().trim(), 
    									sesionGrabacion.trim(), control.getCodCurso().trim(), 
    									control.getCodProducto(), control.getCodEspecialidad(), control.getTipoSesion(),
    									control.getCodSeccion().trim(), request);
    			
    			control.setNroSesionConsulta(sesionGrabacion.trim());
    			control.setNroSesion(sesionGrabacion.trim());
    			control.setFe(fechaGrabacion.trim());
    			
        		control.setMessage(CommonMessage.GRABAR_EXITO);
        		control.setTypeMessage("OK");
        	}
    	}
    	else if ("BUSCAR_ALUM".equals(control.getOperacion().trim())){
    		inicializaDataAlumno(control, control.getCodCurso().trim(),
    				control.getCodProducto().trim(), control.getCodEspecialidad().trim(), control.getTipoSesion().trim(),
    				control.getCodSeccion().trim(), control.getNombre().trim(), control.getApellido().trim(), request);
    	}
    	else if ("BUSCAR_ASIST".equals(control.getOperacion().trim())){
    		
    		searchAlumnosByAsistencia(control, "", control.getNombre().trim(), control.getApellido().trim(), 
    								control.getNroSesion().trim(), control.getCodCurso().trim(), 
    								control.getCodProducto(), control.getCodEspecialidad(), control.getTipoSesion(),
    								control.getCodSeccion().trim(), request);
    		
    		control.setNroSesionConsulta(control.getNroSesion());
    	}
    	else if("ELIMINAR".equals(control.getOperacion())){
    		String rpta = "";
    		log.info("Eliminando session: "+control.getNroSesionEliminar());
    		rpta = deleteAsistencia(control.getCodCurso(), control.getTipoSesion(), control.getCodSeccion(),
    								control.getNroSesionEliminar(), control.getFechaEliminar());
    		log.info("RPTA:" + rpta);
    		if ("-1".equals(rpta)){
        		control.setMessage(CommonMessage.GRABAR_ERROR);
        		control.setTypeMessage("ERROR");
        	}
    		else if ("0".equals(rpta)){
        		control.setMessage(CommonMessage.ELIMINAR_EXITO);
        		control.setTypeMessage("OK");
        	}
    		
    		inicializaDataAlumno(control, control.getCodCurso().trim(),
    				control.getCodProducto(),control.getCodEspecialidad(),control.getTipoSesion(),
    				control.getCodSeccion().trim(),"","",request);
    		
    	}
    	
    	inicializaDataFecha(control, control.getCodCurso().trim(),control.getCodProducto().trim(), 
    			control.getCodEspecialidad().trim(),control.getTipoSesion().trim(),control.getCodSeccion().trim(),request);
    	
    	control.setNroSesionEliminar("");
    	control.setFechaEliminar("");
    	control.setSaveFecha("");
    			
	    return new ModelAndView("/evaluaciones/cargaOperativa/eva_carga_registrar_asistencia","control",control);		
    }
    
    /*SETTER*/
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
    
    /*Metodos*/
	private void inicializaDataFecha(RegistrarAsistenciaCommand command, String codCurso,
									String codProducto, String codEspecialidad, String tipoSesion, 
									String codSeccion, HttpServletRequest request){
		
		command.setListaFechas(this.operDocenteManager.getAllFechaAsistenciaCurso(command.getCodPeriodo(), 
								codCurso, codProducto, codEspecialidad, tipoSesion, codSeccion));
		log.info("Total de Fechas: "+command.getListaFechas().size());
		for (Asistencia asis : (List<Asistencia>)command.getListaFechas()) {
			log.info("Fechas: "+asis.getNroSesion()+" - "+asis.getFecha());
		}
		
		request.setAttribute("listaFechas", command.getListaFechas());
	}
	
	
	private void inicializaDataCurso(RegistrarAsistenciaCommand command, String strPeriodo, String strEval,
											String codProducto, String codEspecialidad, String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval,codProducto,codEspecialidad, strCodCurso
								, "", "");
		
    	if (dataCurso.size()>0){
    		lObject = (CursoEvaluador) dataCurso.get(0);
    		command.setCurso(lObject.getCurso());
    		command.setSistEval(lObject.getSistEval());
    	}
	}
	
	private void inicializaDataAlumno(RegistrarAsistenciaCommand command, String codCurso,
						String codProducto, String codEspecialidad, String tipoSesion, 
						String codSeccion, String strNombre, String strApellido, HttpServletRequest request){
		
		command.setListaAlumnos(this.operDocenteManager.getAllAlumnosCurso(command.getCodPeriodo(), codCurso, 
								codProducto, codEspecialidad, tipoSesion, 
								codSeccion, strNombre, strApellido));
		
		request.setAttribute("listIndex", command.getListaAlumnos().size());
		//Llenado Tabla Interes - Session
    	request.setAttribute("listaAsistenciaCurso", command.getListaAlumnos());
	}
	
	private String insertarAsistencia(String codPeriodo, String codAsistencia, String codAlumno, String codCurso, String tipoSesion, 
									String codSeccion, String nroSesion, String fecha, String tipoAsist, String usuario, String flagProg){
	
		return this.operDocenteManager.insertAsistencia(codPeriodo, codAsistencia, codAlumno, codCurso, tipoSesion, codSeccion, 
														nroSesion, fecha, tipoAsist, usuario, flagProg);
	}
    
	private void searchAlumnosByAsistencia(RegistrarAsistenciaCommand command, String codAsistencia, String nombre, 
											String apellido, String nroSesion, String codCurso, 
											String codProducto, String codEspecialidad, String tipoSesion, 
											String codSeccion, HttpServletRequest request){
		
		command.setListaAlumnos(this.operDocenteManager.getAllAlumnosXasistencia(command.getCodPeriodo(), 
								codAsistencia, nombre, apellido, nroSesion, codCurso, codProducto,
								codEspecialidad, tipoSesion, codSeccion));
		
		request.setAttribute("listaAsistenciaCurso", command.getListaAlumnos());
		
		if(command.getListaAlumnos().size() == 0){
			command.setTypeMessage("REG");
		}
	}
	
	private String deleteAsistencia(String codCurso, String tipoSesion, String codSeccion, 
									String nroSesion, String fecha){

		return this.operDocenteManager.deleteAsistencia(codCurso, tipoSesion, codSeccion, nroSesion, fecha);
}
	
}
