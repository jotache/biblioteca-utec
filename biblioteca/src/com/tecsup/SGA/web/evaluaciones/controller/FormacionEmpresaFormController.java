package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.service.evaluaciones.MtoConfiguracionComponentesManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.DetallePracticasCommand;
import com.tecsup.SGA.web.evaluaciones.command.FormacionEmpresaCommand;
import com.tecsup.SGA.web.evaluaciones.controller.*;
import com.tecsup.SGA.modelo.ConsultaEmpresa;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.FormacionEmpresa;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.common.CommonConstants;

public class FormacionEmpresaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(FormacionEmpresaFormController.class);
	
	MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager;
	OperDocenteManager operDocenteManager;
	ComunManager comunManager;
	
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public void setMtoConfiguracionComponentesManager(
			MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager) {
		this.mtoConfiguracionComponentesManager = mtoConfiguracionComponentesManager;
	}

	public FormacionEmpresaFormController() {    	
        super();        
        setCommandClass(FormacionEmpresaCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	    	
    	FormacionEmpresaCommand command = new FormacionEmpresaCommand();
    	
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEva((String)request.getParameter("txhCodEvaluador"));
    	
    	Producto obj = new Producto();
    	obj.setCodProducto(CommonConstants.COD_FORMACION_EMPRESA2);
    	command.setListEva(this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(obj));
    	request.getSession().setAttribute("listEva", command.getListEva());
    	
    	periodo(command);
    	producto(command);
    	
    	command.setNombre("");
    	command.setApellidos("");
    	    	
    	command.setTamanioLista("0");
    	
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	FormacionEmpresaCommand control = (FormacionEmpresaCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{
    		control.setListAlum(this.operDocenteManager.GelAllFormacionEmpresa(control.getCodPeriodo()
    				, CommonConstants.COD_FORMACION_EMPRESA2, control.getCodEspe(), control.getNombre(), control.getApellidos()));
    		request.getSession().setAttribute("ListAlum", control.getListAlum());
    		control.setTamanioLista(String.valueOf(control.getListAlum().size()));
    	}
		
	    return new ModelAndView("/evaluaciones/formacionEmpresa/eva_formacion_empresa","control",control);		
    }
	
        private String periodo(FormacionEmpresaCommand command)
        {
        	
        	Periodo periodo;
        	ArrayList<Periodo> listPeriodo;
    		listPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoById(command.getCodPeriodo());
    		if ( listPeriodo != null )
    			if ( listPeriodo.size() > 0)
    			{
    				periodo =listPeriodo.get(0);
    				command.setPeriodo(periodo.getNombre());
    			}
        	return command.getPeriodo() + command.getCodPeriodo();
        }
        
        private String producto(FormacionEmpresaCommand command)
        {
        	FormacionEmpresa fobj = new FormacionEmpresa();
        	fobj.setCodPeriodo(command.getCodPeriodo());
        	fobj.setCodCurso(CommonConstants.COD_FORMACION_EMPRESA2);
        	
        	/*MODIFICAR PARA QUE RECIBA PERIODO*/
        	Producto pobj = this.operDocenteManager.ProductoxCurso(fobj);
        	if(!(pobj==null)){
        		command.setProducto(pobj.getDescripcion());
        	}
        	else{
        		command.setProducto("");
        	}
        	return command.getProducto();
        }
        
}
