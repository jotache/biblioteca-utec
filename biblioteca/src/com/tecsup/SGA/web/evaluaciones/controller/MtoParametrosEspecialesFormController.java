package com.tecsup.SGA.web.evaluaciones.controller;


import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.ParametrosEspecialesCommand;


public class MtoParametrosEspecialesFormController extends SimpleFormController {
	private static Log log = LogFactory
			.getLog(MtoParametrosEspecialesFormController.class);
	
	private MantenimientoManager mantenimientoManager;
	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	//private TablaDetalleManager tablaDetalleManager;
	
    public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
        this.mantenimientoManager = mantenimientoManager;
    }

	public MtoParametrosEspecialesFormController(){
		super();
		setCommandClass(ParametrosEspecialesCommand.class);
	}
	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		
			ParametrosEspecialesCommand command = new ParametrosEspecialesCommand();
			command.setLstPeriodo(mtoParametrosGeneralesManager.getPeriodos() );
			if(command.getOperacion()==null)
			{	
				command.setCodPeriodo(request.getParameter("txhCodPeriodo"));
				command.setCodEvaluador(request.getParameter("txhCodEvaluador"));
				
			}	
		
		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	
	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors){
		
		ParametrosEspecialesCommand control = (ParametrosEspecialesCommand) command;
		
		//SESSION USUARIO
		log.info("OPERACION:"+control.getOperacion());
		//cambio temporal
		control.setUsuario(control.getCodEvaluador());
		
		if (control.getOperacion().equals("irRegistrar")){			
			control = irRegistrar(request, response, errors, control);
			control = irConsultarPeriodo(request, response, errors, control);
		}else if(control.getOperacion().equals("irActualizar")){
			control = irActualizar(request, response, errors, control);
			control = irConsultarPeriodo(request, response, errors, control);
		}else if(control.getOperacion().equals("irConsultarPeriodo")){
			control = irConsultarPeriodo(request, response, errors, control);
			//control.setOperacion("irActualizar");
		}
		
		control.setTknCiclos("");

		
		return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_parametros_especiales","control", control);
	}

	
	private ParametrosEspecialesCommand irRegistrar(HttpServletRequest request,
			HttpServletResponse response, BindException errors,
			ParametrosEspecialesCommand control) {
					
			try 
			{	String rpta = this.mantenimientoManager.insertParametrosEspeciales(control);
				if(rpta.equalsIgnoreCase("0"))
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
				else
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);				

			}catch (Exception e) 
			{
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR+CommonMessage.CONSULTE_ADMIN);
				e.printStackTrace();
			}
		
		return control;
	}
	
	private ParametrosEspecialesCommand irActualizar(HttpServletRequest request,
			HttpServletResponse response, BindException errors,
			ParametrosEspecialesCommand control) {
		
		
			try 
			{	String rpta = this.mantenimientoManager.updateParametrosEspeciales(control);
				if(rpta.equalsIgnoreCase("0"))
					request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
				else
					request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
				
			}catch (Exception e) 
			{
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR+CommonMessage.CONSULTE_ADMIN);
				e.printStackTrace();
				
				
			}
		
		return control;
	}
	
	private ParametrosEspecialesCommand irConsultarPeriodo(HttpServletRequest request,
			HttpServletResponse response, BindException errors,
			ParametrosEspecialesCommand command) {
		
			try 
			{
				command.setLstCiclos(mantenimientoManager.getLstCilos(command.getVarperiodo()));
				command = mantenimientoManager.setParametrosEspeciales(command);
				
			}catch (Exception e) 
			{
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR+CommonMessage.CONSULTE_ADMIN);
				e.printStackTrace();
			}
		
		return command;
	}

	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}

}

