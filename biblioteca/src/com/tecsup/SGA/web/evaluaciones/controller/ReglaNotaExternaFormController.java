package com.tecsup.SGA.web.evaluaciones.controller;
import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.CursoConfigManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.CursoConfig;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class ReglaNotaExternaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ReglaNotaExternaFormController.class);
	CursoConfigManager cursoConfigManager;
	
	
	public void setCursoConfigManager(CursoConfigManager cursoConfigManager) {
		this.cursoConfigManager = cursoConfigManager;
	}	
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	ReglaNotaExternaCommand command = new ReglaNotaExternaCommand();
    	//******************************************************************
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	
    	//******************************************************************
    	String codCurso = (String)request.getParameter("val");
    	command.setCodCurso(codCurso);    	
    	String modificar = (String)request.getParameter("operacion");    	    	
    	if ( modificar != null ){    		
    		if ( !modificar.trim().equals("") ){    			
    			cargaDetalle(command, codCurso);
    			command.setAux("MODIFICAR");
    		}
    	}
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String resultado = "";
    	ReglaNotaExternaCommand control = (ReglaNotaExternaCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{	resultado = InsertReglaNotaExterna(control);  
    		log.info("RPTA:" + resultado);
    		if ( resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			control.setMsg("OK");
    		}
    	}
    	else{
    		if (control.getOperacion().trim().equals("MODIFICAR")){   		
        		resultado = UpdateReglaNotaExterna(control); 
        		log.info("RPTA:" + resultado);
        		if ( resultado.equals("-1")){
        			control.setMsg("ERROR");
        		}
        		else{
        			control.setMsg("OK");
        		}
        	} 		
    	}    	
				
	    return new ModelAndView("/evaluaciones/mantenimiento/eva_mto_regla_nota_externa","control",control);		
    }
    private String InsertReglaNotaExterna(ReglaNotaExternaCommand control)
    {
    	try
    	{	CursoConfig obj = new CursoConfig();

    		obj.setCodCurso(control.getCodCurso());    		
    		obj.setRegla(control.getRegla());
    		obj.setCondicion(control.getCondicion());
    		obj.setVerdadero01(control.getVerdadero01());
    		obj.setVerdadero02(control.getVerdadero02());
    		obj.setVerdadero03(control.getVerdadero03());
    		obj.setVerdadero04(control.getVerdadero04());
    		obj.setFalso01(control.getFalso01());
    		obj.setFalso02(control.getFalso02());
    		obj.setFalso03(control.getFalso03());
    		obj.setFalso04(control.getFalso04());
    		obj.setOpeVer01(control.getOpeVer01());
    		obj.setOpeVer02(control.getOpeVer02());
    		obj.setOpeVer03(control.getOpeVer03());
    		obj.setOpeVer04(control.getOpeVer04());
    		obj.setOpeFal01(control.getOpeFal01());
    		obj.setOpeFal02(control.getOpeFal02());
    		obj.setOpeFal03(control.getOpeFal03());
    		obj.setOpeFal04(control.getOpeFal04());
    		
    		obj.setResultado(this.cursoConfigManager.InsertCursoConfig(obj, control.getCodEvaluador()));								
						
			return obj.getResultado();  		
    		
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }    
    
    private String UpdateReglaNotaExterna(ReglaNotaExternaCommand control){
    	try
    	{	CursoConfig obj = new CursoConfig();

    		obj.setCodCurso(control.getCodCurso());    		
    		obj.setRegla(control.getRegla());
    		obj.setCondicion(control.getCondicion());
    		obj.setVerdadero01(control.getVerdadero01());
    		obj.setVerdadero02(control.getVerdadero02());
    		obj.setVerdadero03(control.getVerdadero03());
    		obj.setVerdadero04(control.getVerdadero04());
    		obj.setFalso01(control.getFalso01());
    		obj.setFalso02(control.getFalso02());
    		obj.setFalso03(control.getFalso03());
    		obj.setFalso04(control.getFalso04());
    		obj.setOpeVer01(control.getOpeVer01());
    		obj.setOpeVer02(control.getOpeVer02());
    		obj.setOpeVer03(control.getOpeVer03());
    		obj.setOpeVer04(control.getOpeVer04());
    		obj.setOpeFal01(control.getOpeFal01());
    		obj.setOpeFal02(control.getOpeFal02());
    		obj.setOpeFal03(control.getOpeFal03());
    		obj.setOpeFal04(control.getOpeFal04());    		
    		
    		obj.setResultado(this.cursoConfigManager.UpdateCursoConfig(obj, control.getCodEvaluador()));								
						
			return obj.getResultado();  		
    		
       	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	return null;
    }
    
    private void cargaDetalle(ReglaNotaExternaCommand command, String codDetalle)
    {	
    	try
    	{
	    	CursoConfig obj = new CursoConfig();    	    	
	    	
	    	if(codDetalle != null){
				obj = (CursoConfig)this.cursoConfigManager.getCursoConfigById(codDetalle).get(0);
								
				command.setRegla(obj.getRegla().trim());
				command.setCondicion(obj.getCondicion());
				command.setVerdadero01(obj.getVerdadero01());
				command.setVerdadero02(obj.getVerdadero02());
				command.setVerdadero03(obj.getVerdadero03());
				command.setVerdadero04(obj.getVerdadero04());		
				command.setFalso01(obj.getFalso01());
				command.setFalso02(obj.getFalso02());
				command.setFalso03(obj.getFalso03());
				command.setFalso04(obj.getFalso04());
				command.setOpeVer01(obj.getOpeVer01().trim());
				command.setOpeVer02(obj.getOpeVer02().trim());
				command.setOpeVer03(obj.getOpeVer03().trim());
				command.setOpeVer04(obj.getOpeVer04().trim());
				command.setOpeFal01(obj.getOpeFal01().trim());
				command.setOpeFal02(obj.getOpeFal02().trim());
				command.setOpeFal03(obj.getOpeFal03().trim());
				command.setOpeFal04(obj.getOpeFal04().trim());
    		}
    	}
    	catch(Exception ex)
    	{ ex.printStackTrace();}
    	
	}      
}
