package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.ControlAsistenciaCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.*;

public class ControlAsistenciaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ControlAsistenciaFormController.class);
	
	private OperDocenteManager operDocenteManager;

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	//log.info("formBackingObject:INI");
    	ControlAsistenciaCommand command = new ControlAsistenciaCommand();
    	   	
    	command.setTipoUsuarioBandeja((String)request.getParameter("txhTipoUsuarioBandeja"));
    	command.setCodUsuarioBandeja((String)request.getParameter("txhCodUsuarioBandeja"));
    	
    	/*Recogiendo valores*/
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEval((String)request.getParameter("txhCodEvaluador"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));
    	
    	command.setTipoSesion((String)request.getParameter("txhTipoSesion"));
    	
    	if(command.getCodPeriodo() != null && command.getCodEval() != null && command.getCodCurso() != null
    			&& command.getCodProducto() != null && command.getCodEspecialidad() != null){

    		inicializaTipoSesion(command, command.getCodCurso());
    		inicializaSeccion(command, "", command.getCodCurso());
	    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEval(), command.getCodProducto(),
	    						command.getCodCurso());
	    	command.setSeccion(command.getPrimeraSeccion()); //al comienzo siempre q busque la primera secci�n
	    	inicializaDataAlumno(command, command.getCodCurso(), 
	    			command.getCodProducto(), command.getCodEspecialidad(), "",
	    			command.getSeccion(), "", "");
	    	//Llenado Tabla Interes - Session
	    	request.getSession().setAttribute("listaAlumnos", command.getListaAlumnos());
		}
    	
    	if (command.getTipoSesion() != null) {
    		inicializaTipoSesion(command, command.getCodCurso());
    		inicializaSeccion(command, command.getTipoSesion(), command.getCodCurso());
    		command.setTipoSesion(command.getTipoSesion());
    		command.setSeccion("");
    	}
    	else {
    		command.setTipoSesion(""); 
    	}
    		
    	//log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	//log.info("onSubmit:INI");
    	ControlAsistenciaCommand control = (ControlAsistenciaCommand)command;
    	
    	log.info("OPERACION: " + control.getOperacion());
    	
    	if ("SESION".equals(control.getOperacion())){
    		inicializaTipoSesion(control, control.getCodCurso());
    		inicializaSeccion(control, control.getTipoSesion(), control.getCodCurso());
    		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), 
					control.getCodProducto(), control.getCodCurso());
    		
    		control.setTipoSesion(control.getTipoSesion());
    		control.setSeccion(control.getSeccion());
    		control.setFlagCerrarAsistencia(verEstadoRegistro(control.getCodCurso(),control.getSeccion(), control.getTipoSesion()));
    	}
    	else if ("BUSCAR".equals(control.getOperacion())){
    		inicializaTipoSesion(control, control.getCodCurso());
    		inicializaSeccion(control, control.getTipoSesion(), control.getCodCurso());
    		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), 
    							control.getCodProducto(), control.getCodCurso());
    		
    		inicializaDataAlumno(control, control.getCodCurso(), control.getCodProducto(), 
    							control.getCodEspecialidad(), control.getTipoSesion(),
    							control.getSeccion(), control.getNombre(), control.getApellido());
    		
    		control.setTipoSesion(control.getTipoSesion());
    		control.setSeccion(control.getSeccion());
    		
    		//Llenado Tabla Interes - Session
        	request.getSession().setAttribute("listaAlumnos", control.getListaAlumnos());
        	
        	control.setFlagCerrarAsistencia(verEstadoRegistro(control.getCodCurso(),control.getSeccion(), control.getTipoSesion()));
			
    	}else if ("CERRAR".equals(control.getOperacion())){    		    		
    		
    		String cierre = this.operDocenteManager.cerrarRegistroAsistencia(
    					control.getCodCurso(), control.getSeccion(), control.getTipoSesion(), control.getCodEval(), CommonConstants.ESTADO_REGISTRO_NOTAS_CERRADO);	
    		log.info("RPTA: " + cierre);
    		control.setFlagCerrarAsistencia("0");
    		if (cierre.equals("0")){
    			request.setAttribute("mensaje", "Cierre de Asistencia Satisfactorio.");
    			control.setFlagCerrarAsistencia("1");
    		}else if (cierre.equals("-1"))
    			request.setAttribute("mensaje", "No puede Cerrar por que no tiene Sesiones Registradas.");
    		else if (cierre.equals("-2"))
    			request.setAttribute("mensaje", "No puede Cerrar por que No ha pasado Asistencia a todas la Sesiones Creadas.");
    		else if (cierre.equals("-3"))
    			request.setAttribute("mensaje", "Problemas al Cerrar Registro de Asistencia. Consulte con el Administrador");
    		
    		    		
    		inicializaTipoSesion(control, control.getCodCurso());
    		inicializaSeccion(control, control.getTipoSesion(), control.getCodCurso());
    		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), 
    							control.getCodProducto(), control.getCodCurso());
    		
    	}else if ("VER_ESTADO_REGISTRO".equals(control.getOperacion())){
    		    	System.out.println("Seccion Seleccionada:"+  control.getSeccion());
    		control.setFlagCerrarAsistencia(verEstadoRegistro(control.getCodCurso(),control.getSeccion(), control.getTipoSesion()));
    		
    		inicializaTipoSesion(control, control.getCodCurso());
    		inicializaSeccion(control, control.getTipoSesion(), control.getCodCurso());
    		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), 
    							control.getCodProducto(), control.getCodCurso());
    		
    	}else if("ABRIR_REG".equals(control.getOperacion())){
    		String abrir = this.operDocenteManager.abrirRegistroAsistencia(control.getCodCurso(),
    					control.getSeccion(), control.getTipoSesion(), control.getCodUsuarioBandeja());
    		log.info("RPTA: " + abrir);
    		if (abrir.equals("0")){
    			control.setFlagCerrarAsistencia("0");
    			request.setAttribute("mensaje", "Registro de Asistencia Abierto Satisfactoriamente.");		
    		}else if (abrir.equals("-3"))
    			request.setAttribute("mensaje", "Problemas al Abrir Registro de Asistencia. Consulte con el Administrador");
    		
    		inicializaTipoSesion(control, control.getCodCurso());
    		inicializaSeccion(control, control.getTipoSesion(), control.getCodCurso());
    		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), 
    							control.getCodProducto(), control.getCodCurso());
    	}
    	
		//log.info("onSubmit:FIN");
	    return new ModelAndView("/evaluaciones/cargaOperativa/eva_carga_control_asistencia","control",control);		
    }
    
    private String verEstadoRegistro(String codCurso,String codSeccion, String codTipoEvaluacion){
    	
    	if (codSeccion.equals(""))
    		return "0";
    	
    	List<Parciales> lista = this.operDocenteManager	   
    				.obtenerDefinicionesParciales(codCurso, codSeccion, codTipoEvaluacion, "", "A");
    	String estado="";
		int contador=0;
		//contador 0 = el registo de asistencia esta abierto.
		//contador >0 = el registro de asistencia esta cerrado.
		for (Parciales evaluacion : lista) {
			if (evaluacion.getFlagCerrarAsistencia()!=null){
				if (evaluacion.getFlagCerrarAsistencia().equals("1")) 
					contador ++;
			}
		}
		
		if (contador==0)
			estado="0";
		else
			estado="1";
		
		return estado;
    }
    
    /*SETTER*/
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
    
    /*Metodos*/
	 private void inicializaTipoSesion(ControlAsistenciaCommand command, String strCodCurso){
	    	CursoEvaluador lObject;
	    	
	    	command.setListaTipoSesion(this.operDocenteManager.getAllSesionXCurso(strCodCurso));
	    }
	
    private void inicializaSeccion(ControlAsistenciaCommand command, String tipoSesion, String strCodCurso){
    	CursoEvaluador lObject;
    	
    	//command.setListaSeccion(this.operDocenteManager.getAllSeccionCurso(tipoSesion, strCodCurso));
    	command.setListaSeccion(this.operDocenteManager.GetAllSeccionesxCursoxResp(command.getCodEval()
    						, strCodCurso, tipoSesion));	
    	ArrayList firstSeccion = (ArrayList)this.operDocenteManager.getAllSeccionCurso(tipoSesion, strCodCurso);
    	
    	if (firstSeccion.size() > 0){
    		lObject = (CursoEvaluador) firstSeccion.get(0);
    		command.setPrimeraSeccion(lObject.getCodSeccion());
    	}
    	else
    		command.setPrimeraSeccion("");
    }
    
	private void inicializaDataCurso(ControlAsistenciaCommand command, String strPeriodo, String strEval,
									String codProducto, String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval, codProducto, 
																"", strCodCurso
																,"", "");
		
    	if (dataCurso.size()>0){
    		lObject = (CursoEvaluador) dataCurso.get(0);
    		command.setPeriodo(lObject.getPeriodo());
    		command.setEvaluador(lObject.getEvaluador());
    		command.setPrograma(lObject.getProducto());
    		command.setCiclo(lObject.getCiclo());
    		command.setCurso(lObject.getCurso());
    		command.setEspecialidad(lObject.getEspecialidad());
    		command.setSistEval(lObject.getSistEval());
    	}
	}
	
	private void inicializaDataAlumno(ControlAsistenciaCommand command, String codCurso,
									String codProducto, String codEspecialidad, String tipoSesion,
									String codSeccion, String strNombre, String strApellido){
		command.setListaAlumnos(this.operDocenteManager.getAllAlumnosCurso(command.getCodPeriodo(), codCurso,
								codProducto, codEspecialidad, tipoSesion, codSeccion, strNombre, strApellido));
	}
    
}
