package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.evaluaciones.PerfilAlumnoManager;
import com.tecsup.SGA.web.evaluaciones.command.PerfilAlumnoBandejaCommand;
import com.tecsup.SGA.service.evaluaciones.ComunManager;

public class PerfilAlumnoBandejaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(PerfilAlumnoBandejaFormController.class);
	
	private TablaDetalleManager tablaDetalleManager;
	private PerfilAlumnoManager perfilAlumnoManager;
	private ComunManager comunManager;

	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}   
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}	
	
	public void setPerfilAlumnoManager(PerfilAlumnoManager perfilAlumnoManager) {
		this.perfilAlumnoManager = perfilAlumnoManager;
	}


	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	Periodo periodo;
    	ArrayList<Periodo> listPeriodo;
    	PerfilAlumnoBandejaCommand command = new PerfilAlumnoBandejaCommand();
    	/*Recogiendo valores - Codigos*/
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	/*Recogiendo valores*/    	
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEvaluador"));
    	
    	//Llenamos la descripcion del periodo
    	if (command.getCodPeriodo() != null)
    	{
    		listPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoById(command.getCodPeriodo());
    		if ( listPeriodo != null )
    			if ( listPeriodo.size() > 0)
    			{
    				periodo =listPeriodo.get(0);
    				command.setDscPeriodo(periodo.getNombre());
    			}
    	}
    	//Llenando información
    	if ( command.getCodEvaluador() != null )
    	{
    		//Nombre de evaluador.
    		command.setDscEvaluador(usuarioSeguridad.getNomUsuario());    		    		    		
	    	//Lleno lista de competecnias.
	    	llenaListaPerfil(command);
	    	//LLeno la lista de alumnos	    	
	    	buscarAlumnos(command);	    	
    	}
    	request.getSession().setAttribute("listaAlumnoComp", command.getListaBandeja());
    	    	
        return command;
    } 
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    	throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	
    	PerfilAlumnoBandejaCommand control = (PerfilAlumnoBandejaCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	if ( control.getOperacion().trim().equals("BUSCAR"))
    	{
    		buscarAlumnos(control);
    	}    	
    	//Lleno lista de competecnias.
    	llenaListaPerfil(control);
    	request.getSession().setAttribute("listaAlumnoComp", control.getListaBandeja());
		
	    return new ModelAndView("/evaluaciones/perfilAlumno/per_perfilalumno_bandeja","control",control);		
    }
    
    private void llenaListaPerfil(PerfilAlumnoBandejaCommand control)
    {
    	control.setListaCabecera(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PERFIL
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	control.setAnchoColumna(Double.toString(75 / (control.getListaCabecera().size() + 1 )) + "%");
    }
    
    private void buscarAlumnos(PerfilAlumnoBandejaCommand command)
    {
    	command.setListaBandeja(
    			this.perfilAlumnoManager.getBandejaTutor(command.getCodPeriodo(), 
    			command.getCodEvaluador(), command.getNombre(), command.getApellido())
    			);
    }
}
