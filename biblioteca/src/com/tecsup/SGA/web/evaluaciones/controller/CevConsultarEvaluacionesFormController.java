package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonSeguridad;
import com.tecsup.SGA.service.evaluaciones.ConsultasReportesManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.web.evaluaciones.command.ConsultarEvaluacionesCommand;


public class CevConsultarEvaluacionesFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(CevConsultarEvaluacionesFormController.class);

	private MantenimientoManager mantenimientoManager;
	private MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	private ConsultasReportesManager consultasReportesManager;
	
	public void setConsultasReportesManager(
			ConsultasReportesManager consultasReportesManager){
		this.consultasReportesManager = consultasReportesManager;
	}	
	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}

	public void setMantenimientoManager(
			MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}

	public CevConsultarEvaluacionesFormController() {
		super();
		setCommandClass(ConsultarEvaluacionesCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException{
		//log.info("formBackingObject");
		ConsultarEvaluacionesCommand command = new ConsultarEvaluacionesCommand();
		
		//PRIMERA VISITA
		if(command.getOperacion()==null)
		{	
			command.setCodPeriodo(request.getParameter("txhCodPeriodo"));
			command.setCodEvaluador(request.getParameter("txhCodEvaluador"));			
			command.setCodAlumno(request.getParameter("codAlu"));
			/*Variables de tutor*/
			command.setIndProcedencia(request.getParameter("txhIndProcTutor"));
			command.setCodPeriodoTutor(request.getParameter("txhCodPeriodoTutor"));
			command = setCabecera(command);
			
			command.setFlgBuscarAlumnos("0");	
			boolean esEstudiante = false;
			UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
			List<UsuarioPerfil> listaRoles = usuarioSeguridad.getRoles();
			if (listaRoles!=null){
				for (UsuarioPerfil up : listaRoles){
					if (up.getNombrePerfil().trim().equals(CommonSeguridad.EVA_PERFIL_ESTUDIANTE)){
						//command.setFlgBuscarAlumnos("0");
						esEstudiante = true;
					}
				}					
			}
			
			if(esEstudiante)
				command.setFlgBuscarAlumnos("0");
			else
				command.setFlgBuscarAlumnos("1");
				
			//***********************************************
			
			if(request.getParameter("irRegresar")!=null)
			{
				command.setCboProducto(request.getParameter("codProducto"));
				//command.setCboCiclo(request.getParameter("codCiclo"));
				command.setCboPeriodo(request.getParameter("codCboPeriodo"));
				command.setCodAlumno(request.getParameter("codAlu"));
				command = irCambiaProducto(command);
				
				command = consultasReportesManager.getAllInformacionGeneral(command);
			}
			
			if ( command.getIndProcedencia() == null ) command.setIndProcedencia(""); 
			if ( command.getIndProcedencia().trim().equals("1") )
				request.setAttribute("IndProcedencia","1");
			else request.setAttribute("IndProcedencia","0");

		}	
		
		

		command.setLstProductos(this.consultasReportesManager.getProductosxAlumno(command.getCodPeriodo(),command.getCodAlumno()));		
		
		//log.info("formBackingObject:FIN");
		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));
	}

	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors) {
		//log.info("onSubmit:INI");
		ConsultarEvaluacionesCommand control = (ConsultarEvaluacionesCommand) command;

		log.info("OPERACION:" + control.getOperacion());
		control.setUsuario(control.getCodEvaluador());
		
		if(control.getOperacion().equals("irCambiaProducto")){
			control = consultasReportesManager.setConsultaDetalle(control);
			control = irCambiaProducto(control);
		}else if(control.getOperacion().equals("irCambiaPeriodo")) {			
			control = irCambiaPeriodo(control);
			control = irCambiaProducto(control);			
		}else if(control.getOperacion().equals("CABECERA")) {			
			control = setCabecera(control);
		}
		
		control.setLstProductos(this.consultasReportesManager.getProductosxAlumno(control.getCodPeriodo(),control.getCodAlumno()));
		
		if ( control.getIndProcedencia().trim().equals("1") )
			request.setAttribute("IndProcedencia","1");
		else request.setAttribute("IndProcedencia","0");
				
		return new ModelAndView(
				"/evaluaciones/consultarEvaluaciones/eva_con_bandeja",
				"control", control);
	}


	private ConsultarEvaluacionesCommand irCambiaPeriodo(
			ConsultarEvaluacionesCommand control) {
		//log.info("irCambiaPeriodo:INI");

		try {
			control = consultasReportesManager.getAllInformacionGeneral(control);

		}catch (Exception e) {
			e.printStackTrace();
		}
		//log.info("irCambiaCiclo:FIN");
		return control;
	}
	private ConsultarEvaluacionesCommand irCambiaProducto(
			ConsultarEvaluacionesCommand control) {
		//log.info("irCambiaPeriodo:INI");
		control.setLstPeriodos(this.consultasReportesManager.
		getAllPeriodoxProducto(control.getCodAlumno(),control.getCboProducto()));		
		//log.info("irCambiaPeriodo:FIN");
		return control;
	}

	private ConsultarEvaluacionesCommand setCabecera(
			ConsultarEvaluacionesCommand command) {
		
		command = consultasReportesManager.setCabecera(command);
		
		return command;
	}

}
