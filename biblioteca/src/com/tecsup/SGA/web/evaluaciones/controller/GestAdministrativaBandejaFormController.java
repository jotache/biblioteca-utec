package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.Especialidad;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.ProgramaComponentes;
import com.tecsup.SGA.modelo.Programas;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.service.evaluaciones.MtoParametrosGeneralesManager;
import com.tecsup.SGA.service.evaluaciones.MtoConfiguracionComponentesManager;
import com.tecsup.SGA.service.evaluaciones.GestionAdministradtivaManager;
import com.tecsup.SGA.web.evaluaciones.command.GestAdministrativaBandejaCommand;
import com.tecsup.SGA.web.evaluaciones.command.MtoConfiguracionComponentesCommand;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarExamenesCommand;


public class GestAdministrativaBandejaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(GestAdministrativaBandejaFormController.class);
	GestionAdministradtivaManager gestionAdministradtivaManager;
	MtoParametrosGeneralesManager mtoParametrosGeneralesManager;
	MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager;
	MantenimientoManager mantenimientoManager;
	ComunManager comunManager;

	/*SETTER*/
	public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}
	
	public void setMtoConfiguracionComponentesManager(
			MtoConfiguracionComponentesManager mtoConfiguracionComponentesManager) {
		this.mtoConfiguracionComponentesManager = mtoConfiguracionComponentesManager;
	}

	public void setMtoParametrosGeneralesManager(
			MtoParametrosGeneralesManager mtoParametrosGeneralesManager) {
		this.mtoParametrosGeneralesManager = mtoParametrosGeneralesManager;
	}

	public void setGestionAdministradtivaManager(
			GestionAdministradtivaManager gestionAdministradtivaManager) {
		this.gestionAdministradtivaManager = gestionAdministradtivaManager;
	}
	
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	public GestAdministrativaBandejaFormController(){
		super();
		setCommandClass(GestAdministrativaBandejaCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	GestAdministrativaBandejaCommand command = new GestAdministrativaBandejaCommand();  	
    	   	
    	/*Recogiendo valores*/
    	command.setCodPeriodoVig((String)request.getParameter("txhCodPeriodo"));
    	command.setCodUsuario((String)request.getParameter("txhCodEvaluador"));
    	
    	if ( command.getCodUsuario() != null && command.getCodPeriodoVig() != null)
    	{
    		cargaPeriodo(command);
    		inicializaCombos(command);
    		command.setCodProducto("-1"); //--Seleccione
    	}
    	
    	command.setCodListaProducto(this.mtoParametrosGeneralesManager.getProductos());
    	
    	//***************************************************
    	//copiado de renzo
    	command.setCodPFR(CommonConstants.TIPT_PRODUCTO_PFR);
    	command.setCodPCC_I(CommonConstants.TIPT_PRODUCTO_PCC_I);
    	command.setCodPCC_CC(CommonConstants.TIPT_PRODUCTO_PCC_CC);
    	command.setCodCAT(CommonConstants.TIPT_PRODUCTO_CAT);
    	command.setCodPAT(CommonConstants.TIPT_PRODUCTO_PAT);
    	command.setCodPIA(CommonConstants.TIPT_PRODUCTO_PIA);
    	command.setCodPE(CommonConstants.TIPT_PRODUCTO_PE);
    	command.setCodHIBRIDO(CommonConstants.TIPT_PRODUCTO_HIBRIDO);
    	command.setCodTecsupVirtual(CommonConstants.TIPT_PRODUCTO_TECSUP_VIRTUAL);
    	command.setCodEtapaProgramaIntegral(CommonConstants.TIPT_ETAPA_PROGRAMA_INTEGRAL);
    	command.setCodEtapaCursoCorto(CommonConstants.TIPT_ETAPA_CURSOS_CORTOS);
    	//***************************************************
    	
    	
    	if (request.getParameter("regreso")!=null) {
        	if (((String)request.getParameter("regreso")).equals("1")) {
        		//examenes
        		//si txhCodPeriodo > 1000 es BDO, caso contrario es PFR. (Si lo s� es un chiche barato)
        		Long codPeriodo = Long.valueOf((String)request.getParameter("txhCodPeriodo"));
        		log.info("c�digo de periodo:" + codPeriodo.toString());
        		if(codPeriodo<10000){
        			command.setOperacion(request.getParameter("prmOperacion"));
            		command.setCodSelCicloPFR(request.getParameter("prmCodSelCicloPfr"));
            		command.setCodEspecialidadPFR(request.getParameter("prmEspecialidadPfr"));
            		command.setCodProducto(request.getParameter("prmCodProducto"));
            		command.setVerCargos(request.getParameter("prmVerCargos"));
            		command.setCodPeriodoVig(request.getParameter("prmCodPeriodoVig"));
            		command.setCodComponentesSeleccionados("");
            			
        		}else{
        		
        			log.info("Ciclo Bdo selected" + request.getParameter("prmCodSelCicloBdo"));
        			log.info("Espec Bdo selected" + request.getParameter("prmEspecialidadBdo"));
        			
        			command.setOperacion(request.getParameter("prmOperacion"));
            		command.setCodSelCicloBDO(request.getParameter("prmCodSelCicloBdo"));            	
            		command.setCodEspecialidadBDO(request.getParameter("prmEspecialidadBdo"));
            		command.setCodProducto(request.getParameter("prmCodProducto"));
            		command.setVerCargos(request.getParameter("prmVerCargos"));
            		command.setCodPeriodoVig(request.getParameter("prmCodPeriodoVig"));
            		command.setCodComponentesSeleccionados("");
//            		command = irSetCombos(request,command);
        		}
        		command = irSetCombos(request,command);
        		
        	}    		
    	}    	
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	GestAdministrativaBandejaCommand control = (GestAdministrativaBandejaCommand)command;
    	
    	log.info("OPERACION:|"+control.getOperacion());
    	
    	if("CHANGE".equals(control.getOperacion())){
    		cambiaProductos(control);    		
    		inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
    						control.getCodEspecialidad(), control.getCodCiclo(),"","","");    		
    		//Llenado Tabla Interes - Session
        	request.getSession().setAttribute("listaCursos", control.getListaCursos());
        }else{
        	control = irSetCombos(request,control);
        }
    	
		
	    return new ModelAndView("/evaluaciones/gestionAdministrativo/eva_bandeja_adm","control",control);		
    }



	/*Metodos*/
    private void cargaPeriodo(GestAdministrativaBandejaCommand command){
    	Periodo periodo;
    	ArrayList<Periodo> listPeriodo;
    	try{
    		listPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoById(command.getCodPeriodoVig());
    		if ( listPeriodo != null )
    			if ( listPeriodo.size() > 0)
    			{
    				periodo =listPeriodo.get(0);
    				command.setDscPeriodoVig(periodo.getNombre());
    			}
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    private void cambiaProductos(GestAdministrativaBandejaCommand command){
    	ArrayList<Producto> productos = (ArrayList<Producto>)this.mtoParametrosGeneralesManager.getProductos();
    	
    	if(productos!=null && productos.size()>0){
	    	int i;
	    	int indexProducto = 0;
	    	Producto producto;
	    	
	    	command.setListaProductos(productos);
			command.setCodProducto(command.getCodProducto());
	    	
	    		for(i=0; i<productos.size(); i++){
	    			producto = productos.get(i);
	    			if (producto.getCodProducto() == command.getCodProducto()) {
	    				indexProducto = i;
	    			}
	    		}
	    		ArrayList<Especialidad> especialidad = (ArrayList<Especialidad>)this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(productos.get(indexProducto));
	    		
	    		if(especialidad!=null && especialidad.size()>0){
	        		command.setListaEspecialidad(especialidad);
	        	}
    	}
    }
    
    
    //JHPR: 2/Abrl/08 Agregue parametro para filtrar solo curso Cargo en la Bandeja Principal del Administrador.
    private void inicializaDataCurso(GestAdministrativaBandejaCommand command, String codPeriodo, String codProducto, String codEspecialidad, String codCiclo,
    								String codEtapa, String codPrograma,String filtrarCargos){
    	command.setListaCursos(this.gestionAdministradtivaManager.getAllCursosByFiltros(codPeriodo, codProducto, codEspecialidad, codCiclo,
    																					codEtapa, codPrograma,filtrarCargos));	
    }
    
    private void inicializaCombos(GestAdministrativaBandejaCommand command)
    {
    	ArrayList<Producto> arrProductos = (ArrayList<Producto>)this.mtoParametrosGeneralesManager.getProductos();
    	Producto producto;
    	if ( arrProductos != null )
    		
	    	if( arrProductos.size() > 0)
	    	{
	    		producto = arrProductos.get(0);
	    		command.setCodProducto(producto.getCodProducto());	    		
	    		command.setListaEspecialidad(this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(producto));
	    		if ( command.getListaEspecialidad() != null )
	    			if ( command.getListaEspecialidad().size() > 0 )
	    				command.setCodEspecialidad(command.getListaEspecialidad().get(0).getCodEspecialidad());
	    	}
		command.setListaProductos(arrProductos);
    }
    
    private GestAdministrativaBandejaCommand irSetCombos(
    		HttpServletRequest request, GestAdministrativaBandejaCommand control) {
    	
    	Producto producto= new Producto();
    	producto.setCodProducto(control.getCodProducto().trim());
    	Curso curso= new Curso();
    	Programas programas= new Programas();
    	Especialidad especialidad= new Especialidad();
    	String codEtapa="";
    	
    	ProgramaComponentes programaComponentes= new ProgramaComponentes();        
        programaComponentes.setCodProducto(control.getCodProducto().trim());
        programaComponentes.setCodComponente(control.getCodComponentesSeleccionados());
       log.info("Operaci�n irSetCombos: " + control.getOperacion());
        if(control.getOperacion().equals("TECSUP_VIRTUAL")){
		    
		    inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(),"", "", "", "","");
			//Llenado ListaCursos - Session
			request.getSession().setAttribute("listaCursos", control.getListaCursos());
        }
        
        if(control.getOperacion().equals("BDO")){	
        
        	control.setCodListaEspecialidadBDO(ListaEspecialidad(control, producto));
        	request.setAttribute("Producto", "producto");
        	
        	
        }else if(control.getOperacion().equals("PFR")){
    		
    		control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
    		
    		inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
					"-1", "-1", "", "","");
			//Llenado ListaCursos - Session
			request.getSession().setAttribute("listaCursos", control.getListaCursos());
    		request.setAttribute("Producto", "producto");
    	}
       else{		
    	//////////////////////////////////////////////////////////////
    	if(control.getOperacion().equals("CAT")){
    		
    		programas.setCodPrograma("");
    		especialidad.setCodEspecialidad("");
    		control.setCodListaCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodoVig()));
    		
    		request.setAttribute("Producto", "cat");
    	}    	
    	else{
		     ////////////////////////////////////////////////////////
    		if(control.getOperacion().equals("ADMISION")){
        		log.info("Listamos tipos de ADMISION");
    			control.setCodEspecialidadAdmision("210");
        		control.setCodListaTiposAdmision(ListaEspecialidad(control, producto));    		
        		request.setAttribute("Producto", "admision");
        	
    		}else if(control.getOperacion().equals("CursosAdmision")){
            		//curso.setCodCurso(control.getCodSelCursoPFR().trim());
        			//especialidad.setCodEspecialidad("210");
            		especialidad.setCodEspecialidad(control.getCodEspecialidadAdmision());
        			curso.setCodCiclo(control.getCodSelCicloPFR());
        			inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), control.getCodEspecialidadAdmision(), "", "", "","");
        			control.setCodListaTiposAdmision(ListaEspecialidad(control, producto));
        			request.getSession().setAttribute("listaCursos", control.getListaCursos());			
        			request.setAttribute("Producto", "admision");
            	        		
        		
        	}else if(control.getOperacion().equals("PIA")){
    		
    		//if(control.getOperacion().equals("PIA")){
		       
		    	control.setCodListaCursoPIA(CursosSolos(producto, control.getCodPeriodoVig()));
		        request.setAttribute("Producto", "pia");
		        //log.info("Tama�o de la Lista CursoPIA: "+control.getCodListaCursoPIA().size());
		        
		        /*Busqueda curso*/
		        inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
		        		"", "", "", "","");
		        request.getSession().setAttribute("listaCursos", control.getListaCursos());
		    }
			else{
				////////////////////////////////////////////
				if(control.getOperacion().equals("PAT")){
				    
				    control.setCodListaCursoPAT(CursosSolos(producto, control.getCodPeriodoVig()));
				    //log.info("Tama�o de la lista CursosPAT: "+control.getCodListaCursoPAT().size());
				    request.setAttribute("Producto", "pat");
				    
				    /*Busqueda curso*/
			        inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(),"", "", "", "","");
			        request.getSession().setAttribute("listaCursos", control.getListaCursos());
				}
				else{
					//////////////////////////////////////////////////////
					if(control.getOperacion().equals("ESPECIALIZACION")){
						
						control.setCodListaProgramaPE(ListaPrograma(control, producto));					
						//log.info(control.getCodListaProgramaPE().size());
						//control.setCodListaCursoPE(Cursos(control, producto));     
						request.setAttribute("Producto", "especializacion");
						
						/*Busqueda curso*/
						inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
								"", control.getCodSelModuloPE(), "", control.getCodSelProgramaPE(),"");

						request.getSession().setAttribute("listaCursos", control.getListaCursos());
						
						
					}
					else{
						///////////////////////////////////////////////////
						if(control.getOperacion().equals("HIBRIDO")){
							
							control.setCodListaCursoHibrido(CursosSolos(producto, control.getCodPeriodoVig()));
							request.setAttribute("Producto", "hibrido");
							
							/*Busqueda curso*/
					        inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), "", "", "", "","");
					        request.getSession().setAttribute("listaCursos", control.getListaCursos());
					        
						}
						else{
							///////////////////////////////////////////////
							if(control.getOperacion().equals("PCC_I")){
								
								control.setValorEtapa(CommonConstants.TIPT_ETAPA_PROGRAMA_INTEGRAL);
								//control.setCodListaCursoPCC(CursosSolos(producto));
								control.setCodListaProgramaPCC(ListaPrograma(control, producto));
								//log.info("Tama�o lista Cursos PPC1 "+control.getCodListaCursoPCC().size());
								//log.info("Tama�o lista Programas PPC1 "+control.getCodListaProgramaPCC().size());
								request.setAttribute("Producto", "pcc1");
								
								/*Busqueda curso*/
								inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(),"-1", "-1", "", "","");
								System.out.println("Size ListaCursos: "+control.getListaCursos());
								request.getSession().setAttribute("listaCursos", control.getListaCursos());
							}
							else{
								if(control.getOperacion().equals("PCC_CC")){
									
									especialidad.setCodEspecialidad("");
									curso.setCodCiclo("");
									codEtapa="";//control.getValorEtapa();
									programas.setCodPrograma("");
									//control.setCodListaCursoPCC(CursosSolos(producto));
									//control.setCodListaProgramaPCC(ListaPrograma(control, producto));
						    		
									control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
									//log.info("Tama�o lista Cursos PPC_CC "+control.getCodListaCursoPCC().size());
									inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
											"-1", "-1", "", "","");
									
									request.getSession().setAttribute("listaCursos", control.getListaCursos());
									request.setAttribute("Producto", "pcc2");
								}
								else{
									////////////////////////////////////////////////////////////////
									if(control.getOperacion().equals("PCC3")){
										
										control.setCodListaCursoPCC(CursosSolos(producto, control.getCodPeriodoVig()));
										control.setCodListaProgramaPCC(ListaPrograma(control, producto));
										//log.info("Tama�o lista Cursos PPC3 "+control.getCodListaCursoPCC().size());
										request.setAttribute("Producto", "pcc3");
									}
									else{
										////////////////////////  GRABAR   ///////////////////////////
										if(control.getOperacion().equals("GRABAR")){
											/*
											log.info("Dentro del guardar, los codigos son: "+control.getCodComponentesSeleccionados() 
												+" y el tama�o es: "+(control.getCodComponentesSeleccionados().length()/5));
											programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
											ListaComponentes( control, producto, request, response, programaComponentes);	
											//String bb=Guardar(control, request, response, producto, curso);		
											control.setCodComponentesSeleccionados("");*/
										}
										else{
											///////////////////  LLenando los Combos HERE       ////////////////////////////////
											///////////////////////// PFR /////////////////////////////////////////////
											if(control.getOperacion().equals("CicloPFR")){												
												
											    curso.setCodCiclo(control.getCodSelCicloPFR().trim());
												programas.setCodPrograma("");
											    control.setCodSelCursoPFR("-1");
											    especialidad.setCodEspecialidad(control.getCodEspecialidadPFR());
											    
											   
											    control.setCodListaCursoPFR(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));											    
											    control.setCodListaCicloPFR(Ciclos(control, producto, programas,especialidad, control.getCodPeriodoVig()));
											    control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
											    request.setAttribute("Producto", "producto");
											    //System.out.println("irSetCombos 6");
											    
											    /*Busqueda curso*/
												 inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), control.getCodEspecialidadPFR(), control.getCodSelCicloPFR(), "", "",control.getVerCargos());
												 //System.out.println("irSetCombos 7");
												 request.getSession().setAttribute("listaCursos", control.getListaCursos());
											} else{
												
												if(control.getOperacion().equals("CicloBDO")){
													
													curso.setCodCiclo(control.getCodSelCicloBDO().trim());
													programas.setCodPrograma("");
												    control.setCodSelCursoBDO("-1");
												    especialidad.setCodEspecialidad(control.getCodEspecialidadBDO());
												    												   
												    control.setCodListaCursoBDO(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));											    
												    control.setCodListaCicloBDO(Ciclos(control, producto, programas,especialidad, control.getCodPeriodoVig()));
												    control.setCodListaEspecialidadBDO(ListaEspecialidad(control, producto));
												    request.setAttribute("Producto", "producto");

													 inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), control.getCodEspecialidadBDO(), control.getCodSelCicloBDO(), "", "",control.getVerCargos());													
													 request.getSession().setAttribute("listaCursos", control.getListaCursos());
												}else{
												
												if(control.getOperacion().equals("CursoPFR")){
													//log.info("operacion:CursoPFR");
												    //curso.setCodCurso(control.getCodSelCursoPFR().trim());
													especialidad.setCodEspecialidad(control.getCodEspecialidadPFR());
													programas.setCodPrograma("");
													/*log.info("control.getCodSelCicloPFR():"+control.getCodSelCicloPFR());
													log.info("control.getCodSelCursoPFR():"+control.getCodSelCursoPFR());
													log.info("obj producto:" + producto.getCodProducto());
										    		log.info("obj programas: CodPrograma " + programas.getCodPrograma());
										    		log.info("obj especialidad: CodEspecialidad " + especialidad.getCodEspecialidad());
										    		log.info("control.getCodPeriodoVig():"+control.getCodPeriodoVig());
										    		log.info("control.getCodProducto():"+control.getCodProducto().trim());
										    		log.info("control.getCodComponentesSeleccionados():"+control.getCodComponentesSeleccionados());*/
										    		
													curso.setCodCiclo(control.getCodSelCicloPFR());
													control.setCodListaCicloPFR(Ciclos(control, producto, programas,especialidad, control.getCodPeriodoVig()));
													control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
													control.setCodListaCursoPFR(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
													programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
													/*log.info("Ciclo Especialidad PFR "+programaComponentes.getCodEspecialidad());
													log.info("Ciclo Ciclo PFR "+programaComponentes.getCodCiclo());
													log.info("Ciclo Curso PFR "+programaComponentes.getCodCurso());
													log.info("Ciclo Prodcto PFR "+programaComponentes.getCodProducto());*/
													if(((!control.getCodEspecialidadPFR().equals("-1"))&&(!control.getCodSelCursoPFR().equals("-1")))&&
													    		(!control.getCodSelCicloPFR().equals("-1")))
													     ListaComponentes( control, producto, request,  programaComponentes);
													request.setAttribute("Producto", "producto");
												}
												else{
													if(control.getOperacion().equals("EspecialidadPFR")){
														 
														 curso.setCodCurso(control.getCodSelCursoPFR().trim());
														 especialidad.setCodEspecialidad(control.getCodEspecialidadPFR());
														 programas.setCodPrograma("");
														 control.setCodSelCicloPFR("-1");
														 control.setCodSelCursoPFR("-1");
														 control.setCodListaCicloPFR(Ciclos(control, producto, programas,especialidad, control.getCodPeriodoVig()));
														 programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
														 control.setCodListaEspecialidadPFR(ListaEspecialidad(control, producto));
														 
														 if(((!control.getCodEspecialidadPFR().equals("-1"))&&(!control.getCodSelCursoPFR().equals("-1")))&&
														    		(!control.getCodSelCicloPFR().equals("-1")))
														    ListaComponentes( control, producto, request,  programaComponentes);
														 
														 request.setAttribute("Producto", "producto");
														 /*Busqueda curso*/
														 inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
																	control.getCodEspecialidadPFR(), "-1", "-1", "","");
														 request.getSession().setAttribute("listaCursos", control.getListaCursos());
											       }else{
												     
											    	   if(control.getOperacion().equals("EspecialidadBDO")){
														 
											    		   especialidad.setCodEspecialidad(control.getCodEspecialidadBDO());
											    		   programas.setCodPrograma("");
											    		   control.setCodSelCicloBDO("-1");
														   control.setCodSelCursoBDO("-1");
														   control.setCodListaCicloBDO(Ciclos(control, producto, programas,especialidad, control.getCodPeriodoVig()));
														   programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
														   control.setCodListaEspecialidadBDO(ListaEspecialidad(control, producto));
														   request.setAttribute("Producto", "producto");
														   
											    		   
													   }else{
														//////////////////////// FIN PFR //////////////////////////////////
														/////////////////////////// PE ////////////////////////////////////////
														if(control.getOperacion().equals("ModuloPE")){
															/*log.info("operacion:ModuloPE");
															log.info("control.getCodSelModuloPE():"+control.getCodSelModuloPE());
															log.info("control.getCodSelProgramaPE():"+control.getCodSelProgramaPE());
															log.info("control.getCodPeriodoVig():"+control.getCodPeriodoVig());
															log.info("control.getCodProducto():"+control.getCodProducto().trim());*/
															curso.setCodCiclo(control.getCodSelModuloPE().trim());
															programas.setCodPrograma(control.getCodSelProgramaPE());
															especialidad.setCodEspecialidad("");
															control.setCodListaModuloPE(Ciclos(control, producto, programas, especialidad, control.getCodPeriodoVig())); 
															control.setCodListaProgramaPE(ListaPrograma(control, producto));
															control.setCodListaCursoPE(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
															control.setCodSelCursoPE("-1");
															request.setAttribute("Producto", "producto");
															
															/*Busqueda curso*/
															inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
																	"", control.getCodSelModuloPE(), "", control.getCodSelProgramaPE(),"");
															request.getSession().setAttribute("listaCursos", control.getListaCursos());
															
														}
														else{
															if(control.getOperacion().equals("CursoPE")){
																/*log.info("operacion:CursoPE");
																log.info("control.getCodSelModuloPE():"+control.getCodSelModuloPE());
																log.info("control.getCodSelProgramaPE():"+control.getCodSelProgramaPE());
																log.info("control.getCodSelCursoPE():"+control.getCodSelCursoPE());
																log.info("control.getCodPeriodoVig():"+control.getCodPeriodoVig());*/
																//log.info("control.getCodProducto():"+control.getCodProducto().trim());
																especialidad.setCodEspecialidad("");
																curso.setCodCiclo(control.getCodSelModuloPE());
																programas.setCodPrograma(control.getCodSelProgramaPE().trim());
															  	curso.setCodCurso(control.getCodSelCursoPE().trim());
																control.setCodListaModuloPE(Ciclos(control, producto, programas, especialidad, control.getCodPeriodoVig()));
																control.setCodListaProgramaPE(ListaPrograma(control, producto));
																control.setCodListaCursoPE(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
																programaComponentes=LlenarProgramaByProducto(control, programaComponentes);																
																if(((!control.getCodListaProgramaPE().equals("-1"))&&(!control.getCodSelCursoPE().equals("-1")))&&
															    		(!control.getCodSelModuloPE().equals("-1")))
															       ListaComponentes( control, producto, request,  programaComponentes);
																request.setAttribute("Producto", "producto");
															}
															else{
																 if(control.getOperacion().equals("ProgramaPE")){
																	 //log.info("operacion:ProgramaPE");
																	 curso.setCodCurso(control.getCodSelCursoPE().trim());
																	 control.setCodListaModuloPE(Ciclos(control, producto, programas, especialidad, control.getCodPeriodoVig()));
																	 //control.setCodListaCursoPE(Cursos(control, producto));
																	 control.setCodListaProgramaPE(ListaPrograma(control, producto));
																	 programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																	 if(((!control.getCodSelModuloPE().equals("-1"))&&(!control.getCodSelCursoPE().equals("-1")))&&
																	    		(!control.getCodSelProgramaPE().equals("-1")))
																	   { programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																		 ListaComponentes( control, producto, request,  programaComponentes);
																	 }
																		request.setAttribute("Producto", "producto");
																		
																	/*Busqueda curso*/
																	inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
																					"", control.getCodSelModuloPE(), "", control.getCodSelProgramaPE(),"");
																	request.getSession().setAttribute("listaCursos", control.getListaCursos());
																		
																   }
																else{
																	///////////////////////// FIN PE ////////////////////////////////////////////
																	//////////////////////// CAT //////////////////////////////////////////////////
																	if(control.getOperacion().equals("CicloCAT")){
																		//log.info("operacion:CicloCAT");
																		curso.setCodCiclo(control.getCodSelCicloCAT().trim());
																		especialidad.setCodEspecialidad("");
																		programas.setCodPrograma("");
																		control.setCodListaCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodoVig()));
																		control.setCodListaCursoCAT(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
																		//log.info("CURSOCAT VAL: "+control.getCodSelCicloCAT());
																		//log.info("JUVE: "+control.getCodListaCursoCAT().size());
																		control.setCodSelCursoCAT("-1");
																		request.setAttribute("Producto", "producto");
																		
																		/*Busqueda curso*/
																		inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
																				"", control.getCodSelCicloCAT(), "", "","");

																		request.getSession().setAttribute("listaCursos", control.getListaCursos());
																		
																	}
																	else{
																		if(control.getOperacion().equals("CursoCAT")){
																			//log.info("operacion:CursoCAT");
																			curso.setCodCurso(control.getCodSelCursoCAT().trim());
																			curso.setCodCiclo(control.getCodSelCicloCAT().trim());
																			especialidad.setCodEspecialidad("");
																			programas.setCodPrograma("");
																			control.setCodListaCicloCAT(Ciclos(control, producto, programas, especialidad, control.getCodPeriodoVig()));
																			control.setCodListaCursoCAT(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
																			programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																			/*log.info("Programa CAT "+programaComponentes.getCodPrograma());
																			log.info("Ciclo CAT "+programaComponentes.getCodCiclo());
																			log.info("Curso CAT "+programaComponentes.getCodCurso());
																			log.info("Prodcto CAT "+programaComponentes.getCodProducto());*/
																			if((!control.getCodSelCursoCAT().equals("-1"))&&(!control.getCodSelCicloCAT().equals("-1")))
																			    ListaComponentes( control, producto, request,  programaComponentes);
																			request.setAttribute("Producto", "producto");
																		}
																		else{
																			///////////////////////////// FIN CAT ////////////////////////////////////////
																			//////////////////////////// PAT /////////////////////////////////////////////
																			if(control.getOperacion().equals("CursoPAT")){
																				//log.info("operacion:CursoPAT");
																				control.setCodListaCursoPAT(CursosSolos(producto, control.getCodPeriodoVig()));
																				programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																				 /*log.info("Programa PAT "+programaComponentes.getCodPrograma());
																				 log.info("Ciclo PAT "+programaComponentes.getCodCiclo());
																				 log.info("Curso PAT "+programaComponentes.getCodCurso());
																				 log.info("Prodcto PAT "+programaComponentes.getCodProducto());*/
																				 if(!control.getCodSelCursoPAT().equals("-1"))
																				     ListaComponentes( control, producto, request,  programaComponentes);
																				 request.setAttribute("Producto", "producto");
																			}
																			else{	
																			  	///////////////////////// FIN PAT ////////////////////////////////////////////
																			    //////////////////////// PIA ////////////////////////////////////////////////
																				if(control.getOperacion().equals("CursoPIA")){
																					//log.info("operacion:CursoPIA");
																					control.setCodListaCursoPIA(CursosSolos(producto, control.getCodPeriodoVig()));
																					programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																					/*log.info("Programa PIA "+programaComponentes.getCodPrograma());
																					log.info("Ciclo PIA "+programaComponentes.getCodCiclo());
																					log.info("Curso PIA "+programaComponentes.getCodCurso());
																					log.info("Prodcto PIA "+programaComponentes.getCodProducto());*/
																					if(!control.getCodSelCursoPIA().equals("-1"))
																					   ListaComponentes( control, producto, request,  programaComponentes);
																					request.setAttribute("Producto", "producto");
																					}
																				else{
																					////////////////////////// FIN PIA //////////////////////////////////////////////
																					/////////////////////////// HIBRIDO /////////////////////////////////////////////
																					if(control.getOperacion().equals("CursoHibrido")){
																						//log.info("operacion:CursoHibrido");
																						control.setCodListaCursoHibrido(CursosSolos(producto, control.getCodPeriodoVig()));
																						programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																						/*log.info("Programa HIBRIDO "+programaComponentes.getCodPrograma());
																						log.info("Ciclo HIBRIDO "+programaComponentes.getCodCiclo());
																						log.info("Curso HIBRIDO "+programaComponentes.getCodCurso());
																						log.info("Prodcto HIBRIDO "+programaComponentes.getCodProducto());*/
																						if(!control.getCodSelCursoHibrido().equals("-1"))
																						    ListaComponentes( control, producto, request,  programaComponentes);
																						request.setAttribute("Producto", "producto");
																					}
																					else{
																						if(control.getOperacion().equals("ProgramaIntegralPCC")){
																							//log.info("operacion:ProgramaIntegralPCC");
																							especialidad.setCodEspecialidad("");
																							programas.setCodPrograma(control.getCodSelProgramaPCC());
																							codEtapa=control.getValorEtapa();
																							curso.setCodCiclo("");
																							if(!control.getCodSelProgramaPCC().equals("-1"))
																								control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
																							control.setCodListaProgramaPCC(ListaPrograma(control, producto));
																							control.setCodSelCursoPCC("-1");
																							control.setValorEtapa("1");
																							request.setAttribute("Producto", "producto");
																							request.setAttribute("VALORETAPA1", "UNO");
																							
																							/*Busqueda Curso*/
																							inicializaDataCurso(control, control.getCodPeriodoVig(), control.getCodProducto(), 
																									"-1", "-1", "-1", control.getCodSelProgramaPCC(),"");
																							request.getSession().setAttribute("listaCursos", control.getListaCursos());
																							
																					       }
																						else{  if(control.getOperacion().equals("MADFROG1")){
																									//log.info("operacion:MADFROG1");
																									curso.setCodCiclo("");
																									especialidad.setCodEspecialidad("");
																									programas.setCodPrograma("");
																									codEtapa=control.getValorEtapa();
																									control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
																									control.setCodSelCursoPCC("-1");
																									request.setAttribute("CODIGOETAPA", programaComponentes.getCodEtapa());
																									request.setAttribute("VALORETAPA", "DOS");
																								    }
																								else{if(control.getOperacion().equals("TECSUP_VIRTUAL")){
																									/*log.info("operacion:TECSUP_VIRTUAL2");
																								    log.info("es TECSUP_VIRTUAL");*/
																								    control.setCodListaCursoTECSUPVIRTUAL(CursosSolos(producto, control.getCodPeriodoVig()));
																								    request.setAttribute("Producto", "TECSUP_VIRTUAL");
																								   }
																						      	  else{ if(control.getOperacion().equals("CursoTecsupVirtual")){
																						      		  	//log.info("operacion:CursoTecsupVirtual");
																										control.setCodListaCursoTECSUPVIRTUAL(CursosSolos(producto, control.getCodPeriodoVig()));
																										programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																										
																										 if(!control.getCodSelCursoTECSUPVIRTUAL().equals("-1"))
																										     ListaComponentes( control, producto, request,  programaComponentes);
																										 request.setAttribute("Producto", "producto");
																									   }
																						      	  		else{ if(control.getOperacion().equals("CursoPCC_I")){
																						      	  				//log.info("operacion:CursoPCC_I");
																												especialidad.setCodEspecialidad("");
																												//log.info("dentro del CursoPCC_I: "+ control.getCodSelCursoPCC());
																												codEtapa="";
																													programas.setCodPrograma(control.getCodSelProgramaPCC()); 
																													control.setCodListaProgramaPCC(ListaPrograma(control, producto));
																													if((!control.getCodSelCursoPCC().equals("-1"))&&(!control.getCodSelProgramaPCC().equals("-1")))
																													    ListaComponentes( control, producto, request,  programaComponentes);
																												
																												curso.setCodCiclo("");
																												control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
																												/*log.info("CODETAPA ?? "+control.getProgramaPCCEtapa());
																												log.info("Programa WW PCC1 "+programaComponentes.getCodPrograma());
																												log.info("Ciclo WW PCC1 "+programaComponentes.getCodCiclo());
																												log.info("Curso WW PCC1 "+programaComponentes.getCodCurso());
																												log.info("Prodcto WW PCC1 "+programaComponentes.getCodProducto());
																												log.info("ValorEtapa "+control.getValorEtapa());*/
																												request.setAttribute("Producto", "producto");
																												request.setAttribute("CODIGOETAPA", programaComponentes.getCodEtapa());
																												}
																						      	  			else{ if(control.getOperacion().equals("CursoPCC_CC")){
																						      	  				//log.info("operacion:CursoPCC_I");
																												especialidad.setCodEspecialidad("");
																												codEtapa="";
																												programaComponentes=LlenarProgramaByProducto(control, programaComponentes);
																												control.setValorEtapa(""); 
																												programas.setCodPrograma("");
																											    if(!control.getCodSelCursoPCC().equals("-1"))
																												     ListaComponentes( control, producto, request,  programaComponentes);
																												curso.setCodCiclo("");
																												control.setCodListaCursoPCC(Cursos(producto, programas, especialidad, curso, codEtapa, control.getCodPeriodoVig()));
																												/*log.info("CODETAPA PCC_CC: "+control.getProgramaPCCEtapa());
																												log.info("Programa WW PCC_CC: "+programaComponentes.getCodPrograma());
																												log.info("Ciclo WW PCC_CC: "+programaComponentes.getCodCiclo());
																												log.info("Curso WW PCC_CC: "+programaComponentes.getCodCurso());
																												log.info("Prodcto WW PCC_CC: "+programaComponentes.getCodProducto());
																												log.info("ValorEtapa PCC_CC:"+control.getValorEtapa());*/
																												request.setAttribute("Producto", "producto");
																												}
																						      	  				
																						      	  			}
																						      	  			
																						      	  		}
																						      		  
																						      	  }
																						    	  
																						      }
																									 
																							    }
																						    
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													  }
													}
												}
										}
											}
										}
									}
								}
							}
						}
					}
			   }
		  }
	}	
    	///////////////////// FIN PCC1 /////////////////////////////////////////////////////
    	        
		return control;
	}
    
    
	public List ListaPrograma(GestAdministrativaBandejaCommand control, Producto producto){	    
		return this.mtoConfiguracionComponentesManager.getProgramasByProducto(producto, control.getValorEtapa());	
	}
	 
	/*HttpServletResponse response*/
    public void ListaComponentes(GestAdministrativaBandejaCommand control, Producto producto,
    		HttpServletRequest request,  ProgramaComponentes programaComponentes){
    	
    }
	 
	public List Cursos(Producto producto, Programas programas,Especialidad especialidad, Curso curso, 
			String codEtapa, String codPeriodo){
		return this.mtoConfiguracionComponentesManager.getCursoByProductoEspecialidadCiclo(producto, programas,
				especialidad, curso, codEtapa, codPeriodo);
	}
	public List CursosSolos(Producto producto, String codPeriodo){
		return this.mtoConfiguracionComponentesManager.getCursosByProducto(producto, codPeriodo);
	}
	
    public List Ciclos(GestAdministrativaBandejaCommand control, Producto producto, Programas programas, 
    		Especialidad especialidad, String codPeriodo){    
    	return this.mtoConfiguracionComponentesManager.getCicloByProductoEspecialidadPrograma(producto, 
    		especialidad, programas, codPeriodo);	
    }
    
    
    public List ListaEspecialidad(GestAdministrativaBandejaCommand control, Producto producto){
    	return this.mtoConfiguracionComponentesManager.getEspecialidadByProducto(producto);
    }
    
    public ProgramaComponentes LlenarProgramaByProducto(GestAdministrativaBandejaCommand control,
    		ProgramaComponentes programaComponentes ){
    	
    	if(control.getTipoProducto().equals("BDO")){
    	
    		programaComponentes.setCodEspecialidad(control.getCodEspecialidadBDO().trim());
    		programaComponentes.setCodCiclo(control.getCodSelCicloBDO().trim());
    		programaComponentes.setCodCurso(control.getCodSelCursoBDO().trim());
    		programaComponentes.setCodEtapa("");
    		programaComponentes.setCodPrograma("");//CicloPFR
    		
    	}else if(control.getTipoProducto().equals("PFR"))
    	{   programaComponentes.setCodEspecialidad(control.getCodEspecialidadPFR().trim());
    		programaComponentes.setCodCiclo(control.getCodSelCicloPFR().trim());
    		programaComponentes.setCodCurso(control.getCodSelCursoPFR().trim());
    		programaComponentes.setCodEtapa("");
    		programaComponentes.setCodPrograma("");//CicloPFR
    	}
    	else{
    	    if(control.getTipoProducto().equals("CAT"))
    		{   programaComponentes.setCodPrograma("");
    	        programaComponentes.setCodCiclo(control.getCodSelCicloCAT().trim());
    	        programaComponentes.setCodCurso(control.getCodSelCursoCAT().trim());
    	        programaComponentes.setCodEtapa("");
    	        programaComponentes.setCodEspecialidad("");
    		}
    		else{
    		    if(control.getTipoProducto().equals("PIA"))
    			{   programaComponentes.setCodPrograma("");
    		      	programaComponentes.setCodCiclo("");
    		      	programaComponentes.setCodCurso(control.getCodSelCursoPIA().trim());
    		      	programaComponentes.setCodEtapa("");
    		      	programaComponentes.setCodEspecialidad("");
    			}
    		    else{
    			    if(control.getTipoProducto().equals("PAT"))
    				{  programaComponentes.setCodPrograma("");
    		     	   programaComponentes.setCodCiclo("");
    		     	   programaComponentes.setCodCurso(control.getCodSelCursoPAT().trim());
    		     	   programaComponentes.setCodEtapa("");
    		     	   programaComponentes.setCodEspecialidad("");
    				}
    				else{
    			    	if(control.getTipoProducto().equals("PROGRA_ESPECIALIZADO"))
    					{	programaComponentes.setCodPrograma(control.getCodSelProgramaPE().trim());
    			        	programaComponentes.setCodCiclo(control.getCodSelModuloPE().trim());
    			        	programaComponentes.setCodCurso(control.getCodSelCursoPE().trim());
    			        	programaComponentes.setCodEtapa("");
    			        	programaComponentes.setCodEspecialidad("");
    					}
    					else{
    				    	if(control.getTipoProducto().equals("HIBRIDO"))
    						{  programaComponentes.setCodPrograma("");
    				       	   programaComponentes.setCodCiclo("");
    				       	   programaComponentes.setCodCurso(control.getCodSelCursoHibrido().trim());
    				       	   programaComponentes.setCodEtapa("");
    				       	   programaComponentes.setCodEspecialidad("");
    						}
    						else{
    					    	if(control.getTipoProducto().equals("PCC_I"))
    							  {	programaComponentes.setCodCurso(control.getCodSelCursoPCC());
  									programaComponentes.setCodEspecialidad("");
  									programaComponentes.setCodCiclo("");
  									programaComponentes.setCodPrograma(control.getCodSelProgramaPCC());
  									programaComponentes.setCodEtapa("");
    						      }
    					      else{ if(control.getTipoProducto().equals("TECSUP_VIRTUAL"))
 						           {   programaComponentes.setCodPrograma("");
		     				       	   programaComponentes.setCodCiclo("");
		     				       	   programaComponentes.setCodCurso(control.getCodSelCursoTECSUPVIRTUAL().trim());
		     				       	   programaComponentes.setCodEtapa("");
		     				       	   programaComponentes.setCodEspecialidad("");
						           }
					    		   else{ if(control.getTipoProducto().equals("PCC_CC"))
							  		  {	programaComponentes.setCodCurso(control.getCodSelCursoPCC());
							  			programaComponentes.setCodEspecialidad("");
							  			programaComponentes.setCodCiclo("");
							  			programaComponentes.setCodPrograma("");
							  			programaComponentes.setCodEtapa("");
							  		  }
					    		    }
					    		}
    						    }
    						}	  
    					}		  
    				}		  
    	        }
    	    }
    	
     return programaComponentes;	
    }
}
