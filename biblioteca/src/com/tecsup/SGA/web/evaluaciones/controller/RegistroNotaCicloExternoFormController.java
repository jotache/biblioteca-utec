package com.tecsup.SGA.web.evaluaciones.controller;
import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.service.evaluaciones.NotaExternaManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class RegistroNotaCicloExternoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RegistroNotaCicloExternoFormController.class);
	
	private NotaExternaManager notaExternaManager;
	
	public NotaExternaManager getNotaExternaManager() {
		return notaExternaManager;
	}

	public void setNotaExternaManager(NotaExternaManager notaExternaManager) {
		this.notaExternaManager = notaExternaManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	RegistroNotaCicloExternoCommand command = new RegistroNotaCicloExternoCommand();
    	
    	String codPeriodo =(String)request.getParameter("txhCodPeriodo");
    	String codProducto =(String)request.getParameter("txhCodProducto");    	
    	String codEspecialidad =(String)request.getParameter("txhCodEspecialidad");
    	String codEtapa =(String)request.getParameter("txhCodEtapa");
    	String codPrograma =(String)request.getParameter("txhCodPrograma");
    	String codCiclo =(String)request.getParameter("txhCodCiclo");    	
    	String codCurso =(String)request.getParameter("txhCodCurso");
    	String codAlumno =(String)request.getParameter("txhCodAlumno");
    	String nombreAlumno =(String)request.getParameter("txhNombreAlumno");
    	String notaCat=(String)request.getParameter("txhNotaExterno");
    	String codEvaluador =(String)request.getParameter("txhCodEvaluador");
    	String operacion =(String)request.getParameter("txhOperacion");    	
    	
    	command.setCodPeriodo(codPeriodo);
    	command.setCodProducto(codProducto);
    	command.setCodEspecialidad(codEspecialidad);
    	command.setCodEtapa(codEtapa);
    	command.setCodPrograma(codPrograma);
    	command.setCodCiclo(codCiclo);
    	command.setCodCurso(codCurso);
    	command.setCodAlumno(codAlumno);
    	command.setAlumno(nombreAlumno);
    	command.setNotaCat(notaCat);
    	command.setCodEvaluador(codEvaluador);
    	command.setOperacion(operacion);
    	
    	log.info("formBackingObject:FIN");
        return command;    	
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	RegistroNotaCicloExternoCommand control = (RegistroNotaCicloExternoCommand) command;    	
    	log.info("OPERACION:" + control.getOperacion());
    	
    	String resultado = "";
    	if(control.getOperacion().trim().equals("GRABAR")){    		
    		resultado=InsertNotaCicloExterno(control);
    		log.info("RPTA:" + resultado);
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			control.setMsg("OK");
    		}    		
    	}
    	else{
    		if(control.getOperacion().trim().equals("MODIFICAR")){    			
        		resultado=UpdateNotaCicloExterno(control); 
        		log.info("RPTA:" + resultado);
        		if(resultado.equals("-1")){
        			control.setMsg("ERROR");
        		}
        		else{
        			control.setMsg("OK");
        		}
        	}    		
    	}   	
		
		return new ModelAndView("/evaluaciones/registroNota/eva_registro_nota_ciclo_externo","control",control);
	     		
    }
    
    private String UpdateNotaCicloExterno(RegistroNotaCicloExternoCommand control){
    	try{
    		Alumno obj= new Alumno();    		
    		
    		obj.setCodPeriodo(control.getCodPeriodo());
    		obj.setCodProducto(control.getCodProducto());    		
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setCodCurso(control.getCodCurso());    		
    		obj.setCodCiclo(control.getCodCiclo());
    		obj.setTipo(CommonConstants.COD_NOTA_CICLO_EXTERNO_CAT);
    		obj.setNotaExterna(control.getNotaCat());
    		obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodEtapa(control.getCodEtapa());
    		obj.setCodPrograma(control.getCodPrograma());
    		
    		obj.setEstado(this.notaExternaManager.UpdateNotaExterno(obj, control.getCodEvaluador()));
    		return obj.getEstado();
    		//**************************
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String InsertNotaCicloExterno(RegistroNotaCicloExternoCommand control){
    	try{
    		Alumno obj= new Alumno();
    		
    		obj.setCodPeriodo(control.getCodPeriodo());
    		obj.setCodProducto(control.getCodProducto());    		
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setCodCurso(control.getCodCurso());    		
    		obj.setCodCiclo(control.getCodCiclo());
    		obj.setTipo(CommonConstants.COD_NOTA_CICLO_EXTERNO_CAT);
    		obj.setNotaExterna(control.getNotaCat());
    		obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodEtapa(control.getCodEtapa());
    		obj.setCodPrograma(control.getCodPrograma());
    		
    		obj.setEstado(this.notaExternaManager.InsertNotaExterno(obj, control.getCodEvaluador()));
    		return obj.getEstado();
    		//**************************
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}