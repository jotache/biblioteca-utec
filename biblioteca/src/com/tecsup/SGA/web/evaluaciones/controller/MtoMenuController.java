package com.tecsup.SGA.web.evaluaciones.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.ModelMap;

public class MtoMenuController implements Controller {
	private static Log log = LogFactory.getLog(MtoMenuController.class);

	public MtoMenuController(){	
		
	}
	
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
	throws Exception {
    	
    	
    	ModelMap model = new ModelMap();
    	String pagina = "/evaluaciones/mantenimiento/eva_mto_menu";
    	
    	if(request.getParameter("construccion")!=null)
    		pagina = "/construccion";
	   		   	
		return new ModelAndView(pagina, "model", model);
	}
}

