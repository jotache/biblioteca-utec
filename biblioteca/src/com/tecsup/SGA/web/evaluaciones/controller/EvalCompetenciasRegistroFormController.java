package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.web.evaluaciones.command.EvaCompetenciaBandejaCommand;
import com.tecsup.SGA.modelo.CompetenciasPerfil;
import com.tecsup.SGA.modelo.ParametrosEspeciales;

public class EvalCompetenciasRegistroFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(IncidenciaRegistrarDocFormController.class);
	private OperDocenteManager operDocenteManager;
	private MantenimientoManager mantenimientoManager;
	
	public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}

	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}

	public EvalCompetenciasRegistroFormController(){
		super();
		setCommandClass(EvaCompetenciaBandejaCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	EvaCompetenciaBandejaCommand command = new EvaCompetenciaBandejaCommand();  	
    	   	
    	/*Recogiendo valores*/
    	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
    	command.setCodEvaluador((String)request.getParameter("txhCodEval"));
    	command.setCodCurso((String)request.getParameter("txhCodCurso"));
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setNomAlumno((String)request.getParameter("txhNomAlumno"));
    	command.setTipoCalificacion((String)request.getParameter("txhTipoCalificacion"));
    	command.setCodSeccion((String)request.getParameter("txhCodSeccion"));
    	command.setCodTipoSesionDefecto((String)request.getParameter("txhCodTipoSesion"));
    	//Llenando información
    	if ( command.getCodCurso() != null )
    	{
    		command.setNomAlumno( command.getNomAlumno() + " (" + command.getCodAlumno() + ")");
    		if ( command.getTipoCalificacion().trim().equals("1")) command.setDscCalificacion("Inicio");
    		else command.setDscCalificacion("Fin");
    		cargaPesos(command);
    		cargaCompetencias(command);
    	}
    	request.getSession().setAttribute("listCompetenciasByAlumno", command.getListaBandeja());
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	String respuesta = "";
    	EvaCompetenciaBandejaCommand control = (EvaCompetenciaBandejaCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if ( control.getOperacion().trim().equals("GRABAR"))
    	{
    		respuesta = gragarCompetencias(control);
    		log.info("RPTA:" + respuesta);
    		if ( respuesta == null ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("-1") ) control.setMsg("ERROR");
    		else if ( respuesta.trim().equals("0") )
    		{
    			control.setMsg("OK");
    		}
    	}
		cargaCompetencias(control);
    	//Llenando información
    	if ( control.getCodCurso() != null )
    	{
    		if ( control.getTipoCalificacion().trim().equals("1")) control.setDscCalificacion("Inicio");
    		else control.setDscCalificacion("Fin");
    		cargaPesos(control);
    	}
    	request.getSession().setAttribute("listCompetenciasByAlumno", control.getListaBandeja());
		
	    return new ModelAndView("/evaluaciones/cargaOperativa/eva_competenciasregistro","control",control);		
    }
    private void cargaPesos(EvaCompetenciaBandejaCommand control)
    {
    	ParametrosEspeciales parametro;
    	ArrayList<ParametrosEspeciales> listaParametros;
    	listaParametros = (ArrayList<ParametrosEspeciales>)this.mantenimientoManager.getAllParametrosEspeciales(
    			control.getCodPeriodo(), CommonConstants.PARAM_ESP_CAL_MEDIA);
    	if ( listaParametros != null )
    		if ( listaParametros.size() > 0)
    		{
    			for ( int i = 0; i < listaParametros.size(); i++)    				
    			{
    				parametro = listaParametros.get(i);
    				if ( parametro.getValor2().trim().equals(CommonConstants.PARAM_ESP_CAL_EXCELENTE) )
    					control.setCadPesoExce(parametro.getValor1());
    				else if ( parametro.getValor2().trim().equals(CommonConstants.PARAM_ESP_CAL_BUENA) )
    					control.setCadPesoBuen(parametro.getValor1());
        			else if ( parametro.getValor2().trim().equals(CommonConstants.PARAM_ESP_CAL_MEDIA) )
        				control.setCadPesoMedi(parametro.getValor1());    				
            		else if ( parametro.getValor2().trim().equals(CommonConstants.PARAM_ESP_CAL_BAJA) )
            			control.setCadPesoBaja(parametro.getValor1());
    			}
    		}
    	
    }
    private String gragarCompetencias(EvaCompetenciaBandejaCommand control)
    {
    	StringTokenizer stCompetencias = new StringTokenizer(control.getCadCodCompetencias(),"|");
    	StringTokenizer stEvaluaciones = new StringTokenizer(control.getCadCodEvaluaciones(),"|");
    	String cadenaDatos = "";
    	String codEvaluacion = "";
    	try
    	{
    		while (stCompetencias.hasMoreTokens())
    		{
    			cadenaDatos = cadenaDatos + stCompetencias.nextToken() + "$";
    			
    			codEvaluacion = stEvaluaciones.nextToken();
    			if ( codEvaluacion.trim().equals("0") ) codEvaluacion = "";
    			
    			if ( control.getTipoCalificacion().trim().equals("1"))
    				cadenaDatos = cadenaDatos + codEvaluacion + "$$";
    			else cadenaDatos = cadenaDatos + "$" + codEvaluacion + "$";
    			
    			cadenaDatos = cadenaDatos + "|";
    		}
    		
    		return this.operDocenteManager.insertCompetenciasByAlumno(
    					control.getCodAlumno(), control.getCodCurso()
    					, control.getCodPeriodo(), CommonConstants.COMP_IND_DOC
    					, cadenaDatos, control.getCodEvaluador()
    					, control.getCodSeccion(), control.getCodTipoSesionDefecto());
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return null;
    }
	private void cargaCompetencias(EvaCompetenciaBandejaCommand control)
	{
		ParametrosEspeciales parametrosEspeciales;
		ArrayList arrValorComp = (ArrayList)mantenimientoManager.getAllParametrosEspeciales(control.getCodPeriodo(), "0003");
		if ( arrValorComp == null) control.setMsg("-99");
		else if ( arrValorComp.size() == 0) control.setMsg("-99");
		else
			for ( int j = 0; j < arrValorComp.size(); j++)
			{
				parametrosEspeciales = (ParametrosEspeciales)arrValorComp.get(j);
				if ( parametrosEspeciales.getValor1() == null ) control.setMsg("-99");
				else if ( parametrosEspeciales.getValor1().trim().equals("")) control.setMsg("-99");				
			}
		
		ArrayList<CompetenciasPerfil> listCompetencias = 
				(ArrayList<CompetenciasPerfil>)this.operDocenteManager.getAllConpetenciasByAlumno(
													control.getCodPeriodo()
													, control.getCodCurso()
													, control.getCodSeccion()
													, control.getCodAlumno()
													, CommonConstants.COMP_IND_DOC);
		if ( listCompetencias != null )
			for ( int i = 0; i < listCompetencias.size(); i++)
			{
				listCompetencias.get(i).setFlagTipoEval(control.getTipoCalificacion());
			}
		control.setListaBandeja(listCompetencias);
	}
}
