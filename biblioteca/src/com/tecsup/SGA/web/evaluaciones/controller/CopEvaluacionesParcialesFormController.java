package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.EvaluacionesParciales;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.ControlAsistenciaCommand;
import com.tecsup.SGA.web.evaluaciones.command.EvaluacionesParcialesCommand;
import com.tecsup.SGA.web.evaluaciones.command.ParametrosEspecialesCommand;

public class CopEvaluacionesParcialesFormController  extends SimpleFormController {
	private static Log log = LogFactory
	.getLog(CopEvaluacionesParcialesFormController.class);

	private OperDocenteManager operDocenteManager;
	private MantenimientoManager mantenimientoManager;
	
	
    /*SETTER*/
	public void setMantenimientoManager(MantenimientoManager mantenimientoManager) {
		this.mantenimientoManager = mantenimientoManager;
	}
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	
	public CopEvaluacionesParcialesFormController(){
		super();
		setCommandClass(EvaluacionesParcialesCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
		throws ServletException{
		
	EvaluacionesParcialesCommand command = new EvaluacionesParcialesCommand();		
	command.setCodPeriodo((String)request.getParameter("txhCodPeriodo"));
	command.setCodEval((String)request.getParameter("txhCodEvaluador"));
	command.setCodCurso((String)request.getParameter("txhCodCurso"));
	command.setCodProducto((String)request.getParameter("txhCodProducto"));
	command.setCodEspecialidad((String)request.getParameter("txhCodEspecialidad"));    
	command.setTipoUsuarioBandeja((String)request.getParameter("txhTipoUsuarioBandeja"));
	command.setCodUsuarioBandeja((String)request.getParameter("txhCodUsuarioBandeja"));
	
	if(command.getCodPeriodo() != null && command.getCodEval() != null && command.getCodCurso() != null){
    	inicializaDataCurso(command, command.getCodPeriodo(), command.getCodEval(), command.getCodCurso());    	

    	command.setLstEvaluacionParcial(operDocenteManager.getAllTipoPracticaxCursoxEval(
				command.getCodPeriodo(),
				command.getCodCurso(),
				command.getCodEval(),
				command.getCodProducto(),
				command.getCodEspecialidad()
				));		
    }
	
	
	command.setFlagCerrarNota("0");
		
		return command;
	}

	protected void initBinder(HttpServletRequest request,
	ServletRequestDataBinder binder) {
		
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
		Long.class, nf, true));
	}


public ModelAndView processFormSubmission(HttpServletRequest request,
	HttpServletResponse response, Object command, BindException errors)
	throws Exception {
return super.processFormSubmission(request, response, command, errors);
}

public ModelAndView onSubmit(HttpServletRequest request,
	HttpServletResponse response, Object command, BindException errors){
	
	EvaluacionesParcialesCommand control = (EvaluacionesParcialesCommand) command;
	//SESSION USUARIO
	log.info("operacion>"+control.getOperacion());	
	control.setUsuario(control.getCodEval());

	
	if(control.getOperacion().equals("irRegistrar") || control.getOperacion().equals("irActualizar")){
		//JHPR 2008-06-12 Corregiendo para evitar registro de notas duplicadas.
		/*Parciales evaluacion = this.operDocenteManager.obtenerDefinicionParcial(control.getCboNroEvaluacion());			
		if (evaluacion!=null) {
			if (evaluacion.getFlagRealizado().equals("0")) {
				//No realizado (ninguna nota ha sido ingresada) => Insertar
				control = irRegistrar(request, response, errors, control);
				control = irConsultar(request, response, errors, control);				
			} else if (evaluacion.getFlagRealizado().equals("1")) {
				//Ya realizado (al menos una nota ya esta ingresada) => Actualizar
				control = irActualizar(request, response, errors, control);
				control = irConsultar(request, response, errors, control);				
			}	
		}*/
		
		List<EvaluacionesParciales> listaAlumnos = this.operDocenteManager.getAllEvaluacionesParcialesxCurso(
				control.getCodPeriodo(),control.getCodCurso(),control.getCboSeccion(),
				control.getTxtNombre(),control.getTxtApaterno(),control.getTxhNroEvaluacionInner(),
				control.getCboEvaluacionParcial(),control.getCodProducto(),
				control.getCodEspecialidad(),control.getTxtFechaJotache());
		if(listaAlumnos!=null){
			String realizado = "";
			for (EvaluacionesParciales ep : listaAlumnos){
				realizado = ep.getFlgRealizado();
			}
			
			if (realizado.equals("0")) {
				//No realizado (ninguna nota ha sido ingresada) => Insertar
				control = irRegistrar(request, response, errors, control);
				control = irConsultar(request, response, errors, control);				
			} else if (realizado.equals("1")) {
				//Ya realizado (al menos una nota ya esta ingresada) => Actualizar
				control = irActualizar(request, response, errors, control);
				control = irConsultar(request, response, errors, control);				
			}
		}
		
	}else if(control.getOperacion().equals("irConsultar")){
		control = irConsultar(request, response, errors, control);
	}else if(control.getOperacion().equals("irCambiaEvaluacionParcial")){
		control = irListaEvaluacionParcial(control);		
		control.setLstResultado(null);		
		control = irCambiaEvaluacionParcial(control);		
	}else if(control.getOperacion().equals("irCambiaSeccion")){
		control = irCambiaSeccion(control);
		control = irListaEvaluacionParcial(control);
		control = irCambiaEvaluacionParcial(control);
		control.setFlagCerrarNota("0");		
	}
		
	control.setCboNroEvaluacionReg(control.getCboNroEvaluacion());
	control.setCboSeccionReg(control.getCboSeccion());	
	
	//CONTROLANDO EL TAMA�O
	control.setHidTamanio(control.getLstResultado()==null?"0":""+control.getLstResultado().size());
	control.setTxtFechaJotache((control.getLstResultado()==null || control.getLstResultado().size()==0  || control.getTxtFechaJotache()==null )?"":""+control.getTxtFechaJotache());

	
	return new ModelAndView("/evaluaciones/cargaOperativa/eva_cop_evaluaciones",
		"control", control);
}



private EvaluacionesParcialesCommand irCambiaSeccion(
		EvaluacionesParcialesCommand control){
	
	control.setLstNroEvaluacion(operDocenteManager.getAllNroEvaxCursoxEva(
			control.getCodPeriodo(),
			control.getCodCurso(),
			control.getCodEval(),
			control.getCodProducto(),
			control.getCodEspecialidad(),
			control.getCboEvaluacionParcial(),
			control.getCboSeccion()));
	
	//log.info("irCambiaSeccion:FIN");
	return control;
}

private EvaluacionesParcialesCommand irListaEvaluacionParcial(EvaluacionesParcialesCommand control){
	control.setLstEvaluacionParcial(operDocenteManager.getAllTipoPracticaxCursoxEval(
			control.getCodPeriodo(),
			control.getCodCurso(),
			control.getCodEval(),
			control.getCodProducto(),
			control.getCodEspecialidad()
			));
	return control;
}

private EvaluacionesParcialesCommand irCambiaEvaluacionParcial(EvaluacionesParcialesCommand control) {
		
		control.setLstSeccion(operDocenteManager.getAllSeccionesxCursoxEval(
				control.getCodPeriodo(),
				control.getCodCurso(),
				control.getCodEval(),
				control.getCodProducto(),
				control.getCodEspecialidad(),
				control.getCboEvaluacionParcial()));
		
			
		return control;
	}


private EvaluacionesParcialesCommand irConsultar(HttpServletRequest request,
		HttpServletResponse response, BindException errors,
		EvaluacionesParcialesCommand control){
	
		inicializaDataCurso(control, control.getCodPeriodo(), control.getCodEval(), control.getCodCurso());		
		irConsultar(control);
		control.setSeccion(control.getCboSeccion());
		control = irCambiaSeccion(control);
		control = irListaEvaluacionParcial(control);
		control = irCambiaEvaluacionParcial(control);

		//System.out.println("** Id: Nro Definicion:"+ control.getCboNroEvaluacion());
		//JHPR: 2008-06-04 Obtener el estado del registro (EVA_DEF_NRO_EVAL_PARCIALES) para permitir o no al docente modificar nota segun estado del registro de notas.
		//this.operDocenteManager.getAllNroEvaluacionesxParciales(codPeriodo, codSeccion, codCurso, codEvaluacionParcial)
		
		Parciales definicion = this.operDocenteManager.obtenerDefinicionParcial(control.getCboNroEvaluacion());
		if (definicion!=null)
			control.setFlagCerrarNota(definicion.getFlagCerrarNota());
		else 
			control.setFlagCerrarNota("0");
		
		
		return control;
	}

private EvaluacionesParcialesCommand irRegistrar(HttpServletRequest request,
	HttpServletResponse response, BindException errors,
	EvaluacionesParcialesCommand control) {
	

	try 
	{		
		//JHPR 2008-06-27 Transacci�n
		String rpta = (String) this.mantenimientoManager.insertEvaluacioneParciales(control);
		if(rpta.equalsIgnoreCase("0"))
			request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
		else
			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
	}catch (Exception e) 
	{
		request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR+CommonMessage.CONSULTE_ADMIN);
		e.printStackTrace();
	}
	
	return control;
}

private EvaluacionesParcialesCommand irActualizar(HttpServletRequest request,
		HttpServletResponse response, BindException errors,
		EvaluacionesParcialesCommand control){


		try 
		{	String rpta = (String) this.mantenimientoManager.updateEvaluacioneParciales(control);
			if(rpta.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			
		}catch (Exception e) 
		{
			request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR+CommonMessage.CONSULTE_ADMIN);
			e.printStackTrace();
		}
		
		return control;
}


	private void inicializaDataCurso(EvaluacionesParcialesCommand command, String strPeriodo, String strEval, String strCodCurso){
		CursoEvaluador lObject;
		ArrayList dataCurso = (ArrayList)this.operDocenteManager.getAllCursos(strPeriodo, strEval,"",""
				, strCodCurso, "","");
	
		if (dataCurso.size()>0){
			lObject = (CursoEvaluador) dataCurso.get(0);
			command.setTxtPeridoVigente(lObject.getPeriodo());
			command.setTxtProfesor(lObject.getEvaluador());
			command.setTxtPrograma(lObject.getProducto());
			command.setTxtCiclo(lObject.getCiclo());
			command.setTxtCurso(lObject.getCurso());
			command.setTxtEspecialidad(lObject.getEspecialidad());
			command.setTxtSistEval(lObject.getSistEval());
		}
	}

	private void inicializaSeccion(EvaluacionesParcialesCommand command, String strCodCurso){
		command.setLstSeccion(this.operDocenteManager.getAllSeccionCurso("", strCodCurso));
	}
	
	private void irConsultar(EvaluacionesParcialesCommand command){	
		
		List<EvaluacionesParciales> lst = this.operDocenteManager.getAllEvaluacionesParcialesxCurso(
				command.getCodPeriodo(),
				command.getCodCurso(),
				command.getCboSeccion(),
				command.getTxtNombre(),
				command.getTxtApaterno(),
				command.getTxhNroEvaluacionInner(),//command.getCboNroEvaluacion()
				command.getCboEvaluacionParcial(),
				command.getCodProducto(),
				command.getCodEspecialidad(),
				command.getTxtFechaJotache());	
		
		if(lst!=null)
						
		if(lst!=null && lst.size()>0)
		{
			//command.setHidTamanio(""+lst.size());
			command.setTxtFechaJotache(((EvaluacionesParciales) lst.get(0)).getFechaEval());			
		}
		//else
		//	command.setHidTamanio("0");
		
		/*List lstx = operDocenteManager.getAllNroEvaluacionesxParciales
		(command.getCodPeriodo(),command.getCboSeccion(),command.getCodCurso(),command.getCboEvaluacionParcial());
		//log.info("lst nro eval>"+lstx.size()+">");
		command.setLstNroEvaluacion(lstx);*/
		
		//List lst2 = mantenimientoManager.getTipoPractica();
		//log.info("lsttam>"+lst2.size());
		//command.setLstEvaluacionParcial(mantenimientoManager.getTipoPracticaxCurso(command.getCodPeriodo(),command.getCodCurso()));

		command.setLstResultado(lst);
		//command.setTxhNroEvaluacionInner("");
	}

}
