package com.tecsup.SGA.web.evaluaciones.controller;
import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.service.evaluaciones.NotaExternaManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.web.reclutamiento.command.AdjuntarCVCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class RegistroNotaExternoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RegistroNotaExternoFormController.class);	
	private NotaExternaManager notaExternaManager;
	
	public void setNotaExternaManager(NotaExternaManager notaExternaManager) {
		this.notaExternaManager = notaExternaManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	RegistroNotaExternoCommand command = new RegistroNotaExternoCommand();
    	
    	String codPeriodo =(String)request.getParameter("txhCodPeriodo");
    	String codProducto =(String)request.getParameter("txhCodProducto");    	
    	String codEspecialidad =(String)request.getParameter("txhCodEspecialidad");
    	String codEtapa =(String)request.getParameter("txhCodEtapa");
    	String codPrograma =(String)request.getParameter("txhCodPrograma");
    	String codCiclo =(String)request.getParameter("txhCodCiclo");    	
    	String codCurso =(String)request.getParameter("txhCodCurso");
    	String codAlumno =(String)request.getParameter("txhCodAlumno");
    	String nombreAlumno =(String)request.getParameter("txhNombreAlumno");
    	String notaExterno=(String)request.getParameter("txhNotaExterno");
    	String codEvaluador =(String)request.getParameter("txhCodEvaluador");
    	String operacion =(String)request.getParameter("txhOperacion");    	
    	
    	command.setCodPeriodo(codPeriodo);
    	command.setCodProducto(codProducto);
    	command.setCodEspecialidad(codEspecialidad);
    	command.setCodEtapa(codEtapa);
    	command.setCodPrograma(codPrograma);
    	command.setCodCiclo(codCiclo);
    	command.setCodCurso(codCurso);
    	command.setCodAlumno(codAlumno);
    	command.setAlumno(nombreAlumno);
    	command.setNotaExterno(notaExterno);
    	command.setCodEvaluador(codEvaluador);
    	command.setOperacion(operacion);
    	
    	
        return command;
    } 	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
  
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	
    	RegistroNotaExternoCommand control = (RegistroNotaExternoCommand) command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	String resultado = "";
    	if(control.getOperacion().trim().equals("GRABAR")){
    		
    		resultado=InsertNotaExterno(control);
    		log.info("RPTA:" + resultado);
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR");
    		}
    		else{
    			control.setMsg("OK");
    		}    		
    	}
    	else{
    		if(control.getOperacion().trim().equals("MODIFICAR")){
    			
        		resultado=UpdateNotaExterno(control);
        		log.info("RPTA:" + resultado);
        		if(resultado.equals("-1")){
        			control.setMsg("ERROR");
        		}
        		else{
        			control.setMsg("OK");
        		}
        	}    		
    	}    	   	
				
	    return new ModelAndView("/evaluaciones/registroNota/eva_registro_nota_externo","control",control);		
    }
    private String UpdateNotaExterno(RegistroNotaExternoCommand control){
    	try{
    		Alumno obj= new Alumno();    		
    		
    		obj.setCodPeriodo(control.getCodPeriodo());
    		obj.setCodProducto(control.getCodProducto());    		
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setCodCurso(control.getCodCurso());    		
    		obj.setCodCiclo(control.getCodCiclo());
    		obj.setTipo(CommonConstants.COD_NOTA_EXTERNO);
    		obj.setNotaExterna(control.getNotaExterno());
    		obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodEtapa(control.getCodEtapa());
    		obj.setCodPrograma(control.getCodPrograma());
    		
    		obj.setEstado(this.notaExternaManager.UpdateNotaExterno(obj,"1")); //control.getCodEvaluador()));
    		return obj.getEstado();
    		//**************************
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String InsertNotaExterno(RegistroNotaExternoCommand control){
    	try{
    		Alumno obj= new Alumno();
    		
    		obj.setCodPeriodo(control.getCodPeriodo());
    		obj.setCodProducto(control.getCodProducto());    		
    		obj.setCodAlumno(control.getCodAlumno());
    		obj.setCodCurso(control.getCodCurso());    		
    		obj.setCodCiclo(control.getCodCiclo());
    		obj.setTipo(CommonConstants.COD_NOTA_EXTERNO);
    		obj.setNotaExterna(control.getNotaExterno());
    		obj.setCodEspecialidad(control.getCodEspecialidad());
    		obj.setCodEtapa(control.getCodEtapa());
    		obj.setCodPrograma(control.getCodPrograma());
    		
    		
    		obj.setEstado(this.notaExternaManager.InsertNotaExterno(obj, control.getCodEvaluador()));
    		return obj.getEstado();
    		//**************************
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }    
}