package com.tecsup.SGA.web.evaluaciones.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;



import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.evaluaciones.command.*;
import com.tecsup.SGA.web.reclutamiento.command.AdjuntarCVCommand;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota.InsertAdjuntoCasoCat;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.service.evaluaciones.*;

//
import org.springframework.util.FileCopyUtils;
import java.io.File;
import java.io.FilenameFilter;

public class AdjuntarArchivoCatFormController  extends SimpleFormController{

	private static Log log = LogFactory.getLog(AdjuntarArchivoCatFormController.class);	
	private NotaExternaManager notaExternaManager;
	
	public void setNotaExternaManager(NotaExternaManager notaExternaManager) {
		this.notaExternaManager = notaExternaManager;
	}
	
	public AdjuntarArchivoCatFormController(){
		super();
		setCommandClass(AdjuntarArchivoCatCommand.class);
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {    	
    	
    	AdjuntarArchivoCatCommand command = new AdjuntarArchivoCatCommand();
    	
    	request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
    	request.setAttribute("strExtensionXLS", CommonConstants.GSTR_EXTENSION_XLS);
    	request.setAttribute("strExtensionDOCX", CommonConstants.GSTR_EXTENSION_DOCX);
    	request.setAttribute("strExtensionXLSX", CommonConstants.GSTR_EXTENSION_XLSX);
    	//************************************    	
    	String codPeriodo =(String)request.getParameter("txhCodPeriodo");
    	String codProducto =(String)request.getParameter("txhCodProducto");
    	String codCiclo =(String)request.getParameter("txhCodCiclo");
    	String codEvaluador =(String)request.getParameter("txhCodEvaluador");
    	
    	command.setCodPeriodo(codPeriodo);
    	command.setCodProducto(codProducto);
    	command.setCodCiclo(codCiclo);
    	command.setCodEvaluador(codEvaluador);
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	 	
    	AdjuntarArchivoCatCommand control = (AdjuntarArchivoCatCommand)command;
    	log.info("OPERACION:" + control.getOperacion());
    	
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta;    		
    		byte[] bytesArchivo = control.getTxtArchivo();
    		
    		String fileNameArchivo="";
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		
//    		String uploadDirArchivo = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_DOCUMENTOS;            
    		String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA + CommonConstants.STR_RUTA_DOCUMENTOS;
            String sep = System.getProperty("file.separator");
            
	        if (bytesArchivo.length>0){
	        	 fileNameArchivo = CommonConstants.FILE_NAME_DOC+control.getCodPeriodo()+control.getCodProducto()+control.getCodCiclo()+"."+control.getExtArchivo();
	        	 File uploadedFileArchivo = new File(uploadDirArchivo + sep + fileNameArchivo);
	        	 FileCopyUtils.copy(bytesArchivo, uploadedFileArchivo);
	        	 control.setNomNuevoArchivo(fileNameArchivo);
	        	 rpta=InsertAdjuntoCasoCat(control);	
	        	 log.info("RPTA:" + rpta);
	        	 if(rpta.equals("-1")){
	        			control.setMsg("ERROR");
	        	 }
	        		else{
	        			control.setMsg("OK");
	        	 }
	        }
	        else{	        	
	        	//fileNameArchivo = control.getCv();
	        }
    	}
/*	        //aca se guarda en base de datos
//    		rpta = this.notaExternaManager.InsertAdjuntoCasoCat(obj, usuario)
    	
       
    	/*	//Guardando en BD nombre de archivos
    		rpta = this.reclutaManager.updateReclutaArchAdjunto(control.getIdRec(), filenameCV, filenameIMG);*/
    		
    		/*if ("-1".equals(rpta)){ //Error Operación
    			control.setMessage(CommonMessage.RECLUTA_ERROR);
    			control.setTypeMessage("ERROR");
        	}
        	else{ //OK
        		control.setOperacion("ACCESO");
        		control.setMessage(CommonMessage.GRABAR_EXITO);
        		control.setTypeMessage("OK");
        		inicializaDatos(control, control.getIdRec());
        	}
    	}
    	System.out.println("onSubmit:FIN");*/
		
		return new ModelAndView("/evaluaciones/registroNota/eva_adjuntar_archivo_cat","control",control);	    
    }
    private String InsertAdjuntoCasoCat(AdjuntarArchivoCatCommand control){
    	try{
    		Archivo obj= new Archivo();
    		
    		obj.setPeriodo(control.getCodPeriodo());
    		obj.setProducto(control.getCodProducto());
    		obj.setCiclo(control.getCodCiclo());
    		obj.setNombreNuevo(control.getNomNuevoArchivo());
    		obj.setNombre(control.getNomArchivo());
    		
    		obj.setEstado(this.notaExternaManager.InsertAdjuntoCasoCat(obj, control.getCodEvaluador()));
    		return obj.getEstado();    		
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }   
}
