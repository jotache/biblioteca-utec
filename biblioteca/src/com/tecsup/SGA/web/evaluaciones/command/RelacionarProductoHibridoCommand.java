package com.tecsup.SGA.web.evaluaciones.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class RelacionarProductoHibridoCommand {
	private String codDetalle;
	private String operacion;
	private String cursosHibrido;
	private String cursosHibridoSel;
	private String msg; 
	//************************************
	private String codigo;
	private String descripcion;
	private String cadena;
	//************************************	
	private List listaCursosHibrido;
	private List listaCursosHibridoSel;
	private List listaResultado;
	private String nroRegistros;
	private String tknCiclos;
	//************************************
	private String codPeriodo;
	private String codEvaluador;
	
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public List getListaResultado() {
		return listaResultado;
	}
	public void setListaResultado(List listaResultado) {
		this.listaResultado = listaResultado;
	}
	public String getTknCiclos() {
		return tknCiclos;
	}
	public void setTknCiclos(String tknCiclos) {
		this.tknCiclos = tknCiclos;
	}
	public List getListaCursosHibrido() {
		return listaCursosHibrido;
	}
	public void setListaCursosHibrido(List listaCursosHibrido) {
		this.listaCursosHibrido = listaCursosHibrido;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCursosHibrido() {
		return cursosHibrido;
	}
	public void setCursosHibrido(String cursosHibrido) {
		this.cursosHibrido = cursosHibrido;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCadena() {
		return cadena;
	}
	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
	
	public String getCursosHibridoSel() {
		return cursosHibridoSel;
	}
	public void setCursosHibridoSel(String cursosHibridoSel) {
		this.cursosHibridoSel = cursosHibridoSel;
	}
	public List getListaCursosHibridoSel() {
		return listaCursosHibridoSel;
	}
	public void setListaCursosHibridoSel(List listaCursosHibridoSel) {
		this.listaCursosHibridoSel = listaCursosHibridoSel;
	}
	public String getNroRegistros() {
		return nroRegistros;
	}
	public void setNroRegistros(String nroRegistros) {
		this.nroRegistros = nroRegistros;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}		
}
