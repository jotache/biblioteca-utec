package com.tecsup.SGA.web.evaluaciones.command;
import java.util.List;
public class ReportesCommand {
		
	private String operacion;
	private String sede;
	private List listSede;
	private String periodo;
	private List listPeriodo;
	private String producto;
	private List listProducto;
	private String especialidad;
	private List listEspecialidad;
	private String usuario;
	private List listUsuario;
	private List listreportes;
	private String codReporte;
	
	private String dscUsuario;
	
	public List getListreportes() {
		return listreportes;
	}
	public void setListreportes(List listreportes) {
		this.listreportes = listreportes;
	}
	public String getCodReporte() {
		return codReporte;
	}
	public void setCodReporte(String codReporte) {
		this.codReporte = codReporte;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public List getListSede() {
		return listSede;
	}
	public void setListSede(List listSede) {
		this.listSede = listSede;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public List getListPeriodo() {
		return listPeriodo;
	}
	public void setListPeriodo(List listPeriodo) {
		this.listPeriodo = listPeriodo;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public List getListProducto() {
		return listProducto;
	}
	public void setListProducto(List listProducto) {
		this.listProducto = listProducto;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public List getListEspecialidad() {
		return listEspecialidad;
	}
	public void setListEspecialidad(List listEspecialidad) {
		this.listEspecialidad = listEspecialidad;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public List getListUsuario() {
		return listUsuario;
	}
	public void setListUsuario(List listUsuario) {
		this.listUsuario = listUsuario;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}

	
}
