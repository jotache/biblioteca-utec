package com.tecsup.SGA.web.evaluaciones.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class BandejaEvaluadorCommand {
	private String operacion;
	private String typeMessage;
	private String message;
	
	private String codPeriodo;
	private String periodo;
	private String codEval;
	private String evaluador;
	private List listaCursos;
	private String codCurso; //para enviarlo
	private String codSede;
	private String codOpciones;
	
	private String ciclo;
	private String producto;
	private String curso;
	private String especialidad;
	private String sistEval;
	private String nomEvaluador;
	private String fecha;
	private String codProducto;
	private String codEspecialidad;
	
	/**/
	private String cboEvaluadores;
	private List listaEvaluadores;
	
	private String txtCurso;
	
	//agregado RNapa
	private String opcAsistencia;
	private String opcIncidencia;
	
	public String getOpcAsistencia() {
		return opcAsistencia;
	}
	public void setOpcAsistencia(String opcAsistencia) {
		this.opcAsistencia = opcAsistencia;
	}
	public String getOpcIncidencia() {
		return opcIncidencia;
	}
	public void setOpcIncidencia(String opcIncidencia) {
		this.opcIncidencia = opcIncidencia;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public List getListaCursos() {
		return listaCursos;
	}
	public void setListaCursos(List listaCursos) {
		this.listaCursos = listaCursos;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getSistEval() {
		return sistEval;
	}
	public void setSistEval(String sistEval) {
		this.sistEval = sistEval;
	}
	public String getNomEvaluador() {
		return nomEvaluador;
	}
	public void setNomEvaluador(String nomEvaluador) {
		this.nomEvaluador = nomEvaluador;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	
	
	public String getCboEvaluadores() {
		return cboEvaluadores;
	}
	public void setCboEvaluadores(String cboEvaluadores) {
		this.cboEvaluadores = cboEvaluadores;
	}
	public List getListaEvaluadores() {
		return listaEvaluadores;
	}
	public void setListaEvaluadores(List listaEvaluadores) {
		this.listaEvaluadores = listaEvaluadores;
	}
	public String getTxtCurso() {
		return txtCurso;
	}
	public void setTxtCurso(String txtCurso) {
		this.txtCurso = txtCurso;
	}
	
}
