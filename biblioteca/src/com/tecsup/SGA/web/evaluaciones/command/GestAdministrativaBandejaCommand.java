package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.CiclosCat;
import com.tecsup.SGA.modelo.Especialidad;

public class GestAdministrativaBandejaCommand {
	private String operacion;
	private String msg;
	private String codCursoSel;
	private String codUsuario;
	
	private String codPeriodoVig;
	private String dscPeriodoVig;
	
	private String codProducto;
	private List<Producto> listaProductos;
	
	private String codCiclo;
	private List<CiclosCat> listaCiclo;
	
	private String codEspecialidad;
	private List<Especialidad> listaEspecialidad;	
	private List listaCursos;
	
	//ATRIBUTOS DE LA NUEVA CABECERA
	//private String codProducto;
	private List codListaProducto;
	private String codEspecialidadPFR;
	private List codListaEspecialidadPFR;
	private String codSelCicloPFR;
	private List codListaCicloPFR;
	
	private String codSelCicloBDO;
	private List codListaCicloBDO;
	
	private String codSelCursoPFR;
	private List codListaCursoPFR;
	
		private String codSelCursoBDO;
		private List codListaCursoBDO;
	
	private String codSelCicloCAT;
	private List codListaCicloCAT;
	private String codSelCursoCAT;
	private List codListaCursoCAT;
	private String codSelProgramaPCC;
	private List codListaProgramaPCC;
	private String codSelCursoPCC;
	private List codListaCursoPCC;
	private String codSelProgramaPE;
	private List codListaProgramaPE;
	private String codSelModuloPE;
	private List codListaModuloPE;
	private String codSelCursoPE;
	private List codListaCursoPE;
	private String codSelCursoPIA;
	private List codListaCursoPIA;
	private String codSelCursoPAT;
	private List codListaCursoPAT;
	private String codSelCursoHibrido;
	private List codListaCursoHibrido;
	private String tipoProducto;
	private String programaPCCEtapa;
	private String codComponentesSeleccionados;
	private String valorEtapa;
	private String codSelCursoTECSUPVIRTUAL;
	private List codListaCursoTECSUPVIRTUAL;
	
	/***************** NOMBRES DE LAS CONSTANTES DE LOS CODIGOS DE LOS PRODUCTOS ***********************/
	private String codPFR;
	private String codPCC_I;
	private String codPCC_CC;
	private String codCAT;
	private String codPAT;
	private String codPIA;
	private String codPE;
	private String codHIBRIDO;
	private String codTecsupVirtual;
	 
	/*************** NOMBRES DE LA CONSTANTES DE LAS ETAPAS *********************/
	private String codEtapaProgramaIntegral;
	private String codEtapaCursoCorto;
	
	private List codListaTiposAdmision;
	private String codEspecialidadAdmision;
	
		
	private String codEspecialidadBDO;
	private List codListaEspecialidadBDO;
	
	//JHPR: 2/Abr/08 Para filtrar los cursos de cargo en la bandeja principal.
	private String verCargos;
	public String getVerCargos() {
		return verCargos;
	}
	public void setVerCargos(String verCargos) {
		this.verCargos = verCargos;
	}
	
	
	public String getCodPFR() {
		return codPFR;
	}
	public void setCodPFR(String codPFR) {
		this.codPFR = codPFR;
	}
	
	public String getCodPCC_I() {
		return codPCC_I;
	}
	public void setCodPCC_I(String codPCC_I) {
		this.codPCC_I = codPCC_I;
	}
	public String getCodPCC_CC() {
		return codPCC_CC;
	}
	public void setCodPCC_CC(String codPCC_CC) {
		this.codPCC_CC = codPCC_CC;
	}
	public String getCodCAT() {
		return codCAT;
	}
	public void setCodCAT(String codCAT) {
		this.codCAT = codCAT;
	}
	public String getCodPAT() {
		return codPAT;
	}
	public void setCodPAT(String codPAT) {
		this.codPAT = codPAT;
	}
	public String getCodPIA() {
		return codPIA;
	}
	public void setCodPIA(String codPIA) {
		this.codPIA = codPIA;
	}
	public String getCodPE() {
		return codPE;
	}
	public void setCodPE(String codPE) {
		this.codPE = codPE;
	}
	public String getCodHIBRIDO() {
		return codHIBRIDO;
	}
	public void setCodHIBRIDO(String codHIBRIDO) {
		this.codHIBRIDO = codHIBRIDO;
	}
	public String getCodTecsupVirtual() {
		return codTecsupVirtual;
	}
	public void setCodTecsupVirtual(String codTecsupVirtual) {
		this.codTecsupVirtual = codTecsupVirtual;
	}
	public String getCodEtapaProgramaIntegral() {
		return codEtapaProgramaIntegral;
	}
	public void setCodEtapaProgramaIntegral(String codEtapaProgramaIntegral) {
		this.codEtapaProgramaIntegral = codEtapaProgramaIntegral;
	}
	public String getCodEtapaCursoCorto() {
		return codEtapaCursoCorto;
	}
	public void setCodEtapaCursoCorto(String codEtapaCursoCorto) {
		this.codEtapaCursoCorto = codEtapaCursoCorto;
	}
	public String getCodComponentesSeleccionados() {
		return codComponentesSeleccionados;
	}
	public void setCodComponentesSeleccionados(String codComponentesSeleccionados) {
		this.codComponentesSeleccionados = codComponentesSeleccionados;
	}
	public String getProgramaPCCEtapa() {
		return programaPCCEtapa;
	}
	public void setProgramaPCCEtapa(String programaPCCEtapa) {
		this.programaPCCEtapa = programaPCCEtapa;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public List getListaCursos() {
		return listaCursos;
	}
	public void setListaCursos(List listaCursos) {
		this.listaCursos = listaCursos;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodCursoSel() {
		return codCursoSel;
	}
	public void setCodCursoSel(String codCursoSel) {
		this.codCursoSel = codCursoSel;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodPeriodoVig() {
		return codPeriodoVig;
	}
	public void setCodPeriodoVig(String codPeriodoVig) {
		this.codPeriodoVig = codPeriodoVig;
	}
	public String getDscPeriodoVig() {
		return dscPeriodoVig;
	}
	public void setDscPeriodoVig(String dscPeriodoVig) {
		this.dscPeriodoVig = dscPeriodoVig;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public List<Producto> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public List<Especialidad> getListaEspecialidad() {
		return listaEspecialidad;
	}
	public void setListaEspecialidad(List<Especialidad> listaEspecialidad) {
		this.listaEspecialidad = listaEspecialidad;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public List<CiclosCat> getListaCiclo() {
		return listaCiclo;
	}
	public void setListaCiclo(List<CiclosCat> listaCiclo) {
		this.listaCiclo = listaCiclo;
	}
	public List getCodListaProducto() {
		return codListaProducto;
	}
	public void setCodListaProducto(List codListaProducto) {
		this.codListaProducto = codListaProducto;
	}
	public String getCodEspecialidadPFR() {
		return codEspecialidadPFR;
	}
	public void setCodEspecialidadPFR(String codEspecialidadPFR) {
		this.codEspecialidadPFR = codEspecialidadPFR;
	}
	public List getCodListaEspecialidadPFR() {
		return codListaEspecialidadPFR;
	}
	public void setCodListaEspecialidadPFR(List codListaEspecialidadPFR) {
		this.codListaEspecialidadPFR = codListaEspecialidadPFR;
	}
	public String getCodSelCicloPFR() {
		return codSelCicloPFR;
	}
	public void setCodSelCicloPFR(String codSelCicloPFR) {
		this.codSelCicloPFR = codSelCicloPFR;
	}
	public List getCodListaCicloPFR() {
		return codListaCicloPFR;
	}
	public void setCodListaCicloPFR(List codListaCicloPFR) {
		this.codListaCicloPFR = codListaCicloPFR;
	}
	public String getCodSelCursoPFR() {
		return codSelCursoPFR;
	}
	public void setCodSelCursoPFR(String codSelCursoPFR) {
		this.codSelCursoPFR = codSelCursoPFR;
	}
	public List getCodListaCursoPFR() {
		return codListaCursoPFR;
	}
	public void setCodListaCursoPFR(List codListaCursoPFR) {
		this.codListaCursoPFR = codListaCursoPFR;
	}
	public String getCodSelCicloCAT() {
		return codSelCicloCAT;
	}
	public void setCodSelCicloCAT(String codSelCicloCAT) {
		this.codSelCicloCAT = codSelCicloCAT;
	}
	public List getCodListaCicloCAT() {
		return codListaCicloCAT;
	}
	public void setCodListaCicloCAT(List codListaCicloCAT) {
		this.codListaCicloCAT = codListaCicloCAT;
	}
	public String getCodSelCursoCAT() {
		return codSelCursoCAT;
	}
	public void setCodSelCursoCAT(String codSelCursoCAT) {
		this.codSelCursoCAT = codSelCursoCAT;
	}
	public List getCodListaCursoCAT() {
		return codListaCursoCAT;
	}
	public void setCodListaCursoCAT(List codListaCursoCAT) {
		this.codListaCursoCAT = codListaCursoCAT;
	}
	public String getCodSelProgramaPCC() {
		return codSelProgramaPCC;
	}
	public void setCodSelProgramaPCC(String codSelProgramaPCC) {
		this.codSelProgramaPCC = codSelProgramaPCC;
	}
	public List getCodListaProgramaPCC() {
		return codListaProgramaPCC;
	}
	public void setCodListaProgramaPCC(List codListaProgramaPCC) {
		this.codListaProgramaPCC = codListaProgramaPCC;
	}
	public String getCodSelCursoPCC() {
		return codSelCursoPCC;
	}
	public void setCodSelCursoPCC(String codSelCursoPCC) {
		this.codSelCursoPCC = codSelCursoPCC;
	}
	public List getCodListaCursoPCC() {
		return codListaCursoPCC;
	}
	public void setCodListaCursoPCC(List codListaCursoPCC) {
		this.codListaCursoPCC = codListaCursoPCC;
	}
	public String getCodSelProgramaPE() {
		return codSelProgramaPE;
	}
	public void setCodSelProgramaPE(String codSelProgramaPE) {
		this.codSelProgramaPE = codSelProgramaPE;
	}
	public List getCodListaProgramaPE() {
		return codListaProgramaPE;
	}
	public void setCodListaProgramaPE(List codListaProgramaPE) {
		this.codListaProgramaPE = codListaProgramaPE;
	}
	public String getCodSelModuloPE() {
		return codSelModuloPE;
	}
	public void setCodSelModuloPE(String codSelModuloPE) {
		this.codSelModuloPE = codSelModuloPE;
	}
	public List getCodListaModuloPE() {
		return codListaModuloPE;
	}
	public void setCodListaModuloPE(List codListaModuloPE) {
		this.codListaModuloPE = codListaModuloPE;
	}
	public String getCodSelCursoPE() {
		return codSelCursoPE;
	}
	public void setCodSelCursoPE(String codSelCursoPE) {
		this.codSelCursoPE = codSelCursoPE;
	}
	public List getCodListaCursoPE() {
		return codListaCursoPE;
	}
	public void setCodListaCursoPE(List codListaCursoPE) {
		this.codListaCursoPE = codListaCursoPE;
	}
	public String getCodSelCursoPIA() {
		return codSelCursoPIA;
	}
	public void setCodSelCursoPIA(String codSelCursoPIA) {
		this.codSelCursoPIA = codSelCursoPIA;
	}
	public List getCodListaCursoPIA() {
		return codListaCursoPIA;
	}
	public void setCodListaCursoPIA(List codListaCursoPIA) {
		this.codListaCursoPIA = codListaCursoPIA;
	}
	public String getCodSelCursoPAT() {
		return codSelCursoPAT;
	}
	public void setCodSelCursoPAT(String codSelCursoPAT) {
		this.codSelCursoPAT = codSelCursoPAT;
	}
	public List getCodListaCursoPAT() {
		return codListaCursoPAT;
	}
	public void setCodListaCursoPAT(List codListaCursoPAT) {
		this.codListaCursoPAT = codListaCursoPAT;
	}
	public String getCodSelCursoHibrido() {
		return codSelCursoHibrido;
	}
	public void setCodSelCursoHibrido(String codSelCursoHibrido) {
		this.codSelCursoHibrido = codSelCursoHibrido;
	}
	public List getCodListaCursoHibrido() {
		return codListaCursoHibrido;
	}
	public void setCodListaCursoHibrido(List codListaCursoHibrido) {
		this.codListaCursoHibrido = codListaCursoHibrido;
	}
	public String getValorEtapa() {
		return valorEtapa;
	}
	public void setValorEtapa(String valorEtapa) {
		this.valorEtapa = valorEtapa;
	}
	public String getCodSelCursoTECSUPVIRTUAL() {
		return codSelCursoTECSUPVIRTUAL;
	}
	public void setCodSelCursoTECSUPVIRTUAL(String codSelCursoTECSUPVIRTUAL) {
		this.codSelCursoTECSUPVIRTUAL = codSelCursoTECSUPVIRTUAL;
	}
	public List getCodListaCursoTECSUPVIRTUAL() {
		return codListaCursoTECSUPVIRTUAL;
	}
	public void setCodListaCursoTECSUPVIRTUAL(List codListaCursoTECSUPVIRTUAL) {
		this.codListaCursoTECSUPVIRTUAL = codListaCursoTECSUPVIRTUAL;
	}
	public List getCodListaTiposAdmision() {
		return codListaTiposAdmision;
	}
	public void setCodListaTiposAdmision(List codListaTiposAdmision) {
		this.codListaTiposAdmision = codListaTiposAdmision;
	}
	public String getCodEspecialidadAdmision() {
		return codEspecialidadAdmision;
	}
	public void setCodEspecialidadAdmision(String codEspecialidadAdmision) {
		this.codEspecialidadAdmision = codEspecialidadAdmision;
	}
	public String getCodEspecialidadBDO() {
		return codEspecialidadBDO;
	}
	public void setCodEspecialidadBDO(String codEspecialidadBDO) {
		this.codEspecialidadBDO = codEspecialidadBDO;
	}
	public List getCodListaEspecialidadBDO() {
		return codListaEspecialidadBDO;
	}
	public void setCodListaEspecialidadBDO(List codListaEspecialidadBDO) {
		this.codListaEspecialidadBDO = codListaEspecialidadBDO;
	}
	public String getCodSelCicloBDO() {
		return codSelCicloBDO;
	}
	public void setCodSelCicloBDO(String codSelCicloBDO) {
		this.codSelCicloBDO = codSelCicloBDO;
	}
	public List getCodListaCicloBDO() {
		return codListaCicloBDO;
	}
	public void setCodListaCicloBDO(List codListaCicloBDO) {
		this.codListaCicloBDO = codListaCicloBDO;
	}
	public String getCodSelCursoBDO() {
		return codSelCursoBDO;
	}
	public void setCodSelCursoBDO(String codSelCursoBDO) {
		this.codSelCursoBDO = codSelCursoBDO;
	}
	public List getCodListaCursoBDO() {
		return codListaCursoBDO;
	}
	public void setCodListaCursoBDO(List codListaCursoBDO) {
		this.codListaCursoBDO = codListaCursoBDO;
	}
	
	
}
