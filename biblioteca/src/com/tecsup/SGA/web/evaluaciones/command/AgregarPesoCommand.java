package com.tecsup.SGA.web.evaluaciones.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class AgregarPesoCommand {
	private String operacion;
	private String typeMessage;
	private String message;
	private String ciclo;
	private String periodo;
	private String evaluador;
	private List listaCursos;
	private String codCurso;
	private String producto;
	private String curso;
	private String especialidad;
	private String sistEval;
	private List listEvaluadores;
	private String cboDocente;
	private String descripcion;
	private String codEvaluador;
	private String seccion;
	private String flag;
	private String codPeriodo;
	private String codId;
	private String codEval;
	private String codProducto;
	private String codEspecialidad;
	private String cod;
	
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getCboDocente() {
		return cboDocente;
	}
	public void setCboDocente(String cboDocente) {
		this.cboDocente = cboDocente;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public List getListaCursos() {
		return listaCursos;
	}
	public void setListaCursos(List listaCursos) {
		this.listaCursos = listaCursos;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getSistEval() {
		return sistEval;
	}
	public void setSistEval(String sistEval) {
		this.sistEval = sistEval;
	}
	public List getListEvaluadores() {
		return listEvaluadores;
	}
	public void setListEvaluadores(List listEvaluadores) {
		this.listEvaluadores = listEvaluadores;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	
	
}
