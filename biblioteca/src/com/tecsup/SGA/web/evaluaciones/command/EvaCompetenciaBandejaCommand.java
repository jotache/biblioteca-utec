package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;
public class EvaCompetenciaBandejaCommand {
	private String operacion;
	private String msg;
	
	private String codTipoSesionDefecto;
	private String periodoVigente;
	private String codPeriodo;
	private String ciclo;
	private String producto;
	private String escialidad;
	private String curso;
	private String sistemaEval;
	private String codEvaluador;
	private String nomEvaluador;
	
	private String nombreAlumno;
	private String apellidoAlumno;
	private String codSeccion;
	private List listaSeccion;
	
	private String codAlumno;
	private String nomAlumno;
	private String codCurso;
	private List listaBandeja;
	private List listaCompetencias;
	
	private String nroColumna;
	private String anchoColumna;
	private String anchoTabla;
	private String tipoCalificacion;
	private String dscCalificacion;
	
	private String cadCodCompetencias;
	private String cadCodEvaluaciones;	
	
	/*Para el registro*/
	private String cadPesoExce;
	private String cadPesoBuen;
	private String cadPesoMedi;
	private String cadPesoBaja;
	public String getCadCodCompetencias() {
		return cadCodCompetencias;
	}
	public void setCadCodCompetencias(String cadCodCompetencias) {
		this.cadCodCompetencias = cadCodCompetencias;
	}
	public String getCadCodEvaluaciones() {
		return cadCodEvaluaciones;
	}
	public void setCadCodEvaluaciones(String cadCodEvaluaciones) {
		this.cadCodEvaluaciones = cadCodEvaluaciones;
	}
	public String getNomAlumno() {
		return nomAlumno;
	}
	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPeriodoVigente() {
		return periodoVigente;
	}
	public void setPeriodoVigente(String periodoVigente) {
		this.periodoVigente = periodoVigente;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getEscialidad() {
		return escialidad;
	}
	public void setEscialidad(String escialidad) {
		this.escialidad = escialidad;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getSistemaEval() {
		return sistemaEval;
	}
	public void setSistemaEval(String sistemaEval) {
		this.sistemaEval = sistemaEval;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getNomEvaluador() {
		return nomEvaluador;
	}
	public void setNomEvaluador(String nomEvaluador) {
		this.nomEvaluador = nomEvaluador;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public String getApellidoAlumno() {
		return apellidoAlumno;
	}
	public void setApellidoAlumno(String apellidoAlumno) {
		this.apellidoAlumno = apellidoAlumno;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public List getListaSeccion() {
		return listaSeccion;
	}
	public void setListaSeccion(List listaSeccion) {
		this.listaSeccion = listaSeccion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public List getListaCompetencias() {
		return listaCompetencias;
	}
	public void setListaCompetencias(List listaCompetencias) {
		this.listaCompetencias = listaCompetencias;
	}
	public String getAnchoColumna() {
		return anchoColumna;
	}
	public void setAnchoColumna(String anchoColumna) {
		this.anchoColumna = anchoColumna;
	}
	public String getTipoCalificacion() {
		return tipoCalificacion;
	}
	public void setTipoCalificacion(String tipoCalificacion) {
		this.tipoCalificacion = tipoCalificacion;
	}
	public String getDscCalificacion() {
		return dscCalificacion;
	}
	public void setDscCalificacion(String dscCalificacion) {
		this.dscCalificacion = dscCalificacion;
	}
	public String getCodTipoSesionDefecto() {
		return codTipoSesionDefecto;
	}
	public void setCodTipoSesionDefecto(String codTipoSesionDefecto) {
		this.codTipoSesionDefecto = codTipoSesionDefecto;
	}
	public String getAnchoTabla() {
		return anchoTabla;
	}
	public void setAnchoTabla(String anchoTabla) {
		this.anchoTabla = anchoTabla;
	}
	public String getNroColumna() {
		return nroColumna;
	}
	public void setNroColumna(String nroColumna) {
		this.nroColumna = nroColumna;
	}
	public String getCadPesoExce() {
		return cadPesoExce;
	}
	public void setCadPesoExce(String cadPesoExce) {
		this.cadPesoExce = cadPesoExce;
	}
	public String getCadPesoBuen() {
		return cadPesoBuen;
	}
	public void setCadPesoBuen(String cadPesoBuen) {
		this.cadPesoBuen = cadPesoBuen;
	}
	public String getCadPesoMedi() {
		return cadPesoMedi;
	}
	public void setCadPesoMedi(String cadPesoMedi) {
		this.cadPesoMedi = cadPesoMedi;
	}
	public String getCadPesoBaja() {
		return cadPesoBaja;
	}
	public void setCadPesoBaja(String cadPesoBaja) {
		this.cadPesoBaja = cadPesoBaja;
	}
	
}
