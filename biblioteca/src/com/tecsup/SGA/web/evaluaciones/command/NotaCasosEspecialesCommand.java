package com.tecsup.SGA.web.evaluaciones.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class NotaCasosEspecialesCommand {
	
	//************************************
	private List listProductos;
	private List listCurso;
	private List listPeriodos;
	private List listAlumnos;
	private List listAlumnosCat;
	private List listCiclosCat;
	private List listCursosCat;
	private String tipoProducto;
	private String tipoCicloCat;
	private String tipoCursoCat;
	//****************************
	private String codProductoCat;	
	//*****************************
	private String tamListAlumnos;
	private String tamListAlumnosCat;
	//************************************
	private String operacion;
	//***********************************
	private String periodo01;
	private String periodo02;
	private String codPeriodo;
	private String codProducto;	
	private String codEspecialidad;
	private String codEtapa;
	private String codPrograma;
	private String codCiclo;
	private String codCurso;
	//*************************************
	private String codAlumno;
	private String nombreAlumno;
	private String apePaternoAlumno;
	private String apeMaternoAlumno;
	private String notaExterno;	
	private String codTipoProducto;
	
	private String codProductoAlumno;
	private String codEspecialidadAlumno;
	private String codEtapaAlumno;
	private String codProgramaAlumno;
	private String codCicloAlumno;
	private String codCursoAlumno;
	
	private String codEvaluador;
	private String archivo;
	private String rutaArchivo;
	//************************************
	//
	private String tipoEspecialidadPFR; 
	private String tipoCicloPFR;
	private String tipoCursoPFR;	
	private String tipoEtapaPCC;
	private String tipoProgramaPCC;	
	private String tipoCursoPCC01;
	private String tipoCursoPCC02;
	private String tipoCicloCAT;
	private String tipoCursoCAT;
	private String tipoCursoPAT;
	private String tipoCursoPIA;	
	private String tipoProgramaESP;
	private String tipoModuloESP;
	private String tipoCursoESP;
	private String tipoCursoHIB;
	private String tipoCursoVIR; 
	//************************************
	private List listEspecialidadPFR;
	private List listCicloPFR;
	private List listCursoPFR;
	private List listProgramaPCC;
	private List listCursoPCC;
	private List listCicloCAT;
	private List listCursoCAT;
	private List listCursoPAT;
	private List listCursoPIA;
	private List listProgramaESP;
	private List listModuloESP;
	private List listCursoESP;
	private List listCursoHIB;
	private List listCursoVIR; 
	//********PCC
	private String valorEtapa;
	/***************** NOMBRES DE LAS CONSTANTES DE LOS CODIGOS DE LOS PRODUCTOS ***********************/
	 private String codPFR;
	 private String codPCC;
	 private String codPCC01;
	 private String codCAT;
	 private String codPAT;
	 private String codPIA;
	 private String codPE;
	 private String codHIBRIDO;
	 private String codTecsupVirtual;
	  
	 
	 /*************** NOMBRES DE LA CONSTANTES DE LAS ETAPAS *********************/
	 private String codEtapaProgramaIntegral;
	 private String codEtapaCursoCorto;
	
	 //JHPR: 2008-04-07 mostrar el texto de per�odo vigente.
	 private String dscPeriodoVig;
	 
	//JHPR: 2008-05-05 Calculo de Nota Tecsup.
	private ConfiguracionNotaExterna datosConfigCurso;
	private List<ConfiguracionNotaExterna> listaDatosConfigCurso;
	private String cadenaDatos;
	
	public String getCadenaDatos() {
		return cadenaDatos;
	}
	public void setCadenaDatos(String cadenaDatos) {
		this.cadenaDatos = cadenaDatos;
	}
	public String getDscPeriodoVig() {
		return dscPeriodoVig;
	}
	public void setDscPeriodoVig(String dscPeriodoVig) {
		this.dscPeriodoVig = dscPeriodoVig;
	}
	
	public String getCodEtapaProgramaIntegral() {
		return codEtapaProgramaIntegral;
	}
	public void setCodEtapaProgramaIntegral(String codEtapaProgramaIntegral) {
		this.codEtapaProgramaIntegral = codEtapaProgramaIntegral;
	}
	public String getCodEtapaCursoCorto() {
		return codEtapaCursoCorto;
	}
	public void setCodEtapaCursoCorto(String codEtapaCursoCorto) {
		this.codEtapaCursoCorto = codEtapaCursoCorto;
	}
	public String getCodPFR() {
		return codPFR;
	}
	public void setCodPFR(String codPFR) {
		this.codPFR = codPFR;
	}
	public String getCodPCC() {
		return codPCC;
	}
	public void setCodPCC(String codPCC) {
		this.codPCC = codPCC;
	}
	public String getCodCAT() {
		return codCAT;
	}
	public void setCodCAT(String codCAT) {
		this.codCAT = codCAT;
	}
	public String getCodPAT() {
		return codPAT;
	}
	public void setCodPAT(String codPAT) {
		this.codPAT = codPAT;
	}
	public String getCodPIA() {
		return codPIA;
	}
	public void setCodPIA(String codPIA) {
		this.codPIA = codPIA;
	}
	public String getCodPE() {
		return codPE;
	}
	public void setCodPE(String codPE) {
		this.codPE = codPE;
	}
	public String getCodHIBRIDO() {
		return codHIBRIDO;
	}
	public void setCodHIBRIDO(String codHIBRIDO) {
		this.codHIBRIDO = codHIBRIDO;
	}
	public String getCodTecsupVirtual() {
		return codTecsupVirtual;
	}
	public void setCodTecsupVirtual(String codTecsupVirtual) {
		this.codTecsupVirtual = codTecsupVirtual;
	}
	public String getValorEtapa() {
		return valorEtapa;
	}
	public void setValorEtapa(String valorEtapa) {
		this.valorEtapa = valorEtapa;
	}
	public List getListModuloESP() {
		return listModuloESP;
	}
	public void setListModuloESP(List listModuloESP) {
		this.listModuloESP = listModuloESP;
	}
	public List getListCursoHIB() {
		return listCursoHIB;
	}
	public void setListCursoHIB(List listCursoHIB) {
		this.listCursoHIB = listCursoHIB;
	}
	public List getListCursoPIA() {
		return listCursoPIA;
	}
	public void setListCursoPIA(List listCursoPIA) {
		this.listCursoPIA = listCursoPIA;
	}
	public List getListCursoPAT() {
		return listCursoPAT;
	}
	public void setListCursoPAT(List listCursoPAT) {
		this.listCursoPAT = listCursoPAT;
	}
	public List getListCursoCAT() {
		return listCursoCAT;
	}
	public void setListCursoCAT(List listCursoCAT) {
		this.listCursoCAT = listCursoCAT;
	}
	public List getListCicloCAT() {
		return listCicloCAT;
	}
	public void setListCicloCAT(List listCicloCAT) {
		this.listCicloCAT = listCicloCAT;
	}
	public List getListCursoPFR() {
		return listCursoPFR;
	}
	public void setListCursoPFR(List listCursoPFR) {
		this.listCursoPFR = listCursoPFR;
	}
	public List getListCicloPFR() {
		return listCicloPFR;
	}
	public void setListCicloPFR(List listCicloPFR) {
		this.listCicloPFR = listCicloPFR;
	}
	public List getListEspecialidadPFR() {
		return listEspecialidadPFR;
	}
	public void setListEspecialidadPFR(List listEspecialidadPFR) {
		this.listEspecialidadPFR = listEspecialidadPFR;
	}
	public String getTipoCursoHIB() {
		return tipoCursoHIB;
	}
	public void setTipoCursoHIB(String tipoCursoHIB) {
		this.tipoCursoHIB = tipoCursoHIB;
	}
	public String getTipoProgramaESP() {
		return tipoProgramaESP;
	}
	public void setTipoProgramaESP(String tipoProgramaESP) {
		this.tipoProgramaESP = tipoProgramaESP;
	}
	public String getTipoModuloESP() {
		return tipoModuloESP;
	}
	public void setTipoModuloESP(String tipoModuloESP) {
		this.tipoModuloESP = tipoModuloESP;
	}
	public String getTipoCursoESP() {
		return tipoCursoESP;
	}
	public void setTipoCursoESP(String tipoCursoESP) {
		this.tipoCursoESP = tipoCursoESP;
	}
	public String getTipoCursoPIA() {
		return tipoCursoPIA;
	}
	public void setTipoCursoPIA(String tipoCursoPIA) {
		this.tipoCursoPIA = tipoCursoPIA;
	}
	public String getTipoCursoPAT() {
		return tipoCursoPAT;
	}
	public void setTipoCursoPAT(String tipoCursoPAT) {
		this.tipoCursoPAT = tipoCursoPAT;
	}
	public String getTipoCicloCAT() {
		return tipoCicloCAT;
	}
	public void setTipoCicloCAT(String tipoCicloCAT) {
		this.tipoCicloCAT = tipoCicloCAT;
	}
	public String getTipoCursoCAT() {
		return tipoCursoCAT;
	}
	public void setTipoCursoCAT(String tipoCursoCAT) {
		this.tipoCursoCAT = tipoCursoCAT;
	}
	public String getTipoEtapaPCC() {
		return tipoEtapaPCC;
	}
	public void setTipoEtapaPCC(String tipoEtapaPCC) {
		this.tipoEtapaPCC = tipoEtapaPCC;
	}
	public String getTipoProgramaPCC() {
		return tipoProgramaPCC;
	}
	public void setTipoProgramaPCC(String tipoProgramaPCC) {
		this.tipoProgramaPCC = tipoProgramaPCC;
	}	
	public String getTipoEspecialidadPFR() {
		return tipoEspecialidadPFR;
	}
	public void setTipoEspecialidadPFR(String tipoEspecialidadPFR) {
		this.tipoEspecialidadPFR = tipoEspecialidadPFR;
	}
	public String getTipoCicloPFR() {
		return tipoCicloPFR;
	}
	public void setTipoCicloPFR(String tipoCicloPFR) {
		this.tipoCicloPFR = tipoCicloPFR;
	}
	public String getTipoCursoPFR() {
		return tipoCursoPFR;
	}
	public void setTipoCursoPFR(String tipoCursoPFR) {
		this.tipoCursoPFR = tipoCursoPFR;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public String getNotaExterno() {
		return notaExterno;
	}
	public void setNotaExterno(String notaExterno) {
		this.notaExterno = notaExterno;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}	
	public List getListCurso() {
		return listCurso;
	}
	public void setListCurso(List listCurso) {
		this.listCurso = listCurso;
	}		
	public String getPeriodo01() {
		return periodo01;
	}
	public void setPeriodo01(String periodo01) {
		this.periodo01 = periodo01;
	}
	public String getPeriodo02() {
		return periodo02;
	}
	public void setPeriodo02(String periodo02) {
		this.periodo02 = periodo02;
	}
	public List getListPeriodos() {
		return listPeriodos;
	}
	public void setListPeriodos(List listPeriodos) {
		this.listPeriodos = listPeriodos;
	}
	public List getListAlumnos() {
		return listAlumnos;
	}
	public void setListAlumnos(List listAlumnos) {
		this.listAlumnos = listAlumnos;
	}	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodProductoCat() {
		return codProductoCat;
	}
	public void setCodProductoCat(String codProductoCat) {
		this.codProductoCat = codProductoCat;
	}
	public List getListAlumnosCat() {
		return listAlumnosCat;
	}
	public void setListAlumnosCat(List listAlumnosCat) {
		this.listAlumnosCat = listAlumnosCat;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getTamListAlumnos() {
		return tamListAlumnos;
	}
	public void setTamListAlumnos(String tamListAlumnos) {
		this.tamListAlumnos = tamListAlumnos;
	}
	public String getTamListAlumnosCat() {
		return tamListAlumnosCat;
	}
	public void setTamListAlumnosCat(String tamListAlumnosCat) {
		this.tamListAlumnosCat = tamListAlumnosCat;
	}	
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodTipoProducto() {
		return codTipoProducto;
	}
	public void setCodTipoProducto(String codTipoProducto) {
		this.codTipoProducto = codTipoProducto;
	}
	public List getListProductos() {
		return listProductos;
	}
	public void setListProductos(List listProductos) {
		this.listProductos = listProductos;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public List getListProgramaESP() {
		return listProgramaESP;
	}
	public void setListProgramaESP(List listProgramaESP) {
		this.listProgramaESP = listProgramaESP;
	}
	public List getListCursoESP() {
		return listCursoESP;
	}
	public void setListCursoESP(List listCursoESP) {
		this.listCursoESP = listCursoESP;
	}
	public List getListProgramaPCC() {
		return listProgramaPCC;
	}
	public void setListProgramaPCC(List listProgramaPCC) {
		this.listProgramaPCC = listProgramaPCC;
	}
	public List getListCursoPCC() {
		return listCursoPCC;
	}
	public void setListCursoPCC(List listCursoPCC) {
		this.listCursoPCC = listCursoPCC;
	}
	public List getListCursoVIR() {
		return listCursoVIR;
	}
	public void setListCursoVIR(List listCursoVIR) {
		this.listCursoVIR = listCursoVIR;
	}
	public String getTipoCursoVIR() {
		return tipoCursoVIR;
	}
	public void setTipoCursoVIR(String tipoCursoVIR) {
		this.tipoCursoVIR = tipoCursoVIR;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodCursoAlumno() {
		return codCursoAlumno;
	}
	public void setCodCursoAlumno(String codCursoAlumno) {
		this.codCursoAlumno = codCursoAlumno;
	}
	public String getCodProductoAlumno() {
		return codProductoAlumno;
	}
	public void setCodProductoAlumno(String codProductoAlumno) {
		this.codProductoAlumno = codProductoAlumno;
	}
	public String getTipoCicloCat() {
		return tipoCicloCat;
	}
	public void setTipoCicloCat(String tipoCicloCat) {
		this.tipoCicloCat = tipoCicloCat;
	}
	public String getTipoCursoCat() {
		return tipoCursoCat;
	}
	public void setTipoCursoCat(String tipoCursoCat) {
		this.tipoCursoCat = tipoCursoCat;
	}
	public List getListCiclosCat() {
		return listCiclosCat;
	}
	public void setListCiclosCat(List listCiclosCat) {
		this.listCiclosCat = listCiclosCat;
	}
	public List getListCursosCat() {
		return listCursosCat;
	}
	public void setListCursosCat(List listCursosCat) {
		this.listCursosCat = listCursosCat;
	}
	public String getCodEspecialidadAlumno() {
		return codEspecialidadAlumno;
	}
	public void setCodEspecialidadAlumno(String codEspecialidadAlumno) {
		this.codEspecialidadAlumno = codEspecialidadAlumno;
	}
	public String getCodEtapaAlumno() {
		return codEtapaAlumno;
	}
	public void setCodEtapaAlumno(String codEtapaAlumno) {
		this.codEtapaAlumno = codEtapaAlumno;
	}
	public String getCodProgramaAlumno() {
		return codProgramaAlumno;
	}
	public void setCodProgramaAlumno(String codProgramaAlumno) {
		this.codProgramaAlumno = codProgramaAlumno;
	}
	public String getCodCicloAlumno() {
		return codCicloAlumno;
	}
	public void setCodCicloAlumno(String codCicloAlumno) {
		this.codCicloAlumno = codCicloAlumno;
	}
	public String getTipoCursoPCC01() {
		return tipoCursoPCC01;
	}
	public void setTipoCursoPCC01(String tipoCursoPCC01) {
		this.tipoCursoPCC01 = tipoCursoPCC01;
	}
	public String getTipoCursoPCC02() {
		return tipoCursoPCC02;
	}
	public void setTipoCursoPCC02(String tipoCursoPCC02) {
		this.tipoCursoPCC02 = tipoCursoPCC02;
	}
	public String getCodPCC01() {
		return codPCC01;
	}
	public void setCodPCC01(String codPCC01) {
		this.codPCC01 = codPCC01;
	}
	public String getRutaArchivo() {
		return rutaArchivo;
	}
	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}
	public ConfiguracionNotaExterna getDatosConfigCurso() {
		return datosConfigCurso;
	}
	public void setDatosConfigCurso(ConfiguracionNotaExterna datosConfigCurso) {
		this.datosConfigCurso = datosConfigCurso;
	}
	public List<ConfiguracionNotaExterna> getListaDatosConfigCurso() {
		return listaDatosConfigCurso;
	}
	public void setListaDatosConfigCurso(
			List<ConfiguracionNotaExterna> listaDatosConfigCurso) {
		this.listaDatosConfigCurso = listaDatosConfigCurso;
	}
	public String getApePaternoAlumno() {
		return apePaternoAlumno;
	}
	public void setApePaternoAlumno(String apePaternoAlumno) {
		this.apePaternoAlumno = apePaternoAlumno;
	}
	public String getApeMaternoAlumno() {
		return apeMaternoAlumno;
	}
	public void setApeMaternoAlumno(String apeMaternoAlumno) {
		this.apeMaternoAlumno = apeMaternoAlumno;
	}	
	
}
