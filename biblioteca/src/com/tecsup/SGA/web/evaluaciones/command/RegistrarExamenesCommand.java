package com.tecsup.SGA.web.evaluaciones.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class RegistrarExamenesCommand {
	private String operacion;
	private String typeMessage;
	private String message;
	
	/*Encabezado curso*/
	private String codPeriodo;
	private String codEval;
	private String codCurso; //para enviarlo

	private String periodo;
	private String evaluador;
	private String curso;
	private String programa;
	private String especialidad;
	private String ciclo;
	private String sistEval;
	
	private List listaCursos;
	private List listaAlumnos;
	private List listaNotas;
	private List listaTipoExams;
	
	/*Para la b�squeda*/
	private String nombre;
	private String apellido;
	private List listaSeccion;
	private String seccion; //descripcion seccion
	private String primeraSeccion; // codigo 1ra seccion
	
	private String cadena;
	private String tamanhoLista;
	
	//JHPR 28/3/2008 Para registrar examens de cargo
	private String modoExamen;	
	
	
	//parametros de busqueda bandeja principal
	private String prmOperacion;
	private String prmCodSelCicloPfr;
	private String prmEspecialidadPfr;
	private String prmCodProducto;
	private String prmVerCargos;
	private String prmCodPeriodoVig;
	private String nomEvaluadorSeccion;

	private String prmCodSelCicloBdo;
	private String prmEspecialidadBdo;
	
	/**
	 * @return the nomEvaluadorSeccion
	 */
	public String getNomEvaluadorSeccion() {
		return nomEvaluadorSeccion;
	}
	/**
	 * @param nomEvaluadorSeccion the nomEvaluadorSeccion to set
	 */
	public void setNomEvaluadorSeccion(String nomEvaluadorSeccion) {
		this.nomEvaluadorSeccion = nomEvaluadorSeccion;
	}
	public String getPrmCodPeriodoVig() {
		return prmCodPeriodoVig;
	}
	public void setPrmCodPeriodoVig(String prmCodPeriodoVig) {
		this.prmCodPeriodoVig = prmCodPeriodoVig;
	}
	public String getPrmOperacion() {
		return prmOperacion;
	}
	public void setPrmOperacion(String prmOperacion) {
		this.prmOperacion = prmOperacion;
	}
	
	/*private String prmCodPeriodo;
	public String getPrmCodPeriodo() {
		return prmCodPeriodo;
	}
	public void setPrmCodPeriodo(String prmCodPeriodo) {
		this.prmCodPeriodo = prmCodPeriodo;
	}
	public String getPrmCodProducto() {
		return prmCodProducto;
	}
	public void setPrmCodProducto(String prmCodProducto) {
		this.prmCodProducto = prmCodProducto;
	}
	public String getPrmCodEspecialidad() {
		return prmCodEspecialidad;
	}
	public void setPrmCodEspecialidad(String prmCodEspecialidad) {
		this.prmCodEspecialidad = prmCodEspecialidad;
	}
	public String getPrmCodCiclo() {
		return prmCodCiclo;
	}
	public void setPrmCodCiclo(String prmCodCiclo) {
		this.prmCodCiclo = prmCodCiclo;
	}
	public String getPrmCodEtapa() {
		return prmCodEtapa;
	}
	public void setPrmCodEtapa(String prmCodEtapa) {
		this.prmCodEtapa = prmCodEtapa;
	}
	public String getPrmCodPrograma() {
		return prmCodPrograma;
	}
	public void setPrmCodPrograma(String prmCodPrograma) {
		this.prmCodPrograma = prmCodPrograma;
	}
	public String getPrmFiltrarCargos() {
		return prmFiltrarCargos;
	}
	public void setPrmFiltrarCargos(String prmFiltrarCargos) {
		this.prmFiltrarCargos = prmFiltrarCargos;
	}
	private String prmCodProducto;
	private String prmCodEspecialidad;
	private String prmCodCiclo;
	private String prmCodEtapa;
	private String prmCodPrograma;
	private String prmFiltrarCargos;	
	*/
	
	public String getPrmCodSelCicloPfr() {
		return prmCodSelCicloPfr;
	}
	public void setPrmCodSelCicloPfr(String prmCodSelCicloPfr) {
		this.prmCodSelCicloPfr = prmCodSelCicloPfr;
	}
	public String getPrmEspecialidadPfr() {
		return prmEspecialidadPfr;
	}
	public void setPrmEspecialidadPfr(String prmEspecialidadPfr) {
		this.prmEspecialidadPfr = prmEspecialidadPfr;
	}
	public String getPrmCodProducto() {
		return prmCodProducto;
	}
	public void setPrmCodProducto(String prmCodProducto) {
		this.prmCodProducto = prmCodProducto;
	}
	public String getPrmVerCargos() {
		return prmVerCargos;
	}
	public void setPrmVerCargos(String prmVerCargos) {
		this.prmVerCargos = prmVerCargos;
	}
	public String getModoExamen() {
		return modoExamen;
	}
	public void setModoExamen(String modoExamen) {
		this.modoExamen = modoExamen;
	}
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public List getListaCursos() {
		return listaCursos;
	}
	public void setListaCursos(List listaCursos) {
		this.listaCursos = listaCursos;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public List getListaAlumnos() {
		return listaAlumnos;
	}
	public void setListaAlumnos(List listaAlumnos) {
		this.listaAlumnos = listaAlumnos;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getPrograma() {
		return programa;
	}
	public void setPrograma(String programa) {
		this.programa = programa;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getSistEval() {
		return sistEval;
	}
	public void setSistEval(String sistEval) {
		this.sistEval = sistEval;
	}
	public List getListaSeccion() {
		return listaSeccion;
	}
	public void setListaSeccion(List listaSeccion) {
		this.listaSeccion = listaSeccion;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getPrimeraSeccion() {
		return primeraSeccion;
	}
	public void setPrimeraSeccion(String primeraSeccion) {
		this.primeraSeccion = primeraSeccion;
	}
	public List getListaTipoExams() {
		return listaTipoExams;
	}
	public void setListaTipoExams(List listaTipoExams) {
		this.listaTipoExams = listaTipoExams;
	}
	public List getListaNotas() {
		return listaNotas;
	}
	public void setListaNotas(List listaNotas) {
		this.listaNotas = listaNotas;
	}
	public String getCadena() {
		return cadena;
	}
	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
	public String getTamanhoLista() {
		return tamanhoLista;
	}
	public void setTamanhoLista(String tamanhoLista) {
		this.tamanhoLista = tamanhoLista;
	}
	public String getPrmCodSelCicloBdo() {
		return prmCodSelCicloBdo;
	}
	public void setPrmCodSelCicloBdo(String prmCodSelCicloBdo) {
		this.prmCodSelCicloBdo = prmCodSelCicloBdo;
	}
	public String getPrmEspecialidadBdo() {
		return prmEspecialidadBdo;
	}
	public void setPrmEspecialidadBdo(String prmEspecialidadBdo) {
		this.prmEspecialidadBdo = prmEspecialidadBdo;
	}
}
