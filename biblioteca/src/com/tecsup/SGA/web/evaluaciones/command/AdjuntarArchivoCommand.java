package com.tecsup.SGA.web.evaluaciones.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class AdjuntarArchivoCommand {

	private String operacion;
	private String typeMessage;
	private String message;
	private String idRec;
	private String cv;
	private byte[] txtCV; //archivo a subir
	private String extCv;
	private String codProducto;
	private String codPeriodo;
	private String codCurso;
	private String codAlumno;
	private String tdCV;
	private byte[] txtc;
	private byte[] txhPath;
	private String nomArchivo;
	private String codEva;
	
	public String getCodEva() {
		return codEva;
	}
	public void setCodEva(String codEva) {
		this.codEva = codEva;
	}
	public byte[] getTxtc() {
		return txtc;
	}
	public void setTxtc(byte[] txtc) {
		this.txtc = txtc;
	}
	public String getTdCV() {
		return tdCV;
	}
	public void setTdCV(String tdCV) {
		this.tdCV = tdCV;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getIdRec() {
		return idRec;
	}
	public void setIdRec(String idRec) {
		this.idRec = idRec;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	public byte[] getTxtCV() {
		return txtCV;
	}
	public void setTxtCV(byte[] txtCV) {
		this.txtCV = txtCV;
	}
	public String getExtCv() {
		return extCv;
	}
	public void setExtCv(String extCv) {
		this.extCv = extCv;
	}
	public byte[] getTxhPath() {
		return txhPath;
	}
	public void setTxhPath(byte[] txhPath) {
		this.txhPath = txhPath;
	}
	public String getNomArchivo() {
		return nomArchivo;
	}
	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}
	
 	
}
