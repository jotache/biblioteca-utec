package com.tecsup.SGA.web.evaluaciones.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class MantenimientoCompetenciasCommand {
	private String descripcion;
	private String seleccion;
	//**********************************
	private String operacion;
	private String msg;
	private String porcentaje;
	private String valorTope;
	//**********************************
	private List listAreasNivelUno;
	//**********************************
	private String codPeriodo;
	private String codEvaluador;
	//**********************************
	
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	public List getListAreasNivelUno() {
		return listAreasNivelUno;
	}
	public void setListAreasNivelUno(List listAreasNivelUno) {
		this.listAreasNivelUno = listAreasNivelUno;
	}
	public String getValorTope() {
		return valorTope;
	}
	public void setValorTope(String valorTope) {
		this.valorTope = valorTope;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
	
	/*private String ListaNivel;
	private String operacion;
	private String codAreasNivelUno;
	private List listAreasNivelUno;
	private List listAreasNivelDos; 
	private String codAreasNivelDos;
	private String dscNivelDos;
	private String codDetalle;
	private String cadCod;
	private String nroReg;
	private String msg;
	private String competencia;
	private String porcentaje;
	

	

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCadCod() {
		return cadCod;
	}

	public void setCadCod(String cadCod) {
		this.cadCod = cadCod;
	}

	public String getNroReg() {
		return nroReg;
	}

	public void setNroReg(String nroReg) {
		this.nroReg = nroReg;
	}

	public String getCodDetalle() {
		return codDetalle;
	}

	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}

	public String getDscNivelDos() {
		return dscNivelDos;
	}

	public void setDscNivelDos(String dscNivelDos) {
		this.dscNivelDos = dscNivelDos;
	}

	public String getCodAreasNivelDos() {
		return codAreasNivelDos;
	}

	public void setCodAreasNivelDos(String codAreasNivelDos) {
		this.codAreasNivelDos = codAreasNivelDos;
	}

	public String getListaNivel() {
		return ListaNivel;
	}

	public void setListaNivel(String listaNivel) {
		ListaNivel = listaNivel;
	}

	public List getListAreasNivelUno() {
		return listAreasNivelUno;
	}

	public void setListAreasNivelUno(List listAreasNivelUno) {
		this.listAreasNivelUno = listAreasNivelUno;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodAreasNivelUno() {
		return codAreasNivelUno;
	}

	public void setCodAreasNivelUno(String codAreasNivelUno) {
		this.codAreasNivelUno = codAreasNivelUno;
	}

	public List getListAreasNivelDos() {
		return listAreasNivelDos;
	}

	public void setListAreasNivelDos(List listAreasNivelDos) {
		this.listAreasNivelDos = listAreasNivelDos;
	}

	public String getCompetencia() {
		return competencia;
	}

	public void setCompetencia(String competencia) {
		this.competencia = competencia;
	}

	public String getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}*/

	
}
