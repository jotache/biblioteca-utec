package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;

public class ConsultarEvaluacionesCommand {
	private String operacion;
	private String usuario;
	private String codEvaluador;	
	private String txtAlumno;
	private String txtPromAcumulado;
	private String txtRankinNro;
	private String txtRankinDes;
	private String cboProducto;
	private String txtEspecialidad;
	private String cboCiclo;
	private String txtPromCiclo;
	private String txtRankinCicloNro;
	private String txtRankinCicloDesc;
	private String codPeriodo;
	private String codAlumno;
	private String cboPeriodo;
	private String codEspecialidad;
	
	private List lstCiclos;
	private List lstProductos;
	private List lstPeriodos;
	private List lstResultado;
	
	
	//PROMEDIO
	private String promedioAula;
	private String promedioLab;
	private String promedioExamenes;
	private String promedioTaller;
	private String cargo;
	
	
	//PANTALLA DE DETALLES
		//private String txtAlumno;
	private String txtProducto;
		//private String txtEspecialidad;
	private String txtCiclo;
	private String txtCurso;
	private String txtPromedio;

	private List lstAsistencia;
	private List lstPruebaAula;
	private List lstPruebaLaboratorio;
	private List lstExamenes;
	private List lstPruebaTaller;
	
	//Variables para el tutor
	private String indProcedencia;	
	private String codPeriodoTutor;
	private String codCursoTutor;
	private String flgBuscarAlumnos;
	
	public String getFlgBuscarAlumnos() {
		return flgBuscarAlumnos;
	}
	public void setFlgBuscarAlumnos(String flgBuscarAlumnos) {
		this.flgBuscarAlumnos = flgBuscarAlumnos;
	}
	public List getLstPruebaTaller() {
		return lstPruebaTaller;
	}
	public void setLstPruebaTaller(List lstPruebaTaller) {
		this.lstPruebaTaller = lstPruebaTaller;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTxtAlumno() {
		return txtAlumno;
	}
	public void setTxtAlumno(String txtAlumno) {
		this.txtAlumno = txtAlumno;
	}
	public String getTxtPromAcumulado() {
		return txtPromAcumulado;
	}
	public void setTxtPromAcumulado(String txtPromAcumulado) {
		this.txtPromAcumulado = txtPromAcumulado;
	}
	public String getTxtRankinNro() {
		return txtRankinNro;
	}
	public void setTxtRankinNro(String txtRankinNro) {
		this.txtRankinNro = txtRankinNro;
	}
	public String getTxtRankinDes() {
		return txtRankinDes;
	}
	public void setTxtRankinDes(String txtRankinDes) {
		this.txtRankinDes = txtRankinDes;
	}
	public String getCboProducto() {
		return cboProducto;
	}
	public void setCboProducto(String cboProducto) {
		this.cboProducto = cboProducto;
	}
	public String getTxtEspecialidad() {
		return txtEspecialidad;
	}
	public void setTxtEspecialidad(String txtEspecialidad) {
		this.txtEspecialidad = txtEspecialidad;
	}
	public String getCboCiclo() {
		return cboCiclo;
	}
	public void setCboCiclo(String cboCiclo) {
		this.cboCiclo = cboCiclo;
	}
	public List getLstCiclos() {
		return lstCiclos;
	}
	public void setLstCiclos(List lstCiclos) {
		this.lstCiclos = lstCiclos;
	}
	public List getLstProductos() {
		return lstProductos;
	}
	public void setLstProductos(List lstProductos) {
		this.lstProductos = lstProductos;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getTxtPromCiclo() {
		return txtPromCiclo;
	}
	public void setTxtPromCiclo(String txtPromCiclo) {
		this.txtPromCiclo = txtPromCiclo;
	}
	public String getTxtRankinCicloNro() {
		return txtRankinCicloNro;
	}
	public void setTxtRankinCicloNro(String txtRankinCicloNro) {
		this.txtRankinCicloNro = txtRankinCicloNro;
	}
	public String getTxtRankinCicloDesc() {
		return txtRankinCicloDesc;
	}
	public void setTxtRankinCicloDesc(String txtRankinCicloDesc) {
		this.txtRankinCicloDesc = txtRankinCicloDesc;
	}
	public String getTxtProducto() {
		return txtProducto;
	}
	public void setTxtProducto(String txtProducto) {
		this.txtProducto = txtProducto;
	}
	public String getTxtCiclo() {
		return txtCiclo;
	}
	public void setTxtCiclo(String txtCiclo) {
		this.txtCiclo = txtCiclo;
	}
	public String getTxtCurso() {
		return txtCurso;
	}
	public void setTxtCurso(String txtCurso) {
		this.txtCurso = txtCurso;
	}
	public String getTxtPromedio() {
		return txtPromedio;
	}
	public void setTxtPromedio(String txtPromedio) {
		this.txtPromedio = txtPromedio;
	}
	public List getLstAsistencia() {
		return lstAsistencia;
	}
	public void setLstAsistencia(List lstAsistencia) {
		this.lstAsistencia = lstAsistencia;
	}
	public List getLstPruebaAula() {
		return lstPruebaAula;
	}
	public void setLstPruebaAula(List lstPruebaAula) {
		this.lstPruebaAula = lstPruebaAula;
	}
	public List getLstPruebaLaboratorio() {
		return lstPruebaLaboratorio;
	}
	public void setLstPruebaLaboratorio(List lstPruebaLaboratorio) {
		this.lstPruebaLaboratorio = lstPruebaLaboratorio;
	}
	public List getLstExamenes() {
		return lstExamenes;
	}
	public void setLstExamenes(List lstExamenes) {
		this.lstExamenes = lstExamenes;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getPromedioAula() {
		return promedioAula;
	}
	public void setPromedioAula(String promedioAula) {
		this.promedioAula = promedioAula;
	}
	public String getPromedioLab() {
		return promedioLab;
	}
	public void setPromedioLab(String promedioLab) {
		this.promedioLab = promedioLab;
	}
	public String getPromedioExamenes() {
		return promedioExamenes;
	}
	public void setPromedioExamenes(String promedioExamenes) {
		this.promedioExamenes = promedioExamenes;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCboPeriodo() {
		return cboPeriodo;
	}
	public void setCboPeriodo(String cboPeriodo) {
		this.cboPeriodo = cboPeriodo;
	}
	public List getLstPeriodos() {
		return lstPeriodos;
	}
	public void setLstPeriodos(List lstPeriodos) {
		this.lstPeriodos = lstPeriodos;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getPromedioTaller() {
		return promedioTaller;
	}
	public void setPromedioTaller(String promedioTaller) {
		this.promedioTaller = promedioTaller;
	}
	public String getIndProcedencia() {
		return indProcedencia;
	}
	public void setIndProcedencia(String indProcedencia) {
		this.indProcedencia = indProcedencia;
	}
	public String getCodPeriodoTutor() {
		return codPeriodoTutor;
	}
	public void setCodPeriodoTutor(String codPeriodoTutor) {
		this.codPeriodoTutor = codPeriodoTutor;
	}
	public String getCodCursoTutor() {
		return codCursoTutor;
	}
	public void setCodCursoTutor(String codCursoTutor) {
		this.codCursoTutor = codCursoTutor;
	}
}
