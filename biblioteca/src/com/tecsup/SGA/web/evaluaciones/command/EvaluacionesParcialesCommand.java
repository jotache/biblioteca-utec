package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;

public class EvaluacionesParcialesCommand {
	//ATRUBUTOS DE SISTEMA
	private String operacion;
	private String usuario;
	//ATRIBUTOS DE INTERFAZ	
	private String txtPeridoVigente;
	private String txtCiclo;
	private String txtPrograma;
	private String txtEspecialidad;
	private String txtCurso;
	private String txtSistEval;
	private String txtProfesor;
	private String txtNombre;
	private String txtApaterno;
	private String txtFecha;
	private String txtFechaJotache;
	
	private List lstSeccion;
	private List lstNroEvaluacion;
	private List lstEvaluacionParcial;
	
	
	private String cboSeccion;
	private String cboSeccionReg;
	private String cboNroEvaluacion;
	private String cboNroEvaluacionReg;
	private String cboEvaluacionParcial;
	private String lstcboEvaluacionParcial;
	
	private String codPeriodo;
	private String codEval;
	private String codCurso;
	private String seccion;	
	private String hidTamanio;
	private String hidToken;
	private String codProducto;
	private String codEspecialidad;
	private String txhNroEvaluacionInner;
	
	//JHPR 2008-06-04
	private String flagCerrarNota;
	
	//RESULTADO DE LA CONSULTA
	private List lstResultado;
	private String tipoUsuarioBandeja;
	private String codUsuarioBandeja;
	
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
	public String getTxtPeridoVigente() {
		return txtPeridoVigente;
	}
	public void setTxtPeridoVigente(String txtPeridoVigente) {
		this.txtPeridoVigente = txtPeridoVigente;
	}
	public String getTxtCiclo() {
		return txtCiclo;
	}
	public void setTxtCiclo(String txtCiclo) {
		this.txtCiclo = txtCiclo;
	}
	public String getTxtPrograma() {
		return txtPrograma;
	}
	public void setTxtPrograma(String txtPrograma) {
		this.txtPrograma = txtPrograma;
	}
	public String getTxtEspecialidad() {
		return txtEspecialidad;
	}
	public void setTxtEspecialidad(String txtEspecialidad) {
		this.txtEspecialidad = txtEspecialidad;
	}
	public String getTxtCurso() {
		return txtCurso;
	}
	public void setTxtCurso(String txtCurso) {
		this.txtCurso = txtCurso;
	}
	public String getTxtSistEval() {
		return txtSistEval;
	}
	public void setTxtSistEval(String txtSistEval) {
		this.txtSistEval = txtSistEval;
	}
	public String getTxtProfesor() {
		return txtProfesor;
	}
	public void setTxtProfesor(String txtProfesor) {
		this.txtProfesor = txtProfesor;
	}
	public String getTxtNombre() {
		return txtNombre;
	}
	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}
	public String getTxtApaterno() {
		return txtApaterno;
	}
	public void setTxtApaterno(String txtApaterno) {
		this.txtApaterno = txtApaterno;
	}
	public String getTxtFecha() {
		return txtFecha;
	}
	public void setTxtFecha(String txtFecha) {
		this.txtFecha = txtFecha;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public List getLstSeccion() {
		return lstSeccion;
	}
	public void setLstSeccion(List lstSeccion) {
		this.lstSeccion = lstSeccion;
	}
	public List getLstNroEvaluacion() {
		return lstNroEvaluacion;
	}
	public void setLstNroEvaluacion(List lstNroEvaluacion) {
		this.lstNroEvaluacion = lstNroEvaluacion;
	}
	public List getLstEvaluacionParcial() {
		return lstEvaluacionParcial;
	}
	public void setLstEvaluacionParcial(List lstEvaluacionParcial) {
		this.lstEvaluacionParcial = lstEvaluacionParcial;
	}
	public String getCboSeccion() {
		return cboSeccion;
	}
	public void setCboSeccion(String cboSeccion) {
		this.cboSeccion = cboSeccion;
	}
	public String getCboNroEvaluacion() {
		return cboNroEvaluacion;
	}
	public void setCboNroEvaluacion(String cboNroEvaluacion) {
		this.cboNroEvaluacion = cboNroEvaluacion;
	}
	public String getCboEvaluacionParcial() {
		return cboEvaluacionParcial;
	}
	public void setCboEvaluacionParcial(String cboEvaluacionParcial) {
		this.cboEvaluacionParcial = cboEvaluacionParcial;
	}
	public String getLstcboEvaluacionParcial() {
		return lstcboEvaluacionParcial;
	}
	public void setLstcboEvaluacionParcial(String lstcboEvaluacionParcial) {
		this.lstcboEvaluacionParcial = lstcboEvaluacionParcial;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getHidTamanio() {
		return hidTamanio;
	}
	public void setHidTamanio(String hidTamanio) {
		this.hidTamanio = hidTamanio;
	}
	public String getHidToken() {
		return hidToken;
	}
	public void setHidToken(String hidToken) {
		this.hidToken = hidToken;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCboSeccionReg() {
		return cboSeccionReg;
	}
	public void setCboSeccionReg(String cboSeccionReg) {
		this.cboSeccionReg = cboSeccionReg;
	}
	public String getCboNroEvaluacionReg() {
		return cboNroEvaluacionReg;
	}
	public void setCboNroEvaluacionReg(String cboNroEvaluacionReg) {
		this.cboNroEvaluacionReg = cboNroEvaluacionReg;
	}
	public String getTxhNroEvaluacionInner() {
		return txhNroEvaluacionInner;
	}
	public void setTxhNroEvaluacionInner(String txhNroEvaluacionInner) {
		this.txhNroEvaluacionInner = txhNroEvaluacionInner;
	}
	public String getFlagCerrarNota() {
		return flagCerrarNota;
	}
	public void setFlagCerrarNota(String flagCerrarNota) {
		this.flagCerrarNota = flagCerrarNota;
	}
	/**
	 * @return the tipoUsuarioBandeja
	 */
	public String getTipoUsuarioBandeja() {
		return tipoUsuarioBandeja;
	}
	/**
	 * @param tipoUsuarioBandeja the tipoUsuarioBandeja to set
	 */
	public void setTipoUsuarioBandeja(String tipoUsuarioBandeja) {
		this.tipoUsuarioBandeja = tipoUsuarioBandeja;
	}
	/**
	 * @return the codUsuarioBandeja
	 */
	public String getCodUsuarioBandeja() {
		return codUsuarioBandeja;
	}
	/**
	 * @param codUsuarioBandeja the codUsuarioBandeja to set
	 */
	public void setCodUsuarioBandeja(String codUsuarioBandeja) {
		this.codUsuarioBandeja = codUsuarioBandeja;
	}
	public String getTxtFechaJotache() {
		return txtFechaJotache;
	}
	public void setTxtFechaJotache(String txtFechaJotache) {
		this.txtFechaJotache = txtFechaJotache;
	}
	
}
