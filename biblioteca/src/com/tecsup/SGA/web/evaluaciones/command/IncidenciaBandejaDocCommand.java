package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;
public class IncidenciaBandejaDocCommand {
	private String operacion;
	private String msg;
	
	private String codProducto;
	private String codEspecialidad;
	private String codTipoSesionDefecto;
	
	private String periodoVigente;
	private String codPeriodo;
	private String ciclo;
	private String producto;
	private String escialidad;
	private String curso;
	private String sistemaEval;
	private String codEvaluador;
	private String nomEvaluador;
	
	private String nombreAlumno;
	private String apellidoAlumno;
	private String codSeccion;
	private List listaSeccion;
	
	private String codAlumno;
	private String nomAlumno;
	private String codCurso;
	private List listaBandeja;
	
	/*Para las excluciones*/
	private String cadCodExclusiones;
	private String cadCodAlumnos;
	private String cadIndSemestre;
	private String cadIndAcumulado;
	
	public String getNomAlumno() {
		return nomAlumno;
	}
	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPeriodoVigente() {
		return periodoVigente;
	}
	public void setPeriodoVigente(String periodoVigente) {
		this.periodoVigente = periodoVigente;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getEscialidad() {
		return escialidad;
	}
	public void setEscialidad(String escialidad) {
		this.escialidad = escialidad;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getSistemaEval() {
		return sistemaEval;
	}
	public void setSistemaEval(String sistemaEval) {
		this.sistemaEval = sistemaEval;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getNomEvaluador() {
		return nomEvaluador;
	}
	public void setNomEvaluador(String nomEvaluador) {
		this.nomEvaluador = nomEvaluador;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public String getApellidoAlumno() {
		return apellidoAlumno;
	}
	public void setApellidoAlumno(String apellidoAlumno) {
		this.apellidoAlumno = apellidoAlumno;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public List getListaSeccion() {
		return listaSeccion;
	}
	public void setListaSeccion(List listaSeccion) {
		this.listaSeccion = listaSeccion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCadCodExclusiones() {
		return cadCodExclusiones;
	}
	public void setCadCodExclusiones(String cadCodExclusiones) {
		this.cadCodExclusiones = cadCodExclusiones;
	}
	public String getCadCodAlumnos() {
		return cadCodAlumnos;
	}
	public void setCadCodAlumnos(String cadCodAlumnos) {
		this.cadCodAlumnos = cadCodAlumnos;
	}
	public String getCadIndSemestre() {
		return cadIndSemestre;
	}
	public void setCadIndSemestre(String cadIndSemestre) {
		this.cadIndSemestre = cadIndSemestre;
	}
	public String getCadIndAcumulado() {
		return cadIndAcumulado;
	}
	public void setCadIndAcumulado(String cadIndAcumulado) {
		this.cadIndAcumulado = cadIndAcumulado;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodTipoSesionDefecto() {
		return codTipoSesionDefecto;
	}
	public void setCodTipoSesionDefecto(String codTipoSesionDefecto) {
		this.codTipoSesionDefecto = codTipoSesionDefecto;
	}
	public IncidenciaBandejaDocCommand() {
		super();
		this.operacion = "";
		this.msg = "";
		this.codProducto = "";
		this.codEspecialidad = "";
		this.codTipoSesionDefecto = "";
		this.periodoVigente = "";
		this.codPeriodo = "";
		this.ciclo = "";
		this.producto = "";
		this.escialidad = "";
		this.curso = "";
		this.sistemaEval = "";
		this.codEvaluador = "";
		this.nomEvaluador = "";
		this.nombreAlumno = "";
		this.apellidoAlumno = "";
		this.codSeccion = "";
		this.listaSeccion = null;
		this.codAlumno = "";
		this.nomAlumno = "";
		this.codCurso = "";
		this.listaBandeja = null;
		this.cadCodExclusiones = "";
		this.cadCodAlumnos = "";
		this.cadIndSemestre = "";
		this.cadIndAcumulado = "";
	}	
	
	
	
}
