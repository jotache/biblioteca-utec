package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;
public class MtoConfiguracionComponentesCommand {
 private String codProducto;
 private List codListaProducto;
 private String codEspecialidadPFR;
 private List codListaEspecialidadPFR;
 private String codSelCicloPFR;
 private List codListaCicloPFR;
 private String codSelCursoPFR;
 private List codListaCursoPFR;
 private String codSelCicloCAT;
 private List codListaCicloCAT;
 private String codSelCursoCAT;
 private List codListaCursoCAT;
 private String codSelProgramaPCC;
 private List codListaProgramaPCC;
 private String codSelCursoPCC;
 private List codListaCursoPCC;
 private String codSelProgramaPE;
 private List codListaProgramaPE;
 private String codSelModuloPE;
 private List codListaModuloPE;
 private String codSelCursoPE;
 private List codListaCursoPE;
 private String codSelCursoPIA;
 private List codListaCursoPIA;
 private String codSelCursoPAT;
 private List codListaCursoPAT;
 private String codSelCursoHibrido;
 private List codListaCursoHibrido;
 private String operacion;
 private String codSelCursosCortosPCC;
 private List codListaCursosCortosPCC;
 private String codSelCursoTECSUPVIRTUAL;
 private List codListaCursoTECSUPVIRTUAL;
 
 private String descripcion;
 private List listaComponentes;
 private String codDetalle;
  
 private String tipoProducto;
 private String programaPCCEtapa;
 private String nroSelec;
 private String codComponentesSeleccionados;
 private String tipoGrabar;
 private String valorEtapa;
 private String codEvaluador;
 private String codTime;
 private String codPeriodo;
 private List listaPeriodo;
 /***************** NOMBRES DE LAS CONSTANTES DE LOS CODIGOS DE LOS PRODUCTOS ***********************/
 private String codPFR;
 private String codPCC_I;
 private String codCAT;
 private String codPAT;
 private String codPIA;
 private String codPE;
 private String codHIBRIDO;
 private String codTecsupVirtual;
 private String codPCC_CC;
 
 /*************** NOMBRES DE LA CONSTANTES DE LAS ETAPAS *********************/
 private String codEtapaProgramaIntegral;
 private String codEtapaCursoCorto;
 private String codSede;
 
public String getCodSede() {
	return codSede;
}
public void setCodSede(String codSede) {
	this.codSede = codSede;
}
public String getCodPFR() {
	return codPFR;
}
public void setCodPFR(String codPFR) {
	this.codPFR = codPFR;
}

public String getCodPCC_I() {
	return codPCC_I;
}
public void setCodPCC_I(String codPCC_I) {
	this.codPCC_I = codPCC_I;
}
public String getCodPCC_CC() {
	return codPCC_CC;
}
public void setCodPCC_CC(String codPCC_CC) {
	this.codPCC_CC = codPCC_CC;
}
public String getCodCAT() {
	return codCAT;
}
public void setCodCAT(String codCAT) {
	this.codCAT = codCAT;
}
public String getCodPAT() {
	return codPAT;
}
public void setCodPAT(String codPAT) {
	this.codPAT = codPAT;
}
public String getCodPIA() {
	return codPIA;
}
public void setCodPIA(String codPIA) {
	this.codPIA = codPIA;
}
public String getCodPE() {
	return codPE;
}
public void setCodPE(String codPE) {
	this.codPE = codPE;
}
public String getCodHIBRIDO() {
	return codHIBRIDO;
}
public void setCodHIBRIDO(String codHIBRIDO) {
	this.codHIBRIDO = codHIBRIDO;
}
public String getCodTecsupVirtual() {
	return codTecsupVirtual;
}
public void setCodTecsupVirtual(String codTecsupVirtual) {
	this.codTecsupVirtual = codTecsupVirtual;
}
public String getCodEtapaProgramaIntegral() {
	return codEtapaProgramaIntegral;
}
public void setCodEtapaProgramaIntegral(String codEtapaProgramaIntegral) {
	this.codEtapaProgramaIntegral = codEtapaProgramaIntegral;
}
public String getCodEtapaCursoCorto() {
	return codEtapaCursoCorto;
}
public void setCodEtapaCursoCorto(String codEtapaCursoCorto) {
	this.codEtapaCursoCorto = codEtapaCursoCorto;
}
public String getCodPeriodo() {
	return codPeriodo;
}
public void setCodPeriodo(String codPeriodo) {
	this.codPeriodo = codPeriodo;
}
public String getCodEvaluador() {
	return codEvaluador;
}
public void setCodEvaluador(String codEvaluador) {
	this.codEvaluador = codEvaluador;
}
public String getCodTime() {
	return codTime;
}
public void setCodTime(String codTime) {
	this.codTime = codTime;
}
public String getValorEtapa() {
	return valorEtapa;
}
public void setValorEtapa(String valorEtapa) {
	this.valorEtapa = valorEtapa;
}
public String getTipoGrabar() {
	return tipoGrabar;
}
public void setTipoGrabar(String tipoGrabar) {
	this.tipoGrabar = tipoGrabar;
}
public String getNroSelec() {
	return nroSelec;
}
public void setNroSelec(String nroSelec) {
	this.nroSelec = nroSelec;
}
public String getProgramaPCCEtapa() {
	return programaPCCEtapa;
}
public void setProgramaPCCEtapa(String programaPCCEtapa) {
	this.programaPCCEtapa = programaPCCEtapa;
}
public String getCodDetalle() {
	return codDetalle;
}
public void setCodDetalle(String codDetalle) {
	this.codDetalle = codDetalle;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public List getListaComponentes() {
	return listaComponentes;
}
public void setListaComponentes(List listaComponentes) {
	this.listaComponentes = listaComponentes;
}
public String getOperacion() {
	return operacion;
}
public void setOperacion(String operacion) {
	this.operacion = operacion;
}
public String getCodEspecialidadPFR() {
	return codEspecialidadPFR;
}
public void setCodEspecialidadPFR(String codEspecialidadPFR) {
	this.codEspecialidadPFR = codEspecialidadPFR;
}
public String getCodSelCicloPFR() {
	return codSelCicloPFR;
}
public void setCodSelCicloPFR(String codSelCicloPFR) {
	this.codSelCicloPFR = codSelCicloPFR;
}
public String getCodSelCursoPFR() {
	return codSelCursoPFR;
}
public void setCodSelCursoPFR(String codSelCursoPFR) {
	this.codSelCursoPFR = codSelCursoPFR;
}
public String getCodSelCicloCAT() {
	return codSelCicloCAT;
}
public void setCodSelCicloCAT(String codSelCicloCAT) {
	this.codSelCicloCAT = codSelCicloCAT;
}
public String getCodSelCursoCAT() {
	return codSelCursoCAT;
}
public void setCodSelCursoCAT(String codSelCursoCAT) {
	this.codSelCursoCAT = codSelCursoCAT;
}
public String getCodSelProgramaPCC() {
	return codSelProgramaPCC;
}
public void setCodSelProgramaPCC(String codSelProgramaPCC) {
	this.codSelProgramaPCC = codSelProgramaPCC;
}
public String getCodSelCursoPCC() {
	return codSelCursoPCC;
}
public void setCodSelCursoPCC(String codSelCursoPCC) {
	this.codSelCursoPCC = codSelCursoPCC;
}
public String getCodSelProgramaPE() {
	return codSelProgramaPE;
}
public void setCodSelProgramaPE(String codSelProgramaPE) {
	this.codSelProgramaPE = codSelProgramaPE;
}
public String getCodSelModuloPE() {
	return codSelModuloPE;
}
public void setCodSelModuloPE(String codSelModuloPE) {
	this.codSelModuloPE = codSelModuloPE;
}
public String getCodSelCursoPE() {
	return codSelCursoPE;
}
public void setCodSelCursoPE(String codSelCursoPE) {
	this.codSelCursoPE = codSelCursoPE;
}
public String getCodSelCursoPIA() {
	return codSelCursoPIA;
}
public void setCodSelCursoPIA(String codSelCursoPIA) {
	this.codSelCursoPIA = codSelCursoPIA;
}
public String getCodSelCursoPAT() {
	return codSelCursoPAT;
}
public void setCodSelCursoPAT(String codSelCursoPAT) {
	this.codSelCursoPAT = codSelCursoPAT;
}
public String getCodSelCursoHibrido() {
	return codSelCursoHibrido;
}
public void setCodSelCursoHibrido(String codSelCursoHibrido) {
	this.codSelCursoHibrido = codSelCursoHibrido;
}
public List getCodListaEspecialidadPFR() {
	return codListaEspecialidadPFR;
}
public void setCodListaEspecialidadPFR(List codListaEspecialidadPFR) {
	this.codListaEspecialidadPFR = codListaEspecialidadPFR;
}
public List getCodListaCicloPFR() {
	return codListaCicloPFR;
}
public void setCodListaCicloPFR(List codListaCicloPFR) {
	this.codListaCicloPFR = codListaCicloPFR;
}
public List getCodListaCursoPFR() {
	return codListaCursoPFR;
}
public void setCodListaCursoPFR(List codListaCursoPFR) {
	this.codListaCursoPFR = codListaCursoPFR;
}
public List getCodListaCicloCAT() {
	return codListaCicloCAT;
}
public void setCodListaCicloCAT(List codListaCicloCAT) {
	this.codListaCicloCAT = codListaCicloCAT;
}
public List getCodListaCursoCAT() {
	return codListaCursoCAT;
}
public void setCodListaCursoCAT(List codListaCursoCAT) {
	this.codListaCursoCAT = codListaCursoCAT;
}
public List getCodListaProgramaPCC() {
	return codListaProgramaPCC;
}
public void setCodListaProgramaPCC(List codListaProgramaPCC) {
	this.codListaProgramaPCC = codListaProgramaPCC;
}
public List getCodListaCursoPCC() {
	return codListaCursoPCC;
}
public void setCodListaCursoPCC(List codListaCursoPCC) {
	this.codListaCursoPCC = codListaCursoPCC;
}
public List getCodListaProgramaPE() {
	return codListaProgramaPE;
}
public void setCodListaProgramaPE(List codListaProgramaPE) {
	this.codListaProgramaPE = codListaProgramaPE;
}
public List getCodListaModuloPE() {
	return codListaModuloPE;
}
public void setCodListaModuloPE(List codListaModuloPE) {
	this.codListaModuloPE = codListaModuloPE;
}
public List getCodListaCursoPE() {
	return codListaCursoPE;
}
public void setCodListaCursoPE(List codListaCursoPE) {
	this.codListaCursoPE = codListaCursoPE;
}
public List getCodListaCursoPIA() {
	return codListaCursoPIA;
}
public void setCodListaCursoPIA(List codListaCursoPIA) {
	this.codListaCursoPIA = codListaCursoPIA;
}
public List getCodListaCursoPAT() {
	return codListaCursoPAT;
}
public void setCodListaCursoPAT(List codListaCursoPAT) {
	this.codListaCursoPAT = codListaCursoPAT;
}
public List getCodListaCursoHibrido() {
	return codListaCursoHibrido;
}
public void setCodListaCursoHibrido(List codListaCursoHibrido) {
	this.codListaCursoHibrido = codListaCursoHibrido;
}
public String getCodProducto() {
	return codProducto;
}
public void setCodProducto(String codProducto) {
	this.codProducto = codProducto;
}
public List getCodListaProducto() {
	return codListaProducto;
}
public void setCodListaProducto(List codListaProducto) {
	this.codListaProducto = codListaProducto;
}
public String getCodSelCursosCortosPCC() {
	return codSelCursosCortosPCC;
}
public void setCodSelCursosCortosPCC(String codSelCursosCortosPCC) {
	this.codSelCursosCortosPCC = codSelCursosCortosPCC;
}
public List getCodListaCursosCortosPCC() {
	return codListaCursosCortosPCC;
}
public void setCodListaCursosCortosPCC(List codListaCursosCortosPCC) {
	this.codListaCursosCortosPCC = codListaCursosCortosPCC;
}

public String getTipoProducto() {
	return tipoProducto;
}
public void setTipoProducto(String tipoProducto) {
	this.tipoProducto = tipoProducto;
}
public String getCodComponentesSeleccionados() {
	return codComponentesSeleccionados;
}
public void setCodComponentesSeleccionados(String codComponentesSeleccionados) {
	this.codComponentesSeleccionados = codComponentesSeleccionados;
}
public String getCodSelCursoTECSUPVIRTUAL() {
	return codSelCursoTECSUPVIRTUAL;
}
public void setCodSelCursoTECSUPVIRTUAL(String codSelCursoTECSUPVIRTUAL) {
	this.codSelCursoTECSUPVIRTUAL = codSelCursoTECSUPVIRTUAL;
}
public List getCodListaCursoTECSUPVIRTUAL() {
	return codListaCursoTECSUPVIRTUAL;
}
public void setCodListaCursoTECSUPVIRTUAL(List codListaCursoTECSUPVIRTUAL) {
	this.codListaCursoTECSUPVIRTUAL = codListaCursoTECSUPVIRTUAL;
}
public List getListaPeriodo() {
	return listaPeriodo;
}
public void setListaPeriodo(List listaPeriodo) {
	this.listaPeriodo = listaPeriodo;
}
}
