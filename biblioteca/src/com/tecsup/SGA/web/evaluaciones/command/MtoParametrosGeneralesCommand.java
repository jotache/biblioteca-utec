package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;

public class MtoParametrosGeneralesCommand {
 private String operacion;
 
 private String codProducto2;
 private String codPeriodo2;
 
 private String codTabla;
 private List codListTabla;
 
 private String longitud;
 private String valor0;
 private String valor1;
 private String valor2;
 private String valor3;
 private String valor4;
 private String valor5;
 private String valor6;
 private String valor7;
 private String valor8;
 //private String estProducto;
 //private String estPeriodo;
 private String tipo;
 private String descripcion;
 
 
 private String codProducto;
 private List codListaProducto;
 
 private String codigo;
 private List codListaPeriodo;
 
 private List listaCalificaciones;
 private List listaCalificaciones2;
 
 private String codTablaDetalle;
 private String valor;
 
 private String msg;
 private String codTime;
 private String codEvaluador;
 private String codPeriodo;
 private String estadoPeriodo;
 
 private String txtPFR;
 private String constePFR;
 
 private String consteProductoHibrido;
 private String consteProductoVirtual;
 
 private String codPeriodoAperturados;
 
public String getCodPeriodoAperturados() {
	return codPeriodoAperturados;
}
public void setCodPeriodoAperturados(String codPeriodoAperturados) {
	this.codPeriodoAperturados = codPeriodoAperturados;
}
public String getConstePFR() {
	return constePFR;
}
public void setConstePFR(String constePFR) {
	this.constePFR = constePFR;
}

public String getTxtPFR() {
	return txtPFR;
}
public void setTxtPFR(String txtPFR) {
	this.txtPFR = txtPFR;
}
public String getCodTime() {
	return codTime;
}
public void setCodTime(String codTime) {
	this.codTime = codTime;
}
public String getCodEvaluador() {
	return codEvaluador;
}
public void setCodEvaluador(String codEvaluador) {
	this.codEvaluador = codEvaluador;
}
public String getOperacion() {
	return operacion;
}
public void setOperacion(String operacion) {
	this.operacion = operacion;
}
public String getCodProducto() {
	return codProducto;
}
public void setCodProducto(String codProducto) {
	this.codProducto = codProducto;
}
public List getCodListaProducto() {
	return codListaProducto;
}
public void setCodListaProducto(List codListaProducto) {
	this.codListaProducto = codListaProducto;
}
public String getCodigo() {
	return codigo;
}
public void setCodigo(String codigo) {
	this.codigo = codigo;
}
public List getCodListaPeriodo() {
	return codListaPeriodo;
}
public void setCodListaPeriodo(List codListaPeriodo) {
	this.codListaPeriodo = codListaPeriodo;
}
public String getMsg() {
	return msg;
}
public void setMsg(String msg) {
	this.msg = msg;
}

public String getCodTabla() {
	return codTabla;
}
public void setCodTabla(String codTabla) {
	this.codTabla = codTabla;
}
public List getCodListTabla() {
	return codListTabla;
}
public void setCodListTabla(List codListTabla) {
	this.codListTabla = codListTabla;
}
public List getListaCalificaciones() {
	return listaCalificaciones;
}
public void setListaCalificaciones(List listaCalificaciones) {
	this.listaCalificaciones = listaCalificaciones;
}
public String getCodTablaDetalle() {
	return codTablaDetalle;
}
public void setCodTablaDetalle(String codTablaDetalle) {
	this.codTablaDetalle = codTablaDetalle;
}
public String getValor() {
	return valor;
}
public void setValor(String valor) {
	this.valor = valor;
}
public String getCodProducto2() {
	return codProducto2;
}
public void setCodProducto2(String codProducto2) {
	this.codProducto2 = codProducto2;
}
public String getCodPeriodo2() {
	return codPeriodo2;
}
public void setCodPeriodo2(String codPeriodo2) {
	this.codPeriodo2 = codPeriodo2;
}
public List getListaCalificaciones2() {
	return listaCalificaciones2;
}
public void setListaCalificaciones2(List listaCalificaciones2) {
	this.listaCalificaciones2 = listaCalificaciones2;
}
public String getLongitud() {
	return longitud;
}
public void setLongitud(String longitud) {
	this.longitud = longitud;
}
public String getValor0() {
	return valor0;
}
public void setValor0(String valor0) {
	this.valor0 = valor0;
}
public String getValor1() {
	return valor1;
}
public void setValor1(String valor1) {
	this.valor1 = valor1;
}
public String getValor2() {
	return valor2;
}
public void setValor2(String valor2) {
	this.valor2 = valor2;
}
public String getValor3() {
	return valor3;
}
public void setValor3(String valor3) {
	this.valor3 = valor3;
}
public String getValor4() {
	return valor4;
}
public void setValor4(String valor4) {
	this.valor4 = valor4;
}
public String getValor5() {
	return valor5;
}
public void setValor5(String valor5) {
	this.valor5 = valor5;
}
public String getValor6() {
	return valor6;
}
public void setValor6(String valor6) {
	this.valor6 = valor6;
}
public String getValor7() {
	return valor7;
}
public void setValor7(String valor7) {
	this.valor7 = valor7;
}
public String getValor8() {
	return valor8;
}
public void setValor8(String valor8) {
	this.valor8 = valor8;
}
/*public String getEstProducto() {
	return estProducto;
}
public void setEstProducto(String estProducto) {
	this.estProducto = estProducto;
}
public String getEstPeriodo() {
	return estPeriodo;
}
public void setEstPeriodo(String estPeriodo) {
	this.estPeriodo = estPeriodo;
}*/
public String getTipo() {
	return tipo;
}
public void setTipo(String tipo) {
	this.tipo = tipo;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getCodPeriodo() {
	return codPeriodo;
}
public void setCodPeriodo(String codPeriodo) {
	this.codPeriodo = codPeriodo;
}
public String getEstadoPeriodo() {
	return estadoPeriodo;
}
public void setEstadoPeriodo(String estadoPeriodo) {
	this.estadoPeriodo = estadoPeriodo;
}
public String getConsteProductoHibrido() {
	return consteProductoHibrido;
}
public void setConsteProductoHibrido(String consteProductoHibrido) {
	this.consteProductoHibrido = consteProductoHibrido;
}
public String getConsteProductoVirtual() {
	return consteProductoVirtual;
}
public void setConsteProductoVirtual(String consteProductoVirtual) {
	this.consteProductoVirtual = consteProductoVirtual;
}


}
