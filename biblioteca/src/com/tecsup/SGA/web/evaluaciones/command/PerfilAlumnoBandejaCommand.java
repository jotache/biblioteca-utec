package com.tecsup.SGA.web.evaluaciones.command;

import java.util.*;
import com.tecsup.SGA.modelo.CompetenciasPerfil;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class PerfilAlumnoBandejaCommand {

	private String operacion;
	private String msg;
	
	private String codEvaluador;
	private String dscEvaluador;
	private String codPeriodo;
	private String dscPeriodo;
	private String codCiclo;
	private String dscCiclo;
	
	private String codAlumno;
	private String nomAlumno;
	private String nombre;
	private String apellido;
	
	private String anchoColumna;	
	
	private List<TipoTablaDetalle> listaCabecera;
	private List<CompetenciasPerfil> listaBandeja;

	
	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}
	

	public String getDscEvaluador() {
		return dscEvaluador;
	}

	public void setDscEvaluador(String dscEvaluador) {
		this.dscEvaluador = dscEvaluador;
	}

	public String getCodPeriodo() {
		return codPeriodo;
	}

	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}

	public String getDscPeriodo() {
		return dscPeriodo;
	}

	public void setDscPeriodo(String dscPeriodo) {
		this.dscPeriodo = dscPeriodo;
	}

	public String getDscCiclo() {
		return dscCiclo;
	}

	public void setDscCiclo(String dscCiclo) {
		this.dscCiclo = dscCiclo;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodEvaluador() {
		return codEvaluador;
	}

	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}

	public String getCodCiclo() {
		return codCiclo;
	}

	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public List<CompetenciasPerfil> getListaBandeja() {
		return listaBandeja;
	}

	public void setListaBandeja(List<CompetenciasPerfil> listaPerfiles) {
		this.listaBandeja = listaPerfiles;
	}

	public List<TipoTablaDetalle> getListaCabecera() {
		return listaCabecera;
	}

	public void setListaCabecera(List<TipoTablaDetalle> listaCabecera) {
		this.listaCabecera = listaCabecera;
	}

	public String getAnchoColumna() {
		return anchoColumna;
	}

	public void setAnchoColumna(String anchoColumna) {
		this.anchoColumna = anchoColumna;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getNomAlumno() {
		return nomAlumno;
	}

	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	
	
}
