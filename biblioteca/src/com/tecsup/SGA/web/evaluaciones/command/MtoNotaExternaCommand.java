package com.tecsup.SGA.web.evaluaciones.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class MtoNotaExternaCommand {
	private String operacion;
	private String seleccionCheck;
	private String seleccionButton; 	
	private List listCursos;
	
	private String codPeriodo;
	private String codEvaluador;

	public String getCodPeriodo() {
		return codPeriodo;
	}

	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}

	public String getCodEvaluador() {
		return codEvaluador;
	}

	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}

	public List getListCursos() {
		return listCursos;
	}

	public void setListCursos(List listCursos) {
		this.listCursos = listCursos;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getSeleccionCheck() {
		return seleccionCheck;
	}

	public void setSeleccionCheck(String seleccionCheck) {
		this.seleccionCheck = seleccionCheck;
	}

	public String getSeleccionButton() {
		return seleccionButton;
	}

	public void setSeleccionButton(String seleccionButton) {
		this.seleccionButton = seleccionButton;
	}	
	
}
