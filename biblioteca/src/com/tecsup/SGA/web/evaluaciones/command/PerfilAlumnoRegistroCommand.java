package com.tecsup.SGA.web.evaluaciones.command;

import java.util.*;
import com.tecsup.SGA.modelo.CompetenciasPerfil;

public class PerfilAlumnoRegistroCommand {

	private String operacion;
	private String msg;
	
	private String codEvaluador;
	private String codPeriodo;
	private String codAlumno;
	private String dscAlumno;
	
	private List<CompetenciasPerfil> listaBandeja;

	private String cadCodCompetencia;
	private String cadCodRespuesta;
	private String indTipo;
	private String dscCalificacion;
	
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getDscAlumno() {
		return dscAlumno;
	}
	public void setDscAlumno(String dscAlumno) {
		this.dscAlumno = dscAlumno;
	}
	public List<CompetenciasPerfil> getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List<CompetenciasPerfil> listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getCadCodCompetencia() {
		return cadCodCompetencia;
	}
	public void setCadCodCompetencia(String cadCodCompetencia) {
		this.cadCodCompetencia = cadCodCompetencia;
	}
	public String getCadCodRespuesta() {
		return cadCodRespuesta;
	}
	public void setCadCodRespuesta(String cadCodRespuesta) {
		this.cadCodRespuesta = cadCodRespuesta;
	}
	public String getIndTipo() {
		return indTipo;
	}	
	public void setIndTipo(String indTipo) {
		this.indTipo = indTipo;
	}
	public String getDscCalificacion() {
		return dscCalificacion;
	}
	public void setDscCalificacion(String dscCalificacion) {
		this.dscCalificacion = dscCalificacion;
	}
	
	

}
