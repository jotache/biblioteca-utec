package com.tecsup.SGA.web.evaluaciones.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class RegistrarEmpresaCommand {
	private String nombreAlumno;
	private String codAlumno;
	private String cadena;
	private String tipoPractica;
	private String codEspecialidad;
	private String empresa;
	private String fechaInicio;
	private String fechaFin;
	private String tipo;
	private String operacion;
	private String valida;
	private String cant;
	private String estado;
	private String empresa1;
	private String empresa2;
	private String cant1;
	private String msg;
	private String fechaInicio1;
	private String fechaFin1;
	private String horas;
	private String horasPasatia;
	private String horasParciales;
	private String men;
	private String emp1;
	private String emp2;
	private String emp3;
	private String emp4;
	private String fini1;
	private String fini2;
	private String fini3;
	private String fini4;
	private String ffin1;
	private String ffin2;
	private String ffin3;
	private String ffin4;
	private String ca1;
	private String ca2;
	private String ca3;
	private String ca4;
	private String ob1;
	private String ob2;
	private String ob3;
	private String ob4;
	private int totemp;
	private List listEmpresas;
	private int flag;
	private String modificado;
	private String mens;
	private String codId1;
	private String codId2;
	private String codId3;
	private String codId4;
	private String codId;
	private List listEmpresasPractica;
	private List listInformes;
	private String inf;
	private List listEmpresasPasasntia;
    private String codPeriodo;              
	private String codEva;
	private String cantHoras;
	private String codFlagApto;
	private String codFlagNoApto;
	private String ver;
	private String vez;
	private String observacion;
	private String caHoras;
	
	private String nota;
	private String indSabado;
	private String indDomingo;
	private String horaIni;
	private String horaFin;
	private String sab;
	private String dom;
	
	private String obser;
	private String not;
	private String horaInicio;
	private String horafinaliza;
	private String selSabado;
	private String selDomingo;

	//JHPR: 2008-06-05 Registrar formación de empresa en masa.
	private String codAlumnosSelec; 
	private List<Alumno> alumnosSeleccionados;
	private String tipoIngreso="";
	private String tipoIngreso2="";	
	private String tipoIngreso3="";	
	private String f1NomEmpresa;
	private String f1FechaIni;
 	private String f1FechaFin;
 	private String f1Obs;
 	private String f1CantHrs;
 	private String ids;
 	private String ids2;
 	private String ids3;
 	private String f2NomEmpresa;
 	private String f2Cantidad;
 	private String f3NomEmpresa;
	private String f3FechaIni;
 	private String f3FechaFin;
 	private String f3Cantidad;
 	private String f3HoraIni;
 	private String f3HoraFin;
 	private String f3Nota;
 	
	public String getF3HoraIni() {
		return f3HoraIni;
	}

	public void setF3HoraIni(String horaIni) {
		f3HoraIni = horaIni;
	}

	public String getF3HoraFin() {
		return f3HoraFin;
	}

	public void setF3HoraFin(String horaFin) {
		f3HoraFin = horaFin;
	}

	public String getF2Cantidad() {
		return f2Cantidad;
	}

	public void setF2Cantidad(String cantidad) {
		f2Cantidad = cantidad;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getSelDomingo() {
		return selDomingo;
	}

	public String getSab() {
		return sab;
	}

	public void setSab(String sab) {
		this.sab = sab;
	}

	public String getDom() {
		return dom;
	}

	public void setDom(String dom) {
		this.dom = dom;
	}

	public String getHoraIni() {
		return horaIni;
	}

	public void setHoraIni(String horaIni) {
		this.horaIni = horaIni;
	}

	public String getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	public String getIndSabado() {
		return indSabado;
	}

	public void setIndSabado(String indSabado) {
		this.indSabado = indSabado;
	}

	public String getIndDomingo() {
		return indDomingo;
	}

	public void setIndDomingo(String indDomingo) {
		this.indDomingo = indDomingo;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getVez() {
		return vez;
	}

	public void setVez(String vez) {
		this.vez = vez;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getCodFlagApto() {
		return codFlagApto;
	}

	public void setCodFlagApto(String codFlagApto) {
		this.codFlagApto = codFlagApto;
	}

	public String getCodFlagNoApto() {
		return codFlagNoApto;
	}

	public void setCodFlagNoApto(String codFlagNoApto) {
		this.codFlagNoApto = codFlagNoApto;
	}

	public String getCantHoras() {
		return cantHoras;
	}

	public void setCantHoras(String cantHoras) {
		this.cantHoras = cantHoras;
	}

	public String getCodEva() {
		return codEva;
	}

	public void setCodEva(String codEva) {
		this.codEva = codEva;
	}

	public String getCodPeriodo() {
		return codPeriodo;
	}

	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}

	public List getListInformes() {
		return listInformes;
	}

	public void setListInformes(List listInformes) {
		this.listInformes = listInformes;
	}

	public String getInf() {
		return inf;
	}

	public void setInf(String inf) {
		this.inf = inf;
	}

	public List getListEmpresasPractica() {
		return listEmpresasPractica;
	}

	public void setListEmpresasPractica(List listEmpresasPractica) {
		this.listEmpresasPractica = listEmpresasPractica;
	}

	public String getCodId() {
		return codId;
	}

	public void setCodId(String codId) {
		this.codId = codId;
	}

	public String getCodId1() {
		return codId1;
	}

	public void setCodId1(String codId1) {
		this.codId1 = codId1;
	}

	public String getCodId2() {
		return codId2;
	}

	public void setCodId2(String codId2) {
		this.codId2 = codId2;
	}

	

	public String getMens() {
		return mens;
	}

	public void setMens(String mens) {
		this.mens = mens;
	}

	public String getModificado() {
		return modificado;
	}

	public void setModificado(String modificado) {
		this.modificado = modificado;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public List getListEmpresas() {
		return listEmpresas;
	}

	public void setListEmpresas(List listEmpresas) {
		this.listEmpresas = listEmpresas;
	}

	public int getTotemp() {
		return totemp;
	}

	public void setTotemp(int totemp) {
		this.totemp = totemp;
	}

	public String getFini1() {
		return fini1;
	}

	public void setFini1(String fini1) {
		this.fini1 = fini1;
	}

	public String getFini2() {
		return fini2;
	}

	public void setFini2(String fini2) {
		this.fini2 = fini2;
	}

	public String getFini3() {
		return fini3;
	}

	public void setFini3(String fini3) {
		this.fini3 = fini3;
	}

	public String getFfin1() {
		return ffin1;
	}

	public void setFfin1(String ffin1) {
		this.ffin1 = ffin1;
	}

	public String getFfin2() {
		return ffin2;
	}

	public void setFfin2(String ffin2) {
		this.ffin2 = ffin2;
	}

	public String getFfin3() {
		return ffin3;
	}

	public void setFfin3(String ffin3) {
		this.ffin3 = ffin3;
	}

	public String getMen() {
		return men;
	}

	public void setMen(String men) {
		this.men = men;
	}

	public String getHoras() {
		return horas;
	}

	public void setHoras(String horas) {
		this.horas = horas;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getEmpresa1() {
		return empresa1;
	}

	public void setEmpresa1(String empresa1) {
		this.empresa1 = empresa1;
	}

	public String getEmpresa2() {
		return empresa2;
	}

	public void setEmpresa2(String empresa2) {
		this.empresa2 = empresa2;
	}

	public String getCant1() {
		return cant1;
	}

	public void setCant1(String cant1) {
		this.cant1 = cant1;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCant() {
		return cant;
	}

	public void setCant(String cant) {
		this.cant = cant;
	}

	public String getValida() {
		return valida;
	}

	public void setValida(String valida) {
		this.valida = valida;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getCodEspecialidad() {
		return codEspecialidad;
	}

	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}

	public String getTipoPractica() {
		return tipoPractica;
	}

	public void setTipoPractica(String tipoPractica) {
		this.tipoPractica = tipoPractica;
	}

	public String getCadena() {
		return cadena;
	}

	public void setCadena(String cadena) {
		this.cadena = cadena;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getNombreAlumno() {
		return nombreAlumno;
	}

	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getFechaInicio1() {
		return fechaInicio1;
	}

	public void setFechaInicio1(String fechaInicio1) {
		this.fechaInicio1 = fechaInicio1;
	}

	public String getFechaFin1() {
		return fechaFin1;
	}

	public void setFechaFin1(String fechaFin1) {
		this.fechaFin1 = fechaFin1;
	}

	public String getHorasPasatia() {
		return horasPasatia;
	}

	public void setHorasPasatia(String horasPasatia) {
		this.horasPasatia = horasPasatia;
	}

	public String getHorasParciales() {
		return horasParciales;
	}

	public void setHorasParciales(String horasParciales) {
		this.horasParciales = horasParciales;
	}

	public String getEmp1() {
		return emp1;
	}

	public void setEmp1(String emp1) {
		this.emp1 = emp1;
	}

	public String getEmp2() {
		return emp2;
	}

	public void setEmp2(String emp2) {
		this.emp2 = emp2;
	}

	public String getEmp3() {
		return emp3;
	}

	public void setEmp3(String emp3) {
		this.emp3 = emp3;
	}

	public String getCodId3() {
		return codId3;
	}

	public void setCodId3(String codId3) {
		this.codId3 = codId3;
	}

	public List getListEmpresasPasasntia() {
		return listEmpresasPasasntia;
	}

	public void setListEmpresasPasasntia(List listEmpresasPasasntia) {
		this.listEmpresasPasasntia = listEmpresasPasasntia;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getCaHoras() {
		return caHoras;
	}

	public void setCaHoras(String caHoras) {
		this.caHoras = caHoras;
	}

	public String getCa1() {
		return ca1;
	}

	public void setCa1(String ca1) {
		this.ca1 = ca1;
	}

	public String getCa2() {
		return ca2;
	}

	public void setCa2(String ca2) {
		this.ca2 = ca2;
	}

	public String getCa3() {
		return ca3;
	}

	public String getOb1() {
		return ob1;
	}

	public void setOb1(String ob1) {
		this.ob1 = ob1;
	}

	public String getOb2() {
		return ob2;
	}

	public void setOb2(String ob2) {
		this.ob2 = ob2;
	}

	public String getOb3() {
		return ob3;
	}

	public void setOb3(String ob3) {
		this.ob3 = ob3;
	}

	public void setCa3(String ca3) {
		this.ca3 = ca3;
	}

	public String getObser() {
		return obser;
	}

	public void setObser(String obser) {
		this.obser = obser;
	}

	public String getNot() {
		return not;
	}

	public void setNot(String not) {
		this.not = not;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHorafinaliza() {
		return horafinaliza;
	}

	public void setHorafinaliza(String horafinaliza) {
		this.horafinaliza = horafinaliza;
	}

	public String getSelSabado() {
		return selSabado;
	}

	public void setSelSabado(String selSabado) {
		this.selSabado = selSabado;
	}

	public void setSelDomingo(String selDomingo) {
		this.selDomingo = selDomingo;
	}



	public List<Alumno> getAlumnosSeleccionados() {
		return alumnosSeleccionados;
	}

	public void setAlumnosSeleccionados(List<Alumno> alumnosSeleccionados) {
		this.alumnosSeleccionados = alumnosSeleccionados;
	}

	public String getCodAlumnosSelec() {
		return codAlumnosSelec;
	}

	public void setCodAlumnosSelec(String codAlumnosSelec) {
		this.codAlumnosSelec = codAlumnosSelec;
	}

	public String getTipoIngreso() {
		return tipoIngreso;
	}

	public void setTipoIngreso(String tipoIngreso) {
		this.tipoIngreso = tipoIngreso;
	}

	public String getF1NomEmpresa() {
		return f1NomEmpresa;
	}

	public void setF1NomEmpresa(String nomEmpresa) {
		f1NomEmpresa = nomEmpresa;
	}

	public String getF1FechaIni() {
		return f1FechaIni;
	}

	public void setF1FechaIni(String fechaIni) {
		f1FechaIni = fechaIni;
	}

	public String getF1FechaFin() {
		return f1FechaFin;
	}

	public void setF1FechaFin(String fechaFin) {
		f1FechaFin = fechaFin;
	}

	public String getF1Obs() {
		return f1Obs;
	}

	public void setF1Obs(String obs) {
		f1Obs = obs;
	}

	public String getF1CantHrs() {
		return f1CantHrs;
	}

	public void setF1CantHrs(String cantHrs) {
		f1CantHrs = cantHrs;
	}

	public String getTipoIngreso2() {
		return tipoIngreso2;
	}

	public void setTipoIngreso2(String tipoIngreso2) {
		this.tipoIngreso2 = tipoIngreso2;
	}

	public String getTipoIngreso3() {
		return tipoIngreso3;
	}

	public void setTipoIngreso3(String tipoIngreso3) {
		this.tipoIngreso3 = tipoIngreso3;
	}

	public String getIds2() {
		return ids2;
	}

	public void setIds2(String ids2) {
		this.ids2 = ids2;
	}

	public String getIds3() {
		return ids3;
	}

	public void setIds3(String ids3) {
		this.ids3 = ids3;
	}

	public String getF2NomEmpresa() {
		return f2NomEmpresa;
	}

	public void setF2NomEmpresa(String nomEmpresa) {
		f2NomEmpresa = nomEmpresa;
	}

	public String getF3NomEmpresa() {
		return f3NomEmpresa;
	}

	public void setF3NomEmpresa(String nomEmpresa) {
		f3NomEmpresa = nomEmpresa;
	}

	public String getF3FechaIni() {
		return f3FechaIni;
	}

	public void setF3FechaIni(String fechaIni) {
		f3FechaIni = fechaIni;
	}

	public String getF3FechaFin() {
		return f3FechaFin;
	}

	public void setF3FechaFin(String fechaFin) {
		f3FechaFin = fechaFin;
	}

	public String getF3Cantidad() {
		return f3Cantidad;
	}

	public void setF3Cantidad(String cantidad) {
		f3Cantidad = cantidad;
	}

	public String getF3Nota() {
		return f3Nota;
	}

	public void setF3Nota(String nota) {
		f3Nota = nota;
	}

	public String getEmp4() {
		return emp4;
	}

	public void setEmp4(String emp4) {
		this.emp4 = emp4;
	}

	public String getFini4() {
		return fini4;
	}

	public void setFini4(String fini4) {
		this.fini4 = fini4;
	}

	public String getFfin4() {
		return ffin4;
	}

	public void setFfin4(String ffin4) {
		this.ffin4 = ffin4;
	}

	public String getCa4() {
		return ca4;
	}

	public void setCa4(String ca4) {
		this.ca4 = ca4;
	}

	public String getOb4() {
		return ob4;
	}

	public void setOb4(String ob4) {
		this.ob4 = ob4;
	}

	public String getCodId4() {
		return codId4;
	}

	public void setCodId4(String codId4) {
		this.codId4 = codId4;
	}
	
	
}
