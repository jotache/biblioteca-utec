package com.tecsup.SGA.web.evaluaciones.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class ControlAsistenciaCommand {
	private String operacion;
	private String typeMessage;
	private String message;
	
	/*Encabezado curso*/
	private String codPeriodo;
	private String codEval;
	private String codCurso; //para enviarlo
	private String codProducto;
	private String codEspecialidad;

	private String periodo;
	private String evaluador;
	private String curso;
	private String programa;
	private String especialidad;
	private String ciclo;
	private String sistEval;
	
	private List listaCursos;
	private List listaAlumnos;
	
	/*TipoSesionXCurso*/
	private List listaTipoSesion;
	private String tipoSesion;
	
	/*Para la b�squeda*/
	private String nombre;
	private String apellido;
	private List listaSeccion;
	private String seccion; //descripcion seccion
	private String primeraSeccion; // codigo 1ra seccion
	private String flagCerrarAsistencia;
	private String tipoUsuarioBandeja;
	private String codUsuarioBandeja;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public List getListaCursos() {
		return listaCursos;
	}
	public void setListaCursos(List listaCursos) {
		this.listaCursos = listaCursos;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public List getListaAlumnos() {
		return listaAlumnos;
	}
	public void setListaAlumnos(List listaAlumnos) {
		this.listaAlumnos = listaAlumnos;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getPrograma() {
		return programa;
	}
	public void setPrograma(String programa) {
		this.programa = programa;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getSistEval() {
		return sistEval;
	}
	public void setSistEval(String sistEval) {
		this.sistEval = sistEval;
	}
	public List getListaSeccion() {
		return listaSeccion;
	}
	public void setListaSeccion(List listaSeccion) {
		this.listaSeccion = listaSeccion;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getPrimeraSeccion() {
		return primeraSeccion;
	}
	public void setPrimeraSeccion(String primeraSeccion) {
		this.primeraSeccion = primeraSeccion;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public List getListaTipoSesion() {
		return listaTipoSesion;
	}
	public void setListaTipoSesion(List listaTipoSesion) {
		this.listaTipoSesion = listaTipoSesion;
	}
	public String getTipoSesion() {
		return tipoSesion;
	}
	public void setTipoSesion(String tipoSesion) {
		this.tipoSesion = tipoSesion;
	}
	public String getFlagCerrarAsistencia() {
		return flagCerrarAsistencia;
	}
	public void setFlagCerrarAsistencia(String flagCerrarAsistencia) {
		this.flagCerrarAsistencia = flagCerrarAsistencia;
	}
	public String getTipoUsuarioBandeja() {
		return tipoUsuarioBandeja;
	}
	public void setTipoUsuarioBandeja(String tipoUsuarioBandeja) {
		this.tipoUsuarioBandeja = tipoUsuarioBandeja;
	}
	public String getCodUsuarioBandeja() {
		return codUsuarioBandeja;
	}
	public void setCodUsuarioBandeja(String codUsuarioBandeja) {
		this.codUsuarioBandeja = codUsuarioBandeja;
	}
}
