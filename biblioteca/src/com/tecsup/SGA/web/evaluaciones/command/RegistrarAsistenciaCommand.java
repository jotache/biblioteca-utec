package com.tecsup.SGA.web.evaluaciones.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class RegistrarAsistenciaCommand {
	private String operacion;
	private String typeMessage;
	private String message;
	
	/*Encabezado curso*/
	private String codPeriodo;
	private String codEval;
	private String codCurso;
	private String codProducto;
	private String codEspecialidad;
	private String codSeccion;
	private String seccion;
	private String curso;
	private String sistEval;
	private String nomEval;
	private String fe;
	private int nro;

	
	private String nombre;
	private String apellido;
	private String idAsistBusqueda; //id de la asistencia a buscar

	private List listaAlumnos;
	private List listaFechas;
	private String fechaActual;
	
	private String nroSesion;
	private String nroSesionConsulta;
	
	private String tipoSesion;
	
	private String cadena;
	
	/*04/2008 - Asistencia Programada*/
	private String flgProg; //1-programada / 0-ya tomada
	
	private String saveFecha;
	
	private String fechaEliminar;
	private String nroSesionEliminar;
	
	private String codCursoEjec;
	private String periodo;
	private String codTipoSesion;
	private String especialidad;
	private String ciclo;
	
	private String ambiente;
	private String tipoSemana;
	private String fecha;
	private String horaInicio;
	private String horaFin;
	
	public String getCodCursoEjec() {
		return codCursoEjec;
	}
	public void setCodCursoEjec(String codCursoEjec) {
		this.codCursoEjec = codCursoEjec;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getCodTipoSesion() {
		return codTipoSesion;
	}
	public void setCodTipoSesion(String codTipoSesion) {
		this.codTipoSesion = codTipoSesion;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getAmbiente() {
		return ambiente;
	}
	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}
	public String getTipoSemana() {
		return tipoSemana;
	}
	public void setTipoSemana(String tipoSemana) {
		this.tipoSemana = tipoSemana;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public List getListaAlumnos() {
		return listaAlumnos;
	}
	public void setListaAlumnos(List listaAlumnos) {
		this.listaAlumnos = listaAlumnos;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getSistEval() {
		return sistEval;
	}
	public void setSistEval(String sistEval) {
		this.sistEval = sistEval;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getCadena() {
		return cadena;
	}
	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNroSesion() {
		return nroSesion;
	}
	public void setNroSesion(String nroSesion) {
		this.nroSesion = nroSesion;
	}
	public String getIdAsistBusqueda() {
		return idAsistBusqueda;
	}
	public void setIdAsistBusqueda(String idAsistBusqueda) {
		this.idAsistBusqueda = idAsistBusqueda;
	}
	public List getListaFechas() {
		return listaFechas;
	}
	public void setListaFechas(List listaFechas) {
		this.listaFechas = listaFechas;
	}
	public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	public String getNroSesionConsulta() {
		return nroSesionConsulta;
	}
	public void setNroSesionConsulta(String nroSesionConsulta) {
		this.nroSesionConsulta = nroSesionConsulta;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getTipoSesion() {
		return tipoSesion;
	}
	public void setTipoSesion(String tipoSesion) {
		this.tipoSesion = tipoSesion;
	}
	public String getNomEval() {
		return nomEval;
	}
	public void setNomEval(String nomEval) {
		this.nomEval = nomEval;
	}
	public String getFe() {
		return fe;
	}
	public void setFe(String fe) {
		this.fe = fe;
	}
	public int getNro() {
		return nro;
	}
	public void setNro(int nro) {
		this.nro = nro;
	}
	public String getFlgProg() {
		return flgProg;
	}
	public void setFlgProg(String flgProg) {
		this.flgProg = flgProg;
	}
	public String getSaveFecha() {
		return saveFecha;
	}
	public void setSaveFecha(String saveFecha) {
		this.saveFecha = saveFecha;
	}
	public String getFechaEliminar() {
		return fechaEliminar;
	}
	public void setFechaEliminar(String fechaEliminar) {
		this.fechaEliminar = fechaEliminar;
	}
	public String getNroSesionEliminar() {
		return nroSesionEliminar;
	}
	public void setNroSesionEliminar(String nroSesionEliminar) {
		this.nroSesionEliminar = nroSesionEliminar;
	}
	
}
