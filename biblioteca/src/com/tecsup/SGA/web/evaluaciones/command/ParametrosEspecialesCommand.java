package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;

public class ParametrosEspecialesCommand {
	private String operacion;
	private String usuario;
	
	private String txtFeCierre;
	private String txtNroVisitas;
	private String txtNroHoras2;
	private String txtNroHoras3;
	private String txtHorasMinima;
	
	private String txtExelente;
	private String txtBuena;
	private String txtMedia;
	private String txtBaja;
	
	private String hidExelente;
	private String hidBuena;
	private String hidMedia;
	private String hidBaja;
	
	
	private String txtMinima;
	
	private String varperiodo;
	private String nroRegistros;
	
	private String cboCicloCat;
	private String cboCicloCatSel;
	private String codSelCalificacion;
	private List lstCiclos;
	private List lstCiclosSel;
	private String tknCiclos;
	private List lstPeriodo;
	private String codEvaluador;
	private String codPeriodo;
	

	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public List getLstPeriodo() {
		return lstPeriodo;
	}
	public void setLstPeriodo(List lstPeriodo) {
		this.lstPeriodo = lstPeriodo;
	}
	public String getTknCiclos() {
		return tknCiclos;
	}
	public void setTknCiclos(String tknCiclos) {
		this.tknCiclos = tknCiclos;
	}
	public String getCodSelCalificacion() {
		return codSelCalificacion;
	}
	public void setCodSelCalificacion(String codSelCalificacion) {
		this.codSelCalificacion = codSelCalificacion;
	}
	public List getLstCiclos() {
		return lstCiclos;
	}
	public void setLstCiclos(List lstCiclos) {
		this.lstCiclos = lstCiclos;
	}
	public String getCboCicloCat() {
		return cboCicloCat;
	}
	public void setCboCicloCat(String cboCicloCat) {
		this.cboCicloCat = cboCicloCat;
	}
	public String getVarperiodo() {
		return varperiodo;
	}
	public void setVarperiodo(String varperiodo) {
		this.varperiodo = varperiodo;
	}
	public String getTxtMinima() {
		return txtMinima;
	}
	public void setTxtMinima(String txtMinima) {
		this.txtMinima = txtMinima;
	}
	public String getTxtFeCierre() {
		return txtFeCierre;
	}
	public void setTxtFeCierre(String txtFeCierre) {
		this.txtFeCierre = txtFeCierre;
	}
	public String getTxtNroVisitas() {
		return txtNroVisitas;
	}
	public void setTxtNroVisitas(String txtNroVisitas) {
		this.txtNroVisitas = txtNroVisitas;
	}
	public String getTxtNroHoras2() {
		return txtNroHoras2;
	}
	public void setTxtNroHoras2(String txtNroHoras2) {
		this.txtNroHoras2 = txtNroHoras2;
	}
	public String getTxtNroHoras3() {
		return txtNroHoras3;
	}
	public void setTxtNroHoras3(String txtNroHoras3) {
		this.txtNroHoras3 = txtNroHoras3;
	}
	public String getTxtHorasMinima() {
		return txtHorasMinima;
	}
	public void setTxtHorasMinima(String txtHorasMinima) {
		this.txtHorasMinima = txtHorasMinima;
	}
	public String getTxtExelente() {
		return txtExelente;
	}
	public void setTxtExelente(String txtExelente) {
		this.txtExelente = txtExelente;
	}
	public String getTxtBuena() {
		return txtBuena;
	}
	public void setTxtBuena(String txtBuena) {
		this.txtBuena = txtBuena;
	}
	public String getTxtMedia() {
		return txtMedia;
	}
	public void setTxtMedia(String txtMedia) {
		this.txtMedia = txtMedia;
	}
	public String getTxtBaja() {
		return txtBaja;
	}
	public void setTxtBaja(String txtBaja) {
		this.txtBaja = txtBaja;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getNroRegistros() {
		return nroRegistros;
	}
	public void setNroRegistros(String nroRegistros) {
		this.nroRegistros = nroRegistros;
	}
	public String getCboCicloCatSel() {
		return cboCicloCatSel;
	}
	public void setCboCicloCatSel(String cboCicloCatSel) {
		this.cboCicloCatSel = cboCicloCatSel;
	}
	public List getLstCiclosSel() {
		return lstCiclosSel;
	}
	public void setLstCiclosSel(List lstCiclosSel) {
		this.lstCiclosSel = lstCiclosSel;
	}
	public String getHidExelente() {
		return hidExelente;
	}
	public void setHidExelente(String hidExelente) {
		this.hidExelente = hidExelente;
	}
	public String getHidBuena() {
		return hidBuena;
	}
	public void setHidBuena(String hidBuena) {
		this.hidBuena = hidBuena;
	}
	public String getHidMedia() {
		return hidMedia;
	}
	public void setHidMedia(String hidMedia) {
		this.hidMedia = hidMedia;
	}
	public String getHidBaja() {
		return hidBaja;
	}
	public void setHidBaja(String hidBaja) {
		this.hidBaja = hidBaja;
	}
	
}
