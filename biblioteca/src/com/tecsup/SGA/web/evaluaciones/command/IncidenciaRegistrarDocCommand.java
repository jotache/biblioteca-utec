package com.tecsup.SGA.web.evaluaciones.command;

import java.util.List;
public class IncidenciaRegistrarDocCommand {
	
	private String operacion;
	private String msg;
	
	private String codProducto;
	private String codEspecialidad;
	private String codSeccion;
	private String codTipoSesionDefecto;
	
	private String fechaActual;
	private String codIncidencia;
	private String codCurso;
	private String codAlumno;	
	private String nomAlumno;
	private String fechaIncidencia;
	private String dscIncidencia;
	private List listIncidencias;
	private String codEvaluador;
	private String codPeriodo;
	private String evaluador;
	
	private String codTipoIncidencia;
	private List listaTipoIncidencia;
	
	private String fechaIni;
	private String fechaFin;
	private String codTipoIncSancion;	
	
	private String fechaIniPer;
	private String fechaFinPer;
	
	/*Cuando se entra como tutor*/
	//Variables para el tutor
	private String indProcedencia;
	private String codPeriodoTutor;
	private String codCiclo;
	
	public String getCodTipoIncSancion() {
		return codTipoIncSancion;
	}
	public void setCodTipoIncSancion(String codTipoIncSancion) {
		this.codTipoIncSancion = codTipoIncSancion;
	}
	public String getCodTipoIncidencia() {
		return codTipoIncidencia;
	}
	public void setCodTipoIncidencia(String codTipoIncidencia) {
		this.codTipoIncidencia = codTipoIncidencia;
	}
	public List getListaTipoIncidencia() {
		return listaTipoIncidencia;
	}
	public void setListaTipoIncidencia(List listaTipoIncidencia) {
		this.listaTipoIncidencia = listaTipoIncidencia;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNomAlumno() {
		return nomAlumno;
	}
	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	public String getFechaIncidencia() {
		return fechaIncidencia;
	}
	public void setFechaIncidencia(String fechaIncidencia) {
		this.fechaIncidencia = fechaIncidencia;
	}
	public String getDscIncidencia() {
		return dscIncidencia;
	}
	public void setDscIncidencia(String dscIncidencia) {
		this.dscIncidencia = dscIncidencia;
	}
	public List getListIncidencias() {
		return listIncidencias;
	}
	public void setListIncidencias(List listIncidencias) {
		this.listIncidencias = listIncidencias;
	}
	public String getCodIncidencia() {
		return codIncidencia;
	}
	public void setCodIncidencia(String codIncidencia) {
		this.codIncidencia = codIncidencia;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getCodTipoSesionDefecto() {
		return codTipoSesionDefecto;
	}
	public void setCodTipoSesionDefecto(String codTipoSesionDefecto) {
		this.codTipoSesionDefecto = codTipoSesionDefecto;
	}
	public String getFechaIniPer() {
		return fechaIniPer;
	}
	public void setFechaIniPer(String fechaIniPer) {
		this.fechaIniPer = fechaIniPer;
	}
	public String getFechaFinPer() {
		return fechaFinPer;
	}
	public void setFechaFinPer(String fechaFinPer) {
		this.fechaFinPer = fechaFinPer;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public String getIndProcedencia() {
		return indProcedencia;
	}
	public void setIndProcedencia(String indProcedencia) {
		this.indProcedencia = indProcedencia;
	}
	public String getCodPeriodoTutor() {
		return codPeriodoTutor;
	}
	public void setCodPeriodoTutor(String codPeriodoTutor) {
		this.codPeriodoTutor = codPeriodoTutor;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}

}
