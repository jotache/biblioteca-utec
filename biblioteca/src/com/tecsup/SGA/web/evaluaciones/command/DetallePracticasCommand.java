package com.tecsup.SGA.web.evaluaciones.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class DetallePracticasCommand {
	private String operacion;
	private String typeMessage;
	private String message;
	//JHPR: 2008-04-22 Envio de mensaje:Evaluaciones que no se pudieron eliminar por que ya tienen notas registradas
	private String message2;
	
	private String ciclo;
	private String periodo;
	private String evaluador;
	private List listaCursos;
	private String codCurso;
	private String producto;
	private String curso;
	private String especialidad;
	private String sistEval;
	private List listEvaluaciones;
	private String codigo;
	private String descripcion;
	private String codEvaluacion;
	private String numero;
	private String flag;
	private int cant;
	private String codPeriodo;
	private String codProducto;
	private String minimo;
	private String min;
	private String valor;
	private int cantidad;
	private String codEval;
	private List ListaSeccion;
	private String PrimeraSeccion;
	private String seccion;
	private String sec;
	private String codId;
	private String peso;
	private String codEvaluador;
	private String flagEliminar;
	private String codEva;
	private String codEspecialidad;
	private String codEvalu;
	private String cod;
	private String usuario;
	
	//JHPR 2008-07-01 En definici�n de evaluaciones, para impedir crear,actualizar o eliminar evaluaciones cuando el registro ya est� cerrado.
	private String flagCerrarNota;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodEva() {
		return codEva;
	}
	public void setCodEva(String codEva) {
		this.codEva = codEva;
	}
	public String getFlagEliminar() {
		return flagEliminar;
	}
	public void setFlagEliminar(String flagEliminar) {
		this.flagEliminar = flagEliminar;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	public String getSec() {
		return sec;
	}
	public void setSec(String sec) {
		this.sec = sec;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getMinimo() {
		return minimo;
	}
	public void setMinimo(String minimo) {
		this.minimo = minimo;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getTypeMessage() {
		return typeMessage;
	}
	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public List getListaCursos() {
		return listaCursos;
	}
	public void setListaCursos(List listaCursos) {
		this.listaCursos = listaCursos;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getSistEval() {
		return sistEval;
	}
	public void setSistEval(String sistEval) {
		this.sistEval = sistEval;
	}
	public List getListEvaluaciones() {
		return listEvaluaciones;
	}
	public void setListEvaluaciones(List listEvaluaciones) {
		this.listEvaluaciones = listEvaluaciones;
	}
	public String getCodEvaluacion() {
		return codEvaluacion;
	}
	public void setCodEvaluacion(String codEvaluacion) {
		this.codEvaluacion = codEvaluacion;
	}
	public int getCant() {
		return cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getMin() {
		return min;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public List getListaSeccion() {
		return ListaSeccion;
	}
	public void setListaSeccion(List listaSeccion) {
		ListaSeccion = listaSeccion;
	}
	public String getPrimeraSeccion() {
		return PrimeraSeccion;
	}
	public void setPrimeraSeccion(String primeraSeccion) {
		PrimeraSeccion = primeraSeccion;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getCodEvalu() {
		return codEvalu;
	}
	public void setCodEvalu(String codEvalu) {
		this.codEvalu = codEvalu;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	
	//JHPR: 2008-04-22 Envio de mensaje:Evaluaciones que no se pudieron eliminar por que ya tienen notas registradas
	public String getMessage2() {
		return message2;
	}
	public void setMessage2(String message2) {
		this.message2 = message2;
	}
	public String getFlagCerrarNota() {
		return flagCerrarNota;
	}
	public void setFlagCerrarNota(String flagCerrarNota) {
		this.flagCerrarNota = flagCerrarNota;
	}		
}
