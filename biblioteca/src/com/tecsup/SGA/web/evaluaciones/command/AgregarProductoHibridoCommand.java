package com.tecsup.SGA.web.evaluaciones.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class AgregarProductoHibridoCommand {

	private String codDetalle;
	private String operacion;	
	private String denominacion;
	private String cadEvaluacion;	
	private String msg;
	private String tipoExamen;
	private String tipoPractica;
	private String nroRegistros;
	private String peso;
	//***********************************
	private String seleccion;
	private String codigo;
	private String descripcion;
	private List listaExamenes;
	private List listaPracticas;
	//***********************************
	private List listaBandeja;
	private String estadoCodigo;
	//***********************************
	private String codPeriodo;
	private String codEvaluador;
	
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getEstadoCodigo() {
		return estadoCodigo;
	}
	public void setEstadoCodigo(String estadoCodigo) {
		this.estadoCodigo = estadoCodigo;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List getListaExamenes() {
		return listaExamenes;
	}
	public void setListaExamenes(List listaExamenes) {
		this.listaExamenes = listaExamenes;
	}
	public List getListaPracticas() {
		return listaPracticas;
	}
	public void setListaPracticas(List listaPracticas) {
		this.listaPracticas = listaPracticas;
	}	
	public String getCadEvaluacion() {
		return cadEvaluacion;
	}
	public void setCadEvaluacion(String cadEvaluacion) {
		this.cadEvaluacion = cadEvaluacion;
	}
	public String getTipoExamen() {
		return tipoExamen;
	}
	public void setTipoExamen(String tipoExamen) {
		this.tipoExamen = tipoExamen;
	}
	public String getTipoPractica() {
		return tipoPractica;
	}
	public void setTipoPractica(String tipoPractica) {
		this.tipoPractica = tipoPractica;
	}
	public String getNroRegistros() {
		return nroRegistros;
	}
	public void setNroRegistros(String nroRegistros) {
		this.nroRegistros = nroRegistros;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}	
}
