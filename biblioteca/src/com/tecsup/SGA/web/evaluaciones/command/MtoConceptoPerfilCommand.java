package com.tecsup.SGA.web.evaluaciones.command;
import java.util.*;

public class MtoConceptoPerfilCommand {
 private String codAreasNivelUno;//esta variable tiene el codigo del valor 
 //seleccionado en el primer display:table
 private String codAreasNivelDos;//esta variable tiene el codigo del valor 
 //seleccionado en el segundo display:table
 private String operacion;//tiene el tipo de accion a realizar
 private String dscNivelUno;//esta variable tiene la descripcion del valor 
 //seleccionado en el primer display:table
 private String dscNivelDos;//esta variable tiene la descripcion del valor 
 //seleccionado en el primer display:table
 private String desPerfil;
 private List listAreasNivelUno;//esta variable tiene la lista de todos los valores 
 //que tiene el primer display:table
 private List listAreasNivelDos;//esta variable tiene la lista de todos los valores 
 //que tiene el segundo display:table
 private List listaAreasIntUno;//esta variable es una lista de todos los valores
 //que tiene que mostrar en el primer display:table del jsp
 private List listaAreasIntDos;//esta variable es una lista de todos los valores
 //que tiene que mostrar en el segundo display:table del jsp
 private String codProcesosSeleccionados;
 private String codProcesosSeleccionados2;
 private String descripcionNivel1;
 private String msg;
 private String msg2;
 private String nroSelec;
 private String tamano;
 private String mad;
 private String codEvaluador;
 private String codPeriodo;
public String getCodEvaluador() {
	return codEvaluador;
}
public void setCodEvaluador(String codEvaluador) {
	this.codEvaluador = codEvaluador;
}
public String getCodPeriodo() {
	return codPeriodo;
}
public void setCodPeriodo(String codPeriodo) {
	this.codPeriodo = codPeriodo;
}
public String getMad() {
	return mad;
}
public void setMad(String mad) {
	this.mad = mad;
}
public String getTamano() {
	return tamano;
}
public void setTamano(String tamano) {
	this.tamano = tamano;
}
public String getNroSelec() {
	return nroSelec;
}
public void setNroSelec(String nroSelec) {
	this.nroSelec = nroSelec;
}
public String getMsg2() {
	return msg2;
}
public void setMsg2(String msg2) {
	this.msg2 = msg2;
}
public String getMsg() {
	return msg;
}
public void setMsg(String msg) {
	this.msg = msg;
}
public String getCodAreasNivelUno() {
	return codAreasNivelUno;
}
public void setCodAreasNivelUno(String codAreasNivelUno) {
	this.codAreasNivelUno = codAreasNivelUno;
}
public String getCodAreasNivelDos() {
	return codAreasNivelDos;
}
public void setCodAreasNivelDos(String codAreasNivelDos) {
	this.codAreasNivelDos = codAreasNivelDos;
}
public String getOperacion() {
	return operacion;
}
public void setOperacion(String operacion) {
	this.operacion = operacion;
}
public String getDscNivelUno() {
	return dscNivelUno;
}
public void setDscNivelUno(String dscNivelUno) {
	this.dscNivelUno = dscNivelUno;
}
public String getDscNivelDos() {
	return dscNivelDos;
}
public void setDscNivelDos(String dscNivelDos) {
	this.dscNivelDos = dscNivelDos;
}
public String getDesPerfil() {
	return desPerfil;
}
public void setDesPerfil(String desPerfil) {
	this.desPerfil = desPerfil;
}
public List getListAreasNivelUno() {
	return listAreasNivelUno;
}
public void setListAreasNivelUno(List listAreasNivelUno) {
	this.listAreasNivelUno = listAreasNivelUno;
}
public List getListAreasNivelDos() {
	return listAreasNivelDos;
}
public void setListAreasNivelDos(List listAreasNivelDos) {
	this.listAreasNivelDos = listAreasNivelDos;
}
public List getListaAreasIntUno() {
	return listaAreasIntUno;
}
public void setListaAreasIntUno(List listaAreasIntUno) {
	this.listaAreasIntUno = listaAreasIntUno;
}
public List getListaAreasIntDos() {
	return listaAreasIntDos;
}
public void setListaAreasIntDos(List listaAreasIntDos) {
	this.listaAreasIntDos = listaAreasIntDos;
}
public String getDescripcionNivel1() {
	return descripcionNivel1;
}
public void setDescripcionNivel1(String descripcionNivel1) {
	this.descripcionNivel1 = descripcionNivel1;
}
public String getCodProcesosSeleccionados() {
	return codProcesosSeleccionados;
}
public void setCodProcesosSeleccionados(String codProcesosSeleccionados) {
	this.codProcesosSeleccionados = codProcesosSeleccionados;
}
public String getCodProcesosSeleccionados2() {
	return codProcesosSeleccionados2;
}
public void setCodProcesosSeleccionados2(String codProcesosSeleccionados2) {
	this.codProcesosSeleccionados2 = codProcesosSeleccionados2;
}
 
 
}
