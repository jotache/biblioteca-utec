package com.tecsup.SGA.web.evaluaciones.command;
import com.tecsup.SGA.modelo.*;
import java.util.*;
public class FormacionEmpresaCommand {
	private String operacion;
	private List listEva;
	private String codEva;
	private String nombre;
	private String apellidos;
	private List listAlum;
	private String nombreAlumno;
	private String codAlumno;
	private String codEspecialidad;
	private String periodo;
	private String producto;
	private List peri;
	private String codPeriodo;
	private String codEspe;

	//JHPR: 2008-06-06 
	private String tamanioLista;
		

	public String getTamanioLista() {
		return tamanioLista;
	}

	public void setTamanioLista(String tamanioLista) {
		this.tamanioLista = tamanioLista;
	}

	public String getCodEspe() {
		return codEspe;
	}

	public void setCodEspe(String codEspe) {
		this.codEspe = codEspe;
	}

	public String getCodPeriodo() {
		return codPeriodo;
	}

	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}

	public List getPeri() {
		return peri;
	}

	public void setPeri(List peri) {
		this.peri = peri;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodEva() {
		return codEva;
	}

	public void setCodEva(String codEva) {
		this.codEva = codEva;
	}

	public List getListEva() {
		return listEva;
	}

	public void setListEva(List listEva) {
		this.listEva = listEva;
	}

	public List getListAlum() {
		return listAlum;
	}

	public void setListAlum(List listAlum) {
		this.listAlum = listAlum;
	}

	public String getNombreAlumno() {
		return nombreAlumno;
	}

	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}

	public String getCodEspecialidad() {
		return codEspecialidad;
	}

	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}

	

}
