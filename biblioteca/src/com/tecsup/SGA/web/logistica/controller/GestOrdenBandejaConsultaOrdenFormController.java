package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;

import com.tecsup.SGA.service.logistica.GestionOrdenesManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.GestOrdenBandejaConsultaOrdenCommand;

public class GestOrdenBandejaConsultaOrdenFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(GestOrdenBandejaConsultaOrdenFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	GestionOrdenesManager gestionOrdenesManager;
	
	public GestionOrdenesManager getGestionOrdenesManager() {
		return gestionOrdenesManager;
	}

	public void setGestionOrdenesManager(GestionOrdenesManager gestionOrdenesManager) {
		this.gestionOrdenesManager = gestionOrdenesManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	GestOrdenBandejaConsultaOrdenCommand command = new GestOrdenBandejaConsultaOrdenCommand();
    	command.setCodSede(request.getParameter("txhCodSede"));
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    		command.setDscUsuario(usuarioSeguridad.getNomUsuario());
    	}
    	
    	System.out.println("CodPerfil: "+command.getCodPerfil());
    	try{
    	if(command.getCodPerfil().indexOf(CommonConstants.CONSTE_COD_PERFIL_PROVEEDOR)==-1)
    	   command.setBanderaPerfil("1");
    	else command.setBanderaPerfil("0");
    	}catch (Exception e) {
			
		}
    	//ALQD,04/02/09.SE A�ADE PROPIEDAD PARA INDICAR QUE TIENE PERFIL COMPRADOR
    	//ESTE INDICADOR PERMITIRA EN LA BANDEJA DE CONSULTA DE OC SI EL ESTADO SE 
    	//MUESTRA TODOS O EN APROBACION
    	//ALQD,14/09/09.SE COLOCARA VALOR 2 SI ES PARA CONSULTA
    	if(command.getCodPerfil().indexOf("LCOMP")>-1) {
    		command.setIndPerComprador("1");
    	} else {
    		if(command.getCodPerfil().indexOf("LCONS")>-1) {
    			command.setIndPerComprador("2");
    		}else command.setIndPerComprador("0");
    	}
    	//ALQD,14/07/09.SE AGREGA PROPIEDAD PARA INDICAR SI ES DIRECTOR O JLOG PARA DELEGAR APROBACION
    	command.setIndDirector("0");
    	if(command.getCodPerfil().indexOf("LDCECO")>-1 ||
    		command.getCodPerfil().indexOf("LDADM")>-1 ||
    		command.getCodPerfil().indexOf("LJLOG")>-1 ||
    		command.getCodPerfil().indexOf("LGEGE")>-1){
    			command.setIndDirector("1");
    	}
    	Fecha fecha=new Fecha();
    	command.setFechaBD(fecha.getFechaActual());
    	System.out.println("BanderaPerfil: "+command.getBanderaPerfil());
    	command.setCodEstadoAprobado(CommonConstants.GEST_DOC_ESTADO_APROBADO);
    	command.setCodTipoReporte(CommonConstants.REPORTE_LOG_16);
    	
    	System.out.println("CoDSede: "+command.getCodSede());
    	List lista1= new ArrayList();
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
        if(lista1!=null)
    	command.setListaCodTipoOrden(lista1);
        
    	List lista2= new ArrayList();
   	   lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_CONSTE_ESTADO_LOG 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
        if(lista2!=null)
    	command.setListaEstado(lista2);
        
    	        
    	BandejaLogisticaBean bandejaBean1 =	(BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
         if(bandejaBean1!= null)
         {  log.info("BandejaLogisticaBean:INICIO");
         	if(bandejaBean1.getCodBandeja().equals("5")){
	      	  command.setNroOrden(bandejaBean1.getValor1());
	      	  command.setFecInicio(bandejaBean1.getValor2());
	      	  command.setFecFinal(bandejaBean1.getValor3());
	      	  command.setCodEstado(bandejaBean1.getValor4());
	      	  command.setNroRucProveedor(bandejaBean1.getValor5());
	      	  command.setNombreProveedor(bandejaBean1.getValor6());
	      	  command.setNroRucComprador(bandejaBean1.getValor7());
	      	  command.setNombreComprador(bandejaBean1.getValor8());
	      	  command.setCodTipoOrden(bandejaBean1.getValor9());
	      	  BuscarBandeja(command, request);
         	}
      	  log.info("BandejaLogisticaBean:FIN");
         }
         else command.setBanListaBandeja("0"); 
     	System.out.println("codEstado: "+command.getCodEstado());
         log.info("formBackingObject:FIN");
    	return command;
         
      }
  	
  	protected void initBinder(HttpServletRequest request,
              ServletRequestDataBinder binder) {
      	NumberFormat nf = NumberFormat.getNumberInstance();
      	binder.registerCustomEditor(Long.class,
  	                  new CustomNumberEditor(Long.class, nf, true));
      	
      }
      /**
       * Redirect to the successView when the cancel button has been pressed.
       */
      public ModelAndView processFormSubmission(HttpServletRequest request,
                                                HttpServletResponse response,
                                                Object command,
                                                BindException errors)
      throws Exception {
          //this.onSubmit(request, response, command, errors);
      	return super.processFormSubmission(request, response, command, errors);
      }
  	
      public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
              BindException errors)
      		throws Exception {
      	log.info("onSubmit:INI");
      	String resultado="";
      	GestOrdenBandejaConsultaOrdenCommand control= (GestOrdenBandejaConsultaOrdenCommand) command;
      	control.setBanListaBandeja("");
      	
      	if(control.getOperacion().equals("BUSCAR"))
      	{
      		BuscarBandeja(control, request);
      	}
      	else{ if(control.getOperacion().equals("CERRAR_ORDEN"))
      		   resultado=CerrarOrden(control);
      	       if(resultado.equals("0")) 
      	       {control.setMsg("ORDEN_CERRADA_OK");
      	       }
      	       else{ if(resultado.equals("-1")) control.setMsg("CERRAR_ORDEN_ERROR");
      	    	} 
      	     BuscarBandeja(control, request);
      	}
      	
      	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/GestOrdenCompraServicio/GestOrdenBandejaConsultaOrden","control",control);
    }

     public void BuscarBandeja(GestOrdenBandejaConsultaOrdenCommand control,HttpServletRequest request)
     {
    	 List lista1= new ArrayList();
    	 
    	 GuardarDatosBandeja(control, request);
    	 
    	 if(control.getCodEstado().equals("-1"))
    		 control.setCodEstado("");
    	 if(control.getCodTipoOrden().equals("-1"))
    		 control.setCodTipoOrden("");
    	System.out.println(">>CodSede<<"+control.getCodSede()+">>CodUsuario<<"+
   			 control.getCodUsuario()+">>CodPerfil<<"+
			 control.getCodPerfil()+">>NroOrden<<"+ control.getNroOrden()+">>FecInicio<<"+ 
			 control.getFecInicio()+">>FecFinal<<"+ control.getFecFinal()+">>CodEstado<<"+
			 control.getCodEstado()+">>NroRucProveedor<<"+ 
			 control.getNroRucProveedor()+">>NombreProveedor<<"+control.getNombreProveedor()+
			 ">>NroRucComprador<<"+control.getNroRucComprador()
			 +">>NombreComprador<<"+control.getNombreComprador()+">>CodTipoOrden<<"+control.getCodTipoOrden()); 
    	 lista1=this.gestionOrdenesManager.GetAllBandejaConsultaOrdenes(control.getCodSede(),
    			 control.getCodUsuario(), 
    			 control.getCodPerfil(), control.getNroOrden(), 
    			 control.getFecInicio(), control.getFecFinal(), control.getCodEstado(), 
    			 control.getNroRucProveedor(), control.getNombreProveedor(), control.getNroRucComprador(),
    			 control.getNombreComprador(), control.getCodTipoOrden());
    	 if(lista1!=null)
    	    { control.setListaBandeja(lista1);
    	      if(lista1.size()==0)
    	    	  control.setBanListaBandeja("0");
    	    }
    	 else{ control.setBanListaBandeja("0");
    	 	   control.setListaBandeja(new ArrayList());
    	 }
    	 System.out.println("Size: "+lista1.size());
    	 
     }
      
     public String CerrarOrden(GestOrdenBandejaConsultaOrdenCommand control)
     { String resultado="";
       System.out.println("CodRequerimiento:<<"+control.getCodRequerimiento()+">>CodUsuario:<<"+control.getCodUsuario());
       resultado=this.gestionOrdenesManager.UpdateCerrarOrden(control.getCodRequerimiento(),
    		   control.getCodUsuario()); 
      System.out.println("CerrarOrden Resultado: "+resultado);
       return resultado;	 
     }
     
     public void GuardarDatosBandeja(GestOrdenBandejaConsultaOrdenCommand control,HttpServletRequest request)
     {
    	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
    	 
    	 bandejaBean.setCodBandeja("5");
    	 bandejaBean.setValor1(control.getNroOrden());
    	 bandejaBean.setValor2(control.getFecInicio());
    	 bandejaBean.setValor3(control.getFecFinal());
    	 bandejaBean.setValor4(control.getCodEstado());
    	 bandejaBean.setValor5(control.getNroRucProveedor());
    	 bandejaBean.setValor6(control.getNombreProveedor());
    	 bandejaBean.setValor7(control.getNroRucComprador());
    	 bandejaBean.setValor8(control.getNombreComprador());
    	 bandejaBean.setValor9(control.getCodTipoOrden());
    	 log.info("GuardarDatosBandeja");
    	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
     }
     
     
	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
}
