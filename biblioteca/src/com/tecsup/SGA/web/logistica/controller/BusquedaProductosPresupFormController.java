package com.tecsup.SGA.web.logistica.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CatalogoProducto;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.web.logistica.command.BusquedaProductosCommand;

public class BusquedaProductosPresupFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(BusquedaProductosPresupFormController.class);
	DetalleManager detalleManager;
	SolRequerimientoManager solRequerimientoManager;
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	} 
	
	protected Object formBackingObject(HttpServletRequest request) throws ServletException {
    	   	
    	BusquedaProductosCommand command = new BusquedaProductosCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodUsuario"));
    	command.setCodReq((String)request.getParameter("txhCodRequerimiento"));
    	command.setSede((String)request.getParameter("txhCodSede"));
    	command.setCodTipoBien((String)request.getParameter("txhCodTipoBien"));  //""  	
    	request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02);
    	request.setAttribute("msgRutaServer2",CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_DOCUMENTO_02);
    	if (command.getCodTipoBien()!= null)
    		if (!command.getCodTipoBien().trim().equals(""))
    			cargaData(command);
    	
  		command.setListProducto(null);
  		command.setDes("");
  		request.getSession().setAttribute("listTipoAdjunto", command.getListTipoAdjunto());
    	request.getSession().setAttribute("listProducto", command.getListProducto());    
        return command;
    }
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	int resultado;
    	BusquedaProductosCommand control = (BusquedaProductosCommand) command;
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{
    		cargaData(control);
    		buscar(control);
    		llenaSubFamilia(control);
    		request.getSession().setAttribute("listTipoAdjunto", control.getListTipoAdjunto());
        	request.getSession().setAttribute("listProducto", control.getListProducto());
    	}
    	if (control.getOperacion().trim().equals("LISTAR_FAMILIA")){
    		System.out.println("LISTAR_FAMILIA");
    		cargaData(control);
    		llenaSubFamilia(control);
    	}
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		resultado = grabar(control);
    		cargaData(control);
    		buscar(control);
    		llenaSubFamilia(control);
    		request.getSession().setAttribute("listTipoAdjunto", control.getListTipoAdjunto());
        	request.getSession().setAttribute("listProducto", control.getListProducto());
      		System.out.println("RES: "+resultado);
      		 if(resultado>=1){
	    		control.setMsg("DOBLE");
	    	}
      		 else if(resultado<=-1){
      			 control.setMsg("ERROR");
      		 }
      		else control.setMsg("OK");
      		
      		control.setCodProducto("");
    	}
    	if (control.getOperacion().trim().equals("MUESTRA"))
    	{
    		control.setCodProducto(control.getCodProducto1());
    		muestra(control);
    		cargaData(control);
    		buscar(control);
    		llenaSubFamilia(control);    		
    		request.getSession().setAttribute("listTipoAdjunto", control.getListTipoAdjunto());
        	request.getSession().setAttribute("listProducto", control.getListProducto());
        	control.setCodProducto("");
       
    	}
    	if (control.getOperacion().trim().equals("MUESTRASUBFAMILIA"))
    	{
    		cargaData(control);
    		llenaSubFamilia(control);
    		
    	}
    	if (control.getOperacion().trim().equals("LIMPIA"))
    	{
    		cargaData(control);
    		control.setCodFamilia("");
    		control.setCodigo("");
    		control.setDescripcion("");
    		control.setCodSubFamilia("");
    		control.setDescTecnica("");
    		control.setDes("");
    	}
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/SolAproRequerimientos/log_Busqueda_ProductosPresup","control",control);		
    }
		
    private void buscar(BusquedaProductosCommand control){
	    List lista= new ArrayList();	    
	    lista=this.detalleManager.GetAllProducto("",control.getSede()
    			, control.getCodFamilia(), control.getCodSubFamilia(), control.getCodigo()
    			, "", control.getCodTipoBien(), "", control.getDescripcion(),"");
	    if(lista!=null)
	       control.setListProducto(lista);		
	    else control.setListProducto(new ArrayList());
   }
    private void cargaData(BusquedaProductosCommand control){
    	TipoLogistica obj = new TipoLogistica();    	
    	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	obj.setCodId(control.getCodTipoBien());
    	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));   
    }    
   
   private int grabar(BusquedaProductosCommand control){
	   SolRequerimientoDetalle obj = new SolRequerimientoDetalle();
	   obj.setCodRequerimiento(control.getCodReq());
	//   obj.setCodigoBien(control.getCodProducto());
	   String codEvaluacion;
	   int i = 0;
		obj.setCodigo("");
		obj.setNombreServicio("");
		obj.setDescripcion("");
		obj.setCantidad("");
		obj.setTipoGasto("");
		obj.setFechaEntrega("");
		StringTokenizer stkEvaluaciones = new StringTokenizer(control.getCodProducto(),"|");
		while ( stkEvaluaciones.hasMoreTokens() ){		
			codEvaluacion = stkEvaluaciones.nextToken();
			 obj.setCodigoBien(codEvaluacion);
			control.setGraba(this.solRequerimientoManager.InsertSolRequerimientoDetalle(obj, control.getCodAlumno()));			
			if(control.getGraba().equals("-2")){
				i = i + 1;			
			}
			else if(control.getGraba().equals("-1")){
				i = i -1;
			}		
		}		
		control.setNopo(i);
	   return control.getNopo();
   }
   private void muestra(BusquedaProductosCommand control){   	   
		control.setListTipoAdjunto(this.detalleManager.GetAllDocCatalogo(control.getCodProducto(), ""));   	
		control.setListProducto(this.detalleManager.GetAllProducto(control.getCodProducto(),control.getSede()
				, control.getCodFamilia(), control.getCodSubFamilia(), control.getCodigo()
				, "", control.getCodTipoBien(), "", control.getDescripcion(),""));
		List lst  = control.getListProducto();
		if(lst!=null ){
			if ( lst.size()>0 ){   		
				CatalogoProducto catalogoProducto = null;
				catalogoProducto = (CatalogoProducto)lst.get(0);    			
				control.setDes(catalogoProducto.getDescripcion());   		
			}
		}
   }
   private void llenaSubFamilia(BusquedaProductosCommand control){   	
   	TipoLogistica obj = new TipoLogistica();   	
   	obj.setCodId(control.getCodFamilia());
   	obj.setCodTabla(CommonConstants.TIPT_SUB_FAMILIA);
   	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
   	
   	control.setListSubFamilia(null);
   	if ( control.getCodFamilia() != null)
   		if ( !control.getCodFamilia().trim().equals(""))
   			control.setListSubFamilia(this.detalleManager.GetAllTablaDetalle(obj));   
   }
}
