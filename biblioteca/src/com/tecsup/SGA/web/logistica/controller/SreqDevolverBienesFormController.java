package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.SreqDevolverBienesCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;

public class SreqDevolverBienesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(SreqDevolverBienesFormController.class);
	private DetalleManager detalleManager;	
	private SolRequerimientoManager solRequerimientoManager;
	
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
	
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	SreqDevolverBienesCommand command = new SreqDevolverBienesCommand();
    	//*********************************************************
    	command.setOpcion(request.getParameter("txhOpcion") == null ? "": request.getParameter("txhOpcion"));//txhOpcion=0 --> Req txhOpcion=1 --> Almacen
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setCodTipoBien(request.getParameter("txhCodTipoBien") == null ? "": request.getParameter("txhCodTipoBien"));
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");
    	System.out.println("opcion="+command.getOpcion()+"<<");
    	System.out.println("CodTipoBien="+command.getCodTipoBien()+"<<");
    	//*********************************************************
    	command.setCodTipoBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setCodTipoBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	
    	if(!command.getCodSede().equals(""))
        { command.setCodSede(request.getParameter("txhCodSede"));
    	  log.info("CodSede: "+command.getCodSede());
          command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
          System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
        }
        command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN);
    	
    	
    	if(command.getCodRequerimiento() != null){
    		if(!command.getCodRequerimiento().equals("")){
    			CargaDatosCabecera(command);
    	    }
    	}
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	SreqDevolverBienesCommand control = (SreqDevolverBienesCommand)command;
    	String resultado="";    	
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = InsertSolicitudDevolucion(control);
    		System.out.println("resultado de grabar-->"+resultado);
    		if(resultado.equals("-2")){
    			     control.setMsg("ALMACEN_CERRADO");
    			}
    		else{ if(resultado.equals("--1")){
    			  control.setMsg("ERROR_GRABAR");
    			  CargaDatosCabecera(control);
    				}
    			  else{
    	    			control.setMsg("OK_GRABAR");
    	    			control.setCodDevolucion(resultado);
    	        		CargaDatosCabecera(control);
    	    		}
    			}
    		
    	}
    	else
    	if(control.getOperacion().trim().equals("ENVIAR_ALMACEN")){
    		resultado=EnviarAlmacen(control);
    		System.out.println("resultado: "+resultado);
    		if(resultado.equals("0")){
    			control.setMsg("OK_ENVIAR_ALMACEN");
    			CargaDatosCabecera(control);
    		}
    		else{ if(resultado.equals("-3")){
    			   control.setMsg("ALMACEN_CERRADO");
    			   CargaDatosCabecera(control);
    			  }
    			  else{ if(resultado.equals("-2")){
    				  	  control.setMsg("DETALLES_NO_SELECCIONADOS");
    				  	  CargaDatosCabecera(control);
    			        }
    				  
    			  control.setMsg("ERROR_ENVIAR_ALMACEN");
    			  CargaDatosCabecera(control);
    			  }
    		}
    	}
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/SolRequerimiento/Sreq_Devolver_Bienes","control",control);
    }    
    private void CargaDatosCabecera(SreqDevolverBienesCommand control){
    	//carga los codRadio tipo de bien se guardan en dos hidden
    	TipoLogistica obj2 = new TipoLogistica();    	
    	obj2.setCodPadre("");
    	obj2.setCodId("");
    	obj2.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
    	obj2.setCodigo("");
    	obj2.setDescripcion("");
    	obj2.setTipo(CommonConstants.TIPO_ORDEN_COD);    	
    	control.setListaCodTipoBien(this.detalleManager.GetAllTablaDetalle(obj2));
    	Logistica logistica = new Logistica();
    	logistica=(Logistica)control.getListaCodTipoBien().get(0);
    	control.setCodTipoBienConsumible(logistica.getCodSecuencial());
    	logistica=(Logistica)control.getListaCodTipoBien().get(1);
    	control.setCodTipoBienActivo(logistica.getCodSecuencial());
    	//*****************************************
    	ArrayList<SolRequerimiento> listaSolReq;
    	if(!control.getCodRequerimiento().equals("")){    		
    		
    		listaSolReq = (ArrayList<SolRequerimiento>)this.solRequerimientoManager.getSolReqUsuario(control.getCodRequerimiento());
    		
    		if ( listaSolReq != null )
    			if ( listaSolReq.size() > 0 )
    				{
			    		SolRequerimiento solReqUsuario=listaSolReq.get(0);
			    		
			    		System.out.println("codigo sol req usuario-->"+solReqUsuario.getIdRequerimiento());
			    		
			    		control.setCodRequerimiento(solReqUsuario.getIdRequerimiento());
			    		control.setCodDevolucion(solReqUsuario.getCodDevolucion());
			    		control.setNroDevolucion(solReqUsuario.getNroDevolucion());
			    		control.setFechaDevolución(solReqUsuario.getFechaDevolucion());
			    		control.setCentroCosto(solReqUsuario.getDscCecoSolicitante());
			    		control.setUsuSolicitante(solReqUsuario.getUsuSolicitante());
			    		control.setTipoRequerimiento(solReqUsuario.getDscTipoRequerimiento());
			    		control.setEstado(solReqUsuario.getEstadoReg());
			    		//control.setCodSubTipoRequerimiento(solReqUsuario.getSubTipoRequerimiento());
			    		control.setCodTipoBien(solReqUsuario.getSubTipoRequerimiento());
			    		control.setNroReqAsociado(solReqUsuario.getNroRequerimientoOri());
			    		control.setMotivoDevolucion(solReqUsuario.getMotivoDevolucion());
			    		//carga la bandeja de la solicitud del usuario
			    		cargaBandejaDetalle(control);   
    				}
    	}
    }
    private void cargaBandejaDetalle(SreqDevolverBienesCommand control){    	
		List listaBien=this.solRequerimientoManager.getAllSolReqDetalle(control.getCodRequerimiento(), "");		
		if(listaBien!=null){
			if(listaBien.size()>0){
				control.setListaBienes(listaBien);
				control.setTamListaBienes(""+listaBien.size());
			}
			else{
				control.setTamListaBienes("0");
			}
		}    	
    }
    private String InsertSolicitudDevolucion(SreqDevolverBienesCommand control){
    	try{
    		System.out.println("getCodDevolucion>>"+control.getCodDevolucion()+"<<");
    		System.out.println("getCodRequerimiento>>"+control.getCodRequerimiento()+"<<");
    		System.out.println("getCadenaCodigo>>"+control.getCadenaCodigo()+"<<");
    		System.out.println("getCadenaCantidad>>"+control.getCadenaCantidad()+"<<");
    		System.out.println("setCantidad>>"+control.getTamListaBienes()+"<<");
    		System.out.println("getMotivoDevolucion>>"+control.getMotivoDevolucion()+"<<");
    		
    		SolRequerimiento solicitudRequerimiento = new SolRequerimiento();
    		
    		solicitudRequerimiento.setCodDevolucion(control.getCodDevolucion());
    		solicitudRequerimiento.setIdRequerimiento(control.getCodRequerimiento());
    		solicitudRequerimiento.setCadenaCodigo(control.getCadenaCodigo());
    		solicitudRequerimiento.setCadenaCantidad(control.getCadenaCantidad());
    		solicitudRequerimiento.setCantidad(control.getTamListaBienes());
    		solicitudRequerimiento.setMotivoDevolucion(control.getMotivoDevolucion());
    		
    		String resultado="";
    		resultado=this.solRequerimientoManager.InsertSolDevolucion(solicitudRequerimiento, control.getCodUsuario());    		
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }/*
	private void InsertSolicitudRequerimientoDetalle(SreqBienesCommand control){
		if(control.getCodTipoRequerimiento().equals("0001")){
			System.out.println("InsertSolicitudRequerimeintoDetalleBien--->");
			InsertSolicitudRequerimientoDetalleBien(control);
		}
		if(control.getCodTipoRequerimiento().equals("0002")){
			System.out.println("InsertSolicitudRequerimeintoDetalleServicio--->");
			InsertSolicitudRequerimientoDetalleServicio(control);
		}
	}
	private void InsertSolicitudRequerimientoDetalleBien(SreqBienesCommand control){
		StringTokenizer cadenaCodigo = new StringTokenizer(control.getCadenaCodigo(),"|");
		StringTokenizer cadenaCodBien = new StringTokenizer(control.getCadenaCodBien(),"|");
		StringTokenizer cadenaCantidad = new StringTokenizer(control.getCadenaCantidad(),"|");
		StringTokenizer cadenaCodTipoGasto = new StringTokenizer(control.getCadenaCodTipoGasto(),"|");
		StringTokenizer cadenaFechaEntrega = new StringTokenizer(control.getCadenaFechaEntrega(),"|");
		StringTokenizer cadenaDescripcion = new StringTokenizer(control.getCadenaDescripcion(),"|");
		
		while(cadenaCodBien.hasMoreTokens()){
			String tokenCodigo = cadenaCodigo.nextToken();
			String tokenCodBien = cadenaCodBien.nextToken();
			String tokenCantidad = cadenaCantidad.nextToken();
			if(tokenCantidad.equals("$")){
				tokenCantidad="";
			}
			String tokenCodTipoGasto = cadenaCodTipoGasto.nextToken();
			String tokenFechaEntrega = cadenaFechaEntrega.nextToken();
			if(tokenFechaEntrega.equals("$")){
				tokenFechaEntrega="";
			}
			String tokenDescripcion = cadenaDescripcion.nextToken();
			if(tokenDescripcion.equals("$")){
				tokenDescripcion="";
			}		
			
			SolRequerimientoDetalle obj = new SolRequerimientoDetalle();
			obj.setCodRequerimiento(control.getCodRequerimiento());
			obj.setCodigo(tokenCodigo);
			obj.setNombreServicio("");
			obj.setDescripcion(tokenDescripcion);
			obj.setCodigoBien(tokenCodBien);
			obj.setCantidad(tokenCantidad);
			obj.setTipoGasto(tokenCodTipoGasto);
			obj.setFechaEntrega(tokenFechaEntrega);			
			
			System.out.println("-->"+obj.getCodRequerimiento()+
					"|"+obj.getCodigo()+
					"|"+obj.getNombreServicio()+
					"|"+obj.getDescripcion()+
					"|"+obj.getCodigoBien()+
					"|"+obj.getCantidad()+
					"|"+obj.getTipoGasto()+
					"|"+obj.getFechaEntrega()+
					"|"+control.getCodUsuario());
			this.solRequerimientoManager.InsertSolRequerimientoDetalle(obj, control.getCodUsuario());
		}		
	}
    private void InsertSolicitudRequerimientoDetalleServicio(SreqBienesCommand control){
    	SolRequerimientoDetalle obj = new SolRequerimientoDetalle();
		obj.setCodRequerimiento(control.getCodRequerimiento());
		obj.setCodigo(control.getCodReqDetalleServicio());
		obj.setNombreServicio(control.getNombreServicio());
		obj.setDescripcion(control.getDescripcion());
		obj.setCodigoBien("");
		obj.setCantidad("");
		obj.setTipoGasto("");
		obj.setFechaEntrega(control.getFechaAtencion());
		System.out.println("-->"+obj.getCodRequerimiento()+
				"|"+obj.getCodigo()+
				"|"+obj.getNombreServicio()+
				"|"+obj.getDescripcion()+
				"|"+obj.getCodigoBien()+
				"|"+obj.getCantidad()+
				"|"+obj.getTipoGasto()+
				"|"+obj.getFechaEntrega()+
				"|"+control.getCodUsuario());
		this.solRequerimientoManager.InsertSolRequerimientoDetalle(obj, control.getCodUsuario());
    }
	private String DeleteSolRequerimientoDetalle(SreqBienesCommand control){
    	try{
    		String resultado="";
    		resultado=this.solRequerimientoManager.DeleteSolRequerimientoDetalle(control.getCodRequerimiento(), control.getSeleccion(), control.getCodUsuario());
    		return resultado;
    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
	private String EnviarSolicitudRequerimientoUsuario(SreqBienesCommand control){
		String resultado="";
		resultado=this.solRequerimientoManager.EnviarSolRequerimientoUsuario(control.getCodRequerimiento(), control.getCodUsuario());
		return resultado;
	}*/
    private String EnviarAlmacen(SreqDevolverBienesCommand control){
    	String resultado="";
		resultado=this.solRequerimientoManager.EnviarSolDevolucionUsuario(control.getCodDevolucion(), control.getCodUsuario());		
		return resultado;
    }
}
