package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.DetalleOrden;
import com.tecsup.SGA.modelo.OrdenesAprobadas;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.service.logistica.GestionOrdenesManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.GestOrdenDetalleOrdenCommand;

public class GestOrdenDetalleOrden_AprobFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(GestOrdenDetalleOrden_AprobFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	GestionOrdenesManager gestionOrdenesManager;
	
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	GestOrdenDetalleOrdenCommand command= new GestOrdenDetalleOrdenCommand();
    	
    	command.setCodOrden(request.getParameter("txhCodOrden"));
    	System.out.println("CodOrden: "+command.getCodOrden());
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setCodPerfil(request.getParameter("txhCodPerfil"));
    	command.setBanderaPerfil(request.getParameter("txhBanderaPerfil"));
    	command.setCodSede(request.getParameter("txhCodSede"));
    	command.setCodEstado(request.getParameter("txhCodEstado"));
    	command.setIndiceAprobacion(request.getParameter("txhIndiceAprob"));
    	command.setConsteCodEstadoEnAprobacion(CommonConstants.COD_ESTADO_APROBACION);
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion"));
    	command.setCodEstCotizacion(request.getParameter("txhCodEstCotizacion"));
    	command.setCodTipoReq(request.getParameter("txhCodTipoReq"));
    	//SETEANDO LA PROPIEDAD DE COMENTARIO
    	command.setDesComentario("");
    	
    	if(command.getCodOrden()!=null)
    	{ BuscarOrdenById(command);
    	  BuscarDetalleByOrden(command);
    	  BuscarCondicionByOrden(command);
    	  BuscarAprobadorByOrden(command);
    	  BuscarSolicitanteByOrden(command);
    	}
    	
    	log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	GestOrdenDetalleOrdenCommand control= (GestOrdenDetalleOrdenCommand) command;
    	
    	if(control.getOperacion().trim().equals("APROBAR"))
    	{ String resultado="";
    		resultado=Aprobar(control);	
    		if(resultado.trim().equals("-1")) 
    			control.setMsg("APROBAR_ERROR");
    		else{ if(resultado.trim().equals("0")) 
   	       	      control.setMsg("APROBAR_OK");
   	         	  else  
   	         	  { if(resultado.trim().equals("1"))
   	       	       { control.setMsg("APROBAR_OK");
   	       	    	ServletEnvioCorreo servletEnvioCorreo= new ServletEnvioCorreo();
   	       	      	String mensaje="";
   	       	      	String asunto="";
   	       	      	List lista1= new ArrayList();
   	       	      	List lista2= new ArrayList();
   	       	      	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_LOGISTICA_EMAIL 
	    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
   	       	      	lista2=this.gestionOrdenesManager.GetEnvioCorreoOrden(control.getCodOrden());
   	       	      	if(lista1!=null && lista2!=null)
   	       	      	{  TipoTablaDetalle TTDetalle= new TipoTablaDetalle();
   	       	      		if(lista1.size()>0 && lista2.size()>0)
   	       	      		{ TTDetalle=(TipoTablaDetalle)lista1.get(1);
   	       	      		  asunto=TTDetalle.getDscValor2().trim();  
	   	       	      	   for(int i=0;i<lista2.size();i++)
	   	       	      	   { OrdenesAprobadas ordenesAprobadas = new OrdenesAprobadas();
	   	       	      	   	 ordenesAprobadas=(OrdenesAprobadas)lista2.get(i);
	  		   		     
	   	       	      	     mensaje=TTDetalle.getDescripcion().trim();
	   	       	      	     mensaje=mensaje.replace("NMB_PROV", ordenesAprobadas.getDscProveedor().trim());
	   	       	      	     mensaje=mensaje.replace("NRO_ORDEN", ordenesAprobadas.getNroOrden().trim());
	   	       	      	     asunto=asunto.replace("TIPO_ORDEN", ordenesAprobadas.getDscTipoOrden().trim()); 
	   	       	      	     mensaje=mensaje.replace("URL_SISTEMA", CommonConstantsEmail.DIRECCION_LOGISTICA);
	   	       	      	     System.out.println("Texto a enviar en el Correo: \n"+mensaje);
	   	       	      	    try{
	   	       	      		    servletEnvioCorreo.EnviarCorreo(ordenesAprobadas.getEmailProveedor(), CommonConstantsEmail.TECSUP_EMAIL, 
	   	       	      			ordenesAprobadas.getDscProveedor().trim(), CommonConstantsEmail.TECSUP_LOGISTICA_EMAIL_COTIZACION, 
	  		        	    		asunto, mensaje);
	  		        
	   	       	      		   System.out.println("Texto a enviar en el Correo: \n"+mensaje);
	  		        	   
	   	       	      	   }
   	       	      	       catch(Exception ex){
  		    	    	
  		    	          }
  		   		        }
   	       	      		} 
   	       	      		
   	       	      	}
   	       	      }
   	         	
   	       	    }   
   	    	} 
    	}
    	else{ if(control.getOperacion().equals("RECHAZAR"))
    		{String resultado="";
    		 resultado=Rechazar(control);
    		 if(resultado.equals("0")) 
    	       	{control.setMsg("RECHAZAR_OK");
    	       	}
    	       	else{ if(resultado.equals("-1")) control.setMsg("RECHAZAR_ERROR");
    	    	} 
    		}  		
    	}
    	
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/GestOrdenCompraServicio/GestOrdenDetalleOrden_Aprob","control",control);
    }
    
    public void BuscarOrdenById(GestOrdenDetalleOrdenCommand control)
    {
		System.out.println("Inicio de buscar OrdenById");
    	List lista= new ArrayList();
    	lista=this.gestionOrdenesManager.GetAllOrdenById(control.getCodOrden());
    	if(lista!=null)
    	{ if(lista.size()>0)
    		{ DetalleOrden detalleOrden= new DetalleOrden();
    		  detalleOrden=(DetalleOrden)lista.get(0);
    		  control.setNroOrden(detalleOrden.getDscNroOrden().trim());
    		  control.setFecEmision(detalleOrden.getFechaEmision().trim());
    		  control.setTxtProveedorRuc(detalleOrden.getDscNroDocumento().trim());
    		  control.setTxtProveedorNombre(detalleOrden.getDscProveedor().trim());
    		  control.setTxtAtencion(detalleOrden.getDscAtencionA().trim());
    		  control.setTxtMoneda(detalleOrden.getDscMonto().trim());
    		  control.setTxtComprador(detalleOrden.getDscComprador().trim());
    		  control.setTxtTipoMoneda(detalleOrden.getDscMoneda().trim());
    		  control.setTxtIgv(detalleOrden.getIgv().trim());
    		  control.setIndImportacion(detalleOrden.getIndImportacion());
    		  control.setImporteOtros(detalleOrden.getImporteOtros());
    		  //ALQD,18/08/08. nuevas propiedades
    		  control.setCodCotizacion(detalleOrden.getCodCotizacion().trim());
    		  control.setCodEstCotizacion(detalleOrden.getCodEstCotizacion().trim());
    		  control.setCodTipoReq(detalleOrden.getCodTipoReq().trim());
    		  //ALQD,05/02/09. NUEVA PROPIEDAD
    		  control.setNumCotizacion(detalleOrden.getNumCotizacion().trim());
    		  //ALQD,19/02/09. LEYENDO NUEVAS PROPIEDADES
    		  control.setIndInversion(detalleOrden.getIndInversion());
    		  control.setCodOrdenTipoGasto(detalleOrden.getCodOrdenTipoGasto());
    		  control.setDesOrdenTipoGasto(detalleOrden.getDesOrdenTipoGasto());
    		  //ALQD,24/02/09.A�ADIENDO NUEVA PROPIEDAD
    		  control.setCodTipPago(detalleOrden.getTipoDePago());
    		  control.setDesTipPago(detalleOrden.getDesTipPago());
    		  //ALQD,04/04/09.A�ADIENDO NUEVA PROPIEDAD
    		  control.setDesEstado(detalleOrden.getDescEstadoOC());
    		}
    	}
    }
    
    public void BuscarDetalleByOrden(GestOrdenDetalleOrdenCommand control)
	{	List lista= new ArrayList();
		float suma=0;
		float sk=0;
		float b=0;
		lista=this.gestionOrdenesManager.GetAllDetalleByOrden(control.getCodOrden());
		if(lista!=null)
    	{
			control.setListaBandeja(lista);
			if(lista.size()==0)
			{ control.setBanListaBandeja("0");
			}
			
			if(lista.size()>0)
    		{ 
    		  control.setTxtSubTotal("");
    		  control.setTxtTotal("");
    		  float sumaTotal=0;
    		  for(int a=0;a<lista.size();a++)
			  { DetalleOrden detalleOrden= new DetalleOrden();
    			detalleOrden=(DetalleOrden)lista.get(a);
			    sk=Float.valueOf(detalleOrden.getSubtotal().trim());
			    suma=suma+sk;

			  } 
    		  System.out.println("valor de Suma: "+suma);
			  control.setTxtSubTotal(String.valueOf(suma));
			  sumaTotal=suma+ Float.valueOf(control.getTxtIgv().trim());
			  control.setTxtTotal(String.valueOf(sumaTotal));
    		}
			
    	}
		else control.setListaBandeja(new ArrayList());
    }
    
    public void BuscarAprobadorByOrden(GestOrdenDetalleOrdenCommand control)
    { List lista= new ArrayList();
	  lista=this.gestionOrdenesManager.GetAllAprobadorByOC(control.getCodOrden());
	  if(lista!=null)
  	{
		control.setListaAprobadores(lista);
		if(lista.size()==0)
		   control.setBanListaAprobadores("0");
  	}
	  else control.setListaAprobadores(new ArrayList());
    }

    public void BuscarSolicitanteByOrden(GestOrdenDetalleOrdenCommand control)
    { List lista= new ArrayList();
	  lista=this.gestionOrdenesManager.getSolicitantesOrdenCompra(control.getCodOrden());
	  if(lista!=null)
  	{
		control.setListaSolicitantesProd(lista);
		if(lista.size()==0)
		   control.setBanListaSolicitantesProd("0");
  	}
	  else control.setListaSolicitantesProd(new ArrayList());
    }

    public void BuscarCondicionByOrden(GestOrdenDetalleOrdenCommand control)
    { List lista= new ArrayList();
	  lista=this.gestionOrdenesManager.GetAllCondicionByOrden(control.getCodOrden());
	  if(lista!=null)
  	{
		control.setListaBandejaCondicion(lista);
		if(lista.size()==0)
		   control.setBanListaBandejaCondicion("0");
  	}
	  else control.setListaBandejaCondicion(new ArrayList());
    }

    public String Aprobar(GestOrdenDetalleOrdenCommand control)
    { 
     //ALQD,19/02/09. AGREGANDO NUEVO PARAMETRO
     String resultado="";
     System.out.println("CodOrden: "+control.getCodOrden()
    		 +">>CodUsuario<<"+control.getCodUsuario()
    		 +">>IndInversion<<"+control.getIndInversion());
     resultado=this.gestionOrdenesManager.UpdateAprobarOrden(control.getCodOrden(),
    		 control.getCodUsuario(),control.getIndInversion());
     System.out.println("Resultado Aprobar: "+resultado);	
     return resultado;
    }
    
    public String Rechazar(GestOrdenDetalleOrdenCommand control)
    {
    	//ALQD,19/02/09. AHORA SE PASARA UN COMENTARIO EN EL RECHAZO
    	String resultado="";
    	System.out.println("CodOrden: "+control.getCodOrden()
    			+">>CodUsuario<<"+control.getCodUsuario()
    			+">>Comentario<<"+control.getDesComentario());
        resultado=this.gestionOrdenesManager.UpdateRechazarOrden(control.getCodOrden(),
        		control.getCodUsuario(),control.getDesComentario());
        System.out.println("Resultado Rechazar: "+resultado);	
        return resultado;
    }
    
    public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public GestionOrdenesManager getGestionOrdenesManager() {
		return gestionOrdenesManager;
	}

	public void setGestionOrdenesManager(GestionOrdenesManager gestionOrdenesManager) {
		this.gestionOrdenesManager = gestionOrdenesManager;
	}
	
}
