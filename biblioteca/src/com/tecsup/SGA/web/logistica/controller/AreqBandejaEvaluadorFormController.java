package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.AreqBandejaEvaluadorCommand;
import com.tecsup.SGA.web.logistica.command.AtenRequerimientosBandejaMantenimientoCommand;
import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;

public class AreqBandejaEvaluadorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AreqBandejaEvaluadorFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	AreqBandejaEvaluadorCommand command = new AreqBandejaEvaluadorCommand();

    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    	}
    	command.setCodSede(request.getParameter("txhCodSede"));
    	log.info("CodSede: "+command.getCodSede());
        command.setConsteCodBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
        command.setConsteCodServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
    	command.setValorIndGrupo("0");
        command.setIndGrupo("0");
    	
        BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean!= null)
         {  log.info("BandejaLogisticaBean:INICIO");
            if(bandejaBean.getCodBandeja().equals("2")){
            	     	 	
            	command.setCodTipoRequerimiento(bandejaBean.getValor1());
            	command.setNroRequerimiento(bandejaBean.getValor2());
            	command.setFecInicio(bandejaBean.getValor3());
            	command.setFecFinal(bandejaBean.getValor4());
            	command.setCodCeco(bandejaBean.getValor5());
            	command.setUsuSolicitante(bandejaBean.getValor6());
       	 	    if(command.getCodTipoRequerimiento().equals(command.getConsteCodServicio())) 
	    	    {
       	 	    command.setCodTipoServicio(bandejaBean.getValor7()); 
       	 	    command.setIndGrupo(bandejaBean.getValor8());
	    	    }
       	 	    else{ command.setCodTipoServicio("-1"); 
       	 	    	  command.setIndGrupo("0");}
       	 	    
            	BuscarBandeja(command, request);
            }
       	  log.info("BandejaLogisticaBean:FIN");
         }
        else{ command.setListaBien(new ArrayList());
        	  command.setListaServicio(new ArrayList()); 
        	  command.setBanListaServicio("0");
        	  command.setBanListaBien("0");
        	  }
        
        List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();
    	List lista4= new ArrayList();
    	
    	command.setConsteGrupoGenerales(CommonConstants.SOL_AREQ_GRUPOS_GENERALES);
    	command.setConsteGrupoMantenimiento(CommonConstants.SOL_AREQ_GRUPOS_MANTENIMIENTO);
    	command.setConsteGrupoServicios(CommonConstants.SOL_AREQ_GRUPOS_SERVICOS);
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    	command.setListTipoRequerimiento(lista1);
    	lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista2!=null)
    	command.setListEstado(lista2);
    	lista3=this.seguridadManager.GetAllCentrosCosto(command.getCodSede(),command.getCodPerfil(),command.getCodUsuario());
    	if(lista3!=null)
    	command.setListCeco(lista3);
    	System.out.println("codTipoReq: "+command.getCodTipoRequerimiento());
    	ListarCombo(command, request);
    	
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AreqBandejaEvaluadorCommand control = (AreqBandejaEvaluadorCommand)command;
    	control.setBanListaServicio("");
    	control.setBanListaBien("");
    	
    	if(control.getOperacion().equals("BUSCAR")){
    		 BuscarBandeja( control,  request);
    		
    	}
    	else{
    		  if(control.getOperacion().equals("RADIO")){
       		     ListarCombo( control,  request);
       		     BuscarBandeja( control,  request);
       		     
    	 }
    	}
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/SolRequerimiento/Areq_Bandeja_Evaluador","control",control);
    }
    
    public void BuscarBandeja(AreqBandejaEvaluadorCommand control, HttpServletRequest request){
    
    GuardarDatosBandeja(control, request);
    	
    if(control.getCodCeco().equals("-1")) control.setCodCeco("");
    if(control.getCodTipoServicio().equals("-1")) control.setCodTipoServicio("");
    	
    log.info(control.getCodUsuario()+">><<"+
    		control.getCodPerfil()+">><<"+control.getCodTipoRequerimiento()+">><<"+
    		control.getNroRequerimiento()+">><<"+control.getFecInicio()+">><<"+control.getFecFinal()+
    		">><<"+control.getCodCeco()+">><<"+"xD");
    
    if(control.getIndGrupo()==null)
    	control.setIndGrupo("");
    
    System.out.println("IndGrupo: "+control.getIndGrupo());
    String TipoServicio="";
    String IndGrupo="";
    if(control.getCodTipoRequerimiento().equals(control.getConsteCodBien()))
    {	TipoServicio="";
        IndGrupo="";
    }
    else{ if(control.getCodTipoRequerimiento().equals(control.getConsteCodServicio())) 
    	   {
    	    TipoServicio=control.getCodTipoServicio(); 
    	    IndGrupo=control.getIndGrupo();
    	    }
    }
    System.out.println("TipoServicio: "+TipoServicio+" IndGrupo: "+IndGrupo + " ControlTipoServicio: "+control.getCodTipoServicio());
    List listaTipo=this.solRequerimientoManager.GetAllAproRequerimientoUsuario(control.getCodUsuario(),
    		control.getCodPerfil(), control.getCodTipoRequerimiento(), control.getNroRequerimiento(),
    		control.getFecInicio(), control.getFecFinal(), control.getCodCeco(), TipoServicio, IndGrupo,
    		control.getCodSede());
   
    if(listaTipo==null)
    	  log.info("xD");
    else  log.info("Size: "+listaTipo.size());
    
    
    request.getSession().setAttribute("listaNovedades", listaTipo);
    if(control.getCodTipoRequerimiento().equals(control.getConsteCodBien()))
    {	control.setBanderaTipoReq("BIEN");  ///request.setAttribute("TIPO_REQ", "BIEN");
        log.info("Size Bien :P :"+listaTipo.size());
       
        //request.getSession().removeAttribute("listaServicio");
        if(listaTipo!=null)
        {control.setListaBien(listaTipo);
         if(listaTipo.size()==0)
        	 control.setBanListaBien("0");
         else control.setBanListaBien("1");
        }
        else{ control.setBanListaBien("0");
        	  control.setListaBien(new ArrayList());
        }
        control.setValorIndGrupo("");
        System.out.println("BanListaBien: "+control.getBanListaBien());
        request.getSession().setAttribute("listaBien", listaTipo);
        
    }
    else{ if(control.getCodTipoRequerimiento().equals(control.getConsteCodServicio()))
    		{ control.setBanderaTipoReq("SERVICIO"); //request.setAttribute("TIPO_REQ", "SERVICIO");
    		  log.info("Size Servicio :D :"+listaTipo.size());
    		  
    		  //request.getSession().removeAttribute("listaBien");
    		  if(listaTipo!=null)
    		  {  control.setListaServicio(listaTipo);
    		  	 if(listaTipo.size()==0)
    		  		 control.setBanListaServicio("0");
    		  	 else control.setBanListaServicio("1");
    		  }
    		  else{ control.setBanListaServicio("0");
    		  	    control.setListaServicio(new ArrayList()); 
    		  }
    		  
    		  if(control.getIndGrupo().equals("0"))
    	        	control.setValorIndGrupo("0");
    		  else{ if(control.getIndGrupo().equals("1"))
    			     control.setValorIndGrupo("1");}
    		  request.setAttribute("TIPO", "RADIO");
    		  request.getSession().setAttribute("listaServicio", listaTipo); 
    		  System.out.println("BanListaServicio: "+control.getBanListaServicio());
    		 }
    	  }
    System.out.println("BanListaBien: "+control.getBanListaBien()+
    		           ">>BanListaServicio: "+control.getBanListaServicio());
    control.setListCeco(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),control.getCodPerfil(),control.getCodUsuario()));	
    ListarCombo(control, request);
    }
    
    public void ListarCombo(AreqBandejaEvaluadorCommand control, HttpServletRequest request)
    {List lista= new ArrayList();
     List lista1= new ArrayList();
     List lista2= new ArrayList();
     List lista3= new ArrayList();
     TipoTablaDetalle tipoTablaDetalle1 = new TipoTablaDetalle();
     TipoTablaDetalle tipoTablaDetalle2 = new TipoTablaDetalle();
    if(control.getIndGrupo().equals("0"))
    { System.out.println(":D 0");
      lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
    			, "", "", control.getConsteGrupoGenerales(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
      lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
  			, "", "", control.getConsteGrupoServicios(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
      for(int i=0;i<lista1.size();i++)
      {  tipoTablaDetalle1=(TipoTablaDetalle)lista1.get(i);
    	  lista.add(tipoTablaDetalle1);
      }
      for(int k=0;k<lista2.size();k++)
      {  tipoTablaDetalle2=(TipoTablaDetalle)lista2.get(k);
    	  lista.add(tipoTablaDetalle2);
      }
      if(lista!=null)
    	  control.setListaCodTipoServicio(lista);
      System.out.println(":D  Lista: "+lista.size()+" Lista1: "+lista1.size()+" Lista2: "+lista2.size());
      control.setValorIndGrupo("0");
      
    }
    else{ if(control.getIndGrupo().equals("1"))
    		{ System.out.println(":D 1");
    	       lista3=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
        			, "", "", control.getConsteGrupoMantenimiento(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	      if(lista3!=null)
    	    	  control.setListaCodTipoServicio(lista3);
    	      control.setValorIndGrupo("1");
    	      System.out.println(":D Lista "+control.getListaCodTipoServicio().size());
    	     
    		}
    	
    }
    
     control.setListCeco(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),control.getCodPerfil(),control.getCodUsuario()));
    }
    
    public void GuardarDatosBandeja(AreqBandejaEvaluadorCommand control,HttpServletRequest request)
	  {
	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	 	
	 	 bandejaBean.setCodBandeja("2");
	 	 String TipoServicio="-1";
	     String IndGrupo="0";
	      if(control.getCodTipoRequerimiento().equals(control.getConsteCodServicio())) 
	    	   {
	    	    TipoServicio=control.getCodTipoServicio(); 
	    	    IndGrupo=control.getIndGrupo();
	    	   }
	     bandejaBean.setValor1(control.getCodTipoRequerimiento());
	 	 bandejaBean.setValor2(control.getNroRequerimiento());
	 	 bandejaBean.setValor3(control.getFecInicio());
	 	 bandejaBean.setValor4(control.getFecFinal());
	 	 bandejaBean.setValor5(control.getCodCeco());
	 	 bandejaBean.setValor6(control.getUsuSolicitante());
	 	 bandejaBean.setValor7(TipoServicio);
	 	 bandejaBean.setValor8(IndGrupo);
	 	 
	 		 		 	 
	 	 log.info("GuardarDatosBandeja");
	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	  }
    
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
}
