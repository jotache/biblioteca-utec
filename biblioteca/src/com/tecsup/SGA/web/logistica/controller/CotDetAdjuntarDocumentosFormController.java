package com.tecsup.SGA.web.logistica.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;

import java.io.FilenameFilter;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.MetodosConstants;
import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.evaluaciones.command.AdjuntarArchivoCatCommand;
import com.tecsup.SGA.web.logistica.command.CotDetAdjuntarDocumentosCommand;

public class CotDetAdjuntarDocumentosFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(CotDetAdjuntarDocumentosFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	MetodosConstants metodo= new MetodosConstants();
	
	public CotDetAdjuntarDocumentosFormController(){
		super();
		setCommandClass(CotDetAdjuntarDocumentosCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	CotDetAdjuntarDocumentosCommand command= new CotDetAdjuntarDocumentosCommand();
        
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion"));
    	command.setCodCotizacionDet(request.getParameter("txhCodDetCotizacion"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setTipoProcedencia(request.getParameter("txhTipoProcedencia"));
    	
//    	request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
//    	request.setAttribute("strExtensionXLS", CommonConstants.GSTR_EXTENSION_XLS); 
//    	request.setAttribute("strExtensionDOCX", CommonConstants.GSTR_EXTENSION_DOCX);
//    	request.setAttribute("strExtensionXLSX", CommonConstants.GSTR_EXTENSION_XLSX);
//    	request.setAttribute("strExtensionPDF", CommonConstants.GSTR_EXTENSION_PDF); 
    	
    	command.setListaTipoAdjunto(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ADJUNTOS,
    			"", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
//    	System.out.println("TipoProcedencia: "+command.getTipoProcedencia());
    	log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
	 throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	CotDetAdjuntarDocumentosCommand control= (CotDetAdjuntarDocumentosCommand) command;
    	
    	if("GRABAR".equals(control.getOperacion())){
    		String rpta="";    		
    		byte[] bytesArchivo = control.getTxtArchivo();
    		
    		String fileNameArchivo="";
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		
    		//String uploadDirArchivo = "\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_DOCUMENTOS;            
//    		String uploadDirArchivo = getServletContext().getRealPath("/"+CommonConstants.STR_RUTA_DOCUMENTOS_02);
    		String uploadDirArchivo = CommonConstants.DIRECTORIO_DATA_PRIVADO + CommonConstants.STR_RUTA_DOCUMENTOS_02;
    		File dirPath = new File(uploadDirArchivo);
    		if (!dirPath.exists()) {
    			dirPath.mkdirs();
    		} 
    		
    		String sep = System.getProperty("file.separator");
            control.setRuta(uploadDirArchivo);
                        
	        if (bytesArchivo.length>0){
	        	 /*fileNameArchivo = CommonConstants.FILE_NAME_DOC+control.getCodPeriodo()+control.getCodProducto()+
	        	 control.getCodCiclo()+"."+control.getExtArchivo();*/
	        	 fileNameArchivo=metodo.GenerarNombreArchivoAdjuntar(CommonConstants.SOL_COTIZACION_ADJUNTO, 
	        			 control.getExtArchivo());
	        	 
//	        	 File uploadedFileArchivo = new File(uploadDirArchivo + sep + fileNameArchivo);
//	        	 FileCopyUtils.copy(bytesArchivo, uploadedFileArchivo);
	        	 
	        	 FileOutputStream output = null;
	        	 try{
	        		 output = new FileOutputStream(new File(uploadDirArchivo + sep + fileNameArchivo));
	        		 output.write(bytesArchivo);
	        		 output.close();	        		 
	        	 }catch(Exception ex){
	        		 log.error("Error subiendo archivo",ex);
	        	 }	
	        	 
	        	 control.setNomNuevoArchivo(fileNameArchivo);
	        	 rpta = Insertar(control);	

	        	 if(rpta.equals("-1"))
	        		control.setMsg("ERROR");
	        	 else
	        		control.setMsg("OK");
	        				        	 
	        }else{   	
	        	control.setMsg("ERROR1");
	        }
    	}
    	
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/CotBienesYServicios/CotDet_AdjuntarDocumento","control",control);
    }
    
    private String Insertar(CotDetAdjuntarDocumentosCommand control){
    	try{ String resultado="";
    		
    		log.info("InsertAdjuntoCasoCat");
    	
    	if(control.getTipoProcedencia()!=null)
    	{	if(control.getTipoProcedencia().equals("0"))
    			 control.setCodProveedor("");
    		else control.setCodProveedor(control.getCodUsuario());
    		log.info(">>CodCotizacion<<"+control.getCodCotizacion()+">>CodCotizacionDet<<"+
					 control.getCodCotizacionDet()+">>CodTipoAdjunto<<"+
					 control.getCodTipoAdjunto()+">>CodProveedor<<"+
					 control.getCodProveedor()+">>NomArchivo<<"+
					 control.getNomArchivo()+">>NomNuevoArchivo<<"+
					 control.getNomNuevoArchivo()+">>CodUsuario<<"+
					 control.getCodUsuario());
    		resultado=this.cotBienesYServiciosManager.insertDocumentoByItem(control.getCodCotizacion()
    					, control.getCodCotizacionDet()
    					, control.getCodTipoAdjunto()
    					, control.getCodProveedor()
    					, control.getNomArchivo()
    					, control.getNomNuevoArchivo()
    					, control.getCodUsuario());
    		log.info("resultado: "+resultado);
    	}
    		return resultado; 
    		
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }  
}
