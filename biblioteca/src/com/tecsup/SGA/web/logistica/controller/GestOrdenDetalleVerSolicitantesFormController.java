package com.tecsup.SGA.web.logistica.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.logistica.GestionOrdenesManager;
import com.tecsup.SGA.web.logistica.command.GestOrdenDetalleOrdenCommand;

public class GestOrdenDetalleVerSolicitantesFormController extends
		SimpleFormController {
	
	private static Log log = LogFactory.getLog(GestOrdenDetalleVerSolicitantesFormController.class);
	GestionOrdenesManager gestionOrdenesManager;
	
	/**
	 * @param gestionOrdenesManager the gestionOrdenesManager to set
	 */
	public void setGestionOrdenesManager(GestionOrdenesManager gestionOrdenesManager) {
		this.gestionOrdenesManager = gestionOrdenesManager;
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.AbstractFormController#formBackingObject(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		GestOrdenDetalleOrdenCommand command= new GestOrdenDetalleOrdenCommand();
		
		command.setCodOrden(request.getParameter("prmCodOrden"));
		String codOrden = command.getCodOrden();
		//System.out.println("codOrden:"+codOrden);
		log.info("codOrden:"+codOrden);
		//listar productos y sus solicitantes...
		command.setListaSolicitantesProd(this.gestionOrdenesManager.getSolicitantesOrdenCompra(codOrden));
		
		System.out.println("lista size:" + command.getListaSolicitantesProd().size());
		
		return command;
	}
	
	
}
