package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.logistica.command.ReportesConsultaBandejaCommand;

public class ReportesConsultaBandejaFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(ReportesConsultaBandejaFormController.class);
	TablaDetalleManager tablaDetalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
	
    	ReportesConsultaBandejaCommand command= new ReportesConsultaBandejaCommand();
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setDscUsuario(usuarioSeguridad.getNomUsuario());
    	}
    	
    	command.setCodSede(request.getParameter("txhCodSede"));
    	
    	command.setCodReporte1(CommonConstants.REPORTE_LOG_1);
    	command.setCodReporte2(CommonConstants.REPORTE_LOG_2);
    	command.setCodReporte3(CommonConstants.REPORTE_LOG_3);
    	command.setCodReporte4(CommonConstants.REPORTE_LOG_4);
    	command.setCodReporte5(CommonConstants.REPORTE_LOG_5);
    	command.setCodReporte6(CommonConstants.REPORTE_LOG_6);
    	command.setCodReporte7(CommonConstants.REPORTE_LOG_7);
    	command.setCodReporte8(CommonConstants.REPORTE_LOG_8);
    	command.setCodReporte9(CommonConstants.REPORTE_LOG_9);
    	command.setCodReporte10(CommonConstants.REPORTE_LOG_10);
    	command.setCodReporte11(CommonConstants.REPORTE_LOG_011);
    	command.setCodReporte29(CommonConstants.REPORTE_LOG_29);
    	
    	List lista1= new ArrayList();
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_REPORTE_LOGISTICA 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    		command.setListaReportes(lista1);
    	else command.setListaReportes(new ArrayList());
    	
   	 log.info("formBackingObject:FIN");
     return command;
 }
	
	protected void initBinder(HttpServletRequest request,
         ServletRequestDataBinder binder) {
 	NumberFormat nf = NumberFormat.getNumberInstance();
 	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
 	
 }
	 /**
	  * Redirect to the successView when the cancel button has been pressed.
	  */
	 public ModelAndView processFormSubmission(HttpServletRequest request,
	                                           HttpServletResponse response,
	                                           Object command,
	                                           BindException errors)
	 throws Exception {
	     //this.onSubmit(request, response, command, errors);
	 	return super.processFormSubmission(request, response, command, errors);
	 }
		
	 public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	         BindException errors)
	 		throws Exception {
	 	log.info("onSubmit:INI");
	 	
	 	ReportesConsultaBandejaCommand control= (ReportesConsultaBandejaCommand) command;
	 	
	 	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/consultaReportes/log_consulta_reportes","control",control);
    }


	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
}