package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.web.logistica.command.CotizacionBienesProveedorCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionDetalle;
import com.tecsup.SGA.modelo.Proveedor;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;

public class CotizacionBienesProveedorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotizacionBienesProveedorFormController.class);
	private DetalleManager detalleManager;
	private SeguridadManager seguridadManager;
	private SolRequerimientoManager solRequerimientoManager;
	private CotBienesYServiciosManager cotBienesYServiciosManager;
	private TablaDetalleManager tablaDetalleManager;	

	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public static void setLog(Log log) {
		CotizacionBienesProveedorFormController.log = log;
	}

	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	CotizacionBienesProveedorCommand command = new CotizacionBienesProveedorCommand();
    	//*********************************************************    	    	
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion") == null ? "": request.getParameter("txhCodCotizacion"));    	
    	command.setCodProveedor(request.getParameter("txhCodProveedor") == null ? "": request.getParameter("txhCodProveedor"));
    	command.setCodEstCotizacion(request.getParameter("txhCodEstCotizacion") == null ? "": request.getParameter("txhCodEstCotizacion"));
    	command.setCodTipoReq(request.getParameter("txhCodTipoReq") == null ? "": request.getParameter("txhCodTipoReq"));
		//ALQD,04/02/09. NUEVO PARAMETRO
		command.setNumCotizacion(request.getParameter("txhNumCotizacion"));
    	
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");
    	System.out.println("codCotizacion="+command.getCodCotizacion()+"<<");
    	System.out.println("codProveedor="+command.getCodProveedor()+"<<");
    	System.out.println("txhCodEstCotizacion="+command.getCodEstCotizacion()+"<<");
    	System.out.println("txhCodTipoReq="+command.getCodTipoReq()+"<<");
    	//*********************************************************
    	if(!command.getCodUsuario().equals("")){
    		cargaMoneda(command);
    		cargaBandeja(command);
    		
    	}
    	command.setFlagProv("0");
    	if(request.getParameter("prmFlgComprador")!=null){
    		/*OPCION PARA EL PREFIL DE COMPRADOR QUE TIENE ACCESO A LOS PRECIOS DE LOS PROVEEDORES*/
    		log.info("USUARIO COMPRADOR CARGANDO COMBO DE PROVEEDORES");
    		
    		log.info("FLG SETEADO..");
    		//LLAMADA AL SP DE CARLITOS
    		command.setListaProveedores(cotBienesYServiciosManager.getAllProvSenvByCoti(command.getCodCotizacion(),null));
    		
    		if( command.getListaProveedores().size()==0  )
    			{Proveedor obj = new Proveedor();
			      List lista= new ArrayList();
			      obj.setCodUnico("0099");
			      obj.setRazSoc("--Ninguno--");
			      lista.add(obj);
				  command.setListaProveedores(lista);
				  }
    		else{  Proveedor obj = new Proveedor();
    				obj=(Proveedor)command.getListaProveedores().get(0);
    			    command.setCodProveedor(obj.getCodUnico());
    			    cargaMoneda(command);
    			    cargaBandeja(command);}
    		 command.setFlgComprador("1");
    		
    		log.info("LISTA SETEADA..");
    	}
    	setIgvBD(command);
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	CotizacionBienesProveedorCommand control = (CotizacionBienesProveedorCommand)command;    	
    	String resultado="";
    	String resultadoEnviar="";
    	log.info("OPERACION>>"+control.getOperacion()+">>");
    	if (control.getOperacion().trim().equals("GRABARPRODUCTOS")){
    		System.out.println("getOtroCostos--> "+control.getOtroCostos());
    		resultado = GrabarCotizacionBienesProducto(control);
    		System.out.println("resultado de GRABAR COTIZACION-->"+resultado);
    		cargaMoneda(control);
    		cargaBandeja(control);
    		if (resultado.equals("0")){
    			control.setMsg("OKGRABARPRODUCTOS");
    		}
    		else{
    			control.setMsg("ERROR");    			
    		}
    	}
    	else if (control.getOperacion().trim().equals("GRABARCONDICIONES")){
    		resultado = GrabarCotizacionCondicionesProducto(control);
    		System.out.println("resultado de GRABAR COTIZACION-->"+resultado);
    		cargaMoneda(control);
    		cargaBandeja(control);
    		if (resultado.equals("0")){
    			control.setMsg("OKGRABARCONDICIONES");
    		}
    		else{
    			control.setMsg("ERROR");    			
    		}
    	}else if (control.getOperacion().trim().equals("ENVIAR")){
    		//CCORDVOA 14/06/2008 GRABAMOS ANTES DE ENVIAR
    		resultado = GrabarCotizacionBienesProducto(control);
    		System.out.println("resultado de GRABAR COTIZACION-->"+resultado);
    		if (resultado.equals("0")){
    			resultadoEnviar=EnviarCotizacion(control);
    			System.out.println("este es el resultado de enviar-->"+resultadoEnviar+"<<");
    			if (resultadoEnviar.equals("0")){
        			control.setMsg("OKENVIAR");
        		}
        		else{ if(resultadoEnviar.equals("-1"))
            		  control.setMsg("ERRORENVIAR");
        			  else if(resultadoEnviar.equals("-2"))
        			  		  control.setMsg("ERRORDETALLES");
        		}
    		}
    		else
    		{
    			control.setMsg("ERRORENVIAR");
    		}
    		cargaMoneda(control);
    		cargaBandeja(control);
			
			
    	}else if(control.getOperacion().trim().equals("CONSULTAR")){
    		control.setCodProveedor(control.getCboProveedor());
    		cargaMoneda(control);
			cargaBandeja(control);
    	}
    	//**************************
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/CotBienesYServicios/Cotizacion_Bienes_Proveedor","control",control);
    }
    
    //se cargan las dos bandejas la de productos y condiciones
    private void cargaBandeja(CotizacionBienesProveedorCommand control){
    	//ALQD,29/05/09.LEYENDO LOS DATOS DEL PROVEEDOR PARA CONOCER SI ES EXTRANJERO
    	System.out.println("CodProveedor>>"+control.getCodProveedor()+"<<");
    	control.setListaProveedores(this.cotBienesYServiciosManager.getAllProvSenvByCoti(control.getCodCotizacion(),control.getCodProveedor()));
    	Proveedor obj = new Proveedor();
		if(control.getListaProveedores()!=null){
	    	if(control.getListaProveedores().size()>0){
				obj=(Proveedor)control.getListaProveedores().get(0);
				control.setTipoProveedor(obj.getCodTipoProveedor());
				control.setOtroCostos(obj.getOtroCostos());
				System.out.println("setTipoProveedor>>"+control.getTipoProveedor()+"<<");
				System.out.println("setOtroCostos>>"+control.getOtroCostos()+"<<");
	    	}
		}
		
    	List lista=this.cotBienesYServiciosManager.GetAllBienesByProveedor(control.getCodProveedor(), control.getCodCotizacion());
    	if(lista!=null){
    		if(lista.size()>0){
    			control.setListaBienesProveedor(lista);
        		control.setNumListaBienes(""+lista.size());
        		
        		CotizacionDetalle cotizacionDetalle = (CotizacionDetalle)lista.get(0);
        		if ( cotizacionDetalle.getCodMoneda().trim().equals(""))
        			control.setCodMoneda(CommonConstants.MONEDA_SOLES);
        		else
        		{
        			log.info("Moneda Cotizacion Proveedor: " + cotizacionDetalle.getCodMoneda());
        			control.setCodMoneda(cotizacionDetalle.getCodMoneda());
        		}
    		}
    		else{
    			control.setNumListaBienes("0");
    		}	
    	}
    	
    	List listaCondicion=this.cotBienesYServiciosManager.GetAllCondicionesByProveedor(control.getCodCotizacion(), control.getCodProveedor());
    	
    	if(listaCondicion!=null){
    		if(listaCondicion.size()>0){
    			control.setListaCondicionesProveedor(listaCondicion);
        		control.setNumListaCondiciones(""+listaCondicion.size());
    		}
    		else{
    			control.setNumListaCondiciones("0");
    		}	
    	}
    	
    	
    	/***********************/
    	control.setListaProveedores(cotBienesYServiciosManager.getAllProvSenvByCoti(control.getCodCotizacion(),null));
    }
    private String GrabarCotizacionBienesProducto(CotizacionBienesProveedorCommand control){    	
    	try{
    		System.out.println("codCotizacion>>"+control.getCodCotizacion()+"<<");
    		System.out.println("codProveedor>>"+control.getCodProveedor()+"<<");    		
    		System.out.println("cadenaCodDetalle>>"+control.getCadenaCodDetalle()+"<<");
    		System.out.println("cadenaDescripcion>>"+control.getCadenaDescripcion()+"<<");
    		System.out.println("cadenaPrecios>>"+control.getCadenaPrecios()+"<<");
    		System.out.println("cantidad>>"+control.getCantidad()+"<<");
    		System.out.println("codUsuario>>"+control.getCodUsuario()+"<<");
    		System.out.println("otroCostos-->"+control.getOtroCostos());
    		
    		CotizacionDetalle cotizacion = new CotizacionDetalle();   		
    		
    		cotizacion.setCodigo(control.getCodCotizacion());
    		cotizacion.setCodProveedor(control.getCodProveedor());    		
    		cotizacion.setCadenaCodCotDetalle(control.getCadenaCodDetalle());
    		cotizacion.setCadenaDescripcion(control.getCadenaDescripcion());
    		cotizacion.setCadenaPrecios(control.getCadenaPrecios());
    		cotizacion.setCantidad(control.getCantidad());
    		cotizacion.setCodMoneda(control.getCodMoneda());
    		cotizacion.setOtroCostos(control.getOtroCostos());
    		
    		String resultado="";
    		resultado=this.cotBienesYServiciosManager.InsertBienesByProveedor(cotizacion, control.getCodUsuario());    				
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }    
    private String GrabarCotizacionCondicionesProducto(CotizacionBienesProveedorCommand control){
		String codCotizacion=control.getCodCotizacion();
		String codProveedor=control.getCodProveedor();
		String codUsuario=control.getCodUsuario();
		
		StringTokenizer cadenaCodDetalle = new StringTokenizer(control.getCadenaCodDetalle(),"|");		
		StringTokenizer cadenaDescripcion = new StringTokenizer(control.getCadenaDescripcion(),"|");
		
		String resultado="";
		
		while(cadenaCodDetalle.hasMoreTokens()){
			String tokenCodDetalle = cadenaCodDetalle.nextToken();
			
			String tokenDescripcion = cadenaDescripcion.nextToken();
			if(tokenDescripcion.equals("$")){
				tokenDescripcion="";
			}
			CotizacionDetalle cotizacion = new CotizacionDetalle();
			cotizacion.setCodigo(control.getCodCotizacion());
			cotizacion.setCodProveedor(control.getCodProveedor());
			cotizacion.setCodDetalle(tokenCodDetalle);
			cotizacion.setDescripcion(tokenDescripcion);
			
			System.out.println("-->"+cotizacion.getCodigo()+					
					"|"+cotizacion.getCodProveedor()+
					"|"+cotizacion.getCodDetalle()+
					"|"+cotizacion.getDescripcion());
			
			
    		resultado=this.cotBienesYServiciosManager.InsertCondicionesByProveedor(cotizacion, control.getCodUsuario());
		}
		return resultado;
    }
    private String EnviarCotizacion(CotizacionBienesProveedorCommand control){
    	try {
    		String resultado="";
    		resultado=this.cotBienesYServiciosManager.EnviarCotizacionByProveedor(control.getCodCotizacion(), control.getCodProveedor());
    		return resultado;
		}
    	catch (Exception e) {
    		e.printStackTrace();
		}
		return "-1";
	}
    private void cargaMoneda(CotizacionBienesProveedorCommand command)
    {
    	try
    	{
	    	TipoLogistica obj = new TipoLogistica();
	    	
	    	obj.setCodTabla(CommonConstants.TIPT_MONEDA);
	    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
	    	
	    	command.setListMonedas(this.detalleManager.GetAllTablaDetalle(obj));
    	}
    	catch(Exception ex)
    	{
    		command.setListMonedas(null);
    	}
    }
    private void setIgvBD(CotizacionBienesProveedorCommand command){
    	List lista = new ArrayList();
        lista = this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PARAMETROS_GENERALES 
    			, CommonConstants.TTDE_IGV_ID, "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
        
        TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
        if(lista!=null && lista.size()>0)
        {   tipoTablaDetalle = (TipoTablaDetalle)lista.get(0);
    	    command.setIgvBD(""+Float.valueOf(tipoTablaDetalle.getDscValor1().trim())/100);
        }
    }

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
}
