package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.AprobacionResponsableCommand;

public class AprobacionResponsableFomController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AprobacionResponsableFomController.class);
	DetalleManager detalleManager;
	SeguridadManager seguridadManager;
	SolRequerimientoManager solRequerimientoManager;
    public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public AprobacionResponsableFomController() {    	
        super();        
        setCommandClass(AprobacionResponsableCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AprobacionResponsableCommand command = new AprobacionResponsableCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodUsuario"));
    	command.setCodRequerimiento((String)request.getParameter("txhCodRequerimiento"));
    	command.setSede((String)request.getParameter("txhCodSede"));
    	command.setNroRequerimiento((String)request.getParameter("txhNroReq"));
    	System.out.println("nroReq: "+command.getNroRequerimiento());
    	System.out.println("codAlum: "+command.getCodAlumno());
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null && command.getCodRequerimiento()!= null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setDscUsuario(usuarioSeguridad.getNomUsuario());
    		System.out.println("DscUsuario: "+command.getDscUsuario());
    		List listaSede= new ArrayList();
    		listaSede=this.seguridadManager.getAllSedes();
    		if(listaSede!=null)
    		{  SedeBean sede=new SedeBean();
    			for(int a=0;a<listaSede.size();a++)
    			{ sede=(SedeBean)listaSede.get(a);
    			  if(command.getSede().equals(sede.getCodSede()))
    			   {  command.setNomSede(sede.getDscSede());
    			      a=listaSede.size();
    			   }
    			}
    			
    		}	
    	}
    	System.out.println("NomSede: "+command.getNomSede());
    	command.setCodTipoReporte(CommonConstants.REPORTE_LOG_13);
    	
    	command.setHaber("");
    	command.setFlag("1");
    	command.setCentroCostos(this.seguridadManager.GetAllCentrosCosto(command.getSede(),"",""));
    	System.out.println("total: "+command.getCentroCostos().size());
    	
    	command.setListResponsable1(this.solRequerimientoManager.GetAllAsigResp(CommonConstants.TIPO_ORDEN_COD, command.getSede()
    			, "", "", "", ""));
    	command.setListResponsable2(this.solRequerimientoManager.GetAllAsigResp(CommonConstants.TIPO_ORDEN_DSC, command.getSede()
    			, "", "", "", ""));
    	System.out.println("list1: "+command.getListResponsable1().size());
    	System.out.println("list2: "+command.getListResponsable2().size());
    	TipoLogistica obj = new TipoLogistica();
    	//llenando la lista de tipo de bien
    	obj.setCodTabla(CommonConstants.TIPT_NIVEL_COMPLEJIDAD);
    	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
    	command.setListComplejidad(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listComplejidad", command.getListComplejidad());
    	request.getSession().setAttribute("listResponsable1", command.getListResponsable1());
    	request.getSession().setAttribute("listResponsable2", command.getListResponsable2());
 //       	}
//	}
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
		String fla = "";
		AprobacionResponsableCommand control = (AprobacionResponsableCommand) command;
		control.setHaber("");
		if (control.getOperacion().trim().equals("BUSCAR"))
    	{
			control.setCentroCostos(this.seguridadManager.GetAllCentrosCosto(control.getSede(),"",""));
	    	System.out.println("total: "+control.getCentroCostos().size());
		if(control.getFlag().equals("")){
			busca1(control);
			busca2(control);
		}
		else{
			busca1(control);
			busca2(control);
		}
    	}
		else if (control.getOperacion().trim().equals("GRABAR"))
    	{   
			resultado = graba(control);
			if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
			else control.setMsg("OK");
			busca1(control);
			busca2(control);
			control.setCentroCostos(this.seguridadManager.GetAllCentrosCosto(control.getSede(),"",""));
	    	System.out.println("total: "+control.getCentroCostos().size());
    	}
		else if (control.getOperacion().trim().equals("LLENAR"))
    	{   
		llenar(control);
		busca1(control);
		busca2(control);
		control.setCentroCostos(this.seguridadManager.GetAllCentrosCosto(control.getSede(),"",""));
    	System.out.println("total: "+control.getCentroCostos().size());
    	}
		if(control.getFlag().equals("")){
			control.setFlag("1");
			
		}
		else if(control.getFlag().equals("1")){
			control.setFlag("");
			
		}
		request.getSession().setAttribute("listComplejidad", control.getListComplejidad());
		request.getSession().setAttribute("listDetalle", control.getListDetalle());
		request.getSession().setAttribute("listResponsable1", control.getListResponsable1());
    	request.getSession().setAttribute("listResponsable2", control.getListResponsable2());
		System.out.println("flag:   "+control.getFlag());
		request.setAttribute("flag",control.getFlag());
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/SolAproRequerimientos/log_Aprobacion_Responsable","control",control);		
    }
        private void llenar(AprobacionResponsableCommand control){
        	control.setListDetalle(this.solRequerimientoManager.GetAllDetalle(control.getCodigo()));
        	control.setHaber("OK");
        }
        
        private void busca1(AprobacionResponsableCommand control){
        	String fla = "";
			fla = "0";
			//	control.setListResponsable1(this.solRequerimientoManager.GetAllAsigResp(fla, control.getSede()
			//			, control.getCodCentroCostos(), control.getNombre(), control.getApPaterno(), control.getApMaterno()));
		    	
				control.setListResponsable1(this.solRequerimientoManager.GetAllAsigResp(fla, control.getSede()
						, "", "", "", ""));
        }
        
        private void busca2(AprobacionResponsableCommand control){
        	String fla = "";
        	fla = "1";
			control.setListResponsable2(this.solRequerimientoManager.GetAllAsigResp(fla, control.getSede()
					, control.getCodCentroCostos(), control.getNombre(), control.getApPaterno(), control.getApMaterno()));
	    
        }
        
		public void setSolRequerimientoManager(
				SolRequerimientoManager solRequerimientoManager) {
			this.solRequerimientoManager = solRequerimientoManager;
		}
      
        private String graba(AprobacionResponsableCommand control){
        System.out.println("1.- "+control.getCodRequerimiento());
        System.out.println("2.- "+control.getCodigo1());
        System.out.println("3.- "+control.getCodAlumno());
        System.out.println("4.- "+control.getCodComplejidad());
        	control.setGraba(this.solRequerimientoManager.InsertResponsable(control.getCodRequerimiento()
        			, control.getCodigo1(), control.getCodAlumno(),control.getCodComplejidad()));
        	
        	return control.getGraba();	
        }
    
        
}
