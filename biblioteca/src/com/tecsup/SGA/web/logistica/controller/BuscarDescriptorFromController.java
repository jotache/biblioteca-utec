package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.BuscarDescriptorCommand;

public class BuscarDescriptorFromController extends SimpleFormController{
	private static Log log = LogFactory.getLog(BuscarDescriptorFromController.class);
 	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public BuscarDescriptorFromController() {    	
        super();        
        setCommandClass(BuscarDescriptorCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	BuscarDescriptorCommand command = new BuscarDescriptorCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	System.out.println("codAlu: "+command.getCodAlumno());
    	System.out.println("codPro: "+command.getCodProducto());
    	TipoLogistica obj = new TipoLogistica();
		
    	obj.setCodTabla(CommonConstants.TIPT_DESCRIPTOR_LOG);
    	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
    	obj.setDescripcion(command.getCodDes());
    	System.out.println("codTabla: "+CommonConstants.TIPT_DESCRIPTOR_LOG);
    	System.out.println("tipo    : "+CommonConstants.DELETE_MTO_BIB);
    	System.out.println("descrip : "+command.getCodDes());
    	command.setListDes(this.detalleManager.GetAllTablaDetalle(obj));
    	System.out.println("encontrados: "+command.getListDes().size());
    	request.getSession().setAttribute("listDes", command.getListDes());
        log.info("formBackingObject:FIN");
        return command;
    }  
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	BuscarDescriptorCommand control = (BuscarDescriptorCommand) command;
    	if (control.getOperacion().trim().equals("DETALLE"))
    	{	
    		TipoLogistica obj = new TipoLogistica();
    		
        	obj.setCodTabla(CommonConstants.TIPT_DESCRIPTOR_LOG);
        	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
        	obj.setDescripcion(control.getCodDes());
        	System.out.println("codTabla: "+CommonConstants.TIPT_DESCRIPTOR_LOG);
        	System.out.println("tipo    : "+CommonConstants.DELETE_MTO_BIB);
        	System.out.println("descrip : "+control.getCodDes());
        	control.setListDes(this.detalleManager.GetAllTablaDetalle(obj));
        	System.out.println("encontrados: "+control.getListDes().size());
        	request.getSession().setAttribute("listDes", control.getListDes());
     	}
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{	
    		resultado = grabar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("DOBLE");
    		}
    		else control.setMsg("OK");
    		System.out.println("msg: "+control.getMsg());
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Buscar_Descriptor","control",control);		
    }
        
        private String grabar(BuscarDescriptorCommand control){
        	System.out.println("codPro: "+control.getCodProducto());
        	System.out.println("codDes: "+control.getCodDescriptor());
        	System.out.println("codAlu: "+control.getCodAlumno());
        	 String codEvaluacion;
        	StringTokenizer stkEvaluaciones = new StringTokenizer(control.getCodDescriptor(),"|");
			while ( stkEvaluaciones.hasMoreTokens() )
    		{
				codEvaluacion = stkEvaluaciones.nextToken();
        	control.setGrabar(this.detalleManager.InsDescriptoresByBien(control.getCodProducto()
        			, codEvaluacion, control.getCodAlumno()));
    		}
        	return  control.getGrabar();
        }
}
