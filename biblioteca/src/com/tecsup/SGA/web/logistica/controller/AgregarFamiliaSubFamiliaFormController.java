package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarFamiliaSubFamiliaCommand;

public class AgregarFamiliaSubFamiliaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarFamiliaSubFamiliaFormController.class);
  DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public AgregarFamiliaSubFamiliaFormController() {    	
        super();        
        setCommandClass(AgregarFamiliaSubFamiliaCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarFamiliaSubFamiliaCommand command = new AgregarFamiliaSubFamiliaCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setCodProveedor((String)request.getParameter("txhCodUnico"));
    	System.out.println("codAlu: "+command.getCodAlumno());
    	System.out.println("codPro: "+command.getCodProveedor());
    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
          		cargaCombos(command);	
            	command.setListFamSubFam(this.detalleManager.GetAllSubFamilias("", "", CommonConstants.UPDATE_MTO_BIB,"","",""));
            	request.getSession().setAttribute("ListFamSubFam", command.getListFamSubFam());
            	request.getSession().setAttribute("listBien", command.getListBien());
          		}
          	}
    	
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
   String rsultado = "";
    	AgregarFamiliaSubFamiliaCommand control = (AgregarFamiliaSubFamiliaCommand) command;
    	if("BUSCAR".equals(control.getOperacion())){
    		cargaCombos(control);	
    		control.setListFamSubFam(this.detalleManager.GetAllSubFamilias(control.getCodBien(), control.getCodFamilia(), CommonConstants.UPDATE_MTO_BIB,control.getCodSubFamilia(),
    				control.getFamilia(),control.getSubFamilia()));
        	request.getSession().setAttribute("ListFamSubFam", control.getListFamSubFam());
        	request.getSession().setAttribute("listBien", control.getListBien());
        	
    	}
      	if("GRABAR".equals(control.getOperacion())){
      		rsultado = grabar(control);
      		if ( rsultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
			else if(rsultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
      	}
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Familia_SubFamilia","control",control);		
    }
        private void cargaCombos(AgregarFamiliaSubFamiliaCommand control){
        	TipoLogistica obj = new TipoLogistica();
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	control.setListBien(this.detalleManager.GetAllTablaDetalle(obj));
        
        }
        private String grabar(AgregarFamiliaSubFamiliaCommand control){
        	System.out.println("codPro: "+control.getCodProveedor());
        	System.out.println("codDet: "+control.getCodFamiliaSubFamilia());
        	System.out.println("codAlu: "+control.getCodAlumno());
        	 String codEvaluacion;
        	StringTokenizer stkEvaluaciones = new StringTokenizer(control.getCodFamiliaSubFamilia(),"|");
			while ( stkEvaluaciones.hasMoreTokens() )
    		{
			codEvaluacion = stkEvaluaciones.nextToken();
    		
        	control.setGrabo(this.detalleManager.InsertSubFamiliaByPro(control.getCodProveedor()
        			, codEvaluacion, control.getCodAlumno()));
    		}
        	return control.getGrabo();
        }
      
}
