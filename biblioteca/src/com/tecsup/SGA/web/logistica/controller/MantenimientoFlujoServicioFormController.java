package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoFlujoServicioCommand;

public class MantenimientoFlujoServicioFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoFlujoServicioFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public MantenimientoFlujoServicioFormController() {    	
        super();        
        setCommandClass(MantenimientoFlujoServicioCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoFlujoServicioCommand command = new MantenimientoFlujoServicioCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
        llenaData(command);
    	request.getSession().setAttribute("listTipoReqBien", command.getListTipoReqBien());
    	request.getSession().setAttribute("listFamilia", command.getListFamilia());
    	request.getSession().setAttribute("listTipReq", command.getListTipReq());
    	request.getSession().setAttribute("listSede", command.getListSede());
      	
          	}
      	}
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	MantenimientoFlujoServicioCommand control = (MantenimientoFlujoServicioCommand) command;
    	if("LLENA".equals(control.getOperacion())){
    		llenaData(control);
    		llena(control);
    		listar(control);
    		busca1(control);
    	   	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
    	   	request.getSession().setAttribute("listTipoReqBien", control.getListTipoReqBien());
    	           
    	}
    	else if("BUSCAR".equals(control.getOperacion())){
    		System.out.println(""+control.getCodTipReq());
    		if("0001".equals(control.getCodTipReq())){
    			llenaData(control);
    			busca1(control);
    			 llena(control);
    			 listar(control);
    		   	request.getSession().setAttribute("listTipoReqBien", control.getListTipoReqBien());
    	    	
    		}
    		else if("0002".equals(control.getCodTipReq())){
    			llenaData(control);
    			busca2(control);
    			 llena(control);
    			 listar(control);
    		   	request.getSession().setAttribute("listTipoReqServicio", control.getListTipoReqServicio());
    	    	
    		}
    		else{
    			llenaData(control);	
    			request.getSession().setAttribute("listTipoReqBien",control.getListTipoReqBien());
    			request.getSession().setAttribute("listSede", control.getListSede());
    		}
    		
    		
    	}
    	else if("ELIMINAR".equals(control.getOperacion())){
    		resultado = eliminar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    			}
    		else if ( resultado.equals("-1")) {
    			control.setMsg("DOBLE");
    		}
    		else {
    			control.setMsg("OK");
    			}
    		llena(control);
    		llenaData(control);
			busca1(control);
			busca2(control);
			listar(control);
	  	   	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
		   	request.getSession().setAttribute("listTipoReqServicio", control.getListTipoReqServicio());
		   	request.getSession().setAttribute("listTipoReqBien", control.getListTipoReqBien());
			request.getSession().setAttribute("listSede", control.getListSede());
          	
    	}
    	else if("".equals(control.getOperacion())){
    		 llena(control);
    		llenaData(control);
    	
    	 busca1(control);
    	 busca2(control);
    	 listar(control);
  	   	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
		   	request.getSession().setAttribute("listTipoReqServicio", control.getListTipoReqServicio());
		   	request.getSession().setAttribute("listTipoReqBien", control.getListTipoReqBien());
		   	request.getSession().setAttribute("listSede", control.getListSede());
    	}
    	else if("BUSCABIEN".equals(control.getOperacion())){
    		LlernaFamilia(control);
    		 llena(control);
     		llenaData(control);
     	
     	 busca1(control);
     	 busca2(control);
     	listar(control);
   	   	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
 		   	request.getSession().setAttribute("listTipoReqServicio", control.getListTipoReqServicio());
 		   	request.getSession().setAttribute("listTipoReqBien", control.getListTipoReqBien());
 		 	request.getSession().setAttribute("listFamilia", control.getListFamilia());
 		 	request.getSession().setAttribute("listSede", control.getListSede());
    	}
    	request.getSession().setAttribute("listSede", control.getListSede());
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Flujo_Atencion","control",control);		
    }
        private void llena(MantenimientoFlujoServicioCommand control){
        	if(control.getCodFamilia().equals("")){
        		TipoLogistica obj = new TipoLogistica();
            	obj.setCodId(CommonConstants.TIPT_VALOR);
            	obj.setCodTabla(CommonConstants.TIPT_SUB_FAMILIA);
            	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
              	control.setListSubFamilia(this.detalleManager.GetAllTablaDetalle(obj));
            }
        	else{
        	   	TipoLogistica obj = new TipoLogistica();
            	obj.setCodId(control.getCodFamilia());
            	obj.setCodTabla(CommonConstants.TIPT_SUB_FAMILIA);
            	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
            	control.setListSubFamilia(this.detalleManager.GetAllTablaDetalle(obj));
            	
        		}
        	}
        private void busca1(MantenimientoFlujoServicioCommand control){
        	control.setListTipoReqBien(this.detalleManager.GetAllAtencionSolicitudes(control.getCodSede(),control.getCodTipReq()
        			, control.getCodTipBien(), control.getCodRes(), control.getNomRes(), control.getCodFamilia(), control.getCodSubFamilia()));
       System.out.println("total: "+control.getListTipoReqBien().size());
        }
        private void busca2(MantenimientoFlujoServicioCommand control){
        	control.setListTipoReqServicio(this.detalleManager.GetAllAtencionSolicitudes(control.getCodSede(),control.getCodTipReq()
        			, control.getCodTipoServicio(), control.getCodRes(), control.getNomRes(), "", ""));
        	System.out.println("total: "+control.getListTipoReqServicio().size());
        }
        private void llenaData(MantenimientoFlujoServicioCommand command){
        	TipoLogistica obj = new TipoLogistica();
        	System.out.println("llenando la grillla.........");
        	//llenando la lista por el tip de requerimiento
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_REQUERIMIENTO);
        	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
        	command.setListTipReq(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_SEDE);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	command.setListSede(this.detalleManager.GetAllTablaDetalle(obj));
       
        	
        	// llenando la lista por el tipo de familia
        	
        	obj.setCodPadre(CommonConstants.TIPT_TIPO_BIEN);
    		obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
    		obj.setCodId(command.getCodTipBien());
        	obj.setTipo(CommonConstants.FAMILIA_MTO_LOG);
        	command.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	//llenando la lista de acuerdo al tipo de bien
        	  	
        }
        
       private String eliminar(MantenimientoFlujoServicioCommand control){
        System.out.println("entrando al llamado de stor de eliminar");
       
       String codEvaluacion1;   
       String codEvaluacion;
   	
     StringTokenizer stkEvaluaciones = new StringTokenizer(control.getCodigoTipo(),"|");
       StringTokenizer stkEvaluaciones1 = new StringTokenizer(control.getCodigo(),"|");
   	
  	while ( stkEvaluaciones.hasMoreTokens() || stkEvaluaciones1.hasMoreTokens())
		{
   		codEvaluacion = stkEvaluaciones.nextToken();
   		codEvaluacion1 = stkEvaluaciones1.nextToken();
     	control.setGraba(this.detalleManager.DeleteAtencionSolicitudes(control.getCodTipReq()
       			, codEvaluacion, codEvaluacion1, control.getCodAlumno(),control.getCodSede()));
		}
   	
     return control.getGraba();
       }
       private void LlernaFamilia(MantenimientoFlujoServicioCommand control){
    	   if(control.getCodTipBien().equals("")){
           	TipoLogistica obj = new TipoLogistica();
           	obj.setCodId(CommonConstants.TIPT_VALOR);
           	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
           	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
           	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
           	}
           	else{
           		TipoLogistica obj = new TipoLogistica();
               	obj.setCodId(control.getCodTipBien());
               	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
               	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
               	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
               
           	}
       }
       private void listar(MantenimientoFlujoServicioCommand control){
    	   TipoLogistica obj = new TipoLogistica();
    	   
    	   obj.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
       	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
       	control.setListTipoBien(this.detalleManager.GetAllTablaDetalle(obj));
       	
       
       	obj.setCodTabla(CommonConstants.TIPT_GRUPO_SERVICIO);
       	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
       	
       	control.setListTipoServicio(this.detalleManager.GetAllTablaDetalle(obj));
     
       }
}
