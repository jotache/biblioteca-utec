package com.tecsup.SGA.web.logistica.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;

public class ActualizarInventarioFisicoController implements Controller{
	private static Log log = LogFactory.getLog(ActualizarInventarioFisicoController.class);
	private AlmacenManager almacenManager;

	public void setAlmacenManager(AlmacenManager almacenManager) {
		this.almacenManager = almacenManager;
	}

	public ActualizarInventarioFisicoController(){
		
	}
    
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception {
    	log.info("onSubmit:INI");
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if(usuarioSeguridad==null){
    		InicioCommand command = new InicioCommand();
    		return new ModelAndView("/cabeceraLogistica","control",command);
    	}	
    	
    	String codSede = request.getParameter("txhCodSede");
    	String codOpcion = request.getParameter("txhOpcion");
    	
    	System.out.println("codSede>>"+codSede);
    	System.out.println("codOpcion>>"+codOpcion);
    	
    	String fechaRep = Fecha.getFechaActual();
		String horaRep = Fecha.getHoraActual();
		String fechaActRep = Fecha.getFechaActualReporte();
    	
    	ModelMap model = new ModelMap();
    	
    	model.addObject("fechaRep", fechaRep);
		model.addObject("horaRep", horaRep);
		model.addObject("codSede", codSede);	
		model.addObject("fechaActRep", fechaActRep);
		
		List consulta=null;
		String pagina="";
		
		if(codOpcion.equals("0")){
			consulta = this.almacenManager.getAllImpresionTickets(codSede);			
	    	if(consulta != null){
    			request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/Almacen/ImpresionTickets";	    		
			}
		}
		else
		if(codOpcion.equals("1")){			
			consulta = this.almacenManager.getAllInventario(codSede, "");			
			if(consulta!=null){
				if(consulta.size()==1){
					Producto producto = new Producto();
					producto=(Producto)consulta.get(0);
					if(producto.getCodigo()!=null){
						if(producto.getCodigo().equals("-1")){
							pagina = "/logistica/Almacen/bib_close";							
						}
					}
					else{						
						request.getSession().setAttribute("consulta", consulta);
						request.getSession().setAttribute("codSede", codSede);
						pagina = "/logistica/Almacen/DescargarInventario";
					}
				}
				else{					
					request.getSession().setAttribute("consulta", consulta);
					request.getSession().setAttribute("codSede", codSede);
					pagina = "/logistica/Almacen/DescargarInventario";
				}
			}
		}

    	log.info("onSubmit:FIN");
	    return new ModelAndView(pagina,"model",model);
    }
}