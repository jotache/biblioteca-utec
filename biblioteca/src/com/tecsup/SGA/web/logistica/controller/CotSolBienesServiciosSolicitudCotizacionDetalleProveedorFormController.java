package com.tecsup.SGA.web.logistica.controller;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.logistica.command.CotSolDetalleProveedorCommand;


public class CotSolBienesServiciosSolicitudCotizacionDetalleProveedorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotSolBienesServiciosSolicitudCotizacionDetalleProveedorFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public CotSolBienesServiciosSolicitudCotizacionDetalleProveedorFormController() {    	
        super();        
        setCommandClass(CotSolDetalleProveedorCommand.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	CotSolDetalleProveedorCommand command = new CotSolDetalleProveedorCommand();
    	
    	if(request.getParameter("prmCodProveedor")!=null){
    		command.setTxhCodProveedor(request.getParameter("prmCodProveedor"));
    		command.setUsuario(request.getParameter("prmUsuario"));
    		command.setTxhCodCotizacion(request.getParameter("prmCodCotizacion"));
    		command.setTxtRuc(request.getParameter("prmCodRuc"));
    		command.setTxtDesProveedor(request.getParameter("prmCodDescripcion"));
    		
    		List lstResultado = cotBienesYServiciosManager.
    		GetAllCondicionesByProveedor(command.getTxhCodCotizacion(),command.getTxhCodProveedor());
    		log.info("TAMM>"+lstResultado.size());
    		command.setLstResultado(lstResultado);
    	}
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
    

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	CotSolDetalleProveedorCommand control = (CotSolDetalleProveedorCommand) command;
		
		if("irConsultar".equalsIgnoreCase(control.getOperacion()))
		{

			
			
		}		
		control.setOperacion("");
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/CotBienesYServicios/Sol_Bandeja_Solicitud_Cotizacion_Detalle_Proveedor","control",control);		
    }
        
    


}
