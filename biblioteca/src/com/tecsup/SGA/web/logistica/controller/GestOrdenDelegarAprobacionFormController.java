package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.Email.CommonConstantsEmail;
import com.tecsup.SGA.Email.ServletEnvioCorreo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.OrdenesAprobadas;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.GestionOrdenesManager;
import com.tecsup.SGA.web.logistica.command.AprobacionResponsableCommand;
import com.tecsup.SGA.web.logistica.command.GestOrdenDetalleOrdenCommand;

public class GestOrdenDelegarAprobacionFormController
		extends SimpleFormController {
	private static Log log = LogFactory.getLog(GestOrdenDelegarAprobacionFormController.class);
	GestionOrdenesManager gestionOrdenesManager;
	public GestionOrdenesManager getGestionOrdenesManager() {
		return gestionOrdenesManager;
	}
	public void setGestionOrdenesManager(
			GestionOrdenesManager gestionOrdenesManager) {
		this.gestionOrdenesManager = gestionOrdenesManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		log.info("formBackingObject:INI");
		AprobacionResponsableCommand command = new AprobacionResponsableCommand();
    	
    	if( request.getParameter("prmCodUsuario") != null ){
    		String codUsuario = request.getParameter("prmCodUsuario");
    		String codSede = request.getParameter("prmCodSede");
    		String tipoAprobacion = request.getParameter("prmTipoAprobacion");//0: Req. 1:O/C
    		log.info("codUsuario(): "+codUsuario);
    		command.setCodUsuario(codUsuario);
    		command.setSede(codSede);
    		command.setCodTipoReporte(tipoAprobacion);
    		List lstResultado1 = this.gestionOrdenesManager.GetAllAprobacionOcSuplantadoPor(command.getCodUsuario()
    				, command.getCodTipoReporte());
            command.setListResponsable1(lstResultado1);
            List lstResultado2 = this.gestionOrdenesManager.GetAllAprobacionOcSuplantandoA(command.getCodUsuario()
            		, command.getCodTipoReporte());
            command.setListResponsable2(lstResultado2);
    	}
    	log.info("formBackingObject:FIN");
        return command;
	}
	
	protected void initBinder(HttpServletRequest request,ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class,
			new CustomNumberEditor(Long.class, nf, true));
    }

    public ModelAndView processFormSubmission(HttpServletRequest request,
    		HttpServletResponse response,
    		Object command,
    		BindException errors)
    throws Exception {
    	return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AprobacionResponsableCommand control= (AprobacionResponsableCommand) command;
    	
    	if(control.getOperacion().trim().equals("GRABAR"))
    	{
    		String resultado="";
    		String codPrincipal = control.getCodUsuario();
    		String codSede = control.getSede();
    		String cadAprobacion = control.getMsg();
    		System.out.println("codPrincipal: " + codPrincipal);
    		resultado=this.gestionOrdenesManager.UpdateSuplenteAprobacionOc(codPrincipal
    			, codSede, cadAprobacion);	
    		if(resultado.trim().equals("-1")) 
    			control.setMsg("GRABAR_ERROR");
    		else{ if(resultado.trim().equals("0")) {
   	       			control.setMsg("GRABAR_OK");
					List lstResultado1 = this.gestionOrdenesManager.GetAllAprobacionOcSuplantadoPor(control.getCodUsuario()
							, control.getCodTipoReporte());
					control.setListResponsable1(lstResultado1);
		            List lstResultado2 = this.gestionOrdenesManager.GetAllAprobacionOcSuplantandoA(control.getCodUsuario()
		            		, control.getCodTipoReporte());
		            control.setListResponsable2(lstResultado2);
    			}
   	       	}
   	    }
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/GestOrdenCompraServicio/GestAprobadorOrdenSuplente","control",control);
    }

}
