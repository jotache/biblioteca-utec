package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.GestDocPagoDocumentosPagoCommand;
import com.tecsup.SGA.web.logistica.command.GestOrdenBandejaConsultaOrdenCommand;

public class GestDocPagoDocumentosPagoFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(GestDocPagoDocumentosPagoFormController.class);
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	GestionDocumentosManager gestionDocumentosManager;
	
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	GestDocPagoDocumentosPagoCommand command= new GestDocPagoDocumentosPagoCommand();
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    		command.setDscUsuario(usuarioSeguridad.getNomUsuario());
    		System.out.println("DscUsuario: "+command.getDscUsuario());
    	}
    	command.setCodSede(request.getParameter("txhCodSede"));
    	command.setNomSede(request.getParameter("txhNomSede"));
    	command.setConsteSoloDoc("1");
    	command.setConsteSoloTipoPago("1");
    	command.setConsteIngresoPendiente("1");
    	command.setCodTipoReporte(CommonConstants.REPORTE_LOG_17);
    	
    	Fecha fecha=new Fecha();
    	command.setFechaBD(fecha.getFechaActual());
    	System.out.println("FechaBD: "+command.getFechaBD());
    	
    	List lista1= new ArrayList();    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    	command.setListaCodTipoOrden(lista1);
    	
   	
     BandejaLogisticaBean bandejaBean1 = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
      if(bandejaBean1!= null)
       {  log.info("BandejaLogisticaBean:INICIO");
       	  if(bandejaBean1.getCodBandeja().equals("6")){
	     	  command.setNroOrden(bandejaBean1.getValor1());
	     	  command.setFecInicio(bandejaBean1.getValor2());
	     	  command.setFecFinal(bandejaBean1.getValor3());
	     	  command.setNroRucProveedor(bandejaBean1.getValor4());
	     	  command.setNombreProveedor(bandejaBean1.getValor5());
	     	  command.setNroRucComprador(bandejaBean1.getValor6());
	     	  command.setNombreComprador(bandejaBean1.getValor7());
	     	  command.setCodTipoOrden(bandejaBean1.getValor8());
	     	  command.setValSelec1(bandejaBean1.getValor9());
	     	  command.setValSelec2(bandejaBean1.getValor10());
	     	  command.setNumFactura(bandejaBean1.getValor11());
	     	  //ALQD,21/01/09. AGREGANDO NUEVA PROPIEDAD PARA FILTRAR LOS PENDIENTES POR CONFIRMAR
		      command.setValSelec3(bandejaBean1.getValor12());
	     	  BuscarBandeja(command, request);
       	  }
     	  log.info("BandejaLogisticaBean:FIN");
       }
       else command.setBanListaBandeja("0");
    	
    log.info("formBackingObject:FIN");
	 
 	return command;
  }
	
	protected void initBinder(HttpServletRequest request,
          ServletRequestDataBinder binder) {
  	NumberFormat nf = NumberFormat.getNumberInstance();
  	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
  	
  }
  /**
   * Redirect to the successView when the cancel button has been pressed.
   */
  public ModelAndView processFormSubmission(HttpServletRequest request,
                                            HttpServletResponse response,
                                            Object command,
                                            BindException errors)
  throws Exception {
      //this.onSubmit(request, response, command, errors);
  	return super.processFormSubmission(request, response, command, errors);
  }
	
  public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
          BindException errors)
  		throws Exception {
  	log.info("onSubmit:INI");
  	
  	GestDocPagoDocumentosPagoCommand control= (GestDocPagoDocumentosPagoCommand) command;
  	control.setBanListaBandeja("");
  	
  	if(control.getOperacion().equals("BUSCAR"))
  	{
  		BuscarBandeja(control, request);
  	}	
  	
 	log.info("onSubmit:FIN");		
    return new ModelAndView("/logistica/GestionDocumentos/Bandeja_Consultar_Ordenes_Aprobadas","control",control);
}

  public void BuscarBandeja(GestDocPagoDocumentosPagoCommand control, HttpServletRequest request)
  { List lista= new ArrayList();
  
    GuardarDatosBandeja(control, request);
    
    if(control.getCodTipoOrden().equals("-1"))
    	control.setCodTipoOrden("");
    if(control.getValSelec2().equals("0"))
    	control.setValSelec2("");
    if(control.getValSelec1().equals("0"))
    	control.setValSelec1("");
	//ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA
    //ALQD,21/01/09. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA
    if(control.getValSelec3().equals("0"))
    	control.setValSelec3("");
    System.out.println(">>CodSede<<"+control.getCodSede()+">>CodUsuario<<"+control.getCodUsuario()+">>CodPerfil<<"+
			control.getCodPerfil()+">>NroOrden<<"+control.getNroOrden()+">>FecInicio<<"+control.getFecInicio()+">>FecFinal<<"+control.getFecFinal() 
			+">>NroRucProveedor<<"+control.getNroRucProveedor()+">>NombreProveedor<<"+control.getNombreProveedor()+">>NroRucComprador<<"+
			control.getNroRucComprador()+">>NombreComprador<<"+ 
			control.getNombreComprador()+">>CodTipoOrden<<"+control.getCodTipoOrden()+">>ValSelec1<<"+control.getValSelec1()
			+">>ValSelec2<<"+control.getValSelec2()+">>numFactura<<"+control.getNumFactura()
			+">>ValSelec3<<"+control.getValSelec3());
	lista=this.gestionDocumentosManager.GetAllOrdenesAprobadas(control.getCodSede(), control.getCodUsuario(), 
			control.getCodPerfil(), control.getNroOrden(), control.getFecInicio(), control.getFecFinal(), 
			control.getNroRucProveedor(), control.getNombreProveedor(), control.getNroRucComprador(), 
			control.getNombreComprador(), control.getCodTipoOrden(), control.getValSelec1(),
			control.getValSelec2(), control.getNumFactura(),control.getValSelec3());
	if(lista!=null)
	  {  control.setListaBandeja(lista);
	     if(lista.size()==0)
	    	 control.setBanListaBandeja("0");
	  }
	else{ control.setBanListaBandeja("0");
		  control.setListaBandeja(new ArrayList());
		}
	System.out.println("Size xD: "+lista.size());
  }
  
  public void GuardarDatosBandeja(GestDocPagoDocumentosPagoCommand control,HttpServletRequest request)
  {
 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
 	 
 	 bandejaBean.setCodBandeja("6");
 	 bandejaBean.setValor1(control.getNroOrden());
 	 bandejaBean.setValor2(control.getFecInicio());
 	 bandejaBean.setValor3(control.getFecFinal());
 	 bandejaBean.setValor4(control.getNroRucProveedor());
 	 bandejaBean.setValor5(control.getNombreProveedor());
 	 bandejaBean.setValor6(control.getNroRucComprador());
 	 bandejaBean.setValor7(control.getNombreComprador());
 	 bandejaBean.setValor8(control.getCodTipoOrden());
 	 bandejaBean.setValor9(control.getValSelec1());
 	 bandejaBean.setValor10(control.getValSelec2());
 	 bandejaBean.setValor11(control.getNumFactura());
 	 bandejaBean.setValor12(control.getValSelec3());
 	 log.info("GuardarDatosBandeja");
 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
  } 
  
  
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}
	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
	public DetalleManager getDetalleManager() {
		return detalleManager;
	}
	
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
	
	public GestionDocumentosManager getGestionDocumentosManager() {
		return gestionDocumentosManager;
	}
	
	public void setGestionDocumentosManager(
			GestionDocumentosManager gestionDocumentosManager) {
		this.gestionDocumentosManager = gestionDocumentosManager;
	}
}
