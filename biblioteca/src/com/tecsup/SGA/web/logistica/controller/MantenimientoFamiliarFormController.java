package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoFamiliarCommand;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.common.CommonConstants;

public class MantenimientoFamiliarFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoFamiliarFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public MantenimientoFamiliarFormController() {    	
        super();        
        setCommandClass(MantenimientoFamiliarCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoFamiliarCommand command = new MantenimientoFamiliarCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	TipoLogistica obj = new TipoLogistica();
    	//llenando la lista de tipo de bien
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	command.setTipoBien(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("tipoBien", command.getTipoBien());
    	//llenando la lista de acuedo a al tipo de bien y la familia
    	obj.setCodPadre(CommonConstants.TIPT_TIPO_BIEN);
    	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
    	obj.setTipo(CommonConstants.FAMILIA_MTO_LOG);
    	command.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listFamilia", command.getListFamilia());
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	MantenimientoFamiliarCommand control = (MantenimientoFamiliarCommand) command;
    	TipoLogistica obj = new TipoLogistica();
    	if (control.getOperacion().trim().equals("DETALLE"))
    	{
    	
    		obj.setCodPadre(CommonConstants.TIPT_TIPO_BIEN);
    		obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
    		obj.setCodId(control.getCodTipo());
        	obj.setTipo(CommonConstants.FAMILIA_MTO_LOG);
        	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listFamilia", control.getListFamilia());
        }
    
    	
    	else if(control.getOperacion().trim().equals("ELIMINAR")){
    		resultado = delete(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    			}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("DOBLE");
    		}
    		else {
    			control.setMsg("OK");
    			}
    		obj.setCodPadre(CommonConstants.TIPT_TIPO_BIEN);
    		obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
    		obj.setCodId(control.getCodTipo());
        	obj.setTipo(CommonConstants.FAMILIA_MTO_LOG);
        	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listFamilia", control.getListFamilia());
       	}
   		obj.setCodPadre(CommonConstants.TIPT_TIPO_BIEN);
		obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
		obj.setCodId(control.getCodTipo());
    	obj.setTipo(CommonConstants.FAMILIA_MTO_LOG);
    	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listFamilia", control.getListFamilia());

		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Familia","control",control);		
    }
       
        private String delete(MantenimientoFamiliarCommand control){
        	
        	control.setElimino(this.detalleManager.DeleteTablaDetalle(CommonConstants.TIPT_FAMILIA, control.getSecuencial()
        			, control.getCodAlumno()));
        	
        	return control.getElimino();
        }
        
}
