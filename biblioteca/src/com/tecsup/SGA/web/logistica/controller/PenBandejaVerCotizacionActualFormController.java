package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.PenBandejaVerCotizacionActualCommand;

public class PenBandejaVerCotizacionActualFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PenBandejaVerCotizacionActualFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	PenBandejaVerCotizacionActualCommand command= new PenBandejaVerCotizacionActualCommand();
    	
    	command.setCodSede((String)request.getParameter("txhCodSede"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	command.setCodTipoReq((String)request.getParameter("txhCodTipoReq"));
    	command.setTxtDescripcion((String)request.getParameter("txhDescripcion")); 
    	
    	System.out.println("CodSede: "+command.getCodSede());
    	System.out.println("CodUsuario: "+command.getCodUsuario());
    	System.out.println("CodTipoReq: "+command.getCodTipoReq());
    	System.out.println("TxtDescripcion: "+command.getTxtDescripcion());
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("txhCodUsuario");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    	}
    	List lista1= new ArrayList();
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_GASTOS 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    	command.setListaCodTipoBien(lista1);
    	
   	 log.info("formBackingObject:FIN");
     return command;
 }
	
	protected void initBinder(HttpServletRequest request,
         ServletRequestDataBinder binder) {
 	NumberFormat nf = NumberFormat.getNumberInstance();
 	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
 	
 }
 /**
  * Redirect to the successView when the cancel button has been pressed.
  */
 public ModelAndView processFormSubmission(HttpServletRequest request,
                                           HttpServletResponse response,
                                           Object command,
                                           BindException errors)
 throws Exception {
     //this.onSubmit(request, response, command, errors);
 	return super.processFormSubmission(request, response, command, errors);
 }
	
 public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
         BindException errors)
 		throws Exception {
 	log.info("onSubmit:INI");
 	
 	PenBandejaVerCotizacionActualCommand control= new PenBandejaVerCotizacionActualCommand();
 	
 	log.info("onSubmit:FIN");		
    return new ModelAndView("/logistica/CotBienesYServicios/Pen_Bandeja_Ver_Cotizacion_Actual","control",control);
}

public SolRequerimientoManager getSolRequerimientoManager() {
	return solRequerimientoManager;
}

public void setSolRequerimientoManager(
		SolRequerimientoManager solRequerimientoManager) {
	this.solRequerimientoManager = solRequerimientoManager;
}

public TablaDetalleManager getTablaDetalleManager() {
	return tablaDetalleManager;
}

public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
	this.tablaDetalleManager = tablaDetalleManager;
}

public SeguridadManager getSeguridadManager() {
	return seguridadManager;
}

public void setSeguridadManager(SeguridadManager seguridadManager) {
	this.seguridadManager = seguridadManager;
}
}
