package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarSubFamiliaCommand;

public class AgregarSubFamiliaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarSubFamiliaFormController.class);
  DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public AgregarSubFamiliaFormController() {    	
        super();        
        setCommandClass(AgregarSubFamiliaCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarSubFamiliaCommand command = new AgregarSubFamiliaCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setCodBien((String)request.getParameter("txtCodBien"));
    	command.setCodigo((String)request.getParameter("txhCodUnico"));
    	command.setSubfamilia((String)request.getParameter("txhDesSubFam"));
    	command.setCodFam((String)request.getParameter("txhDesFam"));
    	command.setSecuencial((String)request.getParameter("txhCodGen"));
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	
    	command.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listFamilia", command.getListFamilia());
    
    	
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	AgregarSubFamiliaCommand control = (AgregarSubFamiliaCommand) command;
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
    			resultado = modificar(control);
    			if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
    			else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
            	else control.setMsg("OK");
    		}
    		else{
    		resultado = grabar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if(resultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
    	}
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_SubFamilia","control",control);		
    }
        private String grabar(AgregarSubFamiliaCommand control){
        	
        	control.setGrabar(this.detalleManager.InsertTablaDetalle(control.getCodFam()
        			, CommonConstants.TIPT_SUB_FAMILIA, control.getSubfamilia(), control.getCodBien(), "", "", control.getCodAlumno()));
        	
        	return control.getGrabar();
        }
        private String modificar(AgregarSubFamiliaCommand control){
        	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getCodFam()
        			, CommonConstants.TIPT_SUB_FAMILIA, control.getSecuencial(), control.getCodigo()
        			, control.getSubfamilia(), "", "", "", control.getCodAlumno()));
        	
        	return control.getGrabar();
        }
        
}
