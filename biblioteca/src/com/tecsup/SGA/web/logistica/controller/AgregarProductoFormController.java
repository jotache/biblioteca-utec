package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;  
import com.tecsup.SGA.modelo.CatalogoProducto;
import com.tecsup.SGA.modelo.TipoTablaDetalle; 
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarProductoCommand;

public class AgregarProductoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarProductoFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}	
    public AgregarProductoFormController() {    	
        super();        
        setCommandClass(AgregarProductoCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	String ru ="";
    	AgregarProductoCommand command = new AgregarProductoCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
    	command.setCodEstado((String)request.getParameter("txhEstado"));
    	System.out.println("estado LLega: "+command.getCodEstado());
    	if ( (String)request.getParameter("txhSede") != null )
    		command.setCodSedeAux((String)request.getParameter("txhSede"));
    	
    	command.setCodFamilia((String)request.getParameter("txhFamilia"));
    	command.setCodSubFamilia((String)request.getParameter("txhSubFamlia")); 
    	command.setNomPro((String)request.getParameter("txhNomPro"));
    	ru = (String)request.getParameter("txhRuta");
    	System.out.println("ruta: "+ru);
    	command.setRu(ru);
    	command.setRuta(ru);
    	System.out.println("archivo >>"+command.getRuta()+"<<");
    	request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02);
    	request.setAttribute("msgRutaServerDoc",CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_DOCUMENTO_02);

    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
          		cargarDatos(command);
          		command.setCondicion(CommonConstants.COMP_CAL_BUENO);
          		if(command.getTipo().equals("MODIFICAR"))
          		{
          			llenar(command);
          			subFamilia(command);
          			request.getSession().setAttribute("listdocRela", command.getListdocRela());
          			command.setRuta(CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02 + ru);
          			command.setBien(chekea(command));
          		}
          		if(command.getCondicion().equals(""))
          		{
          			command.setCondicion(CommonConstants.COMP_CAL_BUENO);
          		}
          		request.getSession().setAttribute("listFamilia", command.getListFamilia());
		    	request.getSession().setAttribute("listSubFamilia", command.getListSubFamilia());
		    	request.getSession().setAttribute("listunidad", command.getListunidad());
		    	request.getSession().setAttribute("listUbicacion", command.getListUbicacion());
		    	request.getSession().setAttribute("listDescriptores", command.getListDescriptores());
		    	request.getSession().setAttribute("listEstado", command.getListEstado());
		    	request.setAttribute("condicion",command.getCondicion());
          	}
    	}
        log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
        public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
       
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
		String res = "";
		String ru ="";
		String cost = "IMG_";
    	AgregarProductoCommand control = (AgregarProductoCommand) command;
    	System.out.println("ope: "+control.getOperacion());
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		resultado = grabar(control);
    		System.out.println("result: "+resultado);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("DOBLE");
    		}
    		else {
    			control.setMsg("OK");
    			control.setCodProducto(resultado);
    		}
    		control.setTipo("MODIFICAR");
    		llenar(control);
    		cargarDatos(control);
    		subFamilia(control);
    		request.getSession().setAttribute("listdocRela", control.getListdocRela());
      		request.getSession().setAttribute("listFamilia", control.getListFamilia());
	    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
	    	request.getSession().setAttribute("listunidad", control.getListunidad());
	    	request.getSession().setAttribute("listUbicacion", control.getListUbicacion());
	    	request.getSession().setAttribute("listDescriptores", control.getListDescriptores());
	    	request.setAttribute("condicion",control.getCondicion());
	    	control.setBien(chekea(control));
    	}
    	if (control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		res = eliminar(control);
    		if ( res.equals("-1")) {
    			control.setMens("ERROR");
    		}
    		else if ( res.equals("-2")) {
    			control.setMens("DOBLE");
    		}
    		else control.setMens("OK");
    		llenar(control);
    		cargarDatos(control);
    		subFamilia(control);
    		
    		request.getSession().setAttribute("listdocRela", control.getListdocRela());
      		request.getSession().setAttribute("listFamilia", control.getListFamilia());
	    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
	    	request.getSession().setAttribute("listunidad", control.getListunidad());
	    	request.getSession().setAttribute("listUbicacion", control.getListUbicacion());
	    	request.getSession().setAttribute("listDescriptores", control.getListDescriptores());
	    	request.setAttribute("condicion",control.getCondicion());
	    	control.setBien(chekea(control));
    	}
    	if (control.getOperacion().trim().equals(""))
    	{
	    	llenar(control);
	    	cargarDatos(control);
	    	subFamilia(control);
	    	
	    	request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02);
	    	request.setAttribute("msgRutaServerDoc",CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_DOCUMENTO_02);
	    	
			control.setRuta(CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02 + control.getRu());
	    	
			request.getSession().setAttribute("listdocRela", control.getListdocRela());
	  		request.getSession().setAttribute("listFamilia", control.getListFamilia());
	    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
	    	request.getSession().setAttribute("listunidad", control.getListunidad());
	    	request.getSession().setAttribute("listUbicacion", control.getListUbicacion());
	    	request.getSession().setAttribute("listDescriptores", control.getListDescriptores());
	    	request.setAttribute("condicion",control.getCondicion());
	    	control.setBien(chekea(control));
    	}
    	if (control.getOperacion().trim().equals("ELIMINARDESCRIPTOR"))
    	{
    		resultado = deleteDescriptor(control);
    		if ( res.equals("-1")) {
    			control.setMens("ERROR");
    		}
    		else control.setMens("OK");
    		System.out.println("men: "+control.getMens());
    	
    		llenar(control);
    		cargarDatos(control);
    		subFamilia(control);
    	
    		request.getSession().setAttribute("listdocRela", control.getListdocRela());
      		request.getSession().setAttribute("listFamilia", control.getListFamilia());
	    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
	    	request.getSession().setAttribute("listunidad", control.getListunidad());
	    	request.getSession().setAttribute("listUbicacion", control.getListUbicacion());
	    	request.getSession().setAttribute("listDescriptores", control.getListDescriptores());
	    	request.setAttribute("condicion",control.getCondicion());
	    	control.setBien(chekea(control));
    	}
    	if (control.getOperacion().trim().equals("MUESTRASUBFAMILIA"))
    	{
    		subFamilia(control);
    		cargarDatos(control);
    		
    		request.getSession().setAttribute("listdocRela", control.getListdocRela());
      		request.getSession().setAttribute("listFamilia", control.getListFamilia());
	    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
	    	request.getSession().setAttribute("listunidad", control.getListunidad());
	    	request.getSession().setAttribute("listUbicacion", control.getListUbicacion());
	    	request.getSession().setAttribute("listDescriptores", control.getListDescriptores());
	    	request.setAttribute("condicion",control.getCondicion());
	    	control.setBien(chekea(control));
    	}
    	if (control.getOperacion().trim().equals("ELI_FOTO"))
    	{
    		res = elimFoto(control);
    		if ( res.equals("-1")) {
    			control.setMens("ERROR");
    		}
    		else if ( res.equals("-2")) {
    			control.setMens("DOBLE");
    		}
    		else control.setMens("OK");
    		llenar(control);
    		cargarDatos(control);
    		subFamilia(control);
    		
    		request.getSession().setAttribute("listdocRela", control.getListdocRela());
      		request.getSession().setAttribute("listFamilia", control.getListFamilia());
	    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
	    	request.getSession().setAttribute("listunidad", control.getListunidad());
	    	request.getSession().setAttribute("listUbicacion", control.getListUbicacion());
	    	request.getSession().setAttribute("listDescriptores", control.getListDescriptores());
	    	request.setAttribute("condicion",control.getCondicion());
	    	control.setBien(chekea(control));
    	}
		log.info("onSubmit:FIN");
		System.out.println("NomProd End onSubmit:"+control.getNomPro());
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Producto","control",control);		
    }
        private void cargarDatos(AgregarProductoCommand control){
        	TipoLogistica obj = new TipoLogistica();
        
        	obj.setCodTabla(CommonConstants.TIPT_ESTADO);
        	obj.setTipo(CommonConstants.MTO_CATALOGO_ESTADO);
        	control.setListEstado(this.detalleManager.GetAllTablaDetalle(obj));
        	System.out.println("estados: "+control.getListEstado().size());
        	
        	obj.setCodTabla(CommonConstants.TIPT_SEDE);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	control.setListSede(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
        	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_UNIDAD_MEDIDA);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
        	control.setListunidad(this.detalleManager.GetAllTablaDetalle(obj));
            System.out.println("sede: "+control.getCodSedeAux());
        	obj.setCodId(control.getCodSedeAux());
        	obj.setCodTabla(CommonConstants.TIPT_UBICACION);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	control.setListUbicacion(this.detalleManager.GetAllTablaDetalle(obj));
        
         	System.out.println("codPro: "+control.getCodProducto());
        	control.setListDescriptores(this.detalleManager.GetAllDescriptoresByBien(control.getCodProducto()));
        	System.out.println("tot"+control.getListDescriptores().size());
        	
        	System.out.println("nomprod Cargadatos: "+control.getNomPro());
        }
        //ALQD,23/02/09.AGREGANDO UNA PROPIEDAD MAS. PROMOCIONAL
        private String grabar(AgregarProductoCommand control){
        	if(control.getChkInv()==null){
        		control.setChkInv("");
        	}
        	if(control.getChkPromo()==null){
        		control.setChkPromo("");
        	}
        	System.out.println("con pa grabar: "+control.getCondicion());
        	System.out.println("Promo. "+control.getChkPromo());
        	System.out.println("Pre.Ref. "+control.getPreURef());
        	if(control.getCodUbicacion().equals("-1")) control.setCodUbicacion("");
        	//ALQD,24/02/09.ACTUALIZAREMOS EL PRE.REFERENCIAL
        	//ALQD,20/05/09.NUEVA PROPIEDAD NO AFECTA
        	control.setGrabar(this.detalleManager.InsertProducto(control.getCodProducto()
        			, control.getCodSedeAux(), control.getCodFamilia(), control.getCodSubFamilia()
        			, control.getNomPro(), control.getDescripcion(), control.getCodUnidad()
        			, control.getCondicion(), control.getCodUbicacion(), control.getUbiFila()
        			, control.getUbiColum(), control.getMin(), control.getMax()
        			,control.getNroDias(), control.getMarca(), control.getModelo()
        			, control.getCodAlumno(), control.getChkInv(), control.getStockMostrar()
        			,control.getCodEstado(), control.getChkPromo(),control.getPreURef()
        			,control.getChkNoAfecto()));
        	return control.getGrabar();
        }
        //ALQD,23/02/09. NUEVA PROPIEDAD
        private void llenar(AgregarProductoCommand control){
        	control.setListdocRela(this.detalleManager.GetAllDocCatalogo(control.getCodProducto(), ""));
        	System.out.println("sede  : "+control.getCodSedeAux());
        	System.out.println("famili: "+control.getCodFamilia());
        	System.out.println("subFam: "+control.getCodSubFamilia());
        	System.out.println("pro   : "+control.getCodProducto());
        	System.out.println("nom   : "+control.getNomPro());
        	control.setListProductos(this.detalleManager.GetAllProducto(control.getCodProducto(),control.getCodSedeAux()
        			, control.getCodFamilia(), control.getCodSubFamilia(), ""
        			, control.getNomPro(), "", "", "",control.getCodEstado()));
        	List lst  = control.getListProductos();
        	if(lst!=null ){
        		if ( lst.size()>0 )
        		{
        			CatalogoProducto catalogoProducto = null;
        			catalogoProducto = (CatalogoProducto)lst.get(0); 
        			
        			control.setNomPro(catalogoProducto.getNomProducto());
        			System.out.println("nom  2: "+control.getNomPro());
        			control.setDescripcion(catalogoProducto.getDescripcion());
        			control.setCodUnidad(catalogoProducto.getUnidadMedida());
        			control.setCondicion(catalogoProducto.getCondicion());
        			control.setCodUbicacion(catalogoProducto.getCodUbicacion());
        			control.setUbiColum(catalogoProducto.getUbicacionColum());
        			control.setUbiFila(catalogoProducto.getUbicacionFila());
        			control.setMin(catalogoProducto.getStockMin());
        			control.setMax(catalogoProducto.getStockMax());
        			control.setNroDias(catalogoProducto.getNroDiasAtencion());
        			control.setRu(catalogoProducto.getRutaImagen());
        			control.setMarca(catalogoProducto.getMarca());
        			control.setModelo(catalogoProducto.getModelo());
        			control.setCondicion(catalogoProducto.getTipoBien());
        			control.setCodFamilia(catalogoProducto.getCodFamilia());
        			control.setCodSubFamilia(catalogoProducto.getCodSubFamilia());
        			control.setChkInv(catalogoProducto.getInversion());
        			control.setStockMostrar(catalogoProducto.getStockMostrar());
        			control.setCodEstado(catalogoProducto.getEstado());
        			control.setStockDisponible(catalogoProducto.getStockDisponible());
        			control.setStockActual(catalogoProducto.getStockActual());
        			control.setPreURef(catalogoProducto.getPrecioReferencial());
        			control.setChkPromo(catalogoProducto.getArtPromocional());
        			control.setPrecio(catalogoProducto.getPrecio());
//ALQD,20/05/09.A�ADIENDO NUEVA PROPIEDAD AFECTO
        			control.setChkNoAfecto(catalogoProducto.getArtNoAfecto());
        		}
        	}
        }
        
        private String eliminar(AgregarProductoCommand control){
        	
        	control.setGrabar(this.detalleManager.DeleteDocProducto(control.getCodProducto()
        			, control.getCodAdj(), control.getCodAlumno()));
        	return control.getGrabar();
        }
        //ALQD,20/08/09.ELIMINA FOTO DEL PRODUCTO
        private String elimFoto(AgregarProductoCommand control){
        	
        	control.setGrabar(this.detalleManager.DeleteFotoProducto(control.getCodProducto()
        		, control.getCodAlumno()));
        	return control.getGrabar();
        }
        
        private String deleteDescriptor(AgregarProductoCommand control){
        	 	control.setGrabar(this.detalleManager.DeleteDescriptoresByBien(control.getCodProducto()
        	 			, control.getCodDescriptor(), control.getCodAlumno()));
        	return control.getGrabar();
        }
      
        private void subFamilia(AgregarProductoCommand control){
        	if(control.getCodFamilia().equals("")){
        		TipoLogistica obj = new TipoLogistica();
            	obj.setCodId(CommonConstants.TIPT_VALOR);
            	obj.setCodTabla(CommonConstants.TIPT_SUB_FAMILIA);
            	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
              	control.setListSubFamilia(this.detalleManager.GetAllTablaDetalle(obj));
            }
        	else{
        	   	TipoLogistica obj = new TipoLogistica();
            	obj.setCodId(control.getCodFamilia());
            	obj.setCodTabla(CommonConstants.TIPT_SUB_FAMILIA);
            	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
            	control.setListSubFamilia(this.detalleManager.GetAllTablaDetalle(obj));
            	
        	}
        }
        private String chekea(AgregarProductoCommand control){
        	TipoLogistica obj = new TipoLogistica();
        	
        	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
        	obj.setCodigo(control.getCodFamilia());
        	List lst  = this.detalleManager.GetAllTablaDetalle(obj);
        	if(lst!=null ){
        		if ( lst.size()>0 )
        		{
        			Logistica logistica = null;
        			logistica = (Logistica)lst.get(0); 
        			control.setBien(logistica.getCodPadre());
        		}
        		}
        	return control.getBien();
        }
}
