package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoEvaProveedorCommand;

public class MantenimientoEvaProveedorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoEvaProveedorFormController.class);
  DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public MantenimientoEvaProveedorFormController() {    	
        super();        
        setCommandClass(MantenimientoEvaProveedorCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoEvaProveedorCommand command = new MantenimientoEvaProveedorCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	if(!(command.getCodAlumno()==null)){
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_CALIFICACION);
    	obj.setTipo(CommonConstants.DELETE_MTO_LOG);
    	command.setListCalificacion(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listCalificacion", command.getListCalificacion());
    	command.setTotal(command.getListCalificacion().size());
    	}
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado = "";
    	MantenimientoEvaProveedorCommand control = (MantenimientoEvaProveedorCommand) command;
    	TipoLogistica obj = new TipoLogistica();
    	if (control.getOperacion().trim().equals(""))
    	{
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_CALIFICACION);
    	obj.setTipo(CommonConstants.DELETE_MTO_LOG);
    	control.setListCalificacion(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listCalificacion", control.getListCalificacion());
    	}
    	if (control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		resultado = delete(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    			}
    		else {
    			control.setMsg("OK");
    			}
    		obj.setCodTabla(CommonConstants.TIPT_TIPO_CALIFICACION);
    		obj.setTipo(CommonConstants.DELETE_MTO_LOG);
        	control.setListCalificacion(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listCalificacion", control.getListCalificacion());
        
    	}
    	if (control.getOperacion().trim().equals("ACTUALIZAMOS"))
    	{
    		resultado=actualiza(control);
    		if ( resultado.equals("-1")) {
    			control.setMen("ERROR");
    			}
    		else {
    			control.setMen("OK");
    			}
    		obj.setCodTabla(CommonConstants.TIPT_TIPO_CALIFICACION);
    		obj.setTipo(CommonConstants.DELETE_MTO_LOG);
        	control.setListCalificacion(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listCalificacion", control.getListCalificacion());
        	control.setTotal(control.getListCalificacion().size());
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Eva_Proveedores","control",control);		
    }
        private String delete(MantenimientoEvaProveedorCommand control){
        	
        	control.setEliminar(this.detalleManager.DeleteTablaDetalle(CommonConstants.TIPT_TIPO_CALIFICACION
        			, control.getSecuencial(), control.getCodAlumno()));
        	
        	return control.getEliminar();
        }
        private String actualiza(MantenimientoEvaProveedorCommand control){
        System.out.println("codigo: "+CommonConstants.TIPT_TIPO_CALIFICACION);
        System.out.println("cadDet: "+control.getCadDetalle());
        System.out.println("cadInd: "+control.getCadIndicador());
        System.out.println("codUsu: "+control.getCodAlumno());
        	control.setGraba(this.detalleManager.ActIndiceFlujo(CommonConstants.TIPT_TIPO_CALIFICACION
        			, control.getCadDetalle(), control.getCadIndicador()));
        	
        	return  control.getGraba();	
        }
        
}
