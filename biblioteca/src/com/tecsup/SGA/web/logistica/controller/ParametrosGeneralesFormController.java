package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.ParametrosGeneralesCommand;

public class ParametrosGeneralesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ParametrosGeneralesFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public ParametrosGeneralesFormController() {    	
        super();        
        setCommandClass(ParametrosGeneralesCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ParametrosGeneralesCommand command = new ParametrosGeneralesCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
     	
    	Anios anios= new Anios();
		command.setCodListaAnios(anios.getAniosTodosFromToDay(CommonConstants.FECHA_BASE_MAXIMO));
    	command.setCodListaMeses(anios.getMeses02());
  
    	
    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
    	llena(command);
          	}
    	}
     	
    	log.info("formBackingObject:FIN");
    	return command;
		}  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
		ParametrosGeneralesCommand control = (ParametrosGeneralesCommand) command;
    	TipoLogistica obj = new TipoLogistica();
    	
    	if(control.getOperacion().equals("GRABAR")){
    		resultado = actualiza(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else control.setMsg("OK");
    		llena(control);
    	}
    	
    	
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_ParametrosGenerales","control",control);		
    }
	
        public String actualiza(ParametrosGeneralesCommand control){
          	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getIgv()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0002","", "", "", "", "", control.getCodAlumno()));
        	
          	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getCtaCentroCosto()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0007","", "", "", "", "", control.getCodAlumno()));
        	
          	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getGlosaCabecera()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0004","", "", "", "", "", control.getCodAlumno()));
        	
          	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getCtaExis()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0005","", "", "", "", "", control.getCodAlumno()));
        	
          	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getCtaTipoGas()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0006","", "", "", "", "", control.getCodAlumno()));
        	System.out.println("detalle: "+control.getGlosaDetalle());
          	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getGlosaDetalle()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0009","", "", "", "", "", control.getCodAlumno()));
        	
          	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getCodMeses()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0008",control.getCodAnios(), "", "", "", "", control.getCodAlumno()));
        
         	control.setGrabar(this.detalleManager.UpdateTablaDetalle(control.getCuser()
        			, CommonConstants.TIPT_PARAMETROS_GENERALES, "0010","", "", "", "", "", control.getCodAlumno()));
       
        	return control.getGrabar();
        	
        }
        private void llena(ParametrosGeneralesCommand control){
        	TipoLogistica obj = new TipoLogistica();
        	Logistica logistica = null;
        	List lista= null;

        	//GLosa CAbecera
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0002");
        	lista=this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
        			if (lista.size()>0)
        			{
            			logistica = (Logistica)lista.get(0);
            			control.setIgv(logistica.getCodPadre());
        			}
        	
        	//GLosa CAbecera
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0004");
        	lista=this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
        			if (lista.size()>0)
        			{
            			logistica = (Logistica)lista.get(0);
            			control.setGlosaCabecera(logistica.getCodPadre());
        			}

        	//Cta. Contable Existencia por Existir
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0005");
        	lista=this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
        			if (lista.size()>0)
        			{
            			logistica = (Logistica)lista.get(0);
            			control.setCtaExis(logistica.getCodPadre());
        			}

        	//Cta. Contable Tipo Gasto Ajustes
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0006");
        	lista=this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
        			if (lista.size()>0)
        			{
            			logistica = (Logistica)lista.get(0);
            			control.setCtaTipoGas(logistica.getCodPadre());
        			}

        	//Cta. Contable Centro Costo Ajustes
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0007");
        	lista=this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
        			if (lista.size()>0)
        			{
            			logistica = (Logistica)lista.get(0);
            			control.setCtaCentroCosto(logistica.getCodPadre());
        			}
        	
        	//Periodo
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0008");
        	lista = this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
        			if (lista.size()>0)
        			{
            			logistica = (Logistica)lista.get(0);
            			control.setCodAnios(logistica.getCodigo());
            			control.setCodMeses(logistica.getCodPadre());
        			}
        	
        	//Glosa Detalle
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0009");
        	lista=this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
        			if (lista.size()>0)
        			{
            			logistica = (Logistica)lista.get(0);
            			control.setGlosaDetalle(logistica.getCodPadre());
        			}
        	//Cuser
        	obj.setCodTabla(CommonConstants.TIPT_PARAMETROS_GENERALES);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_LOG);
        	obj.setCodId("0010");
        	lista=this.detalleManager.GetAllTablaDetalle(obj);
        	if(lista!=null)
    			if (lista.size()>0)
    			{
        			logistica = (Logistica)lista.get(0);
        			control.setCuser(logistica.getCodPadre());
    			}
        	/*if(lista!=null && lista.size()>0){
				
        		if ( lista.size()>0 )
        		{
        			Logistica logistica = null;
        			logistica = (Logistica)lista.get(1);
        			control.setCodAnios(logistica.getCodigo());
        			control.setCodMeses(logistica.getCodPadre());
        			
        			logistica = (Logistica)lista.get(2);
        			control.setCtaCentroCosto(logistica.getCodPadre());
        		
        			
        			logistica = (Logistica)lista.get(3);
        			control.setGlosaCabecera(logistica.getCodPadre());
        			
        			
        			logistica = (Logistica)lista.get(4);
        			control.setCtaExis(logistica.getCodPadre());
        			
        			
        			logistica = (Logistica)lista.get(5);
        			control.setCtaTipoGas(logistica.getCodPadre());
        			
        			
        			logistica = (Logistica)lista.get(6);
        			control.setIgv(logistica.getCodPadre());
        			
        			logistica = (Logistica)lista.get(8);
        			control.setGlosaDetalle(logistica.getCodPadre());
        		}
        	}*/
        	
        }
	
}
