package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.RecepcionBienesCommand;
import com.tecsup.SGA.web.logistica.command.RecepcionBienesDevueltosCommand;

public class RecepcionBienesDevueltosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RecepcionBienesDevueltosFormController.class);
	 AlmacenManager almacenManager;
	 TablaDetalleManager tablaDetalleManager;
	 SeguridadManager seguridadManager;
	 
	    public RecepcionBienesDevueltosFormController() {    	
	        super();        
	        setCommandClass(RecepcionBienesDevueltosCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	RecepcionBienesDevueltosCommand command = new RecepcionBienesDevueltosCommand();
	        command.setSede((String)request.getParameter("txhCodSede"));	
	        command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
	        
	        System.out.println("codU: "+command.getCodUsuario());
	        System.out.println("sede: "+command.getSede());
	        
	        List lista2= new ArrayList();
	        List lista3= new ArrayList();
	        
	        lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO_ALMACEN 
	    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
	        if(lista2!=null)
	    	command.setListEstado(lista2);
	        lista3=this.seguridadManager.GetAllCentrosCosto(command.getSede(),"","");
	        if(lista3!=null)
	    	command.setListCentroCosto(lista3);
	        
        BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean!= null)
         {  log.info("BandejaLogisticaBean:INICIO");
         if(bandejaBean.getCodBandeja().equals("9")){
	         command.setFechaDevolucionIni(bandejaBean.getValor1());
	         command.setFechaDevolucionFin(bandejaBean.getValor2());
	         command.setCodCentro(bandejaBean.getValor3());
	         command.setSolicitante(bandejaBean.getValor4());
	         command.setCodEstado(bandejaBean.getValor5());
	         buscar(command, request);
            }         	 
		 	 
       	 log.info("BandejaLogisticaBean:FIN");
       	 
         }
	        
	        
	        log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	RecepcionBienesDevueltosCommand control = (RecepcionBienesDevueltosCommand) command;
	    
	    	List lista3= new ArrayList();
	    	lista3=this.seguridadManager.GetAllCentrosCosto(control.getSede(),"","");
	        if(lista3!=null){
	    	control.setListCentroCosto(lista3);
	        }
	        
	        if(control.getOperacion().equals("BUSCAR"))
	        { buscar(control, request);
	    	  
	        }
	  		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/Almacen/RecepcionBienesDevueltos","control",control);		
	    }
	    private void buscar(RecepcionBienesDevueltosCommand control, HttpServletRequest request){
	      List lista4= new ArrayList();
	        	
	       GuardarDatosBandeja(control, request);
	        	
	       lista4=this.almacenManager.GetAllBienesDevolucion(control.getSede()
	        		, control.getFechaDevolucionIni(), control.getFechaDevolucionFin()
	        		, control.getCodCentro(), control.getSolicitante(), control.getCodEstado());	
	       if(lista4!=null){
	       control.setListBienesDevueltos(lista4);
	        }
	       request.getSession().setAttribute("listBienesDevueltos", control.getListBienesDevueltos());
	        }
	        
	public void GuardarDatosBandeja(RecepcionBienesDevueltosCommand control,HttpServletRequest request)
	{
	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	 	 
	 	 bandejaBean.setCodBandeja("9");
	 	 
	 	 bandejaBean.setValor1(control.getFechaDevolucionIni());
	 	 bandejaBean.setValor2(control.getFechaDevolucionFin());
	 	 bandejaBean.setValor3(control.getCodCentro());
	 	 bandejaBean.setValor4(control.getSolicitante());
	 	 bandejaBean.setValor5(control.getCodEstado());
	 			    	 	 
		 log.info("GuardarDatosBandeja");
		 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	}
	        
		public void setAlmacenManager(AlmacenManager almacenManager) {
			this.almacenManager = almacenManager;
		}

		public TablaDetalleManager getTablaDetalleManager() {
			return tablaDetalleManager;
		}

		public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
			this.tablaDetalleManager = tablaDetalleManager;
		}

		public SeguridadManager getSeguridadManager() {
			return seguridadManager;
		}

		public void setSeguridadManager(SeguridadManager seguridadManager) {
			this.seguridadManager = seguridadManager;
		}

		public AlmacenManager getAlmacenManager() {
			return almacenManager;
		}
}
