package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.CotizacionActualServiciosCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;

public class CotizacionActualServiciosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotizacionActualServiciosFormController.class);
	private DetalleManager detalleManager;
	private SeguridadManager seguridadManager;
	private SolRequerimientoManager solRequerimientoManager;
	private CotBienesYServiciosManager cotBienesYServiciosManager;

	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public static void setLog(Log log) {
		CotizacionActualServiciosFormController.log = log;
	}

	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	CotizacionActualServiciosCommand command = new CotizacionActualServiciosCommand();
    	//*********************************************************
    	command.setCodTipoCotizacion(request.getParameter("txhCodTipoCotizacion") == null ? "": request.getParameter("txhCodTipoCotizacion"));
    	command.setCodSubTipoCotizacion(request.getParameter("txhCodSubTipoCotizacion") == null ? "": request.getParameter("txhCodSubTipoCotizacion"));    	
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setIndInversion(request.getParameter("txhIndInversion") == null ? "": request.getParameter("txhIndInversion"));
    	System.out.println("codTipoCotizacion="+command.getCodTipoCotizacion()+"<<");
    	System.out.println("codSubTipoCotizacion="+command.getCodSubTipoCotizacion()+"<<");
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");    	
    	System.out.println("indInversion="+command.getIndInversion()+"<<");
    	//*********************************************************
    	
    	command.setConsteCajaChica(CommonConstants.COD_CAJA_CHICA);
    	command.setConsteOrdenCompra(CommonConstants.COD_ORDEN_COMPRA);
    	
    	if(!command.getCodUsuario().equals("")){    		
    		cargaCabecera(command);
    	}    
    	
    	Fecha fecha= new Fecha();
    	command.setFechaVigenciaIniBD(fecha.getFechaActual());
    	command.setHoraVigenciaIniBD("00:00");
    	command.setFechaVigenciaFinBD(fecha.getFechaActual());
    	command.setHoraVigenciaFinBD("23:59");
    	command.setFechaVigenciaIniBD2(fecha.getFechaActual());
    	command.setHoraVigenciaIniBD2(fecha.getHoraActual());
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	CotizacionActualServiciosCommand control = (CotizacionActualServiciosCommand)command;
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = GrabarCotizacion(control);
    		System.out.println("resultado de GRABAR COTIZACION-->"+resultado);
    		cargaCabecera(control);
    		if (resultado.equals("0")){
    			control.setMsg("OK_GRABAR");
    		}
    		else{
    			control.setMsg("ERROR_GRABAR");    			
    		}
    	}
    	else
		if (control.getOperacion().trim().equals("QUITAR")){
    		resultado = DeleteDetalleCotizacion(control);
    		System.out.println("resultado de QUITAR DETALLE COTIZACION-->"+resultado);
    		cargaCabecera(control);
    		if (resultado.equals("0")){
    			control.setMsg("OK_QUITAR");
    		}
    		else{
    			control.setMsg("ERROR_QUITAR");    			
    		}
    	}
		else
		if (control.getOperacion().trim().equals("ENVIAR")){
    		resultado = EnviarCotizacion(control);
    		System.out.println("resultado de ENVIAR COTIZACION-->"+resultado);
    		cargaCabecera(control);
    		if (resultado.equals("0")){
    			control.setMsg("OK_ENVIAR");
    		}
    		else if (resultado.equals("-1")){
    			control.setMsg("ERROR_ENVIAR");
    		}
    		else if (resultado.equals("-2")){
    			control.setMsg("ERROR_PROV");
    		}
    		else if (resultado.equals("-3")){
    			control.setMsg("ERROR_DATOS");
    		}
    	}
		else{
			cargaCabecera(control);
		}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/CotBienesYServicios/Cotizacion_Actual_Servicios","control",control);
    }
    private void cargaCabecera(CotizacionActualServiciosCommand control){
    	//carga combo tipo de pago
    	TipoLogistica obj = new TipoLogistica();    	
    	obj.setCodPadre("");
    	obj.setCodId("");
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_PAGO);
    	obj.setCodigo("");
    	obj.setDescripcion("");
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	control.setListaPago(this.detalleManager.GetAllTablaDetalle(obj));
    	cargaDatosCabecera(control);
    }
    private void cargaDatosCabecera(CotizacionActualServiciosCommand control){
    	List lista=this.cotBienesYServiciosManager.GetCotizacionActual(control.getCodSede()
    			, control.getCodTipoCotizacion()
    			, control.getCodSubTipoCotizacion()
    			, control.getCodUsuario()
    			, control.getIndInversion());
    	
    	if(lista!=null && lista.size()>0){
    		Cotizacion cotizacion = (Cotizacion)lista.get(0);    		
    		
    		control.setCboTipoPago(cotizacion.getCodTipoPago());
    		control.setFechaVigenciaIni(cotizacion.getFechaInicio());
    		control.setHoraVigenciaIni(cotizacion.getHoraInicio());
    		control.setFechaVigenciaFin(cotizacion.getFechaFin());
    		control.setHoraVigenciaFin(cotizacion.getHoraFin());
    		if(control.getFechaVigenciaIni().equals("")&&control.getHoraVigenciaIni().equals(""))
      		  control.setFlag("1");
      		else control.setFlag("0");
    		//establece el codigo de cotizacion
    		System.out.println("codigo>>"+cotizacion.getCodigo()+"<<");
    		control.setCodCotizacion(cotizacion.getCodigo());
    		cargaBandeja(control);
    	}
    	else{
    		control.setCodCotizacion("");
    	}
    }
    private void cargaBandeja(CotizacionActualServiciosCommand control){
    	List lista=this.cotBienesYServiciosManager.GetAllDetalleCotizacionActual(control.getCodCotizacion(), control.getCodTipoCotizacion());
    	if(lista!=null && lista.size()>0){
    		control.setListaCotizacion(lista);
    	}
    }
    private String GrabarCotizacion(CotizacionActualServiciosCommand control){    	
    	try{
    		System.out.println("codCotizacion>>"+control.getCodCotizacion()+"<<");
    		System.out.println("usuarioResponsable>>"+control.getCodUsuario()+"<<");
    		System.out.println("codTipoPago>>"+control.getCboTipoPago()+"<<");
    		System.out.println("fechaInicio>>"+control.getFechaVigenciaIni()+"<<");
    		System.out.println("horaInicio>>"+control.getHoraVigenciaIni()+"<<");
    		System.out.println("fechaFin>>"+control.getFechaVigenciaFin()+"<<");
    		System.out.println("horaFin>>"+control.getHoraVigenciaFin()+"<<");
    		System.out.println("cadenaDescripcion>>"+control.getCadenaDescripcion()+"<<");
    		System.out.println("cadenaCodDetalle>>"+control.getCadenaCodDetalle()+"<<");
    		System.out.println("cantidad>>"+control.getCantidad()+"<<");
    		
    		Cotizacion cotizacion = new Cotizacion();   		
    		
    		cotizacion.setCodigo(control.getCodCotizacion());
    		cotizacion.setUsuResponsable(control.getCodUsuario());
    		cotizacion.setCodTipoPago(control.getCboTipoPago());
    		cotizacion.setFechaInicio(control.getFechaVigenciaIni());
    		cotizacion.setHoraInicio(control.getHoraVigenciaIni());
    		cotizacion.setFechaFin(control.getFechaVigenciaFin());
    		cotizacion.setHoraFin(control.getHoraVigenciaFin());    		
    		
    		String resultado="";
    		resultado=this.cotBienesYServiciosManager.UpdateCotizacionActual(cotizacion, control.getCadenaDescripcion(), 
    				control.getCadenaCodDetalle(), control.getCantidad());    		
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String DeleteDetalleCotizacion(CotizacionActualServiciosCommand control){
    	try{
    		System.out.println("codCotizacion>>"+control.getCodCotizacion()+"<<");
    		System.out.println("codDetalle>>"+control.getCodDetalle()+"<<");
    		System.out.println("codUsuario>>"+control.getCodUsuario()+"<<");
    		String resultado=""; 
    		resultado=this.cotBienesYServiciosManager.DeleteDetalleCotizacionActual(control.getCodCotizacion(), control.getCodDetalle(), control.getCodUsuario());
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }    
    private String EnviarCotizacion(CotizacionActualServiciosCommand control){
    	try{
    		System.out.println("codCotizacion>>"+control.getCodCotizacion()+"<<");    	
    	
    		String resultado=""; 
    		resultado=this.cotBienesYServiciosManager.EnviarCotizacionUsuario(control.getCodCotizacion(),control.getCodUsuario());    		
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}
