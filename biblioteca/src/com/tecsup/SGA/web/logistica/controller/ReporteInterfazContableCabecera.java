package com.tecsup.SGA.web.logistica.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.logistica.InterfazContableManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;

public class ReporteInterfazContableCabecera implements Controller{
	
	private static Log log = LogFactory.getLog(ReporteInterfazContableCabecera.class);
	private InterfazContableManager interfazContableManager;
	private SeguridadManager seguridadManager;
	
	 public InterfazContableManager getInterfazContableManager() {
		return interfazContableManager;
	}

	public void setInterfazContableManager(
			InterfazContableManager interfazContableManager) {
		this.interfazContableManager = interfazContableManager;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
	            HttpServletResponse response)
	    throws Exception {
		 log.info("onSubmit:INI");
	    
		 UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if(usuarioSeguridad==null){
    		InicioCommand command = new InicioCommand();
    		return new ModelAndView("/cabeceraLogistica","control",command);
    	} 		 		 
		 
	     ModelMap model = new ModelMap();
	     String pagina="";
	     String fechaRep = Fecha.getFechaActual();
		 String horaRep = Fecha.getHoraActual();
	     List lista= new ArrayList();
	     
	     String codSede=request.getParameter("txhCodSede");
	     String codAnio=request.getParameter("txhCodAnio");
	     String codMes=request.getParameter("txhCodMes");
	     String codTipoMovimiento=request.getParameter("txhCodTipoMovimiento");
	     String operacion=request.getParameter("txhOperacion");
	     String codUsuario="";
	     String dscUsuario="";
	     String txhNomSede="";
	     
	     
	    	if ( usuarioSeguridad != null )
	    	{
	    		codUsuario=usuarioSeguridad.getIdUsuario();
	    		dscUsuario=usuarioSeguridad.getNomUsuario();
	    		System.out.println("DscUsuario: "+dscUsuario);
	    		List listaSede= new ArrayList();
	    		listaSede=this.seguridadManager.getAllSedes();
	    		if(codSede!=null)
	    		{if(listaSede!=null)
	    			{  SedeBean sede=new SedeBean();
	    				for(int a=0;a<listaSede.size();a++)
	    				{ sede=(SedeBean)listaSede.get(a);
	    				if(codSede.equals(sede.getCodSede()))
	    				{  txhNomSede=sede.getDscSede();
	    			      a=listaSede.size();
	    			    }
	    			}
	    			
	    			}
	    		}
	    	}
	     
	    
	    if(operacion.equals("CABECERA"))
	    {   try {lista=interfazContableManager.GetCabeceraInterfaz(codSede, codMes, codAnio, codTipoMovimiento);
		          if(lista!=null && lista.size()>0)
		          System.out.println("lista: "+lista.size());
			} catch (Exception e) {
				
				System.out.println("Exception");
			}
			request.getSession().setAttribute("lista_cabecera", lista);
			pagina = "/logistica/consultaReportes/rep_Gen_InterfazContable_Cabecera";
	    }
	    else{ if(operacion.equals("DETALLE"))
		    {   try {lista=interfazContableManager.GetDetalleInterfaz(codSede, codMes, codAnio, codTipoMovimiento);
	               if(lista!=null && lista.size()>0)
	               System.out.println("lista: "+lista.size());
		    	} catch (Exception e) {
		    		
		    		System.out.println("Exception");
		    	}
		    	request.getSession().setAttribute("lista_cabecera", lista);
		    	pagina = "/logistica/consultaReportes/rep_Gen_InterfazContable_Detalle";
		    }
	    	
	    }
		model.addObject("fechaRep", fechaRep);
		model.addObject("horaRep", horaRep);
		model.addObject("NOM_SEDE", txhNomSede);
		model.addObject("usuario", dscUsuario);
				
		 
		 
		 log.info("onSubmit:FIN");
		 return new ModelAndView(pagina,"model",model);
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	

}
