package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.AtenderRequerimientoServiciosCommand;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;

public class AtenderRequerimientoServiciosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AtenderRequerimientoServiciosFormController.class);	
	private AtencionRequerimientosManager atencionRequerimientosManager;
	private SolRequerimientoManager solRequerimientoManager;

	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	AtenderRequerimientoServiciosCommand command = new AtenderRequerimientoServiciosCommand();
    	//*********************************************************
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");
    	
    	if(command.getCodRequerimiento()!=null){
	    	if(!command.getCodRequerimiento().equals("")){    		
	    		cargaDatosCabecera(command);
	    	}
    	}
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	AtenderRequerimientoServiciosCommand control = (AtenderRequerimientoServiciosCommand)command;    	
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("AGREGAR")){
    		resultado = InsertGuiaRequerimiento(control);
    		cargaBandeja(control);
    		System.out.println("resultado codGuia-->"+resultado);    		
    		
    		if(resultado.equals("-2"))
    			  control.setMsg("NO_MAS_GUIAS");
    		else{ if(resultado.equals("-1"))
    				     control.setMsg("ERROR");
    			  else{
    		    			 control.setCodGuia(resultado);
    		    			 control.setMsg("OKAGREGAR");
    		    	  }
    			  }   
    			  
    	
    	}
    	else
		if (control.getOperacion().trim().equals("QUITAR")){
    		resultado = DeleteGuiaRequerimiento(control);
    		System.out.println("resultado Quitar-->"+resultado);
    		if (resultado!=null){
    			if (resultado.equals("-2")){    			
        			control.setMsg("ERRORPENDIENTE");
        		}
        		else{
        			if (resultado.equals("-1")){
        				control.setMsg("ERROR");
        			}        					    			
        		}
    		}
    		else{
				control.setMsg("OKQUITAR");
			}     		
    		cargaBandeja(control);
    	}
		else
		if (control.getOperacion().trim().equals("ACTUALIZAR")){    		     		
    		cargaBandeja(control);
    		control.setMsg("OKAGREGAR");
    	}
		else
		if (control.getOperacion().trim().equals("REGISTRARCONFORMIDAD")){
			
    		resultado = RegistrarConformidad(control);    		
    		    		
    		if (resultado.equals("0")){    			
    			control.setMsg("OK_REGISTRAR_CONFORMIDAD");
    		}
    		else
    		if(resultado.equals("-2")){
    			control.setMsg("ERROR_RESPONSABLE");	
    		}
    		else{ if(resultado.equals("-3")){
    				 control.setMsg("ERROR_CANT_EXCEDE");	
    				}
    			  else if(resultado.equals("-4")){
     				 	control.setMsg("ERROR_CANT_ENTREGADA");	
  						}
    			  		else
    				   control.setMsg("ERROR_REGISTRAR_CONFORMIDAD");
    		}
    		cargaBandeja(control);
    	}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/Atencion_Requerimiento_Servicios","control",control);
    }
    private void cargaDatosCabecera(AtenderRequerimientoServiciosCommand control){
    	List lista=this.solRequerimientoManager.getSolReqUsuario(control.getCodRequerimiento());   
    	
    	if(lista!=null && lista.size()>0){
    		SolRequerimiento obj = (SolRequerimiento)lista.get(0);    		
    		
    		control.setNroRequerimiento(obj.getNroRequerimiento());
    		control.setUsuSolicitante(obj.getUsuSolicitante());

    		cargaBandeja(control);
    	}
    }
    private void cargaBandeja(AtenderRequerimientoServiciosCommand control){
    	List lista=this.atencionRequerimientosManager.GetAllGuiasAsociadas(control.getCodRequerimiento());
    	if(lista!=null && lista.size()>0){
    		control.setListaGuias(lista);
    		control.setTamListaGuias(""+lista.size());
    	}
    }
    private String InsertGuiaRequerimiento(AtenderRequerimientoServiciosCommand control){
    	try {
			String codGuia=this.atencionRequerimientosManager.InsertGuiaRequerimiento(control.getCodRequerimiento(), control.getCodUsuario());
			return codGuia;
		}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    	return null;
    }
    private String DeleteGuiaRequerimiento(AtenderRequerimientoServiciosCommand control){
    	try{
    		System.out.println("codGuia>>"+control.getCodGuia()+"<<");    		
    		System.out.println("codUsuario>>"+control.getCodUsuario()+"<<");
    		String resultado=""; 
    		resultado=this.atencionRequerimientosManager.DeleteGuiaRequerimiento(control.getCodGuia(), control.getCodUsuario());
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
    private String RegistrarConformidad(AtenderRequerimientoServiciosCommand control){
    	try{
    		System.out.println("codGuia>>"+control.getCodGuia()+"<<");    		
    		System.out.println("codUsuario>>"+control.getCodUsuario()+"<<");
    		String resultado=""; 
    		resultado=this.atencionRequerimientosManager.RegistrarConformidad(control.getCodGuia(), control.getCodUsuario());    		
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}
