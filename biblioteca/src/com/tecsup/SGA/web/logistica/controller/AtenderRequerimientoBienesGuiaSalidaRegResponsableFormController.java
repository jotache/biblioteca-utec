package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.tecsup.SGA.web.logistica.command.AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand;
import com.tecsup.SGA.bean.ResponsableByActivoBean;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;

public class AtenderRequerimientoBienesGuiaSalidaRegResponsableFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AtenderRequerimientoBienesGuiaSalidaRegResponsableFormController.class);	
	private CotBienesYServiciosManager cotBienesYServiciosManager;
	private AtencionRequerimientosManager atencionRequerimientosManager;
	
	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}
	
	public AtenderRequerimientoBienesGuiaSalidaRegResponsableFormController() {    	
        super();        
        setCommandClass(AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand control = new AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand();    	
    	
    	if(request.getParameter("prmCodGuia")!=null){
    		
    		control.setCodGuia(request.getParameter("prmCodGuia"));
    		control.setCodGuiaDetalle(request.getParameter("prmCodGuiaDetalle"));
    		control.setCantidadEntregada(request.getParameter("prmCantidadEntregada"));
    		control.setCodBien(request.getParameter("prmCodBien"));
    		control.setCodSede(request.getParameter("prmCodSede"));
    		control.setUsuario(request.getParameter("prmUsuario"));
    		control.setCodEstado(request.getParameter("txhCodEstado"));
    		System.out.println("CodEstado: "+control.getCodEstado());
    		log.info("PARAMETROS PARA EL SP getAllRespByActivo");
    		log.info("control.getCodGuia()>>"+control.getCodGuia()+">>");
    		log.info("control.getCodBien()>>"+control.getCodBien()+">>");
    		System.out.println("CantidadEntregada1: "+control.getCantidadEntregada());
    		if(control.getCantidadEntregada()!=null)
    		  if(control.getCantidadEntregada().equals(".00")) control.setCantidadEntregada("0");
    		System.out.println("CantidadEntregada2: "+control.getCantidadEntregada());
    		control = irConsultar(control);

    		
    	}
    	
        log.info("formBackingObject:FIN");
        return control;
    }
	
	private void setArrayCompletaVacios(List lst,int numReg) {

		if( lst==null){
			lst = new ArrayList();
		}
		
		ResponsableByActivoBean obj = null;		
		log.info("NUM REG ACTUALES "+lst.size()+" REG");
		log.info("SE ADICIONARAN "+numReg+" REG");
		
		if(numReg>0){			
			for(int i=0;i<numReg;i++){
				 obj = new ResponsableByActivoBean();
				 obj.setFlgBD("0");//NO VIENE DE BD				
				 lst.add(obj);
			}
		}
		//return lst;
	}

	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {

    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand control = (AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand)command;
    	
    	
    	if("irRegistrar".equalsIgnoreCase(control.getOperacion())){

    		
    		log.info("control.getCodGuia()|"+control.getCodGuia()+"|");
    		log.info("control.getCodBien()|"+control.getCodBien()+"|");
    		log.info("txhCadCodBienDet>"+request.getParameter("txhCadCodBienDet")+">");
    		log.info("txhCadCodUsuResp>"+request.getParameter("txhCadCodUsuResp")+">");
    		log.info("txhCadCodDescUbicacion>"+request.getParameter("txhCadCodDescUbicacion")+">");
    		log.info("txhCantidad>"+request.getParameter("txhCantidad")+">");
    		log.info("control.getUsuario()|"+control.getUsuario()+"|");
    		
    		String resp = atencionRequerimientosManager.InsertRespByActivo(
    				control.getCodGuia(),
    				control.getCodBien(),
    				request.getParameter("txhCadCodBienDet"),
    				request.getParameter("txhCadCodUsuResp"),
    				request.getParameter("txhCadCodDescUbicacion"),
    				request.getParameter("txhCantidad"),
    				control.getUsuario()
    		);
    		
    		log.info("resp>>"+resp+">>");
    		
    		if(resp.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
    		
    		control = irConsultar(control);

    	
    	}else if("irConsultar".equalsIgnoreCase(control.getOperacion())){
    		control = irConsultar(control);
    	}else if("irEliminar".equalsIgnoreCase(control.getOperacion())){
    		
    		log.info("PARAMEROS PARA EL SP SP_DEL_RESP_X_ACTIVO");
    		log.info("control.getCodGuia()|"+control.getCodGuia()+"|");
    		log.info("control.getCodBien()|"+control.getCodBien()+"|");
    		log.info("txhCodBienSel>"+request.getParameter("txhCodBienSel")+">");
    		log.info("control.getUsuario()|"+control.getUsuario()+"|");
    		
    		String resp = atencionRequerimientosManager.deleteRespByActivo(
    				control.getCodGuia(),
    				control.getCodBien(),
    				request.getParameter("txhCodBienSel"),//BIEN SELECCIONADO
    				control.getUsuario()
    		);
    		
    		log.info("resp>>"+resp+">>");
    		
    		if(resp.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.ELIMINAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);
    		
    		control = irConsultar(control);    		
    	}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/Atencion_Requerimiento_Bienes_Guia_Salida_Reg_Responsable","control",control);
    }

	private AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand irConsultar(
			AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand control){
		
		List lstResultado = cotBienesYServiciosManager.getAllRespByActivo(control.getCodGuia(),control.getCodBien());
		log.info("TAMMM >>"+(lstResultado==null?"0.":lstResultado.size())+">>");
        try{
		int numSP = Integer.parseInt(lstResultado==null?"0":""+lstResultado.size());
		int numPagina=  Integer.valueOf(control.getCantidadEntregada()==null?"0":control.getCantidadEntregada());
		
		log.info("numSP>"+numSP+">");
		log.info("numPagina>>"+numPagina+">>");    		
		
		if( numSP == 0 || lstResultado==null ){
			log.info("CASO 1");
			//MOSTRAR NUM PAGINA    			
			setArrayCompletaVacios(lstResultado,numPagina);
			
		}else if( numPagina<numSP ){
			log.info("CASO 2");
			//NO MOSTRAR NADA    
			lstResultado = null;
		}else if( numPagina==numSP ){
			log.info("CASO 3");
			//MOSTRAR SP    			
		}else if( numPagina>numSP ){
			log.info("CASO 4");
			//MOSTRAR SP +
			//LA DIFERENCIA
			setArrayCompletaVacios(lstResultado,numPagina-numSP);
		}
        }catch (Exception e) {
			
		}
		
		
		log.info("tam list >>"+(lstResultado==null?"0.":lstResultado.size())+">>");
		
		//request.setAttribute("numCantEntregada", request.getParameter("prmCantidadEntregada"));
		
		log.info("TAM LISTA FINAL >>"+(lstResultado==null?"0.":lstResultado.size())+">>");
		
		if(lstResultado!=null)
		{control.setLstRespuesta(lstResultado);
		 control.setTamaņoLista(String.valueOf(lstResultado.size()));
		}
		
		return control;
	}    
    
     
   
}
