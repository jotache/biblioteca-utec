package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoEscalaCalifCommand;

public class MantenimientoEscalaCalifFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaCatalogoFormController.class);
    //private ConfInicioManager confInicioManager;
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public MantenimientoEscalaCalifFormController() {    	
        super();        
        setCommandClass(MantenimientoEscalaCalifCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoEscalaCalifCommand command = new MantenimientoEscalaCalifCommand();
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		
    	MantenimientoEscalaCalifCommand control = (MantenimientoEscalaCalifCommand) command;
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Escala_Calif","control",control);		
    }
}
