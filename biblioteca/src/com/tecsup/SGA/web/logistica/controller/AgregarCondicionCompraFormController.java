package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarCondicionCompraCommand;

public class AgregarCondicionCompraFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarCondicionCompraFormController.class);
	  DetalleManager detalleManager;
	  CotBienesYServiciosManager cotBienesYServiciosManager;
	  
	    public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public AgregarCondicionCompraFormController() {    	
	        super();        
	        setCommandClass(AgregarCondicionCompraCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	AgregarCondicionCompraCommand 
	    	command = new AgregarCondicionCompraCommand();	    	
	    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
	    	command.setCodId(request.getParameter("txhCodCotizacion") == null ? "": request.getParameter("txhCodCotizacion"));
	    	llenarData(command);
	    	System.out.println("codAlu: "+command.getCodUsuario());
	    	System.out.println("codCot: "+command.getCodId());
	    	log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	String resul = "";
	    	AgregarCondicionCompraCommand control = (AgregarCondicionCompraCommand) command;
	    	if("GRABAR".equals(control.getOperacion())){
	    	resul = elimina(control);
	    		System.out.println("resul: "+resul);
	    		resultado = grabar(control);
	    		if ( resultado.equals("-1")) {
	    			control.setMsg("ERROR");
	    		}
	    		else {
	    			control.setMsg("OK");
	    		}
	    		llenarData(control);
	    		System.out.println("resul: "+resultado);
	    		System.out.println("men: "+control.getMsg());
	    	}
	    	
			log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/CotBienesYServicios/agregarCondicionCompra","control",control);		
	    }
			public void setCotBienesYServiciosManager(
					CotBienesYServiciosManager cotBienesYServiciosManager) {
				this.cotBienesYServiciosManager = cotBienesYServiciosManager;
			}
			private void llenarData(AgregarCondicionCompraCommand control){
				control.setListcondiciones(this.cotBienesYServiciosManager.GetAllCondicionCotizacion(control.getCodId()));
			control.setTot(control.getListcondiciones().size());
			}
			private String grabar(AgregarCondicionCompraCommand control){
				int i = 0;
				control.setGraba("");
				System.out.println("entro a grabar");
				StringTokenizer stkDescripcion = new StringTokenizer(control.getCodDescripcion(),"|");
				StringTokenizer stkIndicador = new StringTokenizer(control.getIndicador(),"|");
				StringTokenizer stkDesTipo = new StringTokenizer(control.getDesTipo(),"|");
				StringTokenizer stkCodCotizacion = new StringTokenizer(control.getCodCotizacion(),"|");
	    		String codDescripcion;
	    		String codIndicador;
	    		String codDesTipo;
	    		String codCotizacion;
	    		while ( stkIndicador.hasMoreTokens() || stkDescripcion.hasMoreTokens() || stkDesTipo.hasMoreTokens() || stkCodCotizacion.hasMoreTokens())
	    		{	
	    			System.out.println("I: "+i);
	    			System.out.println("grabando....");
	    			codDescripcion = stkDescripcion.nextToken();
	    			codIndicador = stkIndicador.nextToken();
	    			codDesTipo = stkDesTipo.nextToken();
	    			codCotizacion = stkCodCotizacion.nextToken();
	    			System.out.println("coddes   : "+codDescripcion);
	    			System.out.println("codindic : "+codIndicador);
	    			System.out.println("codDestip: "+codDesTipo);
	    			System.out.println("codCotiza: "+codCotizacion);
	    			System.out.println("codId: "+control.getCodId());
	    			System.out.println("--------------------------");
	    			if(codCotizacion.equals("-1")){
	    				codCotizacion="";
	    			}
	    			control.setGraba(this.cotBienesYServiciosManager.InsertCondicionCotizacion(codCotizacion
	    					,control.getCodId(), codDescripcion, codDesTipo, codIndicador, control.getCodUsuario()));
	    			i++;
	    		}
	    		System.out.println(control.getGraba());
	    		return control.getGraba();
	    		
			}
			
			private String elimina(AgregarCondicionCompraCommand control){
			
				control.setEliminar(this.cotBienesYServiciosManager.DeleteCondicionCotizacion(control.getCodId()
						, control.getCodUsuario()));
				
				return control.getEliminar();
			}
	        
}
