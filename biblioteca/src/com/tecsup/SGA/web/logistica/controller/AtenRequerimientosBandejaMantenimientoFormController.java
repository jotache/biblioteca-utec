package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.AtenRequerimientoBandejaBienesServiciosCommand;
import com.tecsup.SGA.web.logistica.command.AtenRequerimientosBandejaMantenimientoCommand;


public class AtenRequerimientosBandejaMantenimientoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AtenRequerimientosBandejaMantenimientoFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	AtencionRequerimientosManager atencionRequerimientosManager;
	
	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	AtenRequerimientosBandejaMantenimientoCommand command= new AtenRequerimientosBandejaMantenimientoCommand();    	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    	}
    	command.setCodSede(request.getParameter("txhCodSede"));
    	
    	if(command.getCodUsuario()!=null){
    		if(!command.getCodUsuario().equals(""))
    			cargaCabecera(command);
    	}
    	
    	BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean!= null)
         {  log.info("BandejaLogisticaBean:INICIO");
            if(bandejaBean.getCodBandeja().equals("11")){
            	     	 	
            	command.setNroRequerimiento(bandejaBean.getValor1());
            	command.setCodServicio(bandejaBean.getValor2());
            	command.setCodCentroCosto(bandejaBean.getValor3());
            	command.setUsuSolicitante(bandejaBean.getValor4());
            	command.setCodEstado(bandejaBean.getValor5());
            	cargaBandeja(command, request);
            }
       	  log.info("BandejaLogisticaBean:FIN");
         }
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	AtenRequerimientosBandejaMantenimientoCommand control= (AtenRequerimientosBandejaMantenimientoCommand) command;
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("BUSCAR")){
    		cargaCabecera(control);
    		cargaBandeja(control, request);
    	}
    	else
		if (control.getOperacion().trim().equals("REGISTRAR_CONFORMIDAD")){			
    		resultado = RegistrarConformidad(control);
    		
    		if (resultado.equals("0")){    			
    			control.setMsg("OK_REGISTRAR_CONFORMIDAD");
    		}
    		else
    		if(resultado.equals("-2")){
    			control.setMsg("ERROR_GUIA");	
    		}
    		else{ if(resultado.equals("-3")){
				 control.setMsg("ERROR_CANT_EXCEDE");	
				}
			  else if(resultado.equals("-4")){
				 	control.setMsg("ERROR_CANT_ENTREGADA");	
						}
			  	  else{
    			    control.setMsg("ERROR_REGISTRAR_CONFORMIDAD");
    		}    		
    	  }
		}
     	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/AtenRequerimientosBandejaMantenimiento","control",control);
    }
    private void cargaCabecera(AtenRequerimientosBandejaMantenimientoCommand control){    	
    	List lista=this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),"","");
    	
    	if(lista!=null && lista.size()>0){    	
    		control.setListaCentroCosto(lista);
    	}
    	
    	List listaServicio=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
    			, "", "", CommonConstants.SOL_AREQ_GRUPOS_MANTENIMIENTO, "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);   	
    	
    	if(listaServicio!=null && listaServicio.size()>0){
    		control.setListaServicio(listaServicio);
    	}
    	//ALQD,17/04/09. SETEANDO NUEVA PROPIEDAD PARA FILTRO POR ESTADO
    	List listaEstado=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(listaEstado!=null && listaEstado.size()>0){
    		control.setListaEstado(listaEstado);
    	}
    }
    
    private void cargaBandeja(AtenRequerimientosBandejaMantenimientoCommand control, HttpServletRequest request){
    	SolRequerimiento obj = new SolRequerimiento();
    	
    	GuardarDatosBandeja(control, request);
    	
    	obj.setTipoRequerimiento("");
    	obj.setSubTipoRequerimiento(control.getCodServicio());
    	obj.setNroRequerimiento(control.getNroRequerimiento());
    	obj.setCecoSolicitante(control.getCodCentroCosto());
    	obj.setUsuSolicitante(control.getUsuSolicitante());
    	obj.setCodEstado(control.getCodEstado());
    	
    	List lista=this.atencionRequerimientosManager.GetAllBandejaAtencionReqMan(obj, control.getCodSede()
    			,control.getCodUsuario(), control.getCodPerfil(), control.getCodEstado());
    	
    	if(lista!=null && lista.size()>0){
    		control.setListaRequerimientos(lista);
    		control.setTamListaRequerimientos(""+lista.size());    		
    	}
    	else{
    		control.setListaRequerimientos(null);
    		control.setTamListaRequerimientos("0");
    	}
    }
    private String RegistrarConformidad(AtenRequerimientosBandejaMantenimientoCommand control){
    	try{
    		System.out.println("codRequerimiento>>"+control.getCodRequerimiento()+"<<");    		
    		System.out.println("codUsuario>>"+control.getCodUsuario()+"<<");
    		String resultado=""; 
    		resultado=this.atencionRequerimientosManager.RegistrarConformidadMantenimiento(control.getCodRequerimiento(), control.getCodUsuario());    		
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }

    public void GuardarDatosBandeja(AtenRequerimientosBandejaMantenimientoCommand control,HttpServletRequest request)
	  {
	 	BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	 	
	 	bandejaBean.setCodBandeja("11");
	 	
	 	bandejaBean.setValor1(control.getNroRequerimiento());
	 	bandejaBean.setValor2(control.getCodServicio());
	 	bandejaBean.setValor3(control.getCodCentroCosto());
	 	bandejaBean.setValor4(control.getUsuSolicitante());
	 	bandejaBean.setValor5(control.getCodEstado());
	 		 		 	 
	 	 log.info("GuardarDatosBandeja");
	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	  }
    
    
	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
}
