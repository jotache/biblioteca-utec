package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CatalogoProducto;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarCondCompraCommand;

public class AgregarCondCompraFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarCondCompraFormController.class);
  DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public AgregarCondCompraFormController() {    	
        super();        
        setCommandClass(AgregarCondCompraCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarCondCompraCommand command = new AgregarCondCompraCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setSecuencial((String)request.getParameter("txhSecuencial"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setConComp((String)request.getParameter("txhDesc"));
    	if("MODIFICAR".equals(command.getTipo())){
    	TipoLogistica obj = new TipoLogistica();
    	obj.setCodTabla(CommonConstants.TIPT_CONDICICONES_COMPRA);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	obj.setCodigo(command.getSecuencial());
    	List lst  = this.detalleManager.GetAllTablaDetalle(obj);
    	
    	if(lst!=null ){
    		if ( lst.size()>0 )
    		{
    			Logistica logistica = null;
    			logistica = (Logistica)lst.get(0); 
    			
    			command.setTextoDefecto(logistica.getCodPadre());
    		}
    	}
    	}	
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado="";
    	AgregarCondCompraCommand control = (AgregarCondCompraCommand) command;
    	
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
    			resultado = modifica(control);
    			if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
    			else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
            	else control.setMsg("OK");
        		}
    	
    		else{	
    		resultado = grabar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if(resultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
    		}
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Cond_Compra","control",control);		
    }
        private String grabar(AgregarCondCompraCommand control){
        	
        	control.setGraba(this.detalleManager.InsertTablaDetalle( control.getTextoDefecto()
        			, CommonConstants.TIPT_CONDICICONES_COMPRA, control.getConComp(),"", "", "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        
        private String modifica(AgregarCondCompraCommand control){
        	control.setGraba(this.detalleManager.UpdateTablaDetalle(control.getTextoDefecto().trim()
        	, CommonConstants.TIPT_CONDICICONES_COMPRA, control.getSecuencial().trim(), control.getSecuencial().trim(), 
        	control.getConComp().trim(), "", "", "", control.getCodAlumno().trim()));
        	
        	return control.getGraba();
        }
        	
        
        
}
