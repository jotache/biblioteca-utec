package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.GestDocAprobarDocumentosPagoCommand;
import com.tecsup.SGA.web.logistica.command.GestDocPagoDocumentosPagoCommand;

public class GestDocAprobarDocumentosPagoFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(GestDocAprobarDocumentosPagoFormController.class);
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	GestionDocumentosManager gestionDocumentosManager;
	
	public GestionDocumentosManager getGestionDocumentosManager() {
		return gestionDocumentosManager;
	}

	public void setGestionDocumentosManager(
			GestionDocumentosManager gestionDocumentosManager) {
		this.gestionDocumentosManager = gestionDocumentosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	GestDocAprobarDocumentosPagoCommand command= new GestDocAprobarDocumentosPagoCommand();
    	
    	command.setCodSede(request.getParameter("txhCodSede"));
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    	}
    	
    	List lista2= new ArrayList();
    	lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ESTADO_LOG 
     			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
         if(lista2!=null)
     	command.setListaCodEstado(lista2);
        
        BandejaLogisticaBean bandejaBean1 = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean1!= null)
         {  log.info("BandejaLogisticaBean:INICIO");
            if(bandejaBean1.getCodBandeja().equals("7")){
	         command.setNroFactura(bandejaBean1.getValor1());
	         command.setFecInicio1(bandejaBean1.getValor2());
	         command.setFecInicio2(bandejaBean1.getValor3());
	         command.setNroRucProveedor(bandejaBean1.getValor4());
	         command.setNombreProveedor(bandejaBean1.getValor5());
	         command.setCodEstado(bandejaBean1.getValor6());
	         command.setNroOrden(bandejaBean1.getValor7());
	         command.setFecFinal1(bandejaBean1.getValor8());
	         command.setFecFinal2(bandejaBean1.getValor9());
	                	 
	       	 BuscarBandeja(command, request);
            }
       	  log.info("BandejaLogisticaBean:FIN");
         }
         else command.setBanListaBandeja("0"); 
        
    	log.info("formBackingObject:FIN");
    	 
    	 	return command;
    }
    		
    	
	protected void initBinder(HttpServletRequest request,
    	          ServletRequestDataBinder binder) {
    	  	NumberFormat nf = NumberFormat.getNumberInstance();
    	  	binder.registerCustomEditor(Long.class,
    		                  new CustomNumberEditor(Long.class, nf, true));
    	  	
    	  }
    	  /**
    	   * Redirect to the successView when the cancel button has been pressed.
    	   */
    	  public ModelAndView processFormSubmission(HttpServletRequest request,
    	                                            HttpServletResponse response,
    	                                            Object command,
    	                                            BindException errors)
    	  throws Exception {
    	      //this.onSubmit(request, response, command, errors);
    	  	return super.processFormSubmission(request, response, command, errors);
    	  }
    		
    	  public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
    	          BindException errors)
    	  		throws Exception {
    	  	log.info("onSubmit:INI");
    	  	
    	  	GestDocAprobarDocumentosPagoCommand control= (GestDocAprobarDocumentosPagoCommand) command;
    	  	control.setBanListaBandeja("");
    	  	
    	  	if(control.getOperacion().equals("BUSCAR"))
    	  	{
    	  		BuscarBandeja(control, request);
    	  	}	
    	  	
    	  	log.info("onSubmit:FIN");		
    	    return new ModelAndView("/logistica/GestionDocumentos/Gest_Doc_Aprobar_Documentos_Pago","control",control);
    	}

    	public void BuscarBandeja(GestDocAprobarDocumentosPagoCommand control, HttpServletRequest request){
    		List lista = new ArrayList();
    		
    		GuardarDatosBandeja(control, request);
    		
    		if(control.getCodEstado().equals("-1"))
    			control.setCodEstado("");
    		System.out.println(">>NroFactura<<"+control.getNroFactura()+">>FecInicio1<<"+
    				control.getFecInicio1()+">>FecInicio2<<"+control.getFecInicio2()+">>NroRucProveedor<<"+control.getNroRucProveedor()
    				+">>NombreProveedor<<"+control.getNombreProveedor()+">>CodEstado<<"+control.getCodEstado()+">>NroOrden<<"+control.getNroOrden()
    				+">>FecFinal1<<"+control.getFecFinal1()+">>ecFinal2<<"+control.getFecFinal2()+">>codPerfil<<"+control.getCodPerfil());
    		lista=this.gestionDocumentosManager.GetAllAprobarDocumentosPago(control.getNroFactura(),
    				control.getFecInicio1(), control.getFecInicio2(), control.getNroRucProveedor(),
    				control.getNombreProveedor(), control.getCodEstado(), control.getNroOrden(),
    				control.getFecFinal1(), control.getFecFinal2(),control.getCodPerfil(),
    				control.getCodSede(), control.getCodUsuario());
    		if(lista!=null)
    			{ control.setListaBandeja(lista);
    			  if(lista.size()==0)
    				   control.setBanListaBandeja("0");
    			}
    		else{ control.setBanListaBandeja("0");
    			  control.setListaBandeja(new ArrayList());
    		}
    		System.out.println("Size:D :"+control.getListaBandeja().size());
    	}  
    	 
    	public void GuardarDatosBandeja(GestDocAprobarDocumentosPagoCommand control,HttpServletRequest request)
    	  {
    	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
    	 	 
    	 	 bandejaBean.setCodBandeja("7");
    	 	 
    	 	 bandejaBean.setValor1(control.getNroFactura());
    	 	 bandejaBean.setValor2(control.getFecInicio1());
    	 	 bandejaBean.setValor3(control.getFecInicio2());
    	 	 bandejaBean.setValor4(control.getNroRucProveedor());
    	 	 bandejaBean.setValor5(control.getNombreProveedor());
    	 	 bandejaBean.setValor6(control.getCodEstado());
    	 	 bandejaBean.setValor7(control.getNroOrden());
    	 	 bandejaBean.setValor8(control.getFecFinal1());
    	 	 bandejaBean.setValor9(control.getFecFinal2());
    	 	 
    	 	 log.info("GuardarDatosBandeja");
    	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
    	  }
    	
    	
		public TablaDetalleManager getTablaDetalleManager() {
			return tablaDetalleManager;
		}

		public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
			this.tablaDetalleManager = tablaDetalleManager;
		}

		public SeguridadManager getSeguridadManager() {
			return seguridadManager;
		}

		public void setSeguridadManager(SeguridadManager seguridadManager) {
			this.seguridadManager = seguridadManager;
		}

		public DetalleManager getDetalleManager() {
			return detalleManager;
		}

		public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
}
