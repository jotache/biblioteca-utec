package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.logistica.command.CotDetBandejaProveedoresCommand;

public class CotDetInsertarProveedoresFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotDetInsertarProveedoresFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public CotDetInsertarProveedoresFormController() {    	
        super();        
        setCommandClass(CotDetBandejaProveedoresCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	CotDetBandejaProveedoresCommand command = new CotDetBandejaProveedoresCommand();
    	
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion"));
    	command.setCodCotizacionDet(request.getParameter("txhCodDetCotizacion"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	if(command.getCodUsuario()!=null)
    	{ 
    		command.setListaProveedor(
    				this.cotBienesYServiciosManager.getAllProveedores(command.getRuc(), command.getRazonSocial() )
    				);
    		request.getSession().setAttribute("listaProveedores", command.getListaProveedor());
    	}
    	
    	request.getSession().setAttribute("listaProveedores", command.getListaProveedor());
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	CotDetBandejaProveedoresCommand control = (CotDetBandejaProveedoresCommand) command;
		String rpt = "";
		if (control.getOperacion().trim().equals("GRABAR"))
		{
			rpt = this.cotBienesYServiciosManager.insertProveedorByItem(control.getCodCotizacion()
					, control.getCodCotizacionDet(), control.getCodProveedor(), control.getCodUsuario());
			if ( rpt == null ) control.setMsg("ERROR"); 
			else if ( rpt.trim().equals("-1")) control.setMsg("ERROR");
			else if ( rpt.trim().equals("-2")) control.setMsg("ERROR1");
			else if ( rpt.trim().equals("0")) control.setMsg("OK");
			
			control.setListaProveedor(
					this.cotBienesYServiciosManager.getAllProveedores(control.getRuc(), control.getRazonSocial() )
					);
		}
		else if (control.getOperacion().trim().equals("BUSCAR"))
		{
			control.setListaProveedor(
				this.cotBienesYServiciosManager.getAllProveedores(control.getRuc(), control.getRazonSocial() )
				);
		}
		
		control.setOperacion("");
		control.setCodProveedor("");
		
    	request.getSession().setAttribute("listaProveedores", control.getListaProveedor());
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/CotBienesYServicios/CotDet_InsertProveedores","control",control);		
    }
}
