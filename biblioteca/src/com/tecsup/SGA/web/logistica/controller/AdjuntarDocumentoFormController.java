package com.tecsup.SGA.web.logistica.controller;

import java.io.File;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.MetodosConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AdjuntarDocumentoCommand;

public class AdjuntarDocumentoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AdjuntarDocumentoFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public AdjuntarDocumentoFormController() {    	
        super();        
        setCommandClass(AdjuntarDocumentoCommand.class);        
    }
	MetodosConstants metodo= new MetodosConstants();

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AdjuntarDocumentoCommand command = new AdjuntarDocumentoCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setCodPro((String)request.getParameter("txhCodProducto"));
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_ADJUNTOS);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	command.setListTipo(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listTipo", command.getListTipo());
    
     	request.setAttribute("strExtensionDOC", CommonConstants.GSTR_EXTENSION_DOC);
     	request.setAttribute("strExtensionDOCX", CommonConstants.GSTR_EXTENSION_DOCX);
    	request.setAttribute("strExtensionXMS", CommonConstants.GSTR_EXTENSION_XLS);
    	request.setAttribute("strExtensionXLSX", CommonConstants.GSTR_EXTENSION_XLSX);
    	request.setAttribute("strExtensionPDF", CommonConstants.GSTR_EXTENSION_PDF);
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
		System.out.println("iniciando......");
    	AdjuntarDocumentoCommand control = (AdjuntarDocumentoCommand) command;
    	if("GRABAR".equals(control.getOperacion())){
    		System.out.println("grabando");
    		byte[] bytesCV = control.getTxtCV();
    		String filenameCV;
    		System.out.println("tama�o archivo: "+bytesCV.length);
    		if(bytesCV.length>4194304)//4MB
    		{
    			System.out.println("El tama�o del archivo es muy grande!");
    			control.setMessage("ERROR_TAMANIO");
    		}else{
    		
    		 String sep = System.getProperty("file.separator");
    		//*************************************************************
// 			String uploadDirCV = getServletContext().getRealPath("/"+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_DOCUMENTO_02);
 			String uploadDirCV = CommonConstants.DIRECTORIO_DATA+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_DOCUMENTO_02;
 			File dirPath = new File(uploadDirCV);
 			if (!dirPath.exists()) {
 				dirPath.mkdirs();
 			}			
 			//*************************************************************
            	 filenameCV = metodo.GenerarNombreArchivoAdjuntar(CommonConstants.FILE_NAME_PRODUCTO, 
	        			 control.getExtCv());
            	 System.out.println("Nombre ArchivO: "+filenameCV);
            	 File uploadedFileCV = new File(uploadDirCV + sep + filenameCV);
  	        	 FileCopyUtils.copy(bytesCV, uploadedFileCV);
  	        	 control.setGraba(this.detalleManager.InsertDocProducto(control.getCodPro(), control.getCodtipo(), filenameCV, control.getCodAlumno()));
  	        	 System.out.println("resul: "+control.getGraba());
  	        	 resultado = control.getGraba();
  	       	if ( resultado.equals("-1")) {
    			control.setMessage("ERROR");
    		}
  	      else control.setMessage("OK");
    	
    		}
    	
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Adjuntar_Documento","control",control);		
    }
}
