package com.tecsup.SGA.web.logistica.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.logistica.command.CotSolVerCotizacionFormController;

public class CotSolBienesServiciosSolicitudCotizacionVerAutorizacionFormController
		extends SimpleFormController {
	private static Log log = LogFactory.getLog(CotSolBienesServiciosSolicitudCotizacionVerAutorizacionFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;
	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}
	
	public CotSolBienesServiciosSolicitudCotizacionVerAutorizacionFormController() {	
		super();
		setCommandClass(CotSolVerCotizacionFormController.class);
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
    	Cotizacion oCotizacion;
    	CotSolVerCotizacionFormController command = new CotSolVerCotizacionFormController();
    	
    	if( request.getParameter("prmCodOrden") != null ){
    		
    		//command.setTxhCodCotizacion(request.getParameter("prmCodOrden"));
    		String codOrden = request.getParameter("prmCodOrden");
    		log.info("codOrden():"+codOrden);
    		//command.setUsuario(request.getParameter("prmUsuario"));
    		//command.setTxhCodSede(request.getParameter("prmCodSede"));
    		//command.setTxhCodEstCotizacion(request.getParameter("prmCodEstCotizacion"));
    		command.setCodTipoReq(request.getParameter("txhCodTipoReq"));    		    		
    		List lstResultado = cotBienesYServiciosManager.getLstDetalleAprobacion(codOrden);
    		getDatosCotizacion(command);
            command.setLstResultado(lstResultado);            
            
    	}
    	
        return command;
	}

	private void getDatosCotizacion(CotSolVerCotizacionFormController command)
	{
		List lstCotizacion = cotBienesYServiciosManager.GetAllCotizacionById(command.getTxhCodCotizacion());
		Cotizacion oCotizacion;
		if ( lstCotizacion != null)
		{
			if ( lstCotizacion.size() > 0)
			{
				oCotizacion = (Cotizacion)lstCotizacion.get(0); 
				
				command.setFecFin(oCotizacion.getFechaFin());
				command.setFecInicio(oCotizacion.getFechaInicio());
				command.setHoraFin(oCotizacion.getHoraFin());
				command.setHoraInicio(oCotizacion.getHoraInicio());
			}
		}
	}	
}
