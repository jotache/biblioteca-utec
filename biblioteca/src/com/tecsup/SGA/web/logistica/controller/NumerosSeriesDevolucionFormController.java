package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.NumeroSeries;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.web.logistica.command.NumerosSeriesDevolucionCommand;

public class NumerosSeriesDevolucionFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(NumerosSeriesDevolucionFormController.class);
	  DetalleManager detalleManager;
	  AlmacenManager almacenManager;
	  AtencionRequerimientosManager atencionRequerimientosManager;
	    
	    public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public NumerosSeriesDevolucionFormController() {    	
	        super();        
	        setCommandClass(NumerosSeriesDevolucionCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	NumerosSeriesDevolucionCommand command = new NumerosSeriesDevolucionCommand();
	    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
	        command.setCodBien((String)request.getParameter("txhCodBien"));
	        command.setCodReq((String)request.getParameter("txhCodReq"));
	        command.setCodReqDet((String)request.getParameter("txhCodReqDet"));
	        command.setCodDev((String)request.getParameter("txhCodDev"));
	        command.setCodDevDet((String)request.getParameter("txhCodDevDet"));
	        command.setTipo((String)request.getParameter("txhTipoProcedencia"));
	        llenar(command);
	        
	        System.out.println("codUsu: "+command.getCodUsuario());
	        System.out.println("codBien: "+command.getCodBien());
	        System.out.println("CodReq: "+command.getCodReq());
	        System.out.println("CodDevDet: "+command.getCodDevDet());
	        System.out.println("CodDev: "+command.getCodDev());
	        System.out.println("CodDevDet: "+command.getCodDevDet());
	        System.out.println("tipo: "+command.getTipo());    
	     
	            	 log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	NumerosSeriesDevolucionCommand control = (NumerosSeriesDevolucionCommand) command;
	    	if(control.getOperacion().equals("GRABAR")){
	    		resultado = grabar(control);
	    		if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
	    		else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
	    		else control.setMsg("OK");
	    	}
	    	llenar(control);
	    	
	    	
	  		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/SolAproRequerimientos/log_Numeros_Series_Devolucion","control",control);		
	    }
	        
	        private void llenar(NumerosSeriesDevolucionCommand control){
	        	control.setListaNumero(this.atencionRequerimientosManager.GetAllNumerosSeries(control.getCodReq()
	        			, control.getCodReqDet(), control.getCodDev(), control.getCodDevDet()));
	       
	        	 if(control.getListaNumero()== null){
		    		   control.setListaNumero(new ArrayList());
		    		   control.setBanListaBandeja("0");
		    	   }	
	        	System.out.println("totList: "+control.getListaNumero().size());
	        	String cadena="";
	        	int can =0;
	        	if(control.getListaNumero()!=null)
	        	{ if(control.getListaNumero().size()>0)
	        		{ NumeroSeries numeroSerie=new NumeroSeries();
	        		  for(int a=0;a<control.getListaNumero().size();a++)
	        		  { numeroSerie=(NumeroSeries)control.getListaNumero().get(a);
	        		    if(numeroSerie.getIndSel().equals("1"))
	        		    	{ cadena= cadena+numeroSerie.getCodIngreso()+"|";
	        		    	can = can+1;  
	        		    	}
	        		  }
	        		  control.setCodCadIngresados(cadena);
	        		  control.setCantidad(String.valueOf(can));
	        		  control.setBanListaBandeja("1");
	        		}
	        	  else control.setBanListaBandeja("0");
	        	}
	        	control.setTotal(control.getListaNumero().size());
	        	System.out.println("cadena: "+control.getCodCadIngresados());
	        	System.out.println("cantidad: "+control.getCantidad());
	        }
	        
	        private String grabar(NumerosSeriesDevolucionCommand control){
	        	System.out.println("getCodDev: "+control.getCodDev());
	        	System.out.println("getCodDevDet: "+control.getCodDevDet());
	        	System.out.println("getCodBien: "+control.getCodBien());
	        	System.out.println("getCodCadIngresados: "+control.getCodCadIngresados());
	        	System.out.println("getCantidad: "+control.getCantidad());
	        	System.out.println("getCodUsuario: "+control.getCodUsuario());
	        	control.setGrabar(this.atencionRequerimientosManager.InsertNumerosSeries(control.getCodDev()
	        			, control.getCodDevDet(), control.getCodBien(), control.getCodCadIngresados()
	        			, control.getCantidad(), control.getCodUsuario()));
	        	
	        	return control.getGrabar();
	        }
	        
			public void setAlmacenManager(AlmacenManager almacenManager) {
				this.almacenManager = almacenManager;
			}
			public void setAtencionRequerimientosManager(
					AtencionRequerimientosManager atencionRequerimientosManager) {
				this.atencionRequerimientosManager = atencionRequerimientosManager;
			}
}
