package com.tecsup.SGA.web.logistica.controller;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.web.logistica.command.ActualizarInventarioFisicoCommand;

public class ActualizarInventarioFisicoFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(ActualizarInventarioFisicoFormController.class);
	AlmacenManager almacenManager;
	 
    public ActualizarInventarioFisicoFormController() {    	
        super();        
        setCommandClass(ActualizarInventarioFisicoCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ActualizarInventarioFisicoCommand command = new ActualizarInventarioFisicoCommand();
    	//**********************************************************
        command.setCodSede((String)request.getParameter("txhCodSede"));
        command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
        System.out.println("codSede>>"+command.getCodSede()+"<<");
        System.out.println("codUsuario>>"+command.getCodSede()+"<<");
        //**********************************************************
        request.setAttribute("strExtensionXLS", CommonConstants.GSTR_EXTENSION_XLS);
        request.setAttribute("strExtensionXLSX", CommonConstants.GSTR_EXTENSION_XLSX);
        //ESTABLECEMOS EL ESTADO DEL ALMACEN LA PRIMERA VEZ
        if(command.getCodSede()!=null && !command.getCodSede().equals(""))
        	setEstadoAlmacen(command);
        //***********************************************************
        log.info("formBackingObject:FIN");
        return command;
    }  
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    	binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
        BindException errors)
		throws Exception {
        	
    	log.info("onSubmit:INI");    	
    	ActualizarInventarioFisicoCommand control = (ActualizarInventarioFisicoCommand) command;
    	String resultado="";
    
    	if("REGISTRAR_INVENTARIO".equals(control.getOperacion())){
    		resultado = registrarInventario(control, request);
    		System.out.println("resultado registrar almacen>>"+resultado+"<<");
    		if ( resultado.equals("0")) {
    			control.setMsg("OK_REGISTRAR_INVENTARIO");
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("ALMACEN_ABIERTO");
    		}
    		else if ( resultado.equals("-3")) {
    			control.setMsg("ACTUALIZACION_REGISTRAR_INVENTARIO");
    		}
  	      	else control.setMsg("ERROR_REGISTRAR_INVENTARIO");
        }	
    	else if("ABRIR_ALMACEN".equals(control.getOperacion())){
    		resultado = abrirAlmacen(control);
    		System.out.println("resultado abrir almacen>>"+resultado+"<<");
    		if ( resultado.equals("0")) {
    			control.setMsg("OK_ABRIR_ALMACEN");
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("ACTUALIZACION_ABRIR_ALMACEN");
    		}
  	      	else control.setMsg("ERROR_ABRIR_ALMACEN");
    	}
    	else if("CERRAR_ALMACEN".equals(control.getOperacion())){
    		resultado = cerrarAlmacen(control);
    		System.out.println("resultado cerrar almacen>>"+resultado+"<<");
    		if ( resultado.equals("0")) {
    			control.setMsg("OK_CERRAR_ALMACEN");    			
    		}
  	      	else control.setMsg("ERROR_CERRAR_ALMACEN");
    	}
    	else if("ENVIAR_INVENTARIO".equals(control.getOperacion())){
    		resultado = enviarInventario(control);    		
    		System.out.println("resultado enviar Inventario>>"+resultado+"<<");
    		if ( resultado.equals("0")) {
    			control.setMsg("OK_ENVIAR_INVENTARIO");    			
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("ACTUALIZACION_ENVIAR_INVENTARIO");
    		}
  	      	else control.setMsg("ERROR_ENVIAR_INVENTARIO");
    	}
    	else if("APROBAR_INVENTARIO".equals(control.getOperacion())){
    		resultado = aprobarInventario(control);
    		System.out.println("resultado aprobar Inventario>>"+resultado+"<<");
    		if ( resultado.equals("0")) {
    			control.setMsg("OK_APROBAR_INVENTARIO");    			
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("ACTUALIZACION_APROBAR_INVENTARIO");
    		}
  	      	else control.setMsg("ERROR_APROBAR_INVENTARIO");
    	}
    	else if("RECHAZAR_INVENTARIO".equals(control.getOperacion())){
    		resultado = rechazarInventario(control);
    		System.out.println("resultado rechazar Inventario>>"+resultado+"<<");
    		if ( resultado.equals("0")) {
    			control.setMsg("OK_RECHAZAR_INVENTARIO");    			
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("ACTUALIZACION_RECHAZAR_INVENTARIO");
    		}
  	      	else control.setMsg("ERROR_RECHAZAR_INVENTARIO");
    	}
    	//ESTABLECEMOS EL ESTADO DEL ALMACEN LUEGO DE HACER CUALQUIER OPERACION
    	setEstadoAlmacen(control);
  		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/Almacen/ActualizarInventarioFisico","control",control);		
    }
    private String abrirAlmacen(ActualizarInventarioFisicoCommand control){
    	String resultado = this.almacenManager.ActAbrirAlmacen(control.getCodSede());
    	return resultado;
    }	    
    private String cerrarAlmacen(ActualizarInventarioFisicoCommand control){
    	String resultado = this.almacenManager.ActCerrarAlmacen(control.getCodSede());
    	return resultado;  
    }
    private String enviarInventario(ActualizarInventarioFisicoCommand control){
    	String resultado = this.almacenManager.actEnviarInventario(control.getCodSede(), control.getCodUsuario());
    	return resultado;  
    }
    private String aprobarInventario(ActualizarInventarioFisicoCommand control){
    	String resultado = this.almacenManager.actAprobarInventario(control.getCodSede(), control.getCodUsuario());
    	return resultado;  
    }
    private String rechazarInventario(ActualizarInventarioFisicoCommand control){
    	String resultado = this.almacenManager.actRechazarInventario(control.getCodSede(), control.getCodUsuario());
    	return resultado;  
    }
    private void setEstadoAlmacen(ActualizarInventarioFisicoCommand control){
    	String estado = this.almacenManager.getEstadoAlmacen(control.getCodSede());
    	if(estado != null && !estado.equals("")){
    		control.setEstadoAlmacen(estado);
    		System.out.println("estado>>"+control.getEstadoAlmacen()+"<<");
    	}    	
    }
    private String registrarInventario(ActualizarInventarioFisicoCommand control, HttpServletRequest request) throws IOException{
		String resultado;
		byte[] bytesCV = control.getTxtCV();
		String filenameCV;
//		String uploadDirCV = getServletContext().getRealPath("/"+CommonConstants.STR_RUTA_REGISTRAR_INVENTARIO);
		String uploadDirCV = CommonConstants.DIRECTORIO_DATA+CommonConstants.STR_RUTA_REGISTRAR_INVENTARIO;
		File dirPath = new File(uploadDirCV);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}		
		String sep = System.getProperty("file.separator");
		
        filenameCV = CommonConstants.FILE_NAME_INVENTARIO + control.getCodSede() + "." +control.getExtCv();
        File uploadedFileCV = new File(uploadDirCV + sep + filenameCV);
        FileCopyUtils.copy(bytesCV, uploadedFileCV);
		//PARA CARGAR LOS DATOS DE LAS CELDAS
        //***********************************
    	Workbook workbook;
		try {
			log.info("cargando..");
			log.info(">>"+uploadedFileCV.getAbsolutePath()+"<<");
			workbook = Workbook.getWorkbook(uploadedFileCV);
			//workbook = Workbook.getWorkbook(new File("C:\\DOCA.xls"));			
			Sheet sheet = workbook.getSheet(0);
			log.info("FILAS>>>>"+sheet.getRows()+">>");
			log.info("COL>>>>"+sheet.getColumns()+">>");				    	
	    	Cell a1 = null;
	    	Cell a2 = null;
	    	int columnaCod=4;
	    	int columnaCan=8;
	    	int filaInicial=8; 
	    	String cadCodigo="";
	    	String cadCantidad="";
	    	String cantidadTemp;
	    	String rg = "";
	    	for(int i=filaInicial;i<sheet.getRows();i++){
	    		a1 = sheet.getCell(columnaCod, i);
	    		a2 = sheet.getCell(columnaCan, i);	    		
	    		rg = (a2.getContents()==null?"":a2.getContents());
	    		if(!(rg.equals(""))){
	    			cadCodigo = cadCodigo + a1.getContents() + "|";
		    		cantidadTemp = a2.getContents().replace(",", ".");
		    		cadCantidad = cadCantidad + cantidadTemp + "|";	
	    		}
	    	}
	    	System.out.println("cod Sede>>"+control.getCodSede());
	    	System.out.println("cadena codigos>>"+cadCodigo);
	    	System.out.println("cadena cantidades>>"+cadCantidad);
	    	System.out.println("cantidad>>"+(sheet.getRows()-8));
	    	System.out.println("cod usuario>>"+control.getCodUsuario());
	    	int total =sheet.getRows()-8;
	    	String cantidad = ""+total;
	    	resultado = this.almacenManager.insertInventario(control.getCodSede(),
	    			cadCodigo, cadCantidad, cantidad, control.getCodUsuario());	    	
	    	workbook.close();
	    	return resultado;
			
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {		
			e.printStackTrace();
		}
    	return null;
    	//***********************************
    }
	public void setAlmacenManager(AlmacenManager almacenManager) {
		this.almacenManager = almacenManager;
	}
}