package com.tecsup.SGA.web.logistica.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.ReporteIngresoAlmacenDevBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.logistica.ReportesLogisticaManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;

public class ReporteLogisticaController implements Controller{
	private static Log log = LogFactory.getLog(ReporteLogisticaController.class);
	
	private ReportesLogisticaManager reportesLogisticaManager;
	private SolRequerimientoManager solRequerimientoManager;
	
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public void setReportesLogisticaManager(
			ReportesLogisticaManager reportesLogisticaManager) {
		this.reportesLogisticaManager = reportesLogisticaManager;
	}

	public ReporteLogisticaController(){
		
	}
    
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception {
    	
    	log.info("onSubmit:INI");

    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if(usuarioSeguridad==null){
    		InicioCommand command = new InicioCommand();
    		return new ModelAndView("/cabeceraLogistica","control",command);
    	}    	
    	
    	ModelMap model = new ModelMap();
    	
    	
    	/** PARAMETROS GENERALES
    	 * */
    	String operacion = request.getParameter("operacion");
    	String fechaRep = Fecha.getFechaActual();
		String horaRep = Fecha.getHoraActual();
		String pagina = "/logistica/consultaReportes/reportes_ingresosAlmacen";
		model.addObject("FECHA_ACTUAL", fechaRep);
		model.addObject("HORA_ACTUAL", horaRep);

    	/** PARAMETROS PARTICULARES
    	 * */
		String txhCodSede = request.getParameter("txhCodSede");
		String txhNomSede = request.getParameter("txhNomSede");
		String txhCodTipo = request.getParameter("txhCodTipo");
		String txhNomTipo = request.getParameter("txhNomTipo");
		String txhCodPeriodo = request.getParameter("txhCodPeriodo");
		String txhNomPeriodo = request.getParameter("txhNomPeriodo");
		String txhCodFamilia = request.getParameter("txhCodFamilia");
		String txhNomFamilia = request.getParameter("txhNomFamilia");
		String txhFecIni = request.getParameter("txhFecIni");
		String txhFecFin = request.getParameter("txhFecFin");
		String txhCodUsuario = request.getParameter("txhCodUsuario");
		String txhDscUsuario = request.getParameter("txhDscUsuario");
		String txhCodCentroCosto = request.getParameter("txhCodCentroCosto");
		String txhCodTipoGasto = request.getParameter("txhCodTipoGasto");
		String txhCodBien = request.getParameter("txhCodBien");
		String txhRadio  = request.getParameter("txhRadio");
		//ALQD,22/09/09.SE AGREGA UN PARAMETRO MAS PARA SALDO > 0
		String txhConSaldo = request.getParameter("txhConSaldo");
		
    	/** PARAMETROS USADOS EN PAGINA
    	 * */
		model.addObject("FECHA_INI", txhFecIni);
		model.addObject("FECHA_FIN", txhFecFin);    		
		model.addObject("COD_SEDE", txhCodSede);
		model.addObject("NOM_SEDE", txhNomSede);
		model.addObject("COD_TIPO", txhCodTipo);
		model.addObject("NOM_TIPO", txhNomTipo);
		model.addObject("COD_PERIODO", txhCodPeriodo);
		model.addObject("NOM_PERIODO", txhNomPeriodo);
		model.addObject("COD_FAMILIA", txhCodFamilia);
		model.addObject("NOM_FAMILIA", txhNomFamilia);
		model.addObject("COD_USUARIO", txhCodUsuario);
		model.addObject("NOM_USUARIO", txhDscUsuario);
		
		log.info("OPERACION>>"+operacion+">>");
		log.info("txhRadio>>"+txhRadio+">>");
		
		log.info("txhFecIni>"+txhFecIni+">");
		log.info("txhFecFin>"+txhFecFin+">");
		log.info("txhDscUsuario>"+txhDscUsuario+">");
		log.info("txhNomSede>"+txhNomSede+">");
		
		if( CommonConstants.REPORTE_LOG_1.equalsIgnoreCase(operacion) && CommonConstants.COD_REP_BIEN_SERVICIO.equalsIgnoreCase(txhRadio) )
    	{
    		List resultado = reportesLogisticaManager.GetAllIngresoAlmacen
    				(
    					txhCodSede,
    					txhCodTipo,
    					txhFecIni,
    					txhFecFin,
    					txhCodFamilia
    				);
    		
    		log.info("T>>"+(resultado==null?"0":resultado.size())+">>");
    		request.setAttribute("LST_RESULTADO", resultado);
    		
    		pagina = "/logistica/consultaReportes/reportes_ingresosAlmacen";
    		
    	}else if( CommonConstants.REPORTE_LOG_1.equalsIgnoreCase(operacion)  && CommonConstants.COD_REP_DEVOLUCIONES.equalsIgnoreCase(txhRadio) ){
    		//"REP_INGRESOS_ALMACEN_DEV"
    		
    		List resultado = reportesLogisticaManager.
    		getAllIngresoAlmacenDev(
    				txhCodSede,
    				txhCodTipo,
    				txhFecIni,
    				txhFecFin,
    				txhCodFamilia);
	
    		log.info("T>>"+(resultado==null?"0":resultado.size())+">>");
			request.setAttribute("LST_RESULTADO", resultado);
			pagina = "/logistica/consultaReportes/reportes_ingresosAlmacen_dev";
			
    		
    	}else if( CommonConstants.REPORTE_LOG_2.equalsIgnoreCase(operacion) ){
    		//"REP_SALIDA_ALMACEN"
    		List resultado = reportesLogisticaManager.GetAllSalidaAlmacen
    				(
    						txhCodSede,
    						txhCodTipo,
    						txhFecIni,
    						txhFecFin,
    						txhCodCentroCosto,
    						txhCodTipoGasto
    				);
    		
	
    		log.info("T>>"+(resultado==null?"0":resultado.size())+">>");
			request.setAttribute("LST_RESULTADO", resultado);
			pagina = "/logistica/consultaReportes/reportes_salidaAlmacen";
			
    	}else if( CommonConstants.REPORTE_LOG_3.equalsIgnoreCase(operacion)){
    		//"REP_KARDEX_VALORIZADO"
    		List resultado = reportesLogisticaManager.
    		GetAllKardezValorizado(
    				txhCodSede,
    				txhCodTipo,
    				txhFecIni,
    				txhFecFin,
    				txhCodFamilia,
    				txhCodBien
    				);


    		log.info("T>>"+(resultado==null?"0":resultado.size())+">>");
			request.setAttribute("LST_RESULTADO", resultado);
			pagina = "/logistica/consultaReportes/reportes_kardexValorizado";
		
    	}else if( CommonConstants.REPORTE_LOG_4.equalsIgnoreCase(operacion)){
    		//"REP_MAESTRO_SALDOS"
    		List resultado = reportesLogisticaManager.GetAllMaestrosSaldos
    			(
    				txhCodSede,
    				txhCodTipo,
    				txhCodPeriodo,
    				txhCodFamilia,
    				txhConSaldo
    			);
	

    		log.info("T>>"+(resultado==null?"0":resultado.size())+">>");
			request.setAttribute("LST_RESULTADO", resultado);
			pagina = "/logistica/consultaReportes/reportes_maestroSaldos";
			
    	}else if( CommonConstants.REPORTE_LOG_011.equalsIgnoreCase(operacion)){
    		List resultado = reportesLogisticaManager.
    		GetAllOrdenesInversion(
    				txhCodSede,    				
    				txhFecIni,
    				txhFecFin   				
    				);

    		log.info("T>>"+(resultado==null?"0":resultado.size())+">>");
			request.setAttribute("LST_RESULTADO", resultado);
			pagina = "/logistica/consultaReportes/reportes_opresupInversion";
    	}
    	
    	
    	log.info(">"+pagina+">");
    	
    	log.info("onSubmit:FIN");
	    return new ModelAndView(pagina,"model",model);
    }
}

