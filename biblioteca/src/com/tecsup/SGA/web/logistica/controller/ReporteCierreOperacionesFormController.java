package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.AreqBandejaEvaluadorCommand;
import com.tecsup.SGA.web.logistica.command.PenBandejaAtencionCotizacionCommand;
import com.tecsup.SGA.web.logistica.command.ReporteCierreOperacionesCommand;

public class ReporteCierreOperacionesFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(ReporteCierreOperacionesFormController.class);
	
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	ReporteCierreOperacionesCommand command= new ReporteCierreOperacionesCommand();
    	
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setCodTipoReporte(request.getParameter("txhCodTipoReporte"));
    	command.setCodSede(request.getParameter("txhCodSede"));
    	command.setDscUsuario(request.getParameter("txhDscUsuario"));
    	command.setConsteSede(request.getParameter("txhCodSede"));
    	command.setListaSedes(this.seguridadManager.getAllSedes());
    	
    	System.out.println("CodUsuario: "+command.getCodUsuario()+">> CodTipoReporte: "+command.getCodTipoReporte());
    	
    	command.setTituloReporte("WilDATilA");
    	command.setNroReporte("1");
    	
    	command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setConsteBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    	command.setConsteServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
    	command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setValorIndGrupo("0");
    	command.setConsteGrupoGenerales(CommonConstants.SOL_AREQ_GRUPOS_GENERALES);
    	command.setConsteGrupoMantenimiento(CommonConstants.SOL_AREQ_GRUPOS_MANTENIMIENTO);
    	command.setConsteGrupoServicios(CommonConstants.SOL_AREQ_GRUPOS_SERVICOS);
    	
    	command.setCodReporte5(CommonConstants.REPORTE_LOG_5);
    	command.setCodReporte7(CommonConstants.REPORTE_LOG_7);
    	command.setCodReporte9(CommonConstants.REPORTE_LOG_9);
    	
    	List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();
    	List lista5= new ArrayList();
    	List lista6= new ArrayList();
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    		command.setListCodTipoRequerimiento(lista1);
    	else command.setListCodTipoRequerimiento(new ArrayList());
    	
    	lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_GASTOS 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista2!=null)
    		command.setListaCodTipoGastos(lista2);
    	else command.setListaCodTipoGastos(new ArrayList());
    	
    	LlenarComboCentroCosto(command);
    	
    	String fecha = Fecha.getFechaActual();
    	String[] arrFecha = fecha.split("/");
    	
    	command.setFecInicio("01/" + arrFecha[1] + "/" + arrFecha[2]);
    	command.setFecFinal(Fecha.getDiasPorMes(arrFecha[1], arrFecha[2]) + "/" + arrFecha[1] + "/" + arrFecha[2]);
    	lista5=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRUPO_SERVICIO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista5!=null)
    	command.setListaCodGrupoServicio(lista5);
    	
    	lista6=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista6!=null)
    		command.setListaCodEstadoReq(lista6);
    	else command.setListaCodEstadoReq(new ArrayList());
    	
    	lista3=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRUPO_SERVICIO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista3!=null)
    	command.setListaCodGrupoServicio(lista3);
    	
    	log.info("formBackingObject:FIN");
         return command;
     }
 	
 	protected void initBinder(HttpServletRequest request,
             ServletRequestDataBinder binder) {
     	NumberFormat nf = NumberFormat.getNumberInstance();
     	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
     	
     }
     /**
      * Redirect to the successView when the cancel button has been pressed.
      */
     public ModelAndView processFormSubmission(HttpServletRequest request,
                                               HttpServletResponse response,
                                               Object command,
                                               BindException errors)
     throws Exception {
         //this.onSubmit(request, response, command, errors);
     	return super.processFormSubmission(request, response, command, errors);
     }
 	
     public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
             BindException errors)
     		throws Exception {
     	log.info("onSubmit:INI");
     	
     	ReporteCierreOperacionesCommand control= (ReporteCierreOperacionesCommand) command;
     	System.out.println("Operacion: "+control.getOperacion());
     	if(control.getOperacion().equals("SEDE"))
     	{
     		LlenarComboCentroCosto(control);
     	}
     	else{ if(control.getOperacion().equals("SERVICIO_RADIO"))
     		  {
     			//ListarComboTipoServicio(control);
     		    //Buscar_GrupoServicio(control, request);
     			LlenarComboCentroCosto(control);
     		  }
     	     
     		else{	if(control.getOperacion().equals("BIEN_RADIO"))
     		  		{
     					//ListarComboTipoServicio(control);
     					//Buscar_GrupoServicio(control, request);
     					LlenarComboCentroCosto(control);
     		  		}
     				else{ if(control.getOperacion().equals("LIMPIAR"))
     		  			{
     						Limpiar(control);
     						//Buscar_GrupoServicio(control, request);
     						//ListarComboTipoServicio(control);
     		  			}
     					else if(control.getOperacion().equals("GRUPOSERVICIO")){
     	     				  //Buscar_GrupoServicio(control, request);
     	     				  LlenarComboCentroCosto(control);
     						}
     				}
     			
     			
     		}
     		
     	}
     	
      	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/consultaReportes/rep_CierreOperaciones","control",control);
    }
    
     public void LlenarComboCentroCosto(ReporteCierreOperacionesCommand control){
    	List lista2=new ArrayList();
    	System.out.println("CodSede: "+control.getCodSede());
    	if(control.getCodSede()!=null)
    	{	lista2=this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),"","");
     		if(lista2!=null)
     			control.setListaCodCentroCosto(lista2);
     		else control.setListaCodCentroCosto(new ArrayList());
     		System.out.println("Size CentroCosto: "+lista2.size());
    	}
     }
     
    /* public void ListarComboTipoServicio(ReporteCierreOperacionesCommand control)
     {List lista= new ArrayList();
      List lista1= new ArrayList();
      List lista2= new ArrayList();
      List lista3= new ArrayList();
      TipoTablaDetalle tipoTablaDetalle1 = new TipoTablaDetalle();
      TipoTablaDetalle tipoTablaDetalle2 = new TipoTablaDetalle();
     if(control.getValorIndGrupo().equals("0"))
     { System.out.println(":D 0");
       lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
     			, "", "", control.getConsteGrupoGenerales(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
       lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
   			, "", "", control.getConsteGrupoServicios(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
       for(int i=0;i<lista1.size();i++)
       {  tipoTablaDetalle1=(TipoTablaDetalle)lista1.get(i);
     	  lista.add(tipoTablaDetalle1);
       }
       for(int k=0;k<lista2.size();k++)
       {  tipoTablaDetalle2=(TipoTablaDetalle)lista2.get(k);
     	  lista.add(tipoTablaDetalle2);
       }
       if(lista!=null)
     	  control.setListaCodGrupoServicio(lista);
       System.out.println(":D  Lista: "+lista.size()+" Lista1: "+lista1.size()+" Lista2: "+lista2.size());
       control.setValorIndGrupo("0");
       
     }
     else{ if(control.getValorIndGrupo().equals("1"))
     		{ System.out.println(":D 1");
     	       lista3=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
         			, "", "", control.getConsteGrupoMantenimiento(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
     	      if(lista3!=null)
     	    	  control.setListaCodGrupoServicio(lista3);
     	      control.setValorIndGrupo("1");
     	      System.out.println(":D Lista "+control.getListaCodGrupoServicio().size());
     	     
     		}
     	
     }
           
     }*/
     
     public void Limpiar(ReporteCierreOperacionesCommand control)
     {
    	List lista2=new ArrayList();
     	System.out.println("CodSede: "+control.getConsteSede());
     	if(control.getConsteSede()!=null)
     	{	lista2=this.seguridadManager.GetAllCentrosCosto(control.getConsteSede(),"","");
      		if(lista2!=null)
      			control.setListaCodCentroCosto(lista2);
      		else control.setListaCodCentroCosto(new ArrayList());
      		System.out.println("Size CentroCosto Limpiar: "+lista2.size());
     	}
    	 
     }
     
     public void Buscar_GrupoServicio(ReporteCierreOperacionesCommand control, HttpServletRequest request){
    	 
    	 System.out.println("Buscar_GrupoServicio :D");
    	
    	 //control.setValRadio(control.getConsteRadio()); 
    	 control.setOperacion("");
    	 if(!control.getCodGrupoServicio().equals("-1"))
      	     ListarComboTipoServicio(control);
    	 else control.setListaCodTipoServicio(new ArrayList());
    	 
     }
     
     public void ListarComboTipoServicio(ReporteCierreOperacionesCommand control){
    	 System.out.println("ListarComboTipoServicio :D");
    	 List lista6=new ArrayList();
    	 System.out.println("CodGrupoServicio: "+control.getCodGrupoServicio());
    	 TipoLogistica obj = new TipoLogistica();
    	 obj.setCodPadre(CommonConstants.TIPT_GRUPO_SERVICIO);
 		 obj.setCodId(control.getCodGrupoServicio());
 		 obj.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
 		 obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	 lista6=this.detalleManager.GetAllTablaDetalle(obj);
    	/* lista6=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
     			, control.getCodGrupoServicio(), "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);*/
     	if(lista6!=null)
     	control.setListaCodTipoServicio(lista6);
    	System.out.println("ListaCodTipoServicio Size: "+control.getListaCodTipoServicio().size());
     }
     
	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}
	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	public DetalleManager getDetalleManager() {
		return detalleManager;
	}
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

}
