package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Archivo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.modelo.CotizacionAdjunto;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.logistica.command.CotDetBandejaDocumentosCommand;

public class CotDetBandejaDocumentoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotDetBandejaDocumentoFormController.class);
	CotBienesYServiciosManager CotBienesYServiciosManager;
    
	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		CotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public CotDetBandejaDocumentoFormController() {    	
        super();        
        setCommandClass(CotDetBandejaDocumentosCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	CotDetBandejaDocumentosCommand command = new CotDetBandejaDocumentosCommand();
    	
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion"));
    	command.setCodCotizacionDet(request.getParameter("txhCodDetCotizacion"));
    	command.setCondicion(request.getParameter("txhCondicion"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setTipoAdjunto(request.getParameter("txhTipoAdjunto"));
    	command.setTipoProcedencia(request.getParameter("txhTipoProcendencia"));
    	
    	log.info("TipoProcednecia: "+command.getTipoProcedencia());
    	/*
    	 * txhTipoProcendencia
    	 * 0 -> Pagina de Cotizacion Actual
    	 * 1 -> Pagina Detalle Proveedor - Adjuntos Cotizacion
    	 * 2 -> Pagina Detalle Proveedor - Adjuntos Proveedor
    	 */
    	if(command.getTipoProcedencia()!=null)
    	{  if(command.getTipoProcedencia().equals("0"))
    		  {command.setDscTipoProcedencia("Documentos Relacionados - Comprador");
    		   command.setDscTipoProcedencia2("Documentos Relacionados");
    		  }
    	  else{ if(command.getTipoProcedencia().equals("1"))
    		    {command.setDscTipoProcedencia("Adjuntos");
    		    command.setDscTipoProcedencia2("Adjuntos");
    		    }
    	  		else{  if(command.getTipoProcedencia().equals("2"))
        		       {command.setDscTipoProcedencia("Adjuntos Proveedor");
        		        command.setDscTipoProcedencia2("Adjuntos Proveedor");
        		       }    	  			   
    	  		}
    	  }
    	}
    	
    	if ( command.getCodCotizacion() != null  && 
    			command.getCodCotizacionDet() != null && 
    			command.getCodUsuario() != null )
    	{
    		if (command.getTipoProcedencia().trim().equals("2"))
    		{
    			command.setListaDocumentosCompItem(
    				this.CotBienesYServiciosManager.getAllDocumentosCompByItem(command.getCodCotizacion(),
    						command.getCodCotizacionDet(), command.getCodUsuario()));
    		}
    		else
    		{
    			command.setListaDocumentosCompItem(
        				this.CotBienesYServiciosManager.getAllDocumentosCompByItem(command.getCodCotizacion(),
        						command.getCodCotizacionDet(), ""));
    		}
    		command.setListaDocumentosItem(
    				this.CotBienesYServiciosManager.getAllDocumentosByItem(command.getCodCotizacion()
    						, command.getCodCotizacionDet()));
    	}
   	
//		request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_PUERTO_WEB + CommonConstants.DIRECTORIO_PROY + CommonConstants.STR_RUTA_DOCUMENTOS_02);
		request.getSession().setAttribute("tipoAdjunto", command.getTipoAdjunto());
    	request.getSession().setAttribute("listaDocumentosComp", command.getListaDocumentosCompItem());
    	request.getSession().setAttribute("listaDocumentos", command.getListaDocumentosItem());
        log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	CotDetBandejaDocumentosCommand control = (CotDetBandejaDocumentosCommand) command;
		String resultado="";
    	
		if (control.getOperacion().trim().equals("ELIMINAR"))
		{   //String codProveedor="";
			if(!control.getTipoProcedencia().equals("2"))
				control.setCodUsuario("");
				
			String respuesta = this.CotBienesYServiciosManager.deleteDocumentoByItem(control.getCodCotizacion()
					, control.getCodCotizacionDet(), control.getCodAdjunto(), control.getCodUsuario());
			
			if ( respuesta == null ) control.setMsg("ERROR");
			else if ( respuesta.trim().equals("-1")) control.setMsg("ERROR");
			else{ if(respuesta.trim().equals("0")) 
				   {control.setMsg("OK");
				   	 if (control.getTipoProcedencia().trim().equals("2"))
					 {
						control.setListaDocumentosCompItem(
   					 this.CotBienesYServiciosManager.getAllDocumentosCompByItem(control.getCodCotizacion(),
   						control.getCodCotizacionDet(), control.getCodUsuario()));
					 }
					 else
					 {
						control.setListaDocumentosCompItem(
       				 this.CotBienesYServiciosManager.getAllDocumentosCompByItem(control.getCodCotizacion(),
       						control.getCodCotizacionDet(), ""));
					 }
				   }
			      
			}
			
		}
		else{ if(control.getOperacion().trim().equals("COPIAR")){
			    resultado=CopiarDocumentos(control);
				    
			    if ( resultado == null ) control.setMsg("FALLO");				
			    else if ( resultado.trim().equals("-1")) control.setMsg("FALLO");
				else{ control.setMsg("EXITO");
							if (control.getTipoProcedencia().trim().equals("2"))
							{
								control.setListaDocumentosCompItem(
		    					this.CotBienesYServiciosManager.getAllDocumentosCompByItem(control.getCodCotizacion(),
		    						control.getCodCotizacionDet(), control.getCodUsuario()));
							}
							else
							{
								control.setListaDocumentosCompItem(
		        				this.CotBienesYServiciosManager.getAllDocumentosCompByItem(control.getCodCotizacion(),
		        						control.getCodCotizacionDet(), ""));
							}
						}
			    }
			else if(control.getOperacion().equals("")){
			 
				log.info("LLenando la Bandeja de los Documentos");
				 LlenarBandeja(control);
				 //request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_IP + CommonConstants.STR_RUTA_DOCUMENTOS);
//				 request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_PUERTO_WEB + CommonConstants.DIRECTORIO_PROY + CommonConstants.STR_RUTA_DOCUMENTOS_02);
				 request.getSession().setAttribute("tipoAdjunto", control.getTipoAdjunto());
				 request.getSession().setAttribute("listaDocumentosComp", control.getListaDocumentosCompItem());
				 request.getSession().setAttribute("listaDocumentos", control.getListaDocumentosItem());
					
			}
		}
		
		
		if (control.getTipoProcedencia().trim().equals("2")){
			control.setListaDocumentosCompItem(
				this.CotBienesYServiciosManager.getAllDocumentosCompByItem(control.getCodCotizacion(),
						control.getCodCotizacionDet(), control.getCodUsuario()));
		}
		else
		{
			this.CotBienesYServiciosManager.getAllDocumentosCompByItem(control.getCodCotizacion(),
					control.getCodCotizacionDet(), "");
		}
		
		control.setListaDocumentosItem(
				this.CotBienesYServiciosManager.getAllDocumentosByItem(control.getCodCotizacion()
						, control.getCodCotizacionDet()));
		
    	
		if (control.getOperacion().trim().equals("OPEN_DOCUMENTO")){
			String sep = System.getProperty("file.separator");
			String uploadDir = CommonConstants.DIRECTORIO_DATA_PRIVADO + CommonConstants.STR_RUTA_DOCUMENTOS_02;			
			Archivo.downloadFile(control.getDocumentDownload(), uploadDir + sep + control.getDocumentDownload(), response);
			return null;		
		}
		
		control.setCodAdjunto("");
		control.setOperacion("");
		
		//request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_IP + CommonConstants.STR_RUTA_DOCUMENTOS);
//		request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_PUERTO_WEB + CommonConstants.DIRECTORIO_PROY + CommonConstants.STR_RUTA_DOCUMENTOS_02);
		request.getSession().setAttribute("tipoAdjunto", control.getTipoAdjunto());
    	request.getSession().setAttribute("listaDocumentosComp", control.getListaDocumentosCompItem());
    	request.getSession().setAttribute("listaDocumentos", control.getListaDocumentosItem());
    	
    	
    	
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/CotBienesYServicios/CotDet_BandejaDocumentosByItem","control",control);		
    }
        
     public String CopiarDocumentos(CotDetBandejaDocumentosCommand control){
    	 String resultado="";
    	 if(!control.getTipoProcedencia().equals("2"))
				control.setCodUsuario("");
    	 
    	 resultado=this.CotBienesYServiciosManager.UpdateDocumentosReqACot(control.getCodCotizacion(), 
    			 control.getCodCotizacionDet(), control.getCodUsuario());
    	 System.out.println("resultado del Copiar: "+resultado);
    	 return resultado;
     }  
     
     public void LlenarBandeja(CotDetBandejaDocumentosCommand command){
    	 
    	 if ( command.getCodCotizacion() != null  && 
     			command.getCodCotizacionDet() != null && 
     			command.getCodUsuario() != null )
     	{
     		if (command.getTipoProcedencia().trim().equals("2"))
     		{
     			command.setListaDocumentosCompItem(
     				this.CotBienesYServiciosManager.getAllDocumentosCompByItem(command.getCodCotizacion(),
     						command.getCodCotizacionDet(), command.getCodUsuario()));
     		}
     		else
     		{
     			command.setListaDocumentosCompItem(
         				this.CotBienesYServiciosManager.getAllDocumentosCompByItem(command.getCodCotizacion(),
         						command.getCodCotizacionDet(), ""));
     		}
     		command.setListaDocumentosItem(
     				this.CotBienesYServiciosManager.getAllDocumentosByItem(command.getCodCotizacion()
     						, command.getCodCotizacionDet()));
     	}
    	 
    	 if(command.getTipoProcedencia()!=null)
     	{  if(command.getTipoProcedencia().equals("0"))
     		  command.setDscTipoProcedencia("Adjuntar");
     	  else{ if(command.getTipoProcedencia().equals("1"))
     		    command.setDscTipoProcedencia("Adjuntos Cotizacion");
     	  		else{  if(command.getTipoProcedencia().equals("2"))
         		       command.setDscTipoProcedencia("Adjuntos Proveedor");
     	  			
     	  		}
     	  }
     	}
     }
}
