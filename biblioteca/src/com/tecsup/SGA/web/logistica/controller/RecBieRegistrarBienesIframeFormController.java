package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.GuiaRemision;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.RecBieRegistrarBienesIframeCommand;

public class RecBieRegistrarBienesIframeFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(RecBieRegistrarBienesIframeFormController.class);
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	AlmacenManager almacenManager;
	SolRequerimientoManager solRequerimientoManager;
	
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    log.info("formBackingObject:INI");
    
    RecBieRegistrarBienesIframeCommand command= new RecBieRegistrarBienesIframeCommand();
   
    command.setCodOrden(request.getParameter("txhCodOrden"));
    command.setCodDocumento(request.getParameter("txhCodCtaPago"));
    command.setCodUsuario(request.getParameter("txhCodUsuario"));
    command.setIndModi(request.getParameter("txhIndModi"));
    
    if(request.getParameter("txhCodSede")!=null)
    { command.setCodSede(request.getParameter("txhCodSede"));
	  log.info("CodSede: "+command.getCodSede());
      command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
      System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
    }
    command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN);
    
    
    Fecha fecha=new Fecha();
	command.setFechaBD(fecha.getFechaActual());
	System.out.println("FechaBD: "+command.getFechaBD());
    
    BuscarBandejaSuperior(command);
    BuscarBandejaDetalle(command);
    
    log.info("formBackingObject:FIN");
	 
  	return command;
   }
 	
 	protected void initBinder(HttpServletRequest request,
           ServletRequestDataBinder binder) {
   	NumberFormat nf = NumberFormat.getNumberInstance();
   	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
   	
   }
   /**
    * Redirect to the successView when the cancel button has been pressed.
    */
   public ModelAndView processFormSubmission(HttpServletRequest request,
                                             HttpServletResponse response,
                                             Object command,
                                             BindException errors)
   throws Exception {
       //this.onSubmit(request, response, command, errors);
   	return super.processFormSubmission(request, response, command, errors);
   }
 	
   public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
           BindException errors)
   		throws Exception {
   	log.info("onSubmit:INI");
   	
   	RecBieRegistrarBienesIframeCommand control= (RecBieRegistrarBienesIframeCommand) command;
   	String resultado="";
   	
   	if(control.getOperacion().equals("GUARDAR"))
   	{	resultado=GuardarGuiaRemision(control);
   	    if(resultado.equals("0")) control.setMsg("OK_GRABAR");
   	    else if(resultado.equals("-1")) control.setMsg("ERROR_GRABAR");
   	    BuscarBandejaDetalle(control);
   	}
   	else{ if(control.getOperacion().equals("CERRAR"))
   	      {	
   		     resultado=CerrarGuiaRemision(control);
   		    if(resultado.equals("0")) control.setMsg("OK_CERRAR");
   			else{ if(resultado.equals("-1")) control.setMsg("ERROR_CERRAR");
   				  else{ if(resultado.equals("-2")) control.setMsg("ERROR_CANT_ASIG");
   				  		else{ if(resultado.equals("-3")) control.setMsg("ERROR_CANT_CORRE");
   				  			  else{ if(resultado.equals("-4")) control.setMsg("ERROR_CERRAR_GUIA");
   				  			  		else if(resultado.equals("-5")) control.setMsg("ALMACEN_CERRADO");
   				  			  }
   				  		}
   				  }
   			}
	      }
   		
   	}
   	
   	BuscarBandejaDetalle(control);
   	
   	log.info("onSubmit:FIN");		
    return new ModelAndView("/logistica/Almacen/Rec_Bie_Registrar_Entrega_Bienes_Iframe","control",control);
   }

   
   public void BuscarBandejaSuperior(RecBieRegistrarBienesIframeCommand control){
	   List lista= new ArrayList();
	   lista=this.almacenManager.GetAllDatosGuiaRemision(control.getCodDocumento());
	   if(lista!=null)
	   { if(lista.size()>0)
	   		{ GuiaRemision guiaRemision= new GuiaRemision();
	   		  guiaRemision=(GuiaRemision)lista.get(0);
	   		  control.setNroFactura(guiaRemision.getNroCtaPago());
	   		  control.setFecInicio1(guiaRemision.getFecEmisionGuia());
	   		  control.setNroGuia(guiaRemision.getNroGuia());
	   		  control.setFecFinal1(guiaRemision.getFecVencimiento());
	   		  control.setFecInicio2(guiaRemision.getFecEmisionCta());
	   		  control.setFecFinal2(guiaRemision.getFecRecepcion());
	   		  System.out.println("Size1: "+lista.size());
	   		}
	    
	   }
	   
   }
   
   public void BuscarBandejaDetalle(RecBieRegistrarBienesIframeCommand control){
	   List lista= new ArrayList(); 
	   System.out.println("CodDoc: "+control.getCodDocumento());
	   lista=this.almacenManager.GetAllDetalleGuiaRemision(control.getCodDocumento());
	   if(lista!=null)
		   { if(lista.size()>0)
		  	 { control.setListaBandeja(lista);
		       control.setLongitud(String.valueOf(lista.size()));
		       GuiaRemision guiaRemision= new GuiaRemision();
		        float producto=0;
	        	float precioUni=0;
	        	float cantidad=0;
	        	float precioTotal=0;
		       for(int i=0;i<lista.size();i++)
		       {   guiaRemision=(GuiaRemision)lista.get(i);
		           precioUni=Float.valueOf(guiaRemision.getDscPrecioUnitario().trim());
		           cantidad=Float.valueOf(guiaRemision.getCanEntregada().trim());
		           producto=precioUni*cantidad;
		           precioTotal=precioTotal+Math.round(producto*1000f)/1000f;
		       }
		       control.setTotal(String.valueOf(precioTotal));
		       System.out.println("setTotal: "+String.valueOf(precioTotal));
		     }
		   }
	   else control.setListaBandeja(new ArrayList());
	   
   }
   
  public String GuardarGuiaRemision(RecBieRegistrarBienesIframeCommand control){
	  String resultado="";
	  System.out.println(">>CodDocumento<<"+control.getCodDocumento()+">>CadCodDocDetalle<<"+ control.getCadCodDocDetalle()
			  +">>CadCantidades<<"+	control.getCadCantidades()+">>Cantidad<<"+control.getLongitud()
			  +">>CodUsuario<<"+control.getCodUsuario());
	  resultado=this.almacenManager.UpdateGuiaRemision(control.getCodDocumento(), control.getCadCodDocDetalle(),
			  control.getCadCantidades(), control.getLongitud(), control.getCodUsuario());
	  System.out.println("Resultado Guardar: "+resultado);
	  return resultado;
  }
   
  public String CerrarGuiaRemision(RecBieRegistrarBienesIframeCommand control){
	  String resultado="";
	  resultado=this.almacenManager.UpdateCerrarGuiaRemision(control.getCodDocumento(), 
			  control.getCodUsuario());
	  System.out.println("Resultado Cerrar: "+resultado);
	  return resultado;
  }
  
public TablaDetalleManager getTablaDetalleManager() {
	return tablaDetalleManager;
}

public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
	this.tablaDetalleManager = tablaDetalleManager;
}

public SeguridadManager getSeguridadManager() {
	return seguridadManager;
}

public void setSeguridadManager(SeguridadManager seguridadManager) {
	this.seguridadManager = seguridadManager;
}

public DetalleManager getDetalleManager() {
	return detalleManager;
}

public void setDetalleManager(DetalleManager detalleManager) {
	this.detalleManager = detalleManager;
}

public AlmacenManager getAlmacenManager() {
	return almacenManager;
}

public void setAlmacenManager(AlmacenManager almacenManager) {
	this.almacenManager = almacenManager;
}
}
