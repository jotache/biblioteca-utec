package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.AtenderRequerimientoServiciosGuiaAtencionCommand;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;

public class AtenderRequerimientoServiciosGuiaAtencionFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AtenderRequerimientoServiciosGuiaAtencionFormController.class);	
	private AtencionRequerimientosManager atencionRequerimientosManager;

	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	AtenderRequerimientoServiciosGuiaAtencionCommand command = new AtenderRequerimientoServiciosGuiaAtencionCommand();
    	//*********************************************************
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodGuia(request.getParameter("txhCodGuia") == null ? "": request.getParameter("txhCodGuia"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");
    	System.out.println("codSede="+command.getCodGuia()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");
    	//*********************************************************
    	if(command.getCodRequerimiento()!=null){
	    	if(!command.getCodRequerimiento().equals("")){    		
	    		cargaDatosCabecera(command);
	    	}
    	}   	
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	AtenderRequerimientoServiciosGuiaAtencionCommand control = (AtenderRequerimientoServiciosGuiaAtencionCommand)command;
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = GrabarDetalleGuia(control);
    		System.out.println("resultado codGuia-->"+resultado);    		
    		if (resultado.equals("0")){  		
    			control.setMsg("OKGRABAR");
    		}
    		else{ if (resultado.equals("-2")){  		
    				 control.setMsg("ERROR_EXCEDE");
    				}
    			 else
    			  control.setMsg("ERRORGRABAR");    			
    		}
    		cargaDatosCabecera(control);
    	}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/Atencion_Requerimiento_Servicios_Guia_Atencion","control",control);
    }    
    private void cargaDatosCabecera(AtenderRequerimientoServiciosGuiaAtencionCommand control){    
    	List lista=this.atencionRequerimientosManager.GetGuiaRequerimiento(control.getCodGuia());
    	
    	if(lista!=null && lista.size()>0){
    		Guia guia = (Guia)lista.get(0);
    		//se asigna valores a la input
    		control.setCodGuia(guia.getCodGuia());
    		control.setNroGuia(guia.getNroGuia());
    		control.setFechaGuia(guia.getFechaEmision());
    		control.setEstado(guia.getEstado());
    		control.setNroFactura(guia.getNroFactura());
    		control.setFechaFactura(guia.getFecEmiFactura());
    		control.setNroOS(guia.getNroOrden());
    		control.setObservacion(guia.getObservacion());
    		control.setCodDetalle(guia.getCodReqDetalle());
    	}
    }
    private String GrabarDetalleGuia(AtenderRequerimientoServiciosGuiaAtencionCommand control){    	
    	try{
    		/*
    		 * PROCEDURE SP_INS_DET_GUIA_REQ(
				E_C_CODGUIA IN CHAR,
				E_C_CODREQ IN CHAR,
				E_C_FEC_GUIA IN CHAR,
				E_V_OBSERVACION IN VARCHAR2,
				E_V_CAD_CODDETREQ IN VARCHAR2,
				E_V_CAD_CANTSOL IN VARCHAR2,
				E_V_CAD_PRECIOS IN VARCHAR2,
				E_V_CAD_CANTENT IN VARCHAR2,
				E_C_CANT_REG IN CHAR,
				E_C_CODUSUARIO IN CHAR,
				S_V_RETVAL IN OUT VARCHAR2)
    		 */
    	
    	
    		System.out.println("codGuia>>"+control.getCodGuia()+"<<");
    		System.out.println("codRequerimiento>>"+control.getCodRequerimiento()+"<<");
    		System.out.println("fechaGuia>>"+control.getFechaGuia()+"<<");
    		System.out.println("Observacion>><<");
    		System.out.println("CadenaCodDetalle>><<");
    		System.out.println("CadenaCantidadSolicitada>><<");
    		System.out.println("CadenaPrecios>><<");
    		System.out.println("CadenaCantidadEntregada>><<");    		
    		System.out.println("cantidad>><<");
    		
    		GuiaDetalle obj = new GuiaDetalle();
    		
    		obj.setCodGuia(control.getCodGuia());
    		obj.setCodRequerimiento(control.getCodRequerimiento());
    		obj.setFechaGuia(control.getFechaGuia());
    		obj.setObservacion(control.getObservacion().trim());
    		obj.setCadenaCodDetalle(control.getCodDetalle());///
    		obj.setCadenaCantidadSolicitada("");
    		obj.setCadenaPrecios("");
    		obj.setCadenaCantidadEntregada("");
    		obj.setCantidad("");
    		
    		
    		String resultado="";
    		resultado=this.atencionRequerimientosManager.InsertDetalleGuiaRequerimiento(obj, control.getCodUsuario());    		
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}
