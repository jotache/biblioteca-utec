package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.AtenRequerimientosBandejaMantenimientoCommand;
import com.tecsup.SGA.web.logistica.command.SreqBandejaEvaluadorCommand;
import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;

public class SreqBandejaEvaluadorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(SreqBandejaEvaluadorFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	SreqBandejaEvaluadorCommand command = new SreqBandejaEvaluadorCommand();

    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    	}
    	
        command.setConsteCodBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
        command.setConsteCodServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
        command.setConsteCodEstadoPendiente(CommonConstants.COD_ESTADO_PENDIENTE);
        command.setConsteCodEstadoRechazado(CommonConstants.COD_ESTADO_RECHAZO);
        command.setConsteCodEstadoAprobacion(CommonConstants.COD_ESTADO_APROBACION);
        command.setConsteCodEstadoCerrado(CommonConstants.COD_ESTADO_CERRADO);
            
        if(request.getParameter("txhCodSede")!=null)
        { command.setCodSede(request.getParameter("txhCodSede"));
    	  log.info("CodSede: "+command.getCodSede());
          command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
          System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
        }
        command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN); 
        
        List lista1= new ArrayList();
        List lista2= new ArrayList();
        List lista3= new ArrayList();
        lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
        if(lista1!=null)
    	command.setListTipoRequerimiento(lista1);
        lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
        if(lista2!=null)
    	command.setListEstado(lista2);
        lista3=this.seguridadManager.GetAllCentrosCosto(command.getCodSede(),"","");
        if(lista3!=null)
    	command.setListCeco(lista3);
        
        BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean!= null)
         {  log.info("BandejaLogisticaBean:INICIO");
            if(bandejaBean.getCodBandeja().equals("1")){
            	     	 	
            	command.setCodTipoRequerimiento(bandejaBean.getValor1());
            	command.setNroRequerimiento(bandejaBean.getValor2());
            	command.setFecInicio(bandejaBean.getValor3());
            	command.setFecFinal(bandejaBean.getValor4());
            	command.setCodCeco(bandejaBean.getValor5());
            	command.setCodEstado(bandejaBean.getValor6());
       	 	 	BuscarBandeja(command, request);
            }
       	  log.info("BandejaLogisticaBean:FIN");
         }
        else{
        	
        	request.getSession().removeAttribute("listaBien");
        	request.getSession().removeAttribute("listaServicios");
        	
        }
        
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	SreqBandejaEvaluadorCommand control = (SreqBandejaEvaluadorCommand)command;
    	String resultado="";
    	if(control.getOperacion().equals("BUSCAR")){
    		 BuscarBandeja( control,  request);
    		
    	}
    	else{ if(control.getOperacion().equals("ELIMINAR")){
    		   {   resultado= EliminarRegistro( control);
		    		if ( resultado.equals("-2")) control.setMsg("ERROR");
		       		else{ if(resultado.equals("0"))
		       			  {control.setMsg("OK");
		       			   BuscarBandeja( control,  request);
		       			  }
		       			else{ if(resultado.equals("-1"))
		       				     control.setMsg("NO_SE_ELIMINA");
		       				 else  if(resultado.equals("-3"))
		       				       control.setMsg("ALMACEN_CERRADO");
		       			}
		       			   
		       		
		       		}
    		   		
    		   }
    		 }
    		
    	}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/SolRequerimiento/Sreq_Bandeja_Evaluador","control",control);
    }
    
    public void BuscarBandeja(SreqBandejaEvaluadorCommand control, HttpServletRequest request){
    
    	GuardarDatosBandeja(control, request);
    	
    if(control.getCodCeco().equals("-1")) control.setCodCeco("");
    if(control.getCodEstado().equals("-1")) control.setCodEstado("");
    	
    log.info(control.getCodSede()+">><<"+
    		control.getCodTipoRequerimiento()+">><<"+control.getNroRequerimiento()+">><<"+
    		control.getFecInicio()+">><<"+control.getFecFinal()+">><<"+control.getCodCeco()+
    		">><<"+control.getCodEstado()+">><<"+control.getCodUsuario());
    	
    List listaTipo=this.solRequerimientoManager.getAllSolReqUsuario(control.getCodSede(),
    		control.getCodTipoRequerimiento(),control.getNroRequerimiento(),control.getFecInicio(),
    		control.getFecFinal(),control.getCodCeco(),control.getCodEstado(), control.getCodUsuario());
    if(listaTipo==null)
    	  log.info("xD");
    else  log.info("Size: "+listaTipo.size());
    //request.getSession().setAttribute("listaNovedades", listaTipo);
    if(control.getCodTipoRequerimiento().equals(control.getConsteCodBien()))
    {	control.setBanderaTipoReq("BIEN");  ///request.setAttribute("TIPO_REQ", "BIEN");
        request.getSession().setAttribute("listaBien", listaTipo);
        log.info("BIEN");
       
    }
    else{ if(control.getCodTipoRequerimiento().equals(control.getConsteCodServicio()))
    		{ control.setBanderaTipoReq("SERVICIO"); //request.setAttribute("TIPO_REQ", "SERVICIO");
    		  request.getSession().setAttribute("listaServicios", listaTipo); 
    		  log.info("SERVICIO");
    		  
    		}
    	  }
    control.setListCeco(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),"",""));	
    }
    
    public String EliminarRegistro(SreqBandejaEvaluadorCommand control){
    	String resultado="";
    	System.out.println("CodRequerimiento: "+control.getCodRequerimiento());
    	System.out.println("CodUsuario(): "+control.getCodUsuario());
    	 resultado=this.solRequerimientoManager.DeleteSolRequerimiento(control.getCodRequerimiento(),
    			 control.getCodUsuario());
    	 System.out.println("Resultado: "+resultado);
    	return resultado;
    }
    
    public void GuardarDatosBandeja(SreqBandejaEvaluadorCommand control,HttpServletRequest request)
	  {
	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	 	
	 	 bandejaBean.setCodBandeja("1");
	 	 
	 	 bandejaBean.setValor1(control.getCodTipoRequerimiento());
	 	 bandejaBean.setValor2(control.getNroRequerimiento());
	 	 bandejaBean.setValor3(control.getFecInicio());
	 	 bandejaBean.setValor4(control.getFecFinal());
	 	 bandejaBean.setValor5(control.getCodCeco());
	 	 bandejaBean.setValor6(control.getCodEstado());
	 		 		 	 
	 	 log.info("GuardarDatosBandeja");
	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	  }
    
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
}
