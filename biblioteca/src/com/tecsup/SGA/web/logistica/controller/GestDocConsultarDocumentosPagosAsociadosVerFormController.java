package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DocPago;
import com.tecsup.SGA.modelo.TipoLogistica; 
import com.tecsup.SGA.modelo.DetalleDocPago;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
import com.tecsup.SGA.web.logistica.command.VerDocumentoCommand;

public class GestDocConsultarDocumentosPagosAsociadosVerFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(GestDocConsultarDocumentosPagosAsociadosVerFormController.class);
	  DetalleManager detalleManager;
	  GestionDocumentosManager gestionDocumentosManager;
	    public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public GestDocConsultarDocumentosPagosAsociadosVerFormController() {
	        super();
	        setCommandClass(VerDocumentoCommand.class);
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");
	    	VerDocumentoCommand command = new VerDocumentoCommand();
	    	command.setCodAlumno((String)request.getParameter("txhCodUsuario"));
	    	command.setOrdeId((String)request.getParameter("txhCodOrden"));
	    	command.setDocId((String)request.getParameter("txhCodDocumento"));
	    	command.setSede((String)request.getParameter("txhCodSede"));
	    	System.out.println("codUsu:<"+command.getCodAlumno()+">");
	    	System.out.println("codOrd:<"+command.getOrdeId()+">");
	    	System.out.println("codDoc:<"+command.getDocId()+">");
	    	System.out.println("codSed:<"+command.getSede()+">");
	    	llena(command);
	    	llenaDocumentos(command);
	    	request.getSession().setAttribute("listDocumentos", command.getListDocumentos());
	    	request.getSession().setAttribute("listDocumentosRelacionados", command.getListDocumentosRelacionados());
	        log.info("formBackingObject:FIN");
	        return command;
	    }
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	VerDocumentoCommand control = (VerDocumentoCommand) command;
	    	if("GRABAR".equals(control.getOperacion())){
	    		resultado = Grabar(control);
	    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
			else if(resultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
	    		
	    	}
	    	
	    	if("RECHAZAR".equals(control.getOperacion())){
	    		
	    		resultado=rechaza(control);
	    		if ( resultado.equals("-1")) {
	    			control.setMsgRe("ERROR");
	    		}
	    		else if(resultado.equals("-2")){
		    		control.setMsgRe("DOBLE");
		    	}
	        	else control.setMsgRe("OK");
	    	}
	    	
	    	if("VALIDAR".equals(control.getOperacion())){
	    		
	    		resultado=validar(control);
	    		if ( resultado.equals("-1")) {
	    			control.setMsgVa("ERROR");
	    		}
	    		else if(resultado.equals("-2")){
		    		control.setMsgVa("DOBLE");
		    	}
	        	else control.setMsgVa("OK");
	    	}
	 		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/GestionDocumentos/Gest_Doc_Consultar_Documentos_Pagos_Asociados_ver","control",control);
	    }
	     
	        private String Grabar(VerDocumentoCommand control){
	        	return control.getGrabar();
	        }
	        private void llena(VerDocumentoCommand control){
	        	control.setListDetalles(this.gestionDocumentosManager.GetAllDocPago(control.getOrdeId()
	        			, control.getDocId()));
	        	List lst  = control.getListDetalles();
	        	
	        	if(lst!=null ){
	        		if ( lst.size()>0 )
	        		{
	        			DocPago docPago = null;
	        			docPago = (DocPago)lst.get(0);
	        			
	        			control.setNroFactura(docPago.getFactura());
	        			control.setNroGuia(docPago.getNroGuia());
	        			control.setNroOrden(docPago.getNroOrden());
	        			control.setFechaEmicion(docPago.getFecEmiFactura());
	        			control.setFechaEmicion2(docPago.getFecEmiGuia());
	        			control.setFechaEmicion3(docPago.getFecEmiOrden());
	        			control.setFechaVcto(docPago.getFecVctoFactura());
	        			control.setMoneda(docPago.getSigMoneda().trim());
	        			control.setMonto(docPago.getMontoOrden().trim());
	        			control.setSubTotal(docPago.getSubTotal().trim());
	        			control.setIgv(docPago.getIgv().trim());
	        			control.setTot(Float.valueOf(docPago.getTotalFacturado().trim()));
	        			//ALQD,12/06/09. NUEVA PROPIEDAD
	        			control.setIndImportacion(docPago.getIndImportacion().trim());
	        			//ALQD,01/07/09. NUEVA PROPIEDA - OTROS COSTOS
	        			control.setImpOtros(docPago.getImpOtros());
	        		}
	        	}
	        	else{
	        		control.setNroFactura("");
        			control.setNroGuia("");
        			control.setNroOrden("");
        			control.setFechaEmicion("");
        			control.setFechaEmicion2("");
        			control.setFechaEmicion3("");
        			control.setFechaVcto("");
        			control.setMoneda("");
        			control.setMonto("");
        			control.setSubTotal("");
        			control.setTot(0);
        			control.setIndImportacion("0");
	        	}
	        }
	     
			public void setGestionDocumentosManager(
					GestionDocumentosManager gestionDocumentosManager) {
				this.gestionDocumentosManager = gestionDocumentosManager;
			}
			
			public void llenaDocumentos(VerDocumentoCommand control){
				int can=0;
				float tot=0;
				control.setListDocumentos(this.gestionDocumentosManager.GetAllDetalleDocPago(control.getDocId()));
		
				List lst  = control.getListDocumentos();
				if(lst!=null ){
					can = control.getListDocumentos().size();
	        		if ( lst.size()>0 )
	        		{
		        	for(int i=0;i<can;i++){
		        		DetalleDocPago detalleDocPago = null;
		        		detalleDocPago = (DetalleDocPago)lst.get(i);
		        		tot = tot + Float.valueOf(detalleDocPago.getSubTotal());
		        	}
	        		}
	        		control.setCant(control.getListDocumentos().size());
	        	}
			//ALQD,12/06/09.LEER DOCUMENTOS ASOCIADOS CUANDO ES OC IMPORTACION
				if(control.getIndImportacion().equals("1")) {
					control.setListDocumentosRelacionados(this.detalleManager.GetAllDocAsociado(control.getOrdeId()));

				}
			}
			
			private String rechaza(VerDocumentoCommand control){
				control.setRechaza(this.gestionDocumentosManager.ActRechazarDocPago(control.getDocId()
						,control.getCodAlumno()));
				return control.getRechaza();
			}

			private String validar(VerDocumentoCommand control){
				control.setValida(this.gestionDocumentosManager.ActValidarDocPago(control.getDocId()
						, control.getCodAlumno()));
				return control.getValida();
			}
}
