package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.ConsultaCatalogoCommand;
import com.tecsup.SGA.web.logistica.command.SreqBandejaEvaluadorCommand;

public class ConsultaCatalogoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultaCatalogoFormController.class);
    //private ConfInicioManager confInicioManager;
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public ConsultaCatalogoFormController() {    	
        super();        
        setCommandClass(ConsultaCatalogoCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	ConsultaCatalogoCommand command = new ConsultaCatalogoCommand();
    	
    	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
		String fecha1 =  sdf.format(Fecha.getSumaFechaDias("-120", Fecha.getCurrentDate())).toString();
    	String fecha2 = sdf.format(Fecha.getCurrentDate()).toString();		
    	command.setFec1(fecha1);  	
    	command.setFec2(fecha2);  	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{    		
    		command.setDscUsuario(usuarioSeguridad.getNomUsuario());
    	}
    	
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	cargaDatos(command);
    	String bandera="";
    	String val="";
    	request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02);
    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
               	request.getSession().setAttribute("listProductos", command.getListProductos());
          		request.getSession().setAttribute("listSede", command.getListSede());
          		request.getSession().setAttribute("listFamilia", command.getListFamilia());	
          		request.getSession().setAttribute("listSubFamilia", command.getListSubFamilia());	
          		request.getSession().setAttribute("listCondicion", command.getListCondicion());
          		request.getSession().setAttribute("listestado", command.getListestado());
          		bandera=(String)request.getParameter("txhBandera");
          		}
          	}
    	
    	if(bandera!=null)
    	{  
    		val="1";
    	}
    	else val="0";
    	
    	try{
    	 if(val.equals("1"))	request.getSession().removeAttribute("bandejaLogisticaBean");
    	 else
    		{BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
	         if(bandejaBean!= null)
	          {  log.info("BandejaLogisticaBean:INICIO");
	            if(bandejaBean.getCodBandeja().equals("20")){
	            	     	 	
	            	command.setCodSede(bandejaBean.getValor1());
	            	command.setCodFamilia(bandejaBean.getValor2());
	            	command.setCodSubFamilia(bandejaBean.getValor3());
	            	command.setCodPro(bandejaBean.getValor4());
	            	command.setProducto(bandejaBean.getValor5());
	            	command.setCodCondicion(bandejaBean.getValor6());
	            	command.setNroSerie(bandejaBean.getValor7()); 
	            	command.setCodEstado(bandejaBean.getValor8());
	            	
	            	llenaSubFamilia(command);
	            	buscando(command, request);
	            	request.getSession().setAttribute("listProductos", command.getListProductos());
	            }
	       	   log.info("BandejaLogisticaBean:FIN");
	          }
    		 }
    	}catch (Exception e) {
			System.out.println("Exception"+bandera);
		}
    	
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	 String resultado = "";
    	ConsultaCatalogoCommand control = (ConsultaCatalogoCommand) command;
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{
	    	cargaDatos(control);
	    	buscando(control, request);
	    	llenaSubFamilia(control);
	     	request.getSession().setAttribute("listProductos", control.getListProductos());
	  		request.getSession().setAttribute("listSede", control.getListSede());
	  		request.getSession().setAttribute("listFamilia", control.getListFamilia());	
	  		request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());	
	  		request.getSession().setAttribute("listCondicion", control.getListCondicion());	
      		request.getSession().setAttribute("listestado", control.getListestado());
    	}
    	else if (control.getOperacion().trim().equals("MUESTRASUBFAMILIA"))
    	{
    		llenaSubFamilia(control);
    		if ( !control.getCodSede().trim().equals("") )
    			buscando(control, request);
	     	request.getSession().setAttribute("listProductos", control.getListProductos());
    		request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
    	  	
    	}
    	else if (control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		resultado = delete(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    			}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("DOBLE");
    		}
    		else {
    			control.setMsg("OK");
    			}
    		cargaDatos(control);
        	buscando(control, request);
        	llenaSubFamilia(control);
         	request.getSession().setAttribute("listProductos", control.getListProductos());
      		request.getSession().setAttribute("listSede", control.getListSede());
      		request.getSession().setAttribute("listFamilia", control.getListFamilia());	
      		request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());	
      		request.getSession().setAttribute("listCondicion", control.getListCondicion());	
      		request.getSession().setAttribute("listestado", control.getListestado());
 
    	}
    	else if (control.getOperacion().trim().equals(""))
    	{
    		cargaDatos(control);
        	buscando(control, request);
        	llenaSubFamilia(control);
         	request.getSession().setAttribute("listProductos", control.getListProductos());
      		request.getSession().setAttribute("listSede", control.getListSede());
      		request.getSession().setAttribute("listFamilia", control.getListFamilia());	
      		request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());	
      		request.getSession().setAttribute("listCondicion", control.getListCondicion());	
      		request.getSession().setAttribute("listestado", control.getListestado());
        	
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Consulta_Catalogo_Productos","control",control);		
    }
        private void cargaDatos(ConsultaCatalogoCommand control){
        	TipoLogistica obj = new TipoLogistica();
        	
        	obj.setCodTabla(CommonConstants.TIPT_SEDE);
        	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
        	control.setListSede(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
        	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
        	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	
        
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
        	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
        	control.setListCondicion(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_ESTADO);
        	obj.setTipo(CommonConstants.MTO_CATALOGO_ESTADO);
        	control.setListestado(this.detalleManager.GetAllTablaDetalle(obj));
        	System.out.println("estado: "+control.getCodEstado());
        }
        private void buscando(ConsultaCatalogoCommand control,HttpServletRequest request){
        	GuardarDatosBandeja(control, request);
        	System.out.println("estados: "+control.getListestado().size());
        	try{
          	control.setListProductos(this.detalleManager.GetAllProducto("",control.getCodSede()
        			, control.getCodFamilia(), control.getCodSubFamilia(), control.getCodPro()
        			, control.getProducto(), control.getCodCondicion(), control.getNroSerie(), "",
        			control.getCodEstado()));
        	}catch (Exception e) {
			}
        }
        
        private void llenaSubFamilia(ConsultaCatalogoCommand control){
        	TipoLogistica obj = new TipoLogistica();
        	obj.setCodId(control.getCodFamilia());
        	obj.setCodTabla(CommonConstants.TIPT_SUB_FAMILIA);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	control.setListSubFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        
        }
        
        private String delete(ConsultaCatalogoCommand control){
        	
        	control.setElimina(this.detalleManager.DeleteProducto(control.getCodProducto()
        			, control.getCodAlumno()));
        	
        	return control.getElimina();
        }
        
        public void GuardarDatosBandeja(ConsultaCatalogoCommand control,HttpServletRequest request)
  	  {
  	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
  	 	 
  	 	 bandejaBean.setCodBandeja("20");
  	 	 
  	 	 bandejaBean.setValor1(control.getCodSede());
  	 	 bandejaBean.setValor2(control.getCodFamilia());
  	 	 bandejaBean.setValor3(control.getCodSubFamilia());
  	 	 bandejaBean.setValor4(control.getCodPro());
  	 	 bandejaBean.setValor5(control.getProducto());
  	 	 bandejaBean.setValor6(control.getCodCondicion());
  	 	 bandejaBean.setValor7(control.getNroSerie());
  	 	 bandejaBean.setValor8(control.getCodEstado());
  	 	 		 	 
  	 	 log.info("GuardarDatosBandeja");
  	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
  	  }
}
