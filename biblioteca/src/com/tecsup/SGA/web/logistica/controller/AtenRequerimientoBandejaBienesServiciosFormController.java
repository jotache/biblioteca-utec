package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;

import com.tecsup.SGA.web.logistica.command.AtenRequerimientoBandejaBienesServiciosCommand;
import com.tecsup.SGA.web.logistica.command.GestDocAprobarDocumentosPagoCommand;

public class AtenRequerimientoBandejaBienesServiciosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AtenRequerimientoBandejaBienesServiciosFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	AtencionRequerimientosManager atencionRequerimientosManager;
	
	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	AtenRequerimientoBandejaBienesServiciosCommand command= new AtenRequerimientoBandejaBienesServiciosCommand();
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    	}
    	//command.setCodSede(request.getParameter("txhCodSede"));
    	log.info("CodSede: "+command.getCodSede());
        command.setConsteBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
        command.setConsteServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
        command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setConsteGrupoGenerales(CommonConstants.SOL_AREQ_GRUPOS_GENERALES);
    	command.setConsteGrupoMantenimiento(CommonConstants.SOL_AREQ_GRUPOS_MANTENIMIENTO);
    	command.setConsteGrupoServicios(CommonConstants.SOL_AREQ_GRUPOS_SERVICOS);
    	command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN);
    	command.setIndGrupo("0");
        //ALQD,23/01/09. NUEVA CONSTANTE PARA FILTRAR PENDIENTES X CONFIRMAR
        command.setConsteIndSalPorConfirmar("1");
    	
    	if(request.getParameter("txhCodSede")!=null){
    		command.setCodSede(request.getParameter("txhCodSede"));    		
    		command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));    		
        }
    	
    	List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();

		//PARA LLENAR LAS LISTAS
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    		command.setListCodTipoRequerimiento(lista1);
    	
    	lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista2!=null)
    		command.setListaCodEstado(lista2);
    	//ALQD,23/01/09. SE TRAERA TODOS LOS CENTROS DE COSTOS
    	//lista3=this.seguridadManager.GetAllCentrosCosto(command.getCodSede(), command.getCodPerfil(), command.getCodUsuario());
    	lista3=this.seguridadManager.GetAllCentrosCosto(command.getCodSede(), command.getCodPerfil(), "");
    	if(lista3!=null)
    		command.setListCeco(lista3);
    	//***********************    	
    	ListarCombo(command, request);
    	
    	BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean!= null){
            if(bandejaBean.getCodBandeja().equals("10")){
            	command.setCodTipoRequerimiento(bandejaBean.getValor1());
            	
                if(command.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN)){
            		command.setConsteRadio(bandejaBean.getValor2());
            		command.setCodTipoServicio("-1");
        		}
        	    else
    	    	if(command.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
    	        	 command.setCodTipoServicio(bandejaBean.getValor2());
        	            	 	
                command.setTxtNroRequerimiento(bandejaBean.getValor3());
                command.setFecInicio(bandejaBean.getValor4());
                command.setFecFinal(bandejaBean.getValor5());
                command.setCodCeco(bandejaBean.getValor6());
                command.setUsuSolicitante(bandejaBean.getValor7());
                command.setCodEstado(bandejaBean.getValor8());
                command.setNroGuia(bandejaBean.getValor9());
		         //ALQD,23/01/09. A�ADIENDO NUEVO FILTRO DE BUSQUEDA
		         command.setIndSalPorConfirmar(bandejaBean.getValor10());
                //MODIFICADO RNAPA 19/08/2008
        		StringTokenizer cadenaPerfil = new StringTokenizer(command.getCodPerfil(),"|");
        		while(cadenaPerfil.hasMoreTokens()){
        			String tokenPerfil = cadenaPerfil.nextToken();
        			if(tokenPerfil.equals("LALMC")){//LDCECO        				
        				command.setEstadoComboTipoReq("1");
        			}
        			else
        			if(tokenPerfil.equals("LCOMP")){        				
        				command.setEstadoComboTipoReq("1");
        			}
        		}
        		//***************************                
            }            
        }
        else{
        	/*command.setBanListaServicio("0");
    		command.setBanListaBien("0");*/    		
    		//MODIFICADO RNAPA 19/08/2008
    		StringTokenizer cadenaPerfil = new StringTokenizer(command.getCodPerfil(),"|");
    		
    		while(cadenaPerfil.hasMoreTokens()){
    			String tokenPerfil = cadenaPerfil.nextToken();
    			if(tokenPerfil.equals("LALMC")){//LALMC
    				command.setCodTipoRequerimiento(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    				command.setEstadoComboTipoReq("1");
    			}
    			else
    			if(tokenPerfil.equals("LCOMP")){
    				command.setCodTipoRequerimiento(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
    				command.setEstadoComboTipoReq("1");
    			}
    		}
    		//***************************
		}
        if(command.getCodEstado()==null)
			command.setCodEstado("");    		
		
        if(command.getCodCeco()==null)
        	command.setCodCeco("");
        
        if(command.getCodTipoServicio()==null)
        	command.setCodTipoServicio("");
        
		if(command.getCodTipoRequerimiento()==null)
			command.setCodTipoRequerimiento(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
		
		if(command.getIndSalPorConfirmar()==null)
			command.setIndSalPorConfirmar("");
		
        Buscar(command, request);
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	AtenRequerimientoBandejaBienesServiciosCommand control= (AtenRequerimientoBandejaBienesServiciosCommand) command;
    	control.setBanListaServicio("");
    	control.setBanListaBien("");
    	
    	if(control.getOperacion().equals("BUSCAR"))
    	{Buscar(control, request);
    		List lista3= new ArrayList();
    		//ALQD,23/01/09. SE TRAERA TODOS LOS CENTROS DE COSTOS
    		//lista3=this.seguridadManager.GetAllCentrosCosto(control.getCodSede(), control.getCodPerfil(), control.getCodUsuario());
    		lista3=this.seguridadManager.GetAllCentrosCosto(control.getCodSede(), control.getCodPerfil(), "");
    		if(lista3!=null)
    			control.setListCeco(lista3);	
    	}
    	else{ if(control.getOperacion().equals("CERRAR_REQ"))
    			{ String resultado="";
    			  resultado= CerrarRequerimiento(control);
    			  if( resultado.equals("-1")) 
    				  control.setMsg("ERROR");
    			  else if(resultado.equals("0")) control.setMsg("OK");
    			  Buscar(control, request);
    			}
    	}
    	
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/AtenRequerimientosBandejaBienesServicios","control",control);
    }
    
    public void ListarCombo(AtenRequerimientoBandejaBienesServiciosCommand control, HttpServletRequest request)
    {List lista= new ArrayList();
     List lista1= new ArrayList();
     List lista2= new ArrayList();
     List lista3= new ArrayList();
     TipoTablaDetalle tipoTablaDetalle1 = new TipoTablaDetalle();
     TipoTablaDetalle tipoTablaDetalle2 = new TipoTablaDetalle();
     System.out.println(":D 0");
      lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
    			, "", "", control.getConsteGrupoGenerales(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
      lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
  			, "", "", control.getConsteGrupoServicios(), "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
      for(int i=0;i<lista1.size();i++)
      {  tipoTablaDetalle1=(TipoTablaDetalle)lista1.get(i);
    	  lista.add(tipoTablaDetalle1);
      }
      for(int k=0;k<lista2.size();k++)
      {  tipoTablaDetalle2=(TipoTablaDetalle)lista2.get(k);
    	  lista.add(tipoTablaDetalle2);
      }
      if(lista!=null)
    	  control.setListaCodTipoServicio(lista);
      System.out.println(":D  Lista: "+lista.size()+" Lista1: "+lista1.size()+" Lista2: "+lista2.size());
      //control.setValorIndGrupo("0");
      //ALQD,23/01/09. SE TRAERA TODOS LOS CECOS.
     //control.setListCeco(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(), control.getCodPerfil(), control.getCodUsuario()));
      control.setListCeco(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(), control.getCodPerfil(), ""));
    }

    public void Buscar(AtenRequerimientoBandejaBienesServiciosCommand control, HttpServletRequest request){
    	
    	List lista1= new ArrayList();
    	
    	GuardarDatosBandeja(control, request);
    	
    	System.out.println("En el Buscar :D");
        String SubTipoReq="";
          if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
	        	SubTipoReq=control.getConsteRadio();
	      else if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
	        	 {if(!control.getCodTipoServicio().equals("-1"))
	        	       SubTipoReq=control.getCodTipoServicio();
	        	  else SubTipoReq="";
	        	 }
	        if(control.getCodEstado().equals("-1"))
	        	control.setCodEstado("");
	        if(control.getCodCeco().equals("-1"))
	        	control.setCodCeco("");
	        if(control.getCodTipoServicio().equals("-1"))
	        	control.setCodTipoServicio("");
	        if(control.getIndSalPorConfirmar().equals("0"))
	        	control.setIndSalPorConfirmar("");

	        //ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA 
	    System.out.println(">>CodSede<<"+control.getCodSede()+">>CodTipoRequerimiento<<"+control.getCodTipoRequerimiento()+">>SubTipoReq<<"+
    			SubTipoReq+">>NroRequerimiento<<"+control.getTxtNroRequerimiento()+">>FecInicio<<"+control.getFecInicio()
    			+">>FecFinal<<"+control.getFecFinal()+">>CodCeco<<"+
    			control.getCodCeco()+">>NomUsuario<<"+control.getUsuSolicitante()+">>CodEstado<<"
    			+control.getCodEstado()+">>NroGuiaSalida<<"+control.getNroGuia()
    			+">>indSalPorConfirmar<<"+control.getIndSalPorConfirmar());
	    	      
    	lista1=this.atencionRequerimientosManager.GetAllBandejaAtencionRequerimientos(control.getCodSede(),
    			control.getCodTipoRequerimiento(), SubTipoReq, control.getTxtNroRequerimiento(),
    			control.getFecInicio(), control.getFecFinal(), control.getCodCeco(), 
    			control.getUsuSolicitante(), control.getCodEstado(), control.getNroGuia(),
    			control.getIndSalPorConfirmar());
    if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
    {	if(lista1!=null)
      	  {control.setListaBandejaBienes(lista1);
      	   if(lista1.size()==0) 
      		   control.setBanListaBien("0");
      	  }
      	else{ control.setListaBandejaBienes(new ArrayList());
      		  control.setBanListaBien("0");
      	}
    control.setListaBandejaServicios(new ArrayList()); 
    }
    else{if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
    	  {if(lista1!=null)
    	  { control.setListaBandejaServicios(lista1);
    	    if(lista1.size()==0) 
    	    	control.setBanListaServicio("0");
    	  }
          else{ control.setListaBandejaServicios(new ArrayList());
                control.setBanListaServicio("0");
          }
    	  control.setListaBandejaBienes(new ArrayList());
    	  }
    }
    }
    
    public String CerrarRequerimiento(AtenRequerimientoBandejaBienesServiciosCommand control){
    	String resultado="";
    	System.out.println("CerrarRequerimiento xD");
    	resultado=this.atencionRequerimientosManager.UpdateCerrarRequerimiento(control.getIdReq(), 
    			control.getCodUsuario());
    	
    	System.out.println("resultado: "+resultado);
    	return resultado;
    }
    
    public void GuardarDatosBandeja(AtenRequerimientoBandejaBienesServiciosCommand control,HttpServletRequest request)
	  {
	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	 	 
	 	String SubTipoReq="";
        if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
	        	SubTipoReq=control.getConsteRadio();
	     else if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
	        	 {if(!control.getCodTipoServicio().equals("-1"))
	        	       SubTipoReq=control.getCodTipoServicio();
	        	  else SubTipoReq="";
	        	 }
	 	 
	 	 bandejaBean.setCodBandeja("10");
	 	 
	 	 bandejaBean.setValor1(control.getCodTipoRequerimiento());
	 	 bandejaBean.setValor2(SubTipoReq);
	 	 bandejaBean.setValor3(control.getTxtNroRequerimiento());
	 	 bandejaBean.setValor4(control.getFecInicio());
	 	 bandejaBean.setValor5(control.getFecFinal());
	 	 bandejaBean.setValor6(control.getCodCeco());
	 	 bandejaBean.setValor7(control.getUsuSolicitante());
	 	 bandejaBean.setValor8(control.getCodEstado());
	 	 bandejaBean.setValor9(control.getNroGuia());
	 	 //ALQD,23/01/09. NUEVO PARAMETRO DE FILTRO
	 	 bandejaBean.setValor10(control.getIndSalPorConfirmar());
	 		 	 
	 	 log.info("GuardarDatosBandeja");
	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	  }
    
    
	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
    
}
