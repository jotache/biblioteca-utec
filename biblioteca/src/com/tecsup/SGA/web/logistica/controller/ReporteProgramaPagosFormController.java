package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.logistica.command.ReporteProgramaPagosCommand;

public class ReporteProgramaPagosFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(ReporteProgramaPagosFormController.class);
	
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
     log.info("formBackingObject:INI");
     
     ReporteProgramaPagosCommand command= new ReporteProgramaPagosCommand();
     
     command.setCodUsuario(request.getParameter("txhCodUsuario"));
 	 command.setCodTipoReporte(request.getParameter("txhCodTipoReporte"));
 	 command.setCodSede(request.getParameter("txhCodSede"));
 	 command.setDscUsuario(request.getParameter("txhDscUsuario"));
 	 command.setConsteRadio(CommonConstants.COD_REP_VALIDADO);
 	 command.setConsteValidado(CommonConstants.COD_REP_VALIDADO);
 	 command.setConsteVencido(CommonConstants.COD_REP_VENCIDO);
 	 command.setConsteSede(request.getParameter("txhCodSede"));
 	 
 	 if ( command.getCodUsuario()!= null )
 	 {
 		command.setListaSedes(this.seguridadManager.getAllSedes());
 	 }
 	List lista1= new ArrayList();
 	List lista2= new ArrayList();
 	
	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    if(lista1!=null)
	command.setListaCodTipoOrden(lista1);
    
    lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_ESTADO_LOG 
 			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
     if(lista2!=null)
 	command.setListaCodEstado(lista2);
     
     ListaTipoPago(command);
 	 
 	 System.out.println("CodUsuario: "+command.getCodUsuario()+">> CodTipoReporte: "+command.getCodTipoReporte());
 	
 	String fecha = Fecha.getFechaActual();
	String[] arrFecha = fecha.split("/");
	command.setFecInicio("01/" + arrFecha[1] + "/" + arrFecha[2]);
	command.setFecFinal(Fecha.getDiasPorMes(arrFecha[1], arrFecha[2]) + "/" + arrFecha[1] + "/" + arrFecha[2]);
	
     
   	 log.info("formBackingObject:FIN");
     return command;
 }
	
	protected void initBinder(HttpServletRequest request,
         ServletRequestDataBinder binder) {
 	NumberFormat nf = NumberFormat.getNumberInstance();
 	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
 	
 }
 /**
  * Redirect to the successView when the cancel button has been pressed.
  */
	public ModelAndView processFormSubmission(HttpServletRequest request,
                                           HttpServletResponse response,
                                           Object command,
                                           BindException errors)
 	throws Exception {
     //this.onSubmit(request, response, command, errors);
 	return super.processFormSubmission(request, response, command, errors);
 	}
	
 	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
         BindException errors)
 		throws Exception {
 	log.info("onSubmit:INI");
 	
	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
	if(usuarioSeguridad==null){
		InicioCommand control = new InicioCommand();
		return new ModelAndView("/cabeceraLogistica","control",control);
	} 	
 	
 	ReporteProgramaPagosCommand control= (ReporteProgramaPagosCommand) command;
 	
   	log.info("onSubmit:FIN");		
    return new ModelAndView("/logistica/consultaReportes/rep_ProgramaPagos","control",control);
}

 	private void ListaTipoPago(ReporteProgramaPagosCommand control){
	 	//carga combo tipo de pago
		 List lista1= new ArrayList();
	 	TipoLogistica obj = new TipoLogistica();    	
	 	obj.setCodPadre("");
	 	obj.setCodId("");
	 	obj.setCodTabla(CommonConstants.TIPT_TIPO_PAGO);
	 	obj.setCodigo("");
	 	obj.setDescripcion("");
	 	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
	 	lista1=this.detalleManager.GetAllTablaDetalle(obj);
	 	if(lista1!=null)
	    	control.setListaCodTipoPago(lista1);
	 	//***********************************
	 	
	 }
 	
 	
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	

}
