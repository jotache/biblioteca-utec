package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoTipoGastoCommand;

public class MantenimientoTipoGastoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoTipoGastoFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public MantenimientoTipoGastoFormController() {    	
        super();        
        setCommandClass(MantenimientoTipoGastoCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoTipoGastoCommand command = new MantenimientoTipoGastoCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
   
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	
    	command.setListGastos(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listGastos", command.getListGastos());
    
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado = "";
    	MantenimientoTipoGastoCommand control = (MantenimientoTipoGastoCommand) command;
    	if (control.getOperacion().trim().equals(""))
    	{
    		TipoLogistica obj = new TipoLogistica();
        	
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
        	
        	control.setListGastos(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listGastos", control.getListGastos());
        
    	}
    	else if (control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		resultado = delete(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    			}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("DOBLE");
    		}
    		else {
    			control.setMsg("OK");
    			}
    		TipoLogistica obj = new TipoLogistica();
        	
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
        	
        	control.setListGastos(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listGastos", control.getListGastos());
    
    	}
    	
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Tipo_Gastos","control",control);		
    }
        
        private String delete(MantenimientoTipoGastoCommand control){
        	
        	control.setEliminar(this.detalleManager.DeleteTablaDetalle(CommonConstants.TIPT_TIPO_GASTOS
        			, control.getSecuencial(), control.getCodAlumno()));
        	
        	return control.getEliminar();
        }
        
        
       
}
