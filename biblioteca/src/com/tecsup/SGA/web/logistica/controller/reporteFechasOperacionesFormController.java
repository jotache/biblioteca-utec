package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.ReporteCierreOperacionesCommand;

public class reporteFechasOperacionesFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(reporteFechasOperacionesFormController.class);
	
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	ReporteCierreOperacionesCommand command= new ReporteCierreOperacionesCommand();
    	
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setCodTipoReporte(request.getParameter("txhCodTipoReporte"));
    	command.setCodSede(request.getParameter("txhCodSede"));
    	command.setDscUsuario(request.getParameter("txhDscUsuario"));
    	command.setConsteSede(request.getParameter("txhCodSede"));
    	command.setListaSedes(this.seguridadManager.getAllSedes());
    	
    	System.out.println("CodUsuario: "+command.getCodUsuario()+">> CodTipoReporte: "+command.getCodTipoReporte());
    	
    	command.setTituloReporte("WilDATilA");
    	command.setNroReporte("1");
    	
    	command.setConsteRadio("1");
    	command.setConsteBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    	command.setConsteServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
    	command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	
    	    	    	
    	command.setValorIndGrupo("0");
    	command.setConsteGrupoGenerales(CommonConstants.SOL_AREQ_GRUPOS_GENERALES);
    	command.setConsteGrupoMantenimiento(CommonConstants.SOL_AREQ_GRUPOS_MANTENIMIENTO);
    	command.setConsteGrupoServicios(CommonConstants.SOL_AREQ_GRUPOS_SERVICOS);
    	
    	command.setCodReporte5(CommonConstants.REPORTE_LOG_5);
    	command.setCodReporte7(CommonConstants.REPORTE_LOG_7);
    	command.setCodReporte9(CommonConstants.REPORTE_LOG_9);
    	command.setCodReporte29(CommonConstants.REPORTE_LOG_29);
    	
    	List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();
    	List lista5= new ArrayList();
    	List lista6= new ArrayList();
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    		command.setListCodTipoRequerimiento(lista1);
    	else command.setListCodTipoRequerimiento(new ArrayList());
    	
    	lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_GASTOS 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista2!=null)
    		command.setListaCodTipoGastos(lista2);
    	else command.setListaCodTipoGastos(new ArrayList());
    	
    	//LlenarComboCentroCosto(command);
    	
    	String fecha = Fecha.getFechaActual();
    	String[] arrFecha = fecha.split("/");
    	
    	command.setFecInicio("01/" + arrFecha[1] + "/" + arrFecha[2]);
    	command.setFecFinal(Fecha.getDiasPorMes(arrFecha[1], arrFecha[2]) + "/" + arrFecha[1] + "/" + arrFecha[2]);
    	    	    	
    	lista5=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRUPO_SERVICIO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista5!=null)
    	command.setListaCodGrupoServicio(lista5);
    	
    	lista6=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista6!=null)
    		command.setListaCodEstadoReq(lista6);
    	else command.setListaCodEstadoReq(new ArrayList());
    	
    	lista3=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRUPO_SERVICIO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista3!=null)
    	command.setListaCodGrupoServicio(lista3);
    	
    	log.info("formBackingObject:FIN");
         return command;
     }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
	
	
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
	throws Exception {
	//this.onSubmit(request, response, command, errors);
	return super.processFormSubmission(request, response, command, errors);
	}
	
		
	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
	
	
}
