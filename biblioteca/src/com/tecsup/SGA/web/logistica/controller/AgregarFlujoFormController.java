package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarFlujoCommand;

public class AgregarFlujoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarFlujoFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public AgregarFlujoFormController() {    	
        super();        
        setCommandClass(AgregarFlujoCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarFlujoCommand command = new AgregarFlujoCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setCodigo((String)request.getParameter("txhCodigo"));
    	command.setCodReq((String)request.getParameter("txhTipoReq"));
    	command.setCodSede((String)request.getParameter("txhSede"));
    	System.out.println("codalu: "+command.getCodAlumno());
    	System.out.println("codigo: "+command.getCodigo());
    	System.out.println("codreq: "+command.getCodReq());
    	System.out.println("codSed: "+command.getCodSede());
    	TipoLogistica obj = new TipoLogistica();
    		
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_PERSONAL);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	command.setListPersonal(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listPersonal", command.getListPersonal());
    	
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	AgregarFlujoCommand control = (AgregarFlujoCommand) command;
    	if("GRABAR".equals(control.getOperacion())){
    		resultado = graba(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else control.setMsg("OK");
    	}
    	else if("BUSCA".equals(control.getOperacion())){
    		busca(control);
    		request.getSession().setAttribute("listEmpleados", control.getListEmpleados());
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Flujo","control",control);		
    }
        
        private String graba(AgregarFlujoCommand control){
        	System.out.println("codReq: "+control.getCodReq());
        	System.out.println("codigo: "+control.getCodigo());
        	System.out.println("codRes: "+control.getCodResponsable());
        	System.out.println("codAlu: "+control.getCodAlumno());
        	System.out.println("codSed: "+control.getCodSede());
        	System.out.println("-------------------");
        	String codEvaluacion;
        	StringTokenizer stkEvaluaciones = new StringTokenizer(control.getCodigo(),"|");
        	while ( stkEvaluaciones.hasMoreTokens() )
    		{
        		codEvaluacion = stkEvaluaciones.nextToken();
        		
        	control.setGraba(this.detalleManager.InsertAtencionSolicitante(control.getCodReq()
        			, codEvaluacion, control.getCodResponsable(), control.getCodAlumno(),control.getCodSede()));
    		}
        	return control.getGraba();
        }
        private void busca(AgregarFlujoCommand control){
        	System.out.println("codPer: "+control.getCodPer());
        	System.out.println("nombre: "+control.getNombre());
        	System.out.println("apPAt : "+control.getApPat());
        	System.out.println("apMat : "+control.getApMat());
        	control.setListEmpleados(this.detalleManager.GetAllEmpleados(control.getCodPer()
        			, control.getNombre(), control.getApPat(), control.getApMat(), control.getCodSede()));
        }
}
