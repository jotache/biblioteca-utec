package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.OrdenesAprobadas;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.GestDocConsultarDocumentosPagosAsociadosCommand;

public class GestDocConsultarDocumentosPagosAsociadosFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(GestDocConsultarDocumentosPagosAsociadosFormController.class);
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	GestionDocumentosManager gestionDocumentosManager;
	
	public GestionDocumentosManager getGestionDocumentosManager() {
		return gestionDocumentosManager;
	}

	public void setGestionDocumentosManager(
			GestionDocumentosManager gestionDocumentosManager) {
		this.gestionDocumentosManager = gestionDocumentosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    log.info("formBackingObject:INI");
	
    GestDocConsultarDocumentosPagosAsociadosCommand command=new GestDocConsultarDocumentosPagosAsociadosCommand();
    
    command.setCodSede(request.getParameter("txhCodSede"));
    command.setCodOrden(request.getParameter("txhCodOrden"));
    command.setCodEstado(request.getParameter("txhCodEstado"));
    command.setCodUsuario(request.getParameter("txhCodUsuario"));
    command.setIndDocPago(request.getParameter("txhIndDocPago"));
    
    command.setConsteCodEstado(CommonConstants.COD_DOC_PAG_ASOCIADOS_PENDIENTE);
    command.setConsteCodEstadoEnviado(CommonConstants.COD_DOC_PAG_ESTADO_ENVIADO);
    
    command.setCodEstadoOrden(request.getParameter("txhCodEstado"));
    //ALQD,16/09/09.LEYENDO EL CODPERFIL DEL USUARIO
    command.setCodPerfil(request.getParameter("txhCodPerfil"));
    System.out.println("codPerfil: "+command.getCodPerfil());
    BandejaInicial(command);
    BuscarBandejaMedio(command);
   	log.info("formBackingObject:FIN");
	
  	return command;
   }
 	
 	protected void initBinder(HttpServletRequest request,
           ServletRequestDataBinder binder) {
   	NumberFormat nf = NumberFormat.getNumberInstance();
   	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
   	
   }
   /**
    * Redirect to the successView when the cancel button has been pressed.
    */
   public ModelAndView processFormSubmission(HttpServletRequest request,
                                             HttpServletResponse response,
                                             Object command,
                                             BindException errors)
   throws Exception {
   	return super.processFormSubmission(request, response, command, errors);
   }
 
   public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
           BindException errors)
   		throws Exception {
   	log.info("onSubmit:INI");
   	
   	GestDocConsultarDocumentosPagosAsociadosCommand control= (GestDocConsultarDocumentosPagosAsociadosCommand) command;
   	String resultado="";
   	String enviarDoc="";
   	if(control.getOperacion().equals("ANULAR"))
   	{  
   		resultado= AnularDocumento(control);
   		if(resultado.equals("0")) control.setMsg("OK");
   		else if(resultado.equals("-1")) control.setMsg("ERROR");
   		BandejaInicial(control);
	    BuscarBandejaMedio(control);
   	}
   	else{ if(control.getOperacion().equals("ENVIAR"))
   			{  enviarDoc=Enviar(control);
   			   if(enviarDoc.equals("0")) control.setMsg("ENVIAR_OK");
   	   		   else if(enviarDoc.equals("-1")) control.setMsg("ENVIAR_ERROR");
   			   BandejaInicial(control);
		       BuscarBandejaMedio(control);
   			}
   		  else{ if(control.getOperacion().equals("BUSCAR"))
   				{  BandejaInicial(control);
   			       BuscarBandejaMedio(control);
   				}
   		  		else{ if(control.getOperacion().equals("BUSCAR_IFRAME"))
   		  				{ BandejaInicial(control);
   	   			          BuscarBandejaMedio(control);
   	   			          control.setMsg("Buscar_iFrame");
   		  				}
   		  		}
   		  }
   	}
   	
   	log.info("onSubmit:FIN");
    return new ModelAndView("/logistica/GestionDocumentos/Gest_Doc_Consultar_Documentos_Pagos_Asociados","control",control);
}

   //ALQD,16/09/09.SI CODPERFIL CONTIENE LCONS => CONTENDRA SOLO ESE VALOR
   public void BandejaInicial(GestDocConsultarDocumentosPagosAsociadosCommand control)
   { List lista= new ArrayList();
     System.out.println("CodOrden: "+control.getCodOrden());
	 lista=this.gestionDocumentosManager.GetAllDatosOrden(control.getCodOrden());
	 if(lista!=null)
	 { if(lista.size()>0)
	   { OrdenesAprobadas ordenesAprobadas= new OrdenesAprobadas();
	     ordenesAprobadas=(OrdenesAprobadas)lista.get(0);
	     control.setNroOrden(ordenesAprobadas.getNroOrden().trim());
	     control.setFecEmision(ordenesAprobadas.getFechaEmision().trim());
	     control.setTxtMontoOrden(ordenesAprobadas.getMonto().trim());
	     control.setTxtNroRucProveedor(ordenesAprobadas.getDscProveedor().trim());
	     control.setTxtNombreProveedor(ordenesAprobadas.getDscRazonSocial().trim());
	     control.setTxtMontoFacturadoValido(ordenesAprobadas.getMontoFacturado().trim());
	     control.setTxtSaldoValidado(ordenesAprobadas.getMontoSaldoVal().trim());
	     control.setTxtSaldoPendiente(ordenesAprobadas.getMontoSaldo().trim());
	     control.setTxtMoneda(ordenesAprobadas.getDscMoneda().trim());
	     
	     control.setIndImportacion(ordenesAprobadas.getIndImportacion().trim());
	     control.setImporteOtros(ordenesAprobadas.getImporteOtros());
		 //ALQD,27/05/09.PARA OBTENER EL IGV DE LA OC Y SI ES CERO ACTUALIZAR EL IGVDB
		 control.setIndAfectoIGV(ordenesAprobadas.getIndAfectoIGV());
		 if(control.getCodPerfil()!=null){
			 if(control.getCodPerfil().indexOf("LCONS")>-1){
				 control.setCodPerfil("LCONS");
			 }
		 }
	   }
	 }
	 
   }
   
   public void BuscarBandejaMedio(GestDocConsultarDocumentosPagosAsociadosCommand control){
	   List lista= new ArrayList();	   
	   lista=this.gestionDocumentosManager.GetAllDocPagosAsociados(control.getCodOrden());
	   if(lista!=null){
		   control.setListaBandejaMedio(lista);
		   control.setTamListaBandejaMedio(""+lista.size());
	   }
	   else{
		   control.setListaBandejaMedio(new ArrayList());
		   control.setTamListaBandejaMedio("0");
	   }
   }
   
   public String AnularDocumento(GestDocConsultarDocumentosPagosAsociadosCommand control)
   {	String resultado="";
        System.out.println(control.getCodDocumento()+">><<"+control.getCodUsuario());
   		resultado=this.gestionDocumentosManager.UpdateAnularDocPago(control.getCodDocumento(),
   				control.getCodUsuario());
       System.out.println("Resultado Anular Documento :D :"+resultado);
	   return resultado;
   }
   
   public String Enviar(GestDocConsultarDocumentosPagosAsociadosCommand control){
	  String resultado="";
	  resultado=this.gestionDocumentosManager.UpdateEnviarDocPago(control.getCodDocumento(),
   				control.getCodUsuario());
	  System.out.println("Resultado Enviar Documento :D :"+resultado);
	  return resultado;
   }
   
public TablaDetalleManager getTablaDetalleManager() {
	return tablaDetalleManager;
}

public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
	this.tablaDetalleManager = tablaDetalleManager;
}

public SeguridadManager getSeguridadManager() {
	return seguridadManager;
}

public void setSeguridadManager(SeguridadManager seguridadManager) {
	this.seguridadManager = seguridadManager;
}

public DetalleManager getDetalleManager() {
	return detalleManager;
}

public void setDetalleManager(DetalleManager detalleManager) {
	this.detalleManager = detalleManager;
}
}
