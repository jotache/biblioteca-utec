package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Almacen;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.RecBieRegistrarBienesCommand;

public class RecBieRegistrarBienesFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(RecBieRegistrarBienesFormController.class);
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	AlmacenManager almacenManager;
	SolRequerimientoManager solRequerimientoManager;
	
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    log.info("formBackingObject:INI");
    
    RecBieRegistrarBienesCommand command= new RecBieRegistrarBienesCommand();
    
    //command.setCodSede(request.getParameter("txhCodSede"));
    command.setCodOrden(request.getParameter("txhCodOrden"));
    //command.setCodEstado(request.getParameter("txhCodEstado"));
    command.setCodUsuario(request.getParameter("txhCodUsuario"));
    if(request.getParameter("txhCodSede")!=null)
    { command.setCodSede(request.getParameter("txhCodSede"));
	  log.info("CodSede: "+command.getCodSede());
      command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
      System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
    }
    command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN); 
    
    BuscarBienes(command);
    BuscarGuiDetalle(command);
    
 	log.info("formBackingObject:FIN");
	 
  	return command;
   }
 	
 	protected void initBinder(HttpServletRequest request,
           ServletRequestDataBinder binder) {
   	NumberFormat nf = NumberFormat.getNumberInstance();
   	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
   	
   }
   /**
    * Redirect to the successView when the cancel button has been pressed.
    */
   public ModelAndView processFormSubmission(HttpServletRequest request,
                                             HttpServletResponse response,
                                             Object command,
                                             BindException errors)
   throws Exception {
       //this.onSubmit(request, response, command, errors);
   	return super.processFormSubmission(request, response, command, errors);
   }
 	
   public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
           BindException errors)
   		throws Exception {
   	log.info("onSubmit:INI");
   	
   	RecBieRegistrarBienesCommand control= (RecBieRegistrarBienesCommand) command;
   	if(control.getOperacion().equals("BUSCAR"))
   	{ BuscarBienes(control);
      BuscarGuiDetalle(control);
   	}
 	log.info("onSubmit:FIN");		
    return new ModelAndView("/logistica/Almacen/Rec_Bie_Registrar_Entrega_Bienes","control",control);
   }
   
   public void BuscarBienes(RecBieRegistrarBienesCommand control)
   {
	   List lista= new ArrayList();
	   System.out.println("CodOrden: "+control.getCodOrden());
	   lista=this.almacenManager.GetAllDatosOrden(control.getCodOrden());
	   if(lista!=null)
	   {  if(lista.size()>0)
	   		{	Almacen almacen= new Almacen();
	   			almacen=(Almacen)lista.get(0);
	   			control.setNroOrden(almacen.getNroOrden());
	   			control.setFecEmision(almacen.getFecEmision());
	   			control.setTxtNroRucProveedor(almacen.getNroProveedor());
	   			control.setTxtNombreProveedor(almacen.getDscProveedor());
	   			control.setTxtAtencion(almacen.getDscAtencion());
	   			control.setTxtMonto(almacen.getMontoTotal().trim());
	   			control.setTxtComprador(almacen.getDscComprador());
	   			
	   		}
	   }
   }
   
   public void BuscarGuiDetalle(RecBieRegistrarBienesCommand control)
   {
	   List lista= new ArrayList();
	   lista=this.almacenManager.GetAllGuiaRemisionAsociadas(control.getCodOrden());
	   if(lista!=null)
	    control.setListaSuperior(lista);
	   else control.setListaSuperior(new ArrayList());
	   System.out.println("Size: "+control.getListaSuperior().size());
   }
   
   public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public AlmacenManager getAlmacenManager() {
		return almacenManager;
	}

	public void setAlmacenManager(AlmacenManager almacenManager) {
		this.almacenManager = almacenManager;
	}
   
}
