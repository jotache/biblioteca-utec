package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.CotSolGenerarOrdenCommand;


public class CotSolGenerarOrdenFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(CotSolGenerarOrdenFormController.class);
	//SolRequerimientoManager solRequerimientoManager;
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	CotSolGenerarOrdenCommand command= new CotSolGenerarOrdenCommand();
    	
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setCodSede(request.getParameter("txhCodSede"));
    	command.setCodTipoReq(request.getParameter("txhCodTipoReq"));
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion"));
    	command.setNroCotizacion(request.getParameter("txhNroCotizacion"));
    	command.setCodTipoPago(request.getParameter("txhCodTipoPago"));
    	command.setCodTipoPago2(request.getParameter("txhCodTipoPago"));
    	
    	if(command.getCodCotizacion()!=null)
    	 {LlenarBandejaSuperior(command);
    	 try{
    	    	if(command.getListaBandeja().size()>0){
    	    	    CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
    	    	    cotizacionProveedor=(CotizacionProveedor)command.getListaBandeja().get(0);
    	    	    
    	    	    if(cotizacionProveedor.getDscNroOrden().trim().equals("Sin Orden")){
    	    	    	request.setAttribute("WilDATilA", "OK");
    	    	        command.setDscProveedor(cotizacionProveedor.getRazonSocial());
    	    	        command.setCodProveedor(cotizacionProveedor.getCodProv());
    	    	        command.setDscNroOrden(cotizacionProveedor.getDscNroOrden());
    	    	        System.out.println("DscNroOrden2: "+cotizacionProveedor.getDscNroOrden().trim());
    	    	    }
    	    	    else System.out.println("DscNroOrden3: "+cotizacionProveedor.getDscNroOrden().trim());
    	    	}
    	    	LlenarBandejaInferior(command);
    	 	}
    	 	catch (Exception e){
    	 		System.out.println("Exception");
    	 	}
    	 }
    	
    	LlenarComboTipoPago(command);
    	
    	log.info("formBackingObject:FIN");
    	 
     	return command;
      }
  	
  	protected void initBinder(HttpServletRequest request,
              ServletRequestDataBinder binder) {
      	NumberFormat nf = NumberFormat.getNumberInstance();
      	binder.registerCustomEditor(Long.class,
  	                  new CustomNumberEditor(Long.class, nf, true));
      	
      }
      /**
       * Redirect to the successView when the cancel button has been pressed.
       */
      public ModelAndView processFormSubmission(HttpServletRequest request,
                                                HttpServletResponse response,
                                                Object command,
                                                BindException errors)
      throws Exception {
          //this.onSubmit(request, response, command, errors);
      	return super.processFormSubmission(request, response, command, errors);
      }
  	
      public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
              BindException errors)
      		throws Exception {
      	log.info("onSubmit:INI");
      	
      	CotSolGenerarOrdenCommand control= (CotSolGenerarOrdenCommand) command;
      	String resultado="";
      	log.info("CodTipoPago: "+control.getCodTipoPago());
      	
      	if(control.getOperacion().equals("GENERAR"))
      	{   System.out.println("Generar");
      		LlenarBandejaSuperior(control);
      		LlenarBandejaInferior(control);
      		request.setAttribute("WilDATilA", "OK");
      	}
      	else{if(control.getOperacion().equals("GUARDAR"))
      		 {
      			resultado=Guardar(control);
      			if(resultado.equals("0")) control.setMsg("OK");
      			else if(resultado.equals("-1")) control.setMsg("ERROR");
      		 }
      		else{ if(control.getOperacion().equals("BUSCAR"))
      			  {  
      					LlenarBandejaSuperior(control);
      					LlenarComboTipoPago(control);
      			  }
      		}
      	}
      	
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/CotBienesYServicios/CotSol_Generar_Orden","control",control);
    }
    
      public void LlenarBandejaSuperior(CotSolGenerarOrdenCommand control)
      { List listaSuperior= new ArrayList(); 
        System.out.println("CotCotizacion: "+control.getCodCotizacion());
       listaSuperior=this.cotBienesYServiciosManager.GetAllBandejaGenerarOrdenCompra(control.getCodCotizacion());
    	control.setCodTipoPago(control.getCodTipoPago2());
       if(listaSuperior!=null)
         {	control.setListaBandeja(listaSuperior);
            if(listaSuperior.size()==1){ control.setEstado("UNICO");
               control.setBanListaBandeja("1");
            }
            else{ control.setEstado("OTROS");
                  if(listaSuperior.size()<1) control.setBanListaBandeja("0");
                  else control.setBanListaBandeja("1");
            }
         }
       else{ control.setListaBandeja(new ArrayList());
       		 control.setBanListaBandeja("0");
       		control.setEstado("VACIO");
       }
      }
   
      public void LlenarBandejaInferior(CotSolGenerarOrdenCommand control){
    	  List listaInferior= new ArrayList();
    	  listaInferior=this.cotBienesYServiciosManager.GetAllCotizacionByProveedor(control.getCodCotizacion(),
    			 control.getCodProveedor());
    	  if(listaInferior!=null){
    		  control.setListaBandejaInferior(listaInferior);
    		  control.setLongitud(String.valueOf(listaInferior.size()));
    		  if(listaInferior.size()<1)
    			  control.setBanListaBandejaInferior("0");
    		  else 
    			  control.setBanListaBandejaInferior("1");
		  }
    	  else{
    		  control.setListaBandejaInferior(new ArrayList());
      		  control.setBanListaBandejaInferior("0");
  		  }
    	  for(int i=0; i < listaInferior.size(); i++){
    		  CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
    		  cotizacionProveedor = (CotizacionProveedor)listaInferior.get(i);
    		  System.out.println("objeto->>"+i+"<<");
    		  System.out.println(">>"+cotizacionProveedor.getDscDetalleCoti()+"<<");
    		  System.out.println(">>"+cotizacionProveedor.getDscDetalleProv()+"<<");
    	  }
      }

      public void LlenarComboTipoPago(CotSolGenerarOrdenCommand control){
    	List lista1= new ArrayList();
  	 	TipoLogistica obj = new TipoLogistica();    	
  	 	obj.setCodPadre("");
  	 	obj.setCodId("");
  	 	obj.setCodTabla(CommonConstants.TIPT_TIPO_PAGO);
  	 	obj.setCodigo("");
  	 	obj.setDescripcion("");
  	 	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
  	 	lista1=this.detalleManager.GetAllTablaDetalle(obj);
  	 	if(lista1!=null)
  	    	control.setListaCodTipoPago(lista1);
      }
      
    public String Guardar(CotSolGenerarOrdenCommand control){
    	String resultado="";
    	System.out.println(">>CodCotizacion<<"+control.getCodCotizacion()+">>CodProveedor<<"+ 
    			control.getCodProveedor()+">>CadCodigosDetalle<<"+control.getCadCodigosDetalle()+
    			">>CadDescripcionDetalle<<"+control.getCadDescripcionDetalle()
    			+">>Longitud<<"+ 
    			control.getLongitud()+">>CodUsuario<<"+control.getCodUsuario());
    	        resultado=this.cotBienesYServiciosManager.InsertCotSolGenerarOrdenCompraByProveedor(control.getCodCotizacion(), 
    			control.getCodProveedor(), control.getCadCodigosDetalle(), control.getCadDescripcionDetalle(), 
    			control.getLongitud(), control.getCodUsuario());
    	System.out.println("Resultado: "+resultado);      
    	return resultado;
    }  
      
	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
}
