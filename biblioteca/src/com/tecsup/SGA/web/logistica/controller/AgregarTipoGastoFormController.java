package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarTipoGastoCommand;

public class AgregarTipoGastoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarTipoGastoFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public AgregarTipoGastoFormController() {    	
        super();        
        setCommandClass(AgregarTipoGastoCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarTipoGastoCommand command = new AgregarTipoGastoCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setSecuencial((String)request.getParameter("txhSecuencial"));
    	command.setTipGas((String)request.getParameter("txhDesc"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setCodigo((String)request.getParameter("txhCodigo"));
    	command.setCtaContable((String)request.getParameter("txhCtaCont"));
    	System.out.println("cta: "+command.getCtaContable());
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado="";
    	AgregarTipoGastoCommand control = (AgregarTipoGastoCommand) command;
    	
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
    		resultado = modifica(control);	
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if(resultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
    		
    		}
    		
    		else{	
	    		resultado = grabar(control);
	    		if ( resultado.equals("-1")) {
	    			control.setMsg("ERROR");
	    		}
	    		else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
	        	else control.setMsg("OK");
    		}
    	}
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_TipoGasto","control",control);		
    }
        
        private String grabar(AgregarTipoGastoCommand control){
        	System.out.println("1.- "+"");
        	System.out.println("2.- "+CommonConstants.TIPT_TIPO_GASTOS);
        	System.out.println("3.- "+control.getTipGas());
        	System.out.println("4.- "+control.getCtaContable());
        	System.out.println("5.- "+control.getCodigo());
        	System.out.println("6.- "+"");
        	System.out.println("7.- "+control.getCodAlumno());
        	control.setGraba(this.detalleManager.InsertTablaDetalle(control.getCtaContable()
        			, CommonConstants.TIPT_TIPO_GASTOS, control.getTipGas(),"" , control.getCodigo(), "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        
        private String modifica(AgregarTipoGastoCommand control){
        	
        	control.setGraba(this.detalleManager.UpdateTablaDetalle(control.getCtaContable()
        			, CommonConstants.TIPT_TIPO_GASTOS, control.getSecuencial(), "", control.getTipGas(), "", control.getCodigo(), "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        
}
