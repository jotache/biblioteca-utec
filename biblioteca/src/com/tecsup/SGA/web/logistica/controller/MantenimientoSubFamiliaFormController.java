package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoSubFamiliaCommand;

public class MantenimientoSubFamiliaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoSubFamiliaFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public MantenimientoSubFamiliaFormController() {    	
        super();        
        setCommandClass(MantenimientoSubFamiliaCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	request.getSession().removeAttribute("listSubFamilia");
    	MantenimientoSubFamiliaCommand command = new MantenimientoSubFamiliaCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	
    	command.setListTipoBien(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listTipoBien", command.getListTipoBien());
    	
    	        log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	MantenimientoSubFamiliaCommand control = (MantenimientoSubFamiliaCommand) command;
    	System.out.println("ope: "+control.getOperacion());
    	if (control.getOperacion().trim().equals("BUSCAR"))
    	{
    		muestra(control);
    	control.setListSubFamilia(this.detalleManager.GetAllSubFamilias(control.getCodTipo()
    			, control.getCodFam(), CommonConstants.TIPO_ORDEN_DSC,"","",""));
    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
    	}
    	else if (control.getOperacion().trim().equals("ELIMINAR"))
    	{
    	resultado = delete(control);
    	if ( resultado.equals("-1")) {
			control.setMsg("ERROR");
			}
    	else if ( resultado.equals("-2")) {
    		control.setMsg("DOBLE");
    	}
		else {
			control.setMsg("OK");
			}
    	muestra(control);
    	control.setListSubFamilia(this.detalleManager.GetAllSubFamilias(control.getCodTipo()
    			, control.getCodFam(), CommonConstants.TIPO_ORDEN_DSC,"","",""));
    	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());
    	
    	}
    	else if (control.getOperacion().trim().equals("MUESTRA"))
    	{
    		muestra(control);
    	   	control.setListSubFamilia(this.detalleManager.GetAllSubFamilias(control.getCodTipo()
        			, control.getCodFam(), CommonConstants.TIPO_ORDEN_DSC,"","",""));
        	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());

    	     
    	}
    	else if (control.getOperacion().trim().equals(""))
    	{
    		muestra(control);
    	   	control.setListSubFamilia(this.detalleManager.GetAllSubFamilias(control.getCodTipo()
        			, control.getCodFam(), CommonConstants.TIPO_ORDEN_DSC,"","",""));
        	request.getSession().setAttribute("listSubFamilia", control.getListSubFamilia());

    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_SubFamilia","control",control);		
    }
        private String delete(MantenimientoSubFamiliaCommand control){
        
        	control.setEliminar(this.detalleManager.DeleteTablaDetalle(CommonConstants.TIPT_SUB_FAMILIA
        			, control.getCodUnico(), control.getCodAlumno()));
        	
        	return control.getEliminar();	
        }
        private void muestra(MantenimientoSubFamiliaCommand control){
        	if(control.getCodTipo().equals("")){
        	TipoLogistica obj = new TipoLogistica();
        	obj.setCodId(CommonConstants.TIPT_VALOR);
        	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
        	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
        	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	}
        	else{
        		TipoLogistica obj = new TipoLogistica();
            	obj.setCodId(control.getCodTipo());
            	obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
            	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
            	control.setListFamilia(this.detalleManager.GetAllTablaDetalle(obj));
            
        	}
        }
}
