package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.web.logistica.command.GestDocAprobarDocumentosPagoCommand;
import com.tecsup.SGA.web.logistica.command.GestDocPagoDocumentosPagoCommand;
import com.tecsup.SGA.web.logistica.command.RecepcionBienesCommand;

public class RecepcionBienesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RecepcionBienesFormController.class);
	  DetalleManager detalleManager;
	  AlmacenManager almacenManager;
	  SolRequerimientoManager solRequerimientoManager;
	    
	    public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
		public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public RecepcionBienesFormController() {    	
	        super();        
	        setCommandClass(RecepcionBienesCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	RecepcionBienesCommand command = new RecepcionBienesCommand();
	    	
	    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
	    	if ( usuarioSeguridad != null )
	    	{
	    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
	    		command.setCodOpciones(usuarioSeguridad.getOpciones());
	    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
	    	}
	    	
	    	//command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
	        //command.setSede((String)request.getParameter("txhCodSede"));
	        if(request.getParameter("txhCodSede")!=null)
	        { command.setSede(request.getParameter("txhCodSede"));
	    	  log.info("CodSede: "+command.getSede());
	          command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getSede()));
	          System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
	        }
	        
	        System.out.println("sede: "+command.getSede());
	        //ALQD,23/01/09. NUEVA CONSTANTE PARA FILTRAR PENDIENTES X CONFIRMAR
	        command.setConsteIndIngPorConfirmar("1");
	        System.out.println("ConsteIndIngPorConfirmar: "+command.getConsteIndIngPorConfirmar());
	        BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
	        if(bandejaBean!= null)
	         {  log.info("BandejaLogisticaBean:INICIO");
	           if(bandejaBean.getCodBandeja().equals("8")){
		         command.setNroOrden(bandejaBean.getValor1());
		         command.setFechaEmision(bandejaBean.getValor2());
		         command.setFechaEmision2(bandejaBean.getValor3());
		         command.setCodProveedor(bandejaBean.getValor4());
		         command.setProveedor(bandejaBean.getValor5());
		         command.setComprador(bandejaBean.getValor6());
		         command.setNroGuia(bandejaBean.getValor7());
		         command.setFechaEmision3(bandejaBean.getValor8());
		         command.setFechaEmision4(bandejaBean.getValor9());
		         //ALQD,23/01/09. A�ADIENDO NUEVO FILTRO DE BUSQUEDA
		         command.setIndIngPorConfirmar(bandejaBean.getValor10());
	    	 	 llena(command, request);
	          }
	       	 log.info("BandejaLogisticaBean:FIN");
	       	 
	         }
	        
	        log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	RecepcionBienesCommand control = (RecepcionBienesCommand) command;
	    	if(control.getOperacion().equals("BUSCAR")){
	    		llena(control, request);
	    	}
	    	
	 		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/Almacen/RecepcionBienes","control",control);		
	    }
	        private void llena(RecepcionBienesCommand control, HttpServletRequest request){
	        	
	        	GuardarDatosBandeja(control, request);
	        	
	        	control.setListCentroCostos(this.almacenManager.GetAllBienesRecepcion(control.getSede()
	        			, control.getNroOrden(), control.getFechaEmision(), control.getFechaEmision2()
	        			, control.getCodProveedor(), control.getProveedor(), control.getComprador()
	        			, control.getNroGuia(), control.getFechaEmision3(), control.getFechaEmision4(),
	        			control.getIndIngPorConfirmar()));
	        	System.out.println("tot: "+control.getListCentroCostos().size());
	        	request.getSession().setAttribute("listCentroCostos", control.getListCentroCostos());
	        }
	        
			public void setAlmacenManager(AlmacenManager almacenManager) {
				this.almacenManager = almacenManager;
			}
	
		public void GuardarDatosBandeja(RecepcionBienesCommand control,HttpServletRequest request)
	    {
	    	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	    	 	 
	    	 	 bandejaBean.setCodBandeja("8");
	    	 	 
	    	 	 bandejaBean.setValor1(control.getNroOrden());
	    	 	 bandejaBean.setValor2(control.getFechaEmision());
	    	 	 bandejaBean.setValor3(control.getFechaEmision2());
	    	 	 bandejaBean.setValor4(control.getCodProveedor());
	    	 	 bandejaBean.setValor5(control.getProveedor());
	    	 	 bandejaBean.setValor6(control.getComprador());
	    	 	 bandejaBean.setValor7(control.getNroGuia());
	    	 	 bandejaBean.setValor8(control.getFechaEmision3());
	    	 	 bandejaBean.setValor9(control.getFechaEmision4());
	    	 	 //ALQD,23/01/09. NUEVA PROPIEDAD DE BUSQUEDA
	    	 	 bandejaBean.setValor10(control.getIndIngPorConfirmar());
	    	 	 log.info("GuardarDatosBandeja");
	    	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	    }
	
}
