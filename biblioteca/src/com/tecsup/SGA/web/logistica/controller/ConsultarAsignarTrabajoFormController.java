package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.web.logistica.command.ConsultarAsignarTrabajoCommand;

public class ConsultarAsignarTrabajoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(ConsultarAsignarTrabajoFormController.class);
	  DetalleManager detalleManager;
	  SolRequerimientoManager solRequerimientoManager;  
	    public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public void setSolRequerimientoManager(
				SolRequerimientoManager solRequerimientoManager) {
			this.solRequerimientoManager = solRequerimientoManager;
		}
		public ConsultarAsignarTrabajoFormController() {    	
	        super();        
	        setCommandClass(ConsultarAsignarTrabajoCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	ConsultarAsignarTrabajoCommand command = new ConsultarAsignarTrabajoCommand();
	    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
	    	command.setCodReq((String)request.getParameter("txhCodReq"));
	    	command.setSede((String)request.getParameter("txhCodSede"));
	    	buscar(command);
	    	TipoLogistica obj = new TipoLogistica();
	        
	    	obj.setCodTabla(CommonConstants.TIPT_NIVEL_COMPLEJIDAD);
	    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
	    	command.setListComplejidad(this.detalleManager.GetAllTablaDetalle(obj));;
	    	request.getSession().setAttribute("listTrabajo", command.getListTrabajo());
	            log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	ConsultarAsignarTrabajoCommand control = (ConsultarAsignarTrabajoCommand) command;
	    	if("GRABAR".equals(control.getOperacion())){ 
	    		resultado = Grabar(control);
	    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
			else if(resultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
	    		buscar(control);
	    	}
	    	else if("BUSCAR".equals(control.getOperacion())){ 
	    		buscar(control);
	    	}
	    	else if("".equals(control.getOperacion())){ 
	    		buscar(control);
	    	}
	 		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/SolAproRequerimientos/log_ConsultarAsignarTrabajo","control",control);		
	    }
	     
	        private String Grabar(ConsultarAsignarTrabajoCommand control){
	        	control.setGrabar(this.solRequerimientoManager.InsertResponsableTrabajo(control.getCodReq()
	        			, control.getCodRes(), control.getCodComplejidad(), control.getCodAlumno()));
	        	
	        	System.out.println("R.-"+control.getGrabar()+"<<");
	        	return control.getGrabar();
	        }
	     
	        private void buscar(ConsultarAsignarTrabajoCommand control){
	        	control.setListTrabajo(this.solRequerimientoManager.GetAllAsigResp(CommonConstants.FLG_TIPO_PRESTAMO
	        			, control.getSede(), "", "", "", ""));
	        }
}
