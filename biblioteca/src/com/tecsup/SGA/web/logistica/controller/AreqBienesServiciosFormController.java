package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.AreqBienesServiciosCommand;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;

public class AreqBienesServiciosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AreqBienesServiciosFormController.class);
	private DetalleManager detalleManager;
	private SeguridadManager seguridadManager;
	private SolRequerimientoManager solRequerimientoManager;

	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public static void setLog(Log log) {
		AreqBienesServiciosFormController.log = log;
	}

	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	AreqBienesServiciosCommand command = new AreqBienesServiciosCommand();
    	//*********************************************************
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodTipoRequerimiento(request.getParameter("txhCodTipoRequerimiento") == null ? "": request.getParameter("txhCodTipoRequerimiento"));    	
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	//command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setIndicadorGrupo(request.getParameter("txhIndGrupo") == null ? "": request.getParameter("txhIndGrupo"));
    	command.setBanderaEnviar(request.getParameter("txhIndiceEnvio") == null ? "": request.getParameter("txhIndiceEnvio"));
    	//ALQD,31/10/08. SETEANDO LAS NUEVAS PROPIEDADES
    	//command.setIndInversion("0");
    	//command.setIndParaStock("0");
    	//ALQD,20/01/10. NUEVO VALOR SETEADO
    	//command.setIndEmpLogistica("0");
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		//command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    		if(command.getCodPerfil().indexOf(CommonConstants.COD_JEFE_LOG)!=-1
    			|| command.getCodPerfil().indexOf(CommonConstants.COD_JEFE_MANTTO)!=-1)
    			command.setJefeLog("1");
    		else command.setJefeLog("0");
    		System.out.println("JefeLog: "+command.getJefeLog());
    	}
    	
    	//*********************************************************
    	if(!command.getCodSede().equals(""))
        { command.setCodSede(request.getParameter("txhCodSede"));
    	  log.info("CodSede: "+command.getCodSede());
          command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
          System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
        }
        command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN);
                
        if(!command.getCodRequerimiento().equals("")){
    		cargaCabecera(command);
    	}
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");
    	System.out.println("codTipoRequerimiento="+command.getCodTipoRequerimiento()+"<<");
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");
    	System.out.println("indGrupo="+command.getIndicadorGrupo()+"<<");
    	System.out.println("banderaEnviar="+command.getBanderaEnviar()+"<<");
    	System.out.println("indParastock="+command.getIndParaStock()+"<<");
    	System.out.println("indInversion="+command.getIndInversion()+"<<");
    	System.out.println("Centro de Costo="+command.getCboTipoCentroCosto()+"<<");
    	System.out.println("indEmpLogistica="+command.getIndEmpLogistica()+"<<");
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	AreqBienesServiciosCommand control = (AreqBienesServiciosCommand)command;
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("APROBARBIEN")){
    		resultado = AprobarBien(control);
    		System.out.println("resultado de APROBAR BIEN-->"+resultado);
    		if (resultado.equals("0")){
    			control.setMsg("OK");
    		}
    		else{
    			if (resultado.equals("-1")){
        			control.setMsg("ERROR");
    			}
    			else { if(resultado.equals("-3"))
						control.setMsg("ERRORCANT");
				   	   else  if(resultado.equals("-4"))
				   		   	    control.setMsg("ALMACEN_CERRADO");
				   	   	     else control.setMsg("ERRORRESP");
    			}    			
    		}
    	}
    	else
		if (control.getOperacion().trim().equals("APROBARSERVICIO")){
			resultado = AprobarServicio(control);
    		System.out.println("resultado de APROBAR SERVICIO-->"+resultado);
    		if (resultado.equals("0")){
    			control.setMsg("OK");
    		}
    		else{
    			if (resultado.equals("-1")){
        			control.setMsg("ERROR");
    			}
    			else { if(resultado.equals("-3"))
    					control.setMsg("ERRORCANT");
    				   else if(resultado.equals("-4"))
			   		   	    	control.setMsg("ALMACEN_CERRADO");
    				   		else
    				   			control.setMsg("ERRORRESP");
    			}    			
    		}
    	}
		else
		if (control.getOperacion().trim().equals("ENVIARAPROBACION")){
			resultado = EnviarAprobacion(control);
    		System.out.println("resultado de ENVIAR APROBACION-->"+resultado);
    		if (resultado.equals("0")){
    			control.setMsg("OKENVIAR");
    		}
    		else{
    			if (resultado.equals("-1")){
        			control.setMsg("ERROR");
    			}
    			else {    if(resultado.equals("-4"))
   		   	    			control.setMsg("ALMACEN_CERRADO");
    					  else
    					  control.setMsg("ERRORRESP");
    			}    			
    		}
		}
		else
		if (control.getOperacion().trim().equals("RECHAZAR")){
			resultado = RechazarAprobacion(control);
    		System.out.println("resultado de RECHAZAR APROBACION-->"+resultado);
    		if (resultado.equals("0")){
    			control.setMsg("OKRECHAZAR");
    		}
    		else{	if(resultado.equals("-2"))
  	    				control.setMsg("ALMACEN_CERRADO");
			 		else
			 			control.setMsg("ERROR");    			
    		}
		}
    	cargaCabecera(control);
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/SolRequerimiento/Areq_Bienes_Servicios","control",control);
    }
    private void cargaCabecera(AreqBienesServiciosCommand control){
    	//tipo requerimiento bien
    	if(control.getCodTipoRequerimiento().equals("0001")){
    		cargaCabeceraBien(control);
    	}
    	//tipo requerimiento servicio
    	if(control.getCodTipoRequerimiento().equals("0002")){
    		cargaCabeceraServicio(control);
    	}
    }
    private void cargaCabeceraBien(AreqBienesServiciosCommand control){
    	//carga lista de combo centro de costo    	
    	control.setListaCentroCosto(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),"",""));    	
    	//entra y carga los atributos de la solicitud de requerimiento del usuario
    	cargaDatosSolicitudRequerimiento(control);
    	//************************************
    }
    private void cargaCabeceraServicio(AreqBienesServiciosCommand control){
    	//carga lista de combo centro de costo    	
    	control.setListaCentroCosto(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),"",""));
    	//************************************
    	//carga combo tipo de requerimiento
    	TipoLogistica obj = new TipoLogistica();    	
    	obj.setCodPadre("");
    	obj.setCodId("");
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_REQUERIMIENTO);
    	obj.setCodigo("");
    	obj.setDescripcion("");
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);    	
    	control.setListaRequerimiento(this.detalleManager.GetAllTablaDetalle(obj));
    	//***********************************
    	//carga lista de combo tipo de servicio
    	TipoLogistica obj3 = new TipoLogistica();
    	obj3.setCodPadre("");
    	obj3.setCodId("");
    	obj3.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
    	obj3.setCodigo("");
    	obj3.setDescripcion("");
    	obj3.setTipo(CommonConstants.TIPO_ORDEN_DSC);    	
    	control.setListaServicio(this.detalleManager.GetAllTablaDetalle(obj3));
    	//*****************************************
    	//entra y carga los atributos de la solicitud de requerimiento del usuario
    	cargaDatosSolicitudRequerimiento(control);
    	//************************************
    }
    private void cargaDatosSolicitudRequerimiento(AreqBienesServiciosCommand control){
    	if(!control.getCodRequerimiento().equals("")){
    		
    		SolRequerimiento solReqUsuario=this.solRequerimientoManager.getSolReqUsuario(control.getCodRequerimiento()).get(0);
    		
    		System.out.println("codigo sol req usuario-->"+solReqUsuario.getIdRequerimiento());
    		
    		control.setCodRequerimiento(solReqUsuario.getIdRequerimiento());
    		control.setNroRequerimiento(solReqUsuario.getNroRequerimiento());
    		control.setFechaRequerimiento(solReqUsuario.getFechaEmision());
    		control.setCboTipoCentroCosto(solReqUsuario.getCecoSolicitante());
    		control.setUsuSolicitante(solReqUsuario.getUsuSolicitante());
    		control.setCboTipoRequerimiento(solReqUsuario.getTipoRequerimiento());
    		control.setEstado(solReqUsuario.getEstadoReg());
    		control.setCodUsuarioResponsable(solReqUsuario.getUsuResponsable());
    		control.setUsuAsignado(solReqUsuario.getUsuAsignado());
    		//ALQD,31/10/08. SETEANDO LAS NUEVAS PROPIEDADES 
    		control.setIndInversion(solReqUsuario.getIndInversion());
    		control.setIndParaStock(solReqUsuario.getIndParaStock());
    		//ALQD,20/01/10. LEYENDO NUEVA PROPIEDAD
    		control.setIndEmpLogistica(solReqUsuario.getIndEmpLogistica());
    		if(control.getCboTipoRequerimiento().equals("0001")){
    			control.setCodSubTipoRequerimiento(solReqUsuario.getSubTipoRequerimiento());
    		}
    		else{
    			control.setCboTipoServicio(solReqUsuario.getSubTipoRequerimiento());
    		}
    		//carga la bandeja de la solicitud del usuario
    		cargaBandejaSolicitudDetalle(control);    		
    	}
    }
    private void cargaBandejaSolicitudDetalle(AreqBienesServiciosCommand control){
    	System.out.println("entra para cargar la bandeja");
    	if(control.getCodTipoRequerimiento().equals("0001")){
    		control.setListaSolicitudDetalle(this.solRequerimientoManager.getAllSolReqDetalle(control.getCodRequerimiento(), ""));    	
        	if(control.getListaSolicitudDetalle()!=null && control.getListaSolicitudDetalle().size()>0){
        		//carga el combo tipo de gasto solo si existen el detalle de la solicitud de req.
        		TipoLogistica obj = new TipoLogistica();    	
            	obj.setCodPadre("");
            	obj.setCodId("");
            	obj.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
            	obj.setCodigo("");
            	obj.setDescripcion("");
            	obj.setTipo(CommonConstants.TIPO_ORDEN_COD);    	
            	control.setListaGasto(this.detalleManager.GetAllTablaDetalle(obj));
        	}
    	}
    	if(control.getCodTipoRequerimiento().equals("0002")){
    		List listaSolReqDetalle=this.solRequerimientoManager.getAllSolReqDetalle(control.getCodRequerimiento(), "");
    		if(listaSolReqDetalle!=null && listaSolReqDetalle.size()>0){
    			SolRequerimientoDetalle solRequerimientoDetalle = (SolRequerimientoDetalle)listaSolReqDetalle.get(0);
    			control.setCodReqDetalleServicio(solRequerimientoDetalle.getCodigo());
    			control.setNombreServicio(solRequerimientoDetalle.getNombreServicio());
    			control.setFechaAtencion(solRequerimientoDetalle.getFechaEntrega());
    			control.setDescripcion(solRequerimientoDetalle.getDescripcion());
    		}
    	}
    }
    //ALQD,20/01/10.ACTUALIZANDO TRES CAMPOS MAS: INDINVERSION, INDPARASTOCK Y CCOSTO
    private String AprobarBien(AreqBienesServiciosCommand control){
    	try{
    		System.out.println("Centro de costo="+control.getCboTipoCentroCosto());
    		System.out.println("indParaStock="+control.getIndParaStock());
    		System.out.println("indInversion="+control.getIndInversion());
    		return this.solRequerimientoManager.AprobarSolRequerimientoUsuario(
    			control.getCodRequerimiento(),control.getCodUsuario()
    			,CommonConstants.SOL_REQ_USUARIO_APROBAR,control.getCadena()
    			,control.getCodPerfil(),control.getCboTipoCentroCosto()
    			,control.getIndInversion(),control.getIndParaStock());
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    //ALQD,20/01/10.ACTUALIZANDO DOS CAMPOS MAS: INDINVERSION Y CCOSTO
    private String AprobarServicio(AreqBienesServiciosCommand control){
    	try{
    		System.out.println("Centro de costo="+control.getCboTipoCentroCosto());
    		System.out.println("indInversion="+control.getIndInversion());
    		return this.solRequerimientoManager.AprobarSolRequerimientoUsuario(
    			control.getCodRequerimiento(),control.getCodUsuario()
    			,CommonConstants.SOL_REQ_USUARIO_APROBAR,"",control.getCodPerfil()
    			,control.getCboTipoCentroCosto(),control.getIndInversion(),"0");
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    //ALQD,20/01/10.ACTUALIZANDO DOS CAMPOS MAS: INDINVERSION Y CCOSTO
    private String EnviarAprobacion(AreqBienesServiciosCommand control){
    	try{
    		System.out.println("Centro de costo="+control.getCboTipoCentroCosto());
    		System.out.println("indInversion="+control.getIndInversion());
    		return this.solRequerimientoManager.AprobarSolRequerimientoUsuario(
    			control.getCodRequerimiento(),control.getCodUsuario()
    			,CommonConstants.SOL_REQ_USUARIO_ENVIAR_APROBAR,""
    			,control.getCodPerfil(),control.getCboTipoCentroCosto()
    			,control.getIndInversion(),"0");
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }    
    private String RechazarAprobacion(AreqBienesServiciosCommand control){
    	try{
    		return this.solRequerimientoManager.RechazarSolRequerimientoUsuario(control.getCodRequerimiento(), 
    				control.getCodUsuario());    		
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
}
