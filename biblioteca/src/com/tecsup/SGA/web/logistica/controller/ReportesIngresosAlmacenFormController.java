package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.PenBandejaAtencionCotizacionCommand;
import com.tecsup.SGA.web.logistica.command.ReportesIngresosAlmacenCommand;

public class ReportesIngresosAlmacenFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(ReportesIngresosAlmacenFormController.class);
	//SolRequerimientoManager solRequerimientoManager;
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    log.info("formBackingObject:INI");
    	
    	ReportesIngresosAlmacenCommand command= new ReportesIngresosAlmacenCommand();
    	
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setCodTipoReporte(request.getParameter("txhCodTipoReporte"));
    	command.setCodSede(request.getParameter("txhCodSede"));
    	command.setTituloReporte(request.getParameter("txhTituloReporte"));
    	command.setDscUsuario(request.getParameter("txhDscUsuario"));
    	command.setConsteSede(request.getParameter("txhCodSede"));
    	/*if ( command.getCodUsuario()!= null )
    	{
    		command.setListaSedes(this.seguridadManager.getAllSedes());
    	}*/
    	command.setListaSedes(this.seguridadManager.getAllSedes());
    	System.out.println("CodUsuario: "+command.getCodUsuario()+">> CodTipoReporte: "+command.getCodTipoReporte()
    			+">> CodSede: "+command.getCodSede());
    	    	
    	command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setCodReporte1(CommonConstants.REPORTE_LOG_1);
    	command.setCodReporte2(CommonConstants.REPORTE_LOG_2);
    	command.setCodReporte3(CommonConstants.REPORTE_LOG_3);
    	command.setCodReporte4(CommonConstants.REPORTE_LOG_4);
    	command.setCodReporte6(CommonConstants.REPORTE_LOG_6);
    	command.setCodReporte11(CommonConstants.REPORTE_LOG_011);
    	command.setValRadioRep(CommonConstants.COD_REP_BIEN_SERVICIO);
    	command.setConsteReporteAlmacen(CommonConstants.COD_REP_BIEN_SERVICIO);
    	command.setConsteReporteAlmacenDev(CommonConstants.COD_REP_DEVOLUCIONES);
    	
    	
    	Buscar_Lista_Familia(command);
    	
    	List lista= new ArrayList();
    	List lista3= new ArrayList();
    	
    	lista=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_GASTOS 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista!=null)
    	command.setListaCodTipoGastos(lista);
    	else command.setListaCodTipoGastos(new ArrayList());
    	
    	LlenarComboCentroCosto(command);
    	
    	String fecha = Fecha.getFechaActual();
    	String[] arrFecha = fecha.split("/");
    	command.setPeriodo(fecha);//arrFecha[1] + "/" + arrFecha[2]);
    	command.setFecInicio("01/" + arrFecha[1] + "/" + arrFecha[2]);
    	command.setFecFinal(Fecha.getDiasPorMes(arrFecha[1], arrFecha[2]) + "/" + arrFecha[1] + "/" + arrFecha[2]);
    	
    	Anios anios= new Anios();    	
    	command.setCodListaAnios(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
    	command.setCodListaMeses(anios.getMeses());
    	command.setPeriodoActual(CommonConstants.TIPT_PERIODO_ACTUAL);
     log.info("formBackingObject:FIN");
     return command;
 }
	
	protected void initBinder(HttpServletRequest request,
         ServletRequestDataBinder binder) {
 	NumberFormat nf = NumberFormat.getNumberInstance();
 	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
 	
 }
 /**
  * Redirect to the successView when the cancel button has been pressed.
  */
	 public ModelAndView processFormSubmission(HttpServletRequest request,
	                                           HttpServletResponse response,
	                                           Object command,
	                                           BindException errors)
	 throws Exception {
	     //this.onSubmit(request, response, command, errors);
	 	return super.processFormSubmission(request, response, command, errors);
	 }
		
	 public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	         BindException errors)
	 		throws Exception {
	 	log.info("onSubmit:INI");
	 	
	 	ReportesIngresosAlmacenCommand control= (ReportesIngresosAlmacenCommand) command;
	 	if(control.getOperacion().equals("FAMILIA"))
	 	{ Buscar_Lista_Familia(control);
	 	  LlenarComboCentroCosto(control);
	 	}
	 	else{  if(control.getOperacion().equals("SEDE"))
     			{
	 				LlenarComboCentroCosto(control);
     			}
	 			else{ if(control.getOperacion().equals("LIMPIAR"))
     				{
	 					Limpiar(control);
     				}
	 		
	 				}
	 		 
	 	}
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/consultaReportes/rep_IngresosAlmacen","control",control);
    }
	 
	 public void Buscar_Lista_Familia(ReportesIngresosAlmacenCommand control){
	    	List lista3= new ArrayList();
	    	System.out.println("ConsteRadio: "+control.getConsteRadio());
	    		    	
	    	TipoLogistica obj = new TipoLogistica();
	    	obj.setCodPadre(CommonConstants.TIPT_TIPO_BIEN);
			obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
			obj.setCodId(control.getConsteRadio());
	    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
	    	lista3=this.detalleManager.GetAllTablaDetalle(obj);
	    	if(lista3!=null)
	    	control.setListaCodFamilia(lista3);
	    	else control.setListaCodFamilia(new ArrayList());
	    }

	public void LlenarComboCentroCosto(ReportesIngresosAlmacenCommand command){
		List lista3= new ArrayList();
		if(command.getCodSede()!=null)
    	{	lista3=this.seguridadManager.GetAllCentrosCosto(command.getCodSede(),"","");
    		if(lista3!=null)
    			command.setListaCodTipoCentroCosto(lista3);
    		else command.setListaCodTipoCentroCosto(new ArrayList());
    	}
	}
	
	public void Limpiar(ReportesIngresosAlmacenCommand command){
		
		List lista3= new ArrayList();
		if(command.getCodSede()!=null)
    	{	lista3=this.seguridadManager.GetAllCentrosCosto(command.getConsteSede(),"","");
    		if(lista3!=null)
    			command.setListaCodTipoCentroCosto(lista3);
    		else command.setListaCodTipoCentroCosto(new ArrayList());
    	}
		
	}
	
	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
}
