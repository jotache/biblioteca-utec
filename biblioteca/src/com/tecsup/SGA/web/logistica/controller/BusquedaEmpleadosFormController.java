package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.BusquedaEmpleadosCommand;

public class BusquedaEmpleadosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(BusquedaEmpleadosFormController.class);
	  DetalleManager detalleManager;
	    
	    public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public BusquedaEmpleadosFormController() {    	
	        super();        
	        setCommandClass(BusquedaEmpleadosCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	BusquedaEmpleadosCommand command = new BusquedaEmpleadosCommand();
	    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
	    	command.setPosicion((String)request.getParameter("prmPos"));
	    	command.setSede((String)request.getParameter("txhSede"));
	    	System.out.println("sede: "+command.getSede());
	    	TipoLogistica obj = new TipoLogistica();
    		
	    	obj.setCodTabla(CommonConstants.TIPT_TIPO_PERSONAL);
	    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
	    	command.setListTipoPersonal(this.detalleManager.GetAllTablaDetalle(obj));
	    	request.getSession().setAttribute("listTipoPersonal", command.getListTipoPersonal());
	    	
	            log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	BusquedaEmpleadosCommand control = (BusquedaEmpleadosCommand) command;
	    	if("GRABAR".equals(control.getOperacion())){ 
	    		resultado = Grabar(control);
	    		if ( resultado.equals("-1")) {
      			control.setMsg("ERROR");
      		}
  			else if(resultado.equals("-2")){
  	    		control.setMsg("DOBLE");
  	    	}
          	else control.setMsg("OK");
	    		buscar(control);
	    	}
	    	else if("BUSCAR".equals(control.getOperacion())){ 
	    		buscar(control);
	    		request.getSession().setAttribute("listEmpleados", control.getListEmpleados());
	    	}
	    	else if("".equals(control.getOperacion())){ 
	    		buscar(control);
	    	}
	 		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/SolAproRequerimientos/log_BusquedaEmpleados","control",control);		
	    }
	     
	        private String Grabar(BusquedaEmpleadosCommand control){
	        	return control.getGrabar();
	        }
	     
	        private void buscar(BusquedaEmpleadosCommand control){
	        	control.setListEmpleados(this.detalleManager.GetAllEmpleados(control.getCodTipoPersonal()
	        			, control.getNombre(), control.getApePaterno(), control.getApeMaterno(), control.getSede()));
	        }
}
