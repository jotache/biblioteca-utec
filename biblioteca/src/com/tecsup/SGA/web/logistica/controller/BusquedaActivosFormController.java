package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CatalogoProducto;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
import com.tecsup.SGA.web.logistica.command.AgregarProductoCommand;
import com.tecsup.SGA.web.logistica.command.BusquedaActivosCommand;

public class BusquedaActivosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(BusquedaActivosFormController.class);
	  DetalleManager detalleManager;
	  GestionDocumentosManager gestionDocumentosManager;  
	    public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public BusquedaActivosFormController() {    	
	        super();        
	        setCommandClass(BusquedaActivosCommand.class);        
	    }
	    public void setGestionDocumentosManager(
				GestionDocumentosManager gestionDocumentosManager) {
			this.gestionDocumentosManager = gestionDocumentosManager;
		}
		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	BusquedaActivosCommand command = new BusquedaActivosCommand();
	    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
	    	command.setCodProducto((String)request.getParameter("txhCodProducto"));
	    	command.setSede((String)request.getParameter("txhCodSede"));
	    	command.setPosicion((String)request.getParameter("prmPos"));
	    	System.out.println("codPro: "+command.getCodProducto());
	    	System.out.println("sede  : "+command.getSede());
	    	System.out.println("posici: "+command.getPosicion());
	    	llenar(command);
	    	buscar(command);
	    	request.getSession().setAttribute("listProductos", command.getListProductos());
		    
	            log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	BusquedaActivosCommand control = (BusquedaActivosCommand) command;
	    	if("".equals(control.getOperacion())){ 
	    		buscar(control);
	        	request.getSession().setAttribute("listProductos", control.getListProductos());
	        	
	    	}
	 		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/SolAproRequerimientos/log_BusquedaActivos","control",control);		
	    }
	     
	        private String Grabar(BusquedaActivosCommand control){
	        	return control.getGrabar();
	        }
	     
	        private void buscar(BusquedaActivosCommand control){
	        	control.setListProductos(this.gestionDocumentosManager.GetAllActivosDisponibles(control.getCodProducto(),control.getSede()));
	        }
	        private void llenar(BusquedaActivosCommand control){
	        	control.setListProductos(this.detalleManager.GetAllProducto( control.getCodProducto(),
	        			control.getSede()
	        			, "", "",""
	        			, "", "", "","",""));
	        	List lst  = control.getListProductos();	        	
	        	if(lst!=null ){
	        		System.out.println("total   : "+lst.size());
	        		if ( lst.size()>0 )
	        		{
	        			CatalogoProducto catalogoProducto = null;
	        			catalogoProducto = (CatalogoProducto)lst.get(0); 
	        			
	        			control.setProducto(catalogoProducto.getNomProducto());
	        			control.setFamilia(catalogoProducto.getFamilia());
	        			control.setSubFamilia(catalogoProducto.getSubFamilia());
	        		}
	        	}
	        }
	   
			
}
