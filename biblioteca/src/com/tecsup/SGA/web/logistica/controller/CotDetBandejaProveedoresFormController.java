package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.logistica.command.CotDetBandejaProveedoresCommand;

public class CotDetBandejaProveedoresFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotDetBandejaProveedoresFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public CotDetBandejaProveedoresFormController() {    	
        super();        
        setCommandClass(CotDetBandejaProveedoresCommand.class);        
    }
	String cadProveedores="";
    String cadIndicadores="";
    String cantidad="";
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	CotDetBandejaProveedoresCommand command = new CotDetBandejaProveedoresCommand();
    	
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion"));
    	command.setCodCotizacionDet(request.getParameter("txhCodDetCotizacion"));
    	command.setCondicion(request.getParameter("txhCondicion"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	
    	if ( command.getCodCotizacion() != null  && 
    			command.getCodCotizacionDet() != null && 
    			command.getCodUsuario() != null )
    	{
    		command.setListaProveedor(
    				this.cotBienesYServiciosManager.getAllProveedoresByItem(
    				command.getCodCotizacion()
    				, command.getCodCotizacionDet())
    				);
    		CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
    		if(command.getListaProveedor().size()>0) 
            { cadProveedores="";
    		  for(int i=0;i<command.getListaProveedor().size();i++)
 	          { cotizacionProveedor=(CotizacionProveedor)command.getListaProveedor().get(i);
 	            if(cotizacionProveedor.getCodProv()!=null)
 	             { if(i==(command.getListaProveedor().size()-1))
 	                 cadProveedores=cadProveedores+cotizacionProveedor.getCodProv(); 
 	               else cadProveedores=cadProveedores+cotizacionProveedor.getCodProv()+"|";   
 	             }
 	           System.out.println("IndEnvioCorreo"+i+" : "+cotizacionProveedor.getIndEnvioCorreo());  
 	          }
            cantidad=String.valueOf(command.getListaProveedor().size());
            }
    		System.out.println("Size1: "+command.getListaProveedor().size());
    	}
    	
    	if(command.getListaProveedor()!=null)
    	command.setLongFlagCorreo(String.valueOf(command.getListaProveedor().size()));
    	else command.setLongFlagCorreo("0");
    	request.getSession().setAttribute("listaProveedores", command.getListaProveedor());
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	CotDetBandejaProveedoresCommand control = (CotDetBandejaProveedoresCommand) command;
		String rpt = "";
		//System.out.println("Size2: "+control.getListaProveedor().size());
		if (control.getOperacion().trim().equals("ELIMINAR"))
		{
			rpt = this.cotBienesYServiciosManager.deleteProveedorByItem(control.getCodCotizacion()
					, control.getCodCotizacionDet(), control.getCodProveedor(), control.getCodUsuario());
			if ( rpt == null ) control.setMsg("ERRORELI"); 
			else if ( rpt.trim().equals("-1")) control.setMsg("ERRORELI");
			else if ( rpt.trim().equals("0")) control.setMsg("OKELI");
			control.setListaProveedor(
    				this.cotBienesYServiciosManager.getAllProveedoresByItem(
    						control.getCodCotizacion()
    						, control.getCodCotizacionDet())
    				);
		}
		else{ if (control.getOperacion().trim().equals("BUSCAR"))
		{
			control.setListaProveedor(
    				this.cotBienesYServiciosManager.getAllProveedoresByItem(
    						control.getCodCotizacion()
    						, control.getCodCotizacionDet())
    				);
			CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
    		if(control.getListaProveedor().size()>0) 
            { cadProveedores="";
    		  for(int i=0;i<control.getListaProveedor().size();i++)
 	          { cotizacionProveedor=(CotizacionProveedor)control.getListaProveedor().get(i);
 	            if(cotizacionProveedor.getCodProv()!=null)
 	             { if(i==(control.getListaProveedor().size()-1))
 	                 cadProveedores=cadProveedores+cotizacionProveedor.getCodProv(); 
 	               else cadProveedores=cadProveedores+cotizacionProveedor.getCodProv()+"|";   
 	             }
 	           System.out.println("IndEnvioCorreo"+i+" : "+cotizacionProveedor.getIndEnvioCorreo());  
 	          }
            cantidad=String.valueOf(control.getListaProveedor().size());
            if(control.getListaProveedor()!=null)
            	control.setLongFlagCorreo(String.valueOf(control.getListaProveedor().size()));
            	else control.setLongFlagCorreo("0");
            }
    		System.out.println("Size1: "+control.getListaProveedor().size());
		}
			else{ if (control.getOperacion().trim().equals("GRABAR"))
					{ rpt=GuardarProveedores(control);
						if ( rpt == null ) control.setMsg("FALLO"); 
						else if ( rpt.trim().equals("-1")) control.setMsg("FALLO");
						else if ( rpt.trim().equals("0")) control.setMsg("EXITO");
					}
			}
		}
		control.setOperacion("");
		control.setCodProveedor("");
		if ( control.getCodCotizacion() != null  && 
				control.getCodCotizacionDet() != null && 
				control.getCodUsuario() != null )
    	{
			control.setListaProveedor(
    				this.cotBienesYServiciosManager.getAllProveedoresByItem(
    						control.getCodCotizacion()
    				, control.getCodCotizacionDet())
    				);
    		CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
    		if(control.getListaProveedor().size()>0) 
            {   cadProveedores="";
    			for(int i=0;i<control.getListaProveedor().size();i++)
 	           { cotizacionProveedor=(CotizacionProveedor)control.getListaProveedor().get(i);
 	            if(cotizacionProveedor.getCodProv()!=null)
 	             { if(i==(control.getListaProveedor().size()-1))
 	            	  cadProveedores=cadProveedores+cotizacionProveedor.getCodProv(); 
	               else cadProveedores=cadProveedores+cotizacionProveedor.getCodProv()+"|";  
 	             }
 	            System.out.println("IndEnvioCorreo"+i+" : "+cotizacionProveedor.getIndEnvioCorreo()); 
 	           }
            cantidad=String.valueOf(control.getListaProveedor().size());
            }
    		System.out.println("Size1: "+control.getListaProveedor().size());
    	}
    	request.getSession().setAttribute("listaProveedores", control.getListaProveedor());
    	//System.out.println("Size3: "+control.getListaProveedor().size());
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/CotBienesYServicios/CotDet_BandejaProveedores","control",control);		
    }
        public String GuardarProveedores(CotDetBandejaProveedoresCommand control)
        { String resultado="";
            
           System.out.println(">>CodCotizacion<<"+control.getCodCotizacion()+">>CodCotizacionDet<<"+ 
          		 control.getCodCotizacionDet()+">>cadProveedores<<"+cadProveedores+">>" +
          		 "CodIndicesSeleccionados<<"+control.getCodIndicesSeleccionados()
          		 +">>cantidad<<"+cantidad+">>CodUsuario<<"+control.getCodUsuario());
           resultado=this.cotBienesYServiciosManager.UpdateProveedorByItem(control.getCodCotizacion(), 
        		 control.getCodCotizacionDet(), cadProveedores, control.getCodIndicesSeleccionados(),
        		 cantidad, control.getCodUsuario());
         
         System.out.println("resultado: "+resultado); 
         return resultado;	
        }
}
