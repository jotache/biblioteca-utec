package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarUnidadMedidaCommand;

public class AgregarUnidadMedidaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarUnidadMedidaFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public AgregarUnidadMedidaFormController() {    	
        super();        
        setCommandClass(AgregarUnidadMedidaCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarUnidadMedidaCommand command = new AgregarUnidadMedidaCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setSecuencial((String)request.getParameter("txhSecuencial"));
    	command.setAbrev((String)request.getParameter("txhValor1"));
    	command.setNombre((String)request.getParameter("txhDescripcion"));
    	command.setNroDecimales((String)request.getParameter("txhValor2"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	AgregarUnidadMedidaCommand control = (AgregarUnidadMedidaCommand) command;
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
    			resultado = modifica(control);
    	    	if ( resultado.equals("-1")) {
    				control.setMsg("ERROR");
    			}
    	    	else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
    	    	else control.setMsg("OK");
    		}
    		else{
		    	resultado = grabar(control);
		    	if ( resultado.equals("-1")) {
					control.setMsg("ERROR");
				}
		    	else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
		    	}
		    	else control.setMsg("OK");
	    		}
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Unidad_Medida","control",control);		
    }
        private String grabar(AgregarUnidadMedidaCommand control){
        
        	control.setGrabar(this.detalleManager.InsertTablaDetalle(""
        			, CommonConstants.TIPT_UNIDAD_MEDIDA, control.getNombre()
        			, control.getAbrev(), control.getNroDecimales(), "", control.getCodAlumno()));
        	
        	return control.getGrabar();
        }
        
        private String modifica(AgregarUnidadMedidaCommand control){
        	
        	control.setGrabar(this.detalleManager.UpdateTablaDetalle(""
        			, CommonConstants.TIPT_UNIDAD_MEDIDA, control.getSecuencial(), "", control.getNombre(), control.getAbrev(), control.getNroDecimales(), "", control.getCodAlumno()));
        	
        	return control.getGrabar();
        }
        
}
