package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoTipoServicioCommand;

public class MantenimientoTipoServicioFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoTipoServicioFormController.class);
	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public MantenimientoTipoServicioFormController() {    	
        super();        
        setCommandClass(MantenimientoTipoServicioCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoTipoServicioCommand command = new MantenimientoTipoServicioCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_GRUPO_SERVICIO);
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	
    	command.setListServicios(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listServicios", command.getListServicios());
    	obj.setCodPadre(CommonConstants.TIPT_GRUPO_SERVICIO); 
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
    	command.setListGrupoFamilia(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listGrupoFamilia", command.getListGrupoFamilia());
    
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado = "";
    	MantenimientoTipoServicioCommand control = (MantenimientoTipoServicioCommand) command;
    	TipoLogistica obj = new TipoLogistica();
    	if (control.getOperacion().trim().equals("DETALLE"))
    	{
    		obj.setCodPadre(CommonConstants.TIPT_GRUPO_SERVICIO);
    		obj.setCodId(control.getCodServicio());
    		obj.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
    		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    		control.setListGrupoFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listGrupoFamilia", control.getListGrupoFamilia());
       	}
    	else if (control.getOperacion().trim().equals("ELIMINAR"))
    	{
    		resultado = delete(control);
    		if(resultado.equals("0")){
    			control.setMsg("OK");
    		}
    		else    			
    		if ( resultado.equals("-2")) {
    			control.setMsg("DOBLE");    			    		
    		}   
    		else{
    			control.setMsg("ERROR");
    		}
    			
    		obj.setCodPadre(CommonConstants.TIPT_GRUPO_SERVICIO);
    		obj.setCodId(control.getCodServicio());
    		obj.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
    		obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    		control.setListGrupoFamilia(this.detalleManager.GetAllTablaDetalle(obj));
        	request.getSession().setAttribute("listGrupoFamilia", control.getListGrupoFamilia());   
    	}
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_TiposServicio","control",control);		
    }
        
        private String delete(MantenimientoTipoServicioCommand control){
        	
        	control.setEliminar(this.detalleManager.DeleteTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO
        			, control.getSecuencial(),control.getCodAlumno()));
        	System.out.println(">>"+control.getEliminar()+"<<");
        	return control.getEliminar();
        }
        
        
        
        
}
