package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.CotBienesServiciosSolicitudCotizacionCommand;
import com.tecsup.SGA.web.logistica.command.PenBandejaAtencionCotizacionCommand;

public class CotBienesServiciosSolicitudCotizacionFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(CotBienesServiciosSolicitudCotizacionFormController.class);
	//SolRequerimientoManager solRequerimientoManager;
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	CotBienesServiciosSolicitudCotizacionCommand command= new CotBienesServiciosSolicitudCotizacionCommand();
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    		command.setDscUser(usuarioSeguridad.getNomUsuario());
    	}
    	if(request.getParameter("txhDscSede")!=null)
    	command.setDscSede((String)request.getParameter("txhDscSede"));
    	
    	command.setCodSede(request.getParameter("txhCodSede"));
    	if(command.getCodSede()==null)
    		command.setCodSede("");
    	    	
    	command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setConsteBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    	command.setConsteServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
    	command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setValRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setCodEstadoCerrado(CommonConstants.COTIZACION_ESTADO_CERRADA);
    	//ALQD,30/01/09. NUEVA CONSTANTE PARA FILTRAR COTs SIN OC
    	command.setConsteCotSinOc("1");
    	
    	List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();
    	List lista4= new ArrayList();
    	List lista5= new ArrayList();
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    	command.setListCodTipoRequerimiento(lista1);
    	
    	 lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTADO_REQUERIMIENTO 
     			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
         if(lista2!=null)
     	command.setListaCodEstado(lista2);
          	
         ListaTipoPago(command); 
         lista5=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRUPO_SERVICIO 
     			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
     	 if(lista5!=null)
     	 command.setListaCodGrupoServicio(lista5);
         
         
         BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
         if(bandejaBean!= null)
          {  log.info("BandejaLogisticaBean:INICIO");
             if(bandejaBean.getCodBandeja().equals("4")){
             	     	 	
             	command.setCodTipoRequerimiento(bandejaBean.getValor1());
        	 	 
                 if(command.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
                 	{command.setConsteRadio(bandejaBean.getValor2());
                 	 command.setCodTipoServicio("-1"); }
         	    else if(command.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
         	    	 command.setCodTipoServicio(bandejaBean.getValor2());
         	   
                 command.setNroCotizacion(bandejaBean.getValor3());
                 command.setFecInicio(bandejaBean.getValor4());
                 command.setFecFinal(bandejaBean.getValor5());
                 command.setCodEstado(bandejaBean.getValor6());
                 command.setCodTipoPago(bandejaBean.getValor7());
                 command.setTxtProveedor1(bandejaBean.getValor8());
                 command.setTxtProveedor2(bandejaBean.getValor9());
                 command.setCodGrupoServicio(bandejaBean.getValor10());
                 command.setValSelec1(bandejaBean.getValor11());
                 Buscar(command, request);
             }
        	  log.info("BandejaLogisticaBean:FIN");
          }
     	else command.setBanListaBandeja("0");
         
        log.info("formBackingObject:FIN");
    	 
    	return command;
     }
 	
 	protected void initBinder(HttpServletRequest request,
             ServletRequestDataBinder binder) {
     	NumberFormat nf = NumberFormat.getNumberInstance();
     	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
     	
     }
     /**
      * Redirect to the successView when the cancel button has been pressed.
      */
     public ModelAndView processFormSubmission(HttpServletRequest request,
                                               HttpServletResponse response,
                                               Object command,
                                               BindException errors)
     throws Exception {
         //this.onSubmit(request, response, command, errors);
     	return super.processFormSubmission(request, response, command, errors);
     }
 	
     public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
             BindException errors)
     		throws Exception {
     	log.info("onSubmit:INI");
     	
     	CotBienesServiciosSolicitudCotizacionCommand control= (CotBienesServiciosSolicitudCotizacionCommand) command;
     	control.setBanListaBandeja("");
     	
     	if(control.getOperacion().equals("TIPOREQ"))
     	{
     		System.out.println("TIPOREQ");
     	}
     	else{ if(control.getOperacion().equals("BUSCAR"))
     		  { Buscar(control, request);
     		
     		  }
     		else{ if(control.getOperacion().equals("GRUPOSERVICIO"))
   		  		{ Buscar_GrupoServicio(control, request);
   		
   		  		}
 		
     			
     		} 
     	}
     	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/CotBienesYServicios/Sol_Bandeja_Cotizacion","control",control);
    }

     public void ListarComboTipoServicio(CotBienesServiciosSolicitudCotizacionCommand control){
    	 System.out.println("ListarComboTipoServicio :D");
    	 List lista6=new ArrayList();
    	 System.out.println("CodGrupoServicio: "+control.getCodGrupoServicio());
    	 TipoLogistica obj = new TipoLogistica();
    	 obj.setCodPadre(CommonConstants.TIPT_GRUPO_SERVICIO);
 		 obj.setCodId(control.getCodGrupoServicio());
 		 obj.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
 		 obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	 lista6=this.detalleManager.GetAllTablaDetalle(obj);
    	/* lista6=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
     			, control.getCodGrupoServicio(), "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);*/
     	if(lista6!=null)
     	control.setListaCodTipoServicio(lista6);
    	System.out.println("ListaCodTipoServicio Size: "+control.getListaCodTipoServicio().size());
     }
     private void ListaTipoPago(CotBienesServiciosSolicitudCotizacionCommand control){
    	 	//carga combo tipo de pago
    		 List lista1= new ArrayList();
    	 	TipoLogistica obj = new TipoLogistica();    	
    	 	obj.setCodPadre("");
    	 	obj.setCodId("");
    	 	obj.setCodTabla(CommonConstants.TIPT_TIPO_PAGO);
    	 	obj.setCodigo("");
    	 	obj.setDescripcion("");
    	 	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	 	lista1=this.detalleManager.GetAllTablaDetalle(obj);
    	 	if(lista1!=null)
    	    	control.setListaCodTipoBien(lista1);
    	 	//***********************************
    	 	
    	 }
     
     public void Buscar(CotBienesServiciosSolicitudCotizacionCommand control, HttpServletRequest request)
     {  List lista= new ArrayList();
     
       GuardarDatosBandeja(control, request);
       
        System.out.println("En el Buscar :D");
        String SubTipoReq="";
          if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
	        	{SubTipoReq=control.getConsteRadio();
	        	 control.setCodGrupoServicio("-1");
	        	}
	      else if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
	        	 {if(!control.getCodTipoServicio().equals("-1"))
	        	       SubTipoReq=control.getCodTipoServicio();
	        	  else SubTipoReq="";
	        	 }
	        if(control.getCodEstado().equals("-1")) control.setCodEstado("");
	        if(control.getCodTipoPago().equals("-1"))  control.setCodTipoPago("");
	        if(control.getCodGrupoServicio().equals("-1")) control.setCodGrupoServicio("");
	        if(control.getCodTipoServicio().equals("-1"))  control.setCodTipoServicio("");
	        //ALQD,30/01/09. SETEANDO NUEVA PROPIEDAD
	        if(control.getValSelec1().equals("0")) control.setValSelec1("");
      
        System.out.println("SolicitudCotizacion: CodSede"+control.getCodSede()+">>NroCotizacion<<"+ 
    			control.getNroCotizacion()+">>FecInicio<<"+ control.getFecInicio()+">>FecFinal<<"+ control.getFecFinal()+
    			">>CodEstado<<"+control.getCodEstado()+">>CodTipoRequerimiento<<"+ control.getCodTipoRequerimiento()+">>CodSubTipoReq<<"+ SubTipoReq+ 
    			">>CodTipoPago<<"+control.getCodTipoPago()+ ">>TxtProveedor1<<"+control.getTxtProveedor1()+ ">>TxtProveedor2<<"+control.getTxtProveedor2()+
    			">>CodPerfil<<"+control.getCodPerfil()+">>CodUsuario<<"+control.getCodUsuario()
    			+">>ValSelec1<<"+control.getValSelec1());
        
    	lista=this.cotBienesYServiciosManager.GetAllSolicitudCotizacion(control.getCodSede(), 
    			control.getNroCotizacion(), control.getFecInicio(), control.getFecFinal(),
    			control.getCodEstado(), control.getCodTipoRequerimiento(), SubTipoReq, 
    			control.getCodTipoPago(), control.getTxtProveedor1(), control.getTxtProveedor2(),
    			control.getCodPerfil(), control.getCodUsuario(), control.getCodGrupoServicio(),
    			control.getValSelec1());
    	
    	if(lista!=null)
    	  {control.setListaBandeja(lista);
    	   if(lista.size()==0)
    	     control.setBanListaBandeja("0");
    	  }
    	else{ control.setBanListaBandeja("0");
    		  control.setListaBandeja(new ArrayList());
    	}
    	
    	if(!control.getCodGrupoServicio().equals(""))
     	   ListarComboTipoServicio(control);
   	    else control.setListaCodTipoServicio(new ArrayList());
    	
    	request.setAttribute("Bandera", "OK");
    	System.out.println("Size: "+control.getListaBandeja().size()+" ListaCodTipoBien:"+control.getListaCodTipoBien().size()
    			+" ListaCodTipoServicio: "+control.getListaCodTipoServicio().size()+" ListaCodEstado: "+control.getListaCodEstado().size());
     }
     
     public void GuardarDatosBandeja(CotBienesServiciosSolicitudCotizacionCommand control,HttpServletRequest request)
	  {
	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	 	
	 	 bandejaBean.setCodBandeja("4");
	 		 	 
	 	 bandejaBean.setValor1(control.getCodTipoRequerimiento());
	 	 
	 	String SubTipoReq="";
        if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
	        	SubTipoReq=control.getConsteRadio();
	    else if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
	          {if(!control.getCodTipoServicio().equals("-1"))
	        	       SubTipoReq=control.getCodTipoServicio();
	          else SubTipoReq="";
	          }
                
         bandejaBean.setValor2(SubTipoReq);
	 	 bandejaBean.setValor3(control.getNroCotizacion());
	 	 bandejaBean.setValor4(control.getFecInicio());
	 	 bandejaBean.setValor5(control.getFecFinal());
	 	 bandejaBean.setValor6(control.getCodEstado());
	 	 bandejaBean.setValor7(control.getCodTipoPago());
	 	 bandejaBean.setValor8(control.getTxtProveedor1());
	 	 bandejaBean.setValor9(control.getTxtProveedor2());
	 	 bandejaBean.setValor10(control.getCodGrupoServicio());
	 	 //ALQD,30/01/09. NUEVA PROPIEDAD
	 	 bandejaBean.setValor11(control.getValSelec1());
	 	 log.info("GuardarDatosBandeja");
	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	  }     
    
     public void Buscar_GrupoServicio(CotBienesServiciosSolicitudCotizacionCommand control, HttpServletRequest request){
    	 
    	 System.out.println("Buscar_GrupoServicio :D");
    	
    	 //control.setValRadio(control.getConsteRadio()); 
    	 control.setOperacion("");
    	 if(!control.getCodGrupoServicio().equals("-1"))
      	     ListarComboTipoServicio(control);
    	 else control.setListaCodTipoServicio(new ArrayList());
    	 
     } 
     
    public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
}
