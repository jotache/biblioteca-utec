package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;


import com.tecsup.SGA.web.logistica.command.SreqBienesCommand;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;

public class SreqBienesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(SreqBienesFormController.class);
	private DetalleManager detalleManager;
	private SeguridadManager seguridadManager;
	private SolRequerimientoManager solRequerimientoManager;
	
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	SreqBienesCommand command = new SreqBienesCommand();
    	//*********************************************************    	
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setCodRequerimientoOri(request.getParameter("txhCodRequerimientoOri") == null ? "": request.getParameter("txhCodRequerimientoOri"));
    	command.setIndTitulo(request.getParameter("txhIndTitulo") == null ? "": request.getParameter("txhIndTitulo"));
    	command.setCboTipoRequerimiento(request.getParameter("txhCodTipoRequerimiento") == null ? "": request.getParameter("txhCodTipoRequerimiento"));  
    	command.setCodTipoBien(request.getParameter("txhCodTipoBien") == null ? "": request.getParameter("txhCodTipoBien"));
    	command.setTipoProcedencia(request.getParameter("txhTipoProcedencia") == null ? "": request.getParameter("txhTipoProcedencia"));
    	
    	System.out.println("CboTipoRequerimiento="+command.getCboTipoRequerimiento()+"<<");
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");
    	System.out.println("codRequerimientoOri="+command.getCodRequerimientoOri()+"<<");
    	System.out.println("indTitulo="+command.getIndTitulo()+"<<");
    	System.out.println("CodTipoBien="+command.getCodTipoBien()+"<<");
    	//*********************************************************
    	if(request.getParameter("txhCodSede")!=null)
        { command.setCodSede(request.getParameter("txhCodSede"));
    	  log.info("CodSede: "+command.getCodSede());
          command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
//ALQD,31/10/08. SETEANDO EL INDEMPLOGISTICA
          command.setIndEmpLogistica(solRequerimientoManager.GetIndEmpLogistica(command.getCodUsuario()));
          System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
        }
    	    	
        command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN); 
        command.setConsteCodBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    	
    	if(command.getCodUsuario() != null ){
    		if(!command.getCodUsuario().equals("")){    		
        		cargaCabecera(command);
        	}   	
    	}
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	SreqBienesCommand control = (SreqBienesCommand)command;
    	String resultado="";
    	String resultadoEnviar="";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = InsertSolicitudRequerimiento(control);
    		System.out.println("resultado de grabar-->"+resultado);
    		control.setCodRequerimiento(resultado);
    		InsertSolicitudRequerimientoDetalle(control);
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{ if(resultado.equals("-2")){
    				control.setMsg("ALMACEN_CERRADO");
    			  }
    			  control.setMsg("OK_GRABAR");
    		}
    		
    		/*************************************************************/
    	
    		cargaCabecera(control);
    		//cargaDatosSolicitudRequerimiento(control);
    	}
    	else
		if (control.getOperacion().trim().equals("QUITAR")){
			System.out.println("entra para quitar");
    		resultado = DeleteSolRequerimientoDetalle(control);
    		System.out.println("resultado de quitar-->"+resultado);
    		cargaCabecera(control);
    		//cargaDatosSolicitudRequerimiento(control);    		
    	}
		else
		if (control.getOperacion().trim().equals("REFRESCAR")){			
			cargaCabecera(control);    		
    	}
		else
		if (control.getOperacion().trim().equals("ENVIAR")){
			//ALQD,05/02/09. SE A�ADE NUEVO RESULTADO '1' QUE INDICA APROBADO
			resultado = InsertSolicitudRequerimiento(control);
    		System.out.println("resultado de grabar-->"+resultado);
    		control.setCodRequerimiento(resultado);
    		InsertSolicitudRequerimientoDetalle(control);
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{ if(resultado.equals("-2")){
    				control.setMsg("ALMACEN_CERRADO");
    			  }
    			  else{
    				  resultadoEnviar=EnviarSolicitudRequerimientoUsuario(control);
    					System.out.println("este es el resultado de enviar-->"+resultadoEnviar+"<<");
    					if (resultadoEnviar.equals("-1")){
    		    			control.setMsg("ERROR");
    		    		}
    		    		else{
    		    			if(resultadoEnviar.equals("-2")){
    		    				control.setMsg("ERROR_PROCESO");
    		    			}
    		    			else{
    		    				if(resultadoEnviar.equals("-3")){
    		        				control.setMsg("ERROR_INGRESO");
    		        			}
    		        			else{
    		        				if(resultadoEnviar.equals("-4")){
    		            				control.setMsg("ERROR_NO_RESP");
    		            			}
    		        				else{ if(resultadoEnviar.equals("-5")){
    		            				  control.setMsg("ALMACEN_CERRADO");
    		            			      }
    		        					  else{ if(resultadoEnviar.equals("1")){
    		        							control.setMsg("APROBADO");
    		        						}else
  		        					  	     control.setMsg("OK");
    		        					}
    		        				}
    		        			}
    		    			}
    		    		}  
    			  }
    		}
			
			
			cargaCabecera(control);
    	}
		else if(control.getOperacion().trim().equals("CANCELAR"))
			  {  resultado= EliminarRegistro( control);
			     System.out.println("Resultado: "+resultado);
	    		 if ( resultado.equals("-2")) control.setMsg("ERROR_ELIMINAR");
	       		 else{ if(resultado.equals("0"))
	       			  {control.setMsg("OK_ELIMINAR");
	       			  }
	       			else{ if(resultado.equals("-1"))
	       				     control.setMsg("NO_SE_ELIMINA");
	       				 else  if(resultado.equals("-3"))
	       				       control.setMsg("ALMACEN_CERRADO");
	       			}
	       		
	       		}
	    		 cargaCabecera(control);
			  }
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/SolRequerimiento/Sreq_Bienes","control",control);
    }
    private void cargaCabecera(SreqBienesCommand control){
    	//carga lista del combo tipo Requerimiento
    	TipoLogistica obj = new TipoLogistica();
    	obj.setCodPadre("");
    	obj.setCodId("");
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_REQUERIMIENTO);
    	obj.setCodigo("");
    	obj.setDescripcion("");
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	control.setListaRequerimiento(this.detalleManager.GetAllTablaDetalle(obj));
    	
    	//carga lista del combo centro de costo    	
    	control.setListaCentroCosto(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(), "", ""));
    	
    	//carga los codRadio tipo de bien se guardan en dos hidden
    	TipoLogistica obj2 = new TipoLogistica();    	
    	obj2.setCodPadre("");
    	obj2.setCodId("");
    	obj2.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
    	obj2.setCodigo("");
    	obj2.setDescripcion("");
    	obj2.setTipo(CommonConstants.TIPO_ORDEN_COD);    	
    	control.setListaCodTipoBien(this.detalleManager.GetAllTablaDetalle(obj2));
    	Logistica logistica = new Logistica();
    	logistica=(Logistica)control.getListaCodTipoBien().get(0);
    	control.setCodTipoBienConsumible(logistica.getCodSecuencial());
    	logistica=(Logistica)control.getListaCodTipoBien().get(1);
    	control.setCodTipoBienActivo(logistica.getCodSecuencial());
    	//*****************************************
    	//carga lista de combo tipo de servicio
    	TipoLogistica obj3 = new TipoLogistica();    	
    	obj3.setCodPadre("");
    	obj3.setCodId("");
    	obj3.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
    	obj3.setCodigo("");
    	obj3.setDescripcion("");
    	obj3.setTipo(CommonConstants.TIPO_ORDEN_DSC);    	
    	control.setListaServicio(this.detalleManager.GetAllTablaDetalle(obj3));
    	//*****************************************
    	
    	//CCORDOVA TIPO GASTO REQUERIMIENTO SERVICIO
    	TipoLogistica obj4 = new TipoLogistica();
    	obj4.setCodPadre("");
    	obj4.setCodId("");
    	obj4.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
    	obj4.setCodigo("");
    	obj4.setDescripcion("");
    	obj4.setTipo(CommonConstants.TIPO_ORDEN_DSC);    	
    	control.setListTipoGastoServicio(this.detalleManager.GetAllTablaDetalle(obj4));
    	//carga fecha actual de la BD
    	try {
    		control.setFechaActual(Fecha.getFechaActual());
		} catch (Exception e) {
			e.printStackTrace(); 
		}   			
    	//entra y carga los atributos de la solicitud de requerimiento del usuario
    	cargaDatosSolicitudRequerimiento(control);
    }
    private void cargaDatosSolicitudRequerimiento(SreqBienesCommand control){
    	ArrayList<SolRequerimiento> listaSolReq;
    	//si el codRequerimiento != "" entra a modificar
    	if(!control.getCodRequerimiento().equals("")){
    		
    		listaSolReq = (ArrayList<SolRequerimiento>)this.solRequerimientoManager.getSolReqUsuario(control.getCodRequerimiento());
    		
    		if ( listaSolReq != null )
    			if ( listaSolReq.size() > 0 )
    				{
			    		SolRequerimiento solReqUsuario=listaSolReq.get(0);
			    		
			    		System.out.println("codigo sol req usuario-->"+solReqUsuario.getIdRequerimiento());
			    		
			    		control.setCodRequerimiento(solReqUsuario.getIdRequerimiento());
			    		control.setNroRequerimiento(solReqUsuario.getNroRequerimiento());
			    		control.setFechaRequerimiento(solReqUsuario.getFechaEmision());
			    		control.setCboTipoCentroCosto(solReqUsuario.getCecoSolicitante());
			    		control.setUsuSolicitante(solReqUsuario.getUsuSolicitante());
			    		control.setCboTipoRequerimiento(solReqUsuario.getTipoRequerimiento());
			    		control.setEstado(solReqUsuario.getEstadoReg());
			    		if(control.getCboTipoRequerimiento().equals("0001")){
			    			control.setCodSubTipoRequerimiento(solReqUsuario.getSubTipoRequerimiento());			    			
			    		}
			    		else{
			    			control.setCboTipoServicio(solReqUsuario.getSubTipoRequerimiento());
			    		}
			    		control.setIndInversion(solReqUsuario.getIndInversion());
			    		control.setNroAsociado(solReqUsuario.getNroRequerimientoOri());
			    		//ALQD,31/10/08. RECUPERANDO LAS NUEVAS PROPIEDADES A�ADIDAS
			    		control.setIndParaStock(solReqUsuario.getIndParaStock());
			    		control.setIndEmpLogistica(solReqUsuario.getIndEmpLogistica());
			    		//ALQD,25/02/09.MOSTRANDO EL NOMBRE DEL RESPONSABLE DE ATENCION
			    		control.setNomResAtencion(solReqUsuario.getUsuAsignado());
			    		//carga la bandeja de la solicitud del usuario
			    		cargaBandejaSolicitudDetalle(control);
    				}
    	}
    	else
    	if(control.getCodRequerimiento().equals("") && !control.getCodRequerimientoOri().equals("")){
    		ArrayList<SolRequerimiento> listaSolReqOri = (ArrayList<SolRequerimiento>)this.solRequerimientoManager.getSolReqUsuario(control.getCodRequerimientoOri());
    		if ( listaSolReqOri != null ){
    			if ( listaSolReqOri.size() > 0 ){
		    		SolRequerimiento solReqOri=listaSolReqOri.get(0);
		    		
		    		//control.setCodRequerimientoOri(solReqUsuario.getIdRequerimiento());
		    		//control.setNroRequerimiento(solReqUsuario.getNroRequerimiento());
		    		//control.setFechaRequerimiento(solReqUsuario.getFechaEmision());
		    		control.setCboTipoCentroCosto(solReqOri.getCecoSolicitante());
		    		control.setNroAsociado(solReqOri.getNroRequerimiento());
		    		//control.setUsuSolicitante(solReqUsuario.getUsuSolicitante());
		    		//control.setCboTipoRequerimiento(solReqUsuario.getTipoRequerimiento());
		    		//control.setEstado(solReqUsuario.getEstadoReg());
		    		control.setCodSubTipoRequerimiento(solReqOri.getSubTipoRequerimiento());		    		
		    		//carga la bandeja de la solicitud del usuario
		    		//cargaBandejaSolicitudDetalle(control);   
    			}
    		}
    	}
    }
    private void cargaBandejaSolicitudDetalle(SreqBienesCommand control){
    	System.out.println("entra para cargar la bandeja");
    	if(control.getCboTipoRequerimiento().equals("0001")){
    		List listaBien=this.solRequerimientoManager.getAllSolReqDetalle(control.getCodRequerimiento(), "");
    		
    		if(listaBien!=null){
    			if(listaBien.size()>0){
    				control.setListaSolicitudDetalle(listaBien);
    				control.setTamListaSolicitudDetalle(""+listaBien.size());
    				//carga el combo tipo de gasto solo si existen el detalle de la solicitud de req.
            		TipoLogistica obj = new TipoLogistica();    	
                	obj.setCodPadre("");
                	obj.setCodId("");
                	obj.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
                	obj.setCodigo("");
                	obj.setDescripcion("");
                	obj.setTipo(CommonConstants.TIPO_ORDEN_COD);    	
                	control.setListaGasto(this.detalleManager.GetAllTablaDetalle(obj));
    			}
    			else{
    				control.setTamListaSolicitudDetalle("0");
    			}
    		}
    	}
    	if(control.getCboTipoRequerimiento().equals("0002")){    		
    		List listaServicio=this.solRequerimientoManager.getAllSolReqDetalle(control.getCodRequerimiento(), "");
    		if(listaServicio!=null && listaServicio.size()>0){
    			SolRequerimientoDetalle solRequerimientoDetalle = (SolRequerimientoDetalle)listaServicio.get(0);
    			control.setCodReqDetalleServicio(solRequerimientoDetalle.getCodigo());
    			control.setNombreServicio(solRequerimientoDetalle.getNombreServicio());
    			control.setFechaAtencion(solRequerimientoDetalle.getFechaEntrega());
    			control.setDescripcion(solRequerimientoDetalle.getDescripcion());
    			control.setCodTipoGastoServicio(solRequerimientoDetalle.getTipoGasto());
    		}    		 
    	}
    }
    private String InsertSolicitudRequerimiento(SreqBienesCommand control){
    	try{
    		System.out.println("codRequerimiento>>"+control.getCodRequerimiento()+"<<");
    		System.out.println("codCentroCosto>>"+control.getCodTipoCentroCosto()+"<<");
    		System.out.println("sede>>"+control.getCodSede()+"<<");
    		System.out.println("usuario>>"+control.getCodUsuario()+"<<");
    		System.out.println("codTipoReq>>"+control.getCodTipoRequerimiento()+"<<");
    		
    		SolRequerimiento solicitudRequerimiento = new SolRequerimiento();
    		
    		solicitudRequerimiento.setIdRequerimiento(control.getCodRequerimiento());
    		solicitudRequerimiento.setCecoSolicitante(control.getCodTipoCentroCosto());
    		solicitudRequerimiento.setSede(control.getCodSede());
    		solicitudRequerimiento.setUsuSolicitante(control.getCodUsuario());
    		solicitudRequerimiento.setTipoRequerimiento(control.getCodTipoRequerimiento());
    		solicitudRequerimiento.setSubTipoRequerimiento(control.getCodSubTipoRequerimiento());
    		solicitudRequerimiento.setCodRequerimientoOri(control.getCodRequerimientoOri());
    		
    		solicitudRequerimiento.setIndInversion(control.getIndInversion());
    		//ALQD,31/10/08. GRABANDO INDPARASTOCK
    		solicitudRequerimiento.setIndParaStock(control.getIndParaStock());
    		String resultado="";
    		resultado=this.solRequerimientoManager.InsertSolRequerimientoUsuario(solicitudRequerimiento);
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }
	private void InsertSolicitudRequerimientoDetalle(SreqBienesCommand control){
		if(control.getCodTipoRequerimiento().equals("0001")){
			System.out.println("InsertSolicitudRequerimeintoDetalleBien--->");
			InsertSolicitudRequerimientoDetalleBien(control);
		}
		if(control.getCodTipoRequerimiento().equals("0002")){
			System.out.println("InsertSolicitudRequerimeintoDetalleServicio--->");
			InsertSolicitudRequerimientoDetalleServicio(control);
		}
	}
	private void InsertSolicitudRequerimientoDetalleBien(SreqBienesCommand control){
		StringTokenizer cadenaCodigo = new StringTokenizer(control.getCadenaCodigo(),"|");
		StringTokenizer cadenaCodBien = new StringTokenizer(control.getCadenaCodBien(),"|");
		StringTokenizer cadenaCantidad = new StringTokenizer(control.getCadenaCantidad(),"|");
		StringTokenizer cadenaCodTipoGasto = new StringTokenizer(control.getCadenaCodTipoGasto(),"|");
		StringTokenizer cadenaFechaEntrega = new StringTokenizer(control.getCadenaFechaEntrega(),"|");
		StringTokenizer cadenaDescripcion = new StringTokenizer(control.getCadenaDescripcion(),"|");
		
		while(cadenaCodBien.hasMoreTokens()){
			String tokenCodigo = cadenaCodigo.nextToken();
			String tokenCodBien = cadenaCodBien.nextToken();
			String tokenCantidad = cadenaCantidad.nextToken();
			if(tokenCantidad.equals("$")){
				tokenCantidad="";
			}
			String tokenCodTipoGasto = cadenaCodTipoGasto.nextToken();
			String tokenFechaEntrega = cadenaFechaEntrega.nextToken();
			if(tokenFechaEntrega.equals("$")){
				tokenFechaEntrega="";
			}
			String tokenDescripcion = cadenaDescripcion.nextToken();
			if(tokenDescripcion.equals("$")){
				tokenDescripcion="";
			}		
			
			SolRequerimientoDetalle obj = new SolRequerimientoDetalle();
			obj.setCodRequerimiento(control.getCodRequerimiento());
			obj.setCodigo(tokenCodigo);
			obj.setNombreServicio("");
			obj.setDescripcion(tokenDescripcion);
			obj.setCodigoBien(tokenCodBien);
			obj.setCantidad(tokenCantidad);
			obj.setTipoGasto(tokenCodTipoGasto);
			obj.setFechaEntrega(tokenFechaEntrega);			
			
			System.out.println("-->"+obj.getCodRequerimiento()+
					"|"+obj.getCodigo()+
					"|"+obj.getNombreServicio()+
					"|"+obj.getDescripcion()+
					"|"+obj.getCodigoBien()+
					"|"+obj.getCantidad()+
					"|"+obj.getTipoGasto()+
					"|"+obj.getFechaEntrega()+
					"|"+control.getCodUsuario());
			this.solRequerimientoManager.InsertSolRequerimientoDetalle(obj, control.getCodUsuario());
		}		
	}
    private void InsertSolicitudRequerimientoDetalleServicio(SreqBienesCommand control){
    	SolRequerimientoDetalle obj = new SolRequerimientoDetalle();
		obj.setCodRequerimiento(control.getCodRequerimiento());
		obj.setCodigo(control.getCodReqDetalleServicio());
		obj.setNombreServicio(control.getNombreServicio());
		obj.setDescripcion(control.getDescripcion());
		obj.setCodigoBien("");
		obj.setCantidad("");
		obj.setTipoGasto(control.getCodTipoGastoServicio());
		obj.setFechaEntrega(control.getFechaAtencion());
		System.out.println("-->"+obj.getCodRequerimiento()+
				"|"+obj.getCodigo()+
				"|"+obj.getNombreServicio()+
				"|"+obj.getDescripcion()+
				"|"+obj.getCodigoBien()+
				"|"+obj.getCantidad()+
				"|"+obj.getTipoGasto()+
				"|"+obj.getFechaEntrega()+
				"|"+control.getCodUsuario());
		this.solRequerimientoManager.InsertSolRequerimientoDetalle(obj, control.getCodUsuario());
    }
	private String DeleteSolRequerimientoDetalle(SreqBienesCommand control){
    	try{
    		String resultado="";
    		resultado=this.solRequerimientoManager.DeleteSolRequerimientoDetalle(control.getCodRequerimiento(), control.getCodDetRequerimiento(), control.getCodUsuario());
    		return resultado;
    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
	private String EnviarSolicitudRequerimientoUsuario(SreqBienesCommand control){
		String resultado="";
		resultado=this.solRequerimientoManager.EnviarSolRequerimientoUsuario(control.getCodRequerimiento(), control.getCodUsuario());
		return resultado;
	}
	
	public String EliminarRegistro(SreqBienesCommand control){
    	String resultado="";
    	System.out.println("CodRequerimiento: "+control.getCodRequerimiento());
    	System.out.println("CodUsuario(): "+control.getCodUsuario());
    	 resultado=this.solRequerimientoManager.DeleteSolRequerimiento(control.getCodRequerimiento(),
    			 control.getCodUsuario());
    	 System.out.println("Resultado: "+resultado);
    	return resultado;
    }
}
