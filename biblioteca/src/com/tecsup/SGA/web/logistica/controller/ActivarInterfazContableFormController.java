package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.InterfazContable;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.InterfazContableManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.ActivarInterfazContableCommand;

public class ActivarInterfazContableFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(ActivarInterfazContableFormController.class);
	private TablaDetalleManager tablaDetalleManager;
	private SeguridadManager seguridadManager;
	private InterfazContableManager interfazContableManager;
	
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public InterfazContableManager getInterfazContableManager() {
		return interfazContableManager;
	}

	public void setInterfazContableManager(
			InterfazContableManager interfazContableManager) {
		this.interfazContableManager = interfazContableManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
		log.info("formBackingObject:INI");
		
		ActivarInterfazContableCommand command= new ActivarInterfazContableCommand();
		
		UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    	}
		
		command.setCodSede(request.getParameter("txhCodSede"));
		
		List lista= new ArrayList();
		List lista1= new ArrayList();
		
		Anios anios= new Anios();
		command.setCodListaAnios(anios.getAniosTodosFromToDay(CommonConstants.FECHA_BASE_MAXIMO));
    	command.setCodListaMeses(anios.getMeses02());
    	command.setConsteGenerada(CommonConstants.COD_ESTADO_GENERADO);
    	
    	lista=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_INTERFAZ 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	if(lista!=null)
    		command.setListaTipoMovimiento(lista);
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTADO_INTERFAZ 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    	
    	
    	if(lista1!=null)
    	{  if(lista1.size()>0)
    		{ try{ for(int i=0;i<lista1.size();i++)
    			   {  TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
    			      tipoTablaDetalle=(TipoTablaDetalle)lista1.get(i);
    			      if(tipoTablaDetalle.getCodTipoTablaDetalle().equals(CommonConstants.TIPT_ESTADO_INTERFAZ_HIJO))
    			         {  command.setCodAnios(tipoTablaDetalle.getDscValor2());
    			    	    command.setCodMeses(tipoTablaDetalle.getDscValor1());
    			    	    i=lista1.size();
    			         }  
    			   }
    			
    		  }catch (Exception e) {
				
			 }
    		
    		}
    	}
    	
		log.info("formBackingObject:FIN:");
    	return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	ActivarInterfazContableCommand control=(ActivarInterfazContableCommand) command;
    	String resultado="";
    	
    	if(control.getOperacion().equals("MOVIMIENTO"))
    	{   
    		Buscar_Estado(control);
    	}
    	else if(control.getOperacion().equals("CABECERA"))
    		 {  resultado=InsertInterfazContable(control);
    		    if(resultado.equals("0"))
    		    {   
        		    Buscar_Estado(control);
    		    	control.setMsg("OK");
    		    }
    		    else control.setMsg("ERROR");
    		 }
    	control.setOperacion("");
    	log.info("onSubmit:FIN");
    	return new ModelAndView("/logistica/InterfazContable/Activar_Interfaz_Contable","control",control);		
    }

    public void Buscar_Estado(ActivarInterfazContableCommand control){
    	List lista= new ArrayList();
    	lista=interfazContableManager.GetAllEstadoInterfazContable(control.getCodSede(), control.getCodMeses(),
    			control.getCodAnios(), control.getCodTipoMovimiento());
    	if(lista!=null)
    	{   if(lista.size()>0)
    	    { InterfazContable interfazContable= new InterfazContable();
    	      interfazContable=(InterfazContable)lista.get(0);
    	      if(interfazContable.getCodEstado().equals(CommonConstants.COD_ESTADO_GENERADO))
    	    	{control.setDscEstado(interfazContable.getDscEstado());
    	    	 control.setCodEstado(interfazContable.getCodEstado());
    	    	}
    	      else{control.setDscEstado("");
 	    	 	   control.setCodEstado(interfazContable.getCodEstado());
  	    		  }
    	    }
    	}	
    }
    
    public String InsertInterfazContable(ActivarInterfazContableCommand control){
    	String resultado="";    	
    	resultado=interfazContableManager.InsertInterfazContable(control.getCodSede(), control.getCodMeses(), 
				  control.getCodAnios(), control.getCodTipoMovimiento(), control.getCodUsuario());
    	System.out.println("resultado: "+resultado);
    	return resultado;
    }
    
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
}
