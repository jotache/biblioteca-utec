package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarFamiliaCommand;

public class AgregarFamiliaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarFamiliaFormController.class);
  DetalleManager detalleManager;
   
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public AgregarFamiliaFormController() {    	
        super();        
        setCommandClass(AgregarFamiliaCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarFamiliaCommand command = new AgregarFamiliaCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setCodigo((String)request.getParameter("txhCodigo"));
    	command.setFamilia((String)request.getParameter("txhFamilia"));
    	command.setCodTipo((String)request.getParameter("txhCodPadre"));
    	command.setCta((String)request.getParameter("txhCtaBle"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setSecuencial((String)request.getParameter("txhSecuencial"));
      	TipoLogistica obj = new TipoLogistica();
    
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	
    	command.setTipoBien(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("tipoBien", command.getTipoBien());
    	
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado="";
    	AgregarFamiliaCommand control = (AgregarFamiliaCommand) command;
    	
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
    			resultado = modificar(control);
    			if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
    			else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
            	else control.setMsg("OK");
    		}
    		else{
    		resultado = grabar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if(resultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
    	}
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Familia","control",control);		
    }
        
        private String grabar(AgregarFamiliaCommand control){
        	control.setGraba(this.detalleManager.InsertTablaDetalle(control.getCodTipo()
        			, CommonConstants.TIPT_FAMILIA, control.getFamilia(), control.getCta(), "", "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        private String modificar(AgregarFamiliaCommand control){
           	control.setGraba(this.detalleManager.UpdateTablaDetalle(control.getCodTipo()
        			, CommonConstants.TIPT_FAMILIA, control.getSecuencial(), control.getCodigo(), control.getFamilia(), control.getCta(), "", "", control.getCodAlumno()));
        	
        	
        	return control.getGraba();
        }
        
}
