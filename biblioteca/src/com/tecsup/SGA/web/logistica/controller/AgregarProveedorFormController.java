package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.modelo.TipoProveedor;
import com.tecsup.SGA.modelo.Proveedor;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.ReclutaManager;
import com.tecsup.SGA.web.logistica.command.AgregarProveedorCommand;

public class AgregarProveedorFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarProveedorFormController.class);
	DetalleManager detalleManager;
	ReclutaManager reclutaManager;

    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public void setReclutaManager(ReclutaManager reclutaManager) {
		this.reclutaManager = reclutaManager;
	}
    public AgregarProveedorFormController() {
        super();
        setCommandClass(AgregarProveedorCommand.class);
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	String fla= "";
    	AgregarProveedorCommand command = new AgregarProveedorCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setCodUnico((String)request.getParameter("txhCodUnico"));
    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
          		cargaDatos(command);
          		provincia(command);
          		distrito(command);
          		if(command.getTipo().trim().equals("MODIFICAR")){
          			llenar(command);
          			if(!(command.getCodEstado()==null)){
          	    		if(command.getCodEstado().equals("0001")){
          	    		command.setFlag("");
          	    	  }
          	    		else{
          	    			command.setFlag("1");
          	    		}
          	    	 }
          			provincia(command);
          			distrito(command);
          		request.getSession().setAttribute("listaDepartamento", command.getListaDepartamento());
          		request.getSession().setAttribute("listProvincia", command.getListProvincia());
        		request.getSession().setAttribute("listDistrito", command.getListDistrito());
        		fla = command.getFlag();
        		request.setAttribute("flag",fla);
          		}
          		request.getSession().setAttribute("listaDepartamento", command.getListaDepartamento());
            	request.getSession().setAttribute("listCalificacion", command.getListCalificacion());
            	request.getSession().setAttribute("listEstado", command.getListEstado());
            	request.getSession().setAttribute("listGiro", command.getListGiro());
            	request.getSession().setAttribute("listProveedor", command.getListProveedor());
            	request.getSession().setAttribute("listmoneda", command.getListmoneda());
            	request.getSession().setAttribute("listFamSubFam", command.getListFamSubFam());
          	}
    		
    	}
    	
    	log.info("formBackingObject:FIN");
        return command;
    }
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado="";
		String fla= "";
    	AgregarProveedorCommand control = (AgregarProveedorCommand) command;
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
    			resultado = modificar(control);
        		if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
        		else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
            	else control.setMsg("OK");
        		cargaDatos(control);
        		provincia(control);
        		distrito(control);
        		llenar(control);
        		if(control.getCodEstado().equals("0001")){
        			fla = "0";
        		}
        		else{
        			fla = "1";
        		}
        		request.setAttribute("flag",fla);
        		request.getSession().setAttribute("listaDepartamento", control.getListaDepartamento());
        		request.getSession().setAttribute("listProvincia", control.getListProvincia());
        		request.getSession().setAttribute("listDistrito", control.getListDistrito());
    		}
    		else{
    		resultado = grabar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if(resultado.equals("-2")){
    			control.setCodUnico(resultado);
	    		control.setMsg("DOBLE");
	    	}
        	else{
        	control.setMsg("OK");
    		control.setCodUnico(resultado);
    		control.setTipo("MODIFICAR");
    		}
    		System.out.println("resultado: "+resultado);
    		if(resultado.equals("-2")){
    			control.setCodUnico("");
    			provincia(control);
        		distrito(control);
        		cargaDatos(control);
    		}
    		else{
    		provincia(control);
    		distrito(control);
    		cargaDatos(control);
    		llenar(control);
   			}
    		
    		if(control.getFlag().equals("")){
        		control.setCodEstado("0000");
        	}
        	if(control.getFlag()==null){
        		control.setCodEstado("0000");
        	}
        	if(!control.getFlag().equals("")){
        		control.setCodEstado("0001");
        	}
    		if(control.getCodEstado().equals("0001")){
    			fla = "0";
    		}
    		else{
    			fla = "1";
    		}
    		
    		if(control.getCodUnico().equals("-2")){
    			control.setCodUnico("");
    		}
    		request.setAttribute("flag",fla);
    		request.getSession().setAttribute("listaDepartamento", control.getListaDepartamento());
    		request.getSession().setAttribute("listProvincia", control.getListProvincia());
    		request.getSession().setAttribute("listDistrito", control.getListDistrito());
    		}
    	}
    	else if("PROV".equals(control.getOperacion())){
    		provincia(control);
    		cargaDatos(control);
    		request.getSession().setAttribute("listaDepartamento", control.getListaDepartamento());
    		request.getSession().setAttribute("listProvincia", control.getListProvincia());
    	}
    	else if("DIST".equals(control.getOperacion())){
    		distrito(control);
    		cargaDatos(control);
    		request.getSession().setAttribute("listaDepartamento", control.getListaDepartamento());
    		request.getSession().setAttribute("listProvincia", control.getListProvincia());
    		request.getSession().setAttribute("listDistrito", control.getListDistrito());
    	}
    	else if("".equals(control.getOperacion())){
    		provincia(control);
    		distrito(control);
    		cargaDatos(control);
    		request.getSession().setAttribute("listaDepartamento", control.getListaDepartamento());
    		request.getSession().setAttribute("listProvincia", control.getListProvincia());
           	request.getSession().setAttribute("listFamSubFam", control.getListFamSubFam());
           	request.getSession().setAttribute("listDistrito", control.getListDistrito());
    	}
    	else if("ELIMINAR".equals(control.getOperacion())){
    		resultado =eliminar(control);
    		if ( resultado.equals("-1")) {
    			control.setMssg("ERROR");
    		}
    		else if ( resultado.equals("-2")) {
    			control.setMssg("DOBLE");
    		}
    		else control.setMssg("OK");
       		provincia(control);
    		distrito(control);
    		cargaDatos(control);
    		llenar(control);
    		if(control.getCodEstado().equals("0001")){
    			fla = "0";
    		}
    		else{
    			fla = "1";
    		}
    		request.setAttribute("flag",fla);
    		request.getSession().setAttribute("listaDepartamento", control.getListaDepartamento());
    		request.getSession().setAttribute("listProvincia", control.getListProvincia());
    		request.getSession().setAttribute("listDistrito", control.getListDistrito());
    	}
    	if(control.getFlag().equals("A")){
    		control.setFlag("");
    	}
    	if(control.getFlag()==null){
    		control.setFlag("");
    	}
    	request.getSession().setAttribute("listFamSubFam", control.getListFamSubFam());
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Proveedores","control",control);
    }
        private String grabar(AgregarProveedorCommand control)
        {
        	TipoProveedor obj = new TipoProveedor();
        	if(control.getFlag().equals("")){
        		control.setFlag("A");
        		control.setCodEstado("0000");
        	}
        	if(control.getFlag()==null){
        		control.setFlag("A");
        		control.setCodEstado("0000");
        	}
        	else{
        		control.setCodEstado("0001");
        	}
        	obj.setCodTipoCalif(control.getCodCalif());
        	obj.setCodTipoProv(control.getCodProv());
			obj.setTipoDoc(control.getTipDoc());
			obj.setNroDoc(control.getNroDoc());
			obj.setRazSoc(control.getRazonSocial());
			obj.setRazSocCorto(control.getNomCorto());
			obj.setEmail(control.getCorreo());
			obj.setDirec(control.getDireccion());
			obj.setCodDpto(control.getCodDepa());
			obj.setCodProv(control.getCodProvincia());
			obj.setCodDsto(control.getCodDis());
			obj.setAtencion(control.getAtencion());
			obj.setCodGiro(control.getCodGiro());
			obj.setCodEstado(control.getFlag());
			obj.setBanco(control.getBanco());
			obj.setTipCta(control.getTipoCta());
			obj.setNroCta(control.getNroCta());
			obj.setCodMon(control.getCodMoneda());
			//ALQD,03/07/09.LEYENDO DOS NUEVAS PROPIEDADES
			obj.setTelefono(control.getTelefono());
			obj.setPais(control.getPais());
        	control.setCodGraba(this.detalleManager.InsertProveedor(obj, control.getCodAlumno()));
        	return control.getCodGraba();
        }
        
        private void cargaDatos(AgregarProveedorCommand command){
        	TipoLogistica obj = new TipoLogistica();
        	
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_CALIFICACION);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	command.setListCalificacion(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_ESTADO);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	command.setListEstado(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_GIRO);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	command.setListGiro(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_TIPO_PROVEEDORES);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	command.setListProveedor(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	obj.setCodTabla(CommonConstants.TIPT_MONEDA);
        	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
        	command.setListmoneda(this.detalleManager.GetAllTablaDetalle(obj));
        	
        	command.setListaDepartamento(this.reclutaManager.getAllDepartamento());
        	ArrayList lis = (ArrayList)this.detalleManager.GetAllSubFamliaByPro(command.getCodUnico());
        	if(!(lis==null)){
        	command.setListFamSubFam(lis);
        	}
        }
        
        private void llenar(AgregarProveedorCommand command){
        	Proveedor obj;
        	System.out.println("cod: "+command.getCodUnico());
   
        	ArrayList firstSeccion = (ArrayList)this.detalleManager.GetAllProveedor(command.getCodUnico(), "", "", "", "", "");
        	
        	if(!(firstSeccion==null)){
        		System.out.println("tam: "+firstSeccion.size());
        		if (firstSeccion.size() > 0){
        			obj = (Proveedor)firstSeccion.get(0);
        			command.setCodCalif(obj.getCodTipoCalificacion());
        			command.setCodProv(obj.getCodTipoProveedor());
        			command.setTipDoc(obj.getTipoDoc());
        			command.setNroDoc(obj.getNroDco());
        			command.setRazonSocial(obj.getRazSoc());
        			command.setNomCorto(obj.getRazSocCorto());
        			command.setCodDepa(obj.getDpto());
        			command.setCorreo(obj.getCorreo());
        			command.setCodProvincia(obj.getProv());
        			command.setDireccion(obj.getDirec());
        			command.setCodDis(obj.getDtrito());
        			command.setAtencion(obj.getAtencionA());
        			command.setCodEstado(obj.getCodEstado());
        			command.setCodGiro(obj.getCodTipGiro());
        			command.setBanco(obj.getNomBanco());
        			command.setTipoCta(obj.getTipCta());
        			command.setNroCta(obj.getNroCta());
        			command.setCodMoneda(obj.getCodTipoMoneda());
        			//ALQD,03/07/09.LEYENDO NUEVAS PROPIEDADES
        			System.out.println("getPais: "+obj.getPais());
        			command.setTelefono(obj.getTelefono());
        			command.setPais(obj.getPais());
        		}
        	 }
        }
        private String modificar(AgregarProveedorCommand control){
        	if(control.getFlag().equals("")){
        		control.setFlag("A");
        	}
        	if(control.getFlag()==null){
        		control.setFlag("A");
        	}
        	control.setCodGraba(this.detalleManager.UpdateProveedor(control.getCodUnico()
        			, control.getCodCalif(), control.getCodProv(), control.getTipDoc(), control.getNroDoc(), control.getRazonSocial()
        			, control.getNomCorto(), control.getCorreo(), control.getDireccion(), control.getCodDepa(), control.getCodProvincia()
        			, control.getCodDis(), control.getAtencion(), control.getCodGiro(), control.getFlag(), control.getBanco(), control.getTipoCta()
        			, control.getNroCta(), control.getCodMoneda(), control.getCodAlumno()
        			, control.getPais(), control.getTelefono()));
        	return control.getCodGraba();
        }
        private void provincia(AgregarProveedorCommand command){
        	command.setListaDepartamento(this.reclutaManager.getAllDepartamento());
        	command.setListProvincia(this.reclutaManager.getAllProvincia(command.getCodDepa()));
        }
        private void distrito(AgregarProveedorCommand control){
        	control.setListaDepartamento(this.reclutaManager.getAllDepartamento());
        	control.setListProvincia(this.reclutaManager.getAllProvincia(control.getCodDepa()));
    		control.setListDistrito(this.reclutaManager.getAllDistrito(control.getCodProvincia()));
        }
        private String eliminar(AgregarProveedorCommand control){
        	
        	control.setCodGraba(this.detalleManager.DeleteFamiliaByProveedor(control.getCodSubFamilia(), control.getCodAlumno()));
        	return control.getCodGraba();
        }
}
