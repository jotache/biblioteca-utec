package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.AtenderRequerimientoBienesGuiaSalidaCommand;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;


public class AtenderRequerimientoBienesGuiaSalidaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AtenderRequerimientoBienesGuiaSalidaFormController.class);	
	private AtencionRequerimientosManager atencionRequerimientosManager;

	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	AtenderRequerimientoBienesGuiaSalidaCommand command = new AtenderRequerimientoBienesGuiaSalidaCommand();    	
    	//*********************************************************
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodGuia(request.getParameter("txhCodGuia") == null ? "": request.getParameter("txhCodGuia"));
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	
    	/*System.out.println("codEstado="+command.getCodEstado()+"<<");
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");
    	System.out.println("codGuia="+command.getCodGuia()+"<<");
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");*/
    	//*********************************************************
    	if(command.getCodRequerimiento()!=null){
	    	if(!command.getCodRequerimiento().equals("")){    		
	    		cargaDatosCabecera(command);
	    	}
    	}   	
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	AtenderRequerimientoBienesGuiaSalidaCommand control = (AtenderRequerimientoBienesGuiaSalidaCommand)command;
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = GrabarDetalleGuia(control);
    		System.out.println("resultado codGuia-->"+resultado);    		
    		if (resultado.equals("0")){    		
    			control.setMsg("OKGRABAR");
    		}
    		else{	if (resultado.equals("-2")){    		
    				control.setMsg("ERROR_CANTIDAD_PENDIENTE");
    				}
    				else
    					control.setMsg("ERRORGRABAR");    			
    		}
    		cargaDatosCabecera(control);
    	}
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/Atencion_Requerimiento_Bienes_Guia_Salida","control",control);
    }    
    private void cargaDatosCabecera(AtenderRequerimientoBienesGuiaSalidaCommand control){
    	List lista=this.atencionRequerimientosManager.GetGuiaRequerimiento(control.getCodGuia());
    	
    	if(lista!=null && lista.size()>0){
    		Guia guia = (Guia)lista.get(0);
    		Fecha fecha= new Fecha();	
    		control.setNroGuia(guia.getNroGuia());
    		
    		log.info("solicitante:"+guia.getSolicitante());
    		log.info("LugarTrabajo:"+guia.getLugarTrabajo());
    		
    		control.setSolicitante(guia.getSolicitante());//jhpr 2008-09-26
    		control.setLugarTrabajo(guia.getLugarTrabajo());
    		
    		//ALQD,13/11/08. A�ADIENDIO NUEVAS PROPIEDADES
    		control.setNroRequerimiento(guia.getNroRequerimiento());
    		control.setFecAprRequerimiento(guia.getFecAprRequerimiento());
    		control.setNomCenCosto(guia.getNomCenCosto());
    		control.setFecCierre(guia.getFecCierre());
    		control.setFechaGuia(guia.getFechaEmision());
    		if(control.getFechaGuia().trim().equals("")) control.setFechaGuia(fecha.getFechaActual());
    		control.setEstado(guia.getEstado());
    		control.setCodEstado(guia.getCodEstado());
    		
    		cargaBandeja(control);
    	}    	
    }
    private void cargaBandeja(AtenderRequerimientoBienesGuiaSalidaCommand control){
    	List lista=this.atencionRequerimientosManager.GetAllDetalleGuiaRequerimiento(control.getCodGuia());    	
    	//log.info("tama�o de lista:" + lista.size());
    	if(lista!=null){
    		if (lista.size()>0){
        		control.setListaGuiaDetalle(lista);
        		control.setTamListaGuiaDetalle(""+lista.size());    			
    		}
    	}
    }
    private String GrabarDetalleGuia(AtenderRequerimientoBienesGuiaSalidaCommand control){    	
    	try{
    		/*
    		 * PROCEDURE SP_INS_DET_GUIA_REQ(
				E_C_CODGUIA IN CHAR,
				E_C_CODREQ IN CHAR,
				E_C_FEC_GUIA IN CHAR,
				E_V_OBSERVACION IN VARCHAR2,
				E_V_CAD_CODDETREQ IN VARCHAR2,
				E_V_CAD_CANTSOL IN VARCHAR2,
				E_V_CAD_PRECIOS IN VARCHAR2,
				E_V_CAD_CANTENT IN VARCHAR2,
				E_C_CANT_REG IN CHAR,
				E_C_CODUSUARIO IN CHAR,
				S_V_RETVAL IN OUT VARCHAR2)
    		 */
    	
    	
    		System.out.println("codGuia>>"+control.getCodGuia()+"<<");
    		System.out.println("codRequerimiento>>"+control.getCodRequerimiento()+"<<");
    		System.out.println("fechaGuia>>"+control.getFechaGuia()+"<<");
    		System.out.println("Observacion>><<");
    		System.out.println("CadenaCodDetalle>>"+control.getCadenaCodDetReq()+"<<");
    		System.out.println("CadenaCantidadSolicitada(>>"+control.getCadenaCantSol()+"<<");
    		System.out.println("CadenaPrecios>>"+control.getCadenaPrecios()+"<<");
    		System.out.println("CadenaCantidadEntregada>>"+control.getCadenaCantEnt()+"<<");    		
    		System.out.println("cantidad>>"+control.getTamListaGuiaDetalle()+"<<");
    		
    		GuiaDetalle obj = new GuiaDetalle();
    		
    		obj.setCodGuia(control.getCodGuia());
    		obj.setCodRequerimiento(control.getCodRequerimiento());
    		obj.setFechaGuia(control.getFechaGuia());
    		obj.setObservacion("");
    		obj.setCadenaCodDetalle(control.getCadenaCodDetReq());
    		obj.setCadenaCantidadSolicitada(control.getCadenaCantSol());
    		obj.setCadenaPrecios(control.getCadenaPrecios());
    		obj.setCadenaCantidadEntregada(control.getCadenaCantEnt());
    		obj.setCantidad(control.getTamListaGuiaDetalle());
    		
    		String resultado="";
    		resultado=this.atencionRequerimientosManager.InsertDetalleGuiaRequerimiento(obj, control.getCodUsuario());    		
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}
