package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarTipoServicioCommand;

public class AgregarTipoServicioFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarTipoServicioFormController.class);
   DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public AgregarTipoServicioFormController() {    	
        super();        
        setCommandClass(AgregarTipoServicioCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarTipoServicioCommand command = new AgregarTipoServicioCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setSecuencial((String)request.getParameter("txhSecuencial"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setTipSer((String)request.getParameter("txhDesc"));
    	command.setCodSer((String)request.getParameter("txhCodPadre"));
    	command.setCta((String)request.getParameter("txhValor1"));
    	System.out.println("Secuencial: "+command.getSecuencial());
    	System.out.println("CodSer: "+command.getCodSer());
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_GRUPO_SERVICIO);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	
    	command.setListSer(this.detalleManager.GetAllTablaDetalle(obj));
    	request.getSession().setAttribute("listSer", command.getListSer());
    	
         log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado="";
    	AgregarTipoServicioCommand control = (AgregarTipoServicioCommand) command;
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
    			resultado = modifica(control);
        		if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
        		else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
            	else control.setMsg("OK");
    		}
    		else{
	    		resultado = grabar(control);
	    		if ( resultado.equals("-1")) {
	    			control.setMsg("ERROR");
	    		}
	    		else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
	        	else control.setMsg("OK");
    		}
    	}
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_TipoServicio","control",control);		
    }
        private String grabar(AgregarTipoServicioCommand control){
        	
        	control.setGraba(this.detalleManager.InsertTablaDetalle(control.getCodSer()
        			, CommonConstants.TIPT_TIPO_SERVICIO, control.getTipSer(), control.getCta(), "", "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        
        private String modifica(AgregarTipoServicioCommand control){
        	System.out.println(control.getCodSer()+" -"+CommonConstants.TIPT_GRUPO_SERVICIO+" -"+control.getSecuencial()+" -"+CommonConstants.TIPT_TIPO_SERVICIO+" -"+control.getTipSer()+" -"+control.getCta()+"----"+control.getCodAlumno());
        	control.setGraba(this.detalleManager.UpdateTablaDetalle(control.getCodSer()
        			, CommonConstants.TIPT_TIPO_SERVICIO, control.getSecuencial(), CommonConstants.TIPT_GRUPO_SERVICIO, control.getTipSer(), control.getCta(), "", "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        
}
