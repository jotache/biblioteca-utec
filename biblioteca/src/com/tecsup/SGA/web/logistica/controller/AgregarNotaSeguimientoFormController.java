package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.NotaSeguimiento;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.web.logistica.command.AgregarNotaSeguimientoCommand;

public class AgregarNotaSeguimientoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarNotaSeguimientoFormController.class);
	AtencionRequerimientosManager atencionRequerimientosManager;  
	
	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	public AgregarNotaSeguimientoFormController() {    	
	        super();        
	        setCommandClass(AgregarNotaSeguimientoCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	AgregarNotaSeguimientoCommand command = new AgregarNotaSeguimientoCommand();
	    	command.setCodAlumno((String)request.getParameter("txhCodUsuario"));
	    	//command.setNota((String)request.getParameter("txhNota"));
	    	command.setCodNota((String)request.getParameter("txhCodNota"));
	    	command.setCodReq((String)request.getParameter("txhCodRequerimiento"));
	    	System.out.println("codUsu: "+command.getCodAlumno());
	    	System.out.println("codNot: "+command.getCodNota());
	    	System.out.println("codReq: "+command.getCodReq());
	    	
	    	if(!(command.getCodNota()==null)){
	          	if(!command.getCodNota().trim().equals("")){
	    		command.setTipo("MODIFICAR");
	    		llena(command);
		    	System.out.println("nota: "+command.getNota());
	    	}
	    	}
	    	else{
	    		command.setTipo("AGREGAR");
	    		}
	    	System.out.println("tipo: "+command.getTipo());
	            log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	AgregarNotaSeguimientoCommand control = (AgregarNotaSeguimientoCommand) command;
	    	if("GRABAR".equals(control.getOperacion())){
	    		if("MODIFICAR".equals(control.getTipo())){
	    			resultado = modificar(control);
	    			if ( resultado.equals("-1")) {
	        			control.setMsg("ERROR");
	        		}
	    			else if(resultado.equals("-2")){
	    	    		control.setMsg("DOBLE");
	    	    	}
	            	else control.setMsg("OK");
	    		}
	    		else{
	    		resultado = grabar(control);
	    		if ( resultado.equals("-1")) {
	    			control.setMsg("ERROR");
	    		}
	    		else if(resultado.equals("-2")){
		    		control.setMsg("DOBLE");
		    	}
	        	else control.setMsg("OK");
	    	}
	    	}
	    	System.out.println("men: "+control.getMsg());
	    	log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/AtencionRequerimientos/AgregarNotaSeguimiento","control",control);		
	    }
	        private String modificar(AgregarNotaSeguimientoCommand control){
	        	control.setGraba(this.atencionRequerimientosManager.UpdateNotaSeguimiento(control.getCodReq()
	        			, control.getCodNota(), control.getNota(), control.getCodAlumno()));
	        	return control.getGraba();
	        }
	        private String grabar(AgregarNotaSeguimientoCommand control){
	        	control.setGraba(this.atencionRequerimientosManager.InsertNotaSeguimiento(control.getCodReq()
	        			, control.getNota(), control.getCodPerfil(), control.getCodAlumno()));
	        	return control.getGraba();
	        }
	        private void llena(AgregarNotaSeguimientoCommand control){
	        	control.setListDes(this.atencionRequerimientosManager.GetAllNotasSeguimientoRequerimiento(control.getCodReq(), control.getCodNota()));
	        	List lst  = control.getListDes();
	        	System.out.println("total   : "+lst.size());
	        	if(lst!=null ){
	        		if ( lst.size()>0 )
	        		{
	        			NotaSeguimiento notaSeguimiento = null;
	        			notaSeguimiento = (NotaSeguimiento)lst.get(0); 
	        			
	        			control.setNota(notaSeguimiento.getDscNota());
	        		
	        		}
	        	}
	        }
	       
}
