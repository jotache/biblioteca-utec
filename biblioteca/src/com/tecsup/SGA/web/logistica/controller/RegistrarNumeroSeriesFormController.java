package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.math.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.NumeroSeries;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.web.logistica.command.RegistrarNumeroSeriesCommand;

public class RegistrarNumeroSeriesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(RegistrarNumeroSeriesFormController.class);
	  DetalleManager detalleManager;
	  AlmacenManager almacenManager;
	  SolRequerimientoManager solRequerimientoManager;
	    
	    public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
		public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public RegistrarNumeroSeriesFormController() {    	
	        super();        
	        setCommandClass(RegistrarNumeroSeriesCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");   	
	    	RegistrarNumeroSeriesCommand command = new RegistrarNumeroSeriesCommand();
	    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));//codigo de usuario
	        //command.setSede((String)request.getParameter("txhCodSede"));	//sede
	        command.setCodBien((String)request.getParameter("txhCodBien"));//codigo de bien
	        command.setCodDocPago((String)request.getParameter("txhCodDocPago"));//codigo de documento de pago
	        command.setCodDetalle((String)request.getParameter("txhCodDetalle"));//codigo del detalle
	        command.setCantEntregada((String)request.getParameter("txhCantEntregada"));//cantida entregada que se pinta eso
	        command.setPreUni((String)request.getParameter("txhPrecioUnitario"));//precio
	        
	        System.out.println("codUsu: "+command.getCodUsuario());
	        System.out.println("codBien: "+command.getCodBien());
	        System.out.println("codDcoPago: "+command.getCodDocPago());
	        System.out.println("cantEntrega: "+command.getCantEntregada());
	        System.out.println("preUni: "+command.getPreUni());
	        
	        if(command.getCodUsuario()!=null){
	        selecciona(command);
	        command.setSede(request.getParameter("txhCodSede"));
	  	    log.info("CodSede: "+command.getSede());
	        command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getSede()));
	        System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
	        }
	        command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN);
	        
	        System.out.println("conver: "+command.getTotReci());
	        System.out.println("falta: "+command.getFalta());
	        System.out.println("TotPintar: "+command.getTotPintar());
	      
	        request.setAttribute("faltaTot",command.getFalta());
	        request.setAttribute("totList",command.getTotList());
	            log.info("formBackingObject:FIN");
	        return command;
	    }  
	        protected void initBinder(HttpServletRequest request,
	            ServletRequestDataBinder binder) {
	    	NumberFormat nf = NumberFormat.getNumberInstance();
	    	binder.registerCustomEditor(Long.class,
		                  new CustomNumberEditor(Long.class, nf, true));	
	    }
	    public ModelAndView processFormSubmission(HttpServletRequest request,
	                                              HttpServletResponse response,
	                                              Object command,
	                                              BindException errors)
	    throws Exception {
	        return super.processFormSubmission(request, response, command, errors);
	    }
	        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
	            BindException errors)
	    		throws Exception {
	    	log.info("onSubmit:INI");
	    	String resultado="";
	    	RegistrarNumeroSeriesCommand control = (RegistrarNumeroSeriesCommand) command;
	    	if(control.getOperacion().equals("GRABAR")){
	    		resultado = grabar(control);
	    		if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
	    		else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
	    		else{ if(resultado.equals("-3"))
	    			    control.setMsg("ALMACEN_CERRADO");
	    			  else
	    			    control.setMsg("OK");
	    		    }
	    		System.out.println("men: "+control.getMsg());
	    	}
	    	if(control.getOperacion().equals("ELIMINAR")){
	    		resultado = eliminar(control);
	    		if ( resultado.equals("-1")) {
        			control.setMsg("EERROR");
        		}
	    		else if(resultado.equals("-2"))
    			         control.setMsg("ALMACEN_CERRADO");
  			  		 else
  			  			 control.setMsg("EOK");
	    		System.out.println("men: "+control.getMsg());
	    		
	    	}
	    	selecciona(control);
	    	
	    	 request.setAttribute("faltaTot",control.getFalta());
		     request.setAttribute("totList",control.getTotList());
	  		log.info("onSubmit:FIN");
		    return new ModelAndView("/logistica/Almacen/RegistrarNumeroSeries","control",control);		
	    }
	        public String grabar(RegistrarNumeroSeriesCommand control){
	        	System.out.println("codDocPago: "+control.getCodDocPago());
	        	System.out.println("codBien   : "+control.getCodBien());
	        	System.out.println("preUni    : "+control.getPreUni());
	        	System.out.println("codIng    : "+control.getCodIng());
	        	System.out.println("nroSerie  : "+control.getNroSerie());
	        	System.out.println("totPintar : "+control.getTotPintar());
	        	System.out.println("codUsuario: "+control.getCodUsuario());
	        	control.setGrabar(this.almacenManager.InsertSeriesActivo(control.getCodDocPago()
	        			, control.getCodBien(), control.getPreUni(), control.getCodIng()
	        			, control.getNroSerie(), String.valueOf(control.getTotPintar()), control.getCodUsuario(),control.getCodDetalle()));
	        	
	        	return control.getGrabar();
	        }
	        
	        public String eliminar(RegistrarNumeroSeriesCommand control){
	        	System.out.println("codDocPago  : "+control.getCodDocPago());
	        	System.out.println("codBien     : "+control.getCodBien());
	        	System.out.println("codIngresoE : "+control.getCodIngE());
	        	System.out.println("codUsuario  : "+control.getCodUsuario());
	        	control.setGrabar(this.almacenManager.DelSeriesActivos(control.getCodDocPago()
	        			, control.getCodBien(), control.getCodIngE(), control.getCodUsuario()));
	        	
	        	return control.getGrabar();
	        }
	        
	       public void selecciona(RegistrarNumeroSeriesCommand control){
	    	   control.setTotReci(Math.round(Float.valueOf(control.getCantEntregada())));	  
	    	   
	    	   System.out.println("codDocPago : "+control.getCodDocPago());
	    	   System.out.println("codBien    : "+control.getCodBien());
	    	   System.out.println("codDetalle : "+control.getCodDetalle());
	    	   
	    	   control.setListSeleccion(this.almacenManager.GetAllSeriesActivos(control.getCodDocPago()
	    			   , control.getCodBien(),control.getCodDetalle()));
	    	   if(control.getListSeleccion()== null){
	    		   control.setListSeleccion(new ArrayList());  
	    	   }	    	   
	    	   control.setTotList(control.getListSeleccion().size());
	 		   for ( int i =0; i < control.getTotReci() - control.getTotList() ;i++)
    		   {
    			   control.getListSeleccion().add(new NumeroSeries());   
    		   }
    		   if(control.getTotList()>control.getTotReci()){
    			   control.setTotPintar(control.getTotList());
    			   control.setFalta(0);
   	        }
   	        else if(control.getTotList()<control.getTotReci()){
   	        	control.setTotPintar(control.getTotReci());
   	        	control.setFalta(control.getTotReci()-control.getTotList());
   	        }
    		   
	       }
	       
			public void setAlmacenManager(AlmacenManager almacenManager) {
				this.almacenManager = almacenManager;
			}
}
