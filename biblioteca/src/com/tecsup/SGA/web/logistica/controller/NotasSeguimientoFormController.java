package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.NotasSeguimientoCommand;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;

public class NotasSeguimientoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(NotasSeguimientoFormController.class);	
	private AtencionRequerimientosManager atencionRequerimientosManager;

	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	NotasSeguimientoCommand command = new NotasSeguimientoCommand();
    	//*********************************************************    	    	
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");    	
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");    	
    	//*********************************************************
    	if(command.getCodRequerimiento()!=null){
	    	if(!command.getCodRequerimiento().equals("")){    		
	    		cargaBandeja(command);
	    	}
    	}
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	NotasSeguimientoCommand control = (NotasSeguimientoCommand)command;
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("QUITAR")){
			System.out.println("entra para quitar");
    		resultado = DeleteNotaSeguimiento(control);    		    		
    	}
    	cargaBandeja(control);
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/Notas_Seguimiento","control",control);
    }    
    private void cargaBandeja(NotasSeguimientoCommand control){
    	List lista=this.atencionRequerimientosManager.GetAllNotasSeguimientoRequerimiento(control.getCodRequerimiento(),"");

    	if(lista!=null && lista.size()>0){
    		control.setListaNotas(lista);
    		control.setTamListaNotas(""+lista.size());
    	}
    }
    private String DeleteNotaSeguimiento(NotasSeguimientoCommand control){
    	try{
    		System.out.println("codRequerimiento>>"+control.getCodRequerimiento()+"<<");
    		System.out.println("codNota>>"+control.getCodNota()+"<<");
    		System.out.println("codUsuario>>"+control.getCodUsuario()+"<<");
    		String resultado=""; 
    		resultado=this.atencionRequerimientosManager.DeleteNotaSeguimiento(control.getCodRequerimiento(), control.getCodNota(), control.getCodUsuario());
    		return resultado;
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
}
