package com.tecsup.SGA.web.logistica.controller;

import java.io.File;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.MetodosConstants;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AdjuntarImagenCommand;

public class AdjuntarImagenFormController extends SimpleFormController {
	private static Log log = LogFactory
			.getLog(AdjuntarImagenFormController.class);
	DetalleManager detalleManager;

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public AdjuntarImagenFormController() {
		super();
		setCommandClass(AdjuntarImagenCommand.class);
	}
	MetodosConstants metodo= new MetodosConstants();

	protected Object formBackingObject(HttpServletRequest request)
			throws ServletException {
		log.info("formBackingObject:INI");
		AdjuntarImagenCommand command = new AdjuntarImagenCommand();
		command.setCodAlumno((String) request.getParameter("txhCodAlumno"));
		command.setCodPro((String) request.getParameter("txhCodProducto"));
		System.out.println("codPro: " + command.getCodPro());
		System.out.println("codAlu: " + command.getCodAlumno());
		request.setAttribute("strExtensionGIF",CommonConstants.GSTR_EXTENSION_GIF);
		request.setAttribute("strExtensionJPG",CommonConstants.GSTR_EXTENSION_JPG);

		log.info("formBackingObject:FIN");
		return command;
	}

	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(
				Long.class, nf, true));

		binder.registerCustomEditor(byte[].class,
				new ByteArrayMultipartFileEditor());
	}

	public ModelAndView processFormSubmission(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		return super.processFormSubmission(request, response, command, errors);
	}

	public ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		log.info("onSubmit:INI");
		String resultado = "";
		AdjuntarImagenCommand control = (AdjuntarImagenCommand) command;
		if ("GRABAR".equals(control.getOperacion())) {
			byte[] bytesCV = control.getTxtCV();
			String filenameCV;
			
			String sep = System.getProperty("file.separator");
			//*************************************************************
//			String uploadDirCV = getServletContext().getRealPath("/"+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02);
			String uploadDirCV = CommonConstants.DIRECTORIO_DATA+CommonConstants.STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02;
			File dirPath = new File(uploadDirCV);
			if (!dirPath.exists()) {
				dirPath.mkdirs();
			}			
			//*************************************************************
			filenameCV =metodo.GenerarNombreArchivoAdjuntar(CommonConstants.FILE_NAME_IMG, 
       			 control.getExtCv()); 
			System.out.println(">>"+uploadDirCV + sep + filenameCV+"<<");
			File uploadedFileCV = new File(uploadDirCV + sep + filenameCV);
			FileCopyUtils.copy(bytesCV, uploadedFileCV);

			control.setGraba(this.detalleManager.InsertImageProducto(control
					.getCodPro(), filenameCV, control.getCodAlumno()));
			resultado = control.getGraba();
			if (resultado.equals("-1")) {
				control.setMsg("ERROR");
			} else
				control.setMsg("OK");
		}
		System.out.println("mensaje: "+control.getMsg());
		log.info("onSubmit:FIN");
		return new ModelAndView("/logistica/mantenimiento/log_Adjuntar_Imagen","control", control);
	}

}
