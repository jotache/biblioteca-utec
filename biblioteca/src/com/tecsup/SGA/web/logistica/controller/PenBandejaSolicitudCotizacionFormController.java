package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.PenBandejaSolicitudCotizacionCommand;

public class PenBandejaSolicitudCotizacionFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PenBandejaSolicitudCotizacionFormController.class);
	//SolRequerimientoManager solRequerimientoManager;
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	PenBandejaSolicitudCotizacionCommand command= new PenBandejaSolicitudCotizacionCommand();
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    	}
    	command.setCodSede(request.getParameter("txhCodSede"));
    	if(command.getCodSede()==null)
    		command.setCodSede("");
    	    	
    	command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setConsteBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    	command.setConsteServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
    	command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setValRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	
    	List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();
    	List lista4= new ArrayList();
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    	command.setListCodTipoRequerimiento(lista1);
    	
    	 lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO 
     			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
         if(lista2!=null)
     	command.setListaCodEstado(lista2);
          	
         ListaTipoPago(command); 
         ListarComboTipoServicio(command);
         
        command.setListaCodTipoServicio(new ArrayList());
    	 log.info("formBackingObject:FIN");
    	 
    	return command;
     }
 	
 	protected void initBinder(HttpServletRequest request,
             ServletRequestDataBinder binder) {
     	NumberFormat nf = NumberFormat.getNumberInstance();
     	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
     	
     }
     /**
      * Redirect to the successView when the cancel button has been pressed.
      */
     public ModelAndView processFormSubmission(HttpServletRequest request,
                                               HttpServletResponse response,
                                               Object command,
                                               BindException errors)
     throws Exception {
         //this.onSubmit(request, response, command, errors);
     	return super.processFormSubmission(request, response, command, errors);
     }
 	
     public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
             BindException errors)
     		throws Exception {
     	log.info("onSubmit:INI");
     	
     	PenBandejaSolicitudCotizacionCommand control= (PenBandejaSolicitudCotizacionCommand) command;
     	
     	if(control.getOperacion().equals("TIPOREQ"))
     	{
     		System.out.println("TIPOREQ");
     		/*if(control.getConsteRadio().equals(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO))
     			{  control.setValRadio(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
     			    System.out.println("Activo");
     			}
     		else{ if(control.getConsteRadio().equals(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE))
     				{ control.setValRadio(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO); 
     				  System.out.println("Consumible");
     				}
     		       }*/
     	}	
     	
     	log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/CotBienesYServicios/Pen_Bandeja_Solicitud_Cotizacion","control",control);
    }

     public void ListarComboTipoServicio(PenBandejaSolicitudCotizacionCommand control){
    	 System.out.println("ListarComboTipoServicio :D");
    	List lista6=new ArrayList();
    	 lista6=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
     			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
     	if(lista6!=null)
     	control.setListaCodTipoServicio(lista6);
    	 
     }
     
     private void ListaTipoPago(PenBandejaSolicitudCotizacionCommand control){
    	 	//carga combo tipo de pago
    		 List lista1= new ArrayList();
    	 	TipoLogistica obj = new TipoLogistica();    	
    	 	obj.setCodPadre("");
    	 	obj.setCodId("");
    	 	obj.setCodTabla(CommonConstants.TIPT_TIPO_PAGO);
    	 	obj.setCodigo("");
    	 	obj.setDescripcion("");
    	 	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	 	lista1=this.detalleManager.GetAllTablaDetalle(obj);
    	 	if(lista1!=null)
    	    	control.setListaCodTipoBien(lista1);
    	 	//***********************************
    	 	
    	 }
     
	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
}
