package com.tecsup.SGA.web.logistica.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.CatalogoProducto;
import com.tecsup.SGA.modelo.DetalleOrden;
import com.tecsup.SGA.modelo.DocPago;
import com.tecsup.SGA.modelo.Guia; 
import com.tecsup.SGA.modelo.InterfazContableResultado;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
import com.tecsup.SGA.service.logistica.GestionOrdenesManager;
import com.tecsup.SGA.service.logistica.ReportesLogisticaManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.ReportesLogisticaManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.service.logistica.InterfazContableManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.modelo.CotizacionDetalle;;

public class ReportesLogisticaFormController  implements Controller{
	private static Log log = LogFactory.getLog(ReportesLogisticaFormController.class);
	private AtencionRequerimientosManager atencionRequerimientosManager;
	ReportesLogisticaManager reportesLogisticaManager;
	DetalleManager detalleManager;
	GestionDocumentosManager gestionDocumentosManager;
	GestionOrdenesManager gestionOrdenesManager;
	SolRequerimientoManager solRequerimientoManager;
	CotBienesYServiciosManager cotBienesYServiciosManager;
	SeguridadManager seguridadManager;
	InterfazContableManager interfazContableManager;
	
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public SolRequerimientoManager getSolRequerimientoManager() {
		return solRequerimientoManager;
	}

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public GestionOrdenesManager getGestionOrdenesManager() {
		return gestionOrdenesManager;
	}

	public void setGestionOrdenesManager(GestionOrdenesManager gestionOrdenesManager) {
		this.gestionOrdenesManager = gestionOrdenesManager;
	}

	public GestionDocumentosManager getGestionDocumentosManager() {
		return gestionDocumentosManager;
	}

	public void setGestionDocumentosManager(
			GestionDocumentosManager gestionDocumentosManager) {
		this.gestionDocumentosManager = gestionDocumentosManager;
	}

	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public ReportesLogisticaManager getReportesLogisticaManager() {
		return reportesLogisticaManager;
	}

	public void setReportesLogisticaManager(
			ReportesLogisticaManager reportesLogisticaManager) {
		this.reportesLogisticaManager = reportesLogisticaManager;
	}

	public ReportesLogisticaFormController(){
		
	}
	
	public ModelAndView handleRequest(HttpServletRequest request,
	            HttpServletResponse response)
	    throws Exception {
	    	log.info("onSubmit:INI");
	    	
	    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
	    	if(usuarioSeguridad==null){
	    		InicioCommand command = new InicioCommand();
	    		return new ModelAndView("/cabeceraLogistica","control",command);
	    	}	    	
	    	
	    	List consulta= new ArrayList();
	    	//ALQD,21/09/08. ADICIONANDO LISTA DE CONDICIONES.
	    	List lstCondComerciales= new ArrayList();
	    	
    		//ALQD,23/04/09. LEYENDO DOS VALORES PARA UNA COTIZACION DE SERVICIO
	    	//REPORTE 21
    		String gruServicio="";
    		String tipServicio="";
    		
	    	ModelMap model = new ModelMap();
	    	
	    	String pagina = "";
	    	String fechaRep = Fecha.getFechaActual();
			String horaRep = Fecha.getHoraActual();
			
			model.addObject("fechaRep", fechaRep);
			model.addObject("horaRep", horaRep);
			
			String codTipoReporte=request.getParameter("tCodRep");
			if(codTipoReporte==null)
				codTipoReporte="";
			
			String imagen = "${ctx}/images/logoTecsup.jpg";
			String codSede=request.getParameter("txhCodSede");
			if(request.getParameter("tCodSede")!=null)
				codSede=request.getParameter("tCodSede");
			//5
			String fecIni = "";
	    	String fecFin = "";
	    	String nroMax = "";
	    	String codTipoReq = "";
	    	String codTipoBien = "";
	    	String codTipoServicio = "";
	    	String dscUsuSol= "";
	    	String codProveLocal= "";
	    	String periodoIni = request.getParameter("tFecIni");
	    	String periodoFin = request.getParameter("tFecFin");
	    	//System.out.println("periodoFin1:"+periodoFin);
	    	String txhNomSede = request.getParameter("tNomSede");
	    	String codCentroCosto = "";
	    	String codTipoGasto = "";
	    	//6
	    	String codFamilia = "";
	    	String codBien = "";
	    	//7
	    	String estadoReq = "";
	    	String usuSolicitante = request.getParameter("tCodUsu");
	    	String dscUsuarioSolicitante = request.getParameter("tDscUsu");
	    	String codTipoPago = "";
	    	String codTipoMoneda = "";
	    	//8
	    	String tipoLugar = "";
	    	String proveedor = "";
	    	String dscProveedor = "";
	    	//10
	    	String validado = "";
	    	String codTipoOrden = "";
	    	String codTipoEstado = "";
	    	//
	    	String codProducto="";
	    	String codSedeAux="";
	    	String codSubFamilia="";
	    	String dscProduc="";
	    	String dscFamilia="";
	    	String dscSubFamilia="";
	    	String dscSede="";
	    	String condicion = "";
	    	String dscCondicion = "";
	    	String nroSerie = "";
	    	
	    	String nroDocumento = "";
	    	String codUsuario = "";
	    	
	    	//UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
	    	if ( usuarioSeguridad != null )
	    	{
	    		usuSolicitante=usuarioSeguridad.getIdUsuario();
	    		dscUsuarioSolicitante=usuarioSeguridad.getNomUsuario();
	    		System.out.println("DscUsuario: "+dscUsuarioSolicitante);
	    		List listaSede= new ArrayList();
	    		listaSede=this.seguridadManager.getAllSedes();
	    		if(codSede!=null)
	    		{if(listaSede!=null)
	    			{  SedeBean sede=new SedeBean();
	    				for(int a=0;a<listaSede.size();a++)
	    				{ sede=(SedeBean)listaSede.get(a);
	    				if(codSede.equals(sede.getCodSede()))
	    				{  txhNomSede=sede.getDscSede();
	    			      a=listaSede.size();
	    			    }
	    			}
	    			
	    			}
	    		}
	    	}
	    	
	    	/*------------------REPORTE 5--------------------*/
	    	codTipoReq=request.getParameter("tCodTipoReq");
			codTipoBien=request.getParameter("tCodTipoBien");
			codTipoServicio=request.getParameter("tCodTipoServicio");
			codCentroCosto=request.getParameter("tCodCeco");
			codTipoGasto=request.getParameter("tCodTipoGasto");
			
			/*------------------REPORTE 6--------------------*/
			//codTipoBien=request.getParameter("txhCodTipoBien");
			codFamilia=request.getParameter("tCodFamilia");
			codBien=request.getParameter("tCodBien");
			
			/*------------------REPORTE 7--------------------*/
			estadoReq=request.getParameter("tCodEstadoReq");
			dscUsuSol=request.getParameter("tUSol");
			/*------------------REPORTE 8--------------------*/
			tipoLugar=request.getParameter("tTipoLugar");
			proveedor=request.getParameter("tProveedor");
			codTipoPago=request.getParameter("tCodTipoPago");
			codTipoMoneda=request.getParameter("tCodTipoMoneda");
			
			/*------------------REPORTE 9--------------------*/
			
			/*------------------REPORTE 10--------------------*/
			validado = request.getParameter("tValidado");
	    	codTipoOrden = request.getParameter("tCodTipoOrden");
	    	codTipoEstado = request.getParameter("tCodTipoEstado");
	    	/*-----------------Reporte 12---------------------*/
	    	String nroGuia = "";
	    	String fechaGuia= "";
	    	String estado = "";
	    	String total="";
	    	String codGuia="";
	    	String solicitante = "";
	    	String lugarTrabajo = "";
	    	//ALQD,13/11/08. A�ADIENDO MAS DATOS
	    	String nroRequerimiento = "";
	    	String fecAprRequerimiento= "";
	    	String nomCenCosto = "";
	    	String fecCierre = "";
	    	/*----------------Reporte 18----------------------*/
	    	String nroFac="";
	    	String fechaEmi="";
	    	String nroOS="";
	    	String Obs="";
	    	String codEstado = "";
	    	/*---------------Reporte 15------------------------*/
	    	//codSede = request.getParameter("codSede");
	    	String codPerfil = "";
	    	String nroOrden = "";
	    	String nroRucProv = "";
	    	String nombreProv = "";
	    	String nroRucComp = "";
	    	String nombreComp = "";
	    	String tipoOrden = "";
	    	String codSedeE = "";
	    	if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_15))
			{
			 usuSolicitante = request.getParameter("usuSolicitante");
			 codPerfil = request.getParameter("codPerfil");
			 codEstado = request.getParameter("codEstado");
			 nroOrden = request.getParameter("nroOrden");
			 nroRucProv = request.getParameter("nroRucProv");
			 nombreProv = request.getParameter("nroRucProv");
			 nroRucComp = request.getParameter("nroRucComp");
			 nombreComp = request.getParameter("nombreComp");
			 codTipoOrden = request.getParameter("codTipoOrden");
			 tipoOrden = request.getParameter("tipoOrden");
			 estado = request.getParameter("estado");
			 periodoIni = request.getParameter("periodoIni");
		     periodoFin  = request.getParameter("periodoFin");
		     codSedeE = request.getParameter("codSedeE");
			}
	    	/*----------------Reporte 14--------------------*/
	    	 String nroCotizacion ="";
	    	 String codTipoReque ="";
	    	 String codTipoPagoE ="";
	    	 String txtProv1 = "";
	    	 String txtProv2 ="";
	    	 String ConsteRadio ="";
	    	 String CodTipoServicio="";
	    	 String txhtipo ="";
	    	 String txhDetalle ="";
	    	 String  DesTipoPago ="";
	    	if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_14))
			{
		     nroCotizacion = request.getParameter("nroCotizacion");
		     codTipoReque = request.getParameter("codTipoReque");
		     codTipoPagoE = request.getParameter("codTipoPagoE");
		     txtProv1 = request.getParameter("txtProv1");
		     txtProv2 = request.getParameter("txtProv2");
		     periodoIni = request.getParameter("periodoIni");
		     periodoFin  = request.getParameter("periodoFin");
		     codEstado = request.getParameter("codEstado");
		     ConsteRadio = request.getParameter("ConsteRadio");
		     CodTipoServicio= request.getParameter("CodTipoServicio");
		     txhDetalle = request.getParameter("txhDetalle");
		     DesTipoPago = request.getParameter("tDTP");
		     estado = request.getParameter("estado");
		      
			}
	    	
	    	//--------------reporte 19 ---------
	    	String tCPro = request.getParameter("tCPro");
	    	String tPago = request.getParameter("tPago");
	    	String Vc = request.getParameter("Vc");
	    	String Vc1 = request.getParameter("Vc1");
	    	String Hi = request.getParameter("Hi");
	    	String Hi1 = request.getParameter("Hi1");
	    	String Ccot = request.getParameter("Ccot");
	    	String Ctco = request.getParameter("Ctco");
	    	String CtRe = request.getParameter("CtRe");
	    	String Cco = request.getParameter("Cco");
	    	String NroCo = "";
	    	
	    	String cMes = request.getParameter("cMes");
	    	String cAnio = request.getParameter("cAnio");
	    	String tInt = request.getParameter("tInt"); 
	    	String dAnio = (String)request.getParameter("dAnio");
	    	String dMes = (String)request.getParameter("dMes");
	    	String totDebe = "";
	    	String totHaber = "";
	    	//-----------------------------------
	    	if(periodoIni!=null)
	    	  {if(periodoIni.equals("-1"))
	    		  periodoIni="";}
	        else periodoIni="";
	    	
	    	System.out.println("periodoFin2:"+periodoFin);
	    	if(periodoFin!=null)
	    	  {if(periodoFin.equals("-1"))
	    		  periodoFin="";}
	        else periodoFin="";
	    	System.out.println("periodoFin3:"+periodoFin);
	    	
	    	if(usuSolicitante!=null)
	    	  {if(usuSolicitante.equals("-1"))
	    		  usuSolicitante="";}
	        else usuSolicitante="";
	    	
	    	if(dscUsuarioSolicitante!=null)
	    	  {if(dscUsuarioSolicitante.equals("-1"))
	    		  dscUsuarioSolicitante="";}
	        else dscUsuarioSolicitante="";
	    	
	    	if(codTipoReq!=null)
	    	  {if(codTipoReq.equals("-1"))
	    		 codTipoReq="";}
	        else codTipoReq="";
	    	
	    	if(codTipoBien!=null)
	    	  {if(codTipoBien.equals("-1"))
	    		  codTipoBien="";}
	        else codTipoBien="";
	    	
	    	if(codTipoServicio!=null)
	    	  {if(codTipoServicio.equals("-1"))
	    		  codTipoServicio="";}
	        else codTipoServicio="";
	    	
	    	if(codCentroCosto!=null)
	    	  {if(codCentroCosto.equals("-1"))
	    		  codCentroCosto="";}
	    	else codCentroCosto="";
	    	
	    	if(codTipoGasto!=null)
	    	  {if(codTipoGasto.equals("-1"))
	    		  codTipoGasto="";}
	        else codTipoGasto="";
	    	
	    	if(codFamilia!=null)
	    	  {if(codFamilia.equals("-1"))
	    		  codFamilia="";}
	        else codFamilia="";
	    	
	    	if(codBien!=null)
	    	  {if(codBien.equals("-1"))
	    		  codBien="";}
	        else codBien="";
	    	
	    	if(estadoReq!=null)
	    	  {if(estadoReq.equals("-1"))
	    		  estadoReq="";}
	        else estadoReq="";
	    	
	    	if(tipoLugar!=null)
	    	  {if(tipoLugar.equals("-1"))
	    		  tipoLugar="";}
	        else tipoLugar="";
	    	
	    	if(proveedor!=null)
	    	  {if(proveedor.equals("-1"))
	    		  proveedor="";}
	        else proveedor="";
	    	
	    	if(codTipoPago!=null)
	    	  {if(codTipoPago.equals("-1"))
	    		  codTipoPago="";}
	        else codTipoPago="";
	    	
	    	if(codTipoMoneda!=null)
	    	  {if(codTipoMoneda.equals("-1"))
	    		  codTipoMoneda="";}
	        else codTipoMoneda="";
	    	
	    	if(validado!=null)
	    	  {if(validado.equals("-1"))
	    		  validado="";}
	        else validado="";
	    	
	    	if(codTipoOrden!=null)
	    	  {if(codTipoOrden.equals("-1"))
	    		  codTipoOrden="";}
	        else codTipoOrden="";
	    	
	    	if(codTipoEstado!=null)
	    	  {if(codTipoEstado.equals("-1"))
	    		  codTipoEstado="";}
	        else codTipoEstado="";
	    	
	    	model.addObject("NOM_SEDE", txhNomSede);
	    	model.addObject("periodoInicio", periodoIni);
    		model.addObject("periodoFin", periodoFin);
    		model.addObject("usuario", dscUsuarioSolicitante);
    		
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_5))
			{//Reporte de Cierre de Operaciones
				System.out.println("Reporte de Cierre de Operaciones");
				if(codTipoReq.equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
					codTipoServicio="";
				else{ if(codTipoReq.equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
					codTipoBien="";
				}
				System.out.println("codSede: "+codSede+">>codTipoReq<<"+codTipoReq+
						">>codTipoBien<<"+codTipoBien+">>codTipoServicio<<"+codTipoServicio+"" +
						">>periodoIni<<"+periodoIni+">>periodoFin<<"+periodoFin+">>codCentroCosto<<"
						+codCentroCosto+""+">>codTipoGasto<<"+codTipoGasto);
				try{
				consulta=this.reportesLogisticaManager.GetAllCierreOperaciones(codSede, codTipoReq,
				         codTipoBien, codTipoServicio, periodoIni, periodoFin, codCentroCosto, codTipoGasto);
				}catch (Exception e) {
				
				}
				request.getSession().setAttribute("consulta", consulta);
				if(consulta!=null){
					System.out.println("Size: "+consulta.size());
					log.info("codTipoRequerimiento>>"+request.getParameter("tCodTipoReq")+">>");
					
					if(CommonConstants.TIPO_REQ_BIEN.equalsIgnoreCase(request.getParameter("tCodTipoReq")))
						pagina = "/logistica/consultaReportes/rep_Gen_CierreOperaciones";
					else if(CommonConstants.TIPO_REQ_SERVICIO.equalsIgnoreCase(request.getParameter("tCodTipoReq")) ){
						request.setAttribute("reqSservicio","true");
						pagina = "/logistica/consultaReportes/rep_Gen_CierreOperaciones";
					}
					
	    		}
				else System.out.println("Lista Vacia");
				
			}
			else{
				if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_6))
				{//Reporte Acumulado de Salidas por Art�culo
					System.out.println("Reporte Acumulado de Salidas por Art�culo: ");
					System.out.println("codSede: "+codSede+">>codTipoBien<<"+codTipoBien+
							">>periodoIni<<"+periodoIni+">>periodoFin<<"+periodoFin+
							">>codFamilia<<"+codFamilia+">>codBien<<"+codBien+
							">>codCentroCosto<<"+codCentroCosto+"" +">>codTipoGasto<<"+codTipoGasto);
					try{
					consulta=this.reportesLogisticaManager.GetAllAcumuladoSalidaByArticulo(codSede,
							codTipoBien, periodoIni, periodoFin, codFamilia, codBien, codCentroCosto,
							codTipoGasto);
					}catch (Exception e) {
						e.printStackTrace();
					}
					request.getSession().setAttribute("consulta", consulta);
					if(consulta!=null){
		        		pagina = "/logistica/consultaReportes/rep_Gen_AcumuladosSalidasByArticulo";
		    		}
				}
				else{
					if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_7))
					{ //Reporte de requerimientos valorizado
						System.out.println("Reporte de requerimientos valorizado");
						if(codTipoReq.equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
							codTipoServicio="";
						
						System.out.println("codSede: "+codSede+">>codTipoReq<<"+codTipoReq+
								">>codTipoBien<<"+codTipoBien+">>codTipoServicio<<"+codTipoServicio+
								">>periodoIni<<"+periodoIni+">>periodoFin<<"+periodoFin+
								">>dscUsuSol<<"+dscUsuSol+">>estadoReq<<"+estadoReq);
						try{
						consulta=this.reportesLogisticaManager.GetAllRequerimientoValorizado(codSede,
								codTipoReq, codTipoBien, codTipoServicio, periodoIni, periodoFin,
								dscUsuSol, estadoReq);
					}catch (Exception e) {
						e.printStackTrace();
						}
						request.getSession().setAttribute("consulta", consulta);
						if(consulta!=null){
			        		pagina = "/logistica/consultaReportes/rep_Gen_RequerimientoValorizado";
			    		}
					}
					else{
						if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_8))
						{//Reporte de Ordenes Asignadas por Proveedor
							System.out.println("Reporte de Ordenes Asignadas por Proveedor");
							System.out.println("codSede: "+codSede+">>codTipoReq<<"+codTipoReq+
									">>codTipoBien<<"+codTipoBien+">>codTipoServicio<<"+codTipoServicio+"" +
									">>periodoIni<<"+periodoIni+"periodoFin: "+periodoFin+
									">>tipoLugar<<"+tipoLugar+">>proveedor<<"+proveedor+"codTipoMoneda: "+codTipoMoneda+
									">>codTipoPago<<"+codTipoPago);
							try{
							consulta=this.reportesLogisticaManager.GetAllOrdenesAsignadasByProveedor(codSede,
									codTipoReq, codTipoBien, codTipoServicio, periodoIni, periodoFin,
									tipoLugar, proveedor, codTipoMoneda, codTipoPago);
							}catch (Exception e) {
								e.printStackTrace();
								}
							request.getSession().setAttribute("consulta", consulta);
							if(consulta!=null){
				        		pagina = "/logistica/consultaReportes/rep_Gen_OrdenesAsignadasByProveedor";
				    		}
						}
						else{
							if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_9))
							{//Reporte de Solicitudes de Cotizaci�n
								System.out.println("Reporte de Solicitudes de Cotizaci�n");
								System.out.println("codSede: "+codSede+">>codTipoReq<<"+codTipoReq+
									">>codTipoBien<<"+codTipoBien+">>codTipoServicio<<"+codTipoServicio
									+">>periodoIni<<"+periodoIni+
									">>periodoFin<<"+periodoFin+">>proveedor<<"+proveedor);
							try{
								consulta=this.reportesLogisticaManager.GetAllSolicitudCotizaciones(codSede,
										codTipoReq, codTipoBien, codTipoServicio, periodoIni, periodoFin,
										proveedor);
							}catch (Exception e) {
								e.printStackTrace();
								}
								request.getSession().setAttribute("consulta", consulta);
								if(consulta!=null){
					        		pagina = "/logistica/consultaReportes/rep_Gen_SolicitudesCotizacion";
					    		}
							}
							else{
								if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_10))
								{//Reporte de Programa (Cronograma) de Pagos
									System.out.println("Reporte de Programa (Cronograma) de Pagos");
									System.out.println("codSede: "+codSede+">>periodoIni<<"+periodoIni+
											">>periodoFin<<"+periodoFin+">>validado<<"+validado+
											">>codTipoOrden<<"+codTipoOrden+">>codTipoPago<<"+codTipoPago+
											">>codTipoEstado<<"+codTipoEstado+">>proveedor<<"+proveedor);
									try{
							    	consulta=this.reportesLogisticaManager.GetAllProgramaCronogramaPago(codSede,
							    			periodoIni, periodoFin, validado, codTipoOrden, codTipoPago,
							    			codTipoEstado, proveedor);
									}catch (Exception e) {
										e.printStackTrace();
										}
									request.getSession().setAttribute("consulta", consulta);
									if(consulta!=null){
						        		pagina = "/logistica/consultaReportes/rep_Gen_ProgramaCronogramaPago";
						    		}
								}
								
								else{
									if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_11))
									{//Reporte de Cataogos de Productos
										 codProducto=request.getParameter("tCPro");
								    	 codSedeAux=request.getParameter("tCSA");
								    	 codSubFamilia=request.getParameter("tCSFa");
								    	 dscProduc=request.getParameter("tDPro");
								    	 dscFamilia=request.getParameter("tDFa");
								    	 dscSubFamilia=request.getParameter("tDSubFa");
								    	 codFamilia = request.getParameter("tCoFa");
								    	 dscSede = request.getParameter("tDSe");
								    	 condicion = request.getParameter("tCond");
								    	 dscCondicion  = request.getParameter("tDCon");
								    	 nroSerie = request.getParameter("tNSe");

								    	 System.out.println("codProducto: "+codProducto);
										 System.out.println("dscProduc: "+dscProduc);
										 System.out.println("nroSerie: "+nroSerie);
										 System.out.println("dscSede: "+dscSede);
										 try{
										 consulta=this.detalleManager.GetAllProducto("",codSedeAux
							        		, codFamilia, codSubFamilia, codProducto
							        		, dscProduc, condicion, condicion, "","");
											}catch (Exception e) {
												e.printStackTrace();
											}
										 request.getSession().setAttribute("consulta", consulta);
										
							        	 pagina = "/logistica/consultaReportes/rep_Catalogo_Productos";
							    	
									}
									else{ if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_17))
									{//Reporte de Documentos de Pago.
								
								String getCodPerfil=request.getParameter("tCPerfil") == null ? "": request.getParameter("tCPerfil");
								String getNroOrden=request.getParameter("tNOr") == null ? "": request.getParameter("tNOr");
								String getFecInicio=request.getParameter("tFIni") == null ? "": request.getParameter("tFIni");
								String getFecFinal=request.getParameter("tFFin") == null ? "": request.getParameter("tFFin");
								String getNroRucProveedor=request.getParameter("tRucP") == null ? "": request.getParameter("tRucP");
								String getNombreProveedor=request.getParameter("tNomP") == null ? "": request.getParameter("tNomP");
								String getNroRucComprador=request.getParameter("tRucC") == null ? "": request.getParameter("tRucC");
								String getNombreComprador=request.getParameter("tNomC") == null ? "": request.getParameter("tNomC");
								String getCodTipoOrden=request.getParameter("tCTOr") == null ? "": request.getParameter("tCTOr");
								String getValSelec1=request.getParameter("tVa1") == null ? "": request.getParameter("tVa1");
								String getValSelec2=request.getParameter("tVal2") == null ? "": request.getParameter("tVal2");
								String getDscTipoOrden=request.getParameter("tDTOr") == null ? "": request.getParameter("tDTOr");
								getDscTipoOrden=getDscTipoOrden.replace("--", "");
							    //ALQD,13/11/08. ULT. PARAMETRO NUMFACTURA
								String getNumFactura=request.getParameter("tNFact") == null ? "": request.getParameter("tNFact");
								//ALQD,22/01/09. NUEVO FILTRO AGREGADO
								String getIndPorConfirmar=request.getParameter("tVa3")==null ? "":request.getParameter("tVa3");
								if(getCodTipoOrden.equals("-1"))
							    	getCodTipoOrden="";
							    if(getValSelec2.equals("0"))
							    	getValSelec2="";
							    if(getValSelec1.equals("0"))
							    	getValSelec1="";
							    if(getIndPorConfirmar.equals("0"))
							    	getIndPorConfirmar="";
								
							    model.addObject("Nro_Orden", getNroOrden);
							    model.addObject("FechaIni", getFecInicio);
							    model.addObject("FechaFin", getFecFinal);
							    model.addObject("NroProveedor", getNroRucProveedor);
							    model.addObject("NomProveedor", getNombreProveedor);
							    model.addObject("NroComprador", getNroRucComprador);
							    model.addObject("NomComprador", getNombreComprador);
							    model.addObject("Tipo_Orden", getDscTipoOrden);
							    try{
									consulta=this.gestionDocumentosManager.GetAllOrdenesAprobadas(codSede,
									        usuSolicitante,getCodPerfil, getNroOrden,
											getFecInicio, getFecFinal,getNroRucProveedor,
											getNombreProveedor, getNroRucComprador,getNombreComprador,
											getCodTipoOrden, getValSelec1,getValSelec2,getNumFactura,
											getIndPorConfirmar);
							    }catch (Exception e) {
									e.printStackTrace();
								}
								if(getValSelec1.equals("1"))
								   getValSelec1="Solo Tipos de Pago: Caja Chica";
								if(getValSelec2.equals("1"))
								   getValSelec2="Solo Doc's Sin Envio a Proveedor";
								if(getIndPorConfirmar.equals("1"))
									   getIndPorConfirmar="Ingresos por confirmar";
								
								model.addObject("ValSelec1", getValSelec1);
								model.addObject("ValSelec2", getValSelec2);
								model.addObject("ValSelec3", getIndPorConfirmar);
								
								 request.getSession().setAttribute("consultaReporte", consulta);
										
							        	pagina = "/logistica/consultaReportes/reporte_documentosPago";
							    	
									}
							else{  if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_16))
									{//Reporte de Consulta de Ordenes.
								
								String getCodPerfil=request.getParameter("tCPerfil") == null ? "": request.getParameter("tCPerfil");
								String getNroOrden=request.getParameter("tNOr") == null ? "": request.getParameter("tNOr");
								String getFecInicio=request.getParameter("tFIni") == null ? "": request.getParameter("tFIni");
								String getFecFinal=request.getParameter("tFFin") == null ? "": request.getParameter("tFFin");
								String getNroRucProveedor=request.getParameter("tRucP") == null ? "": request.getParameter("tRucP");
								String getNombreProveedor=request.getParameter("tNomP") == null ? "": request.getParameter("tNomP");
								String getNroRucComprador=request.getParameter("tRucC") == null ? "": request.getParameter("tRucC");
								String getNombreComprador=request.getParameter("tNomC") == null ? "": request.getParameter("tNomC");
								String getCodTipoOrden=request.getParameter("tCTOr") == null ? "": request.getParameter("tCTOr");
								String getDscEstado=request.getParameter("tDEst") == null ? "": request.getParameter("tDEst");
								String getCodEstado=request.getParameter("tCEst") == null ? "": request.getParameter("tCEst");
								String getDscTipoOrden=request.getParameter("tDTOr") == null ? "": request.getParameter("tDTOr");
								getDscTipoOrden=getDscTipoOrden.replace("--", "");
								getDscEstado=getDscEstado.replace("--", "");
								
								if(getCodEstado.equals("-1"))
						    		getCodEstado="";
						    	 if(getCodTipoOrden.equals("-1"))
						    		 getCodTipoOrden="";
								
							    model.addObject("Nro_Orden", getNroOrden);
							    model.addObject("FechaIni", getFecInicio);
							    model.addObject("FechaFin", getFecFinal);
							    model.addObject("NroProveedor", getNroRucProveedor);
							    model.addObject("NomProveedor", getNombreProveedor);
							    model.addObject("NroComprador", getNroRucComprador);
							    model.addObject("NomComprador", getNombreComprador);
							    model.addObject("Tipo_Estado", getDscEstado);
							    model.addObject("Tipo_Orden", getDscTipoOrden);
							    System.out.println("codSede<<"+codSede+">>usuSolicitante<<"+
							    		 usuSolicitante+">>getCodPerfil<<"+
						    			 getCodPerfil+">>getNroOrden<<"+getNroOrden+">>getFecInicio<<"+
						    			 getFecInicio+">>getFecFinal<<"+getFecFinal+">>getCodEstado<<"+
						    			 getCodEstado+">>getNroRucProveedor<<"+
						    			 getNroRucProveedor+">>getNombreProveedor<<"+getNombreProveedor+
						    			 ">>getNroRucComprador<<"+getNroRucComprador+">>getNombreComprador<<"+
						    			 getNombreComprador+">>getCodTipoOrden<<"+getCodTipoOrden);
							    try{
							    consulta= this.gestionOrdenesManager.GetAllBandejaConsultaOrdenes(codSede,
							    		 usuSolicitante, getCodPerfil, getNroOrden,
						    			 getFecInicio, getFecFinal, getCodEstado,
						    			 getNroRucProveedor, getNombreProveedor, getNroRucComprador,
						    			 getNombreComprador, getCodTipoOrden);
									}catch (Exception e) {
										e.printStackTrace();
									}
								request.getSession().setAttribute("consultaReporteOrdenes", consulta);
										
							    pagina = "/logistica/consultaReportes/reporte_consultaOrdenes";
							    	
									}
							else{	
								if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_13)){
									//Reporte de Consulta de Ordenes. 
									String getFlagEstado=request.getParameter("tFlagEstado") == null ? "": request.getParameter("tFlagEstado");
									String getCodCentroCostos=request.getParameter("tCodCeco") == null ? "": request.getParameter("tCodCeco");
									String getNombre=request.getParameter("tNombre") == null ? "": request.getParameter("tNombre");
									String getApPaterno=request.getParameter("tApPat") == null ? "": request.getParameter("tApPat");
									String getApMaterno=request.getParameter("tApMat") == null ? "": request.getParameter("tApMat");
									String getDscCentroCostos=request.getParameter("tDscCeco") == null ? "": request.getParameter("tDscCeco");
									String getNroReq=request.getParameter("tNroReq") == null ? "": request.getParameter("tNroReq");
									
								    model.addObject("Nro_Req", getNroReq);
								    model.addObject("Centro_Costo", getDscCentroCostos);
								    model.addObject("Nombres", getNombre);
								    model.addObject("Ap_Paterno", getApPaterno);
								    model.addObject("Ap_Materno", getApMaterno);
								    try{ 
								    System.out.println(getFlagEstado+">><<"+codSede);
								    if(getFlagEstado.equals("0")){
								    	consulta=this.solRequerimientoManager.GetAllAsigResp(getFlagEstado, codSede,"", "", "", "");
								    	model.addObject("Flag", "No");
								    }else{ if(getFlagEstado.equals("1"))
								    	{    System.out.println(getFlagEstado+">><<"+codSede+">><<"+
												getCodCentroCostos+">><<"+getNombre+ ">><<"+
												getApPaterno+">><<"+getApMaterno);
								    	   consulta=this.solRequerimientoManager.GetAllAsigResp(getFlagEstado, codSede,
											getCodCentroCostos, getNombre, getApPaterno, getApMaterno);
								    	 model.addObject("Flag", "Si");
								    	}
								    }
								    }catch (Exception e) {										
									}
								    request.setAttribute("flagBandeja", getFlagEstado);								    
									request.getSession().setAttribute("consultaReporteCarga", consulta);											
								    pagina = "/logistica/consultaReportes/reporte_cargaTrabajoPorUsuario";
								
								
								}
								
								}
							}		
									}
								}
							}
						}
					}
				}
			}
			
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_12)){
				 nroGuia = request.getParameter("nroGuia");
		    	 fechaGuia= request.getParameter("fechaGuia");
		    	 estado = request.getParameter("estado");
		    	 total= request.getParameter("total");
		    	 codGuia= request.getParameter("codGuia");
		    	 //ALQD,13/11/08. LEYENDO MAS DATOS
		    	 nroRequerimiento = request.getParameter("nroRequerimiento");
		    	 fecAprRequerimiento = request.getParameter("fecAprRequerimiento");
		    	 nomCenCosto = request.getParameter("nomCenCosto");
		    	 fecCierre = request.getParameter("fecCierre");
		    	 System.out.println("fecAprRequerimiento: "+fecAprRequerimiento);
		    	 System.out.println("nonCenCosto: "+nomCenCosto);
		    	 System.out.println("fecCierre: "+fecCierre);
		    	 solicitante = request.getParameter("solicitante");
		    	 lugarTrabajo = request.getParameter("lugarTrabajo");
		    	 
		    	 List lista=this.atencionRequerimientosManager.GetAllDetalleGuiaRequerimiento(codGuia);
		    	 if(lista!=null && lista.size()>0){
		    		 consulta=lista;
		     	}
		    	 
		    		request.getSession().setAttribute("consulta", consulta);
					
	        		pagina = "/logistica/consultaReportes/Rep_Guia_Salida";		    	 
		  }
		
			
		if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_29)){
				//parametros:
			
				//codSede
				//codTipoOrden
				//periodoIni
				//periodoFin
				//usuSolicitante
				//dscUsuSol
			
			     model.addObject("dscUsuSol", dscUsuSol);
		    	 List lista=this.solRequerimientoManager.getFlujoProcesosLogistica(codSede, periodoIni, periodoFin, codTipoOrden, usuSolicitante, dscUsuSol);
		    	 if(lista!=null && lista.size()>0){
		    		 consulta=lista;
		     	}		    	 
		    	request.getSession().setAttribute("consulta", consulta);					
	        	pagina = "/logistica/consultaReportes/rep_Gen_FechasOperacion";		    	 
		}
		
			
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_18)){
				 codGuia= request.getParameter("codGuia");
				 List lista=this.atencionRequerimientosManager.GetGuiaRequerimiento(codGuia);
			    	if(lista!=null && lista.size()>0){
			    		Guia guia = (Guia)lista.get(0);
			    		nroGuia = guia.getNroGuia();
			    		fechaGuia = guia.getFechaEmision();
			    		estado = guia.getEstado();
			    		nroFac = guia.getNroFactura();
			    		fechaEmi = guia.getFecEmiFactura();
			    		nroOS = guia.getNroOrden();
			    		Obs = guia.getObservacion();
			    	}
			    	
		    		request.getSession().setAttribute("consulta", consulta);
					
	        		pagina = "/logistica/consultaReportes/Rep_Guia_Atencion";
			}
		
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_15)){
				 if(codEstado.equals("-1"))
					 codEstado="";
		    	 if(codTipoOrden.equals("-1"))
		    		 codTipoOrden="";
				
				consulta=this.gestionOrdenesManager.GetAllBandejaConsultaOrdenes(codSedeE,
						usuSolicitante, 
						codPerfil, nroOrden, 
		    			 periodoIni, periodoFin, codEstado,
		    			 nroRucProv, nombreProv, nroRucComp,
		    			 nombreComp, codTipoOrden);
			
				request.getSession().setAttribute("consulta", consulta);
				
        		pagina = "/logistica/consultaReportes/Rep_Lista_Ordenes";
			}
			
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_14)){
				  
			    String getCodPerfil=request.getParameter("tCP") == null ? "": request.getParameter("tCP");
			    String getCodEstado=request.getParameter("tCE") == null ? "": request.getParameter("tCE");
				String getFechaIni=request.getParameter("tFI") == null ? "": request.getParameter("tFI");
				String getFechaFin=request.getParameter("tFF") == null ? "": request.getParameter("tFF");
				String getNroReq=request.getParameter("tNR") == null ? "": request.getParameter("tNR");
				String getCodTipoReq=request.getParameter("tCTR") == null ? "": request.getParameter("tCTR");
				String getCodTipoPago=request.getParameter("tCTP") == null ? "": request.getParameter("tCTP");
				String getProve1=request.getParameter("tP1") == null ? "": request.getParameter("tP1");
				String getProve2=request.getParameter("tP2") == null ? "": request.getParameter("tP2");
				String getConsteRadio=request.getParameter("tCR") == null ? "": request.getParameter("tCR");
				String getCodTipoServicio=request.getParameter("tCTS") == null ? "": request.getParameter("tCTS");
				String getDscTipoReq="";
				String getDscTipoPago=request.getParameter("tDTP") == null ? "": request.getParameter("tDTP");
				String getCodGrupoServ=request.getParameter("tCGS") == null ? "": request.getParameter("tCGS");
				String getDscGrupoServ=request.getParameter("tDGS") == null ? "": request.getParameter("tDGS");
				String getDscTipoReqBien=request.getParameter("tDTRB") == null ? "": request.getParameter("tDTRB");
				//ALQD,30/01/09. SE A�ADE NUEVO FILTRO
				String getIndCotSinOc=request.getParameter("tVal1") == null ? "": request.getParameter("tVal1");
				 String SubTipoReq="";
		          if(getCodTipoReq.equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
			        	{SubTipoReq=getConsteRadio;
			        	 getCodGrupoServ="";
			        	 request.setAttribute("TipoReq", getCodTipoReq);
			        	 getDscTipoReq="Bien";
			        	}
			      else if(getCodTipoReq.equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
			        	 {if(!getCodTipoServicio.equals("-1"))
			        	       SubTipoReq=getCodTipoServicio;
			        	  else SubTipoReq = "";
			        	  request.setAttribute("TipoReq", getCodTipoReq);
			        	  getDscTipoReq="Servicio";
			        	 }
			    if(getCodEstado.equals("-1"))
			        	getCodEstado = "";
			    if(getCodTipoPago.equals("-1"))
			        	getCodTipoPago = "";
			    if(getDscTipoReqBien.equals("A"))
			    	getDscTipoReqBien = "Activo";
			    if(getDscTipoReqBien.equals("C"))
			    	getDscTipoReqBien = "Consumible";
			    if(getCodGrupoServ.equals("-1"))
			    	getCodGrupoServ = "";
			    if(getIndCotSinOc.equals("0")) getIndCotSinOc="";
				System.out.println("codSede>>"+codSede+">>getNroReq<<"+
						getNroReq+">>getFechaIni<<"+getFechaIni+">>getFechaFin<<"+getFechaFin+">>getCodEstado<<"+
						getCodEstado+">>getCodTipoReq<<"+getCodTipoReq+">>SubTipoReq<<"+SubTipoReq+">>getCodTipoPago<<"+
						getCodTipoPago+">>getProve1<<"+getProve1+">>getProve2<<"+getProve2+">>getCodPerfil<<"+
						getCodPerfil+">>usuSolicitante<<"+usuSolicitante+">>getCodGrupoServ<<"+getCodGrupoServ
						+">>indCotSinOc<<"+getIndCotSinOc);
				consulta=this.cotBienesYServiciosManager.GetAllSolicitudCotizacion(codSede,
						getNroReq, getFechaIni, getFechaFin,
						getCodEstado, getCodTipoReq, SubTipoReq,
						getCodTipoPago, getProve1, getProve2,
						getCodPerfil, usuSolicitante,getCodGrupoServ,getIndCotSinOc);
				
				/*--------REPORTE 14---------*/
				model.addObject("nroCotizacion", getNroReq);
				model.addObject("txhtipo", getDscTipoReq);
				model.addObject("txhTipoBien", getDscTipoReqBien);
				model.addObject("fechaIni", getFechaIni);
				model.addObject("fechaFin", getFechaFin);
				model.addObject("DesTipoPago", getDscTipoPago);
				model.addObject("txtProv1", getProve1);
				model.addObject("txtProv2", getProve1); 
				model.addObject("dscUser", dscUsuarioSolicitante);
				model.addObject("dscSedeE", txhNomSede);
				model.addObject("txhGrupoServ", getDscGrupoServ);
				model.addObject("txhTipoServ", getDscGrupoServ);
				
				request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_Bandeja_Colicitud_Cotizacion";
			}
			
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_19)){
				
				List list= this.cotBienesYServiciosManager.GetAllCotizacionById(Ccot);
				if(list!=null && list.size()>0){
					if ( list.size()>0 )
	        		{
	        			Cotizacion cotizacion = null;
	        			cotizacion = (Cotizacion)list.get(0);
	        			tPago = cotizacion.getDscTipoPago();
	        			Vc = cotizacion.getFechaInicio();
	        			Vc1 = cotizacion.getFechaFin();
	        			Hi = cotizacion.getHoraInicio();
	        			Hi1 = cotizacion.getHoraFin();
	        			NroCo = cotizacion.getNroCoti();
	        		}
				}
        		//ALQD,21/09/08. INVOCANDO LLAMADA DE CONDICIONES COMERCIALES
        		List lst=this.cotBienesYServiciosManager.GetAllCondicionCotizacion(Cco);
        		if(lst!=null && lst.size()>0){
        			lstCondComerciales=lst;
				}
        		request.getSession().setAttribute("condComercial", lstCondComerciales);
				
				List lista=this.cotBienesYServiciosManager.GetAllDetalleCotizacionActual(Ccot, Ctco);
		    	if(lista!=null && lista.size()>0){
		    		consulta=lista;
		    	}
		    	
		    	model.addObject("tCPro", tCPro);
				model.addObject("tPago", tPago);
				model.addObject("Vc", Vc);
				model.addObject("Vc1", Vc1);
				model.addObject("Hi", Hi);
				model.addObject("Hi1", Hi1);
				model.addObject("txhNomSede", txhNomSede);
				model.addObject("dscUsuarioSolicitante", dscUsuarioSolicitante);
		    	
		    	request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_CotizacionActual";
			}
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_20)){
				List list= this.cotBienesYServiciosManager.GetAllCotizacionById(Ccot);
				String nomComprador = "";
				if(list!=null && list.size()>0){
					if ( list.size()>0 )
	        		{
	        			Cotizacion cotizacion = null;
	        			cotizacion = (Cotizacion)list.get(0);
	        			tPago = cotizacion.getDscTipoPago();
	        			Vc = cotizacion.getFechaInicio();
	        			Vc1 = cotizacion.getFechaFin();
	        			Hi = cotizacion.getHoraInicio();
	        			Hi1 = cotizacion.getHoraFin();
	        			NroCo = cotizacion.getNroCoti();
	        			nomComprador = cotizacion.getUsuResponsable();
	        		}
				}
        		//alqd,21/09/08. INVOCANDO LLAMADA DE CONDICIONES COMERCIALES
        		List lst=this.cotBienesYServiciosManager.GetAllCondicionCotizacion(Cco);
        		if(lst!=null && lst.size()>0){
        			lstCondComerciales=lst;
				}
        		request.getSession().setAttribute("condComercial", lstCondComerciales);
				List lista=this.cotBienesYServiciosManager.GetAllDetalleCotizacionActual(Ccot, Ctco);
		    	if(lista!=null && lista.size()>0){
		    		consulta=lista;
		    	}
		    	
		    	model.addObject("tCPro", tCPro);
				model.addObject("tPago", tPago);
				model.addObject("Vc", Vc);
				model.addObject("Vc1", Vc1);
				model.addObject("Hi", Hi);
				model.addObject("Hi1", Hi1);
				model.addObject("txhNomSede", txhNomSede);
				model.addObject("dscUsuarioSolicitante", nomComprador);
		    	
		    	request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_CotizacionServicio";
			}
			
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_21)){
				List lista= this.cotBienesYServiciosManager.GetAllCotizacionById(Cco);
				//ALQD,06/02/09.NUEVA VARIABLE
				String nomComprador = "";
				if(lista!=null && lista.size()>0){
				
		        		if ( lista.size()>0 )
		        		{
		        			Cotizacion cotizacion = null;
		        			cotizacion = (Cotizacion)lista.get(0);
		        			tPago = cotizacion.getDscTipoPago();
		        			Vc = cotizacion.getFechaInicio();
		        			Vc1 = cotizacion.getFechaFin();
		        			Hi = cotizacion.getHoraInicio();
		        			Hi1 = cotizacion.getHoraFin();
		        			NroCo = cotizacion.getNroCoti();
		        			nomComprador = cotizacion.getUsuResponsable();
		        		}
		        		//ALQD,21/09/08. INVOCANDO LLAMADA DE CONDICIONES COMERCIALE
		        		List lst=this.cotBienesYServiciosManager.GetAllCondicionCotizacion(Cco);
		        		if(lst!=null && lst.size()>0){
		        			lstCondComerciales=lst;
						}
		        		request.getSession().setAttribute("condComercial", lstCondComerciales);
		        		if(CtRe.equals(CommonConstants.COD_REP_BIEN_SERVICIO)){
		        			List list=this.cotBienesYServiciosManager.GetAllDetalleCotizacionActual(Cco, CtRe);
		    		    	if(list!=null && list.size()>0){
		    		    		consulta=list;
		    		    	}
		    		    	request.getSession().setAttribute("consulta", consulta);
		            		pagina = "/logistica/consultaReportes/Rep_CotizacionActual";
		        		}
		        		else{
		        			List lis=this.cotBienesYServiciosManager.GetAllDetalleCotizacionActual(Cco, CtRe);
		    		    	if(lis!=null && lis.size()>0){
		    		    		consulta=lis;
		    		    		System.out.println("tot: "+consulta.size());
			    		    	//ALQD,23/04/09. LEYENDO DOS DATOS DEL RECORDSET
			    		    	CotizacionDetalle cotDetalle = (CotizacionDetalle)lis.get(0);
			    		    	gruServicio = cotDetalle.getProducto();
			    		    	tipServicio = cotDetalle.getUnidadMedida();
		    		    	}
		    		    	request.getSession().setAttribute("consulta", consulta);
		            		pagina = "/logistica/consultaReportes/Rep_CotizacionServicio";
		        		}
				}
				
				model.addObject("NroCo", NroCo);
				model.addObject("tCPro", tCPro);
				model.addObject("tPago", tPago);
				model.addObject("Vc", Vc);
				model.addObject("Vc1", Vc1);
				model.addObject("Hi", Hi);
				model.addObject("Hi1", Hi1); 
				model.addObject("txhNomSede", txhNomSede);
				model.addObject("dscUsuarioSolicitante", nomComprador);
				model.addObject("gruServicio", gruServicio);
				model.addObject("tipServicio", tipServicio);
			}
		
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_22)){
				float sumDebe = 0;
				float sumHaber = 0;
				List listas = this.interfazContableManager.GetResultadoInterfazContable(codSede
						, cMes, cAnio, tInt);
					if(listas!=null && listas.size()>0){
					
		        		if ( listas.size()>0 )
		        		{
		        			consulta=listas;
				    		
				    		InterfazContableResultado interfazContableResultado = null;
				    	
				    		for(int i = 0;i<listas.size();i++){
				    			interfazContableResultado = (InterfazContableResultado)listas.get(i);
				    			sumDebe = sumDebe + Float.valueOf(interfazContableResultado.getDebe());
				    			sumHaber = sumHaber + Float.valueOf(interfazContableResultado.getHaber());
				    		}
				    		totDebe = String.valueOf(sumDebe);
				    		totHaber = String.valueOf(sumHaber);
		        		}
		        		}
					model.addObject("totDebe", totDebe);
					model.addObject("totHaber", totHaber);
					model.addObject("dMes", dMes);
            		model.addObject("dAnio", dAnio);
            		model.addObject("txhNomSede", txhNomSede);
            		
            		request.getSession().setAttribute("consulta", consulta);
            		pagina = "/logistica/consultaReportes/Rep_ResultadoInterfazContable";
			}
			if (codTipoReporte.equals(CommonConstants.REPORTE_LOG_24)){
				//datos de cabecera..
		    	nroDocumento = request.getParameter("nroDocumento");
		    	codUsuario = request.getParameter("codUsuario");
		    	nroOrden = request.getParameter("nroOrden");
		    	lugarTrabajo = request.getParameter("lugarTrabajo");
				System.out.println("nroOrden: "+nroOrden);

		    	String sNroFactura = "";
		    	String sNroGuia = "";
		    	String sNroOrden = "";
		    	String sFechaEmi1 = "";
		    	String sFechaEmi2 = "";
		    	String sFechaEmi3 = "";
		    	String sFechaVcto = "";
		    	String sMoneda = "";
		    	String sMonto = "";
		    	String sSubTotal = "";
		    	String sIGV = "";
		    	String sTotal = "0";
//ALQD,13/11/08. A�ADIENDO NUEVAS PROPIEDADES
		    	String sNroInterno = "";
		    	String sFecCierre = "";
		    	String sEstIngreso = "";
		    	String sNomProveedor = "";
		    	
				List cabeceraDocPago = this.gestionDocumentosManager.GetAllDocPago(nroOrden, nroDocumento);
	        	if(cabeceraDocPago!=null ){
	        		if ( cabeceraDocPago.size()>0 ){
	        			DocPago docPago = null;
	        			docPago = (DocPago)cabeceraDocPago.get(0);
	        			sNroFactura = docPago.getFactura();
	        			sNroGuia = docPago.getNroGuia();
	        			sNroOrden = docPago.getNroOrden();
	        			sFechaEmi1 = docPago.getFecEmiFactura();
	        			sFechaEmi2 = docPago.getFecEmiGuia();
	        			sFechaEmi3 = docPago.getFecEmiOrden();
	        			sFechaVcto = docPago.getFecVctoFactura();
	        			sMoneda = docPago.getSigMoneda().trim();
	        			sMonto = docPago.getMontoOrden().trim();
	        			sSubTotal = docPago.getSubTotal().trim();
	        			sIGV = docPago.getIgv().trim();
	        			sTotal = docPago.getTotalFacturado().trim();
	        			sNroInterno = docPago.getNroInterno().trim();
	        			sFecCierre = docPago.getFecCierre().trim();
	        			sEstIngreso = docPago.getEstIngreso().trim();
	        			sNomProveedor = docPago.getNomProveedor().trim();
	        		}
	        	}
				model.addObject("sNroFactura", sNroFactura);
				model.addObject("sNroGuia", sNroGuia);
				model.addObject("sNroOrden", sNroOrden);
        		model.addObject("sFechaEmi1", sFechaEmi1);
        		model.addObject("sFechaEmi2", sFechaEmi2);
        		model.addObject("sFechaEmi3", sFechaEmi3);
        		model.addObject("sFechaVcto", sFechaVcto);
        		model.addObject("sMoneda", sMoneda);
        		model.addObject("sMonto", sMonto);
        		model.addObject("sSubTotal", sSubTotal);
        		model.addObject("sIGV", sIGV);
        		model.addObject("sTotal", sTotal);
        		model.addObject("sNroInterno", sNroInterno);
        		model.addObject("sFecCierre", sFecCierre);
        		model.addObject("sEstIngreso", sEstIngreso);
        		model.addObject("sNomProveedor", sNomProveedor);
	        	
	        	//datos detalles del doc pago...
        		consulta = this.gestionDocumentosManager.GetAllDetalleDocPago(nroDocumento);
        		request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_DocumentoPago";
			}
			//FORMATO DE OC
			if(codTipoReporte.equals(CommonConstants.REPORTE_LOG_23)){
				float suma=0;
				float sk=0;
				float b=0;
				String igv = "";
				String nroOrd ="";
				String fEmi ="";
				String nroDoc = "";
				String prov = "";
				String atenc ="";
				String mont = "";
				String comprador ="";
				String mond ="";
				String indImport = "";
				String importOtros = "";
				String totsum ="";
				String codEstadoOC = ""; //JHPR 2008-09-25
				String desEstadoOC= "";
				String tipoPago="";
				String cadCenCosto=""; //ALQD,28/01/09.
				String numCotizacion=""; //ALQD,05/02/09.
				String pais=""; //ALQD,03/07/09.
				String telefono=""; //ALQD,03/07/09.
				String emailProv=""; //ALQD,03/07/09.
				String emailComprador=""; //ALQD,03/07/09.
				 
				String codOrden=request.getParameter("srtCodOrden");
				System.out.println("codOrden: "+codOrden);
				
				List lista= new ArrayList();
		    	lista=this.gestionOrdenesManager.GetAllOrdenById(codOrden);
		    	if(lista!=null)
		    	{ if(lista.size()>0)
		    		{ DetalleOrden detalleOrden= new DetalleOrden();
		    		  detalleOrden=(DetalleOrden)lista.get(0);
		    		  //ALQD,25/05/09.ADICIONANDO LA SEDE AL # DE LA OC
		    		  nroOrd = detalleOrden.getDscNroOrden().trim()+"-"+detalleOrden.getCodSede();
		    		  fEmi = detalleOrden.getFechaEmision().trim();
		    		  nroDoc = detalleOrden.getDscNroDocumento().trim();
		    		  prov = detalleOrden.getDscProveedor().trim();
		    		  atenc = detalleOrden.getDscAtencionA().trim();
		    		  mont = detalleOrden.getDscMonto().trim();
		    		  comprador = detalleOrden.getDscComprador().trim();
		    		  mond = detalleOrden.getDscMoneda().trim();
		    		  igv = detalleOrden.getIgv().trim();
		    		  indImport = detalleOrden.getIndImportacion();
		    		  importOtros = detalleOrden.getImporteOtros();
		    		  codEstadoOC = detalleOrden.getCodEstado();
		    		  desEstadoOC = detalleOrden.getDescEstadoOC().trim();
		    		  tipoPago = detalleOrden.getTipoDePago(); //0001 CCH, 0002 OC
		    		  cadCenCosto=detalleOrden.getCadCenCosto().trim();
		    		  //ALQD,05/02/09. PASANDO EL NUMCOTIZACION
		    		  numCotizacion=detalleOrden.getNumCotizacion().trim();
		    		  codTipoReq=detalleOrden.getCodTipoReq().trim();//ALQD,24/104/09.
		    		  //ALQD,03/07/09.LEYENDO NUEVAS VARIABLES
		    		  pais=detalleOrden.getPais();
		    		  telefono=detalleOrden.getTelefono();
		    		  emailProv=detalleOrden.getEmailProv();
		    		  emailComprador=detalleOrden.getEmailComprador();
		    		}
		    	}
		    	
		    	List listas= new ArrayList();
			    //OBTENIENDO DETALLE DE LA OC, INCLUYE LAS NOTAS INGRESADA POR LOS USUARIOS
		    	listas=this.gestionOrdenesManager.GetAllDetalleByOrden(codOrden);
				if(listas!=null)
		    	{
					consulta=listas;
		    	}
				
				if(listas.size()>0)
	    		{ 
	    		 float sumaTotal=0;
	    		  for(int a=0;a<listas.size();a++)
				  { DetalleOrden detalleOrden= new DetalleOrden();
	    			detalleOrden=(DetalleOrden)listas.get(a);
				    sk=Float.valueOf(detalleOrden.getSubtotal().trim());
				    suma=suma+sk;
				  }
	    		  System.out.println("valor de Suma: "+suma);
				  String subsum = String.valueOf(suma);
				  sumaTotal=suma+ Float.valueOf(igv.trim());
				  totsum = String.valueOf(sumaTotal);
	    		}
				
				model.addObject("tituloOrden",
					(codTipoReq.trim().equals("0002")?"ORDEN DE SERVICIO":
						tipoPago.trim().equals("0001")?"CAJA CHICA":
							(tipoPago.trim().equals("0002")?"ORDEN DE COMPRA":"")));//ALQD,24/04/09.
				model.addObject("totsum", totsum);
				model.addObject("igv", igv);
				model.addObject("fEmi", fEmi);
				model.addObject("nroOrd", nroOrd);
				model.addObject("prov", prov);
				model.addObject("nroDoc", nroDoc);
				model.addObject("atenc", atenc);
				model.addObject("mont", mont);
				model.addObject("comprador", comprador);
				model.addObject("mond", mond);
				model.addObject("indImport", indImport);
				model.addObject("importOtros", importOtros);
				model.addObject("codEstadoOC",codEstadoOC);
				model.addObject("desEstadoOC", desEstadoOC);
				model.addObject("cadCenCosto", cadCenCosto);
				model.addObject("numCotizacion", numCotizacion);
				model.addObject("telefono",telefono);
				model.addObject("pais",pais);
				model.addObject("emailProv",emailProv);
				model.addObject("emailComprador",emailComprador);
				model.addObject("codSede",codSede);
				
				List listar= new ArrayList();
				//OBTENIENDO DETALLE DE DOCS ASOCIADOS DE UNA OC IMPORTACION
				listar = this.detalleManager.GetAllDocAsociado( codOrden);
		        
				//CodEstado --> Estado orden de compra aprobada.
				
				request.getSession().setAttribute("listar", listar);

				//OBTENIENDO LAS CONDICIONES COMERCIALES
				List lst=this.gestionOrdenesManager.GetAllCondicionByOrden(codOrden);
        		if(lst!=null && lst.size()>0){
        			lstCondComerciales=lst;
				}
        		request.getSession().setAttribute("condComercial", lstCondComerciales);
        		
				request.getSession().setAttribute("consulta", consulta);
				if(!indImport.equals("1")){
					pagina = "/logistica/consultaReportes/Rep_OrdenCompra";
				}
				else{
					pagina = "/logistica/consultaReportes/Rep_OrdenCompra_Importacion";
				}
			}
			//ALQD,09/01/09. NUEVO REPORTE DE OCs PENDIENTE DE ENTREGA
			if (codTipoReporte.equals(CommonConstants.REPORTE_LOG_25)){
		    	codUsuario = request.getParameter("codUsuario");
        		consulta = this.gestionOrdenesManager.GetAllOC_pendienteXentregar(codUsuario);
        		request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_OC_PorEntregar";
			}
			//ALQD,27/01/09. NUEVO REPORTE DE REQUERIMIENTOS PENDIENTES
			if (codTipoReporte.equals(CommonConstants.REPORTE_LOG_26)){
		    	codUsuario = request.getParameter("codUsuario");
		    	
        		consulta = this.solRequerimientoManager.GetAllSolReqAprPenEntUsuario(codUsuario);
        		request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_ReqAprob_PorEntregar";
			}
			//ALQD,18/01/10. NUEVO REPORTE DE REQUERIMIENTOS APROBADOS
			if (codTipoReporte.equals("0028")){
		    	codUsuario = request.getParameter("codUsuario");
		    	
        		consulta = this.solRequerimientoManager.GetAllSolReqAprobados(codUsuario);
        		request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_ReqAprobados";
			}
			
			/*//JHPR 15/09/10 NUEVO REPORTE FECHAS DE PROCESOS
			if (codTipoReporte.equals(CommonConstants.REPORTE_LOG_29)){
		    	codUsuario = request.getParameter("codUsuario");		    	
        		consulta = this.solRequerimientoManager.GetAllSolReqAprobados(codUsuario);
        		request.getSession().setAttribute("consulta", consulta);
        		pagina = "/logistica/consultaReportes/Rep_ReqAprobados";
			}*/
			
			//ALQD,03/07/09.NUEVO REPORTE PARA LIQUIDACION DE IMPORTACION
			if (codTipoReporte.equals("0027")){
				//datos de cabecera..
		    	nroDocumento = request.getParameter("nroDocumento");
		    	codUsuario = request.getParameter("codUsuario");
		    	nroOrden = request.getParameter("nroOrden");
		    	lugarTrabajo = request.getParameter("lugarTrabajo");
				System.out.println("nroOrden: "+nroOrden);

        		float suma=0;
				float sk=0;
				String igv = "";
				String nroOrd ="";
				String fEmi ="";
				String nroDoc = "";
				String prov = "";
				String atenc ="";
				String mont = "";
				String comprador ="";
				String mond ="";
				String indImport = "";
				String importOtros = "";
				String totsum ="";
				String codEstadoOC = "";
				String desEstadoOC= "";
				String tipoPago="";
				String cadCenCosto="";
				String numCotizacion="";
				String pais="";
				String telefono="";
				String emailProv=""; //ALQD,03/07/09.
				String emailComprador=""; //ALQD,03/07/09.
				
				List lista= new ArrayList();
		    	lista=this.gestionOrdenesManager.GetAllOrdenById(nroOrden);
		    	if(lista!=null)
		    	{ if(lista.size()>0)
		    		{ DetalleOrden detalleOrden= new DetalleOrden();
		    		  detalleOrden=(DetalleOrden)lista.get(0);
		    		  nroOrd = detalleOrden.getDscNroOrden().trim()+"-"+detalleOrden.getCodSede();
		    		  fEmi = detalleOrden.getFechaEmision().trim();
		    		  nroDoc = detalleOrden.getDscNroDocumento().trim();
		    		  prov = detalleOrden.getDscProveedor().trim();
		    		  atenc = detalleOrden.getDscAtencionA().trim();
		    		  mont = detalleOrden.getDscMonto().trim();
		    		  comprador = detalleOrden.getDscComprador().trim();
		    		  mond = detalleOrden.getDscMoneda().trim();
		    		  igv = detalleOrden.getIgv().trim();
		    		  indImport = detalleOrden.getIndImportacion();
		    		  importOtros = detalleOrden.getImporteOtros();
		    		  codEstadoOC = detalleOrden.getCodEstado();
		    		  desEstadoOC = detalleOrden.getDescEstadoOC().trim();
		    		  tipoPago = detalleOrden.getTipoDePago(); //0001 CCH, 0002 OC
		    		  cadCenCosto=detalleOrden.getCadCenCosto().trim();
		    		  numCotizacion=detalleOrden.getNumCotizacion().trim();
		    		  codTipoReq=detalleOrden.getCodTipoReq().trim();//ALQD,24/104/09.
		    		  pais=detalleOrden.getPais();
		    		  telefono=detalleOrden.getTelefono();
		    		  emailProv=detalleOrden.getEmailProv();
		    		  emailComprador=detalleOrden.getEmailComprador();
		    		}
		    	}
		    	
		    	List listas= new ArrayList();
			    //OBTENIENDO DETALLE DE LA OC, INCLUYE LAS NOTAS INGRESADA POR LOS USUARIOS
		    	listas=this.gestionOrdenesManager.GetAllDetalleByOrden(nroOrden);
				if(listas!=null)
		    	{
					consulta=listas;
		    	}
				
				if(listas.size()>0)
	    		{ 
	    		 float sumaTotal=0;
	    		  for(int a=0;a<listas.size();a++)
				  { DetalleOrden detalleOrden= new DetalleOrden();
	    			detalleOrden=(DetalleOrden)listas.get(a);
				    sk=Float.valueOf(detalleOrden.getSubtotal().trim());
				    suma=suma+sk;
				  } 
	    		  System.out.println("valor de Suma: "+suma);
				  sumaTotal=suma+ Float.valueOf(igv.trim());
				  totsum = String.valueOf(sumaTotal);
	    		}
				
				model.addObject("tituloOrden",
					(codTipoReq.trim().equals("0002")?"ORDEN DE SERVICIO":
						tipoPago.trim().equals("0001")?"CAJA CHICA":
							(tipoPago.trim().equals("0002")?"ORDEN DE COMPRA":"")));//ALQD,24/04/09.
				model.addObject("totsum", totsum);
				model.addObject("igv", igv); 
				model.addObject("fEmi", fEmi);
				model.addObject("nroOrd", nroOrd);
				model.addObject("prov", prov);
				model.addObject("nroDoc", nroDoc);
				model.addObject("atenc", atenc);
				model.addObject("mont", mont);
				model.addObject("comprador", comprador);
				model.addObject("mond", mond);
				model.addObject("indImport", indImport);
				model.addObject("importOtros", importOtros);
				model.addObject("codEstadoOC",codEstadoOC);
				model.addObject("desEstadoOC", desEstadoOC);
				model.addObject("cadCenCosto", cadCenCosto);
				model.addObject("numCotizacion", numCotizacion);
				model.addObject("telefono",telefono);
				model.addObject("pais",pais);
				model.addObject("emailProv",emailProv);
				model.addObject("emailComprador",emailComprador);
				
				List listar= new ArrayList();
				//OBTENIENDO DETALLE DE DOCS ASOCIADOS DE UNA OC IMPORTACION
				listar = this.detalleManager.GetAllDocAsociado( nroOrden);
		        
				request.getSession().setAttribute("listar", listar);

				//OBTENIENDO LAS CONDICIONES COMERCIALES
				List lst=this.gestionOrdenesManager.GetAllCondicionByOrden(nroOrden);
        		if(lst!=null && lst.size()>0){
        			lstCondComerciales=lst;
				}
        		request.getSession().setAttribute("condComercial", lstCondComerciales);
        		
				request.getSession().setAttribute("consulta", consulta);
    			pagina = "/logistica/consultaReportes/Rep_DocumentoPago_Importacion";
			}

			/*----Reporte 12---*/
			model.addObject("nroGuia", nroGuia);
			model.addObject("solicitante",solicitante);
			model.addObject("lugarTrabajo",lugarTrabajo);//jhpr 2008-09-26
			model.addObject("fechaGuia", fechaGuia);
			model.addObject("estado", estado);
			model.addObject("total", total);
			model.addObject("nroFac", nroFac);
			model.addObject("fechaEmi", fechaEmi);
			model.addObject("nroOS", nroOS);
			model.addObject("Obs", Obs);
			model.addObject("condicion", condicion);
			model.addObject("dscCondicion", dscCondicion);
			model.addObject("nroSerie", nroSerie);
			model.addObject("codProducto", codProducto);
			model.addObject("imagen", imagen);
			//ALQD,13/11/08. A�ADIENDO LOS NUEVOS DATOS
			model.addObject("nroRequerimiento",nroRequerimiento);
			model.addObject("fecAprRequerimiento",fecAprRequerimiento);
			model.addObject("nomCenCosto",nomCenCosto);
			model.addObject("fecCierre",fecCierre);
			
			/*-------REPORTE 15----------*/
			model.addObject("nroOrden", nroOrden);
			model.addObject("nroRucProv", nroRucProv);
			model.addObject("nombreProv", nombreProv);
			model.addObject("nroRucComp", nroRucComp);
			model.addObject("nombreComp", nombreComp);
			model.addObject("codTipoOrden", codTipoOrden);
			model.addObject("tipoOrden", tipoOrden);
			model.addObject("estado", estado);
			model.addObject("periodoIni", periodoIni);
			model.addObject("periodoFin", periodoFin);
			/*--------------------------*/
			
			model.addObject("dscProduc", dscProduc);
			model.addObject("dscFamilia", dscFamilia);
			model.addObject("dscSubFamilia", dscSubFamilia);
			model.addObject("dscSede", dscSede);
	    	log.info("onSubmit:FIN");
	    	
	    	log.info("pagina:>>"+pagina+">>");
		    return new ModelAndView(pagina,"model",model);
	    }
	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public void setInterfazContableManager(
			InterfazContableManager interfazContableManager) {
		this.interfazContableManager = interfazContableManager;
	}

}
