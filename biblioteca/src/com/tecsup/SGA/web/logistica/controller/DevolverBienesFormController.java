package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.DevolverBienesCommand;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Devolucion;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.AlmacenManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;

public class DevolverBienesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(DevolverBienesFormController.class);		
	private AlmacenManager almacenManager;
	private DetalleManager detalleManager;
	SolRequerimientoManager solRequerimientoManager;
	
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public void setAlmacenManager(AlmacenManager almacenManager) {
		this.almacenManager = almacenManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");    	
    	DevolverBienesCommand command = new DevolverBienesCommand();
    	//*********************************************************    	
    	command.setCodDevolucion(request.getParameter("txhCodDevolucion") == null ? "": request.getParameter("txhCodDevolucion"));
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setCodTipoBien(request.getParameter("txhCodTipoBien") == null ? "": request.getParameter("txhCodTipoBien"));
    	System.out.println("codDevolucion="+command.getCodDevolucion()+"<<");
    	System.out.println("codSede="+command.getCodSede()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");   
    	System.out.println("CodTipoBien="+command.getCodTipoBien()+"<<");
    	command.setCodTipoBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setCodTipoBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	//*********************************************************
    	if(command.getCodDevolucion()!= null){
    		if(!command.getCodDevolucion().equals("")){    			
    			CargaDatosCabecera(command);
    			command.setCodSede(request.getParameter("txhCodSede"));
    	  	    log.info("CodSede: "+command.getCodSede());
    	        command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
    	        System.out.println("EstadoAlmacen: "+command.getEstadoAlmacen());
    	    }
    	}
    	command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN);
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	DevolverBienesCommand control = (DevolverBienesCommand)command;
    	String resultado="";    	
    	
    	if (control.getOperacion().trim().equals("CONFIRMAR")){
    		resultado = ConfirmarDevolucion(control);    		
    		 if(resultado.equals("-2"))
			      control.setMsg("ALMACEN_CERRADO");
    		 else if(resultado.equals("-1"))
    			       control.setMsg("ERROR_CONFIRMAR");
    			  else control.setMsg("OK_CONFIRMAR");   
    			
    	}
    	else
    	if(control.getOperacion().trim().equals("RECHAZAR")){
    		resultado=RechazarDevolucion(control);
    		
    		if(resultado.equals("-2"))
			        control.setMsg("ALMACEN_CERRADO");
    		else if(resultado.equals("-1"))
    				  control.setMsg("ERROR_RECHAZAR");
    			 else control.setMsg("OK_RECHAZAR");	
    			      
    	}
    	CargaDatosCabecera(control);
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/Almacen/DevolverBienes","control",control);
    }    
    private void CargaDatosCabecera(DevolverBienesCommand control){
    	//carga los codRadio tipo de bien se guardan en dos hidden
    	TipoLogistica obj2 = new TipoLogistica();    	
    	obj2.setCodPadre("");
    	obj2.setCodId("");
    	obj2.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
    	obj2.setCodigo("");
    	obj2.setDescripcion("");
    	obj2.setTipo(CommonConstants.TIPO_ORDEN_COD);    	
    	control.setListaCodTipoBien(this.detalleManager.GetAllTablaDetalle(obj2));
    	Logistica logistica = new Logistica();
    	logistica=(Logistica)control.getListaCodTipoBien().get(0);
    	control.setCodTipoBienConsumible(logistica.getCodSecuencial());
    	logistica=(Logistica)control.getListaCodTipoBien().get(1);
    	control.setCodTipoBienActivo(logistica.getCodSecuencial());
    	//*****************************************
    	ArrayList<Devolucion> lista;
    	
		lista = (ArrayList<Devolucion>)this.almacenManager.GetDatosDevolucion(control.getCodDevolucion());		
		
		if ( lista != null )
			if ( lista.size() > 0 ){
				
				Devolucion devolucion = lista.get(0);
	    		
	    		control.setCodDevolucion(devolucion.getCodDevolucion());
	    		
	    		control.setNroDevolucion(devolucion.getNroDevolucion());
	    		control.setFechaDevolución(devolucion.getFechaDevolucion());
	    		control.setCentroCosto(devolucion.getDscCentroCosto());
	    		control.setUsuSolicitante(devolucion.getUsuSolicitante());
	    		control.setTipoRequerimiento(devolucion.getDscTipoBien());	    		
	    		//control.setCodSubTipoRequerimiento(devolucion.getCodSubTipoRequerimiento());
	    		control.setCodRequerimiento(devolucion.getCodRequerimiento());
	    		control.setCodEstado(devolucion.getCodEstado());
	    		control.setCodTipoBien(devolucion.getCodSubTipoRequerimiento());
	    		control.setNroReqAsociado(devolucion.getNroReqAsociado());	    		
	    		control.setMotivoDevolucion(devolucion.getMotivoDevolucion());
	    		control.setObservaciones(devolucion.getObservacionDevolucion());
	    		//carga la bandeja de la solicitud del usuario
	    		cargaBandejaDetalle(control);
			}
    }
    private void cargaBandejaDetalle(DevolverBienesCommand control){    	
		List listaBien=this.almacenManager.GetAllDetalleDevolucion(control.getCodDevolucion());
				
		if(listaBien!=null){
			if(listaBien.size()>0){
				control.setListaBienes(listaBien);
				control.setTamListaBienes(""+listaBien.size());
			}
			else{
				control.setTamListaBienes("0");
			}
		}    	
    }
    private String ConfirmarDevolucion(DevolverBienesCommand control){
    	String resultado="";
    	resultado=this.almacenManager.ActConfirmarDev(control.getCodDevolucion(), control.getCodUsuario(), control.getObservaciones());
    	return resultado;
    }    
    private String RechazarDevolucion(DevolverBienesCommand control){
    	String resultado="";
    	resultado=this.almacenManager.ActRechazarDev(control.getCodDevolucion(), control.getCodUsuario(), control.getObservaciones());
    	return resultado;
    }

	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
}
