package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.modelo.ResumenPendientePorCotizar;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.AreqBandejaEvaluadorCommand;
import com.tecsup.SGA.web.logistica.command.PenBandejaAtencionCotizacionCommand;

public class PenBandejaAtencionCotizacionFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PenBandejaAtencionCotizacionFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	PenBandejaAtencionCotizacionCommand command= new PenBandejaAtencionCotizacionCommand();
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    		command.setCodPerfil(usuarioSeguridad.getPerfiles());
    	}
    	command.setCodSede(request.getParameter("txhCodSede"));
    	if(command.getCodSede()==null)
    		command.setCodSede("");
    	command.setValActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setValConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	
    	command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	command.setConsteBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    	command.setConsteServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
    	command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	
    	BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean!= null)
         {  log.info("BandejaLogisticaBean:INICIO");
            if(bandejaBean.getCodBandeja().equals("3")){
            	command.setCodTipoRequerimiento(bandejaBean.getValor1());
            	command.setTxtNroRequerimiento(bandejaBean.getValor3());
            	command.setCodFamilia(bandejaBean.getValor4());
            	command.setCodSubFamilia(bandejaBean.getValor5());
            	command.setCodGrupoServicio(bandejaBean.getValor6());
            	command.setCodCentroCosto(bandejaBean.getValor7());
            	command.setTxtUsuSolicitante(bandejaBean.getValor8());
            	System.out.println("getCodGrupoServicio: "+command.getCodGrupoServicio());
       	 	 if(command.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
       	 		{
       	 		   command.setConsteRadio(bandejaBean.getValor2());
       	 		   command.setCodTipoServicio("-1");
       	 		   Buscar_Lista_Familia(command);
       	 		   Buscar_Familia(command, request);
       	 		   Buscar_Bienes(command, request);
       	 		}
       	 	 else if(command.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
       	 		    {  command.setCodTipoServicio(bandejaBean.getValor2());
       	 		       Buscar_Servicio(command, request);
       	 		    }
            }
       	  log.info("BandejaLogisticaBean:FIN");
         }
    	else{   command.setBanListaServicios("0");
    			command.setBanListaBienesActivos("0");
    			command.setBanListaBienesConsumibles("0");
    			command.setListaCodTipoServicio(new ArrayList());
    	}
    	log.info("CodSede: "+command.getCodSede());
    	
    	List lista1= new ArrayList();
    	List lista2= new ArrayList();
    	List lista3= new ArrayList();
    	List lista4= new ArrayList();
    	List lista5= new ArrayList();
    	List lista6= new ArrayList();
    	
    	lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista1!=null)
    	command.setListCodTipoRequerimiento(lista1);
    	lista2=this.seguridadManager.GetAllCentrosCosto(command.getCodSede(),"","");
    	if(lista2!=null)
    	command.setListaCodCentroCosto(lista2);
    	
    	Buscar_Lista_Familia(command);
    	
    	lista5=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_GRUPO_SERVICIO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista5!=null)
    	command.setListaCodGrupoServicio(lista5);
    	    	
    	log.info("formBackingObject:FIN");
        return command;
     }
 	
 	protected void initBinder(HttpServletRequest request,
             ServletRequestDataBinder binder) {
     	NumberFormat nf = NumberFormat.getNumberInstance();
     	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
     	
     }
     /**
      * Redirect to the successView when the cancel button has been pressed.
      */
     public ModelAndView processFormSubmission(HttpServletRequest request,
                                               HttpServletResponse response,
                                               Object command,
                                               BindException errors)
     throws Exception {
     	return super.processFormSubmission(request, response, command, errors);
     }
 	
     public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
             BindException errors)
     		throws Exception {
     	log.info("onSubmit:INI");
     	
     	PenBandejaAtencionCotizacionCommand control= (PenBandejaAtencionCotizacionCommand) command;
     	control.setBanListaServicios("");
     	control.setBanListaBienesActivos("");
        control.setBanListaBienesConsumibles("");
     	control.setListaCodCentroCosto(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),"",""));
     	
     	String resultado="";
     	log.info("OnSubmit CodFamilia: "+control.getCodFamilia());
     	if(control.getOperacion().equals("BUSCAR_BIENES")){
     		Buscar_Lista_Familia(control);
     		Buscar_Bienes(control, request);
     	}
     	else{ if(control.getOperacion().equals("BUSCAR_SERVICIO")){
     		   Buscar_Lista_Familia(control);
     		   Buscar_Servicio(control, request);
     		}
     		else{ if(control.getOperacion().equals("FAMILIA")){
     			Buscar_Lista_Familia(control);
     			Buscar_Familia(control, request);
     			}
     			else{ if(control.getOperacion().equals("GRUPOSERVICIO")){
     				  Buscar_GrupoServicio(control, request);
     			      }
     				  else{ if(control.getOperacion().equals("AGREGAR_A_COTIZACION")){
     		                   resultado= AgregarACotizacion(control, request);
     		                  if ( resultado.equals("-1")) control.setMsg("ERROR");
     		         		  else{ if(resultado.equals("-2")) control.setMsg("DUPLICADO");
     		         		  	else{ if(resultado.equals("-3")) control.setMsg("DIF_TIPO");
     		         		      else
     		         			  control.setMsg("OK");}}
     				  		}
     				  else{ if(control.getOperacion().equals("LIMPIAR"))
     				  		{ control.setListaCodSubFamilia(new ArrayList());
     				  		  control.setListaCodTipoServicio(new ArrayList());
     				  		}
     				  	   else{ if(control.getOperacion().equals("FAMILY"))
     				  	   		  { 
     				  		   		log.info("Inversion:"+ control.getIndInversion());
     				  		         Buscar_Lista_Familia(control);
     				  		         Buscar_Bienes(control, request);

     				  	   		  }
     				  	   	    else if(control.getOperacion().equals("SOLO_COTIZACION"))
   				  	   		      { 
    				  		         Buscar_Lista_Familia(control);
    				  		         Buscar_GrupoServicio(control, request);
    				  		         ComboTipoServicioConCotizacion(control);
    				  	   		  }
     				  	   	    	else if(control.getOperacion().equals("LLENAR_COMBO"))
     				  	   		      	{control.setValSelec("0");
     				  	   	    	     Buscar_GrupoServicio(control, request);
     				  	   	    	     
     				  	   	    	    }else if (control.getOperacion().equals("ELIMINAR_DE_BANDEJA")){
     				  	   	    	    	log.info("Inicio eliminar elementos de bandeja");
     				  	   	    	    	resultado= EliminarDeBandeja(control, request);     				  	   	    	    	
     				  	   	    	    	log.info("Resultado:" + resultado);
     				  	   	    	    	
     				  	   	    	    	Buscar_Lista_Familia(control);
     				  	   	    	    	Buscar_Bienes(control, request);
     				  	     		
     				  	   	    	    	if ( resultado.equals("-1")) control.setMsg("ERROR");
     				  	   	    	    	else control.setMsg("OK");
     				  	   	    	    }
     				  	   }
     				  }
     					  
     				  }
     			   }
     		}
     		
     	}
     	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/CotBienesYServicios/Pen_Bandeja_Atencion_Cotizacion","control",control);
    }

     public void Buscar_Bienes(PenBandejaAtencionCotizacionCommand control, HttpServletRequest request){
    	 
    	log.info("Buscar_Bienes :D "+control.getCodTipoRequerimiento());
    	GuardarDatosBandeja(control, request);
   	 	//ALQD,05/08/09.LLAMANDO A METODO QUE DEVUELVE TIPOS DE PEDIDOS POR ATENDER
    	ResumenPendientePorCotizar resumenPorcotizar = new ResumenPendientePorCotizar();
    	resumenPorcotizar = (ResumenPendientePorCotizar)this.cotBienesYServiciosManager.GetResumenPendientePorCotizar(control.getCodSede()).get(0);
     	if(resumenPorcotizar!=null){
	    	resumenPorcotizar = (ResumenPendientePorCotizar)this.cotBienesYServiciosManager.GetResumenPendientePorCotizar(control.getCodSede()).get(0);
	    	control.setCantPendActivo(resumenPorcotizar.getCantPendActivo());
	    	control.setCantPendConsInversion(resumenPorcotizar.getCantPendConsInversion());
	    	control.setCantPendConsNormal(resumenPorcotizar.getCantPendConsNormal());
	    	control.setCantPendServGeneral(resumenPorcotizar.getCantPendServGeneral());
	    	control.setCantPendServMantto(resumenPorcotizar.getCantPendServMantto());
	    	control.setCantPendServOtro(resumenPorcotizar.getCantPendServOtro());
     	}
     	else{
	    	control.setCantPendActivo("N/A");
	    	control.setCantPendConsInversion("N/A");
	    	control.setCantPendConsNormal("N/A");
	    	control.setCantPendServGeneral("N/A");
	    	control.setCantPendServMantto("N/A");
	    	control.setCantPendServOtro("N/A");
     	}
    	List listaBienes= new ArrayList(); 
    	String radio="";
    	 if(control.getConsteRadio().equals(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE))
    	 {
    	   radio=CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE;
    	   control.setListaBienesConsumibles(listaBienes);
    	   request.setAttribute("TIPO", "BIEN_CONSUMIBLE");
    	 }
    	 else{ if(control.getConsteRadio().equals(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO))
    	       control.setListaBienesActivos(listaBienes);
    	       radio=CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO;
    	       request.setAttribute("TIPO", "BIEN_ACTIVO");
    		 }
    	if(control.getCodSubFamilia().equals("-1")) control.setCodSubFamilia("");
    	if(control.getCodFamilia().equals("-1")) control.setCodFamilia("");
    	if(control.getCodGrupoServicio().equals("-1")) control.setCodGrupoServicio("");
    	if(control.getCodCentroCosto().equals("-1")) control.setCodCentroCosto("");
    	
  	    listaBienes= this.cotBienesYServiciosManager.GetAllCotBienesYServicios(
  	    		control.getCodTipoRequerimiento(), radio, control.getTxtNroRequerimiento()
  	    		, control.getCodFamilia(), control.getCodSubFamilia()
  	    		, control.getCodGrupoServicio(), control.getCodSede()
  	    		, control.getCodCentroCosto(), control.getTxtUsuSolicitante()
  	    		, control.getCodUsuario(), control.getCodPerfil()
  	    		, control.getIndInversion());
  	    
  	    if(listaBienes!=null)
    	{  if(radio.equals(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO))
    	   {  control.setListaBienesActivos(listaBienes);
    	      control.setListaBienesConsumibles(new ArrayList());
    	      if(listaBienes.size()==0)
    	          control.setBanListaBienesActivos("0");
    	   }
    	   else if(radio.equals(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE))
    		     {control.setListaBienesConsumibles(listaBienes);
    		      control.setListaBienesActivos(new ArrayList());
    		      if(listaBienes.size()==0)
    		    	  control.setBanListaBienesConsumibles("0");
    		     }
    	}
  	    else{ control.setListaBienesActivos(new ArrayList());
  	          control.setListaBienesConsumibles(new ArrayList());
  	          control.setBanListaBienesActivos("0");
  	          control.setBanListaBienesConsumibles("0");
  	    }
    
  	    control.setListaServicios(new ArrayList());
     	
    	 control.setOperacion("");
    	
    	 if(!control.getCodFamilia().equals(""))
    	 { ListarComboSubFamilia(control);
    	  
    	 }
    	 else control.setListaCodSubFamilia(new ArrayList());
    	 
    	 log.info("Lista Bienes Activos size: "+control.getListaBienesActivos().size());
    	 log.info("Lista Bienes Consumibles size: "+control.getListaBienesConsumibles().size());
     }
     
     public void Buscar_Servicio(PenBandejaAtencionCotizacionCommand control, HttpServletRequest request){
    	     	 
    	System.out.println("Buscar_Servicio :D");
    	GuardarDatosBandeja(control, request);
    	//ALQD,05/08/09.LLAMANDO A METODO QUE DEVUELVE TIPOS DE PEDIDOS POR ATENDER
     	ResumenPendientePorCotizar resumenPorcotizar = new ResumenPendientePorCotizar();
     	if(resumenPorcotizar!=null){
	    	resumenPorcotizar = (ResumenPendientePorCotizar)this.cotBienesYServiciosManager.GetResumenPendientePorCotizar(control.getCodSede()).get(0);
	    	control.setCantPendActivo(resumenPorcotizar.getCantPendActivo());
	    	control.setCantPendConsInversion(resumenPorcotizar.getCantPendConsInversion());
	    	control.setCantPendConsNormal(resumenPorcotizar.getCantPendConsNormal());
	    	control.setCantPendServGeneral(resumenPorcotizar.getCantPendServGeneral());
	    	control.setCantPendServMantto(resumenPorcotizar.getCantPendServMantto());
	    	control.setCantPendServOtro(resumenPorcotizar.getCantPendServOtro());
     	}
     	else{
	    	control.setCantPendActivo("N/A");
	    	control.setCantPendConsInversion("N/A");
	    	control.setCantPendConsNormal("N/A");
	    	control.setCantPendServGeneral("N/A");
	    	control.setCantPendServMantto("N/A");
	    	control.setCantPendServOtro("N/A");
     	}
    	control.setListaCodTipoServicio(this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
     			, control.getCodGrupoServicio(), "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC));
    	
    	if(control.getCodSubFamilia().equals("-1")) control.setCodSubFamilia("");
     	if(control.getCodFamilia().equals("-1")) control.setCodFamilia("");
     	if(control.getCodGrupoServicio().equals("-1")) control.setCodGrupoServicio("");
     	if(control.getCodCentroCosto().equals("-1")) control.setCodCentroCosto("");
     	if(control.getCodTipoServicio().equals("-1")) control.setCodTipoServicio("");
    	List listaServicio= new ArrayList(); 
    	listaServicio= this.cotBienesYServiciosManager.GetAllCotBienesYServicios(control.getCodTipoRequerimiento(),
   			control.getCodTipoServicio(), control.getTxtNroRequerimiento(), control.getCodFamilia(), 
   			control.getCodSubFamilia(), control.getCodGrupoServicio(), control.getCodSede(), 
   			control.getCodCentroCosto(), control.getTxtUsuSolicitante(), control.getCodUsuario(),
   			control.getCodPerfil(),control.getIndInversion());
   	    
   	    control.setOperacion("");
   	 System.out.println("Buscar_Servicio :D"+control.getCodGrupoServicio());
   	 
   	 if(!control.getCodGrupoServicio().equals(""))
    	   ListarComboTipoServicio(control);
  	 else control.setListaCodTipoServicio(new ArrayList());
   	
   	 if(listaServicio!=null)
   	   { control.setListaServicios(listaServicio);
   	     if(listaServicio.size()==0)
   	    	 control.setBanListaServicios("0");
   	   }
   	 else{ control.setBanListaServicios("0");
   	 	   control.setListaServicios(new ArrayList());
   	 }
   	
   	control.setListaBienesActivos(new ArrayList());
   	control.setListaBienesConsumibles(new ArrayList());
   	request.setAttribute("TIPO", "SERVICIO");
    System.out.println("Lista Servicios size: "+control.getListaServicios().size());
     }
     public void Buscar_Familia(PenBandejaAtencionCotizacionCommand control, HttpServletRequest request){
    	 
    	 System.out.println("Buscar_Familia :D");
    	 control.setOperacion("");
    	 if(!control.getCodFamilia().equals("-1"))
      	   ListarComboSubFamilia(control);
    	 else control.setListaCodSubFamilia(new ArrayList());
     }
     public void Buscar_GrupoServicio(PenBandejaAtencionCotizacionCommand control, HttpServletRequest request){
    	 
    	 System.out.println("Buscar_GrupoServicio :D");
    	
    	 control.setOperacion("");
    	 if(!control.getCodGrupoServicio().equals("-1"))
      	     ListarComboTipoServicio(control);
    	 else control.setListaCodTipoServicio(new ArrayList());
    	 
     }
     
     public String EliminarDeBandeja(PenBandejaAtencionCotizacionCommand control, HttpServletRequest request){
    	 log.info("EliminarDeBandeja");
    	 log.info("CadCodigos: " + control.getCadCodigos());
    	 log.info("NroCodigos: " + control.getNroCodigos());
    	 
    	 StringTokenizer aa= new StringTokenizer(control.getCadCodigos(), "&");
 	     String lia="";
 	     ArrayList<String> molde= new ArrayList<String>();
 	     int contador=0;
 	     while(aa.hasMoreTokens())
 	     {	lia = aa.nextToken();
 	        molde.add(lia);
 	     }  
 	    log.info("Molde Size: "+molde.size());
 	   String resultado="";
 	   String iResultado="";
 	     for(int i=0;i<molde.size();i++)
 	     {  
	 	      StringTokenizer aa2= new StringTokenizer(molde.get(i), "$");
		      String lia2="";
		      ArrayList<String> molde2= new ArrayList<String>();
		      while(aa2.hasMoreTokens())
		      {	lia2 = aa2.nextToken();
		        molde2.add(lia2);
		      }
 	     	      
	 	    log.info(molde.get(i));
	 	    log.info(molde2.size());
	 	    
	 	    /*****************************************************************/
	 	    resultado = this.cotBienesYServiciosManager.DeleteItemBandejaProducto(control.getCodUsuario(), molde.get(i), String.valueOf(molde2.size()));
		    /*****************************************************************/
	 	      if(resultado.trim().equals("0"))
		    	  contador=contador+1;
		      else if (!iResultado.trim().equals("-3")) 
		    	  iResultado=resultado;
		      
		      resultado="";
 	     }
 	     
 	     log.info("Proceso finalizado");
 	     
 	    if(contador==molde.size())	 
 	    	resultado="0";
 	     else resultado=iResultado;
    	 System.out.println("AgregarACotizacion: "+resultado);
    	 request.setAttribute("ESTADO", "OK1");
    	 if(!control.getCodFamilia().equals("-1"))
    	   ListarComboSubFamilia(control);
    	 else control.setListaCodSubFamilia(new ArrayList());
    	 if(!control.getCodGrupoServicio().equals("-1"))
        	   ListarComboTipoServicio(control);
      	 else control.setListaCodTipoServicio(new ArrayList());
    	     	 
    	 return resultado;
 	     
    	
     }
     
     public String AgregarACotizacion(PenBandejaAtencionCotizacionCommand control, HttpServletRequest request){
    	 
    	 String resultado="";
    	 //ALQD,12/02/09.AGREGANDO VARIABLE PARA ALMACENAR ERROR
    	 String iResultado="";
    	 System.out.println("CadCodigos: "+control.getCadCodigos()+">>NroCodigos: <<"+ control.getNroCodigos());
    	 System.out.println("Cadena a Guardar: "+control.getCadCodigos().trim());
 	     StringTokenizer aa= new StringTokenizer(control.getCadCodigos(), "&");
 	     String lia="";
 	     ArrayList<String> molde= new ArrayList<String>();
 	     int contador=0;
 	     while(aa.hasMoreTokens())
 	     {	lia = aa.nextToken();
 	        molde.add(lia);
 	     }  
 	     System.out.println("Molde Size: "+molde.size());
 	     for(int i=0;i<molde.size();i++)
 	     {  
 	      StringTokenizer aa2= new StringTokenizer(molde.get(i), "$");
	      String lia2="";
	      ArrayList<String> molde2= new ArrayList<String>();
	      while(aa2.hasMoreTokens())
	      {	lia2 = aa2.nextToken();
	        molde2.add(lia2);
	      }
	      /*****************************************************************/
	      resultado=this.cotBienesYServiciosManager.InsertAgregarACotizacion(control.getCodUsuario(), 
	    		  molde.get(i), String.valueOf(molde2.size()));
	      /*****************************************************************/ 
	      if(resultado.trim().equals("0"))
	    	  contador=contador+1;
	      else {
	    	  if (!iResultado.trim().equals("-3")) iResultado=resultado;
	      	}
	      resultado="";
	     }
 	     if(contador==molde.size())	 
 	    	resultado="0";
 	     else resultado=iResultado;
    	 System.out.println("AgregarACotizacion: "+resultado);
    	 request.setAttribute("ESTADO", "OK");
    	 if(!control.getCodFamilia().equals("-1"))
    	   ListarComboSubFamilia(control);
    	 else control.setListaCodSubFamilia(new ArrayList());
    	 if(!control.getCodGrupoServicio().equals("-1"))
        	   ListarComboTipoServicio(control);
      	 else control.setListaCodTipoServicio(new ArrayList());
    	 return resultado;
     }

     public void ListarComboSubFamilia(PenBandejaAtencionCotizacionCommand control){
    	 
    	List lista4=new ArrayList();
    	System.out.println(">>ConsteRadio<<"+control.getConsteRadio()
     			+">>CodFamilia<<"+control.getCodFamilia()+">>Constante<<"+CommonConstants.TIPO_ORDEN_DSC);
     	lista4=this.detalleManager.GetAllSubFamilias(control.getConsteRadio()
    			, control.getCodFamilia(), CommonConstants.TIPO_ORDEN_DSC,"","","");
     	if(lista4!=null)
     	control.setListaCodSubFamilia(lista4);
     	System.out.println("Size SubFamilia: "+lista4.size());
     }
     
    public void ListarComboTipoServicio(PenBandejaAtencionCotizacionCommand control){
    	 System.out.println("ListarComboTipoServicio :D");
    	 List lista6=new ArrayList();
    	 System.out.println("CodGrupoServicio: "+control.getCodGrupoServicio());
    	 TipoLogistica obj = new TipoLogistica();
    	 obj.setCodPadre(CommonConstants.TIPT_GRUPO_SERVICIO);
 		 obj.setCodId(control.getCodGrupoServicio());
 		 obj.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
 		 obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	 lista6=this.detalleManager.GetAllTablaDetalle(obj);
     	if(lista6!=null)
     	control.setListaCodTipoServicio(lista6);
    	System.out.println("ListaCodTipoServicio Size: "+control.getListaCodTipoServicio().size());
     }
     
    public void Buscar_Lista_Familia(PenBandejaAtencionCotizacionCommand control){
    	List lista3= new ArrayList();
    	System.out.println("ConsteRadio: "+control.getConsteRadio());
    	/*lista3=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_FAMILIA 
    			, control.getConsteRadio(), "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista3!=null)
    	control.setListaCodFamilia(lista3);*/
    	
    	TipoLogistica obj = new TipoLogistica();
    	obj.setCodPadre(CommonConstants.TIPT_TIPO_BIEN);
		obj.setCodTabla(CommonConstants.TIPT_FAMILIA);
		obj.setCodId(control.getConsteRadio());
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	lista3=this.detalleManager.GetAllTablaDetalle(obj);
    	if(lista3!=null)
    	control.setListaCodFamilia(lista3);
    	else control.setListaCodFamilia(new ArrayList());
    }
    
    public void GuardarDatosBandeja(PenBandejaAtencionCotizacionCommand control,HttpServletRequest request)
	  {
	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();
	 	
	 	 bandejaBean.setCodBandeja("3");
	 		 	 
	 	 bandejaBean.setValor1(control.getCodTipoRequerimiento());
	 	 bandejaBean.setValor3(control.getTxtNroRequerimiento());
	 	 bandejaBean.setValor4(control.getCodFamilia());
	 	 bandejaBean.setValor5(control.getCodSubFamilia());
	 	 bandejaBean.setValor6(control.getCodGrupoServicio());
	 	 bandejaBean.setValor7(control.getCodCentroCosto());
	 	 bandejaBean.setValor8(control.getTxtUsuSolicitante());
	 	if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_BIEN))
	 	   		 	 bandejaBean.setValor2(control.getConsteRadio());
		else{  if(control.getCodTipoRequerimiento().equals(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO))
	 	 		  	bandejaBean.setValor2(control.getCodTipoServicio());
		
	 	 }
	 		 		 	 
	 	 log.info("GuardarDatosBandeja");
	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	  }
   
    public void ComboTipoServicioConCotizacion(PenBandejaAtencionCotizacionCommand control){
    	List lista= new ArrayList();
    	String codCotizacion="";
    	System.out.println(">>getCodSede<<"+control.getCodSede()+">>getCodUsuario<<"+ 
    			control.getCodUsuario()+">>getCodOpciones<<"+
    			control.getCodOpciones()+">>getCodGrupoServicio<<"+control.getCodGrupoServicio());
    	try{
    	codCotizacion=cotBienesYServiciosManager.GetTipoServicioConCotizacion(control.getCodSede(), control.getCodUsuario(),
    			control.getCodOpciones(), control.getCodGrupoServicio());
    	}catch (Exception e) {
			
		}
    	System.out.println("codCotizacion Antes: "+codCotizacion);
    	if(codCotizacion==null) codCotizacion="";
    	else{
    		try{
	    	StringTokenizer aa= new StringTokenizer(codCotizacion, "|");
		    String lia="";
		    ArrayList<String> molde= new ArrayList<String>();
		    int contador=0;
		    while(aa.hasMoreTokens())
		    {  lia = aa.nextToken();
		       molde.add(lia);
		    }
		    System.out.println("Tama�o: "+molde.size());
		    List listaMad= new ArrayList();
		    for(int i=0;i<control.getListaCodTipoServicio().size();i++)
		    {  Logistica curso = new Logistica();
		       curso=(Logistica)control.getListaCodTipoServicio().get(i);
		       for(int j=0;j<molde.size();j++)
		       { 
		    	 if(curso.getCodSecuencial().equals(molde.get(j)))
		    		listaMad.add(curso);
		       }
		    }
		    
		    for(int k=0;k<control.getListaCodTipoServicio().size();k++)
		    {   Logistica curso1 = new Logistica();
		    	curso1=(Logistica)control.getListaCodTipoServicio().get(k);
		    	System.out.println("CodSecuencial1: "+curso1.getCodSecuencial());
		    }
		    for(int k=0;k<listaMad.size();k++)
		    {   Logistica curso2 = new Logistica();
		        curso2=(Logistica)control.getListaCodTipoServicio().get(k);
		    	System.out.println("CodSecuencial2: "+curso2.getCodSecuencial());
		    }
		    control.setListaCodTipoServicio(listaMad);
    	}catch (Exception e) {
			
		}
    	}
    	System.out.println("codCotizacion Despues: "+codCotizacion);
    }
    
	public TablaDetalleManager getTablaDetalleManager() {
		return tablaDetalleManager;
	}

	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public SeguridadManager getSeguridadManager() {
		return seguridadManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	public CotBienesYServiciosManager getCotBienesYServiciosManager() {
		return cotBienesYServiciosManager;
	}

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}
	
}
