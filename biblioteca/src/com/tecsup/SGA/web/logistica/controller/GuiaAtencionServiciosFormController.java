package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.logistica.command.GuiaAtencionServiciosCommand;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;

public class GuiaAtencionServiciosFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(GuiaAtencionServiciosFormController.class);	
	private AtencionRequerimientosManager atencionRequerimientosManager;

	public AtencionRequerimientosManager getAtencionRequerimientosManager() {
		return atencionRequerimientosManager;
	}

	public void setAtencionRequerimientosManager(
			AtencionRequerimientosManager atencionRequerimientosManager) {
		this.atencionRequerimientosManager = atencionRequerimientosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	GuiaAtencionServiciosCommand command = new GuiaAtencionServiciosCommand();
    	//*********************************************************
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));    	
    	System.out.println("codRequerimiento="+command.getCodRequerimiento()+"<<");
    	System.out.println("codUsuario="+command.getCodUsuario()+"<<");    	
    	//*********************************************************
    	if(command.getCodRequerimiento()!=null){
	    	if(!command.getCodRequerimiento().equals("")){    		
	    		CargaDatos(command);
	    	}   	
    	}
        log.info("formBackingObject:FIN");
        return command;
    }
	
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        //this.onSubmit(request, response, command, errors);
    	return super.processFormSubmission(request, response, command, errors);
    }
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	GuiaAtencionServiciosCommand control = (GuiaAtencionServiciosCommand)command;
    	String resultado="";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = GrabarDetalleGuia(control);
    		CargaDatos(control);
    		
    		System.out.println("resultado GRABAR-->"+resultado);    		
    		if (resultado.equals("0")){    		
    			control.setMsg("OK_GRABAR");
    		}
    		else{
    			if(resultado.equals("-2")){
    				control.setMsg("EXISTE");
    			}
    			else{
    				control.setMsg("ERROR_GRABAR");
    			}   			
    		}    		
    	}    	
    	
		log.info("onSubmit:FIN");		
	    return new ModelAndView("/logistica/AtencionRequerimientos/Guia_Atencion_Servicios","control",control);
    }    
    private void CargaDatos(GuiaAtencionServiciosCommand control){
    	List lista=this.atencionRequerimientosManager.GetGuiaMantenimiento(control.getCodRequerimiento());
    	
    	if(lista!=null && lista.size()>0){
    		GuiaDetalle guia = (GuiaDetalle)lista.get(0);
    		
    		control.setCodRequerimiento(guia.getCodRequerimiento());
    		control.setCodReqDetalle(guia.getCodReqDetalle());
    		control.setCodGuia(guia.getCodGuia());
    		control.setCodGuiaDetalle(guia.getCodGuiaDetalle());
    		control.setNroGuia(guia.getNroGuia());
    		control.setFechaGuia(guia.getFechaGuia());
    		control.setCodEstado(guia.getCodEstado());
    		control.setEstado(guia.getDscEstado());
    		control.setDscGuiaDetalle(guia.getDscGuiaDetalle());
    		control.setCodFactura(guia.getCodDocPago());
    		control.setNroFactura(guia.getNroDocPago());
    		control.setFechaFactura(guia.getFechaDocPago());
    		control.setCodOrden(guia.getCodOrden());
    		control.setFechaOrden(guia.getFechaOrden());
    		control.setNroOS(guia.getNroOrden());
    	}    	
    }
    private String GrabarDetalleGuia(GuiaAtencionServiciosCommand control){    	
    	try{    		
    		GuiaDetalle obj = new GuiaDetalle();
    		
    		obj.setCodRequerimiento(control.getCodRequerimiento());
    		obj.setCodReqDetalle(control.getCodReqDetalle());
    		obj.setCodGuia(control.getCodGuia());
    		obj.setCodGuiaDetalle(control.getCodGuiaDetalle());
    		obj.setNroGuia(control.getNroGuia());
    		obj.setFechaGuia(control.getFechaGuia());
    		obj.setObservacion(control.getDscGuiaDetalle());
    		
    		String resultado="";
    		resultado=this.atencionRequerimientosManager.InsertGuiaMantenimiento(obj, control.getCodUsuario());    		
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }    
}