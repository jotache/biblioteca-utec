package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Adjunto;
import com.tecsup.SGA.modelo.TipoLogistica;  
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoRolesFlujoCommand;

public class MantenimientoRolesFlujoFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoRolesFlujoFormController.class);
	DetalleManager detalleManager;
	TablaDetalleManager tablaDetalleManager;
    
    public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public MantenimientoRolesFlujoFormController() {    	
        super();        
        setCommandClass(MantenimientoRolesFlujoCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoRolesFlujoCommand command = new MantenimientoRolesFlujoCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	TipoLogistica obj = new TipoLogistica();
    	
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_FLUJO);
    	obj.setTipo(CommonConstants.TIPT_ETAPA_CURSOS_CORTOS);
    	
    	
    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
          	command.setCodTipo(CommonConstants.COD_SEC_NOV);	
          	command.setCodSede(CommonConstants.SEDE_LIMA);
          	 buscar(command);
          	}
          	}
    	
    	command.setListTipo(this.detalleManager.GetAllTablaDetalle(obj));
    	
    	obj.setCodTabla(CommonConstants.TIPT_SEDE);
    	obj.setTipo(CommonConstants.UPDATE_MTO_BIB);
    	command.setListSede(this.detalleManager.GetAllTablaDetalle(obj));
    	
    	request.getSession().setAttribute("listTipo", command.getListTipo());
    	request.getSession().setAttribute("listSede", command.getListSede());
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	MantenimientoRolesFlujoCommand control = (MantenimientoRolesFlujoCommand) command;
    	if (control.getOperacion().trim().equals("GRABAR"))
    	{
    		resultado = grabar(control);
    		buscar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else control.setMsg("OK");
    		
    		System.out.println("mensaje: "+control.getMsg());
    	}
    	else if (control.getOperacion().trim().equals("BUSCAR"))
    	{
    		buscar(control);
    	}
    	
    	log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Roles_Flujo","control",control);		
    }
        
        private String grabar(MantenimientoRolesFlujoCommand control){
        	//Nivel Requerimiento
        	if(control.getCodTipo().equals(CommonConstants.COD_SEC_NOV)){
        		int codigo = 0000;
            	String cod = "000";
            	String codAumen = "00";
            	String val = "";
        		for(int i =2;i<=52;i++){
        			cod = cod + i;
        			if(i==2){
        				val = control.getItem1();	
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==3){
        				val = control.getItem2();
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==4){
        				val = control.getItem3();
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==5){
        				val = control.getItem4();
        				if(val ==null){
        					val = "0";
        				}
        			}
        			
        			else if(i==49){
        				codAumen = codAumen + i;
        				val = control.getItem41();
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==50){
        				codAumen = codAumen + i;
        				val = control.getItem42();
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==51){
        				codAumen = codAumen + i;
        				val = control.getItem43();
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==52){
        				codAumen = codAumen + i;
        				val = control.getItem44();
        				if(val ==null){
        					val = "0";
        				}
        			}
        			if(i>=41){
        				control.setGrabar(this.detalleManager.InsertFlujo(CommonConstants.TIPT_ESTD_ROLES_fLUJO
              					, codAumen, val, control.getCodAlumno(),control.getCodSede()));
               		}
        			else{
        			control.setGrabar(this.detalleManager.InsertFlujo(CommonConstants.TIPT_ESTD_ROLES_fLUJO
      					, cod, val, control.getCodAlumno(),control.getCodSede()));
        			}
        			cod = "000";
        			codAumen = "00";
        		}
        		 control.setItem1("");
    			 control.setItem2("");
    			 control.setItem3("");
    			 control.setItem4("");
    			 control.setItem41("");
    			 control.setItem42("");
    			 control.setItem43("");
    			 control.setItem44("");
        		
        	}
        	//Nivel Orden de Compra
        	else if(control.getCodTipo().equals(CommonConstants.COD_SEC_ENL_INT)){
        		String cod = "000";
            	String val = "";
            	String codAumen = "00";
            	//ALQD,09/10/08. A�ADIENDO DOS ROLES DE FLUJO DE APROB.
            	//ALQD,30/01/09. A�ADIENDO UN ROL DE FLUJO DE APROB.
        		for(int i=1;i<=59;i++){
        			if(i==1){
        				val = control.getItem111();
        				cod= "0010";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==2){
        				val = control.getItem112();
        				cod= "0011";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==3){
        				val = control.getItem113();
        				cod= "0012";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==4){
        				val = control.getItem114();
        				cod= "0013";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==5){
        				val = control.getItem5();
        				cod= "0014";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==6){
        				val = control.getItem6();
        				cod= "0015";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==7){
        				val = control.getItem7();
        				cod= "0016";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==8){
        				val = control.getItem8();
        				cod= "0017";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==9){
        				val = control.getItem9();
        				cod= "0018";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==10){
        				val = control.getItem10();
        				cod= "0019";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==11){              
        				val = control.getItem11();
        				cod= "0020";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==12){
        				val = control.getItem12();
        				cod= "0023";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==13){
        				val = control.getItem13();
        				cod= "0024";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==14){
        				val = control.getItem14();
        				cod= "0025";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==53){
        				codAumen = codAumen + i;
        				val = control.getItem81();
        				cod= "0053";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==54){
        				codAumen = codAumen + i;
        				val = control.getItem82();
        				cod= "0054";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==55){
        				codAumen = codAumen + i;
        				val = control.getItem83();
        				cod= "0055";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==56){
        				codAumen = codAumen + i;
        				val = control.getItem84();
        				cod= "0056";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			//ALQD,09/10/08. A�ADIENDO LAS PROPIEDADES
            		else if(i==57){
            			codAumen = codAumen + i;
            			val = control.getItem85();
            			cod= "0057";
            			if(val ==null){
            				val = "0";
            			}
            		}
                	else if(i==58){
                		codAumen = codAumen + i;
                		val = control.getItem86();
                		cod= "0058";
                		if(val ==null){
                			val = "0";
                		}
        			}
        			//ALQD,30/01/09. A�ADIENDO LA PROPIEDAD
            		else if(i==59){
            			codAumen = codAumen + i;
            			val = control.getItem87();
            			cod= "0059";
            			if(val ==null){
            				val = "0";
            			}
            		}
        			if(i>=51){
        				control.setGrabar(this.detalleManager.InsertFlujo(CommonConstants.TIPT_ESTD_ROLES_fLUJO
              					, cod, val, control.getCodAlumno(),control.getCodSede()));
               		}
        			else{
        			control.setGrabar(this.detalleManager.InsertFlujo(CommonConstants.TIPT_ESTD_ROLES_fLUJO
        			, cod, val, control.getCodAlumno(),control.getCodSede()));
        				}
        			if(i<9){
        				cod = "000";
        			}
        			else{
        				cod = "00";
        			}
        		}
        		 control.setItem1("");
    			 control.setItem2("");
    			 control.setItem3("");
    			 control.setItem4("");
    			 control.setItem5("");
    			 control.setItem6("");
    			 control.setItem7("");
    			 control.setItem8("");
    			 control.setItem9("");
    			 control.setItem10("");
    			 control.setItem11("");
    			 control.setItem12("");
    			 control.setItem13("");
    			 control.setItem14("");
    			 control.setItem81("");
    			 control.setItem82("");
    			 control.setItem83("");
    			 control.setItem84("");
    			 control.setItem85("");
    			 control.setItem86("");
    			 control.setItem87("");
        	}
        	//Nivel Documento de Pago
        	else if(control.getCodTipo().equals(CommonConstants.COD_SEC_BIB)){
        		int codigo = 0000;
            	String cod = "000";
            	String val = "";
            	for(int i = 1;i<=23;i++){  
            		if(i==1){
        				val = control.getItem121();	
        				cod= "0026";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==2){
        				val = control.getItem122();
        				cod= "0027";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==3){
        				val = control.getItem123();
        				cod= "0028";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==4){
        				val = control.getItem124();
        				cod= "0029";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==5){
        				val = control.getItem125();
        				cod= "0030";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==6){
        				val = control.getItem126();
        				cod= "0031";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==7){
        				val = control.getItem127();
        				cod= "0032";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==8){
        				val = control.getItem128();
        				cod= "0033";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==9){
        				val = control.getItem129();
        				cod= "0034";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==10){
        				val = control.getItem1210();
        				cod= "0035";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==11){               
        				val = control.getItem1211();
        				cod= "0036";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==12){
        				val = control.getItem1212();
        				cod= "0037";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==13){
        				val = control.getItem12113();
        				cod= "0038";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==14){
        				val = control.getItem12114();
        				cod= "0039";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==15){
        				val = control.getItem15();
        				cod= "0040";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==16){
        				val = control.getItem16();
        				cod= "0041";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if (i==17){
        				val = control.getItem17();
        				cod= "0042";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==18){
        				val = control.getItem18();
        				cod= "0043";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==19){
        				val = control.getItem19();
        				cod= "0044";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==20){
        				val = control.getItem20();
        				cod= "0045";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if (i==21){
        				val = control.getItem21();
        				cod= "0046";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==22){
        				val = control.getItem22();
        				cod= "0047";
        				if(val ==null){
        					val = "0";
        				}
        			}
        			else if(i==23){
        				val = control.getItem23();
        				cod= "0048";
        				if(val ==null){
        					val = "0";
        				}
        			}
            		
        		control.setGrabar(this.detalleManager.InsertFlujo(CommonConstants.TIPT_ESTD_ROLES_fLUJO
    					, cod, val, control.getCodAlumno(),control.getCodSede()));
            		if(i<9){
        				cod = "000";
        			}
        			else{
        				cod = "00";
        			}
        		
            	}
            	 control.setItem2("");
       			 control.setItem3("");
       			 control.setItem4("");
       			 control.setItem5("");
       			 control.setItem6("");
       			 control.setItem7("");
       			 control.setItem8("");
       			 control.setItem9("");
       			 control.setItem10("");
       			 control.setItem11("");
       			 control.setItem12("");
       			 control.setItem13("");
       			 control.setItem14("");
       			 control.setItem15("");
       			 control.setItem16("");
       			 control.setItem17("");
       			 control.setItem18("");
       			 control.setItem19("");
       			 control.setItem20("");
       			 control.setItem21("");
       			 control.setItem22("");
       			 control.setItem23("");
            	
        	}
        	return control.getGrabar();
        }
        
        private void buscar(MantenimientoRolesFlujoCommand control){
        	control.setListObjetos(this.detalleManager.GetAllFlujosAprobacion(control.getCodSede(), control.getCodTipo()));
        	List lst  = control.getListObjetos();
        	if(lst!=null ){
        		if ( lst.size()>0 )
        		{
        		 TipoTablaDetalle tipoTablaDetalle = null;
        		 if(control.getCodTipo().equals(CommonConstants.COD_SEC_NOV)){
        			 control.setItem1("");
        			 control.setItem2("");
        			 control.setItem3("");
        			 control.setItem4("");
        			 control.setItem41("");
        			 control.setItem42("");
        			 control.setItem43("");
        			 control.setItem44("");
        			
        			 for(int i =1;i<=12;i++){
        				 tipoTablaDetalle = (TipoTablaDetalle)lst.get(i); 
        				 if(i==1){
        					 control.setItem1(tipoTablaDetalle.getDscValor5());
        				 }
        				 else if(i==2){
        					 control.setItem2(tipoTablaDetalle.getDscValor5());
        				 }
        				 else if(i==3){
        					 control.setItem3(tipoTablaDetalle.getDscValor5());
        				 }
        				 else if(i==4){
        					 control.setItem4(tipoTablaDetalle.getDscValor5());
        				 }
        				 else if(i==9){
        					 control.setItem41(tipoTablaDetalle.getDscValor5());
        				 }
        				 else if(i==10){
        					 control.setItem42(tipoTablaDetalle.getDscValor5());
        				 }
        				 else if(i==11){
        					 control.setItem43(tipoTablaDetalle.getDscValor5());
        				 }
        				 else if(i==12){
        					 control.setItem44(tipoTablaDetalle.getDscValor5());
        				 }
        			 }
        	    }
     	        else if(control.getCodTipo().equals(CommonConstants.COD_SEC_ENL_INT)){
     	  		 control.setItem111("");
    			 control.setItem112("");
    			 control.setItem113("");
    			 control.setItem114("");
    			 control.setItem5("");
    			 control.setItem6("");
    			 control.setItem7("");
    			 control.setItem8("");
    			 control.setItem9("");
    			 control.setItem10("");
    			 control.setItem11("");
    			 control.setItem12("");
    			 control.setItem13("");
    			 control.setItem14("");
    			 control.setItem81("");
    			 control.setItem82("");
    			 control.setItem83("");
    			 control.setItem84("");
    			 control.setItem85("");
    			 control.setItem86("");
    			 control.setItem87("");
//ALQD,09/10/08. AUMENTADO EN 2 PARA LEER LOS NUEVOS ROLES
//ALQD,30/01/09. AUMENTADO EN 1 PARA LEER EL NUEVO ROL
    				for(int i=0;i<=22;i++){
    				 tipoTablaDetalle = (TipoTablaDetalle)lst.get(i); 
    				
    				  if(i==0){
    					 control.setItem111(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==1){
    					 control.setItem112(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==2){
    					 control.setItem113(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==3){
    					 control.setItem114(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==4){
    					 control.setItem5(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==5){
    					 control.setItem6(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==6){
    					 control.setItem7(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==7){
    					 control.setItem8(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==8){
    					 control.setItem9(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==9){
    					 control.setItem10(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==10){
    					 control.setItem11(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==13){
    					 control.setItem12(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==14){
    					 control.setItem13(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==15){
    					 control.setItem14(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==16){
    					 control.setItem81(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==17){
    					 control.setItem82(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==18){
    					 control.setItem83(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==19){
    					 control.setItem84(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==20){
    					 control.setItem85(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==21){
    					 control.setItem86(tipoTablaDetalle.getDscValor5());
    				 }
    				 else if(i==22){
    					 control.setItem87(tipoTablaDetalle.getDscValor5());
    				 }
    			 }
     	    	}
     	    	else if(control.getCodTipo().equals(CommonConstants.COD_SEC_BIB)){
     	    	 control.setItem121("");
       			 control.setItem122("");
       			 control.setItem123("");
       			 control.setItem124("");
       			 control.setItem125("");
       			 control.setItem126("");
       			 control.setItem127("");
       			 control.setItem128("");
       			 control.setItem129("");
       			 control.setItem1210("");
       			 control.setItem1211("");
       			 control.setItem1212("");
       			 control.setItem12113("");
       			 control.setItem12114("");
       			 control.setItem15("");
       			 control.setItem16("");
       			 control.setItem17("");
       			 control.setItem18("");
       			 control.setItem19("");
       			 control.setItem20("");
       			 control.setItem21("");
       			 control.setItem22("");
       			 control.setItem23("");
       			
       			for(int i = 0;i<=22;i++){
 				 tipoTablaDetalle = (TipoTablaDetalle)lst.get(i); 
 				 if(i==0){
 					 control.setItem121(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==1){
 					 control.setItem122(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==2){
 					 control.setItem123(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==3){
 					 control.setItem124(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==4){
 					 control.setItem125(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==5){
 					 control.setItem126(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==6){
 					 control.setItem127(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==7){
 					 control.setItem128(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==8){
 					 control.setItem129(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==9){
 					 control.setItem1210(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==10){
 					 control.setItem1211(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==11){
 					 control.setItem1212(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==12){
 					 control.setItem12113(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==13){
 					 control.setItem12114(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==14){
 					control.setItem15(tipoTablaDetalle.getDscValor5()); 
 				 }
 				 else if(i==15){
 					 control.setItem16(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==16){
 					 control.setItem17(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==17){
 					 control.setItem18(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==18){
 					 control.setItem19(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==19){
 					 control.setItem20(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==20){
 					 control.setItem21(tipoTablaDetalle.getDscValor5());
 				 }
 				 else if(i==21){
 					 control.setItem22(tipoTablaDetalle.getDscValor5());
 				 }
 				else if(i==22){
					 control.setItem23(tipoTablaDetalle.getDscValor5());
				 }
 				
 			 }
     	    	}
        	}
        	
        	else{
        		control.setItem1("");
        		control.setItem2("");
        		control.setItem3("");
        		control.setItem4("");
        		control.setItem5("");
        		control.setItem6("");
        		control.setItem7("");
        		control.setItem8("");
        		control.setItem9("");
        		control.setItem10("");
        		control.setItem11("");
        		control.setItem12("");
        		control.setItem13("");
        		control.setItem14("");
        		control.setItem15("");
        		control.setItem16("");
        		control.setItem17("");
        		control.setItem18("");
        		control.setItem19("");
        		control.setItem20("");
        		control.setItem21("");
        		control.setItem22("");
        		control.setItem23("");
        	}
        	}        
        }
        
}
