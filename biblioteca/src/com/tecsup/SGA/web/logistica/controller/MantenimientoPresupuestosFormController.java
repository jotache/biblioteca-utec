package com.tecsup.SGA.web.logistica.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Anios;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.modelo.PresupuestoCentroCosto;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoPresupuestoCommand;

public class MantenimientoPresupuestosFormController extends
		SimpleFormController {

	private static Log log = LogFactory.getLog(MantenimientoPresupuestosFormController.class);
	private DetalleManager detalleManager;

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
	
	public MantenimientoPresupuestosFormController() {
		super(); 
		setCommandClass(MantenimientoPresupuestoCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    	throws ServletException {
		MantenimientoPresupuestoCommand command = new MantenimientoPresupuestoCommand();
		command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
		command.setCodSede((String)request.getParameter("txhSede"));
				
		Anios anios= new Anios();		
    	command.setListaAniosCencos(anios.getAniosTodos(CommonConstants.FECHA_BASE_MINIMO));
    	
		return command;
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		MantenimientoPresupuestoCommand control = (MantenimientoPresupuestoCommand) command;
		log.info("Operacion:"+control.getOperacion());
		List<PresupuestoCentroCosto> lista = new ArrayList<PresupuestoCentroCosto>();
		
		if (control.getOperacion().equals("LISTAR")){
			log.info("Sede seleccionado:"+control.getCodSede());			
			lista = detalleManager.listaPresupuestosCenCos(control.getCodAnioSelec(), control.getCodSede());
			control.setListaPresupuestos(lista);
			control.setTamListPresup(lista.size());
		}else if (control.getOperacion().equals("GRABAR")){

			log.info("cadenaDatos:" + control.getCadenaDatos());
			log.info("cod usuario:" + control.getCodAlumno());
			log.info("Tama�o:" + control.getTamListPresup());
			
			String rpta = detalleManager.InsertPresupuesto(control.getCodSede(), control.getCodAnioSelec(), control.getCadenaDatos(), control.getCodAlumno(), String.valueOf(control.getTamListPresup()));
			if(rpta.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			
			lista = detalleManager.listaPresupuestosCenCos(control.getCodAnioSelec(), control.getCodSede());
			control.setListaPresupuestos(lista);
			control.setTamListPresup(lista.size());
		}
    	return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Presupuestos","control",control);	    	 
	}
}
