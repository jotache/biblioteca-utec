package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.logistica.command.CotDetBandejaProdByItemCommand;

public class CotDetBandejaProdByItemFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotDetBandejaProdByItemFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}

	public CotDetBandejaProdByItemFormController() {    	
        super();        
        setCommandClass(CotDetBandejaProdByItemCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	CotDetBandejaProdByItemCommand command = new CotDetBandejaProdByItemCommand();
    	
    	command.setCodCotizacion(request.getParameter("txhCodCotizacion"));
    	command.setCodCotizacionDet(request.getParameter("txhCodDetCotizacion"));
    	command.setCondicion(request.getParameter("txhCondicion"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario"));
    	command.setCodTipoCotizacion(request.getParameter("txhCodTipoCot"));
    	command.setCodSubTipoCotizacion(request.getParameter("txhCodSubTipoCot"));
    	
    	if ( command.getCodCotizacion() != null  && 
    			command.getCodCotizacionDet() != null && 
    			command.getCodUsuario() != null )
    	{
    		command.setListaProductos(
    					this.cotBienesYServiciosManager.getAllProductosByItem(command.getCodCotizacion()
    							, command.getCodCotizacionDet())
    				);
    	}
    	request.getSession().setAttribute("subTipoCot",command.getCodSubTipoCotizacion());
    	request.getSession().setAttribute("listaProductos", command.getListaProductos());
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	CotDetBandejaProdByItemCommand control = (CotDetBandejaProdByItemCommand) command;
		String rpt = "";
		if (control.getOperacion().trim().equals("ELIMINAR"))
		{
			rpt = this.cotBienesYServiciosManager.deleteProductosByItem(control.getCodReq()
					, control.getCodReqDet(), control.getCodUsuario());
			
			if ( rpt == null ) control.setMsg("ERRORELI"); 
			else if ( rpt.trim().equals("-1")) control.setMsg("ERRORELI");
			else if ( rpt.trim().equals("0")) control.setMsg("OKELI");
		}

		control.setListaProductos(
				this.cotBienesYServiciosManager.getAllProductosByItem(control.getCodCotizacion()
						, control.getCodCotizacionDet())
			);
		
		control.setOperacion("");
		control.setCodReq("");
		control.setCodReqDet("");
		
    	request.getSession().setAttribute("subTipoCot", control.getCodSubTipoCotizacion());
    	request.getSession().setAttribute("listaProductos", control.getListaProductos());
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/CotBienesYServicios/CotDet_BandejaProductoByItem","control",control);		
    }
}
