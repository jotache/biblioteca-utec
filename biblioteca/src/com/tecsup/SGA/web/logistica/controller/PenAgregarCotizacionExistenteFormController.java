package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.CotizacionActualServiciosCommand;
import com.tecsup.SGA.web.logistica.command.PenAgregarCotizacionExistenteCommand;

public class PenAgregarCotizacionExistenteFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(PenAgregarCotizacionExistenteFormController.class);
	
	CotBienesYServiciosManager cotBienesYServiciosManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	
	public DetalleManager getDetalleManager() {
		return detalleManager;
	}

	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
	
    	PenAgregarCotizacionExistenteCommand command= new PenAgregarCotizacionExistenteCommand();
    	
    	command.setCodSede((String)request.getParameter("txhCodSede"));
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	
    	command.setCodTipoReq((String)request.getParameter("txhCodTipoReq"));
    	command.setCodSubTipoReq((String)request.getParameter("txhCodSubTipoReq"));
    	String descripcion=(String)request.getParameter("txhDscTipoReq");
    	String bandera=(String)request.getParameter("txhBandera");
    	
    	command.setCodSeleccionados((String)request.getParameter("txhCodigos"));
    	//ALQD,07/08/09.NUEVOS PARAMETROS
    	command.setIndInversion((String)request.getParameter("txhIndInversion"));
    	
    	if(descripcion!=null)
    		command.setDscTipoReq(descripcion);
    	command.setConsteBienActivo(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
    	command.setConsteBienConsumible(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    	
    	if(bandera!=null)
    	{	command.setBandera(bandera);
    		if(command.getBandera().equals("1"))
	    	   {command.setValRadio(command.getConsteBienConsumible());
    		    command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
    		    }
	    	else{ if(command.getBandera().equals("2"))
	    			{ command.setValRadio(command.getConsteBienActivo());
	    			 command.setConsteRadio(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
	    			 }
	    		else{ if(command.getBandera().equals("3"))
	    				{	command.setValRadio(command.getConsteBienConsumible());
	    				 }
	    		        command.setCodTipoServicio(command.getCodSubTipoReq());
	    			  }
	    	}
    	
	    }
    	System.out.println("Bandera: "+command.getBandera());
    	System.out.println("setCodSeleccionados: "+command.getCodSeleccionados());
    	command.setBanListaBandeja("0");
    	ListaTipoPago(command);
    	
    	List lista6= new ArrayList();
    	lista6=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_SERVICIO 
    			, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
    	if(lista6!=null)
    	command.setListaTipoServicio(lista6);
    	
   	 log.info("formBackingObject:FIN");
     return command;
 }
	
	protected void initBinder(HttpServletRequest request,
         ServletRequestDataBinder binder) {
 	NumberFormat nf = NumberFormat.getNumberInstance();
 	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
 	
 }
 /**
  * Redirect to the successView when the cancel button has been pressed.
  */
 public ModelAndView processFormSubmission(HttpServletRequest request,
                                           HttpServletResponse response,
                                           Object command,
                                           BindException errors)
 throws Exception {
 	return super.processFormSubmission(request, response, command, errors);
 }
	
 public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
         BindException errors)
 		throws Exception {
 	log.info("onSubmit:INI");
 	
 	PenAgregarCotizacionExistenteCommand control= (PenAgregarCotizacionExistenteCommand) command;
	control.setMsg("");
 	if(control.getOperacion().equals("BUSCAR"))
 	{
 		BuscarBandeja(control, request);
 	}
 	else if (control.getOperacion().equals("GRABAR"))
 	{
 		StringTokenizer stCodigos = new StringTokenizer(control.getCodSeleccionados(), "*");
		System.out.println("Cadena a Guardar: "+control.getCodSeleccionados());
 		String strCadena="";
	    int contador;
 	    while(stCodigos.hasMoreTokens())
 	    {
 	    	strCadena=(String)stCodigos.nextElement();
 	    	contador=0;
 	    	StringTokenizer subStr = new StringTokenizer(strCadena, "$");
 	    	while(subStr.hasMoreTokens())
 	    	{
 	    		subStr.nextElement();
 	    		contador++;
 	    	}
 			System.out.println("Cadena a Guardar: "+strCadena);
 			System.out.println("getCodUsuario: "+control.getCodUsuario());
 			System.out.println("getCodCotizacion: "+control.getCodCotizacion());
 			System.out.println("Cantidad: "+contador);
 	 		control.setMsg(
 	 	 		this.cotBienesYServiciosManager.InsertDetalleCotizacionExistente(
 	 	 			control.getCodUsuario(), control.getCodCotizacion()
 	 	 			, strCadena, String.valueOf(contador)));
 	    }  
 		BuscarBandeja(control, request);
 	}
 	control.setOperacion("");
 	log.info("onSubmit:FIN");
    return new ModelAndView("/logistica/CotBienesYServicios/Pen_Agregar_Cotizacion_Existente","control",control);
}

 public void BuscarBandeja(PenAgregarCotizacionExistenteCommand control,HttpServletRequest request)
 {
	 System.out.println("Buscar_Bienes :O");
	 if(control.getConsteRadio().equals(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO))
		 control.setValRadio(CommonConstants.COD_SOL_TIPO_BIEN_ACTIVO);
	 else{ if(control.getConsteRadio().equals(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE))
		   control.setValRadio(CommonConstants.COD_SOL_TIPO_BIEN_CONSUMIBLE);
		 }
	 System.out.println("control.getConsteRadio: "+control.getConsteRadio());
	 if(control.getCodTipoServicio().equals("-1")) control.setCodTipoServicio("");
	 
	 List lista2= new ArrayList();
	 if(control.getBandera().equals("1")||control.getBandera().equals("2"))
	 {lista2=this.cotBienesYServiciosManager.GetAllCotizacionesExixtentes(control.getCodSede()
			 , CommonConstants.COD_SOL_TIPO_REQ_BIEN, control.getTxtNroCotizacion()
			 , control.getFecInicio(), control.getFecFinal(), control.getCodTipoPago()
			 , control.getConsteRadio(), control.getIndInversion());
	 }
	 else{  if(control.getBandera().equals("3"))
	 		{lista2=this.cotBienesYServiciosManager.GetAllCotizacionesExixtentes(control.getCodSede()
	 				, CommonConstants.COD_SOL_TIPO_REQ_SERVICIO, control.getTxtNroCotizacion()
	 				, control.getFecInicio(), control.getFecFinal(), control.getCodTipoPago()
	 				, control.getCodTipoServicio(), control.getIndInversion());
	 		}
	 }
	 if(lista2!=null)
		{ if(lista2.size()==0) control.setBanListaBandeja("0");
		  else control.setBanListaBandeja("1");
		  control.setListaConsulta(lista2);
		  }
	 else control.setBanListaBandeja("0");
	 
	 control.setValRadio(control.getConsteRadio());
	 System.out.println("Valor Radio :D"+control.getValRadio());
	 request.setAttribute("BUSCAR", "OK"); 
 }
 
 private void ListaTipoPago(PenAgregarCotizacionExistenteCommand control){
 	//carga combo tipo de pago
	 List lista1= new ArrayList();
 	TipoLogistica obj = new TipoLogistica();
 	obj.setCodPadre("");
 	obj.setCodId("");
 	obj.setCodTabla(CommonConstants.TIPT_TIPO_PAGO);
 	obj.setCodigo("");
 	obj.setDescripcion("");
 	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
 	lista1=this.detalleManager.GetAllTablaDetalle(obj);
 	if(lista1!=null)
    	control.setListaCodTipoBien(lista1);
 	
 }

public CotBienesYServiciosManager getCotBienesYServiciosManager() {
	return cotBienesYServiciosManager;
}

public void setCotBienesYServiciosManager(
		CotBienesYServiciosManager cotBienesYServiciosManager) {
	this.cotBienesYServiciosManager = cotBienesYServiciosManager;
}

public TablaDetalleManager getTablaDetalleManager() {
	return tablaDetalleManager;
}

public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
	this.tablaDetalleManager = tablaDetalleManager;
}

public SeguridadManager getSeguridadManager() {
	return seguridadManager;
}

public void setSeguridadManager(SeguridadManager seguridadManager) {
	this.seguridadManager = seguridadManager;
}
}
