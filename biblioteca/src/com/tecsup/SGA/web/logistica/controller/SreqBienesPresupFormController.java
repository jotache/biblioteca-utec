package com.tecsup.SGA.web.logistica.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.SreqBienesCommand;

public class SreqBienesPresupFormController extends SimpleFormController {
	private static Log log = LogFactory.getLog(SreqBienesPresupFormController.class);
	private DetalleManager detalleManager;
	private SeguridadManager seguridadManager;
	private SolRequerimientoManager solRequerimientoManager;
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)throws ServletException {	    	
		SreqBienesCommand command = new SreqBienesCommand(); 	
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento") == null ? "": request.getParameter("txhCodRequerimiento"));
    	command.setCodSede(request.getParameter("txhCodSede") == null ? "": request.getParameter("txhCodSede"));
    	command.setCodUsuario(request.getParameter("txhCodUsuario") == null ? "": request.getParameter("txhCodUsuario"));
    	command.setCodRequerimientoOri(request.getParameter("txhCodRequerimientoOri") == null ? "": request.getParameter("txhCodRequerimientoOri"));
    	command.setIndTitulo(request.getParameter("txhIndTitulo") == null ? "": request.getParameter("txhIndTitulo"));
    	command.setCboTipoRequerimiento(request.getParameter("txhCodTipoRequerimiento") == null ? "": request.getParameter("txhCodTipoRequerimiento"));  
    	command.setCodTipoBien(request.getParameter("txhCodTipoBien") == null ? "": request.getParameter("txhCodTipoBien"));
    	command.setTipoProcedencia(request.getParameter("txhTipoProcedencia") == null ? "": request.getParameter("txhTipoProcedencia"));
    	    	
    	if(request.getParameter("txhCodSede")!=null){
    		command.setCodSede(request.getParameter("txhCodSede"));    		
    		command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));
    		command.setIndEmpLogistica(solRequerimientoManager.GetIndEmpLogistica(command.getCodUsuario()));          
        }
    	    	
        command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN); 
        command.setConsteCodBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
    	
    	if(command.getCodUsuario() != null ){
    		if(!command.getCodUsuario().equals("")){    		
        		cargaCabecera(command);
        	}   	
    	}
        return command;
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,BindException errors) throws Exception {
		SreqBienesCommand control = (SreqBienesCommand)command;
    	String resultado="";
    	String resultadoEnviar="";
    	
    	if (control.getOperacion().trim().equals("GRABAR")){
    		resultado = InsertSolicitudRequerimiento(control);    		
    		control.setCodRequerimiento(resultado);
    		InsertSolicitudRequerimientoDetalle(control);
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{ if(resultado.equals("-2")){
    				control.setMsg("ALMACEN_CERRADO");
    			  }
    			  control.setMsg("OK_GRABAR");
    		}
    		
    		cargaCabecera(control);    		
    	}
    	else
		if (control.getOperacion().trim().equals("QUITAR")){			
    		resultado = DeleteSolRequerimientoDetalle(control);    		
    		cargaCabecera(control);
    	}
		else
		if (control.getOperacion().trim().equals("REFRESCAR")){			
			cargaCabecera(control);    		
    	}
		else
		if (control.getOperacion().trim().equals("ENVIAR")){			
			resultado = InsertSolicitudRequerimiento(control);    		
    		control.setCodRequerimiento(resultado);
    		InsertSolicitudRequerimientoDetalle(control);
    		if(resultado.equals("-1")){
    			control.setMsg("ERROR_GRABAR");
    		}
    		else{ if(resultado.equals("-2")){
    				control.setMsg("ALMACEN_CERRADO");
    			  }
    			  else{    			  
    				  resultadoEnviar=EnviarSolicitudRequerimientoUsuario(control);    					
    					if (resultadoEnviar.equals("-1")){
    		    			control.setMsg("ERROR");
    		    		}
    		    		else{
    		    			if(resultadoEnviar.equals("-2")){
    		    				control.setMsg("ERROR_PROCESO");
    		    			}
    		    			else{
    		    				if(resultadoEnviar.equals("-3")){
    		        				control.setMsg("ERROR_INGRESO");
    		        			}
    		        			else{
    		        				if(resultadoEnviar.equals("-4")){
    		            				control.setMsg("ERROR_NO_RESP");
    		            			}
    		        				else{ if(resultadoEnviar.equals("-5")){
    		            				  control.setMsg("ALMACEN_CERRADO");
    		            			      }
    		        					  control.setMsg("OK");
    		        				}
    		        			}
    		    			}
    		    		}  
    			  }
    		}
						
			cargaCabecera(control);
    	}
		else if(control.getOperacion().trim().equals("CANCELAR"))
			  {  resultado= EliminarRegistro( control);			     
	    		 if ( resultado.equals("-2")) control.setMsg("ERROR_ELIMINAR");
	       		 else{ if(resultado.equals("0"))
	       			  {control.setMsg("OK_ELIMINAR");
	       			  }
	       			else{ if(resultado.equals("-1"))
	       				     control.setMsg("NO_SE_ELIMINA");
	       				 else  if(resultado.equals("-3"))
	       				       control.setMsg("ALMACEN_CERRADO");
	       			}
	       		}
	    		 cargaCabecera(control);
			  }	
	    return new ModelAndView("/logistica/SolRequerimiento/Sreq_BienesPresup","control",control);
	}
	
	private void cargaCabecera(SreqBienesCommand control){
    	//carga lista del combo tipo Requerimiento
    	TipoLogistica obj = new TipoLogistica();
    	obj.setCodPadre("");
    	obj.setCodId("");
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_REQUERIMIENTO);
    	obj.setCodigo("");
    	obj.setDescripcion("");
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	control.setListaRequerimiento(this.detalleManager.GetAllTablaDetalle(obj));
    	
    	//carga lista del combo centro de costo    	
    	control.setListaCentroCosto(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(), "", ""));
    	
    	//carga los codRadio tipo de bien se guardan en dos hidden
    	TipoLogistica obj2 = new TipoLogistica();    	
    	obj2.setCodPadre("");
    	obj2.setCodId("");
    	obj2.setCodTabla(CommonConstants.TIPT_TIPO_BIEN);
    	obj2.setCodigo("");
    	obj2.setDescripcion("");
    	obj2.setTipo(CommonConstants.TIPO_ORDEN_COD);    	
    	control.setListaCodTipoBien(this.detalleManager.GetAllTablaDetalle(obj2));
    	Logistica logistica = new Logistica();
    	logistica=(Logistica)control.getListaCodTipoBien().get(0);
    	control.setCodTipoBienConsumible(logistica.getCodSecuencial());
    	logistica=(Logistica)control.getListaCodTipoBien().get(1);
    	control.setCodTipoBienActivo(logistica.getCodSecuencial());
    	//*****************************************
    	//carga lista de combo tipo de servicio
    	TipoLogistica obj3 = new TipoLogistica();    	
    	obj3.setCodPadre("");
    	obj3.setCodId("");
    	obj3.setCodTabla(CommonConstants.TIPT_TIPO_SERVICIO);
    	obj3.setCodigo("");
    	obj3.setDescripcion("");
    	obj3.setTipo(CommonConstants.TIPO_ORDEN_DSC);    	
    	control.setListaServicio(this.detalleManager.GetAllTablaDetalle(obj3));
    	
    	TipoLogistica obj4 = new TipoLogistica();
    	obj4.setCodPadre("");
    	obj4.setCodId("");
    	obj4.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
    	obj4.setCodigo("");
    	obj4.setDescripcion("");
    	obj4.setTipo(CommonConstants.TIPO_ORDEN_DSC);    	
    	control.setListTipoGastoServicio(this.detalleManager.GetAllTablaDetalle(obj4));
    	try {
    		control.setFechaActual(Fecha.getFechaActual());
		} catch (Exception e) {
			e.printStackTrace(); 
		}   			
    	cargaDatosSolicitudRequerimiento(control);
    }
	
	private void cargaDatosSolicitudRequerimiento(SreqBienesCommand control){
    	ArrayList<SolRequerimiento> listaSolReq;    	
    	if(!control.getCodRequerimiento().equals("")){    		
    		listaSolReq = (ArrayList<SolRequerimiento>)this.solRequerimientoManager.getSolReqUsuario(control.getCodRequerimiento());
    		if ( listaSolReq != null )
    			if ( listaSolReq.size() > 0 )
    				{
			    		SolRequerimiento solReqUsuario=listaSolReq.get(0);
			    		
			    		System.out.println("codigo sol req usuario-->"+solReqUsuario.getIdRequerimiento());
			    		
			    		control.setCodRequerimiento(solReqUsuario.getIdRequerimiento());
			    		control.setNroRequerimiento(solReqUsuario.getNroRequerimiento());
			    		control.setFechaRequerimiento(solReqUsuario.getFechaEmision());
			    		control.setCboTipoCentroCosto(solReqUsuario.getCecoSolicitante());
			    		control.setUsuSolicitante(solReqUsuario.getUsuSolicitante());
			    		control.setCboTipoRequerimiento(solReqUsuario.getTipoRequerimiento());
			    		control.setEstado(solReqUsuario.getEstadoReg());
			    		if(control.getCboTipoRequerimiento().equals("0001")){
			    			control.setCodSubTipoRequerimiento(solReqUsuario.getSubTipoRequerimiento());			    			
			    		}
			    		else{
			    			control.setCboTipoServicio(solReqUsuario.getSubTipoRequerimiento());
			    		}
			    		control.setIndInversion(solReqUsuario.getIndInversion());
			    		control.setNroAsociado(solReqUsuario.getNroRequerimientoOri());			    	
			    		control.setIndParaStock(solReqUsuario.getIndParaStock());
			    		control.setIndEmpLogistica(solReqUsuario.getIndEmpLogistica());
			    		//carga la bandeja de la solicitud del usuario
			    		cargaBandejaSolicitudDetalle(control);
    				}
    	}
    	else
    	if(control.getCodRequerimiento().equals("") && !control.getCodRequerimientoOri().equals("")){
    		ArrayList<SolRequerimiento> listaSolReqOri = (ArrayList<SolRequerimiento>)this.solRequerimientoManager.getSolReqUsuario(control.getCodRequerimientoOri());
    		if ( listaSolReqOri != null ){
    			if ( listaSolReqOri.size() > 0 ){
		    		SolRequerimiento solReqOri=listaSolReqOri.get(0);		    		
		    		control.setCboTipoCentroCosto(solReqOri.getCecoSolicitante());
		    		control.setNroAsociado(solReqOri.getNroRequerimiento());
		    		control.setCodSubTipoRequerimiento(solReqOri.getSubTipoRequerimiento());		    		
    			}
    		}
    	}
    }
	
	private void cargaBandejaSolicitudDetalle(SreqBienesCommand control){
    	System.out.println("entra para cargar la bandeja");
    	if(control.getCboTipoRequerimiento().equals("0001")){
    		List listaBien=this.solRequerimientoManager.getAllSolReqDetalle(control.getCodRequerimiento(), "");
    		
    		if(listaBien!=null){
    			if(listaBien.size()>0){
    				control.setListaSolicitudDetalle(listaBien);
    				control.setTamListaSolicitudDetalle(""+listaBien.size());
    				//carga el combo tipo de gasto solo si existen el detalle de la solicitud de req.
            		TipoLogistica obj = new TipoLogistica();    	
                	obj.setCodPadre("");
                	obj.setCodId("");
                	obj.setCodTabla(CommonConstants.TIPT_TIPO_GASTOS);
                	obj.setCodigo("");
                	obj.setDescripcion("");
                	obj.setTipo(CommonConstants.TIPO_ORDEN_COD);    	
                	control.setListaGasto(this.detalleManager.GetAllTablaDetalle(obj));
    			}
    			else{
    				control.setTamListaSolicitudDetalle("0");
    			}
    		}
    	}
    	if(control.getCboTipoRequerimiento().equals("0002")){    		
    		List listaServicio=this.solRequerimientoManager.getAllSolReqDetalle(control.getCodRequerimiento(), "");
    		if(listaServicio!=null && listaServicio.size()>0){
    			SolRequerimientoDetalle solRequerimientoDetalle = (SolRequerimientoDetalle)listaServicio.get(0);
    			control.setCodReqDetalleServicio(solRequerimientoDetalle.getCodigo());
    			control.setNombreServicio(solRequerimientoDetalle.getNombreServicio());
    			control.setFechaAtencion(solRequerimientoDetalle.getFechaEntrega());
    			control.setDescripcion(solRequerimientoDetalle.getDescripcion());    			
    			control.setCodTipoGastoServicio(solRequerimientoDetalle.getTipoGasto());
    		}    		 
    	}
    }
	
	private String InsertSolicitudRequerimiento(SreqBienesCommand control){
    	try{    		
    		SolRequerimiento solicitudRequerimiento = new SolRequerimiento();    		
    		solicitudRequerimiento.setIdRequerimiento(control.getCodRequerimiento());
    		solicitudRequerimiento.setCecoSolicitante(control.getCodTipoCentroCosto());
    		solicitudRequerimiento.setSede(control.getCodSede());
    		solicitudRequerimiento.setUsuSolicitante(control.getCodUsuario());
    		solicitudRequerimiento.setTipoRequerimiento(control.getCodTipoRequerimiento());
    		solicitudRequerimiento.setSubTipoRequerimiento(control.getCodSubTipoRequerimiento());
    		solicitudRequerimiento.setCodRequerimientoOri(control.getCodRequerimientoOri());    		
    		solicitudRequerimiento.setIndInversion("0");    		
    		solicitudRequerimiento.setIndParaStock("0");
    		String resultado="";
    		resultado=this.solRequerimientoManager.InsertSolRequerimientoUsuario(solicitudRequerimiento);
    		return resultado;    		
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;    	
    }
	
	private void InsertSolicitudRequerimientoDetalle(SreqBienesCommand control){
		if(control.getCodTipoRequerimiento().equals("0001")){			
			InsertSolicitudRequerimientoDetalleBien(control);
		}
		if(control.getCodTipoRequerimiento().equals("0002")){			
			InsertSolicitudRequerimientoDetalleServicio(control);
		}
	}
	
	private void InsertSolicitudRequerimientoDetalleBien(SreqBienesCommand control){
		StringTokenizer cadenaCodigo = new StringTokenizer(control.getCadenaCodigo(),"|");
		StringTokenizer cadenaCodBien = new StringTokenizer(control.getCadenaCodBien(),"|");
		StringTokenizer cadenaCantidad = new StringTokenizer(control.getCadenaCantidad(),"|");
		StringTokenizer cadenaCodTipoGasto = new StringTokenizer(control.getCadenaCodTipoGasto(),"|");
		StringTokenizer cadenaFechaEntrega = new StringTokenizer(control.getCadenaFechaEntrega(),"|");
		StringTokenizer cadenaDescripcion = new StringTokenizer(control.getCadenaDescripcion(),"|");
		
		while(cadenaCodBien.hasMoreTokens()){
			String tokenCodigo = cadenaCodigo.nextToken();
			String tokenCodBien = cadenaCodBien.nextToken();
			String tokenCantidad = cadenaCantidad.nextToken();
			if(tokenCantidad.equals("$")){
				tokenCantidad="";
			}
			String tokenCodTipoGasto = cadenaCodTipoGasto.nextToken();
			String tokenFechaEntrega = cadenaFechaEntrega.nextToken();
			if(tokenFechaEntrega.equals("$")){
				tokenFechaEntrega="";
			}
			String tokenDescripcion = cadenaDescripcion.nextToken();
			if(tokenDescripcion.equals("$")){
				tokenDescripcion="";
			}		
			
			SolRequerimientoDetalle obj = new SolRequerimientoDetalle();
			obj.setCodRequerimiento(control.getCodRequerimiento());
			obj.setCodigo(tokenCodigo);
			obj.setNombreServicio("");
			obj.setDescripcion(tokenDescripcion);
			obj.setCodigoBien(tokenCodBien);
			obj.setCantidad(tokenCantidad);
			obj.setTipoGasto(tokenCodTipoGasto);
			obj.setFechaEntrega(tokenFechaEntrega);			
						
			this.solRequerimientoManager.InsertSolRequerimientoDetalle(obj, control.getCodUsuario());
		}		
	}
	private void InsertSolicitudRequerimientoDetalleServicio(SreqBienesCommand control){
    	SolRequerimientoDetalle obj = new SolRequerimientoDetalle();
		obj.setCodRequerimiento(control.getCodRequerimiento());
		obj.setCodigo(control.getCodReqDetalleServicio());
		obj.setNombreServicio(control.getNombreServicio());
		obj.setDescripcion(control.getDescripcion());
		obj.setCodigoBien("");
		obj.setCantidad("");
		obj.setTipoGasto(control.getCodTipoGastoServicio());
		obj.setFechaEntrega(control.getFechaAtencion());		
		this.solRequerimientoManager.InsertSolRequerimientoDetalle(obj, control.getCodUsuario());
    }
	private String DeleteSolRequerimientoDetalle(SreqBienesCommand control){
    	try{
    		String resultado="";
    		resultado=this.solRequerimientoManager.DeleteSolRequerimientoDetalle(control.getCodRequerimiento(), control.getCodDetRequerimiento(), control.getCodUsuario());
    		return resultado;    	
       	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return null;
    }
	private String EnviarSolicitudRequerimientoUsuario(SreqBienesCommand control){
		String resultado="";
		resultado=this.solRequerimientoManager.EnviarSolRequerimientoUsuario(control.getCodRequerimiento(), control.getCodUsuario());
		return resultado;
	}
	
	public String EliminarRegistro(SreqBienesCommand control){
    	String resultado="";
    	 resultado=this.solRequerimientoManager.DeleteSolRequerimiento(control.getCodRequerimiento(), control.getCodUsuario());
    	return resultado;
    }
}
