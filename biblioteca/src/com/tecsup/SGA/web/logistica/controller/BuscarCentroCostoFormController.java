package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.web.logistica.command.BuscarCentroCostoCommand;

public class BuscarCentroCostoFormController  extends SimpleFormController{
	private static Log log = LogFactory.getLog(BuscarCentroCostoFormController.class);
	   DetalleManager detalleManager;
	   SolRequerimientoManager solRequerimientoManager; 
	   
	    public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
		public void setDetalleManager(DetalleManager detalleManager) {
			this.detalleManager = detalleManager;
		}
	    public BuscarCentroCostoFormController() {    	
	        super();        
	        setCommandClass(BuscarCentroCostoCommand.class);        
	    }

		protected Object formBackingObject(HttpServletRequest request)
	    throws ServletException {
	    	log.info("formBackingObject:INI");
	    	
	    	BuscarCentroCostoCommand command = new BuscarCentroCostoCommand();
	    	
	    	command.setSede((String)request.getParameter("txhCodSede"));
	    	command.setNomObjeto((String)request.getParameter("txhNomControl"));
	    	
	    	 log.info("formBackingObject:FIN");
		        return command;
		    }
			
		    protected void initBinder(HttpServletRequest request,
		            ServletRequestDataBinder binder) {
		    	NumberFormat nf = NumberFormat.getNumberInstance();
		    	binder.registerCustomEditor(Long.class,
			                  new CustomNumberEditor(Long.class, nf, true));	
		    }
		        
		    public ModelAndView processFormSubmission(HttpServletRequest request,
		                                              HttpServletResponse response,
		                                              Object command,
		                                              BindException errors)
		    throws Exception {
		        return super.processFormSubmission(request, response, command, errors);
		    }
		    
		    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
		            BindException errors)
		    		throws Exception {
		    	log.info("onSubmit:INI");
		    	int resultado;
		    	BuscarCentroCostoCommand control = (BuscarCentroCostoCommand) command;
		   
		    	if(control.getOperacion().equals("BUSCAR")){
		    		buscar(control);
		    	}
		    	request.getSession().setAttribute("listCentros", control.getListCentros());
		    	log.info("onSubmit:FIN");
			    return new ModelAndView("/logistica/SolAproRequerimientos/log_Buscar_CentroCosto","control",control);		
		    }
		    
		    private void buscar(BuscarCentroCostoCommand control){
		    	control.setListCentros(this.solRequerimientoManager.GetAllCentrosCostos(control.getSede(),control.getDescripcion()));
		  System.out.println("tot: "+control.getListCentros().size());
		    }
		    
}
