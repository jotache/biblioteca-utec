package com.tecsup.SGA.web.logistica.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.BandejaLogisticaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.SreqBandejaEvaluadorCommand;

public class SreqBandejaEvaluadorPresupFormController extends
		SimpleFormController {

	private static Log log = LogFactory.getLog(SreqBandejaEvaluadorPresupFormController.class);
	SolRequerimientoManager solRequerimientoManager;
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	
	public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
	protected Object formBackingObject(HttpServletRequest request)throws ServletException {
		SreqBandejaEvaluadorCommand command = new SreqBandejaEvaluadorCommand();
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){    	
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    		command.setCodOpciones(usuarioSeguridad.getOpciones());
    	}
    	command.setConsteCodBien(CommonConstants.COD_SOL_TIPO_REQ_BIEN);
        command.setConsteCodServicio(CommonConstants.COD_SOL_TIPO_REQ_SERVICIO);
        command.setConsteCodEstadoPendiente(CommonConstants.COD_ESTADO_PENDIENTE);
        command.setConsteCodEstadoRechazado(CommonConstants.COD_ESTADO_RECHAZO);
        command.setConsteCodEstadoAprobacion(CommonConstants.COD_ESTADO_APROBACION);
        command.setConsteCodEstadoCerrado(CommonConstants.COD_ESTADO_CERRADO);
        if(request.getParameter("txhCodSede")!=null){
          command.setCodSede(request.getParameter("txhCodSede"));    	  
          command.setEstadoAlmacen(solRequerimientoManager.GetEstadoAlmacen(command.getCodSede()));          
        }
        command.setConsteEstadoAlmacen(CommonConstants.CONSTE_ESTADO_ALMACEN); 
        
        List lista1= new ArrayList();
        List lista2= new ArrayList();
        List lista3= new ArrayList();
        lista1=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_TIPO_REQUERIMIENTO, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
        if(lista1!=null) command.setListTipoRequerimiento(lista1);
        lista2=this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_ESTD_REQUERIMIENTO, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
        if(lista2!=null) command.setListEstado(lista2);
        lista3=this.seguridadManager.GetAllCentrosCosto(command.getCodSede(),"","");
        if(lista3!=null) command.setListCeco(lista3);
        
        BandejaLogisticaBean bandejaBean = (BandejaLogisticaBean)request.getSession().getAttribute("bandejaLogisticaBean");
        if(bandejaBean!= null){           
            if(bandejaBean.getCodBandeja().equals("1")){
            	command.setCodTipoRequerimiento(bandejaBean.getValor1());
            	command.setNroRequerimiento(bandejaBean.getValor2());
            	command.setFecInicio(bandejaBean.getValor3());
            	command.setFecFinal(bandejaBean.getValor4());
            	command.setCodCeco(bandejaBean.getValor5());
            	command.setCodEstado(bandejaBean.getValor6());
       	 	 	BuscarBandeja(command, request);
            }       	  
         }else{                
        	request.getSession().removeAttribute("listaBien");
        	request.getSession().removeAttribute("listaServicios");        	
        }
		return command;
	}
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,BindException errors) throws Exception {
		SreqBandejaEvaluadorCommand control = (SreqBandejaEvaluadorCommand)command;
    	String resultado="";
    	if(control.getOperacion().equals("BUSCAR"))
    		 BuscarBandeja( control,  request);    		
    	else { 
    			if(control.getOperacion().equals("ELIMINAR")){    		 
    				resultado= EliminarRegistro(control);
 		   			if ( resultado.equals("-2")) 
 		   				control.setMsg("ERROR");
 		   			else { 
 		   				if(resultado.equals("0")){
 		   					control.setMsg("OK");
 		   					BuscarBandeja( control,  request);
 		   				} else { 
 		   					if(resultado.equals("-1"))
 		   						control.setMsg("NO_SE_ELIMINA");
 		   					else  if(resultado.equals("-3"))
 		   						control.setMsg("ALMACEN_CERRADO");
 		   				}
 		   			}
    			}	
    		}
    	
    	return new ModelAndView("/logistica/SolRequerimiento/Sreq_Bandeja_EvaluadorPresup","control",control);
	}
	
	private void BuscarBandeja(SreqBandejaEvaluadorCommand control,HttpServletRequest request){
		GuardarDatosBandeja(control, request);
		if(control.getCodCeco().equals("-1")) control.setCodCeco("");
	    if(control.getCodEstado().equals("-1")) control.setCodEstado("");
	    List listaTipo=this.solRequerimientoManager.getAllSolReqUsuario(control.getCodSede(),control.getCodTipoRequerimiento(),control.getNroRequerimiento(),control.getFecInicio(),
	    		control.getFecFinal(),control.getCodCeco(),control.getCodEstado(), control.getCodUsuario());
	    	    
	    if(control.getCodTipoRequerimiento().equals(control.getConsteCodBien())){
	    	control.setBanderaTipoReq("BIEN");  ///request.setAttribute("TIPO_REQ", "BIEN");
	        request.getSession().setAttribute("listaBien", listaTipo);	       
	    }
	    else{ if(control.getCodTipoRequerimiento().equals(control.getConsteCodServicio()))
	    		{ control.setBanderaTipoReq("SERVICIO"); //request.setAttribute("TIPO_REQ", "SERVICIO");
	    		  request.getSession().setAttribute("listaServicios", listaTipo); 
	    		  log.info("SERVICIO");
	    		  
	    		}
	    	}
	    control.setListCeco(this.seguridadManager.GetAllCentrosCosto(control.getCodSede(),"",""));
	}
	private String EliminarRegistro(SreqBandejaEvaluadorCommand control){
		String resultado="";    	
    	resultado=this.solRequerimientoManager.DeleteSolRequerimiento(control.getCodRequerimiento(),control.getCodUsuario());    	
    	return resultado;
	}
	
	public void GuardarDatosBandeja(SreqBandejaEvaluadorCommand control,HttpServletRequest request){	  
	 	 BandejaLogisticaBean bandejaBean= new BandejaLogisticaBean();	 	
	 	 bandejaBean.setCodBandeja("1");	 	 
	 	 bandejaBean.setValor1(control.getCodTipoRequerimiento());
	 	 bandejaBean.setValor2(control.getNroRequerimiento());
	 	 bandejaBean.setValor3(control.getFecInicio());
	 	 bandejaBean.setValor4(control.getFecFinal());
	 	 bandejaBean.setValor5(control.getCodCeco());
	 	 bandejaBean.setValor6(control.getCodEstado());	 		 		 	 	 	 
	 	 request.getSession().setAttribute("bandejaLogisticaBean", bandejaBean);
	}
}
