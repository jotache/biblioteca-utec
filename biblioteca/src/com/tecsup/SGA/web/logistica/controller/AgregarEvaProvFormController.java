package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarEvaProvCommand;

public class AgregarEvaProvFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(AgregarEvaProvFormController.class);
 	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public AgregarEvaProvFormController() {    	
        super();        
        setCommandClass(AgregarEvaProvCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	AgregarEvaProvCommand command = new AgregarEvaProvCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	command.setSecuencial((String)request.getParameter("txhSecuencial"));
    	command.setTipo((String)request.getParameter("txhTipo"));
    	command.setCalificacion((String)request.getParameter("txhDesc"));
            log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado="";
    	AgregarEvaProvCommand control = (AgregarEvaProvCommand) command;
    	
    	if("GRABAR".equals(control.getOperacion())){
    		if("MODIFICAR".equals(control.getTipo())){
        		resultado = modifica(control);
        		if ( resultado.equals("-1")) {
        			control.setMsg("ERROR");
        		}
        		else if(resultado.equals("-2")){
    	    		control.setMsg("DOBLE");
    	    	}
            	else control.setMsg("OK");
        		}
    	
    			
    		else{
    			resultado = grabar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else if(resultado.equals("-2")){
	    		control.setMsg("DOBLE");
	    	}
        	else control.setMsg("OK");
    		}
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Agregar_Eva_Prov","control",control);		
    }
        private String grabar(AgregarEvaProvCommand control){
        	
        	control.setGraba(this.detalleManager.InsertTablaDetalle(""
        			, CommonConstants.TIPT_TIPO_CALIFICACION, control.getCalificacion(), "", "", "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        private String modifica(AgregarEvaProvCommand control){
        	
        	control.setGraba(this.detalleManager.UpdateTablaDetalle(""
        			,  CommonConstants.TIPT_TIPO_CALIFICACION, control.getSecuencial()
        			, "", control.getCalificacion(), "", "", "", control.getCodAlumno()));
        	
        	return control.getGraba();
        }
        
}
