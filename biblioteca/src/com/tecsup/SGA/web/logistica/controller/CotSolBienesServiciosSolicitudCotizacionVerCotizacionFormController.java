package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonMessage;
import com.tecsup.SGA.web.logistica.command.CotSolVerCotizacionFormController;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.TipoLogistica;


public class CotSolBienesServiciosSolicitudCotizacionVerCotizacionFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(CotSolBienesServiciosSolicitudCotizacionVerCotizacionFormController.class);
	CotBienesYServiciosManager cotBienesYServiciosManager;
	//ALQD,16/04/09.A�ADIENDO UN OBJETO PARA OBTENER LISTA DE UNIDAD DE MEDIDA
	DetalleManager detalleManager;

	public void setCotBienesYServiciosManager(
			CotBienesYServiciosManager cotBienesYServiciosManager) {
		this.cotBienesYServiciosManager = cotBienesYServiciosManager;
	}
	public void setDetalleManager(DetalleManager detalleManager){
		this.detalleManager=detalleManager;
	}

	public CotSolBienesServiciosSolicitudCotizacionVerCotizacionFormController() {    	
        super();        
        setCommandClass(CotSolVerCotizacionFormController.class);        
    }
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	Cotizacion oCotizacion;
    	CotSolVerCotizacionFormController command = new CotSolVerCotizacionFormController();
    	
    	if( request.getParameter("prmCodCotizacion") != null ){
    		
    		command.setTxhCodCotizacion(request.getParameter("prmCodCotizacion"));
    		command.setUsuario(request.getParameter("prmUsuario"));
    		command.setTxhCodSede(request.getParameter("prmCodSede"));
    		command.setTxhCodEstCotizacion(request.getParameter("prmCodEstCotizacion"));
    		command.setCodTipoReq(request.getParameter("txhCodTipoReq"));
    		//ALQD,04/02/09. NUEVO PARAMETRO
    		command.setNumCotizacion(request.getParameter("txhNumCotizacion"));
    		List lstResultado = cotBienesYServiciosManager.getLstDetalleCotizacion(command.getTxhCodCotizacion());
    		
    		getDatosCotizacion(command);
    		System.out.println("CodGruServicio: "+command.getCodGruServicio());

            command.setLstResultado(lstResultado);
            
            //LLAMADA AL SP
    		command.setListaProveedores(cotBienesYServiciosManager.getAllProvSenvByCoti(command.getTxhCodCotizacion(),null));
    		
    		log.info("TAMM>"+command.getListaProveedores().size()+">>");
    		
			command.setFlgComprador("1");
            
			//ALQD,15/04/09. ASIGNANDO LISTA DE UNIDAD DE MEDIDA
	    	TipoLogistica obj = new TipoLogistica();
	    	obj.setCodTabla(CommonConstants.TIPT_UNIDAD_MEDIDA);
	    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
	    	command.setLstUniMedida(this.detalleManager.GetAllTablaDetalle(obj));
    	}
    	
        log.info("formBackingObject:FIN");
        return command;
    }
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

    	CotSolVerCotizacionFormController control = (CotSolVerCotizacionFormController) command;
		
		if("irEliminar".equalsIgnoreCase(control.getOperacion()))
		{
			log.info("PARAMETROS PARA EL SP DeleteDetalleCotizacionActual");
			log.info("1->control.getTxhCodCotizacion()>>"+control.getTxhCodCotizacion()+">");
			log.info("2->txhCodDetCotizacion>>"+request.getParameter("txhCodDetCotizacion")+">");
			log.info("3->control.getUsuario()>>"+control.getUsuario()+">");
			String resp = "";
			
			resp = cotBienesYServiciosManager.DeleteDetalleCotizacionActual(control.getTxhCodCotizacion(),request.getParameter("txhCodDetCotizacion"),control.getUsuario());
			
			log.info("RESPUESTA:>>"+resp+">>");
			
			if(resp.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.ELIMINAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.ELIMINAR_ERROR);
			
			irConsultar(control);
			
		}else if("irRegistrar".equalsIgnoreCase(control.getOperacion())){
			
			String cantidad = request.getParameter("txhCantidad");
			String cadCodDet = request.getParameter("txhCadCodDet");
			String cadCodProv = request.getParameter("txhCadCodProv");
			
			log.info("PARAMETROS PARA EL SP INSERTAR");
			log.info("1->E_C_CODCOTI>>"+control.getTxhCodCotizacion()+">");
			log.info("2->E_V_CADCODDET>>"+cadCodDet+">");
			log.info("3->E_V_CADCODPROV>>"+cadCodProv+">");
			log.info("4->E_V_CADPRECIOS>>"+control.getCadPrecios()+">");
			log.info("5->E_C_CANTIDAD>>"+cantidad+">");
			log.info("6->E_C_CODUSUARIO>>"+control.getUsuario()+">");
			
			String resp = cotBienesYServiciosManager.UpdateDetProvBndjCotiComp(
					control.getTxhCodCotizacion(),
					cadCodDet,
					cadCodProv,
					control.getCadPrecios(),
					cantidad,
					control.getUsuario()
					);
			
			if(resp.equalsIgnoreCase("0"))
				request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
			else
				request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			
			
	    	TipoLogistica obj = new TipoLogistica();
	    	obj.setCodTabla(CommonConstants.TIPT_UNIDAD_MEDIDA);
	    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);	    	
	    	control.setLstUniMedida(this.detalleManager.GetAllTablaDetalle(obj));
			
			irConsultar(control);

			
		}else if("irCerrarCotizacion".equalsIgnoreCase(control.getOperacion())){
			log.info("PARAMETROS PARA EL SP CERRAR COTIZACION");
			
			log.info("1->E_C_CODCOTI>>"+control.getTxhCodCotizacion()+">");
			log.info("3->E_C_CODUSUARIO>>"+control.getUsuario()+">");

			
			String resp = cotBienesYServiciosManager.UpdateCerrarCotizacion(
					control.getTxhCodCotizacion(),
					control.getUsuario()
					);
			
			if(resp.equalsIgnoreCase("0"))
			{	request.setAttribute("mensaje",CommonMessage.GRABAR_EXITO);
				control.setTxhCodEstCotizacion(CommonConstants.COTIZACION_ESTADO_CERRADA);
			}
			else if(resp.equalsIgnoreCase("-2"))
				 request.setAttribute("mensaje",CommonMessage.CERRAR_ERROR);
				 else
				 request.setAttribute("mensaje",CommonMessage.GRABAR_ERROR);
			
			TipoLogistica obj = new TipoLogistica();
	    	obj.setCodTabla(CommonConstants.TIPT_UNIDAD_MEDIDA);
	    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);	    	
	    	control.setLstUniMedida(this.detalleManager.GetAllTablaDetalle(obj));
			
			irConsultar(control);
			
		}else if("CAMBIAR_FECHAS".equalsIgnoreCase(control.getOperacion())){
			String codCotizacion = control.getTxhCodCotizacion();
			String codUsuario= control.getUsuario();
			String fechaIni = control.getFecInicio();
			String horaIni= control.getHoraInicio();
			String fechaFin = control.getFecFin();
			String horaFin = control.getHoraFin();
			String resp = cotBienesYServiciosManager.actualizarFechas(codCotizacion, codUsuario, fechaIni, horaIni, fechaFin, horaFin);
			if(resp.equalsIgnoreCase("0"))
			{	request.setAttribute("mensaje","Fechas actualizadas satisfactoriamente");
			}
			else if(resp.equalsIgnoreCase("-1"))
				 request.setAttribute("mensaje","Ocurri� error al momento de actualizar las fechas");
			
			irConsultar(control);
		}
		
		control.setOperacion("");
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/CotBienesYServicios/Sol_Bandeja_Solicitud_Cotizacion_Ver_Cotizacion","control",control);
    }

		private void irConsultar(CotSolVerCotizacionFormController control) {
			log.info("cod>>"+control.getTxhCodCotizacion());
	    	List lstResultado = cotBienesYServiciosManager.getLstDetalleCotizacion(control.getTxhCodCotizacion());
			log.info("tamanio de la list>>"+lstResultado.size()+">");
	        control.setLstResultado(lstResultado);
		}
		
		
		private void getDatosCotizacion(CotSolVerCotizacionFormController command)
		{
			List lstCotizacion = cotBienesYServiciosManager.GetAllCotizacionById(command.getTxhCodCotizacion());
			Cotizacion oCotizacion;
			if ( lstCotizacion != null)
			{
				if ( lstCotizacion.size() > 0)
				{
					oCotizacion = (Cotizacion)lstCotizacion.get(0);
					
					command.setFecFin(oCotizacion.getFechaFin());
					command.setFecInicio(oCotizacion.getFechaInicio());
					command.setHoraFin(oCotizacion.getHoraFin());
					command.setHoraInicio(oCotizacion.getHoraInicio());
					//ALQD,15/04/09. SETEANDO NUEVA PROPIEDAD
					command.setCodGruServicio(oCotizacion.getCodGruServicio());
				}
			}
		}
}

