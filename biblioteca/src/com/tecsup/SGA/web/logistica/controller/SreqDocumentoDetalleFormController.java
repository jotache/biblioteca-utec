package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.Archivo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.web.logistica.command.SreqDocumentosDetalleCommand;

public class SreqDocumentoDetalleFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(SreqDocumentoDetalleFormController.class);
	SolRequerimientoManager solRequerimientoManager;
    
    public void setSolRequerimientoManager(
			SolRequerimientoManager solRequerimientoManager) {
		this.solRequerimientoManager = solRequerimientoManager;
	}

	public SreqDocumentoDetalleFormController() {    	
        super();        
        setCommandClass(SreqDocumentosDetalleCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	SreqDocumentosDetalleCommand command = new SreqDocumentosDetalleCommand();
    	
    	command.setCodRequerimiento(request.getParameter("txhCodRequerimiento"));
    	command.setCodDetRequerimiento(request.getParameter("txhCodDetRequerimiento"));
    	//para ocultar botones
    	command.setCondicion(request.getParameter("txhCondicion")==null ? "" : request.getParameter("txhCondicion"));
    	
    	if ( command.getCodDetRequerimiento() != null && command.getCodRequerimiento() != null )
    	{
    		command.setListaAdjuntos(this.solRequerimientoManager.getAllRequerimientosAdjunto(
    				command.getCodRequerimiento()
    				, command.getCodDetRequerimiento(), ""));
    	}
    	
//		request.setAttribute("msgRutaServer",CommonConstants.GSTR_HTTP + CommonConstants.SERVIDOR_PUERTO_WEB + CommonConstants.DIRECTORIO_PROY + CommonConstants.STR_RUTA_DOCUMENTOS_02);
    	request.getSession().setAttribute("listaDocumentos", command.getListaAdjuntos());
        log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");

		SreqDocumentosDetalleCommand control = (SreqDocumentosDetalleCommand) command;
		
		if (control.getOperacion().trim().equals("ELIMINAR"))
		{
			String respuesta = this.solRequerimientoManager.deleteRequerimientosAdjunto(control.getCodRequerimiento()
				, control.getCodDetRequerimiento(), control.getCodDocAdjunto(), control.getCodUsuario());
			
			if ( respuesta == null ) control.setMsg("ERROR");
			else if ( respuesta.trim().equals("-1")) control.setMsg("ERROR");
			else control.setMsg("OK");
			
		} 
		
		control.setListaAdjuntos(this.solRequerimientoManager.getAllRequerimientosAdjunto(
				control.getCodRequerimiento()
				, control.getCodDetRequerimiento(), ""));
		
    	request.getSession().setAttribute("listaDocumentos", control.getListaAdjuntos());
    	
    	
    	if(control.getOperacion().trim().equals("OPEN_DOCUMENTO")){
			String sep = System.getProperty("file.separator");
			String uploadDir = CommonConstants.DIRECTORIO_DATA_PRIVADO + CommonConstants.STR_RUTA_DOCUMENTOS_02;			
			Archivo.downloadFile(control.getDocumentDownload(), uploadDir + sep + control.getDocumentDownload(), response);
			return null;
		}
    	
		control.setCodDocAdjunto("");
		control.setOperacion("");
		
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/SolRequerimiento/Sreq_DocumentosDetalle","control",control);		
    }
}
