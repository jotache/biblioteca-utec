package com.tecsup.SGA.web.logistica.controller;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.AgregarFamiliaCommand;
import com.tecsup.SGA.web.logistica.command.DocumentosRelacionadosCommand;

public class DocumentosRelacionadosFormController  extends SimpleFormController{
	private static Log log = LogFactory.getLog(DocumentosRelacionadosFormController.class);
 	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public DocumentosRelacionadosFormController() {
        super();
        setCommandClass(DocumentosRelacionadosCommand.class);
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	DocumentosRelacionadosCommand command = new DocumentosRelacionadosCommand();
        
    	command.setCodUsuario((String)request.getParameter("txhCodUsuario"));
    	command.setCodDocPago((String)request.getParameter("txhDocPago"));
    	command.setCodEstadoOrden((String)request.getParameter("txhCodEstadoOrden"));

    	cargarData(command);
      	//ALQD,12/06/09.CARGAR LISTADO DE MONEDAS
    	TipoLogistica obj = new TipoLogistica();
    	obj.setCodTabla(CommonConstants.TIPT_MONEDA);
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	command.setListMonedas(this.detalleManager.GetAllTablaDetalle(obj));
    	
    	request.getSession().setAttribute("listDocumentosRelacionados", command.getListDocumentosRelacionados());
        
    	log.info("formBackingObject:FIN");
        return command;
    }
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	String resultado="";
    	DocumentosRelacionadosCommand control = (DocumentosRelacionadosCommand) command;
    	if("GRABAR".equals(control.getOperacion())){
    		
    		resultado = grabar(control);
    	
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    		}
    		else{
    			System.out.println("codigo generado :"+resultado);
    			control.setMsg("OK");
    			control.setCodDoc(resultado);
    		}
    	}
    	if("ELIMINAR".equals(control.getOperacion())){
    		resultado = eliminar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsgE("ERROR");
    		} 
    		else control.setMsgE("OK");
    	}
    	cargarData(control);
     	request.getSession().setAttribute("listDocumentosRelacionados", control.getListDocumentosRelacionados());
        
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/Documentos_Relacionados","control",control);
    }
        //ALQD,11/06/09.SE AGREGA 1 NUEVO PARAMETRO
        //ALQD,30/06/09.SE AGREGA 1 NUEVO PARAMETRO
        private String grabar(DocumentosRelacionadosCommand control){
        	control.setGraba(this.detalleManager.InsertDocAsociado(control.getCodDoc()
        			, control.getCodDocPago(), control.getNumero(), control.getFecha()
        			, control.getMonto(), control.getDescripcion(), control.getCodUsuario()
        			, control.getCodTipMoneda(), control.getImpTipCamSoles()
        			, control.getCodProvee(), control.getNomProvee(), control.getRucProvee()));
        	
        	return control.getGraba();
        }
        
        private void cargarData(DocumentosRelacionadosCommand control){
        	control.setListDocumentosRelacionados(this.detalleManager.GetAllDocAsociado( control.getCodDocPago()));
        	System.out.println("tot: "+control.getListDocumentosRelacionados().size());
        }
        private String eliminar(DocumentosRelacionadosCommand control){
        	System.out.println("getCodDoc: "+control.getCodDoc()+"; CodUsuario: "+control.getCodUsuario());
        	control.setGraba(this.detalleManager.DeleteDocAsociado(control.getCodDoc(), control.getCodUsuario()));
        	
        	return control.getGraba();
        }
       
   
}
