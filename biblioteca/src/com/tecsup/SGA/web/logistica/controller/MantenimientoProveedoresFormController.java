package com.tecsup.SGA.web.logistica.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.web.logistica.command.MantenimientoProveedoresCommand;

public class MantenimientoProveedoresFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MantenimientoProveedoresFormController.class);
 	DetalleManager detalleManager;
    
    public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}
    public MantenimientoProveedoresFormController() {    	
        super();        
        setCommandClass(MantenimientoProveedoresCommand.class);        
    }

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");   	
    	MantenimientoProveedoresCommand command = new MantenimientoProveedoresCommand();
    	command.setCodAlumno((String)request.getParameter("txhCodAlumno"));
    	TipoLogistica obj = new TipoLogistica();
    	
    //	command.setCodEstado("0001");
    	if(!(command.getCodAlumno()==null)){
          	if(!command.getCodAlumno().trim().equals("")){
          		command.setCodEstado("0001");
          		}
          	}
    	command.setListProveedor(this.detalleManager.GetAllProveedor(""
    			, command.getRuc(),command.getRazonSocial(),command.getCodCalif(),command.getCodEstado(),
    		
    			command.getCodProv()));
    	//System.out.println("tot: "+command.getListProveedor());
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_CALIFICACION);
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);  
    	command.setListCalificacion(this.detalleManager.GetAllTablaDetalle(obj));
    	
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_ESTADO);
    	obj.setTipo(CommonConstants.DELETE_MTO_BIB);
    	command.setListEstado(this.detalleManager.GetAllTablaDetalle(obj));
    	
    	obj.setCodTabla(CommonConstants.TIPT_TIPO_PROVEEDORES);
    	obj.setTipo(CommonConstants.TIPO_ORDEN_DSC);
    	command.setListprov(this.detalleManager.GetAllTablaDetalle(obj));
    
    	request.getSession().setAttribute("listProveedor", command.getListProveedor());
    	request.getSession().setAttribute("listprov", command.getListprov());
    	request.getSession().setAttribute("listCalificacion", command.getListCalificacion());
    	request.getSession().setAttribute("listEstado", command.getListEstado());
    	log.info("formBackingObject:FIN");
        return command;
    }  
        protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
        public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
		String resultado = "";
    	MantenimientoProveedoresCommand control = (MantenimientoProveedoresCommand) command;
    	if("BUSCAR".equals(control.getOperacion())){
    		control.setListProveedor(this.detalleManager.GetAllProveedor(""
        			,control.getRuc(),control.getRazonSocial(),control.getCodCalif()
        			,control.getCodEstado(),control.getCodProv()));
        	
        	request.getSession().setAttribute("listProveedor", control.getListProveedor());
    	}
    	else if("ELIMINAR".equals(control.getOperacion())){
    		resultado = eliminar(control);
    		if ( resultado.equals("-1")) {
    			control.setMsg("ERROR");
    			}
    		else if ( resultado.equals("-2")) {
    			control.setMsg("DOBLE");
    		}
    		else {
    			control.setMsg("OK");
    			}
    		control.setListProveedor(this.detalleManager.GetAllProveedor(""
        			,control.getRuc(),control.getRazonSocial(),control.getCodCalif()
        			,control.getCodEstado(),control.getCodProv()));
        	
        	request.getSession().setAttribute("listProveedor", control.getListProveedor());
    	}
    	
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/logistica/mantenimiento/log_Mantenimiento_Proveedores","control",control);		
    }
        private String eliminar(MantenimientoProveedoresCommand control){
        	
        	control.setEliminar(this.detalleManager.DeleteProveedor(control.getCodUnico(), control.getCodAlumno()));
        	
        	return control.getEliminar();
        }
        
}
