package com.tecsup.SGA.web.logistica.controller;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.Fecha;
import com.tecsup.SGA.modelo.DetalleDocPago;
import com.tecsup.SGA.modelo.DetalleOrden;
import com.tecsup.SGA.modelo.DocPago;
import com.tecsup.SGA.modelo.OrdenesAprobadas;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
import com.tecsup.SGA.service.logistica.GestionOrdenesManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.logistica.command.GestDocConsultarDocumentosPagosAsociadosIframeCommand;

public class GestDocConsultarDocumentosPagosAsociadosIframeFormController extends SimpleFormController{
	
	private static Log log = LogFactory.getLog(GestDocConsultarDocumentosPagosAsociadosIframeFormController.class);
	TablaDetalleManager tablaDetalleManager;
	SeguridadManager seguridadManager;
	DetalleManager detalleManager;
	GestionDocumentosManager gestionDocumentosManager;
		
	public void setGestionOrdenesManager(GestionOrdenesManager getionOrdenesManager) {
		this.gestionOrdenesManager = getionOrdenesManager;
	}

	GestionOrdenesManager gestionOrdenesManager;
	
	public GestionDocumentosManager getGestionDocumentosManager() {
		return gestionDocumentosManager;
	}

	public void setGestionDocumentosManager(
			GestionDocumentosManager gestionDocumentosManager) {
		this.gestionDocumentosManager = gestionDocumentosManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    log.info("formBackingObject:INI");
    
    GestDocConsultarDocumentosPagosAsociadosIframeCommand command= new GestDocConsultarDocumentosPagosAsociadosIframeCommand();
    
    command.setCodSede(request.getParameter("txhCodSede"));
    command.setCodOrden(request.getParameter("txhCodOrden"));
    command.setCodDocumento(request.getParameter("txhCodDocumento"));
    command.setCodUsuario(request.getParameter("txhCodUsuario"));
    command.setIndImportacion(request.getParameter("txhIndImportacion"));
    //ALQD,27/05/09.RECIBIENDO NUEVO PARAMETRO IND_AFECTOIGV
    command.setIndAfectoIGV(request.getParameter("txhIndAfectoIGV"));
    
    List lista = new ArrayList();
    lista = this.tablaDetalleManager.getAllTablaDetalle(CommonConstants.TIPT_PARAMETROS_GENERALES 
			, CommonConstants.TTDE_IGV_ID, "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
    
    Fecha fecha=new Fecha();
	command.setFechaBD(fecha.getFechaActual());
	System.out.println("FechaBD: "+command.getFechaBD());
    
    TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
    if(lista!=null && lista.size()>0)
    {   tipoTablaDetalle = (TipoTablaDetalle)lista.get(0);
	    //ALQD,27/05/09.SI ES AFECTO SE GUARDA EL IGV SINO 0
    	if(command.getIndAfectoIGV()!=null){
    	if(command.getIndAfectoIGV().equals("1")) {
    		
    		//TODO:
    		//recuperar la orden de compra (para obtener la fecha de aprobacion)
    		List<DetalleOrden> lstOrden = gestionOrdenesManager.GetAllOrdenById(command.getCodOrden());	
    		if(lstOrden.size()>0){
        		//si la fec. de aprobacion <= 28/02/11    			
    			DetalleOrden oOrden = (DetalleOrden) lstOrden.get(0);
    			String fecAOrden = oOrden.getFechaAprobacion();
    			SimpleDateFormat dFechaOrden = new SimpleDateFormat("dd/MM/yyyy");
    			SimpleDateFormat dFecLimite = new SimpleDateFormat("dd/MM/yyyy");
    			
    			Date fechaO = null;
    			Date oFechaLim = null;
    			try {
    				fechaO = dFechaOrden.parse(fecAOrden);    				        		
        			oFechaLim = dFecLimite.parse("01/03/2011");
    			}catch(ParseException ex){
    				ex.printStackTrace();
    			}
    			    			    			    			    			 
    			if(fechaO.before(oFechaLim)){
    				String igvOrden = oOrden.getIgv();
    				String netOrden = oOrden.getSubTotal1();
    				String igv = String.valueOf(Float.valueOf(igvOrden) / Float.valueOf(netOrden));
    				DecimalFormat df = new DecimalFormat("#.####");
    				
    				DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
    				dfs.setDecimalSeparator('.');
    				df.setDecimalFormatSymbols(dfs);    				
    				igv = df.format(Double.valueOf(igv));
    				command.setIgvDB(igv);
    			}else{
    				command.setIgvDB(String.valueOf(Float.valueOf(tipoTablaDetalle.getDscValor1().trim())/100));    			
    			}
    			
    		}else{
    			command.setIgvDB(String.valueOf(Float.valueOf(tipoTablaDetalle.getDscValor1().trim())/100));
    		}
    		
	    	
	    }else{
	    	command.setIgvDB(String.valueOf(0));
	    }
    	}
    }
        
    
    BuscarDatos(command);
    BuscarBandeja(command);
       
    log.info("formBackingObject:FIN");
	
  	return command;
   }
 	
 	protected void initBinder(HttpServletRequest request,
           ServletRequestDataBinder binder) {
   	NumberFormat nf = NumberFormat.getNumberInstance();
   	binder.registerCustomEditor(Long.class,
 	                  new CustomNumberEditor(Long.class, nf, true));
   	
   }
   /**
    * Redirect to the successView when the cancel button has been pressed.
    */
   public ModelAndView processFormSubmission(HttpServletRequest request,
                                             HttpServletResponse response,
                                             Object command,
                                             BindException errors)
   throws Exception {
   	return super.processFormSubmission(request, response, command, errors);
   }
 	
   public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
           BindException errors)
   		throws Exception {
   	log.info("onSubmit:INI");
   	
   	GestDocConsultarDocumentosPagosAsociadosIframeCommand control= (GestDocConsultarDocumentosPagosAsociadosIframeCommand) command;
   	String resultado="";
   	System.out.println("Operacion: "+control.getOperacion());
	if(control.getOperacion().equals("GRABAR"))
	{
		resultado=Guardar(control);
		if(resultado.equals("0")) control.setMsg("OK");
		else{ if(resultado.equals("-1")) control.setMsg("ERROR");
			   else{ if(resultado.equals("-2")) control.setMsg("FAC_REG");
			   		 else{ if(resultado.equals("-3")) control.setMsg("NRO_GUIA");
			   		 	   else{ if(resultado.equals("-4")) control.setMsg("CANT_FACTURADA");
			   		 		     
			   		 	   }
			   		 }
			   }
		   
		}
	   BuscarBandeja(control);
	}	
   	
   	log.info("onSubmit:FIN");		
    return new ModelAndView("/logistica/GestionDocumentos/Gest_Doc_Consultar_Documentos_Pagos_Asociados_iframe","control",control);
}

   public void BuscarDatos(GestDocConsultarDocumentosPagosAsociadosIframeCommand control){
	   List lista=new ArrayList();
	   System.out.println("Here CodOrden: "+control.getCodOrden()+">>CodDocumento<<"+control.getCodDocumento());
	   lista=this.gestionDocumentosManager.GetAllDocPago(control.getCodOrden(), control.getCodDocumento());
	   if(lista!=null)
	   {  if(lista.size()>0)
	   	  { DocPago docPago= new DocPago();
	   	  	docPago=(DocPago)lista.get(0);
		    control.setNroFactura(docPago.getFactura());
		    control.setFecInicio1(docPago.getFecEmiFactura());
		    control.setFecInicio2(docPago.getFecVctoFactura());
		    control.setNroGuia(docPago.getNroGuia());
		    control.setFecFinal1(docPago.getFecEmiOrden());
		    control.setFecFinal2(docPago.getFecGuiaRecep());	    
	   	  }
		   control.setListaBandeja(lista);
	   }
	   else control.setListaBandeja(new ArrayList());
   }
   
   public void BuscarBandeja(GestDocConsultarDocumentosPagosAsociadosIframeCommand control){
	   List lista=new ArrayList();
	   System.out.println("There CodOrden: "+control.getCodOrden());
	   lista=this.gestionDocumentosManager.GetAllDetDocPagosAsociados(control.getCodOrden());
	   if(lista!=null){
		   if(lista.size()>0){
			   control.setListaBandejaMedio(lista);
		       control.setLongitud(String.valueOf(lista.size()));
		       float producto=0;
		       float precioUni=0;
		       float cantidad=0;
		       float precioTotal=0;
		       for(int i=0;i<lista.size();i++){
		    	   DetalleDocPago curso = new DetalleDocPago();
		           curso=(DetalleDocPago)lista.get(i);
		           precioUni=Float.valueOf(curso.getPreUni().trim());
		           cantidad=Float.valueOf(curso.getCantidad().trim());
		           producto=precioUni*cantidad;
		           precioTotal=precioTotal+producto;
	           }
		       float igv=Float.valueOf(control.getIgvDB())*precioTotal;
		       //MODIFICADO RNAPA 21/08/2008
		       if(control.getIndImportacion().equals("1")){
		    	   control.setIgv("0.00");
			       control.setSubTotal(String.valueOf(precioTotal));
			       control.setTotal(String.valueOf(precioTotal));
		       }
		       else{
			       control.setIgv(String.valueOf(igv));
			       control.setSubTotal(String.valueOf(precioTotal));
			       control.setTotal(String.valueOf(precioTotal+igv));
		       }
		   }
	   }
	   else control.setListaBandejaMedio(new ArrayList());
   }
   
   public String Guardar(GestDocConsultarDocumentosPagosAsociadosIframeCommand control)
   {	String resultado="";
   		System.out.println(">>CodOrden<<"+control.getCodOrden()+">>NroFactura<<"+ 
	    		control.getNroFactura()+">>FecInicio1<<"+control.getFecInicio1()+">>FecInicio2<<"+
	    		control.getFecInicio2()+">>NroGuia<<"+control.getNroGuia()+">>FecFinal1<<"+
	    		control.getFecFinal1()+">>CadCantidades<<"+control.getCadCantidades() 
	    		+">>CadObservaciones<<"+control.getCadObservaciones()+">>CodUsuario<<"+
	    		control.getCodUsuario()+">>FecFinal2<<"+control.getFecFinal2()); 
      resultado=this.gestionDocumentosManager.UpdateDocumentoPago(control.getCodOrden(), 
	    		control.getNroFactura(), control.getFecInicio1(), control.getFecInicio2()
	    		, control.getNroGuia(), control.getFecFinal1(), control.getCadCantidades(), 
	    		control.getCadObservaciones(), control.getCodUsuario(), control.getFecFinal2());
	   
     System.out.println("Resultado :D : "+resultado); 
      return resultado; 
   }
   
public TablaDetalleManager getTablaDetalleManager() {
	return tablaDetalleManager;
}

public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
	this.tablaDetalleManager = tablaDetalleManager;
}

public SeguridadManager getSeguridadManager() {
	return seguridadManager;
}

public void setSeguridadManager(SeguridadManager seguridadManager) {
	this.seguridadManager = seguridadManager;
}

public DetalleManager getDetalleManager() {
	return detalleManager;
}

public void setDetalleManager(DetalleManager detalleManager) {
	this.detalleManager = detalleManager;
}
}
