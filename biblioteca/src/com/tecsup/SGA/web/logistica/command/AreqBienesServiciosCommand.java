package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class AreqBienesServiciosCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codUsuarioResponsable;
	private String cadena;
	private String estado;
	
	private List listaRequerimiento;
	private String cboTipoRequerimiento;
	//*******************************************
	private List listaCentroCosto;
	private String cboTipoCentroCosto;
	//*******************************************
	private List listaServicio;
	private String cboTipoServicio;
	//*******************************************
	private List listaGasto;
	private String cboTipoGasto;
	//*******************************************
	//valores de cabecera
	private String nroRequerimiento;
	private String fechaRequerimiento;
	private String usuSolicitante;	
	//*******************************************
	private List listaSolicitudDetalle;
	private List listaCodTipoBien;
	//*******************************************
	//para trabajar con la base de datos
	private String codSede;
	private String codRequerimiento;
	private String codTipoRequerimiento;
	private String codSubTipoRequerimiento;	
	private String codTipoCentroCosto;
	//bandeja de servicios se mandan valores
	private String descripcion;
	private String nombreServicio;
	private String fechaAtencion;
	//codigo del detalle de requerimento caso sewrvicio
	private String codReqDetalleServicio;	
	private String codDetRequerimiento;
	//*************************************************
	private String indicadorGrupo;
	private String banderaEnviar;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	private String usuAsignado;
	private String codPerfil;
	private String jefeLog;
	//ALQD,31/10/108. NUEVAS PROPIEDAS
	private String indInversion;
	private String indParaStock;
	private String indEmpLogistica;
	
	public String getIndInversion() {
		return indInversion;
	}
	public void setIndInversion(String indInversion) {
		this.indInversion = indInversion;
	}
	public String getIndParaStock() {
		return indParaStock;
	}
	public void setIndParaStock(String indParaStock) {
		this.indParaStock=indParaStock;
	}
	public String getIndEmpLogistica() {
		return indEmpLogistica;
	}
	public void setIndEmpLogistica(String indEmpLogistica) {
		this.indEmpLogistica=indEmpLogistica;
	}
	
	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}

	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}

	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}

	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}

	public String getIndicadorGrupo() {
		return indicadorGrupo;
	}

	public void setIndicadorGrupo(String indicadorGrupo) {
		this.indicadorGrupo = indicadorGrupo;
	}

	public String getCodDetRequerimiento() {
		return codDetRequerimiento;
	}

	public void setCodDetRequerimiento(String codDetRequerimiento) {
		this.codDetRequerimiento = codDetRequerimiento;
	}

	public String getCodReqDetalleServicio() {
		return codReqDetalleServicio;
	}

	public void setCodReqDetalleServicio(String codReqDetalleServicio) {
		this.codReqDetalleServicio = codReqDetalleServicio;
	}
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}

	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}

	public String getCodSubTipoRequerimiento() {
		return codSubTipoRequerimiento;
	}

	public void setCodSubTipoRequerimiento(String codSubTipoRequerimiento) {
		this.codSubTipoRequerimiento = codSubTipoRequerimiento;
	}

	public List getListaCodTipoBien() {
		return listaCodTipoBien;
	}

	public void setListaCodTipoBien(List listaCodTipoBien) {
		this.listaCodTipoBien = listaCodTipoBien;
	}

	public List getListaSolicitudDetalle() {
		return listaSolicitudDetalle;
	}

	public void setListaSolicitudDetalle(List listaSolicitudDetalle) {
		this.listaSolicitudDetalle = listaSolicitudDetalle;
	}

	public String getUsuSolicitante() {
		return usuSolicitante;
	}

	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}

	public String getFechaRequerimiento() {
		return fechaRequerimiento;
	}

	public void setFechaRequerimiento(String fechaRequerimiento) {
		this.fechaRequerimiento = fechaRequerimiento;
	}

	public String getNroRequerimiento() {
		return nroRequerimiento;
	}

	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}

	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public List getListaCentroCosto() {
		return listaCentroCosto;
	}

	public void setListaCentroCosto(List listaCentroCosto) {
		this.listaCentroCosto = listaCentroCosto;
	}
	
	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List getListaRequerimiento() {
		return listaRequerimiento;
	}

	public void setListaRequerimiento(List listaRequerimiento) {
		this.listaRequerimiento = listaRequerimiento;
	}

	public String getCboTipoCentroCosto() {
		return cboTipoCentroCosto;
	}

	public void setCboTipoCentroCosto(String cboTipoCentroCosto) {
		this.cboTipoCentroCosto = cboTipoCentroCosto;
	}

	public List getListaServicio() {
		return listaServicio;
	}

	public void setListaServicio(List listaServicio) {
		this.listaServicio = listaServicio;
	}

	public String getCboTipoServicio() {
		return cboTipoServicio;
	}

	public void setCboTipoServicio(String cboTipoServicio) {
		this.cboTipoServicio = cboTipoServicio;
	}
	
	public String getCboTipoRequerimiento() {
		return cboTipoRequerimiento;
	}

	public void setCboTipoRequerimiento(String cboTipoRequerimiento) {
		this.cboTipoRequerimiento = cboTipoRequerimiento;
	}

	public String getNombreServicio() {
		return nombreServicio;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public String getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(String fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodTipoCentroCosto() {
		return codTipoCentroCosto;
	}

	public void setCodTipoCentroCosto(String codTipoCentroCosto) {
		this.codTipoCentroCosto = codTipoCentroCosto;
	}

	public List getListaGasto() {
		return listaGasto;
	}

	public void setListaGasto(List listaGasto) {
		this.listaGasto = listaGasto;
	}

	public String getCboTipoGasto() {
		return cboTipoGasto;
	}

	public void setCboTipoGasto(String cboTipoGasto) {
		this.cboTipoGasto = cboTipoGasto;
	}

	public String getCadena() {
		return cadena;
	}

	public void setCadena(String cadena) {
		this.cadena = cadena;
	}

	public String getCodUsuarioResponsable() {
		return codUsuarioResponsable;
	}

	public void setCodUsuarioResponsable(String codUsuarioResponsable) {
		this.codUsuarioResponsable = codUsuarioResponsable;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getBanderaEnviar() {
		return banderaEnviar;
	}

	public void setBanderaEnviar(String banderaEnviar) {
		this.banderaEnviar = banderaEnviar;
	}

	public String getUsuAsignado() {
		return usuAsignado;
	}

	public void setUsuAsignado(String usuAsignado) {
		this.usuAsignado = usuAsignado;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getJefeLog() {
		return jefeLog;
	}

	public void setJefeLog(String jefeLog) {
		this.jefeLog = jefeLog;
	}
		
}
