package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ReportesConsultaBandejaCommand {

	private String codUsuario;
	private String operacion;
	private String msg;
	private String codSede;
	private String codOpciones;
	private String codTipoReporte;
	private List listaReportes;
	private String codReporte1;
	private String codReporte2;
	private String codReporte3;
	private String codReporte4;
	private String codReporte5;
	private String codReporte6;
	private String codReporte7;
	private String codReporte8;
	private String codReporte9;
	private String codReporte10;
	private String codReporte11;
	private String codReporte29;
	private String dscUsuario;
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodTipoReporte() {
		return codTipoReporte;
	}
	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}
	public List getListaReportes() {
		return listaReportes;
	}
	public void setListaReportes(List listaReportes) {
		this.listaReportes = listaReportes;
	}
	public String getCodReporte1() {
		return codReporte1;
	}
	public void setCodReporte1(String codReporte1) {
		this.codReporte1 = codReporte1;
	}
	public String getCodReporte2() {
		return codReporte2;
	}
	public void setCodReporte2(String codReporte2) {
		this.codReporte2 = codReporte2;
	}
	public String getCodReporte3() {
		return codReporte3;
	}
	public void setCodReporte3(String codReporte3) {
		this.codReporte3 = codReporte3;
	}
	public String getCodReporte4() {
		return codReporte4;
	}
	public void setCodReporte4(String codReporte4) {
		this.codReporte4 = codReporte4;
	}
	public String getCodReporte5() {
		return codReporte5;
	}
	public void setCodReporte5(String codReporte5) {
		this.codReporte5 = codReporte5;
	}
	public String getCodReporte6() {
		return codReporte6;
	}
	public void setCodReporte6(String codReporte6) {
		this.codReporte6 = codReporte6;
	}
	public String getCodReporte7() {
		return codReporte7;
	}
	public void setCodReporte7(String codReporte7) {
		this.codReporte7 = codReporte7;
	}
	public String getCodReporte8() {
		return codReporte8;
	}
	public void setCodReporte8(String codReporte8) {
		this.codReporte8 = codReporte8;
	}
	public String getCodReporte9() {
		return codReporte9;
	}
	public void setCodReporte9(String codReporte9) {
		this.codReporte9 = codReporte9;
	}
	public String getCodReporte10() {
		return codReporte10;
	}
	public void setCodReporte10(String codReporte10) {
		this.codReporte10 = codReporte10;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	public String getCodReporte11() {
		return codReporte11;
	}
	public void setCodReporte11(String codReporte11) {
		this.codReporte11 = codReporte11;
	}
	public String getCodReporte29() {
		return codReporte29;
	}
	public void setCodReporte29(String codReporte29) {
		this.codReporte29 = codReporte29;
	}
	
	
}
