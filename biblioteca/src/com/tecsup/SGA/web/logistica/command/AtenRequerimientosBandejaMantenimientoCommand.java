package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class AtenRequerimientosBandejaMantenimientoCommand {

	private String operacion;
	private String msg;
	private String codSede;
	private String codUsuario;
	private String codPerfil;
	private String codOpciones;
	private String codRequerimiento;	

	private String nroRequerimiento;
	private String codServicio;
	private List listaServicio;
	private String codCentroCosto;
	private List listaCentroCosto;
	private String usuSolicitante;

	private List listaRequerimientos;
	private String tamListaRequerimientos;
	//ALQD,17/04/09. NUEVAS PROPIEDADES PARA FILTRO POR ESTADO
	private List listaEstado;
	private String codEstado;

	public List getListaEstado() {
		return listaEstado;
	}
	public void setListaEstado(List listaEstado) {
		this.listaEstado=listaEstado;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado=codEstado;
	}
	public String getTamListaRequerimientos() {
		return tamListaRequerimientos;
	}

	public void setTamListaRequerimientos(String tamListaRequerimientos) {
		this.tamListaRequerimientos = tamListaRequerimientos;
	}

	public List getListaRequerimientos() {
		return listaRequerimientos;
	}

	public void setListaRequerimientos(List listaRequerimientos) {
		this.listaRequerimientos = listaRequerimientos;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getUsuSolicitante() {
		return usuSolicitante;
	}

	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}

	public List getListaCentroCosto() {
		return listaCentroCosto;
	}

	public void setListaCentroCosto(List listaCentroCosto) {
		this.listaCentroCosto = listaCentroCosto;
	}

	public List getListaServicio() {
		return listaServicio;
	}

	public void setListaServicio(List listaServicio) {
		this.listaServicio = listaServicio;
	}

	public String getCodServicio() {
		return codServicio;
	}

	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}

	public String getCodCentroCosto() {
		return codCentroCosto;
	}

	public void setCodCentroCosto(String codCentroCosto) {
		this.codCentroCosto = codCentroCosto;
	}

	public String getNroRequerimiento() {
		return nroRequerimiento;
	}

	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}

	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
}
