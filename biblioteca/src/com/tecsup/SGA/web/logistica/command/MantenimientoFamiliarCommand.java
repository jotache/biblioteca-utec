package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoFamiliarCommand {
	private String operacion;
	private List tipoBien;
	private String codTipo;
	private List listFamilia;
	private String codigo;
	private String familia;
	private String tipB;
	private String ctaBle;
	private String secuencial;
	private String msg;
	private String elimino;
	private String codAlumno;
	private String codPadre;
	
	public String getCodPadre() {
		return codPadre;
	}
	public void setCodPadre(String codPadre) {
		this.codPadre = codPadre;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getElimino() {
		return elimino;
	}
	public void setElimino(String elimino) {
		this.elimino = elimino;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getFamilia() {
		return familia;
	}
	public void setFamilia(String familia) {
		this.familia = familia;
	}
	public String getTipB() {
		return tipB;
	}
	public void setTipB(String tipB) {
		this.tipB = tipB;
	}
	public String getCtaBle() {
		return ctaBle;
	}
	public void setCtaBle(String ctaBle) {
		this.ctaBle = ctaBle;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(List tipoBien) {
		this.tipoBien = tipoBien;
	}
	public List getListFamilia() {
		return listFamilia;
	}
	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}
	
}
