package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarSubFamiliaCommand {
	private String operacion;
	private String codigo;
	private String subfamilia;
	private List listFamilia;
	private String codFam;
	private String codAlumno;
	private String msg;
	private String tipo;
	private String grabar;
	private String codBien;
	private String secuencial;
	
	
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getCodFam() {
		return codFam;
	}
	public void setCodFam(String codFam) {
		this.codFam = codFam;
	}
	public List getListFamilia() {
		return listFamilia;
	}
	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getSubfamilia() {
		return subfamilia;
	}
	public void setSubfamilia(String subfamilia) {
		this.subfamilia = subfamilia;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
