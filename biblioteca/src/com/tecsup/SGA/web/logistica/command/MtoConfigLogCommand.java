package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class MtoConfigLogCommand {
	private long idOrden;
	private String descripcion;
	private String operacion;
	private String cboOrden;
	private List MtoConfInicioList;
	private String IdRec;
	private String lstrURL;
	private String codAlumno;
	private String codSede;
	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public long getIdOrden() {
		return idOrden;
	}
	public void setIdOrden(long idOrden) {
		this.idOrden = idOrden;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCboOrden() {
		return cboOrden;
	}
	public void setCboOrden(String cboOrden) {
		this.cboOrden = cboOrden;
	}
	public List getMtoConfInicioList() {
		return MtoConfInicioList;
	}
	public void setMtoConfInicioList(List mtoConfInicioList) {
		MtoConfInicioList = mtoConfInicioList;
	}
	public String getIdRec() {
		return IdRec;
	}
	public void setIdRec(String idRec) {
		IdRec = idRec;
	}
	public String getLstrURL() {
		return lstrURL;
	}
	public void setLstrURL(String lstrURL) {
		this.lstrURL = lstrURL;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}	
}
