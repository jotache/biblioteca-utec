package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class CotSolVerCotizacionFormController {
	private String operacion;
	private String usuario;
	private List lstResultado;
	private String txhCodCotizacion;
	private String txhCodSede;
	private String txhCodEstCotizacion;
	
	private List listaProveedores;
	private String flgComprador;
	private String codTipoReq;
	
	private String cadPrecios;
	
	/*ccordova - 20080825*/
	private String fecInicio;
	private String fecFin;
	private String horaInicio;
	private String horaFin;
	//ALQD,04/02/09. A�ADIENDO NUEVA PROPIEDAD
	private String numCotizacion;
	//ALQD,15/04/09. NUEVA PROPIEDAD, LISTA DE UNIDADES DE MEDIDA
	private List lstUniMedida;
	private String codGruServicio;

	public List getLstUniMedida() {
		return lstUniMedida;
	}
	public void setLstUniMedida(List lstUniMedida) {
		this.lstUniMedida = lstUniMedida;
	}
	public String getCodGruServicio() {
		return codGruServicio;
	}
	public void setCodGruServicio(String codGruServicio) {
		this.codGruServicio = codGruServicio;
	}
	public String getNumCotizacion() {
		return numCotizacion;
	}
	public void setNumCotizacion(String numCotizacion) {
		this.numCotizacion=numCotizacion;
	}
	public String getCodTipoReq() {
		return codTipoReq;
	}

	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}

	public String getTxhCodEstCotizacion() {
		return txhCodEstCotizacion;
	}

	public void setTxhCodEstCotizacion(String txhCodEstCotizacion) {
		this.txhCodEstCotizacion = txhCodEstCotizacion;
	}

	public String getTxhCodSede() {
		return txhCodSede;
	}

	public void setTxhCodSede(String txhCodSede) {
		this.txhCodSede = txhCodSede;
	}

	public String getTxhCodCotizacion() {
		return txhCodCotizacion;
	}

	public void setTxhCodCotizacion(String txhCodCotizacion) {
		this.txhCodCotizacion = txhCodCotizacion;
	}

	public List getLstResultado() {
		return lstResultado;
	}

	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public List getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public String getFlgComprador() {
		return flgComprador;
	}

	public void setFlgComprador(String flgComprador) {
		this.flgComprador = flgComprador;
	}
	
	public String getCadPrecios() {
		return cadPrecios;
	}

	public void setCadPrecios(String cadPrecios) {
		this.cadPrecios = cadPrecios;
	}

	public String getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}

	public String getFecFin() {
		return fecFin;
	}

	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

}
