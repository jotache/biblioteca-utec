package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoEvaProveedorCommand {
	private String operacion;
	private List listCalificacion;
	private String codCalif;
	private String secuencial;
	private String desc;
	private String eliminar;
	private String msg;
	private String codAlumno;
	private int total;
	private int pos;
	private String graba;
	private String cadDetalle;
	private String cadIndicador;
	private String men;
	
	public String getMen() {
		return men;
	}
	public void setMen(String men) {
		this.men = men;
	}
	public String getCadDetalle() {
		return cadDetalle;
	}
	public void setCadDetalle(String cadDetalle) {
		this.cadDetalle = cadDetalle;
	}
	public String getCadIndicador() {
		return cadIndicador;
	}
	public void setCadIndicador(String cadIndicador) {
		this.cadIndicador = cadIndicador;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListCalificacion() {
		return listCalificacion;
	}
	public void setListCalificacion(List listCalificacion) {
		this.listCalificacion = listCalificacion;
	}
	public String getCodCalif() {
		return codCalif;
	}
	public void setCodCalif(String codCalif) {
		this.codCalif = codCalif;
	}
	public String getEliminar() {
		return eliminar;
	}
	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
}
