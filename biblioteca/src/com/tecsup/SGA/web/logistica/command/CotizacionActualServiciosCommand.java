package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class CotizacionActualServiciosCommand {
	private String operacion;
	private String msg;
	private String seleccion;
	private String codUsuario;
	private String codSede;
	private String codCotizacion;	
	private String codDetalle;
	
	private String codTipoCotizacion;
	private String codSubTipoCotizacion;
	
	private List listaPago;
	private String cboTipoPago;
	
	private String fechaVigenciaIni;
	private String fechaVigenciaFin;
	private String horaVigenciaIni;
	private String horaVigenciaFin;	
	
	private List listaCotizacion;
	//usados para grabar
	private String cadenaDescripcion;
	private String cadenaCodDetalle;
	private String cantidad;
	//*****************************
	private String consteCajaChica;
	private String consteOrdenCompra;
	
	private String fechaVigenciaIniBD;
	private String fechaVigenciaFinBD;
	private String horaVigenciaIniBD;
	private String horaVigenciaFinBD;
	private String fechaVigenciaIniBD2;
	private String horaVigenciaIniBD2;
	private String flag;
	//ALQD,13/08/09.NUEVA PROPIEDAD
	private String indInversion;
	
	public String getIndInversion() {
		return indInversion;
	}
	public void setIndInversion(String indInversion) {
		this.indInversion=indInversion;
	}
	public String getConsteCajaChica() {
		return consteCajaChica;
	}
	public void setConsteCajaChica(String consteCajaChica) {
		this.consteCajaChica = consteCajaChica;
	}
	public String getConsteOrdenCompra() {
		return consteOrdenCompra;
	}
	public void setConsteOrdenCompra(String consteOrdenCompra) {
		this.consteOrdenCompra = consteOrdenCompra;
	}
	public String getFechaVigenciaIniBD() {
		return fechaVigenciaIniBD;
	}
	public void setFechaVigenciaIniBD(String fechaVigenciaIniBD) {
		this.fechaVigenciaIniBD = fechaVigenciaIniBD;
	}
	public String getFechaVigenciaFinBD() {
		return fechaVigenciaFinBD;
	}
	public void setFechaVigenciaFinBD(String fechaVigenciaFinBD) {
		this.fechaVigenciaFinBD = fechaVigenciaFinBD;
	}
	public String getHoraVigenciaIniBD() {
		return horaVigenciaIniBD;
	}
	public void setHoraVigenciaIniBD(String horaVigenciaIniBD) {
		this.horaVigenciaIniBD = horaVigenciaIniBD;
	}
	public String getHoraVigenciaFinBD() {
		return horaVigenciaFinBD;
	}
	public void setHoraVigenciaFinBD(String horaVigenciaFinBD) {
		this.horaVigenciaFinBD = horaVigenciaFinBD;
	}
	public String getFechaVigenciaIniBD2() {
		return fechaVigenciaIniBD2;
	}
	public void setFechaVigenciaIniBD2(String fechaVigenciaIniBD2) {
		this.fechaVigenciaIniBD2 = fechaVigenciaIniBD2;
	}
	public String getHoraVigenciaIniBD2() {
		return horaVigenciaIniBD2;
	}
	public void setHoraVigenciaIniBD2(String horaVigenciaIniBD2) {
		this.horaVigenciaIniBD2 = horaVigenciaIniBD2;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public List getListaCotizacion() {
		return listaCotizacion;
	}
	public void setListaCotizacion(List listaCotizacion) {
		this.listaCotizacion = listaCotizacion;
	}
	public String getHoraVigenciaIni() {
		return horaVigenciaIni;
	}
	public void setHoraVigenciaIni(String horaVigenciaIni) {
		this.horaVigenciaIni = horaVigenciaIni;
	}
	public String getHoraVigenciaFin() {
		return horaVigenciaFin;
	}
	public void setHoraVigenciaFin(String horaVigenciaFin) {
		this.horaVigenciaFin = horaVigenciaFin;
	}
	public String getFechaVigenciaIni() {
		return fechaVigenciaIni;
	}
	public void setFechaVigenciaIni(String fechaVigenciaIni) {
		this.fechaVigenciaIni = fechaVigenciaIni;
	}
	public String getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}
	public void setFechaVigenciaFin(String fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}
	public List getListaPago() {
		return listaPago;
	}
	public void setListaPago(List listaPago) {
		this.listaPago = listaPago;
	}
	public String getCboTipoPago() {
		return cboTipoPago;
	}
	public void setCboTipoPago(String cboTipoPago) {
		this.cboTipoPago = cboTipoPago;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCodTipoCotizacion() {
		return codTipoCotizacion;
	}
	public void setCodTipoCotizacion(String codTipoCotizacion) {
		this.codTipoCotizacion = codTipoCotizacion;
	}
	public String getCodSubTipoCotizacion() {
		return codSubTipoCotizacion;
	}
	public void setCodSubTipoCotizacion(String codSubTipoCotizacion) {
		this.codSubTipoCotizacion = codSubTipoCotizacion;
	}
	public String getCadenaDescripcion() {
		return cadenaDescripcion;
	}
	public void setCadenaDescripcion(String cadenaDescripcion) {
		this.cadenaDescripcion = cadenaDescripcion;
	}
	public String getCadenaCodDetalle() {
		return cadenaCodDetalle;
	}
	public void setCadenaCodDetalle(String cadenaCodDetalle) {
		this.cadenaCodDetalle = cadenaCodDetalle;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	
}
