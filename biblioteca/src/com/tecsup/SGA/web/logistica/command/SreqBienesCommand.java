package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class SreqBienesCommand {

	private String estado;
	private String operacion;
	private String msg;
	private String codUsuario;
	private String seleccion;
	private String fechaActual;
	
	private List listaRequerimiento;
	private String cboTipoRequerimiento;
	//*******************************************
	private List listaCentroCosto;
	private String cboTipoCentroCosto;
	//*******************************************
	private List listaServicio;
	private String cboTipoServicio;
	//*******************************************
	private List listaGasto;
	private String cboTipoGasto;
	//*******************************************
	//valores de cabecera
	private String nroRequerimiento;
	private String fechaRequerimiento;
	private String usuSolicitante;	
	//*******************************************
	private List listaSolicitudDetalle;
	private String tamListaSolicitudDetalle;
	private List listaCodTipoBien;
	//*******************************************
	private String codTipoBienConsumible;
	private String codTipoBienActivo;
	//*******************************************
	//para trabajar con la base de datos
	private String codSede;
	private String codRequerimiento;
	private String codTipoRequerimiento;
	private String codSubTipoRequerimiento;	
	private String codTipoCentroCosto;
	//bandeja de bienes se mandan cadenas
	private String cadenaCodigo;
	private String cadenaCodBien;
	private String cadenaCantidad;
	private String cadenaCodTipoGasto;
	private String cadenaFechaEntrega;
	private String cadenaDescripcion;
	//bandeja de servicios se mandan valores
	private String descripcion;
	private String nombreServicio;
	private String fechaAtencion;
	//codigo del detalle de requerimento caso sewrvicio
	private String codReqDetalleServicio;	
	private String codDetRequerimiento;
	private String codRequerimientoOri;
	private String nroAsociado;
	private String codTipoBien;
	
	private String indTitulo;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;

	private String consteCodBien;
	private String tipoProcedencia;
	private String msg2;
	
	private String indInversion;
	
	/*CCORDOVA TIPO GASTO PARA LOS SERVICION*/
	private String codTipoGastoServicio;
	private List listTipoGastoServicio;
	//ALQD,31/10/108. NUEVAS PROPIEDAS
	private String indParaStock;
	private String indEmpLogistica;
	//alqd,25/02/09. NUEVA PROPIEDAD
	private String nomResAtencion;
	
	public String getNomResAtencion() {
		return nomResAtencion;
	}
	public void setNomResAtencion(String nomResAtencion) {
		this.nomResAtencion=nomResAtencion;
	}
	public String getIndParaStock() {
		return indParaStock;
	}
	public void setIndParaStock(String indParaStock) {
		this.indParaStock=indParaStock;
	}
	public String getIndEmpLogistica() {
		return indEmpLogistica;
	}
	public void setIndEmpLogistica(String indEmpLogistica) {
		this.indEmpLogistica=indEmpLogistica;
	}
	public String getMsg2() {
		return msg2;
	}

	public void setMsg2(String msg2) {
		this.msg2 = msg2;
	}

	public String getTipoProcedencia() {
		return tipoProcedencia;
	}

	public void setTipoProcedencia(String tipoProcedencia) {
		this.tipoProcedencia = tipoProcedencia;
	}

	public String getConsteCodBien() {
		return consteCodBien;
	}

	public void setConsteCodBien(String consteCodBien) {
		this.consteCodBien = consteCodBien;
	}

	public String getIndTitulo() {
		return indTitulo;
	}

	public void setIndTitulo(String indTitulo) {
		this.indTitulo = indTitulo;
	}

	public String getNroAsociado() {
		return nroAsociado;
	}

	public void setNroAsociado(String nroAsociado) {
		this.nroAsociado = nroAsociado;
	}

	public String getCodRequerimientoOri() {
		return codRequerimientoOri;
	}

	public void setCodRequerimientoOri(String codRequerimientoOri) {
		this.codRequerimientoOri = codRequerimientoOri;
	}

	public String getCodDetRequerimiento() {
		return codDetRequerimiento;
	}

	public void setCodDetRequerimiento(String codDetRequerimiento) {
		this.codDetRequerimiento = codDetRequerimiento;
	}

	public String getCodReqDetalleServicio() {
		return codReqDetalleServicio;
	}

	public void setCodReqDetalleServicio(String codReqDetalleServicio) {
		this.codReqDetalleServicio = codReqDetalleServicio;
	}

	public String getCadenaCodBien() {
		return cadenaCodBien;
	}

	public void setCadenaCodBien(String cadenaCodBien) {
		this.cadenaCodBien = cadenaCodBien;
	}

	public String getCadenaCantidad() {
		return cadenaCantidad;
	}

	public void setCadenaCantidad(String cadenaCantidad) {
		this.cadenaCantidad = cadenaCantidad;
	}

	public String getCadenaCodTipoGasto() {
		return cadenaCodTipoGasto;
	}

	public void setCadenaCodTipoGasto(String cadenaCodTipoGasto) {
		this.cadenaCodTipoGasto = cadenaCodTipoGasto;
	}

	public String getCadenaFechaEntrega() {
		return cadenaFechaEntrega;
	}

	public void setCadenaFechaEntrega(String cadenaFechaEntrega) {
		this.cadenaFechaEntrega = cadenaFechaEntrega;
	}

	public String getCadenaDescripcion() {
		return cadenaDescripcion;
	}

	public void setCadenaDescripcion(String cadenaDescripcion) {
		this.cadenaDescripcion = cadenaDescripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}

	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}

	public String getCodSubTipoRequerimiento() {
		return codSubTipoRequerimiento;
	}

	public void setCodSubTipoRequerimiento(String codSubTipoRequerimiento) {
		this.codSubTipoRequerimiento = codSubTipoRequerimiento;
	}

	public List getListaCodTipoBien() {
		return listaCodTipoBien;
	}

	public void setListaCodTipoBien(List listaCodTipoBien) {
		this.listaCodTipoBien = listaCodTipoBien;
	}

	public List getListaSolicitudDetalle() {
		return listaSolicitudDetalle;
	}

	public void setListaSolicitudDetalle(List listaSolicitudDetalle) {
		this.listaSolicitudDetalle = listaSolicitudDetalle;
	}

	public String getUsuSolicitante() {
		return usuSolicitante;
	}

	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}

	public String getFechaRequerimiento() {
		return fechaRequerimiento;
	}

	public void setFechaRequerimiento(String fechaRequerimiento) {
		this.fechaRequerimiento = fechaRequerimiento;
	}

	public String getNroRequerimiento() {
		return nroRequerimiento;
	}

	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}

	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public List getListaCentroCosto() {
		return listaCentroCosto;
	}

	public void setListaCentroCosto(List listaCentroCosto) {
		this.listaCentroCosto = listaCentroCosto;
	}
	
	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List getListaRequerimiento() {
		return listaRequerimiento;
	}

	public void setListaRequerimiento(List listaRequerimiento) {
		this.listaRequerimiento = listaRequerimiento;
	}

	public String getCboTipoCentroCosto() {
		return cboTipoCentroCosto;
	}

	public void setCboTipoCentroCosto(String cboTipoCentroCosto) {
		this.cboTipoCentroCosto = cboTipoCentroCosto;
	}

	public List getListaServicio() {
		return listaServicio;
	}

	public void setListaServicio(List listaServicio) {
		this.listaServicio = listaServicio;
	}

	public String getCboTipoServicio() {
		return cboTipoServicio;
	}

	public void setCboTipoServicio(String cboTipoServicio) {
		this.cboTipoServicio = cboTipoServicio;
	}

	public String getCodTipoBienConsumible() {
		return codTipoBienConsumible;
	}

	public void setCodTipoBienConsumible(String codTipoBienConsumible) {
		this.codTipoBienConsumible = codTipoBienConsumible;
	}

	public String getCodTipoBienActivo() {
		return codTipoBienActivo;
	}

	public void setCodTipoBienActivo(String codTipoBienActivo) {
		this.codTipoBienActivo = codTipoBienActivo;
	}

	public String getCboTipoRequerimiento() {
		return cboTipoRequerimiento;
	}

	public void setCboTipoRequerimiento(String cboTipoRequerimiento) {
		this.cboTipoRequerimiento = cboTipoRequerimiento;
	}

	public String getNombreServicio() {
		return nombreServicio;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public String getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(String fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodTipoCentroCosto() {
		return codTipoCentroCosto;
	}

	public void setCodTipoCentroCosto(String codTipoCentroCosto) {
		this.codTipoCentroCosto = codTipoCentroCosto;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}

	public List getListaGasto() {
		return listaGasto;
	}

	public void setListaGasto(List listaGasto) {
		this.listaGasto = listaGasto;
	}

	public String getCboTipoGasto() {
		return cboTipoGasto;
	}

	public void setCboTipoGasto(String cboTipoGasto) {
		this.cboTipoGasto = cboTipoGasto;
	}

	public String getCadenaCodigo() {
		return cadenaCodigo;
	}

	public void setCadenaCodigo(String cadenaCodigo) {
		this.cadenaCodigo = cadenaCodigo;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	public String getTamListaSolicitudDetalle() {
		return tamListaSolicitudDetalle;
	}

	public void setTamListaSolicitudDetalle(String tamListaSolicitudDetalle) {
		this.tamListaSolicitudDetalle = tamListaSolicitudDetalle;
	}

	public String getCodTipoBien() {
		return codTipoBien;
	}

	public void setCodTipoBien(String codTipoBien) {
		this.codTipoBien = codTipoBien;
	}

	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}

	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}

	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}

	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}

	public String getCodTipoGastoServicio() {
		return codTipoGastoServicio;
	}

	public void setCodTipoGastoServicio(String codTipoGastoServicio) {
		this.codTipoGastoServicio = codTipoGastoServicio;
	}

	public List getListTipoGastoServicio() {
		return listTipoGastoServicio;
	}

	public void setListTipoGastoServicio(List listTipoGastoServicio) {
		this.listTipoGastoServicio = listTipoGastoServicio;
	}

	public String getIndInversion() {
		return indInversion;
	}

	public void setIndInversion(String indInversion) {
		this.indInversion = indInversion;
	}
	
}
