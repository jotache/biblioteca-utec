package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ReporteOrdenesAsignadasByProveedorCommand {

	private String codUsuario;
	private String operacion;
	private String consteRadio;
	private String consteBien;
	private String consteServicio;
	private String consteBienConsumible;
	private String consteBienActivo;
	private String consteGrupoGenerales;
	private String consteGrupoServicios;
	private String consteGrupoMantenimiento;
	private String msg;
	private String valorIndGrupo;
	private String codSede;
	private List listaSedes;
	private String codTipoRequerimiento;
	private List listCodTipoRequerimiento;
	private String fecInicio;
	private String fecFinal;
	private String codTipoMoneda;
	private List listCodTipoMoneda;
	private String proveedor;
	private String codOpciones;
	private String codTipoPago;
	private List listCodTipoPago;
	private String codTipoReporte;
	
	private String dscUsuario;
	private String codProveedor;
	private List listaProveedor;
	private String consteSede;
	private List listaCodGrupoServicio;
	private String codGrupoServicio;
	
	private List listaCodTipoBien;
	private String codTipoBien;
	
	public List getListaCodGrupoServicio() {
		return listaCodGrupoServicio;
	}
	public void setListaCodGrupoServicio(List listaCodGrupoServicio) {
		this.listaCodGrupoServicio = listaCodGrupoServicio;
	}
	public String getCodGrupoServicio() {
		return codGrupoServicio;
	}
	public void setCodGrupoServicio(String codGrupoServicio) {
		this.codGrupoServicio = codGrupoServicio;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public List getListaProveedor() {
		return listaProveedor;
	}
	public void setListaProveedor(List listaProveedor) {
		this.listaProveedor = listaProveedor;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	
	public String getCodTipoReporte() {
		return codTipoReporte;
	}
	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}
	public String getCodTipoPago() {
		return codTipoPago;
	}
	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	public List getListCodTipoPago() {
		return listCodTipoPago;
	}
	public void setListCodTipoPago(List listCodTipoPago) {
		this.listCodTipoPago = listCodTipoPago;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getConsteRadio() {
		return consteRadio;
	}
	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}
	public String getConsteBienConsumible() {
		return consteBienConsumible;
	}
	public void setConsteBienConsumible(String consteBienConsumible) {
		this.consteBienConsumible = consteBienConsumible;
	}
	public String getConsteBienActivo() {
		return consteBienActivo;
	}
	public void setConsteBienActivo(String consteBienActivo) {
		this.consteBienActivo = consteBienActivo;
	}
	public String getConsteGrupoGenerales() {
		return consteGrupoGenerales;
	}
	public void setConsteGrupoGenerales(String consteGrupoGenerales) {
		this.consteGrupoGenerales = consteGrupoGenerales;
	}
	public String getConsteGrupoServicios() {
		return consteGrupoServicios;
	}
	public void setConsteGrupoServicios(String consteGrupoServicios) {
		this.consteGrupoServicios = consteGrupoServicios;
	}
	public String getConsteGrupoMantenimiento() {
		return consteGrupoMantenimiento;
	}
	public void setConsteGrupoMantenimiento(String consteGrupoMantenimiento) {
		this.consteGrupoMantenimiento = consteGrupoMantenimiento;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getValorIndGrupo() {
		return valorIndGrupo;
	}
	public void setValorIndGrupo(String valorIndGrupo) {
		this.valorIndGrupo = valorIndGrupo;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListaSedes() {
		return listaSedes;
	}
	public void setListaSedes(List listaSedes) {
		this.listaSedes = listaSedes;
	}
	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}
	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}
	public List getListCodTipoRequerimiento() {
		return listCodTipoRequerimiento;
	}
	public void setListCodTipoRequerimiento(List listCodTipoRequerimiento) {
		this.listCodTipoRequerimiento = listCodTipoRequerimiento;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getCodTipoMoneda() {
		return codTipoMoneda;
	}
	public void setCodTipoMoneda(String codTipoMoneda) {
		this.codTipoMoneda = codTipoMoneda;
	}
	public List getListCodTipoMoneda() {
		return listCodTipoMoneda;
	}
	public void setListCodTipoMoneda(List listCodTipoMoneda) {
		this.listCodTipoMoneda = listCodTipoMoneda;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getConsteBien() {
		return consteBien;
	}
	public void setConsteBien(String consteBien) {
		this.consteBien = consteBien;
	}
	public String getConsteServicio() {
		return consteServicio;
	}
	public void setConsteServicio(String consteServicio) {
		this.consteServicio = consteServicio;
	}
	public String getConsteSede() {
		return consteSede;
	}
	public void setConsteSede(String consteSede) {
		this.consteSede = consteSede;
	}
	public List getListaCodTipoBien() {
		return listaCodTipoBien;
	}
	public void setListaCodTipoBien(List listaCodTipoBien) {
		this.listaCodTipoBien = listaCodTipoBien;
	}
	public String getCodTipoBien() {
		return codTipoBien;
	}
	public void setCodTipoBien(String codTipoBien) {
		this.codTipoBien = codTipoBien;
	}
	
	
}
