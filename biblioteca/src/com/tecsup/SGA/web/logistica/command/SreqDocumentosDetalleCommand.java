package com.tecsup.SGA.web.logistica.command;

import java.util.List;

import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
public class SreqDocumentosDetalleCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codRequerimiento;
	private String codDetRequerimiento;
	private String codDocAdjunto;
	//para ocultar el los botones
	private String condicion;
	private List<SolRequerimientoAdjunto> listaAdjuntos;
	private String documentDownload;
	
	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}

	public String getCodDetRequerimiento() {
		return codDetRequerimiento;
	}

	public void setCodDetRequerimiento(String codDetRequerimiento) {
		this.codDetRequerimiento = codDetRequerimiento;
	}

	public String getCodDocAdjunto() {
		return codDocAdjunto;
	}

	public void setCodDocAdjunto(String codDocAdjunto) {
		this.codDocAdjunto = codDocAdjunto;
	}

	public List<SolRequerimientoAdjunto> getListaAdjuntos() {
		return listaAdjuntos;
	}

	public void setListaAdjuntos(List<SolRequerimientoAdjunto> listaAdjuntos) {
		this.listaAdjuntos = listaAdjuntos;
	}

	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}

	public String getDocumentDownload() {
		return documentDownload;
	}

	public void setDocumentDownload(String documentDownload) {
		this.documentDownload = documentDownload;
	}
	
}
