package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class RecepcionBienesDevueltosCommand {
	private String operacion;
	private String sede;
	private String fechaDevolucionIni;
	private String fechaDevolucionFin;
	private String solicitante;
	private List listCentroCosto;
	private String codCentro;
	private List listEstado;
	private String codEstado;
	private List listBienesDevueltos; 
	private String codUsuario;
	private String codTipoBien;
	private String codRequerimiento;
	private String codDevolucion;
	
	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}

	public String getCodDevolucion() {
		return codDevolucion;
	}

	public void setCodDevolucion(String codDevolucion) {
		this.codDevolucion = codDevolucion;
	}

	public String getCodTipoBien() {
		return codTipoBien;
	}

	public void setCodTipoBien(String codTipoBien) {
		this.codTipoBien = codTipoBien;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getFechaDevolucionIni() {
		return fechaDevolucionIni;
	}

	public void setFechaDevolucionIni(String fechaDevolucionIni) {
		this.fechaDevolucionIni = fechaDevolucionIni;
	}

	public String getFechaDevolucionFin() {
		return fechaDevolucionFin;
	}

	public void setFechaDevolucionFin(String fechaDevolucionFin) {
		this.fechaDevolucionFin = fechaDevolucionFin;
	}

	public String getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	public List getListCentroCosto() {
		return listCentroCosto;
	}

	public void setListCentroCosto(List listCentroCosto) {
		this.listCentroCosto = listCentroCosto;
	}

	public String getCodCentro() {
		return codCentro;
	}

	public void setCodCentro(String codCentro) {
		this.codCentro = codCentro;
	}

	public List getListEstado() {
		return listEstado;
	}

	public void setListEstado(List listEstado) {
		this.listEstado = listEstado;
	}

	public String getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public List getListBienesDevueltos() {
		return listBienesDevueltos;
	}

	public void setListBienesDevueltos(List listBienesDevueltos) {
		this.listBienesDevueltos = listBienesDevueltos;
	}
	
}
