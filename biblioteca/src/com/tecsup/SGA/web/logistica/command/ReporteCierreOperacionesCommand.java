package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ReporteCierreOperacionesCommand {

	private String operacion;
	private String codUsuario;
	private String consteRadio;
	private String consteBien;
	private String consteServicio;
	private String consteBienConsumible;
	private String consteBienActivo;	
	private String msg;
	private String codSede;
	private List listaSedes;
	private String codTipoRequerimiento;
	private List listCodTipoRequerimiento;
	private String codGrupoServicio;
	private List listaCodGrupoServicio;
	private String fecInicio;
	private String fecFinal;	
	private String codCentroCosto;
	private List listaCodCentroCosto;
	private String codOpciones;
	private String valorIndGrupo;
	private String consteGrupoGenerales;
	private String consteGrupoMantenimiento;
	private String consteGrupoServicios;
	private String tituloReporte;
	private String codEstadoReq;
	private List listaCodEstadoReq;
	private String usuSolicitante;
	private String nomAprob;
	private String nroReporte;
	private String codTipoGastos;
	private List listaCodTipoGastos;
	private String proveedor;
	private String codTipoReporte;
		
	private String codReporte5;
	private String codReporte7;
	private String codReporte9;
	private String codReporte29;
	private String dscUsuario;
	private String consteSede;
	
	private List listaCodTipoServicio;
	private String codTipoServicio;
	
	public List getListaCodTipoServicio() {
		return listaCodTipoServicio;
	}
	public void setListaCodTipoServicio(List listaCodTipoServicio) {
		this.listaCodTipoServicio = listaCodTipoServicio;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getNroReporte() {
		return nroReporte;
	}
	public void setNroReporte(String nroReporte) {
		this.nroReporte = nroReporte;
	}
	public String getTituloReporte() {
		return tituloReporte;
	}
	public void setTituloReporte(String tituloReporte) {
		this.tituloReporte = tituloReporte;
	}
	public String getCodEstadoReq() {
		return codEstadoReq;
	}
	public void setCodEstadoReq(String codEstadoReq) {
		this.codEstadoReq = codEstadoReq;
	}
	public List getListaCodEstadoReq() {
		return listaCodEstadoReq;
	}
	public void setListaCodEstadoReq(List listaCodEstadoReq) {
		this.listaCodEstadoReq = listaCodEstadoReq;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getValorIndGrupo() {
		return valorIndGrupo;
	}
	public void setValorIndGrupo(String valorIndGrupo) {
		this.valorIndGrupo = valorIndGrupo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getConsteRadio() {
		return consteRadio;
	}
	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}
	
	public String getConsteServicio() {
		return consteServicio;
	}
	public void setConsteServicio(String consteServicio) {
		this.consteServicio = consteServicio;
	}
	public String getConsteBienConsumible() {
		return consteBienConsumible;
	}
	public void setConsteBienConsumible(String consteBienConsumible) {
		this.consteBienConsumible = consteBienConsumible;
	}
	public String getConsteBienActivo() {
		return consteBienActivo;
	}
	public void setConsteBienActivo(String consteBienActivo) {
		this.consteBienActivo = consteBienActivo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListaSedes() {
		return listaSedes;
	}
	public void setListaSedes(List listaSedes) {
		this.listaSedes = listaSedes;
	}
	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}
	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}
	public List getListCodTipoRequerimiento() {
		return listCodTipoRequerimiento;
	}
	public void setListCodTipoRequerimiento(List listCodTipoRequerimiento) {
		this.listCodTipoRequerimiento = listCodTipoRequerimiento;
	}
	public String getCodGrupoServicio() {
		return codGrupoServicio;
	}
	public void setCodGrupoServicio(String codGrupoServicio) {
		this.codGrupoServicio = codGrupoServicio;
	}
	public List getListaCodGrupoServicio() {
		return listaCodGrupoServicio;
	}
	public void setListaCodGrupoServicio(List listaCodGrupoServicio) {
		this.listaCodGrupoServicio = listaCodGrupoServicio;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getCodCentroCosto() {
		return codCentroCosto;
	}
	public void setCodCentroCosto(String codCentroCosto) {
		this.codCentroCosto = codCentroCosto;
	}
	public List getListaCodCentroCosto() {
		return listaCodCentroCosto;
	}
	public void setListaCodCentroCosto(List listaCodCentroCosto) {
		this.listaCodCentroCosto = listaCodCentroCosto;
	}
	public String getConsteBien() {
		return consteBien;
	}
	public void setConsteBien(String consteBien) {
		this.consteBien = consteBien;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getConsteGrupoGenerales() {
		return consteGrupoGenerales;
	}
	public void setConsteGrupoGenerales(String consteGrupoGenerales) {
		this.consteGrupoGenerales = consteGrupoGenerales;
	}
	public String getConsteGrupoMantenimiento() {
		return consteGrupoMantenimiento;
	}
	public void setConsteGrupoMantenimiento(String consteGrupoMantenimiento) {
		this.consteGrupoMantenimiento = consteGrupoMantenimiento;
	}
	public String getConsteGrupoServicios() {
		return consteGrupoServicios;
	}
	public void setConsteGrupoServicios(String consteGrupoServicios) {
		this.consteGrupoServicios = consteGrupoServicios;
	}
	public String getCodTipoGastos() {
		return codTipoGastos;
	}
	public void setCodTipoGastos(String codTipoGastos) {
		this.codTipoGastos = codTipoGastos;
	}
	public List getListaCodTipoGastos() {
		return listaCodTipoGastos;
	}
	public void setListaCodTipoGastos(List listaCodTipoGastos) {
		this.listaCodTipoGastos = listaCodTipoGastos;
	}
	public String getCodTipoReporte() {
		return codTipoReporte;
	}
	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}
	public String getCodReporte5() {
		return codReporte5;
	}
	public void setCodReporte5(String codReporte5) {
		this.codReporte5 = codReporte5;
	}
	public String getCodReporte7() {
		return codReporte7;
	}
	public void setCodReporte7(String codReporte7) {
		this.codReporte7 = codReporte7;
	}
	public String getCodReporte9() {
		return codReporte9;
	}
	public void setCodReporte9(String codReporte9) {
		this.codReporte9 = codReporte9;
	}
	public String getConsteSede() {
		return consteSede;
	}
	public void setConsteSede(String consteSede) {
		this.consteSede = consteSede;
	}
	public String getCodReporte29() {
		return codReporte29;
	}
	public void setCodReporte29(String codReporte29) {
		this.codReporte29 = codReporte29;
	}	
	public String getNomAprob() {
		return nomAprob;
	}
	public void setNomAprob(String nomAprob) {
		this.nomAprob = nomAprob;
	}			
}
