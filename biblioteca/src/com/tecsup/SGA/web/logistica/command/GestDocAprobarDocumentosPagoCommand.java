package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class GestDocAprobarDocumentosPagoCommand {

	private String operacion;
	private String msg;
	private String codPerfil;
	private String codUsuario;
	private String codOpciones;
	private String codSede;
	private String nroFactura;
	private String fecInicio1;
	private String fecFinal1;
	private String nroRucProveedor;
	private String nombreProveedor;
	private String codEstado;
	private String nroOrden;
	private String fecInicio2;
	private String fecFinal2;
	
	private List listaCodEstado;
	private List listaBandeja;
	
	private String codOrden;
	private String codDocumento;
	private String banListaBandeja;
	
	public String getBanListaBandeja() {
		return banListaBandeja;
	}
	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getCodDocumento() {
		return codDocumento;
	}
	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getFecInicio1() {
		return fecInicio1;
	}
	public void setFecInicio1(String fecInicio1) {
		this.fecInicio1 = fecInicio1;
	}
	public String getFecFinal1() {
		return fecFinal1;
	}
	public void setFecFinal1(String fecFinal1) {
		this.fecFinal1 = fecFinal1;
	}
	public String getNroRucProveedor() {
		return nroRucProveedor;
	}
	public void setNroRucProveedor(String nroRucProveedor) {
		this.nroRucProveedor = nroRucProveedor;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFecInicio2() {
		return fecInicio2;
	}
	public void setFecInicio2(String fecInicio2) {
		this.fecInicio2 = fecInicio2;
	}
	public String getFecFinal2() {
		return fecFinal2;
	}
	public void setFecFinal2(String fecFinal2) {
		this.fecFinal2 = fecFinal2;
	}
	public List getListaCodEstado() {
		return listaCodEstado;
	}
	public void setListaCodEstado(List listaCodEstado) {
		this.listaCodEstado = listaCodEstado;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
}
