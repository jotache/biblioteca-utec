package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class GestDocConsultarDocumentosPagosAsociadosIframeCommand {

	private String operacion;
	private String msg;
	private String codPerfil;
	
	private String codUsuario;
	private String codOpciones;
	private String codSede;
	private String nroFactura;
	private String fecInicio1;
	
	private String fecInicio2;
	private String nroGuia;
	private String fecFinal1;
	private String total;
	
	private List listaBandeja;
	private List listaBandejaMedio;
	
	private String codOrden;
	private String codDocumento;
	private String longitud;
	private String cadCantidades;
	private String cadObservaciones;
	private String fechaBD;
	private String fecFinal2;
	private String subTotal;
	private String igv;
	private String igvDB;
	
	private String indImportacion;
	//ALQD,28/05/09.NUEVA PROPIEDAD
	private String indAfectoIGV;
	
	public String getIndAfectoIGV() {
		return indAfectoIGV;
	}
	public void setIndAfectoIGV(String indAfectoIGV) {
		this.indAfectoIGV=indAfectoIGV;
	}
	public String getFecFinal2() {
		return fecFinal2;
	}

	public void setFecFinal2(String fecFinal2) {
		this.fecFinal2 = fecFinal2;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getFecInicio1() {
		return fecInicio1;
	}

	public void setFecInicio1(String fecInicio1) {
		this.fecInicio1 = fecInicio1;
	}

	public String getFecInicio2() {
		return fecInicio2;
	}

	public void setFecInicio2(String fecInicio2) {
		this.fecInicio2 = fecInicio2;
	}

	public String getNroGuia() {
		return nroGuia;
	}

	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}

	public String getFecFinal1() {
		return fecFinal1;
	}

	public void setFecFinal1(String fecFinal1) {
		this.fecFinal1 = fecFinal1;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List getListaBandeja() {
		return listaBandeja;
	}

	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}

	public List getListaBandejaMedio() {
		return listaBandejaMedio;
	}

	public void setListaBandejaMedio(List listaBandejaMedio) {
		this.listaBandejaMedio = listaBandejaMedio;
	}

	public String getCodOrden() {
		return codOrden;
	}

	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}

	public String getCodDocumento() {
		return codDocumento;
	}

	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getCadCantidades() {
		return cadCantidades;
	}

	public void setCadCantidades(String cadCantidades) {
		this.cadCantidades = cadCantidades;
	}

	public String getCadObservaciones() {
		return cadObservaciones;
	}

	public void setCadObservaciones(String cadObservaciones) {
		this.cadObservaciones = cadObservaciones;
	}

	public String getFechaBD() {
		return fechaBD;
	}

	public void setFechaBD(String fechaBD) {
		this.fechaBD = fechaBD;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getIgv() {
		return igv;
	}

	public void setIgv(String igv) {
		this.igv = igv;
	}

	public String getIgvDB() {
		return igvDB;
	}

	public void setIgvDB(String igvDB) {
		this.igvDB = igvDB;
	}

	public String getIndImportacion() {
		return indImportacion;
	}

	public void setIndImportacion(String indImportacion) {
		this.indImportacion = indImportacion;
	}
			
}
