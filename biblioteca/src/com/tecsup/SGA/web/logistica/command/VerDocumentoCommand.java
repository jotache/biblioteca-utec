package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class VerDocumentoCommand {
	private String operacion;
	private String codAlumno;
	private String msg;
	private String msgRe;
	private String msgVa;
	private String grabar;
	private String ordeId;
	private String docId;
	private String sede;
	
	private String nroFactura;
	private String nroGuia;
	private String nroOrden;
	private String fechaEmicion;
	private String fechaEmicion2;
	private String fechaEmicion3;
	private String fechaVcto;
	private String moneda;
	private String monto;
	private List listDocumentos;
	private String total;
	private List listDetalles;
	private float tot;
	private int cant;
	private String valida;
	private String rechaza;
	private String igv;
	private String subTotal;
	//ALQD,12/06/09.AGREGANDO NUEVAS PROPIEDADES
	private String indImportacion;
	private List listDocumentosRelacionados;
	//ALQD,01/07/09.AGREGANDO NUEVAS PROPIEDADES
	private String impOtros;
	
	public String getImpOtros() {
		return impOtros;
	}
	public void setImpOtros(String impOtros) {
		this.impOtros = impOtros;
	}
	public List getListDocumentosRelacionados() {
		return listDocumentosRelacionados;
	}
	public void setListDocumentosRelacionados(List listDocumentosRelacionados) {
		this.listDocumentosRelacionados = listDocumentosRelacionados;
	}
	public String getIndImportacion() {
		return indImportacion;
	}
	public void setIndImportacion(String indImportacion) {
		this.indImportacion = indImportacion;
	}
	public String getValida() {
		return valida;
	}
	public void setValida(String valida) {
		this.valida = valida;
	}
	public String getRechaza() {
		return rechaza;
	}
	public void setRechaza(String rechaza) {
		this.rechaza = rechaza;
	}
	public List getListDetalles() {
		return listDetalles;
	}
	public void setListDetalles(List listDetalles) {
		this.listDetalles = listDetalles;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFechaEmicion() {
		return fechaEmicion;
	}
	public void setFechaEmicion(String fechaEmicion) {
		this.fechaEmicion = fechaEmicion;
	}
	public String getFechaEmicion2() {
		return fechaEmicion2;
	}
	public void setFechaEmicion2(String fechaEmicion2) {
		this.fechaEmicion2 = fechaEmicion2;
	}
	public String getFechaEmicion3() {
		return fechaEmicion3;
	}
	public void setFechaEmicion3(String fechaEmicion3) {
		this.fechaEmicion3 = fechaEmicion3;
	}
	public String getFechaVcto() {
		return fechaVcto;
	}
	public void setFechaVcto(String fechaVcto) {
		this.fechaVcto = fechaVcto;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public List getListDocumentos() {
		return listDocumentos;
	}
	public void setListDocumentos(List listDocumentos) {
		this.listDocumentos = listDocumentos;
	}
	public String getOrdeId() {
		return ordeId;
	}
	public void setOrdeId(String ordeId) {
		this.ordeId = ordeId;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public float getTot() {
		return tot;
	}
	public void setTot(float tot) {
		this.tot = tot;
	}
	public int getCant() {
		return cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	public String getMsgRe() {
		return msgRe;
	}
	public void setMsgRe(String msgRe) {
		this.msgRe = msgRe;
	}
	public String getMsgVa() {
		return msgVa;
	}
	public void setMsgVa(String msgVa) {
		this.msgVa = msgVa;
	}	
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getIgv() {
		return igv;
	}
	public void setIgv(String igv) {
		this.igv = igv;
	}
}
