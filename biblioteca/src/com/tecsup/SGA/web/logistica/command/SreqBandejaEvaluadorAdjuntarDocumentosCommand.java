package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class SreqBandejaEvaluadorAdjuntarDocumentosCommand {

	private String operacion;
	private String codUsuario;
	private String codRequerimiento;
	private String codDetRequerimiento;
	private String codTipoAdjunto;
	private String txtDescripArchivo;
	
	private List listaTipoAdjunto;
	
	private String msg;
	private String extArchivo;
	private String nomArchivo;
	
	private byte[] txtArchivo;
	private String nomNuevoArchivo;
	
	private String ruta;

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getNomNuevoArchivo() {
		return nomNuevoArchivo;
	}

	public void setNomNuevoArchivo(String nomNuevoArchivo) {
		this.nomNuevoArchivo = nomNuevoArchivo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public String getExtArchivo() {
		return extArchivo;
	}

	public void setExtArchivo(String extArchivo) {
		this.extArchivo = extArchivo;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}

	public String getCodDetRequerimiento() {
		return codDetRequerimiento;
	}

	public void setCodDetRequerimiento(String codDetRequerimiento) {
		this.codDetRequerimiento = codDetRequerimiento;
	}

	public String getCodTipoAdjunto() {
		return codTipoAdjunto;
	}

	public void setCodTipoAdjunto(String codTipoAdjunto) {
		this.codTipoAdjunto = codTipoAdjunto;
	}

	public String getTxtDescripArchivo() {
		return txtDescripArchivo;
	}

	public void setTxtDescripArchivo(String txtDescripArchivo) {
		this.txtDescripArchivo = txtDescripArchivo;
	}

	public List getListaTipoAdjunto() {
		return listaTipoAdjunto;
	}

	public void setListaTipoAdjunto(List listaTipoAdjunto) {
		this.listaTipoAdjunto = listaTipoAdjunto;
	}

	public byte[] getTxtArchivo() {
		return txtArchivo;
	}

	public void setTxtArchivo(byte[] txtArchivo) {
		this.txtArchivo = txtArchivo;
	}
}
