package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class ConsultaCatalogoCommand {
	private String operacion;
	private List listSede;
	private List listFamilia;
	private List listSubFamilia;
	private List listCondicion;
	private List listProductos;
	private String codSede;
	private String codFamilia;
	private String codSubFamilia;
	private String codCondicion;
	private String codPro;
	private String producto;
	private String nroSerie;
	private String codAlumno;
	private String codProducto;
	private String ruta;
	private String nomPro;
	private String msg;
	private String elimina;
	private String codEstado;
	private List listestado;
	private String fec1;
	private String fec2;
	private String dscUsuario;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNomPro() {
		return nomPro;
	}
	public void setNomPro(String nomPro) {
		this.nomPro = nomPro;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	
	public String getCodPro() {
		return codPro;
	}
	public void setCodPro(String codPro) {
		this.codPro = codPro;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getNroSerie() {
		return nroSerie;
	}
	public void setNroSerie(String nroSerie) {
		this.nroSerie = nroSerie;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListSede() {
		return listSede;
	}
	public void setListSede(List listSede) {
		this.listSede = listSede;
	}
	public List getListFamilia() {
		return listFamilia;
	}
	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}
	public List getListSubFamilia() {
		return listSubFamilia;
	}
	public void setListSubFamilia(List listSubFamilia) {
		this.listSubFamilia = listSubFamilia;
	}
	public List getListCondicion() {
		return listCondicion;
	}
	public void setListCondicion(List listCondicion) {
		this.listCondicion = listCondicion;
	}
	public List getListProductos() {
		return listProductos;
	}
	public void setListProductos(List listProductos) {
		this.listProductos = listProductos;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	public String getCodCondicion() {
		return codCondicion;
	}
	public void setCodCondicion(String codCondicion) {
		this.codCondicion = codCondicion;
	}
	public String getElimina() {
		return elimina;
	}
	public void setElimina(String elimina) {
		this.elimina = elimina;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public List getListestado() {
		return listestado;
	}
	public void setListestado(List listestado) {
		this.listestado = listestado;
	}
	public String getFec1() {
		return fec1;
	}
	public void setFec1(String fec1) {
		this.fec1 = fec1;
	}
	public String getFec2() {
		return fec2;
	}
	public void setFec2(String fec2) {
		this.fec2 = fec2;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	
	
}
