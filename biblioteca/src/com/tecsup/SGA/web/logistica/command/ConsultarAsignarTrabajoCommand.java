package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ConsultarAsignarTrabajoCommand {
	private String operacion;
	private String codAlumno;
	private String msg;
	private String grabar;
	private List listComplejidad;
	private String codComplejidad;
	private List listTrabajo;
	private String nroReq;
	private String sede;
	private String codReq;
	private String codRes;
	
	public String getCodRes() {
		return codRes;
	}
	public void setCodRes(String codRes) {
		this.codRes = codRes;
	}
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public List getListComplejidad() {
		return listComplejidad;
	}
	public void setListComplejidad(List listComplejidad) {
		this.listComplejidad = listComplejidad;
	}
	public String getCodComplejidad() {
		return codComplejidad;
	}
	public void setCodComplejidad(String codComplejidad) {
		this.codComplejidad = codComplejidad;
	}
	public List getListTrabajo() {
		return listTrabajo;
	}
	public void setListTrabajo(List listTrabajo) {
		this.listTrabajo = listTrabajo;
	}
	public String getNroReq() {
		return nroReq;
	}
	public void setNroReq(String nroReq) {
		this.nroReq = nroReq;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	
}
