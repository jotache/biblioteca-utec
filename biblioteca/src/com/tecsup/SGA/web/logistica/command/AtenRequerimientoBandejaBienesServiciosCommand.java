package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class AtenRequerimientoBandejaBienesServiciosCommand {

	private String operacion;
	private String msg;
	private String valConsumible;
	private String valActivo;
	private String codSede;
	private String consteRadio;
	private String consteBien;
	private String consteServicio;
	private String consteBienConsumible;
	private String consteBienActivo;
	private String codUsuario;
	private String codOpciones;
	private String codPerfil;
	private String consteGrupoGenerales;
	private String consteGrupoMantenimiento;
	private String consteGrupoServicios;
	private String indGrupo;
	
	private String codTipoRequerimiento;
	private List listCodTipoRequerimiento;
	private String codTipoServicio;
	private List listaCodTipoServicio;
	private String txtNroRequerimiento;
	private String fecInicio;
	private String fecFinal;
	private String codCeco;
	private List listCeco;
	private String usuSolicitante;
	private String codEstado;
	private List listaCodEstado;
	private List listaBandejaBienes;
	private List listaBandejaServicios;
	
	private String idReq;
	private String codProveedor;
	private String banListaBien;
	private String banListaServicio;
	private String indiceCerrar;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	
	private String estadoComboTipoReq;
//ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA
	private String nroGuia;
	//ALQD,23/01/09. A�ADIENDO NUEVAS PROPIEDAS PARA BUSCAR SALIDAS PENDIENTES DE CONFIRMAR
	private String indSalPorConfirmar;
	private String consteIndSalPorConfirmar;
	
	public String getIndSalPorConfirmar() {
		return indSalPorConfirmar;
	}
	public void setIndSalPorConfirmar(String indSalPorConfirmar) {
		this.indSalPorConfirmar = indSalPorConfirmar;
	}
	public String getConsteIndSalPorConfirmar() {
		return consteIndSalPorConfirmar;
	}
	public void setConsteIndSalPorConfirmar(String consteIndSalPorConfirmar) {
		this.consteIndSalPorConfirmar = consteIndSalPorConfirmar;
	}

	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	
	public String getEstadoComboTipoReq() {
		return estadoComboTipoReq;
	}
	public void setEstadoComboTipoReq(String estadoComboTipoReq) {
		this.estadoComboTipoReq = estadoComboTipoReq;
	}
	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}
	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}
	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}
	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}
	public String getIndiceCerrar() {
		return indiceCerrar;
	}
	public void setIndiceCerrar(String indiceCerrar) {
		this.indiceCerrar = indiceCerrar;
	}
	public String getBanListaBien() {
		return banListaBien;
	}
	public void setBanListaBien(String banListaBien) {
		this.banListaBien = banListaBien;
	}
	public String getBanListaServicio() {
		return banListaServicio;
	}
	public void setBanListaServicio(String banListaServicio) {
		this.banListaServicio = banListaServicio;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public String getIdReq() {
		return idReq;
	}
	public void setIdReq(String idReq) {
		this.idReq = idReq;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getValConsumible() {
		return valConsumible;
	}
	public void setValConsumible(String valConsumible) {
		this.valConsumible = valConsumible;
	}
	public String getValActivo() {
		return valActivo;
	}
	public void setValActivo(String valActivo) {
		this.valActivo = valActivo;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getConsteRadio() {
		return consteRadio;
	}
	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}
	public String getConsteBien() {
		return consteBien;
	}
	public void setConsteBien(String consteBien) {
		this.consteBien = consteBien;
	}
	public String getConsteServicio() {
		return consteServicio;
	}
	public void setConsteServicio(String consteServicio) {
		this.consteServicio = consteServicio;
	}
	public String getConsteBienConsumible() {
		return consteBienConsumible;
	}
	public void setConsteBienConsumible(String consteBienConsumible) {
		this.consteBienConsumible = consteBienConsumible;
	}
	public String getConsteBienActivo() {
		return consteBienActivo;
	}
	public void setConsteBienActivo(String consteBienActivo) {
		this.consteBienActivo = consteBienActivo;
	}
	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}
	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}
	public List getListCodTipoRequerimiento() {
		return listCodTipoRequerimiento;
	}
	public void setListCodTipoRequerimiento(List listCodTipoRequerimiento) {
		this.listCodTipoRequerimiento = listCodTipoRequerimiento;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public List getListaCodTipoServicio() {
		return listaCodTipoServicio;
	}
	public void setListaCodTipoServicio(List listaCodTipoServicio) {
		this.listaCodTipoServicio = listaCodTipoServicio;
	}
	public String getTxtNroRequerimiento() {
		return txtNroRequerimiento;
	}
	public void setTxtNroRequerimiento(String txtNroRequerimiento) {
		this.txtNroRequerimiento = txtNroRequerimiento;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getCodCeco() {
		return codCeco;
	}
	public void setCodCeco(String codCeco) {
		this.codCeco = codCeco;
	}
	public List getListCeco() {
		return listCeco;
	}
	public void setListCeco(List listCeco) {
		this.listCeco = listCeco;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public List getListaCodEstado() {
		return listaCodEstado;
	}
	public void setListaCodEstado(List listaCodEstado) {
		this.listaCodEstado = listaCodEstado;
	}
	public List getListaBandejaBienes() {
		return listaBandejaBienes;
	}
	public void setListaBandejaBienes(List listaBandejaBienes) {
		this.listaBandejaBienes = listaBandejaBienes;
	}
	public List getListaBandejaServicios() {
		return listaBandejaServicios;
	}
	public void setListaBandejaServicios(List listaBandejaServicios) {
		this.listaBandejaServicios = listaBandejaServicios;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getConsteGrupoGenerales() {
		return consteGrupoGenerales;
	}
	public void setConsteGrupoGenerales(String consteGrupoGenerales) {
		this.consteGrupoGenerales = consteGrupoGenerales;
	}
	public String getConsteGrupoMantenimiento() {
		return consteGrupoMantenimiento;
	}
	public void setConsteGrupoMantenimiento(String consteGrupoMantenimiento) {
		this.consteGrupoMantenimiento = consteGrupoMantenimiento;
	}
	public String getConsteGrupoServicios() {
		return consteGrupoServicios;
	}
	public void setConsteGrupoServicios(String consteGrupoServicios) {
		this.consteGrupoServicios = consteGrupoServicios;
	}
	public String getIndGrupo() {
		return indGrupo;
	}
	public void setIndGrupo(String indGrupo) {
		this.indGrupo = indGrupo;
	};
}
