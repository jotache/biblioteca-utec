package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class GestOrdenDetalleOrdenCommand {

	public String operacion;
	public String msg;
	public String codPerfil;
	public String codUsuario;
	
	public String codOpciones;
	public String codSede;
	public String codTipoReq;
	
	public String codReq;
	public String nroOrden;
	public String fecEmision;
	public String txtProveedorRuc;
	public String txtAtencion;
	public String txtMoneda;
	public String txtComprador;
	public String txtProveedorNombre;
	public String codOrden;
	private String txtTotal;
	//ALQD,18/08/08. A�adiendo nuevas propiedades para lanzar cotizaciones
	private String codCotizacion;
	private String codEstCotizacion;
	
	public List listaBandeja;
	public List listaBandejaCondicion;
	
	private String codBandera;
	private String banderaPerfil;
	private String codEstado;
	private String consteCodEstadoEnAprobacion;
	private String txtTipoMoneda;
	private String txtIgv;
	private String txtSubTotal;
	private String banListaBandejaCondicion;
	private String banListaBandeja;
	private String indiceAprobacion;
	private String indImportacion;
	private String importeOtros;
//ALQD,21/10/08. AGREGANDO NUEVAS PROPIEDADES PARA LA NUEVA PANTALLA DE APOROBACION
	private String banListaAprobadores;
	private String banListaSolicitantesProd;
	public List listaSolicitantesProd;
	public List listaAprobadores;
	//ALQD,05/02/09. NUEVA PROPIEDAD
	private String numCotizacion;
	//ALQD,19/02/09.NUEVAS PROPIEDADES
	private String indInversion;
	private String codOrdenTipoGasto;
	private String desOrdenTipoGasto;
	private String desComentario; //Utilizado para comentario de rechazo 
	//ALQD,24/02/09. NUEVA PROPIEDAD
	private String codTipPago;
	private String desTipPago;
	//ALQD,04/04/09.NUEVA PROPIEDAD
	private String desEstado;

	public String getDesEstado() {
		return desEstado;
	}
	public void setDesEstado(String desEstado) {
		this.desEstado=desEstado;
	}
	public String getCodTipPago() {
		return codTipPago;
	}
	public void setCodTipPago(String codTipPago) {
		this.codTipPago=codTipPago;
	}
	public String getDesTipPago() {
		return desTipPago;
	}
	public void setDesTipPago(String desTipPago) {
		this.desTipPago=desTipPago;
	}
	public String getIndInversion() {
		return indInversion;
	}
	public void setIndInversion(String indInversion) {
		this.indInversion=indInversion;
	}
	public String getCodOrdenTipoGasto() {
		return codOrdenTipoGasto;
	}
	public void setCodOrdenTipoGasto(String codOrdenTipoGasto) {
		this.codOrdenTipoGasto=codOrdenTipoGasto;
	}
	public String getDesOrdenTipoGasto() {
		return desOrdenTipoGasto;
	}
	public void setDesOrdenTipoGasto(String desOrdenTipoGasto) {
		this.desOrdenTipoGasto=desOrdenTipoGasto;
	}
	public String getDesComentario() {
		return desComentario;
	}
	public void setDesComentario(String desComentario) {
		this.desComentario=desComentario;
	}
	public String getNumCotizacion() {
		return numCotizacion;
	}
	public void setNumCotizacion(String numCotizacion) {
		this.numCotizacion=numCotizacion;
	}
	public List getListaAprobadores(){
		return listaAprobadores;
	}
	public void setListaAprobadores(List listaAprobadores){
		this.listaAprobadores=listaAprobadores;
	}
	/**
	 * @return the listaSolicitantesProd
	 */
	public List getListaSolicitantesProd() {
		return listaSolicitantesProd;
	}

	/**
	 * @param listaSolicitantesProd the listaSolicitantesProd to set
	 */
	public void setListaSolicitantesProd(List listaSolicitantesProd) {
		this.listaSolicitantesProd = listaSolicitantesProd;
	}

	public String getIndImportacion() {
		return indImportacion;
	}

	public void setIndImportacion(String indImportacion) {
		this.indImportacion = indImportacion;
	}

	public String getImporteOtros() {
		return importeOtros;
	}

	public void setImporteOtros(String importeOtros) {
		this.importeOtros = importeOtros;
	}

	public String getTxtTipoMoneda() {
		return txtTipoMoneda;
	}

	public void setTxtTipoMoneda(String txtTipoMoneda) {
		this.txtTipoMoneda = txtTipoMoneda;
	}

	public String getConsteCodEstadoEnAprobacion() {
		return consteCodEstadoEnAprobacion;
	}

	public void setConsteCodEstadoEnAprobacion(String consteCodEstadoEnAprobacion) {
		this.consteCodEstadoEnAprobacion = consteCodEstadoEnAprobacion;
	}

	public String getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}

	public String getCodBandera() {
		return codBandera;
	}

	public void setCodBandera(String codBandera) {
		this.codBandera = codBandera;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getCodTipoReq() {
		return codTipoReq;
	}

	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}

	public String getCodReq() {
		return codReq;
	}

	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}

	public String getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}

	public String getFecEmision() {
		return fecEmision;
	}

	public void setFecEmision(String fecEmision) {
		this.fecEmision = fecEmision;
	}

	public String getTxtProveedorRuc() {
		return txtProveedorRuc;
	}

	public void setTxtProveedorRuc(String txtProveedorRuc) {
		this.txtProveedorRuc = txtProveedorRuc;
	}

	public String getTxtAtencion() {
		return txtAtencion;
	}

	public void setTxtAtencion(String txtAtencion) {
		this.txtAtencion = txtAtencion;
	}

	public String getTxtMoneda() {
		return txtMoneda;
	}

	public void setTxtMoneda(String txtMoneda) {
		this.txtMoneda = txtMoneda;
	}

	public String getTxtComprador() {
		return txtComprador;
	}

	public void setTxtComprador(String txtComprador) {
		this.txtComprador = txtComprador;
	}

	public List getListaBandeja() {
		return listaBandeja;
	}

	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}

	public String getTxtProveedorNombre() {
		return txtProveedorNombre;
	}

	public void setTxtProveedorNombre(String txtProveedorNombre) {
		this.txtProveedorNombre = txtProveedorNombre;
	}

	public String getCodOrden() {
		return codOrden;
	}

	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}

	public List getListaBandejaCondicion() {
		return listaBandejaCondicion;
	}

	public void setListaBandejaCondicion(List listaBandejaCondicion) {
		this.listaBandejaCondicion = listaBandejaCondicion;
	}

	public String getTxtTotal() {
		return txtTotal;
	}

	public void setTxtTotal(String txtTotal) {
		this.txtTotal = txtTotal;
	}

	public String getBanderaPerfil() {
		return banderaPerfil;
	}

	public void setBanderaPerfil(String banderaPerfil) {
		this.banderaPerfil = banderaPerfil;
	}

	public String getTxtIgv() {
		return txtIgv;
	}

	public void setTxtIgv(String txtIgv) {
		this.txtIgv = txtIgv;
	}

	public String getTxtSubTotal() {
		return txtSubTotal;
	}

	public void setTxtSubTotal(String txtSubTotal) {
		this.txtSubTotal = txtSubTotal;
	}

	public String getBanListaBandejaCondicion() {
		return banListaBandejaCondicion;
	}

	public void setBanListaBandejaCondicion(String banListaBandejaCondicion) {
		this.banListaBandejaCondicion = banListaBandejaCondicion;
	}

	public String getBanListaBandeja() {
		return banListaBandeja;
	}

	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}

	public String getIndiceAprobacion() {
		return indiceAprobacion;
	}

	public void setIndiceAprobacion(String indiceAprobacion) {
		this.indiceAprobacion = indiceAprobacion;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}

	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}

	public String getCodEstCotizacion() {
		return codEstCotizacion;
	}

	public void setCodEstCotizacion(String codEstado) {
		this.codEstCotizacion = codEstado;
	}

//ALQD,21/10/08. METODOS PARA LAS NUEVAS PROPIEDADES
	public String getBanListaAprobadores() {
		return banListaAprobadores;
	}

	public void setBanListaAprobadores(String banListaAprobadores) {
		this.banListaAprobadores= banListaAprobadores;
	}

	public String getBanListaSolicitantesProd() {
		return banListaSolicitantesProd;
	}

	public void setBanListaSolicitantesProd(String banListaSolicitantesProd) {
		this.banListaSolicitantesProd= banListaSolicitantesProd;
	}
}
