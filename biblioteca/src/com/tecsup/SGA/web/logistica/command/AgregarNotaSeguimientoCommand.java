package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarNotaSeguimientoCommand {
	private String operacion;
	private String msg;
	private String nota;
	private String graba;
	private String codAlumno;
	private String tipo;
	private String codReq;
	private String codPerfil;
	private String codNota;
	private List listDes;
	
	public List getListDes() {
		return listDes;
	}
	public void setListDes(List listDes) {
		this.listDes = listDes;
	}
	public String getCodNota() {
		return codNota;
	}
	public void setCodNota(String codNota) {
		this.codNota = codNota;
	}
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
}
