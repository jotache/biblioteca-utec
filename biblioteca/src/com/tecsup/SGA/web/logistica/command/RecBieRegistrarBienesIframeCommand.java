package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class RecBieRegistrarBienesIframeCommand {

	private String operacion;
	private String msg;
	private String codPerfil;
	private String codUsuario;
	private String codOpciones;
	private String codSede;
	private String codOrden;
	private String codDocumento;
	private String longitud;
	private String cadCantidades;
	private String cadObservaciones;
	private String nroFactura;
	private String fecInicio1;
	private String nroGuia;
	private String fecFinal1;
	private String fecInicio2;
	private String total;
	
	private List listaSuperior ;
	private List listaBandeja ;
	
	private String cadCodDocDetalle;
	private String fechaBD;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	private String fecFinal2;
	private String indModi;
	
	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}
	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}
	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}
	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getCodDocumento() {
		return codDocumento;
	}
	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getCadCantidades() {
		return cadCantidades;
	}
	public void setCadCantidades(String cadCantidades) {
		this.cadCantidades = cadCantidades;
	}
	public String getCadObservaciones() {
		return cadObservaciones;
	}
	public void setCadObservaciones(String cadObservaciones) {
		this.cadObservaciones = cadObservaciones;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getFecInicio1() {
		return fecInicio1;
	}
	public void setFecInicio1(String fecInicio1) {
		this.fecInicio1 = fecInicio1;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getFecFinal1() {
		return fecFinal1;
	}
	public void setFecFinal1(String fecFinal1) {
		this.fecFinal1 = fecFinal1;
	}
	public String getFecInicio2() {
		return fecInicio2;
	}
	public void setFecInicio2(String fecInicio2) {
		this.fecInicio2 = fecInicio2;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public List getListaSuperior() {
		return listaSuperior;
	}
	public void setListaSuperior(List listaSuperior) {
		this.listaSuperior = listaSuperior;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getCadCodDocDetalle() {
		return cadCodDocDetalle;
	}
	public void setCadCodDocDetalle(String cadCodDocDetalle) {
		this.cadCodDocDetalle = cadCodDocDetalle;
	}
	public String getFechaBD() {
		return fechaBD;
	}
	public void setFechaBD(String fechaBD) {
		this.fechaBD = fechaBD;
	}
	public String getFecFinal2() {
		return fecFinal2;
	}
	public void setFecFinal2(String fecFinal2) {
		this.fecFinal2 = fecFinal2;
	}
	public String getIndModi() {
		return indModi;
	}
	public void setIndModi(String indModi) {
		this.indModi = indModi;
	}
		
}
