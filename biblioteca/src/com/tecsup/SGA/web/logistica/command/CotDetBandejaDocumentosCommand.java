package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.modelo.CotizacionAdjunto;

public class CotDetBandejaDocumentosCommand {
	private String operacion;
	private String msg;
	private String codUsuario;
	private String codCotizacion;
	private String codCotizacionDet;
	private String codAdjunto;
	private String tipoAdjunto;
	private String tipoProcedencia;
	private String dscTipoProcedencia;
	private String dscTipoProcedencia2;
	//para ocultar el los botones
	private String condicion;
	private List<SolRequerimientoAdjunto> listaDocumentosItem;
	private List<CotizacionAdjunto> listaDocumentosCompItem;
	
	private String documentDownload;
	
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCodCotizacionDet() {
		return codCotizacionDet;
	}
	public void setCodCotizacionDet(String codCotizacionDet) {
		this.codCotizacionDet = codCotizacionDet;
	}
	public String getCodAdjunto() {
		return codAdjunto;
	}
	public void setCodAdjunto(String codAdjunto) {
		this.codAdjunto = codAdjunto;
	}
	public String getTipoAdjunto() {
		return tipoAdjunto;
	}
	public void setTipoAdjunto(String tipoAdjunto) {
		this.tipoAdjunto = tipoAdjunto;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public List<SolRequerimientoAdjunto> getListaDocumentosItem() {
		return listaDocumentosItem;
	}
	public void setListaDocumentosItem(
			List<SolRequerimientoAdjunto> listaDocumentosItem) {
		this.listaDocumentosItem = listaDocumentosItem;
	}
	public List<CotizacionAdjunto> getListaDocumentosCompItem() {
		return listaDocumentosCompItem;
	}
	public void setListaDocumentosCompItem(
			List<CotizacionAdjunto> listaDocumentosCompItem) {
		this.listaDocumentosCompItem = listaDocumentosCompItem;
	}
	public String getTipoProcedencia() {
		return tipoProcedencia;
	}
	public void setTipoProcedencia(String tipoProcedencia) {
		this.tipoProcedencia = tipoProcedencia;
	}
	public String getDscTipoProcedencia() {
		return dscTipoProcedencia;
	}
	public void setDscTipoProcedencia(String dscTipoProcedencia) {
		this.dscTipoProcedencia = dscTipoProcedencia;
	}
	public String getDscTipoProcedencia2() {
		return dscTipoProcedencia2;
	}
	public void setDscTipoProcedencia2(String dscTipoProcedencia2) {
		this.dscTipoProcedencia2 = dscTipoProcedencia2;
	}
	public String getDocumentDownload() {
		return documentDownload;
	}
	public void setDocumentDownload(String documentDownload) {
		this.documentDownload = documentDownload;
	}
	
	
}
