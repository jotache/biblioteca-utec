package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class BusquedaEmpleadosCommand {
	private String operacion;
	private String codAlumno;
	private String msg;
	private String grabar;
	private List listTipoPersonal;
	private String codTipoPersonal;
	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private List listEmpleados;
	private String posicion;
	private String sede;
	
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	public List getListTipoPersonal() {
		return listTipoPersonal;
	}
	public void setListTipoPersonal(List listTipoPersonal) {
		this.listTipoPersonal = listTipoPersonal;
	}
	public String getCodTipoPersonal() {
		return codTipoPersonal;
	}
	public void setCodTipoPersonal(String codTipoPersonal) {
		this.codTipoPersonal = codTipoPersonal;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApePaterno() {
		return apePaterno;
	}
	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}
	public String getApeMaterno() {
		return apeMaterno;
	}
	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}
	public List getListEmpleados() {
		return listEmpleados;
	}
	public void setListEmpleados(List listEmpleados) {
		this.listEmpleados = listEmpleados;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
}
