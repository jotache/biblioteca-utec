package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class RegistrarNumeroSeriesCommand {
	private String operacion;
	private String codUsuario;
	private String sede;
	private String codBien;
	private String codDocPago;
	private String cantEntregada;
	private String grabar;
	private List listSeleccion;
	private int totList;
	private int totReci;
	private int totPintar;
	private int falta;
	private String preUni;
	private String codIng;
	private String nroSerie;
	private String msg;
	private String codIngE;
	private String codDetalle;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	
	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}

	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}

	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}

	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}

	public String getCodDetalle() {
		return codDetalle;
	}

	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}

	public String getCodIngE() {
		return codIngE;
	}

	public void setCodIngE(String codIngE) {
		this.codIngE = codIngE;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodIng() {
		return codIng;
	}

	public void setCodIng(String codIng) {
		this.codIng = codIng;
	}

	public String getNroSerie() {
		return nroSerie;
	}

	public void setNroSerie(String nroSerie) {
		this.nroSerie = nroSerie;
	}

	public String getPreUni() {
		return preUni;
	}

	public void setPreUni(String preUni) {
		this.preUni = preUni;
	}

	public int getFalta() {
		return falta;
	}

	public void setFalta(int falta) {
		this.falta = falta;
	}

	public int getTotPintar() {
		return totPintar;
	}

	public void setTotPintar(int totPintar) {
		this.totPintar = totPintar;
	}

	public int getTotList() {
		return totList;
	}

	public void setTotList(int totList) {
		this.totList = totList;
	}

	public int getTotReci() {
		return totReci;
	}

	public void setTotReci(int totReci) {
		this.totReci = totReci;
	}

	public List getListSeleccion() {
		return listSeleccion;
	}

	public void setListSeleccion(List listSeleccion) {
		this.listSeleccion = listSeleccion;
	}

	public String getGrabar() {
		return grabar;
	}

	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}

	public String getCodBien() {
		return codBien;
	}

	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}

	public String getCodDocPago() {
		return codDocPago;
	}

	public void setCodDocPago(String codDocPago) {
		this.codDocPago = codDocPago;
	}

	public String getCantEntregada() {
		return cantEntregada;
	}

	public void setCantEntregada(String cantEntregada) {
		this.cantEntregada = cantEntregada;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
}
