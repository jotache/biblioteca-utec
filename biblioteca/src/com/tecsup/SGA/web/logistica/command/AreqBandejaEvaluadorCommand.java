package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class AreqBandejaEvaluadorCommand {

	private String operacion;
	private String msg;
	
	private String codSede;
	private String codUsuario;
	private String codOpciones;

	private String codTipoRequerimiento;
	private List<TipoTablaDetalle> listTipoRequerimiento;

	private String nroRequerimiento;
	private String fecInicio;
	private String fecFinal;
	
	private String codCeco;
	private List<CentroCosto> listCeco;	
	
	private String codEstado;
	private List<TipoTablaDetalle> listEstado;
	
	private List<SolRequerimiento> listaSolicitudes;
	
	private String consteCodBien;
	private String consteCodServicio; 
	private String banderaTipoReq;
	
	private String usuSolicitante;
	private String codPerfil;
	
	private String codTipoServicio;
	private List<TipoTablaDetalle> listaCodTipoServicio;
	
	private String indGrupo;
	private String valorIndGrupo;
	private String codIndRequerimiento;
		
	private List listaBien;
	private List listaServicio;
	
	private String consteGrupoGenerales;
	private String consteGrupoServicios;
	private String consteGrupoMantenimiento;
	private String banListaBien;
	private String banListaServicio;
	private String bandera;
	
	public String getBanListaBien() {
		return banListaBien;
	}

	public void setBanListaBien(String banListaBien) {
		this.banListaBien = banListaBien;
	}

	public String getBanListaServicio() {
		return banListaServicio;
	}

	public void setBanListaServicio(String banListaServicio) {
		this.banListaServicio = banListaServicio;
	}

	public String getConsteGrupoGenerales() {
		return consteGrupoGenerales;
	}

	public void setConsteGrupoGenerales(String consteGrupoGenerales) {
		this.consteGrupoGenerales = consteGrupoGenerales;
	}

	public String getConsteGrupoServicios() {
		return consteGrupoServicios;
	}

	public void setConsteGrupoServicios(String consteGrupoServicios) {
		this.consteGrupoServicios = consteGrupoServicios;
	}

	public String getConsteGrupoMantenimiento() {
		return consteGrupoMantenimiento;
	}

	public void setConsteGrupoMantenimiento(String consteGrupoMantenimiento) {
		this.consteGrupoMantenimiento = consteGrupoMantenimiento;
	}

	public List getListaBien() {
		return listaBien;
	}

	public void setListaBien(List listaBien) {
		this.listaBien = listaBien;
	}

	public List getListaServicio() {
		return listaServicio;
	}

	public void setListaServicio(List listaServicio) {
		this.listaServicio = listaServicio;
	}

	public String getCodIndRequerimiento() {
		return codIndRequerimiento;
	}

	public void setCodIndRequerimiento(String codIndRequerimiento) {
		this.codIndRequerimiento = codIndRequerimiento;
	}

	public String getValorIndGrupo() {
		return valorIndGrupo;
	}

	public void setValorIndGrupo(String valorIndGrupo) {
		this.valorIndGrupo = valorIndGrupo;
	}

	public String getCodTipoServicio() {
		return codTipoServicio;
	}

	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}

	public List<TipoTablaDetalle> getListaCodTipoServicio() {
		return listaCodTipoServicio;
	}

	public void setListaCodTipoServicio(List<TipoTablaDetalle> listaCodTipoServicio) {
		this.listaCodTipoServicio = listaCodTipoServicio;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getUsuSolicitante() {
		return usuSolicitante;
	}

	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}

	public String getBanderaTipoReq() {
		return banderaTipoReq;
	}

	public void setBanderaTipoReq(String banderaTipoReq) {
		this.banderaTipoReq = banderaTipoReq;
	}

	public String getConsteCodBien() {
		return consteCodBien;
	}

	public void setConsteCodBien(String consteCodBien) {
		this.consteCodBien = consteCodBien;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}

	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}

	public List<TipoTablaDetalle> getListTipoRequerimiento() {
		return listTipoRequerimiento;
	}

	public void setListTipoRequerimiento(
			List<TipoTablaDetalle> listTipoRequerimiento) {
		this.listTipoRequerimiento = listTipoRequerimiento;
	}

	public String getNroRequerimiento() {
		return nroRequerimiento;
	}

	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}

	public String getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}

	public String getFecFinal() {
		return fecFinal;
	}

	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}

	public String getCodCeco() {
		return codCeco;
	}

	public void setCodCeco(String codCeco) {
		this.codCeco = codCeco;
	}

	public List<CentroCosto> getListCeco() {
		return listCeco;
	}

	public void setListCeco(List<CentroCosto> listCeco) {
		this.listCeco = listCeco;
	}

	public String getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}

	public List<TipoTablaDetalle> getListEstado() {
		return listEstado;
	}

	public void setListEstado(List<TipoTablaDetalle> listEstado) {
		this.listEstado = listEstado;
	}

	public List<SolRequerimiento> getListaSolicitudes() {
		return listaSolicitudes;
	}

	public void setListaSolicitudes(List<SolRequerimiento> listaSolicitudes) {
		this.listaSolicitudes = listaSolicitudes;
	}
	
	public AreqBandejaEvaluadorCommand()
	{
		this.operacion = "";
		this.msg = "";
		this.codUsuario = "";
		this.codOpciones = "";
		this.codTipoRequerimiento = "";
		this.listTipoRequerimiento = null;
		this.nroRequerimiento = "";
		this.fecInicio = "";
		this.fecFinal = "";
		this.codCeco = "";
		this.listCeco = null;
		this.codEstado = "";
		this.listEstado = null;
		this.listaSolicitudes = null;
		this.indGrupo= "";
	}

	public String getConsteCodServicio() {
		return consteCodServicio;
	}

	public void setConsteCodServicio(String consteCodServicio) {
		this.consteCodServicio = consteCodServicio;
	}

	public String getIndGrupo() {
		return indGrupo;
	}

	public void setIndGrupo(String indGrupo) {
		this.indGrupo = indGrupo;
	}

	public String getBandera() {
		return bandera;
	}

	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
}
