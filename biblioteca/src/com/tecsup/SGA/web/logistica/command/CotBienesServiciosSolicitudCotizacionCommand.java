package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class CotBienesServiciosSolicitudCotizacionCommand {

	private String operacion;
	private String codUsuario;
	private String codOpciones;
	
	private String codSede;
	private String consteRadio;
	private String consteBien;
	
	private String consteServicio;
	private String consteBienConsumible;
	private String consteBienActivo;
	private String valRadio;
	private String indice;
	private String msg;
	
	private String nroCotizacion;
	private String fecInicio;
	private String fecFinal;
	private String codEstado;
	private List listaCodEstado;
	
	private String codTipoRequerimiento;
	private List listCodTipoRequerimiento;
	private String txtProveedor1;
	private String txtProveedor2;
	private List listaBandeja;
	
	private String codTipoServicio;
	private List listaCodTipoServicio;
	
	private String codTipoPago;
	private List listaCodTipoBien;
	private String codPerfil;
	private String codCotizacion;
	private String codOrdenCompra;
	private String codEstadoBD;
	private String codEstadoCerrado;
	private String banListaBandeja;
	private String nroCotizacionBD;
	private String dscUser;
	private String dscSede;
	private String dscTipoPago;
	private String codTipoPagoBD;
	
	private String bandera;
	private List ListaCodGrupoServicio;
	private String codGrupoServicio;
	//ALQD,30/01/09. AGREGANDO DOS PROPIEDADES QUE PERMITIRA FILTRAR COTs SIN OC
	private String valSelec1;
	private String consteCotSinOc;
	
	public String getValSelec1() {
		return valSelec1;
	}
	public void setValSelec1(String valSelec1) {
		this.valSelec1=valSelec1;
	}
	public String getConsteCotSinOc() {
		return consteCotSinOc;
	}
	public void setConsteCotSinOc(String consteCotSinOc) {
		this.consteCotSinOc=consteCotSinOc;
	}
	public List getListaCodGrupoServicio() {
		return ListaCodGrupoServicio;
	}
	public void setListaCodGrupoServicio(List listaCodGrupoServicio) {
		ListaCodGrupoServicio = listaCodGrupoServicio;
	}
	public String getCodGrupoServicio() {
		return codGrupoServicio;
	}
	public void setCodGrupoServicio(String codGrupoServicio) {
		this.codGrupoServicio = codGrupoServicio;
	}
	public String getDscSede() {
		return dscSede;
	}
	public void setDscSede(String dscSede) {
		this.dscSede = dscSede;
	}
	public String getDscUser() {
		return dscUser;
	}
	public void setDscUser(String dscUser) {
		this.dscUser = dscUser;
	}
	public String getNroCotizacionBD() {
		return nroCotizacionBD;
	}
	public void setNroCotizacionBD(String nroCotizacionBD) {
		this.nroCotizacionBD = nroCotizacionBD;
	}
	public String getBanListaBandeja() {
		return banListaBandeja;
	}
	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public List getListaCodTipoServicio() {
		return listaCodTipoServicio;
	}
	public void setListaCodTipoServicio(List listaCodTipoServicio) {
		this.listaCodTipoServicio = listaCodTipoServicio;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getConsteRadio() {
		return consteRadio;
	}
	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}
	public String getConsteBien() {
		return consteBien;
	}
	public void setConsteBien(String consteBien) {
		this.consteBien = consteBien;
	}
	public String getConsteServicio() {
		return consteServicio;
	}
	public void setConsteServicio(String consteServicio) {
		this.consteServicio = consteServicio;
	}
	public String getConsteBienConsumible() {
		return consteBienConsumible;
	}
	public void setConsteBienConsumible(String consteBienConsumible) {
		this.consteBienConsumible = consteBienConsumible;
	}
	public String getConsteBienActivo() {
		return consteBienActivo;
	}
	public void setConsteBienActivo(String consteBienActivo) {
		this.consteBienActivo = consteBienActivo;
	}
	public String getValRadio() {
		return valRadio;
	}
	public void setValRadio(String valRadio) {
		this.valRadio = valRadio;
	}
	public String getIndice() {
		return indice;
	}
	public void setIndice(String indice) {
		this.indice = indice;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNroCotizacion() {
		return nroCotizacion;
	}
	public void setNroCotizacion(String nroCotizacion) {
		this.nroCotizacion = nroCotizacion;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public List getListaCodEstado() {
		return listaCodEstado;
	}
	public void setListaCodEstado(List listaCodEstado) {
		this.listaCodEstado = listaCodEstado;
	}
	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}
	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}
	public List getListCodTipoRequerimiento() {
		return listCodTipoRequerimiento;
	}
	public void setListCodTipoRequerimiento(List listCodTipoRequerimiento) {
		this.listCodTipoRequerimiento = listCodTipoRequerimiento;
	}
	public String getTxtProveedor1() {
		return txtProveedor1;
	}
	public void setTxtProveedor1(String txtProveedor1) {
		this.txtProveedor1 = txtProveedor1;
	}
	public String getTxtProveedor2() {
		return txtProveedor2;
	}
	public void setTxtProveedor2(String txtProveedor2) {
		this.txtProveedor2 = txtProveedor2;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodTipoPago() {
		return codTipoPago;
	}
	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	public List getListaCodTipoBien() {
		return listaCodTipoBien;
	}
	public void setListaCodTipoBien(List listaCodTipoBien) {
		this.listaCodTipoBien = listaCodTipoBien;
	}
	public String getCodOrdenCompra() {
		return codOrdenCompra;
	}
	public void setCodOrdenCompra(String codOrdenCompra) {
		this.codOrdenCompra = codOrdenCompra;
	}
	public String getCodEstadoBD() {
		return codEstadoBD;
	}
	public void setCodEstadoBD(String codEstadoBD) {
		this.codEstadoBD = codEstadoBD;
	}
	public String getCodEstadoCerrado() {
		return codEstadoCerrado;
	}
	public void setCodEstadoCerrado(String codEstadoCerrado) {
		this.codEstadoCerrado = codEstadoCerrado;
	}
	public String getBandera() {
		return bandera;
	}
	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
	public String getDscTipoPago() {
		return dscTipoPago;
	}
	public void setDscTipoPago(String dscTipoPago) {
		this.dscTipoPago = dscTipoPago;
	}
	public String getCodTipoPagoBD() {
		return codTipoPagoBD;
	}
	public void setCodTipoPagoBD(String codTipoPagoBD) {
		this.codTipoPagoBD = codTipoPagoBD;
	}
}
