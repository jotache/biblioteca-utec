package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoSubFamiliaCommand {
	private String operacion;
	private List listTipoBien;
	private List listFamilia;
	private String codTipo;
	private String codFam;
	private String secuencial;
	private String descripcion;
	private List listSubFamilia;
	private String codUnico;
	private String codGen;
	private String desSubFam;
	private String desFam;
	private String codAlumno;
	private String eliminar;
	private String msg;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getEliminar() {
		return eliminar;
	}
	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getCodGen() {
		return codGen;
	}
	public void setCodGen(String codGen) {
		this.codGen = codGen;
	}
	public String getDesSubFam() {
		return desSubFam;
	}
	public void setDesSubFam(String desSubFam) {
		this.desSubFam = desSubFam;
	}
	public String getDesFam() {
		return desFam;
	}
	public void setDesFam(String desFam) {
		this.desFam = desFam;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public List getListSubFamilia() {
		return listSubFamilia;
	}
	public void setListSubFamilia(List listSubFamilia) {
		this.listSubFamilia = listSubFamilia;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListTipoBien() {
		return listTipoBien;
	}
	public void setListTipoBien(List listTipoBien) {
		this.listTipoBien = listTipoBien;
	}
	public List getListFamilia() {
		return listFamilia;
	}
	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public String getCodFam() {
		return codFam;
	}
	public void setCodFam(String codFam) {
		this.codFam = codFam;
	}
	
	
}
