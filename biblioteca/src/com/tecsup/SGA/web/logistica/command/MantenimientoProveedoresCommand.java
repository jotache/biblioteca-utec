package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoProveedoresCommand {
	private String operacion;
	private List listCalificacion;
	private List listEstado;
	private List listProveedor;
	private String ruc;
	private String razonSocial;
	private String codCalif;
	private String codEstado;
	private String codAlumno;
	private String codUnico;
	private String eliminar;
	private String msg;
	private List listprov;
	private String codProv;
	
	public String getCodProv() {
		return codProv;
	}
	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}
	public List getListprov() {
		return listprov;
	}
	public void setListprov(List listprov) {
		this.listprov = listprov;
	}
	public String getEliminar() {
		return eliminar;
	}
	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodCalif() {
		return codCalif;
	}
	public void setCodCalif(String codCalif) {
		this.codCalif = codCalif;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListCalificacion() {
		return listCalificacion;
	}
	public void setListCalificacion(List listCalificacion) {
		this.listCalificacion = listCalificacion;
	}
	public List getListEstado() {
		return listEstado;
	}
	public void setListEstado(List listEstado) {
		this.listEstado = listEstado;
	}
	public List getListProveedor() {
		return listProveedor;
	}
	public void setListProveedor(List listProveedor) {
		this.listProveedor = listProveedor;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	
	
}
