package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class PenBandejaAtencionCotizacionCommand {
	
	private String operacion;
	private String codUsuario;
	private String valConsumible;
	private String valActivo;
	
	private String codTipoRequerimiento;
	private List listCodTipoRequerimiento;
	
	private String consteConsumible;
	private String consteActivo;
	private String txtNroRequerimiento;

	private String codFamilia;
	private String codSubFamilia;
	private List listaCodSubFamilia;
	private List listaCodFamilia;
	
	
	private String codGrupoServicio;
	private List listaCodGrupoServicio;
	
	private String codTipoServicio;
	private List listaCodTipoServicio;
	
	private String codCentroCosto;
	private List listaCodCentroCosto;
	private String txtUsuSolicitante;
	
	private String codOpciones;
	private String codSede;
	private String consteRadio;
	private String consteServicio;
	private String consteBien;
	private String consteBienConsumible;
	private String consteBienActivo;
	private String valRadio;
	
	private List listaBienesConsumibles;
	private List listaBienesActivos;
	private List listaServicios;
	
	private String codDetalle;
	private String codRequerimiento;
	
	private String txtDescripcion;
	private String txtComentario;
	
	private String cadCodigos;
	private String nroCodigos;
	
	private String msg;
	private String indice;
	
	private String valCotActual;
	private String banListaBienesConsumibles;
	private String banListaBienesActivos;
	private String banListaServicios;
	
	/******************** Parametros a devolver ala Bandeja ******************/
	private String bandera;
	private String valSelec;
	private String codPerfil;
	
	private String indInversion;//jhpr 2008-09-30
	//ALQD,12/08/09. NUEVA PROPIEDADES
	private String cantPendConsNormal;
	private String cantPendConsInversion;
	private String cantPendActivo;
	private String cantPendServGeneral;
	private String cantPendServMantto;
	private String cantPendServOtro;
	
	public String getCantPendConsNormal() {
		return cantPendConsNormal;
	}
	public void setCantPendConsNormal(String cantPendConsNormal) {
		this.cantPendConsNormal = cantPendConsNormal;
	}
	public String getCantPendConsInversion() {
		return cantPendConsInversion;
	}
	public void setCantPendConsInversion(String cantPendConsInversion) {
		this.cantPendConsInversion = cantPendConsInversion;
	}
	public String getCantPendActivo() {
		return cantPendActivo;
	}
	public void setCantPendActivo(String cantPendActivo) {
		this.cantPendActivo= cantPendActivo;
	}
	public String getCantPendServGeneral() {
		return cantPendServGeneral;
	}
	public void setCantPendServGeneral(String cantPendServGeneral) {
		this.cantPendServGeneral= cantPendServGeneral;
	}
	public String getCantPendServMantto() {
		return cantPendServMantto;
	}
	public void setCantPendServMantto(String cantPendServMantto) {
		this.cantPendServMantto= cantPendServMantto;
	}
	public String getCantPendServOtro() {
		return cantPendServOtro;
	}
	public void setCantPendServOtro(String cantPendServOtro) {
		this.cantPendServOtro= cantPendServOtro;
	}
	public String getIndInversion() {
		return indInversion;
	}
	public void setIndInversion(String indInversion) {
		this.indInversion = indInversion;
	}
	public String getValSelec() {
		return valSelec;
	}
	public void setValSelec(String valSelec) {
		this.valSelec = valSelec;
	}
	public String getBandera() {
		return bandera;
	}
	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
	public String getBanListaBienesConsumibles() {
		return banListaBienesConsumibles;
	}
	public void setBanListaBienesConsumibles(String banListaBienesConsumibles) {
		this.banListaBienesConsumibles = banListaBienesConsumibles;
	}
	public String getBanListaBienesActivos() {
		return banListaBienesActivos;
	}
	public void setBanListaBienesActivos(String banListaBienesActivos) {
		this.banListaBienesActivos = banListaBienesActivos;
	}
	public String getBanListaServicios() {
		return banListaServicios;
	}
	public void setBanListaServicios(String banListaServicios) {
		this.banListaServicios = banListaServicios;
	}
	public String getValCotActual() {
		return valCotActual;
	}
	public void setValCotActual(String valCotActual) {
		this.valCotActual = valCotActual;
	}
	public String getIndice() {
		return indice;
	}
	public void setIndice(String indice) {
		this.indice = indice;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCadCodigos() {
		return cadCodigos;
	}
	public void setCadCodigos(String cadCodigos) {
		this.cadCodigos = cadCodigos;
	}
	public String getNroCodigos() {
		return nroCodigos;
	}
	public void setNroCodigos(String nroCodigos) {
		this.nroCodigos = nroCodigos;
	}
	public String getTxtDescripcion() {
		return txtDescripcion;
	}
	public void setTxtDescripcion(String txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}
	public String getTxtComentario() {
		return txtComentario;
	}
	public void setTxtComentario(String txtComentario) {
		this.txtComentario = txtComentario;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public List getListaBienesConsumibles() {
		return listaBienesConsumibles;
	}
	public void setListaBienesConsumibles(List listaBienesConsumibles) {
		this.listaBienesConsumibles = listaBienesConsumibles;
	}
	public List getListaBienesActivos() {
		return listaBienesActivos;
	}
	public void setListaBienesActivos(List listaBienesActivos) {
		this.listaBienesActivos = listaBienesActivos;
	}
	public List getListaServicios() {
		return listaServicios;
	}
	public void setListaServicios(List listaServicios) {
		this.listaServicios = listaServicios;
	}
	public String getValRadio() {
		return valRadio;
	}
	public void setValRadio(String valRadio) {
		this.valRadio = valRadio;
	}
	public String getConsteServicio() {
		return consteServicio;
	}
	public void setConsteServicio(String consteServicio) {
		this.consteServicio = consteServicio;
	}
	public String getConsteBien() {
		return consteBien;
	}
	public void setConsteBien(String consteBien) {
		this.consteBien = consteBien;
	}
	public String getConsteRadio() {
		return consteRadio;
	}
	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getValConsumible() {
		return valConsumible;
	}
	public void setValConsumible(String valConsumible) {
		this.valConsumible = valConsumible;
	}
	public String getValActivo() {
		return valActivo;
	}
	public void setValActivo(String valActivo) {
		this.valActivo = valActivo;
	}
	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}
	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}
	public List getListCodTipoRequerimiento() {
		return listCodTipoRequerimiento;
	}
	public void setListCodTipoRequerimiento(List listCodTipoRequerimiento) {
		this.listCodTipoRequerimiento = listCodTipoRequerimiento;
	}
	public String getConsteConsumible() {
		return consteConsumible;
	}
	public void setConsteConsumible(String consteConsumible) {
		this.consteConsumible = consteConsumible;
	}
	public String getConsteActivo() {
		return consteActivo;
	}
	public void setConsteActivo(String consteActivo) {
		this.consteActivo = consteActivo;
	}
	public String getTxtNroRequerimiento() {
		return txtNroRequerimiento;
	}
	public void setTxtNroRequerimiento(String txtNroRequerimiento) {
		this.txtNroRequerimiento = txtNroRequerimiento;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public List getListaCodSubFamilia() {
		return listaCodSubFamilia;
	}
	public void setListaCodSubFamilia(List listaCodSubFamilia) {
		this.listaCodSubFamilia = listaCodSubFamilia;
	}
	public String getCodGrupoServicio() {
		return codGrupoServicio;
	}
	public void setCodGrupoServicio(String codGrupoServicio) {
		this.codGrupoServicio = codGrupoServicio;
	}
	public List getListaCodGrupoServicio() {
		return listaCodGrupoServicio;
	}
	public void setListaCodGrupoServicio(List listaCodGrupoServicio) {
		this.listaCodGrupoServicio = listaCodGrupoServicio;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public List getListaCodTipoServicio() {
		return listaCodTipoServicio;
	}
	public void setListaCodTipoServicio(List listaCodTipoServicio) {
		this.listaCodTipoServicio = listaCodTipoServicio;
	}
	public String getCodCentroCosto() {
		return codCentroCosto;
	}
	public void setCodCentroCosto(String codCentroCosto) {
		this.codCentroCosto = codCentroCosto;
	}
	public List getListaCodCentroCosto() {
		return listaCodCentroCosto;
	}
	public void setListaCodCentroCosto(List listaCodCentroCosto) {
		this.listaCodCentroCosto = listaCodCentroCosto;
	}
	public String getTxtUsuSolicitante() {
		return txtUsuSolicitante;
	}
	public void setTxtUsuSolicitante(String txtUsuSolicitante) {
		this.txtUsuSolicitante = txtUsuSolicitante;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	public List getListaCodFamilia() {
		return listaCodFamilia;
	}
	public void setListaCodFamilia(List listaCodFamilia) {
		this.listaCodFamilia = listaCodFamilia;
	}
	public String getConsteBienConsumible() {
		return consteBienConsumible;
	}
	public void setConsteBienConsumible(String consteBienConsumible) {
		this.consteBienConsumible = consteBienConsumible;
	}
	public String getConsteBienActivo() {
		return consteBienActivo;
	}
	public void setConsteBienActivo(String consteBienActivo) {
		this.consteBienActivo = consteBienActivo;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
}
