package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarFlujoCommand {
	private String operacion;
	private List listPersonal;
	private String codPer;
	private String nombre;
	private String apPat;
	private String apMat;
	private String codAlumno;
	private String codigo;
	private String codReq;
	private List listEmpleados;
	private String codEmpleados;
	private String msg;
	private String graba;
	private String codResponsable;
	private String codSede;
	
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListPersonal() {
		return listPersonal;
	}
	public void setListPersonal(List listPersonal) {
		this.listPersonal = listPersonal;
	}
	public String getCodPer() {
		return codPer;
	}
	public void setCodPer(String codPer) {
		this.codPer = codPer;
	}
	public String getApPat() {
		return apPat;
	}
	public void setApPat(String apPat) {
		this.apPat = apPat;
	}
	public String getApMat() {
		return apMat;
	}
	public void setApMat(String apMat) {
		this.apMat = apMat;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public List getListEmpleados() {
		return listEmpleados;
	}
	public void setListEmpleados(List listEmpleados) {
		this.listEmpleados = listEmpleados;
	}
	public String getCodEmpleados() {
		return codEmpleados;
	}
	public void setCodEmpleados(String codEmpleados) {
		this.codEmpleados = codEmpleados;
	}
	
	
}
