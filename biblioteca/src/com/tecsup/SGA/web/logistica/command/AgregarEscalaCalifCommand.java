package com.tecsup.SGA.web.logistica.command;

public class AgregarEscalaCalifCommand {
		private String operacion;
		private String calificacion;
		public String getOperacion() {
			return operacion;
		}
		public void setOperacion(String operacion) {
			this.operacion = operacion;
		}
		public String getCalificacion() {
			return calificacion;
		}
		public void setCalificacion(String calificacion) {
			this.calificacion = calificacion;
		}
		

}
