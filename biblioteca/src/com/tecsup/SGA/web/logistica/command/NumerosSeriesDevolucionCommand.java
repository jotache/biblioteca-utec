package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class NumerosSeriesDevolucionCommand {
	private String codUsuario;
	private String codBien;
	private List listaNumero;
	private String codReq;
	private String codReqDet;
	private String codDev;
	private String codDevDet;
	private String codCadIngresados;
	private String grabar;
	private String msg;
	private String cantidad;
	private String operacion;
	private String tipo;
	private int total;
	private String banListaBandeja;
	
	public String getBanListaBandeja() {
		return banListaBandeja;
	}
	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getCodCadIngresados() {
		return codCadIngresados;
	}
	public void setCodCadIngresados(String codCadIngresados) {
		this.codCadIngresados = codCadIngresados;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List getListaNumero() {
		return listaNumero;
	}
	public void setListaNumero(List listaNumero) {
		this.listaNumero = listaNumero;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getCodReqDet() {
		return codReqDet;
	}
	public void setCodReqDet(String codReqDet) {
		this.codReqDet = codReqDet;
	}
	public String getCodDev() {
		return codDev;
	}
	public void setCodDev(String codDev) {
		this.codDev = codDev;
	}
	public String getCodDevDet() {
		return codDevDet;
	}
	public void setCodDevDet(String codDevDet) {
		this.codDevDet = codDevDet;
	}
	
}
