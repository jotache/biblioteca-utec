package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class RecepcionBienesCommand {
	private String operacion;
	private String codUsuario;
	private List listCentroCostos;
	private String codCentroCostos;
	private String fechaEmision;
	private String fechaEmision2;
	private String nroOrden;
	private String codProveedor;
	private String proveedor;
	private String comprador;
	private String nroGuia;
	private String fechaEmision3;
	private String fechaEmision4;
	private String sede;
	private String codOrden;
	
	private String codOpciones;
	private String codPerfil;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	//ALQD,23/01/09. NUEVA PROPIEDAD PARA FILTRAR LOS PENDIENTE DE CONFIRMAR
	private String indIngPorConfirmar;
	private String consteIndIngPorConfirmar;
	
	public String getIndIngPorConfirmar() {
		return indIngPorConfirmar;
	}

	public void setIndIngPorConfirmar(String indIngPorConfirmar) {
		this.indIngPorConfirmar = indIngPorConfirmar;
	}

	public String getConsteIndIngPorConfirmar() {
		return consteIndIngPorConfirmar;
	}

	public void setConsteIndIngPorConfirmar(String consteIndIngPorConfirmar) {
		this.consteIndIngPorConfirmar = consteIndIngPorConfirmar;
	}

	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}

	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}

	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}

	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}

	public String getCodOrden() {
		return codOrden;
	}

	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public List getListCentroCostos() {
		return listCentroCostos;
	}

	public void setListCentroCostos(List listCentroCostos) {
		this.listCentroCostos = listCentroCostos;
	}

	public String getCodCentroCostos() {
		return codCentroCostos;
	}

	public void setCodCentroCostos(String codCentroCostos) {
		this.codCentroCostos = codCentroCostos;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getFechaEmision2() {
		return fechaEmision2;
	}

	public void setFechaEmision2(String fechaEmision2) {
		this.fechaEmision2 = fechaEmision2;
	}

	public String getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}

	public String getCodProveedor() {
		return codProveedor;
	}

	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getComprador() {
		return comprador;
	}

	public void setComprador(String comprador) {
		this.comprador = comprador;
	}

	public String getNroGuia() {
		return nroGuia;
	}

	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}

	public String getFechaEmision3() {
		return fechaEmision3;
	}

	public void setFechaEmision3(String fechaEmision3) {
		this.fechaEmision3 = fechaEmision3;
	}

	public String getFechaEmision4() {
		return fechaEmision4;
	}

	public void setFechaEmision4(String fechaEmision4) {
		this.fechaEmision4 = fechaEmision4;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	
	
}
