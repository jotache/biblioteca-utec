package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class BusquedaActivosCommand {
	private String operacion;
	private String codAlumno;
	private String familia;
	private String subFamilia;
	private String producto;
	private List listProductos;
	private String msg;
	private String grabar;
	private String codProducto;
	private String sede;
	private String posicion;
	
	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getGrabar() {
		return grabar;
	}

	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}

	public String getFamilia() {
		return familia;
	}

	public void setFamilia(String familia) {
		this.familia = familia;
	}

	public String getSubFamilia() {
		return subFamilia;
	}

	public void setSubFamilia(String subFamilia) {
		this.subFamilia = subFamilia;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public List getListProductos() {
		return listProductos;
	}

	public void setListProductos(List listProductos) {
		this.listProductos = listProductos;
	}

	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
}
