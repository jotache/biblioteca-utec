package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class RecBieRegistrarBienesCommand {

	private String operacion;
	private String msg;
	private String codSede;
	private String codUsuario;
	private String codProveedor;
	private String codPerfil;
	private String codOpciones;
	private String codOrden;
	private String codEstado;
	private String nroOrden;
	private String fecEmision;
	private String txtNroRucProveedor;
	private String txtAtencion;
	private String txtMonto;
	private String txtComprador;
	private String codDocumento;
	private String txtNombreProveedor;
	private String indiceModificacion;
	
	private List listaSuperior;
	private List listaBandeja;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	
	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}
	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}
	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}
	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getCodDocumento() {
		return codDocumento;
	}
	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}
	public List getListaSuperior() {
		return listaSuperior;
	}
	public void setListaSuperior(List listaSuperior) {
		this.listaSuperior = listaSuperior;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFecEmision() {
		return fecEmision;
	}
	public void setFecEmision(String fecEmision) {
		this.fecEmision = fecEmision;
	}
	public String getTxtNroRucProveedor() {
		return txtNroRucProveedor;
	}
	public void setTxtNroRucProveedor(String txtNroRucProveedor) {
		this.txtNroRucProveedor = txtNroRucProveedor;
	}
	public String getTxtAtencion() {
		return txtAtencion;
	}
	public void setTxtAtencion(String txtAtencion) {
		this.txtAtencion = txtAtencion;
	}
	public String getTxtMonto() {
		return txtMonto;
	}
	public void setTxtMonto(String txtMonto) {
		this.txtMonto = txtMonto;
	}
	public String getTxtComprador() {
		return txtComprador;
	}
	public void setTxtComprador(String txtComprador) {
		this.txtComprador = txtComprador;
	}
	public String getTxtNombreProveedor() {
		return txtNombreProveedor;
	}
	public void setTxtNombreProveedor(String txtNombreProveedor) {
		this.txtNombreProveedor = txtNombreProveedor;
	}
	public String getIndiceModificacion() {
		return indiceModificacion;
	}
	public void setIndiceModificacion(String indiceModificacion) {
		this.indiceModificacion = indiceModificacion;
	}
	
}
