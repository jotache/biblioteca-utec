package com.tecsup.SGA.web.logistica.command;

import java.util.List;

import com.tecsup.SGA.bean.ProductoByItemBean;

public class CotDetBandejaProdByItemCommand {
	
	private String operacion;
	private String msg;
	private String codCotizacion;
	private String codCotizacionDet;
	private String codTipoCotizacion;
	private String codSubTipoCotizacion;
	private String condicion;
	List<ProductoByItemBean> listaProductos;
	private String codReq;
	private String codReqDet;
	private String codUsuario;
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCodCotizacionDet() {
		return codCotizacionDet;
	}
	public void setCodCotizacionDet(String codCotizacionDet) {
		this.codCotizacionDet = codCotizacionDet;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public List<ProductoByItemBean> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<ProductoByItemBean> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getCodReqDet() {
		return codReqDet;
	}
	public void setCodReqDet(String codReqDet) {
		this.codReqDet = codReqDet;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodTipoCotizacion() {
		return codTipoCotizacion;
	}
	public void setCodTipoCotizacion(String codTipoCotizacion) {
		this.codTipoCotizacion = codTipoCotizacion;
	}
	public String getCodSubTipoCotizacion() {
		return codSubTipoCotizacion;
	}
	public void setCodSubTipoCotizacion(String codSubTipoCotizacion) {
		this.codSubTipoCotizacion = codSubTipoCotizacion;
	}
}
