package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class NotasSeguimientoCommand {
	private String operacion;
	private String msg;
	private String seleccion;
	private String codUsuario;
	private String codSede;
	private String codRequerimiento;	
	private String codNota;
	
	private List listaNotas;
	private String tamListaNotas;	
	
	public String getTamListaNotas() {
		return tamListaNotas;
	}
	public void setTamListaNotas(String tamListaNotas) {
		this.tamListaNotas = tamListaNotas;
	}
	public String getCodNota() {
		return codNota;
	}
	public void setCodNota(String codNota) {
		this.codNota = codNota;
	}
	public List getListaNotas() {
		return listaNotas;
	}
	public void setListaNotas(List listaNotas) {
		this.listaNotas = listaNotas;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
}
