package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class BusquedaProductosCommand {	
	private String operacion;
	private String codAlumno;
	private String descripcion;
	private String codigo;
	private List listFamilia;
	private String codFamilia;
	private List listSubFamilia;
	private String codSubFamilia;
	private List listProducto;
	private String descTecnica;
	private List listTipoAdjunto;
	private String sede;
	private String graba;
	private String codReq;
	private String codProducto;
	private String msg;
	private String des;
	private String codProducto1;
	/*Modificado CCORDOVA*/
	private String codTipoBien;
	private int nopo;
	
	public String getCodTipoBien() {
		return codTipoBien;
	}

	public void setCodTipoBien(String codTipoBien) {
		this.codTipoBien = codTipoBien;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodReq() {
		return codReq;
	}

	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}

	public String getGraba() {
		return graba;
	}

	public void setGraba(String graba) {
		this.graba = graba;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public List getListSubFamilia() {
		return listSubFamilia;
	}

	public void setListSubFamilia(List listSubFamilia) {
		this.listSubFamilia = listSubFamilia;
	}

	public String getCodSubFamilia() {
		return codSubFamilia;
	}

	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}

	public List getListProducto() {
		return listProducto;
	}

	public void setListProducto(List listProducto) {
		this.listProducto = listProducto;
	}

	public String getDescTecnica() {
		return descTecnica;
	}

	public void setDescTecnica(String descTecnica) {
		this.descTecnica = descTecnica;
	}

	public List getListTipoAdjunto() {
		return listTipoAdjunto;
	}

	public void setListTipoAdjunto(List listTipoAdjunto) {
		this.listTipoAdjunto = listTipoAdjunto;
	}

	public List getListFamilia() {
		return listFamilia;
	}

	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}

	public String getCodFamilia() {
		return codFamilia;
	}

	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getCodProducto1() {
		return codProducto1;
	}

	public void setCodProducto1(String codProducto1) {
		this.codProducto1 = codProducto1;
	}

	

	public int getNopo() {
		return nopo;
	}

	public void setNopo(int nopo) {
		this.nopo = nopo;
	}
}
