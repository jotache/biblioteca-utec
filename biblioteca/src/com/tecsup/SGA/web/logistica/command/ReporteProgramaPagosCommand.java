package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ReporteProgramaPagosCommand {

	private String operacion;
	private String codUsuario;
	private String codSede;
	private String codTipoReporte;
	private List listaSedes;
	private String fecInicio;
	private String fecFinal;
	private String proveedor;
	private String codTipoPago;
	private List listaCodTipoPago;
	private String codEstado;
	private List listaCodEstado;
	private String codTipoOrden;
	private List listaCodTipoOrden;
	private String dscUsuario;
	private String consteRadio;
	private String consteValidado;
	private String consteVencido;
	private String consteSede;
	
	public String getConsteRadio() {
		return consteRadio;
	}
	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}
	public String getConsteValidado() {
		return consteValidado;
	}
	public void setConsteValidado(String consteValidado) {
		this.consteValidado = consteValidado;
	}
	public String getConsteVencido() {
		return consteVencido;
	}
	public void setConsteVencido(String consteVencido) {
		this.consteVencido = consteVencido;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodTipoReporte() {
		return codTipoReporte;
	}
	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}
	public List getListaSedes() {
		return listaSedes;
	}
	public void setListaSedes(List listaSedes) {
		this.listaSedes = listaSedes;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getCodTipoPago() {
		return codTipoPago;
	}
	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	
	public List getListaCodTipoPago() {
		return listaCodTipoPago;
	}
	public void setListaCodTipoPago(List listaCodTipoPago) {
		this.listaCodTipoPago = listaCodTipoPago;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public List getListaCodEstado() {
		return listaCodEstado;
	}
	public void setListaCodEstado(List listaCodEstado) {
		this.listaCodEstado = listaCodEstado;
	}
	public String getCodTipoOrden() {
		return codTipoOrden;
	}
	public void setCodTipoOrden(String codTipoOrden) {
		this.codTipoOrden = codTipoOrden;
	}
	public List getListaCodTipoOrden() {
		return listaCodTipoOrden;
	}
	public void setListaCodTipoOrden(List listaCodTipoOrden) {
		this.listaCodTipoOrden = listaCodTipoOrden;
	}
	public String getConsteSede() {
		return consteSede;
	}
	public void setConsteSede(String consteSede) {
		this.consteSede = consteSede;
	}
	
	
	
}
