package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarProveedorCommand {
	private String operacion;
	private List listCalificacion;
	private List listProveedor;
	private List listGiro;
	private List listEstado;
	private List listmoneda;
	private List listaDepartamento;
	private List listProvincia;
	private List listDistrito;
	private List listFamSubFam;
	
	private String codGraba;
	private String codCalif;
	private String codProv;
	private String codGiro;
	private String codEstado;
	private String codMoneda;
	private String tipDoc;
	private String nroDoc;
	private String razonSocial;
	private String nomCorto;
	private String correo;
	private String direccion;
	private String atencion;
	private String banco;
	private String nroCta;
	private String tipoCta;
	private String codDepa;
	private String codProvincia;
	private String codDis;
	private String msg;
	private String flag;
	private String codAlumno;
	private String codUnico;
	private String tipo;
	private String mssg;
	private String codSubFamilia;
	//ALQD,03/07/09.NUEVAS PROPIEDADES
	private String pais;
	private String telefono;
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getMssg() {
		return mssg;
	}
	public void setMssg(String mssg) {
		this.mssg = mssg;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodCalif() {
		return codCalif;
	}
	public void setCodCalif(String codCalif) {
		this.codCalif = codCalif;
	}
	
	public String getCodGiro() {
		return codGiro;
	}
	public void setCodGiro(String codGiro) {
		this.codGiro = codGiro;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getCodMoneda() {
		return codMoneda;
	}
	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListCalificacion() {
		return listCalificacion;
	}
	public void setListCalificacion(List listCalificacion) {
		this.listCalificacion = listCalificacion;
	}
	public List getListProveedor() {
		return listProveedor;
	}
	public void setListProveedor(List listProveedor) {
		this.listProveedor = listProveedor;
	}
	public List getListGiro() {
		return listGiro;
	}
	public void setListGiro(List listGiro) {
		this.listGiro = listGiro;
	}
	public List getListEstado() {
		return listEstado;
	}
	public void setListEstado(List listEstado) {
		this.listEstado = listEstado;
	}
	public List getListmoneda() {
		return listmoneda;
	}
	public void setListmoneda(List listmoneda) {
		this.listmoneda = listmoneda;
	}
	public String getTipDoc() {
		return tipDoc;
	}
	public void setTipDoc(String tipDoc) {
		this.tipDoc = tipDoc;
	}
	public String getNroDoc() {
		return nroDoc;
	}
	public void setNroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getNomCorto() {
		return nomCorto;
	}
	public void setNomCorto(String nomCorto) {
		this.nomCorto = nomCorto;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getAtencion() {
		return atencion;
	}
	public void setAtencion(String atencion) {
		this.atencion = atencion;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getNroCta() {
		return nroCta;
	}
	public void setNroCta(String nroCta) {
		this.nroCta = nroCta;
	}
	public String getTipoCta() {
		return tipoCta;
	}
	public void setTipoCta(String tipoCta) {
		this.tipoCta = tipoCta;
	}
	
	public String getCodDepa() {
		return codDepa;
	}
	public void setCodDepa(String codDepa) {
		this.codDepa = codDepa;
	}
	
	public List getListaDepartamento() {
		return listaDepartamento;
	}
	public void setListaDepartamento(List listaDepartamento) {
		this.listaDepartamento = listaDepartamento;
	}
	public List getListProvincia() {
		return listProvincia;
	}
	public void setListProvincia(List listProvincia) {
		this.listProvincia = listProvincia;
	}
	public List getListDistrito() {
		return listDistrito;
	}
	public void setListDistrito(List listDistrito) {
		this.listDistrito = listDistrito;
	}
	public String getCodDis() {
		return codDis;
	}
	public void setCodDis(String codDis) {
		this.codDis = codDis;
	}
	public String getCodProv() {
		return codProv;
	}
	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}
	public String getCodProvincia() {
		return codProvincia;
	}
	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}
	public String getCodGraba() {
		return codGraba;
	}
	public void setCodGraba(String codGraba) {
		this.codGraba = codGraba;
	}
	public List getListFamSubFam() {
		return listFamSubFam;
	}
	public void setListFamSubFam(List listFamSubFam) {
		this.listFamSubFam = listFamSubFam;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	
}
