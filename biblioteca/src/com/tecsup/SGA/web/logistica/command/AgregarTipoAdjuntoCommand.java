package com.tecsup.SGA.web.logistica.command;

public class AgregarTipoAdjuntoCommand {
	private String opracion;
	private String tipoAdj;
	private String graba;
	private String msg;
	private String operacion;
	private String codAlumno;
	private String secuencial;
	private String tipo;

	
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOpracion() {
		return opracion;
	}
	public void setOpracion(String opracion) {
		this.opracion = opracion;
	}
	public String getTipoAdj() {
		return tipoAdj;
	}
	public void setTipoAdj(String tipoAdj) {
		this.tipoAdj = tipoAdj;
	}

}
