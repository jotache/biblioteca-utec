package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class CotizacionBienesProveedorCommand {
	private String operacion;
	private String msg;	
	private String codUsuario;
	private String codSede;
	private String codCotizacion;
	private String codProveedor;
	
	private List listaBienesProveedor;
	private String numListaBienes;
	private List listaCondicionesProveedor;
	private String numListaCondiciones;
	//*******************************************	
	private String cadenaCodDetalle;
	private String cadenaDescripcion;
	private String cadenaPrecios;
	private String cantidad;
	//modificar 
	
	//PERFIL COMPRADOR
	private String flgComprador;
	private String cboProveedor;	
	private List listaProveedores;
	private String codEstCotizacion;
	private String codTipoReq;
	
	//FLAG DE COMPRADOR
	private String codDetalle;
	
	//Lista de monedas
	private String codMoneda;
	private List listMonedas;
	
	private String flagProv;
	
	private String igvBD;
	//ALQD,04/02/09.NUEVA PROPIEDAD
	private String numCotizacion;
	//ALQD,29/05/09.NUEVA PROPIEDAD
	private String tipoProveedor;
	private String otroCostos;
	
	public String getOtroCostos(){
		return otroCostos;
	}
	public void setOtroCostos(String otroCostos){
		this.otroCostos=otroCostos;
	}
	public String getTipoProveedor() {
		return tipoProveedor;
	}
	public void setTipoProveedor(String tipoProveedor) {
		this.tipoProveedor=tipoProveedor;
	}
	public String getNumCotizacion() {
		return numCotizacion;
	}
	public void setNumCotizacion(String numCotizacion) {
		this.numCotizacion=numCotizacion;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getCadenaPrecios() {
		return cadenaPrecios;
	}
	public void setCadenaPrecios(String cadenaPrecios) {
		this.cadenaPrecios = cadenaPrecios;
	}
	public List getListaCondicionesProveedor() {
		return listaCondicionesProveedor;
	}
	public void setListaCondicionesProveedor(List listaCondicionesProveedor) {
		this.listaCondicionesProveedor = listaCondicionesProveedor;
	}
	public List getListaBienesProveedor() {
		return listaBienesProveedor;
	}
	public void setListaBienesProveedor(List listaBienesProveedor) {
		this.listaBienesProveedor = listaBienesProveedor;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCadenaCodDetalle() {
		return cadenaCodDetalle;
	}
	public void setCadenaCodDetalle(String cadenaCodDetalle) {
		this.cadenaCodDetalle = cadenaCodDetalle;
	}
	public String getCadenaDescripcion() {
		return cadenaDescripcion;
	}
	public void setCadenaDescripcion(String cadenaDescripcion) {
		this.cadenaDescripcion = cadenaDescripcion;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getNumListaBienes() {
		return numListaBienes;
	}
	public void setNumListaBienes(String numListaBienes) {
		this.numListaBienes = numListaBienes;
	}
	public String getNumListaCondiciones() {
		return numListaCondiciones;
	}
	public void setNumListaCondiciones(String numListaCondiciones) {
		this.numListaCondiciones = numListaCondiciones;
	}
	public String getFlgComprador() {
		return flgComprador;
	}
	public void setFlgComprador(String flgComprador) {
		this.flgComprador = flgComprador;
	}
	public List getListaProveedores() {
		return listaProveedores;
	}
	public void setListaProveedores(List listaProveedores) {
		this.listaProveedores = listaProveedores;
	}
	public String getCboProveedor() {
		return cboProveedor;
	}
	public void setCboProveedor(String cboProveedor) {
		this.cboProveedor = cboProveedor;
	}
	public String getCodEstCotizacion() {
		return codEstCotizacion;
	}
	public void setCodEstCotizacion(String codEstCotizacion) {
		this.codEstCotizacion = codEstCotizacion;
	}
	public List getListMonedas() {
		return listMonedas;
	}
	public void setListMonedas(List listMonedas) {
		this.listMonedas = listMonedas;
	}
	public String getCodMoneda() {
		return codMoneda;
	}
	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}
	public String getCodTipoReq() {
		return codTipoReq;
	}
	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}
	public String getFlagProv() {
		return flagProv;
	}
	public void setFlagProv(String flagProv) {
		this.flagProv = flagProv;
	}
	public String getIgvBD() {
		return igvBD;
	}
	public void setIgvBD(String igvBD) {
		this.igvBD = igvBD;
	}
	
	
}
