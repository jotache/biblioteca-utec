package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoTipoGastoCommand {
	private String operacion;
	private List listGastos;
	private String codGastos;
	private String secuencial;
	private String desc;
	private String graba;
	private String msg;
	private String eliminar;
	private String codAlumno;
	private String codigo;
	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getEliminar() {
		return eliminar;
	}
	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListGastos() {
		return listGastos;
	}
	public void setListGastos(List listGastos) {
		this.listGastos = listGastos;
	}
	public String getCodGastos() {
		return codGastos;
	}
	public void setCodGastos(String codGastos) {
		this.codGastos = codGastos;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
