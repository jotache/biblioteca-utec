package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class GestDocPagoDocumentosPagoCommand {

	private String operacion;
	private String msg;
	private String codPerfil;
	private String codUsuario;
	private String codOpciones;
	private String codSede;
	private String nroOrden;
	private String fecInicio;
	private String fecFinal;
	private String nroRucProveedor;
	private String nombreProveedor;
	private String nroRucComprador;
	private String nombreComprador;
	private String codTipoOrden;
	private String valSelec1;
	private String valSelec2;
	private String consteSoloTipoPago;
	private String consteSoloDoc;
	
	private List listaBandeja;
	private List listaCodTipoOrden;
	
	private String codOrden;
	private String codEstado;
	private String indDocPago;
	private String fechaBD;
	private String banListaBandeja;
	private String codTipoReporte;
	private String dscUsuario;
	private String nomSede;
	//ALQD,13/11/08. A�ADIENDO PROPIEDAD PARA BUSCAR POR NUMFACTURA
	private String numFactura;
	//ALQD,21/01/09. A�ADIENDO NUEVO PARAMETRO DE FILTRO
	private String valSelec3;
	private String consteIngresoPendiente;
	
	public String getValSelec3() {
		return valSelec3;
	}

	public void setValSelec3(String valSelec3) {
		this.valSelec3 = valSelec3;
	}
	public String getConsteIngresoPendiente() {
		return consteIngresoPendiente;
	}

	public void setConsteIngresoPendiente(String consteIngPendiente) {
		this.consteIngresoPendiente = consteIngPendiente;
	}
	
	public String getNumFactura() {
		return numFactura;
	}
	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}
	
	public String getDscUsuario() {
		return dscUsuario;
	}

	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}

	public String getCodTipoReporte() {
		return codTipoReporte;
	}

	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}

	public String getBanListaBandeja() {
		return banListaBandeja;
	}

	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}

	public String getCodOrden() {
		return codOrden;
	}

	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}

	public String getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}

	public List getListaCodTipoOrden() {
		return listaCodTipoOrden;
	}

	public void setListaCodTipoOrden(List listaCodTipoOrden) {
		this.listaCodTipoOrden = listaCodTipoOrden;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}

	public String getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}

	public String getFecFinal() {
		return fecFinal;
	}

	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}

	public String getNroRucProveedor() {
		return nroRucProveedor;
	}

	public void setNroRucProveedor(String nroRucProveedor) {
		this.nroRucProveedor = nroRucProveedor;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getNroRucComprador() {
		return nroRucComprador;
	}

	public void setNroRucComprador(String nroRucComprador) {
		this.nroRucComprador = nroRucComprador;
	}

	public String getNombreComprador() {
		return nombreComprador;
	}

	public void setNombreComprador(String nombreComprador) {
		this.nombreComprador = nombreComprador;
	}

	public String getCodTipoOrden() {
		return codTipoOrden;
	}

	public void setCodTipoOrden(String codTipoOrden) {
		this.codTipoOrden = codTipoOrden;
	}

	public String getValSelec1() {
		return valSelec1;
	}

	public void setValSelec1(String valSelec1) {
		this.valSelec1 = valSelec1;
	}

	public String getValSelec2() {
		return valSelec2;
	}

	public void setValSelec2(String valSelec2) {
		this.valSelec2 = valSelec2;
	}

	public String getConsteSoloTipoPago() {
		return consteSoloTipoPago;
	}

	public void setConsteSoloTipoPago(String consteSoloTipoPago) {
		this.consteSoloTipoPago = consteSoloTipoPago;
	}

	public String getConsteSoloDoc() {
		return consteSoloDoc;
	}

	public void setConsteSoloDoc(String consteSoloDoc) {
		this.consteSoloDoc = consteSoloDoc;
	}

	public List getListaBandeja() {
		return listaBandeja;
	}

	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}

	public String getIndDocPago() {
		return indDocPago;
	}

	public void setIndDocPago(String indDocPago) {
		this.indDocPago = indDocPago;
	}

	public String getFechaBD() {
		return fechaBD;
	}

	public void setFechaBD(String fechaBD) {
		this.fechaBD = fechaBD;
	}

	public String getNomSede() {
		return nomSede;
	}

	public void setNomSede(String nomSede) {
		this.nomSede = nomSede;
	}

}
