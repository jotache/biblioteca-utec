package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoRolesFlujoCommand {
	private String operacion;
	private List listTipo;
	private String codTipo;
	private String codAlumno;
	private String item1;
	private String item2;
	private String item3;
	private String item4;
	private String item5;
	private String item6;
	private String item7;
	private String item8;
	private String item9;
	private String item10;
	private String item11;
	private String item12;
	private String item13;
	private String item14;
	private String item15;
	private String item16;
	private String item17;
	private String item18;
	private String item19;
	private String item20;
	private String item21;
	private String item22;
	private String item23;
	
	private String item111;
	private String item112;
	private String item113;
	private String item114;
	
	private String item121;
	private String item122;
	private String item123;
	private String item124;
	private String item125;
	private String item126;
	private String item127;
	private String item128;
	private String item129;
	private String item1210;
	private String item1211;
	private String item1212;
	private String item12113;
	private String item12114;
	private String msg;
	private String grabar;
	private List listObjetos;
	private List listSede;
	private String codSede;
	
	//agregados despues del cambio de ccordova
	private String item41;
	private String item42;
	private String item43;
	private String item44;
	
	//agregados 04/08/08
	private String item81;
	private String item82;
	private String item83;
	private String item84;
	
	//ALQD,09/10/08. AGREGANDO DOS PROPIEDADES MAS
	private String item85;
	private String item86;
	//ALQD,30/01/09. AGREGANDO UNA PROPIEDAD MAS
	private String item87;
	
	public String getItem87() {
		return item87;
	}
	public void setItem87(String item87) {
		this.item87=item87;
	}
	public List getListSede() {
		return listSede;
	}
	public void setListSede(List listSede) {
		this.listSede = listSede;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListObjetos() {
		return listObjetos;
	}
	public void setListObjetos(List listObjetos) {
		this.listObjetos = listObjetos;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListTipo() {
		return listTipo;
	}
	public void setListTipo(List listTipo) {
		this.listTipo = listTipo;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public String getItem1() {
		return item1;
	}
	public void setItem1(String item1) {
		this.item1 = item1;
	}
	public String getItem2() {
		return item2;
	}
	public void setItem2(String item2) {
		this.item2 = item2;
	}
	public String getItem3() {
		return item3;
	}
	public void setItem3(String item3) {
		this.item3 = item3;
	}
	public String getItem4() {
		return item4;
	}
	public void setItem4(String item4) {
		this.item4 = item4;
	}
	public String getItem5() {
		return item5;
	}
	public void setItem5(String item5) {
		this.item5 = item5;
	}
	public String getItem6() {
		return item6;
	}
	public void setItem6(String item6) {
		this.item6 = item6;
	}
	public String getItem7() {
		return item7;
	}
	public void setItem7(String item7) {
		this.item7 = item7;
	}
	public String getItem8() {
		return item8;
	}
	public void setItem8(String item8) {
		this.item8 = item8;
	}
	public String getItem9() {
		return item9;
	}
	public void setItem9(String item9) {
		this.item9 = item9;
	}
	public String getItem10() {
		return item10;
	}
	public void setItem10(String item10) {
		this.item10 = item10;
	}
	public String getItem11() {
		return item11;
	}
	public void setItem11(String item11) {
		this.item11 = item11;
	}
	public String getItem12() {
		return item12;
	}
	public void setItem12(String item12) {
		this.item12 = item12;
	}
	public String getItem13() {
		return item13;
	}
	public void setItem13(String item13) {
		this.item13 = item13;
	}
	public String getItem14() {
		return item14;
	}
	public void setItem14(String item14) {
		this.item14 = item14;
	}
	public String getItem15() {
		return item15;
	}
	public void setItem15(String item15) {
		this.item15 = item15;
	}
	public String getItem16() {
		return item16;
	}
	public void setItem16(String item16) {
		this.item16 = item16;
	}
	public String getItem17() {
		return item17;
	}
	public void setItem17(String item17) {
		this.item17 = item17;
	}
	public String getItem18() {
		return item18;
	}
	public void setItem18(String item18) {
		this.item18 = item18;
	}
	public String getItem19() {
		return item19;
	}
	public void setItem19(String item19) {
		this.item19 = item19;
	}
	public String getItem20() {
		return item20;
	}
	public void setItem20(String item20) {
		this.item20 = item20;
	}
	public String getItem21() {
		return item21;
	}
	public void setItem21(String item21) {
		this.item21 = item21;
	}
	public String getItem22() {
		return item22;
	}
	public void setItem22(String item22) {
		this.item22 = item22;
	}
	public String getItem23() {
		return item23;
	}
	public void setItem23(String item23) {
		this.item23 = item23;
	}
	public String getItem111() {
		return item111;
	}
	public void setItem111(String item111) {
		this.item111 = item111;
	}
	public String getItem112() {
		return item112;
	}
	public void setItem112(String item112) {
		this.item112 = item112;
	}
	public String getItem113() {
		return item113;
	}
	public void setItem113(String item113) {
		this.item113 = item113;
	}
	public String getItem114() {
		return item114;
	}
	public void setItem114(String item114) {
		this.item114 = item114;
	}
	public String getItem121() {
		return item121;
	}
	public void setItem121(String item121) {
		this.item121 = item121;
	}
	public String getItem122() {
		return item122;
	}
	public void setItem122(String item122) {
		this.item122 = item122;
	}
	public String getItem123() {
		return item123;
	}
	public void setItem123(String item123) {
		this.item123 = item123;
	}
	public String getItem124() {
		return item124;
	}
	public void setItem124(String item124) {
		this.item124 = item124;
	}
	public String getItem125() {
		return item125;
	}
	public void setItem125(String item125) {
		this.item125 = item125;
	}
	public String getItem126() {
		return item126;
	}
	public void setItem126(String item126) {
		this.item126 = item126;
	}
	public String getItem127() {
		return item127;
	}
	public void setItem127(String item127) {
		this.item127 = item127;
	}
	public String getItem128() {
		return item128;
	}
	public void setItem128(String item128) {
		this.item128 = item128;
	}
	public String getItem129() {
		return item129;
	}
	public void setItem129(String item129) {
		this.item129 = item129;
	}
	public String getItem1210() {
		return item1210;
	}
	public void setItem1210(String item1210) {
		this.item1210 = item1210;
	}
	public String getItem1211() {
		return item1211;
	}
	public void setItem1211(String item1211) {
		this.item1211 = item1211;
	}
	public String getItem1212() {
		return item1212;
	}
	public void setItem1212(String item1212) {
		this.item1212 = item1212;
	}
	public String getItem12113() {
		return item12113;
	}
	public void setItem12113(String item12113) {
		this.item12113 = item12113;
	}
	public String getItem12114() {
		return item12114;
	}
	public void setItem12114(String item12114) {
		this.item12114 = item12114;
	}
	public String getItem41() {
		return item41;
	}
	public void setItem41(String item41) {
		this.item41 = item41;
	}
	public String getItem42() {
		return item42;
	}
	public void setItem42(String item42) {
		this.item42 = item42;
	}
	public String getItem43() {
		return item43;
	}
	public void setItem43(String item43) {
		this.item43 = item43;
	}
	public String getItem44() {
		return item44;
	}
	public void setItem44(String item44) {
		this.item44 = item44;
	}
	public String getItem81() {
		return item81;
	}
	public void setItem81(String item81) {
		this.item81 = item81;
	}
	public String getItem82() {
		return item82;
	}
	public void setItem82(String item82) {
		this.item82 = item82;
	}
	public String getItem83() {
		return item83;
	}
	public void setItem83(String item83) {
		this.item83 = item83;
	}
	public String getItem84() {
		return item84;
	}
	public void setItem84(String item84) {
		this.item84 = item84;
	}
	//ALQD,07/10/08. AGREGANDO METODOS PARA LAS DOS PROPIEDADES A�ADIDAS
	public String getItem85() {
		return item85;
	}
	public void setItem85(String item85) {
		this.item85 = item85;
	}
	public String getItem86() {
		return item86;
	}
	public void setItem86(String item86) {
		this.item86 = item86;
	}
}
