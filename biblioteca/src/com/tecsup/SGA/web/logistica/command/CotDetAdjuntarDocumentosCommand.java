package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class CotDetAdjuntarDocumentosCommand {

	private String operacion;
	private String codUsuario;
	private String codCotizacion;
	private String codCotizacionDet;
	private String codTipoAdjunto;
	private String txtDescripArchivo;
	
	private List listaTipoAdjunto;
	
	private String msg;
	private String extArchivo;
	private String nomArchivo;
	
	private byte[] txtArchivo;
	private String nomNuevoArchivo;
	
	private String ruta;
	private String TipoProcedencia;
	private String codProveedor;
	
	public String getTipoProcedencia() {
		return TipoProcedencia;
	}

	public void setTipoProcedencia(String tipoProcedencia) {
		TipoProcedencia = tipoProcedencia;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodCotizacion() {
		return codCotizacion;
	}

	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}

	public String getCodCotizacionDet() {
		return codCotizacionDet;
	}

	public void setCodCotizacionDet(String codCotizacionDet) {
		this.codCotizacionDet = codCotizacionDet;
	}

	public String getCodTipoAdjunto() {
		return codTipoAdjunto;
	}

	public void setCodTipoAdjunto(String codTipoAdjunto) {
		this.codTipoAdjunto = codTipoAdjunto;
	}

	public String getTxtDescripArchivo() {
		return txtDescripArchivo;
	}

	public void setTxtDescripArchivo(String txtDescripArchivo) {
		this.txtDescripArchivo = txtDescripArchivo;
	}

	public List getListaTipoAdjunto() {
		return listaTipoAdjunto;
	}

	public void setListaTipoAdjunto(List listaTipoAdjunto) {
		this.listaTipoAdjunto = listaTipoAdjunto;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getExtArchivo() {
		return extArchivo;
	}

	public void setExtArchivo(String extArchivo) {
		this.extArchivo = extArchivo;
	}

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public byte[] getTxtArchivo() {
		return txtArchivo;
	}

	public void setTxtArchivo(byte[] txtArchivo) {
		this.txtArchivo = txtArchivo;
	}

	public String getNomNuevoArchivo() {
		return nomNuevoArchivo;
	}

	public void setNomNuevoArchivo(String nomNuevoArchivo) {
		this.nomNuevoArchivo = nomNuevoArchivo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getCodProveedor() {
		return codProveedor;
	}

	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}

	
}
