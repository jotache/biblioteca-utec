package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class SolicitudReqRelCommand {
	private String operacion;
	private String msg;
	private String seleccion;
	private String codUsuario;
	private String codSede;
	private String codRequerimiento;
	
	private List listaSolicitudes;
	private String tamListaSolicitudes;	
	
	public String getTamListaSolicitudes() {
		return tamListaSolicitudes;
	}
	public void setTamListaSolicitudes(String tamListaSolicitudes) {
		this.tamListaSolicitudes = tamListaSolicitudes;
	}
	public List getListaSolicitudes() {
		return listaSolicitudes;
	}
	public void setListaSolicitudes(List listaSolicitudes) {
		this.listaSolicitudes = listaSolicitudes;
	}		
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
}
