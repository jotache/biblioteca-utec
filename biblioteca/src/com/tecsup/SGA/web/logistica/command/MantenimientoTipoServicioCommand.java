package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class MantenimientoTipoServicioCommand {
	private String operacion;
	private String codServicio;
	private List listServicios;
	private List listGrupoFamilia;
	private String secuencial;
	private String desc;
	private String eliminar;
	private String msg;
	private String codAlumno;
	private String codPadre;
	private String dscPadre;
	
	public String getCodPadre() {
		return codPadre;
	}
	public void setCodPadre(String codPadre) {
		this.codPadre = codPadre;
	}
	public String getDscPadre() {
		return dscPadre;
	}
	public void setDscPadre(String dscPadre) {
		this.dscPadre = dscPadre;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getEliminar() {
		return eliminar;
	}

	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSecuencial() {
		return secuencial;
	}

	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public List getListGrupoFamilia() {
		return listGrupoFamilia;
	}

	public void setListGrupoFamilia(List listGrupoFamilia) {
		this.listGrupoFamilia = listGrupoFamilia;
	}

	public String getCodServicio() {
		return codServicio;
	}

	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}

	public List getListServicios() {
		return listServicios;
	}

	public void setListServicios(List listServicios) {
		this.listServicios = listServicios;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
}
