package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class MantenimientoDescriptoresCommand {
	private String operacion;
	private String codTipo;
	private List listDescriptores;
	private String codigo;
	private String descriptor;
	private String msg;
	private String graba;
	private String codAlumno;
	private String secuencial;
	private String tipo;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public List getListDescriptores() {
		return listDescriptores;
	}
	public void setListDescriptores(List listDescriptores) {
		this.listDescriptores = listDescriptores;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}


}
