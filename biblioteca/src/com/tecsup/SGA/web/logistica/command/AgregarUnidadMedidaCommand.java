package com.tecsup.SGA.web.logistica.command;

public class AgregarUnidadMedidaCommand {
	private String operacion;
	private String nombre;
	private String nroDecimales;
	private String abrev;
	private String grabar;
	private String msg;
	private String codigo;
	private String codAlumno;
	private String tipo;
	private String secuencial;
	
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getGrabar() {
		return grabar;
	}

	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}

	public String getAbrev() {
		return abrev;
	}

	public void setAbrev(String abrev) {
		this.abrev = abrev;
	}

	public String getNroDecimales() {
		return nroDecimales;
	}

	public void setNroDecimales(String nroDecimales) {
		this.nroDecimales = nroDecimales;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
}
