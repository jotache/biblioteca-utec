package com.tecsup.SGA.web.logistica.command;

public class AgregarTipoGastoCommand {
	private String operacion;
	private String codigo;
	private String tipGas;
	private String graba;
	private String msg;
	private String codAlumno;
	private String tipo;
	private String secuencial;
	private String ctaContable;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getTipGas() {
		return tipGas;
	}

	public void setTipGas(String tipGas) {
		this.tipGas = tipGas;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getGraba() {
		return graba;
	}

	public void setGraba(String graba) {
		this.graba = graba;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCtaContable() {
		return ctaContable;
	}
	public void setCtaContable(String ctaContable) {
		this.ctaContable = ctaContable;
	}
}
