package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class AgregarFamiliaCommand {
	private String operacion;
	private List tipoBien;
	private String codTipo;
	private List listFamilia;
	private String codigo;
	private String familia;
	private String cta;
	private String msg;
	private String graba;
	private String codAlumno;
	private String tipo;
	private String codigoUni;
	private String secuencial;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(List tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	public List getListFamilia() {
		return listFamilia;
	}
	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getFamilia() {
		return familia;
	}
	public void setFamilia(String familia) {
		this.familia = familia;
	}
	public String getCta() {
		return cta;
	}
	public void setCta(String cta) {
		this.cta = cta;
	}
	public String getCodigoUni() {
		return codigoUni;
	}
	public void setCodigoUni(String codigoUni) {
		this.codigoUni = codigoUni;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	
}
