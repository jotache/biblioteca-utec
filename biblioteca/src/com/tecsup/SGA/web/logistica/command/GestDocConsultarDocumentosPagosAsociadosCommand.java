package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class GestDocConsultarDocumentosPagosAsociadosCommand {

	private String operacion;
	private String msg;
	private String codSede;
	private String codUsuario;
	private String codProveedor;
	private String nroOrden;
	private String fecEmision;
	private String txtMontoOrden;
	private String txtNroRucProveedor;
	private String txtNombreProveedor;
	private String txtMontoFacturadoValido;
	private String txtSaldoValidado;
	private String txtSaldoPendiente;
	private String codPerfil;
	private String codOpciones;
	
	private List listaBandeja;
	private List listaBandejaMedio;
	
	private String codOrden;
	private String codEstado;
	private String codDocumento;
	private String consteCodEstado;
	private String indDocPago;
	private String txtMoneda;
	private String consteCodEstadoEnviado;
	
	private String indImportacion;
	private String importeOtros;
	
	private String tamListaBandejaMedio;
	
	private String codEstadoOrden;
	private String opcionDocRel;
	//ALQD,27/05/09.NUEVA PROPIEDAD
	private String indAfectoIGV;
	
	public String getIndAfectoIGV() {
		return indAfectoIGV;
	}
	public void setIndAfectoIGV(String indAfectoIGV) {
		this.indAfectoIGV=indAfectoIGV;
	}
	public String getOpcionDocRel() {
		return opcionDocRel;
	}

	public void setOpcionDocRel(String opcionDocRel) {
		this.opcionDocRel = opcionDocRel;
	}

	public String getCodEstadoOrden() {
		return codEstadoOrden;
	}

	public void setCodEstadoOrden(String codEstadoOrden) {
		this.codEstadoOrden = codEstadoOrden;
	}

	public String getIndImportacion() {
		return indImportacion;
	}

	public void setIndImportacion(String indImportacion) {
		this.indImportacion = indImportacion;
	}

	public String getImporteOtros() {
		return importeOtros;
	}

	public void setImporteOtros(String importeOtros) {
		this.importeOtros = importeOtros;
	}

	public String getConsteCodEstadoEnviado() {
		return consteCodEstadoEnviado;
	}

	public void setConsteCodEstadoEnviado(String consteCodEstadoEnviado) {
		this.consteCodEstadoEnviado = consteCodEstadoEnviado;
	}

	public String getConsteCodEstado() {
		return consteCodEstado;
	}

	public void setConsteCodEstado(String consteCodEstado) {
		this.consteCodEstado = consteCodEstado;
	}

	public String getCodDocumento() {
		return codDocumento;
	}

	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}

	public String getCodOrden() {
		return codOrden;
	}

	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}

	public String getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodProveedor() {
		return codProveedor;
	}

	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}

	public String getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}

	public String getFecEmision() {
		return fecEmision;
	}

	public void setFecEmision(String fecEmision) {
		this.fecEmision = fecEmision;
	}

	public String getTxtMontoOrden() {
		return txtMontoOrden;
	}

	public void setTxtMontoOrden(String txtMontoOrden) {
		this.txtMontoOrden = txtMontoOrden;
	}

	public String getTxtNroRucProveedor() {
		return txtNroRucProveedor;
	}

	public void setTxtNroRucProveedor(String txtNroRucProveedor) {
		this.txtNroRucProveedor = txtNroRucProveedor;
	}

	public String getTxtNombreProveedor() {
		return txtNombreProveedor;
	}

	public void setTxtNombreProveedor(String txtNombreProveedor) {
		this.txtNombreProveedor = txtNombreProveedor;
	}

	public String getTxtMontoFacturadoValido() {
		return txtMontoFacturadoValido;
	}

	public void setTxtMontoFacturadoValido(String txtMontoFacturadoValido) {
		this.txtMontoFacturadoValido = txtMontoFacturadoValido;
	}

	public String getTxtSaldoValidado() {
		return txtSaldoValidado;
	}

	public void setTxtSaldoValidado(String txtSaldoValidado) {
		this.txtSaldoValidado = txtSaldoValidado;
	}

	public String getTxtSaldoPendiente() {
		return txtSaldoPendiente;
	}

	public void setTxtSaldoPendiente(String txtSaldoPendiente) {
		this.txtSaldoPendiente = txtSaldoPendiente;
	}

	public List getListaBandeja() {
		return listaBandeja;
	}

	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public List getListaBandejaMedio() {
		return listaBandejaMedio;
	}

	public void setListaBandejaMedio(List listaBandejaMedio) {
		this.listaBandejaMedio = listaBandejaMedio;
	}

	public String getIndDocPago() {
		return indDocPago;
	}

	public void setIndDocPago(String indDocPago) {
		this.indDocPago = indDocPago;
	}

	public String getTxtMoneda() {
		return txtMoneda;
	}

	public void setTxtMoneda(String txtMoneda) {
		this.txtMoneda = txtMoneda;
	}

	public String getTamListaBandejaMedio() {
		return tamListaBandejaMedio;
	}

	public void setTamListaBandejaMedio(String tamListaBandejaMedio) {
		this.tamListaBandejaMedio = tamListaBandejaMedio;
	}
	
	
}
