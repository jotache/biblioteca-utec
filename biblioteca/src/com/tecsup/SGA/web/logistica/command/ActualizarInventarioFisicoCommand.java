package com.tecsup.SGA.web.logistica.command;

public class ActualizarInventarioFisicoCommand {
	private String operacion;
	private String codSede;
	private String codUsuario;	
	private String msg;	
	
	private String cv;
	private byte[] txtCV; //archivo a subir
	private String extCv;	
	private String nomArchivo;
	private String tdCV;
	private byte[] txtc;
	private byte[] txhPath;
	
	private String archivo;
	private String estadoAlmacen;

	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}

	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public byte[] getTxtCV() {
		return txtCV;
	}

	public void setTxtCV(byte[] txtCV) {
		this.txtCV = txtCV;
	}

	public String getExtCv() {
		return extCv;
	}

	public void setExtCv(String extCv) {
		this.extCv = extCv;
	}
	
	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public String getTdCV() {
		return tdCV;
	}

	public void setTdCV(String tdCV) {
		this.tdCV = tdCV;
	}

	public byte[] getTxtc() {
		return txtc;
	}

	public void setTxtc(byte[] txtc) {
		this.txtc = txtc;
	}

	public byte[] getTxhPath() {
		return txhPath;
	}

	public void setTxhPath(byte[] txhPath) {
		this.txhPath = txhPath;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}	

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
}
