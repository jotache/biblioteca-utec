package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class GestOrdenBandejaConsultaOrdenCommand {

	private String operacion;
	private String msg;
	private String codSede;
	private String codUsuario;
	private String codPerfil;
	
	private String codOpciones;
	
	private String nroOrden;
	private String fecInicio;
	private String fecFinal;
	private String codEstado;
	private List listaEstado;
	private String nroRucProveedor;
	private String nroRucComprador;
	private String codTipoOrden;
	
	private List listaCodTipoOrden;
	private List listaBandeja;
	
	private String nombreComprador;
	private String nombreProveedor;
	private String codRequerimiento;
	
	private String codEstadoAprobado;
	private String codBDEstado;
	private String banderaPerfil;
	private String fechaBD;
	private String banListaBandeja;
	private String dscUsuario;
	private String codTipoReporte;
	private String nomSede;
	private String indiceAprobacion;
	//ALQD,04/02/09. NUEVA PROPIEDAD
	private String indPerComprador;
	//ALQD,14/07/09. NUEVA PROPIEDAD
	private String indDirector;
	
	public String getIndDirector() {
		return indDirector;
	}
	public void setIndDirector(String indDirector) {
		this.indDirector=indDirector;
	}
	public String getIndPerComprador() {
		return indPerComprador;
	}
	public void setIndPerComprador(String indPerComprador) {
		this.indPerComprador=indPerComprador;
	}
	public String getBanListaBandeja() {
		return banListaBandeja;
	}
	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}
	public String getCodEstadoAprobado() {
		return codEstadoAprobado;
	}
	public void setCodEstadoAprobado(String codEstadoAprobado) {
		this.codEstadoAprobado = codEstadoAprobado;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public List getListaEstado() {
		return listaEstado;
	}
	public void setListaEstado(List listaEstado) {
		this.listaEstado = listaEstado;
	}
	public String getNroRucProveedor() {
		return nroRucProveedor;
	}
	public void setNroRucProveedor(String nroRucProveedor) {
		this.nroRucProveedor = nroRucProveedor;
	}
	public String getNroRucComprador() {
		return nroRucComprador;
	}
	public void setNroRucComprador(String nroRucComprador) {
		this.nroRucComprador = nroRucComprador;
	}
	public String getCodTipoOrden() {
		return codTipoOrden;
	}
	public void setCodTipoOrden(String codTipoOrden) {
		this.codTipoOrden = codTipoOrden;
	}
	public List getListaCodTipoOrden() {
		return listaCodTipoOrden;
	}
	public void setListaCodTipoOrden(List listaCodTipoOrden) {
		this.listaCodTipoOrden = listaCodTipoOrden;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public String getNombreComprador() {
		return nombreComprador;
	}
	public void setNombreComprador(String nombreComprador) {
		this.nombreComprador = nombreComprador;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public String getCodBDEstado() {
		return codBDEstado;
	}
	public void setCodBDEstado(String codBDEstado) {
		this.codBDEstado = codBDEstado;
	}
	public String getBanderaPerfil() {
		return banderaPerfil;
	}
	public void setBanderaPerfil(String banderaPerfil) {
		this.banderaPerfil = banderaPerfil;
	}
	public String getFechaBD() {
		return fechaBD;
	}
	public void setFechaBD(String fechaBD) {
		this.fechaBD = fechaBD;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	public String getCodTipoReporte() {
		return codTipoReporte;
	}
	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}
	public String getNomSede() {
		return nomSede;
	}
	public void setNomSede(String nomSede) {
		this.nomSede = nomSede;
	}
	public String getIndiceAprobacion() {
		return indiceAprobacion;
	}
	public void setIndiceAprobacion(String indiceAprobacion) {
		this.indiceAprobacion = indiceAprobacion;
	}
}
