package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoTipoAdjuntoCommand {
	private List listAdjuntos;
	private String operacion;
	private String secuencial;
	private String desc;
	private String eliminar;
	private String msg;
private String codAlumno;
	
	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getEliminar() {
		return eliminar;
	}

	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSecuencial() {
		return secuencial;
	}

	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public List getListAdjuntos() {
		return listAdjuntos;
	}

	public void setListAdjuntos(List listAdjuntos) {
		this.listAdjuntos = listAdjuntos;
	}
	
}
