package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class PenAgregarCotizacionExistenteCommand {

	private String txtNroCotizacion;
	private String operacion;
	private String codSede;
	private String fecInicio;
	private String fecFinal;
	private String codTipoPago;
	private String valRadio;
	private String consteBienConsumible;
	private String consteBienActivo;
	private String codUsuario;
	private String codOpciones;
	private String consteRadio;
	private String txtDescripcion;
	
	private List listaCodTipoBien;
	
	private String codTipoReq;
	private String codSubTipoReq;
	private String dscTipoReq;
	private String bandera;
	
	private String codTipoServicio;
	private List listaTipoServicio;
	
	private List listaConsulta;
	private String banListaBandeja;
	
	//ccordova 13/06/2008
	private String codSeleccionados;
	private String msg;
	private String codCotizacion;
	//ALQD,07/08/09.NUEVAS PROPIEDADES
	private String indInversion;
	
	public String getIndInversion() {
		return indInversion;
	}
	public void setIndInversion(String indInversion) {
		this.indInversion=indInversion;
	}
	public String getBanListaBandeja() {
		return banListaBandeja;
	}
	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}
	public List getListaConsulta() {
		return listaConsulta;
	}
	public void setListaConsulta(List listaConsulta) {
		this.listaConsulta = listaConsulta;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public List getListaTipoServicio() {
		return listaTipoServicio;
	}
	public void setListaTipoServicio(List listaTipoServicio) {
		this.listaTipoServicio = listaTipoServicio;
	}
	public String getBandera() {
		return bandera;
	}
	public void setBandera(String bandera) {
		this.bandera = bandera;
	}
	public String getDscTipoReq() {
		return dscTipoReq;
	}
	public void setDscTipoReq(String dscTipoReq) {
		this.dscTipoReq = dscTipoReq;
	}
	public String getCodTipoReq() {
		return codTipoReq;
	}
	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}
	public String getCodSubTipoReq() {
		return codSubTipoReq;
	}
	public void setCodSubTipoReq(String codSubTipoReq) {
		this.codSubTipoReq = codSubTipoReq;
	}
	public String getTxtNroCotizacion() {
		return txtNroCotizacion;
	}
	public void setTxtNroCotizacion(String txtNroCotizacion) {
		this.txtNroCotizacion = txtNroCotizacion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getCodTipoPago() {
		return codTipoPago;
	}
	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	public String getValRadio() {
		return valRadio;
	}
	public void setValRadio(String valRadio) {
		this.valRadio = valRadio;
	}


	public String getConsteBienConsumible() {
		return consteBienConsumible;
	}


	public void setConsteBienConsumible(String consteBienConsumible) {
		this.consteBienConsumible = consteBienConsumible;
	}


	public String getConsteBienActivo() {
		return consteBienActivo;
	}


	public void setConsteBienActivo(String consteBienActivo) {
		this.consteBienActivo = consteBienActivo;
	}


	public List getListaCodTipoBien() {
		return listaCodTipoBien;
	}


	public void setListaCodTipoBien(List listaCodTipoBien) {
		this.listaCodTipoBien = listaCodTipoBien;
	}


	public String getCodUsuario() {
		return codUsuario;
	}


	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}


	public String getCodOpciones() {
		return codOpciones;
	}


	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}


	public String getConsteRadio() {
		return consteRadio;
	}


	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}


	public String getTxtDescripcion() {
		return txtDescripcion;
	}


	public void setTxtDescripcion(String txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}


	public String getCodSeleccionados() {
		return codSeleccionados;
	}


	public void setCodSeleccionados(String codSeleccionados) {
		this.codSeleccionados = codSeleccionados;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public String getCodCotizacion() {
		return codCotizacion;
	}


	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
}
