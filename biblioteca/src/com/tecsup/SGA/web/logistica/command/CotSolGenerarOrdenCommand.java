package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class CotSolGenerarOrdenCommand {
	
	private String codUsuario;
	private String operacion;
	private String codCotizacion;
	private String msg;
	private String codSede;
	private String codTipoReq;
	
	private String nroCotizacion;
	private String codTipoPago;
	private List listaCodTipoPago;
	private List listaBandeja;
	private List listaBandejaInferior;
	private String codProveedor;
	private String dscProveedor;
	private String dscNroOrden;
	
	private String cadCodigosDetalle;
	private String cadDescripcionDetalle;
	private String cantidad;
	
	private String longitud;
	private String dscTipoPago;
	private String codTipoPago2;
	private String estado;
	private String banListaBandeja;
	private String banListaBandejaInferior;
	
	public String getBanListaBandeja() {
		return banListaBandeja;
	}
	public void setBanListaBandeja(String banListaBandeja) {
		this.banListaBandeja = banListaBandeja;
	}
	public String getBanListaBandejaInferior() {
		return banListaBandejaInferior;
	}
	public void setBanListaBandejaInferior(String banListaBandejaInferior) {
		this.banListaBandejaInferior = banListaBandejaInferior;
	}
	public String getDscProveedor() {
		return dscProveedor;
	}
	public void setDscProveedor(String dscProveedor) {
		this.dscProveedor = dscProveedor;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	//private String operacion;
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNroCotizacion() {
		return nroCotizacion;
	}
	public void setNroCotizacion(String nroCotizacion) {
		this.nroCotizacion = nroCotizacion;
	}
	public String getCodTipoPago() {
		return codTipoPago;
	}
	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	public List getListaCodTipoPago() {
		return listaCodTipoPago;
	}
	public void setListaCodTipoPago(List listaCodTipoPago) {
		this.listaCodTipoPago = listaCodTipoPago;
	}
	public List getListaBandeja() {
		return listaBandeja;
	}
	public void setListaBandeja(List listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	public List getListaBandejaInferior() {
		return listaBandejaInferior;
	}
	public void setListaBandejaInferior(List listaBandejaInferior) {
		this.listaBandejaInferior = listaBandejaInferior;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodTipoReq() {
		return codTipoReq;
	}
	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}
	public String getDscNroOrden() {
		return dscNroOrden;
	}
	public void setDscNroOrden(String dscNroOrden) {
		this.dscNroOrden = dscNroOrden;
	}
	public String getCadCodigosDetalle() {
		return cadCodigosDetalle;
	}
	public void setCadCodigosDetalle(String cadCodigosDetalle) {
		this.cadCodigosDetalle = cadCodigosDetalle;
	}
	public String getCadDescripcionDetalle() {
		return cadDescripcionDetalle;
	}
	public void setCadDescripcionDetalle(String cadDescripcionDetalle) {
		this.cadDescripcionDetalle = cadDescripcionDetalle;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getDscTipoPago() {
		return dscTipoPago;
	}
	public void setDscTipoPago(String dscTipoPago) {
		this.dscTipoPago = dscTipoPago;
	}
	public String getCodTipoPago2() {
		return codTipoPago2;
	}
	public void setCodTipoPago2(String codTipoPago2) {
		this.codTipoPago2 = codTipoPago2;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
