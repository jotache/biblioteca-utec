package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ParametrosGeneralesCommand {
	private String operacion;
	private String codAlumno;
	private String msg;
	private String igv;
	private String glosaCabecera;
	private String ctaExis;
	private String ctaTipoGas;
	private String ctaCentroCosto;
	private String grabar;
	private List codListaAnios;
	private List codListaMeses;
	private String codAnios;
	private String codMeses;
	private String glosaDetalle;
	private String cuser;
	
	public String getCuser() {
		return cuser;
	}
	public void setCuser(String cuser) {
		this.cuser = cuser;
	}
	public String getGlosaDetalle() {
		return glosaDetalle;
	}
	public void setGlosaDetalle(String glosaDetalle) {
		this.glosaDetalle = glosaDetalle;
	}
	public String getCodAnios() {
		return codAnios;
	}
	public void setCodAnios(String codAnios) {
		this.codAnios = codAnios;
	}
	public String getCodMeses() {
		return codMeses;
	}
	public void setCodMeses(String codMeses) {
		this.codMeses = codMeses;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getIgv() {
		return igv;
	}
	public void setIgv(String igv) {
		this.igv = igv;
	}
	public String getGlosaCabecera() {
		return glosaCabecera;
	}
	public void setGlosaCabecera(String glosaCabecera) {
		this.glosaCabecera = glosaCabecera;
	}
	public String getCtaExis() {
		return ctaExis;
	}
	public void setCtaExis(String ctaExis) {
		this.ctaExis = ctaExis;
	}
	public String getCtaTipoGas() {
		return ctaTipoGas;
	}
	public void setCtaTipoGas(String ctaTipoGas) {
		this.ctaTipoGas = ctaTipoGas;
	}
	public String getCtaCentroCosto() {
		return ctaCentroCosto;
	}
	public void setCtaCentroCosto(String ctaCentroCosto) {
		this.ctaCentroCosto = ctaCentroCosto;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public List getCodListaAnios() {
		return codListaAnios;
	}
	public void setCodListaAnios(List codListaAnios) {
		this.codListaAnios = codListaAnios;
	}
	public List getCodListaMeses() {
		return codListaMeses;
	}
	public void setCodListaMeses(List codListaMeses) {
		this.codListaMeses = codListaMeses;
	}
	
	
}
