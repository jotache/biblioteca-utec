package com.tecsup.SGA.web.logistica.command;

public class AdjuntarImagenCommand {
	private String operacion;
	private String cv;
	private byte[] txtCV; //archivo a subir
	private String extCv;
	private String message;
	private String nomArchivo;
	private String tdCV;
	private byte[] txtc;
	private byte[] txhPath;
	private String codAlumno;
	private String graba;
	private String codPro;
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodPro() {
		return codPro;
	}

	public void setCodPro(String codPro) {
		this.codPro = codPro;
	}

	public String getGraba() {
		return graba;
	}

	public void setGraba(String graba) {
		this.graba = graba;
	}

	public String getTdCV() {
		return tdCV;
	}

	public void setTdCV(String tdCV) {
		this.tdCV = tdCV;
	}

	public byte[] getTxtc() {
		return txtc;
	}

	public void setTxtc(byte[] txtc) {
		this.txtc = txtc;
	}

	public byte[] getTxhPath() {
		return txhPath;
	}

	public void setTxhPath(byte[] txhPath) {
		this.txhPath = txhPath;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public byte[] getTxtCV() {
		return txtCV;
	}

	public void setTxtCV(byte[] txtCV) {
		this.txtCV = txtCV;
	}

	public String getExtCv() {
		return extCv;
	}

	public void setExtCv(String extCv) {
		this.extCv = extCv;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	
}
