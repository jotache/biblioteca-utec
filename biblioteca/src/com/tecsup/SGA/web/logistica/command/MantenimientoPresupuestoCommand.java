package com.tecsup.SGA.web.logistica.command;

import java.util.List;

import com.tecsup.SGA.modelo.PresupuestoCentroCosto;

public class MantenimientoPresupuestoCommand {

	private String codAlumno;
	private String codAnioSelec;
	private List listaAniosCencos;
	private List<PresupuestoCentroCosto> listaPresupuestos;
	private String operacion;
	private int tamListPresup;
	private String codSede;
	private String cadenaDatos;
	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodAnioSelec() {
		return codAnioSelec;
	}

	public void setCodAnioSelec(String codAnioSelec) {
		this.codAnioSelec = codAnioSelec;
	}

	public List getListaAniosCencos() {
		return listaAniosCencos;
	}

	public void setListaAniosCencos(List listaAniosCencos) {
		this.listaAniosCencos = listaAniosCencos;
	}
	

	public List<PresupuestoCentroCosto> getListaPresupuestos() {
		return listaPresupuestos;
	}

	public void setListaPresupuestos(List<PresupuestoCentroCosto> listaPresupuestos) {
		this.listaPresupuestos = listaPresupuestos;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public int getTamListPresup() {
		return tamListPresup;
	}

	public void setTamListPresup(int tamListPresup) {
		this.tamListPresup = tamListPresup;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getCadenaDatos() {
		return cadenaDatos;
	}

	public void setCadenaDatos(String cadenaDatos) {
		this.cadenaDatos = cadenaDatos;
	}
	
}
