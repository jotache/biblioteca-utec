package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarCondicionCompraCommand {
	private String operacion;
	private String graba;
	private String msg;
	private String codUsuario;
	private String codigo;
	private String descripcion;
	private List listcondiciones;
	private String indicador;
	private String desTipo;
	private String codCotizacion;
	private String codId;
	private String codDescripcion;
	private int tot;
	private String eliminar;
	
	public String getEliminar() {
		return eliminar;
	}
	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getIndicador() {
		return indicador;
	}
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	public String getDesTipo() {
		return desTipo;
	}
	public void setDesTipo(String desTipo) {
		this.desTipo = desTipo;
	}
	public List getListcondiciones() {
		return listcondiciones;
	}
	public void setListcondiciones(List listcondiciones) {
		this.listcondiciones = listcondiciones;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodDescripcion() {
		return codDescripcion;
	}
	public void setCodDescripcion(String codDescripcion) {
		this.codDescripcion = codDescripcion;
	}
	public int getTot() {
		return tot;
	}
	public void setTot(int tot) {
		this.tot = tot;
	}
	

	
}
