package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class BuscarCentroCostoCommand {
	private String operacion;
	private List listCentros;
	private String descripcion;
	private String sede;
	//ccordova 14/06/2008
	private String nomObjeto;
	private String codCecoSeleccionado; 

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List getListCentros() {
		return listCentros;
	}

	public void setListCentros(List listCentros) {
		this.listCentros = listCentros;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getNomObjeto() {
		return nomObjeto;
	}

	public void setNomObjeto(String nomObjeto) {
		this.nomObjeto = nomObjeto;
	}

	public String getCodCecoSeleccionado() {
		return codCecoSeleccionado;
	}

	public void setCodCecoSeleccionado(String codCecoSeleccionado) {
		this.codCecoSeleccionado = codCecoSeleccionado;
	}
	
}
