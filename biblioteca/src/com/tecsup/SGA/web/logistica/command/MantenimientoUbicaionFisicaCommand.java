package com.tecsup.SGA.web.logistica.command;

import java.util.List;
public class MantenimientoUbicaionFisicaCommand {
	private String operacion;
	private List listUbicacion;
	private String secuencial;
	private String descripcion;
	private String eliminar;
	private String msg;
	private String codAlumno;
	private String codSede;
	private List listSede;
	
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListSede() {
		return listSede;
	}
	public void setListSede(List listSede) {
		this.listSede = listSede;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getEliminar() {
		return eliminar;
	}

	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSecuencial() {
		return secuencial;
	}

	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List getListUbicacion() {
		return listUbicacion;
	}

	public void setListUbicacion(List listUbicacion) {
		this.listUbicacion = listUbicacion;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	
}
