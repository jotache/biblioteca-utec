package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class SreqDevolverBienesCommand {
	
	private String operacion;
	private String msg;
	private String codUsuario;
	private String estado;	
	private String codSede;
	private String codRequerimiento;
	
	private String opcion;
	private String tipoBien;
	
	private String codDevolucion;
	private String nroDevolucion;
	private String fechaDevolución;
	private String centroCosto;
	private String usuSolicitante;
	private String tipoRequerimiento;	
	private String nroReqAsociado;
	private String motivoDevolucion;
	
	private List listaBienes;
	private String tamListaBienes;
	
	private String codSubTipoRequerimiento;
	private List listaCodTipoBien;
	private String codTipoBienConsumible;
	private String codTipoBienActivo;
	
	private String cadenaCodigo;
	private String cadenaCantidad;	
	private String codTipoBien;
	private String codRequerimientoDet;
	private String codDevolucionDet;
	
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	
	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}
	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}
	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}
	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}
	public String getCodRequerimientoDet() {
		return codRequerimientoDet;
	}
	public void setCodRequerimientoDet(String codRequerimientoDet) {
		this.codRequerimientoDet = codRequerimientoDet;
	}
	public String getCodDevolucionDet() {
		return codDevolucionDet;
	}
	public void setCodDevolucionDet(String codDevolucionDet) {
		this.codDevolucionDet = codDevolucionDet;
	}
	public String getCadenaCodigo() {
		return cadenaCodigo;
	}
	public void setCadenaCodigo(String cadenaCodigo) {
		this.cadenaCodigo = cadenaCodigo;
	}
	public String getCadenaCantidad() {
		return cadenaCantidad;
	}
	public void setCadenaCantidad(String cadenaCantidad) {
		this.cadenaCantidad = cadenaCantidad;
	}	
	public List getListaCodTipoBien() {
		return listaCodTipoBien;
	}
	public void setListaCodTipoBien(List listaCodTipoBien) {
		this.listaCodTipoBien = listaCodTipoBien;
	}
	public String getCodTipoBienConsumible() {
		return codTipoBienConsumible;
	}
	public void setCodTipoBienConsumible(String codTipoBienConsumible) {
		this.codTipoBienConsumible = codTipoBienConsumible;
	}
	public String getCodTipoBienActivo() {
		return codTipoBienActivo;
	}
	public void setCodTipoBienActivo(String codTipoBienActivo) {
		this.codTipoBienActivo = codTipoBienActivo;
	}
	public String getCodSubTipoRequerimiento() {
		return codSubTipoRequerimiento;
	}
	public void setCodSubTipoRequerimiento(String codSubTipoRequerimiento) {
		this.codSubTipoRequerimiento = codSubTipoRequerimiento;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getNroDevolucion() {
		return nroDevolucion;
	}
	public void setNroDevolucion(String nroDevolucion) {
		this.nroDevolucion = nroDevolucion;
	}
	public String getFechaDevolución() {
		return fechaDevolución;
	}
	public void setFechaDevolución(String fechaDevolución) {
		this.fechaDevolución = fechaDevolución;
	}
	public String getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(String centroCosto) {
		this.centroCosto = centroCosto;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getTipoRequerimiento() {
		return tipoRequerimiento;
	}
	public void setTipoRequerimiento(String tipoRequerimiento) {
		this.tipoRequerimiento = tipoRequerimiento;
	}
	public String getNroReqAsociado() {
		return nroReqAsociado;
	}
	public void setNroReqAsociado(String nroReqAsociado) {
		this.nroReqAsociado = nroReqAsociado;
	}
	public List getListaBienes() {
		return listaBienes;
	}
	public void setListaBienes(List listaBienes) {
		this.listaBienes = listaBienes;
	}
	public String getTamListaBienes() {
		return tamListaBienes;
	}
	public void setTamListaBienes(String tamListaBienes) {
		this.tamListaBienes = tamListaBienes;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getCodDevolucion() {
		return codDevolucion;
	}
	public void setCodDevolucion(String codDevolucion) {
		this.codDevolucion = codDevolucion;
	}
	public String getMotivoDevolucion() {
		return motivoDevolucion;
	}
	public void setMotivoDevolucion(String motivoDevolucion) {
		this.motivoDevolucion = motivoDevolucion;
	}
	public String getCodTipoBien() {
		return codTipoBien;
	}
	public void setCodTipoBien(String codTipoBien) {
		this.codTipoBien = codTipoBien;
	}
}
