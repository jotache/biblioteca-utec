package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class AtenderRequerimientoBienesGuiaSalidaRegResponsableCommand {
	private String operacion;
	private String usuario;	
	private String codGuia;
	private String codGuiaDetalle;
	private String cantidadEntregada;
	private String codBien;	
	private String codSede;
	private List lstRespuesta;
	private String tamaņoLista;
	private String codEstado;
	private String banListaGuiaDetalle;
	
	public String getBanListaGuiaDetalle() {
		return banListaGuiaDetalle;
	}
	public void setBanListaGuiaDetalle(String banListaGuiaDetalle) {
		this.banListaGuiaDetalle = banListaGuiaDetalle;
	}
	public String getTamaņoLista() {
		return tamaņoLista;
	}
	public void setTamaņoLista(String tamaņoLista) {
		this.tamaņoLista = tamaņoLista;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCodGuia() {
		return codGuia;
	}
	public void setCodGuia(String codGuia) {
		this.codGuia = codGuia;
	}
	public String getCodGuiaDetalle() {
		return codGuiaDetalle;
	}
	public void setCodGuiaDetalle(String codGuiaDetalle) {
		this.codGuiaDetalle = codGuiaDetalle;
	}
	public String getCantidadEntregada() {
		return cantidadEntregada;
	}
	public void setCantidadEntregada(String cantidadEntregada) {
		this.cantidadEntregada = cantidadEntregada;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public List getLstRespuesta() {
		return lstRespuesta;
	}
	public void setLstRespuesta(List lstRespuesta) {
		this.lstRespuesta = lstRespuesta;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
}
