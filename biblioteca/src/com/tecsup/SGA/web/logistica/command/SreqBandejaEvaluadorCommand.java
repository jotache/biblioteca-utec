package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class SreqBandejaEvaluadorCommand {

	private String operacion;
	private String msg;
	
	private String codSede;
	private String codUsuario;
	private String codOpciones;

	private String codTipoRequerimiento;
	private List<TipoTablaDetalle> listTipoRequerimiento;

	private String nroRequerimiento;
	private String fecInicio;
	private String fecFinal;
	
	private String codCeco;
	private List<CentroCosto> listCeco;	
	
	private String codEstado;
	private List<TipoTablaDetalle> listEstado;
	
	private List<SolRequerimiento> listaSolicitudes;
	
	private String consteCodBien;
	private String consteCodServicio; 
	private String banderaTipoReq;
	
	private String codRequerimiento;
	
	private String consteCodEstadoPendiente;
	private String consteCodEstadoRechazado;
	private String codEstadoBD;
	private String consteCodEstadoAprobacion;
	private String consteCodEstadoCerrado;
	private String indiceDevolucion;
	private String codTipoBien;
	private String estadoAlmacen;
	private String consteEstadoAlmacen;
	private String bandera;
	
	public String getBandera() {
		return bandera;
	}

	public void setBandera(String bandera) {
		this.bandera = bandera;
	}

	public String getConsteCodEstadoPendiente() {
		return consteCodEstadoPendiente;
	}

	public void setConsteCodEstadoPendiente(String consteCodEstadoPendiente) {
		this.consteCodEstadoPendiente = consteCodEstadoPendiente;
	}

	public String getConsteCodEstadoRechazado() {
		return consteCodEstadoRechazado;
	}

	public void setConsteCodEstadoRechazado(String consteCodEstadoRechazado) {
		this.consteCodEstadoRechazado = consteCodEstadoRechazado;
	}

	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}

	public String getBanderaTipoReq() {
		return banderaTipoReq;
	}

	public void setBanderaTipoReq(String banderaTipoReq) {
		this.banderaTipoReq = banderaTipoReq;
	}

	public String getConsteCodBien() {
		return consteCodBien;
	}

	public void setConsteCodBien(String consteCodBien) {
		this.consteCodBien = consteCodBien;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}

	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}

	public List<TipoTablaDetalle> getListTipoRequerimiento() {
		return listTipoRequerimiento;
	}

	public void setListTipoRequerimiento(
			List<TipoTablaDetalle> listTipoRequerimiento) {
		this.listTipoRequerimiento = listTipoRequerimiento;
	}

	public String getNroRequerimiento() {
		return nroRequerimiento;
	}

	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}

	public String getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}

	public String getFecFinal() {
		return fecFinal;
	}

	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}

	public String getCodCeco() {
		return codCeco;
	}

	public void setCodCeco(String codCeco) {
		this.codCeco = codCeco;
	}

	public List<CentroCosto> getListCeco() {
		return listCeco;
	}

	public void setListCeco(List<CentroCosto> listCeco) {
		this.listCeco = listCeco;
	}

	public String getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}

	public List<TipoTablaDetalle> getListEstado() {
		return listEstado;
	}

	public void setListEstado(List<TipoTablaDetalle> listEstado) {
		this.listEstado = listEstado;
	}

	public List<SolRequerimiento> getListaSolicitudes() {
		return listaSolicitudes;
	}

	public void setListaSolicitudes(List<SolRequerimiento> listaSolicitudes) {
		this.listaSolicitudes = listaSolicitudes;
	}
	
	public SreqBandejaEvaluadorCommand()
	{
		this.operacion = "";
		this.msg = "";
		this.codUsuario = "";
		this.codOpciones = "";
		this.codTipoRequerimiento = "";
		this.listTipoRequerimiento = null;
		this.nroRequerimiento = "";
		this.fecInicio = "";
		this.fecFinal = "";
		this.codCeco = "";
		this.listCeco = null;
		this.codEstado = "";
		this.listEstado = null;
		this.listaSolicitudes = null;
	}

	public String getConsteCodServicio() {
		return consteCodServicio;
	}

	public void setConsteCodServicio(String consteCodServicio) {
		this.consteCodServicio = consteCodServicio;
	}

	public String getCodEstadoBD() {
		return codEstadoBD;
	}

	public void setCodEstadoBD(String codEstadoBD) {
		this.codEstadoBD = codEstadoBD;
	}

	public String getConsteCodEstadoAprobacion() {
		return consteCodEstadoAprobacion;
	}

	public void setConsteCodEstadoAprobacion(String consteCodEstadoAprobacion) {
		this.consteCodEstadoAprobacion = consteCodEstadoAprobacion;
	}

	public String getConsteCodEstadoCerrado() {
		return consteCodEstadoCerrado;
	}

	public void setConsteCodEstadoCerrado(String consteCodEstadoCerrado) {
		this.consteCodEstadoCerrado = consteCodEstadoCerrado;
	}

	public String getIndiceDevolucion() {
		return indiceDevolucion;
	}

	public void setIndiceDevolucion(String indiceDevolucion) {
		this.indiceDevolucion = indiceDevolucion;
	}

	public String getCodTipoBien() {
		return codTipoBien;
	}

	public void setCodTipoBien(String codTipoBien) {
		this.codTipoBien = codTipoBien;
	}

	public String getEstadoAlmacen() {
		return estadoAlmacen;
	}

	public void setEstadoAlmacen(String estadoAlmacen) {
		this.estadoAlmacen = estadoAlmacen;
	}

	public String getConsteEstadoAlmacen() {
		return consteEstadoAlmacen;
	}

	public void setConsteEstadoAlmacen(String consteEstadoAlmacen) {
		this.consteEstadoAlmacen = consteEstadoAlmacen;
	}

	
}
