package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class MantenimientoFlujoServicioCommand {
	private String operacion;
	private List listTipReq;
	private List listTipoBien;
	private List listFamilia;
	private List listSubFamilia;
	private String responsable;
	private String codTipReq;
	private String codTipBien;
	private String codFamilia;
	private String codSubFamilia;
	private String codRes;
	private String  nomRes;
	private String codAlumno;
	private List listTipoReqBien;
	private List listTipoReqServicio;
	private List listTipoServicio;
	private String codTipoServicio;
	private String codigo;
	private String graba;
	private String msg;
	private String codigoTipo;
	private List listSede;
	private String codSede;
	private int cant;
	

	public List getListSede() {
		return listSede;
	}
	public void setListSede(List listSede) {
		this.listSede = listSede;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodigoTipo() {
		return codigoTipo;
	}
	public void setCodigoTipo(String codigoTipo) {
		this.codigoTipo = codigoTipo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public List getListTipoServicio() {
		return listTipoServicio;
	}
	public void setListTipoServicio(List listTipoServicio) {
		this.listTipoServicio = listTipoServicio;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodRes() {
		return codRes;
	}
	public void setCodRes(String codRes) {
		this.codRes = codRes;
	}
	public String getNomRes() {
		return nomRes;
	}
	public void setNomRes(String nomRes) {
		this.nomRes = nomRes;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListTipReq() {
		return listTipReq;
	}
	public void setListTipReq(List listTipReq) {
		this.listTipReq = listTipReq;
	}
	public List getListTipoBien() {
		return listTipoBien;
	}
	public void setListTipoBien(List listTipoBien) {
		this.listTipoBien = listTipoBien;
	}
	public List getListFamilia() {
		return listFamilia;
	}
	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}
	public List getListSubFamilia() {
		return listSubFamilia;
	}
	public void setListSubFamilia(List listSubFamilia) {
		this.listSubFamilia = listSubFamilia;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getCodTipReq() {
		return codTipReq;
	}
	public void setCodTipReq(String codTipReq) {
		this.codTipReq = codTipReq;
	}
	public String getCodTipBien() {
		return codTipBien;
	}
	public void setCodTipBien(String codTipBien) {
		this.codTipBien = codTipBien;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	public List getListTipoReqBien() {
		return listTipoReqBien;
	}
	public void setListTipoReqBien(List listTipoReqBien) {
		this.listTipoReqBien = listTipoReqBien;
	}
	public List getListTipoReqServicio() {
		return listTipoReqServicio;
	}
	public void setListTipoReqServicio(List listTipoReqServicio) {
		this.listTipoReqServicio = listTipoReqServicio;
	}
	public int getCant() {
		return cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	
	
}
