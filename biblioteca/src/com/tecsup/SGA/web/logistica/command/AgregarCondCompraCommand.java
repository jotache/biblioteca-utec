package com.tecsup.SGA.web.logistica.command;

public class AgregarCondCompraCommand {
	private String operacion;
	private String conComp;
	private String graba;
	private String msg;
	private String codAlumno;
	private String secuencial;
	private String tipo;
	private String textoDefecto;
	
	public String getTextoDefecto() {
		return textoDefecto;
	}
	public void setTextoDefecto(String textoDefecto) {
		this.textoDefecto = textoDefecto;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getConComp() {
		return conComp;
	}
	public void setConComp(String conComp) {
		this.conComp = conComp;
	}
	
	
}
