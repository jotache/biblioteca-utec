package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ActivarInterfazContableCommand {
	
	private String operacion;
	private String codUsuario;
	private String msg;
	private String codSede;
	private String codMeses;
	private String codAnios;
	private String codTipoMovimiento;
	private String dscEstado;
	private List listaTipoMovimiento;
	private List codListaMeses;
	private List codListaAnios;
	private String codOpciones;
	private String consteGenerada;
	private String codEstado;
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodMeses() {
		return codMeses;
	}
	public void setCodMeses(String codMeses) {
		this.codMeses = codMeses;
	}
	public String getCodAnios() {
		return codAnios;
	}
	public void setCodAnios(String codAnios) {
		this.codAnios = codAnios;
	}
	public String getCodTipoMovimiento() {
		return codTipoMovimiento;
	}
	public void setCodTipoMovimiento(String codTipoMovimiento) {
		this.codTipoMovimiento = codTipoMovimiento;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public List getListaTipoMovimiento() {
		return listaTipoMovimiento;
	}
	public void setListaTipoMovimiento(List listaTipoMovimiento) {
		this.listaTipoMovimiento = listaTipoMovimiento;
	}
	public List getCodListaMeses() {
		return codListaMeses;
	}
	public void setCodListaMeses(List codListaMeses) {
		this.codListaMeses = codListaMeses;
	}
	public List getCodListaAnios() {
		return codListaAnios;
	}
	public void setCodListaAnios(List codListaAnios) {
		this.codListaAnios = codListaAnios;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getConsteGenerada() {
		return consteGenerada;
	}
	public void setConsteGenerada(String consteGenerada) {
		this.consteGenerada = consteGenerada;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
}
