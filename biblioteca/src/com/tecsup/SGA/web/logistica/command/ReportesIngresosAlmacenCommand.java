package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class ReportesIngresosAlmacenCommand {

	private String operacion;
	private String codOpciones;
	private String codUsuario;
	private String codSede;
	private List listaSedes;
	private String codTipoRequerimiento;
	private List listCodTipoRequerimiento;
	private String fecInicio;
	private String fecFinal;
	private String codFamilia;
	private List listaCodFamilia;
	private String consteRadio;
	private String consteBienActivo;
	private String consteBienConsumible;
	private String msg;
	private String codTipoGastos;
	private List listaCodTipoGastos;
	private String codTipoCentroCosto;
	private List listaCodTipoCentroCosto;
	private String tituloReporte;
	private String nroReporte;
	private String codTipoReporte;
	
	private String codReporte1;
	private String codReporte2;
	private String codReporte3;
	private String codReporte4;
	private String codReporte6;
	private String codReporte11;
	private String dscUsuario;
	
	private String codBien;
	private String valRadioRep;
	private String consteReporteAlmacen;
	private String ConsteReporteAlmacenDev;
	private String periodo;
	private String consteSede;
	
	private String codMeses;
	private String codAnios;
	private List codListaMeses;
	private List codListaAnios;
	private String periodoActual;
	
	public String getPeriodoActual() {
		return periodoActual;
	}
	public void setPeriodoActual(String periodoActual) {
		this.periodoActual = periodoActual;
	}
	public String getCodMeses() {
		return codMeses;
	}
	public void setCodMeses(String codMeses) {
		this.codMeses = codMeses;
	}
	public String getCodAnios() {
		return codAnios;
	}
	public void setCodAnios(String codAnios) {
		this.codAnios = codAnios;
	}
	public List getCodListaMeses() {
		return codListaMeses;
	}
	public void setCodListaMeses(List codListaMeses) {
		this.codListaMeses = codListaMeses;
	}
	public List getCodListaAnios() {
		return codListaAnios;
	}
	public void setCodListaAnios(List codListaAnios) {
		this.codListaAnios = codListaAnios;
	}
	public String getConsteSede() {
		return consteSede;
	}
	public void setConsteSede(String consteSede) {
		this.consteSede = consteSede;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getValRadioRep() {
		return valRadioRep;
	}
	public void setValRadioRep(String valRadioRep) {
		this.valRadioRep = valRadioRep;
	}
	public String getConsteReporteAlmacen() {
		return consteReporteAlmacen;
	}
	public void setConsteReporteAlmacen(String consteReporteAlmacen) {
		this.consteReporteAlmacen = consteReporteAlmacen;
	}
	public String getConsteReporteAlmacenDev() {
		return ConsteReporteAlmacenDev;
	}
	public void setConsteReporteAlmacenDev(String consteReporteAlmacenDev) {
		ConsteReporteAlmacenDev = consteReporteAlmacenDev;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getCodReporte1() {
		return codReporte1;
	}
	public void setCodReporte1(String codReporte1) {
		this.codReporte1 = codReporte1;
	}
	public String getCodReporte2() {
		return codReporte2;
	}
	public void setCodReporte2(String codReporte2) {
		this.codReporte2 = codReporte2;
	}
	public String getCodReporte3() {
		return codReporte3;
	}
	public void setCodReporte3(String codReporte3) {
		this.codReporte3 = codReporte3;
	}
	public String getCodReporte4() {
		return codReporte4;
	}
	public void setCodReporte4(String codReporte4) {
		this.codReporte4 = codReporte4;
	}
	public String getCodReporte6() {
		return codReporte6;
	}
	public void setCodReporte6(String codReporte6) {
		this.codReporte6 = codReporte6;
	}
	public String getCodTipoReporte() {
		return codTipoReporte;
	}
	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}
	public String getCodTipoCentroCosto() {
		return codTipoCentroCosto;
	}
	public void setCodTipoCentroCosto(String codTipoCentroCosto) {
		this.codTipoCentroCosto = codTipoCentroCosto;
	}
	public List getListaCodTipoCentroCosto() {
		return listaCodTipoCentroCosto;
	}
	public void setListaCodTipoCentroCosto(List listaCodTipoCentroCosto) {
		this.listaCodTipoCentroCosto = listaCodTipoCentroCosto;
	}
	public String getTituloReporte() {
		return tituloReporte;
	}
	public void setTituloReporte(String tituloReporte) {
		this.tituloReporte = tituloReporte;
	}
	public String getNroReporte() {
		return nroReporte;
	}
	public void setNroReporte(String nroReporte) {
		this.nroReporte = nroReporte;
	}
	public String getCodTipoGastos() {
		return codTipoGastos;
	}
	public void setCodTipoGastos(String codTipoGastos) {
		this.codTipoGastos = codTipoGastos;
	}
	public List getListaCodTipoGastos() {
		return listaCodTipoGastos;
	}
	public void setListaCodTipoGastos(List listaCodTipoGastos) {
		this.listaCodTipoGastos = listaCodTipoGastos;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListaSedes() {
		return listaSedes;
	}
	public void setListaSedes(List listaSedes) {
		this.listaSedes = listaSedes;
	}
	public String getCodTipoRequerimiento() {
		return codTipoRequerimiento;
	}
	public void setCodTipoRequerimiento(String codTipoRequerimiento) {
		this.codTipoRequerimiento = codTipoRequerimiento;
	}
	public List getListCodTipoRequerimiento() {
		return listCodTipoRequerimiento;
	}
	public void setListCodTipoRequerimiento(List listCodTipoRequerimiento) {
		this.listCodTipoRequerimiento = listCodTipoRequerimiento;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFinal() {
		return fecFinal;
	}
	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public List getListaCodFamilia() {
		return listaCodFamilia;
	}
	public void setListaCodFamilia(List listaCodFamilia) {
		this.listaCodFamilia = listaCodFamilia;
	}
	public String getCodOpciones() {
		return codOpciones;
	}
	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}
	public String getConsteRadio() {
		return consteRadio;
	}
	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}
	public String getConsteBienActivo() {
		return consteBienActivo;
	}
	public void setConsteBienActivo(String consteBienActivo) {
		this.consteBienActivo = consteBienActivo;
	}
	public String getConsteBienConsumible() {
		return consteBienConsumible;
	}
	public void setConsteBienConsumible(String consteBienConsumible) {
		this.consteBienConsumible = consteBienConsumible;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	public String getCodReporte11() {
		return codReporte11;
	}
	public void setCodReporte11(String codReporte11) {
		this.codReporte11 = codReporte11;
	}
}
