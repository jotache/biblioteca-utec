package com.tecsup.SGA.web.logistica.command;

import java.util.List;

import com.tecsup.SGA.modelo.CotizacionProveedor;

public class CotDetBandejaProveedoresCommand {
	
	private String operacion;
	private String msg;
	private String codCotizacion;
	private String codCotizacionDet;
	private String condicion;
	private String codProveedor;	
	List<CotizacionProveedor> listaProveedor;
	private String codUsuario;
	
	private String codIndicesSeleccionados;
	private String longFlagCorreo;
	
	private String ruc; 
	private String razonSocial;
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCodCotizacionDet() {
		return codCotizacionDet;
	}
	public void setCodCotizacionDet(String codCotizacionDet) {
		this.codCotizacionDet = codCotizacionDet;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public List<CotizacionProveedor> getListaProveedor() {
		return listaProveedor;
	}
	public void setListaProveedor(List<CotizacionProveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getCodIndicesSeleccionados() {
		return codIndicesSeleccionados;
	}
	public void setCodIndicesSeleccionados(String codIndicesSeleccionados) {
		this.codIndicesSeleccionados = codIndicesSeleccionados;
	}
	public String getLongFlagCorreo() {
		return longFlagCorreo;
	}
	public void setLongFlagCorreo(String longFlagCorreo) {
		this.longFlagCorreo = longFlagCorreo;
	}
	
}
