package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class CotizacionBienesCommand {
	private String operacion;
	private String codAlumno;
	private String nroCotizacion;
	private String fechaIni;
	private String fechaFin;
	private String codTipoPago;
	private List listTipoPagpo;
	private String tipoBien;
	private String codTipoServicio;
	private List listTipoServicio;
	private List listCotizacion;
	private String tipo;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNroCotizacion() {
		return nroCotizacion;
	}
	public void setNroCotizacion(String nroCotizacion) {
		this.nroCotizacion = nroCotizacion;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getCodTipoPago() {
		return codTipoPago;
	}
	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	public List getListTipoPagpo() {
		return listTipoPagpo;
	}
	public void setListTipoPagpo(List listTipoPagpo) {
		this.listTipoPagpo = listTipoPagpo;
	}
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public List getListTipoServicio() {
		return listTipoServicio;
	}
	public void setListTipoServicio(List listTipoServicio) {
		this.listTipoServicio = listTipoServicio;
	}
	public List getListCotizacion() {
		return listCotizacion;
	}
	public void setListCotizacion(List listCotizacion) {
		this.listCotizacion = listCotizacion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	
}
