package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class CotSolDetalleProveedorCommand {
	private String operacion;
	private String usuario;
	private String txhCodProveedor;
	private String txtDesProveedor;
	private List lstResultado;
	private String txhCodCotizacion;
	private String txtRuc;
	
	public String getTxhCodProveedor() {
		return txhCodProveedor;
	}
	public void setTxhCodProveedor(String txhCodProveedor) {
		this.txhCodProveedor = txhCodProveedor;
	}
	public String getTxtRuc() {
		return txtRuc;
	}
	public void setTxtRuc(String txtRuc) {
		this.txtRuc = txtRuc;
	}
	public String getTxhCodCotizacion() {
		return txhCodCotizacion;
	}
	public void setTxhCodCotizacion(String txhCodCotizacion) {
		this.txhCodCotizacion = txhCodCotizacion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getTxtDesProveedor() {
		return txtDesProveedor;
	}
	public void setTxtDesProveedor(String txtDesProveedor) {
		this.txtDesProveedor = txtDesProveedor;
	}
	public List getLstResultado() {
		return lstResultado;
	}
	public void setLstResultado(List lstResultado) {
		this.lstResultado = lstResultado;
	}
		
}
