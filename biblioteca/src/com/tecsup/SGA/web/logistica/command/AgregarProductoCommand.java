package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarProductoCommand {
	private String operacion;
	private List listSede;
	private List listFamilia;
	private List listSubFamilia;
	private List listunidad;
	private List listUbicacion;
	private String codFamilia;
	private String codSubFamilia;
	private String codUnidad;
	private String codUbicacion;
	private String descripcion;
	private String condicion;
	private String min;
	private String max;
	private String precio;
	private String stockDis;
	private String stockAct;
	private String nroDias;
	private List listDescriptores;
	private List listdocRela;
	private String codAlumno;
	private String grabar;
	private String codSede;
	private String codSedeAux;
	private String nomPro;
	private String ubiFila;
	private String ubiColum;
	private String marca;
	private String modelo;
	private String imagen;
	private String msg;
	private String tipo;
	private String codProducto;
	private String codAdj;
	private String mens;
	private String ruta;
	private String ru;
	private List listProductos;
	private String codDescriptor;
	private String bien;
	private String chkInv;
	private String stockMostrar;
	private String codEstado;
	private List listEstado;
	private String stockDisponible;
	private String stockActual;
	private String preURef;
	//ALQD,23/02/09. NUEVA PROPIEDAD
	private String chkPromo;
	//ALQD,20/05/09. NUEVA PROPIEDAD
	private String chkNoAfecto;
	
	public String getChkNoAfecto() {
		return chkNoAfecto;
	}
	public void setChkNoAfecto(String chkNoAfecto) {
		this.chkNoAfecto=chkNoAfecto;
	}
	public String getChkPromo() {
		return chkPromo;
	}
	public void setChkPromo(String chkPromo) {
		this.chkPromo=chkPromo;
	}
	public String getStockDisponible() {
		return stockDisponible;
	}
	public void setStockDisponible(String stockDisponible) {
		this.stockDisponible = stockDisponible;
	}
	public String getStockActual() {
		return stockActual;
	}
	public void setStockActual(String stockActual) {
		this.stockActual = stockActual;
	}
	public String getPreURef() {
		return preURef;
	}
	public void setPreURef(String preURef) {
		this.preURef = preURef;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public List getListEstado() {
		return listEstado;
	}
	public void setListEstado(List listEstado) {
		this.listEstado = listEstado;
	}
	public String getStockMostrar() {
		return stockMostrar;
	}
	public void setStockMostrar(String stockMostrar) {
		this.stockMostrar = stockMostrar;
	}
	public String getChkInv() {
		return chkInv;
	}
	public void setChkInv(String chkInv) {
		this.chkInv = chkInv;
	}
	public List getListProductos() {
		return listProductos;
	}
	public void setListProductos(List listProductos) {
		this.listProductos = listProductos;
	}
	public String getRu() {
		return ru;
	}
	public void setRu(String ru) {
		this.ru = ru;
	}
	public String getMens() {
		return mens;
	}
	public void setMens(String mens) {
		this.mens = mens;
	}
	public String getCodAdj() {
		return codAdj;
	}
	public void setCodAdj(String codAdj) {
		this.codAdj = codAdj;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNomPro() {
		return nomPro;
	}
	public void setNomPro(String nomPro) {
		this.nomPro = nomPro;
	}
	public List getListSede() {
		return listSede;
	}
	public void setListSede(List listSede) {
		this.listSede = listSede;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListFamilia() {
		return listFamilia;
	}
	public void setListFamilia(List listFamilia) {
		this.listFamilia = listFamilia;
	}
	public List getListSubFamilia() {
		return listSubFamilia;
	}
	public void setListSubFamilia(List listSubFamilia) {
		this.listSubFamilia = listSubFamilia;
	}
	public List getListunidad() {
		return listunidad;
	}
	public void setListunidad(List listunidad) {
		this.listunidad = listunidad;
	}
	public List getListUbicacion() {
		return listUbicacion;
	}
	public void setListUbicacion(List listUbicacion) {
		this.listUbicacion = listUbicacion;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	public String getCodUnidad() {
		return codUnidad;
	}
	public void setCodUnidad(String codUnidad) {
		this.codUnidad = codUnidad;
	}
	public String getCodUbicacion() {
		return codUbicacion;
	}
	public void setCodUbicacion(String codUbicacion) {
		this.codUbicacion = codUbicacion;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getMin() {
		return min;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public String getMax() {
		return max;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getStockDis() {
		return stockDis;
	}
	public void setStockDis(String stockDis) {
		this.stockDis = stockDis;
	}
	public String getStockAct() {
		return stockAct;
	}
	public void setStockAct(String stockAct) {
		this.stockAct = stockAct;
	}
	public String getNroDias() {
		return nroDias;
	}
	public void setNroDias(String nroDias) {
		this.nroDias = nroDias;
	}
	public List getListDescriptores() {
		return listDescriptores;
	}
	public void setListDescriptores(List listDescriptores) {
		this.listDescriptores = listDescriptores;
	}
	public List getListdocRela() {
		return listdocRela;
	}
	public void setListdocRela(List listdocRela) {
		this.listdocRela = listdocRela;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getUbiFila() {
		return ubiFila;
	}
	public void setUbiFila(String ubiFila) {
		this.ubiFila = ubiFila;
	}
	public String getUbiColum() {
		return ubiColum;
	}
	public void setUbiColum(String ubiColum) {
		this.ubiColum = ubiColum;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getCodDescriptor() {
		return codDescriptor;
	}
	public void setCodDescriptor(String codDescriptor) {
		this.codDescriptor = codDescriptor;
	}
	public String getCodSedeAux() {
		return codSedeAux;
	}
	public void setCodSedeAux(String codSedeAux) {
		this.codSedeAux = codSedeAux;
	}
	public String getBien() {
		return bien;
	}
	public void setBien(String bien) {
		this.bien = bien;
	}
	
	
	
}
