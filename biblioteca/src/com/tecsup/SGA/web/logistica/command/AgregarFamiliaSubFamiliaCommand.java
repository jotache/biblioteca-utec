package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarFamiliaSubFamiliaCommand {
	private String operacion;
	private List listBien;
	private String codBien;
	private String codFamilia;
	private String codSubFamilia;
	private String familia;
	private String subFamilia;
	private List ListFamSubFam;
	private String msg;
	private String grabo;
	private String codAlumno;
	private String codProveedor;
	private String codFamiliaSubFamilia;

	public String getCodFamiliaSubFamilia() {
		return codFamiliaSubFamilia;
	}

	public void setCodFamiliaSubFamilia(String codFamiliaSubFamilia) {
		this.codFamiliaSubFamilia = codFamiliaSubFamilia;
	}

	public String getCodProveedor() {
		return codProveedor;
	}

	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getGrabo() {
		return grabo;
	}

	public void setGrabo(String grabo) {
		this.grabo = grabo;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public List getListBien() {
		return listBien;
	}

	public void setListBien(List listBien) {
		this.listBien = listBien;
	}

	public String getCodBien() {
		return codBien;
	}

	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}

	public String getCodFamilia() {
		return codFamilia;
	}

	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}

	public String getCodSubFamilia() {
		return codSubFamilia;
	}

	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}

	public String getFamilia() {
		return familia;
	}

	public void setFamilia(String familia) {
		this.familia = familia;
	}

	public String getSubFamilia() {
		return subFamilia;
	}

	public void setSubFamilia(String subFamilia) {
		this.subFamilia = subFamilia;
	}

	public List getListFamSubFam() {
		return ListFamSubFam;
	}

	public void setListFamSubFam(List listFamSubFam) {
		ListFamSubFam = listFamSubFam;
	}
}
