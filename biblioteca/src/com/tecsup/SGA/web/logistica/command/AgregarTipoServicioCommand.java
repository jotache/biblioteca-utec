package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AgregarTipoServicioCommand {
	private String operacion;
	private List listSer;
	private String codSer;
	private String tipSer;
	private String graba;
	private String msg;
	private String codAlumno;
	private String secuencial;
	private String tipo;
	private String cta;
	
	
	public String getCta() {
		return cta;
	}
	public void setCta(String cta) {
		this.cta = cta;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTipSer() {
		return tipSer;
	}
	public void setTipSer(String tipSer) {
		this.tipSer = tipSer;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListSer() {
		return listSer;
	}
	public void setListSer(List listSer) {
		this.listSer = listSer;
	}
	public String getCodSer() {
		return codSer;
	}
	public void setCodSer(String codSer) {
		this.codSer = codSer;
	}
	
	
}
