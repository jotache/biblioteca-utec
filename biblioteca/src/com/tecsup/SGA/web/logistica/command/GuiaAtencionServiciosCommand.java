package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class GuiaAtencionServiciosCommand {
	private String operacion;
	private String msg;
	private String seleccion;
	private String codUsuario;
	private String codSede;
	
	private String codRequerimiento;
	private String codReqDetalle;
	private String codGuia;
	private String codGuiaDetalle;
	private String nroGuia;
	private String fechaGuia;
	private String codEstado;
	private String estado;
	private String dscGuiaDetalle;//observacion
	private String codFactura;//codDocPago
	private String nroFactura;//nroDocPago
	private String fechaFactura;
	private String codOrden;
	private String fechaOrden;
	private String nroOS;		
		 
	public String getCodReqDetalle() {
		return codReqDetalle;
	}
	public void setCodReqDetalle(String codReqDetalle) {
		this.codReqDetalle = codReqDetalle;
	}
	public String getCodGuia() {
		return codGuia;
	}
	public void setCodGuia(String codGuia) {
		this.codGuia = codGuia;
	}
	public String getCodGuiaDetalle() {
		return codGuiaDetalle;
	}
	public void setCodGuiaDetalle(String codGuiaDetalle) {
		this.codGuiaDetalle = codGuiaDetalle;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDscGuiaDetalle() {
		return dscGuiaDetalle;
	}
	public void setDscGuiaDetalle(String dscGuiaDetalle) {
		this.dscGuiaDetalle = dscGuiaDetalle;
	}
	public String getCodFactura() {
		return codFactura;
	}
	public void setCodFactura(String codFactura) {
		this.codFactura = codFactura;
	}
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getFechaOrden() {
		return fechaOrden;
	}
	public void setFechaOrden(String fechaOrden) {
		this.fechaOrden = fechaOrden;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getFechaGuia() {
		return fechaGuia;
	}
	public void setFechaGuia(String fechaGuia) {
		this.fechaGuia = fechaGuia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(String fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public String getNroOS() {
		return nroOS;
	}
	public void setNroOS(String nroOS) {
		this.nroOS = nroOS;
	}	
}
