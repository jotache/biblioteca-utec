package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class PenBandejaVerCotizacionActualCommand {

	
	private String operacion;
	private String codSede;
	private String fecInicio;
	private String fecFinal;
	
	private String codTipoPago;
	
	private String txtDescripcion;
	
	private String codUsuario;
	private String codOpciones;
	private String txtHoraInicio;
	private String txtHoraFin;
	
	private List listaCodTipoBien;
	private String codTipoReq;
	private String consteRadio;
	
	private List listaBienesConsumibles;
	private List listaBienesActivos;
	private List listaServicios;
	private List listaBienes;

	public List getListaBienes() {
		return listaBienes;
	}

	public void setListaBienes(List listaBienes) {
		this.listaBienes = listaBienes;
	}

	public List getListaBienesConsumibles() {
		return listaBienesConsumibles;
	}

	public void setListaBienesConsumibles(List listaBienConsumibles) {
		this.listaBienesConsumibles = listaBienConsumibles;
	}

	public List getListaBienesActivos() {
		return listaBienesActivos;
	}

	public void setListaBienesActivos(List listaBienActivos) {
		this.listaBienesActivos = listaBienActivos;
	}

	public List getListaServicios() {
		return listaServicios;
	}

	public void setListaServicios(List listaServicios) {
		this.listaServicios = listaServicios;
	}

	public String getCodTipoReq() {
		return codTipoReq;
	}

	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}

	public String getFecFinal() {
		return fecFinal;
	}

	public void setFecFinal(String fecFinal) {
		this.fecFinal = fecFinal;
	}

	public String getCodTipoPago() {
		return codTipoPago;
	}

	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}

	public String getTxtDescripcion() {
		return txtDescripcion;
	}

	public void setTxtDescripcion(String txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getCodOpciones() {
		return codOpciones;
	}

	public void setCodOpciones(String codOpciones) {
		this.codOpciones = codOpciones;
	}

	public String getTxtHoraInicio() {
		return txtHoraInicio;
	}

	public void setTxtHoraInicio(String txtHoraInicio) {
		this.txtHoraInicio = txtHoraInicio;
	}

	public String getTxtHoraFin() {
		return txtHoraFin;
	}

	public void setTxtHoraFin(String txtHoraFin) {
		this.txtHoraFin = txtHoraFin;
	}

	public List getListaCodTipoBien() {
		return listaCodTipoBien;
	}

	public void setListaCodTipoBien(List listaCodTipoBien) {
		this.listaCodTipoBien = listaCodTipoBien;
	}

	public String getConsteRadio() {
		return consteRadio;
	}

	public void setConsteRadio(String consteRadio) {
		this.consteRadio = consteRadio;
	}

	
}
