package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class BuscarDescriptorCommand {
	private String operacion;
	private List listDescriptor;
	private String codDes;
	private List listDes;
	private String codAlumno;
	private String grabar;
	private String codProducto;
	private String codDescriptor;
	private String msg;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCodDescriptor() {
		return codDescriptor;
	}
	public void setCodDescriptor(String codDescriptor) {
		this.codDescriptor = codDescriptor;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public List getListDes() {
		return listDes;
	}
	public void setListDes(List listDes) {
		this.listDes = listDes;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public List getListDescriptor() {
		return listDescriptor;
	}
	public void setListDescriptor(List listDescriptor) {
		this.listDescriptor = listDescriptor;
	}
	public String getCodDes() {
		return codDes;
	}
	public void setCodDes(String codDes) {
		this.codDes = codDes;
	}
	
	
}
