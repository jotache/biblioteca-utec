package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class DocumentosRelacionadosCommand {
	private String docPago;
	private String codUsuario;
	private String numero;
	private String fecha;
	private String monto;
	private String descripcion;
	private String operacion;
	private String msg;
	private String graba;
	private String codDoc;
	private String codDocPago;
	private List listDocumentosRelacionados;
	private String msgE;
	private String codEstadoOrden;
	//ALQD,11/06/09.AGREGANDO 2 NUEVAS PROPIEDADES
	private String codTipMoneda;
	private String impTipCambio;
	private List listMonedas;
	//ALQD,01/07/09.AGREGANDO NUEVA PROPIEDAD
	private String impTipCamSoles;
	//ALQD,21/07/09.AGREGANDO TRES NUEVAS PROPIEDADES
	private String codProvee;
	private String nomProvee;
	private String rucProvee;
	
	public String getCodProvee() {
		return codProvee;
	}
	public void setCodProvee(String codProvee) {
		this.codProvee=codProvee;
	}
	public String getNomProvee() {
		return nomProvee;
	}
	public void setNomProvee(String nomProvee) {
		this.nomProvee=nomProvee;
	}
	public String getRucProvee() {
		return rucProvee;
	}
	public void setRucProvee(String rucProvee) {
		this.rucProvee=rucProvee;
	}
	public String getImpTipCamSoles() {
		return impTipCamSoles;
	}
	public void setImpTipCamSoles(String impTipCamSoles) {
		this.impTipCamSoles = impTipCamSoles;
	}
	public List getListMonedas() {
		return listMonedas;
	}
	public void setListMonedas(List listMonedas) {
		this.listMonedas = listMonedas;
	}
	public String getCodTipMoneda() {
		return codTipMoneda;
	}
	public void setCodTipMoneda(String codTipMoneda) {
		this.codTipMoneda=codTipMoneda;
	}
	public String getImpTipCambio() {
		return impTipCambio;
	}
	public void setImpTipCambio(String impTipCambio) {
		this.impTipCambio=impTipCambio;
	}
	public String getCodEstadoOrden() {
		return codEstadoOrden;
	}
	public void setCodEstadoOrden(String codEstadoOrden) {
		this.codEstadoOrden = codEstadoOrden;
	}
	public String getMsgE() {
		return msgE;
	}
	public void setMsgE(String msgE) {
		this.msgE = msgE;
	}
	public List getListDocumentosRelacionados() {
		return listDocumentosRelacionados;
	}
	public void setListDocumentosRelacionados(List listDocumentosRelacionados) {
		this.listDocumentosRelacionados = listDocumentosRelacionados;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getDocPago() {
		return docPago;
	}
	public void setDocPago(String docPago) {
		this.docPago = docPago;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getGraba() {
		return graba;
	}
	public void setGraba(String graba) {
		this.graba = graba;
	}
	public String getCodDoc() {
		return codDoc;
	}
	public void setCodDoc(String codDoc) {
		this.codDoc = codDoc;
	}
	public String getCodDocPago() {
		return codDocPago;
	}
	public void setCodDocPago(String codDocPago) {
		this.codDocPago = codDocPago;
	}
}
