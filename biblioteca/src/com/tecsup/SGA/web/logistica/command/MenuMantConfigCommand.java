package com.tecsup.SGA.web.logistica.command;

import java.util.List;

public class MenuMantConfigCommand {

	private long idOrden;
	private String descripcion;
	private String operacion;
	private String cboOrden;
	private List MtoMenuList;
	private String codAlumno;
	private String sedeSel;
	
	public long getIdOrden() {
		return idOrden;
	}
	public void setIdOrden(long idOrden) {
		this.idOrden = idOrden;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCboOrden() {
		return cboOrden;
	}
	public void setCboOrden(String cboOrden) {
		this.cboOrden = cboOrden;
	}
	public List getMtoMenuList() {
		return MtoMenuList;
	}
	public void setMtoMenuList(List mtoMenuList) {
		MtoMenuList = mtoMenuList;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getSedeSel() {
		return sedeSel;
	}
	public void setSedeSel(String sedeSel) {
		this.sedeSel = sedeSel;
	}		
}
