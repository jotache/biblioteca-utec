package com.tecsup.SGA.web.logistica.command;
import java.util.List;
public class AprobacionResponsableCommand {
	private String operacion;
	private String codAlumno;
	private String codRequerimiento;
	private String flag;
	private List centroCostos;
	private String codCentroCostos;
	private String nombre;
	private String apMaterno;
	private String apPaterno;
	private List listResponsable1;
	private List listResponsable2;
	private String sede;
	private String codigo;
	private String codigo1;
	private String graba;
	private String msg;
	private List listDetalle;
	private String haber;
	private List listComplejidad;
	private String codComplejidad;
	private String nroRequerimiento;
	private String codTipoReporte;
	private String dscUsuario;
	private String nomSede;
	private String codUsuario;
	
	public String getNroRequerimiento() {
		return nroRequerimiento;
	}

	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}

	public List getListComplejidad() {
		return listComplejidad;
	}

	public void setListComplejidad(List listComplejidad) {
		this.listComplejidad = listComplejidad;
	}

	public String getCodComplejidad() {
		return codComplejidad;
	}

	public void setCodComplejidad(String codComplejidad) {
		this.codComplejidad = codComplejidad;
	}

	public List getListDetalle() {
		return listDetalle;
	}

	public void setListDetalle(List listDetalle) {
		this.listDetalle = listDetalle;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getCodRequerimiento() {
		return codRequerimiento;
	}

	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public List getCentroCostos() {
		return centroCostos;
	}

	public void setCentroCostos(List centroCostos) {
		this.centroCostos = centroCostos;
	}

	public String getCodCentroCostos() {
		return codCentroCostos;
	}

	public void setCodCentroCostos(String codCentroCostos) {
		this.codCentroCostos = codCentroCostos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public List getListResponsable1() {
		return listResponsable1;
	}

	public void setListResponsable1(List listResponsable1) {
		this.listResponsable1 = listResponsable1;
	}

	public List getListResponsable2() {
		return listResponsable2;
	}

	public void setListResponsable2(List listResponsable2) {
		this.listResponsable2 = listResponsable2;
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getGraba() {
		return graba;
	}

	public void setGraba(String graba) {
		this.graba = graba;
	}

	public String getHaber() {
		return haber;
	}

	public void setHaber(String haber) {
		this.haber = haber;
	}

	public String getCodigo1() {
		return codigo1;
	}

	public void setCodigo1(String codigo1) {
		this.codigo1 = codigo1;
	}

	public String getCodTipoReporte() {
		return codTipoReporte;
	}

	public void setCodTipoReporte(String codTipoReporte) {
		this.codTipoReporte = codTipoReporte;
	}

	public String getDscUsuario() {
		return dscUsuario;
	}

	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}

	public String getNomSede() {
		return nomSede;
	}

	public void setNomSede(String nomSede) {
		this.nomSede = nomSede;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
	
}
