package com.tecsup.SGA.web.logistica.command;

import java.util.List;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CentroCosto;

public class AtenderRequerimientoBienesGuiaSalidaCommand {
	private String operacion;
	private String msg;
	private String seleccion;
	private String codUsuario;
	private String codSede;
	private String codRequerimiento;
	private String codGuia;
	
	private String nroGuia;
	private String fechaGuia;
	private String estado;
	//************************
	private String cadenaCodDetReq;
	private String cadenaCantSol;
	private String cadenaPrecios;
	private String cadenaCantEnt;	
	
	private List listaGuiaDetalle;	
	private String tamListaGuiaDetalle;	
	//para ruben
	private String codGuiaDetalle;
	private String codBien;
	private String cantidad;
	private String codEstado;
		
	private String solicitante;//jhpr 2008-09-26
	//ALQD,13/11/08. AŅADIENDIO NUEVAS PROPIEDADES
	private String nroRequerimiento;
	private String fecAprRequerimiento;
	private String nomCenCosto;
	private String fecCierre;
	public String getNroRequerimiento() {
		return nroRequerimiento;
	}
	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}
	public String getFecAprRequerimiento() {
		return fecAprRequerimiento;
	}
	public void setFecAprRequerimiento(String fecAprRequerimiento) {
		this.fecAprRequerimiento = fecAprRequerimiento;
	}
	public String getNomCenCosto() {
		return nomCenCosto;
	}
	public void setNomCenCosto(String nomCenCosto) {
		this.nomCenCosto = nomCenCosto;
	}
	public String getFecCierre() {
		return fecCierre;
	}
	public void setFecCierre(String fecCierre) {
		this.fecCierre = fecCierre;
	}
	/**
	 * @return the solicitante
	 */
	public String getSolicitante() {
		return solicitante;
	}
	/**
	 * @param solicitante the solicitante to set
	 */
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}
	/**
	 * @return the lugarTrabajo
	 */
	public String getLugarTrabajo() {
		return lugarTrabajo;
	}
	/**
	 * @param lugarTrabajo the lugarTrabajo to set
	 */
	public void setLugarTrabajo(String lugarTrabajo) {
		this.lugarTrabajo = lugarTrabajo;
	}
	private String lugarTrabajo;
	
	public String getCodGuiaDetalle() {
		return codGuiaDetalle;
	}
	public void setCodGuiaDetalle(String codGuiaDetalle) {
		this.codGuiaDetalle = codGuiaDetalle;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getTamListaGuiaDetalle() {
		return tamListaGuiaDetalle;
	}
	public void setTamListaGuiaDetalle(String tamListaGuiaDetalle) {
		this.tamListaGuiaDetalle = tamListaGuiaDetalle;
	}
	public List getListaGuiaDetalle() {
		return listaGuiaDetalle;
	}
	public void setListaGuiaDetalle(List listaGuiaDetalle) {
		this.listaGuiaDetalle = listaGuiaDetalle;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getFechaGuia() {
		return fechaGuia;
	}
	public void setFechaGuia(String fechaGuia) {
		this.fechaGuia = fechaGuia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodGuia() {
		return codGuia;
	}
	public void setCodGuia(String codGuia) {
		this.codGuia = codGuia;
	}
	public String getCadenaCodDetReq() {
		return cadenaCodDetReq;
	}
	public void setCadenaCodDetReq(String cadenaCodDetReq) {
		this.cadenaCodDetReq = cadenaCodDetReq;
	}
	public String getCadenaCantSol() {
		return cadenaCantSol;
	}
	public void setCadenaCantSol(String cadenaCantSol) {
		this.cadenaCantSol = cadenaCantSol;
	}
	public String getCadenaPrecios() {
		return cadenaPrecios;
	}
	public void setCadenaPrecios(String cadenaPrecios) {
		this.cadenaPrecios = cadenaPrecios;
	}
	public String getCadenaCantEnt() {
		return cadenaCantEnt;
	}
	public void setCadenaCantEnt(String cadenaCantEnt) {
		this.cadenaCantEnt = cadenaCantEnt;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}	
}
