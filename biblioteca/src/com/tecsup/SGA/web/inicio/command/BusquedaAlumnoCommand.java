package com.tecsup.SGA.web.inicio.command;

import java.util.List;

import com.tecsup.SGA.modelo.Alumno;

public class BusquedaAlumnoCommand {
	private String tipoRetorno;
	private String operacion;
	private List<Alumno> listaAlumnos;
	private String apellPaterno;
	private String ventanaOrigen;
	private String respuesta;
	public String getVentanaOrigen() {
		return ventanaOrigen;
	}

	public void setVentanaOrigen(String ventanaOrigen) {
		this.ventanaOrigen = ventanaOrigen;
	}

	private String ctrlUSE;
	public String getCtrlUSE() {
		return ctrlUSE;
	}

	public void setCtrlUSE(String ctrlUSE) {
		this.ctrlUSE = ctrlUSE;
	}

	public String getCtrlCOD() {
		return ctrlCOD;
	}

	public void setCtrlCOD(String ctrlCOD) {
		this.ctrlCOD = ctrlCOD;
	}

	public String getCtrlNOM() {
		return ctrlNOM;
	}

	public void setCtrlNOM(String ctrlNOM) {
		this.ctrlNOM = ctrlNOM;
	}

	private String ctrlCOD;
	private String ctrlNOM;
	
	public String getApellPaterno() {
		return apellPaterno;
	}

	public void setApellPaterno(String apellPaterno) {
		this.apellPaterno = apellPaterno;
	}

	public String getApellMaterno() {
		return apellMaterno;
	}

	public void setApellMaterno(String apellMaterno) {
		this.apellMaterno = apellMaterno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodCarnet() {
		return codCarnet;
	}

	public void setCodCarnet(String codCarnet) {
		this.codCarnet = codCarnet;
	}

	private String apellMaterno;
	private String nombre;
	private String codCarnet;
	
	public List<Alumno> getListaAlumnos() {
		return listaAlumnos;
	}

	public void setListaAlumnos(List<Alumno> listaAlumnos) {
		this.listaAlumnos = listaAlumnos;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getTipoRetorno() {
		return tipoRetorno;
	}

	public void setTipoRetorno(String tipoRetorno) {
		this.tipoRetorno = tipoRetorno;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	} 
}
