package com.tecsup.SGA.web.inicio.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class InicioCommand {
	private String codPeriodo;
	private String codEval;
	private String accion;
	private String sedeOperacion;
	
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getSedeOperacion() {
		return sedeOperacion;
	}
	public void setSedeOperacion(String sedeOperacion) {
		this.sedeOperacion = sedeOperacion;
	}
	
	
}