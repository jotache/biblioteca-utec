package com.tecsup.SGA.web.inicio.command;

import com.tecsup.SGA.modelo.*;
import java.util.*;

public class MenuCommand {
	private String accion;
	private String codSede;
	private List listaSedes;
	private List listaPeriodos;
	private String perfil;
	private String codPeriodo;
	private String codEval;
	private String opcion;
	//para biblioteca
	private String codUsuario;
	private String sedeUsuario;
	
	private String type;
	private String sedeSelWork;
	
	public List getListaPeriodos() {
		return listaPeriodos;
	}
	public void setListaPeriodos(List listaPeriodos) {
		this.listaPeriodos = listaPeriodos;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodEval() {
		return codEval;
	}
	public void setCodEval(String codEval) {
		this.codEval = codEval;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public List getListaSedes() {
		return listaSedes;
	}
	public void setListaSedes(List listaSedes) {
		this.listaSedes = listaSedes;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	/**
	 * @return the sedeUsuario
	 */
	public String getSedeUsuario() {
		return sedeUsuario;
	}
	/**
	 * @param sedeUsuario the sedeUsuario to set
	 */
	public void setSedeUsuario(String sedeUsuario) {
		this.sedeUsuario = sedeUsuario;
	}
	/**
	 * @return the sedeSelWork
	 */
	public String getSedeSelWork() {
		return sedeSelWork;
	}
	/**
	 * @param sedeSelWork the sedeSelWork to set
	 */
	public void setSedeSelWork(String sedeSelWork) {
		this.sedeSelWork = sedeSelWork;
	}
	
}