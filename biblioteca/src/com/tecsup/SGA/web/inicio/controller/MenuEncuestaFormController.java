package com.tecsup.SGA.web.inicio.controller;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.web.inicio.command.MenuCommand;

public class MenuEncuestaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MenuEncuestaFormController.class);	
	private ComunManager comunManager;
	private ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	public void setConfiguracionGeneracionManager(
			ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	
	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	
    	MenuCommand command = new MenuCommand();  	
    	   	
    	/*Recogiendo valores - Codigos*/
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null ){
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());
    	}
    	//***************************************************************
    	//para verificar si el usuario tiene encuestas por completar
		//y obtiene la cantidad de reportes por completar
		List lista=null;
		
		if(usuarioSeguridad!=null){
			lista=this.configuracionGeneracionManager.GetAllEncuestasPublicadas(usuarioSeguridad.getIdUsuario());
			String nroEncuestasCompletar="0";
			if(lista!=null){
				nroEncuestasCompletar=""+lista.size();
			}
			request.getSession().setAttribute("nroEncuestasCompletar", nroEncuestasCompletar);
			log.info("NRO ENCUESTAS COMPLETAR>>"+nroEncuestasCompletar+">>");
			
			//***************************************************************
		}	
	    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	MenuCommand control = (MenuCommand) command;
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/menuEncuesta","control",control);		
    }
    
    private void llenarSedes(MenuCommand command){
    	command.setListaSedes(this.comunManager.GetSedesByEvaluador(command.getCodEval()));
    }

}
