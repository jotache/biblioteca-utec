package com.tecsup.SGA.web.inicio.controller;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.inicio.command.MenuCommand;

public class MenuLogisticaFormController extends SimpleFormController{
private static Log log = LogFactory.getLog(MenuLogisticaFormController.class);
	private SeguridadManager seguridadManager; 
	private DetalleManager detalleManager;
	
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	MenuCommand command = new MenuCommand();  	
    	//command.setSedeSelWork((String)request.getParameter("txhSedeSel"));
    	
    	/*Recogiendo valores - Codigos*/
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	
    	String codSede = request.getParameter("txhCodSede");
    	
    	
    	if ( usuarioSeguridad != null )
    	{
    		command.setSedeSelWork((String)usuarioSeguridad.getSedeOperacion());
    		command.setCodEval(usuarioSeguridad.getIdUsuario());
    		command.setListaSedes(this.seguridadManager.getAllSedes());
    		command.setPerfil(usuarioSeguridad.getPerfiles());
    		command.setSedeUsuario(usuarioSeguridad.getSede());
    		if ( codSede != null )
    		{
    			command.setCodSede(codSede);
    			//command.setCodSede("L");
    		}
    		else{ try{ command.setCodSede(detalleManager.GetSedeByUsuario(usuarioSeguridad.getIdUsuario()));
    			
    		}catch (Exception e) {
				
			}
    			
    		}
    	}
    	    	
    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	MenuCommand control = (MenuCommand) command;  	
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/menuLogistica","control",control);		
    }
}
