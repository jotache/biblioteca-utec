package com.tecsup.SGA.web.inicio.controller;

import java.text.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.web.inicio.command.MenuCommand;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.common.CommonSeguridad;

public class MenuEvaluacionesFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MenuEvaluacionesFormController.class);	
	private ComunManager comunManager;	
	private TablaDetalleManager tablaDetalleManager;
	private AlumnoManager alumnoManager;
	
	public void setTablaDetalleManager(TablaDetalleManager tablaDetalleManager) {
		this.tablaDetalleManager = tablaDetalleManager;
	}

	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}
	
	private OperDocenteManager operDocenteManager;
	
	public void setOperDocenteManager(OperDocenteManager operDocenteManager) {
		this.operDocenteManager = operDocenteManager;
	}
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	MenuCommand command = new MenuCommand();  	
    	command.setOpcion("");
    	/*Recogiendo valores - Codigos*/
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodEval(usuarioSeguridad.getIdUsuario());
    		command.setCodPeriodo(usuarioSeguridad.getPeriodo());
    		
    		//thisss...
    		command.setCodSede(usuarioSeguridad.getSede());
    		
    		
    		log.info("formBackingObject usuarioSeguridad.getPeriodo():"+usuarioSeguridad.getPeriodo());
    		llenarSedes(command, request);    		
    		log.info("ListaSedes.size: "+command.getListaSedes().size());
    		
    		String sSede = "";
    		if(command.getCodSede()==null){
    			SedeBean sede = (SedeBean)command.getListaSedes().get(0);
    			sSede = sede.getCodSede();
    		}else{
    			sSede = command.getCodSede();
    		}
    			
    			
//    		if(command.getListaSedes().size() == 1){
//    			SedeBean sede = (SedeBean)command.getListaSedes().get(0);
//    			ArrayList<Periodo> listaPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoBySede(sSede);
    		ArrayList<Periodo> listaPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoBySedeUsuario(sSede,usuarioSeguridad.getIdUsuario());
    			if ( listaPeriodo != null ){
    				log.info("listaPeriodo.size():"+listaPeriodo.size());
    				command.setListaPeriodos(listaPeriodo);
    			}
//    		}
    		
    		//Solo si es alumno se le cargar� automaticamente el periodo
    		if (
    			  !(
					  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CARG") == 1 ||
					  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_ADMI") == 1 ||
					  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_JDEP") == 1 ||
					  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1 ||
					  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_PERF") == 1 || 
					  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_NOTA") == 1 ||
					  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_EMPR") == 1
				  )
		  	) 
			{
    			String codPeriodo = this.comunManager.getPeriodoByUsuario(usuarioSeguridad.getIdUsuario());
    			command.setCodPeriodo(codPeriodo);
    			usuarioSeguridad.setPeriodo(codPeriodo);
    			request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
			}
    		
    		log.info("Usuario Id:" + usuarioSeguridad.getIdUsuario());
    		
			if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CARG") == 1
					|| CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_ADMI") == 1
					|| CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_JDEP") == 1
					|| CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1) {

				
	    		/*Buscar cursos en ejecucion para registrar asistencia*/
	    		log.info("Informacion de curso para: "+usuarioSeguridad.getIdUsuario());
	    		List<CursoEvaluador> cursos = this.operDocenteManager.getCursoInTime(usuarioSeguridad.getIdUsuario());
	    		request.setAttribute("cursos", cursos);
    		
			}
			
			Alumno oAlumno = alumnoManager.obtenerAlumno(usuarioSeguridad.getIdUsuario());
			log.info(oAlumno);
			String debeDatosPer = "N";
			if(oAlumno!=null){
				if(oAlumno.getEdadActual()!=null){
					if(oAlumno.getFlgDatosPer().equals("?")  && Integer.valueOf(oAlumno.getEdadActual()).intValue()>17){
						debeDatosPer = "S";
					}
				}
			}
			request.setAttribute("debeAceptarLey", debeDatosPer);
			
			
			// Encuesta PFR
			String debeEncuesta = "N";
			if(oAlumno != null && alumnoManager.debeEncuestaPFR(Integer.parseInt(usuarioSeguridad.getIdUsuario())))
				debeEncuesta = "S";			
			
			request.setAttribute("hayEncuestaPFR", debeEncuesta);
			
    	}
    	
    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	MenuCommand control = (MenuCommand) command;
    	UsuarioSeguridad usuarioSeguridad = null;
//    	ArrayList<Periodo> listaPeriodo;
//    	Periodo periodo;
    	
    	if (control.getAccion().trim().equals("PERIODO"))
    	{
    		
    		usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    		if ( usuarioSeguridad != null )
			{
				usuarioSeguridad.setPeriodo(control.getCodPeriodo());
				
				//thissss...
				usuarioSeguridad.setSede(control.getCodSede());
				
				
				request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
			}
    		
    		log.info("Sede: " + control.getCodSede());
//    		listaPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoBySede(control.getCodSede());
    		ArrayList<Periodo> listaPeriodo = (ArrayList<Periodo>)this.comunManager.getPeriodoBySede(control.getCodSede());
    		if ( listaPeriodo != null ){
				log.info("listaPeriodo.size():"+listaPeriodo.size());
				control.setListaPeriodos(listaPeriodo);
			}
    		
//    		if ( listaPeriodo != null )
//    			if ( listaPeriodo.size() > 0)
//    			{
//    				periodo = listaPeriodo.get(0);
//    				log.info("Periodo:" + periodo.getCodigo());
//    				control.setCodPeriodo(periodo.getCodigo());
//    				usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
//    				if ( usuarioSeguridad != null )
//    				{
//    					usuarioSeguridad.setPeriodo(control.getCodPeriodo());
//    				}
//    				request.getSession().setAttribute("usuarioSeguridad", usuarioSeguridad);
//    			}
    		
    		
    	}
    	
    	
    	if (control.getAccion().trim().equals("ACEPTA"))
    	{
    		UsuarioSeguridad usuario = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");    		
    		alumnoManager.guardarRespuestaDatosPersonales(usuario.getIdUsuario(), "S");    		
    		request.setAttribute("debeAceptarLey", "N");    		
    	}
    	
    	control.setAccion("");
		log.info("onSubmit:FIN");
	    return new ModelAndView("/menuEvaluaciones","control",control);		
    }
    
    private void llenarSedes(MenuCommand command, HttpServletRequest request)
    {
      ArrayList<TipoTablaDetalle> arrSedes;
      ArrayList<SedeBean> listaSede;
      SedeBean sede;
      
  	  if ( CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CARG") == 1
	  || CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_ADMI") == 1
	  || CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_JDEP") == 1
	  || CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1 )
  	  {
    	command.setListaSedes(this.comunManager.GetSedesByEvaluador(command.getCodEval()));
  	  }
  	  else
  	  {
  		listaSede = new ArrayList<SedeBean>();
  		arrSedes = (ArrayList<TipoTablaDetalle>)this.tablaDetalleManager.getAllTablaDetalle( 
  					CommonConstants.TIPT_SEDE, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
  		if ( arrSedes != null)
  		{
  			if ( arrSedes.size() > 0 )
  			{
  				for (int j = 0; j < arrSedes.size() ; j++)
  				{
  					sede = new SedeBean();
  					sede.setCodSede(arrSedes.get(j).getDscValor1());
  					sede.setDscSede(arrSedes.get(j).getDescripcion());
  					listaSede.add(sede);
  				}
  			}
  		}
  		command.setListaSedes(listaSede);
  	  }
    }

	public void setAlumnoManager(AlumnoManager alumnoManager) {
		this.alumnoManager = alumnoManager;
	}
}
