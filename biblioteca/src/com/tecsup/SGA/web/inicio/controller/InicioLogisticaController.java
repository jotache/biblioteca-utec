package com.tecsup.SGA.web.inicio.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.inicio.*;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.*;

public class InicioLogisticaController extends SimpleFormController{
	private static Log log = LogFactory.getLog(InicioLogisticaController.class);

	private SeguridadManager seguridadManager;
	private DetalleManager detalleManager;
	public void setDetalleManager(DetalleManager detalleManager) {
		this.detalleManager = detalleManager;
	}

	/**
	 * @param seguridadManager the seguridadManager to set
	 */
	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	InicioCommand command = new InicioCommand();  	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		
    		command.setSedeOperacion(detalleManager.GetSedeByUsuario(usuarioSeguridad.getIdUsuario()));
    		
    		command.setCodEval(usuarioSeguridad.getNomUsuario());
    	}    	
    	    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	InicioCommand control = (InicioCommand) command;  
    	log.info("OPERACION:" + control.getAccion());
    	if (control.getAccion().equals("CERRAR_SESION"))
    	{
    		//JHPR:2008-04-11 Registrar el cierre de sesion en seguridad.
    		UsuarioSeguridad seg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
    		if (seg!=null) {
        		String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();    		
        		seguridadManager.RegistrarSesion(false, estacioncliente, seg.getCodUsuario(), CommonConstants.SGA_LOG);        		
    		}
    		    		
    		request.getSession().removeAttribute("usuarioSeguridad");
    		request.getSession().removeAttribute("reingreso");
			request.getSession().removeAttribute("contextoIngreso");
			request.getSession().removeAttribute("usuarioClave");
			
    		response.sendRedirect("/SGA/logeoLogistica.html");
    	}else if(control.getAccion().equals("CAMBIASEDE")){
    		UsuarioSeguridad seg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
    		//seg.setSede(sede)
    		
    		seg.setSedeOperacion(control.getSedeOperacion());
    		
    	}
    	
	    return new ModelAndView("/cabeceraLogistica","control",control);		
    }
}