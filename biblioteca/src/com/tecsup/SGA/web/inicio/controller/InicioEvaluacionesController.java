package com.tecsup.SGA.web.inicio.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.inicio.*;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.*;

public class InicioEvaluacionesController extends SimpleFormController{
	private static Log log = LogFactory.getLog(InicioEvaluacionesController.class);

	//JHPR: 2008-04-11 Registrar el termino de la sesion
	private SeguridadManager seguridadManager;
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	InicioCommand command = new InicioCommand();  	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		//command.setCodEval(usuarioSeguridad.getCodUsuario() + " - " + usuarioSeguridad.getNomUsuario());
    		command.setCodEval(usuarioSeguridad.getNomUsuario());
    	}    	
    	
    
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	InicioCommand control = (InicioCommand) command; 
    	log.info("OPERACION:" + control.getAccion());
    	if (control.getAccion().equals("CERRAR_SESION"))
    	{    	    		
    		
    		//JHPR:2008-04-11 Registrar el cierre de sesion en seguridad.
    		UsuarioSeguridad seg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
    		if (seg!=null) {
        		String estacioncliente = request.getRemoteAddr() + "/" + request.getRemoteHost();    		
        		seguridadManager.RegistrarSesion(false, estacioncliente, seg.getCodUsuario(), CommonConstants.SGA_EVA);        		
    		}
    		
    		request.getSession().removeAttribute("usuarioSeguridad");
    		
    		//JHPR 2008-04-15 Quitar variables de session.
    		request.getSession().removeAttribute("reingreso");
			request.getSession().removeAttribute("contextoIngreso");
			request.getSession().removeAttribute("usuarioClave");
			
    		response.sendRedirect("/SGA/logeoEvaluacion.html");
    	}    	
		
	    return new ModelAndView("/cabeceraEvaluaciones","control",control);		
    }

	public void setSeguridadManager(SeguridadManager seguridadManager) {
		this.seguridadManager = seguridadManager;
	}
}