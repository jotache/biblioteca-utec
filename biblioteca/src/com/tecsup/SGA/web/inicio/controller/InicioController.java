package com.tecsup.SGA.web.inicio.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.inicio.*;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.*;

public class InicioController extends SimpleFormController{
	private static Log log = LogFactory.getLog(InicioController.class);

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	
    	InicioCommand command = new InicioCommand();  	
    	   	
    	/*Recogiendo valores - Codigos*/
    	command.setCodPeriodo((String)request.getParameter("txhPeriodo"));
    	command.setCodEval((String)request.getParameter("txhEval"));
    	
    	/*<Prueba>*/
    	command.setCodPeriodo("120");
    	command.setCodEval("jmoran");
    	/*</Prueba>*/
    	
    	
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
   
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	
    	InicioCommand control = (InicioCommand) command;  	
    	
	    return new ModelAndView("/cabecera","control",control);		
    }
}