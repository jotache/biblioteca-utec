package com.tecsup.SGA.web.inicio.controller;

import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.web.inicio.command.InicioCommand;
import com.tecsup.SGA.web.inicio.*;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.*;

public class InicioEncuestaController extends SimpleFormController{
	private static Log log = LogFactory.getLog(InicioEncuestaController.class);

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	InicioCommand command = new InicioCommand();  	
    	
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		//command.setCodEval(usuarioSeguridad.getCodUsuario() + " - " + usuarioSeguridad.getNomUsuario());
    		command.setCodEval(usuarioSeguridad.getNomUsuario());
    	}    	
    	
    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	InicioCommand control = (InicioCommand) command;  	
    	
    	if (control.getAccion().equals("CERRAR_SESION")){
    		request.getSession().removeAttribute("usuarioSeguridad");
    		response.sendRedirect("/SGA/logeoEncuesta.html");
    	}
    	
		log.info("onSubmit:FIN");
	    return new ModelAndView("/cabeceraEncuesta","control",control);		
    }
}