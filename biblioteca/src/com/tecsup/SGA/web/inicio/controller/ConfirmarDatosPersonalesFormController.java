package com.tecsup.SGA.web.inicio.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.web.inicio.command.BusquedaAlumnoCommand;

public class ConfirmarDatosPersonalesFormController extends
		SimpleFormController {
	private static Log log = LogFactory.getLog(ConfirmarDatosPersonalesFormController.class);
	private AlumnoManager alumnoManager;
	public void setAlumnoManager(AlumnoManager alumnoManager) {
		this.alumnoManager = alumnoManager;
	}

	public ConfirmarDatosPersonalesFormController(){
		super();
		setCommandClass(BusquedaAlumnoCommand.class);
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		BusquedaAlumnoCommand command = new BusquedaAlumnoCommand();
		return command;
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		BusquedaAlumnoCommand control = (BusquedaAlumnoCommand) command;
		log.info("OPERACION:" + control.getOperacion());
		
		UsuarioSeguridad usuario = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
		
		String rptaDatosPer = alumnoManager.guardarRespuestaDatosPersonales(usuario.getIdUsuario(), control.getRespuesta());
		
		request.setAttribute("rptaDatosPer", rptaDatosPer);
		
		return new ModelAndView("/confirmarDatosPersonales","control",control);

	}
}
