package com.tecsup.SGA.web.inicio.controller;

import java.text.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.web.inicio.command.MenuCommand;
import com.tecsup.SGA.bean.UsuarioSeguridad;

public class MenuBibliotecaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(MenuBibliotecaFormController.class);	
	private ComunManager comunManager;	
	private BiblioGestionMaterialManager biblioGestionMaterialManager; 
	/**
	 * @param biblioGestionMaterialManager the biblioGestionMaterialManager to set
	 */
	public void setBiblioGestionMaterialManager(
			BiblioGestionMaterialManager biblioGestionMaterialManager) {
		this.biblioGestionMaterialManager = biblioGestionMaterialManager;
	}

	public void setComunManager(ComunManager comunManager) {
		this.comunManager = comunManager;
	}

	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI");
    	MenuCommand command = new MenuCommand();  	
    	command.setSedeSelWork((String)request.getParameter("txhSedeSel"));
    	/*Recogiendo valores - Codigos*/
    	UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
    	if ( usuarioSeguridad != null )
    	{
    		command.setCodUsuario(usuarioSeguridad.getIdUsuario());    		
    		/*command.setCodEval(usuarioSeguridad.getIdUsuario());
    		command.setCodPeriodo(usuarioSeguridad.getPeriodo());*/
    		command.setSedeUsuario(usuarioSeguridad.getSede());
    		command.setSedeSelWork(command.getSedeUsuario());
    		llenarSedes(command);
    		
    	}
    	log.info("formBackingObject:FIN");
        return command;
    } 
	
    protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
    /**
     * Redirect to the successView when the cancel button has been pressed.
     */
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	MenuCommand control = (MenuCommand) command;  	
    	
		log.info("onSubmit:FIN");
		
	    return new ModelAndView("/menuBiblioteca","control",control);		
    }
    
    private void llenarSedes(MenuCommand command)
    {
    	command.setListaSedes(this.biblioGestionMaterialManager.GetSedesBibPorUsuario(command.getCodUsuario()));
    }
}
