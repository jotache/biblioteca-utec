package com.tecsup.SGA.exception;
/**
 * @author jsierra
 * 
 * Representa a un error cuando el formato del archivo o
 * documento es inv�lido.
 * 
 * @version 1.0
 * @modelguid {98E41F54-CF0D-4296-A78A-FEC3B7C7A9AA}
 */
public class FormatoInvalidoException extends Exception {
	
	/**
	 * 
	 * @param s
	 */
	public FormatoInvalidoException(){
		super();
	}
	public FormatoInvalidoException(String s){
		super(s);
	}
	
	/**
	 * 
	 * @param e
	 */
	public FormatoInvalidoException(Exception e){
		super(e.getMessage());
	}
}

