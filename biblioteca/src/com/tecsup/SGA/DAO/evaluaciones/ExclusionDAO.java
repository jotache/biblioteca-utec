package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;
public interface ExclusionDAO {
	
	/**
	 * @param codCurso
	 * @param nombre
	 * @param apellido
	 * @param codSeccion
	 * @return Lista de alumnos con flag de exclusiones por curso.
	 */
	public List getAllExclusionesByCurso(String codCurso, String nombre, String apellido
				, String codSeccion, String codPeriodo);
	
	/**
	 * @param codCurso
	 * @param cadDatos
	 * @param nroRegistros
	 * @param codUsuario
	 * @return Inserta y actualiza las exclusiones por curso de manera masiva.
	 */
	public String insertExclusiones(String codCurso, String cadDatos, String nroRegistros
			, String codUsuario, String codPeriodo, String codSeccion);
}
