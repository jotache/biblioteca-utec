package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

import com.tecsup.SGA.modelo.Periodo;

public interface MtoPeriodosDAO {
	public List getPeriodos();
	
	/**
	 * @param sede
	 * @return Saca el periodo Activo por Sede.
	 */
	
	public List<Periodo> getPeriodoBySede(String sede);
	public List<Periodo> getPeriodoBySedeUsuario(String sede, String codUsuario);
	
	/**
	 * @param sede
	 * @return Saca el periodo Activo por Sede.
	 */
	public List<Periodo> getPeriodoById(String codSede);
	
	public String getPeriodoByUsuario(String usuario);
	
}
