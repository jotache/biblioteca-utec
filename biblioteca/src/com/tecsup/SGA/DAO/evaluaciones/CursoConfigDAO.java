package com.tecsup.SGA.DAO.evaluaciones;

import java.util.*;

public interface CursoConfigDAO {	
	
	public List getCursoConfigById(String codCurso);
		
	public String InsertCursoConfig(String codCurso, String regla, String condicion,
			String verdadero01, String verdadero02, String verdadero03, String verdadero04,
			String falso01, String falso02, String falso03, String falso04,
			String opeVer01, String opeVer02, String opeVer03, String opeVer04,
			String opeFal01, String opeFal02, String opeFal03, String opeFal04,
			String usuario);
	
	public String UpdateCursoConfig(String codCurso, String regla, String condicion,
			String verdadero01, String verdadero02, String verdadero03, String verdadero04,
			String falso01, String falso02, String falso03, String falso04,
			String opeVer01, String opeVer02, String opeVer03, String opeVer04,
			String opeFal01, String opeFal02, String opeFal03, String opeFal04,
			String usuario);
}
