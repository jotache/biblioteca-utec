package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

public interface AsistenciaDAO {
	/**
	 * @return
	 */
	public String getIdAsistencia();
	
	/**
	 * @param codAsistencia
	 * @param codAlumno
	 * @param codCurso
	 * @param nroSesion
	 * @param fecha
	 * @param tipoAsist
	 * @param usuario
	 * @return 
	 */
	public String insertAsistencia(String codPeriodo, String codAsistencia, String codAlumno, String codCurso, String tipoSesion, 
									String codSeccion, String nroSesion, String fecha, String tipoAsist, String usuario, String flagProg);
	
	/**
	 * @param codCurso
	 * @param codSeccion
	 * @return
	 */
	public List getAllFechaAsistenciaCurso(String codPeriodo, String codCurso, 
			String codProducto, String codEspecialidad, String tipoSesion, 
			String codSeccion);
	
	/**
	 * @param codAsistencia
	 * @param nombre
	 * @param apellido
	 * @param nroSesion
	 * @return
	 */
	public List getAllAlumnosXasistencia(String codPeriodo, String codAsistencia, String nombre, String apellido, String nroSesion, 
										String codCurso, String codProducto, String codEspecialidad, String tipoSesion, 
										String codSeccion);
	
	
	/**
	 * @param codCurso
	 * @param tipoSesion
	 * @param codSeccion
	 * @param nroSesion
	 * @param fecha
	 * @return Elimina una fecha de asistencia siempre y cuando sea programada
	 */
	public String deleteAsistencia(String codCurso, String tipoSesion, String codSeccion, String nroSesion, String fecha);
	
}
