package com.tecsup.SGA.DAO.evaluaciones;

import java.util.*;

import com.tecsup.SGA.modelo.*;
          
public interface OperDocenteDAO {
	
	public List<CursoEvaluador> getCursoInTime(String evaluador);
	
	public List getAllCursos(String periodo, String evaluador, String producto, String especialidad, String curso,
			String codEvalSeleccionado, String dscCurso);
	
	public List getAllSeccionCurso(String tipoSesion, String codCurso);
	
	public List getAllAlumnosCurso(String codPeriodo, String codCurso,
									String codProducto, String codEspecialidad, String tipoSesion,
									String codSeccion, String nombre, String apellido);
	public List GetAllTipoEvaluaciones();
	
	public List GetAllEvaluaciones(String codPeriodo,String codPro,String codEspe, String codCurso
    		, String seccion, String codEvaluador, String codTipoEva);
	
	public String InsertPeso(String codPeriodo, String codId, String codCurso,String codPro, String codEspe, String codSeccion, String codTipoEva, String peso
    		,String codEva, String flag, String usuCrea);

	public List GetAllEvaluadores(String codCurso,String codPeriodo);
	
	public String DeleteEvaluaciones(String codPeriodo,String codPro,String codEspe, String codCurso,String CodSeccion, String codTipoEva
    		, String cadCod, String nroReg, String  usuCrea);
	
	public NumeroMinimo GetAllNumeroMinimo(String codProducto,String codPeriodo);

	public List GelAllFormacionEmpresa(String codPeriodo, String codProducto
			,String codEspecialidad, String nombre, String apPaterno);
	
	public String InsertEmpresa(String codPeriodo, String codProducto, String codEspecilidad, String codCurso,
			String codAlumno, String tipoPractica, String nombreEmpresa,String fechaInicio,
			String fechaFin, String CanridadHoras,String codEstado,String observacion,
			String nota,String horaIni,String horaFin,String indSabado,String indDomingo,String usuCrea);

	public String InsertEmpresaId(String codPeriodo, String codProducto, String codEspecilidad, String codCurso,
			String codAlumno, String tipoPractica, String nombreEmpresa,String fechaInicio,
			String fechaFin, String CanridadHoras,String codEstado,String observacion,
			String nota,String horaIni,String horaFin,String indSabado,String indDomingo,String usuCrea);
	
	public List GetAllDetalleEvalParcial(String codPeriodo,String codCurso, String codSeccion,
			String nombre, String apellido, String codNroEval,
			String codTipoEval,String codProducto,
			String codEspecialidad);	
	
	public String AdjuntarArchivo(String codProducto, String codPeriodo, String codAlumno, String codCurso,
			String nomNuevoArchivo, String nomArchivo, String usucrea);
	
	public HorasXalumnos HorasPorAlumno(String codPeriodo,String codEspecialidad
    		, String codAlumno, String codTipoPractica);

	public List EmpresaXAlumno(String codPeriodo,String codEspecialidad
    		, String codAlumno, String codTipoPractica); 
	
	public Producto ProductoxCurso(String codCurso);
	
	public List GetAllPeriodo();
	
	public String ModificarFormacionEmpresa(String codId, String codPeriodo, String codProducto, String codEspecialidad
    		,String codCurso, String codAlumno, String tipoPractica, String nombreEmpresa, String fechaInicio
    		,String fechaFin, String cantHoras, String codEstado,String observacion,String nota,String horaIni,
    		String horaFin,String indSabado,String indDomingo, String usuCrea);
	
	public List GetAllAdjuntoEmpresa(String codPeriodo, String codProducto
			,String codAlumno, String codCurso);

	public List getAllTipoPracticaxCursoxEval(String codPeriodo,
			String codCurso, String codEval, String codProducto,
			String codEspecialidad);

	public List getAllSeccionesxCursoxEval(String codPeriodo, String codCurso,
			String codEval,String codProducto,String codEspecialidad, String cboEvaluacionParcial);

	public List getAllNroEvaxCursoxEva(String codPeriodo, String codCurso,
			String codEval, String codProducto, String codEspecialidad,
			String cboEvaluacionParcial, String cboSeccion);
	
	//TipoSesionCurso
	public List getAllSesionXCurso(String codCurso);
	
	// CACT : 15-04-2008 Listado de Evaluadores para los jefes de Departamento
	public List<Evaluador> getEvaluadoresByJefeDep(String codEvaluador, String codSede, String codPeriodo, String tipoUsuario);
	
	// CACT : 17-04-2008 Lista de Secciones por responsable de ltomar asistencia
	public List<CursoEvaluador> GetAllSeccionesxCursoxResp(String codResponsable, String codCurso, String codTipoSesion);
	
	//JHPR: 2008-06-04 Cerrar registro de notas.
	public String cerrarRegistroNotas(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario, String flagCerrado);
	
	//JHPR: 2008-06-04
	public Parciales obtenerDefinicionParcial(String codigo);

	//JHPR: 2008-06-04
	public List<Parciales> obtenerDefinicionesParciales(String codCurso,
			String codSeccion, String TipoEvaluacion,String NroEvaluacion, String Estado);
	
	public List<DefNroEvalParciales> consultarCierreNotas(String sede,String codPeriodo,String codProducto,String codEspecialida);
	
	public List<Evaluador> obtenerEvaluadorSeccion(String codCursoEjec,String codSeccion);
	
	public String abrirRegistroNotas(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario);
	
	public String cerrarRegistroAsistencia(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario, String flagCerrado);

	public String abrirRegistroAsistencia(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario);
	//	
	public List<Alumno> listarDesaprobadosXDI(String curso, String seccion, String nombres, String apellidos);
}
