package com.tecsup.SGA.DAO.evaluaciones;
import java.util.List;
public interface MtoConfiguracionComponentesDAO {

	public List getComponentes(String CODPRODUCTO, String CODESPECIALIDAD, String CODCICLO, String CODCURSO, 
			String CODETAPA, String CODPROGRAMA);
	public String InsertComponentes(String codProducto, String CODESPECIALIDAD, String CODCICLO, String CODCURSO,
			String CODETAPA, String CODPROGRAMA, String CADCOMPONENTE, String NROREGISTROS, String CODUSUARIO);
	public List getEspecialidadByProducto(String CODPRODUCTO);
	public List getProgramasByProducto(String CODPRODUCTO, String codEtapa);
	public List getCursosByProducto(String CODPRODUCTO, String codPeriodo);
	public List getCicloByCursoAndProducto(String CODPRODUCTO, String CodCurso);
	public List getCicloByProductoEspecialidadPrograma(String codProducto, String codEspecialidad, 
			String codPrograma, String codPeriodo);
	public List getCursoByProductoEspecialidadCiclo(String codProducto, String codPrograma, 
			String codEspecialidad, String codCiclo, String codEtapa, String codPeriodo);
	
	public List getCursoByProductoEspecialidadCicloExt(String codProducto, String codPrograma, 
			String codEspecialidad, String codCiclo, String codEtapa, String codPeriodo);

	//JHPR: 2008-05-20 Listar Cursos de Nota Externa...
	/*public List ListarCursosNotaExternaXProductoCiclo(String codProducto,String codEspecialidad,
			String codCiclo, String codPrograma,String codEtapa, String codPeriodo);
			*/
}
