package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.ConfiguracionNotaExterna;

public interface NotaExternaDAO {
	/**
	 * @return
	 */	
	public String UpdateNotaExterno(String codPeriodo,
			String codProducto, String codAlumno, String codCurso, 
			String codCiclo, String codTipo, String notaExterno,
			String codEspecialidad, String codEtapa, String codPrograma, String usuario);
	
	public String InsertNotaExterno(String codPeriodo,
			String codProducto, String codAlumno, String codCurso, 
			String codCiclo, String codTipo, String notaExterno,
			String codEspecialidad, String codEtapa, String codPrograma, String usuario);
	
	public String InsertAdjuntoCasoCat(String codPeriodo, String codProducto, String codCiclo,
			String nuevoArchivo, String archivo, String usuario);
	
	public List getAdjuntoCasoCatById(String codPeriodo,String codProducto,String codCiclo);
	
	public List getAllCursosNotaExterna();
	
	//JHPR 2008-05-05 Calcular Nota Tecsup.
	public ConfiguracionNotaExterna configuracionNotaExterna(String codCursoEjec);
	/*public String InsertNotaExterno(String codPeriodo, 
	 * 		String codProducto,String codCiclo, String codCurso, String cadenaDatos,
			String tipoReg,String usuario,String nroRegistros);*/
	public String InsertNotaExterno(String codPeriodo, String codProducto, String cadenaDatos,
			String tipoReg,String usuario,String nroRegistros);
	public List<ConfiguracionNotaExterna> listaConfiguracionNotaExterna(String codPeriodo);
}
