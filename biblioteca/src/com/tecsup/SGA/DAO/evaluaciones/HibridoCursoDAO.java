package com.tecsup.SGA.DAO.evaluaciones;

import java.util.*;

public interface HibridoCursoDAO {
	public List getHibridoCursoById(String codigo);
	public String InsertHibridoCurso(String codigo, String cadCodCursos, String nroRegistros, String codUsuario);
	public String UpdateHibridoCurso(String codigo, String cadCodCursos, String nroRegistros, String codUsuario);	
}
