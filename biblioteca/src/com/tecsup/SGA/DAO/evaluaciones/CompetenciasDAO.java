package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

import com.tecsup.SGA.modelo.CompetenciasPerfil;
public interface CompetenciasDAO {
	
	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codSeccion
	 * @param nombre
	 * @param apellido
	 * @return Lista las competencias por curso.
	 */
	public List getAllCompetenciasByCurso(String codPeriodo, String codCurso, String codSeccion
			, String nombre, String apellido, String codTipoSesion);
	
	/**
	 * @param codCurso
	 * @param codSeccion
	 * @param codAlumno
	 * @param tipoOpe 
	 * @return Lista los resultados de los alumnos por competencia.
	 */
	public List<CompetenciasPerfil> getAllCompetenciasByCursoByAlumno(String codPeriodo, String codCurso, String codSeccion
			, String codAlumno, String tipoOpe);
	
	/**
	 * @param codAlumno
	 * @param codCurso
	 * @param codPeriodo
	 * @param tipoOpe
	 * @param nroComp
	 * @param datosComp
	 * @param codUsuario
	 * @return Insert de forma masiva la evaluaciones de las competencias para un alumno
	 */
	public String insertCompetenciasByAlumno(String codAlumno,String codCurso,String codPeriodo,String tipoOpe,
    		String nroComp,String datosComp,String codUsuario, String codSeccion, String codTipoSesion);
	
	/**
	 * @param codPeriodo
	 * @param codEvaluador
	 * @param nomAlumno
	 * @param apeAlumno
	 * @return Lista de alumnos por evaludor por periodo
	 */
	public List<CompetenciasPerfil> getAllAlumnosByTutor(String codPeriodo, String codEvaluador
														, String nomAlumno, String apeAlumno);

	
	/**
	 * @param codPeriodo
	 * @param codEvaluador
	 * @param codAlumno
	 * @return Lista consolidada de alumno por evaluador
	 */
	public List<CompetenciasPerfil> getAllComsByAlumno(String codPeriodo, String codEvaluador
													, String codAlumno);
	
	/**
	 * @param codPeriodo
	 * @param codEvaluador
	 * @param codAlumno
	 * @param codPerfil
	 * @param tipoOpe
	 * @return Lista el detalle de las competencias por perfil
	 */
	public List<CompetenciasPerfil> getAllCompPerfilByAlumno(String codPeriodo, String codEvaluador
											, String codAlumno, String codPerfil, String tipoOpe);	
}
