package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

public interface MtoParametrosGeneralesDAO {
	
	public List getAllParametrosGenerales(String codProducto, String codPeriodo);
	
	public List getProductos();
	
	public String Insert_O_UpdateParametrosGenerales(String codProducto, String codPeriodo, String codTabla
			, String valor, String codUsuario);
}
