package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

public interface ConsultasReportesDAO  {
	public List getAllInformacionGeneral(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo);

	public List getAllInformacionGeneralDatos(String codPeriodo,
			String codProducto, String codAlumno, String codCiclo);

	public List getAllDetalleInformacionGral(String codPeriodo,
			String codProducto, String codAlumno, String codCiclo,
			String codCurso);

	public List getAllDetalleAsistencia(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso);

	public List getAllDetallePruebaAula(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso);

	public List getAllDetallePruebaLab(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso);

	public List getAllDetalleExamenes(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso);

	public List getAllDetallePruebaTaller(String codPeriodo,
			String codProducto, String codAlumno, String codCiclo,
			String codCurso);

	public List getProductosxAlumno(String codPeriodo, String codAlumno);

	public List getDatosxProductoxAlumno(String codPeriodo, String cboProducto,
			String codAlumno);

	public List getAllPeriodoxProducto(String codAlumno, String codPeriodo);

	public List getObtenerRankinProm(String codAlumno, String codProducto,
			String codEspecialidad, String codPeriodo);
	
	//Eddy Dominguez - 22/04/2008
	public List GetAllRepNotasAvance(String codPeriodo,String sede,String codProducto);
	
	public List GetAllEstadoFinalAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad);
	
	public List GetAllRepNotasFinal(String codPeriodo,String sede,String codProducto);
	
	public List GetAllRepestExamenes(String codPeriodo,String sede,String codProducto,String codEspe);

	public List GetAllCursosAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad,String usuario);
}
