package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

import com.tecsup.SGA.bean.EvaluacionesParciales;

public interface IncidenciaDAO {
	public List getAllIncidencias(String codPeriodo, String codAlumno, String codCurso, String tipoOpe
			, String codTipoSesion, String codSeccion);

	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codProducto
	 * @param codEspecialidad
	 * @param codTipoSesion
	 * @param codSeccion
	 * @param tipoOpe
	 * @param nombre
	 * @param apellido
	 * @return Incidencias por curso
	 */
	public List getAllIncidenciasByCurso(String codPeriodo, String codCurso, String codProducto
    		, String codEspecialidad, String codTipoSesion, String codSeccion
    		, String tipoOpe, String nombre, String apellido);
	
	
	/**
	 * @param codIncidencia
	 * @param codCurso
	 * @param codTipoSesion
	 * @param codSeccion
	 * @param codAlumno
	 * @param fecha
	 * @param fechaIni
	 * @param fechaFin
	 * @param dscIncidencia
	 * @param tipoIncidencia
	 * @param tipoOpe
	 * @param usuario
	 * @return Inserta incidencia.
	 */
	public String insertIncidencia(String codIncidencia, String codCurso, String codTipoSesion, String codSeccion
    		, String codAlumno, String fecha, String fechaIni, String fechaFin
			, String dscIncidencia, String tipoIncidencia, String tipoOpe
			, String usuario);

	public List getAllNroEvaluacionesxParciales(String codPeriodo,String codSeccion,String codCurso,
			String codEvaluacionParcial);

	public String deleteIncidencia(String codIncidencia, String codUsuario);

	public List<EvaluacionesParciales> getAllEvaluacionesParcialesxCurso(String codPeriodo,String codCurso,
			String codSeccion, String strNombre, String strApellido,
			String cboNroEvaluacion,String cboEvaluacionParcial,String codProducto,
			String codEspecialidad,String fecEvaluacion);
}
