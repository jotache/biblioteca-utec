package com.tecsup.SGA.DAO.evaluaciones;

import java.util.*;

public interface SistevalHibridoDAO {
	public List getAllSistevalHibrido(String codigo);
	
	public List getSistevalHibridoById(String codigo);
	
	public String InsertSistevalHibrido(String inCodigo, String inDescripcion, String inCadEvaluacion, String inNroRegistros, String inCodUsuario);
	
	public String UpdateSistevalHibrido(String inCodigo, String inDescripcion, String inCadEvaluacion, String inNroRegistros, String inCodUsuario);
	
	public String DeleteSistevalHibrido(String seleccion,String usuModi);	
	
}
