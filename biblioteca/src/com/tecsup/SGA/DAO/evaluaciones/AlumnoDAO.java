package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;
import java.util.StringTokenizer;

import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Curso;

public interface AlumnoDAO {
	/**
	 * @return
	 */
	
	public List getAllAlumnoCasoEspecial(String codPeriodo, String codProducto, String codEspecialidad, String codEtapa, String codPrograma, String codCiclo, String codCurso,String nombreAlumno, String apePaternoAlumno, String apeMaternoAlumno);	
	public List getAllAlumnoCasoEspecialCat(String codPeriodo,String codProducto,String codCiclo,String codCurso);	
	
	//JHPR 2008-06-05
	public Alumno obtenerAlumno(String codAlumno);
	public int NroPasantiasAlumno(String codAlumno);
	public String guardarRespuestaDatosPersonales(String codAlumno, String respuesta);
	
	public boolean debeEncuestaPFR(Integer codalumno);
	
	public List<Curso> listaCursosExternos(String codPeriodo);
}
