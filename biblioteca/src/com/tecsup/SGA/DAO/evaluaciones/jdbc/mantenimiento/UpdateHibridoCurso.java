package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class UpdateHibridoCurso extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateHibridoCurso.class);
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	                                 	+ ".pkg_eval_mantto_config.sp_act_relac_prod_hibrido";

private static final String E_C_CODIGO = "E_C_CODIGO";
private static final String E_C_CADCODCURSOS = "E_C_CADEVALUACION";
private static final String E_C_NROREGISTROS = "E_V_NROREGISTROS";
private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
private static final String S_V_RETVAL = "S_V_RETVAL";

	public UpdateHibridoCurso(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODIGO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CADCODCURSOS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_NROREGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(String codigo, String cadCodCursos, String nroRegistros, String codUsuario) {
	
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("E_C_CODIGO: "+codigo);
		log.info("E_C_CADCODCURSOS: "+cadCodCursos);
		log.info("E_C_NROREGISTROS: "+nroRegistros);	
		log.info("E_V_CODUSUARIO: "+codUsuario);		
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();

		inputs.put(E_C_CODIGO, codigo);		
		inputs.put(E_C_CADCODCURSOS, cadCodCursos);
		inputs.put(E_C_NROREGISTROS, nroRegistros);
		inputs.put(E_V_CODUSUARIO, codUsuario);

		return super.execute(inputs);	
	}

}