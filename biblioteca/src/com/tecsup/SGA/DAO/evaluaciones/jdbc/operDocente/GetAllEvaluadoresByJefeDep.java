package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Evaluador;

/*
PROCEDURE SP_SEL_EVALUADORES(
E_C_COD_EVALUADOR IN CHAR,
E_C_COD_SEDE IN CHAR,
E_C_COD_PERIODO IN CHAR,
E_C_TIPO_USUARIO IN CHAR,--1 -> JefeDepartamenet 2 -> Director
S_C_RECORDSET IN OUT CUR_RECORDSET)
*/

public class GetAllEvaluadoresByJefeDep extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
				+ ".pkg_eval_oper_docente.SP_SEL_EVALUADORES";
	
	private static final String E_C_COD_EVALUADOR = "E_C_COD_EVALUADOR"; //CODIGO PERIODO
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE"; //CODIGO EVALUADOR
	private static final String E_C_COD_PERIODO = "E_C_COD_PERIODO"; //CODIGO PRODCUTO
	private static final String E_C_TIPO_USUARIO = "E_C_TIPO_USUARIO"; //CODIGO ESPECIALIDAD
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllEvaluadoresByJefeDep(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_EVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_PERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO_USUARIO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new EvaluadoresMapper()));
        compile();
    }

    public Map execute(String codEvaluador, String codSede, String codPeriodo, String tipoUsuario) {
    	
    	Map inputs = new HashMap();
    	System.out.println(codEvaluador +" / "+ codSede +" / "+ codPeriodo +" / "+ tipoUsuario);
    	inputs.put(E_C_COD_EVALUADOR, codEvaluador);
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_C_COD_PERIODO, codPeriodo);
    	inputs.put(E_C_TIPO_USUARIO, tipoUsuario);
        return super.execute(inputs);
    }
    
    final class EvaluadoresMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Evaluador evaluador = new Evaluador();
        	
        	evaluador.setCodEvaluador(rs.getString("CODEVAL") == null ? "" : rs.getString("CODEVAL"));
        	evaluador.setDscEvaluador(rs.getString("EVALUADOR") == null ? "" : rs.getString("EVALUADOR"));
        	
            return evaluador;
        }
    }
}