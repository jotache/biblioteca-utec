package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.*;

public class GetAllProductos extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllProductos.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	 	+ ".PKG_EVAL_COMUN.SP_SEL_PRODUCTOS";//pkg_eval_mantto_config.sp_sel_productos
	
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllProductos(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	compile();
	}
	
	public Map execute() {	
		log.info("*** EXEC " + SPROC_NAME + " ****");
		Map inputs = new HashMap();			
		return super.execute(inputs);
	}
	
   final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Producto producto = new Producto();
        	producto.setCodProducto(rs.getString("CODPRODUCTO"));
        	producto.setDescripcion(rs.getString("DESCRIPCION"));        
            return producto;
        }

    }
}
