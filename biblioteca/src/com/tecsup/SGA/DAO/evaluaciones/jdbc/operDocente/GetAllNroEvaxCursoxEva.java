package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCursosEvaluador.CursosMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.DefNroEvalParciales;

public class GetAllNroEvaxCursoxEva  extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.SP_SEL_NROEVA_X_CURSO_X_EVA";
	
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";	 
	private static final String E_C_CODEVALUADOR = "E_C_CODEVALUADOR";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODTIPOPRACTICA = "E_C_CODTIPOPRACTICA";
	private static final String E_C_CODSECCION = "E_C_CODSECCION";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";

    
    public GetAllNroEvaxCursoxEva(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));        
        declareParameter(new SqlParameter(E_C_CODEVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOPRACTICA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new DefNroEvalParcialesMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codCurso,
			String codEval, String codProducto, String codEspecialidad,
			String cboEvaluacionParcial, String cboSeccion) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODPERIODO,codPeriodo);
    	inputs.put(E_C_CODCURSO,codCurso);
    	inputs.put(E_C_CODEVALUADOR,codEval);
    	inputs.put(E_C_CODPRODUCTO,codProducto);
    	inputs.put(E_C_CODESPECIALIDAD,codEspecialidad);
    	inputs.put(E_C_CODTIPOPRACTICA,cboEvaluacionParcial);
    	inputs.put(E_C_CODSECCION,cboSeccion);
        return super.execute(inputs);
    }
    
    //DefNroEvalParciales
    final class DefNroEvalParcialesMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	DefNroEvalParciales obj = new DefNroEvalParciales();
        	
        	obj.setNrevId(rs.getString("CODEVALUACION"));
        	obj.setNreNroEval(rs.getString("DESCRIPCION"));
        	
            return obj;
        }
    }
}