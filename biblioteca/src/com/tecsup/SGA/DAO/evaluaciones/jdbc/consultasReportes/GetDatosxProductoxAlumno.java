package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.bean.DetalleAsistencia;
import com.tecsup.SGA.bean.InformacionGeneral;
import com.tecsup.SGA.bean.ProductoxAlumno;
import com.tecsup.SGA.common.CommonConstants;

public class GetDatosxProductoxAlumno extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetDatosxProductoxAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
			+ ".PKG_EVAL_CONSULTAS_REPORTES.sp_sel_datos_x_producto_alumno";
/*
 * E_C_CODPERIODO IN CHAR,
	E_C_CODPRODUCTO IN CHAR,
	E_C_CODALUMNO IN CHAR,
 * */
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";		
	private static final String RECORDSET = "S_C_RECORDSET";

	public GetDatosxProductoxAlumno(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR,
				new GetDatosxProductoxAlumnoMapper()));
		compile();
	}

	public Map execute(String codPeriodo,String codProducto, String codAlumno) {

		log.info("*** INI " + SPROC_NAME + "***");
		log.info("E_C_CODPERIODO: "+codPeriodo);
		log.info("E_C_CODPRODUCTO: "+codProducto);
		log.info("E_C_CODALUMNO: "+codAlumno);
		log.info("*** FIN " + SPROC_NAME + "***");
		
		Map inputs = new HashMap();
		inputs.put(E_C_CODPERIODO, codPeriodo);
		inputs.put(E_C_CODPRODUCTO, codProducto);
		inputs.put(E_C_CODALUMNO, codAlumno);	
		return super.execute(inputs);
	}

	final class GetDatosxProductoxAlumnoMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			InformacionGeneral informacionGeneral = new InformacionGeneral();
			informacionGeneral.setCodAlumno(rs.getString("CODALUMNO"));
			informacionGeneral.setNomAlumno(rs.getString("NOMBREALUMNO"));
			informacionGeneral.setCodEspecialidad(rs.getString("CODESPECIALIDAD"));
			informacionGeneral.setDesEspecialidad(rs.getString("DESCESPECIALIDAD"));
			informacionGeneral.setNroRankin(rs.getString("NRORANKINGA"));
			informacionGeneral.setDesRankin(rs.getString("DESCRANKING"));
			informacionGeneral.setPromAcumulado(rs.getString("PROMEDIOACUMULADO"));
			
			return informacionGeneral;
		}
	}
}