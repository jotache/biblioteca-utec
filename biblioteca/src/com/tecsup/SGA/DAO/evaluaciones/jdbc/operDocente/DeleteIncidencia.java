package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


//SP_DEL_INCIDENCIA
public class DeleteIncidencia extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
									+ ".PKG_EVAL_OPER_DOCENTE.SP_DEL_INCIDENCIA";
	private static final String CODIGO_ID = "E_C_CODIGO_ID"; 
	private static final String CODUSUARIO = "E_V_CODUSUARIO";
	private static final String RETVAL = "S_V_RETVAL";

    public DeleteIncidencia(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODIGO_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }
    
    public Map execute(String codIncidencia, String codUsuario) {    	
    	Map inputs = new HashMap();
    	inputs.put(CODIGO_ID, codIncidencia);
    	inputs.put(CODUSUARIO, codUsuario);
        return super.execute(inputs);
    }

}
