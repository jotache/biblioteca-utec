package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCursosEvaluador.CursosMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.DefNroEvalParciales;

public class GetAllNroEvaluacionesxParciales  extends StoredProcedure{
	
	/*PROCEDURE SP_SEL_NROEVAL_X_EXPARCIALES(
	E_C_CODPERIODO IN CHAR,
	E_C_CODSECCION IN CHAR,
	
	E_C_CODCURSO IN CHAR,
	E_C_CODEXAMENPARCIAL IN CHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
	 * 
	 * */
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.SP_SEL_NROEVAL_X_EXPARCIALES";
	
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODSECCION = "E_C_CODSECCION";
	private static final String E_C_CODCURSO = "E_C_CODCURSO"; 
	private static final String E_C_CODEXAMENPARCIAL = "E_C_CODEXAMENPARCIAL"; 	
    private static final String S_C_RECORDSET = "S_C_RECORDSET";

    
    public GetAllNroEvaluacionesxParciales(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));        
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODEXAMENPARCIAL, OracleTypes.VARCHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new DefNroEvalParcialesMapper()));
        compile();
    }

    public Map execute(String codPeriodo,String codSeccion,String codCurso,String codEvaluacionParcial) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODPERIODO,codPeriodo);
    	inputs.put(E_C_CODSECCION,codSeccion);
    	inputs.put(E_C_CODCURSO,codCurso);
    	inputs.put(E_C_CODEXAMENPARCIAL,codEvaluacionParcial);    	
        return super.execute(inputs);
    }  
    
    //DefNroEvalParciales
    final class DefNroEvalParcialesMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	DefNroEvalParciales obj = new DefNroEvalParciales();
        	
        	obj.setNrevId(rs.getString("CODDEFEVALPARCIAL"));
        	obj.setNreNroEval(rs.getString("NROEVAL"));
        	
            return obj;
        }
    }
}