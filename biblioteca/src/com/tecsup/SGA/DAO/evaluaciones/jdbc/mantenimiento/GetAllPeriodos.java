package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;


public class GetAllPeriodos extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllPeriodos.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
 	+ ".pkg_eval_comun.sp_sel_periodo";

	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllPeriodos(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	compile();
	}
	
	public Map execute() {
	
		Map inputs = new HashMap();
		log.info("*** EXEC " + SPROC_NAME + " ****");
		return super.execute(inputs);
	
	}
	final class ProcesoMapper implements RowMapper {
	    
	    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	    	
	    	Periodo periodo = new Periodo();
	    	periodo.setCodigo(rs.getString("CODIGO"));
	    	periodo.setNombre(rs.getString("NOMBRE"));
	        periodo.setTipo(rs.getString("SITUACIONREGISTRO"));
//	        System.out.println("T_T "+periodo.getTipo());
	        return periodo;
	    }
	
	}
}
