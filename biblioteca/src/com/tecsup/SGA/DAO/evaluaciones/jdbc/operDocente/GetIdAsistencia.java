package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;

/*
	PROCEDURE SP_GET_ID_ASISTENCIA(
		S_V_RETVAL IN OUT VARCHAR2;
	);
*/

public class GetIdAsistencia extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + 
											".pkg_eval_oper_docente.sp_get_id_asistencia";
	private static final String RETVAL = "S_V_RETVAL";
    
    public GetIdAsistencia(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute() {
    	Map inputs = new HashMap();
        return super.execute(inputs);
    }
    
}