package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.AdminCursosDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.GetAllCursosByFiltros;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.GetAllTypesOfExams;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.GetAllExamsByAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.InsertNotasExamenes;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.InsertNotasExamenesCargo;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.InsertNotasExamenesRecuperacion;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.InsertNotasExamenesSubsanacion;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.UpdateLevantarDI;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota.UpdateNotaExterno;
import com.tecsup.SGA.common.CommonConstants;

public class AdminCursosDAOJdbc extends JdbcDaoSupport implements AdminCursosDAO{
	
	public List getAllTypesOfExams(String codPeriodo, String codCurso, String tipoExamen){
		
		GetAllTypesOfExams getAllTypesOfExams = new GetAllTypesOfExams(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllTypesOfExams.execute(codPeriodo, codCurso, tipoExamen);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	//JHPR: 2/Abrl/08 Agregue parametro para filtrar solo curso Cargo en la Bandeja Principal del Administrador.
	public List getAllCursosByFiltros(String codPeriodo, String codProducto, String codEspecialidad, String codCiclo,
									String codEtapa, String codPrograma , String filtrarCargos) {
		GetAllCursosByFiltros getAllCursosByFiltros = new GetAllCursosByFiltros(this.getDataSource());
		
		if ("".equals(filtrarCargos.trim())) {
			filtrarCargos="X";
		}
		
		HashMap outputs = (HashMap)getAllCursosByFiltros.execute(codPeriodo, codProducto, codEspecialidad, codCiclo,
										codEtapa, codPrograma,filtrarCargos);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	//JHPR 31/3/2008 Para visualizar los alumnos con examenes de cargo. (Parametro tipoExamen agregado.)
	public List getAllExamsByAlumno(String codPeriodo, String codCurso, String codSeccion, String nombre, String apellido,String tipoExamen){
		System.out.println("DAO:"+tipoExamen);
		GetAllExamsByAlumno getAllExamsByAlumno = new GetAllExamsByAlumno(this.getDataSource(),tipoExamen);
		
		//Agregado Parametro tipoExamen.
		HashMap outputs = (HashMap)getAllExamsByAlumno.execute(codPeriodo, codCurso, codSeccion, nombre, apellido , tipoExamen);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String insertNotasExamenes(String codPeriodo, String codCurso, String codSeccion, String cadNotas, 
			String nroRegistros, String codUsuario){
		
		InsertNotasExamenes insertNotasExamenes = new InsertNotasExamenes(this.getDataSource());
		
		HashMap inputs = (HashMap)insertNotasExamenes.execute(codPeriodo, codCurso, codSeccion, cadNotas, nroRegistros, codUsuario);
	
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	
	//JHPR: 1/Abril/2008 Ingresar notas de examenes de cargo
	public String insertNotasExamenesCargo(String codPeriodo, String codCurso, String cadNotas, 
			String nroRegistros, String codUsuario,String tipoExamen) {
		
		InsertNotasExamenesCargo insertNotasExamenesCargo = new InsertNotasExamenesCargo(this.getDataSource());
		HashMap inputs = (HashMap) insertNotasExamenesCargo.execute(codPeriodo, codCurso, cadNotas, nroRegistros, codUsuario, tipoExamen);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}

	public String insertNotasExamenesSubsanacion(String codPeriodo,
			String codCurso, String cadNotas, String nroRegistros,
			String codUsuario) {
			InsertNotasExamenesSubsanacion insert = new InsertNotasExamenesSubsanacion(this.getDataSource());
			HashMap inputs = (HashMap) insert.execute(codPeriodo, codCurso, cadNotas, nroRegistros, codUsuario);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get(CommonConstants.RET_STRING);
			}
		return null;
	}

	public String insertNotasExamenesRecuperacion(String codPeriodo,
			String codCurso, String cadNotas, String nroRegistros,
			String codUsuario) {
		InsertNotasExamenesRecuperacion insert = new InsertNotasExamenesRecuperacion(this.getDataSource());
		HashMap inputs = (HashMap) insert.execute(codPeriodo, codCurso, cadNotas, nroRegistros, codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}

	public String updateLevantarDI(String codCurso, String codTipoSesion,
			String codSeccion, String codAlumno, String tipoIncidencia, String tipoOpe, String usuario) {
		UpdateLevantarDI upDi = new UpdateLevantarDI(this.getDataSource());
		try{
			HashMap inputs =(HashMap) upDi.execute(codCurso, codTipoSesion, codSeccion, codAlumno, tipoIncidencia, tipoOpe, usuario);
			if (!inputs.isEmpty()){
				return (String)inputs.get(CommonConstants.RET_STRING);
			}			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
