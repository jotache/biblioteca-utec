package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.evaluaciones.ProductosDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllProductos;

public class ProductosDAOJdbc extends JdbcDaoSupport implements ProductosDAO{
	
	public List getAllProductos(){	
		GetAllProductos getAllProductos = new GetAllProductos(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProductos.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}
