package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluacionesParcialesxCurso.EvaluacionesParcialesMapper;
import com.tecsup.SGA.bean.DetalleEvalParcial;
import com.tecsup.SGA.bean.EvaluacionesParciales;
import com.tecsup.SGA.common.CommonConstants;

public class GetAllDetalleEvalParcial extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".PKG_EVAL_OPER_DOCENTE.SP_SEL_DET_EVAL_PARCIAL";
	private static Log log = LogFactory.getLog(GetAllDetalleEvalParcial.class);
	private static final String E_V_CODPERIODO = "E_V_CODPERIODO";
	private static final String E_V_CODCURSO = "E_V_CODCURSO";
	private static final String E_V_CODSECCION = "E_V_CODSECCION";
	private static final String E_V_NOMBRE = "E_V_NOMBRE";
	private static final String E_V_APELLIDO = "E_V_APELLIDO";
	private static final String E_C_CODNROEVAL = "E_C_CODNROEVAL";
	private static final String E_C_TIPOEVAL = "E_C_TIPOEVAL";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetalleEvalParcial(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_CODPERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODCURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODSECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_APELLIDO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODNROEVAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPOEVAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));        
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new DetalleEvalParcialMapper()));
        compile();
    }

    public Map execute(String codPeriodo,String codCurso,String codSeccion,
    		String  nombre,String apellido,String codNroEval,
    		String codTipoEval,String codProducto,
			String codEspecialidad){
    	Map inputs = new HashMap();
    	    	    
    	log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("E_V_CODPERIODO: "+codPeriodo);
    	log.info("E_V_CODCURSO: "+codCurso);
    	log.info("E_V_CODSECCION: "+codSeccion);
    	log.info("E_V_NOMBRE: "+nombre);
    	log.info("E_V_APELLIDO: "+apellido);
    	log.info("E_C_CODNROEVAL: "+codNroEval);
    	log.info("E_C_TIPOEVAL: "+codTipoEval);
    	log.info("E_C_CODPRODUCTO: "+codProducto);
    	log.info("E_C_CODESPECIALIDAD: "+codEspecialidad);
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
    	inputs.put(E_V_CODPERIODO, codPeriodo);
    	inputs.put(E_V_CODCURSO, codCurso);
    	inputs.put(E_V_CODSECCION, codSeccion);
    	inputs.put(E_V_NOMBRE, nombre);
    	inputs.put(E_V_APELLIDO, apellido);    	 
    	inputs.put(E_C_CODNROEVAL, codNroEval);
    	inputs.put(E_C_TIPOEVAL, codTipoEval);
    	inputs.put(E_C_CODPRODUCTO, codProducto);
    	inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
    	    	    	
        return super.execute(inputs);
    }
    
    final class DetalleEvalParcialMapper implements RowMapper{
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	DetalleEvalParcial detalleEvalParcial = new DetalleEvalParcial();     	

        	detalleEvalParcial.setCodAlumno(rs.getString("CODALUMNO"));
        	detalleEvalParcial.setNroEvaluacion(rs.getString("NROEVAL"));
        	detalleEvalParcial.setNota(rs.getString("NOTA"));
        	//System.out.println("DetalleEvalParcialMapper:NroEval::"+detalleEvalParcial.getNroEvaluacion());
        	detalleEvalParcial.setNombre(rs.getString("NOMBRE"));
        	detalleEvalParcial.setProm(rs.getString("PROM"));
        	detalleEvalParcial.setPeso(rs.getString("PESO"));
        	detalleEvalParcial.setNroNotas(rs.getString("NRO_NOTAS"));

            return detalleEvalParcial;
        }
    }
}
