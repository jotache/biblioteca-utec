package com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Examen;


public class GetAllExamsByAlumno extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllExamsByAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
											+ ".pkg_eval_oper_admin.sp_sel_exams_curso_secc";
	
	//JHPR 31/3/2008 Para visualizar los alumnos con examenes de cargo.
	private static final String SPROC_NAME_CARGO = CommonConstants.ESQ_EVALUACION 
						+ ".pkg_eval_oper_admin.SP_SEL_EXAMS_CARGO_CURSO_SECC";
	
	private static final String SPROC_NAME_SUBSANACION = CommonConstants.ESQ_EVALUACION
					+ ".PKG_EVAL_OPER_ADMIN.sp_sel_exam_subsanacion_curso";
	
	private static final String SPROC_NAME_RECUPERACION = CommonConstants.ESQ_EVALUACION
					+ ".PKG_EVAL_OPER_ADMIN.SP_SEL_EXAM_RECUPERACION_CURSO";
	
	private static final String COD_PERIODO = "E_V_CODPERIODO";
	private static final String COD_CURSO = "E_C_CODCURSO";
	private static final String COD_SECCION = "E_V_CODSECCION"; 
	private static final String NOMBRE = "E_V_NOMBRE"; 
	private static final String APELLIDO = "E_V_APELLIDO"; 
	private static final String TIPO_EXAM = "E_C_TIPO_EXAM";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllExamsByAlumno(DataSource dataSource,String tipoExamen) { //,    	    		
    	
    	super(dataSource,(tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_PARCIAL) ? SPROC_NAME : tipoExamen.equals(CommonConstants.TIPO_EXAMEN_CARGO)? SPROC_NAME_CARGO: (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_EXTRA)? SPROC_NAME_CARGO: (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_FINAL)?SPROC_NAME : (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_SUBSANACION)?SPROC_NAME_SUBSANACION:  (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_RECUPERACION)?SPROC_NAME_RECUPERACION:"" )  )))));
    	
    	if (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_PARCIAL) || tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_FINAL)){
            declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.CHAR));
            declareParameter(new SqlParameter(COD_CURSO, OracleTypes.CHAR));        
            declareParameter(new SqlParameter(COD_SECCION, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(APELLIDO, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(TIPO_EXAM, OracleTypes.CHAR));
    	} else if (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_CARGO) || tipoExamen.equals(CommonConstants.TIPO_EXAMEN_EXTRA)) {
            declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.CHAR));
            declareParameter(new SqlParameter(COD_CURSO, OracleTypes.CHAR));                  
            declareParameter(new SqlParameter(TIPO_EXAM, OracleTypes.CHAR));          
            declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(APELLIDO, OracleTypes.VARCHAR));    		
    	} else if (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_SUBSANACION) || tipoExamen.equals(CommonConstants.TIPO_EXAMEN_RECUPERACION) ){
            declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.CHAR));
            declareParameter(new SqlParameter(COD_CURSO, OracleTypes.CHAR));        
            declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(APELLIDO, OracleTypes.VARCHAR));                	    	                
    	}
    	    	
    	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ExamsMapper()));    	           
        compile();
    }
    
  //(Parametro tipoExamen agregado.)
    public Map execute(String codPeriodo, String codCurso, String codSeccion, String nombre
    		, String apellido,String tipoExamen) {
    	
    	Map inputs = new HashMap();
    	log.info("******* INI PKG_EVAL_OPER_ADMIN." + (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_PARCIAL) ? SPROC_NAME : tipoExamen.equals(CommonConstants.TIPO_EXAMEN_CARGO)? SPROC_NAME_CARGO: (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_EXTRA)? SPROC_NAME_CARGO: (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_FINAL)?SPROC_NAME : (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_SUBSANACION)?SPROC_NAME_SUBSANACION:  (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_RECUPERACION)?SPROC_NAME_RECUPERACION:"" )  )))) +  " TIPO("+ tipoExamen +") ******");
    	log.info("codPeriodo:"+codPeriodo);
    	log.info("codCurso:"+codCurso);
    	log.info("codSeccion:"+codSeccion);
    	log.info("nombre:"+nombre);
    	log.info("apellido:"+apellido);
    	log.info("tipoExamen:"+tipoExamen);
    	log.info("******* FIN PKG_EVAL_OPER_ADMIN.sp_sel_exams_curso_secc ******");
    	
    	if (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_PARCIAL) || tipoExamen.equals(CommonConstants.TIPO_EXAMEN_REGULAR_FINAL)){
        	inputs.put(COD_PERIODO, codPeriodo);
        	inputs.put(COD_CURSO, codCurso);        	
        	inputs.put(COD_SECCION, codSeccion);        	
        	inputs.put(NOMBRE, nombre);
        	inputs.put(APELLIDO, apellido);
        	inputs.put(TIPO_EXAM, tipoExamen);    	
    	} else if (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_CARGO) || tipoExamen.equals(CommonConstants.TIPO_EXAMEN_EXTRA) ){
        	inputs.put(COD_PERIODO, codPeriodo);
        	inputs.put(COD_CURSO, codCurso);
        	inputs.put(TIPO_EXAM, tipoExamen);
        	inputs.put(NOMBRE, nombre);
        	inputs.put(APELLIDO, apellido);
    	} else if (tipoExamen.equals(CommonConstants.TIPO_EXAMEN_SUBSANACION) || tipoExamen.equals(CommonConstants.TIPO_EXAMEN_RECUPERACION)){
        	inputs.put(COD_PERIODO, codPeriodo);
        	inputs.put(COD_CURSO, codCurso);
        	inputs.put(NOMBRE, nombre);
        	inputs.put(APELLIDO, apellido);    		
    	} 
    	
        return super.execute(inputs);
    }
    
    final class ExamsMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Examen examen = new Examen(); 
        	
        	examen.setCodEvaluacion(rs.getString("CODIGOEXAMEN") == null ? "" : rs.getString("CODIGOEXAMEN") );
        	examen.setCodAlumno(rs.getString("CODALUMNO") == null ? "" : rs.getString("CODALUMNO") );
        	examen.setCodCarnet(rs.getString("CARNET") == null ? "" : rs.getString("CARNET"));
        	examen.setNombre(rs.getString("NOMBRE") == null ? "" : rs.getString("NOMBRE") );
        	examen.setCodigo(rs.getString("CODTIPOEXAM") == null ? "" : rs.getString("CODTIPOEXAM"));
        	examen.setDescripcion(rs.getString("TIPOEXAM") == null ? "" : rs.getString("TIPOEXAM"));
        	examen.setNota(rs.getString("NOTA") == null ? "" : rs.getString("NOTA"));
        	examen.setFlagNP(rs.getString("FLAGNP") == null ? "" : rs.getString("FLAGNP"));
        	examen.setFlagAN(rs.getString("FLAGAN") == null ? "" : rs.getString("FLAGAN"));
        	examen.setFlagSU(rs.getString("FLAGSU") == null ? "" : rs.getString("FLAGSU")); //jhpr
        	examen.setFlagIndExamRecu(rs.getString("IND_EXAM_RECU") == null ? "" : rs.getString("IND_EXAM_RECU"));
        	examen.setFlagIndExamSub(rs.getString("IND_EXAM_SUB") == null ? "" : rs.getString("IND_EXAM_SUB"));
        	examen.setFlagIndExamPar(rs.getString("IND_EXAM") == null ? "" : rs.getString("IND_EXAM"));
        	//
        	examen.setCodCurso(rs.getString("CODCURSO") == null ? "" : rs.getString("CODCURSO"));
        	examen.setCodSeccion(rs.getString("CODSECCION") == null ? "" : rs.getString("CODSECCION"));
        	examen.setDesSeccion(rs.getString("SECCION") == null ? "" : rs.getString("SECCION"));
        	examen.setNomCurso(rs.getString("DESCRIPCION") == null ? "" : rs.getString("DESCRIPCION"));
        	examen.setExamenNp(rs.getString("CODEXAMNP") == null ? "" : rs.getString("CODEXAMNP"));
        	examen.setTipoExamenNp(rs.getString("TIPOEXAMNP") == null ? "" : rs.getString("TIPOEXAMNP"));
        	      
        	examen.setTieneNotaSubsa(rs.getString("TIENE_SUBSA") == null ? "" : rs.getString("TIENE_SUBSA"));
        	//CODSECCION(si esta en regulares)	SECCION(YA esta)
        	//CODCURSO(si esta en regulares)	DESCRIPCION(ya esta)
        	//CODEXAMNP(no esta) TIPOEXAMNP (no esta)(a que examen reemplaza)
        	
            return examen;
        }
    }

}
