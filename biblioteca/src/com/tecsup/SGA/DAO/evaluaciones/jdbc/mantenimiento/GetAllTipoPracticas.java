package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Practica;

public class GetAllTipoPracticas  extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllTipoPracticas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_mantto_config.sp_sel_tipo_practicas";	  
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllTipoPracticas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new TipoPracticasMapper()));
        compile();
    }

    public Map execute() {  
    	log.info("*** EXEC " + SPROC_NAME + " ****");
        return super.execute(new HashMap());
    }
    
    final class TipoPracticasMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Practica practica = new Practica();        	
        	
        	practica.setCodigo(rs.getString("CODIGO"));
        	practica.setDescripcion(rs.getString("DESCRIPCION"));
        	
        	return practica;
        }
    }
}
