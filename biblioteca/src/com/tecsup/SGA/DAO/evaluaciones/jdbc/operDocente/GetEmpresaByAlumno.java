package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormacionEmpresa;

public class GetEmpresaByAlumno extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_admin.sp_sel_form_empresa_x_alumno";
		
		private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; 
		private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD"; 
		private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
		private static final String E_C_CODTIPOPRACTICA = "E_C_CODTIPOPRACTICA"; 
		private static final String S_C_RECORDSET = "S_C_RECORDSET";
	    
	    public GetEmpresaByAlumno(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_CODTIPOPRACTICA, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
	        compile();
	    }

	    public Map execute(String codPeriodo, String codEspecialidad
	    					,String codAlumno, String codTipoPractica) {
	    	
	    	Map inputs = new HashMap();
	    	inputs.put(E_C_CODPERIODO, codPeriodo);
	    	inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
	    	inputs.put(E_C_CODALUMNO, codAlumno);
	    	inputs.put(E_C_CODTIPOPRACTICA, codTipoPractica);
	        return super.execute(inputs);
	    }
	    
	    final class CursosMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	FormacionEmpresa curso = new FormacionEmpresa();
	        	
	        	curso.setCodAlumno(rs.getString("CODIGO_ID"));
	        	curso.setNombreAlumno(rs.getString("NOMBREEMPRESA"));
	        	curso.setAPaterno(rs.getString("APATERNO"));
	        	curso.setAMaterno(rs.getString("AMATERNO"));
	        	curso.setNombre(rs.getString("NOMBRE"));
	        	curso.setFlagPracticaInicial(rs.getString("FLAGPRACTICAINICIAL"));
	        	curso.setFlagPasantia(rs.getString("FLAGPASANTIA"));
	        	curso.setFlagPracticaPreProf(rs.getString("FLAGPRACTICAPREPROF"));
	            return curso;
	        }
	    }
		
}
