package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.bean.DetalleExamenes;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;

public class GetAllPeriodoxProducto extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAllPeriodoxProducto.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
	+ ".PKG_EVAL_CONSULTAS_REPORTES.SP_SEL_PERIODO_X_PROD";
	
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	
	private static final String RECORDSET = "S_C_RECORDSET";

	public GetAllPeriodoxProducto(DataSource dataSource) {
		
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR,
				new PeriodoMapper()));
		compile();
	}

	public Map execute(String codAlumno,String codPeriodo){

		log.info("*** INI " + SPROC_NAME + "***");
		log.info("E_C_CODALUMNO: " + codAlumno);  
    	log.info("E_C_CODPRODUCTO: " + codPeriodo);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
		Map inputs = new HashMap();		
		inputs.put(E_C_CODALUMNO, codAlumno);
		inputs.put(E_C_CODPRODUCTO, codPeriodo);

		return super.execute(inputs);
	}

	final class PeriodoMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			Periodo periodo = new Periodo();

			periodo.setCodigo(rs.getString("CODPERIODO"));
			periodo.setNombre(rs.getString("DESCPERIODO"));

			return periodo;
		}
	}
}