package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.OperDocenteDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.AbrirRegistroAsistencia;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.AbrirRegistroNotas;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.AdjuntarArchivo;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.CerrarRegistrosAsistencia;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.CerrarRegistrosNotas;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.ConsultaCierreNotas;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.DeleteEvaluaciones;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.EmpresaXAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GelAllFormacionEmpresa;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllAdjuntoEmpresa;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllAlumnosCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCursosEvaluador;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllDetalleEvalParcial;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluaciones;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluadores;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluadoresByJefeDep;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllNroEvaxCursoxEva;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllNumeroMinimo;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllPeriodo;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllSeccionCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllSeccionesxCursoxEval;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllSeccionesxCursoxResp;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllSesionXCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllTipoEvaluaciones;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllTipoPracticaxCursoxEval;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetCursoInTime;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetEvaluadorSeccion;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.HorasPorAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.InsertEmpresa;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.InsertPeso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.ModificarFormacionEmpresa;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.ProductoxCurso;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.DefNroEvalParciales;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.HorasXalumnos;
import com.tecsup.SGA.modelo.NumeroMinimo;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.modelo.Producto;

public class OperDocenteDAOJdbc extends JdbcDaoSupport implements OperDocenteDAO{

	private static Log log = LogFactory.getLog(OperDocenteDAOJdbc.class);
	
	@SuppressWarnings("unchecked")
	public List<CursoEvaluador> getCursoInTime(String evaluador){
		List<CursoEvaluador> lista = new ArrayList<CursoEvaluador>();
		GetCursoInTime getCursoInTime = new GetCursoInTime(this.getDataSource());
		HashMap outputs = (HashMap)getCursoInTime.execute(evaluador);
		if (!outputs.isEmpty())
		{
			List recordset = (List)outputs.get("S_C_RECORDSET");
			if(!recordset.isEmpty()){
				String hora = null;
				for (Iterator i = recordset.iterator(); i.hasNext();) {
					CursoEvaluador curso = (CursoEvaluador) i.next();
					curso.setCodEvaluador(evaluador);
					if(hora==null || !hora.equals(curso.getHoraInicio())){
						lista.add(curso);
						hora = curso.getHoraInicio();
						log.info("AGREGADO: "+curso);
					}else{
						log.info("IGNORADO: "+curso);
					}				
				}
			}else{
				log.info("getCursoInTime(): No se encontr� registro para "+ evaluador);
			}
		}
		return lista;
	}
	
	public List getAllCursos(String periodo, String evaluador, String producto, String especialidad, String curso,
			String codEvalSeleccionado, String dscCurso)
	{
		GetAllCursosEvaluador getAllCursos = new GetAllCursosEvaluador(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCursos.execute(periodo, evaluador, producto, especialidad, curso
						, codEvalSeleccionado, dscCurso);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllSeccionCurso(String tipoSesion, String codCurso){
		GetAllSeccionCurso getAllSeccionCurso = new GetAllSeccionCurso(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSeccionCurso.execute(tipoSesion, codCurso);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllAlumnosCurso(String codPeriodo, String codCurso, 
			String codProducto, String codEspecialidad, String tipoSesion,
			String codSeccion, String nombre, String apellido)
	{	
		GetAllAlumnosCurso getAllAlumnos = new GetAllAlumnosCurso(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllAlumnos.execute(codPeriodo, codCurso, codProducto, 
									codEspecialidad, tipoSesion, codSeccion,nombre, apellido);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	
	public List GetAllTipoEvaluaciones()
	{
		GetAllTipoEvaluaciones GetAllTipoEvaluaciones = new GetAllTipoEvaluaciones(this.getDataSource());
	
		HashMap outputs = (HashMap)GetAllTipoEvaluaciones.execute();
		if(!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllEvaluaciones(String codPeriodo,String codPro, String codEspe, String codCurso
    		, String seccion, String codEvaluador, String codTipoEva)
	{
		GetAllEvaluaciones GetAllEvaluaciones = new GetAllEvaluaciones(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllEvaluaciones.execute(codPeriodo,codPro,codEspe, codCurso, seccion, codEvaluador, codTipoEva);
		if(!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;

	}
	
	public String InsertPeso(String codPeriodo, String codId, String codCurso,String codPro, String codEspe, String codSeccion, String codTipoEva, String peso
    		,String codEva, String flag, String usuCrea)
	{
		InsertPeso InsertPeso = new InsertPeso(this.getDataSource());
		HashMap inputs = (HashMap)InsertPeso.execute(codPeriodo, codId, codCurso,codPro,codEspe, codSeccion, codTipoEva, peso, codEva, flag, usuCrea);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	//JHPR 2008-06-30 Filtrando a tambi�n por per�odo a los evaluadores de un curso.
	public List GetAllEvaluadores(String codCurso,String codPeriodo)
	{
		GetAllEvaluadores GetAllEvaluadores = new GetAllEvaluadores(this.getDataSource());
		HashMap outputs = (HashMap)GetAllEvaluadores.execute(codCurso,codPeriodo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String DeleteEvaluaciones(String codPeriodo,String codPro, String codEspe, String codCurso,String CodSeccion, String codTipoEva
    		, String cadCod, String nroReg, String  usuCrea)
	{
		DeleteEvaluaciones DeleteEvaluaciones = new DeleteEvaluaciones(this.getDataSource());
	HashMap inputs = (HashMap)DeleteEvaluaciones.execute(codPeriodo,codPro,codEspe, codCurso,CodSeccion, codTipoEva, cadCod, nroReg, usuCrea);;
	if(!inputs.isEmpty())
	{
		return (String)inputs.get("S_V_RETVAL");
	}
		
	return null;
	}
	

	public NumeroMinimo GetAllNumeroMinimo(String codProducto,String codPeriodo){
		
		GetAllNumeroMinimo GetAllNumeroMinimo = new GetAllNumeroMinimo(this.getDataSource());
		HashMap outputs = (HashMap)GetAllNumeroMinimo.execute(codProducto, codPeriodo);
		if(!outputs.isEmpty())
		{
			ArrayList lista = (ArrayList)outputs.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (NumeroMinimo)lista.get(0);
			}
			else return null;
			}
		return null;
	}
	
	public List GelAllFormacionEmpresa(String codPeriodo, String codProducto
			,String codEspecialidad, String nombre, String apPaterno)
	{
		GelAllFormacionEmpresa GelAllFormacionEmpresa = new GelAllFormacionEmpresa(this.getDataSource());
		HashMap outputs = (HashMap)GelAllFormacionEmpresa.execute(codPeriodo, codProducto, codEspecialidad, nombre, apPaterno);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	

	public String InsertEmpresa(String codPeriodo, String codProducto, String codEspecilidad, String codCurso,
			String codAlumno, String tipoPractica, String nombreEmpresa,String fechaInicio,
			String fechaFin, String CanridadHoras,String codEstado,String observacion,
			String nota,String horaIni,String horaFin,String indSabado,String indDomingo,String usuCrea)
	{
		InsertEmpresa InsertEmpresa = new InsertEmpresa(this.getDataSource());
		HashMap inputs = (HashMap)InsertEmpresa.execute(codPeriodo, codProducto
				, codEspecilidad, codCurso, codAlumno, tipoPractica, nombreEmpresa
				, fechaInicio, fechaFin, CanridadHoras, codEstado, observacion, nota
				, horaIni, horaFin, indSabado, indDomingo, usuCrea);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertEmpresaId(String codPeriodo, String codProducto, String codEspecilidad, String codCurso,
			String codAlumno, String tipoPractica, String nombreEmpresa,String fechaInicio,
			String fechaFin, String CanridadHoras,String codEstado,String observacion,
			String nota,String horaIni,String horaFin,String indSabado,String indDomingo,String usuCrea)
	{
		InsertEmpresa InsertEmpresa = new InsertEmpresa(this.getDataSource());
		HashMap inputs = (HashMap)InsertEmpresa.execute(codPeriodo, codProducto
				, codEspecilidad, codCurso, codAlumno, tipoPractica, nombreEmpresa
				, fechaInicio, fechaFin, CanridadHoras, codEstado, observacion, nota
				, horaIni, horaFin, indSabado, indDomingo, usuCrea);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_OUT_ID");
		}
		return null;
	}	
	
	public List GetAllDetalleEvalParcial(String codPeriodo,String codCurso, String codSeccion,
			String nombre, String apellido, String codNroEval,
			String codTipoEval,String codProducto,
			String codEspecialidad){
		
		GetAllDetalleEvalParcial getAllDetalleEvalParcial = new GetAllDetalleEvalParcial(this.getDataSource());
				
		HashMap outputs = (HashMap)getAllDetalleEvalParcial.execute(codPeriodo, codCurso,  codSeccion,
				 nombre,  apellido,  codNroEval,
				 codTipoEval, codProducto, codEspecialidad);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String AdjuntarArchivo(String codProducto, String codPeriodo, String codAlumno, String codCurso,
			String nomNuevoArchivo, String nomArchivo, String usucrea)
	{
		AdjuntarArchivo AdjuntarArchivo = new AdjuntarArchivo(this.getDataSource());
		HashMap inputs = (HashMap)AdjuntarArchivo.execute(codProducto
				, codPeriodo, codAlumno, codCurso, nomNuevoArchivo, nomArchivo, usucrea);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public HorasXalumnos HorasPorAlumno(String codPeriodo,String codEspecialidad
    		, String codAlumno, String codTipoPractica){
		HorasPorAlumno HorasPorAlumno = new HorasPorAlumno(this.getDataSource());
		HashMap outputs = (HashMap)HorasPorAlumno.execute(codPeriodo
				, codEspecialidad, codAlumno, codTipoPractica);
		if(!outputs.isEmpty())
		{
			ArrayList lista = (ArrayList)outputs.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (HorasXalumnos)lista.get(0);
			}
			else return null;
		}
		return null;
	}
	
	public List EmpresaXAlumno(String codPeriodo,String codEspecialidad
    		, String codAlumno, String codTipoPractica)
	{
		EmpresaXAlumno EmpresaXAlumno = new EmpresaXAlumno(this.getDataSource());
		HashMap outputs = (HashMap)EmpresaXAlumno.execute(codPeriodo, codEspecialidad
											, codAlumno, codTipoPractica);
		if(!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	
	public Producto ProductoxCurso(String codCurso){
		ProductoxCurso ProductoxCurso = new ProductoxCurso(this.getDataSource());
		HashMap outputs = (HashMap)ProductoxCurso.execute(codCurso);
		if(!outputs.isEmpty())
		{
			ArrayList lista = (ArrayList)outputs.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (Producto)lista.get(0);
			}
			else return null;
		}
		return null;
	}
	
	public List GetAllPeriodo()
	{
		GetAllPeriodo GetAllPeriodo = new GetAllPeriodo(this.getDataSource());
		HashMap outputs = (HashMap)GetAllPeriodo.execute();
		if(!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String ModificarFormacionEmpresa(String codId, String codPeriodo, String codProducto, String codEspecialidad
    		,String codCurso, String codAlumno, String tipoPractica, String nombreEmpresa, String fechaInicio
    		,String fechaFin, String cantHoras, String codEstado,String observacion,String nota,String horaIni,
    		String horaFin,String indSabado,String indDomingo, String usuCrea)
	{
		ModificarFormacionEmpresa ModificarFormacionEmpresa = new ModificarFormacionEmpresa(this.getDataSource());
		HashMap inputs = (HashMap)ModificarFormacionEmpresa.execute(codId, codPeriodo
				, codProducto, codEspecialidad, codCurso, codAlumno, tipoPractica
				, nombreEmpresa, fechaInicio, fechaFin, cantHoras, codEstado, observacion
				, nota, horaIni, horaFin, indSabado, indDomingo, usuCrea);
		
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;
	}
	                        
	
	public List GetAllAdjuntoEmpresa(String codPeriodo, String codProducto
			,String codAlumno, String codCurso){
		GetAllAdjuntoEmpresa GetAllAdjuntoEmpresa = new GetAllAdjuntoEmpresa(this.getDataSource());
		HashMap outputs = (HashMap)GetAllAdjuntoEmpresa.execute(codPeriodo
				, codProducto, codAlumno, codCurso);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}

	public List getAllTipoPracticaxCursoxEval(String codPeriodo,
			String codCurso, String codEval, String codProducto,
			String codEspecialidad) {
		GetAllTipoPracticaxCursoxEval getAllTipoPracticaxCursoxEval = new GetAllTipoPracticaxCursoxEval(this.getDataSource());
		HashMap outputs = (HashMap)getAllTipoPracticaxCursoxEval.execute( codPeriodo,
				 codCurso,  codEval,  codProducto,
				 codEspecialidad);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}

	public List getAllSeccionesxCursoxEval(String codPeriodo, String codCurso,
			String codEval,String codProducto,String codEspecialidad, String cboEvaluacionParcial){
		
		GetAllSeccionesxCursoxEval getAllSeccionesxCursoxEval = new GetAllSeccionesxCursoxEval(this.getDataSource());
		HashMap outputs = (HashMap)getAllSeccionesxCursoxEval.execute(  codPeriodo,  codCurso,
				 codEval, codProducto, codEspecialidad,  cboEvaluacionParcial);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}

	public List getAllNroEvaxCursoxEva(String codPeriodo, String codCurso,
			String codEval, String codProducto, String codEspecialidad,
			String cboEvaluacionParcial, String cboSeccion) {
		
		GetAllNroEvaxCursoxEva getAllNroEvaxCursoxEva = new GetAllNroEvaxCursoxEva(this.getDataSource());
		HashMap outputs = (HashMap)getAllNroEvaxCursoxEva.execute( codPeriodo,  codCurso,
				 codEval,  codProducto,  codEspecialidad,
				 cboEvaluacionParcial,  cboSeccion);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}
	
	public List getAllSesionXCurso(String codCurso){
		GetAllSesionXCurso getAllSesionXCurso = new GetAllSesionXCurso(this.getDataSource());
		HashMap outputs = (HashMap)getAllSesionXCurso.execute(codCurso);
		if(!outputs.isEmpty()){
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}

	// CACT : 15-04-2008 Listado de Evaluadores para los jefes de Departamento
	public List<Evaluador> getEvaluadoresByJefeDep(String codEvaluador,
			String codSede, String codPeriodo, String tipoUsuario) {

		GetAllEvaluadoresByJefeDep getAllEvaluadoresByJefeDep = new GetAllEvaluadoresByJefeDep(this.getDataSource());
		HashMap outputs = (HashMap)getAllEvaluadoresByJefeDep.execute(codEvaluador, codSede, codPeriodo, tipoUsuario);

		if(!outputs.isEmpty()){
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	// CACT : 17-04-2008 Lista de Secciones por responsable de ltomar asistencia
	public List<CursoEvaluador> GetAllSeccionesxCursoxResp(String codResponsable, String codCurso, String codTipoSesion) {

		GetAllSeccionesxCursoxResp getAllSeccionesxCursoxResp = new GetAllSeccionesxCursoxResp(this.getDataSource());
		HashMap outputs = (HashMap)getAllSeccionesxCursoxResp.execute(codResponsable, codCurso, codTipoSesion);

		if(!outputs.isEmpty()){
			return (List<CursoEvaluador>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	//JHPR: 2008-06-04 Cerrar registro de notas.
	public String cerrarRegistroNotas(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario, String flagCerrado) {

		CerrarRegistrosNotas cierreNotas = new CerrarRegistrosNotas(this.getDataSource());
		HashMap output = (HashMap) cierreNotas.execute(codCurso, codSeccion, tipoEvaluacion, codUsuario, flagCerrado);
		if(!output.isEmpty())
		{					
			return (String) output.get("S_V_RETVAL");
		}
		
		return null;
	}

	//JHPR: 2008-06-04 
	public Parciales obtenerDefinicionParcial(String codigo) {
		
		Connection conexion = null;
		Parciales parcial = null;
		String sql="";
		
		try {
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			sql = "select NREV_ID,CODCURSOEJEC,CODSECCION,CODEVALUADOR,NREV_NROEVAL,NREV_PESO,FLGCERRARNOTA,NREV_FLGREALIZADO from EVA_DEF_NRO_EVAL_PARCIALES where NREV_ID=" + codigo;
			System.out.println("obtenerDefinicionParcial sql:"+sql);
			stmt.executeQuery(sql);
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				parcial = new Parciales();
				parcial.setCodId(String.valueOf(rs.getLong("NREV_ID")));
				parcial.setCodCurso(String.valueOf(rs.getLong("CODCURSOEJEC")));
				parcial.setSeccion(String.valueOf(rs.getLong("CODSECCION")));
				parcial.setCodEvaluador(String.valueOf(rs.getLong("CODEVALUADOR")));
				parcial.setNroEval(String.valueOf(rs.getLong("NREV_NROEVAL")));
				parcial.setPeso(String.valueOf(rs.getLong("NREV_PESO")));
				parcial.setFlagCerrarNota(rs.getString("FLGCERRARNOTA"));
				parcial.setFlagRealizado(rs.getString("NREV_FLGREALIZADO")==null?"0":rs.getString("NREV_FLGREALIZADO"));				
			}
			
			rs.close();
			stmt.close();
			conexion.close();			
		} catch (SQLException ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());			
		}
		
		return parcial;
	}

	//JHPR: 2008-06-05
	public List<Parciales> obtenerDefinicionesParciales(String codCurso,
			String codSeccion, String TipoEvaluacion,String NroEvaluacion, String Estado) {
		Connection conexion = null;
		Parciales parcial = null;		
		List<Parciales> lista = null;
		String sql="";
		try {
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			sql = "select NREV_ID,CODCURSOEJEC,CODSECCION,CODEVALUADOR,NREV_NROEVAL,NREV_PESO,FLGCERRARNOTA,FLGCERRARASIST " +
				  "from EVA_DEF_NRO_EVAL_PARCIALES where CODCURSOEJEC = " + codCurso + " " +
				  		"and CODSECCION = " + codSeccion + " and NREV_CODTIPOEVALUACION = " + TipoEvaluacion + "" +
				  				" and NREV_EST_REG = '" + Estado + "'";
			
			if (!NroEvaluacion.equals("")) {
				sql = "select NREV_ID,CODCURSOEJEC,CODSECCION,CODEVALUADOR,NREV_NROEVAL,NREV_PESO,FLGCERRARNOTA,FLGCERRARASIST " +
				  "from EVA_DEF_NRO_EVAL_PARCIALES where NREV_ID = " + NroEvaluacion + " " +
				  " and NREV_EST_REG = '" + Estado + "'";
			}
			
			System.out.println("Sql:" + sql);
			
			stmt.executeQuery(sql);
			ResultSet rs = stmt.executeQuery(sql);
			lista = new ArrayList<Parciales>(); 
			while (rs.next()) {
				parcial = new Parciales();
				parcial.setCodId(String.valueOf(rs.getLong("NREV_ID")));
				parcial.setCodCurso(String.valueOf(rs.getLong("CODCURSOEJEC")));
				parcial.setSeccion(String.valueOf(rs.getLong("CODSECCION")));
				parcial.setCodEvaluador(String.valueOf(rs.getLong("CODEVALUADOR")));
				parcial.setNroEval(String.valueOf(rs.getLong("NREV_NROEVAL")));
				parcial.setPeso(String.valueOf(rs.getLong("NREV_PESO")));
				parcial.setFlagCerrarNota(rs.getString("FLGCERRARNOTA"));
				parcial.setFlagCerrarAsistencia(rs.getString("FLGCERRARASIST"));
				lista.add(parcial);
			}
			
			rs.close();
			stmt.close();
			conexion.close();			
		} catch (SQLException ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return lista;
	}

	public List<DefNroEvalParciales> consultarCierreNotas(String sede,
			String codPeriodo, String codProducto, String codEspecialida) {
		ConsultaCierreNotas consulta = new ConsultaCierreNotas(this.getDataSource());
		HashMap outputs = (HashMap) consulta.execute(sede,codPeriodo,codProducto,codEspecialida);
		if(!outputs.isEmpty())
		{					
			return (List<DefNroEvalParciales>) outputs.get("S_C_RECORDSET");
		}		
		return null;

	}

	public List<Evaluador> obtenerEvaluadorSeccion(String codCursoEjec,String codSeccion) {
		GetEvaluadorSeccion getEvaluador = new GetEvaluadorSeccion(this.getDataSource());
		HashMap outputs = (HashMap) getEvaluador.execute(codCursoEjec, codSeccion);
		if(!outputs.isEmpty())
			return (List<Evaluador>) outputs.get("S_C_RECORDSET");
			
		return null;
	}

	public String abrirRegistroNotas(String codCurso, String codSeccion,String tipoEvaluacion, String codUsuario) {
		AbrirRegistroNotas abrirNotas = new AbrirRegistroNotas(this.getDataSource());
		HashMap output = (HashMap) abrirNotas.execute(codCurso, codSeccion, tipoEvaluacion, codUsuario);
		if(!output.isEmpty())
		{					
			return (String) output.get("S_V_RETVAL");
		}
		
		return null;
	}

	public String cerrarRegistroAsistencia(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario, String flagCerrado) {
		CerrarRegistrosAsistencia cierre = new CerrarRegistrosAsistencia(this.getDataSource());
		HashMap output = (HashMap) cierre.execute(codCurso, codSeccion, tipoEvaluacion, codUsuario, flagCerrado);
		if(!output.isEmpty())
		{					
			return (String) output.get("S_V_RETVAL");
		}
		
		return null;
	}

	public String abrirRegistroAsistencia(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario) {
		AbrirRegistroAsistencia abrir = new AbrirRegistroAsistencia(this.getDataSource());
		HashMap output = (HashMap) abrir.execute(codCurso, codSeccion, tipoEvaluacion, codUsuario);
		if(!output.isEmpty())
		{					
			return (String) output.get("S_V_RETVAL");
		}
		
		return null;
	}

	public List<Alumno> listarDesaprobadosXDI(String curso, String seccion, String nombres, String apellidos) {
		Connection conexion = null;
		Alumno alumno = null;
		List<Alumno> listaAlumnos = null;
		String sql="";
		try{
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			sql = "select ca.codalumno,(select a.codcarnet from docencia.doc_alumno a where a.codalumno=ca.codalumno) carnet, general.nombrecliente(ca.codalumno) nombre " +
				//" ca.codsecteo,ca.codseclab,ca.codsectal " +
				" from eva_curso_alumno ca , general.gen_persona p " +
				" where ca.codalumno=p.codpersona and ca.indnoaproboxdi = '1' and ca.codcursoejec="+ curso + "" +
				" and ('" + nombres + "' is null or translate(upper(trim(p.nomuno||' '||nvl(p.nomdos,''))),'WV�������ZKCYJH','BBAEIOUUNSSSII') " +
				" like '%'|| translate(upper(trim('" + nombres + "')),'WV�������ZKCYJH','BBAEIOUUNSSSII') ||'%') " +
				" and (ca.codsecteo="+seccion+" or ca.codseclab="+seccion+" or ca.codsectal="+seccion+")" +
				" and ('"+apellidos+"' is null or translate(upper(trim(p.apepaterno||' '||nvl(p.apematerno,''))),'WV�������ZKCYJH','BBAEIOUUNSSSII') " +
				" like '%'|| translate(upper(trim('"+apellidos+"')),'WV�������ZKCYJH','BBAEIOUUNSSSII') ||'%') " +
				" order by 3 ";
			
			//System.out.println("sql:" + sql);
			
			stmt.executeQuery(sql);
			ResultSet rs = stmt.executeQuery(sql);
			listaAlumnos = new ArrayList<Alumno>();
			while (rs.next()) {
				alumno = new Alumno();
				alumno.setCodAlumno(String.valueOf(rs.getLong("codalumno")));
				alumno.setCodCarnet(String.valueOf(rs.getString("carnet")));
				alumno.setNombreAlumno(String.valueOf(rs.getString("nombre")));
				
				/*parcial.setCodId(String.valueOf(rs.getLong("NREV_ID")));
				parcial.setCodCurso(String.valueOf(rs.getLong("CODCURSOEJEC")));
				parcial.setSeccion(String.valueOf(rs.getLong("CODSECCION")));
				parcial.setCodEvaluador(String.valueOf(rs.getLong("CODEVALUADOR")));*/				
				listaAlumnos.add(alumno);
			}
			
			rs.close();
			stmt.close();
			conexion.close();
			
		}catch (SQLException ex) {
			ex.printStackTrace();
		}
		return listaAlumnos;
	}	
	
}
