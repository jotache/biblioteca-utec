package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluaciones.CursosMapper;
import com.tecsup.SGA.bean.EvaluacionesParciales;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Parciales;

public class GetAllEvaluacionesParcialesxCurso extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.SP_SEL_EVALUACIONES_PARCIALES";
	private static Log log = LogFactory.getLog(GetAllEvaluacionesParcialesxCurso.class);
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_V_CODCURSO = "E_V_CODCURSO";
	private static final String E_V_CODSECCION = "E_V_CODSECCION";
	private static final String E_V_NOMBRE = "E_V_NOMBRE";
	private static final String E_V_APELLIDO = "E_V_APELLIDO";
	private static final String E_C_CODNROEVAL = "E_C_CODNROEVAL";
	private static final String E_C_TIPOEVAL = "E_C_TIPOEVAL";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_FECHA_EVAL = "E_C_FECHA_EVAL";
	
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllEvaluacionesParcialesxCurso(DataSource dataSource){
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_CODCURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODSECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_APELLIDO, OracleTypes.VARCHAR));        
        declareParameter(new SqlParameter(E_C_CODNROEVAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPOEVAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FECHA_EVAL, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new EvaluacionesParcialesMapper()));
        compile();
    }

    public Map execute(String codPeriodo,String codCurso,
			String codSeccion, String strNombre, String strApellido
			,String cboNroEvaluacion,String cboEvaluacionParcial,
			String codProducto,String codEspecialidad,String fecEvaluacion) {
    	   			
    	log.info("*** INI "  + SPROC_NAME + " ****");
    	log.info("E_C_CODPERIODO:"+codPeriodo);
    	log.info("E_V_CODCURSO:"+codCurso);
    	log.info("E_V_CODSECCION:"+codSeccion);
    	log.info("E_V_NOMBRE:"+strNombre);
    	log.info("E_V_APELLIDO:"+strApellido);
    	log.info("E_C_CODNROEVAL:"+cboNroEvaluacion);
    	log.info("E_C_TIPOEVAL:"+cboEvaluacionParcial);
    	log.info("E_C_CODPRODUCTO:"+codProducto);
    	log.info("E_C_CODESPECIALIDAD:"+codEspecialidad);
    	log.info("E_C_FECHA_EVAL:"+fecEvaluacion);
    	log.info("*** FIN "  + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_V_CODCURSO, codCurso);
    	inputs.put(E_V_CODSECCION, codSeccion);
    	inputs.put(E_V_NOMBRE, strNombre);
    	inputs.put(E_V_APELLIDO, strApellido);
    	inputs.put(E_C_CODNROEVAL, cboNroEvaluacion);
    	inputs.put(E_C_TIPOEVAL, cboEvaluacionParcial);
    	inputs.put(E_C_CODPRODUCTO, codProducto);
    	inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
    	inputs.put(E_C_FECHA_EVAL, fecEvaluacion);    	
    	
        return super.execute(inputs);
    }
    final class EvaluacionesParcialesMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	EvaluacionesParciales evaluacionesParciales = new EvaluacionesParciales();      	

        	evaluacionesParciales.setCodigo(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
        	evaluacionesParciales.setNombre(rs.getString("NOMBRE")== null ? "" : rs.getString("NOMBRE"));
        	evaluacionesParciales.setFechaEval(rs.getString("FECHAEVAL")== null ? "" : rs.getString("FECHAEVAL"));
        	evaluacionesParciales.setNroEval(rs.getString("NROEVAL")== null ? "" : rs.getString("NROEVAL"));
        	evaluacionesParciales.setCodCurso(rs.getString("CODCURSO")== null ? "" : rs.getString("CODCURSO"));
        	evaluacionesParciales.setCodTipoMotivo(rs.getString("CODTIPOMOTIVO")== null ? "" : rs.getString("CODTIPOMOTIVO"));
        	evaluacionesParciales.setCodTipoExamenParcial(rs.getString("CODTIPOEXAMENPARCIAL")== null ? "" : rs.getString("CODTIPOEXAMENPARCIAL"));
        	evaluacionesParciales.setNota(rs.getString("NOTA")== null ? "" : rs.getString("NOTA"));
        	evaluacionesParciales.setFlgSancion(rs.getString("SANCION_ADM")== null ? "" : rs.getString("SANCION_ADM"));
        	evaluacionesParciales.setCodSeccion(rs.getString("CODSECCION")== null ? "" : rs.getString("CODSECCION"));
        	evaluacionesParciales.setIdDef(rs.getString("nrev_id")== null ? "" : rs.getString("nrev_id"));
        	evaluacionesParciales.setFlgRealizado(rs.getString("nrev_flgrealizado")== null ? "" : rs.getString("nrev_flgrealizado"));
        	evaluacionesParciales.setEstadoCursoAlumno(rs.getString("estado")== null ? "" : rs.getString("estado"));
            return evaluacionesParciales;
        }
    }
}
