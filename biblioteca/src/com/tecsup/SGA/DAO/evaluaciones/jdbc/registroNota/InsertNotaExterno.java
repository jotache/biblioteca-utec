package com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class InsertNotaExterno extends StoredProcedure{	
	private static Log log = LogFactory.getLog(InsertNotaExterno.class);
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	                                 	+ ".pkg_eval_oper_admin.sp_ins_nota_caso_especial";

private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
private static final String E_C_CODCURSO = "E_C_CODCURSO";
private static final String E_C_CODCICLO = "E_C_CODCICLO";
private static final String E_C_TIPO = "E_C_TIPO";
private static final String E_C_NOTA = "E_C_NOTA";
private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
private static final String E_C_CODETAPA = "E_C_CODETAPA";
private static final String E_C_CODPROGRAMA = "E_C_CODPROGRAMA";
private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
private static final String S_V_RETVAL = "S_V_RETVAL";

public InsertNotaExterno(DataSource dataSource) {
super(dataSource, SPROC_NAME);
declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_NOTA, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODETAPA, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODPROGRAMA, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
compile();
}

	public Map execute(String codPeriodo, 
			String codProducto, String codAlumno, String codCurso, String codCiclo, 
			String codTipo, String notaExterno, String codEspecialidad,
			String codEtapa, String codPrograma, String usuario) {
	
		log.info("****** INI " + SPROC_NAME + " *****");
    	log.info("E_C_CODPERIODO:"+codPeriodo);
    	log.info("E_C_CODPRODUCTO:"+codProducto);
    	log.info("E_C_CODALUMNO:"+codAlumno);
    	log.info("E_C_CODCURSO:"+codCurso);
    	log.info("E_C_CODCICLO:"+codCiclo);    	
    	log.info("E_C_TIPO:"+codTipo);
    	log.info("E_C_NOTA:"+notaExterno);
    	log.info("E_C_CODESPECIALIDAD:"+codEspecialidad);
    	log.info("E_C_CODETAPA:"+codEtapa);
    	log.info("E_C_CODPROGRAMA:"+codPrograma);
    	log.info("E_V_CODUSUARIO:"+usuario);
    	log.info("****** FIN " + SPROC_NAME + " *****");
    	
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODPERIODO, codPeriodo);
		inputs.put(E_C_CODPRODUCTO, codProducto);
		inputs.put(E_C_CODALUMNO, codAlumno);
		inputs.put(E_C_CODCURSO, codCurso);
		inputs.put(E_C_CODCICLO, codCiclo);
		inputs.put(E_C_TIPO, codTipo);
		inputs.put(E_C_NOTA, notaExterno);
		inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
		inputs.put(E_C_CODETAPA, codEtapa);
		inputs.put(E_C_CODPROGRAMA, codPrograma);
		inputs.put(E_V_CODUSUARIO, usuario);
		
		return super.execute(inputs);
	
	}

}