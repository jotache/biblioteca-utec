package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllNroEvaluacionesxParciales.DefNroEvalParcialesMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.HorasXalumnos;

public class HorasPorAlumno extends StoredProcedure {
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_admin.sp_sel_valida_horas_x_alumno";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; 
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD"; 
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_CODTIPOPRACTICA = "E_C_CODTIPOPRACTICA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";

    
    public HorasPorAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.VARCHAR));        
        declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOPRACTICA, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new HorasPorAlumnoMapper()));
        compile();
    }

    public Map execute(String codPeriodo,String codEspecialidad
    		, String codAlumno, String codTipoPractica) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO,codPeriodo);
    	inputs.put(E_C_CODESPECIALIDAD,codEspecialidad); 
    	inputs.put(E_C_CODALUMNO,codAlumno);
    	inputs.put(E_C_CODTIPOPRACTICA,codTipoPractica);
    	return super.execute(inputs);
    }  
    
    //DefNroEvalParciales
    final class HorasPorAlumnoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	HorasXalumnos obj = new HorasXalumnos();
        	
        	obj.setCantHoras(rs.getString("CANTIDADTOTAL"));
        	
            return obj;
        }
    }
}
