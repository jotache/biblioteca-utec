package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Evaluador;

public class GetAllEvaluadores extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_comun.sp_sel_evaluadores_x_curso";	
	private static final String E_C_CODCURSO = "E_C_CODCURSO"; //CODIGO PERIODO
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; //JHPR 2008-06-30
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllEvaluadores(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));        
        compile();
    }

    public Map execute(String codCurso,String codPeriodo) {    	
    	System.out.println("GetAllEvaluadores()::codPeriodo:"+codPeriodo);    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODCURSO, codCurso);
    	inputs.put(E_C_CODPERIODO, codPeriodo);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Evaluador curso = new Evaluador();
        	curso.setCodEvaluador(rs.getString("CODEVALUADOR"));
        	curso.setDscEvaluador(rs.getString("NOMBREEVALUADOR"));
            return curso;
        }
    }
}
