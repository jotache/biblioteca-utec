package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursoEvaluador;

/*
	PROCEDURE SP_SEL_TIP_SESIONXCURSO(
		E_V_CODCURSO IN VARCHAR2,
		S_C_RECORDSET IN OUT CUR_RECORDSET
	);
*/

public class GetAllSesionXCurso extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllSesionXCurso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_sel_tip_sesionxcurso";
	private static final String COD_CURSO = "E_V_CODCURSO"; //CODIGO CURSO
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllSesionXCurso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(COD_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new TipoSesionCursoMapper()));
        compile();
    }

    public Map execute(String codCurso) {
    	log.info("******* INICIO "+SPROC_NAME+" ******");
    	log.info("tipoExamen:"+codCurso);
    	log.info("******* INICIO "+SPROC_NAME+" ******");
    	
    	Map inputs = new HashMap();
    	inputs.put(COD_CURSO, codCurso);
    	
        return super.execute(inputs);
    }
    
    final class TipoSesionCursoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CursoEvaluador curso = new CursoEvaluador();
        	
        	curso.setCodTipoSesion(rs.getString("CODIGO"));
        	curso.setDscTipoSesion(rs.getString("DESCRIPCION"));
        	
            return curso;
        }
    }
}