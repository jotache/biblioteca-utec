package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;

import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursoEvaluador;

/*
	PROCEDURE SP_SEL_SECCION(
		E_C_TIPOSESION IN CHAR,
		E_V_CODCURSO IN VARCHAR2,
		S_C_RECORDSET IN OUT CUR_RECORDSET
	);
*/

public class GetAllSeccionCurso extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_comun.sp_sel_seccion";
	private static final String TIPOSESION = "E_C_TIPOSESION"; //TIPO SESION
	private static final String COD_CURSO = "E_V_CODCURSO"; //CODIGO CURSO
    private static final String RECORDSET = "S_C_RECORDSET";
    
    private static Log log = LogFactory.getLog(GetAllSeccionCurso.class);
    
    public GetAllSeccionCurso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(TIPOSESION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(COD_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AlumnosMapper()));
        compile();
    }

    public Map execute(String tipoSesion, String codCurso) {
    	
    	Map inputs = new HashMap();
    	log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("E_C_TIPOSESION:"+tipoSesion);
    	log.info("E_V_CODCURSO:"+codCurso);
    	log.info("*** FIN " + SPROC_NAME + " ***");
    	inputs.put(TIPOSESION, tipoSesion);
    	inputs.put(COD_CURSO, codCurso);
    	
        return super.execute(inputs);
    }
    
    final class AlumnosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CursoEvaluador curso = new CursoEvaluador();
        	
        	curso.setCodSeccion(rs.getString("CODSECCION"));
        	curso.setDscSeccion(rs.getString("DESCRIPCIONSECCION"));
        	
            return curso;
        }
    }
}