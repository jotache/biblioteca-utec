package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
//import org.springframework.transaction.support.TransactionTemplate;


import com.tecsup.SGA.DAO.evaluaciones.MantenimientoDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllCiclosCat;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllCursosCat;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllParametrosEspeciales;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllPeriodosBySede;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllTipoPractica;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllTipoPracticaxCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.InsertEvaluacioneParciales;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.InsertParametrosEspeciales;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.UpdateEvaluacioneParciales;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.UpdateParametrosEspeciales;
//import com.tecsup.SGA.DAO.reclutamiento.mantenimiento.GetAllTabla;
//import com.tecsup.SGA.DAO.reclutamiento.postulante.InsertReclutaDatos;

public class MantenimientoDAOJdbc extends JdbcDaoSupport implements MantenimientoDAO {

	
	/*PROCEDURE SP_INS_PARAMETROS_ESPECIALES(
			E_C_CODPERIODO IN CHAR,
			E_C_CADVALORESFORM IN VARCHAR2,
			E_C_CADVALORESCAT IN VARCHAR2,
			E_C_CADVALORESESCALA IN VARCHAR2,
			E_V_NROREGISTROSCAT IN VARCHAR2,
			E_V_CODUSUARIO IN VARCHAR2,
			S_V_RETVAL IN OUT VARCHAR2);*/
	
	public String insertParametrosEspeciales(String codPeriodo,String cadValoresForm,String cadValoresCat
			,String  cadValoresEscala,String nroRegistrosCat,String codUsuario)
	{	
		InsertParametrosEspeciales insertParametrosEspeciales = new InsertParametrosEspeciales(this.getDataSource());	

		HashMap outputs = (HashMap)insertParametrosEspeciales.execute(codPeriodo,cadValoresForm,cadValoresCat
				,cadValoresEscala,nroRegistrosCat,codUsuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List getLstCilos(String codPeriodo){

			GetAllCiclosCat getAllCiclosCat = new GetAllCiclosCat(this.getDataSource());
			
			HashMap outputs = (HashMap)getAllCiclosCat.execute(codPeriodo);
			{
				return (List)outputs.get("S_C_RECORDSET");//VERIFICAR
			}
		
	}

	public List getAllParametrosEspeciales(String codPeriodo, String codTabla) {
		GetAllParametrosEspeciales getAllParametrosEspeciales = new GetAllParametrosEspeciales(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllParametrosEspeciales.execute(codPeriodo,codTabla);
		{
			return (List)outputs.get("S_C_RECORDSET");//VERIFICAR
		}
	}

	public String updateParametrosEspeciales(String codPeriodo,
			String cadValoresForm, String cadValoresCat,
			String cadValoresEscala, String nroRegistrosCat, String codUsuario){
		UpdateParametrosEspeciales updateParametrosEspeciales = new UpdateParametrosEspeciales(this.getDataSource());	

		HashMap outputs = (HashMap)updateParametrosEspeciales.execute(codPeriodo,cadValoresForm,cadValoresCat
				,cadValoresEscala,nroRegistrosCat,codUsuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List getTipoPractica() {
		
		GetAllTipoPractica getAllTipoPractica = new GetAllTipoPractica(this.getDataSource());
		HashMap outputs = (HashMap)getAllTipoPractica.execute();
		{
			return (List)outputs.get("S_C_RECORDSET");//VERIFICAR
		}
	}

	public String insertEvaluacioneParciales(String codCurso,
			String codSeccion,
			String feEvaluacion,
			String codNroEval,
			String nroEvaluacion,
			String codTipoEvaluacion,
			String cadCodAlumno,
			String nroRegistros,
			String codUsuario){
		
		InsertEvaluacioneParciales insertEvaluacioneParciales = new InsertEvaluacioneParciales(this.getDataSource());	

		HashMap outputs = (HashMap)insertEvaluacioneParciales.execute( codCurso,
				 codSeccion,  feEvaluacion,codNroEval,nroEvaluacion,codTipoEvaluacion,cadCodAlumno,
				 nroRegistros,  codUsuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String updateEvaluacioneParciales(String codCurso, String codSeccion,
			String feEvaluacion, String nroEvaluacion,String codTipoEvaluacion, String cadCodAlumno,
			String nroRegistros, String codUsuario,String codtipoEval) {
		
		UpdateEvaluacioneParciales updateEvaluacioneParciales = new UpdateEvaluacioneParciales(this.getDataSource());	

		HashMap outputs = (HashMap) updateEvaluacioneParciales.execute( codCurso, codSeccion,
				 feEvaluacion,nroEvaluacion,codTipoEvaluacion,cadCodAlumno,
				 nroRegistros,  codUsuario,codtipoEval);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List getTipoPracticaxCurso(String codPeriodo, String codCurso) {
		GetAllTipoPracticaxCurso getAllTipoPracticaxCurso = new GetAllTipoPracticaxCurso(this.getDataSource());
		HashMap outputs = (HashMap)getAllTipoPracticaxCurso.execute( codPeriodo,  codCurso);
		{
			return (List)outputs.get("S_C_RECORDSET");//VERIFICAR
		}	
	}
	public List getAllCursosCat(String codProducto, String codCiclo){
		GetAllCursosCat getAllCursosCat = new GetAllCursosCat(this.getDataSource());
		HashMap outputs = (HashMap)getAllCursosCat.execute( codProducto,  codCiclo);
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
	}
	public List GetAllPeriodosBySede(String codSede){
		GetAllPeriodosBySede getAllPeriodosBySede = new GetAllPeriodosBySede(this.getDataSource());
		HashMap outputs = (HashMap)getAllPeriodosBySede.execute(codSede);
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
	}

}
