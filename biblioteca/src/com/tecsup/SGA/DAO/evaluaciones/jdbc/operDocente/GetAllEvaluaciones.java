package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCursosEvaluador.CursosMapper;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Parciales;;

public class GetAllEvaluaciones extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_sel_def_eval_parciales";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODCURSO = "E_C_CODCURSO"; //CODIGO PERIODO
	private static final String E_C_CODSECCION = "E_C_CODSECCION";
	private static final String E_C_CODEVALUADOR = "E_C_CODEVALUADOR"; //CODIGO EVALUADOR
	private static final String E_C_CODTIPOEVALUACION = "E_C_CODTIPOEVALUACION"; //CODIGO EVALUADOR
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllEvaluaciones(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODEVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOEVALUACION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codPeriodo,String codPro,String codEspe, String codCurso
    		, String seccion, String codEvaluador, String codTipoEva) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_C_CODPRODUCTO, codPro);
    	inputs.put(E_C_CODESPECIALIDAD, codEspe);
    	inputs.put(E_C_CODCURSO, codCurso);
    	inputs.put(E_C_CODSECCION, seccion);
    	inputs.put(E_C_CODEVALUADOR, codEvaluador);
    	inputs.put(E_C_CODTIPOEVALUACION, codTipoEva);
    	return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Parciales curso = new Parciales();
        	
        	curso.setCodId(rs.getString("CODDEF"));
        	curso.setPeso(rs.getString("PESO"));
        	curso.setCodEvaluador(rs.getString("CODEVALUADOR"));
        	curso.setNombreEvaluador(rs.getString("NOMBREEVALUADOR"));
        	curso.setFlag(rs.getString("FLAG"));
        	curso.setFechaRegistro(rs.getString("FECHAREGISTRO"));
        	curso.setNroEval(rs.getString("NROEVAL"));
        	curso.setFlagEliminar(rs.getString("FLAGELIMINAR"));
            return curso;
        }
    }
}
