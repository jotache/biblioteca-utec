package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;

import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursoEvaluador;

public class GetAllCursosEvaluador extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllCursosEvaluador.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_sel_cursos_x_evaluador";
	private static final String PERIODO = "E_V_CODPERIODO"; //CODIGO PERIODO
	private static final String EVALUADOR = "E_V_CODEVALUADOR"; //CODIGO EVALUADOR
	private static final String PRODUCTO = "E_V_CODPRODUCTO"; //CODIGO PRODCUTO
	private static final String ESPECIALIDAD = "E_V_CODESPECIALIDAD"; //CODIGO ESPECIALIDAD
	private static final String CURSO = "E_V_CODCURSO"; //CODIGO EVALUADOR
	private static final String CODEVALUADOR_SEL = "E_C_CODEVALUADOR_SEL"; //COD EVALUADOR SELECCIONADO (combo)
	private static final String DSC_CURSO = "E_V_DSC_CURSO"; //Parte del nombre del curso.
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllCursosEvaluador(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(EVALUADOR, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODEVALUADOR_SEL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(DSC_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String periodo, String evaluador, String producto, String especialidad, String curso,
    			String codEvalSeleccionado, String dscCurso) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("*********" + SPROC_NAME + "*******");
    	log.info("E_V_CODPERIODO:"+periodo);
    	log.info("E_V_CODEVALUADOR:"+evaluador);
    	log.info("E_V_CODPRODUCTO:"+producto);
    	log.info("E_V_CODESPECIALIDAD:"+especialidad);
    	log.info("E_V_CODCURSO:"+curso);
    	log.info("E_C_CODEVALUADOR_SEL:"+codEvalSeleccionado);    	
    	log.info("E_V_DSC_CURSO:"+dscCurso);
    	log.info("***********fin********");
    	
    	inputs.put(PERIODO, periodo);
    	inputs.put(EVALUADOR, evaluador);
    	inputs.put(PRODUCTO, producto);
    	inputs.put(ESPECIALIDAD, especialidad);
    	inputs.put(CURSO, curso);
    	inputs.put(CODEVALUADOR_SEL, codEvalSeleccionado);
    	inputs.put(DSC_CURSO, dscCurso);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CursoEvaluador curso = new CursoEvaluador();
        	
        	curso.setEvaluador(rs.getString("EVALUADOR"));
        	curso.setPeriodo(rs.getString("PERIODO"));
        	curso.setCodProducto(rs.getString("CODPROD"));
        	curso.setCodCurso(rs.getString("CODCURSO"));
        	curso.setCiclo(rs.getString("CICLO"));
        	curso.setProducto(rs.getString("PRODUCTO"));
        	curso.setEspecialidad(rs.getString("ESP"));
        	curso.setCurso(rs.getString("CURSO"));
        	curso.setSistEval(rs.getString("SIST_EVAL"));
        	curso.setCodEspecialidad(rs.getString("CODESP"));
        	curso.setCadComponentes(rs.getString("CADCOMPONENTES"));
        	
            return curso;
        }
    }
}