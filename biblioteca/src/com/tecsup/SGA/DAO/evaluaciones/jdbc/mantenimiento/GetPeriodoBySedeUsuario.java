package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;

public class GetPeriodoBySedeUsuario extends StoredProcedure {

	private static Log log = LogFactory.getLog(GetPeriodoBySedeUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
 	+ ".pkg_eval_comun.SP_SEL_PERIODO_X_SEDE_USUARIO";

	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String E_C_USUARIO = "E_C_USUARIO";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetPeriodoBySedeUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_USUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new PeriodoSedeUsuarioMapper()));
		compile();
	}

	public Map execute(String codSede, String codUsuario) {
		log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("E_C_CODSEDE:"+codSede);
    	log.info("E_C_USUARIO:"+codUsuario);
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
		Map inputs = new HashMap();		
		inputs.put(E_C_CODSEDE, codSede);
		inputs.put(E_C_USUARIO, codUsuario);
			
		return super.execute(inputs);
	}
	
	final class PeriodoSedeUsuarioMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Periodo periodo = new Periodo();
			periodo.setCodigo(rs.getString("CODPERIODO"));
	    	periodo.setNombre(rs.getString("DESCPERIODO"));
	        periodo.setFecInicio(rs.getString("FECHAINICIO"));
	        periodo.setFecFin(rs.getString("FECHAFIN"));
	        periodo.setSede(rs.getString("SEDE"));
	        return periodo;
		}
	}
}
