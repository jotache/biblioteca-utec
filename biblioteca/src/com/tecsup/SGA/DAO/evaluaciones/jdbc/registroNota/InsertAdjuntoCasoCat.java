package com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class InsertAdjuntoCasoCat extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertAdjuntoCasoCat.class);
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	                                 	+ ".pkg_eval_oper_admin.sp_ins_adjunto_caso_cat";

private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
private static final String E_C_CODCICLO = "E_C_CODCICLO";
private static final String E_V_NOMBRENUEVOARCHIVO = "E_V_NOMBRENUEVOARCHIVO";
private static final String E_V_NOMBREARCHIVO = "E_V_NOMBREARCHIVO";
private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
private static final String S_V_RETVAL = "S_V_RETVAL";

public InsertAdjuntoCasoCat(DataSource dataSource) {
super(dataSource, SPROC_NAME);
declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_V_NOMBRENUEVOARCHIVO, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_NOMBREARCHIVO, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
compile();
}

	public Map execute(String codPeriodo, String codProducto, String codCiclo, String nuevoArchivo, String archivo, String usuario) {
	
		log.info("****** INI " + SPROC_NAME + " *****");
    	log.info("E_C_CODPERIODO:"+codPeriodo);
    	log.info("E_C_CODPRODUCTO:"+codProducto);    	
    	log.info("E_C_CODCICLO:"+codCiclo);
    	log.info("E_V_NOMBRENUEVOARCHIVO:"+nuevoArchivo);
    	log.info("E_V_NOMBREARCHIVO:"+archivo);
    	log.info("E_V_CODUSUARIO:"+usuario);
    	log.info("****** FIN " + SPROC_NAME + " *****");
    	
		Map inputs = new HashMap();		
		inputs.put(E_C_CODPERIODO, codPeriodo);
		inputs.put(E_C_CODPRODUCTO, codProducto);		
		inputs.put(E_C_CODCICLO, codCiclo);
		inputs.put(E_V_NOMBRENUEVOARCHIVO, nuevoArchivo);
		inputs.put(E_V_NOMBREARCHIVO, archivo);
		inputs.put(E_V_CODUSUARIO, usuario);
		
		return super.execute(inputs);
	
	}

}