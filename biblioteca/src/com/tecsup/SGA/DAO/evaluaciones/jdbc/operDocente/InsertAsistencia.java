package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.evaluaciones.Impl.OperDocenteManagerImpl;

/*
	PROCEDURE SP_INS_ASISTENCIA(
		E_V_CODPERIODO IN VARCHAR2,
		E_V_CODASISTENCIA IN VARCHAR2,
		E_V_CODALUMNO IN VARCHAR2,
		E_V_CODCURSO IN VARCHAR2,
		E_V_TIPOSESION IN VARCHAR2,
		E_V_CODSECCION IN VARCHAR2,
		E_V_NROSESION IN VARCHAR2,
		E_V_FECHASESION IN VARCHAR2,
		E_V_TIPOASISTENCIA IN VARCHAR2,
		E_V_CODUSUARIO IN VARCHAR2,
		E_C_FLG_PROG IN CHAR,
		S_V_RETVAL IN OUT VARCHAR2
	);
*/

public class InsertAsistencia extends StoredProcedure{
	
	private static Log log = LogFactory.getLog(OperDocenteManagerImpl.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION +
											".pkg_eval_oper_docente.sp_ins_asistencia";
	private static final String ASIS_CODIGO = "E_V_CODASISTENCIA"; 
	private static final String ASIS_COD_ALUMNO = "E_V_CODALUMNO"; 
	private static final String ASIS_COD_CURSO = "E_V_CODCURSO";
	private static final String ASIS_TIPO_SESION = "E_V_TIPOSESION";
	private static final String ASIS_COD_SECCION = "E_V_CODSECCION";
	private static final String ASIS_NRO_SESION = "E_V_NROSESION";
	private static final String ASIS_FECHA = "E_V_FECHASESION";
	private static final String ASIS_TIPO_ASISTENCIA = "E_V_TIPOASISTENCIA";
	private static final String ASIS_COD_USUARIO = "E_V_CODUSUARIO";
	private static final String ASIS_FLG_PROG = "E_C_FLG_PROG";
	private static final String RETVAL = "S_V_RETVAL";
    
    public InsertAsistencia(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(ASIS_CODIGO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_COD_ALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_COD_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_TIPO_SESION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_COD_SECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_NRO_SESION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_FECHA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_TIPO_ASISTENCIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_COD_USUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_FLG_PROG, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String codPeriodo, String codAsistencia, String codAlumno, String codCurso, String tipoSesion, 
    		String codSeccion, String nroSesion, String fecha, String tipoAsist, String usuario, String flagProg) {    	
    	Map inputs = new HashMap();

    	inputs.put(ASIS_CODIGO, codAsistencia);
    	inputs.put(ASIS_COD_ALUMNO, codAlumno);
    	inputs.put(ASIS_COD_CURSO, codCurso);
    	inputs.put(ASIS_TIPO_SESION, tipoSesion);
    	inputs.put(ASIS_COD_SECCION, codSeccion);
    	inputs.put(ASIS_NRO_SESION, nroSesion);
    	inputs.put(ASIS_FECHA, fecha);
    	inputs.put(ASIS_TIPO_ASISTENCIA, tipoAsist);
    	inputs.put(ASIS_COD_USUARIO, usuario);
    	inputs.put(ASIS_FLG_PROG, flagProg);
    	
    	log.info("SPROC_NAME: "+SPROC_NAME);
    	Iterator it = inputs.entrySet().iterator();
    	while (it.hasNext()) {
    		Map.Entry e = (Map.Entry)it.next();
    		log.info(e.getKey() + ": " + e.getValue());
    	}

    	
        return super.execute(inputs);
    }
   
}
