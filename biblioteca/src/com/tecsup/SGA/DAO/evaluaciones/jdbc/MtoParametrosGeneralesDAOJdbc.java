package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.MtoParametrosGeneralesDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.*;
//import com.tecsup.SGA.DAO.jdbc.comun.GetAllInstitucion;
import com.tecsup.SGA.DAO.jdbc.mantenimiento.GetAllTablaDetalle;

public class MtoParametrosGeneralesDAOJdbc extends JdbcDaoSupport implements MtoParametrosGeneralesDAO{

	
	/*public List getParametrosGenerales(String codProducto, String codPeriodo)//esto se saca de tabla_detalle(este metodo deberia estar en el tabladetalle)
	{
		GetParametrosGenerales getParametrosGenerales = new GetParametrosGenerales(this.getDataSource());
		
		HashMap outputs = (HashMap)getParametrosGenerales.execute(codProducto, codPeriodo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}*/
	public List getAllParametrosGenerales(String codProducto, String codPeriodo)
	{
		GetParametrosGenerales getParametrosGenerales = new GetParametrosGenerales(this.getDataSource());
		
		HashMap outputs = (HashMap)getParametrosGenerales.execute(codProducto, codPeriodo);
		if (!outputs.isEmpty())
		{   System.out.println("MtoParametrosGeneralesDAOJdbc ");
			return (List)outputs.get("S_C_RECORDSET");
			
		}
		return null;
	}
	
	public String Insert_O_UpdateParametrosGenerales(String codProducto, String codPeriodo, String codTabla
			, String valor, String codUsuario)//String codProducto, String codPeriodo, String codTabla
	//, String valor, String codUsuario
	{
		Insert_UpdateParametrosGenerales insert_UpdateParametrosGenerales = new Insert_UpdateParametrosGenerales(this.getDataSource());
			
			HashMap inputs = (HashMap)insert_UpdateParametrosGenerales.execute(codProducto, codPeriodo, codTabla
					, valor, codUsuario);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public List getProductos() 
	{
		GetAllProductos getProductos = new GetAllProductos(this.getDataSource());
		
		HashMap outputs = (HashMap)getProductos.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
}
