package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class UpdateSistevalHibrido extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateSistevalHibrido.class);
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	                                 	+ ".pkg_eval_mantto_config.sp_act_config_hibrido";

private static final String E_C_CODIGO = "E_C_CODIGO";
private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
private static final String E_C_CADEVALUACION = "E_C_CADEVALUACION";
private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";
private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
private static final String S_V_RETVAL = "S_V_RETVAL";

public UpdateSistevalHibrido(DataSource dataSource) {
super(dataSource, SPROC_NAME);
declareParameter(new SqlParameter(E_C_CODIGO, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_C_CADEVALUACION, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
compile();
}

	public Map execute(String inCodigo, String inDescripcion, String inCadEvaluacion, String inNroRegistros, String inCodUsuario) {
	
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("E_C_CODIGO: "+inCodigo);
		log.info("E_V_DESCRIPCION: "+inDescripcion);
		log.info("E_C_CADEVALUACION: "+inCadEvaluacion);	
		log.info("E_V_NROREGISTROS: "+inNroRegistros);
		log.info("E_V_CODUSUARIO: "+inCodUsuario);		
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODIGO, inCodigo);
		inputs.put(E_V_DESCRIPCION, inDescripcion);
		inputs.put(E_C_CADEVALUACION, inCadEvaluacion);
		inputs.put(E_V_NROREGISTROS, inNroRegistros);
		inputs.put(E_V_CODUSUARIO, inCodUsuario);
		
		return super.execute(inputs);
	
	}

}