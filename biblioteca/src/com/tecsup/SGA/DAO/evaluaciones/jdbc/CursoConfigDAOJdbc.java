package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.List;
import java.util.HashMap;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.*;//donde se llama al dao
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.*;//donde esta el metodo que implementa el procedure
import com.tecsup.SGA.DAO.evaluaciones.CursoConfigDAO;

public class CursoConfigDAOJdbc extends JdbcDaoSupport implements CursoConfigDAO{
	
	public List getCursoConfigById(String codCurso){
		GetCursoConfigById getProductoConfigById = new GetCursoConfigById(this.getDataSource());
		
		HashMap outputs = (HashMap)getProductoConfigById.execute(codCurso);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String InsertCursoConfig(String codCurso, String regla, String condicion,
			String verdadero01, String verdadero02, String verdadero03, String verdadero04,
			String falso01, String falso02, String falso03, String falso04,
			String opeVer01, String opeVer02, String opeVer03, String opeVer04,
			String opeFal01, String opeFal02, String opeFal03, String opeFal04,
			String usuario){
						
		InsertCursoConfig insertProductoConfig = new InsertCursoConfig(this.getDataSource());
		
		HashMap inputs = (HashMap)insertProductoConfig.execute(codCurso,regla,condicion,
				verdadero01,verdadero02,verdadero03,verdadero04,
				falso01,falso02,falso03,falso04,
				opeVer01,opeVer02,opeVer03,opeVer04,
				opeFal01,opeFal02,opeFal03,opeFal04,
				usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
		
	public String UpdateCursoConfig(String codCurso, String regla, String condicion,
			String verdadero01, String verdadero02, String verdadero03, String verdadero04,
			String falso01, String falso02, String falso03, String falso04,
			String opeVer01, String opeVer02, String opeVer03, String opeVer04,
			String opeFal01, String opeFal02, String opeFal03, String opeFal04,
			String usuario){
		UpdateCursoConfig updateProductoConfig = new UpdateCursoConfig(this.getDataSource());
		
		HashMap inputs = (HashMap)updateProductoConfig.execute(codCurso,regla,condicion,
				verdadero01,verdadero02,verdadero03,verdadero04,
				falso01,falso02,falso03,falso04,
				opeVer01,opeVer02,opeVer03,opeVer04,
				opeFal01,opeFal02,opeFal03,opeFal04,
				usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
}
