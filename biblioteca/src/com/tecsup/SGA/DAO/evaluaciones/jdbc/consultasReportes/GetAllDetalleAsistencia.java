package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.bean.DetalleAsistencia;
import com.tecsup.SGA.common.CommonConstants;

public class GetAllDetalleAsistencia extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAllDetalleAsistencia.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
			+ ".PKG_EVAL_CONSULTAS_REPORTES.SP_SEL_DETALLE_ASISTENCIA";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_CODCICLO = "E_C_CODCICLO";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	private static final String RECORDSET = "S_C_RECORDSET";

	public GetAllDetalleAsistencia(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR,
				new DetalleInformacionGeneralMapper()));
		compile();
	}

	public Map execute(String codPeriodo, String codProducto, String codAlumno,
			String codCiclo, String codCurso) {

		Map inputs = new HashMap();
		
		log.info("**** INI "  + SPROC_NAME + " ****");
		log.info("E_C_CODPERIODO:,"+ codPeriodo);
		log.info("E_C_CODPRODUCTO:"+ codProducto);
		log.info("E_C_CODALUMNO:"+ codAlumno);
		log.info("E_C_CODCICLO:"+ codCiclo);
		log.info("E_C_CODCURSO:"+ codCurso);
		log.info("**** FIN "  + SPROC_NAME + " ****");
		
		inputs.put(E_C_CODPERIODO, codPeriodo);
		inputs.put(E_C_CODPRODUCTO, codProducto);
		inputs.put(E_C_CODALUMNO, codAlumno);
		inputs.put(E_C_CODCICLO, codCiclo);
		inputs.put(E_C_CODCURSO, codCurso);
		return super.execute(inputs);
	}

	final class DetalleInformacionGeneralMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			DetalleAsistencia detalleAsistencia = new DetalleAsistencia();

			detalleAsistencia.setCodigo(rs.getString("CODIGO"));
			detalleAsistencia.setNombre(rs.getString("NOMBRE"));
			detalleAsistencia.setTipoAsist(rs.getString("TIPOASIST"));
			detalleAsistencia.setAsistencia(rs.getString("ASISTENCIA"));
			detalleAsistencia.setFecha(rs.getString("FECHA"));
			detalleAsistencia.setCodCurso(rs.getString("CODCURSO"));
			detalleAsistencia.setCodCiclo(rs.getString("CODCICLO"));

			return detalleAsistencia;
		}
	}
}