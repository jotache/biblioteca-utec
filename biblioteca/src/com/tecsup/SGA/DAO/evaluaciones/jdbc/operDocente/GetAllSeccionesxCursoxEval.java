package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursoEvaluador;

public class GetAllSeccionesxCursoxEval extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllSeccionesxCursoxEval.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.SP_SEL_SECC_X_CURSO_X_EVA";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	private static final String E_C_CODEVALUADOR = "E_C_CODEVALUADOR";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODTIPOPRACTICA = "E_C_CODTIPOPRACTICA";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllSeccionesxCursoxEval(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODEVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOPRACTICA, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AlumnosMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codCurso,
			String codEval,String codProducto, String codEspecialidad, String cboEvaluacionParcial) {
    	
    	log.info("**** " + SPROC_NAME + " *********");
    	log.info("E_C_CODPERIODO:"+codPeriodo);
    	log.info("E_C_CODCURSO:"+codCurso);
    	log.info("E_C_CODEVALUADOR:"+codEval);
    	log.info("E_C_CODPRODUCTO:"+codProducto);
    	log.info("E_C_CODESPECIALIDAD:"+codEspecialidad);
    	log.info("E_C_CODTIPOPRACTICA:"+cboEvaluacionParcial);
    	log.info("*************");
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_C_CODCURSO, codCurso);
    	inputs.put(E_C_CODEVALUADOR, codEval);
    	inputs.put(E_C_CODPRODUCTO, codProducto);
    	inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
    	inputs.put(E_C_CODTIPOPRACTICA, cboEvaluacionParcial);
    	
    	
        return super.execute(inputs);
    }
    
    final class AlumnosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CursoEvaluador curso = new CursoEvaluador();
        	
        	curso.setCodSeccion(rs.getString("CODSECCION"));
        	curso.setDscSeccion(rs.getString("DESCRIPCION"));
        	
            return curso;
        }
    }
}