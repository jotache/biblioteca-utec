package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertEmpresa extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION +
		".pkg_eval_oper_admin.sp_ins_formacion_empresa";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; 
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO"; 
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_TIPO_PRACTICA = "E_C_TIPO_PRACTICA";
	private static final String E_V_NOMBRE_EMPRESA = "E_V_NOMBRE_EMPRESA";
	private static final String E_C_FECHA_INICIO = "E_C_FECHA_INICIO";
	private static final String E_C_FECHA_FIN = "E_C_FECHA_FIN";
	private static final String E_C_CANTIDAD_HORAS = "E_C_CANTIDAD_HORAS";
	private static final String E_C_CODESTADO = "E_C_CODESTADO";
	
	private static final String E_V_OBSERVACION = "E_V_OBSERVACION";
	private static final String E_V_NOTA = "E_V_NOTA";
	private static final String E_V_HORA_INI = "E_V_HORA_INI";
	private static final String E_V_HORA_FIN = "E_V_HORA_FIN";
	private static final String E_V_IND_SABADO = "E_V_IND_SABADO";
	private static final String E_V_IND_DOMINGO = "E_V_IND_DOMINGO";
	
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	private static final String S_V_OUT_ID = "S_V_OUT_ID";
	
	public InsertEmpresa(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_PRACTICA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NOMBRE_EMPRESA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_FECHA_INICIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FECHA_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CANTIDAD_HORAS, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODESTADO, OracleTypes.CHAR));
	
	declareParameter(new SqlParameter(E_V_OBSERVACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NOTA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_HORA_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_HORA_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_IND_SABADO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_IND_DOMINGO, OracleTypes.CHAR));
	
	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	//JHPR 2008-06-24 INGRESO MASIVO
	declareParameter(new SqlOutParameter(S_V_OUT_ID, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codPeriodo, String codProducto, String codEspecilidad, String codCurso,
						String codAlumno, String tipoPractica, String nombreEmpresa,String fechaInicio,
						String fechaFin, String CanridadHoras,String codEstado,String observacion,
						String nota,String horaIni,String horaFin,String indSabado,String indDomingo,String usuCrea) {    	
	Map inputs = new HashMap();
	
	/*System.out.println("*** INI " + SPROC_NAME + " ***");
	System.out.println("E_C_CODPERIODO:"+codPeriodo);
	System.out.println("E_C_CODPRODUCTO:"+codProducto);
	System.out.println("E_C_CODESPECIALIDAD:"+codEspecilidad);
	System.out.println("E_C_CODCURSO:"+codCurso);
	System.out.println("E_C_CODALUMNO:"+codAlumno);
	System.out.println("E_C_TIPO_PRACTICA:"+tipoPractica);
	System.out.println("E_V_NOMBRE_EMPRESA:"+nombreEmpresa);
	System.out.println("E_C_FECHA_INICIO:"+fechaInicio);
	System.out.println("E_C_FECHA_FIN:"+fechaFin);
	System.out.println("E_C_CANTIDAD_HORAS:"+CanridadHoras);
	System.out.println("E_C_CODESTADO:"+codEstado);	
	System.out.println("E_V_OBSERVACION"+ observacion);
	System.out.println("E_V_NOTA"+ nota);
	System.out.println("E_V_HORA_INI"+ horaIni);
	System.out.println("E_V_HORA_FIN"+ horaFin);
	System.out.println("E_V_IND_SABADO"+ indSabado);
	System.out.println("E_V_IND_DOMINGO"+ indDomingo);	
	System.out.println("E_V_CODUSUARIO"+ usuCrea);		
	System.out.println("*** FIN " + SPROC_NAME + " ***");*/
	
	inputs.put(E_C_CODPERIODO, codPeriodo);
	inputs.put(E_C_CODPRODUCTO, codProducto);
	inputs.put(E_C_CODESPECIALIDAD, codEspecilidad);
	inputs.put(E_C_CODCURSO, codCurso);
	inputs.put(E_C_CODALUMNO, codAlumno);
	inputs.put(E_C_TIPO_PRACTICA, tipoPractica);
	inputs.put(E_V_NOMBRE_EMPRESA, nombreEmpresa);
	inputs.put(E_C_FECHA_INICIO, fechaInicio);
	inputs.put(E_C_FECHA_FIN, fechaFin);
	inputs.put(E_C_CANTIDAD_HORAS, CanridadHoras);
	inputs.put(E_C_CODESTADO, codEstado);	
	inputs.put(E_V_OBSERVACION, observacion);
	inputs.put(E_V_NOTA, nota);
	inputs.put(E_V_HORA_INI, horaIni);
	inputs.put(E_V_HORA_FIN, horaFin);
	inputs.put(E_V_IND_SABADO, indSabado);
	inputs.put(E_V_IND_DOMINGO, indDomingo);	
	inputs.put(E_V_CODUSUARIO, usuCrea);
	
	
	return super.execute(inputs);
	}

	
}
