package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetParametrosGenerales.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ProgramaComponentes;

public class GetProgramaComponentes extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetProgramaComponentes.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	+ ".pkg_eval_mantto_config.sp_sel_componentes";
		
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";//e_c_codproducto
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";//e_c_codperiodo
	private static final String E_C_CODCICLO = "E_C_CODCICLO";//e_c_codproducto
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	private static final String E_C_CODETAPA = "E_C_CODETAPA";//e_c_codproducto
	private static final String E_C_CODPROGRAMA = "E_C_CODPROGRAMA";
	private static final String RECORDSET = "S_C_RECORDSET";//s_c_recordset TIPO CURSOR
	public GetProgramaComponentes(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODETAPA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODPROGRAMA, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
	
	public Map execute(String CODPRODUCTO, String CODESPECIALIDAD, String CODCICLO, String CODCURSO, 
			String CODETAPA, String CODPROGRAMA) {
				
		log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("E_C_CODPRODUCTO:"+CODPRODUCTO);
    	log.info("E_C_CODESPECIALIDAD:"+CODESPECIALIDAD);
    	log.info("E_C_CODCICLO:"+CODCICLO);
    	log.info("E_C_CODCURSO:"+CODCURSO);
    	log.info("E_C_CODETAPA:"+CODETAPA);
    	log.info("E_C_CODPROGRAMA:"+CODPROGRAMA);
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODPRODUCTO, CODPRODUCTO);
		inputs.put(E_C_CODESPECIALIDAD, CODESPECIALIDAD);
		inputs.put(E_C_CODCICLO, CODCICLO);
		inputs.put(E_C_CODCURSO, CODCURSO);
		inputs.put(E_C_CODETAPA, CODETAPA);
		inputs.put(E_C_CODPROGRAMA, CODPROGRAMA);
		return super.execute(inputs);

	}
final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ProgramaComponentes programaComponentes= new ProgramaComponentes();
        	programaComponentes.setCodCiclo(rs.getString("CODIGO_ID"));
        	programaComponentes.setCodComponente(rs.getString("CODCOMPONENTE"));
        	programaComponentes.setComDescripcion(rs.getString("DESCRIPCIONOMPONENTE"));
        	programaComponentes.setFlag(rs.getString("FLAG"));
        	programaComponentes.setCodEspecialidad(rs.getString("CODESPECIALIDAD"));
        	programaComponentes.setCodProducto(rs.getString("CODPRODUCTO"));
        	programaComponentes.setCodCurso(rs.getString("CODCURSO"));
        	programaComponentes.setCodCiclo(rs.getString("CODCICLO"));
        	  	
        	return programaComponentes;
        	}
    }
}
