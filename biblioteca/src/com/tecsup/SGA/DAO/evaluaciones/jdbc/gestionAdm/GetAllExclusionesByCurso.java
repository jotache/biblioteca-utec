package com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Exclusion;

public class GetAllExclusionesByCurso extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllExclusionesByCurso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
						+ ".PKG_EVAL_OPER_ADMIN.SP_SEL_EXCLUSION_PROMEDIO";
	private static final String CODPERIODO = "E_C_CODPERIODO";
	private static final String CODCURSO = "E_C_CODCURSO";
	private static final String NOMBRE = "E_V_NOMBRE"; 
	private static final String APATERNO = "E_C_APATERNO"; 
	private static final String CODSECCION = "E_C_CODSECCION"; 
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllExclusionesByCurso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(APATERNO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODSECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ExclusionMapper()));
        compile();
    }
    
    public Map execute(String codCurso, String nombre, String apellido, String codSeccion, String codPeriodo) {
    	
    	log.info("**** INI " + SPROC_NAME + " *****");    	
    	log.info("COD_PERIODO:" + codPeriodo);
    	log.info("CODCURSO:" + codCurso);
    	log.info("NOMBRE:" + nombre);
    	log.info("APATERNO:" + apellido);
    	log.info("CODSECCION:" + codSeccion);
    	log.info("**** FIN " + SPROC_NAME + " *****");
    	
    	Map inputs = new HashMap();
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(NOMBRE, nombre);
    	inputs.put(APATERNO, apellido);
    	inputs.put(CODSECCION, codSeccion);
        return super.execute(inputs);
    }
    
    final class ExclusionMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Exclusion exclusion = new Exclusion();
        	
        	exclusion.setCodExclusion(rs.getString("CODIGO_ID") == null ? "" : rs.getString("CODIGO_ID") );
        	exclusion.setCodAlumno(rs.getString("CODALUMNO") == null ? "" : rs.getString("CODALUMNO") );
        	exclusion.setNomAlumno(rs.getString("NOMBREALUMNO") == null ? "" : rs.getString("NOMBREALUMNO") );
        	exclusion.setFlagCurso(rs.getString("FLAGCURSO") == null ? "" : rs.getString("FLAGCURSO") );
        	exclusion.setFlagSemestre(rs.getString("FLAGSEMESTRE") == null ? "" : rs.getString("FLAGSEMESTRE") );
        	
            return exclusion;
        }
    }

}
