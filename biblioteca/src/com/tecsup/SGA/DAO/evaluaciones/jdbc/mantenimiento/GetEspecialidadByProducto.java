package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetParametrosGenerales.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Especialidad;;

public class GetEspecialidadByProducto extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetEspecialidadByProducto.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	+ ".pkg_eval_comun.sp_sel_especialidad_x_prod";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";//e_c_codproducto
	
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetEspecialidadByProducto(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
	public Map execute(String codProducto) {

		log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("E_C_CODPRODUCTO:"+codProducto);    	
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODPRODUCTO, codProducto);
		
		return super.execute(inputs);

	}
  final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Especialidad especialidad= new Especialidad();
        	
        	especialidad.setCodEspecialidad(rs.getString("CODESPECIALIDAD"));//CODPRODUCTO
        	especialidad.setDescripcion(rs.getString("DESCRIPCIONESPECIALIDAD"));
        	return especialidad;
        	
        	}

    }
}
