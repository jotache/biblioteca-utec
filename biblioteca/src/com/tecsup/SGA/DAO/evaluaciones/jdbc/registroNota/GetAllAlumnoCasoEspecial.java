package com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.CursosHibrido;
import com.tecsup.SGA.modelo.Examen;
import com.tecsup.SGA.modelo.SistevalHibrido;

public class GetAllAlumnoCasoEspecial  extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllAlumnoCasoEspecial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_admin.sp_sel_nota_casos_especial";
	
	
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODETAPA = "E_C_CODETAPA";
	private static final String E_C_CODPROGRAMA = "E_C_CODPROGRAMA";
	private static final String E_C_CODCICLO = "E_C_CODCICLO";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	
	private static final String E_C_NOMBRE_ALUMNO = "E_C_NOMBRE_ALUMNO";
	private static final String E_C_APEPATERNO_ALUMNO = "E_C_APEPATERNO_ALUMNO";
	private static final String E_C_APEMATERNO_ALUMNO = "E_C_APEMATERNO_ALUMNO";	
	
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllAlumnoCasoEspecial(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODETAPA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPROGRAMA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NOMBRE_ALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_APEPATERNO_ALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_APEMATERNO_ALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
    public Map execute(String codPeriodo,String codProducto,String codEspecialidad,String codEtapa,String codPrograma,String codCiclo,String codCurso
    		,String nombreAlumno, String apePaternoAlumno, String apeMaternoAlumno ) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("****** INI " + SPROC_NAME + " *****");
    	log.info("E_C_CODPERIODO:"+codPeriodo);
    	log.info("codProducto:"+codProducto);
    	log.info("codEspecialidad:"+codEspecialidad);
    	log.info("E_C_CODETAPA:"+codEtapa);
    	log.info("codPrograma:"+codPrograma);
    	log.info("codCiclo:"+codCiclo);
    	log.info("E_C_CODCURSO:"+codCurso);
    	log.info("E_C_NOMBRE_ALUMNO:"+nombreAlumno);
    	log.info("E_C_APEPATERNO_ALUMNO:"+apePaternoAlumno);
    	log.info("E_C_APEMATERNO_ALUMNO:"+apeMaternoAlumno);
    	log.info("****** FIN " + SPROC_NAME + " *****");
    	
        inputs.put(E_C_CODPERIODO, codPeriodo);        
        inputs.put(E_C_CODPRODUCTO, codProducto);
        inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
        inputs.put(E_C_CODETAPA, codEtapa);
        inputs.put(E_C_CODPROGRAMA, codPrograma);
        inputs.put(E_C_CODCICLO, codCiclo);
        inputs.put(E_C_CODCURSO, codCurso);
        inputs.put(E_C_NOMBRE_ALUMNO, nombreAlumno);
        inputs.put(E_C_APEPATERNO_ALUMNO, apePaternoAlumno);
        inputs.put(E_C_APEMATERNO_ALUMNO, apeMaternoAlumno);
        return super.execute(inputs);

    }

    public Map execute() {    	
        return super.execute(new HashMap());
    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Alumno alumno = new Alumno();
        	
        	alumno.setCodAlumno(rs.getString("CODIGO"));
        	alumno.setNombreAlumno(rs.getString("NOMBREALUMNO"));
        	alumno.setNotaExterna(rs.getString("NOTAEXTERNA"));
        	alumno.setNotaTecsup(rs.getString("NOTATECSUP"));
        	
        	alumno.setCodProducto(rs.getString("CODPRODUCTO"));
        	alumno.setCodEspecialidad(rs.getString("CODESPECIALIDAD"));
        	alumno.setCodEtapa(rs.getString("CODETAPA"));
        	alumno.setCodPrograma(rs.getString("CODPROGRAMA"));
        	alumno.setCodCiclo(rs.getString("CODCICLO"));
        	alumno.setCodCurso(rs.getString("CODCURSO"));
        	
        	//JHPR 2008-05-05 registro en bloque de notas externas
        	alumno.setId(rs.getString("ID"));
        	alumno.setVecesLlevaUnCurso(rs.getString("VEZ"));
        	
        	//FLAGNP
        	alumno.setFlagNp(rs.getString("FLAGNP")==null?"0":rs.getString("FLAGNP"));
        	
        	alumno.setNombreCurso(rs.getString("NOMCURSO")==null?"0":rs.getString("NOMCURSO"));
        	return alumno;
        }
    }
}