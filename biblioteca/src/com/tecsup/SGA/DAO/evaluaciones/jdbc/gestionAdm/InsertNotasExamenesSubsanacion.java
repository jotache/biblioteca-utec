package com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertNotasExamenesSubsanacion extends StoredProcedure {
	private static Log log = LogFactory.getLog(InsertNotasExamenesSubsanacion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	+ ".PKG_EVAL_OPER_ADMIN.SP_INS_EXAMEN_SUBSANACION";

	private static final String CODPERIODO = "E_V_CODPERIODO";
	private static final String CODCURSO = "E_C_CODCURSO";	
	private static final String CADENA_NOTAS = "E_V_CADENANOTAS"; 
	private static final String NROREGISTROS = "E_V_NROREGISTROS"; 
	private static final String CODUSUARIO = "E_V_CODUSUARIO";	
	private static final String RETVAL = "S_V_RETVAL";

	public InsertNotasExamenesSubsanacion(DataSource ds) {
		super(ds, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));        
        declareParameter(new SqlParameter(CADENA_NOTAS, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(NROREGISTROS, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));        
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();	
	}
	
	public Map execute(String codPeriodo, String codCurso, String cadNotas, 
			String nroRegistros, String codUsuario){

		Map inputs = new HashMap();
		    	
		log.info("*** INI "+SPROC_NAME+" ***");	
		log.info("CODPERIODO:" + codPeriodo);
		log.info("CODCURSO:" + codCurso);	
		log.info("CADENA_NOTAS:" + cadNotas);
		log.info("NROREGISTROS:" + nroRegistros);
		log.info("CODUSUARIO:" + codUsuario);		
		log.info("*** FIN "+SPROC_NAME+" ***");	
		
		inputs.put(CODPERIODO, codPeriodo);
		inputs.put(CODCURSO, codCurso);	
		inputs.put(CADENA_NOTAS, cadNotas);
		inputs.put(NROREGISTROS, nroRegistros);
		inputs.put(CODUSUARIO, codUsuario);
		return super.execute(inputs);
	}
	
}
