package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursoConfig;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetCursoConfigById extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetCursoConfigById.class);
    private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
    										+ ".pkg_eval_mantto_config.sp_sel_nota_externa_x_prog";
    private static final String CODCURSO = "E_C_CODCURSO";    
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetCursoConfigById(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.VARCHAR));        
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codigo) {
    	
    	log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("CODCURSO:"+codigo);    	
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(CODCURSO, codigo);        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CursoConfig cursoConfig = new CursoConfig();
        	
        	cursoConfig.setCodigo(rs.getString("CODIGO"));
        	cursoConfig.setCodCurso(rs.getString("CODPROGRAMA"));
        	cursoConfig.setRegla(rs.getString("CODREGLA"));
        	cursoConfig.setCondicion(rs.getString("CONDICION"));
        	cursoConfig.setVerdadero01(rs.getString("VERDADERO1"));
        	cursoConfig.setVerdadero02(rs.getString("VERDADERO2"));
        	cursoConfig.setVerdadero03(rs.getString("VERDADERO3"));
        	cursoConfig.setVerdadero04(rs.getString("VERDADERO4"));
        	cursoConfig.setFalso01(rs.getString("FALSO1"));
        	cursoConfig.setFalso02(rs.getString("FALSO2"));
        	cursoConfig.setFalso03(rs.getString("FALSO3"));
        	cursoConfig.setFalso04(rs.getString("FALSO4"));
        	cursoConfig.setOpeVer01(rs.getString("CODOPERVERDAD1"));
        	cursoConfig.setOpeVer02(rs.getString("CODOPERVERDAD2"));
        	cursoConfig.setOpeVer03(rs.getString("CODOPERVERDAD3"));
        	cursoConfig.setOpeVer04(rs.getString("CODOPERVERDAD4"));
        	cursoConfig.setOpeFal01(rs.getString("CODOPERFALS1"));
        	cursoConfig.setOpeFal02(rs.getString("CODOPERFALS2"));
        	cursoConfig.setOpeFal03(rs.getString("CODOPERFALS3"));
        	cursoConfig.setOpeFal04(rs.getString("CODOPERFALS4"));        	
        	
            return cursoConfig;
        }

    }
}


