package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class Insert_UpdateParametrosGenerales extends StoredProcedure{
	private static Log log = LogFactory.getLog(Insert_UpdateParametrosGenerales.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
 	+ ".pkg_eval_mantto_config.sp_ins_parametros_grales";
	
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODTABLA = "E_C_CODTABLA";
	private static final String E_V_VALOR = "E_V_VALOR";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public Insert_UpdateParametrosGenerales(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CODTABLA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_VALOR, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codProducto, String codPeriodo, String codTabla
	, String valor, String codUsuario) {
	
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("E_C_CODPRODUCTO: "+codProducto);
		log.info("E_C_CODPERIODO: "+codPeriodo);
		log.info("E_C_CODTABLA: "+codTabla);
		log.info("E_V_VALOR: "+valor);
		log.info("E_V_CODUSUARIO: "+codUsuario);
		log.info("*** FIN " + SPROC_NAME + " ****");
		
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODPRODUCTO, codProducto);
	inputs.put(E_C_CODPERIODO, codPeriodo);
	inputs.put(E_C_CODTABLA, codTabla);
	inputs.put(E_V_VALOR, valor);
	inputs.put(E_V_CODUSUARIO, codUsuario);

	return super.execute(inputs);
	
	}
}
