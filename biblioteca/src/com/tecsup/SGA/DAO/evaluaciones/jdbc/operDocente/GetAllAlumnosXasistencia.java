package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Asistencia;


public class GetAllAlumnosXasistencia extends StoredProcedure{
	
	private static Log log = LogFactory.getLog(GetAllAlumnosXasistencia.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + 
											".pkg_eval_oper_docente.sp_sel_alumnos_x_asistencia";
	private static final String COD_PERIODO = "E_V_CODPERIODO";
	private static final String ASIS_COD = "E_V_CODASISTENCIA";
	private static final String NOMBRE = "E_V_NOMBRE";
	private static final String APELLIDO = "E_V_APELLIDO";
	private static final String NRO_SESION = "E_V_NROSESION";
	private static final String COD_CURSO = "E_V_CODCURSO";
	private static final String CODPRODUCTO = "E_V_CODPRODUCTO";
	private static final String CODESPECIALIDAD = "E_V_CODESPECIALIDAD";
	private static final String TIPOSESION = "E_V_TIPOSESION";
	private static final String COD_SECCION = "E_V_CODSECCION";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllAlumnosXasistencia(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ASIS_COD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(APELLIDO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(NRO_SESION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(COD_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODPRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TIPOSESION, OracleTypes.VARCHAR));  
        declareParameter(new SqlParameter(COD_SECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AlumnosAsistenciaMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codAsistencia, String nombre, String apellido, String nroSesion, String codCurso,
    				String codProducto, String codEspecialidad, String tipoSesion, 
    				String codSeccion) {
    	
    	log.info("SPROC_NAME: "+SPROC_NAME);
    	log.info("COD_PERIODO:" +codPeriodo);
    	log.info("ASIS_COD:" +codAsistencia);
    	log.info("NOMBRE:" +nombre);
    	log.info("APELLIDO:" +apellido);
    	log.info("NRO_SESION:" +nroSesion);
    	log.info("COD_CURSO:" +codCurso);
    	log.info("CODPRODUCTO:" +codProducto);
    	log.info("CODESPECIALIDAD:" +codEspecialidad);
    	log.info("TIPOSESION:" +tipoSesion);
    	log.info("COD_SECCION:" +codSeccion);
    	log.info("---");
    	
    	Map inputs = new HashMap();
    	inputs.put(COD_PERIODO, codPeriodo);
    	inputs.put(ASIS_COD, codAsistencia);
    	inputs.put(NOMBRE, nombre);
    	inputs.put(APELLIDO, apellido);
    	inputs.put(NRO_SESION, nroSesion);
    	inputs.put(COD_CURSO, codCurso);
    	
    	inputs.put(CODPRODUCTO, codProducto);
    	inputs.put(CODESPECIALIDAD, codEspecialidad);
    	inputs.put(TIPOSESION, tipoSesion);
    	
    	inputs.put(COD_SECCION, codSeccion);
        return super.execute(inputs);
    }
    
    final class AlumnosAsistenciaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Asistencia asistencia = new Asistencia();
        	
        	asistencia.setCodAsistencia(rs.getString("CODASISTENCIA"));
        	asistencia.setCodAlumno(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
        	asistencia.setNombre(rs.getString("NOMBRE")== null ? "" : rs.getString("NOMBRE"));
        	asistencia.setTipoAsistencia(rs.getString("TIPOASIST")== null ? "" : rs.getString("TIPOASIST"));
        	asistencia.setFlagSancion(rs.getString("SANCION_ADM")== null ? "0" : rs.getString("SANCION_ADM"));
        	asistencia.setCodSeccion(rs.getString("codseccion")== null ? "0" : rs.getString("codseccion"));
        	
            return asistencia;
        }
    }
}