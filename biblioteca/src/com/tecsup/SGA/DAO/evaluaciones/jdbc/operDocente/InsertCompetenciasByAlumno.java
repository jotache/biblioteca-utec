package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertCompetenciasByAlumno extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
								+ ".PKG_EVAL_OPER_DOCENTE.SP_INS_COMPETENCIAS_X_ALUMNO";
	private static final String CODALUMNO = "E_C_CODALUMNO"; 
	private static final String CODCURSO = "E_C_CODCURSO"; 
	private static final String CODPERIODO = "E_C_CODPERIODO";
	private static final String TIPO_OPE = "E_C_TIPO_OPE";
	private static final String NRO_COMP = "E_V_NRO_COMP";
	private static final String DATOS_COMP = "E_C_DATOS_COMP";
	private static final String CODUSUARIO = "E_V_CODUSUARIO";
	private static final String CODTIPOSESION = "E_C_CODTIPOSESION";
	private static final String CODSECCION = "E_V_CODSECCION";
	private static final String RETVAL = "S_V_RETVAL";
	
	public InsertCompetenciasByAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPO_OPE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(NRO_COMP, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(DATOS_COMP, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODTIPOSESION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String codAlumno,String codCurso,String codPeriodo,String tipoOpe,
    		String nroComp,String datosComp,String codUsuario, String codSeccion
    		, String codTipoSesion) {    	
    	Map inputs = new HashMap();
    	
    	inputs.put(CODALUMNO, codAlumno);
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(TIPO_OPE, tipoOpe);
    	inputs.put(NRO_COMP, nroComp);
    	inputs.put(DATOS_COMP, datosComp);
    	inputs.put(CODUSUARIO, codUsuario);
    	inputs.put(CODTIPOSESION, codTipoSesion);
    	inputs.put(CODSECCION, codSeccion);

    	
        return super.execute(inputs);
    }
	
}
