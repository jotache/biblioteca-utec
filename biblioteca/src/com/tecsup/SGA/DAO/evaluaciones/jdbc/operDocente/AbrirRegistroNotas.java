package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class AbrirRegistroNotas extends StoredProcedure {
	private static Log log = LogFactory.getLog(AbrirRegistroNotas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION +
														".PKG_EVAL_OPER_DOCENTE.SP_ACT_ABRIR_NOTAS";
	private static final String E_C_CODCURSO = "E_C_CODCURSO"; 
	private static final String E_C_CODSECCION = "E_C_CODSECCION"; 
	private static final String E_C_CODTIPOEVALUACION = "E_C_CODTIPOEVALUACION";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public AbrirRegistroNotas(DataSource dataSource){
		super(dataSource,SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODTIPOEVALUACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario)  {
		
		log.info("**** INI " + SPROC_NAME + "****");
		log.info("E_C_CODCURSO: " + codCurso);
		log.info("E_C_CODSECCION: " + codSeccion);
		log.info("E_C_CODTIPOEVALUACION: " + tipoEvaluacion);
		log.info("E_V_CODUSUARIO: " + codUsuario);
		log.info("**** FIN " + SPROC_NAME + "****");
		
		Map inputs = new HashMap();
		inputs.put(E_C_CODCURSO, codCurso);
		inputs.put(E_C_CODSECCION, codSeccion);
		inputs.put(E_C_CODTIPOEVALUACION, tipoEvaluacion);
		inputs.put(E_V_CODUSUARIO, codUsuario);		
		return super.execute(inputs);
	}
}
