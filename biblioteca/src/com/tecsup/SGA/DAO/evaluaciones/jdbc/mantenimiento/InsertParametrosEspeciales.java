package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertParametrosEspeciales extends StoredProcedure {
	private static Log log = LogFactory.getLog(InsertParametrosEspeciales.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION+ ".PKG_EVAL_MANTTO_CONFIG.SP_INS_PARAMETROS_ESPECIALES";
	
	private static final String E_C_CODPERIODO 			= "E_C_CODPERIODO";
	private static final String E_C_CADVALORESFORM 		= "E_C_CADVALORESFORM";
	private static final String E_C_CADVALORESCAT 		= "E_C_CADVALORESCAT";
	private static final String E_C_CADVALORESESCALA 	= "E_C_CADVALORESESCALA";
	private static final String E_V_NROREGISTROSCAT 	= "E_V_NROREGISTROSCAT";
	private static final String E_V_CODUSUARIO 			= "E_V_CODUSUARIO";
	private static final String S_V_RETVAL 				= "S_V_RETVAL";

public InsertParametrosEspeciales(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CADVALORESFORM, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CADVALORESCAT, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CADVALORESESCALA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NROREGISTROSCAT, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
}

public Map execute(String codPeriodo,String cadValoresForm,String cadValoresCat
		,String  cadValoresEscala,String nroRegistrosCat,String codUsuario) {

	log.info("*** INI " + SPROC_NAME + " ****");
	log.info("E_C_CODPERIODO: "+codPeriodo);
	log.info("E_C_CADVALORESFORM: "+cadValoresForm);
	log.info("E_C_CADVALORESCAT: "+cadValoresCat);	
	log.info("E_C_CADVALORESESCALA: "+cadValoresEscala);
	log.info("E_V_NROREGISTROSCAT: "+nroRegistrosCat);
	log.info("E_V_CODUSUARIO: "+codUsuario);
	log.info("*** FIN " + SPROC_NAME + " ****");
	
	Map inputs = new HashMap();
	inputs.put(E_C_CODPERIODO, codPeriodo);
	inputs.put(E_C_CADVALORESFORM, cadValoresForm);
	inputs.put(E_C_CADVALORESCAT, cadValoresCat);
	inputs.put(E_C_CADVALORESESCALA, cadValoresEscala);
	inputs.put(E_V_NROREGISTROSCAT, nroRegistrosCat);
	inputs.put(E_V_CODUSUARIO, codUsuario);	

	return super.execute(inputs);

}
	
}
