package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursoEvaluador;

public class GetAllSeccionesxCursoxResp extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
		+ ".pkg_eval_oper_docente.SP_SEL_SECCIONES_BY_EVAL";
	private static final String E_C_COD_EVALUADOR = "E_C_COD_EVALUADOR";
	private static final String E_C_COD_CURSO = "E_C_COD_CURSO";
	private static final String E_C_COD_TIPO_SESION = "E_C_COD_TIPO_SESION";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    private static Log log = LogFactory.getLog(GetAllSeccionesxCursoxResp.class);
    
    public GetAllSeccionesxCursoxResp(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_EVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_CURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_TIPO_SESION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new SeccionMapper()));
        compile();
    }

    public Map execute(String codResponsable, String codCurso, String codTipoSesion) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("*********** "+SPROC_NAME+" **************");
    	log.info("E_C_COD_EVALUADOR:"+codResponsable);
    	log.info("E_C_COD_CURSO:"+codCurso);
    	log.info("E_C_COD_TIPO_SESION:"+codTipoSesion);
    	log.info("*********** FIN SP_SEL_SECCIONES_BY_EVAL*************");
    	
    	inputs.put(E_C_COD_EVALUADOR, codResponsable);
    	inputs.put(E_C_COD_CURSO, codCurso);
    	inputs.put(E_C_COD_TIPO_SESION, codTipoSesion);
    	
        return super.execute(inputs);
    }
    
    final class SeccionMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CursoEvaluador curso = new CursoEvaluador();
        	
        	curso.setCodSeccion(rs.getString("COD_SECCION"));
        	curso.setDscSeccion(rs.getString("DSC_SECCION"));
        	
            return curso;
        }
    }
}