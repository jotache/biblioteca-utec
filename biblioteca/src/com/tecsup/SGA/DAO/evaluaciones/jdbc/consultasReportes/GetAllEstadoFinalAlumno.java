package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reportes;
import com.tecsup.SGA.common.CommonConstants;

public class GetAllEstadoFinalAlumno extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllEstadoFinalAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_reportes.sp_sel_rep_estado_final_alumno";
	
	private static final String E_C_COD_PERIODO = "E_C_COD_PERIODO";
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_COD_PRODUCTO = "E_C_COD_PRODUCTO";
	private static final String E_C_COD_ESPECIALIDAD = "E_C_COD_ESPECIALIDAD";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllEstadoFinalAlumno(DataSource dataSource){
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_ESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new InformacionGeneralMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("**** INI "  + SPROC_NAME + " ****");
    	log.info("E_C_COD_PERIODO.- "+codPeriodo);
    	log.info("E_C_SEDE.- "+codSede);
    	log.info("E_C_COD_PRODUCTO.- "+codProducto);
    	log.info("E_C_COD_ESPECIALIDAD.- "+codEspecialidad);
    	log.info("**** FIN "  + SPROC_NAME + " ****");
    	
    	inputs.put(E_C_COD_PERIODO, codPeriodo);
    	inputs.put(E_C_SEDE, codSede);
    	inputs.put(E_C_COD_PRODUCTO, codProducto);
    	inputs.put(E_C_COD_ESPECIALIDAD, codEspecialidad);
        return super.execute(inputs);
    }
    
    final class InformacionGeneralMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {   
        	
        	Reportes reportes = new Reportes();
        	
        	reportes.setDscPeriodo(rs.getString("DSC_PERIODO")== null ? "" : rs.getString("DSC_PERIODO"));
        	reportes.setCodAlumno(rs.getString("COD_ALUMNO")== null ? "" : rs.getString("COD_ALUMNO"));
        	reportes.setNivelAlumno(rs.getString("NIVEL_ALUMNO")== null ? "" : rs.getString("NIVEL_ALUMNO"));
        	reportes.setEspecialidadCurso(rs.getString("ESPECIALIDAD_ALUMNO")== null ? "" : rs.getString("ESPECIALIDAD_ALUMNO"));
        	reportes.setNomAlumno(rs.getString("NMB_ALUMNO")== null ? "" : rs.getString("NMB_ALUMNO"));
        	reportes.setSecciones(rs.getString("SECCIONES")== null ? "" : rs.getString("SECCIONES"));
        	reportes.setNroVez(rs.getString("NRO_CURSOS")== null ? "" : rs.getString("NRO_CURSOS"));
        	reportes.setNroExamCargo(rs.getString("NRO_EXAM_CARGO")== null ? "" : rs.getString("NRO_EXAM_CARGO"));
        	reportes.setPromSemestral(rs.getString("PROM_SEMESTRAL")== null ? "" : rs.getString("PROM_SEMESTRAL"));
        	reportes.setPromAcumulado(rs.getString("PROM_ACUMULADO")== null ? "" : rs.getString("PROM_ACUMULADO"));
        	reportes.setIndRecuperacion(rs.getString("IND_RECUPERACION")== null ? "" : rs.getString("IND_RECUPERACION"));
        	reportes.setOrdMerito(rs.getString("ORDEN_MERITO")== null ? "" : rs.getString("ORDEN_MERITO"));
        	reportes.setIndQuinto(rs.getString("IND_QUINTO")== null ? "" : rs.getString("IND_QUINTO"));
        	reportes.setEstInicial(rs.getString("DSC_EST_INICIAL")== null ? "" : rs.getString("DSC_EST_INICIAL"));
        	reportes.setEstFinal(rs.getString("DSC_EST_FINAL")== null ? "" : rs.getString("DSC_EST_FINAL"));
        	reportes.setPce(rs.getString("PCE")== null ? "" : rs.getString("PCE"));
        	reportes.setCursosCargo(rs.getString("CURSOS_CARGO")== null ? "" : rs.getString("CURSOS_CARGO"));
   
        	return reportes;
        }
    }
}
