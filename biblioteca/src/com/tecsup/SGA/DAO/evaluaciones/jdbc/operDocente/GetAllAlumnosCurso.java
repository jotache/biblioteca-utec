package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Asistencia;
//import com.tecsup.SGA.modelo.CursoEvaluador;


public class GetAllAlumnosCurso extends StoredProcedure{
	
	private static Log log = LogFactory.getLog(GetAllAlumnosCurso.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_sel_asistencia";
	private static final String COD_PERIODO = "E_V_CODPERIODO"; //CODIGO CURSO
	private static final String COD_CURSO = "E_V_CODCURSO"; //CODIGO CURSO
	
	private static final String COD_PRODUCTO = "E_V_CODPRODUCTO"; //CODIGO PRODUCTO
	private static final String COD_ESPECIALIDAD = "E_V_CODESPECIALIDAD"; //CODIGO ESPECIALIDAD
	private static final String TIPO_SESION = "E_V_TIPOSESION"; //TIPO SESION
	
	private static final String COD_SECCION = "E_V_CODSECCION"; //CODIGO SECCION
	private static final String NOMBRE = "E_V_NOMBRE"; //NOMBRE
	private static final String APELLIDO_PATERNO = "E_V_APELLIDO"; //APELLIDO PATERNO
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllAlumnosCurso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(COD_CURSO, OracleTypes.VARCHAR));
        
        declareParameter(new SqlParameter(COD_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(COD_ESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TIPO_SESION, OracleTypes.VARCHAR));
        
        declareParameter(new SqlParameter(COD_SECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(APELLIDO_PATERNO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AlumnosMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codCurso, 
    					String codProducto, String codEspecialidad, String tipoSesion, 
    					String codSeccion, String nombre, String apellido) {
    	
    	log.info("SPROC_NAME: "+SPROC_NAME);
    	log.info("COD_PERIODO:" +codPeriodo);
    	log.info("COD_CURSO:" +codCurso);
    	log.info("COD_PRODUCTO:" +codProducto);
    	log.info("COD_ESPECIALIDAD:" +codEspecialidad);
    	log.info("TIPO_SESION:" +tipoSesion);
    	log.info("COD_SECCION:" +codSeccion);
    	log.info("NOMBRE:" +nombre);
    	log.info("APELLIDO_PATERNO:" +apellido);
    	log.info("---");
    	
    	Map inputs = new HashMap();
    	inputs.put(COD_PERIODO, codPeriodo);
    	inputs.put(COD_CURSO, codCurso);
    	
    	inputs.put(COD_PRODUCTO, codProducto);
    	inputs.put(COD_ESPECIALIDAD, codEspecialidad);
    	inputs.put(TIPO_SESION, tipoSesion);
    	
    	inputs.put(COD_SECCION, codSeccion);
    	inputs.put(NOMBRE, nombre);
    	inputs.put(APELLIDO_PATERNO, apellido);
        return super.execute(inputs);
    }
    
    final class AlumnosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	//CursoEvaluador curso = new CursoEvaluador();
        	Asistencia asistencia = new Asistencia();
        	
        	asistencia.setCodAlumno(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
        	asistencia.setNombre(rs.getString("NOMBRE")== null ? "" : rs.getString("NOMBRE"));
        	asistencia.setNroFaltas(rs.getString("FALTAS") == null ? "--" : rs.getString("FALTAS"));
        	asistencia.setPorcentaje(rs.getString("PORCENTAJE") == null ? "--" : rs.getString("PORCENTAJE"));        	
        	asistencia.setFlagSancion(rs.getString("SANCION_ADM")== null ? "0" : rs.getString("SANCION_ADM"));
        	asistencia.setCodSeccion(rs.getString("codSeccion")== null ? "0" : rs.getString("codSeccion"));
        	
            return asistencia;
        }
    }
}