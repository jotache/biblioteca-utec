package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertProgramaComponentes extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
 	+ ".pkg_eval_mantto_config.sp_ins_componentes";
	
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODCICLO = "E_C_CODCICLO";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	private static final String E_C_CODETAPA = "E_C_CODETAPA";
	private static final String E_C_CODPROGRAMA = "E_C_CODPROGRAMA";
	private static final String E_C_CADCOMPONENTE = "E_C_CADCOMPONENTE";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertProgramaComponentes(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODETAPA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODPROGRAMA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CADCOMPONENTE, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
		
		public Map execute(String codProducto, String CODESPECIALIDAD, String CODCICLO, String CODCURSO,
		String CODETAPA, String CODPROGRAMA, String CADCOMPONENTE, String NROREGISTROS, String CODUSUARIO) {
						
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODPRODUCTO, codProducto);
		inputs.put(E_C_CODESPECIALIDAD, CODESPECIALIDAD);
		inputs.put(E_C_CODCICLO, CODCICLO);
		inputs.put(E_C_CODCURSO, CODCURSO);
		inputs.put(E_C_CODETAPA, CODETAPA);
		inputs.put(E_C_CODPROGRAMA, CODPROGRAMA);
		inputs.put(E_C_CADCOMPONENTE, CADCOMPONENTE);
		inputs.put(E_V_NROREGISTROS, NROREGISTROS);
		inputs.put(E_C_CODUSUARIO, CODUSUARIO);
		return super.execute(inputs);
		
		}
}
