package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Reportes;

public class GetAllRepestExamenes extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAllRepestExamenes.class);
		private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
		+ ".pkg_eval_reportes.sp_sel_rep_est_examenes";
	private static final String E_C_COD_PERIODO = "E_C_COD_PERIODO";
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_COD_PRODUCTO = "E_C_COD_PRODUCTO";
	private static final String E_C_COD_ESPECIALIDAD = "E_C_COD_ESPECIALIDAD";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
	
	public GetAllRepestExamenes(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_PERIODO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_PRODUCTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_ESPECIALIDAD, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR,
			new DetallePruebaLabMapper()));
	compile();
	}
	
	public Map execute(String codPeriodo,String sede,String codProducto,String codEspe) {
	
	Map inputs = new HashMap();
	
	log.info("*** INI " + SPROC_NAME + "***");
	log.info("E_C_COD_PERIODO: "+codPeriodo);
	log.info("E_C_SEDE: "+sede);
	log.info("E_C_COD_PRODUCTO: "+codProducto);
	log.info("E_C_COD_ESPECIALIDAD: "+codEspe);
	log.info("*** FIN " + SPROC_NAME + "***");
	
	inputs.put(E_C_COD_PERIODO, codPeriodo);	
	inputs.put(E_C_SEDE, sede);
	inputs.put(E_C_COD_PRODUCTO, codProducto);
	inputs.put(E_C_COD_ESPECIALIDAD, codEspe);
	return super.execute(inputs);
	}
	
	final class DetallePruebaLabMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		Reportes reportes = new Reportes();
	
		reportes.setFecha(rs.getString("FECHA")== null ? "" : rs.getString("FECHA"));
		reportes.setDscPeriodo(rs.getString("DSC_PERIODO")== null ? "" : rs.getString("DSC_PERIODO"));
		reportes.setCodCiclo(rs.getString("COD_CICLO")== null ? "" : rs.getString("COD_CICLO"));
		reportes.setEspecialidadCurso(rs.getString("ESPECIALIDAD")== null ? "" : rs.getString("ESPECIALIDAD"));
		reportes.setCurso(rs.getString("DSC_CURSO")== null ? "" : rs.getString("DSC_CURSO"));
		reportes.setSecciones(rs.getString("DSC_SECCION")== null ? "" : rs.getString("DSC_SECCION"));
		reportes.setCodSeccion(rs.getString("COD_SECCION")== null ? "" : rs.getString("COD_SECCION"));
		reportes.setCodCurso(rs.getString("COD_CURSO")== null ? "" : rs.getString("COD_CURSO"));
		reportes.setTipo(rs.getString("TIPO_EXAMEN")== null ? "" : rs.getString("TIPO_EXAMEN"));
		reportes.setAprobados(rs.getString("APROBADOS")== null ? "" : rs.getString("APROBADOS"));
		reportes.setDesaprobados(rs.getString("DESAPROBADOS")== null ? "" : rs.getString("DESAPROBADOS"));
		reportes.setAnulados(rs.getString("ANULADOS")== null ? "" : rs.getString("ANULADOS"));
		reportes.setNroPresentados(rs.getString("NO_PRESENTADOS")== null ? "" : rs.getString("NO_PRESENTADOS"));
		reportes.setMatriculados(rs.getString("MATRICULADOS")== null ? "" : rs.getString("MATRICULADOS"));
		reportes.setPromedio(rs.getString("PROMEDIO")== null ? "" : rs.getString("PROMEDIO"));
		reportes.setDscEvaluadorTeo(rs.getString("PROFESOR")== null ? "" : rs.getString("PROFESOR"));
			
		return reportes;
	}
	}
}
