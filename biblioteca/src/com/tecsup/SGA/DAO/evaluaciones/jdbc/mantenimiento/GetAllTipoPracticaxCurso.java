package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CiclosCat;
import com.tecsup.SGA.modelo.TipoPracticas;

public class GetAllTipoPracticaxCurso extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAllTipoPracticaxCurso.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
     + ".pkg_eval_comun.sp_sel_tipo_practicas_x_curso";
	 
	 private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	 private static final String E_C_CODCURSO = "E_C_CODCURSO";
	 private static final String S_C_RECORDSET = "S_C_RECORDSET";

public GetAllTipoPracticaxCurso(DataSource dataSource) {
	super(dataSource, SPROC_NAME);        
	declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new TipoPracticasMapper()));
	compile();
}

public Map execute(String codPeriodo, String codCurso) {

	log.info("*** INI " + SPROC_NAME + " ****");
	log.info("E_C_CODPERIODO:" + codPeriodo);
	log.info("E_C_CODCURSO:" + codCurso);
	log.info("*** FIN " + SPROC_NAME + " ****");
	
	Map inputs = new HashMap();
	inputs.put(E_C_CODPERIODO, codPeriodo);
	inputs.put(E_C_CODCURSO, codCurso);
	return super.execute(inputs);
}

	final class TipoPracticasMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			TipoPracticas tipoPracticas = new TipoPracticas();
			tipoPracticas.setCodigo(rs.getString("CODTIPOEVALUACION"));
			tipoPracticas.setDescripcion(rs.getString("DESTIPOPRACTICAS"));
			return tipoPracticas;
		}

	}



}
