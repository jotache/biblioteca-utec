package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Asistencia;

/*
	PROCEDURE SP_SEL_FECHA_ASIST_CURSO(
		E_V_CODPERIODO IN VARCHAR2,
		E_C_CODCURSO IN VARCHAR2,
		E_V_CODPRODUCTO IN VARCHAR2,
		E_V_CODESPECIALIDAD IN VARCHAR2,
		E_V_TIPOSESION IN VARCHAR2,
		E_C_CODSECCION IN VARCHAR2,
		S_C_RECORDSET IN OUT CUR_RECORDSET
	);
*/

public class GetAllFechaAsistenciaCurso extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + 
											".pkg_eval_oper_docente.sp_sel_fecha_asist_curso";
	private static final String COD_PERIODO = "E_V_CODPERIODO";
	private static final String COD_CURSO = "E_C_CODCURSO";
	
	private static final String COD_PRODUCTO = "E_V_CODPRODUCTO";
	private static final String COD_ESPECIALIDAD = "E_V_CODESPECIALIDAD";
	private static final String TIPO_SESION = "E_V_TIPOSESION";
	
	private static final String COD_SECCION = "E_C_CODSECCION";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllFechaAsistenciaCurso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(COD_CURSO, OracleTypes.VARCHAR));
        
        declareParameter(new SqlParameter(COD_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(COD_ESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TIPO_SESION, OracleTypes.VARCHAR));
        
        declareParameter(new SqlParameter(COD_SECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new FechasMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codCurso, 
    		String codProducto, String codEspecialidad, String tipoSesion, 
    		String codSeccion) {
    	
    	Map inputs = new HashMap();
    	inputs.put(COD_PERIODO, codPeriodo);
    	inputs.put(COD_CURSO, codCurso);
    	
    	inputs.put(COD_PRODUCTO, codProducto);
    	inputs.put(COD_ESPECIALIDAD, codEspecialidad);
    	inputs.put(TIPO_SESION, tipoSesion);
    	
    	inputs.put(COD_SECCION, codSeccion);
        return super.execute(inputs);
    }
    
    final class FechasMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Asistencia asistencia = new Asistencia();
        	
        	asistencia.setNroSesion(rs.getString("NROSESION")== null ? "" : rs.getString("NROSESION") );
        	asistencia.setFecha(rs.getString("FECHASESION")== null ? "" : rs.getString("FECHASESION") );
        	
        	asistencia.setFlgProg(rs.getString("FLG_PROGRAMACION")== null ? "0" : rs.getString("FLG_PROGRAMACION") );
        	
        	asistencia.setTipoSesion(rs.getString("TIPOSESION")== null ? "" : rs.getString("TIPOSESION") );
        	asistencia.setCodCurso(rs.getString("CODCURSOEJEC")== null ? "" : rs.getString("CODCURSOEJEC") );
        	asistencia.setCodSeccion(rs.getString("CODSECCION")== null ? "" : rs.getString("CODSECCION") );
        	
            return asistencia;
        }
    }
}