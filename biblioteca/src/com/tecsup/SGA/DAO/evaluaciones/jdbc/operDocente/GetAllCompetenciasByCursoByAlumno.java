package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CompetenciasPerfil;


public class GetAllCompetenciasByCursoByAlumno extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
								+ ".PKG_EVAL_OPER_DOCENTE.SP_SEL_COMPETENCIAS_X_ALUMNO";
	private static final String CODPERIODO = "E_C_CODPERIODO"; 
	private static final String CODCURSO = "E_C_CODCURSO"; 
	private static final String CODSECCION = "E_C_CODSECCION"; 
	private static final String CODALUMNO = "E_C_CODALUMNO";
	private static final String TIPO_OPE = "E_C_TIPO_OPE";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllCompetenciasByCursoByAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPO_OPE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new EvalCompetenciaMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codCurso, String codSeccion, String codAlumno, String tipoOpe) {
    	
    	Map inputs = new HashMap();
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(CODSECCION, codSeccion);
    	inputs.put(CODALUMNO, codAlumno);
    	inputs.put(TIPO_OPE, tipoOpe);
        return super.execute(inputs);
    }
    final class EvalCompetenciaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CompetenciasPerfil evalComp = new CompetenciasPerfil();
        	
        	evalComp.setCodAlumno(rs.getString("CODALUMNO") == null ? "" : rs.getString("CODALUMNO"));
        	evalComp.setCodCompetencia(rs.getString("COD_COMPETENCIA") == null ? "" : rs.getString("COD_COMPETENCIA"));
        	evalComp.setDscCompetencia(rs.getString("DSC_COMPETENCIA") == null ? "" : rs.getString("DSC_COMPETENCIA"));
        	evalComp.setNotaIni(rs.getString("NOTA_INI") == null ? "" : rs.getString("NOTA_INI"));
        	evalComp.setCodEvalIni(rs.getString("COD_EVAL_INI") == null ? "" : rs.getString("COD_EVAL_INI"));
        	evalComp.setNotaFin(rs.getString("NOTA_FIN") == null ? "" : rs.getString("NOTA_FIN"));
        	evalComp.setCodEvalFin(rs.getString("COD_EVAL_FIN") == null ? "" : rs.getString("COD_EVAL_FIN"));
        	
            return evalComp;
        }
    }
}
