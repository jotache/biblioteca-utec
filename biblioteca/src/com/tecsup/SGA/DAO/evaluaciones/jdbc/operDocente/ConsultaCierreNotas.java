//JHPR 2008-06-09
package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCursosEvaluador.CursosMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DefNroEvalParciales;

public class ConsultaCierreNotas extends StoredProcedure {

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_REPORTES.SP_SEL_CIERRENOTAS";
	private static final String PERIODO = "E_C_COD_PERIODO"; //CODIGO PERIODO
	private static final String SEDE = "E_C_SEDE"; //CODIGO EVALUADOR
	private static final String PRODUCTO = "E_C_COD_PRODUCTO"; //CODIGO PRODCUTO
	private static final String ESPECIALIDAD = "E_C_COD_ESPECIALIDAD"; //CODIGO ESPECIALIDAD		
    private static final String RECORDSET = "S_C_RECORDSET";

    public ConsultaCierreNotas(DataSource dataSource) {
    	super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ESPECIALIDAD, OracleTypes.VARCHAR));        
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new DefMapper()));
        //declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();    	
    }
    
    public Map execute(String sede,String codPeriodo,String codProducto,String codEspecialida) {
    	Map inputs = new HashMap();
    	
    	System.out.println("********ConsultaCierreNotas*********");
    	System.out.println("codPeriodo:"+codPeriodo);
    	System.out.println("sede:"+sede);
    	System.out.println("codProducto:"+codProducto);
    	System.out.println("codEspecialida:"+codEspecialida);
    	
    	inputs.put(PERIODO, codPeriodo);
    	inputs.put(SEDE, sede);
    	inputs.put(PRODUCTO, codProducto);
    	inputs.put(ESPECIALIDAD, codEspecialida);
        return super.execute(inputs);
    }
    
    final class DefMapper implements RowMapper {
    	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    		DefNroEvalParciales def = new DefNroEvalParciales();
    		
    		def.setNomEspecialidad(rs.getString("ESPECIALIDAD"));
    		def.setCiclo(rs.getString("CICLO"));
    		def.setNomCurso(rs.getString("CURSO"));
    		def.setNomSeccion(rs.getString("SECCION"));    		
    		def.setNomResponsable(rs.getString("RESPONSABLE"));    
    		def.setNomDptoResponsable(rs.getString("DEPARTAMENTO"));
    		def.setNomTipoEvaluacion(rs.getString("TIPOEVALUA"));    		
    		if (rs.getString("ESTADO").equals("1")) {
    			def.setEstadoCerrado("Cerrado");
    		}else {
    			def.setEstadoCerrado("Abierto");
    		}		
    		
    		def.setNroPractDef(rs.getString("NRO_PRAC_DEF")==null?"":rs.getString("NRO_PRAC_DEF"));
    		def.setNroPractEje(rs.getString("NRO_PRAC_ING")==null?"":rs.getString("NRO_PRAC_ING"));
    		def.setNroAsistenc(rs.getString("NRO_ASISTS")==null?"":rs.getString("NRO_ASISTS"));
    		def.setEstadoAsist(rs.getString("estado_asist")==null?"":rs.getString("estado_asist"));
    		
    		return def;
    	}
    }
    
}
