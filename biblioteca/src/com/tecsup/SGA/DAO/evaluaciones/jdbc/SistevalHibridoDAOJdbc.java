package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.List;
import java.util.HashMap;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.*;//donde se llama al dao
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.*;//donde esta el metodo que implementa el procedure
import com.tecsup.SGA.DAO.evaluaciones.SistevalHibridoDAO;

public class SistevalHibridoDAOJdbc extends JdbcDaoSupport implements SistevalHibridoDAO{
	
	public List getAllSistevalHibrido(String codigo){
		GetAllSistevalHibrido getAllSistevalHibrido = new GetAllSistevalHibrido(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSistevalHibrido.execute(codigo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
		
	}
	
	public List getSistevalHibridoById(String codigo){
		GetSistevalHibridoById getSistevalHibridoById = new GetSistevalHibridoById(this.getDataSource());
		
		HashMap outputs = (HashMap)getSistevalHibridoById.execute(codigo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
		
	}
	
	public String InsertSistevalHibrido(String inCodigo, String inDescripcion, String inCadEvaluacion, String inNroRegistros, String inCodUsuario)
	{
		InsertSistevalHibrido insertSistevalHibrido = new InsertSistevalHibrido(this.getDataSource());
			
			HashMap inputs = (HashMap)insertSistevalHibrido.execute(inCodigo,inDescripcion,inCadEvaluacion,inNroRegistros,inCodUsuario);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public String UpdateSistevalHibrido(String inCodigo, String inDescripcion, String inCadEvaluacion, String inNroRegistros, String inCodUsuario)
	{
			UpdateSistevalHibrido updateSistevalHibrido = new UpdateSistevalHibrido(this.getDataSource());
		
			
			HashMap inputs = (HashMap)updateSistevalHibrido.execute(inCodigo,inDescripcion,inCadEvaluacion,inNroRegistros,inCodUsuario);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public String DeleteSistevalHibrido(String seleccion,String usuModi){
		DeleteSistevalHibrido DeleteTablaDetalle = new DeleteSistevalHibrido(this.getDataSource());
		HashMap inputs = (HashMap)DeleteTablaDetalle.execute(seleccion, usuModi);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
		
	}	
}
