package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.AsistenciaDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.*;
import com.tecsup.SGA.common.CommonConstants;

public class AsistenciaDAOJdbc extends JdbcDaoSupport implements AsistenciaDAO{
	
	public String getIdAsistencia() {
		
		GetIdAsistencia getIdAsistencia = new GetIdAsistencia(this.getDataSource());
		HashMap outputs = (HashMap)getIdAsistencia.execute();
		
		if (!outputs.isEmpty()) {
			return (String)outputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public String insertAsistencia(String codPeriodo, String codAsistencia, String codAlumno, String codCurso, String tipoSesion, 
			String codSeccion, String nroSesion, String fecha, String tipoAsist, String usuario, String flagProg){
		
		InsertAsistencia insertAsistencia = new InsertAsistencia(this.getDataSource());
		HashMap outputs = (HashMap)insertAsistencia.execute(codPeriodo, codAsistencia, codAlumno, codCurso, tipoSesion, 
				 											codSeccion, nroSesion, fecha, tipoAsist, usuario, flagProg);
		
		if (!outputs.isEmpty()) {
			return (String)outputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public List getAllFechaAsistenciaCurso(String codPeriodo, String codCurso, 
			String codProducto, String codEspecialidad, String tipoSesion, 
			String codSeccion) {
		GetAllFechaAsistenciaCurso getAllFechaAsistenciaCurso = new GetAllFechaAsistenciaCurso(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllFechaAsistenciaCurso.execute(codPeriodo, codCurso, 
				codProducto, codEspecialidad, tipoSesion, 
				codSeccion);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getAllAlumnosXasistencia(String codPeriodo, String codAsistencia, String nombre, String apellido, String nroSesion, 
										String codCurso, 
										String codProducto, String codEspecialidad, String tipoSesion, 
										String codSeccion) {
		GetAllAlumnosXasistencia getAllAlumnosXasistencia = new GetAllAlumnosXasistencia(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllAlumnosXasistencia.execute(codPeriodo, codAsistencia, nombre, apellido, nroSesion, codCurso, 
				codProducto, codEspecialidad, tipoSesion, codSeccion);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String deleteAsistencia(String codCurso, String tipoSesion, String codSeccion, String nroSesion, String fecha){
		DeleteAsistencia deleteAsistencia = new DeleteAsistencia(this.getDataSource());
		HashMap outputs = (HashMap)deleteAsistencia.execute(codCurso, tipoSesion, codSeccion, nroSesion, fecha);
		
		if (!outputs.isEmpty()) {
			return (String)outputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}

}
