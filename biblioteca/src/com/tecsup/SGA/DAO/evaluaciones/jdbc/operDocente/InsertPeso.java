package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCursosEvaluador.CursosMapper;
import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Parciales;;

public class InsertPeso extends StoredProcedure{
	
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_ins_nro_eval_parciales";
	private static Log log = LogFactory.getLog(InsertPeso.class);
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";	
	private static final String E_C_CODIGO_ID = "E_C_CODIGO_ID";
	private static final String E_C_CODCURSO = "E_C_CODCURSO"; 
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODSECCION = "E_C_CODSECCION";
	private static final String E_C_CODTIPOEVALUACION = "E_C_CODTIPOEVALUACION"; 
	private static final String E_V_PESO = "E_V_PESO";
	private static final String E_C_CODEVALUADOR = "E_C_CODEVALUADOR";
	private static final String E_C_FLAGELIMINAR = "E_C_FLAGELIMINAR";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
    
    public InsertPeso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODIGO_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOEVALUACION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_PESO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODEVALUADOR, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_FLAGELIMINAR, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String codPeriodo, String codId, String codCurso,
    		String codPro,String codEspe, String codSeccion, String codTipoEva, String peso
    		,String codEva, String flag, String usuCrea) {
    	
    	log.info("SPROC_NAME: INI "+SPROC_NAME);
    	log.info("E_C_CODPERIODO: "+codPeriodo);
    	log.info("E_C_CODIGO_ID: "+codId);
    	log.info("E_C_CODCURSO: "+codCurso);
    	log.info("E_C_CODPRODUCTO: "+codPro);
    	log.info("E_C_CODESPECIALIDAD: "+codEspe);
    	log.info("E_C_CODSECCION: "+codSeccion);
    	log.info("E_C_CODTIPOEVALUACION: "+codTipoEva);
    	log.info("E_C_CODEVALUADOR: "+codEva);
    	log.info("E_V_PESO: "+peso);
    	log.info("E_C_FLAGELIMINAR: "+flag);
    	log.info("E_V_CODUSUARIO: "+usuCrea);
    	
    	log.info("SPROC_NAME: FIN "+SPROC_NAME);
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_C_CODIGO_ID, codId);
    	inputs.put(E_C_CODCURSO, codCurso);
    	inputs.put(E_C_CODPRODUCTO, codPro);
    	inputs.put(E_C_CODESPECIALIDAD, codEspe);
    	inputs.put(E_C_CODSECCION, codSeccion);
    	inputs.put(E_C_CODTIPOEVALUACION, codTipoEva);
    	inputs.put(E_V_PESO, peso);
    	inputs.put(E_C_CODEVALUADOR, codEva);
    	inputs.put(E_C_FLAGELIMINAR, flag);
    	inputs.put(E_V_CODUSUARIO, usuCrea);
    	
        return super.execute(inputs);
    }
    

}

