package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursosHibrido;
import com.tecsup.SGA.modelo.HibridoCurso;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetHibridoCursoById extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetHibridoCursoById.class);
    private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
    										+ ".pkg_eval_mantto_config.sp_sel_relac_prod_hibrido";
    private static final String SIHI_CODIGO = "E_C_CODIGO";    
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetHibridoCursoById(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(SIHI_CODIGO, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codigo) {
    	
    	log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("SIHI_CODIGO:"+codigo);    	
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(SIHI_CODIGO, codigo);        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CursosHibrido cursosHibrido = new CursosHibrido();
        	
        	cursosHibrido.setCodCurso(rs.getString("CODCURSO"));
        	cursosHibrido.setDescripcion(rs.getString("DESCCURSO"));
        	
        	return cursosHibrido;      	
        	
        }
    }
}


