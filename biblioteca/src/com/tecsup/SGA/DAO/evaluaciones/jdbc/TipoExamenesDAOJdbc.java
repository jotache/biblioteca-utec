package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.evaluaciones.TipoExamenesDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllTipoExamenes;;

public class TipoExamenesDAOJdbc extends JdbcDaoSupport implements TipoExamenesDAO{
	
	public List getAllTipoExamenes() {
		GetAllTipoExamenes getAllTipoExamenes = new GetAllTipoExamenes(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllTipoExamenes.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}
