package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.ConsultasReportesDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllDetalleAsistencia;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllDetalleExamenes;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllDetalleInformacionGral;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllDetallePruebaAula;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllDetallePruebaLab;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllDetallePruebaTaller;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllInformacionGeneral;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllInformacionGeneralDatos;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllPeriodoxProducto;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetDatosxProductoxAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetObtenerRankinProm;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetProductosxAlumno; 
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllRepNotasAvance;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllEstadoFinalAlumno; 
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllRepNotasFinal; 
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllCursosAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCursosEvaluador; 
import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllRepestExamenes;

public class ConsultasReportesDAOJdbc extends JdbcDaoSupport implements ConsultasReportesDAO {

	public List getAllInformacionGeneral(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo) {
		
		GetAllInformacionGeneral getAllInformacionGeneral = new GetAllInformacionGeneral(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllInformacionGeneral.execute(codPeriodo,codProducto,
				codAlumno,codCiclo);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAllInformacionGeneralDatos(String codPeriodo,
			String codProducto, String codAlumno, String codCiclo) {		
		GetAllInformacionGeneralDatos getAllInformacionGeneralDatos = new GetAllInformacionGeneralDatos(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllInformacionGeneralDatos.execute(codPeriodo,codProducto,
				codAlumno,codCiclo);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAllDetalleInformacionGral(String codPeriodo,
			String codProducto, String codAlumno, String codCiclo,
			String codCurso) {		
		GetAllDetalleInformacionGral getAllDetalleInformacionGral = new GetAllDetalleInformacionGral(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetalleInformacionGral.execute( codPeriodo,
				 codProducto,codAlumno,codCiclo,codCurso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;	
		
	}

	public List getAllDetalleAsistencia(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso) {		
		GetAllDetalleAsistencia getAllDetalleAsistencia = new GetAllDetalleAsistencia(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetalleAsistencia.execute(codPeriodo,
				 codProducto,codAlumno,codCiclo,codCurso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAllDetallePruebaAula(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso) {		
		GetAllDetallePruebaAula getAllDetallePruebaAula = new GetAllDetallePruebaAula(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetallePruebaAula.execute(codPeriodo,
				 codProducto,codAlumno,codCiclo,codCurso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;	
	}

	public List getAllDetallePruebaLab(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso) {
		
		GetAllDetallePruebaLab getAllDetallePruebaLab = new GetAllDetallePruebaLab(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetallePruebaLab.execute(codPeriodo,
				 codProducto,codAlumno,codCiclo,codCurso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;	
		
	}

	public List getAllDetalleExamenes(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo, String codCurso) {
		
		GetAllDetalleExamenes getAllDetalleExamenes = new GetAllDetalleExamenes(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetalleExamenes.execute(codPeriodo,
				 codProducto,codAlumno,codCiclo,codCurso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;	
		
	}

	public List getAllDetallePruebaTaller(String codPeriodo,
			String codProducto, String codAlumno, String codCiclo,
			String codCurso){

		GetAllDetallePruebaTaller getAllDetallePruebaTaller = new GetAllDetallePruebaTaller(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetallePruebaTaller.execute(codPeriodo,
				 codProducto,codAlumno,codCiclo,codCurso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;	
	}

	public List getProductosxAlumno(String codPeriodo, String codAlumno) {

		GetProductosxAlumno getProductosxAlumno = new GetProductosxAlumno(this.getDataSource());
		
		HashMap outputs = (HashMap)getProductosxAlumno.execute(codPeriodo,codAlumno);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;	
	}

	public List getDatosxProductoxAlumno(String codPeriodo, String cboProducto,
			String codAlumno) {
		GetDatosxProductoxAlumno getDatosxProductoxAlumno = new GetDatosxProductoxAlumno(this.getDataSource());
		
		HashMap outputs = (HashMap)getDatosxProductoxAlumno.execute(codPeriodo,cboProducto,codAlumno);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;	
	}

	public List getAllPeriodoxProducto(String codAlumno, String codPeriodo) {
		GetAllPeriodoxProducto getAllPeriodoxProducto = new GetAllPeriodoxProducto(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllPeriodoxProducto.execute(codAlumno,codPeriodo);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getObtenerRankinProm(String codAlumno, String codProducto,
			String codEspecialidad, String codPeriodo) {
		
		GetObtenerRankinProm getObtenerRankinProm = new GetObtenerRankinProm(this.getDataSource());
		
		HashMap outputs = (HashMap)getObtenerRankinProm.execute( codAlumno,  codProducto,
				 codEspecialidad,  codPeriodo);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//Eddy Dominguez - 22/04/2008
	public List GetAllRepNotasAvance(String codPeriodo,String sede,String codProducto){
		GetAllRepNotasAvance GetAllRepNotasAvance = new GetAllRepNotasAvance(this.getDataSource());
		HashMap outputs =(HashMap)GetAllRepNotasAvance.execute(codPeriodo,sede,codProducto);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllEstadoFinalAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad){
		GetAllEstadoFinalAlumno GetAllEstadoFinalAlumno = new GetAllEstadoFinalAlumno(this.getDataSource());
		HashMap outputs = (HashMap)GetAllEstadoFinalAlumno.execute(codPeriodo, codSede, codProducto, codEspecialidad);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;	
	}
	
	public List GetAllRepNotasFinal(String codPeriodo,String sede,String codProducto){
		GetAllRepNotasFinal GetAllRepNotasFinal = new GetAllRepNotasFinal(this.getDataSource());
		HashMap outputs = (HashMap)GetAllRepNotasFinal.execute(codPeriodo, sede, codProducto);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}
	
	public List GetAllRepestExamenes(String codPeriodo,String sede,String codProducto,String codEspe){
		GetAllRepestExamenes GetAllRepestExamenes = new GetAllRepestExamenes(this.getDataSource());
		HashMap outputs = (HashMap)GetAllRepestExamenes.execute(codPeriodo, sede, codProducto, codEspe);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllCursosAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad,String usuario){
		GetAllCursosAlumno GetAllCursosAlumno = new GetAllCursosAlumno(this.getDataSource());
		HashMap outputs = (HashMap)GetAllCursosAlumno.execute(codPeriodo, codSede, codProducto, codEspecialidad, usuario);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

}
