package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CiclosCat;
import com.tecsup.SGA.modelo.TipoPracticas;

public class GetAllTipoPractica extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAllTipoPractica.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
     + ".PKG_EVAL_MANTTO_CONFIG.SP_SEL_TIPO_PRACTICAS";

	 private static final String S_C_RECORDSET = "S_C_RECORDSET";

public GetAllTipoPractica(DataSource dataSource) {
	super(dataSource, SPROC_NAME);        
	declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new TipoPracticasMapper()));
	compile();
}

public Map execute() {
	log.info("*** EXEC " + SPROC_NAME + " ****");
	Map inputs = new HashMap();	
	return super.execute(inputs);
}

	final class TipoPracticasMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			TipoPracticas tipoPracticas = new TipoPracticas();
			tipoPracticas.setCodigo(rs.getString("CODIGO"));
			tipoPracticas.setDescripcion(rs.getString("DESCRIPCION"));

			return tipoPracticas;
		}

	}



}
