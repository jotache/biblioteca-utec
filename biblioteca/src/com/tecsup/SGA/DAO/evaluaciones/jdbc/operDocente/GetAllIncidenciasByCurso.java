package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.bean.IncidenciaBean;

public class GetAllIncidenciasByCurso extends StoredProcedure{
	
	private static Log log = LogFactory.getLog(GetAllIncidenciasByCurso.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
					+ ".PKG_EVAL_OPER_DOCENTE.SP_SEL_INCIDENCIAS_X_CURSO";
	
	private static final String CODPERIODO = "E_C_CODPERIODO"; 
	private static final String CODCURSO = "E_C_CODCURSO"; 
	private static final String CODPRODUCTO = "E_V_CODPRODUCTO";
	private static final String CODESPECIALIDAD = "E_V_CODESPECIALIDAD";
	private static final String TIPOSESION = "E_V_TIPOSESION";
	private static final String CODSECCION = "E_C_CODSECCION";
	private static final String TIPOOPE = "E_C_TIPOOPE";
	private static final String NOMBRE = "E_V_NOMBRE";
	private static final String APATERNO = "E_V_APATERNO"; 
	private static final String RECORDSET = "S_C_RECORDSET";
	
    public GetAllIncidenciasByCurso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODPRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TIPOSESION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOOPE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(APATERNO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new IncidenciaMapper()));
        compile();
    }
    
    public Map execute(String codPeriodo, String codCurso, String codProducto
    		, String codEspecialidad, String codTipoSesion, String codSeccion
    		, String tipoOpe, String nombre, String apellido) {    	
    	Map inputs = new HashMap();
    	
    	log.info("CODPERIODO:" + codPeriodo);
    	log.info("CODCURSO:" + codCurso);
    	log.info("CODPRODUCTO:" + codProducto);
    	log.info("CODESPECIALIDAD:" + codEspecialidad);
    	log.info("TIPOSESION:" + codTipoSesion);
    	log.info("CODSECCION:" + codSeccion);
    	log.info("TIPOOPE:" + tipoOpe);
    	log.info("NOMBRE:" + nombre);
    	log.info("APATERNO:" + apellido);
    	
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(CODPRODUCTO, codProducto);
    	inputs.put(CODESPECIALIDAD, codEspecialidad);
    	inputs.put(TIPOSESION, codTipoSesion);
    	inputs.put(CODSECCION, codSeccion);
    	inputs.put(TIPOOPE, tipoOpe);
    	inputs.put(NOMBRE, nombre);
    	inputs.put(APATERNO, apellido);
        return super.execute(inputs);
    }
    
    final class IncidenciaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	IncidenciaBean incidenciaBean = new IncidenciaBean();
        	
        	incidenciaBean.setCodAlumno(rs.getString("CODALUMNO") == null ? "" : rs.getString("CODALUMNO"));
        	incidenciaBean.setNomAlumno(rs.getString("NOMBREALUMNO") == null ? "" : rs.getString("NOMBREALUMNO"));
        	incidenciaBean.setTotalIncidencias(rs.getString("TOTALINCIDENCIAS") == null ? "" : rs.getString("TOTALINCIDENCIAS"));
        	
            return incidenciaBean;
        }
    }
}
