package com.tecsup.SGA.DAO.evaluaciones.jdbc.perfilAlum;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CompetenciasPerfil;


public class GetAllConsolidadoByAlumnoPeriodo extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllConsolidadoByAlumnoPeriodo.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
											+ ".PKG_EVAL_PERFIL_ALUMNO.SP_SEL_CONS_PERFIL_ALUMNO";
	private static final String CODPERIODO = "E_C_CODPERIODO";
	private static final String CODEVALUADOR = "E_C_CODEVALUADOR"; 
	private static final String CODALUMNO = "E_C_CODALUMNO"; 
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllConsolidadoByAlumnoPeriodo(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODEVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ConsolidadoAlumnoMapper()));
        compile();
    }
    
    public Map execute(String codPeriodo, String codEvaluador, String codAlumno) {
    	
    	log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("CODPERIODO:"+codPeriodo);
    	log.info("CODEVALUADOR:"+codEvaluador);
    	log.info("CODALUMNO:"+codAlumno);   
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODEVALUADOR, codEvaluador);
    	inputs.put(CODALUMNO, codAlumno);
        return super.execute(inputs);
    }
    
    final class ConsolidadoAlumnoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CompetenciasPerfil competenciasPerfil = new CompetenciasPerfil();
        	
        	competenciasPerfil.setCodCompetencia(rs.getString("CODCOMP") == null ? "" : rs.getString("CODCOMP"));
        	competenciasPerfil.setDscCompetencia(rs.getString("DSCCOMP") == null ? "" : rs.getString("DSCCOMP"));
        	competenciasPerfil.setNotaIni(rs.getString("VALOR_INI") == null ? "" : rs.getString("VALOR_INI"));
        	competenciasPerfil.setNotaFin(rs.getString("VALOR_FIN") == null ? "" : rs.getString("VALOR_FIN"));
        	
            return competenciasPerfil;
        }
    }

}
