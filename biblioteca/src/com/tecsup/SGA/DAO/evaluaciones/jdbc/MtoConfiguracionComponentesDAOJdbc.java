package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import java.util.HashMap;
import java.util.List;

import com.tecsup.SGA.DAO.*;
import com.tecsup.SGA.DAO.evaluaciones.MtoConfiguracionComponentesDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.*;
public class MtoConfiguracionComponentesDAOJdbc extends JdbcDaoSupport implements MtoConfiguracionComponentesDAO{

	public List getComponentes(String CODPRODUCTO, String CODESPECIALIDAD, String CODCICLO, String CODCURSO, 
			String CODETAPA, String CODPROGRAMA){
		GetProgramaComponentes getProgramaComponentes = new GetProgramaComponentes(this.getDataSource());
		HashMap outputs = (HashMap)getProgramaComponentes.execute(CODPRODUCTO, CODESPECIALIDAD, CODCICLO, 
				 CODCURSO, CODETAPA, CODPROGRAMA);
		if (!outputs.isEmpty())	 return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}
	
	public String InsertComponentes(String codProducto, String CODESPECIALIDAD, String CODCICLO, String CODCURSO,
			String CODETAPA, String CODPROGRAMA, String CADCOMPONENTE, String NROREGISTROS, String CODUSUARIO){
		InsertProgramaComponentes insertProgramaComponentes = new InsertProgramaComponentes(this.getDataSource());
		
		HashMap inputs = (HashMap)insertProgramaComponentes.execute(codProducto, CODESPECIALIDAD, CODCICLO,
				CODCURSO, CODETAPA, CODPROGRAMA, CADCOMPONENTE, NROREGISTROS, CODUSUARIO);
		if (!inputs.isEmpty()) return (String)inputs.get("S_V_RETVAL");
		
		return null;
		
	}
	public List getEspecialidadByProducto(String CODPRODUCTO){
		GetEspecialidadByProducto getEspecialidadByProducto = new GetEspecialidadByProducto(this.getDataSource());
		HashMap outputs = (HashMap)getEspecialidadByProducto.execute(CODPRODUCTO);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}
	public List getProgramasByProducto(String CODPRODUCTO, String codEtapa){
		GetProgramasByProducto getProgramasByProducto = new GetProgramasByProducto(this.getDataSource());
		HashMap outputs = (HashMap)getProgramasByProducto.execute(CODPRODUCTO, codEtapa);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}
	public List getCursosByProducto(String CODPRODUCTO, String codPeriodo){
		GetCursosByProducto getCursosByProducto = new GetCursosByProducto(this.getDataSource());
		HashMap outputs = (HashMap)getCursosByProducto.execute(CODPRODUCTO, codPeriodo);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}
	
	public List getCicloByCursoAndProducto(String CODPRODUCTO, String CodCurso){
		GetCicloByCursoAndProducto getCicloByCursoAndProducto = new GetCicloByCursoAndProducto(this.getDataSource());
		HashMap outputs = (HashMap)getCicloByCursoAndProducto.execute(CODPRODUCTO, CodCurso);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}
	public List getCicloByProductoEspecialidadPrograma(String codProducto, String codEspecialidad,
			String codPrograma, String codPeriodo){
		GetCicloByProductoEspecialidadPrograma getCicloByProductoEspecialidadPrograma = new GetCicloByProductoEspecialidadPrograma(this.getDataSource());
		HashMap outputs = (HashMap)getCicloByProductoEspecialidadPrograma.execute(codProducto, codEspecialidad, codPrograma, codPeriodo);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}
	public List getCursoByProductoEspecialidadCiclo(String codProducto, String codPrograma, String codEspecialidad, 
			String codCiclo, String codEtapa, String codPeriodo){
		GetCursoByProductoEspecialidadCiclo getCursoByProductoEspecialidadCiclo = new GetCursoByProductoEspecialidadCiclo(this.getDataSource());
		HashMap outputs = (HashMap)getCursoByProductoEspecialidadCiclo.execute(codProducto, codPrograma, 
				codEspecialidad, codCiclo, codEtapa, codPeriodo);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}
	
	public List getCursoByProductoEspecialidadCicloExt(String codProducto, String codPrograma, String codEspecialidad, String codCiclo, String codEtapa, String codPeriodo){
		GetCursoByProductoEspecialidadCicloExt getCursoByProductoEspecialidadCicloExt = new GetCursoByProductoEspecialidadCicloExt(this.getDataSource());
		HashMap outputs = (HashMap)getCursoByProductoEspecialidadCicloExt.execute(codProducto, codPrograma, codEspecialidad, codCiclo, codEtapa, codPeriodo);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}

	/*public List ListarCursosNotaExternaXProductoCiclo(String codProducto,
			String codEspecialidad, String codCiclo, String codPrograma,
			String codEtapa, String codPeriodo) {
		
		//GetCursoByProductoEspecialidadCiclo getCursoByProductoEspecialidadCiclo = new GetCursoByProductoEspecialidadCiclo(this.getDataSource());
		GetCursosNotaExternaXCicloProducto getCursosNotaExternaXCicloProducto = new GetCursosNotaExternaXCicloProducto(this.getDataSource());
		HashMap outputs = (HashMap)getCursosNotaExternaXCicloProducto.execute(codProducto, codPrograma, 
				codEspecialidad, codCiclo, codEtapa, codPeriodo);
		if (!outputs.isEmpty())	return (List)outputs.get("S_C_RECORDSET");
		
		return null;
	}*/
	
}
