package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Reportes;

public class GetAllRepNotasFinal extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllRepNotasFinal.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
	+ ".pkg_eval_reportes.sp_sel_rep_notas_final_curso";
private static final String E_C_COD_PERIODO = "E_C_COD_PERIODO";
private static final String E_C_SEDE = "E_C_SEDE";
private static final String E_C_COD_PRODUCTO = "E_C_COD_PRODUCTO";
private static final String S_C_RECORDSET = "S_C_RECORDSET";

public GetAllRepNotasFinal(DataSource dataSource) {
super(dataSource, SPROC_NAME);
declareParameter(new SqlParameter(E_C_COD_PERIODO, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_C_COD_PRODUCTO, OracleTypes.VARCHAR));
declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR,
		new DetallePruebaLabMapper()));
compile();
}

public Map execute(String codPeriodo,String sede,String codProducto) {

	Map inputs = new HashMap();
	
	log.info("*** INI " + SPROC_NAME + "***");
	log.info("E_C_COD_PERIODO: "+codPeriodo);
	log.info("E_C_SEDE: "+sede);
	log.info("E_C_COD_PRODUCTO: "+codProducto);
	log.info("*** FIN " + SPROC_NAME + "***");
	
	inputs.put(E_C_COD_PERIODO, codPeriodo);
	inputs.put(E_C_SEDE, sede);
	inputs.put(E_C_COD_PRODUCTO, codProducto);
	return super.execute(inputs);
}

final class DetallePruebaLabMapper implements RowMapper {

public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

	Reportes reportes = new Reportes();

	reportes.setDscPeriodo(rs.getString("DSC_PERIODO")== null ? "" : rs.getString("DSC_PERIODO"));
	reportes.setCodAlumno(rs.getString("COD_ALUMNO")== null ? "" : rs.getString("COD_ALUMNO"));
	reportes.setNomAlumno(rs.getString("NMB_ALUMNO")== null ? "" : rs.getString("NMB_ALUMNO"));
	reportes.setNivelCurso(rs.getString("NIVEL_CURSO")== null ? "" : rs.getString("NIVEL_CURSO"));
	reportes.setEspecialidadCurso(rs.getString("ESPECIALIDAD_CURSO")== null ? "" : rs.getString("ESPECIALIDAD_CURSO"));
	reportes.setSecciones(rs.getString("SECCIONES")== null ? "" : rs.getString("SECCIONES"));
	reportes.setNomCurso(rs.getString("NOMBRE_CURSO")== null ? "" : rs.getString("NOMBRE_CURSO"));
	reportes.setCodSeccion(rs.getString("COD_SECCION")== null ? "" : rs.getString("COD_SECCION"));
	reportes.setCodCurso(rs.getString("COD_CURSO")== null ? "" : rs.getString("COD_CURSO"));
	reportes.setTipo(rs.getString("TIPO")== null ? "" : rs.getString("TIPO"));
	reportes.setNroVez(rs.getString("NRO_VEZ")== null ? "" : rs.getString("NRO_VEZ"));
	reportes.setSisEval(rs.getString("SIS_EVAL")== null ? "" : rs.getString("SIS_EVAL"));
	reportes.setDetSisEval(rs.getString("DET_SIS_EVAL")== null ? "" : rs.getString("DET_SIS_EVAL"));
	reportes.setPromTeo(rs.getString("PROM_TEO")== null ? "" : rs.getString("PROM_TEO"));
	reportes.setPromTal(rs.getString("PROM_TAL")== null ? "" : rs.getString("PROM_TAL"));
	reportes.setPromLab(rs.getString("PROM_LAB")== null ? "" : rs.getString("PROM_LAB"));
	reportes.setExaParcial(rs.getString("EXA_PARCIAL")== null ? "" : rs.getString("EXA_PARCIAL"));
	reportes.setExaFinal(rs.getString("EXA_FINAL")== null ? "" : rs.getString("EXA_FINAL"));
	reportes.setExaCargo(rs.getString("EXA_CARGO")== null ? "" : rs.getString("EXA_CARGO"));
	
	reportes.setNtaExt(rs.getString("NOTA_EXT")== null ? "" : rs.getString("NOTA_EXT"));
	reportes.setNtaTecsup(rs.getString("NOTA_TECSUP")== null ? "" : rs.getString("NOTA_TECSUP"));
	
	reportes.setProInansistencia(rs.getString("POR_INASISTENCIA")== null ? "" : rs.getString("POR_INASISTENCIA")); 
	reportes.setPromFinal(rs.getString("PROM_FINAL")== null ? "" : rs.getString("PROM_FINAL"));
	reportes.setExaRecu(rs.getString("EXA_RECU")== null ? "" : rs.getString("EXA_RECU"));
	reportes.setPromRecu(rs.getString("PROM_RECU")== null ? "" : rs.getString("PROM_RECU"));
	reportes.setEstFinal(rs.getString("ESTADO_FINAL")== null ? "" : rs.getString("ESTADO_FINAL"));
	reportes.setNroPracTeo(rs.getString("NRO_PRAC_TEO")== null ? "" : rs.getString("NRO_PRAC_TEO"));
	reportes.setNroPracTal(rs.getString("NRO_PRAC_TAL")== null ? "" : rs.getString("NRO_PRAC_TAL"));
	reportes.setNroPracLab(rs.getString("NRO_PRAC_LAB")== null ? "" : rs.getString("NRO_PRAC_LAB"));
	reportes.setAisTeo(rs.getString("ASIS_TEO")== null ? "" : rs.getString("ASIS_TEO"));
	reportes.setAsisTal(rs.getString("ASIS_TAL")== null ? "" : rs.getString("ASIS_TAL"));
	reportes.setAsisLab(rs.getString("ASIS_LAB")== null ? "" : rs.getString("ASIS_LAB"));
	reportes.setHorasTeo(rs.getString("HORAS_TEO")== null ? "" : rs.getString("HORAS_TEO"));
	reportes.setHoarsTal(rs.getString("HORAS_TAL")== null ? "" : rs.getString("HORAS_TAL"));
	reportes.setHorasLab(rs.getString("HORAS_LAB")== null ? "" : rs.getString("HORAS_LAB"));
	reportes.setCodEvaluadorTeo(rs.getString("COD_EVALUADOR")== null ? "" : rs.getString("COD_EVALUADOR"));
	reportes.setDscEvaluadorTeo(rs.getString("DSC_EVALUADOR")== null ? "" : rs.getString("DSC_EVALUADOR"));
	
	return reportes;
}
}
}
