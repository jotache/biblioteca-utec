package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.FormacionEmpresa;

public class GelAllFormacionEmpresa extends StoredProcedure{

private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_admin.sp_sel_formacion_empresa";
	
private static Log log = LogFactory.getLog(GelAllFormacionEmpresa.class);

	private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; 
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO"; 
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_V_NOMBRE = "E_V_NOMBRE"; 
	private static final String E_V_APATERNO = "E_V_APATERNO"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GelAllFormacionEmpresa(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_APATERNO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codProducto
    					,String codEspecialidad, String nombre, String apPaterno) {
    	
    	log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("E_C_CODPERIODO:"+codPeriodo);
    	log.info("E_C_CODPRODUCTO:"+codProducto);
    	log.info("E_C_CODESPECIALIDAD:"+codEspecialidad);
    	log.info("E_V_NOMBRE:"+nombre);
    	log.info("E_V_APATERNO:"+apPaterno);
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_C_CODPRODUCTO, codProducto);
    	inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
    	inputs.put(E_V_NOMBRE, nombre);
    	inputs.put(E_V_APATERNO, apPaterno);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	FormacionEmpresa curso = new FormacionEmpresa();
        	
        	curso.setCodAlumno(rs.getString("CODALUMNO"));
        	curso.setNombreAlumno(rs.getString("NOMBREALUMNO"));
        	curso.setCiclo(rs.getString("CICLO"));
        	curso.setAPaterno(rs.getString("APATERNO"));
        	curso.setAMaterno(rs.getString("AMATERNO"));
        	curso.setNombre(rs.getString("NOMBRE"));
        	curso.setFlagPracticaInicial(rs.getString("FLAGPRACTICAINICIAL"));
        	curso.setFlagPasantia(rs.getString("FLAGPASANTIA"));
        	curso.setFlagPracticaPreProf(rs.getString("FLAGPRACTICAPREPROF"));
            return curso;
        }
    }
	
}
