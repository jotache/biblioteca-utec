package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.MtoPeriodosDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllPeriodos;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetPeriodoById;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetPeriodoBySede;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetPeriodoBySedeUsuario;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetPeriodoByUsuario;
import com.tecsup.SGA.modelo.Periodo;

public class MtoPeriodosDAOJdbc extends JdbcDaoSupport implements MtoPeriodosDAO{
  
	public List getPeriodos() 
	{
		GetAllPeriodos getPeriodos = new GetAllPeriodos(this.getDataSource());
		
		HashMap outputs = (HashMap)getPeriodos.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List<Periodo> getPeriodoBySede(String sede)
	{
		GetPeriodoBySede getPeriodoBySede = new GetPeriodoBySede(this.getDataSource());
		
		HashMap outputs = (HashMap)getPeriodoBySede.execute(sede);
		if (!outputs.isEmpty())
		{
			return (List<Periodo>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List<Periodo> getPeriodoById(String codPeriodo)
	{
		GetPeriodoById getPeriodoById = new GetPeriodoById(this.getDataSource());
		
		HashMap outputs = (HashMap)getPeriodoById.execute(codPeriodo);
		if (!outputs.isEmpty())
		{
			return (List<Periodo>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String getPeriodoByUsuario(String usuario)
	{
		GetPeriodoByUsuario getPeriodoByUsuario = new GetPeriodoByUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)getPeriodoByUsuario.execute(usuario);
		if (!outputs.isEmpty())
		{
			return outputs.get("S_N_CODPERIODO").toString();
		}
		return null;
	}

	@Override
	public List<Periodo> getPeriodoBySedeUsuario(String sede, String codUsuario) {
		GetPeriodoBySedeUsuario getPeriodoBySede = new GetPeriodoBySedeUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)getPeriodoBySede.execute(sede,codUsuario);
		if (!outputs.isEmpty())
		{
			return (List<Periodo>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
}
