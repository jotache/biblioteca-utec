package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetEspecialidadByProducto.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;

public class GetCursosByProducto extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetCursosByProducto.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
			+ ".pkg_eval_comun.sp_sel_curso_x_prod";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
    private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetCursosByProducto(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
	public Map execute(String codProducto, String codPeriodo) {

		Map inputs = new HashMap();
		
		log.info("***** SPROC: " + SPROC_NAME + " INI ******");
		log.info("E_C_CODPRODUCTO:" + codProducto);
		log.info("E_C_CODPERIODO:" + codPeriodo);
		log.info("***** SPROC: " + SPROC_NAME + " FIn ******");
		
		inputs.put(E_C_CODPRODUCTO, codProducto);
		inputs.put(E_C_CODPERIODO, codPeriodo);
	    System.out.println("getEspecialidadByProducto");
		
		return super.execute(inputs);

	}
  final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Curso curso= new Curso();
        	curso.setCodCurso(rs.getString("CODCURSO"));//CODPRODUCTO
        	curso.setDescripcion(rs.getString("DESCCURSO"));
        	
        	return curso;
        	
        	}

    }
}
