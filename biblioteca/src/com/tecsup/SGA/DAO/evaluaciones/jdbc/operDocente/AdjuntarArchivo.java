package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class AdjuntarArchivo extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION +
		".pkg_eval_oper_admin.sp_ins_adjunto_form_empresa";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO"; 
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; 
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	private static final String E_V_NOMBRENUEVOARCHIVO = "E_V_NOMBRENUEVOARCHIVO";
	private static final String E_V_NOMBREARCHIVO = "E_V_NOMBREARCHIVO";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public AdjuntarArchivo(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NOMBRENUEVOARCHIVO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NOMBREARCHIVO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codProducto, String codPeriodo, String codAlumno, String codCurso,
	String nomNuevoArchivo, String nomArchivo, String usucrea) {    	
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODPRODUCTO, codProducto);
	inputs.put(E_C_CODPERIODO, codPeriodo);
	inputs.put(E_C_CODALUMNO, codAlumno);
	inputs.put(E_C_CODCURSO, codCurso);
	inputs.put(E_V_NOMBRENUEVOARCHIVO, nomNuevoArchivo);
	inputs.put(E_V_NOMBREARCHIVO, nomArchivo);
	inputs.put(E_V_CODUSUARIO, usucrea);
	
	return super.execute(inputs);
	}

	
}
