package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.IncidenciaDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluacionesParcialesxCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllIncidenciasByAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllIncidenciasByCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.InsertIncidencia;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.DeleteIncidencia;
import com.tecsup.SGA.bean.EvaluacionesParciales;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllNroEvaluacionesxParciales;

public class IncidenciaDAOJdbc extends JdbcDaoSupport implements IncidenciaDAO{
	public List getAllIncidencias(String codPeriodo, String codAlumno
			, String codCurso, String tipoOpe
			, String codTipoSesion, String codSeccion) {
		GetAllIncidenciasByAlumno getAllIncidenciasByAlumno = new GetAllIncidenciasByAlumno(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllIncidenciasByAlumno.execute(codPeriodo, codAlumno, codCurso, tipoOpe
																	, codTipoSesion, codSeccion);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getAllIncidenciasByCurso(String codPeriodo, String codCurso, String codProducto
	    		, String codEspecialidad, String codTipoSesion, String codSeccion
	    		, String tipoOpe, String nombre, String apellido) {
			
		GetAllIncidenciasByCurso getAllIncidenciasByCurso = new GetAllIncidenciasByCurso(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllIncidenciasByCurso.execute(codPeriodo, codCurso
				, codProducto, codEspecialidad, codTipoSesion, codSeccion, tipoOpe, nombre, apellido);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String insertIncidencia(String codIncidencia, String codCurso
			, String codTipoSesion, String codSeccion, String codAlumno, String fecha
			, String fechaIni, String fechaFin, String dscIncidencia, String tipoIncidencia
			, String tipoOpe, String usuario) {
		
		InsertIncidencia insertIncidencia = new InsertIncidencia(this.getDataSource());
		
		HashMap outputs = (HashMap)insertIncidencia.execute( codIncidencia, codCurso
				, codTipoSesion, codSeccion, codAlumno, fecha
				, fechaIni, fechaFin, dscIncidencia, tipoIncidencia
				, tipoOpe, usuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	public List getAllNroEvaluacionesxParciales(String codPeriodo,String codSeccion,String codCurso,
			String codEvaluacionParcial){
		
		GetAllNroEvaluacionesxParciales getAllNroEvaluacionesxParciales = new GetAllNroEvaluacionesxParciales(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllNroEvaluacionesxParciales.execute(codPeriodo,codSeccion,codCurso, codEvaluacionParcial);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
		public String deleteIncidencia(String codIncidencia, String codUsuario) {
		DeleteIncidencia deleteIncidencia = new DeleteIncidencia(this.getDataSource());
		HashMap outputs = (HashMap)deleteIncidencia.execute(codIncidencia, codUsuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}
		
		return null;
	}

		/*	public List getAllEvaluacionesParcialesxCurso(String codPeriodo,String codCurso,
			String codSeccion, String strNombre, String strApellido,String cboEvaluacionParcial
			,String cboNroEvaluacion);
		 * */
		public List<EvaluacionesParciales> getAllEvaluacionesParcialesxCurso(String codPeriodo,String codCurso,
				String codSeccion, String strNombre, String strApellido,
				String cboNroEvaluacion,String cboEvaluacionParcial,
				String codProducto,String codEspecialidad,String fecEvaluacion){
			
			GetAllEvaluacionesParcialesxCurso getAllEvaluacionesParcialesxCurso = new GetAllEvaluacionesParcialesxCurso(this.getDataSource());
			
			HashMap outputs = (HashMap)getAllEvaluacionesParcialesxCurso.execute( codPeriodo,codCurso,
					 codSeccion,  strNombre,  strApellido,cboNroEvaluacion,cboEvaluacionParcial,
					 codProducto,codEspecialidad,fecEvaluacion);
			if (!outputs.isEmpty())
			{
				return (List<EvaluacionesParciales>)outputs.get(CommonConstants.RET_CURSOR);
			}
			return null;
		}
}
