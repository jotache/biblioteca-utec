package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CiclosCat;

public class GetAllCiclosCat extends StoredProcedure  {
	private static Log log = LogFactory.getLog(GetAllCiclosCat.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
		                                      + ".PKG_EVAL_MANTTO_CONFIG.SP_SEL_CICLOS_CAT";
		
    private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";

    public GetAllCiclosCat(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CiclosCatMapper()));
        compile();
    }

    public Map execute(String codPeriodo) {

    	log.info("*** INI " + SPROC_NAME + " ****");
		log.info("E_C_CODPERIODO:" + codPeriodo);		
		log.info("*** FIN " + SPROC_NAME + " ****");
		
              Map inputs = new HashMap();
              inputs.put(E_C_CODPERIODO,codPeriodo);
              return super.execute(inputs);
    }
   
    final class CiclosCatMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CiclosCat ciclosCat = new CiclosCat();
        	ciclosCat.setCodCiclo(rs.getString("CODCICLO"));
        	ciclosCat.setDescripcion(rs.getString("DESCRIPCION"));
        	
            return ciclosCat;
        }

    }
    
    

}
