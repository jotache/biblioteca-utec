package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.ProductoxCurso.CursosMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;

public class GetAllPeriodo extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL + ".pkg_gen_comun.sp_sel_periodo";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllPeriodo(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
       declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute() {
    	Map inputs = new HashMap();
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Periodo curso = new Periodo();
        	curso.setCodigo(rs.getString("CODIGO"));
        	curso.setNombre(rs.getString("NOMBRE"));
            return curso;
        }
    }
	
}
