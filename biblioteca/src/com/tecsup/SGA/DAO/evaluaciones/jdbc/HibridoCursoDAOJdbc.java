package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.List;
import java.util.HashMap;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.*;//donde se llama al dao
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.*;//donde esta el metodo que implementa el procedure
import com.tecsup.SGA.DAO.evaluaciones.CursosHibridoDAO;

public class HibridoCursoDAOJdbc extends JdbcDaoSupport implements HibridoCursoDAO{
	
	public List getHibridoCursoById(String codigo){
		GetHibridoCursoById getHibridoCursoById = new GetHibridoCursoById(this.getDataSource());
		
		HashMap outputs = (HashMap)getHibridoCursoById.execute(codigo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
		
	}
	
	public String InsertHibridoCurso(String codigo, String cadCodCursos, String nroRegistros, String codUsuario){
		InsertHibridoCurso insertHibridoCurso = new InsertHibridoCurso(this.getDataSource());		
		
		HashMap inputs = (HashMap)insertHibridoCurso.execute(codigo,cadCodCursos,nroRegistros,codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;		
	}
	
	public String UpdateHibridoCurso(String codigo, String cadCodCursos, String nroRegistros, String codUsuario){
		UpdateHibridoCurso updateHibridoCurso = new UpdateHibridoCurso(this.getDataSource());
		
		HashMap inputs = (HashMap)updateHibridoCurso.execute(codigo,cadCodCursos,nroRegistros,codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}		
}
