package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateEvaluacioneParciales extends StoredProcedure {
	private static Log log = LogFactory.getLog(UpdateEvaluacioneParciales.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
     + ".PKG_EVAL_OPER_DOCENTE.SP_ACT_EVAL_PARCIALES";
	
	
	private static final String E_C_CODCURSO 			= "E_C_CODCURSO";
	private static final String E_C_CODSECCION 		= "E_C_CODSECCION";
	private static final String E_C_FECHAEVAL 		= "E_C_FECHAEVAL";
	private static final String E_C_NROEVAL 	= "E_C_NROEVAL";
	private static final String E_C_CODTIPOEVALUACION 	= "E_C_CODTIPOEVALUACION";	
	private static final String E_C_CADCODALUMNOS 	= "E_C_CADCODALUMNOS";
	private static final String E_V_NROREGISTROS 			= "E_V_NROREGISTROS";
	private static final String E_V_CODUSUARIO 			= "E_V_CODUSUARIO";	
	private static final String S_V_RETVAL 				= "S_V_RETVAL";

public UpdateEvaluacioneParciales(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FECHAEVAL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_NROEVAL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODTIPOEVALUACION, OracleTypes.CHAR));	
	declareParameter(new SqlParameter(E_C_CADCODALUMNOS, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
}

public Map execute(String codCurso, String codSeccion,
		 String feEvaluacion,String nroEvaluacion,String codTipoEvaluacion,String cadCodAlumno,
		String nroRegistros, String codUsuario,String codtipoEval){

	log.info("*** INI " + SPROC_NAME + " ****");
	log.info("E_C_CODCURSO: "+codCurso);
	log.info("E_C_CODSECCION: "+codSeccion);
	log.info("E_C_FECHAEVAL: "+feEvaluacion);	
	log.info("E_C_NROEVAL: "+nroEvaluacion);
	log.info("E_C_CODTIPOEVALUACION: "+codTipoEvaluacion);
	log.info("E_C_CADCODALUMNOS: "+cadCodAlumno);
	log.info("E_V_NROREGISTROS: "+nroRegistros);
	log.info("E_V_CODUSUARIO: "+codUsuario);
	log.info("*** FIN " + SPROC_NAME + " ****");
	
	Map inputs = new HashMap();	
	inputs.put(E_C_CODCURSO, codCurso);
	inputs.put(E_C_CODSECCION, codSeccion);
	inputs.put(E_C_FECHAEVAL, feEvaluacion);
	inputs.put(E_C_NROEVAL, nroEvaluacion);
	inputs.put(E_C_CODTIPOEVALUACION, codTipoEvaluacion);
	inputs.put(E_C_CADCODALUMNOS, cadCodAlumno);
	inputs.put(E_V_NROREGISTROS, nroRegistros);
	inputs.put(E_V_CODUSUARIO, codUsuario);

	return super.execute(inputs);

}

}
