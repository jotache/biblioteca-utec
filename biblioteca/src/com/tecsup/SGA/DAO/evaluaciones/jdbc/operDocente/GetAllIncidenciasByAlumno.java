package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Incidencia;

public class GetAllIncidenciasByAlumno extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
					+ ".PKG_EVAL_OPER_DOCENTE.SP_SEL_INCIDENCIA_X_ALUMNO";
	private static final String CODPERIODO = "E_C_CODPERIODO"; 
	private static final String CODALUMNO = "E_C_CODALUMNO"; 
	private static final String CODCURSO = "E_C_CODCURSO";
	private static final String TIPOSESION = "E_V_TIPOSESION";
	private static final String CODSECCION = "E_C_CODSECCION";
	private static final String TIPOOPE = "E_C_TIPOOPE";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllIncidenciasByAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOOPE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOSESION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new IncidenciaMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codAlumno, String codCurso, String tipoOpe
    			, String codTipoSesion, String codSeccion) {     	
    	Map inputs = new HashMap();
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODALUMNO, codAlumno);
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(TIPOOPE, tipoOpe);
    	inputs.put(TIPOSESION, codTipoSesion);
    	inputs.put(CODSECCION, codSeccion);
        return super.execute(inputs);
    }
    
    final class IncidenciaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Incidencia incidencia = new Incidencia();
        	
        	incidencia.setCodIincidencia(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO"));
        	incidencia.setDscIncidencia(rs.getString("INCIDENCIA") == null ? "" : rs.getString("INCIDENCIA"));
        	incidencia.setFecha(rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
        	incidencia.setFechaIni(rs.getString("FECHAINICIO") == null ? "" : rs.getString("FECHAINICIO"));
        	incidencia.setFechaFin(rs.getString("FECHAFIN") == null ? "" : rs.getString("FECHAFIN"));
        	incidencia.setCodTipoIncidencia(rs.getString("CODTIPOINCIDENCIA") == null ? "" : rs.getString("CODTIPOINCIDENCIA"));
        	incidencia.setDscTipoIncidencia(rs.getString("TIPOINCIDENCIA") == null ? "" : rs.getString("TIPOINCIDENCIA"));
        	incidencia.setCodUsuario(rs.getString("REGISTRADOPOR") == null ? "" : rs.getString("REGISTRADOPOR"));
        	
            return incidencia;
        }
    }
}
