package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ConsultaEmpresa;

public class EmpresaXAlumno extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_admin.sp_sel_form_empresa_x_alumno";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; 
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_CODTIPOPRACTICA = "E_C_CODTIPOPRACTICA";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
              
    
    public EmpresaXAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR)); 
        declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOPRACTICA, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new EmpresaXAlumnoMapper()));
        compile();
    }

    public Map execute(String codPeriodo,String codEspecialidad
    		, String codAlumno, String codTipoPractica) {
    	
    	Map inputs = new HashMap();
    	System.out.println("***** INI (EmpresaXAlumno) pkg_eval_oper_admin.sp_sel_form_empresa_x_alumno *********");
    	System.out.println("codPeriodo: "+codPeriodo);
    	System.out.println("codEspecialidad: "+codEspecialidad);
    	System.out.println("codAlumno: "+codAlumno);
    	System.out.println("codTipoPractica: "+codTipoPractica);
    	System.out.println("***** FIN (EmpresaXAlumno) pkg_eval_oper_admin.sp_sel_form_empresa_x_alumno *********");
    	inputs.put(E_C_CODPERIODO,codPeriodo);
    	inputs.put(E_C_CODESPECIALIDAD,codEspecialidad);   
    	inputs.put(E_C_CODALUMNO,codAlumno);
    	inputs.put(E_C_CODTIPOPRACTICA,codTipoPractica);
        return super.execute(inputs);
    }  
    
    //DefNroEvalParciales
    final class EmpresaXAlumnoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ConsultaEmpresa obj = new ConsultaEmpresa();
        	
        	System.out.println("1.- "+rs.getString("NOMBREEMPRESA"));
        	System.out.println("2.- "+rs.getString("FECHAINICIO"));
        	System.out.println("3.- "+rs.getString("FECHAFIN"));
        	System.out.println("4.- "+rs.getString("CANTIDADHORAS"));
        	System.out.println("5.- "+rs.getString("CODESTADO"));
        	System.out.println("6.- "+rs.getString("DSC_OBSERVACION"));
        	System.out.println("7.- "+rs.getString("NOTA"));
        	System.out.println("8.- "+rs.getString("HORARIO_INI"));
        	System.out.println("9.- "+rs.getString("HORARIO_FIN"));
        	System.out.println("10- "+rs.getString("IND_SABADO"));
        	System.out.println("11- "+rs.getString("IND_DOMINGO"));
        	System.out.println("12- "+rs.getString("CODIGO_ID"));
        	System.out.println("----------------------------");
        	
        	obj.setCodId(rs.getString("CODIGO_ID"));
        	obj.setNombreEmpresa(rs.getString("NOMBREEMPRESA")== null ? "" : rs.getString("NOMBREEMPRESA"));
        	obj.setFechaInicio(rs.getString("FECHAINICIO")== null ? "" : rs.getString("FECHAINICIO"));
        	obj.setFechaFin(rs.getString("FECHAFIN")== null ? "" : rs.getString("FECHAFIN"));
        	obj.setCantHoras(rs.getString("CANTIDADHORAS")== null ? "" : rs.getString("CANTIDADHORAS"));
        	obj.setFlag(rs.getString("CODESTADO")== null ? "" : rs.getString("CODESTADO"));
        	obj.setObservacion(rs.getString("DSC_OBSERVACION")== null ? "" : rs.getString("DSC_OBSERVACION"));
        	obj.setNota(rs.getString("NOTA")== null ? "" : rs.getString("NOTA"));
        	obj.setHorarioIni(rs.getString("HORARIO_INI")== null ? "" : rs.getString("HORARIO_INI"));
        	obj.setHorariofin(rs.getString("HORARIO_FIN")== null ? "" : rs.getString("HORARIO_FIN"));
        	obj.setIndSabado(rs.getString("IND_SABADO")== null ? "" : rs.getString("IND_SABADO"));
        	obj.setIndDomingo(rs.getString("IND_DOMINGO")== null ? "" : rs.getString("IND_DOMINGO"));
        	
        	
            return obj;
        }
    }
	
}
