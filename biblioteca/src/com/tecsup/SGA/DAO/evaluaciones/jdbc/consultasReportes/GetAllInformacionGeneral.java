package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.bean.InformacionGeneral;
import com.tecsup.SGA.common.CommonConstants;

public class GetAllInformacionGeneral  extends StoredProcedure{

	private static Log log = LogFactory.getLog(GetAllInformacionGeneral.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".PKG_EVAL_CONSULTAS_REPORTES.SP_SEL_INF_GRAL_CRITERIOS";
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_CODCICLO = "E_C_CODCICLO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllInformacionGeneral(DataSource dataSource){
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new InformacionGeneralMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo) {
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("E_C_CODPERIODO: " + codPeriodo);
    	log.info("E_C_CODPRODUCTO: " + codProducto);
    	log.info("E_C_CODALUMNO: " + codAlumno);
    	log.info("E_C_CODCICLO: " + codCiclo);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_C_CODPRODUCTO, codProducto);
    	inputs.put(E_C_CODALUMNO, codAlumno);
    	inputs.put(E_C_CODCICLO, codCiclo);
        return super.execute(inputs);
    }
    
    final class InformacionGeneralMapper implements RowMapper {
    	
    	
    	
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {   
        	
        	InformacionGeneral infoGeneral = new InformacionGeneral();
        	
        	infoGeneral.setCodAlumno(rs.getString("CODALUMNO"));
        	infoGeneral.setNomAlumno(rs.getString("NOMBREALUMNO"));
        	infoGeneral.setApePaterno(rs.getString("APATERNO"));
        	infoGeneral.setApeMaterno(rs.getString("AMATERNO"));
        	infoGeneral.setNomNombre(rs.getString("NOMBRE"));
        	infoGeneral.setCodCurso(rs.getString("CODCURSO"));
        	infoGeneral.setDesCurso(rs.getString("DESCCURSO"));
        	infoGeneral.setCodProducto(rs.getString("CODPRODUCTO"));        	
        	infoGeneral.setDesProducto(rs.getString("DESCPRODUCTO"));        	
        	infoGeneral.setCodEspecialidad(rs.getString("CODESPECIALIDAD"));
        	infoGeneral.setDesEspecialidad(rs.getString("DESCESPECIALIDAD"));        	
        	infoGeneral.setNroRankin(rs.getString("NRORANKINGA"));
        	infoGeneral.setDesRankin(rs.getString("DESCRANKING"));
        	infoGeneral.setPromAcumulado(rs.getString("PROMEDIOACUMULADO"));
        	infoGeneral.setCodCiclo(rs.getString("CODCICLO"));
        	infoGeneral.setCodPeriodo(rs.getString("CODPERIODO"));
        	infoGeneral.setDesPeriodo(rs.getString("DESPERIODO"));
        	infoGeneral.setCodSistEval(rs.getString("CODSISTEVAL"));
        	infoGeneral.setDesSistEval(rs.getString("DESCSISTEVAL"));
        	infoGeneral.setCodEvaluador(rs.getString("CODEVALUADOR"));
        	infoGeneral.setNomEvaluador(rs.getString("NOMBREEVALUADOR"));
        	infoGeneral.setCodSeccion(rs.getString("CODSECCION"));
        	infoGeneral.setDesSeccion(rs.getString("DESSECCION"));
        	
        	DecimalFormat dfprom = new DecimalFormat("0.00");
        	DecimalFormatSymbols dfs = dfprom.getDecimalFormatSymbols();
        	dfs.setDecimalSeparator('.');
        	dfprom.setDecimalFormatSymbols(dfs);
        	
        	if ((rs.getString("PROMEDIO") == null ? "0" : rs.getString("PROMEDIO")).equals("DI")) {
            	infoGeneral.setPromedio(rs.getString("PROMEDIO")); //rs.getString("PROMEDIO")	
        	}else{
        		double prom = Double.valueOf(rs.getString("PROMEDIO") == null ? "0" : rs.getString("PROMEDIO"));        	
            	infoGeneral.setPromedio(dfprom.format(prom));	
        	}        	        	                	        
        	        	
        	infoGeneral.setNroVeces(rs.getString("NROVECES"));
        	
        	//CCORDOVA 2008/05/19
        	infoGeneral.setPromedioPeriodo(rs.getString("PROMEDIO_CICLO") == null ? "" : rs.getString("PROMEDIO_CICLO"));
        	//CCORDOVA 2008/05/20
        	infoGeneral.setInasistenciaTeo(rs.getString("INASIST_TEO") == null ? "" : rs.getString("INASIST_TEO"));
        	infoGeneral.setInasistenciaTal(rs.getString("INASIST_TAL") == null ? "" : rs.getString("INASIST_TAL"));
        	infoGeneral.setInasistenciaLab(rs.getString("INASIST_LAB") == null ? "" : rs.getString("INASIST_LAB"));
        	infoGeneral.setInasistenciaPorcentaje(rs.getString("POR_INASISTENCIA") == null ? "" : rs.getString("POR_INASISTENCIA"));
        	infoGeneral.setExam1(rs.getString("PROM_EXAM1") == null ? "" : rs.getString("PROM_EXAM1"));
        	infoGeneral.setExam2(rs.getString("PROM_EXAM2") == null ? "" : rs.getString("PROM_EXAM2"));
        	
        	//Add JHPR 2008-07-23
        	infoGeneral.setNotaRecuperacion(rs.getString("NOTA_RECU") == null ? "" : rs.getString("NOTA_RECU"));
        	infoGeneral.setPromedioRecuperacion(rs.getString("PROMEDIO_RECU") == null ? "" : rs.getString("PROMEDIO_RECU"));
        	
            return infoGeneral;
        }
    }
}