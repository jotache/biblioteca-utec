package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteSistevalHibrido extends StoredProcedure {
	private static Log log = LogFactory.getLog(DeleteSistevalHibrido.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
 		+ ".pkg_eval_mantto_config.sp_del_config_hibrido";
	
	private static final String SIHI_ID = "E_C_CODIGO_ID";
	private static final String SIHI_USU_MODI = "E_V_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";

	public DeleteSistevalHibrido(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(SIHI_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(SIHI_USU_MODI, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(String seleccion, String usuModi) {
		
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("SIHI_ID:" + seleccion);
		log.info("SIHI_USU_MODI:" + usuModi);
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();
		inputs.put(SIHI_ID, seleccion);
		inputs.put(SIHI_USU_MODI, usuModi);
		return super.execute(inputs);	
	}

}
