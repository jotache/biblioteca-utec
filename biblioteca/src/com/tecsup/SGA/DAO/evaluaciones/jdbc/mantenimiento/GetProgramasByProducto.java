package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetCursosByProducto.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Programas;

public class GetProgramasByProducto extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetProgramasByProducto.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	+ ".pkg_eval_comun.sp_sel_programas_x_prod";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODETAPA = "E_C_CODETAPA";
	
    private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetProgramasByProducto(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODETAPA, OracleTypes.CHAR));
        
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
	public Map execute(String codProducto, String codEtapa) {
		
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("GE_C_CODPRODUCTO: "+codProducto);
		log.info("E_C_CODETAPA: "+codEtapa);
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODPRODUCTO, codProducto);
		inputs.put(E_C_CODETAPA, codEtapa);
		
	    System.out.println("GetProgramasByProducto");
		
		return super.execute(inputs);

	}
  final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Programas programas= new Programas();
        	programas.setCodPrograma(rs.getString("CODPROGRAMA"));//CODPRODUCTO
        	programas.setDescripcion(rs.getString("DESCRIPCIONPROGRAMA"));
        	
        	return programas;
        	
        	}

    }
}
