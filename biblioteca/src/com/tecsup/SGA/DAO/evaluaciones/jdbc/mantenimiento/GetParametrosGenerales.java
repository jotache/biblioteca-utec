package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ParametrosGenerales;
   //obtengo los valores segun el producto y periodo

	public class GetParametrosGenerales extends StoredProcedure{
		private static Log log = LogFactory.getLog(GetParametrosGenerales.class);
		private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
		+ ".pkg_eval_mantto_config.sp_sel_parametros_grales";//pkg_eval_mantto_config.sp_sel_parametros_grales
		
	private static final String PGEN_CODPERIODO = "E_C_CODPERIODO";//e_c_codproducto
	private static final String PGEN_CODPRODUCTO = "E_C_CODPRODUCTO";//e_c_codperiodo
	private static final String RECORDSET = "S_C_RECORDSET";//s_c_recordset TIPO CURSOR
	
	public GetParametrosGenerales(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(PGEN_CODPRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(PGEN_CODPERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
	
	public Map execute(String codProducto, String codPeriodo) {

		log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("PGEN_CODPRODUCTO:"+codProducto);
    	log.info("PGEN_CODPERIODO:"+codPeriodo);
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
		Map inputs = new HashMap();		
		inputs.put(PGEN_CODPRODUCTO, codProducto);
		inputs.put(PGEN_CODPERIODO, codPeriodo);	    
		
		return super.execute(inputs);
	}
	
	final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ParametrosGenerales parametrosGenerales= new ParametrosGenerales();
        	parametrosGenerales.setCodPeriodo(rs.getString("CODPERIODO"));//CODPRODUCTO
        	parametrosGenerales.setCodProducto(rs.getString("CODPRODUCTO"));
        	parametrosGenerales.setDescripcion(rs.getString("DESCRIPCION"));//CODIGOTABLA
        	parametrosGenerales.setCodTabla(rs.getString("CODIGOTABLA"));//VALOR
        	parametrosGenerales.setValor(rs.getString("VALOR"));
        	/*System.out.println("cod periodo "+parametrosGenerales.getCodPeriodo());
        	System.out.println("cod producto "+parametrosGenerales.getCodProducto());
        	System.out.println("descripcion "+parametrosGenerales.getDescripcion());
        	System.out.println("cod tabla "+parametrosGenerales.getCodTabla());
        	System.out.println("valor "+parametrosGenerales.getValor());*/
        	return parametrosGenerales;
        	
        	}

    }
}
