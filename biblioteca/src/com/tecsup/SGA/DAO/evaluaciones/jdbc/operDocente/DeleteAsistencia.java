package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;

public class DeleteAsistencia extends StoredProcedure{
	
	/*
	PROCEDURE SP_DEL_ASISTENCIA(
		E_V_CODCURSO IN VARCHAR2,
		E_V_TIPOSESION IN VARCHAR2,
		E_V_CODSECCION IN VARCHAR2,
		E_V_NROSESION IN VARCHAR2,
		E_V_FECHASESION IN VARCHAR2,
		S_V_RETVAL IN OUT VARCHAR2
	)
	*/
	
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.SP_DEL_ASISTENCIA";

	private static final String E_V_CODCURSO = "E_V_CODCURSO";	
	private static final String E_V_TIPOSESION = "E_V_TIPOSESION";
	private static final String E_V_CODSECCION = "E_V_CODSECCION"; 
	private static final String E_V_NROSESION = "E_V_NROSESION";
	private static final String E_V_FECHASESION = "E_V_FECHASESION";
	private static final String S_V_RETVAL = "S_V_RETVAL";
    
    public DeleteAsistencia(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_CODCURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_TIPOSESION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODSECCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NROSESION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_FECHASESION, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String codCurso, String tipoSesion, String codSeccion, String nroSesion, String fecha) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_V_CODCURSO, codCurso);
    	inputs.put(E_V_TIPOSESION, tipoSesion);
    	inputs.put(E_V_CODSECCION, codSeccion);
    	inputs.put(E_V_NROSESION, nroSesion);
    	inputs.put(E_V_FECHASESION, fecha);
    	
        return super.execute(inputs);
    }
    

}
