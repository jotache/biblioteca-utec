package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.evaluaciones.CursosHibridoDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllCursosHibrido;

public class CursosHibridoDAOJdbc extends JdbcDaoSupport implements CursosHibridoDAO{
	
	public List getAllCursosHibrido(){	
		GetAllCursosHibrido getAllCursosHibrido = new GetAllCursosHibrido(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCursosHibrido.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}
