package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Evaluador;

public class GetEvaluadorSeccion extends StoredProcedure {
	//SP_SEL_EVALUADOR_SECCION
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".PKG_EVAL_OPER_DOCENTE.SP_SEL_EVALUADOR_SECCION";
	
	private static final String E_C_CODCURSO = "E_C_CODCURSO"; 
	private static final String E_C_CODSECCION = "E_C_CODSECCION"; 	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";

	public GetEvaluadorSeccion(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		 declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
	     declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));	        
	     declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new EvaludorMapper()));
	     compile();
	}
	
    public Map execute(String codCursoEjec, String codSeccion) {
		Map inputs = new HashMap();
		inputs.put(E_C_CODCURSO, codCursoEjec);
		inputs.put(E_C_CODSECCION, codSeccion);
		return super.execute(inputs);	
	}
    
    final class EvaludorMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Evaluador evaluador = new Evaluador();
			evaluador.setCodEvaluador(rs.getString("CodEvaluador"));
			evaluador.setDscEvaluador(rs.getString("NombreEvaluador"));
			return evaluador;
		}
    	
    }
}
