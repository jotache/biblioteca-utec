package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluaciones.CursosMapper;
import com.tecsup.SGA.bean.EvaluacionesParciales;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.modelo.TipoExamenes;

public class GetAllTipoPracticaxCursoxEval extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_sel_tip_prac_x_curso_x_eva";
	
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODCURSO = "E_C_CODCURSO";
	private static final String E_C_CODEVALUADOR = "E_C_CODEVALUADOR";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllTipoPracticaxCursoxEval(DataSource dataSource){
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODEVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }
    public Map execute(String codPeriodo,
			String codCurso, String codEval, String codProducto,
			String codEspecialidad){
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_C_CODCURSO, codCurso);
    	inputs.put(E_C_CODEVALUADOR, codEval);
    	inputs.put(E_C_CODPRODUCTO, codProducto);
    	inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);   	
    	
        return super.execute(inputs);
    }

    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	TipoExamenes curso = new TipoExamenes();
        	
        	curso.setCodigo(rs.getString("CODTIPOEVALUACION"));
        	curso.setDescripcion(rs.getString("DESTIPOPRACTICAS"));
            return curso;
        }
    }
}
