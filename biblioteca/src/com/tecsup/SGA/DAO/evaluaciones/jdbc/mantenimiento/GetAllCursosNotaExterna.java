package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.CursosHibrido;
import com.tecsup.SGA.modelo.Examen;

public class GetAllCursosNotaExterna  extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllCursosNotaExterna.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
									+ ".pkg_eval_comun.sp_sel_cursos_externos";	
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllCursosNotaExterna(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosNotaExternaMapper()));
        compile();
    }

    public Map execute() {
    	log.info("*** EXEC " + SPROC_NAME + " ****");
        return super.execute(new HashMap());
    }
    
    final class CursosNotaExternaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Curso curso = new Curso();
        	
        	curso.setCodCurso(rs.getString("CODCURSO"));
        	curso.setDescripcion(rs.getString("DESCURSO"));        	
        	curso.setNomPeriodo(rs.getString("NOMPERIODO"));
        	curso.setNomSede(rs.getString("SEDE"));
        	return curso;
        }
    }
}
