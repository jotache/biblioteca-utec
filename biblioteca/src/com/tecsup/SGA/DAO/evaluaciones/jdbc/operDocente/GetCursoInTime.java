package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CursoEvaluador;

public class GetCursoInTime extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_sel_curso_intime";
	private static final String EVALUADOR = "E_V_CODEVALUADOR"; //CODIGO EVALUADOR
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetCursoInTime(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(EVALUADOR, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String evaluador) {
    	
    	Map inputs = new HashMap();
    	
    	System.out.println("***********GetAllCursosInTime INI********");
    	System.out.println("evaluador:"+evaluador);
    	System.out.println("***********fin********");
    	
    	inputs.put(EVALUADOR, evaluador);
    	
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CursoEvaluador curso = new CursoEvaluador();
        	
        	curso.setCodCursoEjec(rs.getString("CODCURSOEJEC"));
        	curso.setCodPeriodo(rs.getString("CODPERIODO"));
        	curso.setPeriodo(rs.getString("NOMPERIODO"));
        	curso.setCodProducto(rs.getString("CODPRODUCTO"));
        	curso.setCodCurso(rs.getString("CODCURSO"));
        	curso.setCurso(rs.getString("NOMCURSO"));
        	
        	curso.setCodSeccion(rs.getString("CODSECCION"));
        	curso.setCodTipoSesion(rs.getString("CODTIPOSESION"));
        	
        	curso.setSistEval(rs.getString("SISTEMA"));
        	curso.setEvaluador(rs.getString("NOMEVALUADOR"));
        	curso.setDscTipoSesion(rs.getString("NOMTIPOSESION"));
        	curso.setEspecialidad(rs.getString("ESPECIALIDAD"));
        	curso.setCiclo(rs.getString("CICLO"));
        	curso.setDscSeccion(rs.getString("SECCION"));
        	
        	curso.setAmbiente(rs.getString("AMBIENTE"));
        	curso.setTipoSemana(rs.getString("TIPOSEMANA"));
        	curso.setFecha(rs.getString("FECHA"));
        	curso.setHoraInicio(rs.getString("HORA_INICIO"));
        	curso.setHoraFin(rs.getString("HORA_FIN"));
        	
            return curso;
        }
    }
}