package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ParametrosEspeciales;

public class GetAllParametrosEspeciales  extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllParametrosEspeciales.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
     + ".PKG_EVAL_MANTTO_CONFIG.SP_SEL_PARAMETROS_ESPECIALES";


	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODTABLA = "E_C_CODTABLA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";

	public GetAllParametrosEspeciales(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODTABLA, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ParametrosEspecialesMapper()));
		compile();
	}

	public Map execute(String codPeriodo, String codTabla) {
		
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("E_C_CODPERIODO:" + codPeriodo);
		log.info("E_C_CODTABLA:" + codTabla);
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();
		inputs.put(E_C_CODPERIODO,codPeriodo);
		inputs.put(E_C_CODTABLA,codTabla);
		return super.execute(inputs);
	}

	final class ParametrosEspecialesMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException{

			ParametrosEspeciales parametrosEspeciales = new ParametrosEspeciales();
			parametrosEspeciales.setId(rs.getString("CODIGOID"));
			parametrosEspeciales.setCodPeriodo(rs.getString("CODPERIODO"));
			parametrosEspeciales.setValor1(rs.getString("VALOR1"));
			parametrosEspeciales.setValor2(rs.getString("VALOR2"));
			parametrosEspeciales.setValor3(rs.getString("VALOR3"));
			parametrosEspeciales.setValor4(rs.getString("VALOR4"));
			parametrosEspeciales.setDescripcionPeriodo(rs.getString("DESCRIPCION_CICLO"));

			return parametrosEspeciales;
		}

}




}
