package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.AlumnoDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.*;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Curso;

public class AlumnoDAOJdbc extends JdbcDaoSupport implements AlumnoDAO{
	
	private static Log log = LogFactory.getLog(AlumnoDAOJdbc.class);
	
	private DataSource dataSourceArequipa;
	
	public void setDataSourceArequipa(DataSource dataSourceArequipa) {
		this.dataSourceArequipa = dataSourceArequipa;
	}
	public List getAllAlumnoCasoEspecial(String codPeriodo, String codProducto, String codEspecialidad, String codEtapa, String codPrograma, String codCiclo, String codCurso,String nombreAlumno, String apePaternoAlumno, String apeMaternoAlumno){
		GetAllAlumnoCasoEspecial getAllAlumnoCasoEspecial = new GetAllAlumnoCasoEspecial(this.getDataSource());
		
		HashMap resultado = (HashMap)getAllAlumnoCasoEspecial.execute(codPeriodo, codProducto, codEspecialidad, codEtapa, codPrograma, codCiclo, codCurso,nombreAlumno,  apePaternoAlumno,  apeMaternoAlumno);
		if ( !resultado.isEmpty() )
		{
			return (List)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;		
	}
	public List getAllAlumnoCasoEspecialCat(String codPeriodo,String codProducto,String codCiclo,String codCurso){
		GetAllAlumnoCasoEspecialCat getAllAlumnoCasoEspecialCat = new GetAllAlumnoCasoEspecialCat(this.getDataSource());
		
		HashMap resultado = (HashMap)getAllAlumnoCasoEspecialCat.execute(codPeriodo, codProducto, codCiclo, codCurso);
		if ( !resultado.isEmpty() )
		{
			return (List)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public boolean debeEncuestaPFR(Integer codalumno){
		log.info("debeEncuestaPFR("+codalumno+")");
		try {
			String query = "select numerorespuestas from tecsup.V_CAP_ENC_PFR_EXISTE_ENC where codlima_alumno=? and encuestacerrada=0";
			
			Connection con = this.dataSourceArequipa.getConnection();
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, codalumno);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				
				if(result.getInt(1) == 0){
				
					log.info("Debe Encuesta!");
					
					result.close();
					stmt.close();
					con.close();
					
					return true;						
				}
				
			}
			result.close();
			stmt.close();
			con.close();
		} catch (Exception e) {
			log.error(e, e);			
		}		
		return false;
	}
	
	public Alumno obtenerAlumno(String codAlumno) {
		Connection conexion = null;
		Alumno alumno = null;
		String sql="";
		try {
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			sql = "select codalumno,nvl(apellidopaterno,'')||' '||nvl(apellidomaterno,'')||' '||nvl(nombre,'') nombre "
					+ " ,FLGDATOSPER,EDAD from eva_v_alumnos where codalumno = " + codAlumno;
			stmt.executeQuery(sql);
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				alumno = new Alumno();
				alumno.setCodAlumno(rs.getString("codalumno"));
				alumno.setNombreAlumno(rs.getString("nombre"));
				alumno.setFlgDatosPer(rs.getString("FLGDATOSPER"));
				alumno.setEdadActual(rs.getString("EDAD"));
			}
			rs.close();
			stmt.close();
			conexion.close();			
		} catch (SQLException ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());			
		}		
		return alumno;
	}
	
	public int NroPasantiasAlumno(String codAlumno) {	
		JdbcTemplate jt = getJdbcTemplate();
		String sql = "select count(*) from eva_formacion_empresa where codalumno="+ codAlumno +" and foem_tipoetapapract='0'";
		return jt.queryForInt(sql);
	}
	@Override
	public String guardarRespuestaDatosPersonales(String codAlumno,
			String respuesta) {
		
		Connection connection = null;
		PreparedStatement stmt = null;

		try {
			connection = this.getDataSource().getConnection();			
			log.info("guardarRespuestaDatosPersonales: [" + codAlumno + "][" + respuesta + "]");

			stmt = connection.prepareStatement("update general.gen_persona set flgdatosper=?, FECHADATOSPER=sysdate where codpersona=?");

			stmt.setString(1, respuesta);
			stmt.setString(2, codAlumno);		
			stmt.executeUpdate();
			if (stmt!=null) stmt.close();
			if (connection!=null) connection.close();
			return "1";
		} catch(SQLException e) {
			e.printStackTrace();
			log.error(".RegistrarSesion(): " + e.getMessage());
		}
		return "0";	
	}
		
	
	@Override
	public List<Curso> listaCursosExternos(String codPeriodo) {
		List<Curso> lista = new ArrayList<Curso>();
		Connection cnx = null;
		try{
			cnx = this.getDataSource().getConnection();
			Statement stm = cnx.createStatement();
			String sQuery = "SELECT DISTINCT t.CODCURSOEJEC,comercial.nomprod(t.codcurso)||' (Ciclo: '||to_char(t.codciclo)||')'CURSO " + 
					"FROM EVA_V_CURSOS_EXTERNOS t where t.CODPERIODO="+codPeriodo+" AND T.SituacionRegistro='A' " + 
					"AND T.codciclo>0 ORDER BY 2";
			
			log.info("Ini.... " + "listaCursosExternos");
			log.info(sQuery);
			log.info("Fin.... " + "listaCursosExternos");
			
			ResultSet rs = stm.executeQuery(sQuery);
			Curso oCurso = null;
			while(rs.next()){
				oCurso = new Curso();
				oCurso.setCodCurso(String.valueOf(rs.getLong("CODCURSOEJEC")));
				oCurso.setNomCurso(rs.getString("CURSO"));
				lista.add(oCurso);
			}
			rs.close();
			stm.close();
			cnx.close();	
		}catch(Exception e){
			e.printStackTrace();
		}
		return lista;
	}
	
}
