package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.TipoExamenes;;

public class GetAllTipoEvaluaciones extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_mantto_config.sp_sel_tipo_practicas";
	
	
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllTipoEvaluaciones(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
       declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute() {    	
    	Map inputs = new HashMap(); 
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	TipoExamenes curso = new TipoExamenes();
        	
        	curso.setCodigo(rs.getString("CODIGO"));
        	curso.setDescripcion(rs.getString("DESCRIPCION"));
            return curso;
        }
    }
}
