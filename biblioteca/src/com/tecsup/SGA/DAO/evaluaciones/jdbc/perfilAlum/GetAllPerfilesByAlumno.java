package com.tecsup.SGA.DAO.evaluaciones.jdbc.perfilAlum;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CompetenciasPerfil;


public class GetAllPerfilesByAlumno extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllPerfilesByAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
											+ ".PKG_EVAL_PERFIL_ALUMNO.SP_SEL_PERFILES_X_ALUMNO";
	private static final String CODPERIODO = "E_C_CODPERIODO";
	private static final String CODTUTOR = "E_C_CODTUTOR"; 
	private static final String CODALUMNO = "E_C_CODALUMNO"; 
	private static final String CODPERFIL = "E_C_CODPERFIL";
	private static final String TIPO_OPE = "E_C_TIPO_OPE";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllPerfilesByAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODTUTOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODPERFIL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPO_OPE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ConceptoAlumnoMapper()));
        compile();
    }
    
    public Map execute(String codPeriodo, String codEvaluador, String codAlumno, String codPerfil
    					, String tipoOpe) {
    	
    	log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("CODPERIODO:"+codPeriodo);
    	log.info("CODTUTOR:"+codEvaluador);
    	log.info("CODALUMNO:"+codAlumno);
    	log.info("CODPERFIL:"+codPerfil);
    	log.info("TIPO_OPE:"+tipoOpe);   
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODTUTOR, codEvaluador);
    	inputs.put(CODALUMNO, codAlumno);
    	inputs.put(CODPERFIL, codPerfil);
    	inputs.put(TIPO_OPE, tipoOpe);
        return super.execute(inputs);
    }
    
    final class ConceptoAlumnoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CompetenciasPerfil competenciasPerfil = new CompetenciasPerfil();
        	
        	competenciasPerfil.setCodAlumno(rs.getString("CODALUMNO") == null ? "" : rs.getString("CODALUMNO"));
        	competenciasPerfil.setCodCompetencia(rs.getString("COD_COMPETENCIA") == null ? "" : rs.getString("COD_COMPETENCIA"));
        	competenciasPerfil.setDscCompetencia(rs.getString("DSC_COMPETENCIA") == null ? "" : rs.getString("DSC_COMPETENCIA"));
        	competenciasPerfil.setNotaIni(rs.getString("NOTA_INI") == null ? "" : rs.getString("NOTA_INI"));
        	competenciasPerfil.setCodEvalIni(rs.getString("COD_EVAL_INI") == null ? "" : rs.getString("COD_EVAL_INI"));
        	competenciasPerfil.setNotaFin(rs.getString("NOTA_FIN") == null ? "" : rs.getString("NOTA_FIN"));
        	competenciasPerfil.setCodEvalFin(rs.getString("COD_EVAL_FIN") == null ? "" : rs.getString("COD_EVAL_FIN"));
        	
            return competenciasPerfil;
        }
    }

}
