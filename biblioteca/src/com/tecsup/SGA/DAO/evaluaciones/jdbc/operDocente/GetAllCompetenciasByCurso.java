package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CompetenciasPerfil;


public class GetAllCompetenciasByCurso extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
								+ ".PKG_EVAL_OPER_DOCENTE.SP_SEL_ALUMNO_X_CURSO_COMP";
	private static final String CODPERIODO = "E_C_CODPERIODO"; 
	private static final String CODCURSO = "E_C_CODCURSO"; 
	private static final String TIPOEVAL = "E_C_TIPOEVAL";
	private static final String CODSECCION = "E_C_CODSECCION"; 
	private static final String NOMBRE = "E_V_NOMBRE"; 
	private static final String APATERNO = "E_V_APATERNO"; 
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllCompetenciasByCurso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOEVAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(APATERNO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new EvalCompetenciaMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codCurso, String codSeccion, String nombre
    		, String apellido, String codTipoSesion) {
    	
    	Map inputs = new HashMap();
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(TIPOEVAL, codTipoSesion);
    	inputs.put(CODSECCION, codSeccion);
    	inputs.put(NOMBRE, nombre);
    	inputs.put(APATERNO, apellido);
        return super.execute(inputs);
    }
    final class EvalCompetenciaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CompetenciasPerfil evalComp = new CompetenciasPerfil();
        	
        	evalComp.setCodAlumno(rs.getString("CODALUMNO") == null ? "" : rs.getString("CODALUMNO"));
        	evalComp.setNomAlumno(rs.getString("NOMBREALUMNO") == null ? "" : rs.getString("NOMBREALUMNO"));
        	
            return evalComp;
        }
    }
}
