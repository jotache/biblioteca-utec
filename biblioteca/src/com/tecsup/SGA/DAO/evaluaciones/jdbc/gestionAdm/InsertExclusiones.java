package com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertExclusiones extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertExclusiones.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
							+ ".PKG_EVAL_OPER_ADMIN.SP_INS_EXCLUSION_PROMEDIO";
	private static final String CODPERIODO = "E_C_CODPERIODO";
	private static final String CODCURSO = "E_C_CODCURSO";
	private static final String SECCION = "E_C_SECCION";
	private static final String CADCODALUMNOS = "E_C_CADCODALUMNOS"; 
	private static final String NROREGISTROS = "E_V_NROREGISTROS"; 
	private static final String CODUSUARIO = "E_V_CODUSUARIO"; 
	private static final String RETVAL = "S_V_RETVAL";
	
    public InsertExclusiones(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(SECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CADCODALUMNOS, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(NROREGISTROS, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }
    
    public Map execute(String codCurso, String cadDatos, String nroRegistros
    		, String codUsuario, String codPeriodo, String codSeccion) {
    	
    	log.info("**** INI " + SPROC_NAME + " ****");    	
    	log.info("COD_CURSO:"+codCurso);
    	log.info("CADCODALUMNOS:"+cadDatos);
    	log.info("NROREGISTROS:"+nroRegistros);
    	log.info("CODUSUARIO:"+codUsuario);
    	log.info("CODPERIODO:"+codPeriodo);
    	log.info("SECCION:"+codSeccion);
    	log.info("**** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(CADCODALUMNOS, cadDatos);
    	inputs.put(NROREGISTROS, nroRegistros);
    	inputs.put(CODUSUARIO, codUsuario);
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(SECCION, codSeccion);
        return super.execute(inputs);
    }

}
