package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.List;
import java.util.HashMap;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.GetAllExclusionesByCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm.InsertExclusiones;
import com.tecsup.SGA.DAO.evaluaciones.ExclusionDAO;

public class ExclusionDAOJdbc extends JdbcDaoSupport implements ExclusionDAO{
	
	public List getAllExclusionesByCurso(String codCurso, String nombre,
			String apellido, String codSeccion, String codPeriodo) {
		
		GetAllExclusionesByCurso getAllExclusionesByCurso = new GetAllExclusionesByCurso(this.getDataSource());
		HashMap resultado = (HashMap)getAllExclusionesByCurso.execute(codCurso, nombre, apellido
						, codSeccion, codPeriodo);
		
		if ( !resultado.isEmpty() )
		{
			return (List)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String insertExclusiones(String codCurso, String cadDatos,
			String nroRegistros, String codUsuario, String codPeriodo, String codSeccion) {
		InsertExclusiones insertExclusiones = new InsertExclusiones(this.getDataSource());
		
		HashMap resultado = (HashMap)insertExclusiones.execute(codCurso, cadDatos, nroRegistros
				, codUsuario, codPeriodo, codSeccion);
		
		if ( !resultado.isEmpty() )
		{
			return (String)resultado.get(CommonConstants.RET_STRING);
		}
		return null;
		
	}
}
