package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.NumeroMinimo;

public class GetAllNumeroMinimo extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_sel_nro_minevalparciales";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO"; 
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO"; 	
    private static final String S_C_RECORDSET = "S_C_RECORDSET";

    
    public GetAllNumeroMinimo(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new DefNroEvalParcialesMapper()));
        compile();
    }

    public Map execute(String codProducto,String codPeriodo) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPRODUCTO,codProducto);
    	inputs.put(E_C_CODPERIODO,codPeriodo);    	
        return super.execute(inputs);
    }  
    
    //DefNroEvalParciales
    final class DefNroEvalParcialesMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	NumeroMinimo obj = new NumeroMinimo();
        	
        	obj.setValor(rs.getString("VALOR"));
        	
            return obj;
        }
    }
}
