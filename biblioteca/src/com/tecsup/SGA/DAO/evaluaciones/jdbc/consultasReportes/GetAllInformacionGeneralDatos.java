package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes.GetAllInformacionGeneral.InformacionGeneralMapper;
import com.tecsup.SGA.bean.InformacionGeneral;
import com.tecsup.SGA.common.CommonConstants;

public class GetAllInformacionGeneralDatos   extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllInformacionGeneralDatos.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + 
	".PKG_EVAL_CONSULTAS_REPORTES.SP_SEL_INF_GRAL_DATOS";

	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
	private static final String E_C_CODCICLO = "E_C_CODCICLO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllInformacionGeneralDatos(DataSource dataSource){
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new InformacionGeneralMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codProducto,
			String codAlumno, String codCiclo){
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("E_C_CODPERIODO: " + codPeriodo);
    	log.info("E_C_CODPRODUCTO: " + codProducto);
    	log.info("E_C_CODALUMNO: " + codAlumno);
    	log.info("E_C_CODCICLO: " + codCiclo);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_C_CODPRODUCTO, codProducto);
    	inputs.put(E_C_CODALUMNO, codAlumno);
    	inputs.put(E_C_CODCICLO, codCiclo);
        return super.execute(inputs);
    }
    
    final class InformacionGeneralMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {   
        	
        	InformacionGeneral infoGeneral = new InformacionGeneral();
        	
        	infoGeneral.setCodAlumno(rs.getString("CODALUMNO"));
        	infoGeneral.setNomAlumno(rs.getString("NOMBREALUMNO"));
        	infoGeneral.setCodProducto(rs.getString("CODPRODUCTO"));        	
        	infoGeneral.setDesProducto(rs.getString("DESCPRODUCTO"));        	
        	infoGeneral.setCodEspecialidad(rs.getString("CODESPECIALIDAD"));
        	infoGeneral.setDesEspecialidad(rs.getString("DESCESPECIALIDAD"));        	
        	infoGeneral.setNroRankin(rs.getString("NRORANKINGA"));
        	infoGeneral.setDesRankin(rs.getString("DESCRANKING"));
        	infoGeneral.setPromAcumulado(rs.getString("PROMEDIOACUMULADO"));
        	infoGeneral.setCodPeriodo(rs.getString("CODPERIODO"));
        	infoGeneral.setDesPeriodo(rs.getString("DESPERIODO"));

        	
            return infoGeneral;
        }
    }
}