package com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;;


public class GetAllCursosByFiltros extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllCursosByFiltros.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
											+ ".pkg_eval_oper_admin.sp_sel_cursos_x_filtros";
											
	private static final String COD_PERIODO = "E_V_CODPERIODO";
	private static final String COD_PRODUCTO = "E_C_CODPRODUCTO"; 
	private static final String COD_ESPECIALIDAD = "E_C_CODESPECIALIDAD"; 
	private static final String COD_CICLO = "E_C_CODCICLO"; 
	private static final String COD_ETAPA = "E_C_CODETAPA"; 
	private static final String COD_PROGRAMA = "E_C_CODPROGRAMA";
	private static final String COD_FILTROCARGO = "E_C_FILTROCARGO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllCursosByFiltros(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(COD_PRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(COD_ESPECIALIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(COD_CICLO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(COD_ETAPA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(COD_PROGRAMA, OracleTypes.CHAR));
      //JHPR: 2/Abrl/08 Agregue parametro para filtrar solo curso Cargo en la Bandeja Principal del Administrador.
        declareParameter(new SqlParameter(COD_FILTROCARGO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new CursosByFiltrosMapper()));
        compile();
    }
    
    //JHPR: 2/Abrl/08 Agregue parametro para filtrar solo curso Cargo en la Bandeja Principal del Administrador.
    public Map execute(String codPeriodo, String codProducto, String codEspecialidad, String codCiclo,
    					String codEtapa, String codPrograma , String filtrarCargos) {
    	
    	log.info("**** INI " + SPROC_NAME + " *****");    	
    	log.info("COD_PERIODO:" + codPeriodo);
    	log.info("COD_PRODUCTO:" + codProducto);
    	log.info("COD_ESPECIALIDAD:" + codEspecialidad);
    	log.info("COD_CICLO:" + codCiclo);
    	log.info("COD_ETAPA:" + codEtapa);
    	log.info("COD_PROGRAMA:" + codPrograma);
    	log.info("COD_FILTROCARGO:" + filtrarCargos);    	
    	log.info("**** FIN " + SPROC_NAME + " *****");
    	
    	Map inputs = new HashMap();
    	inputs.put(COD_PERIODO, codPeriodo);
    	inputs.put(COD_PRODUCTO, codProducto);
    	inputs.put(COD_ESPECIALIDAD, codEspecialidad);
    	inputs.put(COD_CICLO, codCiclo);
    	inputs.put(COD_ETAPA, codEtapa);
    	inputs.put(COD_PROGRAMA, codPrograma);
    	inputs.put(COD_FILTROCARGO, filtrarCargos);
        return super.execute(inputs);
    }
    
    final class CursosByFiltrosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Curso curso = new Curso();
        	
        	curso.setCodCurso(rs.getString("CODCURSO") == null ? "" : rs.getString("CODCURSO") );
        	curso.setDescripcion(rs.getString("CURSO") == null ? "" : rs.getString("CURSO") );
        	curso.setCodSisteVal(rs.getString("CODSISTEVAL") == null ? "" : rs.getString("CODSISTEVAL"));
        	curso.setDscSistEval(rs.getString("SISTEVAL") == null ? "" : rs.getString("SISTEVAL"));
        	curso.setCodEvaluador(rs.getString("CODEVALUADOR") == null ? "" : rs.getString("CODEVALUADOR"));
        	curso.setDscEvaluador(rs.getString("EVALUADOR") == null ? "" : rs.getString("EVALUADOR"));
        	//JHPR: 31/3/2008 Mostrando Columna "Cargo" (Indica al usuario que un curso tiene alumnos que lo llevan como Cargo).
        	curso.setCargo(rs.getString("CARGO") == null ? "" : rs.getString("CARGO"));
        	
            return curso;
        }
    }

}
