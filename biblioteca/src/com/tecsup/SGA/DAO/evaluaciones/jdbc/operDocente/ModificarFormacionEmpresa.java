package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class ModificarFormacionEmpresa extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_admin.sp_act_formacion_empresa";
/*
 * E_V_OBSERVACION IN VARCHAR2,
E_V_NOTA IN VARCHAR2,
E_V_HORA_INI IN CHAR,
E_V_HORA_FIN IN CHAR,
E_V_IND_SABADO IN CHAR,
E_V_IND_DOMINGO IN CHAR,
 */
		private static final String E_C_CODIGO_ID = "E_C_CODIGO_ID";	
		private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
		private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO"; 
		private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
		private static final String E_C_CODCURSO = "E_C_CODCURSO"; 
		private static final String E_C_CODALUMNO = "E_C_CODALUMNO";
		private static final String E_C_TIPO_PRACTICA = "E_C_TIPO_PRACTICA";
		private static final String E_V_NOMBRE_EMPRESA = "E_V_NOMBRE_EMPRESA";
		private static final String E_C_FECHA_INICIO = "E_C_FECHA_INICIO";
		private static final String E_C_FECHA_FIN = "E_C_FECHA_FIN";
		private static final String E_C_CANTIDAD_HORAS = "E_C_CANTIDAD_HORAS";
		private static final String E_C_CODESTADO = "E_C_CODESTADO";
		
		private static final String E_V_OBSERVACION = "E_V_OBSERVACION";
		private static final String E_V_NOTA = "E_V_NOTA";
		private static final String E_V_HORA_INI = "E_V_HORA_INI";
		private static final String E_V_HORA_FIN = "E_V_HORA_FIN";
		private static final String E_V_IND_SABADO = "E_V_IND_SABADO";
		private static final String E_V_IND_DOMINGO = "E_V_IND_DOMINGO";
		
		private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
	    
	    public ModificarFormacionEmpresa(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        declareParameter(new SqlParameter(E_C_CODIGO_ID, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_TIPO_PRACTICA, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_NOMBRE_EMPRESA, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_FECHA_INICIO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_FECHA_FIN, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_CANTIDAD_HORAS, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_CODESTADO, OracleTypes.VARCHAR));
	        
	        declareParameter(new SqlParameter(E_V_OBSERVACION, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_NOTA, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_HORA_INI, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_HORA_FIN, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_IND_SABADO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_IND_DOMINGO, OracleTypes.VARCHAR));
	        
	        declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	        declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	        compile();
	    }

	    public Map execute(String codId, String codPeriodo, String codProducto, String codEspecialidad
	    		,String codCurso, String codAlumno, String tipoPractica, String nombreEmpresa, String fechaInicio
	    		,String fechaFin, String cantHoras, String codEstado,String observacion,String nota,String horaIni,
	    		String horaFin,String indSabado,String indDomingo, String usuCrea) {
	    	
	    	Map inputs = new HashMap();
	    	
	    	System.out.println("***INI*******************");
	    	System.out.println("codId:"+codId);
	    	System.out.println("codPeriodo:"+codPeriodo);
	    	System.out.println("codProducto:"+codProducto);
	    	System.out.println("codEspecialidad:"+codEspecialidad);
	    	System.out.println("codCurso:"+codCurso);
	    	System.out.println("codAlumno:"+codAlumno);
	    	System.out.println("tipoPractica:"+tipoPractica);
	    	System.out.println("nombreEmpresa:"+nombreEmpresa);
	    	System.out.println("fechaInicio:"+fechaInicio);
	    	System.out.println("fechaFin:"+fechaFin);
	    	System.out.println("cantHoras:"+cantHoras);
	    	System.out.println("codEstado:"+codEstado);
	    	System.out.println("observacion:"+observacion);
	    	System.out.println("nota:"+nota);
	    	System.out.println("horaIni:"+horaIni);
	    	System.out.println("horaFin:"+horaFin);
	    	System.out.println("indSabado:"+indSabado);
	    	System.out.println("indDomingo:"+indDomingo);
	    	System.out.println("usuCrea:"+usuCrea);
	    	System.out.println("***FIN*******************");
	    	
	    	inputs.put(E_C_CODIGO_ID, codId);
	    	inputs.put(E_C_CODPERIODO, codPeriodo);
	    	inputs.put(E_C_CODPRODUCTO, codProducto);
	    	inputs.put(E_C_CODESPECIALIDAD, codEspecialidad);
	    	inputs.put(E_C_CODCURSO, codCurso);
	    	inputs.put(E_C_CODALUMNO, codAlumno);
	    	inputs.put(E_C_TIPO_PRACTICA, tipoPractica);
	    	inputs.put(E_V_NOMBRE_EMPRESA, nombreEmpresa);
	    	inputs.put(E_C_FECHA_INICIO, fechaInicio);
	    	inputs.put(E_C_FECHA_FIN, fechaFin);
	    	inputs.put(E_C_CANTIDAD_HORAS, cantHoras);
	    	inputs.put(E_C_CODESTADO, codEstado);
	    	
	    	inputs.put(E_V_OBSERVACION, observacion);
	    	inputs.put(E_V_NOTA, nota);
	    	inputs.put(E_V_HORA_INI, horaIni);
	    	inputs.put(E_V_HORA_FIN, horaFin);
	    	inputs.put(E_V_IND_SABADO, indSabado);
	    	inputs.put(E_V_IND_DOMINGO, indDomingo);
	    	
	    	inputs.put(E_V_CODUSUARIO, usuCrea);
	    	
	        return super.execute(inputs);
	    }
	    

}
