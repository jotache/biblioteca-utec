package com.tecsup.SGA.DAO.evaluaciones.jdbc;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.ehcache.config.BeanHandler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.NotaExternaDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.*;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ConfiguracionNotaExterna;

public class NotaExternaDAOJdbc extends JdbcDaoSupport implements NotaExternaDAO{	
	
	private static Log log = LogFactory.getLog(NotaExternaDAOJdbc.class);
	
	public String UpdateNotaExterno(String codPeriodo,
			String codProducto, String codAlumno, String codCurso, 
			String codCiclo, String codTipo, String notaExterno,
			String codEspecialidad, String codEtapa, String codPrograma, String usuario){
		
		UpdateNotaExterno updateNotaExterno = new UpdateNotaExterno(this.getDataSource());		
		
		HashMap inputs = (HashMap)updateNotaExterno.execute(codPeriodo, 
				codProducto, codAlumno, codCurso, codCiclo, codTipo, notaExterno,
				codEspecialidad, codEtapa, codPrograma, usuario);
	
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertNotaExterno(String codPeriodo,
			String codProducto, String codAlumno, String codCurso, 
			String codCiclo, String codTipo, String notaExterno,
			String codEspecialidad, String codEtapa, String codPrograma, String usuario){	
		
		InsertNotaExterno insertNotaExterno = new InsertNotaExterno(this.getDataSource());
		
		HashMap inputs = (HashMap)insertNotaExterno.execute(codPeriodo, 
				codProducto, codAlumno, codCurso, codCiclo, codTipo, notaExterno,
				codEspecialidad, codEtapa, codPrograma, usuario);
		
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertAdjuntoCasoCat(String codPeriodo, String codProducto, String codCiclo,
			String nuevoArchivo, String archivo, String usuario){
		
		InsertAdjuntoCasoCat insertAdjuntoCasoCat = new InsertAdjuntoCasoCat(this.getDataSource());
		
		HashMap inputs = (HashMap)insertAdjuntoCasoCat.execute(codPeriodo, codProducto, codCiclo, nuevoArchivo, archivo, usuario);	
		
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;		
	}
	public List getAdjuntoCasoCatById(String codPeriodo,String codProducto,String codCiclo){
		
		GetAdjuntoCasoCatById getAdjuntoCasoCatById = new GetAdjuntoCasoCatById(this.getDataSource());
		
		HashMap resultado = (HashMap)getAdjuntoCasoCatById.execute(codPeriodo, codProducto, codCiclo);		
		if ( !resultado.isEmpty() )
		{
			return (List)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	public List getAllCursosNotaExterna(){		

		GetAllCursosNotaExterna getAllCursosNotaExterna = new GetAllCursosNotaExterna(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCursosNotaExterna.execute();
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public ConfiguracionNotaExterna configuracionNotaExterna(String codCursoEjec) {
		//JHPR 2008-05-05 Calcular Nota Tecsup.
		Connection conexion = null;
		ConfiguracionNotaExterna configNotaExterna = null;
		String sql="";
		
		try {
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			
			sql = "select prco_id, codcursoejec, prco_codregla, prco_condicion, prco_verdadero1, prco_verdadero2, prco_verdadero3, prco_verdadero4, prco_falso1, prco_falso2, prco_falso3, prco_falso4, prco_codoperverd1, prco_codoperverd2, prco_codoperverd3, prco_codoperverd4, prco_codoperfals1, prco_codoperfals2, prco_codoperfals3, prco_codoperfals4, prco_est_reg from eva_producto_config where codcursoejec =" + codCursoEjec;
			
			ResultSet rs = stmt.executeQuery(sql);
			
			if (rs.next()) {
				configNotaExterna= new ConfiguracionNotaExterna();
				configNotaExterna.setId(rs.getLong("prco_id"));
				configNotaExterna.setCodCursoEjec(rs.getLong("codcursoejec"));
				configNotaExterna.setRegla(rs.getString("prco_codregla"));
				configNotaExterna.setCondicion(rs.getString("prco_condicion"));
				configNotaExterna.setVerdadero1(rs.getString("prco_verdadero1"));
				configNotaExterna.setVerdadero2(rs.getString("prco_verdadero2"));
				configNotaExterna.setVerdadero3(rs.getString("prco_verdadero3"));
				configNotaExterna.setVerdadero4(rs.getString("prco_verdadero4"));
				configNotaExterna.setFalso1(rs.getString("prco_falso1"));
				configNotaExterna.setFalso2(rs.getString("prco_falso2"));
				configNotaExterna.setFalso3(rs.getString("prco_falso3"));
				configNotaExterna.setFalso4(rs.getString("prco_falso4"));				
				configNotaExterna.setOperadorVerdad1(rs.getString("prco_codoperverd1"));
				configNotaExterna.setOperadorVerdad2(rs.getString("prco_codoperverd2"));
				configNotaExterna.setOperadorVerdad3(rs.getString("prco_codoperverd3"));
				configNotaExterna.setOperadorVerdad4(rs.getString("prco_codoperverd4"));
				configNotaExterna.setOperadorFalso1(rs.getString("prco_codoperfals1"));
				configNotaExterna.setOperadorFalso2(rs.getString("prco_codoperfals2"));
				configNotaExterna.setOperadorFalso3(rs.getString("prco_codoperfals3"));
				configNotaExterna.setOperadorFalso4(rs.getString("prco_codoperfals4"));
				configNotaExterna.setEstado(rs.getString("prco_est_reg"));
			}
						
			rs.close();
			stmt.close();
			conexion.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		 
		}
		
		return configNotaExterna;
	}
	//JHPR 2008-05-05 Insertar/Actualizar Nota Tecsup.
	/*
	public String InsertNotaExterno(String codPeriodo, String codProducto,
			String codCiclo, String codCurso, String cadenaDatos,
			String tipoReg,String usuario,String nroRegistros) {
		
		InsertUpdateNotaExterno insertNotExterno = new InsertUpdateNotaExterno(this.getDataSource());
		
		HashMap inputs = (HashMap) insertNotExterno.execute(
				codPeriodo, codProducto, codCiclo, codCurso, cadenaDatos, tipoReg, 
				usuario, nroRegistros);	

		if (!inputs.isEmpty()) {
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	*/
	public String InsertNotaExterno(String codPeriodo, String codProducto,
			String cadenaDatos,
			String tipoReg,String usuario,String nroRegistros) {
		
		InsertUpdateNotaExterno insertNotExterno = new InsertUpdateNotaExterno(this.getDataSource());
		
		HashMap inputs = (HashMap) insertNotExterno.execute(
				codPeriodo, codProducto, cadenaDatos, tipoReg, 
				usuario, nroRegistros);	

		if (!inputs.isEmpty()) {
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List<ConfiguracionNotaExterna> listaConfiguracionNotaExterna(String codPeriodo) {
		Connection conexion = null;
		ConfiguracionNotaExterna configNotaExterna = null;
		String sql="";
		List<ConfiguracionNotaExterna> lista = new ArrayList<ConfiguracionNotaExterna>();
		try {
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			
			sql = "select prco_id, codcursoejec, prco_codregla, prco_condicion, prco_verdadero1, prco_verdadero2, prco_verdadero3, prco_verdadero4, prco_falso1, prco_falso2, prco_falso3, prco_falso4, prco_codoperverd1, prco_codoperverd2, prco_codoperverd3, prco_codoperverd4, prco_codoperfals1, prco_codoperfals2, prco_codoperfals3, prco_codoperfals4, prco_est_reg " +
					" from eva_producto_config where codcursoejec in (select codcursoejec from eva_v_cursos_externos where codperiodo=" + codPeriodo + ")";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				configNotaExterna= new ConfiguracionNotaExterna();
				configNotaExterna.setId(rs.getLong("prco_id"));
				configNotaExterna.setCodCursoEjec(rs.getLong("codcursoejec"));
				configNotaExterna.setRegla(rs.getString("prco_codregla"));
				configNotaExterna.setCondicion(rs.getString("prco_condicion"));
				configNotaExterna.setVerdadero1(rs.getString("prco_verdadero1"));
				configNotaExterna.setVerdadero2(rs.getString("prco_verdadero2"));
				configNotaExterna.setVerdadero3(rs.getString("prco_verdadero3"));
				configNotaExterna.setVerdadero4(rs.getString("prco_verdadero4"));
				configNotaExterna.setFalso1(rs.getString("prco_falso1"));
				configNotaExterna.setFalso2(rs.getString("prco_falso2"));
				configNotaExterna.setFalso3(rs.getString("prco_falso3"));
				configNotaExterna.setFalso4(rs.getString("prco_falso4"));				
				configNotaExterna.setOperadorVerdad1(rs.getString("prco_codoperverd1"));
				configNotaExterna.setOperadorVerdad2(rs.getString("prco_codoperverd2"));
				configNotaExterna.setOperadorVerdad3(rs.getString("prco_codoperverd3"));
				configNotaExterna.setOperadorVerdad4(rs.getString("prco_codoperverd4"));
				configNotaExterna.setOperadorFalso1(rs.getString("prco_codoperfals1"));
				configNotaExterna.setOperadorFalso2(rs.getString("prco_codoperfals2"));
				configNotaExterna.setOperadorFalso3(rs.getString("prco_codoperfals3"));
				configNotaExterna.setOperadorFalso4(rs.getString("prco_codoperfals4"));
				configNotaExterna.setEstado(rs.getString("prco_est_reg"));
				
				lista.add(configNotaExterna);
			}
						
			rs.close();
			stmt.close();
			conexion.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		 
		}
				
		return lista;
	}
}

