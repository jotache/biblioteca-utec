package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllEvaluadores.CursosMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Evaluador;

public class DeleteEvaluaciones extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_docente.sp_del_nro_eval_parciales";
	
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_V_CODPRODUCTO = "E_V_CODPRODUCTO";
	private static final String E_C_CODESPECIALIDAD = "E_C_CODESPECIALIDAD";
	private static final String E_V_CODCURSO = "E_V_CODCURSO";
	private static final String E_C_CODSECCION = "E_C_CODSECCION";
	private static final String E_C_CODTIPOEVALUACION = "E_C_CODTIPOEVALUACION";
	private static final String E_C_CADCODIGOS = "E_C_CADCODIGOS";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS"; 
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO"; 
	private static final String S_V_RETVAL = "S_V_RETVAL";
    
    public DeleteEvaluaciones(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_CODPRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOEVALUACION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CADCODIGOS, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
       declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String codPeriodo,String codPro, String codEspe, String codCurso,String codSeccion, String codTipoEva
    		, String cadCod, String nroReg, String  usuCrea) {
    	
    	System.out.println("**** Delete Evaluacion ****");
    	System.out.println("codPeriodo:"+codPeriodo);
    	System.out.println("codPro:"+codPro);
    	System.out.println("codEspe:"+codEspe);
    	System.out.println("codCurso:"+codCurso);
    	System.out.println("codSeccion:"+codSeccion);
    	System.out.println("codTipoEva:"+codTipoEva);
    	System.out.println("cadCod:"+cadCod);
    	System.out.println("nroReg:"+nroReg);
    	System.out.println("usuCrea:"+usuCrea);    	
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODPERIODO, codPeriodo);
    	inputs.put(E_V_CODPRODUCTO, codPro);
    	inputs.put(E_C_CODESPECIALIDAD, codEspe);
    	inputs.put(E_V_CODCURSO, codCurso);
    	inputs.put(E_C_CODSECCION, codSeccion);
    	inputs.put(E_C_CODTIPOEVALUACION, codTipoEva);
    	inputs.put(E_C_CADCODIGOS, cadCod);
    	inputs.put(E_V_NROREGISTROS, nroReg);
    	inputs.put(E_V_CODUSUARIO, usuCrea);
        return super.execute(inputs);
    }

}
