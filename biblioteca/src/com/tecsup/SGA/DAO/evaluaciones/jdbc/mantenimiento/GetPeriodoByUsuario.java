package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.object.StoredProcedure;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import com.tecsup.SGA.common.CommonConstants;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class GetPeriodoByUsuario extends StoredProcedure {

	private static Log log = LogFactory.getLog(GetPeriodoByUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
 	+ ".pkg_eval_comun.SP_SEL_PERIODO_X_USUARIO";

	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_N_CODPERIODO = "S_N_CODPERIODO";
	
	public GetPeriodoByUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_N_CODPERIODO, OracleTypes.NUMBER));
		compile();
	}
	
	public Map execute(String usuario) {
		
		log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("E_C_CODUSUARIO:"+usuario);
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODUSUARIO, usuario);
			
		return super.execute(inputs);
	
	}
	
}
