package com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Incidencia;

public class InsertIncidencia extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertIncidencia.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
					+ ".PKG_EVAL_OPER_DOCENTE.SP_INS_INCIDENCIA";
	private static final String CODIGO_ID = "E_C_CODIGO_ID"; 
	private static final String CODCURSO = "E_C_CODCURSO"; 
	private static final String TIPOSESION = "E_V_TIPOSESION";
	private static final String CODSECCION = "E_V_CODSECCION";
	private static final String CODALUMNO = "E_C_CODALUMNO";
	private static final String FECHA = "E_C_FECHA";
	private static final String FECHAINICIO = "E_C_FECHAINICIO";
	private static final String FECHAFIN = "E_C_FECHAFIN";
	private static final String TIPOINCIDENCIA = "E_C_TIPOINCIDENCIA";
	private static final String TIPOOPE = "E_C_TIPOOPE";
	private static final String INCIDENCIA = "E_V_INCIDENCIA";
	private static final String CODUSUARIO = "E_V_CODUSUARIO";
	private static final String RETVAL = "S_V_RETVAL";
    
    public InsertIncidencia(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODIGO_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODCURSO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOSESION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODSECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(FECHA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(FECHAINICIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(FECHAFIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOINCIDENCIA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOOPE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(INCIDENCIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String codIncidencia, String codCurso, String codTipoSesion, String codSeccion
    		, String codAlumno, String fecha, String fechaIni, String fechaFin
			, String dscIncidencia, String tipoIncidencia, String tipoOpe
			, String usuario) {    	
    	Map inputs = new HashMap();
    	
    	log.info("**** INI " + SPROC_NAME + "****");
    	log.info("CODIGO_ID:"+ codIncidencia);
    	log.info("CODCURSO:"+ codCurso);
    	log.info("TIPOSESION:"+ codTipoSesion);
    	log.info("CODSECCION:"+ codSeccion);
    	log.info("CODALUMNO:"+ codAlumno);
    	log.info("FECHA:"+ fecha);
    	log.info("FECHAINICIO:"+ fechaIni);
    	log.info("FECHAFIN:"+ fechaFin);
    	log.info("TIPOINCIDENCIA:"+ tipoIncidencia);
    	log.info("TIPOOPE:"+ tipoOpe);
    	log.info("INCIDENCIA:"+ dscIncidencia);
    	log.info("CODUSUARIO:"+ usuario);
    	log.info("**** FIN " + SPROC_NAME + "****");
    	
    	inputs.put(CODIGO_ID, codIncidencia);
    	inputs.put(CODCURSO, codCurso);
    	inputs.put(TIPOSESION, codTipoSesion);
    	inputs.put(CODSECCION, codSeccion);
    	inputs.put(CODALUMNO, codAlumno);
    	inputs.put(FECHA, fecha);
    	inputs.put(FECHAINICIO, fechaIni);
    	inputs.put(FECHAFIN, fechaFin);
    	inputs.put(TIPOINCIDENCIA, tipoIncidencia);
    	inputs.put(TIPOOPE, tipoOpe);
    	inputs.put(INCIDENCIA, dscIncidencia);
    	inputs.put(CODUSUARIO, usuario);
        return super.execute(inputs);
    }
   
}
