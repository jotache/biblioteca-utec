package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.evaluaciones.TipoPracticasDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento.GetAllTipoPracticas;

public class TipoPracticasDAOJdbc extends JdbcDaoSupport implements TipoPracticasDAO{
	
	public List getAllTipoPracticas() {
		GetAllTipoPracticas getAllTipoPracticas = new GetAllTipoPracticas(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllTipoPracticas.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}
