package com.tecsup.SGA.DAO.evaluaciones.jdbc.registroNota;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.modelo.CursosHibrido;
import com.tecsup.SGA.modelo.Examen;
import com.tecsup.SGA.modelo.SistevalHibrido;

public class GetAdjuntoCasoCatById  extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAdjuntoCasoCatById.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_oper_admin.sp_sel_adjunto_caso_cat";
		
	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";
	private static final String E_C_CODPRODUCTO = "E_C_CODPRODUCTO";
	private static final String E_C_CODCICLO = "E_C_CODCICLO";	
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAdjuntoCasoCatById(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPRODUCTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCICLO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
    public Map execute(String codPeriodo,String codProducto,String codCiclo) {
    	
    	log.info("**** INI " + SPROC_NAME + " ****");
    	log.info("E_C_CODPERIODO:" + codPeriodo );
    	log.info("E_C_CODPRODUCTO:" + codProducto );
    	log.info("E_C_CODCICLO:" + codCiclo );
    	log.info("**** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
        inputs.put(E_C_CODPERIODO, codPeriodo);        
        inputs.put(E_C_CODPRODUCTO, codProducto);
        inputs.put(E_C_CODCICLO, codCiclo);
        return super.execute(inputs);

    }

    public Map execute() {    	
        return super.execute(new HashMap());
    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Archivo archivo = new Archivo();
        	
        	archivo.setNombre(rs.getString("NOMBREARCHIVO"));
        	archivo.setNombreNuevo(rs.getString("NOMBRENUEVOARCHIVO"));
        	
        	return archivo; 
        }
    }
}