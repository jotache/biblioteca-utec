package com.tecsup.SGA.DAO.evaluaciones.jdbc.gestionAdm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Examen;


public class GetAllTypesOfExams extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllTypesOfExams.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
											+ ".pkg_eval_oper_admin.sp_sel_tipo_examenes";
	private static final String COD_PERIODO = "E_C_CODPERIODO";
	private static final String COD_CURSO = "E_C_CODCURSO";
	private static final String TIPO_EXAM = "E_C_TIPO_EXAM";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllTypesOfExams(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
    	declareParameter(new SqlParameter(COD_PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(COD_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TIPO_EXAM, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new TypesOfExamsMapper()));
        compile();
    }
    
    public Map execute(String codPeriodo, String codCurso, String tipoExamen) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("**** INI " + SPROC_NAME + " ****");
    	log.info("COD_PERIODO:"+codPeriodo);
    	log.info("COD_CURSO:"+codCurso);
    	log.info("TIPO_EXAM:"+tipoExamen);
    	log.info("**** FIN " + SPROC_NAME + " ****");
    	
    	inputs.put(COD_PERIODO, codPeriodo);
    	inputs.put(COD_CURSO, codCurso);
    	inputs.put(TIPO_EXAM, tipoExamen);
        return super.execute(inputs);
    }
    
    final class TypesOfExamsMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Examen examen = new Examen();
        	examen.setCodigo(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO"));
        	examen.setDescripcion(rs.getString("DESCRIPCION") == null ? "" : rs.getString("DESCRIPCION"));
        	examen.setAlias(rs.getString("ALIAS") == null ? "" : rs.getString("ALIAS").trim());
            return examen;
        }
    }
}
