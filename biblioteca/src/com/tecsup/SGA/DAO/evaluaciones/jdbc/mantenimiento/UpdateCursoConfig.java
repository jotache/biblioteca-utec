package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class UpdateCursoConfig extends StoredProcedure{	
	private static Log log = LogFactory.getLog(UpdateCursoConfig.class);
private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
	                                 	+ ".pkg_eval_mantto_config.sp_act_nota_externa";

private static final String E_C_CODCURSO = "E_C_CODCURSO";
private static final String E_C_CODREGLA = "E_C_CODREGLA";
private static final String E_V_CONDICION = "E_V_CONDICION";
private static final String E_V_VERDADERO1 = "E_V_VERDADERO1";
private static final String E_V_VERDADERO2 = "E_V_VERDADERO2";
private static final String E_V_VERDADERO3 = "E_V_VERDADERO3";
private static final String E_V_VERDADERO4 = "E_V_VERDADERO4";
private static final String E_V_FALSO1 = "E_V_FALSO1";
private static final String E_V_FALSO2 = "E_V_FALSO2";
private static final String E_V_FALSO3 = "E_V_FALSO3";
private static final String E_V_FALSO4 = "E_V_FALSO4";
private static final String E_C_CODOPERAVERD1 = "E_C_CODOPERAVERD1";
private static final String E_C_CODOPERAVERD2 = "E_C_CODOPERAVERD2";
private static final String E_C_CODOPERAVERD3 = "E_C_CODOPERAVERD3";
private static final String E_C_CODOPERAVERD4 = "E_C_CODOPERAVERD4";
private static final String E_C_CODOPERAFALS1 = "E_C_CODOPERAFALS1";
private static final String E_C_CODOPERAFALS2 = "E_C_CODOPERAFALS2";
private static final String E_C_CODOPERAFALS3 = "E_C_CODOPERAFALS3";
private static final String E_C_CODOPERAFALS4 = "E_C_CODOPERAFALS4";
private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
private static final String S_V_RETVAL = "S_V_RETVAL";

public UpdateCursoConfig(DataSource dataSource) {
super(dataSource, SPROC_NAME);
declareParameter(new SqlParameter(E_C_CODCURSO, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_C_CODREGLA, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_V_CONDICION, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_VERDADERO1, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_VERDADERO2, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_VERDADERO3, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_VERDADERO4, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_FALSO1, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_FALSO2, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_FALSO3, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_FALSO4, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_C_CODOPERAVERD1, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODOPERAVERD2, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODOPERAVERD3, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODOPERAVERD4, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODOPERAFALS1, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODOPERAFALS2, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODOPERAFALS3, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODOPERAFALS4, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.VARCHAR));
declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
compile();
}

	public Map execute(String codCurso, String regla, String condicion,
			String verdadero01, String verdadero02, String verdadero03, String verdadero04,
			String falso01, String falso02, String falso03, String falso04,
			String opeVer01, String opeVer02, String opeVer03, String opeVer04,
			String opeFal01, String opeFal02, String opeFal03, String opeFal04,
			String usuario){
	
		
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("E_C_CODCURSO: "+codCurso);
		log.info("E_C_CODREGLA: "+regla);
		log.info("E_V_CONDICION: "+condicion);
		log.info("E_V_VERDADERO1: "+verdadero01);
		log.info("E_V_VERDADERO2: "+verdadero02);
		log.info("E_V_VERDADERO3: "+ verdadero03);
		log.info("E_V_VERDADERO4: "+ verdadero04);
		log.info("E_V_FALSO1: "+ falso01);
		log.info("E_V_FALSO2: "+ falso02);
		log.info("E_V_FALSO3: "+ falso03);
		log.info("E_V_FALSO4: "+ falso04);
		log.info("E_C_CODOPERAVERD1: "+ opeVer01);
		log.info("E_C_CODOPERAVERD2: "+ opeVer02);
		log.info("E_C_CODOPERAVERD3: "+ opeVer03);
		log.info("E_C_CODOPERAVERD4: "+ opeVer04);
		log.info("E_C_CODOPERAFALS1: "+ opeFal01);
		log.info("E_C_CODOPERAFALS2: "+ opeFal02);
		log.info("E_C_CODOPERAFALS3: "+ opeFal03);
		log.info("E_C_CODOPERAFALS4: "+ opeFal04);		
		log.info("E_C_CODUSUARIO: "+ usuario);
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODCURSO, codCurso);
		inputs.put(E_C_CODREGLA, regla);
		inputs.put(E_V_CONDICION, condicion);
		inputs.put(E_V_VERDADERO1, verdadero01);
		inputs.put(E_V_VERDADERO2, verdadero02);
		inputs.put(E_V_VERDADERO3, verdadero03);
		inputs.put(E_V_VERDADERO4, verdadero04);
		inputs.put(E_V_FALSO1, falso01);
		inputs.put(E_V_FALSO2, falso02);
		inputs.put(E_V_FALSO3, falso03);
		inputs.put(E_V_FALSO4, falso04);
		inputs.put(E_C_CODOPERAVERD1, opeVer01);
		inputs.put(E_C_CODOPERAVERD2, opeVer02);
		inputs.put(E_C_CODOPERAVERD3, opeVer03);
		inputs.put(E_C_CODOPERAVERD4, opeVer04);
		inputs.put(E_C_CODOPERAFALS1, opeFal01);
		inputs.put(E_C_CODOPERAFALS2, opeFal02);
		inputs.put(E_C_CODOPERAFALS3, opeFal03);
		inputs.put(E_C_CODOPERAFALS4, opeFal04);		
		inputs.put(E_C_CODUSUARIO, usuario);
		
		return super.execute(inputs);	
	}
}