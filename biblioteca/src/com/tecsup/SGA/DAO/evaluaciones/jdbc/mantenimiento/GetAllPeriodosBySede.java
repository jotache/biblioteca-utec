package com.tecsup.SGA.DAO.evaluaciones.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Periodo;

public class GetAllPeriodosBySede extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllPeriodosBySede.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
 	+ ".pkg_eval_comun.sp_sel_all_periodos_x_sede";//pkg_eval_mantto_config.sp_sel_productos
	//E_C_CODSEDE IN CHAR
	
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllPeriodosBySede(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));  
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	compile();
	}
	
	public Map execute(String codSede) {
		
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("E_C_CODSEDE:" + codSede);		
		log.info("*** FIN " + SPROC_NAME + " ****");
		
	Map inputs = new HashMap();	
	
	inputs.put(E_C_CODSEDE, codSede);
	
	return super.execute(inputs);
	
	}
	final class ProcesoMapper implements RowMapper {
	    
	    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	    	
	    	Periodo periodo = new Periodo();
	    	periodo.setCodigo(rs.getString("CODPERIODO") == null ? "": rs.getString("CODPERIODO"));
	    	periodo.setNombre(rs.getString("DESCPERIODO") == null ? "": rs.getString("DESCPERIODO"));
	        periodo.setFecInicio(rs.getString("FECHAINICIO") == null ? "": rs.getString("FECHAINICIO")); 
	        periodo.setFecFin(rs.getString("FECHAFIN") == null ? "": rs.getString("FECHAFIN"));
	        periodo.setSede(rs.getString("SEDE") == null ? "": rs.getString("SEDE"));
	        periodo.setEstado(rs.getString("ESTADO") == null ? "": rs.getString("ESTADO"));
	        return periodo;
	    }
	
	}

}
