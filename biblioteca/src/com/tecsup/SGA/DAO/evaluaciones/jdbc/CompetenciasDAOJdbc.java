package com.tecsup.SGA.DAO.evaluaciones.jdbc;

import java.util.*;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.evaluaciones.CompetenciasDAO;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCompetenciasByCurso;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.GetAllCompetenciasByCursoByAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.operDocente.InsertCompetenciasByAlumno;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.perfilAlum.GetAllAlumnosByTutor;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.perfilAlum.GetAllConsolidadoByAlumnoPeriodo;
import com.tecsup.SGA.DAO.evaluaciones.jdbc.perfilAlum.GetAllPerfilesByAlumno;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CompetenciasPerfil;

public class CompetenciasDAOJdbc extends JdbcDaoSupport implements CompetenciasDAO {

	public List getAllCompetenciasByCurso(String codPeriodo, String codCurso, String codSeccion,
			String nombre, String apellido, String codTipoSesion) {
		GetAllCompetenciasByCurso getAllCompetenciasByCurso = new GetAllCompetenciasByCurso(this.getDataSource());
		
		HashMap resultado = (HashMap)getAllCompetenciasByCurso.execute(codPeriodo, codCurso, codSeccion
				, nombre, apellido, codTipoSesion);
		if ( !resultado.isEmpty() )
		{
			return (List)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List<CompetenciasPerfil> getAllCompetenciasByCursoByAlumno(String codPeriodo, String codCurso,
			String codSeccion, String codAlumno, String tipoOpe) {
		GetAllCompetenciasByCursoByAlumno getAllCompetenciasByCursoByAlumno 
									= new GetAllCompetenciasByCursoByAlumno(this.getDataSource());
		
		HashMap resultado = (HashMap)getAllCompetenciasByCursoByAlumno.execute(codPeriodo, codCurso, codSeccion
								, codAlumno, tipoOpe);
		if ( !resultado.isEmpty() )
		{
			return (List<CompetenciasPerfil>)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String insertCompetenciasByAlumno(String codAlumno, String codCurso,
			String codPeriodo, String tipoOpe, String nroComp,
			String datosComp, String codUsuario, String codSeccion,
			String codTipoSesion) {
		InsertCompetenciasByAlumno insertCompetenciasByAlumno = new InsertCompetenciasByAlumno(this.getDataSource());
		
		HashMap resultado = (HashMap)insertCompetenciasByAlumno.execute(codAlumno, codCurso, codPeriodo
															, tipoOpe, nroComp, datosComp, codUsuario
															, codSeccion, codTipoSesion);
		if ( !resultado.isEmpty() )
		{
			return (String)resultado.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public List<CompetenciasPerfil> getAllAlumnosByTutor(String codPeriodo,
			String codEvaluador, String nomAlumno, String apeAlumno) {
		// 
		GetAllAlumnosByTutor getAllAlumnosByTutor =
			new GetAllAlumnosByTutor(this.getDataSource());

		HashMap resultado = (HashMap)getAllAlumnosByTutor.execute(codPeriodo, codEvaluador
																, nomAlumno, apeAlumno);	
		if ( !resultado.isEmpty() )
		{
			return (List<CompetenciasPerfil>)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<CompetenciasPerfil> getAllComsByAlumno(String codPeriodo,
			String codEvaluador, String codAlumno) {
		GetAllConsolidadoByAlumnoPeriodo getAllConsolidadoByAlumnoPeriodo =
			new GetAllConsolidadoByAlumnoPeriodo(this.getDataSource());

		HashMap resultado = (HashMap)getAllConsolidadoByAlumnoPeriodo.execute(codPeriodo, codEvaluador
									, codAlumno);
		if ( !resultado.isEmpty() )
		{
			return (List<CompetenciasPerfil>)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<CompetenciasPerfil> getAllCompPerfilByAlumno(String codPeriodo,
			String codEvaluador, String codAlumno, String codPerfil,
			String tipoOpe) {
		GetAllPerfilesByAlumno getAllPerfilesByAlumno = new GetAllPerfilesByAlumno(this.getDataSource());
		
		HashMap resultado = (HashMap)getAllPerfilesByAlumno.execute(codPeriodo, codEvaluador, codAlumno
																	, codPerfil, tipoOpe);
		
		if ( !resultado.isEmpty() )
		{
			return (List<CompetenciasPerfil>)resultado.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
}
