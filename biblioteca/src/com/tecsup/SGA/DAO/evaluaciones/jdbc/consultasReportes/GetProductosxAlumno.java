package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.bean.DetalleAsistencia;
import com.tecsup.SGA.bean.ProductoxAlumno;
import com.tecsup.SGA.common.CommonConstants;

public class GetProductosxAlumno extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetProductosxAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
			+ ".PKG_EVAL_CONSULTAS_REPORTES.SP_SEL_PRODUCTOS_X_ALUMNO";

	private static final String E_C_CODPERIODO = "E_C_CODPERIODO";	
	private static final String E_C_CODALUMNO = "E_C_CODALUMNO";		
	private static final String RECORDSET = "S_C_RECORDSET";

	public GetProductosxAlumno(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODPERIODO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODALUMNO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR,
				new GetProductosxAlumnoMapper()));
		compile();
	}

	public Map execute(String codPeriodo, String codAlumno) {

		log.info("*** INI " + SPROC_NAME + "***");
		log.info("E_C_CODPERIODO: "+codPeriodo);
		log.info("E_C_CODALUMNO:"+ codAlumno);		
		log.info("*** FIN " + SPROC_NAME + "***");
		
		Map inputs = new HashMap();
		inputs.put(E_C_CODPERIODO, codPeriodo);		
		inputs.put(E_C_CODALUMNO, codAlumno);	
		return super.execute(inputs);
	}

	final class GetProductosxAlumnoMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

			ProductoxAlumno productoxAlumno = new ProductoxAlumno();
			productoxAlumno.setCodProducto(rs.getString("CODPRODUCTO"));
			productoxAlumno.setCodAlumno(rs.getString("CODALUMNO"));
			productoxAlumno.setDesProducto(rs.getString("DESCPRODUCTO"));
			productoxAlumno.setNomAlumno(rs.getString("NOMBREALUMNO"));
			
			return productoxAlumno;
		}
	}
}