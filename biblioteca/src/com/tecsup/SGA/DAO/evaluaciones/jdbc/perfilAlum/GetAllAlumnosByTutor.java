package com.tecsup.SGA.DAO.evaluaciones.jdbc.perfilAlum;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CompetenciasPerfil;


public class GetAllAlumnosByTutor extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllAlumnosByTutor.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION 
											+ ".PKG_EVAL_PERFIL_ALUMNO.SP_SEL_ALUMNOS_X_TUTOR";
	private static final String CODPERIODO = "E_C_CODPERIODO";
	private static final String CODEVALUADOR = "E_C_CODEVALUADOR"; 
	private static final String NOM_ALUMNO = "E_V_NOM_ALUMNO"; 
	private static final String APE_ALUMNO = "E_V_APE_ALUMNO"; 
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllAlumnosByTutor(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODPERIODO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CODEVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(NOM_ALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(APE_ALUMNO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AlumnoByTutorMapper()));
        compile();
    }
    
    public Map execute(String codPeriodo, String codEvaluador, String nomAlumno, String apeAlumno) {
    	
    	log.info("*** INI " + SPROC_NAME + " ****");
    	log.info("CODPERIODO:"+codPeriodo);
    	log.info("CODEVALUADOR:"+codEvaluador);
    	log.info("NOM_ALUMNO:"+nomAlumno);
    	log.info("APE_ALUMNO:"+apeAlumno);
    	log.info("*** FIN " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	inputs.put(CODPERIODO, codPeriodo);
    	inputs.put(CODEVALUADOR, codEvaluador);
    	inputs.put(NOM_ALUMNO, nomAlumno);
    	inputs.put(APE_ALUMNO, apeAlumno);
        return super.execute(inputs);
    }
    
    final class AlumnoByTutorMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CompetenciasPerfil competenciasPerfil = new CompetenciasPerfil();
        	
        	competenciasPerfil.setCodAlumno(rs.getString("CODALUMNO") == null ? "" : rs.getString("CODALUMNO"));
        	competenciasPerfil.setNomAlumno(rs.getString("NOMBREALUMNO") == null ? "" : rs.getString("NOMBREALUMNO"));
        	
            return competenciasPerfil;
        }
    }

}
