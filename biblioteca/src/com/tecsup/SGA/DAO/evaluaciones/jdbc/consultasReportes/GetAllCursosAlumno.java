package com.tecsup.SGA.DAO.evaluaciones.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Reportes;

public class GetAllCursosAlumno extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllCursosAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".pkg_eval_reportes.sp_sel_rep_cursos_alumno";
	
	private static final String E_C_COD_PERIODO = "E_C_COD_PERIODO";
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_COD_PRODUCTO = "E_C_COD_PRODUCTO";
	private static final String E_C_COD_ESPECIALIDAD = "E_C_COD_ESPECIALIDAD";
	private static final String E_V_DSC_USUARIO = "E_V_DSC_USUARIO";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllCursosAlumno(DataSource dataSource){
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_PERIODO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_ESPECIALIDAD, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DSC_USUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new InformacionGeneralMapper()));
        compile();
    }

    public Map execute(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad,String usuario) {
    	
    	Map inputs = new HashMap();
    	log.info("**** INI "  + SPROC_NAME + " ****");
    	log.info("E_C_COD_PERIODO.- "+codPeriodo);
    	log.info("E_C_SEDE.- "+codSede);
    	log.info("E_C_COD_PRODUCTO.- "+codProducto);
    	log.info("E_C_COD_ESPECIALIDAD.- "+codEspecialidad);
    	log.info("E_V_DSC_USUARIO.- "+usuario);
    	log.info("**** FIN "  + SPROC_NAME + " ****");
    	
    	
    	inputs.put(E_C_COD_PERIODO, codPeriodo);
    	inputs.put(E_C_SEDE, codSede);
    	inputs.put(E_C_COD_PRODUCTO, codProducto);
    	inputs.put(E_C_COD_ESPECIALIDAD, codEspecialidad);
    	inputs.put(E_V_DSC_USUARIO, usuario);
        return super.execute(inputs);
    }
    
    final class InformacionGeneralMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {   
        	
        	Reportes reportes = new Reportes();
        	
        	reportes.setDscPeriodo(rs.getString("DSC_PERIODO")== null ? "" : rs.getString("DSC_PERIODO"));
        	reportes.setCodAlumno(rs.getString("COD_ALUNMO")== null ? "" : rs.getString("COD_ALUNMO"));
        	reportes.setNivelAlumno(rs.getString("NIVEL_ALUMNO")== null ? "" : rs.getString("NIVEL_ALUMNO"));
        	reportes.setEspecialidadAlumno(rs.getString("DSC_ESCPECIALIDAD_ALUM")== null ? "" : rs.getString("DSC_ESCPECIALIDAD_ALUM"));
        	reportes.setNomAlumno(rs.getString("NMB_ALUMNO")== null ? "" : rs.getString("NMB_ALUMNO"));
        	reportes.setCurso(rs.getString("NMB_CURSO")== null ? "" : rs.getString("NMB_CURSO"));
        	reportes.setNivelCurso(rs.getString("NIVEL_CURSO")== null ? "" : rs.getString("NIVEL_CURSO"));
        	reportes.setEspecialidadCurso(rs.getString("DSC_ESCPECIALIDAD_CURSO")== null ? "" : rs.getString("DSC_ESCPECIALIDAD_CURSO"));
        	reportes.setSecciones(rs.getString("DSC_SECCION")== null ? "" : rs.getString("DSC_SECCION"));
        	reportes.setCodSeccion(rs.getString("COD_SECCION")== null ? "" : rs.getString("COD_SECCION"));
        	reportes.setCodCurso(rs.getString("COD_CURSO")== null ? "" : rs.getString("COD_CURSO"));
        	reportes.setTipo(rs.getString("DSC_TIPO")== null ? "" : rs.getString("DSC_TIPO"));
        	reportes.setNroVez(rs.getString("NUM_VEZ")== null ? "" : rs.getString("NUM_VEZ"));
        	reportes.setSisEval(rs.getString("SIS_EVAL")== null ? "" : rs.getString("SIS_EVAL"));
        	reportes.setDetalle(rs.getString("DETALLE")== null ? "" : rs.getString("DETALLE"));
        	reportes.setPromTeo(rs.getString("PROM_AULA")== null ? "" : rs.getString("PROM_AULA"));
        	reportes.setPromTal(rs.getString("PROM_TALLER")== null ? "" : rs.getString("PROM_TALLER"));
        	reportes.setPromLab(rs.getString("PROM_LABORATORIO")== null ? "" : rs.getString("PROM_LABORATORIO"));
        	reportes.setExam1(rs.getString("PROM_EXAM1")== null ? "" : rs.getString("PROM_EXAM1"));
        	reportes.setExam2(rs.getString("PROM_EXAM2")== null ? "" : rs.getString("PROM_EXAM2"));
        	reportes.setNotaCargo(rs.getString("NOTA_CARGO")== null ? "" : rs.getString("NOTA_CARGO"));
        	reportes.setNtaExt(rs.getString("NOTA_EXTERNA")== null ? "" : rs.getString("NOTA_EXTERNA"));
        	reportes.setNtaTecsup(rs.getString("NOTA_EXTERNA_TECSUP")== null ? "" : rs.getString("NOTA_EXTERNA_TECSUP"));
        	reportes.setProInansistencia(rs.getString("POR_INASISTENCIA")== null ? "" : rs.getString("POR_INASISTENCIA"));
        	reportes.setPromFinal(rs.getString("PROM_FINAL")== null ? "" : rs.getString("PROM_FINAL"));
        	reportes.setSubRecu(rs.getString("SUBSANACION_RECUPERACION")== null ? "" : rs.getString("SUBSANACION_RECUPERACION"));
        	reportes.setPromRecu(rs.getString("PROM_RECUPERACION")== null ? "" : rs.getString("PROM_RECUPERACION"));
        	reportes.setEstFinal(rs.getString("ESTADO_FINAL")== null ? "" : rs.getString("ESTADO_FINAL"));
        	reportes.setAisTeo(rs.getString("INASIST_TEO")== null ? "" : rs.getString("INASIST_TEO"));
        	reportes.setAsisTal(rs.getString("INASIST_TAL")== null ? "" : rs.getString("INASIST_TAL"));
        	reportes.setAsisLab(rs.getString("INASIST_LAB")== null ? "" : rs.getString("INASIST_LAB"));
        	reportes.setHorasTeo(rs.getString("H0RAS_TEO")== null ? "" : rs.getString("H0RAS_TEO"));
        	reportes.setHoarsTal(rs.getString("H0RAS_TAL")== null ? "" : rs.getString("H0RAS_TAL"));
        	reportes.setHorasLab(rs.getString("H0RAS_LAB")== null ? "" : rs.getString("H0RAS_LAB"));
        	//reportes.setCodEvaluadorTeo(rs.getString("COD_PROFESOR")== null ? "" : rs.getString("COD_PROFESOR"));
        	//reportes.setDscEvaluadorTeo(rs.getString("NMB_PROFESOR")== null ? "" : rs.getString("NMB_PROFESOR"));
     
        	return reportes;
        }
    }
}
