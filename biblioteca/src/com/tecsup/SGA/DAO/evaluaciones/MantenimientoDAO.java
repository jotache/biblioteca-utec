package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

public interface MantenimientoDAO {
	/**
	 * @param codPeriodo
	 * @param cadValoresForm
	 * @param cadValoresCat
	 * @param cadValoresEscala
	 * @param nroRegistrosCat
	 * @param codUsuario
	 * @return
	 */
	public String insertParametrosEspeciales(String codPeriodo,String cadValoresForm,String cadValoresCat
			,String  cadValoresEscala,String nroRegistrosCat,String codUsuario);


	/**
	 * @param codPeriodo
	 * @return
	 */
	public List getLstCilos(String codPeriodo);


	/**
	 * @param codPeriodo
	 * @param codTabla
	 * @return
	 */
	public List getAllParametrosEspeciales(String codPeriodo, String codTabla);


	/**
	 * @param codPeriodo
	 * @param cadValoresForm
	 * @param cadValoresCat
	 * @param cadValoresEscala
	 * @param nroRegistrosCat
	 * @param codUsuario
	 * @return
	 */
	public String updateParametrosEspeciales(String codPeriodo,
			String cadValoresForm, String cadValoresCat,
			String cadValoresEscala, String nroRegistrosCat, String codUsuario);


	/**
	 * @return
	 */
	public List getTipoPractica();


	/**
	 * @param codCurso
	 * @param codSeccion
	 * @param feEvaluacion
	 * @param cadCodAlumno
	 * @param nroRegistros
	 * @param codUsuario
	 * @return
	 */
	public String insertEvaluacioneParciales(String codCurso,
			String codSeccion,
			String feEvaluacion,
			String codNroEval,
			String nroEvaluacion,
			String codTipoEvaluacion,
			String cadCodAlumno,
			String nroRegistros,
			String codUsuario);


	/**
	 * @param codCurso
	 * @param feEvaluacion
	 * @param codSeccion
	 * @param nroEvaluacion
	 * @param cadCodAlumno
	 * @param nroRegistros
	 * @param codUsuario
	 * @return
	 */
	public String updateEvaluacioneParciales(String codCurso, String codSeccion,
			String feEvaluacion, String nroEvaluacion,String codTipoEvaluacion,String cadCodAlumno,
			String nroRegistros, String codUsuario,String codtipoEval);


	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @return
	 */
	public List getTipoPracticaxCurso(String codPeriodo, String codCurso);
	
	public List getAllCursosCat(String codProducto, String codCiclo);
	
	public List GetAllPeriodosBySede(String codSede);
	
}
