package com.tecsup.SGA.DAO.evaluaciones;

import java.util.List;

public interface AdminCursosDAO {
	
	public List getAllTypesOfExams(String codPeriodo, String codCurso, String tipoExamen);
	
	//JHPR: 2/Abrl/08 Agregue parametro para filtrar solo curso Cargo en la Bandeja Principal del Administrador.
	public List getAllCursosByFiltros(String codPeriodo, String codProducto, String codEspecialidad, String codCiclo,
									String codEtapa, String codPrograma, String filtrarCargos);
	
	//JHPR 31/3/2008 Para visualizar los alumnos con examenes de cargo.
	public List getAllExamsByAlumno(String codPeriodo, String codCurso, String codSeccion, String nombre, String apellido, String tipoExamen);
	
	public String insertNotasExamenes(String codPeriodo, String codCurso, String codSeccion, String cadNotas, 
			String nroRegistros, String codUsuario);
	
	//JHPR 1/4/2008 Para ingresar notas de examenes de cargo.
	public String insertNotasExamenesCargo(String codPeriodo, String codCurso, String cadNotas, 
			String nroRegistros, String codUsuario,String tipoExamen);
	
//JHPR 2008-7-14
	public String insertNotasExamenesSubsanacion(String codPeriodo, String codCurso, String cadNotas, 
			String nroRegistros, String codUsuario);
	
	public String insertNotasExamenesRecuperacion(String codPeriodo, String codCurso, String cadNotas, 
			String nroRegistros, String codUsuario);
	
	public String updateLevantarDI(
			String codCurso, String codTipoSesion, String codSeccion
    		, String codAlumno, String tipoIncidencia, String tipoOpe, String usuario);
}
