package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;

public class GetReporteMatFaltante extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReporteMatFaltante.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.sp_sel_report_mat_faltante";

	 	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	 	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetReporteMatFaltante(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);

	        declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteMatFaltanteMapper()));
	        compile();
	    }

	    public Map execute(String fecIni, String fecFin,String sede){
	    	
	    	Map inputs = new HashMap();

	    	log.info("**** INI " + SPROC_NAME + " ****");
	    	log.info("E_V_FEC_INI:" + fecIni);
	    	log.info("E_V_FEC_FIN:" + fecFin);
	    	log.info("E_C_SEDE:" + sede);
	    	log.info("**** FIN " + SPROC_NAME + " ****");
	    	
	    	inputs.put(E_V_FEC_INI, fecIni);
	    	inputs.put(E_V_FEC_FIN, fecFin);
	    	inputs.put(E_C_SEDE, sede);
	        return super.execute(inputs);
	    }
	    
	    final class ReporteMatFaltanteMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	Reporte reporte = new Reporte();
	        	
	        	reporte.setCodDeweyGenerico(rs.getString("COD_DEWEY_GENERICO") == null ? "" : rs.getString("COD_DEWEY_GENERICO") );
	        	reporte.setNomDeweyGenerico(rs.getString("NOM_DEWEY_GENERICO") == null ? "" : rs.getString("NOM_DEWEY_GENERICO") );
	        	reporte.setCodDewey(rs.getString("COD_DEWEY") == null ? "" : rs.getString("COD_DEWEY") );
	        	reporte.setNomDewey(rs.getString("NOM_DEWEY") == null ? "" : rs.getString("NOM_DEWEY") );
	        	reporte.setCodigoMaterial(rs.getString("COD_MATERIAL") == null ? "" : rs.getString("COD_MATERIAL") );
	        	reporte.setCodigo(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO") );
	        	reporte.setTipoMaterial(rs.getString("TIPO_MATERIAL") == null ? "" : rs.getString("TIPO_MATERIAL") );
	        	reporte.setTituloMaterial(rs.getString("TITULO") == null ? "" : rs.getString("TITULO") );
	        	reporte.setNroIngresos(rs.getString("NRO_INGRESO") == null ? "" : rs.getString("NRO_INGRESO") );
	        	reporte.setUsuario(rs.getString("USUARIO") == null ? "" : rs.getString("USUARIO") );
	        	reporte.setTipoUsuario(rs.getString("TIPOUSUARIO") == null ? "" : rs.getString("TIPOUSUARIO") );
	        	reporte.setFecPrestamo(rs.getString("FECHA_PRESTAMO") == null ? "" : rs.getString("FECHA_PRESTAMO") );
	        	reporte.setIdioma(rs.getString("NOM_IDIOMA") == null ? "" : rs.getString("NOM_IDIOMA") );
	        		        		        	
	            return reporte;
	        }

	    }
}

