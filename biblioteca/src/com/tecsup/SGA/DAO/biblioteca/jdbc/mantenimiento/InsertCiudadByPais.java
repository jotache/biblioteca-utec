package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertCiudadByPais extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertCiudadByPais.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_mantto_config.sp_ins_ciudad_x_pais";	
	
	private static final String E_C_CODPAIS = "E_C_CODPAIS";
	private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertCiudadByPais(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODPAIS, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.CHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(String codigoPais, String descripcion, String usuCrea) {
	
		log.info("**** INI " + SPROC_NAME + "*****");
		log.info("E_C_CODPAIS: "+ codigoPais);//codigo detalle padre
		log.info("E_V_DESCRIPCION: "+ descripcion);
		log.info("E_V_USU_CREA: "+ usuCrea);
		log.info("**** FIN " + SPROC_NAME + "*****");
		
		Map inputs = new HashMap();
		inputs.put(E_C_CODPAIS, codigoPais);//codigo detalle padre
		inputs.put(E_V_DESCRIPCION, descripcion);
		inputs.put(E_V_USU_CREA, usuCrea);
					
		return super.execute(inputs);
	
	}
}
