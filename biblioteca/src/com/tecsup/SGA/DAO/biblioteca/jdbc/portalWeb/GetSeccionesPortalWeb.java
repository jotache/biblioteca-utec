package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb.GetReservaSalas.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.Secciones;

public class GetSeccionesPortalWeb extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetSeccionesPortalWeb.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_web.sp_sel_secciones";
    
 	private static final String E_C_TIPO_SECCION = "E_C_TIPO_SECCION";
 	private static final String E_C_TIPO_MATERIAL = "E_C_TIPO_MATERIAL";
 	    
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetSeccionesPortalWeb(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
      
        declareParameter(new SqlParameter(E_C_TIPO_SECCION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO_MATERIAL, OracleTypes.CHAR));
                      
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codTipoSeccion, String codTipoMaterial) {
    	 
    	log.info("**** INI "  + SPROC_NAME + "*****");
    	log.info("E_C_TIPO_SECCION: " + codTipoSeccion);
    	log.info("E_C_TIPO_MATERIAL: " + codTipoMaterial);
    	log.info("**** FIN "  + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_TIPO_SECCION, codTipoSeccion);
    	inputs.put(E_C_TIPO_MATERIAL, codTipoMaterial);
    	    	        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Secciones seccion= new Secciones();
       
        	seccion.setCodUnico(rs.getString("CODREGISTRO")== null ? "&nbsp;" : rs.getString("CODREGISTRO"));
        	seccion.setCodSeccion(rs.getString("CODSECCION")== null ? "&nbsp;" : rs.getString("CODSECCION"));
        	seccion.setDescripcionSeccion(rs.getString("DESCSECCION")== null ? "&nbsp;" : rs.getString("DESCSECCION"));
        	seccion.setCodGrupo(rs.getString("CODGRUPO")== null ? "&nbsp;" : rs.getString("CODGRUPO"));
        	seccion.setDescripcionGrupo(rs.getString("DESCGRUPO")== null ? "&nbsp;" : rs.getString("DESCGRUPO"));
        	seccion.setTema(rs.getString("TEMA")== null ? "&nbsp;" : rs.getString("TEMA"));
        	seccion.setDescripcionCorta(rs.getString("DESCCORTA")== null ? "&nbsp;" : rs.getString("DESCCORTA"));
        	seccion.setDescripcionLarga(rs.getString("DESCLARGA")== null ? "&nbsp;" : rs.getString("DESCLARGA"));
        	seccion.setUrl(rs.getString("URL")== null ? "&nbsp;" : rs.getString("URL"));
        	seccion.setArchivo(rs.getString("ARCHIVO")== null ? "&nbsp;" : rs.getString("ARCHIVO"));
        	seccion.setImagen(rs.getString("EXTENSION")== null ? "&nbsp;" : rs.getString("EXTENSION"));
        	
        	seccion.setDocumentoNov(rs.getString("DOCUMENTO")== null ? "" : rs.getString("DOCUMENTO"));
        	seccion.setExtDocNov(rs.getString("EXT_DOC_NOV")== null ? "" : rs.getString("EXT_DOC_NOV"));
        	
        	//System.out.println("CodUnico Store: "+seccion.getCodUnico());        	
            return seccion;
        }

    }
}
