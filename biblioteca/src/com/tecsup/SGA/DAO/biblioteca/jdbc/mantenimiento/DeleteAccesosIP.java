package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteAccesosIP extends StoredProcedure{

    private static Log log = LogFactory.getLog(DeleteAccesosIP.class);

    private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA.concat(".pkg_bib_mantto_config.SP_DEL_ACCESOS_IP");

    private static final String E_N_COD_PERSONA = "E_N_COD_PERSONA";

    public DeleteAccesosIP(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_N_COD_PERSONA, OracleTypes.NUMBER));

        compile();
    }

    @SuppressWarnings("rawtypes")
    public Map execute(Integer codPersona) {

        Map<String,Object> inputs = new HashMap<String,Object>();

        log.info("**** INI " + SPROC_NAME + "****");

        log.info("E_N_COD_PERSONA--->" + codPersona + "<---");

        log.info("**** FIN " + SPROC_NAME + "****");

        inputs.put(E_N_COD_PERSONA, codPersona);

        return super.execute(inputs);
    }
}
