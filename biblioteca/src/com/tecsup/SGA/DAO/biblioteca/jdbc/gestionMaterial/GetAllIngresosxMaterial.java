package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.IngresosxMaterialBean;
import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllIngresosxMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllIngresosxMaterial.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_GESTION_MAT.SP_SEL_INGRESOS_X_MATERIAL";
	 	
	 	private static final String E_C_COD_MATERIAL = "E_C_COD_MATERIAL";
	 	private static final String E_C_CODIGO_UNICO = "E_C_CODIGO_UNICO";   
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllIngresosxMaterial(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_C_COD_MATERIAL, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODIGO_UNICO, OracleTypes.CHAR));       
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new IngresosxMaterialMapper()));
	        compile();
	    }

	    public Map execute(String txhCodMaterial,String txhCodUnico){
	    	
	    	Map inputs = new HashMap();
	    	
	    	log.info("*** INI "+ SPROC_NAME + " ***");
	    	log.info("E_C_COD_MATERIAL:"+txhCodMaterial);
	    	log.info("E_C_CODIGO_UNICO:"+txhCodUnico);
	    	log.info("*** FIN "+ SPROC_NAME + " ***");
	    	
	    	inputs.put(E_C_COD_MATERIAL,txhCodMaterial);
	    	inputs.put(E_C_CODIGO_UNICO,txhCodUnico);	        
	        return super.execute(inputs);

	    }
	    
	    final class IngresosxMaterialMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	IngresosxMaterialBean bean = new IngresosxMaterialBean();
	        	
	        	bean.setCodUnico(rs.getString("CODIGOUNICO"));
	        	bean.setFecIngreso(rs.getString("FECHA_INGRESO"));
	        	bean.setNroVolumen(rs.getString("NRO_VOLUMEN"));
	        	bean.setNroIngreso(rs.getString("NRO_INGRESO"));
	        	bean.setCodPrecedencia(rs.getString("COD_PROCEDENCIA"));
	        	bean.setDesPrecedencia(rs.getString("DESC_PROCEDENCIA"));
	        	bean.setPrecio(rs.getString("PRECIO"));
	        	bean.setEstado(rs.getString("ESTADO"));
	        	bean.setFecBaja(rs.getString("FECHA_BAJA"));
	        	bean.setObservacion(rs.getString("OBSERVACIONES"));
	        	bean.setCodMoneda(rs.getString("CODMONEDA"));
	        	bean.setNroPrecio(rs.getString("NROPRECIO"));
	        	bean.setDescEstado(rs.getString("DESCESTADO"));
	        	bean.setDescMoneda(rs.getString("DESCMONEDA"));
	        	
	            return bean;
	        }

	    }
}

