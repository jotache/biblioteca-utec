package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertSecciones extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertSecciones.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 		+ ".pkg_bib_mantto_config.sp_ins_secciones";
		
	private static final String E_V_COD_UNICO = "E_V_COD_UNICO";
	private static final String E_V_SECCION = "E_V_SECCION";
	private static final String E_V_TIPO_MATERIAL = "E_V_TIPO_MATERIAL";
	private static final String E_V_GRUPO = "E_V_GRUPO";
	private static final String E_V_TEMA = "E_V_TEMA";
	private static final String E_V_DESC_CORTA = "E_V_DESC_CORTA";
	private static final String E_V_DESC_LARGA = "E_V_DESC_LARGA";	
	private static final String E_V_URL = "E_V_URL";
	private static final String E_V_ARCHIVO = "E_V_ARCHIVO";
	private static final String E_V_FECHA_PUBLI_INI = "E_V_FECHA_PUBLI_INI";
	private static final String E_V_FECHA_PUBLI_FIN = "E_V_FECHA_PUBLI_FIN";
	private static final String E_V_FLAG_WEB = "E_V_FLAG_WEB";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	private static final String E_V_TIPO = "E_V_TIPO";
	private static final String E_V_SECC_NOM_ADJUNTO = "E_V_SECC_NOM_ADJUNTO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertSecciones(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_COD_UNICO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_SECCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TIPO_MATERIAL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_GRUPO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TEMA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_DESC_CORTA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_DESC_LARGA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_URL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_ARCHIVO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FECHA_PUBLI_INI, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FECHA_PUBLI_FIN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FLAG_WEB, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TIPO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_SECC_NOM_ADJUNTO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}

			public Map execute(String codUnico, 
					String codSeccion, 
					String dscTipoMaterial, 
					String codGrupo, 
					String dscTema, 
					String dscCorta, 
					String dscLarga, 
					String dscUrl, 
					String dscArchivo, 
					String fechaPublicacion,
					String fechaVigencia, 
					String flag,
					String usuario, 
					String tipo,
					String nomAdjunto) {
				
				log.info("**** INI " + SPROC_NAME + "*****");
				log.info("E_V_COD_UNICO>>"+codUnico+"<<");
				log.info("E_V_SECCION>>"+codSeccion+"<<");
				log.info("E_V_TIPO_MATERIAL>>"+dscTipoMaterial+"<<");
				log.info("E_V_GRUPO>>"+codGrupo+"<<");
				log.info("E_V_TEMA>>"+dscTema+"<<");
				log.info("E_V_DESC_CORTA>>"+dscCorta+"<<");
				log.info("E_V_DESC_LARGA>>"+dscLarga+"<<");
				log.info("E_V_URL>>"+dscUrl+"<<");				
				log.info("E_V_ARCHIVO>>"+dscArchivo+"<<");
				log.info("E_V_FECHA_PUBLI_INI>>"+fechaPublicacion+"<<");
				log.info("E_V_FECHA_PUBLI_FIN>>"+fechaVigencia+"<<");
				log.info("E_V_FLAG_WEB>>"+flag+"<<");
				log.info("E_V_USU_CREA>>"+usuario+"<<");
				log.info("E_V_TIPO>>"+tipo+"<<");
				log.info("E_V_SECC_NOM_ADJUNTO>>"+nomAdjunto+"<<");
				log.info("**** INI " + SPROC_NAME + "*****");
				
				Map inputs = new HashMap();
				
				inputs.put(E_V_COD_UNICO, codUnico);
				inputs.put(E_V_SECCION, codSeccion); 
				inputs.put(E_V_TIPO_MATERIAL, dscTipoMaterial);
				inputs.put(E_V_GRUPO, codGrupo);
				inputs.put(E_V_TEMA, dscTema);
				inputs.put(E_V_DESC_CORTA, dscCorta);
				inputs.put(E_V_DESC_LARGA, dscLarga);				
				inputs.put(E_V_URL, dscUrl);
				inputs.put(E_V_ARCHIVO, dscArchivo);
				inputs.put(E_V_FECHA_PUBLI_INI, fechaPublicacion);
				inputs.put(E_V_FECHA_PUBLI_FIN, fechaVigencia);
				inputs.put(E_V_FLAG_WEB, flag);
				inputs.put(E_V_USU_CREA, usuario);
				inputs.put(E_V_TIPO, tipo);
				inputs.put(E_V_SECC_NOM_ADJUNTO, nomAdjunto);
							
				return super.execute(inputs);			
			}
}
