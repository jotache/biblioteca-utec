package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteRolesApoyo extends StoredProcedure{

	private static Log log = LogFactory.getLog(DeleteRolesApoyo.class);

	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA.concat(".pkg_bib_mantto_config.SP_DEL_ROLES_APOYO");

	private static final String E_C_USUARIO = "E_C_USUARIO";

	public DeleteRolesApoyo(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_USUARIO, OracleTypes.CHAR));
		compile();
	}

	@SuppressWarnings("rawtypes")
	public Map execute(String codUsuario) {

		Map<String,Object> inputs = new HashMap<String,Object>();

		log.info("**** INI " + SPROC_NAME + "****");

		log.info("E_C_USUARIO--->" + codUsuario + "<---");

		log.info("**** FIN " + SPROC_NAME + "****");

		inputs.put(E_C_USUARIO, codUsuario);

		return super.execute(inputs);
	}
}
