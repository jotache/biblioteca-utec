package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;

public class GetReservaSalaById extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetReservaSalaById.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA + ".PKG_BIB_WEB.SP_SEL_RESERVA_X_ID";
	private static final String E_C_CODRECURSO = "E_C_CODRECURSO";
	private static final String E_V_SECUENCIA = "E_V_SECUENCIA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";

	public GetReservaSalaById(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODRECURSO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_SECUENCIA, OracleTypes.VARCHAR));
	    declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new RecursoMapper()));
	    compile();
	}
	
    public Map execute(String codRecurso,String codSecuencia) {    
    	Map inputs = new HashMap();
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_C_CODRECURSO:" + codRecurso);
    	log.info("E_V_SECUENCIA:" + codSecuencia);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	inputs.put(E_C_CODRECURSO, codRecurso);
    	inputs.put(E_V_SECUENCIA, codSecuencia);
        return super.execute(inputs);
    }
    
    final class RecursoMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Sala sala = new Sala();
			sala.setCodReserva(rs.getString("CODRESERVA")== null ? "" : rs.getString("CODRESERVA") );
        	sala.setCodSala(rs.getString("COD_SALA")== null ? "" : rs.getString("COD_SALA") );
        	sala.setCodTipoSala(rs.getString("CODTIPOSALA")== null ? "" : rs.getString("CODTIPOSALA") );
        	sala.setNombreSala(rs.getString("NOMBRESALA")== null ? "" : rs.getString("NOMBRESALA") );
        	sala.setFechaReserva(rs.getString("FECHARESERVA")== null ? "" : rs.getString("FECHARESERVA") );
        	sala.setHoraIni(rs.getString("HORA_INI")== null ? "" : rs.getString("HORA_INI") );
        	sala.setHoraFin(rs.getString("HORA_FIN")== null ? "" : rs.getString("HORA_FIN") );
        	sala.setUsuReserva(rs.getString("USU_RESERVA")== null ? "" : rs.getString("USU_RESERVA") );
        	sala.setCodEstado(rs.getString("CODESTADO")== null ? "" : rs.getString("CODESTADO") );
        	sala.setEstado(rs.getString("ESTADO")== null ? "" : rs.getString("ESTADO") );
        	sala.setCodUsuario(rs.getString("CODUSUARIO")== null ? "" : rs.getString("CODUSUARIO") );
        	sala.setUsuario(rs.getString("USUARIO")== null ? "" : rs.getString("USUARIO") );
        	sala.setCapacidadMax(rs.getString("CAPACIDAD_SALA")== null ? "" : rs.getString("CAPACIDAD_SALA") );
        	sala.setReservas(rs.getString("RESERVAS")== null ? "" : rs.getString("RESERVAS") );
        	sala.setLoginUsuario(rs.getString("LOGIN_USUARIO")== null ? "" : rs.getString("LOGIN_USUARIO") );
			return sala;
		}
    	
    }
}
