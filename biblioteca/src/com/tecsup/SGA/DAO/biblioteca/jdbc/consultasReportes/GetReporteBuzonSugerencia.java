package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;

public class GetReporteBuzonSugerencia extends StoredProcedure{
	 private static Log log = LogFactory.getLog(GetReporteBuzonSugerencia.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.sp_sel_report_buzon_sugerencia";

	 	private static final String E_V_PERIODO = "E_V_PERIODO";
	 	private static final String E_V_CODMATERIAL = "E_V_CODMATERIAL";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetReporteBuzonSugerencia(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_V_PERIODO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_CODMATERIAL, OracleTypes.CHAR)); 
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteBuzonMapper()));
	        compile();
	    }

	    public Map execute(String periodo, String codTipoMaterial){
	    	
	    	Map inputs = new HashMap();
	    	
	    	log.info("*** INI "  + SPROC_NAME + " ****");
	    	log.info("E_V_PERIODO:" + periodo);
	    	log.info("E_V_CODMATERIAL:" + codTipoMaterial);
	    	log.info("*** FIN "  + SPROC_NAME + " ****");
	    	
	    	inputs.put(E_V_PERIODO,periodo);
	    	inputs.put(E_V_CODMATERIAL,codTipoMaterial);
	    	
	        return super.execute(inputs);

	    }
	    
	    final class ReporteBuzonMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	Reporte reporte = new Reporte();
	        	
	        	reporte.setFechaReg(rs.getString("FECHA_REGISTRO") == null ? "" : rs.getString("FECHA_REGISTRO") );
	        	reporte.setTipoSugerencia(rs.getString("TIPO_SUGERENCIA") == null ? "" : rs.getString("TIPO_SUGERENCIA") );
	        	reporte.setDetalle(rs.getString("DETALLE") == null ? "" : rs.getString("DETALLE") );
	        	reporte.setCalificacion(rs.getString("CALIFICACION") == null ? "" : rs.getString("CALIFICACION") );
	        	reporte.setOrientadoA(rs.getString("ORIENTADO_A") == null ? "" : rs.getString("ORIENTADO_A") );
	        	reporte.setUsuario(rs.getString("USUARIO") == null ? "" : rs.getString("USUARIO") );
	        	reporte.setEstado(rs.getString("ESTADO") == null ? "" : rs.getString("ESTADO") );
	        	reporte.setSugerencia(rs.getString("SUGERENCIA") == null ? "" : rs.getString("SUGERENCIA"));
	            return reporte;
	        }

	    }
}

