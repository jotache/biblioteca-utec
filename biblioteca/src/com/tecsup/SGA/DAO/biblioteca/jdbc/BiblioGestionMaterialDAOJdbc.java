package com.tecsup.SGA.DAO.biblioteca.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jfree.util.Log;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.biblioteca.BiblioGestionMaterialDAO;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.DeleteArchAdjuntoxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.DeleteDescriptorxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.DeleteIngresosxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.DeleteMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllArchAdjuntoxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllDatosUsuarioBib;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllDescriptorxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllDetalleMaterialFichaBibliografica;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllHistorialPrestamoxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllHistorialProrrogaxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllIngresosxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllMaterialDetalle;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllMaterialesxFiltro;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllReservaByAlumno;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllServicioByCodUsuario;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAlumnoXCodigo;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetLibrosDeudaPorAlumno;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetLibrosPrestadosPorAlumno;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetMaterialxIngreso;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetSedesBibPorCodUsuario;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetUsuariosXTipo;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.InsertArchivoAdjuntoxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.InsertDescriptorxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.InsertDevolucion;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.InsertIngresosxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.InsertMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.InsertPrestamoxAlumno;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.UpdateIngresosxMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.UpdateMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.GetAllParametrosGenerales;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.GetAllTablaDetalleBiblioteca;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.InsertTablaDetalleBiblioteca;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetAllReservaByMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.DeleteReservasMaterial;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetSedesByEvaluador;
import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ParametrosReservados;
import com.tecsup.SGA.modelo.ServiciosUsuarios;


public class BiblioGestionMaterialDAOJdbc extends JdbcDaoSupport implements BiblioGestionMaterialDAO{

	public List getAllMaterialesxFiltro(String txhCodigoUnico,String txtCodigo, String txtNroIngreso,
			String cboTipoMaterial, String cboBuscarPor, String txtTitulo,
			String cboIdioma, String cboAnioIni, String cboAnioFin,
			String txtFechaReservaIni, String txtFechaReservaFin, String sede) {
		
		GetAllMaterialesxFiltro getAllMaterialesxFiltro = new GetAllMaterialesxFiltro(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllMaterialesxFiltro.execute( txhCodigoUnico,txtCodigo,  txtNroIngreso,
				 cboTipoMaterial,  cboBuscarPor,  txtTitulo,
				 cboIdioma,  cboAnioIni,  cboAnioFin,
				 txtFechaReservaIni,  txtFechaReservaFin,sede);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAllMaterialDetalle(String txhCodUnico) {
		GetAllMaterialDetalle getAllMaterialDetalle = new GetAllMaterialDetalle(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllMaterialDetalle.execute(txhCodUnico);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAllIngresosxMaterial(String txhCodMaterial,String txhCodUnico) {
		GetAllIngresosxMaterial getAllIngresosxMaterial = new GetAllIngresosxMaterial(this.getDataSource());
		HashMap outputs = (HashMap)getAllIngresosxMaterial.execute(txhCodMaterial,txhCodUnico);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
		
	}

	public List getAllDescriptorxMaterial(String txhCodUnico) {
		GetAllDescriptorxMaterial getAllDescriptorxMaterial = new GetAllDescriptorxMaterial(this.getDataSource());
		HashMap outputs = (HashMap)getAllDescriptorxMaterial.execute(txhCodUnico);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAllArchAdjuntoxMaterial(String txhCodUnico) {
		GetAllArchAdjuntoxMaterial getAllArchAdjuntoxMaterial = new GetAllArchAdjuntoxMaterial(this.getDataSource());
		HashMap outputs = (HashMap)getAllArchAdjuntoxMaterial.execute(txhCodUnico);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public String InsertArchivoAdjuntoxMaterial(String txhCodigoUnico,
			String txtTitulo, String nomArchivo, String usuario) {
		
		InsertArchivoAdjuntoxMaterial insertArchivoAdjuntoxMaterial = new InsertArchivoAdjuntoxMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)insertArchivoAdjuntoxMaterial.execute( txhCodigoUnico,
				 txtTitulo,  nomArchivo,  usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String deleteArchAdjuntoxMaterial(String txhCodigoUnico,
			String usuario) {
		
		DeleteArchAdjuntoxMaterial deleteArchAdjuntoxMaterial = new DeleteArchAdjuntoxMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteArchAdjuntoxMaterial.execute(  txhCodigoUnico, usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String insertDescriptorxMaterial(String codMaterial, String cadena,
			String nroRegistros, String usuario) {
		
		InsertDescriptorxMaterial insertDescriptorxMaterial = new InsertDescriptorxMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)insertDescriptorxMaterial.execute(  codMaterial,  cadena,
				 nroRegistros,  usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String deleteDescriptorxMaterial(String txhCodigoUnico,
			String usuario) {
		
		DeleteDescriptorxMaterial deleteDescriptorxMaterial = new DeleteDescriptorxMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteDescriptorxMaterial.execute(  txhCodigoUnico, usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String insertIngresosxMaterial(String txhCodigoUnico,
			String txtFecIngreso, String txtFecBaja, String cboProcedencia,
			String cboMoneda, String txtPrecio, String cboEstado,
			String txtObservacion, String usuario) {
		
		InsertIngresosxMaterial insertIngresosxMaterial = new InsertIngresosxMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)insertIngresosxMaterial.execute(  txhCodigoUnico,
				 txtFecIngreso,  txtFecBaja,  cboProcedencia,
				 cboMoneda,  txtPrecio,  cboEstado,
				 txtObservacion,  usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}//

	public String updateIngresosxMaterial(String codUnico, String fechaIngreso, String fechaBaja,
			String codProcedencia, String codMoneda, String precio, String codEstado, 
			String observaciones, String usuario) {
		
		UpdateIngresosxMaterial updateIngresosxMaterial = new UpdateIngresosxMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)updateIngresosxMaterial.execute(codUnico, fechaIngreso, fechaBaja,
				codProcedencia, codMoneda, precio, codEstado, observaciones, usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}//DeleteIngresosxMaterial
	
	public String deleteIngresosxMaterial(String codUnico, String usuario) {
		
		DeleteIngresosxMaterial deleteIngresosxMaterial = new DeleteIngresosxMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteIngresosxMaterial.execute(codUnico, usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String insertMaterial(String tipMaterial, String titulo,
			String codAutor, String codDewey, String nroPaginas,
			String codEditorial, String nroEditorial, String codPais,
			String codCiudad, String fecPublicacion, String urlArchDigital,
			String codIdioma, String isbn, String formato, String codTipoVideo,
			String codProcedencia, String codMoneda, String precio,
			String indPrestamoSala, String indPrestamoCasa, String indReserva,
			String observaciones, String imagen, String usuario,String ubicacionFisica,String codSede
			,String codGenerado,String prefijo,String sufijo,String frecuencia,String mencionSerie, String contenido) {
		
	InsertMaterial insertMaterial = new InsertMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)insertMaterial.execute(  tipMaterial,  titulo,
				 codAutor,  codDewey,  nroPaginas,
				 codEditorial,  nroEditorial,  codPais,
				 codCiudad,  fecPublicacion,  urlArchDigital,
				 codIdioma,  isbn,  formato,  codTipoVideo,
				 codProcedencia,  codMoneda,  precio,
				 indPrestamoSala,  indPrestamoCasa,  indReserva,
				 observaciones,  imagen,  usuario,ubicacionFisica,
				 codSede,codGenerado,prefijo,sufijo, frecuencia,mencionSerie, contenido);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String updateMaterial(String codMaterial, String tipMaterial,
			String titulo, String codAutor, String codDewey, String nroPaginas,
			String codEditorial, String nroEditorial, String codPais,
			String codCiudad, String fecPublicacion, String urlArchDigital,
			String codIdioma, String isbn, String formato, String codTipoVideo,
			String indPrestamoSala, String indPrestamoCasa, String indReserva,
			String imagen, String usuario,String ubicacionFisica,String codSede
			,String codGenerado,String prefijo,String sufijo,String frecuencia,String mencionSerie, String contenido) {
		UpdateMaterial updateMaterial = new UpdateMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)updateMaterial.execute( codMaterial,  tipMaterial,
				 titulo,  codAutor,  codDewey,  nroPaginas,
				 codEditorial,  nroEditorial,  codPais,
				 codCiudad,  fecPublicacion,  urlArchDigital,
				 codIdioma,  isbn,  formato,  codTipoVideo,
				 indPrestamoSala,  indPrestamoCasa,  indReserva,
				 imagen,  usuario,ubicacionFisica,codSede
				 , codGenerado, prefijo, sufijo, frecuencia,mencionSerie, contenido);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String deleteMaterial(String codMaterial, String usuario) {
		
		DeleteMaterial deleteMaterial = new DeleteMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteMaterial.execute(codMaterial, usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List getAllHistorialPrestamoxMaterial(String txhCodUnico) {
		GetAllHistorialPrestamoxMaterial getAllHistorialPrestamoxMaterial = new GetAllHistorialPrestamoxMaterial(this.getDataSource());
		HashMap outputs = (HashMap)getAllHistorialPrestamoxMaterial.execute(txhCodUnico);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllHistorialProrrogaxMaterial(String txhNroIngreso, String txhCodUsuario,String sede) {
		GetAllHistorialProrrogaxMaterial getAllHistorialProrrogaxMaterial = new GetAllHistorialProrrogaxMaterial(this.getDataSource());
		HashMap outputs = (HashMap)getAllHistorialProrrogaxMaterial.execute(txhNroIngreso, txhCodUsuario,sede);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAlumnoXCodigo(String codigo,String indTipoPrestamo,String tipoBusquedaCodUsu,String sede,String numIngreso) {
		GetAlumnoXCodigo getAlumnoXCodigo = new GetAlumnoXCodigo(this.getDataSource());
		HashMap outputs = (HashMap)getAlumnoXCodigo.execute(codigo,indTipoPrestamo,tipoBusquedaCodUsu,sede,numIngreso);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getMaterialxIngreso(String codigo,String tipo,String sede,String codUsuario) {
		GetMaterialxIngreso getMaterialxIngreso = new GetMaterialxIngreso(this.getDataSource());
		HashMap outputs = (HashMap)getMaterialxIngreso.execute(codigo,tipo,sede,codUsuario);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public String insertDevolucion(String codPrestamo, String usuPrestamo,
			String flgSancion, String usuCreacion) {
		InsertDevolucion insertDevolucion = new InsertDevolucion(this.getDataSource());
		
		HashMap inputs = (HashMap)insertDevolucion.execute(   codPrestamo,  usuPrestamo,
				 flgSancion,  usuCreacion);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String insertPrestamoxAlumno(String codMaterial, String nroIngreso,
			String codUsuPrestamo, String indProrroga, String usuCreacion,String indPrestamo, String indReserva,
			String fecPrestamo, Long codMultiple) {
		
		InsertPrestamoxAlumno insertPrestamoxAlumno = new InsertPrestamoxAlumno(this.getDataSource());
		
		HashMap inputs = (HashMap)insertPrestamoxAlumno.execute( codMaterial,  nroIngreso,
				 codUsuPrestamo,  indProrroga,  usuCreacion,indPrestamo, indReserva,
					 fecPrestamo, codMultiple);
		if (!inputs.isEmpty())
		{							   									
			return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;	
		
	}

	public List getAllDetalleMaterialFichaBibliografica(String txhCodUnico) {
		GetAllDetalleMaterialFichaBibliografica obj = new GetAllDetalleMaterialFichaBibliografica(this.getDataSource());
		HashMap outputs = (HashMap)obj.execute(txhCodUnico);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllReservaByAlumno(String codUsuario,String nroIngreso)
	{
		GetAllReservaByAlumno getAllReservaByAlumno = new GetAllReservaByAlumno(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllReservaByAlumno.execute(codUsuario,nroIngreso);
		if(!inputs.isEmpty()){
			return (List)inputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllReservaByMaterial(String codMaterial){
		GetAllReservaByMaterial getAllReservaByMaterial = new GetAllReservaByMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllReservaByMaterial.execute(codMaterial);
		if(!inputs.isEmpty()){
			return (List)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String deleteReservasMaterial(String cadena, String nroReg, String usuCrea){
		DeleteReservasMaterial deleteReservasMaterial = new DeleteReservasMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteReservasMaterial.execute(cadena, nroReg, usuCrea);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	public List getAllDatosUsuarioBib(String codUsuario) {
		GetAllDatosUsuarioBib getAllDatosUsuarioBib = new GetAllDatosUsuarioBib(this.getDataSource());
		HashMap outputs = (HashMap)getAllDatosUsuarioBib.execute(codUsuario);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	//jhpr 2008-08-15
	public List getUsuariosXCodigo(String codigo, String tipo) {
		GetUsuariosXTipo datos = new GetUsuariosXTipo(this.getDataSource());
		HashMap outputs  = (HashMap) datos.execute(codigo,tipo);
		if (!outputs.isEmpty())
			return (List) outputs.get("S_C_RECORDSET");
		
		return null;
	}

	public List<SedeBean> GetSedesBibPorUsuario(String codUsuario) {
		GetSedesBibPorCodUsuario getSedesByEvaluador = new GetSedesBibPorCodUsuario(this.getDataSource());
		HashMap outputs = (HashMap)getSedesByEvaluador.execute(codUsuario);		
		if (!outputs.isEmpty()){		
			return (List<SedeBean>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public int getCantISBNPorSede(String isbn, String sede,Long codMaterial) {
						
		JdbcTemplate jt = getJdbcTemplate();
		codMaterial = (codMaterial==null?0:codMaterial);
		String sql ="";
		if(codMaterial==0)
			sql = "Select COUNT(M.MBIB_ID) from BIB_MATERIAL M where UPPER(TRIM(M.MBIB_COD_ISBN))='"+ isbn.toUpperCase().trim() +"' and M.MBIB_COD_SEDE='"+ sede.toUpperCase() +"'";
		else
			sql = "Select COUNT(M.MBIB_ID) from BIB_MATERIAL M where UPPER(TRIM(M.MBIB_COD_ISBN))='"+ isbn.toUpperCase().trim() +"' and M.MBIB_COD_SEDE='"+ sede.toUpperCase() +"' And M.MBIB_ID <> TO_NUMBER(" + codMaterial.toString() + ")";
		return jt.queryForInt(sql);		
	}
		
	public String getEstadoIngreso(String nCodIngreso) {	
						
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		int nroreg = 0;
		String sql = "select count(*) from bib_material_det where mden_secuencia = " + nCodIngreso;
		Log.info("EstadoIngreso: " + sql);
		nroreg = jdbcTemplate.queryForInt(sql);
		if (nroreg>0){
			sql = "select mdec_est_reg from bib_material_det where mden_secuencia = ?";
			return (String) jdbcTemplate.queryForObject(
					sql, new Object[] { nCodIngreso }, String.class);
		}else{
			return "";
		}
				
		}

	public boolean categoriaDuplicada(String codMaterial, String codGenerado) {
						
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		int nroreg = 0;
		String sql = "select count(m.MBIB_ID) " ;      
		sql += "from BIB_MATERIAL M where UPPER(TRIM(M.MBIB_CODGENERADO)) = '" +  codGenerado + "'" ;
				
		if (!codMaterial.equals(""))
			sql += "and M.MBIB_ID <> " + codMaterial;
		
		//System.out.println("categoriaDuplicada sql:" + sql);
		
		nroreg = jdbcTemplate.queryForInt(sql);
		if (nroreg>0) {
			return true;
		}
		return false;
	}	
	
	public ServiciosUsuarios getAllServicioByCodUsuario(String codUsuario){
		GetAllServicioByCodUsuario getAllServicioByCodUsuario = new GetAllServicioByCodUsuario(this.getDataSource());
		HashMap outputs = (HashMap)getAllServicioByCodUsuario.execute(codUsuario);
		if(!outputs.isEmpty()){
			ArrayList lista = (ArrayList)outputs.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (ServiciosUsuarios)lista.get(0);
			}
		}
		
		return null;
	}

	
	public Long obtenerCodigoPrestamoMultiple() {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		Long result = null;
		String sql = "select bib_mat_prestamo_multiple.nextval from dual";
		result = jdbcTemplate.queryForLong(sql);
		return result;
	}

	public List<MaterialxIngresoBean> getListaLibrosDeudaXUsuario(
			String codUsuario) {
		GetLibrosDeudaPorAlumno getLib = new GetLibrosDeudaPorAlumno(this.getDataSource());
		HashMap outputs = (HashMap)getLib.execute(codUsuario);		
		if (!outputs.isEmpty()){		
			return (List<MaterialxIngresoBean>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	@Override
	public List<MaterialxIngresoBean> getListaLibrosPrestadosXUsuario(
			String codUsuario) {
		GetLibrosPrestadosPorAlumno getLib = new GetLibrosPrestadosPorAlumno(this.getDataSource());
		HashMap outputs = (HashMap)getLib.execute(codUsuario);		
		if (!outputs.isEmpty()){		
			return (List<MaterialxIngresoBean>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
}
