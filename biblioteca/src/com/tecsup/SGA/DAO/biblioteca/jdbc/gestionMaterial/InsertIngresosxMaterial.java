package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertIngresosxMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertIngresosxMaterial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_GESTION_MAT.SP_INS_INGRESOS_X_MATERIAL";
	
	private static final String E_C_COD_MATERIAL = "E_C_COD_MATERIAL"; 
	private static final String E_V_FECHA_INGRESO = "E_V_FECHA_INGRESO";
	private static final String E_V_NRO_VOLUMEN = "E_V_NRO_VOLUMEN";
	private static final String E_C_COD_PROCEDENCIA = "E_C_COD_PROCEDENCIA";
	private static final String E_C_COD_MONEDA = "E_C_COD_MONEDA";
	private static final String E_V_PRECIO = "E_V_PRECIO";
	private static final String E_C_COD_ESTADO = "E_C_COD_ESTADO";
	private static final String E_V_OBSERVACIONES = "E_V_OBSERVACIONES";	
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertIngresosxMaterial(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_MATERIAL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_FECHA_INGRESO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NRO_VOLUMEN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_PROCEDENCIA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_MONEDA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_PRECIO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_ESTADO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_OBSERVACIONES, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codMaterial,
			String fecIngreso, String fecBaja, String codProcedencia,String codMoneda
			,String precio,String codEstado,String observaciones,String usuario) {
		
		log.info("****** INI "+SPROC_NAME+" *****");
		log.info("E_C_COD_MATERIAL:"+ codMaterial);
		log.info("E_V_FECHA_INGRESO:"+ fecIngreso);
		log.info("E_V_NRO_VOLUMEN:"+ fecBaja);
		log.info("E_C_COD_PROCEDENCIA:"+ codProcedencia);
		log.info("E_C_COD_MONEDA:"+ codMoneda);
		log.info("E_V_PRECIO:"+ precio);
		log.info("E_C_COD_ESTADO:"+ codEstado);
		log.info("E_V_OBSERVACIONES:"+ observaciones);
		log.info("E_V_USU_CREA:"+ usuario);
		log.info("****** FIN "+SPROC_NAME+" *****");
		
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_MATERIAL, codMaterial);
	inputs.put(E_V_FECHA_INGRESO, fecIngreso);
	inputs.put(E_V_NRO_VOLUMEN, fecBaja);
	inputs.put(E_C_COD_PROCEDENCIA, codProcedencia);
	inputs.put(E_C_COD_MONEDA, codMoneda);
	inputs.put(E_V_PRECIO, precio);
	inputs.put(E_C_COD_ESTADO, codEstado);
	inputs.put(E_V_OBSERVACIONES, observaciones);
	inputs.put(E_V_USU_CREA, usuario);	
	
	return super.execute(inputs);
	}

	
}
