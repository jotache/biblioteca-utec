package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.DescriptorxMaterialBean;
import com.tecsup.SGA.bean.IngresosxMaterialBean;
import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.bean.UsuarioBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllDatosUsuarioBib extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllDatosUsuarioBib.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_GESTION_MAT.SP_SEL_DATOS_USUARIO_BIB";

	 	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO ";   
	    private static final String S_C_RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllDatosUsuarioBib(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));       
	        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
	        compile();
	    }

	    public Map execute(String codigoUsuario){
	    	
	    	log.info("**** INI " + SPROC_NAME  + " *****");
			log.info("E_V_CODUSUARIO:"+codigoUsuario);			
			log.info("**** FIN " + SPROC_NAME  + " *****");
			
	    	Map inputs = new HashMap();	    	
	    	inputs.put(E_V_CODUSUARIO,codigoUsuario);	        
	        return super.execute(inputs);

	    }
	    
	    final class ObjMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	UsuarioBean bean = new UsuarioBean();
	        	
	        	bean.setNomUsuario(rs.getString("NOMBREUSUARIO")== null ? "0" : rs.getString("NOMBREUSUARIO"));
	        	bean.setIndTipoUsuario(rs.getString("IND_TIPO_USUARIO")== null ? "0" : rs.getString("IND_TIPO_USUARIO"));
	        	bean.setCodTipoUsuario(rs.getString("COD_TIPO_USUARIO")== null ? "0" : rs.getString("COD_TIPO_USUARIO"));
	        	bean.setNomTipoUsuario(rs.getString("NOM_TIPO_USUARIO")== null ? "0" : rs.getString("NOM_TIPO_USUARIO"));
	        	bean.setNomEspecialidad(rs.getString("NOM_ESPECIALIDAD")== null ? "0" : rs.getString("NOM_ESPECIALIDAD"));
	        	bean.setNomCiclo(rs.getString("NOM_CICLO")== null ? "0" : rs.getString("NOM_CICLO"));
	        	bean.setNomCargoPersonal(rs.getString("NOM_CARGO_PERSONAL")== null ? "0" : rs.getString("NOM_CARGO_PERSONAL"));	        	
	        	
	            return bean;
	        }

	    }
}

