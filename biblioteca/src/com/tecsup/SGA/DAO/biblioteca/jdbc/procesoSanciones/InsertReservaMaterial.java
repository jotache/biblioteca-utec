package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertReservaMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertReservaMaterial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_PROCESO_SANCIONES.SP_INS_RESERVA_MATERIAL";

	private static final String E_V_COD_MATERIAL = "E_V_COD_MATERIAL";	
	private static final String E_V_FEC_RESERVA = "E_V_FEC_RESERVA";
	private static final String E_V_COD_USU_RESERVA = "E_V_COD_USU_RESERVA";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertReservaMaterial(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_COD_MATERIAL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FEC_RESERVA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USU_RESERVA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codMaterial,String fecReserva,String codUsuarioReserva,String codUsuario) {    	
	
		log.info("****" + SPROC_NAME + "******");
		log.info("E_V_COD_MATERIAL:"+codMaterial);
		log.info("E_V_FEC_RESERVA:"+fecReserva);
		log.info("E_V_COD_USU_RESERVA:"+codUsuarioReserva);
		log.info("E_V_COD_USUARIO:"+codUsuario);
		log.info("****" + SPROC_NAME + "******");
		
		Map inputs = new HashMap();
		inputs.put(E_V_COD_MATERIAL,codMaterial);
		inputs.put(E_V_FEC_RESERVA, fecReserva);
		inputs.put(E_V_COD_USU_RESERVA, codUsuarioReserva);
		inputs.put(E_V_COD_USUARIO, codUsuario);
		
	
	return super.execute(inputs);
	}

	
}
