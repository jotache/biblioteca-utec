package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.bean.ReservasByMaterial;
	
public class GetAllReservaByMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllReservaByMaterial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_gestion_mat.SP_SEL_RESERVAS_X_MATERIAL";
    
 	private static final String E_V_CODIGO = "E_V_CODIGO";
      private static final String S_C_RECORDSET = "S_C_RECORDSET";
 
 public GetAllReservaByMaterial(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ReservasByMaterialMapper()));
        compile();
    }

    public Map execute(String codMaterial) {
    
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("E_V_CODIGO: "+codMaterial);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();
    	inputs.put(E_V_CODIGO, codMaterial);
        return super.execute(inputs);
    }
    
    final class ReservasByMaterialMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ReservasByMaterial reservasByMaterial = new ReservasByMaterial();
        	
        	reservasByMaterial.setCodReserva(rs.getString("COD_RESERVA")== null ? "" : rs.getString("COD_RESERVA"));
        	reservasByMaterial.setCodMaterial(rs.getString("COD_MATERIAL")== null ? "" : rs.getString("COD_MATERIAL"));
        	reservasByMaterial.setCodigo(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
        	reservasByMaterial.setTipoMaterial(rs.getString("TIPO_MATERIAL")== null ? "" : rs.getString("TIPO_MATERIAL"));
        	reservasByMaterial.setTitulo(rs.getString("TITULO")== null ? "" : rs.getString("TITULO"));
        	reservasByMaterial.setUsuario(rs.getString("USUARIO")== null ? "" : rs.getString("USUARIO"));
        	reservasByMaterial.setTipoUsuario(rs.getString("TIPO_USUARIO")== null ? "" : rs.getString("TIPO_USUARIO"));
        	reservasByMaterial.setFechaReserva(rs.getString("FEC_RESERVA")== null ? "" : rs.getString("FEC_RESERVA"));
        	reservasByMaterial.setHoraReserva(rs.getString("HORA_RESERVA")== null ? "" : rs.getString("HORA_RESERVA"));
        	reservasByMaterial.setFlgEliminar(rs.getString("FLAG_ELIMINAR")== null ? "" : rs.getString("FLAG_ELIMINAR"));
        	
            return reservasByMaterial;
        }

    }
}
