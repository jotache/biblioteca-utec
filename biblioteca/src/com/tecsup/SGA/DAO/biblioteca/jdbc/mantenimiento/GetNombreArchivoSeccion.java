package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetNombreArchivoSeccion extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetNombreArchivoSeccion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_mantto_config.sgaf_fecha_archivo";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	public GetNombreArchivoSeccion(DataSource dataSource) {
		super(dataSource, SPROC_NAME);		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute() {
		log.info("EXECUTE: " + SPROC_NAME);
		Map inputs = new HashMap();
		return super.execute(inputs);
	
	}
}
