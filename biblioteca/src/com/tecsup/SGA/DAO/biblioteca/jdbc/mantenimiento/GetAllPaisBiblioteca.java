package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllPaisBiblioteca extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllPaisBiblioteca.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_comun.sp_sel_pais";
    
 	private static final String E_V_CODIGO = "E_V_CODIGO";
    private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
    private static final String E_C_TIPO = "E_C_TIPO";

   
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetAllPaisBiblioteca(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
        
       
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codigo, String descripcion, String tipoOrden) {
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_V_CODIGO:" + codigo);
    	log.info("E_V_DESCRIPCION:" + descripcion);
    	log.info("E_C_TIPO:" + tipoOrden);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();    	
    	inputs.put(E_V_CODIGO, codigo);
        inputs.put(E_V_DESCRIPCION, descripcion);
        inputs.put(E_C_TIPO, tipoOrden);
        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Pais pais= new Pais();
        	
        	pais.setPaisId(rs.getString("PAIS_ID"));//valor1
        	pais.setPaisCodigo(rs.getString("CODIGO"));//ttde_id
        	pais.setPaisDescripcion(rs.getString("DESCRIPCION"));//descripcion_hijo
        	
            return pais;
        }

    }
}
