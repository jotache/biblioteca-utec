package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertPrestamoxAlumno extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertPrestamoxAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_GESTION_MAT.SP_INS_PRESTAMO_X_ALUMNO";

	private static final String E_V_COD_MATERIAL = "E_V_COD_MATERIAL"; 
	private static final String E_V_NRO_INGRESO = "E_V_NRO_INGRESO";
	private static final String E_V_COD_USU_PRESTAMO = "E_V_COD_USU_PRESTAMO";
	private static final String E_V_IND_PRORROGA = "E_V_IND_PRORROGA";	
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	private static final String E_C_IND_PRESTAMO = "E_C_IND_PRESTAMO"; //E_C_IND_RESERVA IN CHAR,
	private static final String E_C_IND_RESERVA = "E_C_IND_RESERVA";
	private static final String E_V_FEC_PRESTAMO = "E_V_FEC_PRESTAMO";	
	private static final String E_V_CODIGO_MULTIPLE = "E_V_CODIGO_MULTIPLE";
		
	private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public InsertPrestamoxAlumno(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_COD_MATERIAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NRO_INGRESO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_COD_USU_PRESTAMO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_IND_PRORROGA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_IND_PRESTAMO, OracleTypes.CHAR));	
	declareParameter(new SqlParameter(E_C_IND_RESERVA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_FEC_PRESTAMO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CODIGO_MULTIPLE, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(
			String codMaterial,
			String nroIngreso,
			String codUsuPrestamo,
			String indProrroga,
			String usuCreacion,
			String indPrestamo,
			String indReserva,
			String fecPrestamo,
			Long codMultiple
			) {    	
	
		Map inputs = new HashMap();		
		log.info("**** INI " + SPROC_NAME + "*****");
		log.info("codMaterial:"+codMaterial);
		log.info("nroIngreso:"+nroIngreso);
		log.info("codUsuPrestamo:"+codUsuPrestamo);
		log.info("indProrroga:"+indProrroga);
		log.info("usuCreacion:"+usuCreacion);
		log.info("indPrestamo:"+indPrestamo);
		log.info("indReserva:"+indReserva);
		log.info("fecPrestamo:"+fecPrestamo);
		log.info("codMultiple:"+codMultiple);
		log.info("**** FIN " + SPROC_NAME + "*****");
	
		inputs.put(E_V_COD_MATERIAL, codMaterial);
		inputs.put(E_V_NRO_INGRESO, nroIngreso);
		inputs.put(E_V_COD_USU_PRESTAMO, codUsuPrestamo);
		inputs.put(E_V_IND_PRORROGA, indProrroga);
		inputs.put(E_V_USU_CREA, usuCreacion);
		inputs.put(E_C_IND_PRESTAMO, indPrestamo);
		inputs.put(E_C_IND_RESERVA, indReserva);
		inputs.put(E_V_FEC_PRESTAMO, fecPrestamo);
		if(codMultiple==null)
			inputs.put(E_V_CODIGO_MULTIPLE, "");
		else
			inputs.put(E_V_CODIGO_MULTIPLE, codMultiple.toString());
		
		return super.execute(inputs);
	}

	
}
