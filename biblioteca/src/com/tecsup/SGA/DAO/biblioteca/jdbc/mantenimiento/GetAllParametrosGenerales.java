package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ParametrosReservados;

public class GetAllParametrosGenerales extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllParametrosGenerales.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_mantto_config.sp_sel_parametros_reserva";
    
 	private static final String E_C_TIPT_ID = "E_C_TIPT_ID";
      private static final String S_C_RECORDSET = "S_C_RECORDSET";
 
 public GetAllParametrosGenerales(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_C_TIPT_ID, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String tipId) {
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_C_TIPT_ID: "+tipId);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();    	
    	inputs.put(E_C_TIPT_ID, tipId);
      
        return super.execute(inputs);
    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ParametrosReservados feriados = new ParametrosReservados();
        	feriados.setCodSec(rs.getString("CODIGOSECUENCIAL"));//ttde_id
        	feriados.setEsMaDi(rs.getString("EST_MAX_HRAS_RESER_DIA"));//valor1
        	feriados.setEsMiTo(rs.getString("EST_MINUTOS_TOLERANCIA"));//valor2
        	feriados.setEsDiPe(rs.getString("EST_DIAS_PENALIDAD"));//valor3
        	feriados.setInMaDi(rs.getString("INT_MAX_HRAS_RESER_DIA"));//valor4
        	feriados.setInMiTo(rs.getString("INT_MINUTOS_TOLERANCIA"));//valor5
        	feriados.setInDiPe(rs.getString("INT_DIAS_PENALIDAD"));//valor6
        	feriados.setBiNroDiPe(rs.getString("MAT_DIAS_PENALIDAD"));//valor7
        	feriados.setCantMostrarWeb(rs.getString("CANT_MOSTRAR_WEB"));//valor8
            return feriados;
        }

    }
}
