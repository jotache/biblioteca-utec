package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;

public class GetReporteMovPrestDevDiaHra extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReporteMovPrestDevDiaHra.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.sp_sel_report_mov_pres_dev_dia";

	 	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	 	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetReporteMovPrestDevDiaHra(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReportePrestDevDiaHraMapper()));
	        compile();
	    }

	    public Map execute(String fecIni, String fecFin,String sede){
	    	
	    	log.info("**** INI " + SPROC_NAME + " ****");
	    	log.info("E_V_FEC_INI:" + fecIni);
	    	log.info("E_V_FEC_FIN:" + fecFin);
	    	log.info("E_C_SEDE:" + sede);
	    	log.info("**** FIN " + SPROC_NAME + " ****");
	    	
	    	Map inputs = new HashMap();	    		    	
	    	inputs.put(E_V_FEC_INI, fecIni);
	    	inputs.put(E_V_FEC_FIN, fecFin);
	    	inputs.put(E_C_SEDE, sede);
	        return super.execute(inputs);

	    }
	    
	    final class ReportePrestDevDiaHraMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	Reporte reporte = new Reporte();
	        	/*
	        	 *  DIA_HORA.FECHA,
        			DIA_HORA.NOM_DIA,
        			DIA_HORA.HORA,
        			AS NRO_PRESTAMO_SALA,
        			AS NRO_PRESTAMO_DOMICILIO,        
        			AS NRO_PRESTAMOS,        
        			AS NRO_DEVOLUCIONES,        
        			AS NRO_MOVIMIENTOS        
	        	 */
	        	reporte.setFechaPresDev(rs.getString(1) == null ? "" : rs.getString(1) );
	        	reporte.setHora(rs.getString(2) == null ? "" : rs.getString(2) );
	        	reporte.setDiaSem(rs.getString(3) == null ? "" : rs.getString(3) );
	        	reporte.setNroPrestSala(rs.getString("NRO_PRESTAMO_SALA") == null ? "" : rs.getString("NRO_PRESTAMO_SALA") );
	        	reporte.setNroPrestCasa(rs.getString("NRO_PRESTAMO_DOMICILIO") == null ? "" : rs.getString("NRO_PRESTAMO_DOMICILIO") );
	        	reporte.setTotalPrestamos(rs.getString("NRO_PRESTAMOS") == null ? "" : rs.getString("NRO_PRESTAMOS") );
	        	reporte.setDevoluciones(rs.getString("NRO_DEVOLUCIONES") == null ? "" : rs.getString("NRO_DEVOLUCIONES") );
	        	reporte.setTotalMovimientos(rs.getString("NRO_MOVIMIENTOS") == null ? "" : rs.getString("NRO_MOVIMIENTOS") );
	        	
	            return reporte;
	        }

	    }
}

