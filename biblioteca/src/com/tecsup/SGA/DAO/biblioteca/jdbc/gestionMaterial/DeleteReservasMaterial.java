package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class DeleteReservasMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(DeleteReservasMaterial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
 	+ ".pkg_bib_gestion_mat.SP_DEL_RESERVAS_MATERIAL";
	
	private static final String E_V_CADENA = "E_V_CADENA";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteReservasMaterial(DataSource dataSource) {
	super(dataSource, SPROC_NAME);

	declareParameter(new SqlParameter(E_V_CADENA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String cadena, String nroReg, String usuCrea) {
	
		log.info("**** INI " + SPROC_NAME  + " *****");
		log.info("E_V_CADENA:"+cadena);
		log.info("E_V_NROREGISTROS:"+nroReg);
		log.info("E_V_USU_CREA:"+usuCrea);
		log.info("**** FIN " + SPROC_NAME  + " *****");
		
	Map inputs = new HashMap();
	inputs.put(E_V_CADENA, cadena);
	inputs.put(E_V_NROREGISTROS, nroReg);
	inputs.put(E_V_USU_CREA, usuCrea);
	
	return super.execute(inputs);
	
	}
}
