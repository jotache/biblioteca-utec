package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.UsuarioBean;
import com.tecsup.SGA.common.CommonConstants;

public class GetUsuariosXTipo extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetUsuariosXTipo.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 												+ ".PKG_BIB_GESTION_MAT.SP_SEL_USUARIO";

	 	private static final String E_V_CODIGO = "E_V_USER";
	 	private static final String E_V_TIPO_USER = "E_V_TIPO_USER";	 	
	    private static final String RECORDSET = "S_C_RECORDSET";

	    public GetUsuariosXTipo(DataSource dataSource){	    	
	    	super(dataSource, SPROC_NAME);
	        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_TIPO_USER, OracleTypes.VARCHAR));	        
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new UsuariosXTipoMapper()));
	        compile();	    	
	    }
	    
	    public Map execute(String codigo, String tipo){	    	
	    	Map inputs = new HashMap();
	    	log.info("*** INI "+SPROC_NAME+" ***");
	    	log.info("E_V_CODIGO:"+codigo);
	    	log.info("E_V_TIPO_USER:"+tipo);
	    	log.info("*** FIN "+SPROC_NAME+" ***");
	    	inputs.put(E_V_CODIGO,codigo);
	    	inputs.put(E_V_TIPO_USER,tipo);	    	
	        return super.execute(inputs);
	    }
	    
	    final class UsuariosXTipoMapper implements RowMapper {

			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				UsuarioBean bean = new UsuarioBean();
				bean.setNomUsuario(rs.getString("NOMBREUSUARIO")== null ? "0" : rs.getString("NOMBREUSUARIO"));
				bean.setCodTipoUsuario(rs.getString("CODUSUARIO")== null ? "0" : rs.getString("CODUSUARIO"));
				return bean;
			}
	    	
	    }
}
