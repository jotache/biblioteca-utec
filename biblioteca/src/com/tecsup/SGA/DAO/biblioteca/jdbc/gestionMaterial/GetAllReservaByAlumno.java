package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Busqueda;

public class GetAllReservaByAlumno extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllReservaByAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_gestion_mat.sp_sel_reservas_x_alumno";
	
	 private static final String E_V_CODIGO = "E_V_CODIGO";
 	 private static final String E_V_NRO_INGRESO = "E_V_NRO_INGRESO";
     private static final String S_C_RECORDSET = "S_C_RECORDSET";
 
 public GetAllReservaByAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NRO_INGRESO, OracleTypes.VARCHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codUsuario,String nroIngreso) {
    
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("E_V_CODIGO: "+codUsuario);
    	log.info("E_V_NRO_INGRESO: "+nroIngreso);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();    	
    	inputs.put(E_V_CODIGO, codUsuario);
    	inputs.put(E_V_NRO_INGRESO, nroIngreso);
      
        return super.execute(inputs);
    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Busqueda busqueda = new Busqueda();
        	
        	busqueda.setCodUnico(rs.getString("COD_RESERVA")== null ? "" : rs.getString("COD_RESERVA"));
        	busqueda.setCodMaterial(rs.getString("COD_MATERIAL")== null ? "" : rs.getString("COD_MATERIAL"));
        	busqueda.setIsbn(rs.getString("NRO_INGRESO")== null ? "" : rs.getString("NRO_INGRESO"));
        	busqueda.setTipoMaterial(rs.getString("NOM_TIPO_MATERIAL")== null ? "" : rs.getString("NOM_TIPO_MATERIAL"));
        	busqueda.setTitulo(rs.getString("NOM_MATERIAL")== null ? "" : rs.getString("NOM_MATERIAL"));
        	busqueda.setFechaBD(rs.getString("FEC_RESERVA")== null ? "" : rs.getString("FEC_RESERVA"));
        	busqueda.setUsuarioReserva(rs.getString("USUARIO_RESERVA")== null ? "" : rs.getString("USUARIO_RESERVA"));
        	busqueda.setNomUsuario(rs.getString("NOM_USUARIO")== null ? "" : rs.getString("NOM_USUARIO"));        	
        	busqueda.setDescripDewey(rs.getString("COD_DEWEY")== null ? "" : rs.getString("COD_DEWEY"));
        	      	
            return busqueda;
        }

    }
}
