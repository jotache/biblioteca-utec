package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertSancionReservaSalas extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertSancionReservaSalas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_PROCESO_SANCIONES.SP_INS_SANCION_RESERVA_SALAS";

	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String E_C_SREC_CODIGO = "E_C_SREC_CODIGO";	
	private static final String E_V_SREN_SECUENCIA = "E_V_SREN_SECUENCIA";
	private static final String E_V_USUARIO = "E_V_USUARIO";
	private static final String E_C_ESTADO_RESERVA = "E_C_ESTADO_RESERVA";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertSancionReservaSalas(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));	
	declareParameter(new SqlParameter(E_C_SREC_CODIGO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_SREN_SECUENCIA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_ESTADO_RESERVA, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(
			String codUsuario,
			String recCodigo,
			String renSecuencia,
			String usuario,
			String estadoReserva
			) {    	
	Map inputs = new HashMap();
	
	log.info("**** INI "  + SPROC_NAME + "******");
	log.info("codUsuario:"+codUsuario);
	log.info("recCodigo:"+recCodigo);
	log.info("renSecuencia:"+renSecuencia);
	log.info("usuario:"+usuario);
	log.info("estadoReserva:"+estadoReserva);
	log.info("**** FIN " + SPROC_NAME + "******");
	
	inputs.put(E_V_COD_USUARIO,codUsuario);
	inputs.put(E_C_SREC_CODIGO,recCodigo);
	inputs.put(E_V_SREN_SECUENCIA,renSecuencia);
	inputs.put(E_V_USUARIO,usuario);
	inputs.put(E_C_ESTADO_RESERVA,estadoReserva);		
	
	return super.execute(inputs);
	
	}

	
}
