package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.GetAllPaisBiblioteca.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.BuzonSugerencia;
import com.tecsup.SGA.modelo.Pais;

public class GetAllSugerencias extends StoredProcedure{

	private static Log log = LogFactory.getLog(GetAllSugerencias.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_sugerencias.sp_sel_sugerencia";
    
 	private static final String E_C_MES = "E_C_MES";
    private static final String E_C_ANIO = "E_C_ANIO";
    private static final String E_C_TIPOSUG = "E_C_TIPOSUG";
    private static final String E_C_ESTADO = "E_C_ESTADO";
   
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetAllSugerencias(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_C_MES, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_ANIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPOSUG, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_ESTADO, OracleTypes.CHAR));
        
       
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String mes, String anio, String codigoSugerencia, String estado ) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_C_MES:"+ mes);
    	log.info("E_C_ANIO:"+ anio);
    	log.info("E_C_TIPOSUG:"+ codigoSugerencia);
    	log.info("E_C_ESTADO:"+ estado);       
    	log.info("**** fin " + SPROC_NAME + "*****");
        
    	inputs.put(E_C_MES, mes);
        inputs.put(E_C_ANIO, anio);
        inputs.put(E_C_TIPOSUG, codigoSugerencia);
        inputs.put(E_C_ESTADO, estado);
    	
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	BuzonSugerencia buzonSugerencia= new BuzonSugerencia();
        	
        	buzonSugerencia.setCodSugerencia(rs.getString("CODSUGERENCIA")== null ? "": rs.getString("CODSUGERENCIA"));//valor1
        	buzonSugerencia.setFechaCrea(rs.getString("FECCREA")== null ? "": rs.getString("FECCREA"));//ttde_id
        	buzonSugerencia.setCodTipoSugerencia(rs.getString("CODTIPOSUG")== null ? "": rs.getString("CODTIPOSUG"));//descripcion_hijo 
        	buzonSugerencia.setDescripTipoSugerencia(rs.getString("DESCTIPOSUG")== null ? "": rs.getString("DESCTIPOSUG"));
        	buzonSugerencia.setDescripcion(rs.getString("DESCRIPCION")== null ? "": rs.getString("DESCRIPCION"));
        	buzonSugerencia.setCodCalificacion(rs.getString("CODCALIFICACION")== null ? "": rs.getString("CODCALIFICACION"));
        	buzonSugerencia.setDescripCalificacion(rs.getString("DESCCALIFICACION")== null ? "": rs.getString("DESCCALIFICACION"));
        	buzonSugerencia.setCodigoMaterial(rs.getString("CODMATERIAL")== null ? "": rs.getString("CODMATERIAL"));
        	buzonSugerencia.setDescripCodMaterial(rs.getString("DESCCODMATERIAL")== null ? "": rs.getString("DESCCODMATERIAL"));
        	buzonSugerencia.setCodUsuarioCrea(rs.getString("CODUSUCREA")== null ? "": rs.getString("CODUSUCREA"));
        	buzonSugerencia.setDescripcionUsuario(rs.getString("USUARIO")== null ? "": rs.getString("USUARIO"));
        	buzonSugerencia.setEstado(rs.getString("FLAG_ESTADO")== null ? "": rs.getString("FLAG_ESTADO"));
        	buzonSugerencia.setEmail(rs.getString("CORREO")== null ? "": rs.getString("CORREO"));
        	buzonSugerencia.setRespuesta(rs.getString("RESPUESTA")== null ? "": rs.getString("RESPUESTA"));
            return buzonSugerencia;
        }

    }

}
