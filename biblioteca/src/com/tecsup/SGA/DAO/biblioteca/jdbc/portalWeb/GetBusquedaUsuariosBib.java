package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;

public class GetBusquedaUsuariosBib extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetBusquedaUsuariosBib.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA + ".PKG_BIB_COMUN.SP_SEL_BUSQUEDA_USUARIOS";
	private static final String V_USUARIO = "V_USUARIO";
	private static final String NOMBRE = "V_NOMBRE";
	private static final String APELLPATERNO = "V_APELLPATERNO";
	private static final String APELLMATERNO = "V_APELLMATERNO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetBusquedaUsuariosBib(DataSource dataStore){
    	super(dataStore,SPROC_NAME);
    	declareParameter(new SqlParameter(V_USUARIO, OracleTypes.VARCHAR));
    	declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
    	declareParameter(new SqlParameter(APELLPATERNO, OracleTypes.VARCHAR));
    	declareParameter(new SqlParameter(APELLMATERNO, OracleTypes.VARCHAR));
    	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new UsuariosMapper()));
    	compile();
    }
    public Map execute(String apellPaterno, String apellMaterno, String nombre1, String usuario){
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("V_USUARIO:"+ usuario);
    	log.info("NOMBRE:"+ nombre1);
    	log.info("APELLPATERNO:"+ apellPaterno);
    	log.info("APELLMATERNO:"+ apellMaterno);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	HashMap map = new HashMap();
    	map.put(V_USUARIO, usuario);
    	map.put(NOMBRE, nombre1);
    	map.put(APELLPATERNO, apellPaterno);
    	map.put(APELLMATERNO, apellMaterno);    	
    	return super.execute(map);
    }    
    final class UsuariosMapper implements RowMapper{

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Alumno alumno = new Alumno();
			alumno.setCodAlumno(rs.getString("CODUSUARIO")==null?"":rs.getString("CODUSUARIO"));
			alumno.setCodCarnet(rs.getString("USUARIO")==null?"":rs.getString("USUARIO"));
			alumno.setNombreAlumno(rs.getString("NOMBRE")==null?"":rs.getString("NOMBRE"));
			alumno.setTipo(rs.getString("TIPOUSUARIO")==null?"":rs.getString("TIPOUSUARIO"));
			alumno.setSede(rs.getString("SEDE")==null?"":rs.getString("SEDE"));		
			return alumno;
		}
    	
    }
}
