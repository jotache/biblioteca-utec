package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb.GetReservaSalas.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Busqueda;
import com.tecsup.SGA.modelo.Sala;

public class GetBusquedaSimple extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetBusquedaSimple.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_web.sp_sel_busqueda_simple";
    
 	private static final String E_C_TIPO_MATERIAL = "E_C_TIPO_MATERIAL";
 	private static final String E_V_TEXTO = "E_V_TEXTO";
 	private static final String E_C_BUSCAR_POR = "E_C_BUSCAR_POR";
 	private static final String E_C_ORDENAR_POR = "E_C_ORDENAR_POR";
 	private static final String E_C_DISPO_SALA = "E_C_DISPO_SALA";
 	private static final String E_C_DISPO_CASA = "E_C_DISPO_CASA";
 	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetBusquedaSimple(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
      
        declareParameter(new SqlParameter(E_C_TIPO_MATERIAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_TEXTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_BUSCAR_POR, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_ORDENAR_POR, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_DISPO_SALA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_DISPO_CASA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));            
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codTipoMaterial, String txtTexto, String txtBuscarBy, String txtOrdenarBy,
    		String txtDisponibiblidadSala, String txtDisponibilidadCasa,String codSede) {
    	 
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_C_TIPO_MATERIAL:"+ codTipoMaterial);
    	log.info("E_V_TEXTO:"+ txtTexto);
    	log.info("E_C_BUSCAR_POR:"+ txtBuscarBy);
    	log.info("E_C_ORDENAR_POR:"+ txtOrdenarBy);
    	log.info("E_C_DISPO_SALA:"+ txtDisponibiblidadSala);
    	log.info("E_C_DISPO_CASA:"+ txtDisponibilidadCasa);
    	log.info("E_C_COD_SEDE:"+ codSede);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();
    	//System.out.print("para la busqueda >>" + codTipoMaterial + " / " + txtTexto + " / " + txtBuscarBy + " / " + txtOrdenarBy + " / " + txtDisponibiblidadSala + " / " + txtDisponibilidadCasa + "<<");
    	inputs.put(E_C_TIPO_MATERIAL, codTipoMaterial);
    	inputs.put(E_V_TEXTO, txtTexto);
    	inputs.put(E_C_BUSCAR_POR, txtBuscarBy);
    	inputs.put(E_C_ORDENAR_POR, txtOrdenarBy);
    	inputs.put(E_C_DISPO_SALA, txtDisponibiblidadSala);
    	inputs.put(E_C_DISPO_CASA, txtDisponibilidadCasa);
    	inputs.put(E_C_COD_SEDE, codSede);
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Busqueda busqueda= new Busqueda();
       
        	busqueda.setCodUnico(rs.getString("CODIGOUNICO")== null ? "&nbsp;" : rs.getString("CODIGOUNICO"));        	
        	busqueda.setTitulo(rs.getString("NOM_MATERIAL")== null ? "&nbsp;" : rs.getString("NOM_MATERIAL")); 
        	busqueda.setTipoMaterial(rs.getString("TIPO_MATERIAL")== null ? "&nbsp;" : rs.getString("TIPO_MATERIAL"));
        	
        	busqueda.setDescripDewey(rs.getString("NOM_DEWEY")== null ? "&nbsp;" : rs.getString("NOM_DEWEY")); 
        	busqueda.setDescripAutor(rs.getString("NOM_AUTOR")== null ? "&nbsp;" : rs.getString("NOM_AUTOR"));        	
        	busqueda.setNroEjemplares(rs.getString("TOTAL_EJEMPLARES")== null ? "&nbsp;" : rs.getString("TOTAL_EJEMPLARES"));
        	busqueda.setPrestaSala(rs.getString("TOTAL_SALA")== null ? "&nbsp;" : rs.getString("TOTAL_SALA"));        	
        	busqueda.setPrestaDomicilio(rs.getString("TOTAL_DOMICILIO")== null ? "&nbsp;" : rs.getString("TOTAL_DOMICILIO"));
        	busqueda.setDisponibilidad(rs.getString("TOTAL_DISPONIBLES")== null ? "&nbsp;" : rs.getString("TOTAL_DISPONIBLES"));        	
         	busqueda.setReserva(rs.getString("TOTAL_RESERVADO")== null ? "&nbsp;" : rs.getString("TOTAL_RESERVADO"));
         	busqueda.setFlagReserva(rs.getString("FLAG_RESERVA")== null ? "&nbsp;" : rs.getString("FLAG_RESERVA"));
         	
        	busqueda.setIsbn(rs.getString("NRO_ISBN")== null ? "&nbsp;" : rs.getString("NRO_ISBN"));        	
        	
        	busqueda.setImagen(rs.getString("IMAGEN")== null ? "&nbsp;" : rs.getString("IMAGEN"));
        	busqueda.setFechaBD(rs.getString("FECHA_ACTUAL")== null ? "&nbsp;" : rs.getString("FECHA_ACTUAL"));
        	busqueda.setDscSede(rs.getString("NOM_SEDE")== null ? "&nbsp;" : rs.getString("NOM_SEDE"));
        	
        	busqueda.setEditorial(rs.getString("EDITORIAL")== null ? "&nbsp;" : rs.getString("EDITORIAL"));
        	busqueda.setCodigoGenerado(rs.getString("CODIGO_GENERADO")== null ? "&nbsp;" : rs.getString("CODIGO_GENERADO"));
        	busqueda.setUbicacionFisica(rs.getString("UBICACION_FISICA")== null ? "&nbsp;" : rs.getString("UBICACION_FISICA"));
        	
        	/*CODIGOUNICO
 NOM_MATERIAL,
 TIPO_MATERIAL
   NOM_DEWEY, 
       NOM_AUTOR,
       TOTAL_EJEMPLARES,
        TOTAL_SALA,
        TOTAL_DOMICILIO,
        TOTAL_DISPONIBLES, 
         TOTA_RESERVADO,
       FLAG_RESERVA,
       NRO_ISBN,
       IMAGEN,
       FECHA_ACTUAL,
       NOM_SEDE
        	 * 
        	 * busqueda.setCodUnico(rs.getString("CODIGOUNICO")== null ? "&nbsp;" : rs.getString("CODIGOUNICO"));
        	busqueda.setTitulo(rs.getString("TITULO")== null ? "&nbsp;" : rs.getString("TITULO"));
        	busqueda.setTipoMaterial(rs.getString("TIPOMATERIAL")== null ? "&nbsp;" : rs.getString("TIPOMATERIAL"));
        	busqueda.setDescripDewey(rs.getString("DESCDEWEY")== null ? "&nbsp;" : rs.getString("DESCDEWEY"));
        	busqueda.setDescripAutor(rs.getString("DESCAUTOR")== null ? "&nbsp;" : rs.getString("DESCAUTOR"));
        	busqueda.setNroEjemplares(rs.getString("NRO_EJEMPLARES")== null ? "&nbsp;" : rs.getString("NRO_EJEMPLARES"));
        	busqueda.setPrestaSala(rs.getString("PREST_SALA")== null ? "&nbsp;" : rs.getString("PREST_SALA"));
        	busqueda.setPrestaDomicilio(rs.getString("PREST_DOMICILIO")== null ? "&nbsp;" : rs.getString("PREST_DOMICILIO"));
        	busqueda.setDisponibilidad(rs.getString("DISPO")== null ? "&nbsp;" : rs.getString("DISPO"));
        	busqueda.setReserva(rs.getString("RESERVA")== null ? "&nbsp;" : rs.getString("RESERVA"));
        	busqueda.setFlagReserva(rs.getString("FLAG_RESERVA")== null ? "&nbsp;" : rs.getString("FLAG_RESERVA"));
        	busqueda.setIsbn(rs.getString("ISBN")== null ? "&nbsp;" : rs.getString("ISBN"));
        	busqueda.setImagen(rs.getString("IMAGEN")== null ? "&nbsp;" : rs.getString("IMAGEN"));
        	busqueda.setFechaBD(rs.getString("FECHA_ACTUAL")== null ? "&nbsp;" : rs.getString("FECHA_ACTUAL"));
        	busqueda.setDscSede(rs.getString("NOM_SEDE")== null ? "&nbsp;" : rs.getString("NOM_SEDE"));*/
        	
            return busqueda;
        }

    }
}
