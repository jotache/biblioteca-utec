package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertArchivoAdjuntoxMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertArchivoAdjuntoxMaterial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_GESTION_MAT.SP_INS_ARCH_ADJ_X_MATERIAL";	
		
	private static final String E_C_CODIGO_UNICO = "E_C_CODIGO_UNICO"; 
	private static final String E_V_TITULO = "E_V_TITULO"; 
	private static final String E_V_ARCHIVO = "E_V_ARCHIVO";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertArchivoAdjuntoxMaterial(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODIGO_UNICO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_TITULO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ARCHIVO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String txhCodigoUnico,
			String txtTitulo, String nomArchivo, String usuario) {
		
		log.info("*** INI "+SPROC_NAME+" ***");
    	log.info("E_C_CODIGO_UNICO:"+txhCodigoUnico);
    	log.info("E_V_TITULO:"+txtTitulo);
    	log.info("E_V_ARCHIVO:"+nomArchivo);
    	log.info("E_V_USU_CREA:"+usuario);
    	log.info("*** FIN "+SPROC_NAME+" ***");
    	
	Map inputs = new HashMap();	
	inputs.put(E_C_CODIGO_UNICO, txhCodigoUnico);
	inputs.put(E_V_TITULO, txtTitulo);
	inputs.put(E_V_ARCHIVO, nomArchivo);
	inputs.put(E_V_USU_CREA, usuario);	
	
	return super.execute(inputs);
	}

	
}
