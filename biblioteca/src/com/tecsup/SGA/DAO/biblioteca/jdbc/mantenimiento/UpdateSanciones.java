package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateSanciones extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateSanciones.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_mantto_config.sp_act_sanciones";
	
	private static final String E_V_CADENA = "E_V_CADENA";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String E_V_NRO_OCURRENCIAS = "E_V_NRO_OCURRENCIAS";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	public UpdateSanciones(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_CADENA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NRO_OCURRENCIAS, OracleTypes.VARCHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}	
	public Map execute(String cadena, String nroRegistros, String codUsuario, String nroOcurrencias) {

		log.info("**** INI " + SPROC_NAME + "*****");
		log.info("E_V_CADENA" + cadena);//codigo detalle padre
		log.info("E_V_NROREGISTROS" + nroRegistros);
		log.info("E_V_CODUSUARIO" + codUsuario);
		log.info("E_V_NRO_OCURRENCIAS" + nroOcurrencias);
		log.info("**** FIN " + SPROC_NAME + "*****");
		
		Map inputs = new HashMap();
		inputs.put(E_V_CADENA, cadena);//codigo detalle padre
		inputs.put(E_V_NROREGISTROS, nroRegistros);
		inputs.put(E_V_CODUSUARIO, codUsuario);
		inputs.put(E_V_NRO_OCURRENCIAS, nroOcurrencias);					
		return super.execute(inputs);
	
	}
}
