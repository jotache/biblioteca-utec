package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertSancion extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertSancion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_PROCESO_SANCIONES.SP_INS_SANCION_PRESTAMO_MAT";

	private static final String E_V_MDEC_NRO_INGRESO = "E_V_MDEC_NRO_INGRESO";
	private static final String E_V_FEC_DEVOLUCION = "E_V_FEC_DEVOLUCION";	
	private static final String E_V_USUARIO = "E_V_USUARIO";
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_GUARDA_SANCION = "E_C_GUARDA_SANCION";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertSancion(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_MDEC_NRO_INGRESO, OracleTypes.VARCHAR));	
	declareParameter(new SqlParameter(E_V_FEC_DEVOLUCION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_GUARDA_SANCION, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String nroIngreso,String fecDevolucion,String usuario,String sede,String guardaSancion) {    	
	Map inputs = new HashMap();
	
	log.info("**** INI " + SPROC_NAME + "******");
	log.info("E_V_MDEC_NRO_INGRESO:"+nroIngreso);
	log.info("E_V_FEC_DEVOLUCION:"+fecDevolucion);
	log.info("E_V_USUARIO:"+usuario);
	log.info("E_C_SEDE:"+sede);
	log.info("E_C_GUARDA_SANCION:"+guardaSancion);
	log.info("**** FIN " + SPROC_NAME + "******");
	
	inputs.put(E_V_MDEC_NRO_INGRESO,nroIngreso);
	inputs.put(E_V_FEC_DEVOLUCION,fecDevolucion);	
	inputs.put(E_V_USUARIO, usuario);	
	inputs.put(E_C_SEDE, sede);
	inputs.put(E_C_GUARDA_SANCION, guardaSancion);
	
	return super.execute(inputs);
	}

	
}

