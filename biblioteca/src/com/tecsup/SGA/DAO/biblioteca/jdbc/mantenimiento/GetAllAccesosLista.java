package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.AccesoIP;
import com.tecsup.SGA.modelo.UsuarioPM;

public class GetAllAccesosLista extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllAccesosLista.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_mantto_config.SP_SEL_ACCESOS_LISTA";
    
	private static final String E_N_COD_USUARIO = "E_N_COD_USUARIO";
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
 
 public GetAllAccesosLista(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_N_COD_USUARIO, OracleTypes.NUMBER));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(Integer codUsuario) {
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_N_COD_USUARIO: "+codUsuario);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();    	
    	inputs.put(E_N_COD_USUARIO, codUsuario);
    	
        return super.execute(inputs);
    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	AccesoIP acceso = new AccesoIP ();
        	acceso.setNumIP(rs.getString("NUM_IP"));
        	acceso.setFeCreacion(rs.getString("FEC_CREACION"));
        	acceso.setUsuCreacion(rs.getString("USU_CREACION"));
        	
            return acceso;
        }

    }
}
