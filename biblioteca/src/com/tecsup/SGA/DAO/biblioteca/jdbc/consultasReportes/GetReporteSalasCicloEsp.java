package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Reporte;

public class GetReporteSalasCicloEsp extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetReporteSalasCicloEsp.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.SP_SEL_REPORT_SALAS_CICLO_ESP";

	 	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	 	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	    
	    public GetReporteSalasCicloEsp(DataSource dataSource){
	    	super(dataSource, SPROC_NAME);        
	        declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteReservaCicloEsp()));
	        compile();
	    }
	    
	    public Map execute(String fecIni, String fecFin,String sede){
	    	
	    	log.info("**** INI " + SPROC_NAME + " ****");
	    	log.info("E_V_FEC_INI:" + fecIni);
	    	log.info("E_V_FEC_FIN:" + fecFin);
	    	log.info("E_C_SEDE:" + sede);
	    	log.info("**** FIN " + SPROC_NAME + " ****");
	    	
	    	Map inputs = new HashMap();	    		    
	    	inputs.put(E_V_FEC_INI, fecIni);
	    	inputs.put(E_V_FEC_FIN, fecFin);
	    	inputs.put(E_C_SEDE, sede);
	        return super.execute(inputs);
	    }
	    
	    final class ReporteReservaCicloEsp implements RowMapper {

			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Reporte reporte = new Reporte();
	        	reporte.setEspecialidad(rs.getString("NOM_ESPECIALIDAD") == null ? "" : rs.getString("NOM_ESPECIALIDAD"));
	        	reporte.setCiclo(rs.getString("NOM_CICLO") == null ? "" : rs.getString("NOM_CICLO"));
	        	reporte.setSala(rs.getString("SALA") == null ? "" : rs.getString("SALA"));
	        	reporte.setNomUsuario(rs.getString("NOMBRE") == null ? "" : rs.getString("NOMBRE"));
	        	reporte.setFechaReserva(rs.getString("FECHARESERVA") == null ? "" : rs.getString("FECHARESERVA"));
	        	reporte.setHoraIniProg(rs.getString("HORAINI_PROG") == null ? "" : rs.getString("HORAINI_PROG")); 
	        	reporte.setHoraFinProg(rs.getString("HORAFIN_PROG") == null ? "" : rs.getString("HORAFIN_PROG"));
	        	reporte.setHoraIniEjec(rs.getString("HORAINI_EJEC") == null ? "" : rs.getString("HORAINI_EJEC"));
	        	reporte.setHoraFinEjec(rs.getString("HORAFIN_EJEC") == null ? "" : rs.getString("HORAFIN_EJEC"));
	        	reporte.setEstadoReservaSala(rs.getString("ESTADO") == null ? "" : rs.getString("ESTADO"));
	        	reporte.setCodEstadoReserva(rs.getString("CODESTADO") == null ? "" : rs.getString("CODESTADO"));
				return reporte;
			}
	    	
	    }
}
