package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_SEL_REPORT_TIPO_USUARIO(
		E_V_FEC_INI IN VARCHAR2,
		E_V_FEC_FIN IN VARCHAR2,
		S_C_RECORDSET IN OUT CUR_RECORDSET)
*/
public class GetReporteEstadTipoUsuario extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReporteEstadTipoUsuario.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.SP_SEL_REPORT_TIPO_USUARIO";

	 	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	 	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetReporteEstadTipoUsuario(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        
	        declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteEstadTipoUsuario()));
	        compile();
	    }

	    public Map execute(String fecIni, String fecFin,String sede){
	    	
	    	Map inputs = new HashMap();
	    	
	    	log.info("***** INI " + SPROC_NAME + "*****");
	    	log.info("fecIni:"+fecIni);
	    	log.info("fecFin:"+fecFin);
	    	log.info("sede:"+sede);
	    	log.info("***** FIN " + SPROC_NAME + "*****");
	    	
	    	inputs.put(E_V_FEC_INI, fecIni);
	    	inputs.put(E_V_FEC_FIN, fecFin);
	    	inputs.put(E_C_SEDE, sede);
	        return super.execute(inputs);
	    }
	    
	    final class ReporteEstadTipoUsuario implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	Reporte reporte = new Reporte();
	        	
	        	//reporte.setCodigoMaterial(rs.getString(0) == null ? "" : rs.getString(0) );
	        	reporte.setCodigo(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO") );
	        	reporte.setDscMaterial(rs.getString("NOM_MATERIAL") == null ? "" : rs.getString("NOM_MATERIAL") );
	        	reporte.setTipoMaterial(rs.getString("COD_TIPO_MATERIAL") == null ? "" : rs.getString("COD_TIPO_MATERIAL") );
	        	reporte.setTituloMaterial(rs.getString("DES_TIPO_MATERIAL") == null ? "" : rs.getString("DES_TIPO_MATERIAL") );
	        	reporte.setNroUsuarios(rs.getString("NRO_USUARIOS") == null ? "" : rs.getString("NRO_USUARIOS") );
	        	reporte.setDewey(rs.getString("COD_NOM_DEWEY") == null ? "" : rs.getString("COD_NOM_DEWEY") );
	        	reporte.setIdioma(rs.getString("NOM_IDIOMA") == null ? "" : rs.getString("NOM_IDIOMA") );
	        	reporte.setTipoUsuario(rs.getString("COD_TIPO_USUARIO") == null ? "" : rs.getString("COD_TIPO_USUARIO") );
	        	reporte.setUsuario(rs.getString("NOM_TIPO_USUARIO") == null ? "" : rs.getString("NOM_TIPO_USUARIO") );
	        	reporte.setNroPrestSala(rs.getString("PREST_SALA") == null ? "" : rs.getString("PREST_SALA") );
	        	reporte.setNroPrestCasa(rs.getString("PREST_DOMICILIO") == null ? "" : rs.getString("PREST_DOMICILIO") );
	        	reporte.setTotalPrestamos(rs.getString("TOTAL_PRESTAMOS") == null ? "" : rs.getString("TOTAL_PRESTAMOS"));
	        	
	            return reporte;
	        }

	    }
}

