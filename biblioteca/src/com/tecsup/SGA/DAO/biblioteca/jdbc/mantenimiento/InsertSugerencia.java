package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertSugerencia extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertSugerencia.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_web.sp_ins_sugerencia";
	
	private static final String E_C_TIPO = "E_C_TIPO";
	private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
	private static final String E_V_EMAIL = "E_V_EMAIL";
	private static final String E_C_USU_CREA = "E_C_USU_CREA";	
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertSugerencia(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EMAIL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_USU_CREA, OracleTypes.VARCHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(String tipo, String descripcion, String email, String usuario) {
		
		log.info("**** INI " + SPROC_NAME + "*****");
		log.info("E_C_TIPO>>"+tipo+"<<");
		log.info("E_V_DESCRIPCION>>"+descripcion+"<<");
		log.info("E_V_EMAIL>>"+email+"<<");
		log.info("E_C_USU_CREA>>"+usuario+"<<");		
		log.info("**** FIN " + SPROC_NAME + "*****");
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_TIPO, tipo);
		inputs.put(E_V_DESCRIPCION, descripcion); 
		inputs.put(E_V_EMAIL, email);
		inputs.put(E_C_USU_CREA, usuario);		
					
		return super.execute(inputs);	
	}
}
