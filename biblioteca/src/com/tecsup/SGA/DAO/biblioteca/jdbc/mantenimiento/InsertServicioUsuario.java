package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertServicioUsuario extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertServicioUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_mantto_config.sp_ins_config_serv_tipo_user";
	private static final String E_C_TIPT_ID = "E_C_TIPT_ID";
	private static final String E_V_VALOR1 = "E_V_VALOR1";
	private static final String E_V_VALOR2 = "E_V_VALOR2";  
	private static final String E_V_VALOR3 = "E_V_VALOR3";
	private static final String E_V_VALOR4 = "E_V_VALOR4";
	private static final String E_V_VALOR5 = "E_V_VALOR5";
	private static final String E_V_VALOR6 = "E_V_VALOR6";
	private static final String E_V_VALOR7 = "E_V_VALOR7";
	private static final String E_V_VALOR8 = "E_V_VALOR8";
	private static final String E_V_VALOR9 = "E_V_VALOR9";
	private static final String E_V_VALOR10 = "E_V_VALOR10";  
	private static final String E_V_TTDE_USU_CREA = "E_V_TTDE_USU_CREA";
	
	private static final String E_V_CADENA = "E_V_CADENA";
	private static final String E_V_NRODATOS = "E_V_NRODATOS";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertServicioUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(E_C_TIPT_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_VALOR1, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR2, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR3, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR4, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR5, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR6, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR7, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR8, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR9, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_VALOR10, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_USU_CREA, OracleTypes.CHAR));
		
		declareParameter(new SqlParameter(E_V_CADENA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NRODATOS, OracleTypes.VARCHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}

			public Map execute(String tiptId, String valor1, String valor2,String valor3,
					String valor4, String valor5, String valor6, String valor7, String valor8,
					String valor9,String valor10,String usuCrea, String cadenaDatos, String tamanoDatos) {
			
				Map inputs = new HashMap();
				
				log.info("**** INI " + SPROC_NAME + "*****");
				log.info("E_C_TIPT_ID.- "+tiptId);
				log.info("E_V_VALOR1.- "+valor1);
				log.info("E_V_VALOR2.- "+valor2);
				log.info("E_V_VALOR3.- "+valor3);
				log.info("E_V_VALOR4.- "+valor4);
				log.info("E_V_VALOR5.- "+valor5);
				log.info("E_V_VALOR6.- "+valor6);
				log.info("E_V_VALOR7.- "+valor7);
				log.info("E_V_VALOR8.- "+valor8);
				log.info("E_V_VALOR9.- "+valor9);
				log.info("E_V_VALOR10.- "+valor10);
				log.info("E_V_TTDE_USU_CREA.- "+usuCrea);
				log.info("E_V_CADENA.- "+cadenaDatos);
				log.info("E_V_NRODATOS.- "+tamanoDatos);
				log.info("**** FIN " + SPROC_NAME + "*****");
				
				inputs.put(E_C_TIPT_ID, tiptId);//codigo detalle padre
				inputs.put(E_V_VALOR1, valor1);
				inputs.put(E_V_VALOR2, valor2);
				inputs.put(E_V_VALOR3, valor3);
				inputs.put(E_V_VALOR4, valor4);
				inputs.put(E_V_VALOR5, valor5);
				inputs.put(E_V_VALOR6, valor6);
				inputs.put(E_V_VALOR7, valor7);
				inputs.put(E_V_VALOR8, valor8);
				inputs.put(E_V_VALOR9, valor9);
				inputs.put(E_V_VALOR10, valor10);
				inputs.put(E_V_TTDE_USU_CREA, usuCrea);
				inputs.put(E_V_CADENA, cadenaDatos);
				inputs.put(E_V_NRODATOS, tamanoDatos);
				return super.execute(inputs);
			
			}

}
