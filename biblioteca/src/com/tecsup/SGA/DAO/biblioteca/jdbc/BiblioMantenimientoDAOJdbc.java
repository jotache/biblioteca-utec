package com.tecsup.SGA.DAO.biblioteca.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.sun.mail.imap.protocol.SaslAuthenticator;
import com.tecsup.SGA.DAO.biblioteca.BiblioMantenimientoDAO;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.*;
import com.tecsup.SGA.bean.OpcionesApoyo;
import com.tecsup.SGA.modelo.AccesoIP;
import com.tecsup.SGA.modelo.Sala;
//import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.modelo.ParametrosReservados;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.UsuarioPM;

public class BiblioMantenimientoDAOJdbc extends JdbcDaoSupport implements BiblioMantenimientoDAO{
	private static Log log = LogFactory.getLog(BiblioMantenimientoDAOJdbc.class);
	public List getAllTablaDetalleBiblioteca(String codTablaPadre,String codPadre, String codHijo,
			String codigo, String descripcion, String tiopoOrden,String valor4,String valor5,String valor6, String tipoLista)
	{
		GetAllTablaDetalleBiblioteca getAllTablaDetalleBiblioteca = new GetAllTablaDetalleBiblioteca(this.getDataSource());

		HashMap outputs = (HashMap)getAllTablaDetalleBiblioteca.execute(codTablaPadre, codPadre, codHijo, codigo, descripcion, tiopoOrden, valor4, valor5, valor6,tipoLista);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public String insertTablaDetalleBiblioteca(String codDetallePadre, String codTipoTablaDetalle,
			String descripcion, String usuCrea, String valor3, String valor4 , String valor5,String valor6)
	{
		InsertTablaDetalleBiblioteca insertTablaDetalleBiblioteca = new InsertTablaDetalleBiblioteca(this.getDataSource());

			HashMap inputs = (HashMap)insertTablaDetalleBiblioteca.execute(codDetallePadre, codTipoTablaDetalle,
					descripcion, usuCrea, valor3, valor4 , valor5, valor6);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}

	public String UpdateAndDeleteTablaDetalleBiblioteca(String codDetallePadre, String codTipoTablaDetalle,
			String codRegistro,	String codGenerado, String descripcion, String Flag, String usuCrea,
			String valor3, String valor4 , String valor5, String valor6)
	{
		UpdateDeleteTablaDetalleBiblioteca updateDeleteTablaDetalleBiblioteca = new UpdateDeleteTablaDetalleBiblioteca(this.getDataSource());

			HashMap inputs = (HashMap)updateDeleteTablaDetalleBiblioteca.execute(codDetallePadre, codTipoTablaDetalle, codRegistro,
					codGenerado, descripcion, Flag, usuCrea, valor3, valor4 , valor5,valor6);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}

	public List getAllPaises(String codigo, String descripcion, String tiopoOrden)
	{
		GetAllPaisBiblioteca getAllPaises = new GetAllPaisBiblioteca(this.getDataSource());

		HashMap outputs = (HashMap)getAllPaises.execute(codigo, descripcion, tiopoOrden);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}//InsertPaisBiblioteca

	public String insertPaises(	String descripcion, String usuCrea)
	{
		InsertPaisBiblioteca insertPaisBiblioteca = new InsertPaisBiblioteca(this.getDataSource());

			HashMap inputs = (HashMap)insertPaisBiblioteca.execute(descripcion, usuCrea);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}//GetCiudadByPais

	public List getCiudadByPais(String codigoPais, String codigoId, String descripcion, String tipoOrden)
	{
		GetCiudadByPais getCiudadByPais = new GetCiudadByPais(this.getDataSource());

		HashMap outputs = (HashMap)getCiudadByPais.execute(codigoPais, codigoId, descripcion, tipoOrden);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//InsertCiudadByPais UpdateDeletePais
	public String insertCiudadByPais(String codigoPais, String descripcion, String usuCrea)
	{
		InsertCiudadByPais insertCiudadByPais = new InsertCiudadByPais(this.getDataSource());

			HashMap inputs = (HashMap)insertCiudadByPais.execute(codigoPais, descripcion, usuCrea);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	public String updateDeletePais(String paisId, String descripcion, String Flag, String usuCrea)
	{
		UpdateDeletePais updateDeletePais = new UpdateDeletePais(this.getDataSource());

			HashMap inputs = (HashMap)updateDeletePais.execute(paisId, descripcion, Flag, usuCrea);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}//UpdateDeleteCiudadbyPais

	public String updateDeleteCiudadbyPais(String paisId, String ciudadId, String descripcion,
			String Flag, String usuCrea)
	{
		UpdateDeleteCiudadbyPais updateDeleteCiudadbyPais = new UpdateDeleteCiudadbyPais(this.getDataSource());

			HashMap inputs = (HashMap)updateDeleteCiudadbyPais.execute(paisId, ciudadId, descripcion, Flag,
					usuCrea);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	public List GetAllFeriados(String codTablaId, String mes, String anio)
	{
		GetAllFeriados GetAllFeriados = new GetAllFeriados(this.getDataSource());
		HashMap outputs = (HashMap)GetAllFeriados.execute(codTablaId, mes, anio);
		if(!outputs.isEmpty()){
		return (List)outputs.get("S_C_RECORDSET");

		}
		return null;
	}

	public String UpdateAtenderBuzonSugerencia(String codSugerencia, String codCalificacion,
			String codTipoMaterial,	String respuesta)
	{
		UpdateAtenderBuzonSugerencia updateAtenderBuzonSugerencia = new UpdateAtenderBuzonSugerencia(this.getDataSource());

			HashMap inputs = (HashMap)updateAtenderBuzonSugerencia.execute(codSugerencia, codCalificacion,
					codTipoMaterial, respuesta);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	public String InsertFeriados(String codTablaId, String dis, String mes
			, String anio, String descripcion,String usucrea){
		InsertFeriados InsertFeriados = new InsertFeriados(this.getDataSource());
		HashMap outputs = (HashMap)InsertFeriados.execute(codTablaId, dis
				, mes, anio, descripcion, usucrea);
		if(!outputs.isEmpty()){
			return (String)outputs.get("S_V_RETVAL");
		}

		return null;
	}

	public String ActualizarFeriado(String codTablaId, String id, String dis, String mes
			, String anio, String descripcion,String usucrea, String flag){
		ActualizarFeriado ActualizarFeriado = new ActualizarFeriado(this.getDataSource());
		HashMap outputs = (HashMap)ActualizarFeriado.execute(codTablaId
				, id, dis, mes, anio, descripcion, usucrea, flag);
		if(!outputs.isEmpty()){
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;

	}

	public ServiciosUsuarios GetAllServicioUsuario(String tipId, String valor1){
		GetAllServicioUsuario GetAllServicioUsuario = new GetAllServicioUsuario(this.getDataSource());
		HashMap outputs = (HashMap)GetAllServicioUsuario.execute(tipId, valor1);
		if(!outputs.isEmpty()){
			ArrayList lista = (ArrayList)outputs.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (ServiciosUsuarios)lista.get(0);
			}
			else return null;
		}
		return null;
	}
	public String UpdateSanciones(String cadena, String nroRegistros, String codUsuario,String nroOcurrencias){
		UpdateSanciones updateSanciones = new UpdateSanciones(this.getDataSource());

		HashMap outputs = (HashMap)updateSanciones.execute(cadena, nroRegistros, codUsuario, nroOcurrencias);

		if(!outputs.isEmpty()){
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String InsertServicioUsuario(String tiptId, String valor1, String valor2,String valor3,
			String valor4, String valor5, String valor6, String valor7, String valor8,
			String valor9,String valor10,String usuCrea,String cadenaDatos, String tamanoDatos){
		InsertServicioUsuario InsertServicioUsuario = new InsertServicioUsuario(this.getDataSource());
	HashMap inputs = (HashMap)InsertServicioUsuario.execute(tiptId, valor1
			, valor2, valor3, valor4, valor5, valor6, valor7, valor8, valor9, valor10, usuCrea,cadenaDatos,tamanoDatos);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}//GetSeleccionByFiltros

	public List getSeccionesByFiltros(String codigoUnico,String codSeccion, String codGrupo, String codTipo)
	{
		GetSeccionesByFiltros getSeccionesByFiltros = new GetSeccionesByFiltros(this.getDataSource());

		HashMap outputs = (HashMap)getSeccionesByFiltros.execute(codigoUnico, codSeccion, codGrupo, codTipo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public String UpdateGrupos(String codTabla, String cadena, String nroRegistros, String codUsuario){
		UpdateGrupos updateGrupos = new UpdateGrupos(this.getDataSource());

		HashMap outputs = (HashMap)updateGrupos.execute(codTabla, cadena, nroRegistros, codUsuario);

		if(!outputs.isEmpty()){
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	public String insertSecciones(String codUnico, String codSeccion, String dscTipoMaterial,
			String codGrupo, String dscTema, String dscCorta, String dscLarga, String dscUrl, String dscArchivo,
			String fechaPublicacion, String fechaVigencia, String flag, String usuario, String tipo, String nomAdjunto){

		InsertSecciones insertSecciones = new InsertSecciones(this.getDataSource());

		HashMap inputs = (HashMap)insertSecciones.execute(codUnico, codSeccion, dscTipoMaterial,
				codGrupo, dscTema, dscCorta, dscLarga, dscUrl, dscArchivo,
				fechaPublicacion, fechaVigencia, flag, usuario, tipo, nomAdjunto);

		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String InsertParametrosReservados(String codTablaId, String valor1, String valor2
			, String valor3, String valor4,String valor5,String valor6
			,String valor7,String valor8,String valor9,String usuario){
		InsertParametrosReservados InsertParametrosReservados = new InsertParametrosReservados(this.getDataSource());

		HashMap inputs = (HashMap)InsertParametrosReservados.execute(codTablaId, valor1
				, valor2, valor3, valor4, valor5, valor6, valor7,valor8,valor9, usuario);
		if(!inputs.isEmpty()){
		return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public ParametrosReservados GetAllParametrosGenerales(String tipId){
		GetAllParametrosGenerales GetAllParametrosGenerales = new GetAllParametrosGenerales(this.getDataSource());
		HashMap outputs = (HashMap)GetAllParametrosGenerales.execute(tipId);
		if(!outputs.isEmpty()){
			ArrayList lista = (ArrayList)outputs.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (ParametrosReservados)lista.get(0);
			}
			else return null;
		}

		return null;
	}

	public List<UsuarioPM> GetAllUsuariosAcceso(){
		GetAllUsuariosAcceso getAllUsuariosAcceso = new GetAllUsuariosAcceso(this.getDataSource());
		HashMap outputs = (HashMap)getAllUsuariosAcceso.execute();
		if(!outputs.isEmpty()){
			List<UsuarioPM> lista = (ArrayList)outputs.get("S_C_RECORDSET");
			return lista;
		}

		return null;
	}

	public List<AccesoIP> GetAllAccesosLista(Integer codUsuario){
		GetAllAccesosLista getAllAccesosLista = new GetAllAccesosLista(this.getDataSource());
		HashMap outputs = (HashMap)getAllAccesosLista.execute(codUsuario);
		if(!outputs.isEmpty()){
			List<AccesoIP> lista = (ArrayList)outputs.get("S_C_RECORDSET");
			return lista;
		}

		return new ArrayList<AccesoIP>();
	}

	public void InsertAccesoIP(Integer codUsuario, String numIP, String usuCreacion){
		InsertAccesoIP insertAccesoIP = new InsertAccesoIP(this.getDataSource());
		HashMap outputs = (HashMap)insertAccesoIP.execute(codUsuario, numIP, usuCreacion);
	}

	public void DeleteAccesoIP(Integer codUsuario, String numIP){
		DeleteAccesoIP deleteAccesoIP = new DeleteAccesoIP(this.getDataSource());
		HashMap outputs = (HashMap)deleteAccesoIP.execute(codUsuario, numIP);
	}

	public void DeleteAccesosIP(Integer codPersona){
		DeleteAccesosIP deleteAccesosIP = new DeleteAccesosIP(this.getDataSource());
		deleteAccesosIP.execute(codPersona);
	}

	public void DeleteOpcionesApoyo(Integer codPersona){
		DeleteOpcionesApoyo deleteOpcionesApoyo = new DeleteOpcionesApoyo(this.getDataSource());
		deleteOpcionesApoyo.execute(codPersona);
	}

	public void DeleteRolesApoyo(String codUsuario){
		DeleteRolesApoyo deleteRolesApoyo = new DeleteRolesApoyo(this.getDataSource());
		deleteRolesApoyo.execute(codUsuario);
	}

	public String GetNombreArchivoSeccion()
	{
		GetNombreArchivoSeccion getNombreArchivoSeccion = new GetNombreArchivoSeccion(this.getDataSource());

		HashMap inputs = (HashMap)getNombreArchivoSeccion.execute();
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public TipoTablaDetalle GetTipoTablaDetalle(String codPadre, String codHijo) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();

		try{
			log.info("GetTipoTablaDetalle: codPadre=" + codPadre + " codHijo="+codHijo);

			String sql = "select TTDE.TTDE_ID AS ID, TTDE.TIPT_ID,TTDE.TTDE_VALOR1 AS valor1,TTDE.TTDE_VALOR2 AS valor2, " +
			 " TTDE.TTDE_DESCRIPCION AS descripcion,TTDE.TTDE_VALOR3 AS valor3,TTDE.TTDE_VALOR4 AS valor4, " +
			 " TTDE.TTDE_VALOR5 AS valor5,TTDE.TTDE_VALOR6 AS valor6,TTDE.TTDE_VALOR7 AS valor7, "+
			 " TTDE.TTDE_VALOR8 AS valor8,TTDE.TTDE_VALOR9 AS valor9,TTDE.TTDE_VALOR10 AS valor10,TTDE.TTDE_EST_REG as estado "+
			 " from GENERAL.GEN_TIPO_TABLA_DETALLE TTDE LEFT JOIN GENERAL.GEN_TIPO_TABLA TT ON TTDE.TIPT_ID = TT.TIPT_ID "+
			 " where TTDE.TIPT_ID =? AND TRIM(TTDE.TTDE_ID) =?";

			log.info("sql:"+sql);
			final Object[] params = new Object[] {codPadre.trim(),codHijo.trim()};
			final TipoTablaDetalle tipoTablaDet = new TipoTablaDetalle();

			jdbcTemplate.query(sql, params, new RowCallbackHandler() {
				public void processRow(ResultSet rs) throws SQLException {

					tipoTablaDet.setDescripcion(rs.getString("descripcion")==null?"":rs.getString("descripcion"));
					tipoTablaDet.setDscValor1(rs.getString("valor1")==null?"":rs.getString("valor1"));
					tipoTablaDet.setDscValor2(rs.getString("valor2")==null?"":rs.getString("valor2"));
					tipoTablaDet.setDscValor3(rs.getString("valor3")==null?"":rs.getString("valor3"));
					tipoTablaDet.setDscValor4(rs.getString("valor4")==null?"":rs.getString("valor4"));
					tipoTablaDet.setDscValor5(rs.getString("valor5")==null?"":rs.getString("valor5"));
					tipoTablaDet.setDscValor6(rs.getString("valor6")==null?"":rs.getString("valor6"));
					tipoTablaDet.setDscValor7(rs.getString("valor7")==null?"":rs.getString("valor7"));
					tipoTablaDet.setDscValor8(rs.getString("valor8")==null?"":rs.getString("valor8"));
					tipoTablaDet.setDscValor9(rs.getString("valor9")==null?"":rs.getString("valor9"));
					tipoTablaDet.setDscValor10(rs.getString("valor10")==null?"":rs.getString("valor10"));
					tipoTablaDet.setEstReg(rs.getString("estado")==null?"":rs.getString("estado"));
					tipoTablaDet.setCodTipoTabla(rs.getString("TIPT_ID"));
					tipoTablaDet.setCodDetalle(rs.getString("ID"));
				}
			});

			return tipoTablaDet;
		}catch (Exception ex){
			log.error("GetTipoTablaDetalle #Error:" + ex.getMessage());
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public List<ParametrosReservados> GetListParametrosGenerales() {
		log.info("GetListParametrosGenerales() - start");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();

		String sql = "SELECT";
		sql += " 	td1.ttde_id codtipousuario,td2.ttde_descripcion nomtipousuario,td1.ttde_id as codparametro," +
				"td1.ttde_valor1,td1.ttde_valor2,td1.ttde_valor3,td1.ttde_valor4,td1.ttde_valor5, ";
		sql += " 	td1.ttde_valor6,td1.ttde_valor7,td1.ttde_valor8,td1.ttde_valor9,td1.ttde_valor10 ";
		sql += " FROM  general.gen_tipo_tabla_detalle td1 , general.gen_tipo_tabla_detalle td2 ";
		sql += " WHERE td1.tipt_id='0085' and td2.tipt_id='0049' and trim(td1.ttde_descripcion)=trim(td2.ttde_id) ";
		sql += " 	and td2.ttde_est_reg='A' and td1.ttde_est_reg='A' ";
		sql += " ORDER BY ";
		sql += " 	td2.ttde_descripcion";

		log.info("sql:"+sql);

		try{

			return jdbcTemplate.query(sql, new ParametroRowMapper());

		}catch (Exception ex){
			log.error(ex);
		}

		log.info("GetListParametrosGenerales() - end");
		return null;
	}

	final class ParametroRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			ParametrosReservados p = new ParametrosReservados();
			p.setCodParametroUsuario(rs.getString("codparametro"));
			p.setCodTipoUsuario(rs.getString("codtipousuario"));
			p.setNomTipoUsuario(rs.getString("nomtipousuario"));

			//Configuración para Salas de Estudio:
			p.setEsMaDi(rs.getString("ttde_valor1")==null?"0":rs.getString("ttde_valor1"));
			p.setEsMiTo(rs.getString("ttde_valor2")==null?"0":rs.getString("ttde_valor2"));
			p.setEsDiPe(rs.getString("ttde_valor3")==null?"0":rs.getString("ttde_valor3"));

			//Configuración para Salas de Internet:
			p.setInMaDi(rs.getString("ttde_valor4")==null?"0":rs.getString("ttde_valor4"));
			p.setInMiTo(rs.getString("ttde_valor5")==null?"0":rs.getString("ttde_valor5"));
			p.setInDiPe(rs.getString("ttde_valor6")==null?"0":rs.getString("ttde_valor6"));

			//Penalidad Mat. bibliografico:
			p.setBiNroDiPe(rs.getString("ttde_valor7")==null?"0":rs.getString("ttde_valor7"));

			//Nro reg a Visualizar
			p.setCantMostrarWeb(rs.getString("ttde_valor8")==null?"0":rs.getString("ttde_valor8"));

			//Configuración para PoliDeportivo
			p.setMaxHrResPoli(rs.getString("ttde_valor9")==null?"0":rs.getString("ttde_valor9"));
			p.setMinTolePoli(rs.getString("ttde_valor10")==null?"0":rs.getString("ttde_valor10"));

			return p;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Sala> listaHorarioSalas(String sede) {
		log.info("listaHorarioSalas() - start");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();

		String sql = "SELECT trim(td1.ttde_id) codsala ,td1.ttde_descripcion descripcion, ";
		sql += " td1.ttde_valor4 sede, trim(td1.ttde_valor7) codhoraini, trim(td1.ttde_valor8) codhorafin," ;				
		sql += " (select trim(hi.ttde_valor1) from general.gen_tipo_tabla_detalle hi where trim(hi.tipt_id)='0087' and trim(hi.ttde_id)=trim(td1.ttde_valor7)) HORA_INI, ";
		sql += " (select trim(hf.ttde_valor2) from general.gen_tipo_tabla_detalle hf where trim(hf.tipt_id)='0087' and trim(hf.ttde_id)=trim(td1.ttde_valor8)) HORA_FIN, ";
		sql += " trim(td1.ttde_valor9) dias ";
		sql += " FROM  general.gen_tipo_tabla_detalle td1 ";
		sql += " WHERE td1.tipt_id='0045' and td1.ttde_est_reg='A' and ttde_valor4='"+sede.toUpperCase()+"' ";		
		sql += " ORDER BY ";
		sql += " 	2";
		 		
		log.info("sql:"+sql);

		try{

			return jdbcTemplate.query(sql, new SalaRowMapper());

		}catch (Exception ex){
			log.error(ex);
		}

		log.info("listaHorarioSalas() - end");
		return null;
	}	
	final class SalaRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			Sala s = new Sala();
			s.setCodSala(rs.getString("codsala"));
			s.setNombreSala(rs.getString("descripcion"));
			s.setSede(rs.getString("sede"));
			s.setIdHoraIni(rs.getString("codhoraini"));
			s.setIdHoraFin(rs.getString("codhorafin"));
			s.setHoraIni(rs.getString("HORA_INI"));
			s.setHoraFin(rs.getString("HORA_FIN"));
			s.setDias(rs.getString("dias"));
			return s;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TipoTablaDetalle> listaHorasBiblioteca(String sede) {
		log.info("listaHorasBiblioteca() - start");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();

		String sql = "SELECT trim(td1.ttde_id) codigo, ";
		sql += " substr('0'||trim(td1.ttde_valor1),-2,2) horaIni, substr('0'||trim(td1.ttde_valor2),-2,2) horaFin, " ;				
		sql += " td1.ttde_valor9 sede ";		
		sql += " FROM  general.gen_tipo_tabla_detalle td1 ";
		sql += " WHERE td1.tipt_id='0087' and td1.ttde_est_reg='A' and td1.ttde_valor9='"+sede.toUpperCase()+"' ";		
		sql += " ORDER BY ";
		sql += " 	2";
		 		
		log.info("sql:"+sql);

		try{
			return jdbcTemplate.query(sql, new HorasRowMapper());
		}catch (Exception ex){
			log.error(ex);
		}

		log.info("listaHorasBiblioteca() - end");
		return null;
	}	
	final class HorasRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			TipoTablaDetalle t = new TipoTablaDetalle();
			t.setCodDetalle(rs.getString("codigo"));
			t.setDscValor1(rs.getString("horaIni"));
			t.setDscValor2(rs.getString("horaFin"));
			t.setDscValor9(rs.getString("sede"));
			
			return t;
		}
	}
	
	public String UpdateParametrosGenerales(String cadenaDatos, String usuario,
			String nroRegistros) {
		ActualizaParametrosGenerales update = new ActualizaParametrosGenerales(getDataSource());
		HashMap inputs = (HashMap) update.execute(cadenaDatos, usuario, nroRegistros);
		if (!inputs.isEmpty()) {
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<TipoTablaDetalle> listaMaximoDiasPrestamos(String codTipoUsuario) {
		log.info("listaMaximoDiasPrestamos("+codTipoUsuario+") - start");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();

		String query = "SELECT t.ttde_id id,t1.ttde_descripcion tipomat, t.ttde_valor2 as nromaxprest, t1.ttde_id codtipomat ";
		query += "FROM   general.gen_tipo_tabla_detalle t, general.gen_tipo_tabla_detalle t1 ";
		query += "WHERE  trim(t1.ttde_id)=trim(t.ttde_descripcion) and ";
		query += " 		 t1.tipt_id='0036' and t.tipt_id='0134' and trim(t.ttde_valor1)='"+codTipoUsuario+"' and t.ttde_est_reg='A' ";
		query += "       and t1.ttde_est_reg='A' ";
		query += "ORDER BY t1.ttde_descripcion";

		log.info("sql:"+query);


		final Object[] args = new Object[] {codTipoUsuario};
		return jdbcTemplate.query(query,  new MaxDiasPrestamoRowMapper());

	}

	final class MaxDiasPrestamoRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs1, int n) throws SQLException {
			TipoTablaDetalle t = new TipoTablaDetalle();
			t.setCodDetalle(rs1.getString("id"));
			t.setDescripcion(rs1.getString("tipomat"));
			t.setDscValor1(rs1.getString("nromaxprest"));
			t.setCodTipoMat(rs1.getString("codtipomat"));
			return t;
		}
	}

	@SuppressWarnings("unchecked")
	public List<OpcionesApoyo> listaDeOpcionesPorApoyo(String codApoyo) {
		log.info("listaDeOpcionesPorApoyo("+codApoyo+") - start");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();

		String query = "SELECT O.CODOPCION,O.DESCRIPCION, ";
		query += "(CASE ";
		query += "NVL((SELECT TO_CHAR(AO.CODOPCION) FROM BIB_ACCESO_OPCION AO WHERE AO.CODOPCION=O.CODOPCION AND AO.ESTREG='A' AND AO.COD_PERSONA=?),'-') ";
		query += "WHEN '-' THEN 'N' ELSE 'S' END) AS SELECCIONADO, O.ETIQUETA AS ALIAS ";
		query += "FROM BIB_OPCION_APOYO O ";
		query += "ORDER BY O.ORDEN ";

		log.info("sql:"+query);

		final Object[] args = new Object[] {codApoyo};

		return jdbcTemplate.query(query, args, new ListaOpcionesRowMapper());
	}

	final class ListaOpcionesRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			OpcionesApoyo o = new OpcionesApoyo();
//			o.setCodApoyo(rs.getString("CODOPCION"));
			o.setCodOpcion(rs.getString("CODOPCION"));
			o.setNomOpcion(rs.getString("DESCRIPCION"));
			o.setSeleccionado(rs.getString("SELECCIONADO"));
			o.setNomCorto(rs.getString("ALIAS"));
			return o;
		}
	}

	public String updateOpcionesApoyo(String codApoyo, String cadena,
			String nroRegistros, String codUsuario) {
		UpdateOpcionesApoyo update = new UpdateOpcionesApoyo(this.getDataSource());

		HashMap inputs = (HashMap)update.execute(codApoyo, cadena, nroRegistros, codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	@Override
	public String UpdateHorarioSalas(String cadenaDatos, String usuario,
			String nroRegistros) {
		ActualizaHorarioSalas update = new ActualizaHorarioSalas(getDataSource());
		HashMap inputs = (HashMap) update.execute(cadenaDatos, usuario, nroRegistros);
		if (!inputs.isEmpty()) {
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

}
