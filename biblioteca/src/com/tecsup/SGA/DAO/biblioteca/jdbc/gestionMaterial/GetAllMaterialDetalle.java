package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllMaterialDetalle extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllMaterialDetalle.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_GESTION_MAT.SP_SEL_MATERIAL_DET";

	 	private static final String E_C_CODIGO_UNICO = "E_C_CODIGO_UNICO";   
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllMaterialDetalle(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_C_CODIGO_UNICO, OracleTypes.CHAR));       
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new MaterialesxFiltroMapper()));
	        compile();
	    }

	    public Map execute(String txhCodUnico){
	    	
	    	log.info("*** INI "+ SPROC_NAME + " ***");	    	
	    	log.info("E_C_CODIGO_UNICO:"+txhCodUnico);
	    	log.info("*** FIN "+ SPROC_NAME + " ***");
	    	
	    	Map inputs = new HashMap();	    	
	    	inputs.put(E_C_CODIGO_UNICO,txhCodUnico);	        
	        return super.execute(inputs);

	    }
	    
	    final class MaterialesxFiltroMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	BibliotecaMaterialDetalleBean bean 
	        	= new BibliotecaMaterialDetalleBean();
	        	
	        	bean.setCodUnico(rs.getString("CODIGOUNICO"));
	        	bean.setCodTipoMaterial(rs.getString("COD_TIPO_MATERIAL"));
	        	bean.setTitulo(rs.getString("TITULO"));
	        	bean.setCodAutor(rs.getString("COD_AUTOR"));
	        	bean.setDesAutor(rs.getString("DES_CAUTOR"));
	        	bean.setCodDewey(rs.getString("COD_DEWEY"));
	        	bean.setDesDewey(rs.getString("DESC_DEWEY"));
	        	bean.setNroPaginas(rs.getString("NRO_PAGINAS"));
	        	bean.setNroVolumen(rs.getString("NRO_VOLUMEN"));
	        	bean.setNroTotalVolumenes(rs.getString("TOTAL_VOLUMENES"));
	        	bean.setCodEditorial(rs.getString("COD_EDITORIAL"));
	        	bean.setNroEditorial(rs.getString("NRO_EDITORIAL"));
	        	bean.setCodPais(rs.getString("COD_PAIS"));
	        	bean.setCodCiudad(rs.getString("COD_CIUDAD"));
	        	bean.setFecPublicacion(rs.getString("FECHA_PUBLICACION"));
	        	bean.setImagen(rs.getString("IMAGEN"));
	        	bean.setUrlArchDigital(rs.getString("URL_ARCH_DIGITAL"));
	        	bean.setCodIdioma(rs.getString("COD_IDIOMA"));
	        	bean.setIsbn(rs.getString("ISBN"));
	        	bean.setFormato(rs.getString("FORMATO"));
	        	bean.setCodTipoVideo(rs.getString("COD_TIPO_VIDEO"));
	        	bean.setIndPrestSala(rs.getString("IND_PREST_SALA"));
	        	bean.setIndPrestDomicilio(rs.getString("IND_PREST_DOMICILIO"));
	        	bean.setIndReserva(rs.getString("IND_RESERVA"));
	        	bean.setUbicacionFisica(rs.getString("UBICACION_FISICA"));
	        	bean.setCodGenerado(rs.getString("COD_GENERADO"));
	        	bean.setDesEditorial(rs.getString("DES_EDITORIAL"));
	        	bean.setDesIdioma(rs.getString("DES_IDIOMA"));
       			bean.setDesPais(rs.getString("DES_PAIS"));
	        	bean.setDesCiudad(rs.getString("DES_CIUDAD"));
	        	bean.setCodSede(rs.getString("COD_SEDE"));
	        	
	        	bean.setPrefijo(rs.getString("PREFIJO"));
	        	bean.setSufijo(rs.getString("SUFIJO"));
	        	bean.setFrecuencia(rs.getString("FRECUENCIA"));
	        	
	        	bean.setCodGeneradoDewey(rs.getString("CODGENERADODEWEY"));
	        	bean.setMencionSerie(rs.getString("MENCION_SERIE"));
	        	bean.setContenido(rs.getString("CONTENIDO"));
	        		        	
	            return bean;
	        }

	    }
}

