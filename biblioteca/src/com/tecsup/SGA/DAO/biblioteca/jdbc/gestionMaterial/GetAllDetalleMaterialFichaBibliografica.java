package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.DetalleMaterialFichaBibliograficaBean;
import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllDetalleMaterialFichaBibliografica extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllDetalleMaterialFichaBibliografica.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_WEB.SP_SEL_DETALLE_MATERIAL";

	 	private static final String E_C_COD_MATERIAL = "E_C_COD_MATERIAL";   
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllDetalleMaterialFichaBibliografica(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_C_COD_MATERIAL, OracleTypes.CHAR));       
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new MaterialesxFiltroMapper()));
	        compile();
	    }

	    public Map execute(String txhCodUnico){
	    	log.info("**** INI " + SPROC_NAME  + " *****");
			log.info("E_C_COD_MATERIAL:"+txhCodUnico);			
			log.info("**** FIN " + SPROC_NAME  + " *****");
			
	    	Map inputs = new HashMap();	    	
	    	inputs.put(E_C_COD_MATERIAL,txhCodUnico);	        
	        return super.execute(inputs);

	    }
	    
	    final class MaterialesxFiltroMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	DetalleMaterialFichaBibliograficaBean bean 
	        	= new DetalleMaterialFichaBibliograficaBean();
	        	String al="";
	        	
	        	bean.setCodMaterial(rs.getString("COD_MATERIAL"));
	        	bean.setCodTipoMaterial(rs.getString("COD_TIPO_MATERIAL"));
	        	bean.setNomTipoMaterial(rs.getString("NOM_TIPO_MATERIAL"));
	        	bean.setNomMaterial(rs.getString("NOM_MATERIAL"));
	        	bean.setNomAutor(rs.getString("NOM_AUTOR"));
	        	bean.setNomDewey(rs.getString("NOM_DEWEY"));
	        	bean.setNroPags(rs.getString("NRO_PAGS"));
	        	bean.setNomEditorial(rs.getString("NOM_EDITORIAL"));
	        	bean.setNroEdicion(rs.getString("NRO_EDICION"));
	        	bean.setFecPublicacion(rs.getString("FEC_PUBLICACION"));
	        	bean.setNomIdioma(rs.getString("NOM_IDIOMA"));
	        	bean.setNroIsbn(rs.getString("NRO_ISBN"));
	        	bean.setIndSala(rs.getString("IND_SALA"));
	        	bean.setIndDomicilio(rs.getString("IND_DOMICILIO"));
	        	bean.setIndReserva(rs.getString("IND_RESERVA"));
	        	bean.setUbicacionMaterial(rs.getString("UBICACION_MATERIAL"));
	        	bean.setNroEjemplares(rs.getString("NRO_EJEMPLARES"));
	        	
	        	bean.setCodigoGenerado(rs.getString("CODIGO_GENERADO"));
	        	//bean.setFechaPublicacion(rs.getString("FECHA_PUBLICACION"));
	        	bean.setDscCiudad(rs.getString("CIUDAD"));
	        	bean.setDscIsbn(rs.getString("ISBN"));
	        	bean.setContenido(rs.getString("CONTENIDO"));
	        	
	        	al=rs.getString("NRO_INGRESOS");
	        	if(al!=null){
	        		bean.setNroIngresos(al.replace(",", " /"));
	        	}
	        		
	        	bean.setMensionSeries(rs.getString("MENCION_SERIE"));
	        	bean.setDscDescriptores(rs.getString("DESCRIPTORES"));
	        	
	            return bean;
	        }

	    }
}

