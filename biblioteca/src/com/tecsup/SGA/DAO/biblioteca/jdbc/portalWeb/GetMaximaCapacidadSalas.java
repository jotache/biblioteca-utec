package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Sala;

public class GetMaximaCapacidadSalas extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetMaximaCapacidadSalas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_web.sp_sel_max_capac_salas";
    
 	private static final String E_C_TIPO_SALA = "E_C_TIPO_SALA";
    
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetMaximaCapacidadSalas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
      
        declareParameter(new SqlParameter(E_C_TIPO_SALA, OracleTypes.CHAR));
               
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codigoTipoSala) {
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_C_TIPO_SALA " + codigoTipoSala );
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_TIPO_SALA, codigoTipoSala);
    	        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Sala sala= new Sala();
        	
        	sala.setCapacidadMax(rs.getString("CAPACIDAD"));
        	sala.setVacantes(rs.getString("DISPONIBLE"));
        	sala.setFechaBD(rs.getString("FECHA"));
        	
            return sala;
        }

    }
}
