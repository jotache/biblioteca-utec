package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb.GetBusquedaSimple.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.*;

public class GetBusquedaAvanzada extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetBusquedaAvanzada.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_web.sp_sel_busqueda_avanzada";
   	
 	private static final String E_C_TIPO_MATERIAL = "E_C_TIPO_MATERIAL";
 	private static final String E_C_COD_IDIOMA = "E_C_COD_IDIOMA";
 	private static final String E_C_ANIO_INI = "E_C_ANIO_INI";
 	private static final String E_C_ANIO_FIN = "E_C_ANIO_FIN";
 	private static final String E_C_DISPO_SALA = "E_C_DISPO_SALA";
 	private static final String E_C_DISPO_CASA = "E_C_DISPO_CASA";
 	private static final String E_N_BUSCAR_1 = "E_N_BUSCAR_1";
 	private static final String E_V_TEXTO_1 = "E_V_TEXTO_1";
 	private static final String E_N_CONDICION_1 = "E_N_CONDICION_1";
 	private static final String E_N_BUSCAR_2 = "E_N_BUSCAR_2";
 	private static final String E_V_TEXTO_2 = "E_V_TEXTO_2";
 	private static final String E_N_CONDICION_2 = "E_N_CONDICION_2";
 	private static final String E_N_BUSCAR_3 = "E_N_BUSCAR_3";
 	private static final String E_V_TEXTO_3 = "E_V_TEXTO_3";
 	private static final String E_N_ORDENAR_POR = "E_N_ORDENAR_POR";
 	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetBusquedaAvanzada(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
       
        declareParameter(new SqlParameter(E_C_TIPO_MATERIAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_IDIOMA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_ANIO_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_ANIO_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_DISPO_SALA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_DISPO_CASA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_N_BUSCAR_1, OracleTypes.INTEGER));
        declareParameter(new SqlParameter(E_V_TEXTO_1, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_N_CONDICION_1, OracleTypes.INTEGER));
        declareParameter(new SqlParameter(E_N_BUSCAR_2, OracleTypes.INTEGER));
        declareParameter(new SqlParameter(E_V_TEXTO_2, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_N_CONDICION_2, OracleTypes.INTEGER));
        declareParameter(new SqlParameter(E_N_BUSCAR_3, OracleTypes.INTEGER));
        declareParameter(new SqlParameter(E_V_TEXTO_3, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_N_ORDENAR_POR, OracleTypes.INTEGER));
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codTipoMaterial, String codTipoIdioma, String codAnioInicio, String codAnioFin,
    		String txtDisponibiblidadSala, String txtDisponibilidadCasa, int buscar1, String txtTexto1,
    		int condicion1, int buscar2, String txtTexto2, int condicion2, int buscar3, 
    		String txtTexto3, int ordenarBy,String codSede) {
    	 
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_C_TIPO_MATERIAL: "+codTipoMaterial);
    	log.info("E_C_COD_IDIOMA: "+codTipoIdioma);
    	log.info("E_C_ANIO_INI: "+codAnioInicio);
    	log.info("E_C_ANIO_FIN: "+codAnioFin);
    	log.info("E_C_DISPO_SALA: "+txtDisponibiblidadSala);
    	log.info("E_C_DISPO_CASA: "+txtDisponibilidadCasa);
    	log.info("E_N_BUSCAR_1: "+buscar1);
    	log.info("E_V_TEXTO_1: "+txtTexto1);
    	log.info("E_N_CONDICION_1: "+condicion1);
    	log.info("E_N_BUSCAR_2: "+buscar2);
    	log.info("E_V_TEXTO_2: "+txtTexto2);
    	log.info("E_N_CONDICION_2: "+condicion2);
    	log.info("E_N_BUSCAR_3: "+buscar3);
    	log.info("E_V_TEXTO_3: "+txtTexto3);
    	log.info("E_N_ORDENAR_POR: "+ordenarBy);
    	log.info("E_C_COD_SEDE: "+codSede);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_TIPO_MATERIAL, codTipoMaterial);
    	inputs.put(E_C_COD_IDIOMA, codTipoIdioma);
    	inputs.put(E_C_ANIO_INI, codAnioInicio);
    	inputs.put(E_C_ANIO_FIN, codAnioFin);
    	inputs.put(E_C_DISPO_SALA, txtDisponibiblidadSala);
    	inputs.put(E_C_DISPO_CASA, txtDisponibilidadCasa);
    	inputs.put(E_N_BUSCAR_1, buscar1);
    	inputs.put(E_V_TEXTO_1, txtTexto1);
    	inputs.put(E_N_CONDICION_1, condicion1);
    	inputs.put(E_N_BUSCAR_2, buscar2);
    	inputs.put(E_V_TEXTO_2, txtTexto2);
    	inputs.put(E_N_CONDICION_2, condicion2);
    	inputs.put(E_N_BUSCAR_3, buscar3);
    	inputs.put(E_V_TEXTO_3, txtTexto3);
    	inputs.put(E_N_ORDENAR_POR, ordenarBy);
    	inputs.put(E_C_COD_SEDE, codSede);    	        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Busqueda busqueda= new Busqueda();
       
        	busqueda.setCodUnico(rs.getString("CODIGOUNICO")== null ? "&nbsp;" : rs.getString("CODIGOUNICO"));        	
        	busqueda.setTitulo(rs.getString("NOM_MATERIAL")== null ? "&nbsp;" : rs.getString("NOM_MATERIAL")); 
        	busqueda.setTipoMaterial(rs.getString("TIPO_MATERIAL")== null ? "&nbsp;" : rs.getString("TIPO_MATERIAL"));
        	
        	busqueda.setDescripDewey(rs.getString("NOM_DEWEY")== null ? "&nbsp;" : rs.getString("NOM_DEWEY")); 
        	busqueda.setDescripAutor(rs.getString("NOM_AUTOR")== null ? "&nbsp;" : rs.getString("NOM_AUTOR"));        	
        	busqueda.setNroEjemplares(rs.getString("TOTAL_EJEMPLARES")== null ? "&nbsp;" : rs.getString("TOTAL_EJEMPLARES"));
        	busqueda.setPrestaSala(rs.getString("TOTAL_SALA")== null ? "&nbsp;" : rs.getString("TOTAL_SALA"));        	
        	busqueda.setPrestaDomicilio(rs.getString("TOTAL_DOMICILIO")== null ? "&nbsp;" : rs.getString("TOTAL_DOMICILIO"));
        	busqueda.setDisponibilidad(rs.getString("TOTAL_DISPONIBLES")== null ? "&nbsp;" : rs.getString("TOTAL_DISPONIBLES"));        	
         	busqueda.setReserva(rs.getString("TOTAL_RESERVADO")== null ? "&nbsp;" : rs.getString("TOTAL_RESERVADO"));
         	busqueda.setFlagReserva(rs.getString("FLAG_RESERVA")== null ? "&nbsp;" : rs.getString("FLAG_RESERVA"));
         	
        	busqueda.setIsbn(rs.getString("NRO_ISBN")== null ? "&nbsp;" : rs.getString("NRO_ISBN"));        	
        	
        	busqueda.setImagen(rs.getString("IMAGEN")== null ? "&nbsp;" : rs.getString("IMAGEN"));
        	busqueda.setFechaBD(rs.getString("FECHA_ACTUAL")== null ? "&nbsp;" : rs.getString("FECHA_ACTUAL"));
        	busqueda.setDscSede(rs.getString("NOM_SEDE")== null ? "&nbsp;" : rs.getString("NOM_SEDE"));
        	
        	busqueda.setEditorial(rs.getString("EDITORIAL")== null ? "&nbsp;" : rs.getString("EDITORIAL"));
        	busqueda.setCodigoGenerado(rs.getString("CODIGO_GENERADO")== null ? "&nbsp;" : rs.getString("CODIGO_GENERADO"));
        	busqueda.setUbicacionFisica(rs.getString("UBICACION_FISICA")== null ? "&nbsp;" : rs.getString("UBICACION_FISICA"));
        	/*CODIGOUNICO, 
        	NOM_MATERIAL
        	TIPO_MATERIAL
        	NOM_DEWEY
        	NOM_AUTOR

        	 TOTAL_EJEMPLARES
        	 TOTAL_SALA
        	 TOTAL_DOMICILIO
        	 TOTAL_DISPONIBLES
        	 TOTAL_RESERVADO
        	 FLAG_RESERVA
        	 NRO_ISBN
        	IMAGEN
        	FECHA_ACTUAL
        	NOM_SEDE */
            return busqueda;
        }

    }
}
