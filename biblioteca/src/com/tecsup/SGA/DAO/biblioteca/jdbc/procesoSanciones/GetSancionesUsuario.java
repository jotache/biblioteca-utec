package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sancion;

public class GetSancionesUsuario extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetSancionesUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA + ".PKG_BIB_PROCESO_SANCIONES.SP_SEL_SANCIONES_USUARIO";
	private static final String E_V_CODUSUARIO = "V_USUARIO";	
    private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetSancionesUsuario(DataSource dataStore){
    	super(dataStore,SPROC_NAME);
    	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));    	
    	declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new SancionesMapper()));
    	compile();
    }
    public Map execute(String codUsuario){
    	
    	log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("E_V_CODUSUARIO:"+ codUsuario);  
    	log.info("*** FIN " + SPROC_NAME + " ***");
    	
    	HashMap map = new HashMap();
    	map.put(E_V_CODUSUARIO, codUsuario);    	   
    	return super.execute(map);
    }   
    final class SancionesMapper implements RowMapper{

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Sancion sancion = new Sancion();
			sancion.setValorSancion1(rs.getString("CODUSUARIO")==null?"":rs.getString("CODUSUARIO"));
			sancion.setCodFila(rs.getString("ID")==null?"":rs.getString("ID"));
			sancion.setValorSancion2(rs.getString("NOMBRESANCION")==null?"":rs.getString("NOMBRESANCION"));
			sancion.setValorSancion3(rs.getString("FECINI")==null?"":rs.getString("FECINI"));
			sancion.setValorSancion4(rs.getString("FECFIN")==null?"":rs.getString("FECFIN"));
			sancion.setObservacion(rs.getString("OBSERVACION")==null?"":rs.getString("OBSERVACION"));
			sancion.setValorSancion5(rs.getString("ESTADO")==null?"":rs.getString("ESTADO"));
			sancion.setTipoSancion(rs.getString("RUDE_COD_TIPO_SANCION")==null?"":rs.getString("RUDE_COD_TIPO_SANCION"));
			return sancion;
		}
    	
    }
}
