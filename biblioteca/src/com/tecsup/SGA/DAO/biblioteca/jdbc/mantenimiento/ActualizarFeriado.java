package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActualizarFeriado extends StoredProcedure{
	private static Log log = LogFactory.getLog(ActualizarFeriado.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_mantto_config.sp_act_feriados";	
	
	private static final String E_C_COD_TABLA_ID = "E_C_COD_TABLA_ID";
	private static final String E_C_TTDE_ID = "E_C_TTDE_ID";
	private static final String E_C_DIA = "E_C_DIA";
	private static final String E_C_MES = "E_C_MES";
	private static final String E_C_ANIO = "E_C_ANIO";
	private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	private static final String E_V_FLAG = "E_V_FLAG";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	public ActualizarFeriado(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_TABLA_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_TTDE_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_DIA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_MES, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_ANIO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_FLAG, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(String codTablaId, String id, String dis, String mes
			, String anio, String descripcion,String usucrea, String flag) {
	
		log.info("**** INI " + SPROC_NAME + "*****");
		log.info("E_C_COD_TABLA_ID:" + codTablaId);
		log.info("E_C_TTDE_ID:"+ id);
		log.info("E_C_DIA:"+ dis);
		log.info("E_C_MES:"+ mes);
		log.info("E_C_ANIO:"+ anio);
		log.info("E_V_DESCRIPCION:"+ descripcion);
		log.info("E_V_USU_CREA:"+ usucrea);
		log.info("E_V_FLAG:"+ flag);
		log.info("**** FIN " + SPROC_NAME + "*****");
		
		Map inputs = new HashMap();		
		inputs.put(E_C_COD_TABLA_ID, codTablaId);//codigo detalle padre
		inputs.put(E_C_TTDE_ID, id);
		inputs.put(E_C_DIA, dis);
		inputs.put(E_C_MES, mes);
		inputs.put(E_C_ANIO, anio);
		inputs.put(E_V_DESCRIPCION, descripcion);
		inputs.put(E_V_USU_CREA, usucrea);
		inputs.put(E_V_FLAG, flag);			
		return super.execute(inputs);
	
	}
}
