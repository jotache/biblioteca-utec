package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;


public class GetReporteMatMasSolicitado extends StoredProcedure{
    private static Log log = LogFactory.getLog(GetReporteMatMasSolicitado.class);
    private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
    + ".pkg_bib_consultas_reportes.sp_sel_report_mat_mas_solicit";

        private static final String E_V_FEC_INI = "E_V_FEC_INI";
        private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
        private static final String E_V_NRO_MAX = "E_V_NRO_MAX";
        private static final String E_C_SEDE = "E_C_SEDE";
        private static final String RECORDSET = "S_C_RECORDSET";

    public GetReporteMatMasSolicitado(DataSource dataSource) {
            super(dataSource, SPROC_NAME);

            declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(E_V_NRO_MAX, OracleTypes.VARCHAR));
            declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
            declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteMatMasSolicMapper()));
            compile();
        }

        public Map execute(String fecIni, String fecFin, String nroMax,String sede){

            Map inputs = new HashMap();

            log.info("**** INI " + SPROC_NAME + " ****");
            log.info("E_V_FEC_INI:" + fecIni);
            log.info("E_V_FEC_FIN:" + fecFin);
            log.info("E_V_NRO_MAX:" + nroMax);
            log.info("E_C_SEDE:" + sede);
            log.info("**** FIN " + SPROC_NAME + " ****");

            inputs.put(E_V_FEC_INI, fecIni);
            inputs.put(E_V_FEC_FIN, fecFin);
            inputs.put(E_V_NRO_MAX, nroMax);
            inputs.put(E_C_SEDE, sede);
            return super.execute(inputs);

        }

        final class ReporteMatMasSolicMapper implements RowMapper {

            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {


                Reporte reporte = new Reporte();

                reporte.setCodDeweyGenerico(rs.getString("COD_DEWEY_GENERICO") == null ? "" : rs.getString("COD_DEWEY_GENERICO") );
                reporte.setNomDeweyGenerico(rs.getString("NOM_DEWEY_GENERICO") == null ? "" : rs.getString("NOM_DEWEY_GENERICO") );
                reporte.setCodDewey(rs.getString("COD_DEWEY") == null ? "" : rs.getString("COD_DEWEY") );
                reporte.setNomDewey(rs.getString("NOM_DEWEY") == null ? "" : rs.getString("NOM_DEWEY") );
                reporte.setCodigoMaterial(rs.getString("COD_MATERIAL") == null ? "" : rs.getString("COD_MATERIAL") );
                reporte.setCodigo(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO") );
                reporte.setTipoMaterial(rs.getString("TIPO_MATERIAL") == null ? "" : rs.getString("TIPO_MATERIAL") );
                reporte.setTituloMaterial(rs.getString("TITULO") == null ? "" : rs.getString("TITULO") );
                reporte.setNroIngresos(rs.getString("NRO_INGRESO") == null ? "" : rs.getString("NRO_INGRESO") );
                reporte.setTotalEjemplares(rs.getString("TOTAL_EJEMPLARES") == null ? "" : rs.getString("TOTAL_EJEMPLARES") );
                reporte.setNroPrestamosUtec(rs.getString("NRO_PRESTAMOS_UTEC") == null ? "" : rs.getString("NRO_PRESTAMOS_UTEC") );
                reporte.setNroPrestamosTotal(rs.getString("NRO_PRESTAMOS_TOTAL") == null ? "" : rs.getString("NRO_PRESTAMOS_TOTAL") );
                reporte.setIdioma(rs.getString("NOM_IDIOMA") == null ? "" : rs.getString("NOM_IDIOMA") );
                reporte.setAutor(rs.getString("NOM_AUTOR") == null ? "" : rs.getString("NOM_AUTOR"));
                reporte.setAņoPublicacion(rs.getString("ANIO_PUBLICACION") == null ? "" : rs.getString("ANIO_PUBLICACION"));
                reporte.setNombreEditorial(rs.getString("NOM_EDITORIAL") == null ? "" : rs.getString("NOM_EDITORIAL"));

//              System.out.println(String.format("NRO_PRESTAMOS_TOTAL--->%1$s<---|---NRO_PRESTAMOS_UTEC--->%2$s<---",reporte.getNroPrestamosTotal(),reporte.getNroPrestamosUtec()));

                reporte.setNroPrestamosTecsup(String.valueOf(Integer.parseInt(reporte.getNroPrestamosTotal())-Integer.parseInt(reporte.getNroPrestamosUtec())));
                return reporte;
            }

        }
}

