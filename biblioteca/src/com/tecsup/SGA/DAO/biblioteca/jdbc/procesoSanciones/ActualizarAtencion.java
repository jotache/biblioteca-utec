package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActualizarAtencion extends StoredProcedure {

	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".PKG_BIB_PROCESO_SANCIONES.SP_ACT_ATENCION_RECURSO";
	private static Log log = LogFactory.getLog(ActualizarAtencion.class);
	private static final String E_C_COD_ATENCION = "E_C_COD_ATENCION";	
	private static final String E_C_TIPO = "E_C_TIPO";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActualizarAtencion(DataSource dataSource){
		super(dataSource, SPROC_NAME);		
		declareParameter(new SqlParameter(E_C_COD_ATENCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	public Map execute(String codAtencion, String tipoOperacion, String codUsuarioActualiza) {
		Map inputs = new HashMap();
		
		log.info("*** INI " + SPROC_NAME + " ***");
		log.info("E_C_COD_ATENCION: " + codAtencion);
		log.info("E_C_TIPO: " + tipoOperacion);
		log.info("E_V_CODUSUARIO: " + codUsuarioActualiza);	
		log.info("*** FIN " + SPROC_NAME + " ***");
		
		inputs.put(E_C_COD_ATENCION,codAtencion);
		inputs.put(E_C_TIPO,tipoOperacion);	
		inputs.put(E_V_CODUSUARIO, codUsuarioActualiza);				
		
		return super.execute(inputs);
	}
}
