package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class TerminarReservaRecurso extends StoredProcedure {
	private static Log log = LogFactory.getLog(TerminarReservaRecurso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
		".PKG_BIB_PROCESO_SANCIONES.SP_TERM_APLICAR_RESERVA";

	private static final String E_C_CODRECURSO = "E_C_CODRECURSO";
	private static final String E_V_SECUENCIA = "E_V_SECUENCIA";	
	private static final String E_V_COD_USUARIO= "E_V_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";

	public TerminarReservaRecurso(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODRECURSO, OracleTypes.CHAR));	
		declareParameter(new SqlParameter(E_V_SECUENCIA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(
			String codRecurso,String codSecuencia,String usuario			
			) {    	
	Map inputs = new HashMap();
	
	log.info("**** INI "  + SPROC_NAME + "******");
	log.info("E_C_CODRECURSO:"+codRecurso);
	log.info("E_V_SECUENCIA:"+codSecuencia);
	log.info("E_V_COD_USUARIO:"+usuario);	
	log.info("**** FIN "  + SPROC_NAME + "******");
	
	inputs.put(E_C_CODRECURSO,codRecurso);
	inputs.put(E_V_SECUENCIA,codSecuencia);
	inputs.put(E_V_COD_USUARIO,usuario);		
	
	return super.execute(inputs);
	
	}
}
