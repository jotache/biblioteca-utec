package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllTablaDetalleBiblioteca extends StoredProcedure{

	private static Log log = LogFactory.getLog(GetAllTablaDetalleBiblioteca.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
		+ ".pkg_bib_comun.sp_sel_tabla_detalle_biblio";
	    
	 	private static final String E_C_TTDE_PADRE_ID = "E_C_TTDE_PADRE_ID";
	    private static final String E_C_COD_ID = "E_C_COD_ID";
	    private static final String E_C_COD_TABLA_ID = "E_C_COD_TABLA_ID";
	    private static final String E_V_CODIGO = "E_V_CODIGO";
	    private static final String E_V_TTDE_DESCRIPCION = "E_V_TTDE_DESCRIPCION";
	    private static final String E_C_TIPO = "E_C_TIPO";	    
	    private static final String E_V_VALOR4 = "E_V_VALOR4";
	    private static final String E_V_VALOR5 = "E_V_VALOR5";
	    private static final String E_V_VALOR6 = "E_V_VALOR6";
	    private static final String E_C_TIPOLISTA= "E_C_TIPOLISTA";
	   
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllTablaDetalleBiblioteca(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        
	        declareParameter(new SqlParameter(E_C_TTDE_PADRE_ID, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_COD_ID, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_COD_TABLA_ID, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_V_TTDE_DESCRIPCION, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));	       	        
	        declareParameter(new SqlParameter(E_V_VALOR4, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_VALOR5, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_VALOR6, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_TIPOLISTA, OracleTypes.CHAR));	        
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	        compile();
	    }

	    public Map execute(String codTablaPadre, String codPadre, String codHijo, String codigo
	    					, String descripcion, String tipoOrden, String valor4,String valor5,String valor6,
	    					String tipoLista) {
	    	
	    	Map inputs = new HashMap();
	    	
	    	log.info("****** INI .pkg_bib_comun.sp_sel_tabla_detalle_biblio *******");
	    	log.info("E_C_TTDE_PADRE_ID codTablaPadre: "+codTablaPadre);
	    	log.info("E_C_COD_ID codPadre: "+codPadre);
	    	log.info("E_C_COD_TABLA_ID codHijo: "+codHijo);
	    	log.info("E_V_CODIGO codigo: "+codigo);
	    	log.info("E_V_TTDE_DESCRIPCION descripcion: "+descripcion);
	    	log.info("E_C_TIPO tipoOrden: "+tipoOrden);
	    	log.info("E_V_VALOR4 : "+valor4);
	    	log.info("E_V_VALOR5 : "+valor5);
	    	log.info("E_V_VALOR6 : "+valor6);
	    	log.info("E_C_TIPOLISTA: " + tipoLista);
	    	log.info("****** FIN .pkg_bib_comun.sp_sel_tabla_detalle_biblio *******");	    	
	    	
	    	inputs.put(E_C_TTDE_PADRE_ID, codTablaPadre);
	        inputs.put(E_C_COD_ID, codPadre);
	        inputs.put(E_C_COD_TABLA_ID, codHijo);
	        inputs.put(E_V_CODIGO, codigo);
	        inputs.put(E_V_TTDE_DESCRIPCION, descripcion);
	        inputs.put(E_C_TIPO, tipoOrden);	        
	        inputs.put(E_V_VALOR4, valor4);
	        inputs.put(E_V_VALOR5, valor5);
	        inputs.put(E_V_VALOR6, valor6);
	        inputs.put(E_C_TIPOLISTA, tipoLista);
	        return super.execute(inputs);

	    }
	    
	    final class ProcesoMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
	        	tipoTablaDetalle.setCodTipoTablaDetalle(rs.getString("CODIGOSECUENCIAL"));//ttde_id
	        	tipoTablaDetalle.setDescripcion(rs.getString("DESCRIPCION"));//descripcion_hijo
	        	tipoTablaDetalle.setDscValor1(rs.getString("CODIGOPADRE"));//valor1
	        	tipoTablaDetalle.setDscValor2(rs.getString("CODIGO"));//valor2
	        	tipoTablaDetalle.setDescripTipoSala(rs.getString("DESC_PADRE"));//descripcion_padre
	        	tipoTablaDetalle.setDscValor3(rs.getString("VALOR1"));//ULTIMO CAMBIO
	        	//*****************************************************
	        	tipoTablaDetalle.setDscValor4(rs.getString("VALOR2"));//SANCIONES VALOR2
	        	tipoTablaDetalle.setDscValor5(rs.getString("VALOR3"));//SANCIONES VALOR3
	        	
	        	tipoTablaDetalle.setDscValor6(rs.getString("VALOR6"));
	        	tipoTablaDetalle.setDscValor7(rs.getString("VALOR7"));
	        	tipoTablaDetalle.setDscValor8(rs.getString("VALOR8"));
	        	tipoTablaDetalle.setDscValor9(rs.getString("VALOR9"));
	        	tipoTablaDetalle.setDscValor10(rs.getString("VALOR10"));
	        	tipoTablaDetalle.setEstReg(rs.getString("ESTADO"));
	        	
	            return tipoTablaDetalle;
	        }

	    }
}

