package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertDevolucion extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertDevolucion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_GESTION_MAT.SP_INS_DEVOLUCION";	
		
	private static final String E_V_COD_PRESTAMO = "E_V_COD_PRESTAMO"; 
	private static final String E_V_COD_USU_PRESTAMO = "E_V_COD_USU_PRESTAMO"; 
	private static final String E_V_FLAG_SANCION = "E_V_FLAG_SANCION";		
	private static final String E_V_USU_CREA = "E_V_USU_CREA";	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertDevolucion(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_COD_PRESTAMO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_COD_USU_PRESTAMO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FLAG_SANCION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codPrestamo,
			String usuPrestamo, String flgSancion, String usuCreacion) {    	
	Map inputs = new HashMap();
	
	log.info("****** INI "+SPROC_NAME+" *****");
	log.info("E_V_COD_PRESTAMO:"+codPrestamo);
	log.info("E_V_COD_USU_PRESTAMO:"+usuPrestamo);
	log.info("E_V_FLAG_SANCION:"+flgSancion);
	log.info("E_V_USU_CREA:"+usuCreacion);
	log.info("****** FIN "+SPROC_NAME+" *****");
	
	inputs.put(E_V_COD_PRESTAMO, codPrestamo);
	inputs.put(E_V_COD_USU_PRESTAMO, usuPrestamo);
	inputs.put(E_V_FLAG_SANCION, flgSancion);
	inputs.put(E_V_USU_CREA, usuCreacion);	
	
	return super.execute(inputs);
	}

	
}
