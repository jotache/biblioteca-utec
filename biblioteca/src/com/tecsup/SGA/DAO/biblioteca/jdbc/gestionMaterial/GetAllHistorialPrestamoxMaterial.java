package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.HistorialBean;
import com.tecsup.SGA.bean.IngresosxMaterialBean;
import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllHistorialPrestamoxMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllHistorialPrestamoxMaterial.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_GESTION_MAT.sp_sel_hist_prest_x_material";
	 	
	 	private static final String E_C_TIPO_MATERIAL = "E_C_TIPO_MATERIAL";	 	   
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllHistorialPrestamoxMaterial(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_C_TIPO_MATERIAL, OracleTypes.CHAR));	               
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new IngresosxMaterialMapper()));
	        compile();
	    }

	    public Map execute(String txhCodMaterial){
	    	log.info("**** INI " + SPROC_NAME  + " *****");
			log.info("E_C_TIPO_MATERIAL:"+txhCodMaterial);			
			log.info("**** FIN " + SPROC_NAME  + " *****");
			
	    	Map inputs = new HashMap();
	    	inputs.put(E_C_TIPO_MATERIAL,txhCodMaterial);    	
	        return super.execute(inputs);
	    }
	    
	    final class IngresosxMaterialMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	HistorialBean bean = new HistorialBean();
	        	
	        	bean.setCodUsuario(rs.getString("COD_USUARIO"));
	        	bean.setNomUsuario(rs.getString("NOMBRE_USUARIO"));
	        	bean.setUsuario(rs.getString("USUARIO"));
	        	bean.setFecPrestamo(rs.getString("FECHA_PRESTAMO"));
	        	bean.setFecPrestamoProg(rs.getString("FECHA_PRESTAMO_PROG"));
	        	bean.setFecDevolucion(rs.getString("FECHA_DEVOLUCION"));
	        	bean.setNroIngreso(rs.getString("NRO_INGRESO"));
	        	
	            return bean;
	        }

	    }
}

