package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.DescriptorxMaterialBean;
import com.tecsup.SGA.bean.IngresosxMaterialBean;
import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllDescriptorxMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllDescriptorxMaterial.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_GESTION_MAT.sp_sel_descriptor_x_material";

	 	private static final String E_C_CODIGO_UNICO = "E_C_CODIGO_UNICO";   
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllDescriptorxMaterial(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_C_CODIGO_UNICO, OracleTypes.CHAR));       
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new DescriptorxMaterialMapper()));
	        compile();
	    }

	    public Map execute(String txhCodUnico){
	    	
	    	log.info("**** INI " + SPROC_NAME  + " *****");
			log.info("E_C_CODIGO_UNICO:"+txhCodUnico);			
			log.info("**** FIN " + SPROC_NAME  + " *****");
			
	    	Map inputs = new HashMap();	    	
	    	inputs.put(E_C_CODIGO_UNICO,txhCodUnico);	        
	        return super.execute(inputs);

	    }
	    
	    final class DescriptorxMaterialMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	DescriptorxMaterialBean bean = new DescriptorxMaterialBean();
	        	
	        	bean.setCodUnico(rs.getString("CODIGOUNICO"));
	        	bean.setFecRegistro(rs.getString("FECHA_REG"));
	        	bean.setCodDescriptor(rs.getString("COD_DESCRIPTOR"));
	        	bean.setDesDescriptor(rs.getString("DESC_DESCRIPTOR"));
	        	bean.setCodUsuario(rs.getString("COD_USUARIO"));
	        	bean.setUsuario(rs.getString("USUARIO"));
	        	
	            return bean;
	        }

	    }
}

