package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.HistorialProrrogaBean;
import com.tecsup.SGA.common.CommonConstants;

public class GetAllHistorialProrrogaxMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllHistorialProrrogaxMaterial.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_GESTION_MAT.SP_SEL_HIST_PRORROG_X_MATERIAL";
	 	
	 	private static final String E_C_NRO_INGRESO = "E_C_NRO_INGRESO";
	 	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllHistorialProrrogaxMaterial(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_C_NRO_INGRESO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new IngresosxMaterialMapper()));
	        compile();
	    }

	    @SuppressWarnings("unchecked")
		public Map execute(String txhNroIngreso, String txhCodUsuario, String sede){
	    	log.info("**** INI " + SPROC_NAME  + " *****");
			log.info("E_C_NRO_INGRESO:"+txhNroIngreso);
			log.info("E_V_COD_USUARIO:"+txhCodUsuario);
			log.info("E_C_SEDE:"+sede);	
			log.info("**** FIN " + SPROC_NAME  + " *****");
			
	    	Map inputs = new HashMap();
	    	inputs.put(E_C_NRO_INGRESO,txhNroIngreso);
	    	inputs.put(E_V_COD_USUARIO,txhCodUsuario);
	    	inputs.put(E_C_SEDE,sede);
	        return super.execute(inputs);
	    }
	    
	    final class IngresosxMaterialMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	HistorialProrrogaBean bean = new HistorialProrrogaBean();
	        	
	        	bean.setFecPrestamo(rs.getString("FECHA_PRESTAMO"));
	        	bean.setFecDevolucionProg(rs.getString("FECHA_DEVOLUCION_PROG"));
	        	bean.setFecDevolucion(rs.getString("FECHA_DEVOLUCION"));
	        	bean.setFlagProrroga(rs.getString("FLAG_PRORROGA"));
	        	bean.setNomAsistente(rs.getString("NOMBRE_ASISTENTE"));
	        	
	            return bean;
	        }

	    }
}

