package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.Impl.BiblioGestionMaterialManagerImpl;

public class GetAllMaterialesxFiltro extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllMaterialesxFiltro.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
		+ ".PKG_BIB_GESTION_MAT.SP_SEL_MATERIAL_X_FILTROS";
	 
	 	private static final String E_C_CODIGO = "E_C_CODIGO";
	    private static final String E_C_NRO_INGRESO = "E_C_NRO_INGRESO";
	    private static final String E_C_COD_TIPO_MAT = "E_C_COD_TIPO_MAT";
	    private static final String E_C_BUSCAR_POR = "E_C_BUSCAR_POR";
	    private static final String E_V_DESC_BUSCAR_POR = "E_V_DESC_BUSCAR_POR";
	    private static final String E_C_CODIDIOMA = "E_C_CODIDIOMA";
	    private static final String E_C_ANIO_INI = "E_C_ANIO_INI";
	    private static final String E_C_ANIO_FIN = "E_C_ANIO_FIN";
	    private static final String E_C_FEC_RESERVA_INI = "E_C_FEC_RESERVA_INI";
	    private static final String E_C_FEC_RESERVA_FIN = "E_C_FEC_RESERVA_FIN";
	    private static final String E_C_SEDE = "E_C_SEDE IN CHAR";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllMaterialesxFiltro(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        
	       // declareParameter(new SqlParameter(E_C_CODIGO_UNICO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_CODIGO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_NRO_INGRESO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_COD_TIPO_MAT, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_BUSCAR_POR, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_V_DESC_BUSCAR_POR, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_CODIDIOMA, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_ANIO_INI, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_ANIO_FIN, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_FEC_RESERVA_INI, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_FEC_RESERVA_FIN, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new MaterialesxFiltroMapper()));
	        compile();
	    }

	    public Map execute(String txhCodigoUnico,String txtCodigo, String txtNroIngreso,
				String cboTipoMaterial, String cboBuscarPor, String txtTitulo,
				String cboIdioma, String cboAnioIni, String cboAnioFin,
				String txtFechaReservaIni, String txtFechaReservaFin, String sede){
	    	
	    	Map inputs = new HashMap();
	    	
	    	log.info("*** INI " + SPROC_NAME + "***");
	    	log.info("E_C_CODIGO>>" + txtCodigo);
	    	log.info("E_C_NRO_INGRESO>>" + txtNroIngreso);
	    	log.info("E_C_COD_TIPO_MAT>>" + cboTipoMaterial);
	    	log.info("E_C_BUSCAR_POR>>" + cboBuscarPor);
	    	log.info("E_V_DESC_BUSCAR_POR>>" + txtTitulo);
	    	log.info("E_C_CODIDIOMA>>" + cboIdioma);
	    	log.info("E_C_ANIO_INI>>" + cboAnioIni);
	    	log.info("E_C_ANIO_FIN>>" + cboAnioFin);
	    	log.info("E_C_FEC_RESERVA_INI>>" + txtFechaReservaIni);
	    	log.info("E_C_FEC_RESERVA_FIN>>" + txtFechaReservaFin);
	    	log.info("E_C_SEDE>>" + sede);
	    	log.info("*** FIN " + SPROC_NAME + "***");

	    	//inputs.put(E_C_CODIGO_UNICO, txhCodigoUnico);
	    	inputs.put(E_C_CODIGO, txtCodigo);
	        inputs.put(E_C_NRO_INGRESO, txtNroIngreso);
	        inputs.put(E_C_COD_TIPO_MAT, cboTipoMaterial);
	        inputs.put(E_C_BUSCAR_POR, cboBuscarPor);
	        inputs.put(E_V_DESC_BUSCAR_POR, txtTitulo);
	        inputs.put(E_C_CODIDIOMA, cboIdioma);
	        inputs.put(E_C_ANIO_INI, cboAnioIni);
	        inputs.put(E_C_ANIO_FIN, cboAnioFin);
	        inputs.put(E_C_FEC_RESERVA_INI, txtFechaReservaIni);
	        inputs.put(E_C_FEC_RESERVA_FIN, txtFechaReservaFin);
	        inputs.put(E_C_SEDE, sede);
	        
	        return super.execute(inputs);

	    }
	    
	    final class MaterialesxFiltroMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	MaterialesxFiltro materialesxFiltro = new MaterialesxFiltro();
	        	materialesxFiltro.setCodUnico(rs.getString("CODIGOUNICO")== null ? "" : rs.getString("CODIGOUNICO"));
	        	materialesxFiltro.setCodGenerado(rs.getString("CODGENERADO")== null ? "" : rs.getString("CODGENERADO"));
	        	materialesxFiltro.setCodTipoMat(rs.getString("CODTIPOMAT")== null ? "" : rs.getString("CODTIPOMAT"));
	        	materialesxFiltro.setDescTipoMat(rs.getString("DESCTIPOMAT")== null ? "" : rs.getString("DESCTIPOMAT"));
	        	materialesxFiltro.setTitulo(rs.getString("TITULO")== null ? "" : rs.getString("TITULO"));
	        	materialesxFiltro.setCodAutor(rs.getString("CODAUTOR")== null ? "" : rs.getString("CODAUTOR"));	        	
	        	materialesxFiltro.setDescAutor(rs.getString("DESCAUTOR")== null ? "" : rs.getString("DESCAUTOR"));
	        	materialesxFiltro.setVolumenes(rs.getString("VOLUMENES")== null ? "" : rs.getString("VOLUMENES"));
	        	materialesxFiltro.setPrestSala(rs.getString("PREST_SALA")== null ? "" : rs.getString("PREST_SALA"));
	        	materialesxFiltro.setPrestDomicilio(rs.getString("PREST_DOMICILIO")== null ? "" : rs.getString("PREST_DOMICILIO"));
	        	materialesxFiltro.setReserva(rs.getString("RESERVA")== null ? "" : rs.getString("RESERVA"));
	        	materialesxFiltro.setDispo(rs.getString("DISPO")== null ? "" : rs.getString("DISPO"));
	        	materialesxFiltro.setNomSede(rs.getString("NOM_SEDE")== null ? "" : rs.getString("NOM_SEDE"));	        	
	        	materialesxFiltro.setFlgReserva(rs.getString("FLAG_RESERVA")== null ? "" : rs.getString("FLAG_RESERVA"));	        	
	        	materialesxFiltro.setDewey(rs.getString("DEWEY")== null ? "" : rs.getString("DEWEY"));	        	
	        	materialesxFiltro.setFechaPublicacion(rs.getString("FECHA_PUBLICACION")== null ? "" : rs.getString("FECHA_PUBLICACION"));	  
	        	materialesxFiltro.setNomEditorial(rs.getString("EDITORIAL")== null ? "" : rs.getString("EDITORIAL"));	        	
	            return materialesxFiltro;
	        }

	    }
}

