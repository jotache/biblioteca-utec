package com.tecsup.SGA.DAO.biblioteca.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.biblioteca.BiblioProcesosSancionesDAO;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.ActualizarPrestamoBloque;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.GetSancionesUsuario;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.InsertInhabilitaciones;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.InsertReservaMaterial;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.InsertSancion;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.InsertSancionReservaSalas;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.UpdateSancionUsuario;
import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Sancion;



public class BiblioProcesosSancionesDAOJdbc extends JdbcDaoSupport implements BiblioProcesosSancionesDAO{
	private static Log log = LogFactory.getLog(BiblioProcesosSancionesDAOJdbc.class);
	
	public String insertSancion(String nroIngreso,String fecDevolucion,String usuario,String sede,String guardaSancion) {
		InsertSancion insertSancion = new InsertSancion(this.getDataSource());
		
		HashMap inputs = (HashMap)insertSancion.execute( nroIngreso,fecDevolucion,usuario,sede,guardaSancion);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public String insertReservaMaterial(String codMaterial,String fecReserva,String codUsuarioReserva,String codUsuario) {
		InsertReservaMaterial insertReservaMaterial = new InsertReservaMaterial(this.getDataSource());
		
		HashMap inputs = (HashMap)insertReservaMaterial.execute(
				 codMaterial,
				 fecReserva,
				 codUsuarioReserva,
				 codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public String insertSancionReservaSalas(
			String codUsuario,
			String recCodigo,
			String renSecuencia,
			String usuario,
			String estadoReserva) {
		
		InsertSancionReservaSalas insertSancionReservaSalas 
		= new InsertSancionReservaSalas(this.getDataSource());
		
		HashMap inputs = (HashMap)insertSancionReservaSalas.execute(
				codUsuario,
				recCodigo,
				renSecuencia,
				usuario,
				estadoReserva);
		
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}

	public List<Sancion> getSancionesUsuario(String codUsuario) {
		GetSancionesUsuario usuarios = new GetSancionesUsuario(this.getDataSource());
		HashMap outputs = (HashMap) usuarios.execute(codUsuario);
		if (!outputs.isEmpty()){
			return (List<Sancion>) outputs.get(CommonConstants.RET_CURSOR);			
		}
		return null;
	}

	public String updateSancionUsuario(String codUsuario, String idSancion,String codUsuarioAccion) {
		UpdateSancionUsuario udpate = new UpdateSancionUsuario(this.getDataSource());
		HashMap inputs = (HashMap) udpate.execute(codUsuario, idSancion, codUsuarioAccion);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}

	public String insertarInhabilitaciones(String codUsuario,
			String cadSansiones, String nroReg, String fecIni, String fecFin, String obsInhabi,
			String codUsuarioAccion) {
		InsertInhabilitaciones update = new InsertInhabilitaciones(this.getDataSource());
		HashMap inputs = (HashMap) update.execute(codUsuario, cadSansiones, nroReg, fecIni, fecFin, obsInhabi, codUsuarioAccion);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<MaterialxIngresoBean> listaBloqueLibrosPrestados(
			String codBloque) {
		
		log.info("listaBloqueLibrosPrestados() " + "codBloque:"+codBloque+ " - start");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		
		String query = "";		
		query +=" SELECT  	p.mprc_id, m.mbib_id,m.mbib_titulo, p.mden_secuencia ,p.mprc_ind_prestamo, TO_CHAR(p.mprc_fec_prestamo,'dd/MM/YYYY') fecPrestamo, p.mprc_usu_prestamo, ";
		query +=" 		  	(select u.usuario from BIB_USUARIO u where u.codusuario=p.mprc_usu_prestamo and rownum<2) Usuario, (select u.nombreusuario from BIB_USUARIO u where u.codusuario=p.mprc_usu_prestamo and rownum<2) NombreUsuario, TO_CHAR(p.mprc_fec_devolucion,'dd/MM/yyyy') fecDevolucion, TO_CHAR(p.mprc_fec_devolucion_prog,'dd/MM/yyyy') fecDevolucionProg, "; 
		query +=" 			p.mprc_estado codestado, td.ttde_descripcion nomestado, p.mprc_ind_sancion,p.MPRC_COD_PREST_MULTIPLE, m.mbib_codgenerado DEWEY,tm.ttde_descripcion TIPOMATERIAL, ";
		query +="           m.mbib_cod_sede as Sedemat, ";
		query +="           tu.ttde_descripcion as TipoUsuario, NVL(p.mprc_ind_prorroga,'0') ind_prorroga, ";
		query +="           (SELECT COUNT(*) FROM BIB_MATERIAL_PRESTAMO pr WHERE pr.MPRC_EST_REG    ='A' AND pr.MDEN_SECUENCIA = p.mden_secuencia ";
		query +=" 				AND pr.MPRC_USU_PRESTAMO = p.mprc_usu_prestamo AND NVL(pr.MPRC_IND_PRORROGA,'0')='1') nro_prorrogas, ";
		query +="           nvl(tc.ttde_valor10,'0') as TOTAL_prorrogas ";

		query +=" FROM 		BIB_MATERIAL_PRESTAMO p , BIB_MATERIAL m, GENERAL.GEN_TIPO_TABLA_DETALLE td, ";
		query +=" 			general.gen_tipo_tabla_detalle tm, ";
		query +="			bib_usuario u, general.gen_tipo_tabla_detalle tu, general.gen_tipo_tabla_detalle tc ";
		
		query +=" WHERE 	p.mbib_id=m.mbib_id and p.MPRC_COD_PREST_MULTIPLE=? AND TRIM(td.TTDE_ID) = TRIM(p.mprc_estado) AND td.tipt_id='0084' ";
		query +="			and tm.tipt_id='0036' and TRIM(tm.ttde_id)=TRIM(m.mbib_cod_tipo) ";
		query +="			and NVL(p.mprc_ind_prorroga,'0')<>'2' ";
		query +="           and u.codusuario=p.mprc_usu_prestamo and tu.tipt_id='0049' and trim(tu.ttde_id)=trim(u.cod_tipo_usuario_sga) ";
		query +="           and tc.tipt_id='0074' and trim(tc.ttde_valor1) = trim(tu.ttde_id) and trim(tu.ttde_id)=trim(u.cod_tipo_usuario_sga)  ";
		query +=" ORDER BY 	p.mprc_fec_prestamo ";
		
		log.info(query);
		
		final Object[] args = new Object[] {codBloque};
		log.info("listaBloqueLibrosPrestados() " + "codBloque:"+codBloque+ " - end");
		return jdbcTemplate.query(query, args, new LibrosBloquePrestadosRowMapper());
	}
	
	final class LibrosBloquePrestadosRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			MaterialxIngresoBean lib = new MaterialxIngresoBean();
			lib.setCodPrestamo(rs.getString("mprc_id"));
			lib.setCodMaterialBib(rs.getString("mbib_id"));
			lib.setTitulo(rs.getString("mbib_titulo"));
			lib.setNroIngreso(rs.getString("mden_secuencia"));
			lib.setIndTipoPrestamo(rs.getString("mprc_ind_prestamo"));
			lib.setFecPrestamo(rs.getString("fecPrestamo"));
			lib.setCodUsuario(rs.getString("mprc_usu_prestamo"));
			lib.setNombreUsuario(rs.getString("NombreUsuario"));
			lib.setFecDevolucion(rs.getString("fecDevolucion")==null?"":rs.getString("fecDevolucion"));
			lib.setFecDevolucionProg(rs.getString("fecDevolucionProg"));
			lib.setCodPrestamoMultiple(rs.getString("MPRC_COD_PREST_MULTIPLE"));
			lib.setCodDewey(rs.getString("DEWEY"));
			lib.setDesTipoMaterial(rs.getString("TIPOMATERIAL"));
			lib.setUsuarioReserva(rs.getString("Usuario"));
			lib.setSede(rs.getString("Sedemat"));
			lib.setTipoDeUsuario(rs.getString("TipoUsuario"));
			lib.setIndProrroga(rs.getString("ind_prorroga"));
			lib.setNroProrrogas(rs.getString("nro_prorrogas"));
			lib.setLimiteProrrogas(rs.getString("TOTAL_prorrogas"));
			return lib;
		}
	}

	public String actualizarPrestamoBloque(String cadenaDatos, String nroReg,
			String usuarioPrestamo, String usuarioSistema) {
		ActualizarPrestamoBloque update = new ActualizarPrestamoBloque(this.getDataSource());
		
		HashMap inputs = (HashMap)update.execute(cadenaDatos,nroReg,usuarioPrestamo,usuarioSistema);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
}
