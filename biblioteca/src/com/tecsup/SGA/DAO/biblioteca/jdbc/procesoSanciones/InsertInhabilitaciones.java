package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertInhabilitaciones extends StoredProcedure {
	private static Log log = LogFactory.getLog(InsertInhabilitaciones.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_PROCESO_SANCIONES.SP_INS_INHABILITAR_SERVICIOS";

	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String E_V_CADSANCIONES = "E_V_CADSANCIONES";	
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";	
	private static final String E_V_FECHAINI= "E_V_FECHAINI";
	private static final String E_V_FECHAFIN= "E_V_FECHAFIN";
	private static final String E_V_OBSERVACION= "E_V_OBSERVACION";
	private static final String E_V_CODUSUARIO_ACCION = "E_V_CODUSUARIO_ACCION";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertInhabilitaciones(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));	
		declareParameter(new SqlParameter(E_V_CADSANCIONES, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));		
		declareParameter(new SqlParameter(E_V_FECHAINI, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FECHAFIN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_OBSERVACION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO_ACCION, OracleTypes.VARCHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codUsuario,String cadSansiones,String nroReg, String fecIni, String fecFin, String obsInhabi, String codUsuarioAccion) {    	
		Map inputs = new HashMap();
		
		log.info("****" + SPROC_NAME + "******");
		log.info("E_V_CODUSUARIO:"+codUsuario);
		log.info("E_V_CADSANCIONES:"+cadSansiones);
		log.info("E_V_NROREGISTROS:"+nroReg);
		log.info("E_V_FECHAINI:"+fecIni);
		log.info("E_V_FECHAFIN:"+fecFin);
		log.info("E_V_OBSERVACION:"+obsInhabi);
		log.info("E_V_CODUSUARIO_ACCION:"+codUsuarioAccion);
		log.info("**** FIN " + SPROC_NAME + "******");
		
		inputs.put(E_V_CODUSUARIO,codUsuario);
		inputs.put(E_V_CADSANCIONES,cadSansiones);	
		inputs.put(E_V_NROREGISTROS, nroReg);				
		inputs.put(E_V_FECHAINI,fecIni);
		inputs.put(E_V_FECHAFIN,fecFin);
		inputs.put(E_V_OBSERVACION,obsInhabi);
		inputs.put(E_V_CODUSUARIO_ACCION,codUsuarioAccion);
		
		return super.execute(inputs);
	}
}
