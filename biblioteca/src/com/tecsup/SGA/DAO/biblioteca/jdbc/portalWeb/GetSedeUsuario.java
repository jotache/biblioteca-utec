package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetSedeUsuario extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetSedeUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA + ".PKG_BIB_COMUN.SP_SEL_SEDE_USUARIOPORTAL";
	private static final String pUSUARIO = "pUSUARIO";
	private static final String pSEDE = "pSEDE";	
	private static final String pRESULTADO = "pRESULTADO";
	
	public GetSedeUsuario(DataSource dataSource){
		super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(pUSUARIO, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(pSEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(pRESULTADO, OracleTypes.INTEGER));
        compile();
	}
	
	 public Map execute(String usuario ) {
		 
		 log.info("**** INI "  + SPROC_NAME + "*****");
		 log.info("pUSUARIO:" + usuario);
		 log.info("**** FIN "  + SPROC_NAME + "*****");
		 
         Map inputs = new HashMap();
         inputs.put(pUSUARIO, usuario);
         return super.execute(inputs);
}
}
