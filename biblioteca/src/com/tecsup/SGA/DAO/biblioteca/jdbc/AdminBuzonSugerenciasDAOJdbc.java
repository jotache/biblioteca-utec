package com.tecsup.SGA.DAO.biblioteca.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.biblioteca.AdminBuzonSugerenciasDAO;
import com.tecsup.SGA.DAO.biblioteca.BiblioMantenimientoDAO;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.GetAllSugerencias;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.InsertSugerencia;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.InsertTablaDetalleBiblioteca;
import com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento.UpdateAtenderBuzonSugerencia;
import com.tecsup.SGA.modelo.BuzonSugerencia;

public class AdminBuzonSugerenciasDAOJdbc extends JdbcDaoSupport implements AdminBuzonSugerenciasDAO{

	public List GetAllSugerencias(String mes,String anio, String codigoSugerencia, String estado)
	{    
		GetAllSugerencias getAllSugerencias = new GetAllSugerencias(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSugerencias.execute(mes, anio, codigoSugerencia, estado);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}//
	
	public String UpdateAtenderBuzonSugerencia(String codSugerencia, String codCalificacion, 
			String codTipoMaterial,	String respuesta)
	{
		UpdateAtenderBuzonSugerencia updateAtenderBuzonSugerencia = new UpdateAtenderBuzonSugerencia(this.getDataSource());
			
			HashMap inputs = (HashMap)updateAtenderBuzonSugerencia.execute(codSugerencia, codCalificacion,
					codTipoMaterial, respuesta);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public String InsertSugerencia(String tipo, String descripcion, String email, String usuario){
		
		InsertSugerencia insertSugerencia = new InsertSugerencia(this.getDataSource());
		
		HashMap inputs = (HashMap)insertSugerencia.execute(tipo, descripcion, email, usuario);
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public BuzonSugerencia obtenerSugerencia(String id) {				
		JdbcTemplate jt = getJdbcTemplate();
		String sql = "select sugn_id,  sugc_descripcion from bib_sugerencia where sugn_id=?";
		/*buzonSugerencia.getDescripcion().trim()+"','"+buzonSugerencia.getDescripCalificacion().trim()+"','"+
			buzonSugerencia.getDescripCodMaterial().trim()+"','"+buzonSugerencia.getDescripcionUsuario()*/
		final BuzonSugerencia sugerencia = new BuzonSugerencia();
		final Object[] params = new Object[] {id};
		jt.query(sql, params, new RowCallbackHandler() {
			public void processRow(ResultSet rs) throws SQLException {				
				sugerencia.setCodSugerencia(String.valueOf(rs.getLong("sugn_id")));
				sugerencia.setDescripcion(rs.getString("sugc_descripcion"));
			}						
		}
		);
		
		return sugerencia;
	}
}
