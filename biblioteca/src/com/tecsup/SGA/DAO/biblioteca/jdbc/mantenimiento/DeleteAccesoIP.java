package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteAccesoIP extends StoredProcedure{
	private static Log log = LogFactory.getLog(DeleteAccesoIP.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_mantto_config.SP_DEL_ACCESO_IP";	
	
	private static final String E_N_COD_USUARIO = "E_N_COD_USUARIO";
	private static final String E_V_NUM_IP = "E_V_NUM_IP";
	
	public DeleteAccesoIP(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_N_COD_USUARIO, OracleTypes.NUMBER));
		declareParameter(new SqlParameter(E_V_NUM_IP, OracleTypes.VARCHAR));
		
		compile();
	}

	public Map execute(Integer codUsuario, String numIP) {
	
		Map inputs = new HashMap();
		
		log.info("**** INI " + SPROC_NAME + "*****");
		
		log.info("E_N_COD_USUARIO: "+ codUsuario);
		log.info("E_V_NUM_IP: "+ numIP);
		
		log.info("**** FIN " + SPROC_NAME + "*****");
		
		inputs.put(E_N_COD_USUARIO, codUsuario);
		inputs.put(E_V_NUM_IP, numIP);
					
		return super.execute(inputs);
	
	}
}
