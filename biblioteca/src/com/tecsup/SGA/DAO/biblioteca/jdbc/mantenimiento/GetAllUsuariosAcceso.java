package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.UsuarioPM;

public class GetAllUsuariosAcceso extends StoredProcedure{
    private static Log log = LogFactory.getLog(GetAllUsuariosAcceso.class);
    private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
    + ".pkg_bib_mantto_config.sp_sel_usuarios_apoyo";

    private static final String S_C_RECORDSET = "S_C_RECORDSET";

 public GetAllUsuariosAcceso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);

        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute() {
        log.info("**** INI " + SPROC_NAME + "*****");
        log.info("SIN PARAMETROS ----");
        log.info("**** FIN " + SPROC_NAME + "*****");
        Map inputs = new HashMap();
        return super.execute(inputs);
    }

    final class ProcesoMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            UsuarioPM usuarios = new UsuarioPM ();
            usuarios.setCodUsuario(rs.getString("COD_USUARIO"));
            usuarios.setCodSujeto (rs.getInt   ("COD_SUJETO" ));
            usuarios.setNomUsuario(rs.getString("NOM_USUARIO"));
            return usuarios;
        }
    }
}
