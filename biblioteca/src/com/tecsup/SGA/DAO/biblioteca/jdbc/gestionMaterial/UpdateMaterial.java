package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateMaterial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
	".PKG_BIB_GESTION_MAT.SP_ACT_MATERIAL";
	
	private static final String E_C_COD_MATERIAL = "E_C_COD_MATERIAL";
	private static final String E_C_TIPO_MATERIAL = "E_C_TIPO_MATERIAL"; 
	private static final String E_V_TITULO = "E_V_TITULO";
	private static final String E_C_COD_AUTOR = "E_C_COD_AUTOR";
	private static final String E_C_COD_DEWEY = "E_C_COD_DEWEY";
	private static final String E_V_NRO_PAGINAS = "E_V_NRO_PAGINAS";
	private static final String E_C_COD_EDITORIAL = "E_C_COD_EDITORIAL";
	private static final String E_V_NRO_EDITORIAL = "E_V_NRO_EDITORIAL";
	private static final String E_C_COD_PAIS = "E_C_COD_PAIS";
	private static final String E_C_COD_CIUDAD = "E_C_COD_CIUDAD";
	private static final String E_V_FECHA_PUBLICACION = "E_V_FECHA_PUBLICACION";
	private static final String E_V_URL_ARCH_DIGITAL = "E_V_URL_ARCH_DIGITAL";
	private static final String E_C_COD_IDIOMA = "E_C_COD_IDIOMA";
	private static final String E_V_ISBN = "E_V_ISBN";
	private static final String E_V_FORMATO = "E_V_FORMATO";
	private static final String E_C_COD_TIPO_VIDEO = "E_C_COD_TIPO_VIDEO";
	private static final String E_V_IND_PREST_SALA = "E_V_IND_PREST_SALA";
	private static final String E_V_IND_PREST_CASA = "E_V_IND_PREST_CASA";
	private static final String E_V_IND_RESERVA = "E_V_IND_RESERVA";
	private static final String E_V_IMAGEN = "E_V_IMAGEN";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	private static final String E_V_UBICACION_FISICA = "E_V_UBICACION_FISICA";
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	
	private static final String E_V_COD_GENERADO = "E_V_COD_GENERADO";
	private static final String E_V_PREFIJO = "E_V_PREFIJO";
	private static final String E_V_SUFIJO = "E_V_SUFIJO";
	private static final String E_V_FRECUENCIA = "E_V_FRECUENCIA"; //SOLO PARA REVISTAS
	private static final String E_V_MENCION_SERIE = "E_V_MENCION_SERIE";
	private static final String E_V_CONTENIDO = "E_V_CONTENIDO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public UpdateMaterial(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_MATERIAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_TIPO_MATERIAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_TITULO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_AUTOR, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_DEWEY, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NRO_PAGINAS, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_EDITORIAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NRO_EDITORIAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_PAIS, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_CIUDAD, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FECHA_PUBLICACION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_URL_ARCH_DIGITAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_IDIOMA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ISBN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FORMATO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_TIPO_VIDEO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_IND_PREST_SALA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_IND_PREST_CASA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_IND_RESERVA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_IMAGEN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_UBICACION_FISICA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
	
	declareParameter(new SqlParameter(E_V_COD_GENERADO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_PREFIJO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_SUFIJO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FRECUENCIA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_MENCION_SERIE, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CONTENIDO, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(
			String codMaterial,
			String tipMaterial,
			String titulo,
			String codAutor,
			String codDewey,
			String nroPaginas,
			String codEditorial,
			String nroEditorial,
			String codPais,
			String codCiudad,
			String fecPublicacion,
			String urlArchDigital,
			String codIdioma,
			String isbn,
			String formato,
			String codTipoVideo,
			String indPrestamoSala,
			String indPrestamoCasa,
			String indReserva,
			String imagen,			
			String usuario,
			String ubicacionFisica,
			String codSede,
			String codGenerado,
			String prefijo,
			String sufijo,
			String frecuencia,
			String mencionSerie,
			String contenido
			) {
		
		log.info("**** INI " + SPROC_NAME + "*****");
		log.info("E_C_COD_MATERIAL:"+ codMaterial);
		log.info("E_C_TIPO_MATERIAL:"+ tipMaterial);
		log.info("E_V_TITULO:"+ titulo);
		log.info("E_C_COD_AUTOR:"+ codAutor);
		log.info("E_C_COD_DEWEY:"+ codDewey);
		log.info("E_V_NRO_PAGINAS:"+ nroPaginas);
		log.info("E_C_COD_EDITORIAL:"+ codEditorial);
		log.info("E_V_NRO_EDITORIAL:"+ nroEditorial);
		log.info("E_C_COD_PAIS:"+ codPais);
		log.info("E_C_COD_CIUDAD:"+ codCiudad);
		log.info("E_V_FECHA_PUBLICACION:"+ fecPublicacion);
		log.info("E_V_URL_ARCH_DIGITAL:"+ urlArchDigital);
		log.info("E_C_COD_IDIOMA:"+ codIdioma);
		log.info("E_V_ISBN:"+ isbn);
		log.info("E_V_FORMATO:"+ formato);
		log.info("E_C_COD_TIPO_VIDEO:"+ codTipoVideo);
		log.info("E_V_IND_PREST_SALA:"+ indPrestamoSala);
		log.info("E_V_IND_PREST_CASA:"+ indPrestamoCasa);	
		log.info("E_V_IND_RESERVA:"+ indReserva);
		log.info("E_V_IMAGEN:"+ imagen);
		log.info("E_V_USU_CREA:"+ usuario);
		log.info("E_V_UBICACION_FISICA:"+ ubicacionFisica);
		log.info("E_C_COD_SEDE:"+ codSede);	
		log.info("E_V_COD_GENERADO:"+ codGenerado);
		log.info("E_V_PREFIJO:"+ prefijo);
		log.info("E_V_SUFIJO:"+ sufijo);
		log.info("E_V_FRECUENCIA:"+ frecuencia);
		log.info("E_V_MENCION_SERIE:"+ mencionSerie);
		log.info("E_V_CONTENIDO:"+ contenido);
		log.info("**** FIN " + SPROC_NAME + "*****");
		
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_MATERIAL, codMaterial);
	inputs.put(E_C_TIPO_MATERIAL, tipMaterial);
	inputs.put(E_V_TITULO, titulo);
	inputs.put(E_C_COD_AUTOR, codAutor);
	inputs.put(E_C_COD_DEWEY, codDewey);
	inputs.put(E_V_NRO_PAGINAS, nroPaginas);
	inputs.put(E_C_COD_EDITORIAL, codEditorial);
	inputs.put(E_V_NRO_EDITORIAL, nroEditorial);
	inputs.put(E_C_COD_PAIS, codPais);
	inputs.put(E_C_COD_CIUDAD, codCiudad);
	inputs.put(E_V_FECHA_PUBLICACION, fecPublicacion);
	inputs.put(E_V_URL_ARCH_DIGITAL, urlArchDigital);
	inputs.put(E_C_COD_IDIOMA, codIdioma);
	inputs.put(E_V_ISBN, isbn);
	inputs.put(E_V_FORMATO, formato);
	inputs.put(E_C_COD_TIPO_VIDEO, codTipoVideo);
	inputs.put(E_V_IND_PREST_SALA, indPrestamoSala);
	inputs.put(E_V_IND_PREST_CASA, indPrestamoCasa);	
	inputs.put(E_V_IND_RESERVA, indReserva);
	inputs.put(E_V_IMAGEN, imagen);
	inputs.put(E_V_USU_CREA, usuario);
	inputs.put(E_V_UBICACION_FISICA, ubicacionFisica);
	inputs.put(E_C_COD_SEDE, codSede);	
	inputs.put(E_V_COD_GENERADO, codGenerado);
	inputs.put(E_V_PREFIJO, prefijo);
	inputs.put(E_V_SUFIJO, sufijo);
	inputs.put(E_V_FRECUENCIA, frecuencia);
	inputs.put(E_V_MENCION_SERIE, mencionSerie);
	inputs.put(E_V_CONTENIDO, contenido);

	return super.execute(inputs);
	}

	
}
