package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;



public class InsertReservaSalas extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertReservaSalas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
	+ ".pkg_bib_proceso_sanciones.sp_ins_reserva_sala";
 	//+ ".pkg_bib_web.sp_ins_reserva_salas";
	
	private static final String E_C_COD_SALA = "E_C_COD_SALA";
	private static final String E_V_FEC_RESERVA = "E_V_FEC_RESERVA";
	private static final String E_V_HORA_INI = "E_V_HORA_INI";
	private static final String E_V_HORA_FIN = "E_V_HORA_FIN";
	private static final String E_V_COD_USU_RESERVA = "E_V_COD_USU_RESERVA";
	private static final String E_C_USU_CREA = "E_V_COD_USUARIO";
		
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertReservaSalas(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
	
		declareParameter(new SqlParameter(E_C_COD_SALA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FEC_RESERVA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_HORA_INI, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_HORA_FIN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USU_RESERVA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_USU_CREA, OracleTypes.VARCHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}

	public Map execute(String codTipoSala, String fechaReserva, String horaInicio,
			String horaFin, String usuReserva, String usuCre) {			
		Map inputs = new HashMap();	
		
		log.info("*** INI " + SPROC_NAME + "****");
		log.info("E_C_COD_SALA:"+codTipoSala);
		log.info("E_V_FEC_RESERVA:"+fechaReserva);
		log.info("E_V_HORA_INI:"+horaInicio);
		log.info("E_V_HORA_FIN:"+horaFin);
		log.info("E_V_COD_USU_RESERVA:"+usuReserva);
		log.info("E_C_USU_CREA:"+usuCre);
		log.info("*** FIN " + SPROC_NAME + "****");
		
		inputs.put(E_C_COD_SALA, codTipoSala);//codigo detalle padre
		inputs.put(E_V_FEC_RESERVA, fechaReserva);//CODIGO TABLA A GRABAR 
		inputs.put(E_V_HORA_INI, horaInicio);
		inputs.put(E_V_HORA_FIN, horaFin);
		inputs.put(E_V_COD_USU_RESERVA, usuReserva);
		inputs.put(E_C_USU_CREA, usuCre);										
		return super.execute(inputs);			
	}
}
