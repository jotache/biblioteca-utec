package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetSeccionesByFiltros extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetSeccionesByFiltros.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
		+ ".pkg_bib_mantto_config.sp_sel_secciones_x_filtros";
	    
	 	private static final String E_COD_UNICO = "E_COD_UNICO";
	    private static final String E_C_SECCION = "E_C_SECCION";
	    private static final String E_C_GRUPO = "E_C_GRUPO";
	    private static final String E_C_TIPO = "E_C_TIPO";
	    	   
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetSeccionesByFiltros(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        
	        declareParameter(new SqlParameter(E_COD_UNICO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_SECCION, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_GRUPO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
	       	       
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	        compile();
	    }

	    public Map execute(String codigoUnico, String codSeccion, String codGrupo, String codTipo) {
	    	
	    	log.info("**** INI " + SPROC_NAME + "*****");
	    	log.info("E_COD_UNICO: "+ codigoUnico);
	    	log.info("E_C_SECCION: "+ codSeccion);
	    	log.info("E_C_GRUPO: "+ codGrupo);
	    	log.info("E_C_TIPO: "+ codTipo);
	    	log.info("**** FIN " + SPROC_NAME + "*****");
	    	
	    	Map inputs = new HashMap();	    	
	    	inputs.put(E_COD_UNICO, codigoUnico);
	        inputs.put(E_C_SECCION, codSeccion);
	        inputs.put(E_C_GRUPO, codGrupo);
	        inputs.put(E_C_TIPO, codTipo);
	        	        
	        return super.execute(inputs);

	    }
	    
	    final class ProcesoMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	Secciones secciones = new Secciones();
	        	
	        	secciones.setCodUnico(rs.getString("CODUNICO"));
	        	secciones.setCodGrupo(rs.getString("CODGRUPO"));
	        	secciones.setDescripcionGrupo(rs.getString("DESGRUPO"));
	        	secciones.setCodSeccion(rs.getString("CODSECCION"));
	        	secciones.setDescripcionSeccion(rs.getString("DESSECCION"));
	        	secciones.setTipoMaterial(rs.getString("CODMATERIAL"));
	        	secciones.setFechaPublicacion(rs.getString("FECHAPUBLI"));
	        	secciones.setFechaVigencia(rs.getString("FECHAVIGENCIA"));
	        	secciones.setTema(rs.getString("TEMA"));
	        	secciones.setDescripcionCorta(rs.getString("DESC_CORTA"));
	        	secciones.setDescripcionLarga(rs.getString("DESC_LARGA"));
	        	secciones.setUrl(rs.getString("URL")== null ? "" : rs.getString("URL"));
	        	secciones.setArchivo(rs.getString("IMAGEN"));
	        	secciones.setFlag(rs.getString("FLAG"));
	        	secciones.setDocumentoNov(rs.getString("NOM_ADJUNTO"));
	        		        		        	
	            return secciones;
	        }

	    }
}
