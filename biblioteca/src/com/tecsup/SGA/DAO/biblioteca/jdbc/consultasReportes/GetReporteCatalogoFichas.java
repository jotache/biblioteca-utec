package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;


public class GetReporteCatalogoFichas extends StoredProcedure{
	 private static Log log = LogFactory.getLog(GetReporteCatalogoFichas.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.sp_sel_report_catalog_fichas";

	 	private static final String E_C_SEDE = "E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetReporteCatalogoFichas(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteCatalogoMapper()));
	        compile();
	    }

	    public Map execute(String sede){
	    	
	    	log.info("*** INI "  + SPROC_NAME + " ****");
	    	log.info("E_C_SEDE:" + sede);
	    	log.info("*** FIN "  + SPROC_NAME + " ****");
	    	
	    	Map inputs = new HashMap();
	    	inputs.put(E_C_SEDE, sede);
	        return super.execute(inputs);   
	    }
	    
	    final class ReporteCatalogoMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {	        	
	        	Reporte reporte = new Reporte();	        	
	        	reporte.setCodDeweyGenerico(rs.getString("COD_DEWEY_GENERICO") == null ? "" : rs.getString("COD_DEWEY_GENERICO"));
	        	reporte.setNomDeweyGenerico(rs.getString("NOM_DEWEY_GENERICO") == null ? "" : rs.getString("NOM_DEWEY_GENERICO"));
	        	reporte.setCodDewey(rs.getString("COD_DEWEY") == null ? "" : rs.getString("COD_DEWEY"));
	        	reporte.setNomDewey(rs.getString("NOM_DEWEY") == null ? "" : rs.getString("NOM_DEWEY"));
	        	reporte.setCodigoMaterial(rs.getString("COD_MATERIAL") == null ? "" : rs.getString("COD_MATERIAL") );
	        	reporte.setCodigo(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO") );
	        	reporte.setTipoMaterial(rs.getString("TIPO_MATERIAL") == null ? "" : rs.getString("TIPO_MATERIAL") );
	        	reporte.setTituloMaterial(rs.getString("TITULO") == null ? "" : rs.getString("TITULO") );
	        	reporte.setNroIngresos(rs.getString("NRO_INGRESO") == null ? "" : rs.getString("NRO_INGRESO") );
	        	reporte.setAutor(rs.getString("AUTOR") == null ? "" : rs.getString("AUTOR") );
	        	reporte.setDescriptores(rs.getString("DESCRIPTORES") == null ? "" : rs.getString("DESCRIPTORES"));
	        	reporte.setAņoPublicacion(rs.getString("ANO") == null ? "" : rs.getString("ANO"));
	        	reporte.setNombreEditorial(rs.getString("EDITORIAL") == null ? "" : rs.getString("EDITORIAL"));
	        	reporte.setTipoVideo(rs.getString("TIPOVIDEO") == null ? "" : rs.getString("TIPOVIDEO"));
	            return reporte;
	        }

	    }
}

