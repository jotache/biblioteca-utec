package com.tecsup.SGA.DAO.biblioteca.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.biblioteca.PortalWebBibliotecaDAO;
import com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb.*;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.ActualizarAtencion;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.InsertAtencion;
import com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones.TerminarReservaRecurso;
import com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento.GetSedeByUsuario;
import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Sala;

public class PortalWebBibliotecaDAOJdbc extends JdbcDaoSupport implements PortalWebBibliotecaDAO{
	private static Log log = LogFactory.getLog(PortalWebBibliotecaDAOJdbc.class);
	public List getMaximaCapacidadSalas(String codigoTipoSala)
	{
		GetMaximaCapacidadSalas getMaximaCapacidadSalas = new GetMaximaCapacidadSalas(this.getDataSource());
		
		HashMap outputs = (HashMap)getMaximaCapacidadSalas.execute(codigoTipoSala);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	
	public List getReservaSalas(String codigoTipoSala,String nombreSala, String fechaInicio, String fechaFin, String horaInicio,
			String horaFin, String flag_sel, String codUsuario,String estadoPendiente,String estadoReservado,String estadoEjecutado,String sede)
	{
		GetReservaSalas getReservaSalas = new GetReservaSalas(this.getDataSource());
		
		HashMap outputs = (HashMap)getReservaSalas.execute(codigoTipoSala,nombreSala, fechaInicio, fechaFin, 
				horaInicio, horaFin, flag_sel, codUsuario, estadoPendiente, estadoReservado, estadoEjecutado,sede);		
		
		if (!outputs.isEmpty()){		
			return (List)outputs.get("S_C_RECORDSET");
		} 
		return null;
	}
	
	public String insertReservaSalas(String codTipoSala, String fechaReserva,
			String horaInicio, String horaFin, String usuReserva, String usuCre)
	{
		InsertReservaSalas insertReservaSalas = new InsertReservaSalas(this.getDataSource());
			
			HashMap inputs = (HashMap)insertReservaSalas.execute(codTipoSala, fechaReserva, horaInicio, 
					horaFin, usuReserva, usuCre);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public String DeleteReservaSalas(String cadena, String nroRegistros, String usuCrea)
	{
		   DeleteReservaSalas deleteReservaSalas = new DeleteReservaSalas(this.getDataSource());
			
			HashMap inputs = (HashMap)deleteReservaSalas.execute(cadena, nroRegistros, usuCrea);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	//
	public List GetSeccionesPortalWeb(String codTipoSeccion, String codTipoMaterial)
	{
		GetSeccionesPortalWeb getSeccionesPortalWeb = new GetSeccionesPortalWeb(this.getDataSource());
		
		HashMap outputs = (HashMap)getSeccionesPortalWeb.execute(codTipoSeccion, codTipoMaterial);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetBusquedaSimple(String codTipoMaterial, String txtTexto, String txtBuscarBy, 
			String txtOrdenarBy, String txtDisponibiblidadSala, String txtDisponibilidadCasa,String codSede)
	{
		GetBusquedaSimple getBusquedaSimple = new GetBusquedaSimple(this.getDataSource());
		
		HashMap outputs = (HashMap)getBusquedaSimple.execute(codTipoMaterial, txtTexto, txtBuscarBy, 
				txtOrdenarBy, txtDisponibiblidadSala, txtDisponibilidadCasa,codSede);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//InsertReservaByMaterial
	public String InsertReservaByMaterial(String codTipoMaterial, String fechaReserva, String usuCre)
	{
		InsertReservaByMaterial insertReservaByMaterial = new InsertReservaByMaterial(this.getDataSource());
			
			HashMap inputs = (HashMap)insertReservaByMaterial.execute(codTipoMaterial, fechaReserva, usuCre);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public List GetBusquedaAvanzada(String codTipoMaterial, String codTipoIdioma, String codAnioInicio, 
			String codAnioFin, String txtDisponibiblidadSala, String txtDisponibilidadCasa , int buscar1,
			String txtTexto1, int condicion1, int buscar2, String txtTexto2, int condicion2,
			int buscar3, String txtTexto3, int ordenarBy,String codSede)
	{
		GetBusquedaAvanzada getBusquedaAvanzada = new GetBusquedaAvanzada(this.getDataSource());
		
		HashMap outputs = (HashMap)getBusquedaAvanzada.execute(codTipoMaterial, codTipoIdioma, codAnioInicio,
				codAnioFin, txtDisponibiblidadSala, txtDisponibilidadCasa, buscar1, txtTexto1, condicion1,
				buscar2, txtTexto2, condicion2, buscar3, txtTexto3, ordenarBy,codSede);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String AplicarReservaSalas(String codUsuarioReserva, 
			String codSala, String codReserva, String codUsuario, String estado){
		
		AplicarReservaSalas aplicarReservaSalas = new AplicarReservaSalas(this.getDataSource());
		
		HashMap inputs = (HashMap)aplicarReservaSalas.execute(codUsuarioReserva, 
				codSala, codReserva, codUsuario, estado);
		
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String GetVerificarReservaUsuario(String codUsuario)
	{
		GetVerificarReservaUsuario getVerificarReservaUsuario = new GetVerificarReservaUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)getVerificarReservaUsuario.execute(codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
				
		return null;
	}


	public Sala getReservaSalaById(String codRecurso, String codSequencia) {
		GetReservaSalaById sala = new GetReservaSalaById(this.getDataSource());
		HashMap out = (HashMap) sala.execute(codRecurso, codSequencia);
		if(!out.isEmpty()){
			ArrayList lista = (ArrayList)out.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (Sala)lista.get(0);
			}
			else return null;
		}
		return null;
	}

	
	public String terminarReservaSalas(String codRecurso, String codSecuencia,String usuario) {
		TerminarReservaRecurso terminarReserva = new TerminarReservaRecurso(this.getDataSource());		
		HashMap inputs = (HashMap)terminarReserva.execute(codRecurso, codSecuencia, usuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}


	public String sedeUsuario(String username) {
		GetSedeUsuario sede = new GetSedeUsuario(this.getDataSource());
		HashMap outputs = (HashMap) sede.execute(username);
		if (!outputs.isEmpty()){
			int resultado = ((Integer)outputs.get("pRESULTADO")).intValue();
			if ( resultado != -1 )
			{
				return (String)outputs.get("pSEDE");				
			}
		}
		return null;
	}


	public List<Alumno> getUsuarios(String apellPaterno, String apellMaterno,String nombre1, String usuario) {
		GetBusquedaUsuariosBib usuarios = new GetBusquedaUsuariosBib(this.getDataSource());
		HashMap outputs = (HashMap) usuarios.execute(apellPaterno, apellMaterno, nombre1, usuario);
		if (!outputs.isEmpty()){
			return (List<Alumno>) outputs.get(CommonConstants.RET_CURSOR);			
		}
		return null;
	}


	public List<Sala> ListarHorasAtencionPorSede(String sede) {
		log.info("ListarHorasAtencionPorSede() " + "sede:"+sede+ " - start");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		log.info("getJdbcTemplate()");
		String sql = "Select " +
					" (case when to_number(nvl(h.ttde_valor1,0))<10 then" +
			        " substr('0'||h.ttde_valor1,1,2)" +
			        " else" +
			        " h.ttde_valor1"+
			        " end) As IdIni,"+
			        "(case when to_number(nvl(h.ttde_valor1,0))<10 then"+
			        " substr('0'||h.ttde_valor1,1,2) ||':00'"+
			        " else"+ 
			        " h.ttde_valor1 ||':00'"+
			        " end) As HoraIni,"+			        
			        " (case when to_number(nvl(h.ttde_valor2,0))<10 then" +
			        " substr('0'||h.ttde_valor2,1,2) " +
			        " else " +
			        " h.ttde_valor2" + 
			        " end) As IdFin," +			        
			        " (case when to_number(nvl(h.ttde_valor2,0))<10 then"+
			        " substr('0'||h.ttde_valor2,1,2) ||':00'"+
			        " else"+ 
			        " h.ttde_valor2 ||':00'"+
			        " end) As HoraFin"+
					" From general.gen_tipo_tabla_detalle h Where tipt_id='0087' And h.ttde_valor9=? " +
					" And h.ttde_est_reg='A' And nvl(h.ttde_valor10,' ')<>'*'" +
					" Order by 1";		
		log.info(sql);
		log.info("sede:"+sede);
		final Object[] args = new Object[] {sede};
		log.info("ListarHorasAtencionPorSede() " + "sede:"+sede+ " - end");		
		return jdbcTemplate.query(sql, args, new HorasRowMapper());		
	}
	
	final class HorasRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			Sala objSala = new Sala();
			objSala.setIdHoraIni(rs.getString("IdIni"));
			objSala.setHoraIni(rs.getString("HoraIni"));
			objSala.setIdHoraFin(rs.getString("IdFin"));
			objSala.setHoraFin(rs.getString("HoraFin"));
			return objSala;
		}			
	}

	public List<Sala> ListarRecursosAtencion(String fecReserva, String sede, String tipoSala) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		log.info("getJdbcTemplate()");
		String sql = "Select tip.ttde_descripcion as NombreRec,trim(tip.ttde_id) as CodRecurso," +
					 " sr.srec_fec_reserva," +
					 " trim(sr.srec_hora_ini) as HoraIni,trim(sr.srec_hora_fin) as HoraFin," +
					 " sr.sren_usu_reserva,sr.srec_estado as estado,(select bu.nombreusuario from bib_usuario bu where bu.codusuario=sr.sren_usu_reserva) nombreusuario " +
					 " From bib_sala_reserva sr " +
					 " right join general.gen_tipo_tabla_detalle tip" +
					 " on sr.srec_codigo = trim(tip.ttde_id) and to_char(sr.srec_fec_reserva,'dd/MM/yyyy')= ? and sr.srec_estado in ('0001','0002') " +
					 " inner join general.gen_tipo_tabla_detalle cab on trim(tip.ttde_valor1) = trim(cab.ttde_id) " +
					 " Where tip.tipt_id='0046' and tip.ttde_est_reg='A' and cab.tipt_id='0045' " +
					 " and tip.ttde_valor6=? ";  //and tip.ttde_valor1='0002' ahora mostrar PC's y salas 
					 
		 if(!tipoSala.equals("0")){
			 sql += " and trim(tip.ttde_valor1)='"+tipoSala.trim()+"' ";
		 }
		
		sql += " Order by tip.ttde_descripcion,sr.srec_hora_ini";
		
		log.info("sql: "+sql);
		log.info("fecReserva:"+fecReserva+" sede:"+sede);
		final Object[] args = new Object[] {fecReserva,sede};
		log.info("Fin");
		return jdbcTemplate.query(sql, args, new RecursosRowMapper());
	}
	final class RecursosRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			Sala objRecurso = new Sala();
			objRecurso.setCodSala(rs.getString("CodRecurso"));
			objRecurso.setIdHoraIni(rs.getString("HoraIni"));
			objRecurso.setIdHoraFin(rs.getString("HoraFin"));
			objRecurso.setNombreSala(rs.getString("NombreRec"));			
			objRecurso.setCodEstado(rs.getString("estado"));
			objRecurso.setNombreUsuario(rs.getString("nombreusuario"));
			return objRecurso;
		}		
	}

	public AlumnoXCodigoBean obtenerUsuarioAtencion(String usuario) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		log.info("getJdbcTemplate()");
		String sql = "select u.codusuario,u.nombreusuario from bib_v_usuario u" +
		 " where trim(upper(u.usuario)) = trim(upper(?)) and u.situacionregistro='A'" ;
		
		log.info("sql:"+sql);
		log.info("Usuario:"+usuario);
		final Object[] params = new Object[] {usuario};
		final AlumnoXCodigoBean objUsuario = new AlumnoXCodigoBean();
		
		jdbcTemplate.query(sql, params, new RowCallbackHandler() {
			public void processRow(ResultSet rs) throws SQLException {
				objUsuario.setCodUsuario(String.valueOf(rs.getLong("codusuario")));				
				objUsuario.setNombre(rs.getString("nombreusuario")==null?"":rs.getString("nombreusuario"));
			}
		});
		log.info("Fin");
		return objUsuario;
	}


	public String insertarAtencion(String codRecurso,String codUsuarioAtencion,String fecha, String horaIni,String horaFin,String codUsuarioActualiza,String sede) {
		
		InsertAtencion insert = new InsertAtencion(this.getDataSource());		
		HashMap inputs = (HashMap)insert.execute(codRecurso, codUsuarioAtencion, fecha, horaIni, horaFin, codUsuarioActualiza,sede);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}


	public List<Sala> listarAtenciones(String fecha, String sede) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		/*String sql = "select a.arec_codigo,a.arec_codrecurso,a.abib_usuario,";
		sql += " a.abib_fecatencion,a.abib_hora_ini,a.abib_hora_fin, substr(a.abib_hora_ini,1,2) rango_inicio,";
		sql += " case nvl(substr(a.abib_hora_fin,4,2),'-') when '-' then 'XX' "; 
		sql += " when '00' then substr(a.abib_hora_fin,1,2) ";
		sql += " else (case when to_number(substr(a.abib_hora_fin,1,2))+1 <10 then ";
		sql += " substr('0'||to_char(to_number(substr(a.abib_hora_fin,1,2))+1),1,2) ";
		sql += " else " ;
		sql += " to_char(to_number(substr(a.abib_hora_fin,1,2))+1) " ;
		sql += " end) " ;
		sql += " end as rango_fin " ;		
		sql += " from bib_atencion_recurso a , general.gen_tipo_tabla_detalle d ";
		sql += " where a.AREC_CODRECURSO=d.ttde_id and d.tipt_id='0046' and d.ttde_valor1='0002' and d.ttde_valor6=? ";
		sql += " and a.abib_estreg='A'";
		sql += " and to_char(a.abib_fecatencion,'DD/MM/YYYY')=? order by a.arec_codigo, a.abib_hora_ini" ;*/
		log.info("getJdbcTemplate()");
		String sql = "select a.abib_estado,a.arec_codigo,a.arec_codrecurso,a.abib_usuario,";
		sql += " a.abib_fecatencion,a.abib_hora_ini, (case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end) as abib_hora_fin, substr(a.abib_hora_ini,1,2) rango_inicio,";
		sql += " case nvl(substr((case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end),4,2),'-') when '-' then 'XX' "; 
		sql += " when '00' then substr((case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end),1,2) ";
		sql += " else (case when to_number(substr((case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end),1,2))+1 <10 then ";
		sql += " substr('0'||to_char(to_number(substr((case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end),1,2))+1),1,2) ";
		sql += " else " ;
		sql += " to_char(to_number(substr((case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end),1,2))+1) " ;
		sql += " end) " ;
		sql += " end as rango_fin " ;		
		sql += " from bib_atencion_recurso a , general.gen_tipo_tabla_detalle d ";
		sql += " where a.AREC_CODRECURSO=trim(d.ttde_id) and d.tipt_id='0046' and d.ttde_valor6=? "; //and d.ttde_valor1='0002'
		sql += " and a.abib_estado in ('X','T')";
		sql += " and a.abib_estreg='A'";
		sql += " and to_char(a.abib_fecatencion,'DD/MM/YYYY')=? order by a.arec_codigo, a.abib_hora_ini" ;
		log.info("sql:"+sql);
		log.info("fecha:"+fecha+" sede:"+sede);
		final Object[] args = new Object[] {sede,fecha};
		log.info("Fin");
		return jdbcTemplate.query(sql, args, new AtencionesRowMapper());
	}
	final class AtencionesRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			Sala objAtn = new Sala();			
			objAtn.setCodSala(rs.getString("arec_codrecurso"));//id tabla
			objAtn.setCodUsuario(rs.getString("abib_usuario"));//usuario que usa el recurso
			objAtn.setFechaReserva(rs.getString("abib_fecatencion")); //fecha de uso
			objAtn.setHoraIni(rs.getString("abib_hora_ini")); //hora de inicio
			objAtn.setHoraFin(rs.getString("abib_hora_fin")); //hora de fin
			objAtn.setIdHoraIni(rs.getString("rango_inicio"));//inicio de rango de horas
			objAtn.setIdHoraFin(rs.getString("rango_fin"));//fin de rango de horas
			objAtn.setCodEstado(rs.getString("abib_estado")); //T , X , C
			return objAtn;
		}		
	}

	public List<Sala> listarAtencionesPorPC(String fecha, String sede,
			String codPC) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		log.info("getJdbcTemplate():listarAtencionesPorPC");
		String sSql = "select a.arec_codrecurso,a.arec_codigo,d.ttde_descripcion nombrepc,(select bu.nombreusuario from bib_usuario bu where bu.codusuario=a.abib_usuario) usuario, " ;
		sSql += " a.abib_hora_ini, (case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end) horafin," ;
		sSql += " a.abib_fecatencion,a.abib_usuario,a.abib_estado,";
		sSql += " (case a.abib_estado when 'T' then 'Finalizado' when 'X' then 'En Uso' when 'C' then 'Cancelado' end) estado ";
		sSql += " from bib_atencion_recurso a ,general.gen_tipo_tabla_detalle d";
		sSql += " where a.arec_codrecurso = trim(d.ttde_id) and d.tipt_id='0046'"; //and d.ttde_valor1 = '0002' 
		sSql += " and d.ttde_valor6=? and to_char(a.abib_fecatencion,'DD/MM/YYYY')=?";
		sSql += " and a.arec_codrecurso=? and a.abib_estreg='A' order by a.arec_codigo desc";
		log.info("sql:"+sSql);
		log.info("fecha:"+fecha+" sede:"+sede+" codPc:"+codPC);
		final Object[] args = new Object[] {sede,fecha,codPC};		
		return jdbcTemplate.query(sSql, args, new AtencionesPorPCRowMapper());
	}
	
	public List<Sala> listarAtencionesPorPCHistorico(String sede, String codPC) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		log.info("getJdbcTemplate():listarAtencionesPorPCHistorico");
		
		String sSql = "select a.arec_codrecurso,a.arec_codigo,d.ttde_descripcion nombrepc,(select bu.nombreusuario from bib_usuario bu where bu.codusuario=a.abib_usuario) usuario, " ;
		sSql += " a.abib_hora_ini, (case a.abib_estado when 'T' then a.abib_hora_fin_real else a.abib_hora_fin end) horafin," ;
		sSql += " to_char(a.abib_fecatencion,'DD/MM/YYYY') as abib_fecatencion,a.abib_usuario,a.abib_estado,";
		sSql += " (case a.abib_estado when 'T' then 'Finalizado' when 'X' then 'En Uso' when 'C' then 'Cancelado' end) estado ";
		sSql += " from bib_atencion_recurso a ,general.gen_tipo_tabla_detalle d";
		sSql += " where a.arec_codrecurso = trim(d.ttde_id) and d.tipt_id='0046'"; //and d.ttde_valor1 = '0002' 
		sSql += " and d.ttde_valor6=? ";
		sSql += " and a.arec_codrecurso=? and a.abib_estreg='A' order by a.abib_fecatencion desc,a.arec_codigo desc";
		
		log.info("sql:"+sSql);

		final Object[] args = new Object[] {sede,codPC};		
		return jdbcTemplate.query(sSql, args, new AtencionesPorPCRowMapper());
	}
	
	final class AtencionesPorPCRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int n) throws SQLException {
			Sala objAtnPC = new Sala();
			objAtnPC.setCodReserva(rs.getString("arec_codigo")); //Id Recurso
			objAtnPC.setCodSala(rs.getString("arec_codrecurso"));//id tabla
			objAtnPC.setNombreSala(rs.getString("nombrepc"));
			objAtnPC.setCodUsuario(rs.getString("abib_usuario"));//usuario que usa el recurso
			objAtnPC.setFechaReserva(rs.getString("abib_fecatencion")); //fecha de uso
			objAtnPC.setHoraIni(rs.getString("abib_hora_ini")); //hora de inicio
			objAtnPC.setHoraFin(rs.getString("horafin")); //hora de fin
			objAtnPC.setNombreUsuario(rs.getString("usuario"));
			objAtnPC.setEstado(rs.getString("estado"));
			objAtnPC.setCodEstado(rs.getString("abib_estado"));
			return objAtnPC;
		}		
	}

	public String actualizarAtencion(String codAtencion, String tipoOperacion,
			String codUsuarioActualiza) {
		//InsertAtencion insert = new InsertAtencion(this.getDataSource());
		ActualizarAtencion insert = new ActualizarAtencion(this.getDataSource());		
		HashMap inputs = (HashMap)insert.execute(codAtencion, tipoOperacion, codUsuarioActualiza);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
}
