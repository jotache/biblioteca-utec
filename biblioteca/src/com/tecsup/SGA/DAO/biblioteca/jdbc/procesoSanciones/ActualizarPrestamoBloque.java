package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.tecsup.SGA.common.CommonConstants;
import oracle.jdbc.driver.OracleTypes;
import javax.sql.DataSource;

public class ActualizarPrestamoBloque extends StoredProcedure {

	private static Log log = LogFactory.getLog(ActualizarPrestamoBloque.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
			".PKG_BIB_PROCESO_SANCIONES.SP_INS_DEVOL_PREST_MAT_BLOQUE";
			
	private static final String E_V_CADENA = "E_V_CADENA";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";	
	private static final String E_V_COD_USUARIO_PRESTAMO = "E_V_COD_USUARIO_PRESTAMO";
	private static final String E_V_USUARIO = "E_V_USUARIO";	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActualizarPrestamoBloque(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_CADENA, OracleTypes.VARCHAR));	
		declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO_PRESTAMO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_USUARIO, OracleTypes.VARCHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	
	public Map execute(String cadenaDatos,String nroReg,String usuarioPrestamo,String usuarioSistema) {    	
		Map inputs = new HashMap();
		
		log.info("**** INI " + SPROC_NAME + "******");
		log.info("E_V_CADENA:"+cadenaDatos);
		log.info("E_V_NROREGISTROS:"+nroReg);
		log.info("E_V_COD_USUARIO_PRESTAMO:"+usuarioPrestamo);
		log.info("E_V_USUARIO:"+usuarioSistema);		
		log.info("**** FIN " + SPROC_NAME + "******");
		
		inputs.put(E_V_CADENA,cadenaDatos);
		inputs.put(E_V_NROREGISTROS,nroReg);	
		inputs.put(E_V_COD_USUARIO_PRESTAMO, usuarioPrestamo);	
		inputs.put(E_V_USUARIO, usuarioSistema);		
		
		return super.execute(inputs);
		}
	
}
