package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Ciudad;


public class GetCiudadByPais extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetCiudadByPais.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_comun.sp_sel_ciudad_x_pais";
    
 	private static final String E_V_CODPAIS = "E_V_CODPAIS";
 	private static final String E_V_CODIGO = "E_V_CODIGO";
    private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
    private static final String E_C_TIPO = "E_C_TIPO";

   
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetCiudadByPais(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_V_CODPAIS, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
        
       
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codigoPais,String codigoId, String descripcion, String tipoOrden) {
    	
    	log.info("**** INI " + SPROC_NAME + "*****");
    	log.info("E_V_CODPAIS:" + codigoPais);
    	log.info("E_V_CODIGO:" + codigoId);
    	log.info("E_V_DESCRIPCION:" + descripcion);
    	log.info("E_C_TIPO:" + tipoOrden);
    	log.info("**** FIN " + SPROC_NAME + "*****");
    	
    	Map inputs = new HashMap();
    	inputs.put(E_V_CODPAIS, codigoPais);
    	inputs.put(E_V_CODIGO, codigoId);
        inputs.put(E_V_DESCRIPCION, descripcion);
        inputs.put(E_C_TIPO, tipoOrden);
       
      
        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Ciudad ciudad= new Ciudad();
        	
        	ciudad.setCiudadId(rs.getString("CIUDAD_ID"));
        	ciudad.setPaisId(rs.getString("PAIS_ID"));//valor1
        	ciudad.setPaisDescripcion(rs.getString("PAIS_DESC"));//ttde_id
        	ciudad.setCiudadCodigo(rs.getString("CIUDAD_CODIGO"));//descripcion_hijo
        	ciudad.setCiudadDescripcion(rs.getString("CIUDAD_DESC"));
        	
            return ciudad;
        }

    }
}
