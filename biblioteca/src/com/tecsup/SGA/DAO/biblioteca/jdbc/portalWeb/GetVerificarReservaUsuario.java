package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;

public class GetVerificarReservaUsuario extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetVerificarReservaUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_web.sp_verifica_reserva_usuario";
    //E_V_USUARIO IN CHAR

 	private static final String E_V_USUARIO = "E_V_USUARIO"; 	    
    //private static final String RECORDSET = "S_C_RECORDSET";
 	private static final String S_V_RETVAL = "S_V_RETVAL";
 
 public GetVerificarReservaUsuario(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
      
        declareParameter(new SqlParameter(E_V_USUARIO, OracleTypes.CHAR));//1 si esta apto y 0 no lo esta
               
        //declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String codUsuario) {
    	 
    	Map inputs = new HashMap();
    	
    	log.info("**** INI "  + SPROC_NAME + "*****");
    	log.info("E_V_USUARIO: "+codUsuario);
    	log.info("**** FIN "  + SPROC_NAME + "*****");    	
    	
    	inputs.put(E_V_USUARIO, codUsuario);
    	        
        return super.execute(inputs);

    }
    
    /*final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Sala sala= new Sala();
        	
            sala.setEstado(rs.getString("IND_PUEDE_RESERVAR")== null ? "" : rs.getString("IND_PUEDE_RESERVAR") ); 
        	 
            return sala;
        }

    }*/

}
