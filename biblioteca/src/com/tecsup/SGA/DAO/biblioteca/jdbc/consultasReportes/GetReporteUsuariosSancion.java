package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;


public class GetReporteUsuariosSancion extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReporteUsuariosSancion.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 	+ ".pkg_bib_consultas_reportes.sp_sel_report_usuarios_sancion";

	 	private static final String E_V_PERIODO1 = "E_V_PERIODO1";
	 	private static final String E_V_PERIODO2 = "E_V_PERIODO2";
	 	private static final String E_V_TIPO = "E_V_TIPO";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	 	private static final String E_C_TIPOUSUARIO = "E_C_TIPOUSUARIO";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetReporteUsuariosSancion(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_V_PERIODO1, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_PERIODO2, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_TIPO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_TIPOUSUARIO, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteUsuSancionMapper()));
	        compile();
	    }

	    public Map execute(String periodo1,String periodo2, String tipo,String sede,String tipoUsuario){
	    		    	
	    	log.info("**** INI " + SPROC_NAME  + " *****");
	    	log.info("E_V_PERIODO1:"+periodo1);
	    	log.info("E_V_PERIODO2:"+periodo2);
	    	log.info("E_V_TIPO:"+tipo);
	    	log.info("E_C_SEDE:"+sede);
	    	log.info("E_C_TIPOUSUARIO:"+tipoUsuario);
	    	log.info("**** FIN " + SPROC_NAME  + " *****");
	    	
	    	Map inputs = new HashMap();
	    	inputs.put(E_V_PERIODO1, periodo1);
	    	inputs.put(E_V_PERIODO2, periodo2);
	    	inputs.put(E_V_TIPO, tipo);
	    	inputs.put(E_C_SEDE, sede);
	    	inputs.put(E_C_TIPOUSUARIO, tipoUsuario);
	    	
	        return super.execute(inputs);

	    }
	    
	    final class ReporteUsuSancionMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	Reporte reporte = new Reporte();
	        	
	        	reporte.setCodUsuario(rs.getString("COD_USUARIO") == null ? "" : rs.getString("COD_USUARIO") );
	        	reporte.setUsuario(rs.getString("USUARIO") == null ? "" : rs.getString("USUARIO") );
	        	reporte.setNomUsuario(rs.getString("NOM_USUARIO") == null ? "" : rs.getString("NOM_USUARIO") );
	        	reporte.setCodTipoUsuario(rs.getString("COD_TIPO_USUARIO") == null ? "" : rs.getString("COD_TIPO_USUARIO") );
	        	reporte.setTipoUsuario(rs.getString("NOM_TIPO_USUARIO") == null ? "" : rs.getString("NOM_TIPO_USUARIO") );
	        	reporte.setTipoResultSancion(rs.getString("TIPORESULTADO") == null ? "" : rs.getString("TIPORESULTADO") );
	        	
	        	//JHPR: 2008-06-09
	        	reporte.setFecPrestamo(rs.getString("FECPRESTAMO") == null ? "" : rs.getString("FECPRESTAMO"));
	        	reporte.setFechaPresDev(rs.getString("FECDEVOLUCIONPROG") == null ? "" : rs.getString("FECDEVOLUCIONPROG"));
	        	reporte.setFechaRetiro(rs.getString("FECDEVOLUCION") == null ? "" : rs.getString("FECDEVOLUCION"));
	        		        	
	        	//JHPR : Add columns 2008-07-25
	        	reporte.setTituloMaterial(rs.getString("TITULO")==null?"":rs.getString("TITULO"));
	        	reporte.setNroIngresos(rs.getString("NRO_INGRESO")==null?"":rs.getString("NRO_INGRESO"));
	        	reporte.setTipoMaterial(rs.getString("CLASIFICACION")==null?"":rs.getString("CLASIFICACION"));
	        	reporte.setDetalle(rs.getString("DIAS_DEMORA")==null?"":rs.getString("DIAS_DEMORA"));
	        	
	        	//EBC : se recupera el ciclo
	        	reporte.setCiclo(rs.getString("COD_CICLO")==null?"":rs.getString("COD_CICLO"));
	        	reporte.setEspecialidad(rs.getString("ESP")==null?"":rs.getString("ESP"));
	        	
	            return reporte;
	        }

	    }
}

