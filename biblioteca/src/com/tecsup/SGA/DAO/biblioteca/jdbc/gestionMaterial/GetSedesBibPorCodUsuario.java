package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.common.CommonConstants;

public class GetSedesBibPorCodUsuario extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetSedesBibPorCodUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
		+ ".PKG_BIB_COMUN.SP_SEL_SEDES";
	private static final String COUSUARIO = "E_V_COUSUARIO";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetSedesBibPorCodUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(COUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new SedesMapper()));
        compile();
	}
	
	 public Map execute(String codUsuario) {
		 	log.info("*** INI "+SPROC_NAME+" ***");
	    	log.info("COUSUARIO:"+codUsuario);
	    	log.info("*** FIN "+SPROC_NAME+" ***");
		 
	    	Map inputs = new HashMap();	    	
	        inputs.put(COUSUARIO, codUsuario);
	        return super.execute(inputs);
	    }
	
	final class SedesMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			SedeBean sedeBean = new SedeBean();        	
        	sedeBean.setCodSede(rs.getString("codSede") == null ? "" : rs.getString("codSede"));
        	sedeBean.setDscSede(rs.getString("dscSede") == null ? "" : rs.getString("dscSede"));
        	return sedeBean;
		}
	}
}
