package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sala;

public class GetReservaSalas extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReservaSalas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	+ ".pkg_bib_web.sp_sel_reserva_salas2";
    
 	private static final String E_C_TIPO_SALA = "E_C_TIPO_SALA";
 	private static final String E_C_NOMBRE_SALA = "E_C_NOMBRE_SALA";
 	private static final String E_C_FECHA_INI = "E_C_FECHA_INI";
 	private static final String E_C_FECHA_FIN = "E_C_FECHA_FIN";
 	private static final String E_C_HORA_INI = "E_C_HORA_INI";
 	private static final String E_C_HORA_FIN = "E_C_HORA_FIN"; 
 	private static final String E_C_FLAG_DISPO = "E_C_FLAG_DISPO"; 	 	 
 	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
 	private static final String E_C_ESTADO_PEND = "E_C_ESTADO_PEND";
 	private static final String E_C_ESTADO_RESE = "E_C_ESTADO_RESE";
 	private static final String E_C_ESTADO_EJEC = "E_C_ESTADO_EJEC";
 	private static final String E_C_SEDE = "E_C_SEDE";
    private static final String RECORDSET = "S_C_RECORDSET";
 
 public GetReservaSalas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
      
        declareParameter(new SqlParameter(E_C_TIPO_SALA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NOMBRE_SALA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FECHA_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FECHA_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_HORA_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_HORA_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FLAG_DISPO, OracleTypes.CHAR));//1 si esta seleccionado y 0 sino
        declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_ESTADO_PEND, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_ESTADO_RESE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_ESTADO_EJEC, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codigoTipoSala,String nombreSala, String fechaInicio, String fechaFin, String horaInicio,
    		String horaFin, String flag_sel, String codUsuario,String estadoPendiente,String estadoReservado,String estadoEjecutado, String sede) {
    	 
    	Map inputs = new HashMap();
    	
    	log.info("**** INI "  + SPROC_NAME + "*****");
    	log.info("E_C_TIPO_SALA: "+codigoTipoSala);
    	log.info("E_C_NOMBRE_SALA: "+nombreSala);
    	log.info("E_C_FECHA_INI: "+fechaInicio);
    	log.info("E_C_FECHA_FIN: "+fechaFin);
    	log.info("E_C_HORA_INI: "+horaInicio);
    	log.info("E_C_HORA_FIN: "+horaFin);
    	log.info("E_C_FLAG_DISPO: "+flag_sel);
    	log.info("E_C_COD_USUARIO: "+codUsuario);
    	log.info("E_C_ESTADO_PEND: "+estadoPendiente);
    	log.info("E_C_ESTADO_RESE: "+estadoReservado);
    	log.info("E_C_ESTADO_EJEC: "+estadoEjecutado);
    	log.info("E_C_SEDE: "+sede);
    	log.info("**** FIN "  + SPROC_NAME + "*****");
    	
    	inputs.put(E_C_TIPO_SALA, codigoTipoSala);
    	inputs.put(E_C_NOMBRE_SALA, nombreSala);
    	inputs.put(E_C_FECHA_INI, fechaInicio);
    	inputs.put(E_C_FECHA_FIN, fechaFin);
    	inputs.put(E_C_HORA_INI, horaInicio);
    	inputs.put(E_C_HORA_FIN, horaFin);
    	inputs.put(E_C_FLAG_DISPO, flag_sel);
    	inputs.put(E_C_COD_USUARIO, codUsuario);
    	inputs.put(E_C_ESTADO_PEND, estadoPendiente);
    	inputs.put(E_C_ESTADO_RESE, estadoReservado);
    	inputs.put(E_C_ESTADO_EJEC, estadoEjecutado);
    	inputs.put(E_C_SEDE, sede);
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Sala sala= new Sala();
        	       
        	sala.setCodReserva(rs.getString("CODRESERVA")== null ? "" : rs.getString("CODRESERVA") );
        	sala.setCodSala(rs.getString("COD_SALA")== null ? "" : rs.getString("COD_SALA") );
        	sala.setCodTipoSala(rs.getString("CODTIPOSALA")== null ? "" : rs.getString("CODTIPOSALA") );
        	sala.setCodTipSala(rs.getString("COD_TIPOSALA")== null ? "" : rs.getString("COD_TIPOSALA") );
        	sala.setNombreSala(rs.getString("NOMBRESALA")== null ? "" : rs.getString("NOMBRESALA") );
        	sala.setFechaReserva(rs.getString("FECHARESERVA")== null ? "" : rs.getString("FECHARESERVA") );
        	sala.setHoraIni(rs.getString("HORA_INI")== null ? "" : rs.getString("HORA_INI") );
        	sala.setHoraFin(rs.getString("HORA_FIN")== null ? "" : rs.getString("HORA_FIN") );
        	sala.setUsuReserva(rs.getString("USU_RESERVA")== null ? "" : rs.getString("USU_RESERVA") );
        	sala.setCodEstado(rs.getString("CODESTADO")== null ? "" : rs.getString("CODESTADO") );
        	sala.setEstado(rs.getString("ESTADO")== null ? "" : rs.getString("ESTADO") );
        	sala.setCodUsuario(rs.getString("CODUSUARIO")== null ? "" : rs.getString("CODUSUARIO") );
        	sala.setUsuario(rs.getString("USUARIO")== null ? "" : rs.getString("USUARIO") );
        	sala.setCapacidadMax(rs.getString("CAPACIDAD_SALA")== null ? "" : rs.getString("CAPACIDAD_SALA") );
        	sala.setReservas(rs.getString("RESERVAS")== null ? "" : rs.getString("RESERVAS") );
        	sala.setLoginUsuario(rs.getString("LOGIN_USUARIO")== null ? "" : rs.getString("LOGIN_USUARIO") );        	
        	
            return sala;
        }

    }
}
