package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertAtencion extends StoredProcedure {
	
	private static Log log = LogFactory.getLog(InsertAtencion.class);	
	
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
			".PKG_BIB_PROCESO_SANCIONES.SP_INS_ATENCION_RECURSO";
	private static final String E_C_COD_RECURSO = "E_C_COD_RECURSO";
	private static final String E_V_FEC_RESERVA = "E_V_FEC_RESERVA";	
	private static final String E_V_HORA_INI = "E_V_HORA_INI";	
	private static final String E_V_HORA_FIN = "E_V_HORA_FIN";
	private static final String E_V_CODUSUARIO_RSV = "E_V_CODUSUARIO_RSV";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String E_V_SEDE = "E_V_SEDE";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertAtencion(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_RECURSO, OracleTypes.VARCHAR));	
		declareParameter(new SqlParameter(E_V_FEC_RESERVA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_HORA_INI, OracleTypes.VARCHAR));		
		declareParameter(new SqlParameter(E_V_HORA_FIN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO_RSV, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_SEDE, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codRecurso, String codUsuarioAtencion, String fecha, String horaIni, 
			String horaFin,String codUsuarioActualiza, String sede) {
		Map inputs = new HashMap();
		
		log.info("*** INI " + SPROC_NAME + " ***");
		log.info("E_C_COD_RECURSO: " + codRecurso);
		log.info("E_V_FEC_RESERVA: " + fecha);
		log.info("E_V_HORA_INI: " + horaIni);
		log.info("E_V_HORA_FIN: " + horaFin);
		log.info("E_V_CODUSUARIO_RSV: " + codUsuarioAtencion);
		log.info("E_V_CODUSUARIO: " + codUsuarioActualiza);
		log.info("E_V_SEDE: " + sede);
		log.info("*** FIN " + SPROC_NAME + " ***");
		
		inputs.put(E_C_COD_RECURSO,codRecurso);
		inputs.put(E_V_FEC_RESERVA,fecha);	
		inputs.put(E_V_HORA_INI, horaIni);				
		inputs.put(E_V_HORA_FIN,horaFin);
		inputs.put(E_V_CODUSUARIO_RSV,codUsuarioAtencion);
		inputs.put(E_V_CODUSUARIO,codUsuarioActualiza);
		inputs.put(E_V_SEDE,sede);
		return super.execute(inputs);
	}	
}