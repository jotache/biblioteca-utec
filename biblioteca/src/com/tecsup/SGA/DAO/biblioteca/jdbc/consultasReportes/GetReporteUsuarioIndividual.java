package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Reporte;

public class GetReporteUsuarioIndividual extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetReporteUsuarioIndividual.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 						+ ".pkg_bib_consultas_reportes.SP_SEL_REPORT_USUARIO";

 	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
 	private static final String E_V_USUARIO = "E_V_USUARIO";
 	private static final String E_C_SEDE = "E_C_SEDE";
    private static final String RECORDSET = "S_C_RECORDSET";
	    
	public GetReporteUsuarioIndividual(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.NUMBER));
        declareParameter(new SqlParameter(E_V_USUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteUsuIndividualMapper()));
        compile();
	}
	
	public Map execute(String codUsuario, String username,String sede){
		
		log.info("**** INI " + SPROC_NAME + " ****");
    	log.info("E_V_CODUSUARIO:" + (codUsuario==null?null:Long.valueOf(codUsuario)));
    	log.info("E_V_USUARIO:" + username);
    	log.info("E_C_SEDE:" + sede);
    	log.info("**** FIN " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();	    	    	
    	inputs.put(E_V_CODUSUARIO, (codUsuario==null?null:Long.valueOf(codUsuario))); //Long.valueOf(codUsuario));    	
    	inputs.put(E_V_USUARIO, username);    	
    	inputs.put(E_C_SEDE, sede);
        return super.execute(inputs);
	}
	
	final class ReporteUsuIndividualMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Reporte reporte = new Reporte();
			reporte.setCodigo(rs.getString("MBIB_CODGENERADO") == null ? "" : rs.getString("MBIB_CODGENERADO"));
			reporte.setTituloMaterial(rs.getString("TITULO") == null ? "" : rs.getString("TITULO"));
			reporte.setTotalEjemplares(rs.getString("NRO_EJEMPLAR") == null ? "" : rs.getString("NRO_EJEMPLAR"));
			reporte.setAutor(rs.getString("Autor") == null ? "" : rs.getString("Autor"));
			reporte.setDetalle(rs.getString("Pie") == null ? "" : rs.getString("Pie"));			
			reporte.setFecPrestamo(rs.getString("FecPrestamo") == null ? "" : rs.getString("FecPrestamo"));
			reporte.setFechaPresDev(rs.getString("FecDevolProg") == null ? "" : rs.getString("FecDevolProg"));			
			reporte.setFechaDevReal(rs.getString("FecDevolReal") == null ? "" : rs.getString("FecDevolReal")); //Date
			reporte.setTipoResultSancion(rs.getString("sancion") == null ? "" : rs.getString("sancion"));
			reporte.setEstado(rs.getString("Estado") == null ? "" : rs.getString("Estado"));
			reporte.setFechaFinSancion(rs.getString("FinSancion") == null ? "" : rs.getString("FinSancion")); //JHPR 2008-08-27
			reporte.setFechaFinSuspension(rs.getString("FinSuspension") == null ? "" : rs.getString("FinSuspension")); //ESBC 2009-12-31
			
			reporte.setSancion(rs.getString("SANCION") == null ? "" : rs.getString("SANCION"));
			reporte.setIdSancion(rs.getString("ID_SANCION") == null ? "" : rs.getString("ID_SANCION"));
			
			return reporte;
		}
		
	}
}
