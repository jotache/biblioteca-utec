package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.common.CommonConstants;

public class GetLibrosPrestadosPorAlumno extends StoredProcedure {
	
	private static Log log = LogFactory.getLog(GetLibrosPrestadosPorAlumno.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
			+ ".PKG_BIB_GESTION_MAT.SP_SEL_LIBRO_PRESTA_X_USUARIO";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";	
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetLibrosPrestadosPorAlumno(DataSource dataSource){
    	super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LibrosPrestadosMapper()));
        compile();
    }
    
    
    public Map execute(String codUsuario){
		log.info("***" + SPROC_NAME + "***");    	  
    	log.info("E_V_CODUSUARIO:"+codUsuario);	
    	log.info("*** FIN " + SPROC_NAME + "***");
    	Map inputs = new HashMap();    	
    	inputs.put(E_V_CODUSUARIO, codUsuario);	 
        return super.execute(inputs);
	}
    
    
    final class LibrosPrestadosMapper implements RowMapper {        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	MaterialxIngresoBean bean = new MaterialxIngresoBean();
        	bean.setTitulo(rs.getString("TITULO"));
        	bean.setFecPrestamo(rs.getString("FECPRESTAMO"));
        	bean.setCodUnico(rs.getString("INGRESO"));
        	bean.setCodDewey(rs.getString("DEWEY")==null?"":rs.getString("DEWEY"));
        	bean.setFecDevolucionProg(rs.getString("FECDEVPROG")==null?"":rs.getString("FECDEVPROG"));
        	return bean;
        }
	}
    
}
