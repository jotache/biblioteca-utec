package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActualizaParametrosGenerales extends StoredProcedure {

	private static Log log = LogFactory.getLog(ActualizaParametrosGenerales.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
	 		+ ".pkg_BIB_MANTTO_CONFIG.SP_ACT_PARAMETROS_RESERVA";
	
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String E_C_CADENADATOS = "E_C_CADENADATOS";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActualizaParametrosGenerales(DataSource dataSource){
		super(dataSource,SPROC_NAME);		
		declareParameter(new SqlParameter(E_C_CADENADATOS, OracleTypes.VARCHAR));				
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));		
		compile();
	}
	
	public Map execute(String cadenaDatos,String usuario,String nroRegistros)  {
		
		Map inputs = new HashMap();
		log.info("****** INI " + SPROC_NAME + " *****");
		log.info("E_C_CADENADATOS:"+cadenaDatos);
		log.info("E_V_CODUSUARIO:"+usuario);
		log.info("E_V_NROREGISTROS:"+nroRegistros);
		log.info("****** FIN " + SPROC_NAME + " *****");
		
		inputs.put(E_C_CADENADATOS, cadenaDatos);
		inputs.put(E_V_CODUSUARIO, usuario);
		inputs.put(E_V_NROREGISTROS, nroRegistros);
		
		return super.execute(inputs);
	}
}
