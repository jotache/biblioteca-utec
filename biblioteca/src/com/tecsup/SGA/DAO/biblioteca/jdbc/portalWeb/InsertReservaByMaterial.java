package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertReservaByMaterial extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertReservaByMaterial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_web.sp_ins_reserva_x_material";
	
	private static final String E_C_COD_MATERIAL = "E_C_COD_MATERIAL";
	private static final String E_V_FECHA_RESERVA = "E_V_FECHA_RESERVA";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
			
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertReservaByMaterial(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
	
		declareParameter(new SqlParameter(E_C_COD_MATERIAL, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_FECHA_RESERVA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
		
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}

			public Map execute(String codTipoMaterial, String fechaReserva, String usuCre) {
			
				log.info("**** INI "  + SPROC_NAME + "*****");
				log.info("E_C_COD_MATERIAL:" + codTipoMaterial);
				log.info("E_V_FECHA_RESERVA:" + fechaReserva);
				log.info("E_V_USU_CREA:" + usuCre);
				log.info("**** FIN "  + SPROC_NAME + "*****");
				
				Map inputs = new HashMap();
				
				inputs.put(E_C_COD_MATERIAL, codTipoMaterial);//codigo detalle padre
				inputs.put(E_V_FECHA_RESERVA, fechaReserva);//CODIGO TABLA A GRABAR 
				inputs.put(E_V_USU_CREA, usuCre);
											
				return super.execute(inputs);
			
			}
}
