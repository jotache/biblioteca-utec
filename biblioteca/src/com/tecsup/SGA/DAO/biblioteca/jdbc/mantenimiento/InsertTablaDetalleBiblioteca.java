package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertTablaDetalleBiblioteca extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertTablaDetalleBiblioteca.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
 	+ ".pkg_bib_mantto_config.sp_ins_tabla_detalle_biblio";
	private static final String E_C_TTDE_ID = "E_C_TTDE_ID";
	private static final String E_C_TIPT_HIJO_ID = "E_C_TIPT_HIJO_ID";
	private static final String E_V_TTDE_DESCRIPCION = "E_V_TTDE_DESCRIPCION";
	private static final String E_V_TTDE_VALOR3 = "E_V_TTDE_VALOR3";
	private static final String E_V_TTDE_VALOR4 = "E_V_TTDE_VALOR4";
	private static final String E_V_TTDE_VALOR5 = "E_V_TTDE_VALOR5";
	private static final String E_V_TTDE_VALOR6 = "E_V_TTDE_VALOR6";
	private static final String E_V_TTDE_USU_CREA = "E_V_TTDE_USU_CREA";
	
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertTablaDetalleBiblioteca(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_TTDE_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_TIPT_HIJO_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_TTDE_DESCRIPCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_VALOR3, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_VALOR4, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_VALOR5, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_VALOR6, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_USU_CREA, OracleTypes.CHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}

			public Map execute(String codDetallePadre, String codTipoTablaDetalle, String descripcion,
					String usuCrea, String valor3, String valor4 , String valor5,String valor6) {
				
				log.info("**** INI " + SPROC_NAME + "*****");
				log.info("E_C_TTDE_ID>>"+codDetallePadre+"<<");
				log.info("E_C_TIPT_HIJO_ID>>"+codTipoTablaDetalle+"<<");
				log.info("E_V_TTDE_DESCRIPCION>>"+descripcion+"<<");
				log.info("E_V_TTDE_VALOR3>>"+valor3+"<<");
				log.info("E_V_TTDE_VALOR4>>"+valor4+"<<");
				log.info("E_V_TTDE_VALOR5>>"+valor5+"<<");
				log.info("E_V_TTDE_VALOR5>>"+valor6+"<<");
				log.info("E_V_TTDE_USU_CREA>>"+usuCrea+"<<");
				log.info("**** FIN " + SPROC_NAME + "*****");
				
				Map inputs = new HashMap();
				
				inputs.put(E_C_TTDE_ID, codDetallePadre);//codigo detalle padre
				inputs.put(E_C_TIPT_HIJO_ID, codTipoTablaDetalle);//CODIGO TABLA A GRABAR 
				inputs.put(E_V_TTDE_DESCRIPCION, descripcion);
				inputs.put(E_V_TTDE_VALOR3, valor3);
				inputs.put(E_V_TTDE_VALOR4, valor4);
				inputs.put(E_V_TTDE_VALOR5, valor5);
				inputs.put(E_V_TTDE_VALOR6, valor6);
				inputs.put(E_V_TTDE_USU_CREA, usuCrea);
							
				return super.execute(inputs);
			
			}

}
