package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Reporte;

public class GetReporteEtiquetas extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetReporteEtiquetas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.sp_sel_report_etiquetas";
	     

	 	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	 	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	 	private static final String E_V_NROING_INI = "E_V_NROING_INI";
	 	private static final String E_V_NROING_FIN = "E_V_NROING_FIN";
	 	private static final String E_C_TIPOREPORT = "E_C_TIPOREPORT";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	    
	    public GetReporteEtiquetas(DataSource dataSource){
	    	 super(dataSource, SPROC_NAME);        

		        declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
		        declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
		        declareParameter(new SqlParameter(E_V_NROING_INI, OracleTypes.VARCHAR));
		        declareParameter(new SqlParameter(E_V_NROING_FIN, OracleTypes.VARCHAR));
		        declareParameter(new SqlParameter(E_C_TIPOREPORT, OracleTypes.VARCHAR));		        		       
		        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
		        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR,new ReporteEtiquetasMapper()));
		        compile();
	    }
	    
	    public Map execute(String fecIni, String fecFin,String sede,
	    		String tipoRep, String nroIngresoIni, String nroIngresoFin){
	    	
	    	log.info("**** INI " + SPROC_NAME + " ****");
	    	log.info("E_V_FEC_INI:" + fecIni);
	    	log.info("E_V_FEC_FIN:" + fecFin);
	    	log.info("E_V_NROING_INI:" + nroIngresoIni);
	    	log.info("E_V_NROING_FIN:" + nroIngresoFin);
	    	log.info("E_C_TIPOREPORT:" + tipoRep);
	    	log.info("E_C_SEDE:" + sede);
	    	log.info("**** FIN " + SPROC_NAME + " ****");
	    	
	    	Map inputs = new HashMap();	    		    	
	    	inputs.put(E_V_FEC_INI, fecIni);
	    	inputs.put(E_V_FEC_FIN, fecFin);
	    	inputs.put(E_V_NROING_INI, nroIngresoIni);
	    	inputs.put(E_V_NROING_FIN, nroIngresoFin);
	    	inputs.put(E_C_TIPOREPORT, tipoRep);
	    	inputs.put(E_C_SEDE, sede);
	        return super.execute(inputs);

	    }
	    
	    final class ReporteEtiquetasMapper implements RowMapper {

			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Reporte reporte = new Reporte();	
				reporte.setCodigo(rs.getString(1) == null ? "" : rs.getString(1));
				reporte.setNomMaterial(rs.getString(2) == null ? "0" : rs.getString(2));
				reporte.setCiclo(rs.getString(3) == null ? "0" : rs.getString(3));	        	
	        	
	            return reporte;
			}
	    	
	    }
}
