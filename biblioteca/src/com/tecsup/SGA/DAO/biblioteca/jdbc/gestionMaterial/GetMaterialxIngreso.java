package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.common.CommonConstants;

public class GetMaterialxIngreso extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetMaterialxIngreso.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
		+ ".PKG_BIB_GESTION_MAT.sp_sel_material_bib_x_ingreso";

	 	private static final String E_V_CODIGO = "E_V_CODIGO";
	 	private static final String E_V_TIPO = "E_V_TIPO";
	 	private static final String E_C_SEDE = "E_C_SEDE";	 	
	 	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetMaterialxIngreso(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        
	        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_TIPO, OracleTypes.VARCHAR));	        
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));	  
	        declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new MaterialesxFiltroMapper()));
	        compile();
	    }

	    public Map execute(String codigo,String tipo,String sede,String codUsuario){
	    	
	    	log.info("***"+SPROC_NAME+"***");
	    	log.info("E_V_CODIGO:"+codigo);
	    	log.info("E_V_TIPO:"+tipo);
	    	log.info("E_C_SEDE:"+sede);
	    	log.info("E_V_CODUSUARIO:"+codUsuario);	
	    	log.info("*** SIN "+SPROC_NAME+"***");
	    	
	    	Map inputs = new HashMap();
	    	inputs.put(E_V_CODIGO, codigo);
	    	inputs.put(E_V_TIPO, tipo);
	    	inputs.put(E_C_SEDE, sede);
	    	inputs.put(E_V_CODUSUARIO, codUsuario);	 
	        return super.execute(inputs);

	    }
	    
	    final class MaterialesxFiltroMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        		        	
	        	MaterialxIngresoBean bean = new MaterialxIngresoBean();
	        	bean.setNroIngreso(rs.getString("NRO_INGRESO"));//*************
	        	bean.setNroIngresoReal(rs.getString("NRO_INGRESO_REAL"));
	        	bean.setCodUnico(rs.getString("CODUNICO"));//*************
	        	bean.setCodMaterialBib(rs.getString("COD_MATERIAL_BIB"));//*************
	        	bean.setCodTipoMaterial(rs.getString("COD_TIPO_MATERIAL"));
	        	bean.setDesTipoMaterial(rs.getString("DESC_TIPO_MATERIAL"));//*************
	        	bean.setTitulo(rs.getString("TITULO"));//*************	        	
	        	bean.setCodUsuario(rs.getString("CODUSUARIO"));
	        	bean.setNombreUsuario(rs.getString("NOMBREUSUARIO"));
	        	bean.setFecPrestamo(rs.getString("FECHA_PRESTAMO"));
	        	bean.setFecDevolucionProg(rs.getString("FECHA_DEVOLUCION_PROG"));
	        	bean.setFecDevolucion(rs.getString("FECHA_DEVOLUCION"));
	        	bean.setFlgSancion(rs.getString("FLAG_SANCION"));
	        	bean.setObservacion(rs.getString("OBSERVACION"));
	        	bean.setFlgPrestado(rs.getString("FLAG_PRESTADO"));//*************
	        	bean.setIndProrroga(rs.getString("IND_PRORROGA"));
	        	
	        	bean.setFlgReserva(rs.getString("FLAG_RESERVA")== null ? "0" : rs.getString("FLAG_RESERVA"));//*************
	        	bean.setFlgMatIndCasa(rs.getString("FLAG_SE_PUEDE_PRESTAR")== null ? "0" : rs.getString("FLAG_SE_PUEDE_PRESTAR"));//*************
	        	bean.setFlgMatIndSala(rs.getString("FLAG_SE_PUEDE_PRESTAR_SALA")== null ? "0" : rs.getString("FLAG_SE_PUEDE_PRESTAR_SALA"));//*************
	        	bean.setFlgLibroRetirado(rs.getString("FLAG_LIBRO_RETIRADO")== null ? "0" : rs.getString("FLAG_LIBRO_RETIRADO"));//*************
	        	bean.setSede(rs.getString("SEDE")== null ? "" : rs.getString("SEDE"));//*************
	        	
	        	bean.setUsuarioReserva(rs.getString("USUARIO_RESERVA")== null ? "" : rs.getString("USUARIO_RESERVA"));//*************
	        	bean.setCodReserva(rs.getString("COD_RESERVA")== null ? "" : rs.getString("COD_RESERVA"));//*************
	        	bean.setCodDewey(rs.getString("COD_DEWEY")== null ? "" : rs.getString("COD_DEWEY"));
	        	bean.setIndTipoPrestamo(rs.getString("IND_TIPO_PRESTAMO")== null ? "" : rs.getString("IND_TIPO_PRESTAMO"));
	        	bean.setNomLoginUsuPrestamo(rs.getString("USUARIO")== null ? "" : rs.getString("USUARIO"));
	        	//bean.setEstadoIngreso(rs.getString("ESTADO")== null ? "" : rs.getString("ESTADO"));
	        	bean.setCodPrestamoMultiple(rs.getString("COD_PRESTAMO_MULTIPLE")== null ? "" : rs.getString("COD_PRESTAMO_MULTIPLE"));
	        	
	            return bean;
	        }

	    }
}


