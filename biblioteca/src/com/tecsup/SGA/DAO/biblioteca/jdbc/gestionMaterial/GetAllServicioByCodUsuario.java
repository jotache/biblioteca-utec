package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ServiciosUsuarios;

public class GetAllServicioByCodUsuario extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllServicioByCodUsuario.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
		+ ".PKG_BIB_GESTION_MAT.SP_SEL_CONFIG_USUARIO_BIB";
	 
	 	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	    private static final String S_C_RECORDSET = "S_C_RECORDSET";
	 
	 public GetAllServicioByCodUsuario(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        
	        declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	        compile();
	    }

	    @SuppressWarnings("unchecked")
		public Map execute(String codUsuario) {
	    	
	    	log.info("**** INI " + SPROC_NAME + "*****");
	    	log.info("E_V_CODUSUARIO: "+codUsuario);
	    	log.info("**** FIN " + SPROC_NAME + "*****");
	    	
	    	Map inputs = new HashMap();	    	
	    	inputs.put(E_V_CODUSUARIO, codUsuario);
	        
	        return super.execute(inputs);

	    }
	    
	    final class ProcesoMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	ServiciosUsuarios serviciosUsuarios = new ServiciosUsuarios();
	        	
	        	serviciosUsuarios.setTipoUser(rs.getString("TIPOUSER"));//descripcion_hijo
	        	serviciosUsuarios.setPrestamoSala(rs.getString("PRESTAMO_SALA"));//valor1
	        	serviciosUsuarios.setPrestamoDomicilio(rs.getString("PRESTAMO_DOMICILIO"));//valor2
	        	serviciosUsuarios.setNroPrestamoSala(rs.getString("NRO_MAX_PREST"));//descripcion_padre
	        	serviciosUsuarios.setNroPrestamoInternet(rs.getString("NRO_DIAS_MAX_PREST"));
	        	serviciosUsuarios.setSancion(rs.getString("FLAG_SANSION"));
	        	serviciosUsuarios.setReserva(rs.getString("FLAG_RESERVA"));
	        	serviciosUsuarios.setNroProrrogas(rs.getString("NRO_MAX_PRORROGA"));	        	
	        	
	            return serviciosUsuarios;
	        }

	    }
	
}
