package com.tecsup.SGA.DAO.biblioteca.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.DAO.biblioteca.BiblioReportesDAO;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteBuzonSugerencia;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteCatalogoFichas;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteEtiquetas;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteMatFaltante;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteMatAdquiridos;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteMatRetirados;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteMatMasSolicitado;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteMovPrestDevDiaHra;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteResumenMovDia;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteResumenResDia;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteSalasCicloEsp;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteUsuarioIndividual;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteUsuariosSancion;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteEstadTipoUsuario;
import com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes.GetReporteEstadCicloEsp;

public class BiblioReportesDAOJdbc extends JdbcDaoSupport implements BiblioReportesDAO{

	public List getReporteBuzonSugerencia (String periodo, String codTipoMaterial) {   
		GetReporteBuzonSugerencia getReporteBuzonSugerencia = new GetReporteBuzonSugerencia(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteBuzonSugerencia.execute(periodo, codTipoMaterial);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteCatalogoFichas(String sede){
		GetReporteCatalogoFichas getReporteCatalogoFichas = new GetReporteCatalogoFichas(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteCatalogoFichas.execute(sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteMatFaltante(String fecIni, String fecFin,String sede){
		GetReporteMatFaltante getReporteMatFaltante = new GetReporteMatFaltante(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteMatFaltante.execute(fecIni, fecFin,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteMatAdquiridos(String fecIni, String fecFin, String tipoCambio,String sede,String procedencia){
		GetReporteMatAdquiridos getReporteMatAdquiridos = new GetReporteMatAdquiridos(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteMatAdquiridos.execute(fecIni, fecFin, tipoCambio,sede,procedencia);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteMatRetirados(String fecIni, String fecFin,String sede){
		GetReporteMatRetirados getReporteMatRetirados = new GetReporteMatRetirados(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteMatRetirados.execute(fecIni, fecFin,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteMatMasSolicitado(String fecIni, String fecFin, String nroMax,String sede){
		GetReporteMatMasSolicitado getReporteMatMasSolicitado = new GetReporteMatMasSolicitado(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteMatMasSolicitado.execute(fecIni, fecFin, nroMax,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteMovPrestDevDiaHra(String fecIni, String fecFin,String sede){
		GetReporteMovPrestDevDiaHra getReporteMovPrestDevDiaHra = new GetReporteMovPrestDevDiaHra(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteMovPrestDevDiaHra.execute(fecIni, fecFin,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteResumenMovDia(String fecIni, String fecFin,String sede){
		GetReporteResumenMovDia getReporteResumenMovDia = new GetReporteResumenMovDia(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteResumenMovDia.execute(fecIni, fecFin,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteUsuariosSancion(String periodo1,String periodo2, String tipo,String sede,String tipoUsuario){
		GetReporteUsuariosSancion getReporteUsuariosSancion = new GetReporteUsuariosSancion(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteUsuariosSancion.execute(periodo1,periodo2, tipo,sede,tipoUsuario);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteEstadTipoUsuario(String fecIni, String fecFin,String sede){
		GetReporteEstadTipoUsuario getReporteEstadTipoUsuario = new GetReporteEstadTipoUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteEstadTipoUsuario.execute(fecIni, fecFin,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getReporteEstadCicloEsp(String fecIni, String fecFin,String sede){
		GetReporteEstadCicloEsp getReporteEstadCicloEsp = new GetReporteEstadCicloEsp(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteEstadCicloEsp.execute(fecIni, fecFin, sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List getReporteUsuarioIndividual(String codUsuario, String username,String sede) {
		GetReporteUsuarioIndividual getReporteUsuarioInd = new GetReporteUsuarioIndividual(this.getDataSource());
		HashMap outputs = (HashMap) getReporteUsuarioInd.execute(codUsuario, username,sede);
		if (!outputs.isEmpty()){
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List getReporteReservaCicloEsp(String fecIni, String fecFin,String sede) {
		GetReporteSalasCicloEsp getReporteSalasCicloEsp = new GetReporteSalasCicloEsp(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteSalasCicloEsp.execute(fecIni, fecFin,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List getReporteResumenResDia(String fecIni, String fecFin,String sede) {
		GetReporteResumenResDia getReporteResumenResDia = new GetReporteResumenResDia(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteResumenResDia.execute(fecIni, fecFin,sede);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;

	}
	
	public List getReporteEtiquetas(String fecIni, String fecFin,String sede,String tipoRep, String nroIngresoIni, String nroIngresoFin) {
		GetReporteEtiquetas getReporteEtiquetas = new GetReporteEtiquetas(this.getDataSource());
		
		HashMap outputs = (HashMap)getReporteEtiquetas.execute(fecIni, fecFin,sede,tipoRep,nroIngresoIni, nroIngresoFin);
		
		if (!outputs.isEmpty()) {
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;

	}
	
}
