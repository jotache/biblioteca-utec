package com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.DescriptorxMaterialBean;
import com.tecsup.SGA.bean.IngresosxMaterialBean;
import com.tecsup.SGA.bean.MaterialesxFiltro;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAlumnoXCodigo extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAlumnoXCodigo.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".PKG_BIB_GESTION_MAT.SP_SEL_ALUMNO_X_CODIGO";

	 	private static final String E_V_CODIGO = "E_V_CODIGO";
	 	private static final String E_C_IND_TIPO_PRESTAMO = "E_C_IND_TIPO_PRESTAMO";
	 	private static final String E_C_PARAMETRO = "E_C_PARAMETRO";
	 	private static final String E_C_SEDE = "E_C_SEDE";
	 	private static final String E_V_NROINGRESO = "E_V_NROINGRESO";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetAlumnoXCodigo(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_IND_TIPO_PRESTAMO, OracleTypes.CHAR));	        
	        declareParameter(new SqlParameter(E_C_PARAMETRO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_V_NROINGRESO, OracleTypes.VARCHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AlumnoXCodigoMapper()));
	        compile();
	    }

	    public Map execute(String codigo,String indTipoPrestamo,String tipoBusquedaCodUsu,String sede,String numIngreso){
	    	
	    	log.info("*** INI " + SPROC_NAME + "***");
	    	log.info("E_V_CODIGO: "+codigo);
	    	log.info("E_C_IND_TIPO_PRESTAMO: "+indTipoPrestamo);
	    	log.info("E_C_PARAMETRO: "+tipoBusquedaCodUsu);	    	
	    	log.info("E_C_SEDE: "+sede);
	    	log.info("E_V_NROINGRESO: "+numIngreso);
	    	log.info("*** FIN " + SPROC_NAME + "***");
	    	
	    	Map inputs = new HashMap();	    	
	    	inputs.put(E_V_CODIGO,codigo);
	    	inputs.put(E_C_IND_TIPO_PRESTAMO,indTipoPrestamo);
	    	inputs.put(E_C_PARAMETRO, tipoBusquedaCodUsu);
	    	inputs.put(E_C_SEDE, sede);
	    	inputs.put(E_V_NROINGRESO, numIngreso);
	        return super.execute(inputs);
	    }
	    
	    final class AlumnoXCodigoMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	AlumnoXCodigoBean bean = new AlumnoXCodigoBean();
	        	bean.setUsuario(rs.getString("USUARIO"));
	        	bean.setCodUsuario(rs.getString("CODUSUARIO"));
	        	bean.setTipoUsuario(rs.getString("TIPO_USUARIO"));
	        	bean.setDescTipoUsuario(rs.getString("DESC_TIPO_USUARIO"));
	        	bean.setNombre(rs.getString("NOMBRE"));
	        	bean.setFlgPermitido(rs.getString("FLAG_PERMITIDO"));
	        	bean.setFlgSancion(rs.getString("FLAG_SANCION"));
	        	bean.setFechaFinSancion(rs.getString("FECHA_FINSANCION")==null?"":rs.getString("FECHA_FINSANCION"));
	        	bean.setFecActual(rs.getString("FECHA_ACTUAL"));
	        	bean.setNroDiasPrestCasa(rs.getString("NRO_DIAS_PREST_CASA"));
	        	bean.setFecProgDev(rs.getString("FECHA_PROG_DEV")== null ? "" : rs.getString("FECHA_PROG_DEV"));	        	
	        	bean.setFlgInhabilitado(rs.getString("FLAG_INHABILITACION"));
	        	bean.setFechaFinInhabilitado(rs.getString("FECHA_FININHABI")==null?"":rs.getString("FECHA_FININHABI"));	        	
	        	bean.setFlgPermitidoSala(rs.getString("FLAG_PERMITIDO_SALA")== null ? "0" : rs.getString("FLAG_PERMITIDO_SALA"));
	        	bean.setSede(rs.getString("SEDE")== null ? "" : rs.getString("SEDE"));
	        	bean.setEstado(rs.getString("ESTADO")== null ? "" : rs.getString("ESTADO"));
	        	bean.setNroPrestamos(rs.getString("NROPREST")== null ? "" : rs.getString("NROPREST"));
	        	bean.setLimitePrestamos(rs.getString("LIMITEPRESTAMOS")== null ? "" : rs.getString("LIMITEPRESTAMOS"));
	            return bean;
	        }

	    }
}

