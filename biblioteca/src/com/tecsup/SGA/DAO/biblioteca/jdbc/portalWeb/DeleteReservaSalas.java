package com.tecsup.SGA.DAO.biblioteca.jdbc.portalWeb;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteReservaSalas extends StoredProcedure{
	private static Log log = LogFactory.getLog(DeleteReservaSalas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
 	+ ".pkg_bib_web.sp_del_reserva_salas";
	
	private static final String E_V_CADENA = "E_V_CADENA";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
		
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteReservaSalas(DataSource dataSource) {
	super(dataSource, SPROC_NAME);

	declareParameter(new SqlParameter(E_V_CADENA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String cadena, String nroRegistros, String usuCrea) {
	
	Map inputs = new HashMap();
	
	log.info("**** INI " + SPROC_NAME + "*****");
	log.info("E_V_CADENA: "+cadena);
	log.info("E_V_NROREGISTROS: "+nroRegistros);
	log.info("E_V_USU_CREA: "+usuCrea);	
	log.info("**** FIN " + SPROC_NAME + "*****");
	
	inputs.put(E_V_CADENA, cadena);
	inputs.put(E_V_NROREGISTROS, nroRegistros);
	inputs.put(E_V_USU_CREA, usuCrea);
		
	return super.execute(inputs);
	
	}
}
