package com.tecsup.SGA.DAO.biblioteca.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateAtenderBuzonSugerencia extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateAtenderBuzonSugerencia.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA
 	+ ".pkg_bib_sugerencias.sp_act_atender_sugerencia";
	
	private static final String E_C_CODIGO_SUG = "E_C_CODIGO_SUG";
	private static final String E_C_CALIFICACION = "E_C_CALIFICACION";
	private static final String E_C_ORIENTADOA = "E_C_ORIENTADOA";
	private static final String E_C_RPTA = "E_C_RPTA";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateAtenderBuzonSugerencia(DataSource dataSource) {
	super(dataSource, SPROC_NAME);

	declareParameter(new SqlParameter(E_C_CODIGO_SUG, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CALIFICACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_ORIENTADOA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_RPTA, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codSugerencia, String codCalificacion, String codTipoMaterial, 
			String respuesta) {
	
		log.info("**** INI " + SPROC_NAME + "*****");
		log.info("E_C_CODIGO_SUG :"+codSugerencia);
		log.info("E_C_CALIFICACION:"+ codCalificacion);
		log.info("E_C_ORIENTADOA:"+ codTipoMaterial);
		log.info("E_C_RPTA:"+ respuesta);
		log.info("**** FIN " + SPROC_NAME + "*****");
		
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODIGO_SUG, codSugerencia);
	inputs.put(E_C_CALIFICACION, codCalificacion);
	inputs.put(E_C_ORIENTADOA, codTipoMaterial);
	inputs.put(E_C_RPTA, respuesta);
		
	return super.execute(inputs);
	
	}
}
