package com.tecsup.SGA.DAO.biblioteca.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.Reporte;
import com.tecsup.SGA.common.CommonConstants;

public class GetReporteEstadCicloEsp extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReporteEstadCicloEsp.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA 
	 + ".pkg_bib_consultas_reportes.SP_SEL_REPORT_CICLO_ESP";

	 	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	 	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	 	private static final String E_C_SEDE="E_C_SEDE";
	    private static final String RECORDSET = "S_C_RECORDSET";
	 
	 public GetReporteEstadCicloEsp(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);        

	        declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ReporteEstadCicloEsp()));
	        compile();
	    }

	    public Map execute(String fecIni, String fecFin,String sede){
	    	
	    	Map inputs = new HashMap();
	    	
	    	log.info("***** INI " + SPROC_NAME + "*****");
	    	log.info("fecIni:"+fecIni);
	    	log.info("fecFin:"+fecFin);
	    	log.info("sede:"+sede);
	    	log.info("***** FIN " + SPROC_NAME + "*****");
	    	
	    	inputs.put(E_V_FEC_INI, fecIni);
	    	inputs.put(E_V_FEC_FIN, fecFin);
	    	inputs.put(E_C_SEDE, sede);
	        return super.execute(inputs);

	    }
	    
	    final class ReporteEstadCicloEsp implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	
	        	Reporte reporte = new Reporte();
	        
	        	reporte.setCodDeweyGenerico(rs.getString("COD_DEWEY_GENERICO") == null ? "" : rs.getString("COD_DEWEY_GENERICO"));
	        	reporte.setNomDeweyGenerico(rs.getString("NOM_DEWEY_GENERICO") == null ? "" : rs.getString("NOM_DEWEY_GENERICO"));
	        	reporte.setCodDewey(rs.getString("COD_DEWEY") == null ? "" : rs.getString("COD_DEWEY"));
	        	reporte.setNomDewey(rs.getString("NOM_DEWEY") == null ? "" : rs.getString("NOM_DEWEY"));
	        	reporte.setCodigo(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO"));
	        	reporte.setNomMaterial(rs.getString("NOM_MATERIAL") == null ? "" : rs.getString("NOM_MATERIAL"));
	        	reporte.setCodTipoMaterial(rs.getString("COD_TIPO_MATERIAL") == null ? "" : rs.getString("COD_TIPO_MATERIAL"));
	        	reporte.setNomTipoMaterial(rs.getString("DES_TIPO_MATERIAL") == null ? "" : rs.getString("DES_TIPO_MATERIAL"));
	        	reporte.setIdioma(rs.getString("NOM_IDIOMA") == null ? "" : rs.getString("NOM_IDIOMA"));
	        	reporte.setNroUsuarios(rs.getString("NRO_USUARIOS") == null ? "" : rs.getString("NRO_USUARIOS"));
	        	reporte.setEspecialidad(rs.getString("NOM_ESPECIALIDAD") == null ? "" : rs.getString("NOM_ESPECIALIDAD"));
	        	reporte.setCiclo(rs.getString("NOM_CICLO") == null ? "" : rs.getString("NOM_CICLO"));
	        	reporte.setNroPrestSala(rs.getString("PREST_SALA") == null ? "" : rs.getString("PREST_SALA"));
	        	reporte.setNroPrestCasa(rs.getString("PREST_DOMICILIO") == null ? "" : rs.getString("PREST_DOMICILIO"));
	        	reporte.setTotalPrestamos(rs.getString("TOTAL_PRESTAMOS") == null ? "" : rs.getString("TOTAL_PRESTAMOS"));
	        		        	
	            return reporte;
	        }

	    }
}

