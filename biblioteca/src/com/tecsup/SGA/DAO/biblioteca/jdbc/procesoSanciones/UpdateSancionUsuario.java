package com.tecsup.SGA.DAO.biblioteca.jdbc.procesoSanciones;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateSancionUsuario extends StoredProcedure {
	private static Log log = LogFactory.getLog(UpdateSancionUsuario.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_BIBLIOTECA +
													".PKG_BIB_PROCESO_SANCIONES.SP_ACT_SANCION";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";	
	private static final String E_V_IDSANCION = "E_V_IDSANCION";
	private static final String E_V_CODUSUARIO_ACCION = "E_V_CODUSUARIO_ACCION";	
	private static final String S_V_RETVAL = "S_V_RETVAL";

	public UpdateSancionUsuario(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_IDSANCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO_ACCION, OracleTypes.VARCHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codUsuario,String idSancion,String codUsuarioAccion) {    	
		Map inputs = new HashMap();
		
		log.info("**** INI "  + SPROC_NAME + "******");
		log.info("E_V_CODUSUARIO:"+codUsuario);
		log.info("E_V_IDSANCION:"+idSancion);
		log.info("E_V_CODUSUARIO_ACCION:"+codUsuarioAccion);
		log.info("**** FIN "  + SPROC_NAME + "******");
		
		inputs.put(E_V_CODUSUARIO,codUsuario);
		inputs.put(E_V_IDSANCION, idSancion);
		inputs.put(E_V_CODUSUARIO_ACCION, codUsuarioAccion);						
		return super.execute(inputs);
	}
}
