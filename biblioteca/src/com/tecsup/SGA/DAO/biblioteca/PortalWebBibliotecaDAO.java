package com.tecsup.SGA.DAO.biblioteca;

import java.util.List;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Sala;

public interface PortalWebBibliotecaDAO {
	
	public List getMaximaCapacidadSalas(String codigoTipoSala);
	
	public List getReservaSalas(String codigoTipoSala,String nombreSala, String fechaInicio, String fechaFin, String horaInicio,
			String horaFin, String flag_sel, String codUsuario,String estadoPendiente,String estadoReservado,String estadoEjecutado,String sede);	
	public String insertReservaSalas(String codTipoSala, String fechaReserva,
			String horaInicio, String horaFin, String usuReserva, String usuCre);
	public String DeleteReservaSalas(String cadena, String nroRegistros, String usuCrea);
	public List GetSeccionesPortalWeb(String codTipoSeccion, String codTipoMaterial);
	public List GetBusquedaSimple(String codTipoMaterial, String txtTexto, String txtBuscarBy, 
			String txtOrdenarBy, String txtDisponibiblidadSala, String txtDisponibilidadCasa,String codSede);
	public String InsertReservaByMaterial(String codTipoMaterial, String fechaReserva, String usuCre);
	
	public List GetBusquedaAvanzada(String codTipoMaterial, String codTipoIdioma, String codAnioInicio, 
			String codAnioFin, String txtDisponibiblidadSala, String txtDisponibilidadCasa , int buscar1,
			String txtTexto1, int condicion1, int buscar2, String txtTexto2, int condicion2,
			int buscar3, String txtTexto3, int ordenarBy,String codSede);
	
	public String AplicarReservaSalas(String codUsuarioReserva,  
			String codSala, String codReserva, String codUsuario, String estado);
	
	public String GetVerificarReservaUsuario(String codUsuario);
	
	
	public Sala getReservaSalaById(String codRecurso,String codSequencia);
	public String terminarReservaSalas(String codRecurso, String codSecuencia,String usuario);
	public String sedeUsuario(String username);
	public List<Alumno> getUsuarios(String apellPaterno, String apellMaterno, String nombre1,String usuario);
	
	public List<Sala> ListarHorasAtencionPorSede(String sede);
	public List<Sala> ListarRecursosAtencion(String fecReserva,String sede, String tipoSala);
	public AlumnoXCodigoBean obtenerUsuarioAtencion(String sUsuario);
	public String insertarAtencion(String codRecurso,String codUsuarioAtencion,String fecha, String horaIni,String horaFin,String codUsuarioActualiza,String sede);
	public List<Sala> listarAtenciones(String fecha,String sede);
	public List<Sala> listarAtencionesPorPC(String fecha,String sede,String codPC);
	public List<Sala> listarAtencionesPorPCHistorico(String sede, String codPC);
	public String actualizarAtencion(String codAtencion, String tipoOperacion, String codUsuarioActualiza);
}
