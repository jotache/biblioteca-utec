package com.tecsup.SGA.DAO.biblioteca;

import java.util.List;

import com.tecsup.SGA.bean.HistorialProrrogaBean;
import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.modelo.ServiciosUsuarios;

public interface BiblioGestionMaterialDAO {

	/**
	 * @param txtCodigo
	 * @param txtNroIngreso
	 * @param cboTipoMaterial
	 * @param cboBuscarPor
	 * @param txtTitulo
	 * @param cboIdioma
	 * @param cboAnioIni
	 * @param cboAnioFin
	 * @param txtFechaReservaIni
	 * @param txtFechaReservaFin
	 * @return
	 */
	List getAllMaterialesxFiltro(String txhCodigoUnico,String txtCodigo, String txtNroIngreso,
			String cboTipoMaterial, String cboBuscarPor, String txtTitulo,
			String cboIdioma, String cboAnioIni, String cboAnioFin,
			String txtFechaReservaIni, String txtFechaReservaFin,String sede);

	/**
	 * @param txhCodUnico
	 * @return
	 */
	List getAllMaterialDetalle(String txhCodUnico);

	/**
	 * @param txhCodUnico
	 * @return
	 */
	List getAllIngresosxMaterial(String txhCodMaterial,String txhCodUnico);

	/**
	 * @param txhCodUnico
	 * @return
	 */
	List getAllDescriptorxMaterial(String txhCodUnico);

	/**
	 * @param txhCodUnico
	 * @return
	 */
	List getAllArchAdjuntoxMaterial(String txhCodUnico);

	/**
	 * @param txhCodigoUnico
	 * @param txtTitulo
	 * @param nomArchivo
	 * @param usuario
	 * @return
	 */
	String InsertArchivoAdjuntoxMaterial(String txhCodigoUnico,
			String txtTitulo, String nomArchivo, String usuario);

	/**
	 * @param txhCodigoUnico
	 * @param usuario
	 * @return
	 */
	String deleteArchAdjuntoxMaterial(String txhCodigoUnico, String usuario);

	/**
	 * @param codMaterial
	 * @param cadena
	 * @param nroRegistros
	 * @param usuario
	 * @return
	 */
	String insertDescriptorxMaterial(String codMaterial, String cadena,
			String nroRegistros, String usuario);

	/**
	 * @param txhCodigoUnico
	 * @param usuario
	 * @return
	 */
	String deleteDescriptorxMaterial(String txhCodigoUnico, String usuario);

	/**
	 * @param txhCodigoUnico
	 * @param txtFecIngreso
	 * @param txtFecBaja
	 * @param cboProcedencia
	 * @param cboMoneda
	 * @param txtPrecio
	 * @param cboEstado
	 * @param txtObservacion
	 * @param usuario
	 * @return
	 */
	String insertIngresosxMaterial(String txhCodigoUnico, String txtFecIngreso,
			String txtFecBaja, String cboProcedencia, String cboMoneda,
			String txtPrecio, String cboEstado, String txtObservacion,
			String usuario);
	
	public String updateIngresosxMaterial(String codUnico, String fechaIngreso, String fechaBaja,
			String codProcedencia, String codMoneda, String precio, String codEstado, 
			String observaciones, String usuario);
	
	public String deleteIngresosxMaterial(String codUnico, String usuario);

	/**
	 * @param tipMaterial
	 * @param titulo
	 * @param codAutor
	 * @param codDewey
	 * @param nroPaginas
	 * @param codEditorial
	 * @param nroEditorial
	 * @param codPais
	 * @param codCiudad
	 * @param fecPublicacion
	 * @param urlArchDigital
	 * @param codIdioma
	 * @param isbn
	 * @param formato
	 * @param codTipoVideo
	 * @param codProcedencia
	 * @param codMoneda
	 * @param precio
	 * @param indPrestamoSala
	 * @param indPrestamoCasa
	 * @param indReserva
	 * @param observaciones
	 * @param imagen
	 * @param usuario
	 * @return
	 */
	String insertMaterial(String tipMaterial, String titulo, String codAutor,
			String codDewey, String nroPaginas, String codEditorial,
			String nroEditorial, String codPais, String codCiudad,
			String fecPublicacion, String urlArchDigital, String codIdioma,
			String isbn, String formato, String codTipoVideo,
			String codProcedencia, String codMoneda, String precio,
			String indPrestamoSala, String indPrestamoCasa, String indReserva,
			String observaciones, String imagen, String usuario,String ubicacionFisica,String codSede
			,String codGenerado,String prefijo,String sufijo,String frecuencia,String mencionSerie, String contenido);

	/**
	 * @param codMaterial
	 * @param tipMaterial
	 * @param titulo
	 * @param codAutor
	 * @param codDewey
	 * @param nroPaginas
	 * @param codEditorial
	 * @param nroEditorial
	 * @param codPais
	 * @param codCiudad
	 * @param fecPublicacion
	 * @param urlArchDigital
	 * @param codIdioma
	 * @param isbn
	 * @param formato
	 * @param codTipoVideo
	 * @param indPrestamoSala
	 * @param indPrestamoCasa
	 * @param indReserva
	 * @param imagen
	 * @param usuario
	 * @return
	 */
	String updateMaterial(String codMaterial, String tipMaterial,
			String titulo, String codAutor, String codDewey, String nroPaginas,
			String codEditorial, String nroEditorial, String codPais,
			String codCiudad, String fecPublicacion, String urlArchDigital,
			String codIdioma, String isbn, String formato, String codTipoVideo,
			String indPrestamoSala, String indPrestamoCasa, String indReserva,
			String imagen, String usuario,String ubicacionFisica,String codSede
			,String codGenerado,String prefijo,String sufijo,String frecuencia,String mencionSerie,String contenido);

	/**
	 * @param codMaterial
	 * @param usuario
	 * @return
	 */
	String deleteMaterial(String codMaterial, String usuario);

	/**
	 * @param txhCodUnico
	 * @return
	 */
	List getAllHistorialPrestamoxMaterial(String txhCodUnico);
	
	/**
	 * @param txhNroIngreso
	 * @param txhCodUsuario
	 * @return
	 */
	List getAllHistorialProrrogaxMaterial(String txhNroIngreso, String txhCodUsuario,String sede);

	/**
	 * @param codigo
	 * @return
	 */
	List getAlumnoXCodigo(String codigo,String indTipoPrestamo,String tipoBusquedaCodUsu,String sede, String numIngreso);

	/**
	 * @param codigo
	 * @return
	 */
	List getMaterialxIngreso(String codigo,String tipo,String sede,String codUsuario);

	/**
	 * @param codPrestamo
	 * @param usuPrestamo
	 * @param flgSancion
	 * @param usuCreacion
	 * @return
	 */
	String insertDevolucion(String codPrestamo, String usuPrestamo,
			String flgSancion, String usuCreacion);

	/**
	 * @param codMaterial
	 * @param nroIngreso
	 * @param codUsuPrestamo
	 * @param indProrroga
	 * @param usuCreacion
	 * @param indReserva
	 * @return
	 */
	
	String insertPrestamoxAlumno(String codMaterial, String nroIngreso,
			String codUsuPrestamo, String indProrroga, String usuCreacion,String indPrestamo, 
			String indReserva,	String fecPrestamo, Long codMultiple);

	List getAllDetalleMaterialFichaBibliografica(String txhCodUnico);
	
	public List GetAllReservaByAlumno(String codUsuario,String nroIngreso);
	
	
	/**
	 * @param codMaterial
	 * @return lista reservas por material
	 */
	public List getAllReservaByMaterial(String codMaterial);
	
	/**
	 * @param cadena
	 * @param nroReg
	 * @param usuCrea
	 * @return Elimina las reservas de material
	 */
	public String deleteReservasMaterial(String cadena, String nroReg, String usuCrea);
	
	/**
	 * @param codUsuario
	 * @return
	 */
	public List getAllDatosUsuarioBib(String codUsuario);
	
	//jhpr 2008-08-15
	public List getUsuariosXCodigo(String codigo, String tipo);
	public List<SedeBean> GetSedesBibPorUsuario(String codUsuario);
	
	public int getCantISBNPorSede(String isbn,String sede,Long codMaterial);
	public String getEstadoIngreso(String nCodIngreso);
	public boolean categoriaDuplicada(String codMaterial, String codGenerado);
	
	public ServiciosUsuarios getAllServicioByCodUsuario(String codUsuario);
	
	public Long obtenerCodigoPrestamoMultiple();

	//lista los libros que adeuda un usuario 
	public List<MaterialxIngresoBean> getListaLibrosDeudaXUsuario(String codUsuario);
	public List<MaterialxIngresoBean> getListaLibrosPrestadosXUsuario(String codUsuario);
	
}
