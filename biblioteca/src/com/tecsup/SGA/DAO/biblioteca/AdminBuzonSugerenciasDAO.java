package com.tecsup.SGA.DAO.biblioteca;

import java.util.List;

import com.tecsup.SGA.modelo.BuzonSugerencia;

public interface AdminBuzonSugerenciasDAO {

	public List GetAllSugerencias(String mes,String anio, String codigoSugerencia, String estado);
	public String UpdateAtenderBuzonSugerencia(String codSugerencia, String codCalificacion, 
			String codTipoMaterial,	String respuesta);
	public String InsertSugerencia(String tipo, String descripcion, String email, String usuario);
	
	public BuzonSugerencia obtenerSugerencia(String id); //JHPR 2008-7-30
	
}
