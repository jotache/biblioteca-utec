package com.tecsup.SGA.DAO.biblioteca;

import java.util.List;

import com.tecsup.SGA.bean.OpcionesApoyo;
import com.tecsup.SGA.modelo.AccesoIP;
import com.tecsup.SGA.modelo.ParametrosReservados;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.UsuarioPM;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;

public interface BiblioMantenimientoDAO {

	public List getAllTablaDetalleBiblioteca(String codTablaPadre, String codPadre, String codHijo,
			String codigo, String descripcion, String tipoOrden,String valor4,String valor5,String valor6,String tipoLista);
	public String insertTablaDetalleBiblioteca(String codDetallePadre, String codTipoTablaDetalle,
			String descripcion, String usuCrea, String valor3, String valor4 , String valor5,String valor6);
	public String UpdateAndDeleteTablaDetalleBiblioteca(String codDetallePadre, String codTipoTablaDetalle,
			String codRegistro,	String codGenerado, String descripcion, String Flag, String usuCrea,
			String valor3, String valor4 , String valor5,String valor6);
	public List getAllPaises(String codigo, String descripcion, String tiopoOrden);
	public String insertPaises(	String descripcion, String usuCrea);
	public List getCiudadByPais(String codigoPais, String codigoId, String descripcion, String tipoOrden);
	public String insertCiudadByPais(String codigoPais, String descripcion, String usuCrea);
	public List GetAllFeriados(String codTablaId, String mes, String anio);
	public String InsertFeriados(String codTablaId, String dis, String mes
			, String anio, String descripcion,String usucrea);
	public String updateDeletePais(String paisId, String descripcion, String Flag, String usuCrea);
	public String updateDeleteCiudadbyPais(String paisId, String ciudadId, String descripcion,
			String Flag, String usuCrea);
	public String ActualizarFeriado(String codTablaId, String id, String dis, String mes
			, String anio, String descripcion,String usucrea, String flag);
	public ServiciosUsuarios GetAllServicioUsuario(String tipId, String valor1);
	public String InsertServicioUsuario(String tiptId, String valor1, String valor2,String valor3,
			String valor4, String valor5, String valor6, String valor7, String valor8,
			String valor9,String valor10,String usuCrea,String cadenaDatos, String tamanoDatos);
	public String UpdateSanciones(String cadena, String nroRegistros, String codUsuario,String nroOcurrencias);
	public String UpdateGrupos(String codTabla, String cadena, String nroRegistros, String codUsuario);
	public List getSeccionesByFiltros(String codigoUnico,String codSeccion, String codGrupo, String codTipo);
	public String insertSecciones(String codUnico, String codSeccion, String dscTipoMaterial,
			String codGrupo, String dscTema, String dscCorta, String dscLarga, String dscUrl, String dscArchivo,
			String fechaPublicacion, String fechaVigencia, String flag, String usuario, String tipo, String nomAdjunto);

	public String InsertParametrosReservados(String codTablaId, String valor1, String valor2
			, String valor3, String valor4,String valor5,String valor6
			,String valor7,String valor8,String valor9,String usuario);
	public ParametrosReservados GetAllParametrosGenerales(String tipId);
	public List<UsuarioPM> GetAllUsuariosAcceso();
	public String GetNombreArchivoSeccion();
	public TipoTablaDetalle GetTipoTablaDetalle(String codPadre,String codHijo);
	public List<AccesoIP> GetAllAccesosLista(Integer codUsuario);
	public void InsertAccesoIP(Integer codUsuario, String numIP, String usuCreacion);
	public void DeleteAccesoIP(Integer codUsuario, String numIP);
	public void DeleteAccesosIP(Integer codPersona);
	public void DeleteOpcionesApoyo(Integer codPersona);
	public void DeleteRolesApoyo(String codUsuario);
	public List<ParametrosReservados> GetListParametrosGenerales();
	public String UpdateParametrosGenerales(String cadenaDatos,String usuario,String nroRegistros);
	public List<TipoTablaDetalle> listaMaximoDiasPrestamos(String codTipoUsuario);
	public List<OpcionesApoyo> listaDeOpcionesPorApoyo(String codApoyo);
	public String updateOpcionesApoyo (String codApoyo, String cadena, String nroRegistros, String codUsuario);
	public List<Sala> listaHorarioSalas(String sede);
	public List<TipoTablaDetalle> listaHorasBiblioteca(String sede);
	public String UpdateHorarioSalas(String cadenaDatos,String usuario,String nroRegistros);
}
