package com.tecsup.SGA.DAO.reclutamiento;

import java.util.List;

public interface ReportesDAO {

	public List GetAllProcesoEstado(String codTipoProceso, String codUniFunci, String dscProceso,String fecIni,
			String fecFin);
	
	public List GetAllProcesoPostulante(String codTipoProceso, String codUniFunci, String dscProceso,String nombre,
			String apePaterno, String apeMaterno,String fecha1,String fecha2);
	
	public List GetAllOferta(String codTipoProceso, String codUniFunci, String dscProceso,String fecIni,
			String fecFin);
}
