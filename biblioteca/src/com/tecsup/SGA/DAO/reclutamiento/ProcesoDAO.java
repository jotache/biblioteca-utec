package com.tecsup.SGA.DAO.reclutamiento;

import com.tecsup.SGA.modelo.Evaluacion;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.Oferta;
import java.util.*;

public interface ProcesoDAO {
	
	public String insertProceso(String codEtapa, String codProcesoRel,
			String descripcion, String codUnidadFuncional,
			String codAreaInteres, String nroAreaInteres, String codAreaEstudio,
			String codInstAcademica, String codGradoAcademico,
			String sueldoIni, String sueldoFin, String codEdad, String edad,
			String codSexo, String codDedicacion, String codINteresadoEn,
			String flagDispViajar, String anhosExpLab, String anhosExpDoc,
			String codUsuCrea, String codEdad1, String edad1, String codTipoMoneda,String puestoPostula,
			String codAreasEstudios, String nroAreaEstudios,
			String codAreasNivEstudios, String nroAreaNivEstudios,
			String codInstEduc,String nroInstEduc);
	
	public String updateProceso(String CodProceso, String codEtapa,
			String codProcesoRel, String descripcion,
			String codUnidadFuncional, String codAreaInteres, String nroAreaInteres,
			String codAreaEstudio, String codInstAcademica,
			String codGradoAcademico, String sueldoIni, String sueldoFin,
			String codEdad, String edad, String codSexo, String codDedicacion,
			String codINteresadoEn, String flagDispViajar, String anhosExpLab,
			String anhosExpDoc, String codUsuCrea, String codEdad1, String edad1, String codTipoMoneda,String puestoPostula,
			String codAreasEstudios, String nroAreaEstudios,
			String codAreasNivEstudios, String nroAreaNivEstudios,
			String codInstEduc,String nroInstEduc);
	
	public Proceso getProceso(String idProceso);
	
	public List getAllProceso(String unidadFuncional, String fecIni, String fecFin, String tipoEtapa );
	
	public String deleteProceso(String codProceso, String usuario);
	
	public List getAreasInteresByProceso(String codProceso);
	
	public Oferta getOfertaById(String codOferta);
	
	public String insertOferta(String codProceso, String fecIni, String fecFin, String descripcion
			, String denominacion, String nroVacantes, String usuCrea);
	
	public String updateOferta(String codOferta, String codProceso, String fecIni, String fecFin
			, String descripcion, String denominacion, String nroVacantes, String usuCrea);
	
	public List getAllPostulantes(String codProceso, String codEtapa, String nombres, String apellidos,String orden);
	
	public String insertPostulanteByProceso(String codPostulantes, String nroRegistros, String codProc
			, String tipo);
	
	public List getUsuarioByTipo(String tipo, String sistema);
	
	public String insertEvaluadorByProceso(String codProceso, String codTipoEvaluacion
					, String codEvaluador, String usuario);

	public String updateEvaluadorByProceso(String codRegEval, String codProceso, String codTipoEvaluacion
			, String codEvaluador, String usuario);
	
	public List getEvaluadoresByProceso(String codProceso);
	
	public String deletePostulanteByProceso(String codPostulantes, String numreg, String codProceso
			, String tipo, String usuario);
	
	public List getAllEvaluacionByPostulante(String codEtapa, String codProceso, String codPostulante);
	
	public List getAllProcesosParaRevision(String codEstado, String codProceso);
	
	/**
	 * @param codProceso
	 * @param codPostulante
	 * @return Retorna la calificacion final de un postulante.
	 */
	public Evaluacion getCalificacionFinal(String codProceso, String codPostulante);
	
	/**
	 * @param codPostulante
	 * @param archivo
	 * @param archivoGen
	 * @param codUsuario
	 * @return Actualiza el informe del postulante. 
	 */
	public String updateInformeFinalPostulante(String codPostulante, String archivo,String archivoGen,String codUsuario);
	
	/**
	 * 
	 * @param codProceso
	 * @param usuario
	 * @return
	 */
	public String cargarPostulantesXProceso(String codProceso, String usuario);
	
	public List getTrazaEvaluaciones(String codProceso, String codPostulante);
	/*public List getAreasEstudiosXProceso(String codProceso);
	public List getAreasNivelEstudiosXProceso(String codProceso);
	public List getAreasInstAcadXProceso(String codProceso);*/
	public List getDetallesFiltroProceso(String codProceso,String tipo);
	public List<Evaluador> getEvaluadores(String codProceso,String codTipoEvaluacion,String etapa);
	
	public List<Evaluador> getEvaluadoresAsignadosXProceso(String codProceso);
}
