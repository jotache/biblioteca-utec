package com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertEvaluacionPostulante extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertEvaluacionPostulante.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
										+ ".PKG_RECL_PROCESO.SP_INS_EVALUACION";

	private static final String CADPOST_ID = "E_C_CADPOST_ID";
	private static final String NRO_REGISTROS = "E_V_NRO_REGISTROS";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String PROC_CODETAPA = "E_C_PROC_CODETAPA";
	private static final String EVAC_CODTIPOEVALUACION = "E_C_EVAC_CODTIPOEVALUACION";
	private static final String EVAC_CODESTADOEVENTO = "E_C_EVAC_CODESTADOEVENTO";
	private static final String EVAC_COMENTARIO = "E_V_EVAC_COMENTARIO";
	private static final String EVAC_CODCALIFICACION = "E_V_EVAC_CODCALIFICACION";
	private static final String EVAL_CODCALIFICAFINETAPA = "E_C_EVAL_CODCALIFICAFINETAPA";	
	private static final String EVAL_COMENTARIOFINETAPA = "E_V_EVAL_COMENTARIOFINETAPA";	
	private static final String IND_CAMBIO_ESTADO = "E_C_IND_CAMBIO_ESTADO";
	private static final String CODUSUARIO = "E_V_CODUSUARIO";
	private static final String RETVAL = "S_V_RETVAL";
	
	public InsertEvaluacionPostulante(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(CADPOST_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(NRO_REGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODETAPA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAC_CODTIPOEVALUACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAC_CODESTADOEVENTO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAC_COMENTARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(EVAC_CODCALIFICACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAL_CODCALIFICAFINETAPA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAL_COMENTARIOFINETAPA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(IND_CAMBIO_ESTADO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		
		compile();
	}
	
	public Map execute(String codPostulantes, String nroRegistros, String codProc, String codEtapa
			, String tipoEvaluacion, String estadoEvento, String comentario, String calificaion
			, String calificacionEtapa, String comentarioFinEtapa, String flagActualizaEstado, String usuario) {
		Map inputs = new HashMap();
		
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("CADPOST_ID:"+codPostulantes);
		log.info("NRO_REGISTROS:"+nroRegistros);
		log.info("PROC_ID:"+codProc);
		log.info("PROC_CODETAPA:"+codEtapa);
		log.info("EVAC_CODTIPOEVALUACION:"+tipoEvaluacion);
		log.info("EVAC_CODESTADOEVENTO:"+estadoEvento);
		log.info("EVAC_COMENTARIO:"+comentario);
		log.info("EVAC_CODCALIFICACION:"+calificaion);
		log.info("EVAL_CODCALIFICAFINETAPA:"+calificacionEtapa);
		log.info("EVAL_COMENTARIOFINETAPA:"+comentarioFinEtapa);
		log.info("IND_CAMBIO_ESTADO:"+flagActualizaEstado);
		log.info("CODUSUARIO:"+usuario);
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		inputs.put(CADPOST_ID, codPostulantes);
		inputs.put(NRO_REGISTROS, nroRegistros);
		inputs.put(PROC_ID, codProc);
		inputs.put(PROC_CODETAPA, codEtapa);
		inputs.put(EVAC_CODTIPOEVALUACION, tipoEvaluacion);
		inputs.put(EVAC_CODESTADOEVENTO, estadoEvento);
		inputs.put(EVAC_COMENTARIO, comentario);
		inputs.put(EVAC_CODCALIFICACION, calificaion);
		inputs.put(EVAL_CODCALIFICAFINETAPA, calificacionEtapa);
		inputs.put(EVAL_COMENTARIOFINETAPA, comentarioFinEtapa);
		inputs.put(IND_CAMBIO_ESTADO, flagActualizaEstado);
		inputs.put(CODUSUARIO, usuario);
		
		return super.execute(inputs);
	}
}
