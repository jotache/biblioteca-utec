package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;


public class UpdateReclutaEstSec extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateReclutaEstSec.class);
	private static final String SPROC_NAME =CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_act_postulante_estudios_sec";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String POST_INSTITUCION1 = "E_V_POST_INSTITUCION1";
    private static final String POST_INSTITUCION2 = "E_V_POST_INSTITUCION2";
    private static final String POST_ANOINICIO1 = "E_C_POST_ANIOINICIO1";
    private static final String POST_ANOINICIO2 = "E_C_POST_ANIOINICIO2";
    private static final String POST_ANOFIN1 = "E_C_POST_ANIOFIN1";
    private static final String POST_ANOFIN2 = "E_C_POST_ANIOFIN2";
    private static final String POST_USU_MODI = "E_V_POST_USU_MODI";
    private static final String RETVAL = "S_V_RETVAL";
    
    public UpdateReclutaEstSec(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_INSTITUCION1, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_INSTITUCION2, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_ANOINICIO1, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_ANOFIN1, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_ANOINICIO2, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_ANOFIN2, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_USU_MODI, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(	String idRec, String colegio1, String colegio2, String anioIni1, String anioFin1,
    					String anioIni2, String anioFin2,String codUsuario) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_ID: "+ idRec);
    	log.info("POST_INSTITUCION1: "+ colegio1);
    	log.info("POST_INSTITUCION2: "+ colegio2);
    	log.info("POST_ANOINICIO1: "+ anioIni1);
    	log.info("POST_ANOFIN1: "+ anioFin1);
    	log.info("POST_ANOINICIO2: "+ anioIni2);
    	log.info("POST_ANOFIN2: "+ anioFin2);
    	log.info("POST_USU_MODI: "+ codUsuario);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
        inputs.put(POST_ID, idRec);
        inputs.put(POST_INSTITUCION1, colegio1);
        inputs.put(POST_INSTITUCION2, colegio2);
        inputs.put(POST_ANOINICIO1, anioIni1);
        inputs.put(POST_ANOFIN1, anioFin1);
        inputs.put(POST_ANOINICIO2, anioIni2);
        inputs.put(POST_ANOFIN2, anioFin2);
        inputs.put(POST_USU_MODI, codUsuario);
        
        return super.execute(inputs);
    }
}
