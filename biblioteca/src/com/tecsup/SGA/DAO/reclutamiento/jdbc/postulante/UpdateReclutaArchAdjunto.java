package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;


public class UpdateReclutaArchAdjunto extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateReclutaArchAdjunto.class);
	private static final String SPROC_NAME =CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_act_postulante_arch_adj";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String POST_CV = "E_V_POST_CV";
    private static final String POST_FOTO = "E_V_POST_IMG";
    private static final String POST_USU = "E_V_POST_USU_MODI";
    private static final String RETVAL = "S_V_RETVAL";
    
    public UpdateReclutaArchAdjunto(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_CV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_FOTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_USU, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String idRec, String cv, String foto,String codUsuario) {
    	
    	log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("POST_ID:"+idRec);
    	log.info("POST_CV:"+cv);
    	log.info("POST_FOTO:"+foto);
    	log.info("POST_USU:"+codUsuario);
    	log.info("*** FIN " + SPROC_NAME + " ***");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(POST_ID, idRec);
        inputs.put(POST_CV, cv);
        inputs.put(POST_FOTO, foto);
        inputs.put(POST_USU, codUsuario);
        
        return super.execute(inputs);
    }
}
