package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;


public class InsertPostularOferta extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertPostularOferta.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_ins_post_oferta";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String POST_OFERTA_ID = "E_C_OFER_ID";
    private static final String RETVAL = "S_V_RETVAL";
    
    public InsertPostularOferta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_OFERTA_ID, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String idRec, String codOferta) {
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_ID:"+idRec);
    	log.info("POST_OFERTA_ID:"+codOferta);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(POST_ID, idRec);
        inputs.put(POST_OFERTA_ID, codOferta);
        
        return super.execute(inputs);
    }
}
