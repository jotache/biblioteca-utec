package com.tecsup.SGA.DAO.reclutamiento.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;


import com.tecsup.SGA.DAO.reclutamiento.ProcesoEvaluacionesDAO;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.GetEvaluacionById;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.GetEvaluacionesByEvaluador;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.GetEvaluacionesByEvaluadorCbo;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.GetEvaluacionesPostulantesSeleccionPorTipoEval;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.GetEvaluacionesSeleccionPorPostulante;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.InsertEvaluacionPostulante;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.InsertEvaluacionPostulanteEnvio;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion.UpdateEvaluacionPostulante;
import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Postulante;

public class ProcesoEvaluacionesDAOJdbc extends JdbcDaoSupport implements ProcesoEvaluacionesDAO{
	private static Log log = LogFactory.getLog(ProcesoEvaluacionesDAOJdbc.class);
	public List getAllEvaluacionesByEvaluador(String codEtapa,
			String postEstado, String rrhhEstado, String codEvaluador,String codProceso) {
		
		GetEvaluacionesByEvaluador getEvaluacionesByEvaluador = new GetEvaluacionesByEvaluador(this.getDataSource());
		HashMap map = (HashMap)getEvaluacionesByEvaluador.execute(codEtapa, postEstado, rrhhEstado, codEvaluador,codProceso);
		if (!map.isEmpty())
		{
			return (List)map.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String insertEvaluacionPostulante(String codPostulantes,
			String nroRegistros, String codProc, String codEtapa,
			String tipoEvaluacion, String estadoEvento, String comentario,
			String calificaion, String calificacionEtapa,
			String comentarioFinEtapa, String indCambioEstado, String usuario) {
		InsertEvaluacionPostulante insertEvaluacionPostulante = new InsertEvaluacionPostulante(this.getDataSource());
		
		HashMap outputs = (HashMap)insertEvaluacionPostulante.execute(codPostulantes, nroRegistros
				, codProc, codEtapa, tipoEvaluacion, estadoEvento, comentario, calificaion
				, calificacionEtapa, comentarioFinEtapa, indCambioEstado, usuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	
	public String updateEvaluacionPostulante(String codPostulantesProceso,
			String codEvaluaciones, String nroRegistros,
			String codEstadoEvento, String comentario, String codCalificacion,
			String codCalificacionFinEtapa, String comenFinEtapa,
			String indCambioEstado, String usuario) {
		
		UpdateEvaluacionPostulante updateEvaluacionPostulante = new UpdateEvaluacionPostulante(this.getDataSource());
		
		HashMap outputs = (HashMap)updateEvaluacionPostulante.execute(codPostulantesProceso,
				codEvaluaciones, nroRegistros, codEstadoEvento, comentario, codCalificacion,
				codCalificacionFinEtapa, comenFinEtapa, indCambioEstado, usuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}
		
		return null;
	}
	
	public List getEvaluacionById(String codEvaluacion) {
		GetEvaluacionById getEvaluacionById = new GetEvaluacionById(this.getDataSource());
		
		HashMap map = (HashMap)getEvaluacionById.execute(codEvaluacion);
		if (!map.isEmpty())
		{
			return (List)map.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List getAllEvaluacionesByEvaluadorCbo(String codEtapa,
			String postEstado, String rrhhEstado, String codEvaluador) {
		GetEvaluacionesByEvaluadorCbo getEvaluacionesByEvaluador = new GetEvaluacionesByEvaluadorCbo(this.getDataSource());
		HashMap map = (HashMap)getEvaluacionesByEvaluador.execute(codEtapa, postEstado, rrhhEstado, codEvaluador);
		if (!map.isEmpty())
		{
			return (List)map.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public String updateEvaluacionPostulanteEnvio(String codPostulantes,
			String nroRegistros, String codProc, String codEtapa,
			String estadoEvento, String usuario) {
		
		InsertEvaluacionPostulanteEnvio insertEvaluacionPostulante = new InsertEvaluacionPostulanteEnvio(this.getDataSource());
		HashMap outputs = (HashMap)insertEvaluacionPostulante.execute(codPostulantes, nroRegistros, codProc, codEtapa, estadoEvento, usuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	//
	public List<Postulante> getPostulantesEnviadosJefeDpto(String cadenaCodPostulantes,String codProceso,String codEtapa) {
		log.info("INI getPostulantesEnviadosJefeDpto.getJdbcTemplate()");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		String codigo , whereCodigo="" ;
		
		StringTokenizer codigos = new StringTokenizer(cadenaCodPostulantes,"|");
		
		while ( codigos.hasMoreTokens() ){
			codigo = codigos.nextToken();
			whereCodigo = whereCodigo + "'" + codigo + "',";
		}
		
		if (!whereCodigo.equals("")) {
			whereCodigo = whereCodigo.substring(0, whereCodigo.length()-1);
		}
		
		String filtroDet = "";
		if (codEtapa.equals("0001"))
			filtroDet="   AND TTD3.TIPT_ID = '0014'";
		else
			filtroDet="   AND TTD3.TIPT_ID = '0016'";
		
		String sql = "SELECT PP.POST_ID AS CODIGOPOSTULANTE" + 
             ", POST.POST_NOMBRES || ' '  || POST.POST_APELLPAT || ' ' || POST.POST_APELLMAT AS NOMBREPOSTULANTE" +             
             ", PP.PPRO_CODESTADO AS CODESTADO" + 
             ", TTD3.TTDE_DESCRIPCION AS DESCRIPCIONESTADO, " +   
             " (SELECT TD_CAL.TTDE_ID||'-'||TD_CAL.TTDE_DESCRIPCION AS CALIFICACION FROM REC_EVALUACION EVAL" +                
             " 	INNER JOIN GENERAL.GEN_TIPO_TABLA_DETALLE TD ON TD.TIPT_ID = '0007' " +
             "                                        AND TD.TTDE_ID = EVAL.EVAC_CODTIPOEVALUACION " +
             " 	LEFT JOIN REC_CALIFICACION_TIPO_EVAL CAL ON CAL.CALIF_ID = EVAL.EVAC_CODCALIFICACION " +
             " 	LEFT JOIN GENERAL.GEN_TIPO_TABLA_DETALLE TD_CAL ON TD_CAL.TIPT_ID = '0008' " +
             "                                          AND TD_CAL.TTDE_ID = CAL.CALIF_CALIFICACION " +
             " 	WHERE EVAL.POST_ID = PP.POST_ID " +
             " 	AND EVAL.PROC_ID = PP.PROC_ID" +
                                       
             " 	AND EVAL.EVAC_CODETAPA='" + codEtapa  + "'" +
             " 	AND EVAL.EVAC_CODESTADOEVENTO IN ('0002')" +
             " 	AND EVAL.EVAC_FLAG_ESTADO_ACTUAL='1' AND EVAL.EVAC_EST_REG='A') AS CALIFICACION" +
             " FROM REC_POST_PROCESO PP" +
             " LEFT JOIN REC_PROCESO P ON PP.PROC_ID = P.PROC_ID" +
             " LEFT JOIN REC_POSTULANTE POST ON PP.POST_ID = POST.POST_ID" +
             " LEFT JOIN GENERAL.GEN_TIPO_TABLA_DETALLE TTD3 ON PP.PPRO_CODESTADO = TTD3.TTDE_ID" +             
             	filtroDet +
             " WHERE P.PROC_ID = TO_NUMBER(?)" +
             " AND P.PROC_CODETAPA = '" + codEtapa  + "'" +
             " AND PP.POST_ID IN (" + whereCodigo + ")" +
             " AND PP.PPRO_EST_REG = 'A'";
		
		log.info("Sql:" + sql);
		log.info("FIN getPostulantesEnviadosJefeDpto");
		final Object[] args = new Object[] {codProceso};
		
		return jdbcTemplate.query(sql, args, new PostulanteRowMapper());
	}
	
	final class PostulanteRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			Postulante postulante = new Postulante();
			postulante.setCodPostulante(rs.getString("CODIGOPOSTULANTE"));
			postulante.setNombres(rs.getString("NOMBREPOSTULANTE"));
			postulante.setCodEstado(rs.getString("CODESTADO"));
			postulante.setDscEstado(rs.getString("DESCRIPCIONESTADO"));
			postulante.setUltEstadoPostulRevision(rs.getString("CALIFICACION")==null?"":rs.getString("CALIFICACION"));
			return postulante;
		}
	}

	final class PostulanteRevisadosRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			EvaluacionesByEvaluadorBean postulante = new EvaluacionesByEvaluadorBean();
			postulante.setCodPostulante(rs.getString("CODIGOPOSTULANTE"));
			postulante.setDscTipoEvaluacion(rs.getString("TIPOEVALUACION"));
			postulante.setNomPostulante(rs.getString("NOMBREPOSTULANTE"));
			postulante.setCv(rs.getString("CV")==null?"":rs.getString("CV"));
			postulante.setDscProceso(rs.getString("PROCESO"));
			postulante.setCodEstado(rs.getString("CODESTADO"));
			postulante.setDscEstado(rs.getString("DESCRIPCIONESTADO"));
			postulante.setComentario(rs.getString("EVAC_COMENTARIO")==null?"":rs.getString("EVAC_COMENTARIO"));
			postulante.setDscCalificacion(rs.getString("CALIF")); //Calif de Jefe Dpto.
			postulante.setDscCalificacion2(rs.getString("CALIF_RRHH"));
			postulante.setComentario2(rs.getString("COMENTARIO_RRHH"));//comentario de rrhh
			return postulante;
		}
	}
	
	public List<EvaluacionesByEvaluadorBean> getEvaluacionesSeleccionPorPostulante(
			String codProceso, String codPostulante) {
		
		GetEvaluacionesSeleccionPorPostulante getDatos = new GetEvaluacionesSeleccionPorPostulante(this.getDataSource());
		HashMap map = (HashMap) getDatos.execute(codProceso, codPostulante);
		if (!map.isEmpty())
		{
			return (List)map.get(CommonConstants.RET_CURSOR);
		}				
		return null;
	}

	public List<EvaluacionesByEvaluadorBean> getEvaluacionesPostulantesSeleccionPorTipoEvaluacion(
			String codProceso, String codTipoEvaluacion) {
		GetEvaluacionesPostulantesSeleccionPorTipoEval getDatos = new GetEvaluacionesPostulantesSeleccionPorTipoEval(this.getDataSource());
		HashMap map = (HashMap) getDatos.execute(codProceso, codTipoEvaluacion);
		if (!map.isEmpty())
		{
			return (List)map.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List<EvaluacionesByEvaluadorBean> getEvaluacionesRevisadas(
			String codProceso, String codEvaluador) {
		if(codProceso.equals("")) codProceso="0";
		log.info("INI getEvaluacionesRevisadas.getJdbcTemplate()");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		String sql = "SELECT post.post_id as CODIGOPOSTULANTE, t1.ttde_descripcion TipoEvaluacion" +
		" ,POST.POST_APELLPAT || ' ' || POST.POST_APELLMAT ||', '||POST.POST_NOMBRES AS NOMBREPOSTULANTE, POST.POST_CV AS CV" +
		" ,(select rec_proceso.proc_denominacion from rec_proceso where rec_proceso.proc_id=n.proc_id) Proceso"+
		" ,TO_CHAR(POST.POST_PRETENSION) AS SALARIO, n.evac_codestadoevento AS CODESTADO "+
		" ,TTD3.TTDE_DESCRIPCION AS DESCRIPCIONESTADO,n.evac_comentario,"+
		" (SELECT td1.ttde_descripcion FROM rec_calificacion_tipo_eval c, general.gen_tipo_tabla_detalle td1 "+
		" WHERE c.calif_calificacion=td1.ttde_id and td1.tipt_id='0008' AND c.calif_id=n.evac_codcalificacion "+
		" AND c.calif_est_reg='A' AND td1.ttde_est_reg='A') CALIF"+
		
		" ,(SELECT td1.ttde_descripcion FROM rec_calificacion_tipo_eval c, general.gen_tipo_tabla_detalle td1 ," +
		" rec_evaluacion f where f.proc_id=n.proc_id and f.post_id=n.post_id " +
		" and c.calif_calificacion=td1.ttde_id and td1.tipt_id='0008' AND c.calif_id=f.evac_codcalificacion and f.evac_codestadoevento='0002'" +
		" AND c.calif_est_reg='A' AND td1.ttde_est_reg='A') CALIF_RRHH" +
						 				
		",(select f.evac_comentario from rec_evaluacion f where f.proc_id=n.proc_id and f.post_id=n.post_id and f.evac_codestadoevento='0002') COMENTARIO_RRHH" +						
		
		" FROM rec_evaluacion n, general.gen_tipo_tabla_detalle t1 , rec_postulante post,GENERAL.GEN_TIPO_TABLA_DETALLE TTD3 "+
		" WHERE n.evac_codtipoevaluacion = t1.ttde_id and t1.tipt_id='0007' AND post.post_id=n.post_id "+
		" AND n.evac_codestadoevento = TTD3.TTDE_ID AND TTD3.TIPT_ID = '0014' "+
		" AND n.evac_usuario=? AND (?=0 OR n.proc_id=?)"+
		" AND n.evac_codetapa='0001' AND n.evac_codestadoevento='0003'" + //--REVISADO POR JEFE DPTO.
		" AND n.evac_est_reg='A' AND post.post_est_reg='A'" +
		" AND (select count(*) from rec_post_proceso  where rec_post_proceso.proc_id=n.proc_id and rec_post_proceso.ppro_est_reg='A' and rec_post_proceso.ppro_codestado='0002')>0";
		
		log.info("Sql:" + sql);		
		log.info("FIN getPostulantesEnviadosJefeDpto");
		final Object[] args = new Object[] {codEvaluador,codProceso,codProceso};		
		List<EvaluacionesByEvaluadorBean> query = jdbcTemplate.query(sql, args, new PostulanteRevisadosRowMapper());
		return query;		
	}
	
}
