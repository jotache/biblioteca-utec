package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;



public class UpdateReclutaClave extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateReclutaClave.class);
	private static final String SPROC_NAME =CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_act_cambiar_clave";
    private static final String POST_EMAIL = "E_V_POST_EMAIL";
    private static final String POST_CLAVE_ANTERIOR = "E_V_CLAVE_ANTERIOR";
    private static final String POST_CLAVE_NUEVA = "E_V_CLAVE_NUEVA";
    private static final String RETVAL = "S_V_RETVAL";
    
    public UpdateReclutaClave(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_EMAIL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_CLAVE_ANTERIOR, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_CLAVE_NUEVA, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String email, String clave_anterior, String clave_nueva) {
    	
    	log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("POST_EMAIL:"+email);
    	log.info("POST_CLAVE_ANTERIOR:"+clave_anterior);
    	log.info("POST_CLAVE_NUEVA:"+clave_nueva);
    	log.info("*** INI " + SPROC_NAME + " ***");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(POST_EMAIL, email);
        inputs.put(POST_CLAVE_ANTERIOR, clave_anterior);
        inputs.put(POST_CLAVE_NUEVA, clave_nueva);
        
        return super.execute(inputs);
    }
}
