package com.tecsup.SGA.DAO.reclutamiento.jdbc.reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReporteReclutamiento;

public class GetAllProcesoPostulante extends StoredProcedure  {
	
	private static Log log = LogFactory.getLog(GetAllProcesoPostulante.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
	".pkg_recl_reportes.sp_sel_proceso_postulante";
	
	private static final String E_V_COD_TIP_PROCESO = "E_V_COD_TIP_PROCESO";
	private static final String E_V_COD_UNI_FUNCIONAL = "E_V_COD_UNI_FUNCIONAL";
	private static final String E_V_DSC_PROCESO = "E_V_DSC_PROCESO";
	private static final String E_V_NOMBRE = "E_V_NOMBRE";
	private static final String E_V_APE_PAT = "E_V_APE_PAT";
	private static final String E_V_APE_MAT = "E_V_APE_MAT";
	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllProcesoPostulante(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_COD_TIP_PROCESO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_COD_UNI_FUNCIONAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DSC_PROCESO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NOMBRE, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_APE_PAT, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_APE_MAT, OracleTypes.VARCHAR));	
	declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	compile();
	}
	
	public Map execute(String codTipoProceso, String codUniFunci, String dscProceso,String nombre,
			String apePaterno, String apeMaterno,String fecha1,String fecha2) {
	
	Map inputs = new HashMap();
	
	log.info("*** INI " + SPROC_NAME + " ***");
	log.info("	E_V_COD_TIP_PROCESO:" + codTipoProceso );
	log.info("	E_V_COD_UNI_FUNCIONAL:" + codUniFunci );
	log.info("	E_V_DSC_PROCESO:" + dscProceso );
	log.info("	E_V_NOMBRE:" + nombre );
	log.info("	E_V_APE_PAT:" + apePaterno );
	log.info("	E_V_APE_MAT:" + apeMaterno );
	log.info("	E_V_FEC_INI:" + fecha1 );
	log.info("	E_V_FEC_FIN:" + fecha2 );
	log.info("*** FIN " + SPROC_NAME + " ***");
	
	inputs.put(E_V_COD_TIP_PROCESO, codTipoProceso);
	inputs.put(E_V_COD_UNI_FUNCIONAL, codUniFunci);
	inputs.put(E_V_DSC_PROCESO, dscProceso);
	inputs.put(E_V_NOMBRE, nombre);
	inputs.put(E_V_APE_PAT, apePaterno);
	inputs.put(E_V_APE_MAT, apeMaterno);
	inputs.put(E_V_FEC_INI, fecha1);
	inputs.put(E_V_FEC_FIN, fecha2);
	
	return super.execute(inputs);
	
	}
	
	final class ProcesoMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	
	ReporteReclutamiento proceso = new ReporteReclutamiento();
	
	proceso.setCodPostulante(rs.getString("COD_POSTULANTE") == null ? "" : rs.getString("COD_POSTULANTE"));
	proceso.setDscPostulante(rs.getString("NOMBRE_POSTULANTE") == null ? "" : rs.getString("NOMBRE_POSTULANTE"));
	proceso.setDscEstadoCV(rs.getString("DSC_ESTADO_CV") == null ? "" : rs.getString("DSC_ESTADO_CV"));
	proceso.setDscProceso(rs.getString("NMB_PROCESO") == null ? "" : rs.getString("NMB_PROCESO"));
	proceso.setDscTipoProceso(rs.getString("DSC_TIPO_PROCESO") == null ? "" : rs.getString("DSC_TIPO_PROCESO"));
	proceso.setDscEstado(rs.getString("DSC_ESTADO_PROCESO") == null ? "" : rs.getString("DSC_ESTADO_PROCESO"));
	proceso.setFecProceso(rs.getString("FECPROCESO") == null ? "" : rs.getString("FECPROCESO"));
	return proceso;
	}

}

}
