package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Evaluador;

public class GetEvaluadorByProceso extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetEvaluadorByProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
									".pkg_recl_proceso.SP_SEL_EVALUADORES_X_PROCESO";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetEvaluadorByProceso(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new EvaluadorMapper()));
        compile();
    }

    public Map execute(String codProceso) {
    		
    	log.info("**** INI " + SPROC_NAME + "****");
    	log.info("PROC_ID:"+ codProceso);
    	log.info("**** FIN " + SPROC_NAME + "****");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(PROC_ID, codProceso);
        return super.execute(inputs);

    }

    final class EvaluadorMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Evaluador evaluador = new Evaluador();
        	
        	evaluador.setCodRegEvaluador(rs.getString("COD_EVALUACION") == null ? "" : rs.getString("COD_EVALUACION"));
        	evaluador.setCodProceso(rs.getString("CODIGOPROCESO") == null ? "" : rs.getString("CODIGOPROCESO"));
        	evaluador.setCodTipoEvaluacion(rs.getString("CODIGOTIPOEVAL") == null ? "" : rs.getString("CODIGOTIPOEVAL"));
        	evaluador.setDscTipoEvaluacion(rs.getString("DESCRIPCIONTIPOEVALUACION") == null ? "" : rs.getString("DESCRIPCIONTIPOEVALUACION"));
        	evaluador.setCodEvaluador(rs.getString("CODIGOEVALUADOR") == null ? "" : rs.getString("CODIGOEVALUADOR"));
        	evaluador.setDscEvaluador(rs.getString("NOMBREEVALUADOR") == null ? "" : rs.getString("NOMBREEVALUADOR"));
       	
            return evaluador;
        }

    }
}
