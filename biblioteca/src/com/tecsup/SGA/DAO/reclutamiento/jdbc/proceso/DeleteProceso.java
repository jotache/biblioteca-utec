package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteProceso extends StoredProcedure{
	private static Log log = LogFactory.getLog(DeleteProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
		+ ".PKG_RECL_PROCESO.SP_DEL_PROCESO";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String PROC_USU_MODI = "E_V_PROC_USU_MODI";
	private static final String RETVAL = "S_V_RETVAL";

	public DeleteProceso(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_USU_MODI, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	//hola
	
	public Map execute(String CodProceso, String codModCrea)
	{
		log.info("*** INI "  + SPROC_NAME + " ****");
		log.info("PROC_ID:"+ CodProceso);
		log.info("PROC_USU_MODI:"+ codModCrea);
		log.info("*** FIN "  + SPROC_NAME + " ****");
			
		Map inputs = new HashMap();
		inputs.put(PROC_ID, CodProceso);
		inputs.put(PROC_USU_MODI, codModCrea);
	
		return super.execute(inputs);
	
	}
}
