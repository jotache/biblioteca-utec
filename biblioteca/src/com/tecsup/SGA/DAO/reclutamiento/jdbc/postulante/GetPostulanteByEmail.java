package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Postulante;

public class GetPostulanteByEmail extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetPostulanteByEmail.class);
		private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
		+ ".pkg_recl_postulante.sp_sel_postulante_by_mail";
		
	private static final String E_V_MAIL_POSTULANTE = "E_V_MAIL_POSTULANTE";
	
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetPostulanteByEmail(DataSource dataSource)
	{
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_MAIL_POSTULANTE, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new PostulanteMapper()));
	compile();
	}
	
	public Map execute(String emailPostulante)
	{
		log.info("*** INI " + SPROC_NAME + "***");
		log.info("E_V_MAIL_POSTULANTE:" + emailPostulante);
		log.info("*** FIN " + SPROC_NAME + "***");
		
		Map inputs = new HashMap();		
		inputs.put(E_V_MAIL_POSTULANTE, emailPostulante);			
		return super.execute(inputs);	
	}
	
	final class PostulanteMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	
	Postulante postulante = new Postulante();
	
	postulante.setCodPostulante(rs.getString("COD_POSTULANTE") == null ? "" : rs.getString("COD_POSTULANTE").trim());
	postulante.setNombres(rs.getString("NOMBRE") == null ? "" : rs.getString("NOMBRE").trim());
	postulante.setEmail(rs.getString("CORREO") == null ? "" : rs.getString("CORREO").trim());
	postulante.setClave(rs.getString("CLAVE") == null ? "" : rs.getString("CLAVE").trim());
	
	return postulante;
	}
}

}
