package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Recluta;


public class GetAllOfertas extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllOfertas.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_sel_oferta";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllOfertas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new OfertasMapper()));
        compile();
    }

    public Map execute() {
    	log.info("**** EXEC: " + SPROC_NAME);
    	Map inputs = new HashMap();
        
        return super.execute(inputs);
    }
    
    final class OfertasMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Recluta recluta = new Recluta();
        	
        	recluta.setCodOferta(rs.getString("CODOFERTA"));
        	recluta.setArea(rs.getString("AREA"));
        	recluta.setOferta(rs.getString("DENOMINACION"));
        	recluta.setNroVacantes(rs.getString("NROVACANTES"));
        	recluta.setFuncionesOferta(rs.getString("DESCRIPCION"));
        	recluta.setFechaPubli(rs.getString("FECHAINI"));
        	recluta.setFechaCierre(rs.getString("FECHAFIN"));
        	
            return recluta;
        }
    }
}
