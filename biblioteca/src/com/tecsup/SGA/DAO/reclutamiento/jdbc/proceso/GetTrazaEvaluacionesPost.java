package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.TrazaEvaluacion;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Postulante;

public class GetTrazaEvaluacionesPost extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetTrazaEvaluacionesPost.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
			+ ".PKG_RECL_PROCESO.SP_SEL_EVAL_TRAZA_POSTULANTE";
	private static final String E_C_COD_POSTULANTE = "E_C_COD_POSTULANTE";
	private static final String E_C_COD_PROCESO = "E_C_COD_PROCESO";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetTrazaEvaluacionesPost(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_POSTULANTE, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_COD_PROCESO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new TrazaEvalPostMapper()));
		compile();
	}
	
	public Map execute(String codProceso, String codPostulante) {
		
		log.info("**** INI " + SPROC_NAME + "****");
    	log.info("E_C_COD_POSTULANTE:"+ codPostulante);
    	log.info("E_C_COD_PROCESO:"+ codProceso);
    	log.info("**** FIN " + SPROC_NAME + "****");
		
		Map inputs = new HashMap();					
		inputs.put(E_C_COD_POSTULANTE, codPostulante);
		inputs.put(E_C_COD_PROCESO, codProceso);		
		return super.execute(inputs);	
	}
	
	final class TrazaEvalPostMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			TrazaEvaluacion eval = new TrazaEvaluacion();
			
			eval.setEtapa(rs.getString("ETAPA") == null ? "" : rs.getString("ETAPA").trim());
			eval.setCalificacion(rs.getString("CALIFICACION") == null ? "" : rs.getString("CALIFICACION").trim());
						
			return eval;
		}
		
	}
}
