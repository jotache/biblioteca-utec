package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Recluta;



public class GetReclutaDatosPersonales extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReclutaDatosPersonales.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_sel_postulante_x_codigo";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetReclutaDatosPersonales(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new DatosReclutaMapper()));
        compile();
    }

    public Map execute(String idRec) {
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_ID:"+idRec);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();
        inputs.put(POST_ID, idRec);//idRec
        
        return super.execute(inputs);

    }
    
    final class DatosReclutaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Recluta recluta = new Recluta();
        	
        	recluta.setCodRecluta(rs.getString("CodigoPostulante"));
        	recluta.setNombre(rs.getString("Nombre"));
        	recluta.setApepat(rs.getString("ApellidoPaterno"));
        	recluta.setApemat(rs.getString("ApellidoMaterno"));
        	recluta.setEmail(rs.getString("Email"));
        	recluta.setClave(rs.getString("CLAVE"));
        	recluta.setEstadoCivil(rs.getString("CodigoEstadoCivil"));
        	recluta.setEmailAlterno(rs.getString("EmailAlterno"));
        	recluta.setFecnac(rs.getString("FechaNac"));
        	recluta.setFecreg(rs.getString("FECHAREGISTRO"));
        	recluta.setSexo(rs.getString("Sexo"));
        	recluta.setNacionalidad(rs.getString("CodigoNacionalidad"));
        	recluta.setRuc(rs.getString("RUC"));
        	recluta.setDni(rs.getString("DNI").trim());
        	recluta.setAnioExpLaboral(rs.getString("ExperienciaLaboral"));
        	recluta.setDireccion(rs.getString("Domicilio"));
        	recluta.setDepartamento(rs.getString("CodigoDepartamento"));
        	recluta.setProvincia(rs.getString("CodigoProvincia"));
        	recluta.setDistrito(rs.getString("CodigoDistrito"));
        	recluta.setPais(rs.getString("PAIS"));
        	recluta.setTelef(rs.getString("TelefonoDomicilio"));
        	recluta.setTelefAdicional(rs.getString("TelefonoAdicional"));
        	recluta.setTelefMovil(rs.getString("TelefonoMovil"));
        	recluta.setCodPostal(rs.getString("CodPostal"));
        	recluta.setPostuladoAntes(rs.getString("CodPostuladoAntes"));
        	recluta.setTrabajadoAntes(rs.getString("CodTrabajadoAntes"));
        	recluta.setFamiliaTecsup(rs.getString("CodFamilTrabAntes"));
        	recluta.setFamiliaNombres(rs.getString("FamiliaNombres"));
        	recluta.setExpDocente(rs.getString("CodExpDocencia"));
        	recluta.setExpDocenteAnios(rs.getString("ExpDocenciaAnios"));
        	recluta.setDispoViaje(rs.getString("CodDispoViaje"));
        	recluta.setSedePrefTrabajo(rs.getString("CodSedePreferente"));
        	recluta.setInteresEn(rs.getString("CodInteresEn"));
        	recluta.setPretensionEconomica(rs.getString("PretencionEconomica"));
        	recluta.setDispoTrabajar(rs.getString("CodigoDisponibilidad"));
        	recluta.setDedicacion(rs.getString("CodigoDedicacion"));
        	recluta.setPerfil(rs.getString("Perfil"));
        	
        	recluta.setColegio1(rs.getString("INSTITUCION1"));
        	recluta.setColegio2(rs.getString("INSTITUCION2"));
        	recluta.setAnioInicio1(rs.getString("ANIOINICIO1"));
        	recluta.setAnioInicio2(rs.getString("ANIOINICIO2"));
        	recluta.setAnioFin1(rs.getString("ANIOFIN1"));
        	recluta.setAnioFin2(rs.getString("ANIOFIN2"));
        	
        	recluta.setFoto(rs.getString("FOTO"));
        	recluta.setCv(rs.getString("CV"));
        	
        	recluta.setFotoOriginal(rs.getString("FOTO_ORIGINAL"));
        	recluta.setCvOriginal(rs.getString("CV_ORIGINAL"));
        	
        	recluta.setMoneda(rs.getString("MONEDA"));
        	recluta.setTipoPago(rs.getString("IND_TIPO_PAGO"));
        	
        	recluta.setInfFinal(rs.getString("INFORMEFINAL") == null ? "" : rs.getString("INFORMEFINAL"));
        	recluta.setInfFinalGen(rs.getString("INFORMEFINAL_GEN") == null ? "" : rs.getString("INFORMEFINAL_GEN"));
        	recluta.setPuestoPostula(rs.getString("PUESTOPOSTULA") == null ? "" : rs.getString("PUESTOPOSTULA"));
        	
            return recluta;
        }
    }
}
