package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;


public class UpdateReclutaIdiomas extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateReclutaIdiomas.class);
	private static final String SPROC_NAME =CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_act_postulante_idiomas";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String POST_CADENA_IDIOMAS = "E_V_CAD_IDIOMAS";
    private static final String POST_NRO_REG = "E_V_NRO_REGISTROS";
    private static final String POST_USU_MODI = "E_V_CODUSUARIO";
    private static final String RETVAL = "S_V_RETVAL";
    
    public UpdateReclutaIdiomas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_CADENA_IDIOMAS, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_NRO_REG, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_USU_MODI, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String idRec, String cadena, String nroReg,String codUsuario) {
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_ID:" + idRec);
    	log.info("POST_CADENA_IDIOMAS:" + cadena);
    	log.info("POST_NRO_REG:" + nroReg);
    	log.info("POST_USU_MODI:" + codUsuario);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();    	
        inputs.put(POST_ID, idRec);
        inputs.put(POST_CADENA_IDIOMAS, cadena);
        inputs.put(POST_NRO_REG, nroReg);
        inputs.put(POST_USU_MODI, codUsuario);
        
        return super.execute(inputs);
    }
}

