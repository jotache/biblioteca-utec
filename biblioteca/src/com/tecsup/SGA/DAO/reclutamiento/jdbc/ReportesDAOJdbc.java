package com.tecsup.SGA.DAO.reclutamiento.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.reclutamiento.ReportesDAO;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.reportes.*;

public class ReportesDAOJdbc extends JdbcDaoSupport implements ReportesDAO{

	
	public List GetAllProcesoEstado(String codTipoProceso, String codUniFunci, String dscProceso,String fecIni,
			String fecFin)
	{
		GetAllProcesoEstado getAllProcesoEstado = new GetAllProcesoEstado(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProcesoEstado.execute(codTipoProceso, codUniFunci, dscProceso,
				fecIni, fecFin);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}//
	
	public List GetAllProcesoPostulante(String codTipoProceso, String codUniFunci, String dscProceso,String nombre,
			String apePaterno, String apeMaterno,String fecha1,String fecha2)
	{
		GetAllProcesoPostulante getAllProcesoPostulante = new GetAllProcesoPostulante(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProcesoPostulante.execute(codTipoProceso, codUniFunci,
				dscProceso, nombre, apePaterno, apeMaterno,fecha1,fecha2);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}//
	
	public List GetAllOferta(String codTipoProceso, String codUniFunci, String dscProceso,String fecIni,
			String fecFin)
	{
		GetAllOferta getAllOferta = new GetAllOferta(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllOferta.execute(codTipoProceso, codUniFunci, dscProceso, fecIni,
				fecFin);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}
