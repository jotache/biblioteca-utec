package com.tecsup.SGA.DAO.reclutamiento.jdbc;

import java.util.HashMap;
import java.util.List;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.*;
import com.tecsup.SGA.DAO.reclutamiento.PostulanteDao;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante.GetAllPostulante;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante.GetPostulanteByEmail;
import com.tecsup.SGA.common.CommonConstants;

public class PostulanteDaoJdbc extends JdbcDaoSupport implements PostulanteDao {
	
	public List getAllPostulante(String nombres, String apellidos,
			String areasInteres, String profesion, String egresado, String grado,
			String codProceso, String codEtapa) {
		
		GetAllPostulante getAllPostulante = new GetAllPostulante(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllPostulante.execute(nombres, apellidos, areasInteres
															, profesion, egresado, grado
															, codProceso, codEtapa);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	
	public List GetPostulanteByEmail(String emailPostulante) {
		
		GetPostulanteByEmail getPostulanteByEmail = new GetPostulanteByEmail(this.getDataSource());
		
		HashMap outputs = (HashMap)getPostulanteByEmail.execute(emailPostulante);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
}
