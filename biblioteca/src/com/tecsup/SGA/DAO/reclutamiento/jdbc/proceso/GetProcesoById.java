package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Proceso;

public class GetProcesoById extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetProcesoById.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
											+ ".PKG_RECL_PROCESO.SP_SEL_PROCESO_X_ID";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String RECORDSET = "S_C_RECORDSET";

	public GetProcesoById(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
		compile();
	}

	public Map execute(String codProceso) {
	
		log.info("**** INI " + SPROC_NAME + "****");
    	log.info("PROC_ID:"+ codProceso);
    	log.info("**** FIN " + SPROC_NAME + "****");
    	
		Map inputs = new HashMap();		
		inputs.put(PROC_ID, codProceso);		
		return super.execute(inputs);
	
	}

	final class ProcesoMapper implements RowMapper {
	
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Proceso proceso = new Proceso();
		
		proceso.setCodProceso(rs.getString("CodigoProceso") == null ? "" : rs.getString("CodigoProceso").trim());
		proceso.setDscProceso(rs.getString("DESCRIPCIONPROCESO") == null ? "" : rs.getString("DESCRIPCIONPROCESO").trim());
		proceso.setCodUnidadFuncional(rs.getString("CodigoUnidadFuncional") == null ? "" : rs.getString("CodigoUnidadFuncional").trim());
		proceso.setDscUnidadFuncional(rs.getString("DescripcionUnidadFuncional") == null ? "" : rs.getString("DescripcionUnidadFuncional").trim());
		proceso.setCodEtapa(rs.getString("CodigoEtapa") == null ? "" : rs.getString("CodigoEtapa").trim());
		proceso.setDscEtapa(rs.getString("DescripcionEtapa") == null ? "" : rs.getString("DescripcionEtapa").trim());
		proceso.setCodAreaEstudio(rs.getString("CodigoProfesion") == null ? "" : rs.getString("CodigoProfesion").trim());
		proceso.setCodGradoAcedemico(rs.getString("CodigoGradoAcademico") == null ? "" : rs.getString("CodigoGradoAcademico").trim());
		//proceso.setdsc(rs.getString("DescripcionGrado") == null ? "" : rs.getString("DescripcionGrado").trim());
		//proceso.setCodTipoTabla(rs.getString("DescripcionProfesion") == null ? "" : rs.getString("DescripcionProfesion").trim());
		proceso.setCodInstitucionAcademica(rs.getString("CodigoInstitucion") == null ? "" : rs.getString("CodigoInstitucion").trim());
		//proceso.setDsc(rs.getString("DescripcionInstitucion") == null ? "" : rs.getString("DescripcionInstitucion").trim());
		proceso.setSalarioIni(rs.getString("PretencionInicial") == null ? "" : rs.getString("PretencionInicial").trim());
		proceso.setSalarioFin(rs.getString("PretencionFinal") == null ? "" : rs.getString("PretencionFinal").trim());
		proceso.setDscOfertaAsociada(rs.getString("NombreOferta") == null ? "" : rs.getString("NombreOferta").trim());
		proceso.setCodOfertaAsociada(rs.getString("codigooferta") == null ? "" : rs.getString("codigooferta").trim());
		proceso.setCodSexo(rs.getString("SEXO") == null ? "" : rs.getString("SEXO").trim());
		proceso.setCodDedicacion(rs.getString("CODIGODEDICACION") == null ? "" : rs.getString("CODIGODEDICACION").trim());
		proceso.setAnhosExperiencia(rs.getString("ANIOSEXPLAB") == null ? "" : rs.getString("ANIOSEXPLAB").trim());
		proceso.setAnhosExperienciaDocencia(rs.getString("ANIOSEXPDOC") == null ? "" : rs.getString("ANIOSEXPDOC").trim());
		proceso.setFlagDisponibilidadViajar(rs.getString("DISPVIAJAR") == null ? "" : rs.getString("DISPVIAJAR").trim());
		proceso.setCodInteresado(rs.getString("CODINTERESADOEN") == null ? "" : rs.getString("CODINTERESADOEN").trim());
		proceso.setCodTipoEdad(rs.getString("CODSIMBOLO") == null ? "" : rs.getString("CODSIMBOLO").trim());
		proceso.setEdad(rs.getString("EDAD") == null ? "" : rs.getString("EDAD").trim());
		proceso.setCodTipoEdad1(rs.getString("CODSIMBOLO1") == null ? "" : rs.getString("CODSIMBOLO1").trim());
		proceso.setEdad1(rs.getString("EDAD1") == null ? "" : rs.getString("EDAD1").trim());
		proceso.setCodProcRevAsociado(rs.getString("CODPROCESOASOCIADO") == null ? "" : rs.getString("CODPROCESOASOCIADO").trim());
		proceso.setCodTipoMoneda(rs.getString("MONEDA") == null ? "" : rs.getString("MONEDA").trim());
		proceso.setPuestoPostula(rs.getString("PUESTOPOSTULA") == null ? "" : rs.getString("PUESTOPOSTULA").trim());
		
		return proceso;
		}
	}

}
