package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Evaluacion;

public class GetEvaluacionFinal extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetEvaluacionFinal.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
									+ ".PKG_RECL_PROCESO.SP_SEL_CAL_FINAL";
	
	private static final String COD_PROCESO = "E_C_COD_PROCESO";
	private static final String COD_POSTULANTE = "E_C_COD_POSTULANTE";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetEvaluacionFinal(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(COD_PROCESO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(COD_POSTULANTE, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new PostulanteMapper()));
		compile();
	}
	
	public Map execute(String codProceso, String codPostulante) {
		
		log.info("**** INI " + SPROC_NAME + "****");
    	log.info("COD_PROCESO:"+ codProceso);
    	log.info("COD_POSTULANTE:"+ codPostulante);  
    	log.info("**** FIN " + SPROC_NAME + "****");
		
		Map inputs = new HashMap();		
		inputs.put(COD_PROCESO, codProceso);
		inputs.put(COD_POSTULANTE, codPostulante);
		return super.execute(inputs);	
	}
	
	final class PostulanteMapper implements RowMapper {
		
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
			Evaluacion obj = new Evaluacion();
		
			obj.setCodCalificacionFinEtapa(rs.getString("CALIFICACION") == null ? "" : rs.getString("CALIFICACION").trim());
			obj.setComentarioFinEtapa(rs.getString("COMENTARIO") == null ? "" : rs.getString("COMENTARIO").trim());
			/*Usamos los siguiente atributos para rescatar los datos del archivo adjunto.*/
			obj.setCodCalificaion(rs.getString("IFORME") == null ? "" : rs.getString("IFORME").trim());
			obj.setComentario(rs.getString("INFORME_GENERADO") == null ? "" : rs.getString("INFORME_GENERADO").trim());
			return obj;
		}
	}
}
