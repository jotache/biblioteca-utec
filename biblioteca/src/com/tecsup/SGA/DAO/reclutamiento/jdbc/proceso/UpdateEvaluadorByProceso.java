package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateEvaluadorByProceso extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateEvaluadorByProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
										+ ".PKG_RECL_PROCESO.SP_ACT_EVALUADORES";

	private static final String EVAL_ID = "E_C_EVAL_ID";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String EVAL_CODTIPOEVALUACION = "E_C_EVAL_CODTIPOEVALUACION";
	private static final String EVAL_EVALUADOR = "E_C_EVAL_EVALUADOR";
	private static final String CODUSUARIO = "E_V_CODUSUARIO";
	private static final String RETVAL = "S_V_RETVAL";
	
	public UpdateEvaluadorByProceso(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(EVAL_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAL_CODTIPOEVALUACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAL_EVALUADOR, OracleTypes.CHAR));
		declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		
		compile();
	}
	
	public Map execute(String codRegEval, String codProceso, String codTipoEvaluacion, String codEvaluador
					, String usuario) {	
		
		log.info("**** INI:"+SPROC_NAME + " *****");
		log.info("EVAL_ID:"+codRegEval);
		log.info("PROC_ID:"+codProceso);
		log.info("EVAL_CODTIPOEVALUACION:"+codTipoEvaluacion);
		log.info("EVAL_EVALUADOR:"+codEvaluador);
		log.info("CODUSUARIO:"+usuario);
		log.info("**** FIN:"+SPROC_NAME + " *****");
		
		Map inputs = new HashMap();

		inputs.put(EVAL_ID, codRegEval);
		inputs.put(PROC_ID, codProceso);
		inputs.put(EVAL_CODTIPOEVALUACION, codTipoEvaluacion);
		inputs.put(EVAL_EVALUADOR, codEvaluador);
		inputs.put(CODUSUARIO, usuario);

		return super.execute(inputs);
	}
}
