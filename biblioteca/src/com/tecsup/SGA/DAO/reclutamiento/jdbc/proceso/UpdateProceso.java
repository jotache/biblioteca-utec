package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateProceso extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
								+ ".PKG_RECL_PROCESO.SP_ACT_PROCESO";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String PROC_CODETAPA = "E_C_PROC_CODETAPA";
	private static final String PROC_IDPROCESO = "E_C_PROC_IDPROCESO";
	private static final String PROC_DENOMINACION = "E_V_PROC_DENOMINACION";
	private static final String PROC_CODUNIDAD = "E_C_PROC_CODUNIDAD";
	private static final String PROC_CODAREA = "E_C_PROC_CODAREA";
	private static final String NRO_REGISTROS = "E_V_NRO_REGISTROS";
	private static final String PROC_CODPROFESION = "E_C_PROC_CODPROFESION";
	private static final String PROC_CODINSTITUCION = "E_C_PROC_CODINSTITUCION";
	private static final String PROC_CODGRADO = "E_C_PROC_CODGRADO";
	private static final String PROC_PRETENSION_INI = "E_V_PROC_PRETENSION_INI";
	private static final String PROC_PRETENSION_FIN = "E_V_PROC_PRETENSION_FIN";
	private static final String PROC_CODSIMBOLO = "E_V_PROC_CODSIMBOLO";
	private static final String PROC_EDAD = "E_N_PROC_EDAD";
	private static final String PROC_CODSIMBOLO1 = "E_V_PROC_CODSIMBOLO1";
	private static final String PROC_EDAD1 = "E_N_PROC_EDAD1";
	private static final String PROC_SEXO = "E_C_PROC_SEXO";
	private static final String PROC_CODDEDICACION = "E_C_PROC_CODDEDICACION";
	private static final String PROC_CODINTERESADOEN = "E_C_PROC_CODINTERESADOEN";
	private static final String PROC_DISPONIBVIAJAR = "E_C_PROC_DISPONIBVIAJAR";
	private static final String PROC_ANIOSEXPLAB = "E_C_PROC_ANIOSEXPLAB";
	private static final String PROC_ANIOSEXPDOC = "E_C_PROC_ANIOSEXPDOC";
	private static final String PROC_USU_MODI = "E_V_PROC_USU_MODI";
	private static final String E_C_COD_MONEDA = "E_C_COD_MONEDA";
	private static final String E_C_PROC_POSTULA= "E_C_PROC_POSTULA";	
	private static final String E_V_CODAREAS_ESTUDIOS = "E_V_CODAREAS_ESTUDIOS";
	private static final String E_V_NROAREAS_ESTUDIOS = "E_V_NROAREAS_ESTUDIOS";
	private static final String E_V_CODAREAS_NIVELEST = "E_V_CODAREAS_NIVELEST";
	private static final String E_V_NROAREAS_NIVELEST = "E_V_NROAREAS_NIVELEST";
	private static final String E_V_CODINSTEDU = "E_V_CODINSTEDU";
	private static final String E_V_NROINSTEDU = "E_V_NROINSTEDU";
	
	private static final String RETVAL = "S_V_RETVAL";
	
	public UpdateProceso(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODETAPA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_IDPROCESO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_DENOMINACION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(PROC_CODUNIDAD, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODAREA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(NRO_REGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(PROC_CODPROFESION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODINSTITUCION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODGRADO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_PRETENSION_INI, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(PROC_PRETENSION_FIN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(PROC_CODSIMBOLO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_EDAD, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODSIMBOLO1, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_EDAD1, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_SEXO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODDEDICACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODINTERESADOEN, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_DISPONIBVIAJAR, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_ANIOSEXPLAB, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_ANIOSEXPDOC, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_USU_MODI, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_MONEDA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_PROC_POSTULA, OracleTypes.CHAR));	
		declareParameter(new SqlParameter(E_V_CODAREAS_ESTUDIOS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROAREAS_ESTUDIOS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODAREAS_NIVELEST, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROAREAS_NIVELEST, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODINSTEDU, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROINSTEDU, OracleTypes.VARCHAR));				
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String CodProceso, String codEtapa,String codProcesoRel,String descripcion,String codUnidadFuncional
			,String codAreaInteres, String nroAreaInteres, String codAreaEstudio,String codInstAcademica,String codGradoAcademico
			,String sueldoIni,String sueldoFin,String codEdad,String edad,String codSexo,String codDedicacion
			,String codINteresadoEn,String flagDispViajar,String anhosExpLab,String anhosExpDoc,String codUsuCrea
			,String codEdad1, String edad1, String codTipoMoneda,String puestoPostula
			,String codAreasEstudios, String nroAreaEstudios
			,String codAreasNivEstudios, String nroAreaNivEstudios
			,String codInstEduc,String nroInstEduc)
	{
		
		log.info("**** INI:"+SPROC_NAME + " *****");
		log.info("CodProceso:"+CodProceso);
		log.info("codEtapa:"+codEtapa);
		log.info("codProcesoRel:"+codProcesoRel);
		log.info("descripcion:"+descripcion);
		log.info("codUnidadFuncional:"+codUnidadFuncional);
		log.info("codAreaInteres:"+codAreaInteres);
		log.info("nroAreaInteres:"+nroAreaInteres);
		log.info("codAreaEstudio:"+codAreaEstudio);
		log.info("codInstAcademica:"+codInstAcademica);
		log.info("codGradoAcademico:"+codGradoAcademico);
		log.info("sueldoIni:"+sueldoIni);
		log.info("sueldoFin:"+sueldoFin);
		log.info("codEdad:"+codEdad);
		log.info("edad:"+edad);
		log.info("codEdad1:"+codEdad1);
		log.info("edad1:"+edad1);
		log.info("codSexo:"+codSexo);
		log.info("codDedicacion:"+codDedicacion);
		log.info("codINteresadoEn:"+codINteresadoEn);
		log.info("flagDispViajar:"+flagDispViajar);
		log.info("anhosExpLab:"+anhosExpLab);
		log.info("anhosExpDoc:"+anhosExpDoc);
		log.info("codUsuCrea:"+codUsuCrea);		
		log.info("codTipoMoneda:"+codTipoMoneda);
		log.info("puestoPostula:"+puestoPostula);
		log.info("E_V_CODAREAS_ESTUDIOS:"+codAreasEstudios);
		log.info("E_V_NROAREAS_ESTUDIOS:"+nroAreaEstudios);
		log.info("E_V_CODAREAS_NIVELEST:"+codAreasNivEstudios);
		log.info("E_V_NROAREAS_NIVELEST:"+nroAreaNivEstudios);		
		log.info("E_V_CODINSTEDU:"+codInstEduc);
		log.info("E_V_NROINSTEDU:"+nroInstEduc);				
		log.info("**** FIN:"+SPROC_NAME + " *****");
	
		Map inputs = new HashMap();
		
		inputs.put(PROC_ID, CodProceso);
		inputs.put(PROC_CODETAPA, codEtapa);
		inputs.put(PROC_IDPROCESO, codProcesoRel);
		inputs.put(PROC_DENOMINACION, descripcion);
		inputs.put(PROC_CODUNIDAD, codUnidadFuncional);
		inputs.put(PROC_CODAREA, codAreaInteres);
		inputs.put(NRO_REGISTROS, nroAreaInteres);
		inputs.put(PROC_CODPROFESION, codAreaEstudio);
		inputs.put(PROC_CODINSTITUCION, codInstAcademica);
		inputs.put(PROC_CODGRADO, codGradoAcademico);
		inputs.put(PROC_PRETENSION_INI, sueldoIni);
		inputs.put(PROC_PRETENSION_FIN, sueldoFin);
		inputs.put(PROC_CODSIMBOLO, codEdad);
		inputs.put(PROC_EDAD, edad);
		inputs.put(PROC_CODSIMBOLO1, codEdad1);
		inputs.put(PROC_EDAD1, edad1);
		inputs.put(PROC_SEXO, codSexo);
		inputs.put(PROC_CODDEDICACION, codDedicacion);
		inputs.put(PROC_CODINTERESADOEN, codINteresadoEn);
		inputs.put(PROC_DISPONIBVIAJAR, flagDispViajar);
		inputs.put(PROC_ANIOSEXPLAB, anhosExpLab);
		inputs.put(PROC_ANIOSEXPDOC, anhosExpDoc);
		inputs.put(PROC_USU_MODI, codUsuCrea);
		inputs.put(E_C_COD_MONEDA, codTipoMoneda);
		inputs.put(E_C_PROC_POSTULA, puestoPostula);
		inputs.put(E_V_CODAREAS_ESTUDIOS, codAreasEstudios);
		inputs.put(E_V_NROAREAS_ESTUDIOS, nroAreaEstudios);		
		inputs.put(E_V_CODAREAS_NIVELEST, codAreasNivEstudios);
		inputs.put(E_V_NROAREAS_NIVELEST, nroAreaNivEstudios);					
		inputs.put(E_V_CODINSTEDU, codInstEduc);
		inputs.put(E_V_NROINSTEDU, nroInstEduc);
		return super.execute(inputs);
	
	}
}
