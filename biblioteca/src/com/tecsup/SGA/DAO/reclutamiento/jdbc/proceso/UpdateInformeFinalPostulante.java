package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateInformeFinalPostulante extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateInformeFinalPostulante.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
								+ ".PKG_RECL_PROCESO.SP_ACT_INFORME_FINAL_SEL";
	
	private static final String POST_ID = "E_C_POST_ID";
	private static final String ARCHIVOADJ = "E_V_ARCHIVOADJ";
	private static final String ARCHIVOADJ_GEN = "E_V_ARCHIVOADJ_GEN";
	private static final String CODUSUARIO = "E_V_CODUSUARIO";
	private static final String RETVAL = "S_V_RETVAL";
	
	public UpdateInformeFinalPostulante(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(ARCHIVOADJ, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(ARCHIVOADJ_GEN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codPostulante
				, String archivo
				,String archivoGen
				,String codUsuario)
	{
	
		log.info("**** INI:"+SPROC_NAME + " *****");
		log.info("POST_ID:"+codPostulante);
		log.info("ARCHIVOADJ:"+archivo);
		log.info("ARCHIVOADJ_GEN:"+archivoGen);
		log.info("CODUSUARIO:"+codUsuario);
		log.info("**** FIN:"+SPROC_NAME + " *****");
		
		Map inputs = new HashMap();
		
		inputs.put(POST_ID, codPostulante);
		inputs.put(ARCHIVOADJ, archivo);
		inputs.put(ARCHIVOADJ_GEN, archivoGen);
		inputs.put(CODUSUARIO, codUsuario);
	
		return super.execute(inputs);
	
	}
}
