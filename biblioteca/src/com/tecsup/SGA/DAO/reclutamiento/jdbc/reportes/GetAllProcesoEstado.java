package com.tecsup.SGA.DAO.reclutamiento.jdbc.reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReporteReclutamiento;

public class GetAllProcesoEstado extends StoredProcedure  {
	private static Log log = LogFactory.getLog(GetAllProcesoEstado.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
	".pkg_recl_reportes.sp_sel_proceso_estado";
	
	private static final String E_V_COD_TIP_PROCESO = "E_V_COD_TIP_PROCESO";
	private static final String E_V_COD_UNI_FUNCIONAL = "E_V_COD_UNI_FUNCIONAL";
	private static final String E_V_DSC_PROCESO = "E_V_DSC_PROCESO";
	private static final String E_V_FEC_INI = "E_V_FEC_INI";
	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllProcesoEstado(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_COD_TIP_PROCESO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_COD_UNI_FUNCIONAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DSC_PROCESO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
	compile();
	}
	
	public Map execute(String codTipoProceso, String codUniFunci, String dscProceso,String fecIni,
			String fecFin) {
	
		log.info("**** INI " + SPROC_NAME + " ****");
		log.info("E_V_COD_TIP_PROCESO:" +codTipoProceso);
		log.info("E_V_COD_UNI_FUNCIONAL:" +codUniFunci);
		log.info("E_V_DSC_PROCESO:" +dscProceso);
		log.info("E_V_FEC_INI:" +fecIni);
		log.info("E_V_FEC_FIN:" +fecFin);
		log.info("**** INI " + SPROC_NAME + " ****");
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_COD_TIP_PROCESO, codTipoProceso);
	inputs.put(E_V_COD_UNI_FUNCIONAL, codUniFunci);
	inputs.put(E_V_DSC_PROCESO, dscProceso);
	inputs.put(E_V_FEC_INI, fecIni);
	inputs.put(E_V_FEC_FIN, fecFin);
	
	return super.execute(inputs);
	
	}
	
	final class ProcesoMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	
	ReporteReclutamiento proceso = new ReporteReclutamiento();
	
	proceso.setCodProceso(rs.getString("COD_PROCESO") == null ? "" : rs.getString("COD_PROCESO"));
	proceso.setDscTipoProceso(rs.getString("DSC_TIPO_PROCESO") == null ? "" : rs.getString("DSC_TIPO_PROCESO"));
	proceso.setDscProceso(rs.getString("NMB_PROCESO") == null ? "" : rs.getString("NMB_PROCESO"));
	proceso.setDscUniFuncional(rs.getString("DSC_UNIDAD_FUNVIONAL") == null ? "" : rs.getString("DSC_UNIDAD_FUNVIONAL"));
	proceso.setDscEstado(rs.getString("DSC_ESTADO") == null ? "" : rs.getString("DSC_ESTADO"));
	proceso.setFecInicio(rs.getString("FEC_INICIO") == null ? "" : rs.getString("FEC_INICIO"));
	proceso.setFecFin(rs.getString("FEC_FIN") == null ? "" : rs.getString("FEC_FIN"));
	proceso.setNumPostulantes(rs.getString("NUM_POSTULANTES") == null ? "" : rs.getString("NUM_POSTULANTES"));
	proceso.setNumSeleccionados(rs.getString("NUM_SELECCIONADOS") == null ? "" : rs.getString("NUM_SELECCIONADOS"));
	proceso.setIndTieneEncuesta(rs.getString("IND_TIENE_OFERTA") == null ? "" : rs.getString("IND_TIENE_OFERTA"));
	
	return proceso;
	}

}

}
