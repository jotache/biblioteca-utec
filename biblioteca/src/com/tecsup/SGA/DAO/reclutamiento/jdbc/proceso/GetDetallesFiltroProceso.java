package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetDetallesFiltroProceso extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetDetallesFiltroProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_proceso.SP_SEL_DETALLESFILTRO_PROCESO";
	private static final String E_C_COD_PROCESO = "E_C_COD_PROCESO";
	private static final String E_C_TIPO = "E_C_TIPO";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetDetallesFiltroProceso(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_PROCESO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new DetallesMapper()));
		compile();		
	}
	
	public Map execute(String codProceso,String tipo) {	
		
		log.info("**** INI " + SPROC_NAME + "****");
    	log.info("E_C_COD_PROCESO:"+ codProceso);
    	log.info("E_C_TIPO:"+ tipo);  
    	log.info("**** FIN " + SPROC_NAME + "****");
		
		Map inputs = new HashMap();		
		inputs.put(E_C_COD_PROCESO, codProceso);
		inputs.put(E_C_TIPO, tipo);
		return super.execute(inputs);	
	}
	
	final class DetallesMapper implements RowMapper{

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
			
			tipoTablaDetalle.setCodTipoTablaDetalle(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO").trim());
			tipoTablaDetalle.setDescripcion(rs.getString("NOMBRE") == null ? "" : rs.getString("NOMBRE").trim());
						
			return tipoTablaDetalle;
		}
		
	}
}
