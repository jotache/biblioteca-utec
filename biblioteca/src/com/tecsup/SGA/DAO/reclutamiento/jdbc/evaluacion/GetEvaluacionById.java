package com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Evaluacion;

public class GetEvaluacionById extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetEvaluacionById.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
									".pkg_recl_proceso.SP_SEL_EVALUACION_BY_ID";
	private static final String EVALUACION_ID = "E_V_EVALUACION_ID";
	private static final String RECORDSET = "S_C_RECORDSET";
	
    public GetEvaluacionById(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(EVALUACION_ID, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new EvaluacionMapper()));
        compile();
    }
    
    public Map execute(String codEvaluacion) {    
    	
    	log.info("*** INI "+ SPROC_NAME + "***");
    	log.info("EVALUACION_ID:"+codEvaluacion);
    	log.info("*** INI "+ SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();    	
        inputs.put(EVALUACION_ID, codEvaluacion);        
        return super.execute(inputs);
    }
    
    final class EvaluacionMapper implements RowMapper {        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Evaluacion obj = new Evaluacion();
        	
        	obj.setCodUsuario(rs.getString("COD_USUARIO") == null ? "" : rs.getString("COD_USUARIO"));
        	obj.setCodCalificaion(rs.getString("COD_CALIFICACION") == null ? "" : rs.getString("COD_CALIFICACION"));
        	obj.setCodEstadoEvento(rs.getString("COD_ESTADO_EVENTO") == null ? "" : rs.getString("COD_ESTADO_EVENTO"));
        	obj.setComentario(rs.getString("COMENTARIO") == null ? "" : rs.getString("COMENTARIO"));
        	obj.setCodCalificacionFinEtapa(rs.getString("COD_CALIFICACION_FINAL") == null ? "" : rs.getString("COD_CALIFICACION_FINAL"));
        	obj.setComentarioFinEtapa(rs.getString("COMENTARIO_FINAL") == null ? "" : rs.getString("COMENTARIO_FINAL"));
        	
        	//NOMBREEVALUADOR
        	obj.setNombreEvaluador(rs.getString("NOMBREEVALUADOR") == null ? "" : rs.getString("NOMBREEVALUADOR"));
            //PROC_ID
        	obj.setCodProceso(rs.getString("PROC_ID"));
        	
        	//NOMBREPROCESO
        	obj.setNombreProceso(rs.getString("NOMBREPROCESO"));
        	
        	obj.setDesTipoEvaluacion(rs.getString("NOMBREEVALUACION"));
        	return obj;
        }
    }

}
