package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;

public class GetAllEvaluacionesByPostulante extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllEvaluacionesByPostulante.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
									+ ".PKG_RECL_PROCESO.SP_SEL_EVAL_X_POSTULANTE";
	
	private static final String COD_ETAPA = "E_C_COD_ETAPA";
	private static final String COD_PROCESO = "E_C_COD_PROCESO";
	private static final String COD_POSTULANTE = "E_C_COD_POSTULANTE";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllEvaluacionesByPostulante(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(COD_ETAPA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(COD_PROCESO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(COD_POSTULANTE, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new PostulanteMapper()));
		compile();
	}
	
	public Map execute(String codEtapa, String codProceso, String codPostulante) {
		
		log.info("*** INI "  + SPROC_NAME + " ****");
		log.info("COD_ETAPA:"+ codEtapa);
		log.info("COD_PROCESO:"+ codProceso);
		log.info("COD_POSTULANTE:"+ codPostulante);
		log.info("*** FIN "  + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();		
		inputs.put(COD_ETAPA, codEtapa);		
		inputs.put(COD_PROCESO, codProceso);
		inputs.put(COD_POSTULANTE, codPostulante);
		return super.execute(inputs);	
	}
	
	final class PostulanteMapper implements RowMapper {
		
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
			EvaluacionesByEvaluadorBean obj = new EvaluacionesByEvaluadorBean();
		
			obj.setCodPostulante(rs.getString("COD_USUARIO") == null ? "" : rs.getString("COD_USUARIO").trim());
			obj.setNomPostulante(rs.getString("NOMBRE_USUARIO") == null ? "" : rs.getString("NOMBRE_USUARIO").trim());
			obj.setCodTipoEvaluacion(rs.getString("COD_TIPO_EVALUACION") == null ? "" : rs.getString("COD_TIPO_EVALUACION").trim());
			obj.setDscTipoEvaluacion(rs.getString("TIPO_EVALUACION") == null ? "" : rs.getString("TIPO_EVALUACION").trim());
			obj.setCodCalificacion(rs.getString("COD_CALIFICACION") == null ? "" : rs.getString("COD_CALIFICACION").trim());
			obj.setDscCalificacion(rs.getString("CALIFICACION") == null ? "" : rs.getString("CALIFICACION").trim());
			obj.setComentario(rs.getString("COMENTARIO") == null ? "" : rs.getString("COMENTARIO").trim());
			
			return obj;
		}
	}
}
