package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Proceso;

public class GetAllProcesos extends StoredProcedure  {
	private static Log log = LogFactory.getLog(GetAllProcesos.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
											".pkg_recl_proceso.sp_sel_proceso";
    private static final String PROC_CODETAPA = "E_C_PROC_CODETAPA";
    private static final String PROC_CODUNIDAD = "E_C_PROC_CODUNIDAD";
    private static final String PROC_FEC_INI = "E_C_PROC_FEC_INI";
    private static final String PROC_FEC_FIN = "E_C_PROC_FEC_FIN";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllProcesos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(PROC_CODETAPA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(PROC_CODUNIDAD, OracleTypes.CHAR));
        declareParameter(new SqlParameter(PROC_FEC_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(PROC_FEC_FIN, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codEtapa, String codUnidad, String fecIni, String fecFin) {
    	
    	log.info("*** INI "+SPROC_NAME+" ***");		
		log.info("PROC_CODETAPA:"+codEtapa);
		log.info("PROC_CODUNIDAD:"+codUnidad);
		log.info("PROC_FEC_INI:"+fecIni);
		log.info("PROC_FEC_FIN:"+fecFin);
		log.info("*** FIN "+SPROC_NAME+" ***");
		
    	Map inputs = new HashMap();
    	
        inputs.put(PROC_CODETAPA, codEtapa);
        inputs.put(PROC_CODUNIDAD, codUnidad);
        inputs.put(PROC_FEC_INI, fecIni);
        inputs.put(PROC_FEC_FIN, fecFin);
        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Proceso proceso = new Proceso();
        	
        	proceso.setCodProceso(rs.getString("CodigoProceso"));
        	proceso.setDscProceso(rs.getString("DescripcionProceso"));
        	proceso.setCodUnidadFuncional(rs.getString("CodigoUnidadFuncional"));
        	proceso.setDscUnidadFuncional(rs.getString("DescripcionUnidadFunc") == null ? "&nbsp;" : rs.getString("DescripcionUnidadFunc"));
        	proceso.setFecRegistro(rs.getString("FechaRegistro"));
        	proceso.setCodOfertaAsociada(rs.getString("CodigoOferta") == null ? "&nbsp;" : rs.getString("CodigoOferta"));
        	proceso.setDscOfertaAsociada(rs.getString("DescripcionOferta") == null ? "&nbsp;" : rs.getString("DescripcionOferta"));
       	
            return proceso;
        }

    }
}
