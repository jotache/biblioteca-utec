package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Recluta;


public class GetReclutaExpLaboral extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReclutaExpLaboral.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_sel_postulante_explab";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetReclutaExpLaboral(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ExpLaboralMapper()));
        compile();
    }

    public Map execute(String idRec) {
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_ID:"+idRec);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();    	
        inputs.put(POST_ID, idRec);
        
        return super.execute(inputs);

    }
    
    final class ExpLaboralMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Recluta recluta = new Recluta();
        	
        	recluta.setOrganizacion(rs.getString("EMPRESA"));
        	recluta.setPuesto(rs.getString("CODPUESTO"));
        	recluta.setOtroPuesto(rs.getString("OTROPUESTO"));
        	recluta.setFecIni(rs.getString("FECHAINICIO"));
        	recluta.setFecFin(rs.getString("FECHAFIN"));
        	recluta.setReferencia(rs.getString("REFERENCIA"));
        	recluta.setTelefRef(rs.getString("TELEFCONTACTO"));
        	recluta.setFunciones(rs.getString("FUNCIONES"));
        	recluta.setCodExpLaboral(rs.getString("CODEXPLABORAL"));
        	
            return recluta;
        }
    }
}
