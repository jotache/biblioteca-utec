package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Proceso;

public class GetAllProcesoSeleccion extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAllProcesoSeleccion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
											".pkg_recl_proceso.SP_SEL_PROCESO_REVISION";
	private static final String E_C_COD_ESTADO = "E_C_COD_ESTADO";
	private static final String E_C_COD_PROCESO = "E_C_COD_PROCESO";
	private static final String RECORDSET = "S_C_RECORDSET";
	
    public GetAllProcesoSeleccion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_ESTADO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_PROCESO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codEstado, String codProceso) {
    	Map inputs = new HashMap();    
    	
    	log.info("**** INI " + SPROC_NAME + "****");
    	log.info("E_C_COD_ESTADO:"+ codEstado);
    	log.info("E_C_COD_PROCESO:"+ codProceso);
    	log.info("**** FIN " + SPROC_NAME + "****");
    	
        inputs.put(E_C_COD_ESTADO, codEstado);        
        inputs.put(E_C_COD_PROCESO, codProceso);
        return super.execute(inputs);
    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Proceso proceso = new Proceso();
        	
        	proceso.setCodProceso(rs.getString("COD_PROCESO"));
        	proceso.setDscProceso(rs.getString("PROC_DENOMINACION"));
       	
            return proceso;
        }

    }
}
