package com.tecsup.SGA.DAO.reclutamiento.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.jdbc.comun.GetUsuariosByTipo;
import com.tecsup.SGA.DAO.reclutamiento.ProcesoDAO;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.oferta.GetOfertaById;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.oferta.InsertOferta;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.oferta.UpdateOferta;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.CargarPostulantesXProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.DeleteProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetAllPostulanteByProc;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetAllProcesos;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetAreasInteresByProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetDetallesFiltroProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetEvaluadorByProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetProcesoById;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetTrazaEvaluacionesPost;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.InsertEvaluadorByProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.InsertPostulanteByProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.InsertProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.UpdateEvaluadorByProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.UpdateProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.DeletePostulanteByProceso;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetAllEvaluacionesByPostulante;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetAllProcesoSeleccion;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetEvaluacionFinal;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.UpdateInformeFinalPostulante;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.oferta.*;

import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Oferta;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.Evaluacion;
import com.tecsup.SGA.common.CommonConstants;

public class ProcesoDAOJdbc extends JdbcDaoSupport implements ProcesoDAO {
	private static Log log = LogFactory.getLog(ProcesoDAOJdbc.class);
	public List getAllProceso(String unidadFuncional, String fecIni,
			String fecFin, String tipoEtapa) {
		
		GetAllProcesos getAllProcesos = new GetAllProcesos(this.getDataSource());
		HashMap outputs = (HashMap)getAllProcesos.execute(tipoEtapa, unidadFuncional, fecIni, fecFin);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String insertProceso(String codEtapa, String codProcesoRel,
			String descripcion, String codUnidadFuncional,
			String codAreaInteres, String nroAreaInteres, String codAreaEstudio,
			String codInstAcademica, String codGradoAcademico,
			String sueldoIni, String sueldoFin, String codEdad, String edad,
			String codSexo, String codDedicacion, String codINteresadoEn,
			String flagDispViajar, String anhosExpLab, String anhosExpDoc,
			String codUsuCrea, String codEdad1, String edad1, String codTipoMoneda,String puestoPostula,
			String codAreasEstudios, String nroAreaEstudios,
			String codAreasNivEstudios, String nroAreaNivEstudios,
			String codInstEduc,String nroInstEduc) {
		
		
		InsertProceso insertProceso = new InsertProceso(this.getDataSource());
		HashMap outputs = (HashMap)insertProceso.execute(codEtapa, codProcesoRel,
				descripcion, codUnidadFuncional, codAreaInteres, nroAreaInteres, codAreaEstudio,
				codInstAcademica, codGradoAcademico, sueldoIni, sueldoFin, codEdad, edad,
				codSexo, codDedicacion, codINteresadoEn, flagDispViajar, anhosExpLab, anhosExpDoc,
				codUsuCrea, codEdad1, edad1, codTipoMoneda,puestoPostula,
				codAreasEstudios,nroAreaEstudios,
				codAreasNivEstudios, nroAreaNivEstudios,
				codInstEduc,nroInstEduc);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	
	public String updateProceso(String CodProceso, String codEtapa,
			String codProcesoRel, String descripcion,
			String codUnidadFuncional, String codAreaInteres, String nroAreaInteres,
			String codAreaEstudio, String codInstAcademica,
			String codGradoAcademico, String sueldoIni, String sueldoFin,
			String codEdad, String edad, String codSexo, String codDedicacion,
			String codINteresadoEn, String flagDispViajar, String anhosExpLab,
			String anhosExpDoc, String codUsuCrea, String codEdad1, String edad1, String codTipoMoneda,String puestoPostula,
			String codAreasEstudios, String nroAreaEstudios,
			String codAreasNivEstudios, String nroAreaNivEstudios,
			String codInstEduc,String nroInstEduc) {
		
		UpdateProceso updateProceso = new UpdateProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)updateProceso.execute(CodProceso, codEtapa, codProcesoRel,
				descripcion, codUnidadFuncional, codAreaInteres, nroAreaInteres, codAreaEstudio,
				codInstAcademica, codGradoAcademico, sueldoIni, sueldoFin, codEdad, edad,
				codSexo, codDedicacion, codINteresadoEn, flagDispViajar, anhosExpLab, anhosExpDoc,
				codUsuCrea, codEdad1, edad1, codTipoMoneda,puestoPostula,
				codAreasEstudios,nroAreaEstudios,
				codAreasNivEstudios, nroAreaNivEstudios,
				codInstEduc,nroInstEduc);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
		
	public Proceso getProceso(String idProceso) {
		
		GetProcesoById getProcesoById = new GetProcesoById(this.getDataSource());
		
		HashMap outputs = (HashMap)getProcesoById.execute(idProceso);
		
		if (!outputs.isEmpty())
		{
			ArrayList list = (ArrayList)outputs.get(CommonConstants.RET_CURSOR);
			if ( list.size() > 0 ) return (Proceso)list.get(0);  
			else return null;
		}
		return null;
	}
	
	public String deleteProceso(String codProceso, String usuario)
	{
		DeleteProceso deleteProceso = new DeleteProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)deleteProceso.execute(codProceso,usuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	
	public List getAreasInteresByProceso(String codProceso) {
		
		GetAreasInteresByProceso getAreasInteresByProceso = new GetAreasInteresByProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)getAreasInteresByProceso.execute(codProceso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public Oferta getOfertaById(String codOferta) {
		GetOfertaById getOfertaById = new GetOfertaById(this.getDataSource());
		
		HashMap outputs = (HashMap)getOfertaById.execute(codOferta);
		
		if (!outputs.isEmpty())
		{
			ArrayList list = (ArrayList)outputs.get(CommonConstants.RET_CURSOR);
			if ( list.size() > 0 ) return (Oferta)list.get(0);  
			else return null;
		}
		return null;
	}

	public String insertOferta(String codProceso, String fecIni, String fecFin,
			String descripcion, String denominacion, String nroVacantes,
			String usuCrea) {
		
		InsertOferta insertOferta = new InsertOferta(this.getDataSource());
		
		HashMap outputs = (HashMap)insertOferta.execute(codProceso, fecIni, fecFin,
				descripcion, denominacion, nroVacantes,
				usuCrea);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	
	public String updateOferta(String codOferta, String codProceso,
			String fecIni, String fecFin, String descripcion,
			String denominacion, String nroVacantes, String usuCrea) {
		UpdateOferta updateOferta = new UpdateOferta(this.getDataSource());
		
		HashMap outputs = (HashMap)updateOferta.execute(codOferta, codProceso, fecIni, fecFin,
				descripcion, denominacion, nroVacantes,
				usuCrea);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	
	public List getAllPostulantes(String codProceso, String codEtapa,
			String nombres, String apellidos,String orden) {

		GetAllPostulanteByProc getAllPostulanteByProc = new GetAllPostulanteByProc(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllPostulanteByProc.execute(codProceso,codEtapa,nombres,apellidos,orden);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;		
	}
	
	public String insertPostulanteByProceso(String codPostulantes,
			String nroRegistros, String codProc, String tipo) {
		
		InsertPostulanteByProceso insertPostulanteByProceso = new InsertPostulanteByProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)insertPostulanteByProceso.execute(codPostulantes, nroRegistros, codProc, tipo);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	//
	public List getUsuarioByTipo(String tipo, String sistema) {
		
		GetUsuariosByTipo getUsuariosByTipo = new GetUsuariosByTipo(this.getDataSource());
		
		HashMap outputs = (HashMap)getUsuariosByTipo.execute(tipo, sistema);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public String insertEvaluadorByProceso(String codProceso,
			String codTipoEvaluacion, String codEvaluador, String usuario) {
		
		InsertEvaluadorByProceso insertEvaluadorByProceso = new InsertEvaluadorByProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)insertEvaluadorByProceso.execute(codProceso, codTipoEvaluacion
																	, codEvaluador, usuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	
	public String updateEvaluadorByProceso(String codRegEval,
			String codProceso, String codTipoEvaluacion, String codEvaluador,
			String usuario) {
		UpdateEvaluadorByProceso updateEvaluadorByProceso = new UpdateEvaluadorByProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)updateEvaluadorByProceso.execute(codRegEval, codProceso, codTipoEvaluacion
											, codEvaluador, usuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	public List getEvaluadoresByProceso(String codProceso) {
		GetEvaluadorByProceso getEvaluadorByProceso = new GetEvaluadorByProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)getEvaluadorByProceso.execute(codProceso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}		
		return null;
	}
	
	public String deletePostulanteByProceso(String codPostulantes,
			String numreg, String codProceso, String tipo, String usuario) {
		DeletePostulanteByProceso deletePostulanteByProceso = new DeletePostulanteByProceso(this.getDataSource());
		
		HashMap outputs = (HashMap)deletePostulanteByProceso.execute(codPostulantes, numreg, codProceso, tipo, usuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}
	
	public List getAllEvaluacionByPostulante(String codEtapa, String codProceso,
			String codPostulante) {
		GetAllEvaluacionesByPostulante getAllEvaluacionesByPostulante = new GetAllEvaluacionesByPostulante(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEvaluacionesByPostulante.execute(codEtapa,codProceso,codPostulante);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List getAllProcesosParaRevision(String codEstado, String codProceso) {
		GetAllProcesoSeleccion getAllProcesoSeleccion = new GetAllProcesoSeleccion(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProcesoSeleccion.execute(codEstado, codProceso);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public Evaluacion getCalificacionFinal(String codProceso, String codPostulante)
	{
		GetEvaluacionFinal getEvaluacionFinal = new GetEvaluacionFinal(this.getDataSource());
		Evaluacion evaluacion = null;
		ArrayList<Evaluacion> arr;
		
		HashMap outputs = (HashMap)getEvaluacionFinal.execute(codProceso, codPostulante);
		
		if (!outputs.isEmpty())
		{
			arr = (ArrayList<Evaluacion>)outputs.get(CommonConstants.RET_CURSOR);
			if ( arr != null )
				if ( arr.size() > 0)				
				{
					evaluacion = new Evaluacion();
					evaluacion = arr.get(0);
				}
		}
		
		return evaluacion;
	}
	
	public String updateInformeFinalPostulante(String codPostulante
			, String archivo
			,String archivoGen
			,String codUsuario) {
		UpdateInformeFinalPostulante updateInformeFinalPostulante = new UpdateInformeFinalPostulante(this.getDataSource());
		
		HashMap outputs = (HashMap)updateInformeFinalPostulante.execute(codPostulante, archivo
																	, archivoGen, codUsuario);
		
		if (!outputs.isEmpty())
		{
			return (String)outputs.get(CommonConstants.RET_STRING);
		}

		return null;
	}

	public String cargarPostulantesXProceso(String codProceso, String usuario) {
		CargarPostulantesXProceso cargarPost = new CargarPostulantesXProceso(this.getDataSource());		
		HashMap outputs = (HashMap)cargarPost.execute(codProceso,usuario);
		if (!outputs.isEmpty())		
			return (String)outputs.get(CommonConstants.RET_STRING);
		
		return null;
	}

	public List getTrazaEvaluaciones(String codProceso, String codPostulante) {
		GetTrazaEvaluacionesPost getDatos = new GetTrazaEvaluacionesPost(this.getDataSource());
		HashMap outputs = (HashMap)getDatos.execute(codProceso, codPostulante);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List getDetallesFiltroProceso(String codProceso, String tipo) {
		GetDetallesFiltroProceso detalles = new GetDetallesFiltroProceso(this.getDataSource());
		HashMap outputs = (HashMap) detalles.execute(codProceso,tipo);
		if (!outputs.isEmpty()){
			return (List) outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List<Evaluador> getEvaluadores(String codProceso,String codTipoEvaluacion, String etapa) {
		
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		log.info("getEvaluadores.getJdbcTemplate()");
		String sql = ""  +
			"select e.eval_id as id,e.proc_id as idproceso,e.eval_evaluador as codevaluador," +
			" e.eval_codtipoevaluacion as tipoevaluacion,general.nombrecliente(e.eval_evaluador) as nombre, " +
			" case upper(p.lugar) when 'L' then rtrim(p.correo)||'@tecsup.edu.pe'" +
			" when 'A' then rtrim(p.correo)||'@tecsup-aqp.edu.pe'" +
			" when 'T' then rtrim(p.correo)||'@tecsup.edu.pe' end as correo," +
			" d.ttde_descripcion as nometapa, d.ttde_valor3 as orden " +
			" from rec_evaluadores e, personal.per_empleado p , general.gen_tipo_tabla_detalle d where e.eval_evaluador=p.codempleado(+)" +
			" and d.ttde_valor1=? and d.tipt_id='0007' and e.eval_codtipoevaluacion=d.ttde_id" +
			" and e.proc_id=? and e.eval_codtipoevaluacion like '" + codTipoEvaluacion.trim() + "%' order by e.eval_id";
			
		log.info("Sql:" + sql);
		log.info("Paramaetros=> Etapa:" + etapa + " codProceso:"+codProceso);
		final Object[] args = 
				new Object[] { 
					etapa , 
					codProceso 
					};
			
		return jdbcTemplate.query(sql, args, new EvaludoarRowMapper());
	}

	public List<Evaluador> getEvaluadoresAsignadosXProceso(String codProceso) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		log.info("getEvaluadoresAsignadosXProceso.getJdbcTemplate()");
		String sql = " select e.eval_id AS id , e.proc_id AS idproceso , e.eval_codtipoevaluacion AS tipoevaluacion," +
					 " ttd1.ttde_descripcion AS nometapa , e.eval_evaluador AS codevaluador," +
					 " ttd1.ttde_valor3 as orden, eval.NombreEvaluador AS nombre," +
					 " '' as correo"+
					 " from rec_evaluadores e" +
					 " inner join general.gen_tipo_tabla_detalle ttd1 on e.eval_codtipoevaluacion = ttd1.ttde_id and ttd1.tipt_id = '0007'" +
					 " inner join rec_v_evaluador eval on eval.codevaluador = e.eval_evaluador" +
					 " where e.proc_id=? order by ttd1.ttde_valor3";
		
		log.info("Sql:" + sql);
		log.info("Paramaetros=> codProceso:" + codProceso);
		final Object[] args = 
			new Object[] { 				
				codProceso 
				};
				
		return jdbcTemplate.query(sql, args, new EvaludoarRowMapper());
	}
	
	final class EvaludoarRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			Evaluador evaluador = new Evaluador();
			evaluador.setCodEvaluador(rs.getString("codevaluador"));
			evaluador.setCodRegEvaluador(rs.getString("id"));
			evaluador.setDscEvaluador(rs.getString("nombre"));
			evaluador.setCodProceso(rs.getString("idproceso"));
			evaluador.setCorreo(rs.getString("correo")==null?"":rs.getString("correo"));
			evaluador.setCodTipoEvaluacion(rs.getString("tipoevaluacion"));
			evaluador.setDscTipoEvaluacion(rs.getString("nometapa")==null?"":rs.getString("nometapa"));
			evaluador.setOrdenEvaluacion(rs.getString("orden")==null?"":rs.getString("orden")); 
			return evaluador;
		}		
	}	
}
