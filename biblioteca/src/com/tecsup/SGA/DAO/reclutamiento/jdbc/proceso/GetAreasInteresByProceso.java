package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso.GetProcesoById.ProcesoMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAreasInteresByProceso extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAreasInteresByProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
											+ ".PKG_RECL_PROCESO.SP_SEL_AREAS_X_PROCESO";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAreasInteresByProceso(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AreasIntMapper()));
		compile();
	}

	public Map execute(String codProceso) {	
		
		log.info("**** INI " + SPROC_NAME + "****");
    	log.info("PROC_ID:"+ codProceso);    	
    	log.info("**** FIN " + SPROC_NAME + "****");
		
		Map inputs = new HashMap();		
		inputs.put(PROC_ID, codProceso);		
		return super.execute(inputs);	
	}

	final class AreasIntMapper implements RowMapper {
		
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
			TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
		
			tipoTablaDetalle.setCodTipoTablaDetalle(rs.getString("CODAREA") == null ? "" : rs.getString("CODAREA").trim());
			tipoTablaDetalle.setDescripcion(rs.getString("DEScripcionCAREA1") == null ? "" : rs.getString("DEScripcionCAREA1").trim());
			tipoTablaDetalle.setDscValor1(rs.getString("DESCRIPCIONAREA2") == null ? "" : rs.getString("DESCRIPCIONAREA2").trim());
			
			return tipoTablaDetalle;
		}
	}
}
