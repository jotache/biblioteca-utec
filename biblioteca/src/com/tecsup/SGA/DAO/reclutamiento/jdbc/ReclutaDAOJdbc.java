package com.tecsup.SGA.DAO.reclutamiento.jdbc;

import java.util.*; //ArrayList, HashMap, List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.reclutamiento.ReclutaDAO;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante.*;

import com.tecsup.SGA.modelo.Recluta;

public class ReclutaDAOJdbc extends JdbcDaoSupport implements ReclutaDAO{
	public String getAllRecluta(String email, String clave)
	{
		GetReclutaLogueo getAllReclutaLogueo = new GetReclutaLogueo(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllReclutaLogueo.execute(email, clave);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateReclutaClave(String email, String clave_anterior, String clave_nueva)
	{	
		UpdateReclutaClave updateReclutaClave = new UpdateReclutaClave(this.getDataSource());	
		
		HashMap outputs = (HashMap)updateReclutaClave.execute(email, clave_anterior, clave_nueva);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public Recluta getAllDatosPersonaRecluta(String idRec){
		
		GetReclutaDatosPersonales getAllDatosPersonaRecluta = new GetReclutaDatosPersonales(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDatosPersonaRecluta.execute(idRec);
		if (!outputs.isEmpty())
		{
			ArrayList lista = (ArrayList)outputs.get("S_C_RECORDSET");
			if (lista.size()>0){
				return (Recluta)lista.get(0);
			}
			else
				return null;
		}
		return null;
	}
	
	public String insertReclutaDatos(String nombre, String apepat, String apemat, String clave, String fecnac, 
									String dni, String ruc, String sexo, String estadoCivil, String codNacionalidad, 
									String email, String email1, String aniosExpLaboral, String direccion, String codDpto, 
									String codProv, String codDist, String pais, String codPostal, String telefCasa, 
									String telefAdicional, String telefCel, String codPostuladoAntes, String codTrabAntes,
									String expDocencia, String aniosExpDocencia, String familiaAntes, String familiaNombres, 
									String dispoViaje, String sedePref, String codInteresado, String pretension, String codDedicacion, 
									String codDispoTrab, String perfil, String codAreaInteres, String nroRegAreaInt,
									String codMoneda, String tipoPago,String puestoPostula)
	{	
		InsertReclutaDatos insertReclutaDatos = new InsertReclutaDatos(this.getDataSource());	
		
		HashMap outputs = (HashMap)insertReclutaDatos.execute(nombre, apepat, apemat, clave, fecnac, 
															 dni, ruc, sexo, estadoCivil, codNacionalidad, 
															 email, email1, aniosExpLaboral, direccion, codDpto, 
															 codProv, codDist, pais, codPostal, telefCasa, 
															 telefAdicional, telefCel, codPostuladoAntes, codTrabAntes,
															 expDocencia, aniosExpDocencia, familiaAntes, familiaNombres, dispoViaje, 
															 sedePref, codInteresado, pretension, codDedicacion, 
															 codDispoTrab, perfil, codAreaInteres, nroRegAreaInt, codMoneda, tipoPago, puestoPostula);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateReclutaDatos(String idRec, String nombre, String apepat, String apemat, String fecnac, 
			String dni, String ruc, String sexo, String estadoCivil, String codNacionalidad, 
			String email, String email1, String aniosExpLaboral, String direccion, String codDpto, 
			String codProv, String codDist, String pais, String codPostal, String telefCasa, 
			String telefAdicional, String telefCel, String codPostuladoAntes, String codTrabAntes,
			String expDocencia, String aniosExpDocencia, String familiaAntes, String familiaNombres, String dispoViaje, 
			String sedePref, String codInteresado, String pretension, String codDedicacion, 
			String codDispoTrab, String perfil, String codMoneda, String tipoPago,String codAreaInteres,String puestoPostula,String codUsuario){
		
		UpdateReclutaDatos updateReclutaDatos = new UpdateReclutaDatos(this.getDataSource());	
		
		HashMap outputs = (HashMap)updateReclutaDatos.execute(idRec, nombre, apepat, apemat, fecnac, 
															 dni, ruc, sexo, estadoCivil, codNacionalidad, 
															 email, email1, aniosExpLaboral, direccion, codDpto, 
															 codProv, codDist, pais, codPostal, telefCasa, 
															 telefAdicional, telefCel, codPostuladoAntes, codTrabAntes,
															 expDocencia, aniosExpDocencia, familiaAntes, familiaNombres, dispoViaje, 
															 sedePref, codInteresado, pretension, codDedicacion, 
															 codDispoTrab, perfil, codMoneda, tipoPago, codAreaInteres,puestoPostula,codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List getReclutaIdiomas(String idRec)
	{
		GetReclutaIdiomas getReclutaIdiomas = new GetReclutaIdiomas(this.getDataSource());
		
		HashMap outputs = (HashMap)getReclutaIdiomas.execute(idRec);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getReclutaSuperior(String idRec)
	{
		GetReclutaSuperior getReclutaSuperior = new GetReclutaSuperior(this.getDataSource());
		
		HashMap outputs = (HashMap)getReclutaSuperior.execute(idRec);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getReclutaExpLaboral(String idRec)
	{
		GetReclutaExpLaboral getReclutaExpLaboral = new GetReclutaExpLaboral(this.getDataSource());
		
		HashMap outputs = (HashMap)getReclutaExpLaboral.execute(idRec);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String insertReclutaExpLaboral(String idRec, String empresa, String codPuesto, String otroPuesto, String fecIni,
			String fecFin, String referencia, String telef, String funciones,String codUsuario){
		
		InsertReclutaExpLaboral insertReclutaExpLaboral = new InsertReclutaExpLaboral(this.getDataSource());
		
		HashMap outputs = (HashMap)insertReclutaExpLaboral.execute(idRec, empresa, codPuesto, otroPuesto, fecIni, fecFin, 
																	referencia, telef, funciones,codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateReclutaExpLaboral(String idRec, String codExpLab, String empresa, String codPuesto, String otroPuesto, String fecIni,
			String fecFin, String referencia, String telef, String funciones,String codUsuario){
		
		UpdateReclutaExpLaboral updateReclutaExpLaboral = new UpdateReclutaExpLaboral(this.getDataSource());
		
		HashMap outputs = (HashMap)updateReclutaExpLaboral.execute(idRec, codExpLab, empresa, codPuesto, otroPuesto, fecIni, fecFin, referencia, telef, funciones,codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateReclutaArchAdjunto(String idRec, String cv, String foto,String codUsuario){
		UpdateReclutaArchAdjunto updateReclutaAdjunto = new UpdateReclutaArchAdjunto(this.getDataSource());
		
		HashMap outputs = (HashMap)updateReclutaAdjunto.execute(idRec, cv, foto,codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateReclutaEstSec(String idRec, String colegio1, String colegio2, String anioIni1, String anioFin1,
			String anioIni2, String anioFin2,String codUsuario){
		
		UpdateReclutaEstSec updateReclutaEstSec = new UpdateReclutaEstSec(this.getDataSource());
		
		HashMap outputs = (HashMap)updateReclutaEstSec.execute(idRec, colegio1, colegio2, anioIni1, anioFin1, anioIni2, anioFin2,codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateReclutaIdiomas(String idRec, String cadena, String nroReg,String codUsuario){
		
		UpdateReclutaIdiomas updateReclutaIdiomas = new UpdateReclutaIdiomas(this.getDataSource());
		
		HashMap outputs = (HashMap)updateReclutaIdiomas.execute(idRec, cadena, nroReg,codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateReclutaEstSup(String idRec, String cadena, String nroReg,String codUsuario){
		
		UpdateReclutaEstSup updateReclutaEstSup = new UpdateReclutaEstSup(this.getDataSource());
		
		HashMap outputs = (HashMap)updateReclutaEstSup.execute(idRec, cadena, nroReg,codUsuario);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List getAllOfertas(){
		GetAllOfertas getAllOfertas = new GetAllOfertas(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllOfertas.execute();
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String insertPostularOferta(String idRec, String codOferta){
		
		InsertPostularOferta insertPostularOferta = new InsertPostularOferta(this.getDataSource());
		
		HashMap outputs = (HashMap)insertPostularOferta.execute(idRec, codOferta);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String insertReclutaAreaInteres (String idRec, String cadena, String nroReg){
		InsertReclutaAreaInteres insertReclutaAreaInteres = new InsertReclutaAreaInteres(this.getDataSource());
		
		HashMap outputs = (HashMap)insertReclutaAreaInteres.execute(idRec, cadena, nroReg);
		if (!outputs.isEmpty())
		{
			return (String)outputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List getReclutaAreaInteres(String idRec,String area){
		GetReclutaAreaInteres getReclutaAreaInteres = new GetReclutaAreaInteres(this.getDataSource());
		
		HashMap outputs = (HashMap)getReclutaAreaInteres.execute(idRec,area);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
		
	}
	
	public List getReclutaAreaInteresEspecial(String idRec, String tipoArea, String nroReg){
		GetReclutaAreaInteresEspecial getReclutaAreaInteresEspecial = new GetReclutaAreaInteresEspecial(this.getDataSource());
		
		HashMap outputs = (HashMap)getReclutaAreaInteresEspecial.execute(idRec, tipoArea, nroReg);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}

