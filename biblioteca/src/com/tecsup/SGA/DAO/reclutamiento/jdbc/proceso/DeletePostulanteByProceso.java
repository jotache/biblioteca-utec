package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeletePostulanteByProceso extends StoredProcedure{
	private static Log log = LogFactory.getLog(DeletePostulanteByProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
							+ ".PKG_RECL_PROCESO.SP_DEL_POST_X_PROCESO";
	private static final String CADPOST_ID = "E_C_CADPOST_ID";
	private static final String NRO_REGISTROS = "E_V_NRO_REGISTROS";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String TIPO = "E_C_TIPO";
	private static final String RETVAL = "S_V_RETVAL";
	
	public DeletePostulanteByProceso(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(CADPOST_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(NRO_REGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(TIPO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codPostulantes, String numreg, String codProceso
			, String tipo, String usuario)
	{
	
		log.info("*** INI "  + SPROC_NAME + " ****");
		log.info("CADPOST_ID:"+ codPostulantes);
		log.info("NRO_REGISTROS:"+ numreg);
		log.info("PROC_ID:"+ codProceso);
		log.info("TIPO:"+ tipo);
		log.info("*** INI "  + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();
		inputs.put(CADPOST_ID, codPostulantes);
		inputs.put(NRO_REGISTROS, numreg);
		inputs.put(PROC_ID, codProceso);
		inputs.put(TIPO, tipo);
	
		return super.execute(inputs);
	
	}
}
