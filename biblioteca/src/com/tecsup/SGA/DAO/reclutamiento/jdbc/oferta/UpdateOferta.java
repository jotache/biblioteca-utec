package com.tecsup.SGA.DAO.reclutamiento.jdbc.oferta;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateOferta extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateOferta.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
										+ ".PKG_RECL_PROCESO.SP_ACT_OFERTA";

	private static final String OFER_ID = "E_C_OFER_ID";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String OFER_FECFINPUBLI = "E_C_OFER_FECFINPUBLI";
	private static final String OFER_FECINIPUBLI = "E_C_OFER_FECINIPUBLI";
	private static final String OFER_DESCRIPCION = "E_V_OFER_DESCRIPCION";
	private static final String OFER_DENOMINACION = "E_V_OFER_DENOMINACION";
	private static final String OFER_NROVACANTES = "E_N_OFER_NROVACANTES";
	private static final String OFER_USU_MODI = "E_V_OFER_USU_MODI";
	private static final String RETVAL = "S_V_RETVAL";

	public UpdateOferta(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(OFER_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(OFER_FECFINPUBLI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(OFER_FECINIPUBLI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(OFER_DESCRIPCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(OFER_DENOMINACION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(OFER_NROVACANTES, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(OFER_USU_MODI, OracleTypes.VARCHAR));	
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codOferta, String codProceso,String fecIni,String fecFin,String descripcion
				,String denominacion,String nroVacantes, String usuCrea)
	{
		
		log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("PROC_ID:"+codProceso);
    	log.info("OFER_FECFINPUBLI:"+fecFin);
    	log.info("OFER_FECINIPUBLI:"+fecIni);
    	log.info("OFER_DESCRIPCION:"+descripcion);
    	log.info("OFER_DENOMINACION:"+denominacion);
    	log.info("OFER_NROVACANTES:"+nroVacantes);
    	log.info("OFER_USU_CREA:"+usuCrea);
    	log.info("*** FIN " + SPROC_NAME + " ***");
		
		Map inputs = new HashMap();
		
		inputs.put(OFER_ID, codOferta);
		inputs.put(PROC_ID, codProceso);
		inputs.put(OFER_FECFINPUBLI, fecFin);
		inputs.put(OFER_FECINIPUBLI, fecIni);
		inputs.put(OFER_DESCRIPCION, descripcion);
		inputs.put(OFER_DENOMINACION, denominacion);
		inputs.put(OFER_NROVACANTES, nroVacantes);
		inputs.put(OFER_USU_MODI, usuCrea);
		
		return super.execute(inputs);
	
	}
}
