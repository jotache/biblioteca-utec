package com.tecsup.SGA.DAO.reclutamiento.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.reclutamiento.CalificacionDAO;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.comun.GetAllCalificacion;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.comun.InsertCalificacion;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.comun.UpdateCalificacion;
import com.tecsup.SGA.DAO.reclutamiento.jdbc.comun.DeleteCalificaciones;
import com.tecsup.SGA.common.CommonConstants;

public class CalificacionDAOJdbc extends JdbcDaoSupport implements CalificacionDAO{
	public List getAllCalificacion(String codEtapa, String codTipoEvaluacion) {		
		GetAllCalificacion getAllCalificacion = new GetAllCalificacion(this.getDataSource());
		HashMap outputs = (HashMap)getAllCalificacion.execute(codEtapa, codTipoEvaluacion);		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String InsertCalificacion(String codEtapa, String codTipoEva, String calificacion, String codCalFin
			, String usucrea, String estReg)
	{
		InsertCalificacion InsertCalificacion = new InsertCalificacion(this.getDataSource());
		HashMap inputs = (HashMap)InsertCalificacion.execute(codEtapa
				, codTipoEva, calificacion, codCalFin, usucrea, estReg);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}

	public String UpdateCalificacion(String codEtapa, String codTipoEva, String calificacion, String codCalElim
			, String usucrea, String estReg)
	{
		UpdateCalificacion UpdateCalificacion = new UpdateCalificacion(this.getDataSource());
		HashMap inputs = (HashMap)UpdateCalificacion.execute(codEtapa, codTipoEva, calificacion, codCalElim, usucrea, estReg);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public String DeleteCalificaciones(String codEtapa, String codTipoEva
			, String usucrea)
	{
		DeleteCalificaciones DeleteCalificaciones = new DeleteCalificaciones(this.getDataSource());
		HashMap inputs = (HashMap)DeleteCalificaciones.execute(codEtapa, codTipoEva, usucrea);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	
}
