package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;

public class CargarPostulantesXProceso extends StoredProcedure {

	private static Log log = LogFactory.getLog(CargarPostulantesXProceso.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
		+ ".PKG_RECL_PROCESO.SP_SEL_GEN_POST_X_PROCESO";

	private static final String E_C_PROC_ID = "E_C_PROC_ID";
	private static final String E_C_USUARIO = "E_C_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public CargarPostulantesXProceso(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_PROC_ID, OracleTypes.CHAR));		
		declareParameter(new SqlParameter(E_C_USUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codProceso,String usuario)
	{	
		log.info("*** INI "  + SPROC_NAME + " ****");
		log.info("E_C_PROC_ID:"+ codProceso);
		log.info("E_C_USUARIO:"+ usuario);
		log.info("*** INI "  + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();
		inputs.put(E_C_PROC_ID, codProceso);
		inputs.put(E_C_USUARIO, usuario);
		return super.execute(inputs);
		
	}
}
