package com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

	public class InsertEvaluacionPostulanteEnvio extends StoredProcedure {
		private static Log log = LogFactory.getLog(InsertEvaluacionPostulanteEnvio.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
			+ ".PKG_RECL_PROCESO.SP_ACT_EVALUACION_ENVIO";
	
	private static final String CADPOST_ID = "E_C_CADPOST_ID";
	private static final String NRO_REGISTROS = "E_V_NRO_REGISTROS";
	private static final String E_C_CADPROC_ID = "E_C_CADPROC_ID";
	private static final String PROC_CODETAPA = "E_C_PROC_CODETAPA";	
	private static final String EVAC_CODESTADOEVENTO = "E_C_EVAC_CODESTADOEVENTO";	
	private static final String CODUSUARIO = "E_V_CODUSUARIO";
	private static final String RETVAL = "S_V_RETVAL";
	
	public InsertEvaluacionPostulanteEnvio(DataSource dataSource){
		super(dataSource, SPROC_NAME);		
		declareParameter(new SqlParameter(CADPOST_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(NRO_REGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CADPROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODETAPA, OracleTypes.CHAR));		
		declareParameter(new SqlParameter(EVAC_CODESTADOEVENTO, OracleTypes.CHAR));		
		declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));		
		compile();
	}
	
	public Map execute(String codPostulantes, String nroRegistros, String codProc, String codEtapa
			, String estadoEvento 
			, String usuario) {
		Map inputs = new HashMap();
		
		log.info("*** INI " + SPROC_NAME + " ****");
		log.info("CADPOST_ID:"+codPostulantes);
		log.info("NRO_REGISTROS:"+nroRegistros);
		log.info("E_C_CADPROC_ID:"+codProc);
		log.info("PROC_CODETAPA:"+codEtapa);		
		log.info("EVAC_CODESTADOEVENTO:"+estadoEvento);
		log.info("CODUSUARIO:"+usuario);
		log.info("*** FIN " + SPROC_NAME + " ****");
		
		inputs.put(CADPOST_ID, codPostulantes);
		inputs.put(NRO_REGISTROS, nroRegistros);
		inputs.put(E_C_CADPROC_ID, codProc);
		inputs.put(PROC_CODETAPA, codEtapa);		
		inputs.put(EVAC_CODESTADOEVENTO, estadoEvento);		
		inputs.put(CODUSUARIO, usuario);
		
		return super.execute(inputs);
	}
}
