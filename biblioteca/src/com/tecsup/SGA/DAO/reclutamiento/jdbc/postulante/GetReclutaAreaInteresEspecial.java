package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Recluta;

public class GetReclutaAreaInteresEspecial extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetReclutaAreaInteresEspecial.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_sel_area_int_x_post_union";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String AREA_INT = "E_C_TTD_AREA_INT";
    private static final String NRO_REG = "E_V_NRO_REGISTROS";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetReclutaAreaInteresEspecial(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(AREA_INT, OracleTypes.CHAR));
        declareParameter(new SqlParameter(NRO_REG, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new DatosReclutaMapper()));
        compile();
    }

    public Map execute(String idRec, String tipoArea, String nroReg) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_ID:"+idRec);
    	log.info("AREA_INT:"+tipoArea);
    	log.info("NRO_REG:"+nroReg);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
        inputs.put(POST_ID, idRec);//idRec
        inputs.put(AREA_INT, tipoArea);
        inputs.put(NRO_REG, nroReg);
        
        return super.execute(inputs);

    }
    
    final class DatosReclutaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Recluta recluta = new Recluta();
        	
        	recluta.setCodTTDInteres(rs.getString("CodigoArea"));
        	recluta.setCodInteres(rs.getString("CodigoSecuencial"));
        	recluta.setDescInteres(rs.getString("DescripcionArea"));
        	
            return recluta;
        }
    }
}

