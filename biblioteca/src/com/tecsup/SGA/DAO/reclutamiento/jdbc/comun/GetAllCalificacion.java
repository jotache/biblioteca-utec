package com.tecsup.SGA.DAO.reclutamiento.jdbc.comun;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Calificacion;

public class GetAllCalificacion extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllCalificacion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL + 
									".PKG_GEN_COMUN.SP_SEL_TIPO_CALIFICACION";
	private static final String CODETAPA = "E_C_CODETAPA";
	private static final String CODTIPOEVALUACION = "E_C_CODTIPOEVALUACION";
	private static final String RECORDSET = "S_C_RECORDSET";

	public GetAllCalificacion(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(CODETAPA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(CODTIPOEVALUACION, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new CalificacionMapper()));
		compile();
	}

	public Map execute(String codEtapa, String codTipoEvaluacion) {    	
		Map inputs = new HashMap();    	
		
		log.info("*** INI "+ SPROC_NAME + "***");
		log.info("CODETAPA:"+codEtapa);
		log.info("CODTIPOEVALUACION:"+codTipoEvaluacion);		
		log.info("*** FIN "+ SPROC_NAME + "***");
		
		inputs.put(CODETAPA, codEtapa);        
		inputs.put(CODTIPOEVALUACION, codTipoEvaluacion);
		return super.execute(inputs);
	}	

    final class CalificacionMapper implements RowMapper {        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Calificacion obj = new Calificacion();
        	obj.setCodEtapa(rs.getString("CODIGOETAPA") == null ? "" : rs.getString("CODIGOETAPA"));
        	obj.setCodTipoEvaluacion(rs.getString("CODIGOTIPOEVAL") == null ? "" : rs.getString("CODIGOTIPOEVAL"));
        	obj.setCodCalificacion(rs.getString("CODIGOCALIFICACION") == null ? "" : rs.getString("CODIGOCALIFICACION"));
        	obj.setCodCalificacionId(rs.getString("CODIGOCALIFICACION_ID") == null ? "" : rs.getString("CODIGOCALIFICACION_ID"));
        	obj.setCodCalificacionEliminatoria(rs.getString("CODIGOCALIFELIM") == null ? "" : rs.getString("CODIGOCALIFELIM"));
        	obj.setDscCalificacionNormal(rs.getString("DESCRIPCIONcALIFICACION") == null ? "" : rs.getString("DESCRIPCIONcALIFICACION"));        	
        	return obj;
        }
    }
}
