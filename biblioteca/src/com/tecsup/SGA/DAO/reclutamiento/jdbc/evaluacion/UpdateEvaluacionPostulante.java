package com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.date.SpreadsheetDate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class UpdateEvaluacionPostulante extends StoredProcedure{
	private static Log log = LogFactory.getLog(UpdateEvaluacionPostulante.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
									+ ".PKG_RECL_PROCESO.SP_ACT_EVALUACION";

	private static final String CAD_POST_PROC = "E_C_CAD_POST_PROC";
	private static final String CAD_EVALUACIONES = "E_C_CAD_EVALUACIONES";
	private static final String NRO_REGISTROS = "E_V_NRO_REGISTROS";
	private static final String EVAC_CODESTADOEVENTO = "E_C_EVAC_CODESTADOEVENTO";
	private static final String EVAC_COMENTARIO = "E_V_EVAC_COMENTARIO";
	private static final String EVAC_CODCALIFICACION = "E_V_EVAC_CODCALIFICACION";
	private static final String EVAL_CODCALIFICAFINETAPA = "E_C_EVAL_CODCALIFICAFINETAPA";
	private static final String EVAL_COMENTARIOFINETAPA = "E_V_EVAL_COMENTARIOFINETAPA";
	private static final String IND_CAMBIO_ESTADO = "E_C_IND_CAMBIO_ESTADO";	
	private static final String CODUSUARIO = "E_V_CODUSUARIO";	
	private static final String RETVAL = "S_V_RETVAL";
	
	public UpdateEvaluacionPostulante(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(CAD_POST_PROC, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(CAD_EVALUACIONES, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(NRO_REGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(EVAC_CODESTADOEVENTO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAC_COMENTARIO, OracleTypes.VARCHAR));		
		declareParameter(new SqlParameter(EVAC_CODCALIFICACION, OracleTypes.CHAR));		
		declareParameter(new SqlParameter(EVAL_CODCALIFICAFINETAPA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(EVAL_COMENTARIOFINETAPA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(IND_CAMBIO_ESTADO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
		
		compile();
	}
	
	public Map execute(String codPostulantesProceso, String codEvaluaciones, String nroRegistros, String codEstadoEvento 
					, String comentario, String codCalificacion, String codCalificacionFinEtapa, String comenFinEtapa
					, String indCambioEstado, String usuario) {
		Map inputs = new HashMap();
		
		log.info("*** INI " + SPROC_NAME + " *** ");
		log.info("CAD_POST_PROC:"+codPostulantesProceso);
		log.info("CAD_EVALUACIONES:"+codEvaluaciones);
		log.info("NRO_REGISTROS:"+nroRegistros);
		log.info("EVAC_CODESTADOEVENTO:"+codEstadoEvento);
		log.info("EVAC_COMENTARIO:"+comentario);
		log.info("EVAC_CODCALIFICACION:"+codCalificacion);
		log.info("EVAL_CODCALIFICAFINETAPA:"+codCalificacionFinEtapa);
		log.info("EVAL_COMENTARIOFINETAPA:"+comenFinEtapa);
		log.info("IND_CAMBIO_ESTADO:"+indCambioEstado);
		log.info("CODUSUARIO:"+usuario);
		log.info("*** FIN " + SPROC_NAME + " *** ");
		
		inputs.put(CAD_POST_PROC, codPostulantesProceso);
		inputs.put(CAD_EVALUACIONES, codEvaluaciones);
		inputs.put(NRO_REGISTROS, nroRegistros);
		inputs.put(EVAC_CODESTADOEVENTO, codEstadoEvento);
		inputs.put(EVAC_COMENTARIO, comentario);
		inputs.put(EVAC_CODCALIFICACION, codCalificacion);
		inputs.put(EVAL_CODCALIFICAFINETAPA, codCalificacionFinEtapa);
		inputs.put(EVAL_COMENTARIOFINETAPA, comenFinEtapa);
		inputs.put(IND_CAMBIO_ESTADO, indCambioEstado);
		inputs.put(CODUSUARIO, usuario);
		
		return super.execute(inputs);
	}
}
