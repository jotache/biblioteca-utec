package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Postulante;

public class GetAllPostulante extends StoredProcedure {

	private static Log log = LogFactory.getLog(GetAllPostulante.class);
			
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
									+ ".PKG_RECL_PROCESO.SP_SEL_POSTULANTES";
	private static final String POST_NOMBRES = "E_V_POST_NOMBRES";
	private static final String POST_APELLPAT = "E_V_POST_APELLPAT";
	private static final String POST_CODPROFESION = "E_C_POST_CODPROFESION";
	private static final String POST_CODINSTITUCION = "E_C_POST_CODINSTITUCION";
	private static final String POST_CODGRADO = "E_C_POST_CODGRADO";
	private static final String POST_AREA = "E_C_POST_AREA";
	private static final String CODPROCESO = "E_C_CODPROCESO";
	private static final String CODETAPA = "E_C_CODETAPA";
	private static final String RECORDSET = "S_C_RECORDSET";

	public GetAllPostulante(DataSource dataSource)
	{
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(POST_NOMBRES, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_APELLPAT, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_CODPROFESION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_CODINSTITUCION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_CODGRADO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_AREA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(CODPROCESO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(CODETAPA, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new PostulanteMapper()));
		compile();
	}
	
	public Map execute(String nombres, String apellidos, String codProfesion, String codintitucion
					, String codGrado, String codAreas, String codProceso, String codEtapa)
	{
		Map inputs = new HashMap();
		
		log.info("*** INI " + SPROC_NAME + "***");
		log.info("POST_NOMBRES:" + nombres);
		log.info("POST_APELLPAT:" + apellidos);
		log.info("POST_CODPROFESION:" + codProfesion);
		log.info("POST_CODINSTITUCION:" + codintitucion);
		log.info("POST_CODGRADO:" + codGrado);
		log.info("POST_AREA:" + codAreas);
		log.info("CODPROCESO:" + codProceso);
		log.info("CODETAPA:" + codEtapa);
		log.info("*** FIN " + SPROC_NAME + "***");
		
		inputs.put(POST_NOMBRES, nombres);
		inputs.put(POST_APELLPAT, apellidos);
		inputs.put(POST_CODPROFESION, codProfesion);
		inputs.put(POST_CODINSTITUCION, codintitucion);
		inputs.put(POST_CODGRADO, codGrado);
		inputs.put(POST_AREA, codAreas);
		inputs.put(CODPROCESO, codProceso);
		inputs.put(CODETAPA, codEtapa);
		
		return super.execute(inputs);	
	}
	
	final class PostulanteMapper implements RowMapper {
		
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
			Postulante postulante = new Postulante();
		
			postulante.setCodPostulante(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO").trim());
			postulante.setNombres(rs.getString("postulante") == null ? "" : rs.getString("postulante").trim());
			postulante.setApepat(rs.getString("DescripcionInstitucion") == null ? "" : rs.getString("DescripcionInstitucion").trim());
			postulante.setProfesion(rs.getString("DescripcionProfesion") == null ? "" : rs.getString("DescripcionProfesion").trim());
			postulante.setArea_interes(rs.getString("DescripcionAreaInteres") == null ? "" : rs.getString("DescripcionAreaInteres").trim());
			postulante.setFechaModificacion(rs.getString("FEC_MODI") == null ? "" : rs.getString("FEC_MODI").trim());
			
			return postulante;
		}
	}
}
