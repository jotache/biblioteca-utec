package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;



public class InsertReclutaExpLaboral extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertReclutaExpLaboral.class);
	private static final String SPROC_NAME =CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_ins_postulante_explab";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String POST_EMPRESA = "E_V_POST_EMPRESA";
    private static final String POST_CODPUESTO = "E_C_POST_CODPUESTO";
    private static final String POST_OTROPUESTO = "E_V_POST_OTROPUESTO";
    private static final String POST_FECINICIO = "E_C_POST_FECINICIO";
    private static final String POST_FECFIN = "E_C_POST_FECFIN";
    private static final String POST_REFERENCIA = "E_V_POST_REFERENCIA";
    private static final String POST_TELEFREF = "E_V_POST_TLFCONTACTO";
    private static final String POST_FUNCIONES = "E_V_POST_FUNCIONES";
    private static final String POST_USU_MODI = "E_V_POST_USUARIO";
    private static final String RETVAL = "S_V_RETVAL";
    
    public InsertReclutaExpLaboral(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_EMPRESA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_CODPUESTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_OTROPUESTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_FECINICIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_FECFIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_REFERENCIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_TELEFREF, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_FUNCIONES, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_USU_MODI, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(	String idRec, String empresa, String codPuesto, String otroPuesto, String fecIni,
    					String fecFin, String referencia, String telef, String funciones,String codUsuario) {
    	
    	log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("POST_ID:"+idRec);
    	log.info("POST_EMPRESA:"+empresa);
    	log.info("POST_CODPUESTO:"+codPuesto);
    	log.info("POST_OTROPUESTO:"+otroPuesto);
    	log.info("POST_FECINICIO:"+fecIni);
    	log.info("POST_FECFIN:"+fecFin);
    	log.info("POST_REFERENCIA:"+referencia);
    	log.info("POST_TELEFREF:"+telef);
    	log.info("POST_FUNCIONES:"+funciones);
    	log.info("POST_USU_MODI:"+codUsuario);
    	log.info("*** FIN " + SPROC_NAME + " ***");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(POST_ID, idRec);
        inputs.put(POST_EMPRESA, empresa);
        inputs.put(POST_CODPUESTO, codPuesto);
        inputs.put(POST_OTROPUESTO, otroPuesto);
        inputs.put(POST_FECINICIO, fecIni);
        inputs.put(POST_FECFIN, fecFin);
        inputs.put(POST_REFERENCIA, referencia);
        inputs.put(POST_TELEFREF, telef);
        inputs.put(POST_FUNCIONES, funciones);
        inputs.put(POST_USU_MODI, codUsuario);
        
        return super.execute(inputs);
    }
}
