package com.tecsup.SGA.DAO.reclutamiento.jdbc.proceso;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Postulante;

public class GetAllPostulanteByProc extends StoredProcedure {
	 private static Log log = LogFactory.getLog(GetAllPostulanteByProc.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
										+ ".PKG_RECL_PROCESO.SP_SEL_POSTUL_X_PROCESO";
	private static final String PROC_ID = "E_C_PROC_ID";
	private static final String PROC_CODETAPA = "E_C_PROC_CODETAPA";
	private static final String POST_NOMBRE = "E_V_POST_NOMBRE";
	private static final String POST_APATERNO = "E_V_POST_APATERNO";
	private static final String E_V_ORDEN = "E_V_ORDEN";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllPostulanteByProc(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(PROC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(PROC_CODETAPA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_NOMBRE, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_APATERNO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_ORDEN, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new PostulanteMapper()));
		compile();
	}
	
	public Map execute(String codProceso, String codEtapa, String nombre, String apellidos,String orden) {
		
		log.info("*** INI "+SPROC_NAME+" ***");
		log.info("PROC_ID:"+codProceso);
		log.info("PROC_CODETAPA:"+codEtapa);
		log.info("POST_NOMBRE:"+nombre);
		log.info("POST_APATERNO:"+apellidos);
		log.info("*** FIN "+SPROC_NAME+" ***");
		
		Map inputs = new HashMap();			
		inputs.put(PROC_ID, codProceso);		
		inputs.put(PROC_CODETAPA, codEtapa);
		inputs.put(POST_NOMBRE, nombre);
		inputs.put(POST_APATERNO, apellidos);
		inputs.put(E_V_ORDEN, orden);
		return super.execute(inputs);	
	}

	final class PostulanteMapper implements RowMapper {
		
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
			Postulante postulante = new Postulante();		
			postulante.setCodPostulante(rs.getString("CODIGOPOSTULANTE") == null ? "" : rs.getString("CODIGOPOSTULANTE").trim());
			postulante.setNombres(rs.getString("NOMBREPOSTULANTE") == null ? "" : rs.getString("NOMBREPOSTULANTE").trim());
			postulante.setPerfil(rs.getString("CV") == null ? "" : rs.getString("CV").trim());
			postulante.setPretension_economica(rs.getString("SALARIO") == null ? "" : rs.getString("SALARIO").trim());
			postulante.setProfesion(rs.getString("DescripcionProfesion") == null ? "" : rs.getString("DescripcionProfesion").trim());
			postulante.setGradoInstruccion(rs.getString("DescripcionGrado") == null ? "" : rs.getString("DescripcionGrado").trim());
			postulante.setCodEstado(rs.getString("CODESTADO") == null ? "" : rs.getString("CODESTADO").trim());
			postulante.setDscEstado(rs.getString("DESCRIPCIONESTADO") == null ? "" : rs.getString("DESCRIPCIONESTADO").trim());			
			//postulante.setEvaluaciones(rs.getString("EVALUACIONES") == null ? "" : rs.getString("EVALUACIONES").trim());
			postulante.setUltEstadoPostulRevision(rs.getString("CALIFICACION") == null ? "" : rs.getString("CALIFICACION").trim());
			postulante.setHistorico(rs.getString("HIST") == null ? "" : rs.getString("HIST").trim());
			return postulante;
		}
	}
}
