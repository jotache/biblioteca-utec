package com.tecsup.SGA.DAO.reclutamiento.jdbc.comun;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Calificacion;

public class InsertCalificacion extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertCalificacion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL 
 	+ ".pkg_gen_comun.SP_INS_TIPO_CALIFICACION";
	
	private static final String E_C_CALIF_CODETAPA = "E_C_CALIF_CODETAPA";
	private static final String E_C_CALIF_CODTIPOEVALUACION = "E_C_CALIF_CODTIPOEVALUACION";
	private static final String E_C_CALIF_CALIFICACION = "E_C_CALIF_CALIFICACION";
	private static final String E_C_CALIF_CODCALIFFINAL = "E_C_CALIF_CODCALIFFINAL";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String E_C_CALIF_EST_REG = "E_C_CALIF_EST_REG";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertCalificacion(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_CALIF_CODETAPA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CALIF_CODTIPOEVALUACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CALIF_CALIFICACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CALIF_CODCALIFFINAL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CALIF_EST_REG, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	
	compile();
	}
	
	public Map execute(String codEtapa, String codTipoEva, String calificacion, String codCalFin
			, String usucrea, String estReg) {
		
		log.info("*** INI "+ SPROC_NAME + "***");
		log.info("E_C_CALIF_CODETAPA:"+codEtapa);
		log.info("E_C_CALIF_CODTIPOEVALUACION:"+codTipoEva);
		log.info("E_C_CALIF_CALIFICACION:"+calificacion);
		log.info("E_C_CALIF_CODCALIFFINAL:"+codCalFin);
		log.info("E_V_CODUSUARIO:"+usucrea);
		log.info("E_C_CALIF_EST_REG:"+estReg);
		log.info("*** FIN "+ SPROC_NAME + "***");
		
	Map inputs = new HashMap();
	
	inputs.put(E_C_CALIF_CODETAPA, codEtapa);
	inputs.put(E_C_CALIF_CODTIPOEVALUACION, codTipoEva);
	inputs.put(E_C_CALIF_CALIFICACION, calificacion);
	inputs.put(E_C_CALIF_CODCALIFFINAL, codCalFin);
	inputs.put(E_V_CODUSUARIO, usucrea);
	inputs.put(E_C_CALIF_EST_REG, estReg);


return super.execute(inputs);
}
}
