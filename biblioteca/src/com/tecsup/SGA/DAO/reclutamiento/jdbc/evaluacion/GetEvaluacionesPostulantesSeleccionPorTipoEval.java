package com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
import com.tecsup.SGA.common.CommonConstants;

public class GetEvaluacionesPostulantesSeleccionPorTipoEval extends
		StoredProcedure {

	private static Log log = LogFactory.getLog(GetEvaluacionesPostulantesSeleccionPorTipoEval.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_proceso.SP_SEL_EVALUACIONES_X_TIPOEVAL";
	private static final String E_C_COD_PROCESO = "E_C_COD_PROCESO";
	private static final String E_C_COD_TIPOEVAL = "E_C_COD_TIPOEVAL";	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
	
	public GetEvaluacionesPostulantesSeleccionPorTipoEval(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_PROCESO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_TIPOEVAL, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new Evaluaciones()));
	}
	public Map execute(String codProceso,String codTipoEvaluacion){
		
		log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("E_C_COD_PROCESO:"+codProceso);
    	log.info("E_C_COD_TIPOEVAL:"+codTipoEvaluacion);  	    
    	log.info("*** FIN " + SPROC_NAME + " ***");
    	
		Map inputs = new HashMap();
		inputs.put(E_C_COD_PROCESO, codProceso);
        inputs.put(E_C_COD_TIPOEVAL, codTipoEvaluacion);               
        return super.execute(inputs);
	}
	final class Evaluaciones implements RowMapper{
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			EvaluacionesByEvaluadorBean obj = new EvaluacionesByEvaluadorBean();
        	obj.setCodPostulante(rs.getString("POSTULANTE_ID") == null ? "" : rs.getString("POSTULANTE_ID"));        	
        	obj.setCodEvaluador(rs.getString("EVAC_USUARIO") == null ? "" : rs.getString("EVAC_USUARIO"));
        	obj.setFlgEstadoActual(rs.getString("EVAC_FLAG_ESTADO_ACTUAL") == null ? "" : rs.getString("EVAC_FLAG_ESTADO_ACTUAL"));
        	obj.setCodPostProceso(rs.getString("COD_POST_PROCESO") == null ? "" : rs.getString("COD_POST_PROCESO"));
        	obj.setCodEvalEnProceso(rs.getString("COD_EVAL_EN_PROCESO") == null ? "" : rs.getString("COD_EVAL_EN_PROCESO"));
        	obj.setNomPostulante(rs.getString("POSTULANTE") == null ? "" : rs.getString("POSTULANTE"));
        	obj.setCv(rs.getString("CV") == null ? "" : rs.getString("CV"));
        	obj.setCodProceso(rs.getString("PROCESO_ID") == null ? "" : rs.getString("PROCESO_ID"));
        	obj.setCodTipoEvaluacion(rs.getString("EVALUACION_ID") == null ? "" : rs.getString("EVALUACION_ID"));        	
        	obj.setOrden(rs.getString("ORDEN") == null ? "" : rs.getString("ORDEN"));
        	obj.setDscProceso(rs.getString("PROCESO_DSC") == null ? "" : rs.getString("PROCESO_DSC"));
        	obj.setDscTipoEvaluacion(rs.getString("EVALUACION_DSC") == null ? "" : rs.getString("EVALUACION_DSC"));
        	obj.setComentario(rs.getString("COMENTARIO_RRHH") == null ? "" : rs.getString("COMENTARIO_RRHH"));
        	obj.setDscCalificacion(rs.getString("CALIFICACION_DSC") == null ? "" : rs.getString("CALIFICACION_DSC"));
        	obj.setDscCalificacionJefeDpto_Revision(rs.getString("CALIFICACION_JEFE_DPTO") == null ? "" : rs.getString("CALIFICACION_JEFE_DPTO"));
        	return obj;
		}		
	}
}
