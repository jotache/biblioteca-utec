package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;


public class InsertReclutaAreaInteres extends StoredProcedure{

	private static Log log = LogFactory.getLog(InsertReclutaAreaInteres.class);
	private static final String SPROC_NAME =CommonConstants.ESQ_RECLUTAMIENTO + ".pkg_recl_postulante.sp_ins_areainteres_x_post";
    private static final String POST_ID = "E_C_POST_ID";
    private static final String POST_CADENA = "E_V_CAD_CODAREAS";
    private static final String POST_NRO_REG = "E_V_NRO_REGISTROS";
    private static final String RETVAL = "S_V_RETVAL";
    
    public InsertReclutaAreaInteres(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(POST_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_CADENA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_NRO_REG, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));
        compile();
    }

    public Map execute(String idRec, String cadena, String nroReg) {
    	
    	log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_ID:"+idRec);
    	log.info("POST_CADENA:"+cadena);
    	log.info("POST_NRO_REG:"+nroReg);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(POST_ID, idRec);
        inputs.put(POST_CADENA, cadena);
        inputs.put(POST_NRO_REG, nroReg);
        
        return super.execute(inputs);
    }
}
