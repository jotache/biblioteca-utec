package com.tecsup.SGA.DAO.reclutamiento.jdbc.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;

public class GetEvaluacionesByEvaluador extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetEvaluacionesByEvaluador.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
							".pkg_recl_proceso.SP_SEL_EVAL_X_EVALUADOR";
	private static final String COD_ETAPA = "E_C_COD_ETAPA";
	private static final String POST_ESTADO = "E_C_POST_ESTADO";
	private static final String RRHH_ESTADO = "E_C_RRHH_ESTADO";
	private static final String EVALUADOR_ID = "E_V_EVALUADOR_ID";
	private static final String E_V_COD_PROCESO = "E_V_COD_PROCESO";
	private static final String RECORDSET = "S_C_RECORDSET";
	
    public GetEvaluacionesByEvaluador(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(COD_ETAPA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(POST_ESTADO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(RRHH_ESTADO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(EVALUADOR_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_PROCESO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }
    
    public Map execute(String codEtapa, String postEstado, String rrhhEstado, String evaluador,String codProceso) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("COD_ETAPA:"+codEtapa);
    	log.info("POST_ESTADO:"+postEstado);
    	log.info("RRHH_ESTADO:"+rrhhEstado);
    	log.info("EVALUADOR_ID:"+evaluador);
    	log.info("E_V_COD_PROCESO:"+codProceso);
    	log.info("*** FIN " + SPROC_NAME + " ***");
    	
        inputs.put(COD_ETAPA, codEtapa);
        inputs.put(POST_ESTADO, postEstado);
        inputs.put(RRHH_ESTADO, rrhhEstado);
        inputs.put(EVALUADOR_ID, evaluador);
        inputs.put(E_V_COD_PROCESO, codProceso);
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	EvaluacionesByEvaluadorBean obj = new EvaluacionesByEvaluadorBean();
        	
        	obj.setCodPostulante(rs.getString("POSTULANTE_ID") == null ? "" : rs.getString("POSTULANTE_ID"));
        	obj.setCodPostProceso(rs.getString("COD_POST_PROCESO") == null ? "" : rs.getString("COD_POST_PROCESO"));
        	obj.setCodEvalEnProceso(rs.getString("COD_EVAL_EN_PROCESO") == null ? "" : rs.getString("COD_EVAL_EN_PROCESO"));
        	obj.setNomPostulante(rs.getString("POSTULANTE") == null ? "" : rs.getString("POSTULANTE"));
        	obj.setCv(rs.getString("CV") == null ? "" : rs.getString("CV"));
        	obj.setCodProceso(rs.getString("PROCESO_ID") == null ? "" : rs.getString("PROCESO_ID"));
        	obj.setCodTipoEvaluacion(rs.getString("EVALUACION_ID") == null ? "" : rs.getString("EVALUACION_ID"));
        	obj.setDscProceso(rs.getString("PROCESO_DSC") == null ? "" : rs.getString("PROCESO_DSC"));
        	obj.setDscTipoEvaluacion(rs.getString("EVALUACION_DSC") == null ? "" : rs.getString("EVALUACION_DSC"));
        	obj.setComentario(rs.getString("COMENTARIO_RRHH") == null ? "" : rs.getString("COMENTARIO_RRHH"));
        	obj.setDscCalificacion(rs.getString("CALIFICACION_DSC") == null ? "" : rs.getString("CALIFICACION_DSC"));
        	obj.setDscCalificacionJefeDpto_Revision(rs.getString("CALIFICACION_JEFE_DPTO") == null ? "" : rs.getString("CALIFICACION_JEFE_DPTO"));
        	return obj;
        }
    }

}
