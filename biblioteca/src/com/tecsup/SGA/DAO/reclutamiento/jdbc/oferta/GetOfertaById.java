package com.tecsup.SGA.DAO.reclutamiento.jdbc.oferta;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Oferta;

public class GetOfertaById extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetOfertaById.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO 
									+ ".PKG_RECL_PROCESO.SP_SEL_OFERTA_X_ID";
	private static final String OFER_ID = "E_C_OFER_ID";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetOfertaById(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(OFER_ID, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new OfertaMapper()));
		compile();
	}

	public Map execute(String codOferta) {
	
		log.info("*** INI " + SPROC_NAME + " ***");
    	log.info("OFER_ID:"+codOferta);
    	log.info("*** FIN " + SPROC_NAME + " ***");
    	
		Map inputs = new HashMap();		
		inputs.put(OFER_ID, codOferta);		
		return super.execute(inputs);
	
	}
	
	final class OfertaMapper implements RowMapper {
		
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
			Oferta oferta = new Oferta();
		
			oferta.setCodOferta(rs.getString("CodigoOferta") == null ? "" : rs.getString("CodigoOferta").trim());
			oferta.setDescripcion(rs.getString("DescripcionOferta") == null ? "" : rs.getString("DescripcionOferta").trim());
			oferta.setCodProceso(rs.getString("CodigoProceso") == null ? "" : rs.getString("CodigoProceso").trim());
			oferta.setVacantes(rs.getString("NroVacantes") == null ? "" : rs.getString("NroVacantes").trim());
			oferta.setDenominacion(rs.getString("Texto") == null ? "" : rs.getString("Texto").trim());
			oferta.setFecIniPub(rs.getString("FechaInicio") == null ? "" : rs.getString("FechaInicio").trim());
			oferta.setFecFinPub(rs.getString("FechaFin") == null ? "" : rs.getString("FechaFin").trim());
			
			return oferta;
		}
	}
}
