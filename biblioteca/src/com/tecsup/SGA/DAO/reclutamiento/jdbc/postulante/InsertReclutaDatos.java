package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import com.tecsup.SGA.common.*;


public class InsertReclutaDatos extends StoredProcedure{
	private static Log log = LogFactory.getLog(InsertReclutaDatos.class);
	private static final String SPROC_NAME =CommonConstants.ESQ_RECLUTAMIENTO 
				+ ".pkg_recl_postulante.sp_ins_postulante";
    private static final String POST_NOMBRE = "E_V_POST_NOMBRES1";
    private static final String POST_APEPAT = "E_V_POST_APELLPAT2";
    private static final String POST_APEMAT = "E_V_POST_APELLMAT2";
    private static final String POST_CLAVE = "E_V_POST_CLAVE";
    private static final String POST_FEC_NAC = "E_C_POST_FECNAC";
    private static final String POST_DNI = "E_C_POST_DNI";
    private static final String POST_RUC = "E_C_POST_RUC";
    private static final String POST_COD_SEXO = "E_C_POST_CODSEXO";
    private static final String POST_COD_EST_CIVIL = "E_C_POST_CODESTCIVIL";
    private static final String POST_COD_NACIONALIDAD = "E_C_POST_CODNACIONALIDAD";
    private static final String POST_EMAIL = "E_V_POST_EMAIL";
    private static final String POST_EMAIL1 = "E_V_POST_EMAIL1";
    private static final String POST_ANIOS_EXPLAB = "E_V_POST_ANOSEXPLAB";
    private static final String POST_DOMICILIO = "E_V_POST_DOMICILIO";
    private static final String POST_COD_DPTO = "E_C_POST_CODDPTO";
    private static final String POST_COD_PROV = "E_C_POST_CODPROV";
    private static final String POST_COD_DIST = "E_C_POST_CODDIST";
    private static final String POST_PAIS = "E_V_POST_PAISRESIDENCIA";
    private static final String POST_COD_POSTAL = "E_V_POST_CODIGOPOSTAL";
    private static final String POST_TELEF1 = "E_V_POST_TELEFONO1";
    private static final String POST_TELEF2 = "E_V_POST_TELEFONO2";
    private static final String POST_TELEF_CEL = "E_V_POST_CELULAR";
    private static final String POST_POSTULADO_ANTES = "E_C_POST_POSTULANDOANTES";
    private static final String POST_TRABAJADO_ANTES = "E_C_POST_TRABAJADOANTES";
    private static final String POST_EXP_DOCENCIA = "E_C_POST_EXPERIENCIADOC";
    private static final String POST_ANIOSLABORANDODOC = "E_V_POST_ANIOSLABORANDODOC";
    private static final String POST_FAMILIA_ANTES = "E_C_POST_FAMILTRABANTES";
    private static final String POST_FAMILIA_NOMBRES ="E_V_POST_FAMILIARESNOMBRES";
    private static final String POST_DISPO_VIAJE = "E_C_POST_DISPONIBVIAJAR";
    private static final String POST_SEDE_PREF = "E_C_POST_SEDEPREFERENTE";
    private static final String POST_COD_INTERESADO = "E_C_POST_CODINTERESADO";
    private static final String POST_PRETENSION = "E_N_POST_PRETENSION";
    private static final String POST_COD_DEDICACION = "E_C_POST_CODDEDICACION";
    private static final String POST_COD_DISPO_TRAB = "E_C_POST_CODDISPONIBILIDAD";
    private static final String POST_OBS_PERFIL = "E_V_POST_OBSPERFIL";
    private static final String POST_COD_AREA_INTERES = "E_V_CAD_CODAREAINTERES";
    private static final String POST_NRO_REGISTROS = "E_V_NROREGISTROS";
    private static final String POST_COD_MONEDA = "E_C_COD_MONEDA";
    private static final String POST_TIPO_PAGO = "E_C_TIPO_PAGO";
    private static final String E_C_IND_PUESTPOSTULA = "E_C_IND_PUESTPOSTULA";    
    private static final String RETVAL = "S_V_RETVAL";
    
    public InsertReclutaDatos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        //declareParameter(new SqlParameter(POST_EMAIL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(POST_NOMBRE, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_APEPAT, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_APEMAT, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_CLAVE, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_FEC_NAC, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_DNI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_RUC, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_COD_SEXO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_COD_EST_CIVIL, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_COD_NACIONALIDAD, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_EMAIL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_EMAIL1, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_ANIOS_EXPLAB, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_DOMICILIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_COD_DPTO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_COD_PROV, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_COD_DIST, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_PAIS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_COD_POSTAL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_TELEF1, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_TELEF2, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_TELEF_CEL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_POSTULADO_ANTES, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_TRABAJADO_ANTES, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_EXP_DOCENCIA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_ANIOSLABORANDODOC, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_FAMILIA_ANTES, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_FAMILIA_NOMBRES, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_DISPO_VIAJE, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_SEDE_PREF, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_COD_INTERESADO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_PRETENSION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_COD_DEDICACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_COD_DISPO_TRAB, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_OBS_PERFIL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_COD_AREA_INTERES, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_NRO_REGISTROS, OracleTypes.VARCHAR));		
		declareParameter(new SqlParameter(POST_COD_MONEDA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(POST_TIPO_PAGO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_IND_PUESTPOSTULA, OracleTypes.CHAR));		
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR));

        compile();
    }

    public Map execute(	String nombre, String apepat, String apemat, String clave, String fecnac, 
    					String dni, String ruc, String sexo, String estadoCivil, String codNacionalidad, 
    					String email, String email1, String aniosExpLaboral, String direccion, String codDpto, 
    					String codProv, String codDist, String pais, String codPostal, String telefCasa, 
    					String telefAdicional, String telefCel, String codPostuladoAntes, String codTrabAntes,
    					String expDocencia, String aniosExpDocencia, String familiaAntes, String familiaNombres, 
    					String dispoViaje, String sedePref, String codInteresado, String pretension, 
    					String codDedicacion, String codDispoTrab, String perfil, String codAreaInteres, 
    					String nroRegAreaInt, String codMoneda, String tipoPago,String puestoPostula) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("****** INI "+ SPROC_NAME + " ******");
		log.info("POST_NOMBRE:"+ nombre); 		
		log.info("POST_APEPAT:"+ apepat);
		log.info("POST_APEMAT:"+ apemat);
		log.info("POST_CLAVE:"+ clave); 		
		log.info("POST_FEC_NAC:"+ fecnac);
		log.info("POST_DNI:"+ dni);
		log.info("POST_RUC:"+ ruc);
		log.info("POST_COD_SEXO:"+ sexo);
		log.info("POST_COD_EST_CIVIL:"+ estadoCivil);
		log.info("POST_COD_NACIONALIDAD:"+ codNacionalidad);
		log.info("POST_EMAIL:"+ email);
		log.info("POST_EMAIL1:"+ email1);
		log.info("POST_ANIOS_EXPLAB:"+ aniosExpLaboral);
		log.info("POST_DOMICILIO:"+ direccion);
		log.info("POST_COD_DPTO:"+ codDpto);
		log.info("POST_COD_PROV:"+ codProv);
		log.info("POST_COD_DIST:"+ codDist);
		log.info("POST_PAIS:"+ pais);
		log.info("POST_COD_POSTAL:"+ codPostal);
		log.info("POST_TELEF1:"+ telefCasa);
		log.info("POST_TELEF2:"+ telefAdicional);
		log.info("POST_TELEF_CEL:"+ telefCel);
		log.info("POST_POSTULADO_ANTES:"+ codPostuladoAntes);
		log.info("POST_TRABAJADO_ANTES:"+ codTrabAntes);
		log.info("POST_EXP_DOCENCIA:"+ expDocencia);
		log.info("POST_ANIOSLABORANDODOC:"+ aniosExpDocencia);
		log.info("POST_FAMILIA_ANTES:"+ familiaAntes);
		log.info("POST_FAMILIA_NOMBRES:"+ familiaNombres);
		log.info("POST_DISPO_VIAJE:"+dispoViaje);
		log.info("POST_SEDE_PREF:"+ sedePref);
		log.info("POST_COD_INTERESADO:"+ codInteresado);
		log.info("POST_PRETENSION:"+ pretension);
		log.info("POST_COD_DEDICACION:"+ codDedicacion);
		log.info("POST_COD_DISPO_TRAB:"+ codDispoTrab);
		log.info("POST_OBS_PERFIL:"+ perfil);
		log.info("POST_COD_AREA_INTERES:"+ codAreaInteres);
		log.info("POST_NRO_REGISTROS:"+ nroRegAreaInt);		
		log.info("POST_COD_MONEDA:"+ codMoneda);
		log.info("POST_TIPO_PAGO:"+ tipoPago);		
		log.info("E_C_IND_PUESTPOSTULA:"+ puestoPostula);
		log.info("****** FIN "+ SPROC_NAME + " ******");
		
    	inputs.put(POST_NOMBRE, nombre); 		
		inputs.put(POST_APEPAT, apepat);
		inputs.put(POST_APEMAT, apemat);
		inputs.put(POST_CLAVE, clave); 		
		inputs.put(POST_FEC_NAC, fecnac);
		inputs.put(POST_DNI, dni);
		inputs.put(POST_RUC, ruc);
		inputs.put(POST_COD_SEXO, sexo);
		inputs.put(POST_COD_EST_CIVIL, estadoCivil);
		inputs.put(POST_COD_NACIONALIDAD, codNacionalidad);
		inputs.put(POST_EMAIL, email);
		inputs.put(POST_EMAIL1, email1);
		inputs.put(POST_ANIOS_EXPLAB, aniosExpLaboral);
		inputs.put(POST_DOMICILIO, direccion);
		inputs.put(POST_COD_DPTO, codDpto);
		inputs.put(POST_COD_PROV, codProv);
		inputs.put(POST_COD_DIST, codDist);
		inputs.put(POST_PAIS, pais);
		inputs.put(POST_COD_POSTAL, codPostal);
		inputs.put(POST_TELEF1, telefCasa);
		inputs.put(POST_TELEF2, telefAdicional);
		inputs.put(POST_TELEF_CEL, telefCel);
		inputs.put(POST_POSTULADO_ANTES, codPostuladoAntes);
		inputs.put(POST_TRABAJADO_ANTES, codTrabAntes);
		inputs.put(POST_EXP_DOCENCIA, expDocencia);
		inputs.put(POST_ANIOSLABORANDODOC, aniosExpDocencia);
		inputs.put(POST_FAMILIA_ANTES, familiaAntes);
		inputs.put(POST_FAMILIA_NOMBRES, familiaNombres);
		inputs.put(POST_DISPO_VIAJE, dispoViaje);
		inputs.put(POST_SEDE_PREF, sedePref);
		inputs.put(POST_COD_INTERESADO, codInteresado);
		inputs.put(POST_PRETENSION, pretension);
		inputs.put(POST_COD_DEDICACION, codDedicacion);
		inputs.put(POST_COD_DISPO_TRAB, codDispoTrab);
		inputs.put(POST_OBS_PERFIL, perfil);
		inputs.put(POST_COD_AREA_INTERES, codAreaInteres);
		inputs.put(POST_NRO_REGISTROS, nroRegAreaInt);		
		inputs.put(POST_COD_MONEDA, codMoneda);
		inputs.put(POST_TIPO_PAGO, tipoPago);		
		inputs.put(E_C_IND_PUESTPOSTULA, puestoPostula);
 
		
		
        return super.execute(inputs);
    }
}
