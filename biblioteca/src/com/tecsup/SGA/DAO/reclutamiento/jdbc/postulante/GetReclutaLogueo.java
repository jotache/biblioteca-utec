package com.tecsup.SGA.DAO.reclutamiento.jdbc.postulante;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;


public class GetReclutaLogueo extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetReclutaLogueo.class);

	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + 
	".pkg_recl_postulante.sp_sel_validar_clave";
	private static final String POST_EMAIL = "E_C_PROC_CODETAPA";
	private static final String POST_CLAVE = "E_C_PROC_CODUNIDAD";
	private static final String RETVAL = "S_V_RETVAL";
	
	public GetReclutaLogueo(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(POST_EMAIL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(POST_CLAVE, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(RETVAL, OracleTypes.VARCHAR)); //, new LogueoMapper()
		compile();
	}
	
	public Map execute(String email, String clave) {
	
		log.info("*** INI " + SPROC_NAME + "***");
    	log.info("POST_EMAIL:"+email);
    	log.info("POST_CLAVE:"+clave);
    	log.info("*** FIN " + SPROC_NAME + "***");
    	
		Map inputs = new HashMap();
		
		inputs.put(POST_EMAIL, email);
		inputs.put(POST_CLAVE, clave);
		
		return super.execute(inputs);
	}	
}
