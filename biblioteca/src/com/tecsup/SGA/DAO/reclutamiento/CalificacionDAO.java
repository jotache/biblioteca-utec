package com.tecsup.SGA.DAO.reclutamiento;

import java.util.List; 

import com.tecsup.SGA.common.CommonConstants;
public interface CalificacionDAO {
	public List getAllCalificacion(String codEtapa, String codTipoEvaluacion);
	
	public String InsertCalificacion(String codEtapa, String codTipoEva, String calificacion, String codCalFin
			, String usucrea, String estReg);
	
	public String UpdateCalificacion(String codEtapa, String codTipoEva, String calificacion, String codCalElim
			, String usucrea, String estReg);
	
	public String DeleteCalificaciones(String codEtapa, String codTipoEva
			, String usucrea);
	
}
