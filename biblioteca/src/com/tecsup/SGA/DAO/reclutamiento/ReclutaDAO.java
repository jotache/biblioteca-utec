package com.tecsup.SGA.DAO.reclutamiento;

import java.util.*;

import com.tecsup.SGA.modelo.Recluta;

public interface ReclutaDAO {
	
	public String getAllRecluta(String email, String clave);
	
	public Recluta getAllDatosPersonaRecluta(String id);
	
	public String updateReclutaClave(String email, String clave_anterior, String clave_nueva);
	
	public String insertReclutaDatos(String nombre, String apepat, String apemat, String clave, String fecnac, 
			String dni, String ruc, String sexo, String estadoCivil, String codNacionalidad, 
			String email, String email1, String aniosExpLaboral, String direccion, String codDpto, 
			String codProv, String codDist, String pais, String codPostal, String telefCasa, 
			String telefAdicional, String telefCel, String codPostuladoAntes, String codTrabAntes,
			String expDocencia, String aniosExpDocencia, String familiaAntes, String familiaNombres, String dispoViaje, 
			String sedePref, String codInteresado, String pretension, String codDedicacion, 
			String codDispoTrab, String perfil, String codAreaInteres, String nroRegAreaInt, String codMoneda, String tipoPago,String puestoPostula);
		
	public String updateReclutaDatos(String idRec, String nombre, String apepat, String apemat, String fecnac, 
			String dni, String ruc, String sexo, String estadoCivil, String codNacionalidad, 
			String email, String email1, String aniosExpLaboral, String direccion, String codDpto, 
			String codProv, String codDist, String pais, String codPostal, String telefCasa, 
			String telefAdicional, String telefCel, String codPostuladoAntes, String codTrabAntes,
			String expDocencia, String aniosExpDocencia, String familiaAntes, String familiaNombres, String dispoViaje, 
			String sedePref, String codInteresado, String pretension, String codDedicacion, 
			String codDispoTrab, String perfil, String codMoneda, String tipoPago,String codAreaInteres,String puestoPostula,String codUsuario);
	
	public List getReclutaIdiomas(String idRec);
	
	public List getReclutaSuperior(String idRec);
	
	public List getReclutaExpLaboral(String idRec);
	
	public String updateReclutaArchAdjunto(String idRec, String cv, String foto,String codUsuario);
	
	public String updateReclutaEstSec(String idRec, String colegio1, String colegio2, String anioIni1, String anioFin1,
			String anioIni2, String anioFin2,String codUsuario);
	
	public String insertReclutaExpLaboral(String idRec, String empresa, String codPuesto, String otroPuesto, String fecIni,
			String fecFin, String referencia, String telef, String funciones,String codUsuario);
	
	public String updateReclutaExpLaboral(String idRec, String codExpLab, String empresa, String codPuesto, String otroPuesto, String fecIni,
			String fecFin, String referencia, String telef, String funciones,String codUsuario);
	
	public String updateReclutaIdiomas(String idRec, String cadena, String nroReg,String codUsuario);
	
	public String updateReclutaEstSup(String idRec, String cadena, String nroReg,String codUsuario);
	
	public List getAllOfertas();
	
	public String insertPostularOferta(String idRec, String codOferta);
	
	public String insertReclutaAreaInteres (String idRec, String cadena, String nroReg);
	
	public List getReclutaAreaInteres(String idRec,String area); //JHPR 2007-7-9
	
	public List getReclutaAreaInteresEspecial(String idRec, String tipoArea, String nroReg);
	
}