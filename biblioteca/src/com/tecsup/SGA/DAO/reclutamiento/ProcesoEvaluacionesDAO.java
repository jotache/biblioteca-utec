package com.tecsup.SGA.DAO.reclutamiento;

import java.util.List;

import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
import com.tecsup.SGA.modelo.Postulante;
public interface ProcesoEvaluacionesDAO {

	public String insertEvaluacionPostulante(String codPostulantes, String nroRegistros, String codProc
			, String codEtapa, String tipoEvaluacion, String estadoEvento, String comentario
			, String calificaion, String calificacionEtapa, String comentarioFinEtapa
			, String flagCambioEstado, String usuario);
	
	public String updateEvaluacionPostulante(String codPostulantesProceso, String codEvaluaciones
			, String nroRegistros, String codEstadoEvento, String comentario, String codCalificacion
			, String codCalificacionFinEtapa, String comenFinEtapa, String indCambioEstado, String usuario);
	
	public List getAllEvaluacionesByEvaluador(String codEtapa, String postEstado, String rrhhEstado
				, String codEvaluador,String codProceso);
	
	public List getEvaluacionById(String codEvaluacion);
	
	public List getAllEvaluacionesByEvaluadorCbo(String codEtapa, String postEstado, String rrhhEstado
			, String codEvaluador);
	
	public String updateEvaluacionPostulanteEnvio(String codPostulantes, String nroRegistros, String codProc, String codEtapa
			, String estadoEvento 
			, String usuario);
	
	public List<Postulante> getPostulantesEnviadosJefeDpto(String cadenaCodPostulantes,String codProceso,String codEtapa);
	
	public List<EvaluacionesByEvaluadorBean> getEvaluacionesSeleccionPorPostulante(String codProceso,String codPostulante);
	public List<EvaluacionesByEvaluadorBean> getEvaluacionesPostulantesSeleccionPorTipoEvaluacion(String codProceso,String codTipoEvaluacion);
	public List<EvaluacionesByEvaluadorBean> getEvaluacionesRevisadas(String codProceso,String codEvaluador);
}
