package com.tecsup.SGA.DAO.reclutamiento;

import java.util.*;
public interface PostulanteDao {
	public List getAllPostulante(String nombres, String apellidos,
			String areasInteres, String profesion, String egresado, String grado,
			String codProceso, String codEtapa);
	
	public List GetPostulanteByEmail(String emailPostulante);
}
