package com.tecsup.SGA.DAO;

import java.util.*;
import com.tecsup.SGA.bean.*;
public interface SedeDAO {
	
	/**
	 * @param codEvaluador
	 * @return Retorna las sedes de un evaluador.
	 */
	public List<SedeBean> GetAllSedes();
}
