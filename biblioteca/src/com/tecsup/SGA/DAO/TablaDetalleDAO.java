package com.tecsup.SGA.DAO;

import java.util.*;

public interface TablaDetalleDAO {
	public List getAllTablaDetalle(String codPadre, String codDetalle, String descripcion
								, String valor1, String valor2, String valor3, String valor4
								, String valor5, String tipo );
	public List getAllInstitucion(String codGiroInsti);
	
	public String InsertTablaDetalle(String tipId, String inDescripcion, String inDscValor1
			, String inDscValor2, String inDscValor3, String inDscValor4
			, String inDdscValor5, String usuCrea);
 
	public String UpdateTablaDetalle(String tipId, String tpDes,String descripcion
			,String valor1,String valor2,String valor3
			,String valor4,String valor5, String usuModi);

	
	public String DeleteTablaDetalle(String tipId, String cadCod,String nroReg, String usuModi);

	public String InsertarTablaDetalle(String tipId, String descripcion, String valor1
			, String valor2, String usuCrea);
	
	public String EliminarTablaDetalle(String tipId,String valor1
			, String cadCod,String nroReg, String usuModi);
	
	public String ActualizarTablaDetalle(String tipId, String tpDes, String descripcion
			, String valor1, String valor2, String tipo
			, String usuModi);
	
	
}
