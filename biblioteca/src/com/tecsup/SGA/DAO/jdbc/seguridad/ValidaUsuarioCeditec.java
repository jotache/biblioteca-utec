package com.tecsup.SGA.DAO.jdbc.seguridad;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ValidaUsuarioCeditec extends StoredProcedure {

	private static Log log = LogFactory.getLog(ValidaUsuarioCeditec.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_SEGURIDAD + ".PckSeguridad.ValidaUsuarioCeditec";
	
	private static final String pUsuario = "pUsuario";
    private static final String pClave = "pClave";
    private static final String pResultado = "pResultado";
    
    public ValidaUsuarioCeditec(DataSource datasource){
    	super(datasource,SPROC_NAME);
    	declareParameter(new SqlParameter(pUsuario, OracleTypes.CHAR));
        declareParameter(new SqlParameter(pClave, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(pResultado, OracleTypes.INTEGER));
        compile();
    }
    
    public Map execute(String usuario , String clave) {
    	
		log.info("**** INI " + SPROC_NAME + " ****");
		log.info("pUsuario:"+usuario);     	
		log.info("**** INI " + SPROC_NAME + " ****"); 
		
		Map inputs = new HashMap();
		inputs.put(pUsuario, usuario);
		inputs.put(pClave, clave);
		return super.execute(inputs);
     }
}
