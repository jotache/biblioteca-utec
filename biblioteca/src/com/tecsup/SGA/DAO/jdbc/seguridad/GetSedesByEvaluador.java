package com.tecsup.SGA.DAO.jdbc.seguridad;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.bean.SedeBean;

public class GetSedesByEvaluador extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetSedesByEvaluador.class);
    private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION
    										+ ".PKG_EVAL_COMUN.SP_SEL_SEDES";
    private static final String CODEVALUADOR = "E_V_CODEVALUADOR";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetSedesByEvaluador(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODEVALUADOR, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new SedesMapper()));
        compile();
    }

    public Map execute(String codEvaluador) {
    	
    	log.info("**** INI " + SPROC_NAME + " ****");
    	log.info("CODEVALUADOR:"+codEvaluador);    	
    	log.info("**** INI " + SPROC_NAME + " ****");
    	
    	Map inputs = new HashMap();
    	
        inputs.put(CODEVALUADOR, codEvaluador);

        return super.execute(inputs);

    }
    
    final class SedesMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SedeBean sedeBean = new SedeBean();
        	
        	sedeBean.setCodSede(rs.getString("codSede") == null ? "" : rs.getString("codSede"));
        	sedeBean.setDscSede(rs.getString("dscSede") == null ? "" : rs.getString("dscSede"));

        	return sedeBean;
        }

    }
}


