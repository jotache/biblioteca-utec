package com.tecsup.SGA.DAO.jdbc.seguridad;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UsuarioBiblioUTEC extends StoredProcedure {
	private static Log log = LogFactory.getLog(UsuarioBiblioUTEC.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_SEGURIDAD + ".PckSeguridad.obtienedatosBiblioCeditec";
	private static final String pusuario = "pusuario";
	private static final String pcodigo = "pcodigo";
	private static final String pnombre = "pnombre";
	private static final String papellido = "papellido";
	private static final String pcorreo = "pcorreo";		
	private static final String pespecialidad = "pespecialidad";
	private static final String ptipousuario = "ptipousuario";
	private static final String presultado = "presultado";
	private static final String pciclo = "pciclo";
	private static final String pcod_especialidad = "pcod_especialidad";
	
	public UsuarioBiblioUTEC() {
		// TODO Auto-generated constructor stub
	}

	public UsuarioBiblioUTEC(DataSource ds) {
		super(ds, SPROC_NAME);
		declareParameter(new SqlParameter(pusuario, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(pcodigo, OracleTypes.NUMBER));
        declareParameter(new SqlOutParameter(pnombre, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(papellido, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(pcorreo, OracleTypes.VARCHAR));  
        declareParameter(new SqlOutParameter(pciclo, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(pcod_especialidad, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(pespecialidad, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(ptipousuario, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(presultado, OracleTypes.INTEGER));
        compile();
	}

	public UsuarioBiblioUTEC(JdbcTemplate jdbcTemplate, String name) {
		super(jdbcTemplate, name);
		// TODO Auto-generated constructor stub
	}

	public Map execute(String username) {
		log.info("**** INI " + SPROC_NAME + " ****");
	   	log.info("pUsuario:"+ username);
	   	log.info("**** FIN " + SPROC_NAME + " ****");
	   	Map inputs = new HashMap();
        inputs.put(pusuario, username);
        return super.execute(inputs);	   
	}
	
}
