package com.tecsup.SGA.DAO.jdbc.seguridad;
//JHPR 2008-06-19
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

	public class GetUsuarioBiblio extends StoredProcedure {
		private static Log log = LogFactory.getLog(GetUsuarioBiblio.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_SEGURIDAD 
			+ ".PckSeguridad.ObtieneUsuarioBiblio";
	private static final String V_CODSUJETO = "V_CODSUJETO";
	private static final String V_CODUSUARIO = "V_CODUSUARIO";
	private static final String V_OUT_USUARIO = "V_OUT_USUARIO";
	private static final String V_OUT_CLAVE = "V_OUT_CLAVE";
	private static final String presultado = "presultado";
	
	public GetUsuarioBiblio(DataSource dataSource){
		super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(V_CODSUJETO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(V_CODUSUARIO, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(V_OUT_USUARIO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(V_OUT_CLAVE, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(presultado, OracleTypes.INTEGER));
        compile();
	}
	
	public Map execute(String codSujeto,String idUsuario){
		
		log.info("**** INI " + SPROC_NAME + " ****");
    	log.info("V_CODSUJETO:"+codSujeto);
    	log.info("V_CODUSUARIO:"+idUsuario);
    	log.info("**** INI " + SPROC_NAME + " ****");
		
		Map inputs = new HashMap();	
        inputs.put(V_CODSUJETO, codSujeto);
        inputs.put(V_CODUSUARIO, idUsuario);
        return super.execute(inputs);
	}
}
