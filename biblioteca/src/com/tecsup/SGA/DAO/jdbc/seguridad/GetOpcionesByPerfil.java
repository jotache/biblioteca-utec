package com.tecsup.SGA.DAO.jdbc.seguridad;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.bean.SeguridadBean;

public class GetOpcionesByPerfil extends StoredProcedure {

	private static Log log = LogFactory.getLog(GetOpcionesByPerfil.class);
			
    private static final String SPROC_NAME = CommonConstants.ESQ_SEGURIDAD 
    										+ ".PckSeguridad.ObtieneAccesos";
    private static final String pUsuario = "pUsuario";
    private static final String pSistema = "pSistema";
    private static final String pAcceso = "pAcceso";
    private static final String pResultado = "pResultado";
    
    public GetOpcionesByPerfil(DataSource dataSource){
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(pUsuario, OracleTypes.CHAR));
        declareParameter(new SqlParameter(pSistema, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(pAcceso, OracleTypes.CURSOR, new OpcionesMapper()));
        declareParameter(new SqlOutParameter(pResultado, OracleTypes.INTEGER));
        compile();
    }

    public Map execute(String usuario, String sistema) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("**** INI " + SPROC_NAME + " ****");
    	log.info("pUsuario:"+usuario);
    	log.info("pSistema:"+sistema);
    	log.info("**** INI " + SPROC_NAME + " ****");
    	
        inputs.put(pUsuario, usuario);
        inputs.put(pSistema, sistema);

        return super.execute(inputs);

    }
    
    final class OpcionesMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SeguridadBean seguridadBean = new SeguridadBean();
        	seguridadBean.setAcceso(rs.getString("acceso") == null ? "" : rs.getString("acceso"));
        	seguridadBean.setFiltro(rs.getString("filtro") == null ? "" : rs.getString("filtro"));
        	seguridadBean.setOpcion(rs.getString("opcion") == null ? "" : rs.getString("opcion"));

        	return seguridadBean;
        }

    }
}


