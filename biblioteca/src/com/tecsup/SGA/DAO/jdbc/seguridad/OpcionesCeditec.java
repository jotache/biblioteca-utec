package com.tecsup.SGA.DAO.jdbc.seguridad;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.jdbc.seguridad.GetOpcionesByPerfil.OpcionesMapper;
import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.common.CommonConstants;

public class OpcionesCeditec extends StoredProcedure {

	private static Log log = LogFactory.getLog(OpcionesCeditec.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_SEGURIDAD + ".PckSeguridad.ObtieneAccesosCeditec";		
	private static final String pUsuario = "pUsuario";
    private static final String pSistema = "pSistema";
    private static final String pAcceso = "pAcceso";
    private static final String pResultado = "pResultado";
    
	public OpcionesCeditec() {
		// TODO Auto-generated constructor stub
	}

	public OpcionesCeditec(DataSource ds) {
		super(ds, SPROC_NAME);
		declareParameter(new SqlParameter(pUsuario, OracleTypes.CHAR));
        declareParameter(new SqlParameter(pSistema, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(pAcceso, OracleTypes.CURSOR, new OpcionesMapper()));
        declareParameter(new SqlOutParameter(pResultado, OracleTypes.INTEGER));
        compile();
	}

	public OpcionesCeditec(JdbcTemplate jdbcTemplate, String name) {
		super(jdbcTemplate, name);
		// TODO Auto-generated constructor stub
	}

    public Map execute(String usuario, String sistema) {    	
    	Map inputs = new HashMap();    	
    	log.info("**** INI " + SPROC_NAME + " ****");
    	log.info("pUsuario:"+usuario);
    	log.info("pSistema:"+sistema);
    	log.info("**** INI " + SPROC_NAME + " ****");    	
        inputs.put(pUsuario, usuario);
        inputs.put(pSistema, sistema);

        return super.execute(inputs);
    }
    
	final class OpcionesMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {        	
        	SeguridadBean seguridadBean = new SeguridadBean();
        	seguridadBean.setAcceso(rs.getString("acceso") == null ? "" : rs.getString("acceso"));
        	seguridadBean.setFiltro(rs.getString("filtro") == null ? "" : rs.getString("filtro"));
        	seguridadBean.setOpcion(rs.getString("opcion") == null ? "" : rs.getString("opcion"));
        	return seguridadBean;
        }

    }
}
