//JHPR: 2008-04-09 Clase agregada para modificar la clave de acceso.
package com.tecsup.SGA.DAO.jdbc.seguridad;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GrabarFirma extends StoredProcedure {
	
	 private static final String SPROC_NAME = CommonConstants.ESQ_SEGURIDAD + ".PckSeguridad.GrabaFirma";
	 private static final String SPROC_NAME_CEDITEC = CommonConstants.ESQ_SEGURIDAD + ".PckSeguridad.GrabaFirmaCeditec";
	 private static final String pUsuario = "pUsuario";
	 private static final String pClave = "pClave";
	 private static final String pResultado = "pResultado";
	
	public GrabarFirma(DataSource ds,String entidad) {
				
		super(ds, entidad.equals("TECSUP")?SPROC_NAME:SPROC_NAME_CEDITEC);
		
		declareParameter(new SqlParameter(pUsuario, OracleTypes.CHAR));
		declareParameter(new SqlParameter(pClave, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(pResultado, OracleTypes.INTEGER));
		compile();
	}
	
	public Map execute(String usuario, String clave) {
		Map inputs = new HashMap();
		inputs.put(pUsuario, usuario);
		inputs.put(pClave, clave);
		return super.execute(inputs);
	}	

}
