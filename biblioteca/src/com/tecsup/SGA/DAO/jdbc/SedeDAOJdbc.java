package com.tecsup.SGA.DAO.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.SedeDAO;
import com.tecsup.SGA.bean.SedeBean;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.DAO.jdbc.comun.GetAllSedes;

public class SedeDAOJdbc extends JdbcDaoSupport implements SedeDAO {

	public List<SedeBean> GetAllSedes() {
		GetAllSedes getAllSedes = new GetAllSedes(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSedes.execute();
		
		if (!outputs.isEmpty())
		{
			return (ArrayList<SedeBean>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

}
