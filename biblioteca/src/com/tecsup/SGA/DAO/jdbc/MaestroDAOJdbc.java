package com.tecsup.SGA.DAO.jdbc;

import java.util.List;
import java.util.HashMap;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.jdbc.comun.*;
import com.tecsup.SGA.DAO.reclutamiento.*;
import com.tecsup.SGA.DAO.jdbc.mantenimiento.*;
import com.tecsup.SGA.DAO.MaestroDAO;

public class MaestroDAOJdbc extends JdbcDaoSupport implements MaestroDAO{
	public List getAllDepartamento()
	{
		GetAllDepartamento getAllDepartamento = new GetAllDepartamento(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDepartamento.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllProvincia(String departamento)
	{
		GetAllProvincia getAllProvincia = new GetAllProvincia(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProvincia.execute(departamento);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllDistrito(String provincia) 
	{
		GetAllDistrito getAllDistrito = new GetAllDistrito(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDistrito.execute(provincia);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}