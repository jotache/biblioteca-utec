package com.tecsup.SGA.DAO.jdbc;

import java.util.List;
import java.util.HashMap;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.jdbc.comun.GetAllInstitucion;
import com.tecsup.SGA.DAO.reclutamiento.*;
import com.tecsup.SGA.DAO.jdbc.mantenimiento.*;
import com.tecsup.SGA.DAO.TablaDetalleDAO;
public class TablaDetalleDAOJdbc extends JdbcDaoSupport implements TablaDetalleDAO{
	public List getAllTablaDetalle(String codPadre, String codDetalle, String descripcion
			, String valor1, String valor2, String valor3, String valor4
			, String valor5, String tipo )
	{
		GetAllTablaDetalle getAllProcesosBandeja = new GetAllTablaDetalle(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProcesosBandeja.execute(codPadre, codDetalle, descripcion
											, valor1, valor2, valor3, valor4
											, valor5, tipo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllInstitucion(String codGiroInsti) 
	{
		GetAllInstitucion getAllInstitucion = new GetAllInstitucion(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllInstitucion.execute(codGiroInsti);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String InsertTablaDetalle(String tipId, String inDescripcion, String inDscValor1
			, String inDscValor2, String inDscValor3, String inDscValor4
			, String inDscValor5, String usuCrea)
	{
		InsertTablaDetalle InsertTablaDetalle = new InsertTablaDetalle(this.getDataSource());
			
			HashMap inputs = (HashMap)InsertTablaDetalle.execute(tipId, inDescripcion, inDscValor1
					, inDscValor2, inDscValor3, inDscValor4, inDscValor5, usuCrea);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public String UpdateTablaDetalle(String tipId, String tpDes,String descripcion
			,String valor1,String valor2,String valor3
			,String valor4,String valor5, String usuModi)
	{
		UpdateTablaDetalle UpdateTablaDetalle = new UpdateTablaDetalle(this.getDataSource());
	
		HashMap inputs = (HashMap)UpdateTablaDetalle.execute( tipId,  tpDes, descripcion
				, valor1, valor2, valor3
				, valor4, valor5,  usuModi);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteTablaDetalle(String tipId, String cadCod,String nroReg, String usuModi)
	{
		DeleteTablaDetalle DeleteTablaDetalle = new DeleteTablaDetalle(this.getDataSource());
		HashMap inputs = (HashMap)DeleteTablaDetalle.execute(tipId, cadCod, nroReg, usuModi);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertarTablaDetalle(String tipId, String descripcion, String valor1
			, String valor2, String usuCrea)
	{
		InsertarTablaDetalle InsertarTablaDetalle = new InsertarTablaDetalle(this.getDataSource());
		HashMap inputs = (HashMap)InsertarTablaDetalle.execute(tipId, descripcion, valor1, valor2, usuCrea);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String EliminarTablaDetalle(String tipId,String valor1
			, String cadCod,String nroReg, String usuModi)
	{
	EliminarTablaDetalle EliminarTablaDetalle = new EliminarTablaDetalle(this.getDataSource());
	HashMap inputs = (HashMap)EliminarTablaDetalle.execute(tipId, valor1
			, cadCod, nroReg, usuModi);
	if(!inputs.isEmpty())
	{
		return (String)inputs.get("S_V_RETVAL");
	}
	return null;
	}
	
	
	public String ActualizarTablaDetalle(String tipId, String tpDes,String descripcion
			,String valor1,String valor2,String tipo
			,String usuModi)
	{
		ActualizarTablaDetalle ActualizarTablaDetalle = new ActualizarTablaDetalle(this.getDataSource());
		HashMap inputs = (HashMap)ActualizarTablaDetalle.execute(tipId, tpDes, descripcion, valor1, valor2, tipo, usuModi);
		if(!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
/*	public List getLstCilos(String codPeriodo){

			GetAllCiclosCat getAllCiclosCat = new GetAllCiclosCat(this.getDataSource());
			
			HashMap outputs = (HashMap)getAllCiclosCat.execute(codPeriodo);
			{
				return (List)outputs.get("S_C_RECORDSET");//VERIFICAR
			}
		
	}
 * */
	
}
