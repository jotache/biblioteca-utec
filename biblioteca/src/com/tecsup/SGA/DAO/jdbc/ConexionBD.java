package com.tecsup.SGA.DAO.jdbc;

import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import oracle.jdbc.driver.OracleConnection;

public class ConexionBD {
	
	private static OracleConnection con = null;
	
	public static OracleConnection obtenerConexion(String esquema) throws SQLException {
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			String context = "jdbc/" + esquema;			
			DataSource datasource = (DataSource) envContext.lookup(context);
			con = (OracleConnection) datasource.getConnection();
		} catch (NamingException ex) {
			throw new SQLException("No se pudo encontrar el DataSource.");
		} catch (SQLException ex) {
			throw new SQLException("No se pudo obtener una conexion.");
		}
		return con;
	}
	
}
