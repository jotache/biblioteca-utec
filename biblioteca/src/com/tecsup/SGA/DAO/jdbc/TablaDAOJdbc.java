package com.tecsup.SGA.DAO.jdbc;

import java.util.List;
import java.util.HashMap;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.reclutamiento.*;
import com.tecsup.SGA.DAO.jdbc.mantenimiento.GetAllTabla;
import com.tecsup.SGA.DAO.TablaDAO;


public class TablaDAOJdbc extends JdbcDaoSupport implements TablaDAO{

	public List getAllTabla(String mantenimientoTipo, String sistemaTipo)
	{
		GetAllTabla getAllTablaTipo = new GetAllTabla(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllTablaTipo.execute(mantenimientoTipo,sistemaTipo);
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
	}
	
}
