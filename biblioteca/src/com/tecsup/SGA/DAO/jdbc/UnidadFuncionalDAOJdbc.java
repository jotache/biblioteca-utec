package com.tecsup.SGA.DAO.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.UnidadFuncionalDAO;
import com.tecsup.SGA.DAO.jdbc.comun.GetAllUnidadFuncional;

public class UnidadFuncionalDAOJdbc extends JdbcDaoSupport implements UnidadFuncionalDAO{
	
	public List getAllUnidadFuncinal() {
		GetAllUnidadFuncional getAllUnidadFuncional = new GetAllUnidadFuncional(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllUnidadFuncional.execute();
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}
