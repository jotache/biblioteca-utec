package com.tecsup.SGA.DAO.jdbc.mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActualizarTablaDetalle extends StoredProcedure{
	private static Log log = LogFactory.getLog(ActualizarTablaDetalle.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL 
 	+ ".pkg_gen_comun.SP_ACT_TIPO_EVALUACION";
private static final String E_C_TIPT_ID = "E_C_TIPT_ID";
private static final String E_C_TTDE_ID = "E_C_TTDE_ID";
private static final String E_V_TTDE_DESCRIPCION = "E_V_TTDE_DESCRIPCION";
private static final String E_V_TTDE_VALOR1 = "E_V_TTDE_VALOR1";
private static final String E_V_TTDE_VALOR2 = "E_V_TTDE_VALOR2";
private static final String E_C_TIPO= "E_C_TIPO";
private static final String E_V_TTDE_USU_MODI = "E_V_TTDE_USU_MODI";
private static final String S_V_RETVAL = "S_V_RETVAL";

public ActualizarTablaDetalle(DataSource dataSource) {
super(dataSource, SPROC_NAME);
declareParameter(new SqlParameter(E_C_TIPT_ID, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_C_TTDE_ID, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_V_TTDE_DESCRIPCION, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_TTDE_VALOR1, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_V_TTDE_VALOR2, OracleTypes.VARCHAR));
declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_V_TTDE_USU_MODI, OracleTypes.CHAR));
declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
compile();
}

public Map execute(String tipId, String tpDes,String descripcion
		,String valor1,String valor2,String tipo
		,String usuModi) {

	log.info("****** INI " + SPROC_NAME + " *****");
	log.info("E_C_TIPT_ID:"+tipId);
	log.info("E_C_TTDE_ID:"+tpDes);
	log.info("E_V_TTDE_DESCRIPCION:"+descripcion);
	log.info("E_V_TTDE_VALOR1:"+valor1);
	log.info("E_V_TTDE_VALOR2:"+valor2);    	
	log.info("E_C_TIPO:"+tipo);
	log.info("E_V_TTDE_USU_MODI:"+usuModi);
	log.info("****** FIN " + SPROC_NAME + " *****");
	
Map inputs = new HashMap();

inputs.put(E_C_TIPT_ID, tipId);
inputs.put(E_C_TTDE_ID, tpDes);
inputs.put(E_V_TTDE_DESCRIPCION, descripcion);
inputs.put(E_V_TTDE_VALOR1, valor1);
inputs.put(E_V_TTDE_VALOR2, valor2);
inputs.put(E_C_TIPO, tipo);
inputs.put(E_V_TTDE_USU_MODI, usuModi);



return super.execute(inputs);

}
}
