package com.tecsup.SGA.DAO.jdbc.mantenimiento;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllTablaDetalle extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAllTablaDetalle.class);
    private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL 
    										+ ".pkg_gen_comun.sp_sel_tipo_tabla_detalle";
    private static final String TIPT_ID = "E_C_TIPT_ID";
    private static final String TTDE_ID = "E_C_TTDE_ID";
    private static final String TTDE_DESCRIPCION = "E_V_TTDE_DESCRIPCION";
    private static final String TTDE_VALOR1 = "E_V_TTDE_VALOR1";
    private static final String TTDE_VALOR2 = "E_V_TTDE_VALOR2";
    private static final String TTDE_VALOR3 = "E_V_TTDE_VALOR3";
    private static final String TTDE_VALOR4 = "E_V_TTDE_VALOR4";
    private static final String TTDE_VALOR5 = "E_V_TTDE_VALOR5";
    private static final String TIPO = "E_C_TIPO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllTablaDetalle(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(TIPT_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TTDE_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TTDE_DESCRIPCION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TTDE_VALOR1, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TTDE_VALOR2, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TTDE_VALOR3, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TTDE_VALOR4, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TTDE_VALOR5, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(TIPO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProcesoMapper()));
        compile();
    }

    public Map execute(String codTipoTabla, String codTablaDetalle, String descripcion
    					, String dscValor1, String dscValor2, String dscValor3
    					, String dscValor4, String dscValor5, String tipo) {
    	
    	Map inputs = new HashMap();
    	
    	/*log.info("****** INI " + SPROC_NAME + " *****");
    	log.info("TIPT_ID:"+codTipoTabla);
    	log.info("TTDE_ID:"+codTablaDetalle);
    	log.info("TTDE_DESCRIPCION:"+descripcion);
    	log.info("dscValor1:"+dscValor1);
    	log.info("dscValor2:"+dscValor2);
    	log.info("dscValor3:"+dscValor3);
    	log.info("dscValor4:"+dscValor4);
    	log.info("dscValor5:"+dscValor5);
    	log.info("TIPO:"+tipo);
    	log.info("****** FIN " + SPROC_NAME + " *****");*/
    	
        inputs.put(TIPT_ID, codTipoTabla);
        inputs.put(TTDE_ID, codTablaDetalle);
        inputs.put(TTDE_DESCRIPCION, descripcion);
        inputs.put(TTDE_VALOR1, dscValor1);
        inputs.put(TTDE_VALOR2, dscValor2);
        inputs.put(TTDE_VALOR3, dscValor3);
        inputs.put(TTDE_VALOR4, dscValor4);
        inputs.put(TTDE_VALOR5, dscValor5);
        inputs.put(TIPO, tipo);
        
        return super.execute(inputs);

    }
    
    final class ProcesoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
        	tipoTablaDetalle.setCodTipoTabla(rs.getString("CODIGO_PADRE"));
        	tipoTablaDetalle.setCodTipoTablaDetalle(rs.getString("CODIGO_DETALLE"));
        	tipoTablaDetalle.setDescripcion(rs.getString("DESCRIPCION"));
        	tipoTablaDetalle.setDscValor1(rs.getString("VALOR1"));
        	tipoTablaDetalle.setDscValor2(rs.getString("VALOR2"));
        	tipoTablaDetalle.setDscValor3(rs.getString("VALOR3"));
        	tipoTablaDetalle.setDscValor4(rs.getString("VALOR4"));
        	tipoTablaDetalle.setDscValor5(rs.getString("VALOR5"));
        	tipoTablaDetalle.setDscValor6(rs.getString("desc_padre_hijo"));
            return tipoTablaDetalle;
        }

    }
}


