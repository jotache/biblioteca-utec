package com.tecsup.SGA.DAO.jdbc.mantenimiento;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.TipoDetalle;

public class GetAllTabla extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllTabla.class);
	 private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL 
		                                      + ".pkg_gen_comun.sp_sel_tipo_tabla";
     private static final String E_C_TIPT_IND_MANTTO = "e_c_tipt_ind_mantto";
     private static final String E_C_TIPT_SISTEMA = "e_c_tipt_sistema";
     private static final String RECORDSET = "S_C_RECORDSET";

     public GetAllTabla(DataSource dataSource) {
         super(dataSource, SPROC_NAME);
         declareParameter(new SqlParameter(E_C_TIPT_IND_MANTTO, OracleTypes.VARCHAR));
         declareParameter(new SqlParameter(E_C_TIPT_SISTEMA, OracleTypes.VARCHAR));
         declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new TipoMapper()));
         compile();
     }

     public Map execute(String MantenimientoTipo, String SistemaTipo) {

    	log.info("****** INI " + SPROC_NAME + " *****");
    	log.info("E_C_TIPT_IND_MANTTO:"+MantenimientoTipo);
    	log.info("E_C_TIPT_SISTEMA:"+SistemaTipo);
    	log.info("****** INI " + SPROC_NAME + " *****");
    		
               Map inputs = new HashMap();
               inputs.put(E_C_TIPT_IND_MANTTO, MantenimientoTipo);
               inputs.put(E_C_TIPT_SISTEMA, SistemaTipo);
               return super.execute(inputs);
     }
    
     final class TipoMapper implements RowMapper {
         
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
         	
         	TipoDetalle tipoDetalle = new TipoDetalle();
         	tipoDetalle.setCodTipoTipo(rs.getString("TIPT_ID"));
         	tipoDetalle.setDenominacionTipo(rs.getString("TIPT_DENOMINACION"));
         	tipoDetalle.setDescripcionTipo(rs.getString("TIPT_DESCRIPCION"));
         	tipoDetalle.setMantenimientoTipo(rs.getString("TIPT_IND_MANTTO"));
         	tipoDetalle.setSistemaTipo(rs.getString("TIPT_SISTEMA"));

         	
             return tipoDetalle;
         }

     }
     
     
}
