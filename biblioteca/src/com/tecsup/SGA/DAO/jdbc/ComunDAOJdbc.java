package com.tecsup.SGA.DAO.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.ComunDAO;
import com.tecsup.SGA.DAO.biblioteca.jdbc.gestionMaterial.GetSedesBibPorCodUsuario;
import com.tecsup.SGA.DAO.jdbc.comun.GetAlumnos;
import com.tecsup.SGA.DAO.jdbc.comun.GetFechaActual;
import com.tecsup.SGA.bean.FechaBean;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetSedesByEvaluador;
public class ComunDAOJdbc extends JdbcDaoSupport implements ComunDAO {

	public FechaBean getFecha() {
		GetFechaActual getFechaActual = new GetFechaActual(this.getDataSource());
		
		HashMap outputs = (HashMap)getFechaActual.execute();
		
		ArrayList<FechaBean> arrFecha = new ArrayList<FechaBean>();
		if (!outputs.isEmpty())
		{
			arrFecha = (ArrayList<FechaBean>)outputs.get("S_C_RECORDSET");
			if ( arrFecha != null)
				if ( arrFecha.size() > 0 )
					return arrFecha.get(0);
		}
		return null;
	}

	public List<SedeBean> GetSedesByEvaluador(String codEvaluador) {
		
		GetSedesByEvaluador getSedesByEvaluador = new GetSedesByEvaluador(this.getDataSource());
		HashMap outputs = (HashMap)getSedesByEvaluador.execute(codEvaluador);
		
		if (!outputs.isEmpty())
		{
			return (List<SedeBean>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	/**
	 * JHPR 2008-7-3
	 */
	public UsuarioSeguridad getUsuarioById(String tipoBusqueda, String dato) {		
		JdbcTemplate jt = getJdbcTemplate();
		String sql = "select p.codpersona, p.apepaterno, p.apematerno,p.nomuno,p.nomdos " +
						" from general.gen_persona p , seguridad.seg_usuario s " +
						" where p.codpersona=s.codsujeto " ;
				
		if (tipoBusqueda.equals("CODIGO")) 
			sql = sql + " and p.codpersona=?" ;		
		else			
			sql = sql + " and trim(s.usuario)=?" ;
						
		System.out.println("antes de instanciar::" + sql + " :: " + dato);
		
		final UsuarioSeguridad user = new UsuarioSeguridad();
		final Object[] params = new Object[] {dato};
		//System.out.println("Previo lleno datos");
		jt.query(sql, params, new RowCallbackHandler() {
			public void processRow(ResultSet rs) throws SQLException {
				/*System.out.println("Entro");
				System.out.println("user codigo:" + rs.getLong("codpersona"));
				System.out.println("user apellido:" + rs.getString("apepaterno"));*/
				user.setCodUsuario(String.valueOf(rs.getLong("codpersona")));
				String nomCompleto = (rs.getString("apepaterno")==null?"":rs.getString("apepaterno"))+ " " + 
						(rs.getString("apematerno")==null?"":rs.getString("apematerno")).trim() + " " + 
						(rs.getString("nomuno")==null?"":rs.getString("nomuno")).trim()+ " " +
						(rs.getString("nomdos")==null?"":rs.getString("nomdos"));
				user.setNomUsuario(nomCompleto);
			}
		});
				
		return user;						
	}

	//JHPR 2008-09-08
	public List<Alumno> getAlumnos(String apellPaterno, String apellMaterno,
			String nombre1,String codCarnet) {
		GetAlumnos getAlumnos = new GetAlumnos(this.getDataSource());
		HashMap outputs = (HashMap) getAlumnos.execute(apellPaterno, apellMaterno, nombre1, codCarnet);
		if (!outputs.isEmpty()){
			return (List<Alumno>) outputs.get(CommonConstants.RET_CURSOR);			
		}
		return null;
	}

	

}
