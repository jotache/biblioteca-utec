package com.tecsup.SGA.DAO.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.CentroCostoDAO;
import com.tecsup.SGA.DAO.jdbc.comun.GetCentrosCostoBySede;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CentroCosto;

public class CentroCostoDAOJdbc extends JdbcDaoSupport implements CentroCostoDAO {

	public List<CentroCosto> GetAllCentrosCosto(String sede, String perfil, String codUsuario) {
		GetCentrosCostoBySede getCentrosCostoBySede = new GetCentrosCostoBySede(this.getDataSource());
		
		HashMap outputs = (HashMap)getCentrosCostoBySede.execute(sede, perfil, codUsuario);
		
		if (!outputs.isEmpty())
		{
			return (ArrayList<CentroCosto>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

}
