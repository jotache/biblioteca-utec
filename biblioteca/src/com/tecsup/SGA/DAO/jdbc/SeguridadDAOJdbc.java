package com.tecsup.SGA.DAO.jdbc;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.SeguridadDAO;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetDatosUsuarioBiblio;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetUsuarioBiblio;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetValidaUsuario;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetOpcionesByPerfil;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetDatosUsuario;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetSedesByEvaluador;
import com.tecsup.SGA.DAO.jdbc.seguridad.GrabarFirma;
import com.tecsup.SGA.DAO.jdbc.seguridad.GetPerfilByUsuario;
import com.tecsup.SGA.DAO.jdbc.seguridad.OpcionesCeditec;
import com.tecsup.SGA.DAO.jdbc.seguridad.UsuarioBiblioUTEC;
import com.tecsup.SGA.DAO.jdbc.seguridad.ValidaUsuarioCeditec;
import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.UsuarioSeguriBib;
//CACT: 2008-04-15
import com.tecsup.SGA.bean.UsuarioPerfil;

public class SeguridadDAOJdbc extends JdbcDaoSupport implements SeguridadDAO {

	private static Log log = LogFactory.getLog(SeguridadDAOJdbc.class);

	private static final String OBTENER_DATOS_USUARIO = "{call SEGURIDAD.PckSeguridad.obtienedatosBiblio(?,?,?,?,?,?,?,?,?,?)}";
	private static final String OBTENER_DATOS_USUARIO_UTEC = "{call SEGURIDAD.PckSeguridad.obtienedatosBiblioCeditec(?,?,?,?,?,?,?,?,?,?)}";
	
	private static final String OBTENER_ACCESOS_USUARIO = "{call SEGURIDAD.PckSeguridad.ObtieneAccesos(?,?,?,?)}";
	private static final String OBTENER_ACCESOS_USUARIO_UTEC = "{call SEGURIDAD.PckSeguridad.ObtieneAccesosCeditec(?,?,?,?)}";
	
	private static final String VERIFICA_RESERVA_USUARIO = "{call BIBLIO.PKG_BIB_WEB.SP_VERIFICA_RESERVA_USUARIO(?,?)}";
	private static final String OBTENER_USUARIO_BIBLIO = "{call SEGURIDAD.PckSeguridad.ObtieneUsuarioBiblio(?,?,?,?,?)}";
	
	public int getValidaUsuario(String usuario, String clave) {
		log.info("getValidaUsuario: String usuario, String clave: " + usuario);
		GetValidaUsuario getValidaUsuario = new GetValidaUsuario(this.getDataSource());

		HashMap outputs = (HashMap)getValidaUsuario.execute(usuario, clave);
		if (!outputs.isEmpty()) //Habilitar o deshabilitar seguridad
		{
			int result = ((Integer)outputs.get("pResultado")).intValue();
//			if(result==-1 || result==-5) //usuario no existe
//				result = this.getValidaUsuarioCeditec(usuario, clave);
			//return ((Integer)outputs.get("pResultado")).intValue();
			return result;
		}
		return 0;
	}

	//Validación para usuarios Ceditec
	public int getValidaUsuarioCeditec(String usuario, String clave) {
		ValidaUsuarioCeditec getValidacion = new ValidaUsuarioCeditec(this.getDataSource());
		HashMap outputs = (HashMap) getValidacion.execute(usuario,clave);

		if(!outputs.isEmpty()){
			return ((Integer)outputs.get("pResultado")).intValue();
		}else
			return 0;

	}

	public List<SeguridadBean> getOpciones(String usuario, String sistema) {
		GetOpcionesByPerfil getOpcionesByPerfil = new GetOpcionesByPerfil(this.getDataSource());
		HashMap outputs = (HashMap)getOpcionesByPerfil.execute(usuario, sistema);

		if (!outputs.isEmpty())
		{
			int resultado = ((Integer)outputs.get("pResultado")).intValue();
			if ( resultado != -1 )
				return (List<SeguridadBean>)outputs.get("pAcceso");
		}
		return null;
	}


	
	public List<SeguridadBean> getOpcionesTecsup(String usuario, String sistema){
		
		int respuesta = 0;
		ResultSet rs = null;
		List<SeguridadBean> accesos = new ArrayList<SeguridadBean>();
		SeguridadBean acceso = null;
		try {
			final CallableStatement procedure = obtenerSeguridadProcedimiento(OBTENER_ACCESOS_USUARIO);
			procedure.setString(1, usuario);
			procedure.setString(2, sistema);
			procedure.registerOutParameter(3, OracleTypes.CURSOR);
			procedure.registerOutParameter(4, java.sql.Types.SMALLINT);

			procedure.executeQuery();

			respuesta = procedure.getInt(4);

			if (respuesta != 0) {
				throw new SQLException(null, String.valueOf(respuesta),
						null);
			}

			rs = (ResultSet) procedure.getObject(3);
			if (respuesta == 0) {
				while (rs.next()) {
					acceso = new SeguridadBean();										
					acceso.setOpcion(rs.getString("opcion"));
					acceso.setAcceso(rs.getString("acceso"));										
					acceso.setFiltro(rs.getString("filtro"));
					accesos.add(acceso);
				}
			}
		} catch (SQLException sqle) {
			log.error(sqle);		
		}
		return accesos;		
		
	}
	
	public List<SeguridadBean> getOpcionesCeditec(String usuario, String sistema) {

		OpcionesCeditec getOpcionesByPerfil = new OpcionesCeditec(this.getDataSource());
		HashMap outputs = (HashMap)getOpcionesByPerfil.execute(usuario, sistema);

		List<SeguridadBean> lstOpciones1 = null;

		if (!outputs.isEmpty())
		{
			int resultado = ((Integer)outputs.get("pResultado")).intValue();
			if ( resultado != -1 ){
				lstOpciones1 = (List<SeguridadBean>)outputs.get("pAcceso");
				if(lstOpciones1.size()==0){
					return this.getOpciones(usuario, sistema);
				}else{
					return lstOpciones1;
				}
			}
		}
		return null;
	}

	
	public List<SeguridadBean> getOpcionesUtec(String usuario, String sistema){
		
		int respuesta = 0;
		ResultSet rs = null;
		List<SeguridadBean> accesos = new ArrayList<SeguridadBean>();
		SeguridadBean acceso = null;
		try {
			final CallableStatement procedure = obtenerSeguridadProcedimiento(OBTENER_ACCESOS_USUARIO_UTEC);
			procedure.setString(1, usuario);
			procedure.setString(2, sistema);
			procedure.registerOutParameter(3, OracleTypes.CURSOR);
			procedure.registerOutParameter(4, java.sql.Types.SMALLINT);

			procedure.executeQuery();

			respuesta = procedure.getInt(4);

			if (respuesta != 0) {
				throw new SQLException(null, String.valueOf(respuesta),
						null);
			}

			rs = (ResultSet) procedure.getObject(3);
			if (respuesta == 0) {
				while (rs.next()) {
					acceso = new SeguridadBean();										
					acceso.setOpcion(rs.getString("opcion"));
					acceso.setAcceso(rs.getString("acceso"));										
					acceso.setFiltro(rs.getString("filtro"));
					accesos.add(acceso);
				}
			}
		} catch (SQLException sqle) {
			log.error(sqle);		
		}
		return accesos;		
		
	}	
	
	public UsuarioSeguridad getDatosUsuario(String codUsuario) {
		GetDatosUsuario getDatosUsuario = new GetDatosUsuario(this.getDataSource());
		HashMap outputs = (HashMap)getDatosUsuario.execute(codUsuario);

		UsuarioSeguridad usuarioSeguridad = new UsuarioSeguridad();
		if (!outputs.isEmpty())
		{
			int resultado = ((Integer)outputs.get("pResultado")).intValue();
			if ( resultado != -1 )
			{
				usuarioSeguridad.setIdUsuario(((BigDecimal)outputs.get("pCodigo")).toString());
				usuarioSeguridad.setNomUsuario((String)outputs.get("pNombre"));
			}
		}
		return usuarioSeguridad;
	}

	//JHPR 2008-04-09 Para permitir el cambio de contraseña.
	public int GrabarFirma(String usuario, String clave) {
		GrabarFirma grabarFirma = new GrabarFirma(this.getDataSource(),"TECSUP");
		HashMap outputs = (HashMap) grabarFirma.execute(usuario, clave);
		if (!outputs.isEmpty()) {
			return ((Integer) outputs.get("pResultado")).intValue();
		}
		return 0;
	}

	//JHPR: 2008-04-11 Registrar inicio de sesion en "seguridad.seg_log"
	public int RegistrarSesion(
			boolean ingreso, String estacioncliente, String usuario, String sistema)
			{

		Connection connection = null;
		PreparedStatement stmt = null;

		try {
			connection = this.getDataSource().getConnection();
			String conexion = "";
			conexion = ((ingreso==true) ? "S" : "N" );

			log.info("Registrando: [" + estacioncliente + "][" + usuario + "]");

			stmt = connection.prepareStatement(
					"insert into seguridad.seg_log" +
					" (conexion,ipterminal,usuario,sistema,fechahora)" +
					" values (?,?,?,?,sysdate)");

			stmt.setString(1,conexion);
			stmt.setString(2, estacioncliente);
			stmt.setString(3, usuario);
			stmt.setString(4, sistema);

			int result = stmt.executeUpdate();
			if (stmt!=null) stmt.close();
			if (connection!=null) connection.close();

			return result;

		} catch(SQLException e) {
			e.printStackTrace();
			log.error(".RegistrarSesion(): " + e.getMessage());
		}

		return -1;
	}

	//CACT: 2008-04-15 - Retorna los perfiles de un usuario para un determinado sistema
	public List<UsuarioPerfil> getPerfiles(String usuario, String sistema) {
		GetPerfilByUsuario getPerfilByUsuario = new GetPerfilByUsuario(this.getDataSource());
		HashMap outputs = (HashMap)getPerfilByUsuario.execute(usuario, sistema);

		if (!outputs.isEmpty())
		{
				return (List<UsuarioPerfil>)outputs.get("pAcceso");
		}
		return null;
	}

	//JHPR 2008-06-18
	public UsuarioSeguriBib obtenerUsuarioSeguriBiblio(String codsujeto,String idUsuario) {
		
		GetUsuarioBiblio usuarioBiblio = new GetUsuarioBiblio(this.getDataSource());
		HashMap outputs = (HashMap)usuarioBiblio.execute(codsujeto,idUsuario);

		UsuarioSeguriBib usuarioSeguridad = new UsuarioSeguriBib();
		if (!outputs.isEmpty())
		{
			int resultado = ((Integer)outputs.get("presultado")).intValue();
			log.info("DAOJdbc::obtenerUsuarioSeguriBiblio:resultado:" + resultado);
			if ( resultado != -1 )
			{
				usuarioSeguridad.setCodsujeto(codsujeto);
				usuarioSeguridad.setUsuario(((String)outputs.get("V_OUT_USUARIO")).trim());
				usuarioSeguridad.setClave( outputs.get("V_OUT_CLAVE")!=null? ((String)outputs.get("V_OUT_CLAVE")).trim() : ""  );

			}else{
				usuarioSeguridad = null;				
			}
		}
		return usuarioSeguridad;

	}

	public UsuarioSeguriBib obtenerUsuarioSeguriBiblio2(String codsujeto,String idUsuario) {
		
		UsuarioSeguriBib usuarioSeg = new UsuarioSeguriBib();
		int respuesta=0;
		try{
			final CallableStatement procedure = obtenerSeguridadProcedimiento(OBTENER_USUARIO_BIBLIO);
			procedure.setString(1, codsujeto);
			procedure.setString(2, idUsuario);
			
			procedure.registerOutParameter(3, java.sql.Types.VARCHAR); //usuario
			procedure.registerOutParameter(4, java.sql.Types.VARCHAR); //clave
			procedure.registerOutParameter(5, java.sql.Types.SMALLINT); //resultado
						
			procedure.executeQuery();
			
			respuesta = procedure.getInt(5);
			if (respuesta != 0) {
				throw new SQLException(null, String.valueOf(respuesta),
						null);
			}
			if (respuesta == 0) {
				usuarioSeg.setCodsujeto(codsujeto);
				usuarioSeg.setUsuario(procedure.getString(3));
				usuarioSeg.setClave(procedure.getString(4));
			}
			
								
		}catch (SQLException sqle) {
			log.error(sqle);
		}
		
		
		return usuarioSeg;

	}
	
	public UsuarioSeguridad getDatosUsuarioBiblio(String username) {
		GetDatosUsuarioBiblio dub = new GetDatosUsuarioBiblio(this.getDataSource());
		HashMap outputs = (HashMap)dub.execute(username);
		UsuarioSeguridad usuarioSeguridad = new UsuarioSeguridad();
		if (!outputs.isEmpty())
		{
			int resultado = ((Integer)outputs.get("presultado")).intValue();
			if ( resultado != -1 )
			{
				usuarioSeguridad.setIdUsuario(((BigDecimal)outputs.get("pcodigo")).toString());
				usuarioSeguridad.setNomUsuario((String)outputs.get("pnombre"));
				usuarioSeguridad.setApeUsuario((String)outputs.get("papellido"));
				usuarioSeguridad.setCorreo((String)outputs.get("pcorreo"));
				usuarioSeguridad.setTipoUsuario((String)outputs.get("ptipousuario"));
				usuarioSeguridad.setEspecialidad((String)outputs.get("pespecialidad"));
				usuarioSeguridad.setCiclo((String)outputs.get("pciclo"));
				usuarioSeguridad.setCodigoEspecialidad((String)outputs.get("pcod_especialidad"));
			}
		}
		return usuarioSeguridad;
	}

	public UsuarioSeguridad getDatosUsuarioBiblioCeditec(String username) {
		UsuarioBiblioUTEC dub = new UsuarioBiblioUTEC(this.getDataSource());
		HashMap outputs = (HashMap)dub.execute(username);
		UsuarioSeguridad usuarioSeguridad = new UsuarioSeguridad();
		if (!outputs.isEmpty())
		{
			int resultado = ((Integer)outputs.get("presultado")).intValue();
			if ( resultado != -1 )
			{
				usuarioSeguridad.setIdUsuario(((BigDecimal)outputs.get("pcodigo")).toString());
				usuarioSeguridad.setNomUsuario((String)outputs.get("pnombre"));
				usuarioSeguridad.setApeUsuario((String)outputs.get("papellido"));
				usuarioSeguridad.setCorreo((String)outputs.get("pcorreo"));
				usuarioSeguridad.setTipoUsuario((String)outputs.get("ptipousuario"));
				usuarioSeguridad.setCiclo((String)outputs.get("pciclo"));
				usuarioSeguridad.setCodigoEspecialidad((String)outputs.get("pcod_especialidad"));
				usuarioSeguridad.setEspecialidad((String)outputs.get("pespecialidad"));
			}else{
				return this.getDatosUsuarioBiblio(username);
			}
		}
		return usuarioSeguridad;
	}

	
  @SuppressWarnings("deprecation")
  public CallableStatement obtenerSeguridadProcedimiento(
      final String procedimiento) throws SQLException {
    return ConexionBD.obtenerConexion("sga-seg").prepareCall(procedimiento);
  }
	
  @SuppressWarnings("deprecation")
  public CallableStatement obtenerBilioProcedimiento(
      final String procedimiento) throws SQLException {
    return ConexionBD.obtenerConexion("sga-bib").prepareCall(procedimiento);
  }  
	
	public String GetVerificarReservaUsuario(String usuario){
		String respuesta = "";
		try{
			final CallableStatement procedure = obtenerBilioProcedimiento(VERIFICA_RESERVA_USUARIO);
			procedure.setString(1, usuario);
			procedure.registerOutParameter(2, java.sql.Types.VARCHAR); //resultado
			procedure.executeQuery();
			respuesta = procedure.getString(2);
			
			
		}catch (SQLException sqle) {
			log.error(sqle);
		}
		return respuesta;	
	}
  
  	/*
  	 * 
  	 */
	public UsuarioSeguridad getDatosUsuarioCeditecUtec(String username) {
					
		UsuarioSeguridad usuarioSeg = new UsuarioSeguridad();
		int respuesta = 0;
		try{
			final CallableStatement procedure = obtenerSeguridadProcedimiento(OBTENER_DATOS_USUARIO_UTEC);
			procedure.setString(1, username);
			procedure.registerOutParameter(2, java.sql.Types.INTEGER); //pcodigo
			procedure.registerOutParameter(3, java.sql.Types.VARCHAR); //pnombre
			procedure.registerOutParameter(4, java.sql.Types.VARCHAR); //papellido
			procedure.registerOutParameter(5, java.sql.Types.VARCHAR); //Pcorreo
			procedure.registerOutParameter(6, java.sql.Types.VARCHAR); //ciclo
			procedure.registerOutParameter(7, java.sql.Types.VARCHAR); //cod especialidad
			procedure.registerOutParameter(8, java.sql.Types.VARCHAR); //especialidad
			procedure.registerOutParameter(9, java.sql.Types.VARCHAR); //ptipousuario
			procedure.registerOutParameter(10, java.sql.Types.SMALLINT); //resultado
			
			procedure.executeQuery();
			respuesta = procedure.getInt(10);
			if (respuesta != 0) {
				throw new SQLException(null, String.valueOf(respuesta),
						null);
			}
			if (respuesta == 0) {
												
				usuarioSeg.setIdUsuario(((Integer)procedure.getInt(2)).toString());
				usuarioSeg.setNomUsuario(procedure.getString(3));
				usuarioSeg.setApeUsuario(procedure.getString(4));
				usuarioSeg.setCorreo(procedure.getString(5));				
				usuarioSeg.setCiclo(procedure.getString(6));
				usuarioSeg.setCodigoEspecialidad(procedure.getString(7));
				usuarioSeg.setEspecialidad(procedure.getString(8));
				usuarioSeg.setTipoUsuario(procedure.getString(9));
				
			}
		}catch (SQLException sqle) {
			log.error(sqle);
		}
		
		
		return usuarioSeg;
	}	
	
	
	/*
	 * (non-Javadoc)
	 * @see com.tecsup.SGA.DAO.SeguridadDAO#getDatosUsuario(java.lang.String)
	 */
	public UsuarioSeguridad getDatosUsuarioCeditecTecsup(String codUsuario) {
		
		UsuarioSeguridad usuarioSeg = new UsuarioSeguridad();
		int respuesta = 0;
		try{
			final CallableStatement procedure = obtenerSeguridadProcedimiento(OBTENER_DATOS_USUARIO);
			procedure.setString(1, codUsuario);
			procedure.registerOutParameter(2, java.sql.Types.INTEGER); //pcodigo
			procedure.registerOutParameter(3, java.sql.Types.VARCHAR); //pnombre			
			procedure.registerOutParameter(4, java.sql.Types.VARCHAR); //apellido
			procedure.registerOutParameter(5, java.sql.Types.VARCHAR); //Pcorreo
			procedure.registerOutParameter(6, java.sql.Types.VARCHAR); //ciclo
			procedure.registerOutParameter(7, java.sql.Types.VARCHAR); //cod especialidad
			procedure.registerOutParameter(8, java.sql.Types.VARCHAR); //especialidad
			procedure.registerOutParameter(9, java.sql.Types.VARCHAR); //ptipousuario
			procedure.registerOutParameter(10, java.sql.Types.SMALLINT); //resultado
			
			procedure.executeQuery();
			respuesta = procedure.getInt(10);
			if (respuesta != 0) {
				throw new SQLException(null, String.valueOf(respuesta),
						null);
			}
			if (respuesta == 0) {
												
				usuarioSeg.setIdUsuario(((Integer)procedure.getInt(2)).toString());
				usuarioSeg.setNomUsuario(procedure.getString(3));	
				usuarioSeg.setApeUsuario(procedure.getString(4));
				usuarioSeg.setCorreo(procedure.getString(5));				
				usuarioSeg.setCiclo(procedure.getString(6));
				usuarioSeg.setCodigoEspecialidad(procedure.getString(7));
				usuarioSeg.setEspecialidad(procedure.getString(8));
				usuarioSeg.setTipoUsuario(procedure.getString(9));
				
			}
		}catch (SQLException sqle) {
			log.error(sqle);
		}
		
		
		return usuarioSeg;
	}	
	
	public int GrabarFirmaCeditec(String usuario, String clave) {
		GrabarFirma grabarFirma = new GrabarFirma(this.getDataSource(),"CEDITEC"); //considera usuarios UTEC
		HashMap outputs = (HashMap) grabarFirma.execute(usuario, clave);
		if (!outputs.isEmpty()) {
			return ((Integer) outputs.get("pResultado")).intValue();
		}
		return 0;
	}

	public UsuarioSeguridad ObtenerEstadoUsuario(UsuarioSeguridad user){
		Connection conexion = null;
		String estado = "";
		String sql="";
		try {
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			sql = "select 'S' esactivo from LOGISTICA.LOG_V_USUARIOS U where U.CODUSUARIO='" + user.getIdUsuario()+ "' ";
			log.info(sql);
			stmt.executeQuery(sql);
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				estado = rs.getString("esactivo");
				user.setEstadoActivo(estado);
			}
			rs.close();
			stmt.close();
			conexion.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return user;
	}
	
	public String sedeUsuarioCeditec(String usuario){
		Connection conexion = null;
		String sede = "";
		String sql="";
		try {
			conexion = ConexionBD.obtenerConexion("sga-bib");
			Statement stmt = conexion.createStatement();
			sql = " select cod_sede AS LUGAR from bib_v_usuario u where trim(lower(u.usuario))=trim(lower('" + usuario.toLowerCase().trim() + "')) ";

			stmt.executeQuery(sql);
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				sede = rs.getString("LUGAR");					
			}
			rs.close();
			stmt.close();
			conexion.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return sede;
	}

	public boolean esUsuarioTecsup(String username) {
		Connection conexion = null;
		boolean existe = false;
		String sql="";
		try {
			conexion = this.getDataSource().getConnection();
			Statement stmt = conexion.createStatement();
			sql = " select empresa from ( " +
				" select usuario,esactivo,'TECSUP' empresa from seguridad.seg_usuario where usuario='" + username + "' " +
				" union " +
				" select usuario,esactivo,'UTEC' empresa from seguridad.seg_usuario@dbl_utec.tecsup.edu.pe where usuario='" + username + "' " +
				" ) where esactivo='S'" ;

			stmt.executeQuery(sql);
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				if(rs.getString("empresa").equals("TECSUP"))
					existe = true;
			}
			rs.close();
			stmt.close();
			conexion.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return existe;
	}

	

}
