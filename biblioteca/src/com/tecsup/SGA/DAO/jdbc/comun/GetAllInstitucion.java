package com.tecsup.SGA.DAO.jdbc.comun;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllInstitucion extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllInstitucion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL + ".pkg_gen_comun.sp_sel_institucion";
    private static final String CODIGO = "E_C_CODGIRO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllInstitucion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(CODIGO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new InstitucionMapper()));
        compile();
    }

    public Map execute(String codGiroInsti) {
    	
    	log.info("**** INI " + SPROC_NAME+ " ****");
    	log.info("E_C_CODGIRO:" + codGiroInsti);
    	log.info("**** FIN " + SPROC_NAME+ " ****");
    	
    	Map inputs = new HashMap();    	
        inputs.put(CODIGO, codGiroInsti);
        
        return super.execute(inputs);

    }
    
    final class InstitucionMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
        	
        	tipoTablaDetalle.setCodTipoTablaDetalle(rs.getString("CODINSTITUCION"));
        	tipoTablaDetalle.setDescripcion(rs.getString("RAZONSOCIAL"));

            return tipoTablaDetalle;
        }
    }
	
}
