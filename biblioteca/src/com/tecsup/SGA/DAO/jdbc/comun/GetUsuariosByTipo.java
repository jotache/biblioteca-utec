package com.tecsup.SGA.DAO.jdbc.comun;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.jdbc.comun.GetAllUnidadFuncional.UnidadFuncionalMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Usuario;

public class GetUsuariosByTipo extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetUsuariosByTipo.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_RECLUTAMIENTO + ".PKG_RECL_PROCESO.SP_SEL_JEFE_EVAL";
	private static final String CODROL = "E_C_CODROL";
	private static final String SISTEMA = "E_C_SISTEMA";
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetUsuariosByTipo(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(CODROL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(SISTEMA, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new UsuarioMapper()));
        
        compile();
    }
    
    public Map execute(String rol, String sistema) {
    	
    	log.info("**** INI " + SPROC_NAME+ " ****");
    	log.info("CODROL:" + rol);
    	log.info("SISTEMA:" + sistema);    	 
    	log.info("**** FIN " + SPROC_NAME+ " ****");
    	
    	HashMap map = new HashMap();
    	
    	map.put(CODROL, rol);
    	map.put(SISTEMA, sistema);
    	
        return super.execute(map);
    }
    
    final class UsuarioMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Usuario usuario = new Usuario();
        	
        	usuario.setCodUsuario( rs.getString("CODIGOUSUARIO") == null ? "" : rs.getString("CODIGOUSUARIO") );
        	usuario.setNomUsuario( rs.getString("NOMBREUSUARIO") == null ? "" : rs.getString("NOMBREUSUARIO") );
        	
            return usuario;
        }
    }
}
