package com.tecsup.SGA.DAO.jdbc.comun;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;

public class GetAlumnos extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetAlumnos.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL + ".PKG_GEN_COMUN.SP_SEL_BUSQUEDA_ALUMNOS";
	private static final String CODCARNET = "V_CODCARNET";
	private static final String NOMBRE = "V_NOMBRE";
	private static final String APELLPATERNO = "V_APELLPATERNO";
	private static final String APELLMATERNO = "V_APELLMATERNO";
    private static final String RECORDSET = "S_C_RECORDSET";

    public GetAlumnos(DataSource dataStore){
    	super(dataStore,SPROC_NAME);
    	declareParameter(new SqlParameter(CODCARNET, OracleTypes.VARCHAR));
    	declareParameter(new SqlParameter(NOMBRE, OracleTypes.VARCHAR));
    	declareParameter(new SqlParameter(APELLPATERNO, OracleTypes.VARCHAR));
    	declareParameter(new SqlParameter(APELLMATERNO, OracleTypes.VARCHAR));
    	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new AlumnoMapper()));
    	compile();
    }
    
    public Map execute(String apellPaterno, String apellMaterno, String nombre1, String codcarnet){
    	
    	log.info("**** INI " + SPROC_NAME+ " ****");
    	log.info("CODCARNET:" + codcarnet);
    	log.info("NOMBRE:" + nombre1);
    	log.info("APELLPATERNO:" + apellPaterno);
    	log.info("APELLMATERNO:" + apellMaterno);
    	log.info("**** FIN " + SPROC_NAME+ " ****");
    	
    	HashMap map = new HashMap();
    	map.put(CODCARNET, codcarnet);
    	map.put(NOMBRE, nombre1);
    	map.put(APELLPATERNO, apellPaterno);
    	map.put(APELLMATERNO, apellMaterno);    	
    	return super.execute(map);
    }
    
    final class AlumnoMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			Alumno alumno = new Alumno();
			alumno.setCodAlumno(rs.getString("CODALUMNO")==null?"":rs.getString("CODALUMNO"));
			alumno.setCodCarnet(rs.getString("CODCARNET")==null?"":rs.getString("CODCARNET"));
			alumno.setNombreAlumno(rs.getString("NOMBRE")==null?"":rs.getString("NOMBRE"));
			alumno.setEspecialidad(rs.getString("ESPE")==null?"":rs.getString("ESPE"));
			alumno.setCodCiclo(rs.getString("CICLO")==null?"":rs.getString("CICLO"));
			return alumno;
		}    	
    }
}
