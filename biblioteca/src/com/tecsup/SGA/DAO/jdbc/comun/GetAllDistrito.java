package com.tecsup.SGA.DAO.jdbc.comun;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Maestro;

public class GetAllDistrito extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllDistrito.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_MAESTRO + ".pkg_mae_comun.sp_sel_distrito";
	private static final String PROVINCIA = "E_C_PROVINCIA";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllDistrito(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(PROVINCIA, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new DistritoMapper()));
        compile();
    }

    public Map execute(String provincia) {
    	
    	log.info("**** INI " + SPROC_NAME+ " ****");
    	log.info("PROVINCIA:" + provincia);
    	log.info("**** FIN " + SPROC_NAME+ " ****");
    	
    	Map inputs = new HashMap();
    	inputs.put(PROVINCIA, provincia);
    	
        return super.execute(inputs);
    }
    
    final class DistritoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Maestro maestro = new Maestro();
        	
        	maestro.setCodigo(rs.getString("codigo"));
        	maestro.setDscValor1(rs.getString("DISTRITO"));

            return maestro;
        }
    }
}
