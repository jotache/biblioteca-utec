package com.tecsup.SGA.DAO.jdbc.comun;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.bean.FechaBean;

public class GetFechaActual extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetFechaActual.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_EVALUACION + ".PKG_EVAL_COMUN.SP_SEL_FECHAACTUAL";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetFechaActual(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new FechaMapper()));
        compile();
    }

    public Map execute() {    
    	log.info("**** EXEC " + SPROC_NAME);
        return super.execute(new HashMap());
    }
    
    final class FechaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	FechaBean fechaBean = new FechaBean();
        	
        	fechaBean.setFecha(rs.getString("FECHAACTUAL") == null ? "" :  rs.getString("FECHAACTUAL"));
        	fechaBean.setHora(rs.getString("HORA_ACTUAL") == null ? "" :  rs.getString("HORA_ACTUAL"));
        	fechaBean.setMinuto(rs.getString("MINUTO_ACTUAL") == null ? "" :  rs.getString("MINUTO_ACTUAL"));
        	fechaBean.setSegundo(rs.getString("SEGUNDO_ACTUAL") == null ? "" :  rs.getString("SEGUNDO_ACTUAL"));
        	//fechaBean.setDescripcion(rs.getString("") == null ? "" :  rs.getString(""));
        	
            return fechaBean;
        }
    }
}
