package com.tecsup.SGA.DAO.jdbc.comun;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.bean.SedeBean;

public class GetAllSedes extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllSedes.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL + ".PKG_GEN_COMUN.SP_SEL_SEDES_SISTEMA";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllSedes(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new SedeMapper()));
        compile();
    }

    public Map execute() {   
    	log.info("**** EXEC " + SPROC_NAME);
        return super.execute(new HashMap());
    }
    
    final class SedeMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SedeBean sedeBean = new SedeBean();
        	
        	sedeBean.setCodSede(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO"));
        	sedeBean.setDscSede(rs.getString("DESCRIPCION") == null ? "" : rs.getString("DESCRIPCION"));
        	
            return sedeBean;
        }
    }
}
