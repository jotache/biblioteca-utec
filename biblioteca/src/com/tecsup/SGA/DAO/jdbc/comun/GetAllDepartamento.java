package com.tecsup.SGA.DAO.jdbc.comun;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Maestro;

public class GetAllDepartamento extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllDepartamento.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_MAESTRO + ".pkg_mae_comun.sp_sel_departamento";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllDepartamento(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new DepartamentoMapper()));
        compile();
    }

    public Map execute() {
    	log.info("*** EXEC " + SPROC_NAME);
    	Map inputs = new HashMap();      
        return super.execute(inputs);
    }
    
    final class DepartamentoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Maestro maestro = new Maestro();
        	
        	maestro.setCodigo(rs.getString("codigo"));
        	maestro.setDscValor1(rs.getString("departamento"));

            return maestro;
        }
    }
}
