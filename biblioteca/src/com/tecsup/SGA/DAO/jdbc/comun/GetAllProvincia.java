package com.tecsup.SGA.DAO.jdbc.comun;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.Maestro;

public class GetAllProvincia extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllProvincia.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_MAESTRO + ".pkg_mae_comun.sp_sel_provincia";
	private static final String DEPARTAMENTO = "E_C_DEPARTAMENTO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllProvincia(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(DEPARTAMENTO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProvinciaMapper()));
        compile();
    }

    public Map execute(String departamento) {
    	
    	log.info("**** INI " + SPROC_NAME+ " ****");
    	log.info("E_C_DEPARTAMENTO:" + departamento);
    	log.info("**** FIN " + SPROC_NAME+ " ****");
    	
    	Map inputs = new HashMap();    	
    	inputs.put(DEPARTAMENTO, departamento);
    	
        return super.execute(inputs);
    }
    
    final class ProvinciaMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Maestro maestro = new Maestro();
        	
        	maestro.setCodigo(rs.getString("codigo"));
        	maestro.setDscValor1(rs.getString("PROVINCIA"));

            return maestro;
        }
    }
}
