package com.tecsup.SGA.DAO.jdbc.comun;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tecsup.SGA.common.*;
import com.tecsup.SGA.modelo.CentroCosto;

public class GetCentrosCostoBySede extends StoredProcedure {
	private static Log log = LogFactory.getLog(GetCentrosCostoBySede.class);
    private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL
    										+ ".PKG_GEN_COMUN.SP_SEL_CENTROS_COSTO";
    private static final String SEDE = "V_C_SEDE";
    private static final String E_V_PERFIL = "E_V_PERFIL";
    private static final String E_V_USUARIO = "E_V_USUARIO";
    private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetCentrosCostoBySede(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_PERFIL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_USUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new CentrosCostoMapper()));
        compile();
    }

    public Map execute(String codSede, String perfil, String codUsuario) {
    	
    	log.info("**** INI " + SPROC_NAME+ " ****");
    	log.info("SEDE:" + codSede);
    	log.info("E_V_PERFIL:" + perfil);
    	log.info("E_V_USUARIO:" + codUsuario);    	
    	log.info("**** FIN " + SPROC_NAME+ " ****");
    	
    	Map inputs = new HashMap();
        inputs.put(SEDE, codSede);
        inputs.put(E_V_PERFIL, perfil);
        inputs.put(E_V_USUARIO, codUsuario);

        return super.execute(inputs);

    }
    
    final class CentrosCostoMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CentroCosto centroCosto = new CentroCosto();
        	
        	centroCosto.setCodSede(rs.getString("SEDE") == null ? "" : rs.getString("SEDE"));
        	centroCosto.setCodTecsup(rs.getString("CODIGO_CECO") == null ? "" : rs.getString("CODIGO_CECO"));
        	centroCosto.setDescripcion(rs.getString("DESCRIPCION") == null ? "" : rs.getString("DESCRIPCION"));
        	centroCosto.setEstado(rs.getString("ESTADO") == null ? "" : rs.getString("ESTADO"));
        	
        	return centroCosto;
        }

    }
}