package com.tecsup.SGA.DAO;

import java.util.*;

import com.tecsup.SGA.bean.*;
import com.tecsup.SGA.modelo.Alumno;
public interface ComunDAO {
	
	/**
	 * @return Objecto fecha con lo datos de la base de datos
	 */
	public FechaBean getFecha();
	
	/**
	 * @param codEvaluador
	 * @return Retorna las sedes de un evaluador.
	 */
	public List<SedeBean> GetSedesByEvaluador(String codEvaluador);
	
	/** JHPR 2008-7-3
	 * Busca un usuario y retorna codigo, nombre completo.
	 * La Busqueda es por tipo= si es por codigo o por username. 
	 *  
	 */
	public UsuarioSeguridad getUsuarioById(String tipoBusqueda,String dato);
		
	/**
	 * JHPR 2008-09-05
	 * Busqueda de alumnos por apellidos y nombres
	 * @param apellPaterno
	 * @param apellMaterno
	 * @param nombre1
	 * @return
	 */
	public List<Alumno> getAlumnos(String apellPaterno, String apellMaterno, String nombre1,String codCarnet);
	
}
