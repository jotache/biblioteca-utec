package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ObtenerCorreoUsuario extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
			+ ".pkg_enc_config_perfil.sp_obtener_correo_usuario";
	
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ObtenerCorreoUsuario(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codUsuario){
		
		Map inputs = new HashMap();
		inputs.put(E_V_COD_USUARIO,codUsuario);
		return super.execute(inputs);
		
	}
	
}
