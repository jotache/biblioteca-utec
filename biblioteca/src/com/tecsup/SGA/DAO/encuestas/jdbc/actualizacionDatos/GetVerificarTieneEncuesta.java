package com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.Usuario;

public class GetVerificarTieneEncuesta extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_actuliza_datos_alumno.sp_verifica_si_tiene_encuesta";
		
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetVerificarTieneEncuesta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjectaMapper()));
        compile();
    }

    public Map execute(String codUsuario) {
    	
    	Map inputs = new HashMap();

    	inputs.put(E_V_COD_USUARIO, codUsuario);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjectaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	        	
        	Usuario usuario = new Usuario();
        	        	
        	usuario.setTieneEncuesta(rs.getString("TIENE_ENCUESTAS") == null ? "": rs.getString("TIENE_ENCUESTAS"));        	
        	
        	return usuario;
        }
    }
}