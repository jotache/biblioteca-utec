package com.tecsup.SGA.DAO.encuestas.jdbc;


import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;


import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllConfiguracionPerfil;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCondicionCotizacion;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteEncuestaManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteEncuestaFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeletePreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllAgregarFormatoManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllDatosCabeceraEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllDatosPersonal;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllEncFormatoBusqueda;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllEncuestaManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllEncuestasPublicadas;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllFormatoEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllPreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllVerEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetCodigoEncuestadoAnonimo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetEstadoEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertAgregarFormatoManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertCopiaFormatoEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertRespuestaByEncuestado;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateCodPreguntasAbiertas;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateConfiguracionPerfil;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertPreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateGeneraEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdatePreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllEncuestaByFormato;

import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateEncuestaByFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.AmpliarProgramacionEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.AplicarEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllAmbitoGeneral;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllCiclos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllCursos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllDepartamentos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllPerfilEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllPerfilesByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProductos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProfesorPCC;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProfesorPFR;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProgramas;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllTipoDocente;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllTipoPersonal;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAmpliarProgByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.InsertPerfilEncuesta;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class ConfiguracionGeneracionDAOJdbc extends JdbcDaoSupport implements ConfiguracionGeneracionDAO{
	
		public String obtenertxhCodEncuestadoByEncuestado(String nroEncuesta){
			
			GetCodigoEncuestadoAnonimo getCodigoEncuestadoAnonimo = new GetCodigoEncuestadoAnonimo(this.getDataSource());

			HashMap inputs = (HashMap)getCodigoEncuestadoAnonimo.execute(nroEncuesta);
			
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
		}
		
		public String obtenerCodEstadoEncuesta(String nroEncuesta) {
			
			GetEstadoEncuesta getEstadoEncuesta =  new GetEstadoEncuesta(this.getDataSource());
			HashMap inputs = (HashMap)getEstadoEncuesta.execute(nroEncuesta);
			
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
		}
		
	    public String insertFormato(String tipoAplicacion,
			String tipoServicio,
			String tipoEncuesta,
			String numero,
			String nombre,
			String duracion,
			String observacion,
			String codSede,
			String usuCrea) {
		
		InsertFormato insertFormato = new InsertFormato(this.getDataSource());
		
		HashMap inputs = (HashMap)insertFormato.execute( tipoAplicacion,
				 tipoServicio,
				 tipoEncuesta,
				 numero,
				 nombre,
				 duracion,
				 observacion,
				 codSede,
				 usuCrea);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updateFormato(String idEncuesta,
			String tipoAplicacion,
			String tipoServicio,
			String tipoEncuesta,
			String numero,
			String nombre,
			String duracion,
			String observacion,
			String codSede,
			String usuCrea) {
		
		UpdateFormato updateFormato = new UpdateFormato(this.getDataSource());
		
		HashMap inputs = (HashMap)updateFormato.execute( 
				 idEncuesta,
				 tipoAplicacion,
				 tipoServicio,
				 tipoEncuesta,
				 numero,
				 nombre,
				 duracion,
				 observacion,
				 codSede,
				 usuCrea
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List<FormatoEncuesta> getAllFormato(String codTipoAplicacion, 
			String codTipoServicio, String codTipoEncuesta, String codTipoEstado, String nombreEncuesta,
			String codResponsable, String nroEncuesta) {
		
		GetAllFormato getAllFormato = new GetAllFormato(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllFormato.execute(codTipoAplicacion, codTipoServicio, 
				codTipoEncuesta, codTipoEstado, nombreEncuesta, codResponsable, nroEncuesta);
		
		if (!outputs.isEmpty())
		{
			return (List<FormatoEncuesta>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	//GetAllEncuestaByFormato
	public List<FormatoEncuesta> GetAllEncuestaByFormato(String codFormatoEncuesta) {
		
		GetAllEncuestaByFormato getAllEncuestaByFormato = new GetAllEncuestaByFormato(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEncuestaByFormato.execute(codFormatoEncuesta);
		if (!outputs.isEmpty())
		{
			return (List<FormatoEncuesta>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	
	
	public String insertAlternativa(String codEncuesta,
			String codGrupo,
			String codPregunta,
			String alternativa,
			String descripcion,			
			String peso,			
			String usuCrea) {
		
		InsertAlternativa insertAlternativa = new InsertAlternativa(this.getDataSource());
		
		HashMap inputs = (HashMap)insertAlternativa.execute( codEncuesta,
				 codGrupo,
				 codPregunta,
				 alternativa,
				 descripcion,			
				 peso,			
				 usuCrea);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	
	public List getAllAlternativa(String codEncuesta,String codGrupo,String codPregunta,String codAlternativa) {
		
		GetAllAlternativa getAllAlternativa = new GetAllAlternativa(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllAlternativa.execute( codEncuesta,codGrupo,codPregunta,codAlternativa);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	
	public String updateAlternativa(
			String codEncuesta,
			String codAlternativa,
			String codGrupo,
			String codPregunta,
			String alternativa,
			String descripcion,
			String peso,			
			String usuCrea) {
		
		UpdateAlternativa updateAlternativa = new UpdateAlternativa(this.getDataSource());
		
		HashMap inputs = (HashMap)updateAlternativa.execute(
				 codEncuesta,
				 codAlternativa,
				 codGrupo,
				 codPregunta,
				 alternativa,
				 descripcion,
				 peso,			
				 usuCrea		
		);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String deleteAlternativa(
			String codEncuesta,
			String codAlternativa,
			String usuario		
		) {
		
		DeleteAlternativa deleteAlternativa = new DeleteAlternativa(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteAlternativa.execute( 
				 codEncuesta,
				 codAlternativa,
				 usuario
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String insertGruposByEncuesta(
			String codEncuesta,
			String codSeccion,
			String nomGrupo,
			String peso,
			String indAlternativa,//0 no;1:si		
			String usuCrea
		) {
		
		InsertGruposByEncuesta insertGruposByEncuesta = new InsertGruposByEncuesta(this.getDataSource());
		
		HashMap inputs = (HashMap)insertGruposByEncuesta.execute( 
				 codEncuesta,
				 codSeccion,
				 nomGrupo,
				 peso,
				 indAlternativa,//0 no;1:si		
				 usuCrea
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String deleteGruposByEncuesta(String codEncuesta, String codGrupo,
			String usuario) {
		DeleteGruposByEncuesta deleteGruposByEncuesta = new DeleteGruposByEncuesta(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteGruposByEncuesta.execute( 
				 codEncuesta,  codGrupo, usuario
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	
	}
	
	public List getAllGruposByEncuesta(String codEncuesta,String codGrupo) {
		
		GetAllGruposByEncuesta getAllGruposByEncuesta = new GetAllGruposByEncuesta(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllGruposByEncuesta.execute(  codEncuesta, codGrupo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	
	public String updateGruposByEncuesta(
			String codEncuesta,
			String codSeccion,
			String codGrupo,
			String nomGrupo,
			String peso,
			String indAlternativa,//0 no;1:si		
			String usuCrea
		) {
		
		UpdateGruposByEncuesta updateGruposByEncuesta = new UpdateGruposByEncuesta(this.getDataSource());
		
		HashMap inputs = (HashMap)updateGruposByEncuesta.execute( 
				 codEncuesta,
				 codSeccion,
				 codGrupo,
				 nomGrupo,
				 peso,
				 indAlternativa,//0 no;1:si		
				 usuCrea
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List getAllPreguntasByGrupo(String codEncuesta, String codGrupo,
			String codPregunta) {
		GetAllPreguntasByGrupo getAllPreguntasByGrupo = new GetAllPreguntasByGrupo(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllPreguntasByGrupo.execute(   codEncuesta,  codGrupo,
				 codPregunta);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	//
	public String UpdateEncuestaByFormato(String codFormatoEncuesta, String cadCodSeccion, String nroTotalReg, 
			String codUsuario) {
		
		UpdateEncuestaByFormato updateEncuestaByFormato = new UpdateEncuestaByFormato(this.getDataSource());
		
		HashMap inputs = (HashMap)updateEncuestaByFormato.execute(codFormatoEncuesta, cadCodSeccion,
				nroTotalReg, codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String insertPreguntasByGrupo(
			String codEncuesta,
			String codGrupo,
			String pregunta,
			String tipoPregunta,
			String indObligatorio,
			String indUnica,
			String peso,
			String indAlternativa,
			String usuCrea
		) {
		
		InsertPreguntasByGrupo insertPreguntasByGrupo = new InsertPreguntasByGrupo(this.getDataSource());
		
		HashMap inputs = (HashMap)insertPreguntasByGrupo.execute( 
				 codEncuesta,
				 codGrupo,
				 pregunta,
				 tipoPregunta,
				 indObligatorio,
				 indUnica,
				 peso,
				 indAlternativa,
				 usuCrea
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String updatePreguntasByGrupo(
			String codEncuesta,
			String codGrupo,
			String codPregunta,
			String pregunta,
			String tipoPregunta,
			String indObligatorio,
			String indUnica,
			String peso,
			String indAlternativa,
			String usuCrea
		) {
		
		UpdatePreguntasByGrupo updatePreguntasByGrupo = new UpdatePreguntasByGrupo(this.getDataSource());
		
		HashMap inputs = (HashMap)updatePreguntasByGrupo.execute( 
				 codEncuesta,
				 codGrupo,
				 codPregunta,
				 pregunta,
				 tipoPregunta,
				 indObligatorio,
				 indUnica,
				 peso,
				 indAlternativa,
				 usuCrea
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String deletePreguntasByGrupo(String codEncuesta,
			String codPregunta,
			String usuario) {
		DeletePreguntasByGrupo deletePreguntasByGrupo = new DeletePreguntasByGrupo(this.getDataSource());
		
		HashMap inputs = (HashMap)deletePreguntasByGrupo.execute( 
				 codEncuesta,
				 codPregunta,
				 usuario
				 );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	
	}

	public List getAllFormatoEncuesta(String codEncuesta) {
		GetAllFormatoEncuesta getAllFormatoEncuesta = new GetAllFormatoEncuesta(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllFormatoEncuesta.execute(codEncuesta);
		
		if (!outputs.isEmpty())
		{
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	public List<TipoTablaDetalle> GetAllConfiguracionPerfil(String codTabla, String codDetalle){
		
		GetAllConfiguracionPerfil getAllConfiguracionPerfil = new GetAllConfiguracionPerfil(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllConfiguracionPerfil.execute(codTabla, codDetalle);
		if(!outputs.isEmpty()){
			return (List<TipoTablaDetalle>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public String UpdateConfiguracionPerfil(String codEncuesta, 
			String codResponsable, String indicadorManual, String usuario) {
		
		UpdateConfiguracionPerfil updateConfiguracionPerfil = new UpdateConfiguracionPerfil(this.getDataSource());		
		
		HashMap inputs = (HashMap)updateConfiguracionPerfil.execute(codEncuesta, 
				codResponsable, indicadorManual, usuario); 
				 
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}//
	public List<Usuario> GetAllDatosPersonal(String dscPrimerNombre, String dscSegundoNombre, String dscApellidoPat, 
			String dscApellidoMat, String codTipoPersonal){
		
		GetAllDatosPersonal getAllDatosPersonal = new GetAllDatosPersonal(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDatosPersonal.execute(dscPrimerNombre, dscSegundoNombre, dscApellidoPat, dscApellidoMat, 
				codTipoPersonal);
		if(!outputs.isEmpty()){
			return (List<Usuario>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	/*
	 * PERFIL
	 */	
	

	public List GetAllAgregarFormatoManual(String codEncuesta) {
		
		GetAllAgregarFormatoManual getAllAgregarFormatoManual = new GetAllAgregarFormatoManual(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllAgregarFormatoManual.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}	
	
	public String insertAgregarFormatoManual(
			String codEncuesta,
			String cadIdPreguntaCerrada,
			String cadIdRespuestaCerrada,
			String nroIdPreguntaCerrada,
			String cadIdPreguntaAbierta,
			String cadIdRespuestaAbierta,
			String nroIdPreguntaAbierta,
			String usuCrea,
			String indRegistro){
		
		InsertAgregarFormatoManual insertAgregarFormatoManual = new InsertAgregarFormatoManual(this.getDataSource());		
		
		HashMap inputs = (HashMap)insertAgregarFormatoManual.execute(
				codEncuesta,
				cadIdPreguntaCerrada,
				cadIdRespuestaCerrada,
				nroIdPreguntaCerrada,
				cadIdPreguntaAbierta,
				cadIdRespuestaAbierta,
				nroIdPreguntaAbierta,
				usuCrea,
				indRegistro);
		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
		
	}
	
	
	public List getAllEncuestaManual(String codEncuesta) {
		
		GetAllEncuestaManual getAllEncuestaManual = new GetAllEncuestaManual(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllEncuestaManual.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	
	public String deleteEncuestaManual(String codEncuesta, String codPerfil,String epdec_id,String codUsuario) {
				
		DeleteEncuestaManual deleteEncuestaManual = new DeleteEncuestaManual(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteEncuestaManual.execute(codEncuesta, codPerfil, epdec_id, codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;		
	}
	
	public String updateGeneraEncuesta(String codEncuesta,		
			String usuCrea) {
		
		UpdateGeneraEncuesta updateGeneraEncuesta = new UpdateGeneraEncuesta(this.getDataSource());
		
		HashMap inputs = (HashMap)updateGeneraEncuesta.execute(codEncuesta, usuCrea);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;		
	}
	public String InsertCopiaFormatoEncuesta(String codEncuesta) {
		
		InsertCopiaFormatoEncuesta insertCopiaFormatoEncuesta = new InsertCopiaFormatoEncuesta(this.getDataSource());
		
		HashMap inputs = (HashMap)insertCopiaFormatoEncuesta.execute(codEncuesta);		
		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String DeleteEncuestaFormato(String codEncuesta, String codUsuario) {		
		
		DeleteEncuestaFormato deleteEncuestaFormato = new DeleteEncuestaFormato(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteEncuestaFormato.execute(codEncuesta, codUsuario);		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllEncuestasPublicadas(String codUsuario){
		
		GetAllEncuestasPublicadas getAllEncuestasPublicadas = new GetAllEncuestasPublicadas(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllEncuestasPublicadas.execute(codUsuario);				
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	
	public List getAllVerEncuesta(String codEncuesta) {
		
		GetAllVerEncuesta getAllVerEncuesta = new GetAllVerEncuesta(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllVerEncuesta.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	
	public String insertRespuestaByEncuestado(String codEncuesta,
			String codPerfil,
			String epdecId,
			String cadIdPreguntaCerrada,
			String cadIdRespuestaCerrada,
			String nroIdPreguntaCerrada,
			String cadIdPreguntaAbierta,
			String cadIdRespuestaAbierta,
			String nroIdPreguntaAbierta,
			String usuCrea		) {
		
		InsertRespuestaByEncuestado insertRespuestaByEncuestado = new InsertRespuestaByEncuestado(this.getDataSource());
		
		HashMap inputs = (HashMap)insertRespuestaByEncuestado.execute(codEncuesta, codPerfil, epdecId, cadIdPreguntaCerrada, cadIdRespuestaCerrada, nroIdPreguntaCerrada, cadIdPreguntaAbierta, cadIdRespuestaAbierta, nroIdPreguntaAbierta, usuCrea);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;		
	}
	
	
	//public List getAllDatosCabeceraEncuesta(String codEncuesta, String codProfesor,String codProducto,String codPrograma) {
	public List getAllDatosCabeceraEncuesta(String codEncuesta, String codProfesor) {	
		
		GetAllDatosCabeceraEncuesta getAllDatosCabeceraEncuesta = new GetAllDatosCabeceraEncuesta(this.getDataSource());										
		
		//HashMap outputs = (HashMap)getAllDatosCabeceraEncuesta.execute(codEncuesta, codProfesor,codProducto,codPrograma);		
		HashMap outputs = (HashMap)getAllDatosCabeceraEncuesta.execute(codEncuesta, codProfesor);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllEncFormatoBusqueda(String codTipoAplicacion, String codTipoServicio, String codTipoEncuesta, 
    		String codTipoEstado, String nombreEncuesta, String codResponsable, String nroEncuesta,
    		String nomEncuesta) {	
		
		GetAllEncFormatoBusqueda getAllEncFormatoBusqueda = new GetAllEncFormatoBusqueda(this.getDataSource());										
		
		//HashMap outputs = (HashMap)getAllDatosCabeceraEncuesta.execute(codEncuesta, codProfesor,codProducto,codPrograma);		
		HashMap outputs = (HashMap)getAllEncFormatoBusqueda.execute(codTipoAplicacion, codTipoServicio, codTipoEncuesta, codTipoEstado, nombreEncuesta, codResponsable, nroEncuesta,nomEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public String marcarRespuestasAbiertas(String nroEncuesta,
			String codPerfilEnc,String codRespAbierta) {
		
			UpdateCodPreguntasAbiertas updateCodPreguntasAbiertas = new UpdateCodPreguntasAbiertas(this.getDataSource());
			HashMap inputs = (HashMap)updateCodPreguntasAbiertas.execute(nroEncuesta,codPerfilEnc,codRespAbierta);
		
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			
		return null;
	}


	
}