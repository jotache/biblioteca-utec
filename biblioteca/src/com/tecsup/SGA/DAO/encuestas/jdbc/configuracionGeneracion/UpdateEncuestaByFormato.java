package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateEncuestaByFormato extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".pkg_enc_configuracion.sp_act_encuesta_x_formato";
	/*E_V_FENCC_ID IN VARCHAR2,
E_V_CADENA_TIPTC_COD_SECCION IN VARCHAR2,
E_V_TOTAL_REG IN VARCHAR,
E_V_FSECC_USU_CREA  IN VARCHAR2*/
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_CADENA_TIPTC_COD_SECCION = "E_V_CADENA_TIPTC_COD_SECCION"; 
	private static final String E_V_TOTAL_REG = "E_V_TOTAL_REG"; 
	private static final String E_V_FSECC_USU_CREA = "E_V_FSECC_USU_CREA";		
		
	private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public UpdateEncuestaByFormato(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CADENA_TIPTC_COD_SECCION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_TOTAL_REG, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FSECC_USU_CREA, OracleTypes.VARCHAR));
		
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codFormatoEncuesta, String cadCodSeccion, String nroTotalReg, 
			String codUsuario) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_FENCC_ID, codFormatoEncuesta);
	inputs.put(E_V_CADENA_TIPTC_COD_SECCION, cadCodSeccion);
	inputs.put(E_V_TOTAL_REG, nroTotalReg);
	inputs.put(E_V_FSECC_USU_CREA, codUsuario);
		
	return super.execute(inputs);
	}

}
