package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class AmpliarProgramacionEncuesta extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".pkg_enc_config_perfil.sp_act_ampliar_programacion";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_FEC_INI = "E_V_FEC_INI"; 
	private static final String E_V_HOR_INI = "E_V_HOR_INI"; 
	private static final String E_V_FEC_FIN = "E_V_FEC_FIN";		
	private static final String E_V_HOR_FIN = "E_V_HOR_FIN";	
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";	
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	

	public AmpliarProgramacionEncuesta(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FEC_INI, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_HOR_INI, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FEC_FIN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_HOR_FIN, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));		
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(
			String codEncuesta,
			String fechaInicio,
			String horaInicio,
			String fechaFin,
			String horaFin,
			String codUsuario			
			) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_FENCC_ID, codEncuesta);
	inputs.put(E_V_FEC_INI, fechaInicio);
	inputs.put(E_V_HOR_INI, horaInicio);
	inputs.put(E_V_FEC_FIN, fechaFin);
	inputs.put(E_V_HOR_FIN, horaFin);
	inputs.put(E_V_COD_USUARIO, codUsuario);
	
	return super.execute(inputs);
	}
}