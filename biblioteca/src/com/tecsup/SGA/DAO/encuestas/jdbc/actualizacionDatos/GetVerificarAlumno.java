package com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaAlumno;

public class GetVerificarAlumno extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_actuliza_datos_alumno.SP_VERIFICA_SIES_ALUMNO";
//E_V_COD_USUARIO IN VARCHAR2
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetVerificarAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjectaMapper()));
        compile();
    }

    public Map execute(String codAlumno) {
    	
    	Map inputs = new HashMap();

    	inputs.put(E_V_COD_USUARIO, codAlumno);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjectaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	EncuestaAlumno encuestaAlumno= new EncuestaAlumno();
        	        	
        	encuestaAlumno.setCodPerfil(rs.getString("ES_ALUMNO") == null ? "": rs.getString("ES_ALUMNO"));
        	
        	return encuestaAlumno;
        }
    }

}
