package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Usuario;

public class GetAllDatosPersonal extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.sp_sel_datos_personal";
	
	private static final String E_V_NOM_PRIMERO = "E_V_NOM_PRIMERO";
	private static final String E_V_NOM_SEGUNDO = "E_V_NOM_SEGUNDO";
	private static final String E_V_APEPAT = "E_V_APEPAT";
	private static final String E_V_APEMAT = "E_V_APEMAT";
	private static final String E_C_CODTIPOPERSONAL = "E_C_CODTIPOPERSONAL";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllDatosPersonal(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_NOM_PRIMERO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_SEGUNDO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_APEPAT, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_APEMAT, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOPERSONAL, OracleTypes.VARCHAR));
              
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String dscPrimerNombre, String dscSegundoNombre, String dscApellidoPat, String dscApellidoMat, String codTipoPersonal) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_NOM_PRIMERO, dscPrimerNombre);
    	inputs.put(E_V_NOM_SEGUNDO, dscSegundoNombre);
    	inputs.put(E_V_APEPAT, dscApellidoPat);
    	inputs.put(E_V_APEMAT, dscApellidoMat);
    	inputs.put(E_C_CODTIPOPERSONAL, codTipoPersonal);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Usuario usuario= new Usuario();
        	        	
        	usuario.setCodPersonal(rs.getString("COD_PERSONAL") == null ? "": rs.getString("COD_PERSONAL"));
        	usuario.setNombrePersonal(rs.getString("NOM_PERSONAL") == null ? "": rs.getString("NOM_PERSONAL"));
        	usuario.setCodTipoPersonal(rs.getString("COD_TIPO_PERSONAL") == null ? "": rs.getString("COD_TIPO_PERSONAL"));
        	usuario.setNombreTipoPersonal(rs.getString("NOM_TIPO_PERSONAL") == null ? "": rs.getString("NOM_TIPO_PERSONAL"));
        	usuario.setCodUsuario(rs.getString("USUARIO") == null ? "": rs.getString("USUARIO"));
        	        	
        	return usuario;
        }
    }

}
