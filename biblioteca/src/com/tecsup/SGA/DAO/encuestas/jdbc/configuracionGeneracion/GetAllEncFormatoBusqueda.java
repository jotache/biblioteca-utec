package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class GetAllEncFormatoBusqueda extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.SP_SEL_ENC_FORMATO_BUSQUEDA";

	private static final String E_C_TIPTC_TIPO_APLICACION = "E_C_TIPTC_TIPO_APLICACION";
	private static final String E_C_TIPCT_TIPO_SERVICIO = "E_C_TIPCT_TIPO_SERVICIO";
	private static final String E_C_TIPTC_TIPO_ENCUESTA = "E_C_TIPTC_TIPO_ENCUESTA";
	private static final String E_V_TIPTC_ESTADO = "E_V_TIPTC_ESTADO";
	private static final String E_C_FENCC_ID = "E_C_FENCC_ID";
	private static final String E_V_COD_RESPONSABLE = "E_V_COD_RESPONSABLE";
	private static final String E_V_NRO_ENCUESTA = "E_V_NRO_ENCUESTA";
	private static final String E_V_NOM_ENCUESTA = "E_V_NOM_ENCUESTA";

	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllEncFormatoBusqueda(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_TIPTC_TIPO_APLICACION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPCT_TIPO_SERVICIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPTC_TIPO_ENCUESTA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_TIPTC_ESTADO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_FENCC_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_COD_RESPONSABLE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NRO_ENCUESTA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_ENCUESTA, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codTipoAplicacion, String codTipoServicio, String codTipoEncuesta, 
    		String codTipoEstado, String nombreEncuesta, String codResponsable, String nroEncuesta,
    		String nomEncuesta) {
    	
    	Map inputs = new HashMap();
    	
    	System.out.println("******** INI ************");
    	System.out.println("codTipoAplicacion:"+codTipoAplicacion);
    	System.out.println("codTipoServicio:"+codTipoServicio);
    	System.out.println("codTipoEncuesta:"+codTipoEncuesta);
    	System.out.println("codTipoEstado:"+codTipoEstado);
    	System.out.println("nombreEncuesta:"+nombreEncuesta);
    	System.out.println("codResponsable:"+codResponsable);
    	System.out.println("nroEncuesta:"+nroEncuesta);
    	System.out.println("nomEncuesta:"+nomEncuesta);
    	System.out.println("******** FIN ************");
    	
    	inputs.put(E_C_TIPTC_TIPO_APLICACION, codTipoAplicacion);
    	inputs.put(E_C_TIPCT_TIPO_SERVICIO, codTipoServicio);
    	inputs.put(E_C_TIPTC_TIPO_ENCUESTA, codTipoEncuesta);
    	inputs.put(E_V_TIPTC_ESTADO, codTipoEstado);
    	inputs.put(E_C_FENCC_ID, nombreEncuesta);
    	inputs.put(E_V_COD_RESPONSABLE, codResponsable);
    	inputs.put(E_V_NRO_ENCUESTA, nroEncuesta);
    	inputs.put(E_V_NOM_ENCUESTA, nomEncuesta);
    	
    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	FormatoEncuesta formatoEncuesta= new FormatoEncuesta();
        	        	
        	formatoEncuesta.setCodEncuesta(rs.getString("COD_ENCUESTA") == null ? "": rs.getString("COD_ENCUESTA"));
        	formatoEncuesta.setNroEncuesta(rs.getString("NRO_ENCUESTA") == null ? "": rs.getString("NRO_ENCUESTA"));
        	formatoEncuesta.setNombreEncuesta(rs.getString("NOM_ENCUESTA") == null ? "": rs.getString("NOM_ENCUESTA"));
        	formatoEncuesta.setCodTipoAplicacion(rs.getString("COD_TIPO_APLICACION") == null ? "": rs.getString("COD_TIPO_APLICACION"));
        	formatoEncuesta.setNomTipoAplicacion(rs.getString("NOM_TIPO_APLICACION") == null ? "": rs.getString("NOM_TIPO_APLICACION"));
        	formatoEncuesta.setCodTipoEncuesta(rs.getString("COD_TIPO_ENCUESTA") == null ? "": rs.getString("COD_TIPO_ENCUESTA"));
        	formatoEncuesta.setNomTipoEncuesta(rs.getString("NOM_TIPO_ENCUESTA") == null ? "": rs.getString("NOM_TIPO_ENCUESTA"));
        	formatoEncuesta.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	formatoEncuesta.setNomEstado(rs.getString("NOM_ESTADO") == null ? "": rs.getString("NOM_ESTADO"));
        	formatoEncuesta.setObsEncuesta(rs.getString("OBS_ENCUESTA") == null ? "": rs.getString("OBS_ENCUESTA"));
        	formatoEncuesta.setIndEncuestaManual(rs.getString("IND_ENCUESTA_MANUAL") == null ? "": rs.getString("IND_ENCUESTA_MANUAL"));
        	//formatoEncuesta.setTotalEncuestadosProg(rs.getString("TOTAL_ENCUESTADOS_PROG") == null ? "": rs.getString("TOTAL_ENCUESTADOS_PROG"));
        	//formatoEncuesta.setTotalEncuestadosReal(rs.getString("TOTAL_ENCUESTADOS_REAL") == null ? "": rs.getString("TOTAL_ENCUESTADOS_REAL"));
        	formatoEncuesta.setDuracion(rs.getString("DURACION") == null ? "": rs.getString("DURACION"));
        	formatoEncuesta.setCodSede(rs.getString("COD_SEDE") == null ? "": rs.getString("COD_SEDE"));
        	formatoEncuesta.setNomSede(rs.getString("NOM_SEDE") == null ? "": rs.getString("NOM_SEDE"));
        	formatoEncuesta.setCodTipoServicio(rs.getString("COD_TIPO_SERVICIO") == null ? "": rs.getString("COD_TIPO_SERVICIO"));
        	formatoEncuesta.setNomTipoServicio(rs.getString("NOM_TIPO_SERVICIO") == null ? "": rs.getString("NOM_TIPO_SERVICIO"));
        	formatoEncuesta.setCodResponsable(rs.getString("COD_RESPONSABLE") == null ? "": rs.getString("COD_RESPONSABLE"));
        	formatoEncuesta.setNomResponsable(rs.getString("NOM_USUARIO") == null ? "": rs.getString("NOM_USUARIO"));            
            formatoEncuesta.setTotalEncuestados(rs.getString("TOTAL_ENCUESTADOS") == null ? "": rs.getString("TOTAL_ENCUESTADOS"));
            formatoEncuesta.setDscEncuestados(rs.getString("DETAL_ENCUESTADOS") == null ? "": rs.getString("DETAL_ENCUESTADOS"));
            
            formatoEncuesta.setTotalEncuestadosCrpta(rs.getString("TOTAL_ENCUESTADOS_CRPTA") == null ? "": rs.getString("TOTAL_ENCUESTADOS_CRPTA"));
            formatoEncuesta.setFecIniAplicacion(rs.getString("FEC_INI_APLICACION") == null ? "": rs.getString("FEC_INI_APLICACION"));
            formatoEncuesta.setFecFinAplicacion(rs.getString("FEC_FIN_APLICACION") == null ? "": rs.getString("FEC_FIN_APLICACION"));
            /*
				AS USUARIO_CREACION,
				AS USUARIO_RESPONSABLE
             */
            formatoEncuesta.setUsuarioCreacion(rs.getString("USUARIO_CREACION") == null ? "": rs.getString("USUARIO_CREACION"));
            formatoEncuesta.setUsuarioResponsable(rs.getString("USUARIO_RESPONSABLE") == null ? "": rs.getString("USUARIO_RESPONSABLE"));
            
        	return formatoEncuesta;
        }
    }

}

