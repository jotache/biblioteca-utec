package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertPerfilEncuesta extends StoredProcedure{
	/*
	 * E_C_TIPO_ENCUESTA IN CHAR, --0001=PROGRAMA; 0002=SERVICIOS/OTROS
	 */
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".pkg_enc_config_perfil.sp_ins_enc_perfil";
	
	private static final String E_C_TIPO_ENCUESTA = "E_C_TIPO_ENCUESTA";
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID"; 
	private static final String E_C_TIPTC_COD_PERFIL = "E_C_TIPTC_COD_PERFIL"; 
	private static final String E_C_EPERC_COD_PRODUCTO = "E_C_EPERC_COD_PRODUCTO";		
	private static final String E_C_EPERC_COD_PROGRAMA = "E_C_EPERC_COD_PROGRAMA";	
	private static final String E_V_EPERC_CAD_COD_CICLO = "E_V_EPERC_CAD_COD_CICLO";
	private static final String E_V_EPERC_CAD_COD_DPTO_CECO = "E_V_EPERC_CAD_COD_DPTO_CECO";		
	private static final String E_V_EPERC_CAD_COD_CURSO = "E_V_EPERC_CAD_COD_CURSO";	
	private static final String E_V_EPERC_CAD_COD_TIPO_PER = "E_V_EPERC_CAD_COD_TIPO_PER";
	private static final String E_V_EPERC_CAD_COD_TIPO_DOC = "E_V_EPERC_CAD_COD_TIPO_DOC";
	private static final String E_V_EPERC_ANIO_INI_EGRESADO = "E_V_EPERC_ANIO_INI_EGRESADO";
	private static final String E_V_EPERC_ANIO_FIN_EGRESADO = "E_V_EPERC_ANIO_FIN_EGRESADO";
	private static final String E_V_EPERC_USUARIO = "E_V_EPERC_USUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	

	public InsertPerfilEncuesta(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(E_C_TIPO_ENCUESTA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_TIPTC_COD_PERFIL, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_EPERC_COD_PRODUCTO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_EPERC_COD_PROGRAMA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_EPERC_CAD_COD_CICLO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EPERC_CAD_COD_DPTO_CECO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EPERC_CAD_COD_CURSO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EPERC_CAD_COD_TIPO_PER, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EPERC_CAD_COD_TIPO_DOC, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EPERC_ANIO_INI_EGRESADO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EPERC_ANIO_FIN_EGRESADO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EPERC_USUARIO, OracleTypes.VARCHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(
			String codTipoEncuesta,
			String codEncuesta,
			String codPerfil,
			String codProducto,
			String codPrograma,
			String codCiclo,
			String codDepartamento,
			String codCurso,
			String codTipoPersonal,
			String codTipoDocente,
			String añoIniEgresado,
			String añoFinEgresado,
			String usuario
			) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_C_TIPO_ENCUESTA, codTipoEncuesta);
	inputs.put(E_V_FENCC_ID, codEncuesta);
	inputs.put(E_C_TIPTC_COD_PERFIL, codPerfil);
	inputs.put(E_C_EPERC_COD_PRODUCTO, codProducto);
	inputs.put(E_C_EPERC_COD_PROGRAMA, codPrograma);
	inputs.put(E_V_EPERC_CAD_COD_CICLO, codCiclo);
	inputs.put(E_V_EPERC_CAD_COD_DPTO_CECO, codDepartamento);
	inputs.put(E_V_EPERC_CAD_COD_CURSO, codCurso);
	inputs.put(E_V_EPERC_CAD_COD_TIPO_PER, codTipoPersonal);
	inputs.put(E_V_EPERC_CAD_COD_TIPO_DOC, codTipoDocente);
	inputs.put(E_V_EPERC_ANIO_INI_EGRESADO, añoIniEgresado);
	inputs.put(E_V_EPERC_ANIO_FIN_EGRESADO, añoFinEgresado);
	inputs.put(E_V_EPERC_USUARIO, usuario);
	
	return super.execute(inputs);
	}

	
}
