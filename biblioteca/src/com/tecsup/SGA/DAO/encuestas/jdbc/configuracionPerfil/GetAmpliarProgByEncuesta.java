package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;

public class GetAmpliarProgByEncuesta extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_ampliar_programacion";	
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAmpliarProgByEncuesta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID,codEncuesta);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            
            FormatoEncuesta encuesta = new FormatoEncuesta();        	        	
        	
        	encuesta.setFecIniAplicacion(rs.getString(1) == null ? "": rs.getString(1));
        	encuesta.setHoraIniAplicacion(rs.getString(2) == null ? "": rs.getString(2));
        	encuesta.setFecFinAplicacion(rs.getString(3) == null ? "": rs.getString(3));
        	encuesta.setHoraFinAplicacion(rs.getString(4) == null ? "": rs.getString(4));
        	        	
        	return encuesta;
        }
    }

}
