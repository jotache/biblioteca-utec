package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;

public class GetAllGruposByEncuesta extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.SP_SEL_GRUPOS_X_ENCUESTA";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_FGRUC_ID = "E_V_FGRUC_ID";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllGruposByEncuesta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.CHAR));        
        declareParameter(new SqlParameter(E_V_FGRUC_ID, OracleTypes.CHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta,String codGrupo){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID,codEncuesta);    	
    	inputs.put(E_V_FGRUC_ID,codGrupo);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Grupo grupo= new Grupo();
        	        	
        	grupo.setCodGrupo(rs.getString("COD_GRUPO") == null ? "": rs.getString("COD_GRUPO"));
        	grupo.setNomGrupo(rs.getString("NOM_GRUPO") == null ? "": rs.getString("NOM_GRUPO"));
        	grupo.setCodFormato(rs.getString("COD_FORMATO") == null ? "": rs.getString("COD_FORMATO"));
        	grupo.setNomFormato(rs.getString("NOM_FORMATO") == null ? "": rs.getString("NOM_FORMATO"));
        	grupo.setPesoGrupo(rs.getString("PESO_GRUPO") == null ? "": rs.getString("PESO_GRUPO"));
        	grupo.setIndAlternativa(rs.getString("IND_ALTERNATIVA") == null ? "": rs.getString("IND_ALTERNATIVA"));
        	grupo.setDesAlternativa(rs.getString("DES_ALTERNATIVA") == null ? "": rs.getString("DES_ALTERNATIVA"));
        	grupo.setPreguntasRel(rs.getString("PREGUNTAS_REL") == null ? "": rs.getString("PREGUNTAS_REL"));
        	grupo.setAlternativasRel(rs.getString("ALTERNATIVAS_REL") == null ? "": rs.getString("ALTERNATIVAS_REL"));
        	
        	return grupo;
        }
    }

}
