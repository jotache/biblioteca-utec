package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

//import com.sun.java_cup.internal.production;
import com.tecsup.SGA.bean.Ciclo;
import com.tecsup.SGA.bean.Departamento;
import com.tecsup.SGA.bean.Programa;
import com.tecsup.SGA.common.Ciclos;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Usuario;

public class GetAllCiclos extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_ciclo";
	
	private static final String E_V_COD_PROGRAMA = "E_V_COD_PROGRAMA";
	private static final String E_V_COD_CICLO = "E_V_COD_CICLO";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllCiclos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_V_COD_PROGRAMA, OracleTypes.VARCHAR));        
        declareParameter(new SqlParameter(E_V_COD_CICLO, OracleTypes.VARCHAR));
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codPrograma, String codCiclo) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_COD_PROGRAMA, codPrograma);
    	inputs.put(E_V_COD_CICLO, codCiclo);    	    	
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Ciclo ciclo = new Ciclo();        	
        	
        	ciclo.setCodCiclo(rs.getString("COD_CICLO") == null ? "": rs.getString("COD_CICLO"));
        	ciclo.setNomCiclo(rs.getString("NOM_CICLO") == null ? "": rs.getString("NOM_CICLO"));
        	        	
        	return ciclo;
        }
    }

}
