package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

//import com.sun.java_cup.internal.production;
import com.tecsup.SGA.bean.Departamento;
import com.tecsup.SGA.bean.Programa;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Usuario;

public class GetAllDepartamentos extends StoredProcedure{
	/*
	 * PROCEDURE SP_SEL_DPTO(
	E_V_COD_CECO IN VARCHAR2,
	E_V_NOM_CECO IN VARCHAR2,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
	 */
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_dpto";
	
	private static final String E_V_COD_CECO = "E_V_COD_CECO";
	private static final String E_V_NOM_CECO = "E_V_NOM_CECO";		
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllDepartamentos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_CECO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_CECO, OracleTypes.VARCHAR));              
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codDepartamento, String nomDepartamento) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_COD_CECO, codDepartamento);
    	inputs.put(E_V_NOM_CECO, nomDepartamento);    	    	
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Departamento departamento = new Departamento();
        	
        	departamento.setCodDepartamento(rs.getString("COD_CECO") == null ? "": rs.getString("COD_CECO"));
        	departamento.setNomDepartamento(rs.getString("NOM_CECO") == null ? "": rs.getString("NOM_CECO"));
        	        	
        	return departamento;
        }
    }

}
