package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

//import com.sun.java_cup.internal.production;
import com.tecsup.SGA.bean.Programa;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Usuario;

public class GetAllProgramas extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_programas";
	
	private static final String E_V_COD_PRODUCTO = "E_V_COD_PRODUCTO";
	private static final String E_V_COD_PROGRAMA = "E_V_COD_PROGRAMA";
	private static final String E_V_NOM_PROGRAMA = "E_V_NOM_PROGRAMA";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllProgramas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_PROGRAMA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_PROGRAMA, OracleTypes.VARCHAR));      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codProducto, String codPrograma, String nomPrograma) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_COD_PRODUCTO, codProducto);
    	inputs.put(E_V_COD_PROGRAMA, codPrograma);
    	inputs.put(E_V_NOM_PROGRAMA, nomPrograma);    	
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Programa programa = new Programa();
        	
        	programa.setCodPrograma(rs.getString("COD_PROGRAMA") == null ? "": rs.getString("COD_PROGRAMA"));
        	programa.setCodProducto(rs.getString("COD_PRODUCTO") == null ? "": rs.getString("COD_PRODUCTO"));
        	programa.setNomProducto(rs.getString("NOM_PRODUCTO") == null ? "": rs.getString("NOM_PRODUCTO"));
        	programa.setIndCiclo(rs.getString("IND_CICLO") == null ? "": rs.getString("IND_CICLO"));        	
        	        	
        	return programa;
        }
    }

}
