	package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.Pregunta;

public class GetAllPreguntasByGrupo extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.SP_SEL_PREGUNTAS_X_GRUPO";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_FGRUC_ID = "E_V_FGRUC_ID";
	private static final String E_V_FPREC_ID = "E_V_FPREC_ID";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllPreguntasByGrupo(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));        
        declareParameter(new SqlParameter(E_V_FGRUC_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_FPREC_ID, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta,String codGrupo,String codPregunta){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID,codEncuesta);    	
    	inputs.put(E_V_FGRUC_ID,codGrupo);
    	inputs.put(E_V_FPREC_ID,codPregunta);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Pregunta pregunta= new Pregunta();
        	        	
        	pregunta.setCodGrupo(("COD_GRUPO") == null ? "": rs.getString("COD_GRUPO"));
        	pregunta.setCodPregunta(("COD_PREGUNTA") == null ? "": rs.getString("COD_PREGUNTA"));
        	pregunta.setNomPregunta(rs.getString("NOM_PREGUNTA") == null ? "": rs.getString("NOM_PREGUNTA"));
        	pregunta.setCodTipoPregunta(rs.getString("COD_TIPO_PREGUNTA") == null ? "": rs.getString("COD_TIPO_PREGUNTA"));
        	pregunta.setNomTipoPregunta(rs.getString("NOM_TIPO_PREGUNTA") == null ? "": rs.getString("NOM_TIPO_PREGUNTA"));
        	pregunta.setIndObligatorio(rs.getString("IND_OBLIGATORIO") == null ? "": rs.getString("IND_OBLIGATORIO"));
        	pregunta.setIndUnica(rs.getString("IND_UNICA") == null ? "": rs.getString("IND_UNICA"));
        	pregunta.setPesoPregunta(rs.getString("PESO_PREGUNTA") == null ? "": rs.getString("PESO_PREGUNTA"));
        	pregunta.setIndAlternativa(rs.getString("IND_ALTERNATIVA") == null ? "": rs.getString("IND_ALTERNATIVA"));
        	pregunta.setAlternativasRel(rs.getString("ALTERNATIVAS_REL") == null ? "": rs.getString("ALTERNATIVAS_REL"));        	
        	pregunta.setDesAlternativa(rs.getString("DES_ALTERNATIVA") == null ? "": rs.getString("DES_ALTERNATIVA"));
        	return pregunta;
        }
    }

}
