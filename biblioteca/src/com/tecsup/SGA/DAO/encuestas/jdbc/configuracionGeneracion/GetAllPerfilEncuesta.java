package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.PerfilEncuesta;

public class GetAllPerfilEncuesta extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_enc_perfil";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_C_TIPTC_COD_PERFIL = "E_C_TIPTC_COD_PERFIL";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllPerfilEncuesta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPTC_COD_PERFIL, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta, String codPerfil){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID,codEncuesta);
    	inputs.put(E_C_TIPTC_COD_PERFIL,codPerfil);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	PerfilEncuesta encuesta = new PerfilEncuesta();
        	
        	encuesta.setCodProducto(rs.getString("COD_PRODUCTO") == null ? "": rs.getString("COD_PRODUCTO"));
        	encuesta.setCodPrograma(rs.getString("COD_PROGRAMA") == null ? "": rs.getString("COD_PROGRAMA"));
        	encuesta.setCodCiclo(rs.getString("CAD_COD_CICLO") == null ? "": rs.getString("CAD_COD_CICLO"));
        	encuesta.setCodDepartamento(rs.getString("CAD_COD_DPTO_CECO") == null ? "": rs.getString("CAD_COD_DPTO_CECO"));
        	encuesta.setCodCurso(rs.getString("CAD_COD_CURSO") == null ? "": rs.getString("CAD_COD_CURSO"));
        	encuesta.setCodTipoPersonal(rs.getString("CAD_COD_TIPO_PER") == null ? "": rs.getString("CAD_COD_TIPO_PER"));
        	encuesta.setCodTipoDocente(rs.getString("CAD_COD_TIPO_DOC") == null ? "": rs.getString("CAD_COD_TIPO_DOC"));
        	encuesta.setAñoIniEgresado(rs.getString("ANIO_EGRESO_INI") == null ? "": rs.getString("ANIO_EGRESO_INI"));
        	encuesta.setAñoFinEgresado(rs.getString("ANIO_EGRESO_FIN") == null ? "": rs.getString("ANIO_EGRESO_FIN"));
        	
        	return encuesta;
        }
    }

}
