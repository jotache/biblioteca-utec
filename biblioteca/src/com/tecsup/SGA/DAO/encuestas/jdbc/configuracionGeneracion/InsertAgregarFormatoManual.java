package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertAgregarFormatoManual extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".PKG_ENC_CONFIGURACION.SP_INS_AGREGAR_FORMATO_MANUAL";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID"; 
	private static final String E_V_CAD_ID_PREG_CERRADA = "E_V_CAD_ID_PREG_CERRADA"; 
	private static final String E_V_CAD_ID_RPTA_CERRADA = "E_V_CAD_ID_RPTA_CERRADA";		
	private static final String E_V_NRO_ID_PREG_CERRADA = "E_V_NRO_ID_PREG_CERRADA";
	
	private static final String E_V_CAD_ID_PREG_ABIERTA = "E_V_CAD_ID_PREG_ABIERTA";
	private static final String E_V_CAD_ID_RPTA_ABIERTA = "E_V_CAD_ID_RPTA_ABIERTA";
	private static final String E_V_NRO_ID_PREG_ABIERTA = "E_V_NRO_ID_PREG_ABIERTA";
	private static final String E_V_USU_CREA = "E_V_USU_CREA";
	private static final String E_C_IND_REGISTRO = "E_C_IND_REGISTRO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertAgregarFormatoManual(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_ID_PREG_CERRADA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_ID_RPTA_CERRADA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NRO_ID_PREG_CERRADA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_ID_PREG_ABIERTA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_ID_RPTA_ABIERTA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NRO_ID_PREG_ABIERTA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USU_CREA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_IND_REGISTRO, OracleTypes.VARCHAR));	
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(
			String codEncuesta,
			String cadIdPreguntaCerrada,
			String cadIdRespuestaCerrada,
			String nroIdPreguntaCerrada,
			String cadIdPreguntaAbierta,
			String cadIdRespuestaAbierta,
			String nroIdPreguntaAbierta,
			String usuCrea,
			String indRegistro
			
			) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_FENCC_ID, codEncuesta);
	inputs.put(E_V_CAD_ID_PREG_CERRADA, cadIdPreguntaCerrada);
	inputs.put(E_V_CAD_ID_RPTA_CERRADA, cadIdRespuestaCerrada);
	inputs.put(E_V_NRO_ID_PREG_CERRADA, nroIdPreguntaCerrada);
	inputs.put(E_V_CAD_ID_PREG_ABIERTA, cadIdPreguntaAbierta);
	inputs.put(E_V_CAD_ID_RPTA_ABIERTA, cadIdRespuestaAbierta);
	inputs.put(E_V_NRO_ID_PREG_ABIERTA, nroIdPreguntaAbierta);
	inputs.put(E_V_USU_CREA, usuCrea);
	inputs.put(E_C_IND_REGISTRO, indRegistro);
	
	return super.execute(inputs);
	}

	
}
