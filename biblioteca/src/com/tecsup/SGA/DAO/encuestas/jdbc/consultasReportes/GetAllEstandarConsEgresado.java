package com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.ConsultaByProfesorConBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.Pregunta;

public class GetAllEstandarConsEgresado extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA
	+ ".pkg_enc_consultas_reportes.SP_SEL_ESTANDAR_CONS_EGRESADO";


	private static final String V_V_NRO_ENCUESTA = "V_V_NRO_ENCUESTA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllEstandarConsEgresado(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(V_V_NRO_ENCUESTA, OracleTypes.VARCHAR));     
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(V_V_NRO_ENCUESTA,codEncuesta);    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ConsultaByProfesorConBean bean= new ConsultaByProfesorConBean();
        	bean.setIdSeccion(rs.getString("ID_SECCION") == null ? "": rs.getString("ID_SECCION"));        	
        	bean.setCodSeccion(rs.getString("COD_SECCION") == null ? "": rs.getString("COD_SECCION"));        	
        	bean.setNomSeccion(rs.getString("NOM_SECCION") == null ? "": rs.getString("NOM_SECCION"));        	
        	bean.setAnioEgresado(rs.getString("ANIO_EGRESADO") == null ? "": rs.getString("ANIO_EGRESADO"));
        	bean.setNroEncuestadoxGrupo(rs.getString("NRO_ENCUESTADO_X_GRUPO") == null ? "": rs.getString("NRO_ENCUESTADO_X_GRUPO"));
        	bean.setIdGrupo(rs.getString("ID_GRUPO") == null ? "": rs.getString("ID_GRUPO"));       	
        	bean.setNomGrupo(rs.getString("NOM_GRUPO") == null ? "": rs.getString("NOM_GRUPO"));
        	bean.setTotalGrupo(rs.getString("TOTAL_GRUPO") == null ? "": rs.getString("TOTAL_GRUPO"));

        	return bean;
        }
    }

}
