	package com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.ConsultaByProfesorConBean;
import com.tecsup.SGA.bean.ConsultaByProfesorDetBean;
import com.tecsup.SGA.bean.ConsultaEstandarByPerfilBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.Pregunta;

public class GetAllEstandarDetalleServicioAbi extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_consultas_reportes.sp_sel_estandar_deta_servi_a";
	/*
	PROCEDURE SP_SEL_ESTANDAR_DETA_SERVI_A(
		V_V_NRO_ENCUESTA VARCHAR2, --por ahora enviar el id de la encuesta
		S_C_RECORDSET IN OUT CUR_RECORDSET)
	 */
	private static final String V_V_NRO_ENCUESTA = "V_V_NRO_ENCUESTA";		
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllEstandarDetalleServicioAbi(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(V_V_NRO_ENCUESTA, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(V_V_NRO_ENCUESTA,codEncuesta);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ConsultaEstandarByPerfilBean bean= new ConsultaEstandarByPerfilBean();
        	/*       
			  AS ID_SECCION,  
		      AS COD_SECCION,
		      AS NOM_SECCION,
		      AS COD_PERFIL,
  			  AS NOM_PERFIL,
		      AS NOM_GRUPO,
		      AS ID_PREGUNTA,
		      AS DES_PREGUNTA,
		      AS RESPUESTA
        	 */
        	bean.setIdSeccion(rs.getString("ID_SECCION") == null ? "": rs.getString("ID_SECCION"));
        	bean.setCodSeccion(rs.getString("COD_SECCION") == null ? "": rs.getString("COD_SECCION"));
        	bean.setNomSeccion(rs.getString("NOM_SECCION") == null ? "": rs.getString("NOM_SECCION"));
        	bean.setCodPerfil(rs.getString("COD_PERFIL") == null ? "": rs.getString("COD_PERFIL"));
        	bean.setNomPerfil(rs.getString("NOM_PERFIL") == null ? "": rs.getString("NOM_PERFIL"));
        	bean.setNomGrupo(rs.getString("NOM_GRUPO") == null ? "": rs.getString("NOM_GRUPO"));
        	bean.setCodPregunta(rs.getString("ID_PREGUNTA") == null ? "": rs.getString("ID_PREGUNTA"));
        	bean.setNomPregunta(rs.getString("DES_PREGUNTA") == null ? "": rs.getString("DES_PREGUNTA"));        	
        	bean.setRespuesta(rs.getString("RESPUESTA") == null ? "": rs.getString("RESPUESTA"));
        	
        	return bean;
        }
    }

}
