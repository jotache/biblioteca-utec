package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;

public class UpdateFormato extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".PKG_ENC_CONFIGURACION.SP_ACT_ENC_FORMATO";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_C_TIPTC_TIPO_APLICACION = "E_C_TIPTC_TIPO_APLICACION"; 
	private static final String E_C_TIPCT_TIPO_SERVICIO = "E_C_TIPCT_TIPO_SERVICIO"; 
	private static final String E_C_TIPTC_TIPO_ENCUESTA = "E_C_TIPTC_TIPO_ENCUESTA";		
	private static final String E_V_FENCC_NUMERO = "E_V_FENCC_NUMERO";
	
	private static final String E_V_FENCC_NOMBRE = "E_V_FENCC_NOMBRE";
	private static final String E_V_FENCC_DURACION = "E_V_FENCC_DURACION";
	private static final String E_V_FENCC_OBSERVACION = "E_V_FENCC_OBSERVACION";
	private static final String E_C_TIPTC_COD_SEDE = "E_C_TIPTC_COD_SEDE";
	private static final String E_V_FENCC_USU_MODI = "E_V_FENCC_USU_MODI";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public UpdateFormato(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_TIPTC_TIPO_APLICACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPCT_TIPO_SERVICIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPTC_TIPO_ENCUESTA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_FENCC_NUMERO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FENCC_NOMBRE, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FENCC_DURACION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FENCC_OBSERVACION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_TIPTC_COD_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_FENCC_USU_MODI, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(
			String idEncuesta,
			String tipoAplicacion,
			String tipoServicio,
			String tipoEncuesta,
			String numero,
			String nombre,
			String duracion,
			String observacion,
			String codSede,
			String usuCrea
			) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_FENCC_ID, idEncuesta);
	inputs.put(E_C_TIPTC_TIPO_APLICACION, tipoAplicacion);
	inputs.put(E_C_TIPCT_TIPO_SERVICIO, tipoServicio);
	inputs.put(E_C_TIPTC_TIPO_ENCUESTA, tipoEncuesta);
	inputs.put(E_V_FENCC_NUMERO, numero);
	inputs.put(E_V_FENCC_NOMBRE, nombre);
	inputs.put(E_V_FENCC_DURACION, duracion);
	inputs.put(E_V_FENCC_OBSERVACION, observacion);
	inputs.put(E_C_TIPTC_COD_SEDE, codSede);
	inputs.put(E_V_FENCC_USU_MODI, usuCrea);
	
	return super.execute(inputs);
	}

	
}
