package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateCodPreguntasAbiertas extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
			".PKG_ENC_CONFIGURACION.sp_preg_abierta_identificador";
	
	private static final String E_FENCC_ID = "E_FENCC_ID";
	private static final String E_CODPERFIL_ID = "E_CODPERFIL_ID";
	private static final String E_FPREA_ID = "E_FPREA_ID";		

	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateCodPreguntasAbiertas(DataSource dataSource){
		super(dataSource, SPROC_NAME);
	
		declareParameter(new SqlParameter(E_FENCC_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_CODPERFIL_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_FPREA_ID, OracleTypes.VARCHAR));

		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		
	}
	
		public Map execute(
			String codEncuesta,
			String codPerfil,
			String codPregAbierta
			){
		
			Map inputs = new HashMap();
			inputs.put(E_FENCC_ID, codEncuesta);
			inputs.put(E_CODPERFIL_ID, codPerfil);
			inputs.put(E_FPREA_ID, codPregAbierta);
		
		return super.execute(inputs);
	
	}
	
}
