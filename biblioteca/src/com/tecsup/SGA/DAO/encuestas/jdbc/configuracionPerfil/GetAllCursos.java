package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

//import com.sun.java_cup.internal.production;
import com.tecsup.SGA.bean.Departamento;
import com.tecsup.SGA.bean.Programa;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Usuario;

public class GetAllCursos extends StoredProcedure{
	
	private static Log log = LogFactory.getLog(GetAllCursos.class);
			
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
		+ ".pkg_enc_config_perfil.sp_sel_curso";
	
	private static final String E_V_COD_CURSO = "E_V_COD_CURSO";
	private static final String E_V_NOM_CURSO = "E_V_NOM_CURSO";
	private static final String E_V_COD_PRODUCTO = "E_V_COD_PRODUCTO";
	private static final String E_V_COD_PROGRAMA = "E_V_COD_PROGRAMA";
	private static final String E_V_COD_CECO = "E_V_COD_CECO";
	private static final String E_V_COD_CICLO = "E_V_COD_CICLO";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllCursos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_CURSO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_PROGRAMA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_CECO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_CICLO, OracleTypes.VARCHAR));
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codCurso, String nomCurso, String codProducto,
    		String codPrograma, String codDepartamento, String codCiclo) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("--------- Start");
    	log.info("--------- SPROC_NAME: " + SPROC_NAME);
		log.info("E_V_COD_CURSO:" + codCurso);
		log.info("E_V_NOM_CURSO:" + nomCurso);
		log.info("E_V_COD_PRODUCTO:" + codProducto);
		log.info("E_V_COD_PROGRAMA:" + codPrograma);
		log.info("E_V_COD_CECO:" + codDepartamento);
		log.info("E_V_COD_CICLO:" + codCiclo);
		log.info("--------- End");
		
    	inputs.put(E_V_COD_CURSO, codCurso);
    	inputs.put(E_V_NOM_CURSO, nomCurso);
    	inputs.put(E_V_COD_PRODUCTO, codProducto);
    	inputs.put(E_V_COD_PROGRAMA, codPrograma);
    	inputs.put(E_V_COD_CECO, codDepartamento);
    	inputs.put(E_V_COD_CICLO, codCiclo);    	    	
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Curso curso = new Curso();
        	
        	curso.setCodCurso(rs.getString("COD_CURSO") == null ? "": rs.getString("COD_CURSO"));
        	curso.setNomCurso(rs.getString("NOM_CURSO") == null ? "": rs.getString("NOM_CURSO"));
        	        	
        	return curso;
        }
    }

}
