package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.FormatoBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class GetAllFormatoEncuesta extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.SP_SEL_FORMATO_DE_ENCUESTA";

	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllFormatoEncuesta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.CHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjectaMapper()));
        compile();
    }

    public Map execute(String codEncuesta) {
    	
    	Map inputs = new HashMap();

    	inputs.put(E_V_FENCC_ID, codEncuesta);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjectaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	FormatoBean formatoBean= new FormatoBean();
        	        	
        	formatoBean.setCodFormato(rs.getString("COD_FORMATO") == null ? "": rs.getString("COD_FORMATO"));
        	formatoBean.setNomFormato(rs.getString("NOM_FORMATO") == null ? "": rs.getString("NOM_FORMATO"));
        	formatoBean.setIdSeccion(rs.getString("ID_SECCION") == null ? "": rs.getString("ID_SECCION"));
        	
        	
        	return formatoBean;
        }
    }

}
