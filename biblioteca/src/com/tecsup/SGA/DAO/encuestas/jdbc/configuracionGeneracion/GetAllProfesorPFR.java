package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.modelo.PerfilProfesor;

public class GetAllProfesorPFR extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_orientadoa_profesor_pfr";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_TIPTC_COD_PERFIL = "E_V_TIPTC_COD_PERFIL";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllProfesorPFR(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_TIPTC_COD_PERFIL, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta, String codPerfil){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID,codEncuesta);
    	inputs.put(E_V_TIPTC_COD_PERFIL,codPerfil);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {        	
        	
        	PerfilProfesor profesor = new PerfilProfesor();
        	/*
        	 * PROFESOR.COD_PROFESOR AS COD_PROFESOR,
      USUARIO.NOM_USUARIO AS NOM_PROFESOR,
      USUARIO.COD_SECCION AS COD_SECCION,
      SECCION.NOM_SECCION AS NOM_SECCION,
      USUARIO.COD_CICLO AS COD_CICLO,
      CICLO.NOM_CICLO AS NOM_CICLO,
      USUARIO.COD_CECO AS COD_DPTO_CECO,
      DPTO.NOM_CECO AS NOM_DPTO_CECO,      
      USUARIO.COD_CURSO AS COD_CURSO,
      CURSO.NOM_CURSO AS NOM_CURSO
        	 */
        	profesor.setCodProfesor(rs.getString("COD_PROFESOR") == null ? "": rs.getString("COD_PROFESOR"));
        	profesor.setNomProfesor(rs.getString("NOM_PROFESOR") == null ? "": rs.getString("NOM_PROFESOR"));        	
        	profesor.setCodSeccion(rs.getString("COD_SECCION") == null ? "": rs.getString("COD_SECCION"));
        	profesor.setNomSeccion(rs.getString("NOM_SECCION") == null ? "": rs.getString("NOM_SECCION"));
        	profesor.setCodCiclo(rs.getString("COD_CICLO") == null ? "": rs.getString("COD_CICLO"));
        	profesor.setNomCiclo(rs.getString("NOM_CICLO") == null ? "": rs.getString("NOM_CICLO"));
        	profesor.setCodDepartamento(rs.getString("COD_DPTO_CECO") == null ? "": rs.getString("COD_DPTO_CECO"));
        	profesor.setNomDepartamento(rs.getString("NOM_DPTO_CECO") == null ? "": rs.getString("NOM_DPTO_CECO"));
        	profesor.setCodCurso(rs.getString("COD_CURSO") == null ? "": rs.getString("COD_CURSO"));
        	profesor.setNomCurso(rs.getString("NOM_CURSO") == null ? "": rs.getString("NOM_CURSO"));
        	
        	return profesor;
        }
    }

}
