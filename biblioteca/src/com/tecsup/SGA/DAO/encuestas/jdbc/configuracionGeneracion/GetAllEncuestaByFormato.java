package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class GetAllEncuestaByFormato extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.sp_sel_encuesta_x_formato";
	/*E_V_FENCC_ID IN VARCHAR*/
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllEncuestaByFormato(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
              
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codFormatoEncuesta) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID, codFormatoEncuesta);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	FormatoEncuesta formatoEncuesta= new FormatoEncuesta();
        	        	
        	formatoEncuesta.setCodFormato(rs.getString("COD_FORMATO") == null ? "": rs.getString("COD_FORMATO"));
        	formatoEncuesta.setNomFormato(rs.getString("NOM_FORMATO") == null ? "": rs.getString("NOM_FORMATO"));
        	formatoEncuesta.setCodSeccionAsignado(rs.getString("COD_SECCION_ASIGNADO") == null ? "": rs.getString("COD_SECCION_ASIGNADO"));
        	formatoEncuesta.setRegistrosRelacionados(rs.getString("REG_RELACIONADOS") == null ? "": rs.getString("REG_RELACIONADOS"));
        	        	
        	return formatoEncuesta;
        }
    }

}
