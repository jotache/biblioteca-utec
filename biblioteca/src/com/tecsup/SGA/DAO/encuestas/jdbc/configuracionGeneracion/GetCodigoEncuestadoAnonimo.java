package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetCodigoEncuestadoAnonimo extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
			".PKG_ENC_CONFIGURACION.SP_OBT_COD_ENCUESTADO_ANONIMO";
	
	private static final String E_V_COD_ENCUESTA = "E_V_COD_ENCUESTA"; 
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public GetCodigoEncuestadoAnonimo(DataSource dataSource){
		
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_COD_ENCUESTA, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		
	}
	
	public Map execute(String codEncuesta) {
		
		Map inputs = new HashMap();
		inputs.put(E_V_COD_ENCUESTA, codEncuesta);
		
		return super.execute(inputs);
	
	}
	
}
