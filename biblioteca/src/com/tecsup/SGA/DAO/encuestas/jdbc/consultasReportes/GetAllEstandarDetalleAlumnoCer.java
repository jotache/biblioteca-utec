	package com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.ConsultaByProfesorConBean;
import com.tecsup.SGA.bean.ConsultaByProfesorDetBean;
import com.tecsup.SGA.bean.ConsultaEstandarByPerfilBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.Pregunta;

public class GetAllEstandarDetalleAlumnoCer extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_consultas_reportes.sp_sel_estandar_deta_alumno_c";
	/*
	 PROCEDURE SP_SEL_ESTANDAR_DETA_ALUMNO_C(
		V_V_NRO_ENCUESTA VARCHAR2, --por ahora enviar el id de la encuesta
		S_C_RECORDSET IN OUT CUR_RECORDSET)
	 */
	private static final String V_V_NRO_ENCUESTA = "V_V_NRO_ENCUESTA";		
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllEstandarDetalleAlumnoCer(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(V_V_NRO_ENCUESTA, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(V_V_NRO_ENCUESTA,codEncuesta);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ConsultaEstandarByPerfilBean bean= new ConsultaEstandarByPerfilBean();
        	/*
        	 *       
		      AS ID_SECCION,  
		      AS COD_SECCION,
		      AS NOM_SECCION,
		      
		      AS COD_PROFESOR,
		      AS NOM_TIPO_PERSONAL,
		      AS NOM_PROFESOR,      
		      
		      AS NOM_DPTO,
		        AS COD_PRODUCTO,
      			AS COD_PROGRAMA,
      			AS COD_CURSO,
		      AS NOM_CURSO,
		      AS ID_GRUPO,
		      AS NOM_GRUPO,
		      AS ID_PREGUNTA,
		      AS DES_PREGUNTA,
		      AS ID_ALTERNATIVA,
		      AS NOM_ALTERNATIVA,            
		      AS TOTAL_ALTERNATIVA
        	 */
        	bean.setIdSeccion(rs.getString("ID_SECCION") == null ? "": rs.getString("ID_SECCION"));
        	bean.setCodSeccion(rs.getString("COD_SECCION") == null ? "": rs.getString("COD_SECCION"));
        	bean.setNomSeccion(rs.getString("NOM_SECCION") == null ? "": rs.getString("NOM_SECCION"));
        	bean.setCodProfesor(rs.getString("COD_PROFESOR") == null ? "": rs.getString("COD_PROFESOR"));
        	bean.setNomTipoPersonal(rs.getString("NOM_TIPO_PERSONAL") == null ? "": rs.getString("NOM_TIPO_PERSONAL"));
        	bean.setNomProfesor(rs.getString("NOM_PROFESOR") == null ? "": rs.getString("NOM_PROFESOR"));
        	bean.setNomDepartamento(rs.getString("NOM_DPTO") == null ? "": rs.getString("NOM_DPTO"));
        	
        	bean.setCodProducto(rs.getString("COD_PRODUCTO") == null ? "": rs.getString("COD_PRODUCTO"));
        	bean.setCodPrograma(rs.getString("COD_PROGRAMA") == null ? "": rs.getString("COD_PROGRAMA"));
        	bean.setCodCurso(rs.getString("COD_CURSO") == null ? "": rs.getString("COD_CURSO"));
        	
        	bean.setNomCurso(rs.getString("NOM_CURSO") == null ? "": rs.getString("NOM_CURSO"));        	
        	bean.setCodGrupo(rs.getString("ID_GRUPO") == null ? "": rs.getString("ID_GRUPO"));
        	bean.setNomGrupo(rs.getString("NOM_GRUPO") == null ? "": rs.getString("NOM_GRUPO"));
        	bean.setCodPregunta(rs.getString("ID_PREGUNTA") == null ? "": rs.getString("ID_PREGUNTA"));
        	bean.setNomPregunta(rs.getString("DES_PREGUNTA") == null ? "": rs.getString("DES_PREGUNTA"));
        	bean.setCodAlternativa(rs.getString("ID_ALTERNATIVA") == null ? "": rs.getString("ID_ALTERNATIVA"));
        	bean.setNomAlternativa(rs.getString("NOM_ALTERNATIVA") == null ? "": rs.getString("NOM_ALTERNATIVA"));
        	bean.setTotalAlternativa(rs.getString("TOTAL_ALTERNATIVA") == null ? "": rs.getString("TOTAL_ALTERNATIVA"));
        	
        	return bean;
        }
    }

}
