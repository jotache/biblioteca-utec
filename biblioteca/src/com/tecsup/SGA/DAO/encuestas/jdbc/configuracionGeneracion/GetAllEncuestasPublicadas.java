package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

//import com.sun.java_cup.internal.production;
import com.tecsup.SGA.bean.Departamento;
import com.tecsup.SGA.bean.Programa;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Usuario;

public class GetAllEncuestasPublicadas extends StoredProcedure{
	
	private static Log log = LogFactory.getLog(GetAllEncuestasPublicadas.class);
			
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
		+ ".pkg_enc_configuracion.sp_sel_encuestas_publicadas";
	
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";		
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllEncuestasPublicadas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codUsuario) {
    	
    	Map inputs = new HashMap();
    	
    	log.info("--------- Start");
    	log.info("--------- SPROC_NAME: " + SPROC_NAME);
		log.info("E_V_COD_USUARIO:" + codUsuario);
		log.info("--------- End");
		
    	inputs.put(E_V_COD_USUARIO, codUsuario);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	FormatoEncuesta encuesta= new FormatoEncuesta();
        	
        	encuesta.setCodEncuesta(rs.getString("COD_ENCUESTA") == null ? "": rs.getString("COD_ENCUESTA"));
        	encuesta.setCodPerfil(rs.getString("COD_PERFIL") == null ? "": rs.getString("COD_PERFIL"));
        	encuesta.setCodEncuestado(rs.getString("COD_ENCUESTADO") == null ? "": rs.getString("COD_ENCUESTADO"));
        	encuesta.setNombreEncuesta(rs.getString("NRO_NOM_ENCUESTA") == null ? "": rs.getString("NRO_NOM_ENCUESTA"));
        	encuesta.setFecIniAplicacion(rs.getString("FEC_INI_APLICACION") == null ? "": rs.getString("FEC_INI_APLICACION"));
        	encuesta.setFecFinAplicacion(rs.getString("FEC_FIN_APLICACION") == null ? "": rs.getString("FEC_FIN_APLICACION"));
        	encuesta.setCodTipoEncuesta(rs.getString("COD_TIPO_ENCUESTA") == null ? "": rs.getString("COD_TIPO_ENCUESTA"));
        	encuesta.setCodProfesor(rs.getString("COD_PROFESOR") == null ? "": rs.getString("COD_PROFESOR"));
        	
        	return encuesta;
        }
    }

}
