package com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaAlumno;

public class GetAllEncuestaAlumno extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_actuliza_datos_alumno.sp_sel_enc_alumno";
//E_V_ALUMC_ID IN VARCHAR2
	private static final String E_V_ALUMC_ID = "E_V_ALUMC_ID";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllEncuestaAlumno(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_ALUMC_ID, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjectaMapper()));
        compile();
    }

    public Map execute(String codAlumno) {
    	
    	Map inputs = new HashMap();

    	inputs.put(E_V_ALUMC_ID, codAlumno);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjectaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	EncuestaAlumno encuestaAlumno= new EncuestaAlumno();
        	        	
        	encuestaAlumno.setPrimerNombre(rs.getString("PRIMER_NOMBRE") == null ? "": rs.getString("PRIMER_NOMBRE"));
        	encuestaAlumno.setSegundoNombre(rs.getString("SEGUNDO_NOMBRE") == null ? "": rs.getString("SEGUNDO_NOMBRE"));
        	encuestaAlumno.setApellidoMaterno(rs.getString("APE_MATERNO") == null ? "": rs.getString("APE_MATERNO"));
        	encuestaAlumno.setApellidoPaterno(rs.getString("APE_PATERNO") == null ? "": rs.getString("APE_PATERNO"));
        	encuestaAlumno.setCodTipoDoc(rs.getString("COD_TIPO_DOC") == null ? "": rs.getString("COD_TIPO_DOC"));
        	encuestaAlumno.setNroTipoDoc(rs.getString("NRO_TIPO_DOC") == null ? "": rs.getString("NRO_TIPO_DOC"));
        	encuestaAlumno.setEmail(rs.getString("CORREO_PERSONAL") == null ? "": rs.getString("CORREO_PERSONAL"));
        	encuestaAlumno.setDireccion(rs.getString("DIRECCION") == null ? "": rs.getString("DIRECCION"));
        	encuestaAlumno.setTelfCasa(rs.getString("TEL_CASA") == null ? "": rs.getString("TEL_CASA"));
        	encuestaAlumno.setTelfCelular(rs.getString("TEL_CELULAR") == null ? "": rs.getString("TEL_CELULAR"));
        	encuestaAlumno.setCodEmpresa(rs.getString("COD_EMPRESA") == null ? "": rs.getString("COD_EMPRESA"));
        	encuestaAlumno.setNombreEmpresa(rs.getString("NOM_EMPRESA") == null ? "": rs.getString("NOM_EMPRESA"));
        	encuestaAlumno.setNombreArea(rs.getString("NOM_AREA") == null ? "": rs.getString("NOM_AREA"));
        	encuestaAlumno.setNombreCargo(rs.getString("NOM_CARGO") == null ? "": rs.getString("NOM_CARGO"));
        	encuestaAlumno.setCorreoEmpresa(rs.getString("CORRE_EMPRESA") == null ? "": rs.getString("CORRE_EMPRESA"));
        	encuestaAlumno.setNombreContacto(rs.getString("NOM_CONTACTO") == null ? "": rs.getString("NOM_CONTACTO"));
        	encuestaAlumno.setTelfContacto(rs.getString("TEL_CONTACTO") == null ? "": rs.getString("TEL_CONTACTO"));
        	
        	return encuestaAlumno;
        }
    }

}
