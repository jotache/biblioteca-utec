package com.tecsup.SGA.DAO.encuestas.jdbc;


import java.util.HashMap;
import java.util.List;

import org.jfree.util.Log;
import org.springframework.jdbc.core.support.JdbcDaoSupport;



import com.tecsup.SGA.DAO.encuestas.ConfiguracionPerfilDAO;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.AmpliarProgramacionEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.AplicarEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.DeleteAmbitoProfesor;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllAmbitoGeneral;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllCiclos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllCursos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllDepartamentos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllPerfilEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllPerfilesByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProductos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProfesorPCC;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProfesorPFR;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProgramas;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllTipoDocente;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllTipoPersonal;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAmpliarProgByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetFechaHoraSistema;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.InsertPerfilEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.ObtenerCorreoUsuario;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.UpdateTerminarEncuesta;
import com.tecsup.SGA.common.CommonConstants;

public class ConfiguracionPerfilDAOJdbc extends JdbcDaoSupport implements ConfiguracionPerfilDAO{	
	
	public List GetFechaHoraSistema() {
				
		GetFechaHoraSistema getFechaHoraSistema = new GetFechaHoraSistema(this.getDataSource());
		
		HashMap outputs = (HashMap)getFechaHoraSistema.execute();
		
		if (!outputs.isEmpty()){
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}		
	
	public String updateTerminarEncuesta(String idEncuesta,String usuCrea) {
		
		UpdateTerminarEncuesta updateTerminarEncuesta = new UpdateTerminarEncuesta(this.getDataSource());
		
		HashMap inputs = (HashMap)updateTerminarEncuesta.execute(idEncuesta, usuCrea );
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertPerfilEncuesta(String codTipoEncuesta,
			String codEncuesta,
			String codPerfil,
			String codProducto,
			String codPrograma,
			String codCiclo,
			String codDepartamento,
			String codCurso,
			String codTipoPersonal,
			String codTipoDocente,
			String añoIniEgresado,
			String añoFinEgresado,
			String usuario) {
		
		InsertPerfilEncuesta insertPerfilEncuesta = new InsertPerfilEncuesta(this.getDataSource());		
		
		HashMap inputs = (HashMap)insertPerfilEncuesta.execute(codTipoEncuesta,
				codEncuesta, 
				codPerfil, 
				codProducto, 
				codPrograma, 
				codCiclo, 
				codDepartamento, 
				codCurso, 
				codTipoPersonal, 
				codTipoDocente, 
				añoIniEgresado, 
				añoFinEgresado, 
				usuario);
		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllPerfilEncuesta(String codEncuesta, String codPerfil){
		
		GetAllPerfilEncuesta getAllPerfilEncuesta = new GetAllPerfilEncuesta(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllPerfilEncuesta.execute(codEncuesta, codPerfil);				
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllPerfilesByEncuesta(String codEncuesta){
		
		GetAllPerfilesByEncuesta getAllPerfilesByEncuesta = new GetAllPerfilesByEncuesta(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllPerfilesByEncuesta.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List GetAllProductos(String codProducto, String codProductoNo){
		
		GetAllProductos getAllProductos = new GetAllProductos(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllProductos.execute(codProducto, codProductoNo);
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllProgramas(String codProducto, String codPrograma, String nomPrograma){
		
		GetAllProgramas getAllProgramas = new GetAllProgramas(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllProgramas.execute(codProducto, codPrograma, nomPrograma);
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllDepartamentos(String codDepartamento, String nomDepartamento){
		
		GetAllDepartamentos getAllDepartamentos = new GetAllDepartamentos(this.getDataSource());				
		
		HashMap outputs = (HashMap)getAllDepartamentos.execute(codDepartamento, nomDepartamento);
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllCursos(String codCurso, String nomCurso, String codProducto, 
			String codPrograma, String codDepartamento, String codCiclo){
		
		GetAllCursos getAllCursos = new GetAllCursos(this.getDataSource());						
		
		HashMap outputs = (HashMap)getAllCursos.execute(codCurso, nomCurso, codProducto, codPrograma, codDepartamento, codCiclo);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllCiclos(String codPrograma, String codCiclo){
		
		GetAllCiclos getAllCiclos = new GetAllCiclos(this.getDataSource());								
		
		HashMap outputs = (HashMap)getAllCiclos.execute(codPrograma, codCiclo);				
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
		
	public List GetAllTipoPersonal(String codTipoPersonal, String nomTipoPersonal){
		
		GetAllTipoPersonal getAllTipoPersonal = new GetAllTipoPersonal(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllTipoPersonal.execute(codTipoPersonal, nomTipoPersonal);
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllTipoDocente(String codTipoPersonal, String codTipoDocente, String nomTipoDocente){
		
		GetAllTipoDocente getAllTipoDocente = new GetAllTipoDocente(this.getDataSource());				
		
		HashMap outputs = (HashMap)getAllTipoDocente.execute(codTipoPersonal, codTipoDocente, nomTipoDocente);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllProfesorPFR(String codEncuesta, String codPerfil){
		
		Log.info("ingresando_Metodo_getAllProfesorPFR..............................");
		GetAllProfesorPFR getAllProfesorPFR= new GetAllProfesorPFR(this.getDataSource());						
		
		Log.info("Obteniedno valores: codEncuesta= " + codEncuesta + "  -  codPerfil=" + codPerfil);
		
		HashMap outputs = (HashMap)getAllProfesorPFR.execute(codEncuesta, codPerfil);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllProfesorPCC(String codEncuesta, String codPerfil){
		
		GetAllProfesorPCC getAllProfesorPCC= new GetAllProfesorPCC(this.getDataSource());								
		
		HashMap outputs = (HashMap)getAllProfesorPCC.execute(codEncuesta, codPerfil);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllAmbitoGeneral(String codEncuesta, String codPerfil){
		
		GetAllAmbitoGeneral getAllAmbitoGeneral = new GetAllAmbitoGeneral(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllAmbitoGeneral.execute(codEncuesta, codPerfil);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public String AplicarEncuesta(String codEncuesta,
			String fechaInicio,
			String horaInicio,
			String fechaFin,
			String horaFin,
			String usuario) {
		
		AplicarEncuesta aplicarEncuesta = new AplicarEncuesta(this.getDataSource());				
		
		HashMap inputs = (HashMap)aplicarEncuesta.execute(codEncuesta, 
				fechaInicio, horaInicio, fechaFin, horaFin, usuario);
		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAmpliarProgByEncuesta(String codEncuesta) {
		
		GetAmpliarProgByEncuesta getAmpliarProgByEncuesta = new GetAmpliarProgByEncuesta(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAmpliarProgByEncuesta.execute(codEncuesta);
		if (!outputs.isEmpty()){
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String AmpliarProgramacionEncuesta(String codEncuesta,
			String fechaInicio,
			String horaInicio,
			String fechaFin,
			String horaFin,
			String codUsuario) {
		
		AmpliarProgramacionEncuesta ampliarProgramacionEncuesta = new AmpliarProgramacionEncuesta(this.getDataSource());						
		
		HashMap inputs = (HashMap)ampliarProgramacionEncuesta.execute(codEncuesta, 
				fechaInicio, horaInicio, fechaFin, horaFin, codUsuario);
		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String deleteAmbitoProfesor(String codEncuesta, String codPerfil, String codProfesor, 
			String codProducto, String codPrograma,String codCentroCosto, 
			String codCurso, String codCiclo, String codSeccion, String codUsuario) {
		
		DeleteAmbitoProfesor deleteAmbitoProfesor = new DeleteAmbitoProfesor(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteAmbitoProfesor.execute(codEncuesta, codPerfil, codProfesor, 
				codProducto, codPrograma, codCentroCosto, codCurso, codCiclo, codSeccion, codUsuario);
		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String obtener_correo(String codUsuario) {
		
		ObtenerCorreoUsuario obtenerCorreoUsuario = new ObtenerCorreoUsuario (this.getDataSource());
		HashMap inputs = (HashMap)obtenerCorreoUsuario.execute(codUsuario);
		
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;
	}
	
	
	
}