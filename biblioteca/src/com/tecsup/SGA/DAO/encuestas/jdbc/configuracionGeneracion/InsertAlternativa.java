package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertAlternativa extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".PKG_ENC_CONFIGURACION.SP_INS_ENC_ALTERNATIVA";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID"; 
	private static final String E_V_FGRUC_ID = "E_V_FGRUC_ID"; 
	private static final String E_V_FPREC_ID = "E_V_FPREC_ID";		
	private static final String E_V_FALTC_ALTERNATIVA = "E_V_FALTC_ALTERNATIVA";	
	private static final String E_V_FALTC_DESCRIPCION = "E_V_FALTC_DESCRIPCION";
	private static final String E_V_FALTC_PESO = "E_V_FALTC_PESO";
	private static final String E_V_FALTC_USU_CREA = "E_V_FALTC_USU_CREA";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertAlternativa(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_FGRUC_ID, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_FPREC_ID, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_FALTC_ALTERNATIVA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FALTC_DESCRIPCION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FALTC_PESO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_FALTC_USU_CREA, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(
			String codEncuesta,
			String codGrupo,
			String codPregunta,
			String alternativa,
			String descripcion,			
			String peso,			
			String usuCrea
			) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_FENCC_ID, codEncuesta);
	inputs.put(E_V_FGRUC_ID, codGrupo);
	inputs.put(E_V_FPREC_ID, codPregunta);
	inputs.put(E_V_FALTC_ALTERNATIVA, alternativa);
	inputs.put(E_V_FALTC_DESCRIPCION, descripcion);
	inputs.put(E_V_FALTC_PESO, peso);
	inputs.put(E_V_FALTC_USU_CREA, usuCrea);
	
	return super.execute(inputs);
	}

	
}
