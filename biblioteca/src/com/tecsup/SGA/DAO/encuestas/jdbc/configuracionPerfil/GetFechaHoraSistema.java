package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.FechaBean;
import com.tecsup.SGA.bean.FormatoBean;
import com.tecsup.SGA.bean.VerEncuestaBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class GetFechaHoraSistema extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_fecha_hora_sistema";
		
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetFechaHoraSistema(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjectaMapper()));
        compile();
    }

    public Map execute() {
    	
    	Map inputs = new HashMap();
    	     	
        return super.execute(inputs);
    }
    
    final class ObjectaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	FechaBean bean= new FechaBean();
        	
        	bean.setFecha(rs.getString("FEC_SISTEMA") == null ? "": rs.getString("FEC_SISTEMA"));
        	bean.setHora(rs.getString("HH_SISTEMA") == null ? "": rs.getString("HH_SISTEMA"));
        	bean.setHoraMinuto(rs.getString("HH_MM_SISTEMA") == null ? "": rs.getString("HH_MM_SISTEMA"));        	
        	
        	return bean;
        }
    }

}
