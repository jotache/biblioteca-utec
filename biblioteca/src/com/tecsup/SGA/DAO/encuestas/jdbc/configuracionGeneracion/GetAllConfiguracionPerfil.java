package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
/*
 *PROCEDURE SP_SEL_FILTROS_CFG_PERFIL(
	E_C_TIPT_ID IN CHAR,
	E_V_TTDE_VALOR1 IN CHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllConfiguracionPerfil extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.sp_sel_filtros_cfg_perfil";
	
	private static final String E_C_TIPT_ID = "E_C_TIPT_ID";
	private static final String E_V_TTDE_VALOR1 = "E_V_TTDE_VALOR1";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";		
    
    public GetAllConfiguracionPerfil(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_TIPT_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_TTDE_VALOR1, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codTabla, String codDetalle) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_TIPT_ID, codTabla);
    	inputs.put(E_V_TTDE_VALOR1, codDetalle);    	
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	TipoTablaDetalle obj= new TipoTablaDetalle();        	            
        	
        	obj.setCodTipoTablaDetalle(rs.getString("COD_ITEM") == null ? "": rs.getString("COD_ITEM"));
        	obj.setDescripcion(rs.getString("NOM_ITEM") == null ? "": rs.getString("NOM_ITEM"));
        	obj.setDscValor1(rs.getString("COD_DET_ITEM") == null ? "": rs.getString("COD_DET_ITEM"));
        	obj.setDscValor2(rs.getString("IND_CICLO") == null ? "": rs.getString("IND_CICLO"));        	
        	
        	return obj;
        }
    }
}
