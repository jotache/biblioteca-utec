package com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaEmpresa;

public class GetAllEncuestaEmpresa extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_actuliza_datos_alumno.sp_sel_enc_v_empresa";
//E_V_NOM_EMPRESA IN VARCHAR2
	private static final String E_V_NOM_EMPRESA = "E_V_NOM_EMPRESA";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllEncuestaEmpresa(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_NOM_EMPRESA, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjectaMapper()));
        compile();
    }

    public Map execute(String nombreEmpresa) {
    	
    	Map inputs = new HashMap();

    	inputs.put(E_V_NOM_EMPRESA, nombreEmpresa);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjectaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	EncuestaEmpresa encuestaEmpresa= new EncuestaEmpresa();
        	        	
        	encuestaEmpresa.setCodEmpresa(rs.getString("COD_EMPRESA") == null ? "": rs.getString("COD_EMPRESA"));
        	encuestaEmpresa.setNombreEmpresa(rs.getString("NOM_EMPRESA") == null ? "": rs.getString("NOM_EMPRESA"));
        	
        	return encuestaEmpresa;
        }
    }

}
