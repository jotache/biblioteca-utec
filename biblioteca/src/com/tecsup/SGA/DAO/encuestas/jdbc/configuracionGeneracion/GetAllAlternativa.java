package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class GetAllAlternativa extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.SP_SEL_ENC_ALTERNATIVA";

/*
 * PROCEDURE SP_SEL_ENC_ALTERNATIVA(
 IN VARCHAR2,
 IN VARCHAR2,
 IN VARCHAR2,
 IN VARCHAR2,
S_C_RECORDSET IN OUT CUR_RECORDSET);
 * */	
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_FGRUC_ID = "E_V_FGRUC_ID";
	private static final String E_V_FPREC_ID = "E_V_FPREC_ID";
	private static final String E_V_FALTC_ID = "E_V_FALTC_ID";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllAlternativa(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.CHAR));        
        declareParameter(new SqlParameter(E_V_FGRUC_ID, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_FPREC_ID, OracleTypes.CHAR));        
        declareParameter(new SqlParameter(E_V_FALTC_ID, OracleTypes.CHAR));      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta,String codGrupo,String codPregunta,String codAlternativa){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID,codEncuesta);    	
    	inputs.put(E_V_FGRUC_ID,codGrupo);
    	inputs.put(E_V_FPREC_ID,codPregunta);    	
    	inputs.put(E_V_FALTC_ID,codAlternativa);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Alternativa alternativa= new Alternativa();
        	        	
        	alternativa.setCodAlternativa(rs.getString("COD_ALTERNATIVA") == null ? "": rs.getString("COD_ALTERNATIVA"));
        	alternativa.setEtiAlternativa(rs.getString("ETI_ALTERNATIVA") == null ? "": rs.getString("ETI_ALTERNATIVA"));
        	alternativa.setDesAlternativa(rs.getString("DES_ALTERNATIVA") == null ? "": rs.getString("DES_ALTERNATIVA"));
        	alternativa.setPeso(rs.getString("PESO") == null ? "": rs.getString("PESO"));
        	
        	return alternativa;
        }
    }

}
