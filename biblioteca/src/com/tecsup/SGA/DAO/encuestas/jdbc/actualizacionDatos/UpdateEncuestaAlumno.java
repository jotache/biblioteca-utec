package com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.EncuestaAlumno;

public class UpdateEncuestaAlumno extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".pkg_enc_actuliza_datos_alumno.sp_act_enc_alumno";
/*E_V_ALUMC_ID IN VARCHAR2, 
E_V_ALUMC_NOM_PRIMERO IN VARCHAR2,
E_V_ALUMC_NOM_SEGUNDO IN VARCHAR2,
E_V_ALUMC_APE_MATERNO IN VARCHAR2,
E_V_ALUMC_APE_PATERNO IN VARCHAR2,
E_C_TIPTC_TIPO_DOC IN CHAR,
E_V_ALUMC_NRO_DOC IN VARCHAR2,
E_V_ALUMC_CORREO_PERSONAL IN VARCHAR2,

E_V_ALUMC_DIRECCION IN VARCHAR2,
E_V_ALUMC_TEL_CASA IN VARCHAR2,
E_V_ALUMC_TEL_CELULAR IN VARCHAR2,
E_V_ALUMC_COD_EMPRESA IN VARCHAR2,

E_V_ALUMC_AREA_TRABAJO IN VARCHAR2,
E_V_ALUMC_CARGO_TRABAJO IN VARCHAR2,
E_V_ALUMC_CORREO_EMPRESA IN VARCHAR2,

E_V_ALUMC_CONTACTO IN VARCHAR2,
E_V_ALUMC_TEL_CONTACTO IN VARCHAR2,
E_V_ALUMC_USUARIO IN VARCHAR2
 */  //18 parametros declarados
	private static final String E_V_ALUMC_ID = "E_V_ALUMC_ID";
	private static final String E_V_ALUMC_NOM_PRIMERO = "E_V_ALUMC_NOM_PRIMERO"; 
	private static final String E_V_ALUMC_NOM_SEGUNDO = "E_V_ALUMC_NOM_SEGUNDO"; 
	private static final String E_V_ALUMC_APE_MATERNO = "E_V_ALUMC_APE_MATERNO";		
	private static final String E_V_ALUMC_APE_PATERNO = "E_V_ALUMC_APE_PATERNO";	
	private static final String E_C_TIPTC_TIPO_DOC = "E_C_TIPTC_TIPO_DOC";
	private static final String E_V_ALUMC_NRO_DOC = "E_V_ALUMC_NRO_DOC";
	private static final String E_V_ALUMC_CORREO_PERSONAL = "E_V_ALUMC_CORREO_PERSONAL";
	private static final String E_V_ALUMC_DIRECCION = "E_V_ALUMC_DIRECCION";
	private static final String E_V_ALUMC_TEL_CASA = "E_V_ALUMC_TEL_CASA"; 
	private static final String E_V_ALUMC_TEL_CELULAR = "E_V_ALUMC_TEL_CELULAR"; 
	private static final String E_V_ALUMC_COD_EMPRESA = "E_V_ALUMC_COD_EMPRESA";
	private static final String E_V_ALUMC_AREA_TRABAJO = "E_V_ALUMC_AREA_TRABAJO";
	private static final String E_V_ALUMC_CARGO_TRABAJO = "E_V_ALUMC_CARGO_TRABAJO";
	private static final String E_V_ALUMC_CORREO_EMPRESA = "E_V_ALUMC_CORREO_EMPRESA";
	private static final String E_V_ALUMC_CONTACTO = "E_V_ALUMC_CONTACTO";
	private static final String E_V_ALUMC_TEL_CONTACTO = "E_V_ALUMC_TEL_CONTACTO";
	private static final String E_V_ALUMC_USUARIO = "E_V_ALUMC_USUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateEncuestaAlumno(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_V_ALUMC_ID, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_NOM_PRIMERO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_NOM_SEGUNDO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_APE_MATERNO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_APE_PATERNO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_TIPTC_TIPO_DOC, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_NRO_DOC, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_CORREO_PERSONAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_DIRECCION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_TEL_CASA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_TEL_CELULAR, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_COD_EMPRESA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_AREA_TRABAJO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_CARGO_TRABAJO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_CORREO_EMPRESA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_CONTACTO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_TEL_CONTACTO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_ALUMC_USUARIO, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codAlumno, EncuestaAlumno encuestaAlumno,String codUsuario) {
		
	Map inputs = new HashMap();
	//18 -> 17
	/*System.out.println("codAlumno: "+codAlumno);
	System.out.println("PrimerNombre: "+encuestaAlumno.getPrimerNombre());
	System.out.println("SegundoNombre: "+encuestaAlumno.getSegundoNombre());
	System.out.println("ApellidoMaterno: "+encuestaAlumno.getApellidoMaterno());
	System.out.println("ApellidoPaterno :"+encuestaAlumno.getApellidoPaterno());
	System.out.println("CodTipoDoc :"+encuestaAlumno.getCodTipoDoc());
	System.out.println("NroTipoDoc :"+encuestaAlumno.getNroTipoDoc());
	System.out.println("Email :"+encuestaAlumno.getEmail());
	System.out.println("Direccion :"+encuestaAlumno.getDireccion());
	System.out.println("TelfCasa :"+encuestaAlumno.getTelfCasa());
	System.out.println("TelfCelular :"+encuestaAlumno.getTelfCelular());
	System.out.println("CodEmpresa :"+encuestaAlumno.getCodEmpresa());
	System.out.println("NombreArea :"+encuestaAlumno.getNombreArea());
	System.out.println("NombreCargo :"+encuestaAlumno.getNombreCargo());
	System.out.println("CorreoEmpresa :"+encuestaAlumno.getCorreoEmpresa());
	System.out.println("NombreContacto :"+encuestaAlumno.getNombreContacto());
	System.out.println("TelfContacto :"+encuestaAlumno.getTelfContacto());
	System.out.println("codUsuario :"+codUsuario);*/
	
	inputs.put(E_V_ALUMC_ID, codAlumno);// 18 parametros de entrada
	inputs.put(E_V_ALUMC_NOM_PRIMERO, encuestaAlumno.getPrimerNombre());
	inputs.put(E_V_ALUMC_NOM_SEGUNDO, encuestaAlumno.getSegundoNombre());
	inputs.put(E_V_ALUMC_APE_MATERNO, encuestaAlumno.getApellidoMaterno());
	inputs.put(E_V_ALUMC_APE_PATERNO, encuestaAlumno.getApellidoPaterno());
	inputs.put(E_C_TIPTC_TIPO_DOC, encuestaAlumno.getCodTipoDoc());
	inputs.put(E_V_ALUMC_NRO_DOC, encuestaAlumno.getNroTipoDoc());
	inputs.put(E_V_ALUMC_CORREO_PERSONAL, encuestaAlumno.getEmail());
	inputs.put(E_V_ALUMC_DIRECCION, encuestaAlumno.getDireccion());
	inputs.put(E_V_ALUMC_TEL_CASA, encuestaAlumno.getTelfCasa());
	inputs.put(E_V_ALUMC_TEL_CELULAR, encuestaAlumno.getTelfCelular());
	inputs.put(E_V_ALUMC_COD_EMPRESA, encuestaAlumno.getCodEmpresa());
	inputs.put(E_V_ALUMC_AREA_TRABAJO, encuestaAlumno.getNombreArea());
	inputs.put(E_V_ALUMC_CARGO_TRABAJO, encuestaAlumno.getNombreCargo());
	inputs.put(E_V_ALUMC_CORREO_EMPRESA, encuestaAlumno.getCorreoEmpresa());
	inputs.put(E_V_ALUMC_CONTACTO, encuestaAlumno.getNombreContacto());
	inputs.put(E_V_ALUMC_TEL_CONTACTO, encuestaAlumno.getTelfContacto());
	inputs.put(E_V_ALUMC_USUARIO, codUsuario);//
	
	return super.execute(inputs);
	}

}
