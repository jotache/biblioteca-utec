package com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.ConsultaByProfesorConBean;
import com.tecsup.SGA.bean.ValidaReporteBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.Pregunta;

public class GetAllValidaRptaReporte extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_consultas_reportes.SP_SEL_VALIDA_RPTA_REPORTE";

	/*V_C_TIPO_REPORTE VARCHAR2,
V_V_NRO_ENCUESTA VARCHAR2,
	 * 
	 * */
	private static final String V_C_TIPO_REPORTE = "V_C_TIPO_REPORTE";
	private static final String V_V_NRO_ENCUESTA = "V_V_NRO_ENCUESTA";
	private static final String V_V_COD_RESPONSABLE = "V_V_COD_RESPONSABLE";
	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllValidaRptaReporte(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(V_C_TIPO_REPORTE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(V_V_NRO_ENCUESTA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(V_V_COD_RESPONSABLE, OracleTypes.VARCHAR));
        
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String tipoReporte,String codEncuesta,String codResponsable){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(V_C_TIPO_REPORTE,tipoReporte);
    	inputs.put(V_V_NRO_ENCUESTA,codEncuesta);
    	inputs.put(V_V_COD_RESPONSABLE,codResponsable);
    	    	
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ValidaReporteBean bean= new ValidaReporteBean();
        	
        	bean.setCodResultado(rs.getString("COD_RESULTADO") == null ? "": rs.getString("COD_RESULTADO").trim());
        	bean.setDesResultado(rs.getString("DES_RESULTADO") == null ? "": rs.getString("DES_RESULTADO").trim());
        	bean.setIdEncuesta(rs.getString("ID_ENCUESTA") == null ? "": rs.getString("ID_ENCUESTA").trim());        	
        	bean.setCodProducto(rs.getString("COD_PRODUCTO") == null ? "": rs.getString("COD_PRODUCTO").trim());
        	bean.setCodPrograma(rs.getString("COD_PROGRAMA") == null ? "": rs.getString("COD_PROGRAMA").trim());
        	
        	return bean;
        }
    }

}
