package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.FormatoBean;
import com.tecsup.SGA.bean.VerEncuestaBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class GetAllVerEncuesta extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_configuracion.SP_SEL_VER_ENCUESTA";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllVerEncuesta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.CHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjectaMapper()));
        compile();
    }

    public Map execute(String codEncuesta) {
    	
    	Map inputs = new HashMap();

    	inputs.put(E_V_FENCC_ID, codEncuesta);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjectaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	VerEncuestaBean bean= new VerEncuestaBean();
        	        	
        	bean.setIdEncuesta(rs.getString("ID_ENCUESTA") == null ? "": rs.getString("ID_ENCUESTA"));
        	bean.setIdSeccion(rs.getString("ID_SECCION") == null ? "": rs.getString("ID_SECCION"));
        	bean.setNomSeccion(rs.getString("NOM_SECCION") == null ? "": rs.getString("NOM_SECCION"));
        	bean.setIdGrupo(rs.getString("ID_GRUPO") == null ? "": rs.getString("ID_GRUPO"));
        	bean.setNomGrupo(rs.getString("NOM_GRUPO") == null ? "": rs.getString("NOM_GRUPO"));
        	bean.setNivelAlternativa(rs.getString("NIVEL_ALTERNATIVA") == null ? "": rs.getString("NIVEL_ALTERNATIVA"));
        	bean.setIdPregunta(rs.getString("ID_PREGUNTA") == null ? "": rs.getString("ID_PREGUNTA"));
        	bean.setNomPregunta(rs.getString("NOM_PREGUNTA") == null ? "": rs.getString("NOM_PREGUNTA"));
        	bean.setIdAlternativa(rs.getString("ID_ALTERNATIVA") == null ? "": rs.getString("ID_ALTERNATIVA"));
        	bean.setNomAlterntiva(rs.getString("NOM_ALTERNATIVA") == null ? "": rs.getString("NOM_ALTERNATIVA"));
        	bean.setDesAlternativa(rs.getString("DES_ALTERNATIVA") == null ? "": rs.getString("DES_ALTERNATIVA"));
        	bean.setIndObligatorio(rs.getString("IND_OBLIGATORIO") == null ? "": rs.getString("IND_OBLIGATORIO"));
        	bean.setTipoPregunta(rs.getString("TIPO_PREGUNTA") == null ? "": rs.getString("TIPO_PREGUNTA"));
        	bean.setIndUnica(rs.getString("IND_UNICA") == null ? "": rs.getString("IND_UNICA"));        	
        	
        	return bean;
        }
    }

}
