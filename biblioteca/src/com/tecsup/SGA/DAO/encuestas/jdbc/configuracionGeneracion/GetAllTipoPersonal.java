package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.Empleados;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.PerfilEncuesta;

public class GetAllTipoPersonal extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_config_perfil.sp_sel_tipo_personal";
	
	private static final String E_V_COD_TIPO_PERSONAL = "E_V_COD_TIPO_PERSONAL";
	private static final String E_V_NOM_TIPO_PERSONAL = "E_V_NOM_TIPO_PERSONAL";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllTipoPersonal(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_TIPO_PERSONAL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_TIPO_PERSONAL, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codTipoPersonal, String nomTipoPersonal){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_COD_TIPO_PERSONAL,codTipoPersonal);
    	inputs.put(E_V_NOM_TIPO_PERSONAL,nomTipoPersonal);
    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Empleados empleado = new Empleados();
        	
        	empleado.setCodigo(rs.getString("COD_TIPO_PERSONAL") == null ? "": rs.getString("COD_TIPO_PERSONAL"));
        	empleado.setNombre(rs.getString("NOM_TIPO_PERSONAL") == null ? "": rs.getString("NOM_TIPO_PERSONAL"));        	
        	
        	return empleado;
        }
    }

}
