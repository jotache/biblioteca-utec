	package com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.ConsultaByProfesorConBean;
import com.tecsup.SGA.bean.ConsultaByProfesorDetBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.Pregunta;

public class GetAllConsultaByProfesorDetPFRCer extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".pkg_enc_consultas_reportes.sp_sel_prof_detallada_pfr_c";
	
	private static final String V_V_NRO_ENCUESTA = "V_V_NRO_ENCUESTA";
	private static final String V_V_COD_PROFESOR = "V_V_COD_PROFESOR";	
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllConsultaByProfesorDetPFRCer(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(V_V_NRO_ENCUESTA, OracleTypes.VARCHAR));        
        declareParameter(new SqlParameter(V_V_COD_PROFESOR, OracleTypes.VARCHAR));
      
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta,String codProfesor){
    	
    	Map inputs = new HashMap();    	
    	inputs.put(V_V_NRO_ENCUESTA,codEncuesta);    	
    	inputs.put(V_V_COD_PROFESOR,codProfesor);
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ConsultaByProfesorDetBean bean= new ConsultaByProfesorDetBean();        	
    
        	bean.setIdSeccion(rs.getString("ID_SECCION") == null ? "": rs.getString("ID_SECCION"));
        	bean.setCodSeccion(rs.getString("COD_SECCION") == null ? "": rs.getString("COD_SECCION"));
        	bean.setNomSeccion(rs.getString("NOM_SECCION") == null ? "": rs.getString("NOM_SECCION"));
        	bean.setCodProducto(rs.getString("COD_PRODUCTO") == null ? "": rs.getString("COD_PRODUCTO"));
        	bean.setCodPrograma(rs.getString("COD_PROGRAMA") == null ? "": rs.getString("COD_PROGRAMA"));
        	bean.setCodDepartamento(rs.getString("COD_DPTO") == null ? "": rs.getString("COD_DPTO"));
        	bean.setNomDepartamento(rs.getString("NOM_DPTO") == null ? "": rs.getString("NOM_DPTO"));
        	bean.setCodCurso(rs.getString("COD_CURSO") == null ? "": rs.getString("COD_CURSO"));
        	bean.setNomCurso(rs.getString("NOM_CURSO") == null ? "": rs.getString("NOM_CURSO"));
        	bean.setCodCiclo(rs.getString("COD_CICLO") == null ? "": rs.getString("COD_CICLO"));
        	bean.setNomCiclo(rs.getString("NOM_CICLO") == null ? "": rs.getString("NOM_CICLO"));        	
        	bean.setCodSeccionAlumno(rs.getString("COD_SECCION_ALUMNO") == null ? "": rs.getString("COD_SECCION_ALUMNO"));
        	bean.setNomSeccionAlumno(rs.getString("NOM_SECCION_ALUMNO") == null ? "": rs.getString("NOM_SECCION_ALUMNO"));
        	bean.setCodGrupo(rs.getString("ID_GRUPO") == null ? "": rs.getString("ID_GRUPO"));
        	bean.setNomGrupo(rs.getString("NOM_GRUPO") == null ? "": rs.getString("NOM_GRUPO"));
        	bean.setCodPregunta(rs.getString("ID_PREGUNTA") == null ? "": rs.getString("ID_PREGUNTA"));
        	bean.setNomPregunta(rs.getString("DES_PREGUNTA") == null ? "": rs.getString("DES_PREGUNTA"));
        	bean.setCodAlternativa(rs.getString("ID_ALTERNATIVA") == null ? "": rs.getString("ID_ALTERNATIVA"));
        	bean.setNomAlternativa(rs.getString("NOM_ALTERNATIVA") == null ? "": rs.getString("NOM_ALTERNATIVA"));
        	bean.setPesoAlternativa(rs.getString("PESO_ALTERNATIVA") == null ? "": rs.getString("PESO_ALTERNATIVA"));
        	bean.setNroRespuestas(rs.getString("NRO_RPTAS") == null ? "": rs.getString("NRO_RPTAS"));
        	bean.setTotalAlternativa(rs.getString("TOTAL_ALTERNATIVA") == null ? "": rs.getString("TOTAL_ALTERNATIVA"));
        	
        	return bean;
        }
    }

}
