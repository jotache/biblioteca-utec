package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeletePreguntasByGrupo extends StoredProcedure {
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA
			+ ".PKG_ENC_CONFIGURACION.SP_DEL_PREGUNTAS_X_GRUPO";

	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_FPREC_ID = "E_V_FPREC_ID";
	private static final String E_V_FPREC_USU_MODI = "E_V_FPREC_USU_MODI";
	private static final String S_V_RETVAL = "S_V_RETVAL";

	
	public DeletePreguntasByGrupo(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FPREC_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FPREC_USU_MODI, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(
			String codEncuesta,
			String codPregunta,
			String usuario) {

		Map inputs = new HashMap();

		inputs.put(E_V_FENCC_ID, codEncuesta);
		inputs.put(E_V_FPREC_ID, codPregunta);
		inputs.put(E_V_FPREC_USU_MODI, usuario);
		
		return super.execute(inputs);

	}

}
