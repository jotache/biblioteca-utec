package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteAmbitoProfesor extends StoredProcedure {
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA
			+ ".pkg_enc_config_perfil.sp_del_ambito_profesor";
/*
 * PROCEDURE SP_DEL_AMBITO_PROFESOR(
	E_V_FENCC_ID IN VARCHAR2,	
	E_C_COD_PERFIL IN CHAR,
	E_V_COD_PROFESOR IN VARCHAR2,
	E_V_COD_PRODUCTO IN VARCHAR2,
	E_V_COD_PROGRAMA IN VARCHAR2,
	E_V_COD_CECO IN VARCHAR2,
	E_V_COD_CURSO IN VARCHAR2,
	E_V_COD_CICLO IN VARCHAR2,
	E_V_COD_SECCION IN VARCHAR2,      
	E_V_COD_USUARIO IN VARCHAR2,
	S_V_RETVAL IN OUT VARCHAR2);
 */
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";	
	private static final String E_C_COD_PERFIL = "E_C_COD_PERFIL";
	private static final String E_V_COD_PROFESOR = "E_V_COD_PROFESOR";
	private static final String E_V_COD_PRODUCTO = "E_V_COD_PRODUCTO";
	private static final String E_V_COD_PROGRAMA = "E_V_COD_PROGRAMA";
	private static final String E_V_COD_CECO = "E_V_COD_CECO";
	private static final String E_V_COD_CURSO = "E_V_COD_CURSO";
	private static final String E_V_COD_CICLO = "E_V_COD_CICLO";
	private static final String E_V_COD_SECCION = "E_V_COD_SECCION";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";

	public DeleteAmbitoProfesor(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));		
		declareParameter(new SqlParameter(E_C_COD_PERFIL, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_COD_PROFESOR, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_PRODUCTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_PROGRAMA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_CECO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_CURSO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_CICLO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_SECCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}

	public Map execute(String codEncuesta, String codPerfil, String codProfesor, 
			String codProducto, String codPrograma, String codCentroCosto,
			String codCurso, String codCiclo, String codSeccion, String codUsuario) {

		Map inputs = new HashMap();

		inputs.put(E_V_FENCC_ID, codEncuesta);		
		inputs.put(E_C_COD_PERFIL, codPerfil);
		inputs.put(E_V_COD_PROFESOR, codProfesor);
		inputs.put(E_V_COD_PRODUCTO, codProducto);
		inputs.put(E_V_COD_PROGRAMA, codPrograma);
		inputs.put(E_V_COD_CECO, codCentroCosto);
		inputs.put(E_V_COD_CURSO, codCurso);
		inputs.put(E_V_COD_CICLO, codCiclo);
		inputs.put(E_V_COD_SECCION, codSeccion);
		inputs.put(E_V_COD_USUARIO, codUsuario);
		
		return super.execute(inputs);
	}
}
