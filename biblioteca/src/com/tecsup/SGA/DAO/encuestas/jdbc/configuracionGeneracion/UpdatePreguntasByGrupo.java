package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdatePreguntasByGrupo extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".PKG_ENC_CONFIGURACION.SP_ACT_PREGUNTAS_X_GRUPO";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID"; 
	private static final String E_V_FGRUC_ID = "E_V_FGRUC_ID";
	private static final String E_V_FPREC_ID = "E_V_FPREC_ID";
	private static final String E_V_FPREC_PREGUNTA = "E_V_FPREC_PREGUNTA";		
	private static final String E_C_TIPTC_TIPO_PREGUNTA = "E_C_TIPTC_TIPO_PREGUNTA";	
	private static final String E_C_FPREC_IND_OBLIGATORIO = "E_C_FPREC_IND_OBLIGATORIO";
	private static final String E_C_FPREC_IND_UNICA = "E_C_FPREC_IND_UNICA";	
	private static final String E_V_FPREC_PESO = "E_V_FPREC_PESO";
	private static final String E_C_FPREC_IND_ALTERNATIVA = "E_C_FPREC_IND_ALTERNATIVA";
	private static final String E_V_FPREC_USU_MODI = "E_V_FPREC_USU_MODI";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";


	public UpdatePreguntasByGrupo(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FGRUC_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FPREC_ID, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_FPREC_PREGUNTA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_TIPTC_TIPO_PREGUNTA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FPREC_IND_OBLIGATORIO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FPREC_IND_UNICA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_FPREC_PESO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FPREC_IND_ALTERNATIVA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_FPREC_USU_MODI, OracleTypes.VARCHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(
			String codEncuesta,
			String codGrupo,
			String codPregunta,
			String pregunta,
			String tipoPregunta,
			String indObligatorio,
			String indUnica,
			String peso,
			String indAlternativa,
			String usuCrea
			) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_FENCC_ID, codEncuesta);
	inputs.put(E_V_FGRUC_ID, codGrupo);
	inputs.put(E_V_FPREC_ID, codPregunta);
	inputs.put(E_V_FPREC_PREGUNTA, pregunta);
	inputs.put(E_C_TIPTC_TIPO_PREGUNTA, tipoPregunta);
	inputs.put(E_C_FPREC_IND_OBLIGATORIO, indObligatorio);
	inputs.put(E_C_FPREC_IND_UNICA, indUnica);
	inputs.put(E_V_FPREC_PESO, peso);
	inputs.put(E_C_FPREC_IND_ALTERNATIVA, indAlternativa);
	inputs.put(E_V_FPREC_USU_MODI, usuCrea);
	
	return super.execute(inputs);
	}

	
}
