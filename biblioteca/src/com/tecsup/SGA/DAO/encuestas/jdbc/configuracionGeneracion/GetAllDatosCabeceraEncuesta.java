package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class GetAllDatosCabeceraEncuesta extends StoredProcedure{
	
	private static Log log = LogFactory.getLog(GetAllDatosCabeceraEncuesta.class);
			
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
		+ ".pkg_enc_configuracion.SP_SEL_DATOS_CABECERA_ENCUESTA";
	
	/*
	 * E_V_FENCC_ID IN VARCHAR2,
E_V_COD_PROFESOR IN VARCHAR2,
E_V_COD_PRODUCTO IN VARCHAR2,
E_V_COD_PROGRAMA IN VARCHAR2,
S_C_RECORDSET IN OUT CUR_RECORDSET)
	 * */

	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_COD_PROFESOR = "E_V_COD_PROFESOR";
	//private static final String E_V_COD_PRODUCTO = "E_V_COD_PRODUCTO";
	//private static final String E_V_COD_PROGRAMA = "E_V_COD_PROGRAMA";

	private static final String S_C_RECORDSET = "S_C_RECORDSET";	

    public GetAllDatosCabeceraEncuesta(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_PROFESOR, OracleTypes.VARCHAR));
        //declareParameter(new SqlParameter(E_V_COD_PRODUCTO, OracleTypes.VARCHAR));
        //declareParameter(new SqlParameter(E_V_COD_PROGRAMA, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    //public Map execute(String codEncuesta, String codProfesor,String codProducto,String codPrograma) {
    public Map execute(String codEncuesta, String codProfesor){
    	
    	Map inputs = new HashMap();
    	
    	log.info("--------- Start");
    	log.info("--------- SPROC_NAME: " + SPROC_NAME);
    	log.info("E_V_FENCC_ID:"+codEncuesta);
    	log.info("E_V_COD_PROFESOR:"+codProfesor);
    	log.info("--------- End");
    	
    	inputs.put(E_V_FENCC_ID, codEncuesta);
    	inputs.put(E_V_COD_PROFESOR, codProfesor);
    	//inputs.put(E_V_COD_PRODUCTO, codProducto);
    	//inputs.put(E_V_COD_PROGRAMA, codPrograma);
    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	DatosCabeceraBean datosCabeceraBean= new DatosCabeceraBean();
        	        	
        	datosCabeceraBean.setCodTipoAplicacion(rs.getString("COD_TIPO_APLICACION") == null ? "": rs.getString("COD_TIPO_APLICACION"));
        	datosCabeceraBean.setNomTipoAplicacion(rs.getString("NOM_TIPO_APLICACION") == null ? "": rs.getString("NOM_TIPO_APLICACION"));
        	datosCabeceraBean.setCodTipoEncuesta(rs.getString("COD_TIPO_ENCUESTA") == null ? "": rs.getString("COD_TIPO_ENCUESTA"));
        	datosCabeceraBean.setNomTipoEncuesta(rs.getString("NOM_TIPO_ENCUESTA") == null ? "": rs.getString("NOM_TIPO_ENCUESTA"));
        	datosCabeceraBean.setNomServicio(rs.getString("NOM_SERVICIO") == null ? "": rs.getString("NOM_SERVICIO"));
        	datosCabeceraBean.setNomEncuesta(rs.getString("NOM_ENCUESTA") == null ? "": rs.getString("NOM_ENCUESTA"));
        	
        	datosCabeceraBean.setNroNomEncuesta(rs.getString("NRO_NOM_ENCUESTA") == null ? "": rs.getString("NRO_NOM_ENCUESTA"));
        	datosCabeceraBean.setDuracion(rs.getString("DURACION") == null ? "": rs.getString("DURACION"));
        	datosCabeceraBean.setNomProfesor(rs.getString("NOM_PROFESOR") == null ? "": rs.getString("NOM_PROFESOR"));
        	
        	datosCabeceraBean.setTotEncuestados(rs.getString("TOTAL_ENCUESTADOS") == null ? "": rs.getString("TOTAL_ENCUESTADOS"));
        	datosCabeceraBean.setDetEncuestados(rs.getString("DETAL_ENCUESTADOS") == null ? "": rs.getString("DETAL_ENCUESTADOS"));
        	datosCabeceraBean.setTotEncuestadosCrpta(rs.getString("TOTAL_ENCUESTADOS_CRPTA") == null ? "": rs.getString("TOTAL_ENCUESTADOS_CRPTA"));
        	
        	
        	datosCabeceraBean.setNomProducto(rs.getString("NOM_PRODUCTO") == null ? "": rs.getString("NOM_PRODUCTO"));
        	datosCabeceraBean.setNomPrograma(rs.getString("NOM_PROGRAMA") == null ? "": rs.getString("NOM_PROGRAMA"));
        	
        	
        	datosCabeceraBean.setCodSede(rs.getString("COD_SEDE") == null ? "": rs.getString("COD_SEDE"));
        	datosCabeceraBean.setNomSede(rs.getString("NOM_SEDE") == null ? "": rs.getString("NOM_SEDE"));
        	datosCabeceraBean.setCodProducto(rs.getString("COD_PRODUCTO") == null ? "": rs.getString("COD_PRODUCTO"));
        	datosCabeceraBean.setCodPrograma(rs.getString("COD_PROGRAMA") == null ? "": rs.getString("COD_PROGRAMA"));
        	
        	datosCabeceraBean.setAnioIniEgresado(rs.getString("ANIO_INI_EGRESADO") == null ? "": rs.getString("ANIO_INI_EGRESADO"));
        	datosCabeceraBean.setAnioFinEgresado(rs.getString("ANIO_FIN_EGRESADO") == null ? "": rs.getString("ANIO_FIN_EGRESADO"));
            
        	return datosCabeceraBean;
        }
    }

}
