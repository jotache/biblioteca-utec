package com.tecsup.SGA.DAO.encuestas.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.encuestas.ActualizacionDatosDAO;
import com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos.GetAllEncuestaAlumno;
import com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos.GetAllEncuestaEmpresa;
import com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos.GetVerificarAlumno;
import com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos.GetVerificarTieneEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.actualizacionDatos.UpdateEncuestaAlumno;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.EncuestaEmpresa;
import com.tecsup.SGA.modelo.Usuario;

public class ActualizacionDatosDAOJdbc extends JdbcDaoSupport implements ActualizacionDatosDAO{

	
	public List<EncuestaAlumno> GetAllEncuestaAlumno(String codAlumno){
		
		GetAllEncuestaAlumno getAllEncuestaAlumno = new GetAllEncuestaAlumno(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEncuestaAlumno.execute(codAlumno);
		if(!outputs.isEmpty()){
			return (List<EncuestaAlumno>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}//
	public String UpdateEncuestaAlumno(String codAlumno, EncuestaAlumno encuestaAlumno,String codUsuario) {
		
		UpdateEncuestaAlumno updateEncuestaAlumno = new UpdateEncuestaAlumno(this.getDataSource());		
		
		HashMap inputs = (HashMap)updateEncuestaAlumno.execute(codAlumno, encuestaAlumno, codUsuario);
				 
		if (!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}//
	public List<EncuestaEmpresa> GetAllEncuestaEmpresa(String nombreEmpresa){
		
		GetAllEncuestaEmpresa getAllEncuestaEmpresa = new GetAllEncuestaEmpresa(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEncuestaEmpresa.execute(nombreEmpresa);
		if(!outputs.isEmpty()){
			return (List<EncuestaEmpresa>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}//
	
	public List<EncuestaAlumno> GetVerificarAlumno(String codAlumno){
		
		GetVerificarAlumno getVerificarAlumno = new GetVerificarAlumno(this.getDataSource());
		
		HashMap outputs = (HashMap)getVerificarAlumno.execute(codAlumno);
		if(!outputs.isEmpty()){
			return (List<EncuestaAlumno>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List<Usuario> GetVerificarTieneEncuesta(String codUsuario){
		
		GetVerificarTieneEncuesta getVerificarTieneEncuesta = new GetVerificarTieneEncuesta(this.getDataSource());		
		
		HashMap outputs = (HashMap)getVerificarTieneEncuesta.execute(codUsuario);
		
		if(!outputs.isEmpty()){
			return (List<Usuario>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
}
