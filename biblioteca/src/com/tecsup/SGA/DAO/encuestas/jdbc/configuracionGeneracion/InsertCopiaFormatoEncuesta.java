package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertCopiaFormatoEncuesta extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".pkg_enc_configuracion.sp_ins_copia_formato_encuesta";
	/*
	 * PROCEDURE SP_INS_COPIA_FORMATO_ENCUESTA(
		E_V_FENCC_ID IN VARCHAR2,
		S_V_RETVAL IN OUT VARCHAR2)
	 */
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertCopiaFormatoEncuesta(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.CHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	public Map execute(String codEncuesta){
		Map inputs = new HashMap();
		
		inputs.put(E_V_FENCC_ID, codEncuesta);
		
		return super.execute(inputs);
	}	
}