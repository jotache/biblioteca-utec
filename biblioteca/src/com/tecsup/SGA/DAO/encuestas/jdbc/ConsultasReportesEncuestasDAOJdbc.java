package com.tecsup.SGA.DAO.encuestas.jdbc;


import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;


import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.DAO.encuestas.ConsultasReportesEncuestasDAO;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllConfiguracionPerfil;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCondicionCotizacion;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteEncuestaManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteEncuestaFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeleteGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.DeletePreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllAgregarFormatoManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllDatosCabeceraEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllDatosPersonal;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllEncuestaManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllEncuestasPublicadas;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllFormatoEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllPreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllVerEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertAgregarFormatoManual;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertCopiaFormatoEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertRespuestaByEncuestado;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateConfiguracionPerfil;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertPreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateAlternativa;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateGeneraEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateGruposByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdatePreguntasByGrupo;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.GetAllEncuestaByFormato;

import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.UpdateEncuestaByFormato;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.AmpliarProgramacionEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.AplicarEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllAmbitoGeneral;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllCiclos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllCursos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllDepartamentos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllPerfilEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllPerfilesByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProductos;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProfesorPCC;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProfesorPFR;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllProgramas;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllTipoDocente;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAllTipoPersonal;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.GetAmpliarProgByEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil.InsertPerfilEncuesta;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarConsAlumno;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarConsEgresado;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarConsPersonal;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarConsServicio;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllProfConsolidadaPCC;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllProfConsolidadaPFR;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllValidaRptaReporte;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllConsultaByProfesorDetPCCAbi;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllConsultaByProfesorDetPCCCer;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllConsultaByProfesorDetPFRAbi;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllConsultaByProfesorDetPFRCer;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetalleAlumnoAbi;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetalleAlumnoCer;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetalleEgresadoAbi;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetalleEgresadoCer;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetallePersonalAbi;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetallePersonalCer;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetalleServicioAbi;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllEstandarDetalleServicioCer;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllMixtaDetalleAbierta;
import com.tecsup.SGA.DAO.encuestas.jdbc.consultasReportes.GetAllMixtaDetalleCerrada;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.FormatoEncuesta;

public class ConsultasReportesEncuestasDAOJdbc extends JdbcDaoSupport implements ConsultasReportesEncuestasDAO{	

	public List getAllProfConsolidadaPFR(String codEncuesta,
			String codProfesor) {
		
		GetAllProfConsolidadaPFR getAllProfConsolidadaPFR = new GetAllProfConsolidadaPFR(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllProfConsolidadaPFR.execute(codEncuesta, codProfesor);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllProfConsolidadaPCC(String codEncuesta,
			String codProfesor) {
		
		GetAllProfConsolidadaPCC getAllProfConsolidadaPCC = new GetAllProfConsolidadaPCC(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllProfConsolidadaPCC.execute(codEncuesta, codProfesor);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}	

	public List getAllEstandarConsEgresado(String codEncuesta) {
		
		GetAllEstandarConsEgresado getAllEstandarConsEgresado = new GetAllEstandarConsEgresado(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllEstandarConsEgresado.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}	

	public List getAllEstandarConsAlumno(String codEncuesta) {
		
		GetAllEstandarConsAlumno getAllEstandarConsAlumno = new GetAllEstandarConsAlumno(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllEstandarConsAlumno.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}	

	public List getAllEstandarConsPersonal(String codEncuesta) {
		
		GetAllEstandarConsPersonal getAllEstandarConsPersonal = new GetAllEstandarConsPersonal(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllEstandarConsPersonal.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllEstandarConsServicio(String codEncuesta) {
		
		GetAllEstandarConsServicio getAllEstandarConsServicio = new GetAllEstandarConsServicio(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllEstandarConsServicio.execute(codEncuesta);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllConsultaByProfesorDetPRFCer(String codEncuesta, String codProfesor) {
		GetAllConsultaByProfesorDetPFRCer getAllConsultaByProfesorDetPFRCer = new GetAllConsultaByProfesorDetPFRCer(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllConsultaByProfesorDetPFRCer.execute(codEncuesta, codProfesor);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllConsultaByProfesorDetPFRAbi(String codEncuesta, String codProfesor) {
		
		GetAllConsultaByProfesorDetPFRAbi getAllConsultaByProfesorDetPFRAbi = new GetAllConsultaByProfesorDetPFRAbi(this.getDataSource());												
		
		HashMap outputs = (HashMap)getAllConsultaByProfesorDetPFRAbi.execute(codEncuesta, codProfesor);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllConsultaByProfesorDetPCCCer(String codEncuesta, String codProfesor) {
		
		GetAllConsultaByProfesorDetPCCCer getAllConsultaByProfesorDetPCCCer = new GetAllConsultaByProfesorDetPCCCer(this.getDataSource());												
		
		HashMap outputs = (HashMap)getAllConsultaByProfesorDetPCCCer.execute(codEncuesta, codProfesor);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllConsultaByProfesorDetPCCAbi(String codEncuesta, String codProfesor) {
		
		GetAllConsultaByProfesorDetPCCAbi getAllConsultaByProfesorDetPCCAbi = new GetAllConsultaByProfesorDetPCCAbi(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllConsultaByProfesorDetPCCAbi.execute(codEncuesta, codProfesor);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllEstandarDetalleAlumnoCer(String codEncuesta) {
		
		GetAllEstandarDetalleAlumnoCer getAllEstandarDetalleAlumnoCer = new GetAllEstandarDetalleAlumnoCer(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEstandarDetalleAlumnoCer.execute(codEncuesta);				
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllEstandarDetalleAlumnoAbi(String codEncuesta) {
		
		GetAllEstandarDetalleAlumnoAbi getAllEstandarDetalleAlumnoAbi = new GetAllEstandarDetalleAlumnoAbi(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEstandarDetalleAlumnoAbi.execute(codEncuesta);				
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllEstandarDetalleEgresadoCer(String codEncuesta) {
		
		GetAllEstandarDetalleEgresadoCer getAllEstandarDetalleEgresadoCer = new GetAllEstandarDetalleEgresadoCer(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEstandarDetalleEgresadoCer.execute(codEncuesta);						
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllEstandarDetalleEgresadoAbi(String codEncuesta) {
		
		GetAllEstandarDetalleEgresadoAbi getAllEstandarDetalleEgresadoAbi = new GetAllEstandarDetalleEgresadoAbi(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllEstandarDetalleEgresadoAbi.execute(codEncuesta);						
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllEstandarDetallePersonalCer(String codEncuesta) {
		
		GetAllEstandarDetallePersonalCer getAllEstandarDetallePersonalCer = new GetAllEstandarDetallePersonalCer(this.getDataSource());
				
		HashMap outputs = (HashMap)getAllEstandarDetallePersonalCer.execute(codEncuesta);						
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllEstandarDetallePersonalAbi(String codEncuesta) {
		
		GetAllEstandarDetallePersonalAbi getAllEstandarDetallePersonalAbi = new GetAllEstandarDetallePersonalAbi(this.getDataSource());
				
		HashMap outputs = (HashMap)getAllEstandarDetallePersonalAbi.execute(codEncuesta);						
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllEstandarDetalleServicioCer(String codEncuesta) {
		
		GetAllEstandarDetalleServicioCer getAllEstandarDetalleServicioCer = new GetAllEstandarDetalleServicioCer(this.getDataSource());		
				
		HashMap outputs = (HashMap)getAllEstandarDetalleServicioCer.execute(codEncuesta);						
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllEstandarDetalleServicioAbi(String codEncuesta) {
		
		GetAllEstandarDetalleServicioAbi getAllEstandarDetalleServicioAbi = new GetAllEstandarDetalleServicioAbi(this.getDataSource());		
				
		HashMap outputs = (HashMap)getAllEstandarDetalleServicioAbi.execute(codEncuesta);						
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllMixtaDetalleCerrada(String codEncuesta) {
		
		GetAllMixtaDetalleCerrada getAllMixtaDetalleCerrada = new GetAllMixtaDetalleCerrada(this.getDataSource());				
				
		HashMap outputs = (HashMap)getAllMixtaDetalleCerrada.execute(codEncuesta);								
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public List GetAllMixtaDetalleAbierta(String codEncuesta) {
		
		GetAllMixtaDetalleAbierta getAllMixtaDetalleAbierta = new GetAllMixtaDetalleAbierta(this.getDataSource());						
				
		HashMap outputs = (HashMap)getAllMixtaDetalleAbierta.execute(codEncuesta);
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
		public List getAllValidaRptaReporte(String tipoReporte,String codEncuesta,String codResponsable) {
		
		GetAllValidaRptaReporte getAllValidaRptaReporte = new GetAllValidaRptaReporte(this.getDataSource());										
		
		HashMap outputs = (HashMap)getAllValidaRptaReporte.execute(tipoReporte, codEncuesta,codResponsable);		
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}	
	
}