package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.EncuestaManualBean;
import com.tecsup.SGA.bean.FormatoEncuestaBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alternativa;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Grupo;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.modelo.PerfilProfesor;

public class GetAllEncuestaManual extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA 
	+ ".PKG_ENC_CONFIGURACION.sp_sel_encuesta_manual";
	
	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllEncuestaManual(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codEncuesta){
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_FENCC_ID,codEncuesta);    	     	
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {        	
        	
        	EncuestaManualBean bean = new EncuestaManualBean();

        	bean.setCodEncuestado(rs.getString("COD_ENCUESTADO") == null ? "": rs.getString("COD_ENCUESTADO"));
        	bean.setFecRegistro(rs.getString("FEC_REGISTRO") == null ? "": rs.getString("FEC_REGISTRO"));        	
        	bean.setIdGrupo(rs.getString("ID_GRUPO") == null ? "": rs.getString("ID_GRUPO"));
        	//bean.setNivelAlternativa(rs.getString("NIVEL_ALTERNATIVA") == null ? "": rs.getString("NIVEL_ALTERNATIVA"));
        	bean.setIdPregunta(rs.getString("ID_PREGUNTA") == null ? "": rs.getString("ID_PREGUNTA"));
        	bean.setNomPregunta(rs.getString("NOM_PREGUNTA") == null ? "": rs.getString("NOM_PREGUNTA"));
        	bean.setRpta(rs.getString("RPTA") == null ? "": rs.getString("RPTA"));
        	bean.setCadNomAlternativa(rs.getString("CAD_NOM_ALTERNATIVA") == null ? "": rs.getString("CAD_NOM_ALTERNATIVA"));
        	bean.setIndObligatorio(rs.getString("IND_OBLIGATORIO") == null ? "": rs.getString("IND_OBLIGATORIO"));
        	bean.setTipoPregunta(rs.getString("TIPO_PREGUNTA") == null ? "": rs.getString("TIPO_PREGUNTA"));
        	bean.setIndUnica(rs.getString("IND_UNICA") == null ? "": rs.getString("IND_UNICA"));
        	
        	return bean;
        }
    }

}
