package com.tecsup.SGA.DAO.encuestas.jdbc.configuracionPerfil;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;

public class UpdateTerminarEncuesta extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_ENCUESTA +
	".PKG_ENC_CONFIG_PERFIL.SP_ACT_TERMINAR_ENCUESTA";

	private static final String E_V_FENCC_ID = "E_V_FENCC_ID";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public UpdateTerminarEncuesta(DataSource dataSource){
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_V_FENCC_ID, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(
			String codEncuesta,		
			String usuCrea
			) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_V_FENCC_ID, codEncuesta);
	inputs.put(E_V_COD_USUARIO, usuCrea);
	
	return super.execute(inputs);
	}

	
}
