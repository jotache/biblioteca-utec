package com.tecsup.SGA.DAO.encuestas;

import java.util.HashMap;
import java.util.List;

import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertAlternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.Usuario;

public interface ConfiguracionGeneracionDAO {
		
	/**
	 * @param tipoAplicacion
	 * @param tipoServicio
	 * @param tipoEncuesta
	 * @param numero
	 * @param nombre
	 * @param duracion
	 * @param observacion
	 * @param codSede
	 * @param usuCrea
	 * @return
	 */
	
	public String obtenertxhCodEncuestadoByEncuestado(String nroEncuesta);
	
	public String insertFormato(String tipoAplicacion,
			String tipoServicio,
			String tipoEncuesta,
			String numero,
			String nombre,
			String duracion,
			String observacion,
			String codSede,
			String usuCrea);
	
	/**
	 * @param idEncuesta
	 * @param tipoAplicacion
	 * @param tipoServicio
	 * @param tipoEncuesta
	 * @param numero
	 * @param nombre
	 * @param duracion
	 * @param observacion
	 * @param codSede
	 * @param usuCrea
	 * @return
	 */
	public String updateFormato(String idEncuesta,
			String tipoAplicacion,
			String tipoServicio,
			String tipoEncuesta,
			String numero,
			String nombre,
			String duracion,
			String observacion,
			String codSede,
			String usuCrea);
		
	public List<FormatoEncuesta> getAllFormato(String codTipoAplicacion, 
			String codTipoServicio, String codTipoEncuesta, String codTipoEstado, String nombreEncuesta,
			String codResponsable, String nroEncuesta);
	
	/**
	 * @param codEncuesta
	 * @param codGrupo
	 * @param codPregunta
	 * @param alternativa
	 * @param descripcion
	 * @param peso
	 * @param usuCrea
	 * @return
	 */
	public String insertAlternativa(String codEncuesta,
			String codGrupo,
			String codPregunta,
			String alternativa,
			String descripcion,			
			String peso,			
			String usuCrea);
	
	/**
	 * @param codEncuesta
	 * @param codGrupo
	 * @param codPregunta
	 * @param codAlternativa
	 * @return
	 */
	public List getAllAlternativa(String codEncuesta,String codGrupo,String codPregunta,String codAlternativa);
	
	/**
	 * @param codEncuesta
	 * @param codAlternativa
	 * @param codGrupo
	 * @param codPregunta
	 * @param alternativa
	 * @param descripcion
	 * @param peso
	 * @param usuCrea
	 * @return
	 */
	public String updateAlternativa(
			String codEncuesta,
			String codAlternativa,
			String codGrupo,
			String codPregunta,
			String alternativa,
			String descripcion,
			String peso,			
			String usuCrea);
	
	/**
	 * @param codEncuesta
	 * @param codAlternativa
	 * @param usuario
	 * @return
	 */
	public String deleteAlternativa(
			String codEncuesta,
			String codAlternativa,
			String usuario		
		);
	
	/**
	 * @param codEncuesta
	 * @param codSeccion
	 * @param nomGrupo
	 * @param peso
	 * @param indAlternativa
	 * @param usuCrea
	 * @return
	 */
	public String insertGruposByEncuesta(
			String codEncuesta,
			String codSeccion,
			String nomGrupo,
			String peso,
			String indAlternativa,//0 no;1:si		
			String usuCrea
		) ;
	
	/**
	 * @param codEncuesta
	 * @param codGrupo
	 * @param usuario
	 * @return
	 */
	public String deleteGruposByEncuesta(
			String codEncuesta,
			String codGrupo,
			String usuario
		) ;
	
	/**
	 * @param codEncuesta
	 * @param codGrupo
	 * @return
	 */
	public List getAllGruposByEncuesta(String codEncuesta,String codGrupo);
	

	/**
	 * @param codEncuesta
	 * @param codGrupo
	 * @param codPregunta
	 * @return
	 */
	public List getAllPreguntasByGrupo(String codEncuesta,String codGrupo,String codPregunta);
	
	/**
	 * @param codEncuesta
	 * @param codGrupo
	 * @param pregunta
	 * @param tipoPregunta
	 * @param indObligatorio
	 * @param indUnica
	 * @param peso
	 * @param indAlternativa
	 * @param usuCrea
	 * @return
	 */
	public String insertPreguntasByGrupo(
			String codEncuesta,
			String codGrupo,
			String pregunta,
			String tipoPregunta,
			String indObligatorio,
			String indUnica,
			String peso,
			String indAlternativa,
			String usuCrea
		);
	
	/**
	 * @param codEncuesta
	 * @param codGrupo
	 * @param codPregunta
	 * @param pregunta
	 * @param tipoPregunta
	 * @param indObligatorio
	 * @param indUnica
	 * @param peso
	 * @param indAlternativa
	 * @param usuCrea
	 * @return
	 */
	public String updatePreguntasByGrupo(
			String codEncuesta,
			String codGrupo,
			String codPregunta,
			String pregunta,
			String tipoPregunta,
			String indObligatorio,
			String indUnica,
			String peso,
			String indAlternativa,
			String usuCrea
		) ;


	/**
	 * @param codEncuesta
	 * @param codPregunta
	 * @param usuario
	 * @return
	 */
	public String deletePreguntasByGrupo(String codEncuesta,
			String codPregunta,
			String usuario) ;

	/**
	 * @param codEncuesta
	 * @return
	 */
	public List getAllFormatoEncuesta(String codEncuesta);

	/**
	 * @param codEncuesta
	 * @param codSeccion
	 * @param codGrupo
	 * @param nomGrupo
	 * @param peso
	 * @param indAlternativa
	 * @param usuCrea
	 * @return
	 */
	public String updateGruposByEncuesta(
			String codEncuesta,
			String codSeccion,
			String codGrupo,
			String nomGrupo,
			String peso,
			String indAlternativa,//0 no;1:si		
			String usuCrea
		);
		
		
	public List<FormatoEncuesta> GetAllEncuestaByFormato(String codFormatoEncuesta);
	
	public String UpdateEncuestaByFormato(String codFormatoEncuesta, String cadCodSeccion, String nroTotalReg, 
			String codUsuario);	
	
	
	public List<TipoTablaDetalle> GetAllConfiguracionPerfil(String codTabla, String codDetalle);
	
	/**
	 * @param codEncuesta
	 * @param codResponsable
	 * @param indicadorManual 
	 * @param usuario
	 * @return
	 */
	public String UpdateConfiguracionPerfil(String codEncuesta, 
			String codResponsable, String indicadorManual, String usuario);	
	
	public List<Usuario> GetAllDatosPersonal(String dscPrimerNombre, String dscSegundoNombre, String dscApellidoPat, 
			String dscApellidoMat, String codTipoPersonal);
	/*
	 * PERFILES
	 */
	public List GetAllAgregarFormatoManual(String codEncuesta);
	
	public String insertAgregarFormatoManual(
			String codEncuesta,
			String cadIdPreguntaCerrada,
			String cadIdRespuestaCerrada,
			String nroIdPreguntaCerrada,
			String cadIdPreguntaAbierta,
			String cadIdRespuestaAbierta,
			String nroIdPreguntaAbierta,
			String usuCrea,
			String indRegistro);
	
	public List getAllEncuestaManual(String codEncuesta);
	
	public String deleteEncuestaManual(String codEncuesta, String codPerfil,String epdec_id,String codUsuario);
	
	public String updateGeneraEncuesta(String codEncuesta,		
			String usuCrea);
	
	public String InsertCopiaFormatoEncuesta(String codEncuesta);
	
	public String DeleteEncuestaFormato(String codEncuesta, String codUsuario);	
	
	public List GetAllEncuestasPublicadas(String codUsuario);
			
	public List getAllVerEncuesta(String codEncuesta);
	
	public String insertRespuestaByEncuestado(String codEncuesta,
			String codPerfil,
			String epdecId,
			String cadIdPreguntaCerrada,
			String cadIdRespuestaCerrada,
			String nroIdPreguntaCerrada,
			String cadIdPreguntaAbierta,
			String cadIdRespuestaAbierta,
			String nroIdPreguntaAbierta,
			String usuCrea		);
	
	//public List getAllDatosCabeceraEncuesta(String codEncuesta, String codProfesor,String codProducto,String codPrograma);
	public List getAllDatosCabeceraEncuesta(String codEncuesta, String codProfesor);
	
	public List getAllEncFormatoBusqueda(String codTipoAplicacion, String codTipoServicio, String codTipoEncuesta, 
    		String codTipoEstado, String nombreEncuesta, String codResponsable, String nroEncuesta,
    		String nomEncuesta);

	public String obtenerCodEstadoEncuesta(String nroEncuesta);
	
	public String marcarRespuestasAbiertas(String nroEncuesta, String codPerfilEnc,String codRespAbierta);
	
}
