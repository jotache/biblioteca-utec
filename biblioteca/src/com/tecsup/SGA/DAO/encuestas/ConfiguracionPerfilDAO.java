package com.tecsup.SGA.DAO.encuestas;


import java.util.List;

public interface ConfiguracionPerfilDAO {		

	public List GetFechaHoraSistema();
	
	public String updateTerminarEncuesta(String idEncuesta,String usuCrea);
	
	public List GetAllPerfilesByEncuesta(String codEncuesta);
	
	public List GetAllProductos(String codProducto, String codProductoNo);
	
	public List GetAllProgramas(String codProducto, String codPrograma, String nomPrograma);
	
	public List GetAllDepartamentos(String codDepartamento, String nomDepartamento);
	
	public List GetAllCursos(String codCurso, String nomCurso, String codProducto, 
			String codPrograma, String codDepartamento, String codCiclo);
	
	public List GetAllCiclos(String codPrograma, String codCiclo);
	
	public String InsertPerfilEncuesta(String codTipoEncuesta,
			String codEncuesta,
			String codPerfil,
			String codProducto,
			String codPrograma,
			String codCiclo,
			String codDepartamento,
			String codCurso,
			String codTipoPersonal,
			String codTipoDocente,
			String añoIniEgresado,
			String añoFinEgresado,
			String usuario);
	
	public List GetAllPerfilEncuesta(String codEncuesta, String codPerfil);
	
	public List GetAllTipoPersonal(String codTipoPersonal, String nomTipoPersonal);
	
	public List GetAllTipoDocente(String codTipoPersonal, String codTipoDocente, String nomTipoDocente);
	
	public List GetAllProfesorPFR(String codEncuesta, String codPerfil);
	
	public List GetAllProfesorPCC(String codEncuesta, String codPerfil);
	
	public List GetAllAmbitoGeneral(String codEncuesta, String codPerfil);
	
	public String AplicarEncuesta(String codEncuesta,
			String fechaInicio,
			String horaInicio,
			String fechaFin,
			String horaFin,
			String usuario);
	
	public List GetAmpliarProgByEncuesta(String codEncuesta);
	
	public String AmpliarProgramacionEncuesta(String codEncuesta,
			String fechaInicio,
			String horaInicio,
			String fechaFin,
			String horaFin,
			String codUsuario);
	
	public String deleteAmbitoProfesor(String codEncuesta, String codPerfil, 
			String codProfesor, String codProducto, String codPrograma,String codCentroCosto, 
			String codCurso, String codCiclo, String codSeccion, String codUsuario);

	public String obtener_correo(String codUsuario);
	
}
