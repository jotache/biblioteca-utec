package com.tecsup.SGA.DAO.encuestas;

import java.util.HashMap;
import java.util.List;

import com.tecsup.SGA.DAO.encuestas.jdbc.configuracionGeneracion.InsertAlternativa;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.Usuario;

public interface ConsultasReportesEncuestasDAO {
	
	public List getAllProfConsolidadaPFR(String codEncuesta,String codProfesor) ;
	
	public List getAllProfConsolidadaPCC(String codEncuesta,
			String codProfesor);
	
	public List getAllEstandarConsEgresado(String codEncuesta) ;
	
	public List getAllEstandarConsAlumno(String codEncuesta);
	
	public List getAllEstandarConsPersonal(String codEncuesta);
	
	public List getAllEstandarConsServicio(String codEncuesta);
	
		public List getAllValidaRptaReporte(String tipoReporte,String codEncuesta,String codResponsable);
	
	public List GetAllConsultaByProfesorDetPRFCer(String codEncuesta, String codProfesor) ;
	
	public List GetAllConsultaByProfesorDetPFRAbi(String codEncuesta, String codProfesor) ;
	
	public List GetAllConsultaByProfesorDetPCCCer(String codEncuesta, String codProfesor) ;
	
	public List GetAllConsultaByProfesorDetPCCAbi(String codEncuesta, String codProfesor) ;
	
	public List GetAllEstandarDetalleAlumnoCer(String codEncuesta) ;
	
	public List GetAllEstandarDetalleAlumnoAbi(String codEncuesta) ;
	
	public List GetAllEstandarDetalleEgresadoCer(String codEncuesta);
	
	public List GetAllEstandarDetalleEgresadoAbi(String codEncuesta);
	
	public List GetAllEstandarDetallePersonalCer(String codEncuesta);
	
	public List GetAllEstandarDetallePersonalAbi(String codEncuesta);
	
	public List GetAllEstandarDetalleServicioCer(String codEncuesta);
	
	public List GetAllEstandarDetalleServicioAbi(String codEncuesta);
	
	public List GetAllMixtaDetalleCerrada(String codEncuesta);
	
	public List GetAllMixtaDetalleAbierta(String codEncuesta);
}
