package com.tecsup.SGA.DAO.encuestas;

import java.util.List;

import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.EncuestaEmpresa;
import com.tecsup.SGA.modelo.Usuario;

public interface ActualizacionDatosDAO {

	public List<EncuestaAlumno> GetAllEncuestaAlumno(String codAlumno);
	
	public String UpdateEncuestaAlumno(String codAlumno, EncuestaAlumno encuestaAlumno,String codUsuario);
	
	public List<EncuestaEmpresa> GetAllEncuestaEmpresa(String nombreEmpresa);
	
	public List<EncuestaAlumno> GetVerificarAlumno(String codAlumno);
	
	public List<Usuario> GetVerificarTieneEncuesta(String codUsuario);
}
