package com.tecsup.SGA.DAO.logistica;

import java.util.List;

import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.NotaSeguimiento;
import com.tecsup.SGA.modelo.SolRequerimiento;

public interface AtencionRequerimientosDAO {
//ALQD,23/01/09. NUEVO PARAMETRO DE FILTRO
	public List<SolRequerimiento> GetAllBandejaAtencionRequerimientos(String codSede, String codTipoReq, 
			String codSubTipoReq, String nroReq, String fecInicio, String fecFin,
			String codCeco,String nombreUsuarioSol, String codEstado, String nroGuia,
			String indSalPorConfirmar);
	
	public List<SolRequerimiento> GetAllBandejaAtencionReqMan(String codSede, String codTipoRequerimiento, String codSubTipoRequerimiento, 
			String nroRequerimiento, String centroCosto, String usuSolicitante, String codUsuario, String codPerfil,
			String codEstado);
	
	public String UpdateCerrarRequerimiento(String codRequerimiento, String codUsuario);
	
	public List<Guia> GetAllGuiasAsociadas(String codRequerimiento);
	
	public List<Guia> GetGuiaRequerimiento(String codGuia);
	
	public List<GuiaDetalle> GetAllDetalleGuiaRequerimiento(String codGuia);
	
	public String InsertGuiaRequerimiento(String codRequerimiento, String codUsuario);
	
	public String DeleteGuiaRequerimiento(String codGuia, String codUsuario);
	
	public String InsertDetalleGuiaRequerimiento(String codGuia, String codRequerimiento, 
			String fechaGuia, String observacion, String cadenaCodDetalle, String cadenaCantidadSolicitada, 
			String cadenaPrecios, String cadenaCantidadEntregada, String cantidad, String codUsuario);
	
	public List<NotaSeguimiento> GetAllNotasSeguimientoRequerimiento(String codRequerimiento,String codNota);
	
	public String InsertNotaSeguimiento(String codReq, String desNota, String codPerfil,String usuario);

	public String UpdateNotaSeguimiento(String codReq, String codNota,String desNota,String usuario);
	
	public String DeleteNotaSeguimiento(String codRequerimiento, String codNota, String codUsuario);
	
	public List<GuiaDetalle> GetGuiaMantenimiento(String codRequerimiento);
	
	public String InsertGuiaMantenimiento(String codRequerimiento, String codReqDetalle, 
			String codGuia, String codGuiaDetalle, String nroGuia, 
			String fechaGuia, String observacion, String codUsuario);
	
	public String RegistrarConformidad(String codGuia, String codUsuario);
	
	public String RegistrarConformidadMantenimiento(String codRequerimiento, String codUsuario);
	
	public List<SolRequerimiento> GetAllSolicitudesReqRel(String codRequerimiento);

	public String InsertRespByActivo(String codGuia, String codBien,
			String cadCodBienDet, String cadCodUsuResp,
			String cadCodDescUbicacion, String cantidad, String codUsuario);

	public String deleteRespByActivo(String codGuia, String codBien,
			String codBienDet, String usuario);
	
	public List GetAllNumerosSeries(String codReq,String codReqDet,String codDev,String codDevDet);
	
	public String InsertNumerosSeries(String codDev, String codDevDet,
			String codBien, String cadCodIng,String cantidad,
			 String codUsuario);
}
