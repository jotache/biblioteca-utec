package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DetalleOrden;

public class GetSolicitantesProductosOC extends StoredProcedure {

	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
						+ ".pkg_log_orden.SP_SEL_PROD_X_SOLICITANTE";
	
	private static final String E_C_CODORDEN = "E_C_CODORDEN";
	private static final String E_C_BIEN_ID = "E_C_BIEN_ID";
	private static final String S_R_PRODUCTO = "S_R_PRODUCTO";
	private static final String S_R_SOLICITANTE = "S_R_SOLICITANTE";
	
	public GetSolicitantesProductosOC(DataSource dataSource){
		super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODORDEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_BIEN_ID, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_R_PRODUCTO, OracleTypes.CURSOR, new ProductosMapper()));
        declareParameter(new SqlOutParameter(S_R_SOLICITANTE, OracleTypes.CURSOR, new Solicitantes()));
	}
	
	
    public Map execute(String codOrden, String codProd) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODORDEN, codOrden);
    	inputs.put(E_C_BIEN_ID, codProd);
    	
        return super.execute(inputs);
    }

	
	final class ProductosMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			DetalleOrden det = new DetalleOrden();
			det.setCodBien(rs.getString("BIEN_ID") == null ? "": rs.getString("BIEN_ID"));
			det.setDescripcion(rs.getString("NOMPRODUCTO") == null ? "": rs.getString("NOMPRODUCTO"));
			det.setDscUnidad(rs.getString("UNIDAD") == null ? "": rs.getString("UNIDAD"));
			det.setCantidad(rs.getString("CANSOLICITADA") == null ? "": rs.getString("CANSOLICITADA"));
			return det;
		}
	}
	
	final class Solicitantes implements RowMapper{
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			DetalleOrden det = new DetalleOrden();
			//det.setCodBien(rs.getString("BIEN_ID") == null ? "": rs.getString("BIEN_ID"));
			det.setDescripcion(rs.getString("NOMPRODUCTO") == null ? "": rs.getString("NOMPRODUCTO"));
			det.setNomCencos(rs.getString("NOMCECO") == null ? "": rs.getString("NOMCECO"));
			det.setUsuSolicitante(rs.getString("USU_SOLICITANTE") == null ? "": rs.getString("USU_SOLICITANTE"));
			det.setCantidad(rs.getString("CANTIDAD_SOLICITADA") == null ? "": rs.getString("CANTIDAD_SOLICITADA"));
			return det;
		}	
	}
}
