package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_ACT_APRUEBA_SOLREQ(
	E_C_CODREQ IN CHAR,
	E_C_CODUSUARIO IN CHAR,
	E_C_IND_ACCION IN CHAR, -- 1-> Aprobación; 2-> Envio a Resp. CECO.
	E_V_CAD_CANTIDADDES IN VARCHAR2,
	S_V_RETVAL IN OUT VARCHAR2);
 */
//ALQD,20/01/10.NUEVOS PARAMETROS
public class AprobarSolRequerimientoUsuario extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_sol_requerimientos.sp_act_aprueba_solreq";
		
		private static final String E_C_CODREQ = "E_C_CODREQ";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
		private static final String E_C_IND_ACCION = "E_C_IND_ACCION";
		private static final String E_V_CAD_CANTIDADDES = "E_V_CAD_CANTIDADDES";		
		private static final String E_V_PERFIL = "E_V_PERFIL";
		private static final String E_V_CODCENCOSTO = "E_V_CODCENCOSTO";
		private static final String E_C_IND_INVERSION = "E_C_IND_INVERSION";
		private static final String E_C_IND_PARASTOCK = "E_C_IND_PARASTOCK"; 
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public AprobarSolRequerimientoUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_IND_ACCION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_CAD_CANTIDADDES, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_PERFIL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODCENCOSTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_IND_INVERSION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_IND_PARASTOCK, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	//ALQD,20/01/10.GUARDANDO TRES CAMPOS MAS: CCOSTO, INDINVERSION E INDPARASTOCK
	public Map execute(String codRequerimiento,String codUsuario,String indicadorAccion
		,String cadenaCantidad,String codPerfil,String codCenCosto,String indInversion
		,String indParaStock) {
		
		Map inputs = new HashMap();		
		
		inputs.put(E_C_CODREQ, codRequerimiento);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		inputs.put(E_C_IND_ACCION, indicadorAccion);
		inputs.put(E_V_CAD_CANTIDADDES, cadenaCantidad);
		inputs.put(E_V_PERFIL, codPerfil);
		inputs.put(E_V_CODCENCOSTO, codCenCosto);
		inputs.put(E_C_IND_INVERSION, indInversion);
		inputs.put(E_C_IND_PARASTOCK, indParaStock);
		
		return super.execute(inputs);		
	}	
}
