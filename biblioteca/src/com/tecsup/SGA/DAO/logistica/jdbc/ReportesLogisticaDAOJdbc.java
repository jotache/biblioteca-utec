package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.logistica.ReportesLogisticaDAO;

import com.tecsup.SGA.DAO.logistica.jdbc.Reportes.*;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class ReportesLogisticaDAOJdbc extends JdbcDaoSupport implements ReportesLogisticaDAO{

	public List<ReportesLogistica> GetAllIngresoAlmacen(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia){
		GetAllIngresoAlmacen getAllIngresoAlmacen = new GetAllIngresoAlmacen(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllIngresoAlmacen.execute(codSede, codTipoBien, fecInicio, fecFin,
				codTipoFamilia);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<ReportesLogistica> GetAllSalidaAlmacen(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codCentroCosto, String codTipoGasto){
		GetAllSalidaAlmacen getAllSalidaAlmacen = new GetAllSalidaAlmacen(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSalidaAlmacen.execute(codSede, codTipoBien, fecInicio, fecFin,
				codCentroCosto, codTipoGasto);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<ReportesLogistica> GetAllKardezValorizado(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia, String codBien){
		GetAllKardezValorizado getAllKardezValorizado = new GetAllKardezValorizado(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllKardezValorizado.execute(codSede, codTipoBien, fecInicio, fecFin,
				codTipoFamilia, codBien);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//ALQD,22/09/09.SE AGREGA UN PARAMETRO MAS PARA SALDO > 0
	public List<ReportesLogistica> GetAllMaestrosSaldos(String codSede, String codTipoBien, String periodo
			, String codTipoFamilia, String conSaldo){
		GetAllMaestrosSaldos getAllMaestrosSaldos = new GetAllMaestrosSaldos(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllMaestrosSaldos.execute(codSede, codTipoBien, periodo,
				codTipoFamilia, conSaldo);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<ReportesLogistica> GetAllCierreOperaciones(String codSede, String codTipoReq, 
			String codTipoBien, String codTipoServicio,	String fecInicio, String fecFin,
			String codCentroCosto, String codTipoGasto){
		GetAllCierreOperaciones getAllCierreOperaciones = new GetAllCierreOperaciones(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCierreOperaciones.execute(codSede, codTipoReq, codTipoBien,
				codTipoServicio, fecInicio, fecFin, codCentroCosto, codTipoGasto);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<ReportesLogistica> GetAllAcumuladoSalidaByArticulo(String codSede, String codTipoBien,
			String fecInicio, String fecFin, String codTipoFamilia, String codBien,
			String codCentroCosto, String codTipoGasto){
		GetAllAcumuladoSalidaByArticulo getAllAcumuladoSalidaByArticulo = new GetAllAcumuladoSalidaByArticulo(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllAcumuladoSalidaByArticulo.execute(codSede, codTipoBien, fecInicio,
				fecFin, codTipoFamilia, codBien, codCentroCosto, codTipoGasto);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<ReportesLogistica> GetAllSolicitudCotizaciones(String codSede, String codTipoReq, String codTipoBien, String codTipoServicio,
			String fecInicio, String fecFin, String codProveedor){
		GetAllSolicitudCotizaciones getAllSolicitudCotizaciones = new GetAllSolicitudCotizaciones(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSolicitudCotizaciones.execute(codSede, codTipoReq, codTipoBien,
				codTipoServicio, fecInicio, fecFin, codProveedor);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	public List<ReportesLogistica> GetAllRequerimientoValorizado(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,
			String fecInicio , String fecFin, String usuSolicitado, String codEstadoReq){
		GetAllRequerimientoValorizado getAllRequerimientoValorizado = new GetAllRequerimientoValorizado(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllRequerimientoValorizado.execute(codSede, codTipoReq, codTipoBien,
				codTipoServicio, fecInicio, fecFin, usuSolicitado, codEstadoReq);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	public List<ReportesLogistica> GetAllOrdenesAsignadasByProveedor(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,
			String fecInicio , String fecFin, String indiceLocalidad, String codProveedor
			, String codTipoMoneda, String codTipoPago){
		GetAllOrdenesAsignadasByProveedor getAllOrdenesAsignadasByProveedor = new GetAllOrdenesAsignadasByProveedor(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllOrdenesAsignadasByProveedor.execute(codSede, codTipoReq,
				codTipoBien, codTipoServicio, fecInicio, fecFin, indiceLocalidad, codProveedor,
				codTipoMoneda, codTipoPago);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	public List<ReportesLogistica> GetAllProgramaCronogramaPago(String codSede, String fecInicio,
			String fecFin, String indiceValidado,
			String codTipoOrden, String codTipoPago, String codEstadoComp, String dscProveedor){
		GetAllProgramaCronogramaPago getAllProgramaCronogramaPago = new GetAllProgramaCronogramaPago(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProgramaCronogramaPago.execute(codSede, fecInicio, fecFin,
				indiceValidado, codTipoOrden, codTipoPago, codEstadoComp, dscProveedor);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List getAllIngresoAlmacenDev(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia){
		GetAllIngresoAlmacenDev getAllIngresoAlmacenDev = new GetAllIngresoAlmacenDev(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllIngresoAlmacenDev.execute(codSede, codTipoBien, fecInicio, fecFin,
				codTipoFamilia);
		if(!outputs.isEmpty()){
			return (List)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	public List<ReportesLogistica> GetAllOrdenesInversion(String codSede,
			String fecInicio, String fecFin) {
		GetAllOrdenesInversion rpt = new GetAllOrdenesInversion(this.getDataSource());
		
		HashMap outputs = (HashMap)rpt.execute(codSede, fecInicio, fecFin);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	
}
