package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento.GetAllRequerimientosUsuario.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;

public class GetAllAproRequerimientoUsuario extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
			+ ".pkg_log_sol_requerimientos.sp_sel_bndj_req_aprob";
		
	private static Log log = LogFactory.getLog(GetAllAproRequerimientoUsuario.class);
	
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO"; 
	private static final String E_C_CODPERFIL = "E_C_CODPERFIL"; 
	private static final String E_C_TIPO_REQ = "E_C_TIPO_REQ";
	private static final String E_C_NRO_REQ = "E_C_NRO_REQ";
	private static final String E_C_FEC_REQ_INI = "E_C_FEC_REQ_INI";
	private static final String E_C_FEC_REQ_FIN = "E_C_FEC_REQ_FIN";
	private static final String E_C_CECO = "E_C_CECO";
	private static final String E_C_SUBTIPOREQ = "E_C_SUBTIPOREQ";
	private static final String E_C_IND_GRUPO = "E_C_IND_GRUPO";
	//CCORDOVA - SEDE
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllAproRequerimientoUsuario(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        //CCORDOVA - SEDE
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPERFIL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NRO_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_REQ_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_REQ_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CECO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_SUBTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_IND_GRUPO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSUsuario, String codPerfil, String tipoReq
    		, String nroReq, String fechaInicio, String fechaFin
    		, String codCentroCosto, String codEstado, String indGrupo
    		//CCORDOVA - SEDE
    		, String codSede) {
    	
    	log.info("Inicio " + SPROC_NAME);
    	log.info("E_C_CODSEDE: "+codSede);
    	log.info("E_C_CODUSUARIO: "+codSUsuario);
    	log.info("E_C_CODPERFIL: "+codPerfil);
    	log.info("E_C_TIPO_REQ: "+tipoReq);
    	log.info("E_C_NRO_REQ: "+nroReq);
    	log.info("E_C_FEC_REQ_INI: "+fechaInicio);
    	log.info("E_C_FEC_REQ_FIN: "+fechaFin);
    	log.info("E_C_CECO: "+codCentroCosto);
    	log.info("E_C_SUBTIPOREQ: "+codEstado);
    	log.info("E_C_IND_GRUPO: "+indGrupo);
    	log.info("Fin " + SPROC_NAME);
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_C_CODUSUARIO, codSUsuario);
    	inputs.put(E_C_CODPERFIL, codPerfil);
    	inputs.put(E_C_TIPO_REQ, tipoReq);
    	inputs.put(E_C_NRO_REQ, nroReq);
    	inputs.put(E_C_FEC_REQ_INI, fechaInicio);
    	inputs.put(E_C_FEC_REQ_FIN, fechaFin);
    	inputs.put(E_C_CECO, codCentroCosto);
    	inputs.put(E_C_SUBTIPOREQ, codEstado);
    	inputs.put(E_C_IND_GRUPO, indGrupo);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SolRequerimiento solRequerimiento = new SolRequerimiento();
        	
        	solRequerimiento.setIdRequerimiento(rs.getString("CODIGO") == null ? "": rs.getString("CODIGO"));
        	
        	solRequerimiento.setNroRequerimiento(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
        	solRequerimiento.setFechaEmision(rs.getString("FECHA_REQ") == null ? "": rs.getString("FECHA_REQ"));
        	solRequerimiento.setCecoSolicitante(rs.getString("COD_CECO") == null ? "": rs.getString("COD_CECO"));
        	
        	solRequerimiento.setDscCecoSolicitante(rs.getString("DSC_CECO") == null ? "": rs.getString("DSC_CECO"));
        	
        	solRequerimiento.setSubTipoRequerimiento(rs.getString("COD_SUBTIPOREQ") == null ? "": rs.getString("COD_SUBTIPOREQ"));
        	solRequerimiento.setDscSubTipoRequerimiento(rs.getString("DSC_SUBTIPOREQ") == null ? "": rs.getString("DSC_SUBTIPOREQ"));
        	solRequerimiento.setUsuSolicitante(rs.getString("USU_SOLICITANTE") == null ? "": rs.getString("USU_SOLICITANTE"));
        	solRequerimiento.setUsuAsignado(rs.getString("USU_ASIGNADO") == null ? "": rs.getString("USU_ASIGNADO"));
        	solRequerimiento.setTotalRequerimiento(rs.getString("PRECIO_REF") == null ? "": rs.getString("PRECIO_REF"));
        	
        	solRequerimiento.setDscTipoServicio(rs.getString("NOMBRE") == null ? "": rs.getString("NOMBRE"));
        	solRequerimiento.setDscDetalleEstado(rs.getString("DETALLE_ESTADO") == null ? "": rs.getString("DETALLE_ESTADO"));
        	solRequerimiento.setIndiceEnvio(rs.getString("IND_ENVIO") == null ? "": rs.getString("IND_ENVIO"));
        	//--0=>sI PUEDES ENVIAR; 1 => No puedes enviar.
            return solRequerimiento;
        }
    }
}
