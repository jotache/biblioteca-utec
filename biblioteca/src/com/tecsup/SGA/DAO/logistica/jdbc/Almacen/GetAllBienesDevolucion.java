package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.BienesDevolucion;

public class GetAllBienesDevolucion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_bienes_devolucion";
	
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_FEC_DEV_INI = "E_C_FEC_DEV_INI";
	private static final String E_C_FEC_DEV_FIN = "E_C_FEC_DEV_FIN";
	private static final String E_C_COD_CECO = "E_C_COD_CECO";
	private static final String E_V_DSV_USU_SOL = "E_V_DSV_USU_SOL";
	private static final String E_C_COD_ESTADO = "E_C_COD_ESTADO";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllBienesDevolucion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_DEV_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_DEV_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_CECO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_DSV_USU_SOL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_ESTADO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String fecDevIni, String fecDevFin, String codCeco, 
    		 String dsvUsu, String codEstado) {
    	
    	Map inputs = new HashMap();
    	System.out.println("1.- "+codSede);
    	System.out.println("2.- "+fecDevIni);
    	System.out.println("3.- "+fecDevFin);
    	System.out.println("4.- "+codCeco);
    	System.out.println("5.- "+dsvUsu);
    	System.out.println("6.- "+codEstado);
    	inputs.put(E_C_SEDE, codSede);
    	inputs.put(E_C_FEC_DEV_INI, fecDevIni);
    	inputs.put(E_C_FEC_DEV_FIN, fecDevFin);
    	inputs.put(E_C_COD_CECO, codCeco);
    	inputs.put(E_V_DSV_USU_SOL, dsvUsu);
    	inputs.put(E_C_COD_ESTADO, codEstado);
    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	BienesDevolucion almacen= new BienesDevolucion();
        	
        	almacen.setCodDev(rs.getString("COD_DEV") == null ? "": rs.getString("COD_DEV"));
        	almacen.setCodReq(rs.getString("COD_REQ") == null ? "": rs.getString("COD_REQ"));
        	almacen.setNroReq(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
        	almacen.setDscCeco(rs.getString("DSC_CECO") == null ? "": rs.getString("DSC_CECO"));
        	almacen.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
        	almacen.setCantDevuelta(rs.getString("CANT_DEVUELTA") == null ? "0": rs.getString("CANT_DEVUELTA"));
         	almacen.setFecDevolucion(rs.getString("FEC_DEVOLUCION") == null ? "": rs.getString("FEC_DEVOLUCION"));
        	almacen.setDscEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
         	         	
        	return almacen;
        }
    }
}
