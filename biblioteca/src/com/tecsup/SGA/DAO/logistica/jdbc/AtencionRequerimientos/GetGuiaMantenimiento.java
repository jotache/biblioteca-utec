package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
/*
 * PROCEDURE SP_SEL_GUIA_MANT(
E_C_COD_REQ IN CHAR,
S_C_RECORDSET IN OUT CUR_RECORDSET);
 */
public class GetGuiaMantenimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_guia_mant";
	
	private static final String E_C_COD_REQ = "E_C_COD_REQ";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetGuiaMantenimiento(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_REQ, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codRequerimiento) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_REQ, codRequerimiento);
     	
    	System.out.println(">>GetGuiaMantenimiento<<");
    	System.out.println("codRequerimiento>>"+codRequerimiento);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	GuiaDetalle guia= new GuiaDetalle();
        	/*
        	 *   AS COD_REQ
			     AS COD_REQ_DET     
			     AS COD_GUIA
			     AS COD_GUIA_DET
			     AS NRO_GUIA
			     AS FEC_GUIA
			     AS COD_ESTADO
			     AS DSC_ESTADO
			     AS DSC_GUIA_DET     
			     AS COD_DOC_PAGO
			     AS NRO_DOC_PAGO
			     AS FEC_DOC_PAGO
			     AS COD_ORDEN
			     AS FEC_ORDEN
			     AS NRO_ORDEN
        	 */
        	guia.setCodRequerimiento(rs.getString("COD_REQ") == null ? "": rs.getString("COD_REQ"));
        	guia.setCodReqDetalle(rs.getString("COD_REQ_DET") == null ? "": rs.getString("COD_REQ_DET"));
        	guia.setCodGuia(rs.getString("COD_GUIA") == null ? "": rs.getString("COD_GUIA"));
        	guia.setCodGuiaDetalle(rs.getString("COD_GUIA_DET") == null ? "": rs.getString("COD_GUIA_DET"));
        	guia.setNroGuia(rs.getString("NRO_GUIA") == null ? "": rs.getString("NRO_GUIA"));
        	guia.setFechaGuia(rs.getString("FEC_GUIA") == null ? "": rs.getString("FEC_GUIA"));
        	guia.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	guia.setDscEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
        	guia.setDscGuiaDetalle(rs.getString("DSC_GUIA_DET") == null ? "": rs.getString("DSC_GUIA_DET"));
        	guia.setCodDocPago(rs.getString("COD_DOC_PAGO") == null ? "": rs.getString("COD_DOC_PAGO"));
        	guia.setNroDocPago(rs.getString("NRO_DOC_PAGO") == null ? "": rs.getString("NRO_DOC_PAGO"));
        	guia.setFechaDocPago(rs.getString("FEC_DOC_PAGO") == null ? "": rs.getString("FEC_DOC_PAGO"));
        	guia.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
        	guia.setFechaOrden(rs.getString("FEC_ORDEN") == null ? "": rs.getString("FEC_ORDEN"));
        	guia.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	       	
        	return guia;
        }
    }
}
