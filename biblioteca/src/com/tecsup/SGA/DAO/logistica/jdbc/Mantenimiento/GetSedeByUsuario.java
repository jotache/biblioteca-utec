package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetSedeByUsuario extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_comun.sp_sel_sede_by_usuario";
	
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public GetSedeByUsuario(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codUsuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_USUARIO, codUsuario);
	
	return super.execute(inputs);
	
	}

}
