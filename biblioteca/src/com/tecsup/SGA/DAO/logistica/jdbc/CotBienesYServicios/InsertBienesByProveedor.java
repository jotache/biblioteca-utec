package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PPROCEDURE SP_INS_BIENES_BY_PROVEEDOR(
	E_C_CODCOTI IN CHAR,
	E_C_CODPROV IN CHAR,
	E_C_CADCODCOTIDET IN VARCHAR2,
	E_C_CADPRECIOS IN VARCHAR2,
	E_C_CADDESCRIP IN VARCHAR2,
	E_C_CANTIDAD IN CHAR,
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */
//ALQD,02/06/09.NUEVO PARAMETRO OTROCOSTOS
public class InsertBienesByProveedor extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_cotizacion.sp_ins_bienes_by_proveedor";
		
		private static final String E_C_CODCOTI = "E_C_CODCOTI";
		private static final String E_C_CODPROV = "E_C_CODPROV";
		private static final String E_C_CADCODCOTIDET = "E_C_CADCODCOTIDET";
		private static final String E_C_CADPRECIOS = "E_C_CADPRECIOS";
		private static final String E_C_CADDESCRIP = "E_C_CADDESCRIP";
		private static final String E_C_CANTIDAD = "E_C_CANTIDAD";
		private static final String E_C_MONEDA = "E_C_MONEDA";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
		private static final String E_C_OTROCOSTOS = "E_C_OTROCOSTOS";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public InsertBienesByProveedor(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODPROV, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CADCODCOTIDET, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CADPRECIOS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CADDESCRIP, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CANTIDAD, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_MONEDA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_OTROCOSTOS, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codCotizacion, String codProveedor, String cadenaCodCotDetalle
			, String cadenaPrecios, String cadenaDescripcion, String cantidad
			, String codUsuario, String codMoneda, String otroCostos) {
		
		Map inputs = new HashMap();
		System.out.println("InsertBienesByProveedor");
		System.out.println("1.-"+codCotizacion+"<<");
		System.out.println("2.-"+codProveedor+"<<");
		System.out.println("3.-"+cadenaCodCotDetalle+"<<");
		System.out.println("4.-"+cadenaPrecios+"<<");
		System.out.println("5.-"+cadenaDescripcion+"<<");
		System.out.println("6.-"+cantidad+"<<");
		System.out.println("8.-"+codMoneda+"<<");
		System.out.println("9.-"+codUsuario+"<<");
		System.out.println("10.-"+otroCostos+"<<");
		
		inputs.put(E_C_CODCOTI, codCotizacion);
		inputs.put(E_C_CODPROV, codProveedor);
		inputs.put(E_C_CADCODCOTIDET, cadenaCodCotDetalle);
		inputs.put(E_C_CADPRECIOS, cadenaPrecios);
		inputs.put(E_C_CADDESCRIP, cadenaDescripcion);		
		inputs.put(E_C_CANTIDAD, cantidad);
		inputs.put(E_C_MONEDA, codMoneda);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		inputs.put(E_C_OTROCOSTOS, otroCostos);
		
		return super.execute(inputs);
		
	}	
}
