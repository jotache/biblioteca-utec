package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActConfirmarDev extends StoredProcedure {
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_almacen.sp_act_confirmar_dev";
	
	private static final String E_C_COD_REQ_DEV = "E_C_COD_REQ_DEV";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String E_V_OBSERVACION = "E_V_OBSERVACION";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActConfirmarDev(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_COD_REQ_DEV, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_OBSERVACION, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codReqDev, String codUsuario,String observacion) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_REQ_DEV, codReqDev);
	inputs.put(E_C_COD_USUARIO, codUsuario);
	inputs.put(E_V_OBSERVACION, observacion);
	return super.execute(inputs);
	
	}
}
