package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateProveedorByItem extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_cotizacion.sp_act_prov_x_item";
	
	/*E_C_CODCOTI IN CHAR,
	E_C_CODDETCOTI IN CHAR,
	E_C_CADPROVEEDORES IN CHAR, --CADENA DE C�DIGO DE PROVEEDORES EN FORMATO 1|2|3
	E_C_CADINDICADORES IN CHAR, --CADENA DE INDICADORES EN FORMATO 0|1|0
	E_C_CANTIDAD IN CHAR,
	E_C_CODUSUARIO IN CHAR*/
	
	private static final String E_C_CODCOTI = "E_C_CODCOTI";
	private static final String E_C_CODDETCOTI = "E_C_CODDETCOTI";
	private static final String E_C_CADPROVEEDORES = "E_C_CADPROVEEDORES";
	private static final String E_C_CADINDICADORES = "E_C_CADINDICADORES";
	private static final String E_C_CANTIDAD = "E_C_CANTIDAD";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateProveedorByItem(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODDETCOTI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CADPROVEEDORES, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CADINDICADORES, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CANTIDAD, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codCotizacion, String codCotizacionDetalle, String cadProveedores,
			String cadIndicadores, String cantidad, String codUsuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODCOTI, codCotizacion);
	inputs.put(E_C_CODDETCOTI, codCotizacionDetalle);
	inputs.put(E_C_CADPROVEEDORES, cadProveedores);
	inputs.put(E_C_CADINDICADORES, cadIndicadores);
	inputs.put(E_C_CANTIDAD, cantidad);
	inputs.put(E_C_CODUSUARIO, codUsuario);
		
	return super.execute(inputs);
	
	}
}
