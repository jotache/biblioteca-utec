package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_INS_COND_X_COTI_X_PROV(
	E_C_CODCOTIZACION IN CHAR,
	E_C_CODPROVEEDOR IN CHAR,
	E_C_CODDETALLE IN CHAR,
	E_V_DSCDETALLE IN VARCHAR2,
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */

public class InsertCondicionesByProveedor extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_cotizacion.sp_ins_cond_x_coti_x_prov";
		
		private static final String E_C_CODCOTIZACION = "E_C_CODCOTIZACION";
		private static final String E_C_CODPROVEEDOR = "E_C_CODPROVEEDOR";
		private static final String E_C_CODDETALLE = "E_C_CODDETALLE";
		private static final String E_V_DSCDETALLE = "E_V_DSCDETALLE";		
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";		
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public InsertCondicionesByProveedor(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODCOTIZACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODPROVEEDOR, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODDETALLE, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_DSCDETALLE, OracleTypes.VARCHAR));		
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codCotizacion, String codProveedor, String codDetalle, 
			String dscDetalle, String codUsuario) {
		
		Map inputs = new HashMap();
		System.out.println("InsertBienesByProveedor");
		System.out.println("1.-"+codCotizacion+"<<");
		System.out.println("2.-"+codProveedor+"<<");
		System.out.println("3.-"+codDetalle+"<<");
		System.out.println("4.-"+dscDetalle+"<<");
		System.out.println("5.-"+codUsuario+"<<");		
		
		inputs.put(E_C_CODCOTIZACION, codCotizacion);
		inputs.put(E_C_CODPROVEEDOR, codProveedor);
		inputs.put(E_C_CODDETALLE, codDetalle);
		inputs.put(E_V_DSCDETALLE, dscDetalle);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		
		return super.execute(inputs);
	}	
}
