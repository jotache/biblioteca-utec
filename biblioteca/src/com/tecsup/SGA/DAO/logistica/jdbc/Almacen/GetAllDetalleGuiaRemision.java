package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.GuiaRemision;

public class GetAllDetalleGuiaRemision extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_detalle_guia_rem";
	
		/*E_C_COD_DOC_PAG IN CHAR*/
	
	private static final String E_C_COD_DOC_PAG = "E_C_COD_DOC_PAG";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetalleGuiaRemision(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_DOC_PAG, OracleTypes.CHAR));
       
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codDocPago) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_DOC_PAG, codDocPago);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	GuiaRemision guiaRemision= new GuiaRemision();
        	float producto=0;
        	float precioUni=0;
        	float cantidad=0;
        	guiaRemision.setCodDocPago(rs.getString("COD_DOC_PAG") == null ? "": rs.getString("COD_DOC_PAG").trim());
        	guiaRemision.setCodDocPagoDet(rs.getString("COD_DOC_PAG_DET") == null ? "": rs.getString("COD_DOC_PAG_DET").trim());
        	guiaRemision.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN").trim());
        	guiaRemision.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN").trim());
        	guiaRemision.setCanSolicitada(rs.getString("CAN_SOLICITADA") == null ? "": rs.getString("CAN_SOLICITADA").trim());
        	guiaRemision.setDscPrecioUnitario(rs.getString("PRECIO_UNITARIO") == null ? "0": rs.getString("PRECIO_UNITARIO").trim());
         	guiaRemision.setCanFacturada(rs.getString("CANT_FACTURADA") == null ? "": rs.getString("CANT_FACTURADA").trim());
         	guiaRemision.setCanEntregada(rs.getString("CANT_ENTREGADA") == null ? "": rs.getString("CANT_ENTREGADA").trim());         	
         	precioUni=Float.valueOf(guiaRemision.getDscPrecioUnitario().trim());
         	cantidad=Float.valueOf(guiaRemision.getCanEntregada().trim());
         	producto=Math.round(precioUni*cantidad*1000f)/1000f;
         	guiaRemision.setDscSubTotal(String.valueOf(producto));
         	guiaRemision.setDscObservacion(rs.getString("OBSERVACION") == null ? "": rs.getString("OBSERVACION").trim());
         	guiaRemision.setIndActivo(rs.getString("IND_ACTIVO") == null ? "": rs.getString("IND_ACTIVO").trim());
         	guiaRemision.setPrecioCompra(rs.getString("PRECIO_COMPRA") == null ? "": rs.getString("PRECIO_COMPRA").trim());
         	//jhpr 2008-09-25
         	guiaRemision.setUnidadMedida(rs.getString("UNIDAD") == null ? "": rs.getString("UNIDAD").trim());
         	
         	return guiaRemision;
        }
    }
}
