package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertTablaDetalle extends StoredProcedure{

			private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_mantto_config.sp_ins_tabla_detalle_log";
		private static final String E_C_TTDE_ID = "E_C_TTDE_ID";
		private static final String E_C_TIPT_HIJO_ID = "E_C_TIPT_HIJO_ID";
		private static final String E_V_TTDE_DESCRIPCION = "E_V_TTDE_DESCRIPCION";
		private static final String E_V_TTDE_VALOR3 = "E_V_TTDE_VALOR3";
		private static final String E_V_TTDE_VALOR4 = "E_V_TTDE_VALOR4";
		private static final String E_V_TTDE_VALOR5 = "E_V_TTDE_VALOR5";
		private static final String E_V_TTDE_USU_CREA = "E_V_TTDE_USU_CREA";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
		public InsertTablaDetalle(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_TTDE_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_TIPT_HIJO_ID, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_TTDE_DESCRIPCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_VALOR3, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_VALOR4, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_VALOR5, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TTDE_USU_CREA, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
		
		public Map execute(String codTipoId, String codHijo, String descripcion
		,String valor3, String valor4,String valor5,String usuario) {
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_TTDE_ID, codTipoId);
		inputs.put(E_C_TIPT_HIJO_ID, codHijo);
		inputs.put(E_V_TTDE_DESCRIPCION, descripcion);
		inputs.put(E_V_TTDE_VALOR3, valor3);
		inputs.put(E_V_TTDE_VALOR4, valor4);
		inputs.put(E_V_TTDE_VALOR5, valor5);
		inputs.put(E_V_TTDE_USU_CREA, usuario);
		
		return super.execute(inputs);

}
	
}
