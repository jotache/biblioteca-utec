package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertSeriesActivo extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_almacen.sp_ins_series_activo_by_docpag";
	/*
	 * E_C_COD_DOC_PAG IN CHAR,
E_C_COD_BIEN IN CHAR,
E_C_VALOR_COMPRA IN CHAR,
E_C_CAD_COD_ING IN VARCHAR2,
E_C_CAD_NRO_SERIE IN VARCHAR2,
E_C_CANTIDAD IN VARCHAR2,
E_C_COD_USUARIO IN CHAR,
S_V_RETVAL IN OUT VARCHAR2);
	 */
	
	private static final String E_C_COD_DOC_PAG = "E_C_COD_DOC_PAG";
	private static final String E_C_COD_BIEN = "E_C_COD_BIEN";
	private static final String E_C_VALOR_COMPRA = "E_C_VALOR_COMPRA";
	private static final String E_C_CAD_COD_ING = "E_C_CAD_COD_ING";
	private static final String E_C_CAD_NRO_SERIE = "E_C_CAD_NRO_SERIE";
	private static final String E_C_CANTIDAD = "E_C_CANTIDAD";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String E_C_COD_DOC_PAG_DET = "E_C_COD_DOC_PAG_DET";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertSeriesActivo(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_COD_DOC_PAG, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_VALOR_COMPRA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CAD_COD_ING, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CAD_NRO_SERIE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CANTIDAD, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_DOC_PAG_DET, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codDocPago, String codBien,String valorCompra,
			String cadCodIng,String cadNroSerie,String cantidad,String codUsuario,String codDetalle) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_DOC_PAG, codDocPago);
	inputs.put(E_C_COD_BIEN, codBien);
	inputs.put(E_C_VALOR_COMPRA, valorCompra);
	inputs.put(E_C_CAD_COD_ING, cadCodIng);
	inputs.put(E_C_CAD_NRO_SERIE, cadNroSerie);
	inputs.put(E_C_CANTIDAD, cantidad);
	inputs.put(E_C_COD_USUARIO, codUsuario);
	inputs.put(E_C_COD_DOC_PAG_DET, codDetalle);
	return super.execute(inputs);
	
	}
}
