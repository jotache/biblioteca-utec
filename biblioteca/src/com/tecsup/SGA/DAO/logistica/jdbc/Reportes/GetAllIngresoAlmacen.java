package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllIngresoAlmacen extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllIngresoAlmacen.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_ing_almacen";
/* E_C_SEDE IN CHAR,
  E_C_TIPO_BIEN IN CHAR,
  E_C_FEC_INI IN CHAR,
  E_C_FEC_FIN IN CHAR,
  E_C_COD_FAM IN CHAR*/
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN"; 
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_COD_FAM = "E_C_COD_FAM";

	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllIngresoAlmacen(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_FAM, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoBien, String fecInicio
	, String fecFin, String codTipoFamilia) {
	
	log.info("****** INI " + SPROC_NAME + " *****");
	log.info("E_C_SEDE: "+codSede);
	log.info("E_C_TIPO_BIEN: "+codTipoBien);
	log.info("E_C_FEC_INI: "+fecInicio);
	log.info("E_C_FEC_FIN: "+fecFin);
	log.info("E_C_COD_FAM: "+codTipoFamilia);
	log.info("****** Fin " + SPROC_NAME + " *****");
		
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_COD_FAM, codTipoFamilia);
	
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
	
	reportesLogistica.setNroGuia(rs.getString("NRO_GUIA") == null ? "": rs.getString("NRO_GUIA"));
	reportesLogistica.setFechaCierreGuia(rs.getString("FEC_CIERRE_GUIA") == null ? "": rs.getString("FEC_CIERRE_GUIA"));
	reportesLogistica.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
	reportesLogistica.setFechaOrden(rs.getString("FEC_ORDEN") == null ? "": rs.getString("FEC_ORDEN"));
	reportesLogistica.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
	reportesLogistica.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
	reportesLogistica.setDscFamiliaBien(rs.getString("DSC_FAM_BIEN") == null ? "": rs.getString("DSC_FAM_BIEN"));
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	reportesLogistica.setCantidad(rs.getString("CANT") == null ? "": rs.getString("CANT"));
	reportesLogistica.setCostoUnitario(rs.getString("COSTO_UNITARIO") == null ? "": rs.getString("COSTO_UNITARIO"));
	reportesLogistica.setImporte(rs.getString("IMPORTE") == null ? "": rs.getString("IMPORTE"));
	reportesLogistica.setCantPorEntregar(rs.getString("CANT_X_ENTREGAR") == null ? "": rs.getString("CANT_X_ENTREGAR"));
	reportesLogistica.setDscEstadoOrden(rs.getString("DSC_EST_ORDEN") == null ? "": rs.getString("DSC_EST_ORDEN"));
	
	return reportesLogistica;
	}
}

}
