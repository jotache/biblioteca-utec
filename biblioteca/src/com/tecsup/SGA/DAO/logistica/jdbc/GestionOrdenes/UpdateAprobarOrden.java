package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateAprobarOrden extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_orden.sp_act_aprobar_orden";
	//ALQD,19/02/09.AGREGANDO NUEVO PARAMETRO
	private static final String E_C_CODORDEN = "E_C_CODORDEN";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String E_C_INDINVERSION= "E_C_INDINVERSION";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateAprobarOrden(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODORDEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_INDINVERSION, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codOrden, String codUsuario, String indInversion) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODORDEN, codOrden);
	inputs.put(E_C_CODUSUARIO, codUsuario);
	inputs.put(E_C_INDINVERSION, indInversion);
			
	return super.execute(inputs);
	
	}
}
