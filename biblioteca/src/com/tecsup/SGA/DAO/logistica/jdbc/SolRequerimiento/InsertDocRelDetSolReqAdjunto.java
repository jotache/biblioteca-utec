package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertDocRelDetSolReqAdjunto extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_sol_requerimientos.sp_ins_doc_rel_det_sol_req";
	/*E_C_CODREQ IN CHAR,
	E_C_CODDETSOLREQ IN CHAR,
	E_C_CODDOCADJ IN CHAR,
	E_V_RUTA_DOC IN VARCHAR2,
	E_C_CODTIPOADJ IN CHAR,
	E_C_COD_USUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR)*/

	private static final String E_C_CODREQ = "E_C_CODREQ"; 
	private static final String E_C_CODDETSOLREQ = "E_C_CODDETSOLREQ"; 
	private static final String E_C_CODDOCADJ = "E_C_CODDOCADJ";
	private static final String E_V_RUTA_DOC = "E_V_RUTA_DOC";
	private static final String E_C_CODTIPOADJ = "E_C_CODTIPOADJ";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertDocRelDetSolReqAdjunto(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODDETSOLREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODDOCADJ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_RUTA_DOC, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODTIPOADJ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codReque, String codDetalleReq, String codDocAdjunto 
			, String rutaDoc, String codTipoAdjunto, String codUsuario) {
    	
    	System.out.println("codReque: "+codReque);
    	System.out.println("codDetalleReq: "+codDetalleReq);
    	System.out.println("codDocAdjunto: "+codDocAdjunto);
    	System.out.println("rutaDoc: "+rutaDoc);
    	System.out.println("codTipoAdjunto: "+codTipoAdjunto);
    	System.out.println("codUsuario: "+codUsuario);
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODREQ, codReque);
    	inputs.put(E_C_CODDETSOLREQ, codDetalleReq);
    	inputs.put(E_C_CODDOCADJ, codDocAdjunto);
    	inputs.put(E_V_RUTA_DOC, rutaDoc);
    	inputs.put(E_C_CODTIPOADJ, codTipoAdjunto);
    	inputs.put(E_C_COD_USUARIO, codUsuario);
    	
    	
        return super.execute(inputs);
    }
	
}
