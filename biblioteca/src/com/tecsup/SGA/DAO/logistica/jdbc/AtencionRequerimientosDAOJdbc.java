package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.logistica.AtencionRequerimientosDAO;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.DeleteGuiaRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.DeleteNotaSeguimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.DeleteRespByActivo;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllBandejaAtencionReqMan;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllBandejaAtencionRequerimientos;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllDetalleGuiaRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllGuiasAsociadas;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllNotasSeguimientoRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllSolicitudesReqRel;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetGuiaMantenimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetGuiaRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.InsertDetalleGuiaRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.InsertGuiaMantenimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.InsertGuiaRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.InsertRespByActivo;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.RegistrarConformidad;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.RegistrarConformidadMantenimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.UpdateCerrarRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.DeleteDetalleCotizacionActual;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllDetalleCotizacionActual;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertCondicionesByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.UpdateDocumentosReqACot;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.InsertNotaSeguimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.UpdateNotaSeguimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllNumerosSeries;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.InsertNumerosSeries;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionDetalle;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.NotaSeguimiento;
import com.tecsup.SGA.modelo.SolRequerimiento;

public class AtencionRequerimientosDAOJdbc extends JdbcDaoSupport implements AtencionRequerimientosDAO{
//ALQD,23/01/09. NUEVO PARAMETRO DE FILTRO
	public List<SolRequerimiento> GetAllBandejaAtencionRequerimientos(String codSede, String codTipoReq, String codSubTipoReq,
			String nroReq, String fecInicio, String fecFin, String codCeco,String nombreUsuarioSol, 
			String codEstado, String nroGuia, String indSalPorConfirmar) {
		
		GetAllBandejaAtencionRequerimientos getAllBandejaAtencionRequerimientos = new GetAllBandejaAtencionRequerimientos(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllBandejaAtencionRequerimientos.execute(codSede, codTipoReq, codSubTipoReq,
				nroReq, fecInicio, fecFin, codCeco, nombreUsuarioSol, codEstado, nroGuia,
				indSalPorConfirmar);
		if (!outputs.isEmpty())
		{
			return (List<SolRequerimiento>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	public List<SolRequerimiento> GetAllBandejaAtencionReqMan(String codSede, String codTipoRequerimiento, String codSubTipoRequerimiento, 
			String nroRequerimiento, String centroCosto, String usuSolicitante, String codUsuario, String codPerfil,
			String codEstado){
		
		GetAllBandejaAtencionReqMan getAllBandejaAtencionReqMan = new GetAllBandejaAtencionReqMan(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllBandejaAtencionReqMan.execute(codSede, codTipoRequerimiento, codSubTipoRequerimiento, 
				nroRequerimiento, centroCosto, usuSolicitante, codUsuario, codPerfil,
				codEstado);

		if (!outputs.isEmpty())
		{
			return (List<SolRequerimiento>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	//
	public String UpdateCerrarRequerimiento(String codRequerimiento, String codUsuario){
		
		UpdateCerrarRequerimiento updateCerrarRequerimiento = new UpdateCerrarRequerimiento(this.getDataSource());
		
		HashMap inputs = (HashMap)updateCerrarRequerimiento.execute(codRequerimiento, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	public List<Guia> GetAllGuiasAsociadas(String codRequerimiento) {
		
		GetAllGuiasAsociadas getAllGuiasAsociadas = new GetAllGuiasAsociadas(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllGuiasAsociadas.execute(codRequerimiento);		
		
		if(!outputs.isEmpty()){
			return (List<Guia>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	public List<Guia> GetGuiaRequerimiento(String codGuia) {
		
		GetGuiaRequerimiento getGuiaRequerimiento = new GetGuiaRequerimiento(this.getDataSource());				
		
		HashMap outputs = (HashMap)getGuiaRequerimiento.execute(codGuia);				
		
		if(!outputs.isEmpty()){
			return (List<Guia>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	public List<GuiaDetalle> GetAllDetalleGuiaRequerimiento(String codGuia) {
		
		GetAllDetalleGuiaRequerimiento getAllDetalleGuiaRequerimiento = new GetAllDetalleGuiaRequerimiento(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllDetalleGuiaRequerimiento.execute(codGuia);				
		
		if(!outputs.isEmpty()){
			return (List<GuiaDetalle>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	public String InsertGuiaRequerimiento(String codRequerimiento, String codUsuario) {
		
		InsertGuiaRequerimiento insertGuiaRequerimiento = new InsertGuiaRequerimiento(this.getDataSource());
		
		HashMap inputs = (HashMap)insertGuiaRequerimiento.execute(codRequerimiento, codUsuario);						
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public String DeleteGuiaRequerimiento(String codGuia, String codUsuario) {
		DeleteGuiaRequerimiento deleteGuiaRequerimiento = new DeleteGuiaRequerimiento(this.getDataSource());				
		
		HashMap inputs = (HashMap)deleteGuiaRequerimiento.execute(codGuia, codUsuario);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	public String InsertDetalleGuiaRequerimiento(String codGuia, String codRequerimiento, 
			String fechaGuia, String observacion, String cadenaCodDetalle, String cadenaCantidadSolicitada, 
			String cadenaPrecios, String cadenaCantidadEntregada, String cantidad, String codUsuario) {
		
		InsertDetalleGuiaRequerimiento insertDetalleGuiaRequerimiento = new InsertDetalleGuiaRequerimiento(this.getDataSource());
		
		HashMap inputs = (HashMap)insertDetalleGuiaRequerimiento.execute(codGuia, codRequerimiento, 
				fechaGuia, observacion, cadenaCodDetalle, cadenaCantidadSolicitada, 
				cadenaPrecios, cadenaCantidadEntregada, cantidad, codUsuario);								
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	
	public List<NotaSeguimiento> GetAllNotasSeguimientoRequerimiento(String codRequerimiento,String codNota) {
		
		GetAllNotasSeguimientoRequerimiento getAllNotasSeguimientoRequerimiento = new GetAllNotasSeguimientoRequerimiento(this.getDataSource());				
		
		HashMap outputs = (HashMap)getAllNotasSeguimientoRequerimiento.execute(codRequerimiento,codNota);				
		
		if(!outputs.isEmpty()){
			return (List<NotaSeguimiento>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	
	public String InsertNotaSeguimiento(String codReq, String desNota, String codPerfil,String usuario){
		InsertNotaSeguimiento InsertNotaSeguimiento = new InsertNotaSeguimiento(this.getDataSource());
		HashMap inputs = (HashMap)InsertNotaSeguimiento.execute(codReq, desNota, codPerfil, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String UpdateNotaSeguimiento(String codReq, String codNota,String desNota,String usuario){
		UpdateNotaSeguimiento UpdateNotaSeguimiento = new UpdateNotaSeguimiento(this.getDataSource());
		HashMap inputs = (HashMap)UpdateNotaSeguimiento.execute(codReq, codNota, desNota, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteNotaSeguimiento(String codRequerimiento, String codNota, String codUsuario) {
		
		DeleteNotaSeguimiento deleteNotaSeguimiento = new DeleteNotaSeguimiento(this.getDataSource());						
		
		HashMap inputs = (HashMap)deleteNotaSeguimiento.execute(codRequerimiento, codNota, codUsuario);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public List<GuiaDetalle> GetGuiaMantenimiento(String codRequerimiento) {
		
		GetGuiaMantenimiento getGuiaMantenimiento = new GetGuiaMantenimiento(this.getDataSource());						
		
		HashMap outputs = (HashMap)getGuiaMantenimiento.execute(codRequerimiento);				
		
		if(!outputs.isEmpty()){
			return (List<GuiaDetalle>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	
	public String InsertGuiaMantenimiento(String codRequerimiento, String codReqDetalle, 
			String codGuia, String codGuiaDetalle, String nroGuia, 
			String fechaGuia, String observacion, String codUsuario) {
		
		InsertGuiaMantenimiento insertGuiaMantenimiento = new InsertGuiaMantenimiento(this.getDataSource());		
		
		HashMap inputs = (HashMap)insertGuiaMantenimiento.execute(codRequerimiento, codReqDetalle, 
				codGuia, codGuiaDetalle, nroGuia, fechaGuia, observacion, codUsuario);						
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	
	public String RegistrarConformidad(String codGuia, String codUsuario) {
		
		RegistrarConformidad registrarConformidad = new RegistrarConformidad(this.getDataSource());		
		
		HashMap inputs = (HashMap)registrarConformidad.execute(codGuia, codUsuario);						
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	
	public String RegistrarConformidadMantenimiento(String codRequerimiento, String codUsuario) {
		
		RegistrarConformidadMantenimiento registrarConformidadMantenimiento = new RegistrarConformidadMantenimiento(this.getDataSource());		
		
		HashMap inputs = (HashMap)registrarConformidadMantenimiento.execute(codRequerimiento, codUsuario);						
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	
	public List<SolRequerimiento> GetAllSolicitudesReqRel(String codRequerimiento) {
		
		GetAllSolicitudesReqRel getAllSolicitudesReqRel = new GetAllSolicitudesReqRel(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllSolicitudesReqRel.execute(codRequerimiento);				
		
		if(!outputs.isEmpty()){
			return (List<SolRequerimiento>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	public String InsertRespByActivo(String codGuia, String codBien,
			String cadCodBienDet, String cadCodUsuResp,
			String cadCodDescUbicacion, String cantidad, String codUsuario) {
		
		InsertRespByActivo insertRespByActivo = new InsertRespByActivo(this.getDataSource());		
		
		HashMap inputs = (HashMap)insertRespByActivo.execute( codGuia,  codBien,
				 cadCodBienDet,  cadCodUsuResp,
				 cadCodDescUbicacion,  cantidad,  codUsuario);						
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public String deleteRespByActivo(String codGuia, String codBien,
			String codBienDet, String usuario) {
		DeleteRespByActivo deleteRespByActivo = new DeleteRespByActivo(this.getDataSource());				
		
		HashMap inputs = (HashMap)deleteRespByActivo.execute( codGuia,  codBien,
				codBienDet,  usuario);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public List GetAllNumerosSeries(String codReq,String codReqDet,String codDev,String codDevDet){
		GetAllNumerosSeries GetAllNumerosSeries = new GetAllNumerosSeries(this.getDataSource());
		HashMap outputs = (HashMap)GetAllNumerosSeries.execute(codReq, codReqDet, codDev, codDevDet);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String InsertNumerosSeries(String codDev, String codDevDet,
			String codBien, String cadCodIng,String cantidad,
			 String codUsuario){
		InsertNumerosSeries InsertNumerosSeries = new InsertNumerosSeries(this.getDataSource());
		HashMap inputs = (HashMap)InsertNumerosSeries.execute(codDev, codDevDet, codBien, cadCodIng, cantidad, codUsuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;
	}
	
}
