package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllKardezValorizado extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllKardezValorizado.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_kardex_valorizado";
	
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN"; 
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_COD_FAM = "E_C_COD_FAM";
	private static final String E_C_COD_BIEN = "E_C_COD_BIEN";

	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllKardezValorizado(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_FAM, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_BIEN, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoBien, String fecInicio
	, String fecFin, String codTipoFamilia, String codBien) {
	
	log.info("****** INI " + SPROC_NAME + " *****");
	log.info("E_C_SEDE: "+codSede);
	log.info("E_C_TIPO_BIEN: "+codTipoBien);
	log.info("E_C_FEC_INI: "+fecInicio);
	log.info("E_C_FEC_FIN: "+fecFin);
	log.info("E_C_COD_FAM: "+codTipoFamilia);
	log.info("E_C_COD_BIEN: "+codBien);
	log.info("****** Fin " + SPROC_NAME + " *****");
		
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_COD_FAM, codTipoFamilia);
	inputs.put(E_C_COD_BIEN, codBien);
	
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
	
	reportesLogistica.setCodFamilia(rs.getString("COD_FAMILIA") == null ? "": rs.getString("COD_FAMILIA"));
	reportesLogistica.setDscFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	
	reportesLogistica.setNroDocRelacionado(rs.getString("NRO_DOC_RELACIONADO") == null ? "": rs.getString("NRO_DOC_RELACIONADO"));
	reportesLogistica.setFechaDocumento(rs.getString("FEC_DOCUMENTO") == null ? "": rs.getString("FEC_DOCUMENTO"));
	reportesLogistica.setCantidadIngreso(rs.getString("CANT_INGRESO") == null ? "": rs.getString("CANT_INGRESO"));
	reportesLogistica.setCantidadSalida(rs.getString("CANT_SALIDA") == null ? "": rs.getString("CANT_SALIDA"));
	reportesLogistica.setSaldo(rs.getString("SALDO") == null ? "": rs.getString("SALDO"));
	
	reportesLogistica.setCostoUnitario(rs.getString("COSTO_UNITARIO") == null ? "": rs.getString("COSTO_UNITARIO"));
	reportesLogistica.setImporteIngreso(rs.getString("INPORTE_INGRESO") == null ? "": rs.getString("INPORTE_INGRESO"));
	reportesLogistica.setImporteSalida(rs.getString("IMPORTE_SALIDA") == null ? "": rs.getString("IMPORTE_SALIDA"));
	reportesLogistica.setImporteSaldo(rs.getString("IMPORTE_SALDO") == null ? "": rs.getString("IMPORTE_SALDO"));
	

	reportesLogistica.setSaldoInicial(rs.getString("SALDO_INICIAL") == null ? "": rs.getString("SALDO_INICIAL"));
	reportesLogistica.setCostoUnitarioInicial(rs.getString("COSTO_UNITARIO_INICIAL") == null ? "": rs.getString("COSTO_UNITARIO_INICIAL"));	
	reportesLogistica.setImporteSaldoInicial(rs.getString("IMPORTE_SALDO_INICIAL") == null ? "": rs.getString("IMPORTE_SALDO_INICIAL"));	
	reportesLogistica.setSaldoFinal(rs.getString("SALDO_FINAL") == null ? "": rs.getString("SALDO_FINAL"));	
	reportesLogistica.setCostoUnitarioFinal(rs.getString("COSTO_UNITARIO_FINAL") == null ? "": rs.getString("COSTO_UNITARIO_FINAL"));	
	reportesLogistica.setImporteSaldoFinal(rs.getString("IMPORTE_SALDO_FINAL") == null ? "": rs.getString("IMPORTE_SALDO_FINAL"));
	reportesLogistica.setNomSolicitante(rs.getString("NOMSOLICITANTE") == null ? "": rs.getString("NOMSOLICITANTE"));
	return reportesLogistica;
	}
}

}
