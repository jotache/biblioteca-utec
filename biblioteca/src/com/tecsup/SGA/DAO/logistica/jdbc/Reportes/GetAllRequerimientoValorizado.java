package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllRequerimientoValorizado extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA
	+ ".pkg_log_reportes.sp_sel_req_valorizado";
/* E_C_SEDE IN CHAR,
  E_C_TIPO_REQ IN CHAR,
  E_C_TIPO_BIEN IN CHAR,
  E_C_TIPO_SERV IN CHAR,
  E_C_FEC_INI IN CHAR,
  E_C_FEC_FIN IN CHAR,
  E_C_DSC_USU_SOL IN CHAR,
  E_C_COD_EST_REQ IN CHAR*/
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_TIPO_REQ = "E_C_TIPO_REQ";
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN";
	private static final String E_C_TIPO_SERV = "E_C_TIPO_SERV";
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_DSC_USU_SOL = "E_C_DSC_USU_SOL";
	private static final String E_C_COD_EST_REQ = "E_C_COD_EST_REQ";

	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllRequerimientoValorizado(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_REQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_SERV, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_DSC_USU_SOL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_EST_REQ, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoReq, String codTipoBien, String codTipoServicio,
			String fecInicio , String fecFin, String usuSolicitado, String codEstadoReq) {
		
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_REQ, codTipoReq);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_TIPO_SERV, codTipoServicio);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_DSC_USU_SOL, usuSolicitado);
	inputs.put(E_C_COD_EST_REQ, codEstadoReq);
	
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	
	ReportesLogistica reportesLogistica = new ReportesLogistica();
	//19
	reportesLogistica.setDscTipoReq(rs.getString("DSC_TIPO_REQ") == null ? "": rs.getString("DSC_TIPO_REQ"));
	reportesLogistica.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
	reportesLogistica.setNroRequerimiento(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
	reportesLogistica.setFecEnvioReq(rs.getString("FEC_ENV_REQ") == null ? "": rs.getString("FEC_ENV_REQ"));
	reportesLogistica.setDscUsuarioSol(rs.getString("DSC_USU_SOL") == null ? "": rs.getString("DSC_USU_SOL"));
	reportesLogistica.setCodCeco(rs.getString("COD_CECO") == null ? "": rs.getString("COD_CECO"));
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBien(rs.getString("DSC_BIEN_SERV") == null ? "": rs.getString("DSC_BIEN_SERV"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	reportesLogistica.setCantRequerimiento(rs.getString("CANT_REQ") == null ? "": rs.getString("CANT_REQ"));
	reportesLogistica.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
	reportesLogistica.setPrecioUnitario(rs.getString("PRECIO_UNITARIO") == null ? "": rs.getString("PRECIO_UNITARIO"));
	reportesLogistica.setImporte(rs.getString("IMPORTE") == null ? "": rs.getString("IMPORTE"));
	reportesLogistica.setCodTipoGasto(rs.getString("COD_TIPO_GASTO") == null ? "": rs.getString("COD_TIPO_GASTO"));
	reportesLogistica.setDscTipoGasto(rs.getString("DSCD_TIPO_GASTO") == null ? "": rs.getString("DSCD_TIPO_GASTO"));
	reportesLogistica.setDscImporteProrrogaReq(rs.getString("IMP_PRORR_REQ") == null ? "": rs.getString("IMP_PRORR_REQ"));
	reportesLogistica.setDscEstadoReq(rs.getString("DSC_EST_REQ") == null ? "": rs.getString("DSC_EST_REQ"));
	reportesLogistica.setFechaAprobaReq(rs.getString("FEC_APROBACION") == null ? "": rs.getString("FEC_APROBACION"));
	reportesLogistica.setFecAtencion(rs.getString("FEC_ATENCION") == null ? "": rs.getString("FEC_ATENCION"));
	//ALQD,22/09/09.AUMENTANDO DOS COLUMNAS MAS: CANT ATENDIDA Y PU_PROM ATENCION
	reportesLogistica.setCantConsumida(rs.getString("CANT_ATENDIDA") == null ? "": rs.getString("CANT_ATENDIDA"));
	reportesLogistica.setPrecioPromedio(rs.getString("PU_ATENCION") == null ? "": rs.getString("PU_ATENCION"));
	//ALQD,11/02/10.AUMENTANDO COLUMNAS: COMPRADOR Y NUMCOTIZACION O NUM_OC
	reportesLogistica.setComprador(rs.getString("NOMCOMPRADOR") == null ? "": rs.getString("NOMCOMPRADOR"));
	if(rs.getString("NUM_OC") == null){
		reportesLogistica.setNroDocRelacionado(rs.getString("NUM_COT") == null ? "": "Cot: "+rs.getString("NUM_COT"));
	}else reportesLogistica.setNroDocRelacionado(rs.getString("NUM_OC") == null ? "": rs.getString("NUM_OC"));
	
	return reportesLogistica;
	}
}

}
