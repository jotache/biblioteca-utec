package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ResumenPendientePorCotizar;

public class GetResumenPendientePorCotizar extends StoredProcedure {

	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
			+ ".pkg_log_cotizacion.SP_SEL_TIPO_PEND_X_COTIZAR";

	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
	
	public GetResumenPendientePorCotizar(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
	}
	
	public Map execute(String codSede) {
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_SEDE, codSede);
    	
        return super.execute(inputs);
    }
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ResumenPendientePorCotizar cotizacion = new ResumenPendientePorCotizar();  
        	
        	cotizacion.setCantPendActivo(rs.getString("CANT_ACTIVO") == null ? "": rs.getString("CANT_ACTIVO"));
        	cotizacion.setCantPendConsInversion(rs.getString("CANT_CONS_INVERSION") == null ? "": rs.getString("CANT_CONS_INVERSION"));
        	cotizacion.setCantPendConsNormal(rs.getString("CANT_CONSUMIBLE") == null ? "": rs.getString("CANT_CONSUMIBLE"));
        	cotizacion.setCantPendServGeneral(rs.getString("CANT_SERV_GENERAL") == null ? "": rs.getString("CANT_SERV_GENERAL"));
        	cotizacion.setCantPendServMantto(rs.getString("CANT_SERV_MANTTO") == null ? "": rs.getString("CANT_SERV_MANTTO"));
        	cotizacion.setCantPendServOtro(rs.getString("CANT_SERV_OTRO") == null ? "": rs.getString("CANT_SERV_OTRO"));
            return cotizacion;
        }
    }
}

