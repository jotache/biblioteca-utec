package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
/*
	PROCEDURE SP_ACT_COTI_ACTUAL(
	E_C_CODCOTI IN CHAR,
	E_C_USUARIO IN CHAR,
	E_C_TIPOPAGO IN CHAR,
	E_C_FECINI IN CHAR,
	E_C_HORAINI IN CHAR,
	E_C_FECFIN IN CHAR,
	E_C_HORAFIN IN CHAR,
	E_V_CASDESC IN VARCHAR2,
	E_V_CADCODDET IN VARCHAR2,
	E_C_CANDET IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */
public class UpdateCotizacionActual extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_cotizacion.sp_act_coti_actual";
		
	private static final String E_C_CODCOTI = "E_C_CODCOTI";
	private static final String E_C_USUARIO = "E_C_USUARIO";
	private static final String E_C_TIPOPAGO = "E_C_TIPOPAGO";
	private static final String E_C_FECINI = "E_C_FECINI";
	private static final String E_C_HORAINI = "E_C_HORAINI";
	private static final String E_C_FECFIN = "E_C_FECFIN";
	private static final String E_C_HORAFIN = "E_C_HORAFIN";
	private static final String E_V_CASDESC = "E_V_CASDESC";
	private static final String E_V_CADCODDET = "E_V_CADCODDET";
	private static final String E_C_CANDET = "E_C_CANDET";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateCotizacionActual(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPOPAGO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FECINI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_HORAINI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FECFIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_HORAFIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CASDESC, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CADCODDET, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CANDET, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codCotizacion, String usuario, String codTipoPago, 
			String fechaInicio, String horaInicio, String fechaFin, String horaFin,
			String cadenaDescripcion, String cadenaCodDetalle, String cantidad) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODCOTI, codCotizacion);
	inputs.put(E_C_USUARIO, usuario);
	inputs.put(E_C_TIPOPAGO, codTipoPago);
	inputs.put(E_C_FECINI, fechaInicio);
	inputs.put(E_C_HORAINI, horaInicio);
	inputs.put(E_C_FECFIN, fechaFin);
	inputs.put(E_C_HORAFIN, horaFin);
	inputs.put(E_V_CASDESC, cadenaDescripcion);
	inputs.put(E_V_CADCODDET, cadenaCodDetalle);
	inputs.put(E_C_CANDET, cantidad);
	
	return super.execute(inputs);
	
	}
}
