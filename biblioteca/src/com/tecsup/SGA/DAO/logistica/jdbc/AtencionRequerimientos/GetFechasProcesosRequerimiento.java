package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllSolicitudesReqRel.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;

public class GetFechasProcesosRequerimiento extends StoredProcedure {
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
									+ ".pkg_log_reportes.SP_SEL_PROCESO_LOGISTICA";
	
	private static final String E_C_SEDE = "E_C_SEDE";	
	private static final String E_C_DESDE = "E_C_DESDE";
	private static final String E_C_HASTA = "E_C_HASTA";
	private static final String E_C_INVERSION = "E_C_INVERSION";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String E_C_NOMCOMPRADOR = "E_C_NOMCOMPRADOR";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
	
	public GetFechasProcesosRequerimiento(DataSource dataSource){
		super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_DESDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_HASTA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_INVERSION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NOMCOMPRADOR, OracleTypes.CHAR)); 
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
	}
	
    public Map execute(String sede, String fecIni, String fecFin, String tipoInversion, String codUsuario,String nomComprador) {    	
    	Map inputs = new HashMap();    	
    	inputs.put(E_C_SEDE, sede);    	     	
    	inputs.put(E_C_DESDE, fecIni);
    	inputs.put(E_C_HASTA, fecFin);
    	inputs.put(E_C_INVERSION, tipoInversion);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	inputs.put(E_C_NOMCOMPRADOR, nomComprador);
    	System.out.println("*******************************");
    	System.out.println("E_C_SEDE:"+sede);
    	System.out.println("E_C_DESDE:"+fecIni);
    	System.out.println("E_C_HASTA:"+fecFin);
    	System.out.println("E_C_INVERSION:"+tipoInversion);
    	System.out.println("E_C_CODUSUARIO:"+codUsuario);
    	System.out.println("E_C_NOMCOMPRADOR:"+nomComprador);
    	System.out.println("*******************************");
        return super.execute(inputs);
    }
    
	final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SolRequerimiento solRequerimiento= new SolRequerimiento();   
        	solRequerimiento.setUsuAsignado(rs.getString("USUARIO") == null ? "": rs.getString("USUARIO"));        	
        	solRequerimiento.setNroItem(rs.getString("ITEM") == null ? "": rs.getString("ITEM"));
        	solRequerimiento.setCantidad(rs.getString("CANT") == null ? "": rs.getString("CANT"));        	
        	solRequerimiento.setUnidMed(rs.getString("UM") == null ? "": rs.getString("UM"));
        	solRequerimiento.setFamilia(rs.getString("FAMILIA") == null ? "": rs.getString("FAMILIA"));
        	solRequerimiento.setFecEnvio(rs.getString("FECENVIO") == null ? "": rs.getString("FECENVIO"));
        	solRequerimiento.setFecAprobacion(rs.getString("FECAPROBACION") == null ? "": rs.getString("FECAPROBACION"));        	
        	solRequerimiento.setFecEstimada(rs.getString("FECESTIMADA") == null ? "": rs.getString("FECESTIMADA"));
        	solRequerimiento.setComprador(rs.getString("COMPRADOR") == null ? "": rs.getString("COMPRADOR"));
        	solRequerimiento.setFecAsignado(rs.getString("FECASIGNADO") == null ? "": rs.getString("FECASIGNADO"));
        	solRequerimiento.setTipoPago(rs.getString("PAGO") == null ? "": rs.getString("PAGO"));
        	solRequerimiento.setFecGenOC(rs.getString("FECGENOC") == null ? "": rs.getString("FECGENOC"));
        	solRequerimiento.setFecJefeLog(rs.getString("JEFELOG") == null ? "": rs.getString("JEFELOG"));
        	solRequerimiento.setFecDirAdmin(rs.getString("DIRADMI") == null ? "": rs.getString("DIRADMI"));
        	solRequerimiento.setFecDirCencos(rs.getString("DIRCECO") == null ? "": rs.getString("DIRCECO"));
        	solRequerimiento.setNombreServicio(rs.getString("NOMDIR") == null ? "": rs.getString("NOMDIR"));        	
        	solRequerimiento.setFecDirGeneral(rs.getString("DIRGENE") == null ? "": rs.getString("DIRGENE"));
        	solRequerimiento.setFecRecepcion(rs.getString("FECRECEPCION") == null ? "": rs.getString("FECRECEPCION"));
        	solRequerimiento.setFechaEntrega(rs.getString("FECENTREGA") == null ? "": rs.getString("FECENTREGA"));
        	solRequerimiento.setCantApoyo(rs.getString("DESPACHADA") == null ? "": rs.getString("DESPACHADA"));
        	return solRequerimiento;
        }
    }
	
}
