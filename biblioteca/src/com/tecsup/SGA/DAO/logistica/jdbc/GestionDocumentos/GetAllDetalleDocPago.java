package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DetalleDocPago;

public class GetAllDetalleDocPago extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + 
".pkg_log_doc_pago.sp_sel_detalle_doc_pago_val";
	
	private static final String E_V_DOCP_ID = "E_V_DOCP_ID"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetalleDocPago(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_DOCP_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String docId) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_V_DOCP_ID, docId);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	DetalleDocPago curso = new DetalleDocPago();
        	
        	curso.setCodCotizacion(rs.getString("COD_COTIZACION")== null ? "" : rs.getString("COD_COTIZACION"));
        	curso.setCodProveedor(rs.getString("COD_PROVEEDOR")== null ? "" : rs.getString("COD_PROVEEDOR"));
        	curso.setCodDetCotProv(rs.getString("COD_DET_COT_PROV")== null ? "" : rs.getString("COD_DET_COT_PROV"));
        	curso.setCodReq(rs.getString("COD_REQ")== null ? "" : rs.getString("COD_REQ"));
        	curso.setCodReqDet(rs.getString("COD_REQ_DET")== null ? "" : rs.getString("COD_REQ_DET"));
        	curso.setCantidad(rs.getString("CANT_SOLICITADA")== null ? "" : rs.getString("CANT_SOLICITADA"));       	
        	curso.setCodItem(rs.getString("COD_ITEM")== null ? "" : rs.getString("COD_ITEM"));
        	curso.setNomItem(rs.getString("NOM_ITEM")== null ? "" : rs.getString("NOM_ITEM"));
        	curso.setCantFacturada(rs.getString("CANT_FACTURADA")== null ? "" : rs.getString("CANT_FACTURADA"));
        	curso.setPreUni(rs.getString("PRECIO_UNITARIO")== null ? "" : rs.getString("PRECIO_UNITARIO"));
        	curso.setPreUni(curso.getPreUni().trim());
        	curso.setSubTotal(rs.getString("SUB_TOTAL")== null ? "" : rs.getString("SUB_TOTAL"));
        	curso.setSubTotal(curso.getSubTotal().trim());
        	curso.setObservacion(rs.getString("OBSERVACION")== null ? "" : rs.getString("OBSERVACION"));
        	curso.setObservacion(curso.getObservacion().trim());
        	
        	curso.setUnidadMedida(rs.getString("UNIDAD")== null ? "" : rs.getString("UNIDAD")); //jhpr 2008-09-25
         	return curso;
        }
    }
}
