package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.BienesDevolucion;
import com.tecsup.SGA.modelo.Producto;

public class GetAllImpresionTickets extends StoredProcedure{
	/*
	 * PROCEDURE SP_SEL_IMPRESION_TICKETS(
		E_C_COD_SEDE IN CHAR,
		S_C_RECORDSET IN OUT CUR_RECORDSET)
	 */
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_impresion_tickets";
	
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllImpresionTickets(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_SEDE, codSede);    	
    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {        	
        	Producto bien= new Producto();
        	
        	bien.setDscFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
        	bien.setCodProducto(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
        	bien.setNomProducto(rs.getString("NOM_BIEN") == null ? "": rs.getString("NOM_BIEN"));
        	bien.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
        	bien.setDscUbicacion(rs.getString("DSC_UBICACION") == null ? "": rs.getString("DSC_UBICACION"));
        	
        	return bien;
        }
    }
}
