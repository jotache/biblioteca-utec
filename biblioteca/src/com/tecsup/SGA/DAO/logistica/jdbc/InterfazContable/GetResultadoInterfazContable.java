package com.tecsup.SGA.DAO.logistica.jdbc.InterfazContable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.InterfazContable.GetDetalleInterfaz.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.InterfazContableResultado;

public class GetResultadoInterfazContable extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_inter_contable.sp_sel_resultado_interfaz";
	/*E_C_COD_SEDE IN VARCHAR2,
E_C_MES IN VARCHAR2,
E_C_ANHO IN VARCHAR2,
E_C_TIPO_INTERFAZ VARCHAR2*/
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_C_MES = "E_C_MES";
	private static final String E_C_ANHO = "E_C_ANHO";
	private static final String E_C_TIPO_INTERFAZ = "E_C_TIPO_INTERFAZ";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetResultadoInterfazContable(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_MES, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_ANHO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO_INTERFAZ, OracleTypes.VARCHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String codMes, String codAnio, String codTipoInterfaz) {
    	
    	Map inputs = new HashMap();
    	System.out.println("1.- "+codSede);
    	System.out.println("2.- "+codMes);
    	System.out.println("3.- "+codAnio);
    	System.out.println("4.- "+codTipoInterfaz);
    	
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_C_MES, codMes);
    	inputs.put(E_C_ANHO, codAnio);
    	inputs.put(E_C_TIPO_INTERFAZ, codTipoInterfaz);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	InterfazContableResultado interfazContable= new InterfazContableResultado();
        	
        	interfazContable.setCd(rs.getString("CD") == null ? "": rs.getString("CD"));
        	interfazContable.setTd(rs.getString("TD") == null ? "": rs.getString("TD"));
        	interfazContable.setNumero(rs.getString("NUMERO") == null ? "": rs.getString("NUMERO"));
        	interfazContable.setFecha(rs.getString("FECHA") == null ? "": rs.getString("FECHA"));
        	interfazContable.setDescripcion(rs.getString("DESCRIPCION") == null ? "": rs.getString("DESCRIPCION"));
        	interfazContable.setCodigoCuenta(rs.getString("CODIGO_CUENTA") == null ? "": rs.getString("CODIGO_CUENTA"));
        	interfazContable.setContableCeco(rs.getString("CONTABLE_CECO") == null ? "": rs.getString("CONTABLE_CECO"));
        	interfazContable.setDebe(rs.getString("DEBE") == null ? "": rs.getString("DEBE"));
        	interfazContable.setHaber(rs.getString("HABER") == null ? "": rs.getString("HABER"));
        	   return interfazContable;
        }
    }


}
