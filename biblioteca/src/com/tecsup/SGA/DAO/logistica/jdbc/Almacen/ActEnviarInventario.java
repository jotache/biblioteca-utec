package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActEnviarInventario extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_almacen.sp_act_enviar_inventario";
	/*
	 * PROCEDURE SP_ACT_ENVIAR_INVENTARIO(
	E_C_COD_SEDE IN CHAR,
	E_V_COD_USUARIO IN VARCHAR2,
	S_V_RETVAL IN OUT CHAR);
	 */
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActEnviarInventario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		
		declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codSede, String codUsuario) {
		Map inputs = new HashMap();
		
		inputs.put(E_C_COD_SEDE, codSede);
		inputs.put(E_V_COD_USUARIO, codUsuario);
		return super.execute(inputs);
	}
}
