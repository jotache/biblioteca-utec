package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertFlujo extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ". pkg_log_mantto_config.sp_act_ind_flj_act";
	private static final String E_C_COD_TABLA = "E_C_COD_TABLA";
	private static final String E_C_COD_DETALLE = "E_C_COD_DETALLE";
	private static final String E_C_INDICADOR = "E_C_INDICADOR";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO"; 
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertFlujo(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_TABLA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_DETALLE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_INDICADOR, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codTabla, String codDetalle, String indicador,String usuario,String codSede) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_TABLA, codTabla);
	inputs.put(E_C_COD_DETALLE, codDetalle);
	inputs.put(E_C_INDICADOR, indicador);
	inputs.put(E_C_CODUSUARIO, usuario);
	inputs.put(E_C_CODSEDE, codSede);
	
	return super.execute(inputs);
	
	}
}
