package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteProveedor extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_mantto_config.sp_del_proveedor";
	private static final String E_C_COD_PROVEEDOR = "E_C_COD_PROVEEDOR";
	private static final String E_C_USU_CREA = "E_C_USU_CREA";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteProveedor(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_PROVEEDOR, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_USU_CREA, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codPro, String usuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_PROVEEDOR, codPro);
	inputs.put(E_C_USU_CREA, usuario);
	
	return super.execute(inputs);
	
	}
}
