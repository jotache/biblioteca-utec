package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteDocAsociado extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".  pkg_log_doc_pago.sp_del_doc_asociado";
	/*E_C_COD_DOCUMENTO IN CHAR,
E_V_COD_USUARIO IN VARCHAR,
S_V_RETVAL IN OUT VARCHAR2)
	 * */
private static final String E_C_COD_DOCUMENTO = "E_C_COD_DOCUMENTO";
private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
private static final String S_V_RETVAL = "S_V_RETVAL";

public DeleteDocAsociado(DataSource dataSource) {
super(dataSource, SPROC_NAME);
declareParameter(new SqlParameter(E_C_COD_DOCUMENTO, OracleTypes.CHAR));
declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.CHAR));
declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
compile();
}

public Map execute(String codDocumento, String codUsuario) {

Map inputs = new HashMap();
	System.out.println("1.- "+codDocumento);
	System.out.println("2.- "+codUsuario);
inputs.put(E_C_COD_DOCUMENTO, codDocumento);
inputs.put(E_V_COD_USUARIO, codUsuario);


return super.execute(inputs);

}
}
