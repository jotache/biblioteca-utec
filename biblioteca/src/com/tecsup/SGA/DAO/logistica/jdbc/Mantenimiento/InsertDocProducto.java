package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertDocProducto extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_mantto_config.sp_ins_documentos_x_bien";
			
	private static final String E_C_CODBIEN = "E_C_CODBIEN";
	private static final String E_C_TIPOADJ = "E_C_TIPOADJ";
	private static final String E_V_NOM_DOC = "E_V_NOM_DOC"; 
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertDocProducto(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODBIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPOADJ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NOM_DOC, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codBien, String tipoDoc,String nomDoc,String usuario) {
	
	Map inputs = new HashMap();
	System.out.println("1.-"+codBien);
	System.out.println("2.-"+tipoDoc);
	System.out.println("3.-"+nomDoc);
	System.out.println("4.-"+usuario);
	inputs.put(E_C_CODBIEN, codBien);
	inputs.put(E_C_TIPOADJ, tipoDoc);
	inputs.put(E_V_NOM_DOC, nomDoc);
	inputs.put(E_C_CODUSUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
