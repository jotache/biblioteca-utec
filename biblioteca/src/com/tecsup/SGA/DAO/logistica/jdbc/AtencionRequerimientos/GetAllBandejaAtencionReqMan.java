package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.web.logistica.controller.CotDetInsertarProveedoresFormController;
/*
 * PROCEDURE SP_SEL_BNDJ_ATEN_REQ_MANT(
 * 	E_C_CODSEDE IN CHAR,
	E_C_CODTIPOREQ IN CHAR,
	E_C_CODSUBTIPOREQ IN CHAR,
	E_C_NRO_REQ IN CHAR,
	E_C_CECO IN CHAR,
	E_V_NOM_USU_SOL IN VARCHAR2,
	E_C_COD_USUARIO IN CHAR,
E_C_COD_PERFIL IN CHAR
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllBandejaAtencionReqMan extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllBandejaAtencionReqMan.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_bndj_aten_req_mant";
	
	
	//ALQD,17/04/09. A�ADIENDO NUEVO FILTRO, CODESTADO
	//ALQD,21/04/09. A�ADIENDO DOS COLUMNAS MAS
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String E_C_CODTIPOREQ = "E_C_CODTIPOREQ";
	private static final String E_C_CODSUBTIPOREQ = "E_C_CODSUBTIPOREQ";
	private static final String E_C_NRO_REQ = "E_C_NRO_REQ";
	private static final String E_C_CECO = "E_C_CECO";
	private static final String E_V_NOM_USU_SOL = "E_V_NOM_USU_SOL";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String E_C_COD_PERFIL = "E_C_COD_PERFIL";
	private static final String E_C_COD_ESTADO = "E_C_COD_ESTADO";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllBandejaAtencionReqMan(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSUBTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NRO_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CECO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOM_USU_SOL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_PERFIL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_ESTADO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String codTipoRequerimiento, String codSubTipoRequerimiento, 
    		String nroRequerimiento, String centroCosto, String usuSolicitante, String codUsuario,
    		String codPerfil, String codEstado) {
    	
    	Map inputs = new HashMap();
    	log.info("E_C_CODSEDE >> " + codSede);
    	log.info("E_C_CODTIPOREQ >> " + codTipoRequerimiento);
    	log.info("E_C_CODSUBTIPOREQ >> " + codSubTipoRequerimiento);
    	log.info("E_C_NRO_REQ >> " + nroRequerimiento);
    	log.info("E_C_CECO >> " + centroCosto);
    	log.info("E_V_NOM_USU_SOL >> " + usuSolicitante);
    	log.info("E_C_COD_USUARIO >> " + codUsuario);
    	log.info("E_C_COD_PERFIL >> " + codPerfil);
    	log.info("E_C_COD_ESTADO >> " + codEstado);
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_C_CODTIPOREQ, codTipoRequerimiento);
    	inputs.put(E_C_CODSUBTIPOREQ, codSubTipoRequerimiento);
    	inputs.put(E_C_NRO_REQ, nroRequerimiento);
    	inputs.put(E_C_CECO, centroCosto);
    	inputs.put(E_V_NOM_USU_SOL, usuSolicitante);
    	inputs.put(E_C_COD_USUARIO, codUsuario);
    	inputs.put(E_C_COD_PERFIL, codPerfil);
    	inputs.put(E_C_COD_ESTADO, codEstado);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SolRequerimiento solRequerimiento= new SolRequerimiento();
        	/*
		     AS COD_REQ
		     AS NRO_REQ
		     AS DSC_CENTRO_COSTO
		     AS NMB_USUARIO_SOL
		     AS DSC_TIPO_SERVICIO
		     AS NOMBRE_SERVICIO
		     AS FECHA_ENTREGA
		     AS DSC_ESFUERZO
		     AS NRO_DOC_REL
        	 */            
        	solRequerimiento.setIdRequerimiento(rs.getString("COD_REQ") == null ? "": rs.getString("COD_REQ"));
        	solRequerimiento.setNroRequerimiento(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
        	solRequerimiento.setDscCecoSolicitante(rs.getString("DSC_CENTRO_COSTO") == null ? "": rs.getString("DSC_CENTRO_COSTO"));
        	solRequerimiento.setUsuSolicitante(rs.getString("NMB_USUARIO_SOL") == null ? "": rs.getString("NMB_USUARIO_SOL"));
        	solRequerimiento.setDscTipoServicio(rs.getString("DSC_TIPO_SERVICIO") == null ? "": rs.getString("DSC_TIPO_SERVICIO"));
        	solRequerimiento.setNombreServicio(rs.getString("NOMBRE_SERVICIO") == null ? "": rs.getString("NOMBRE_SERVICIO"));
        	solRequerimiento.setFechaEntrega(rs.getString("FECHA_ENTREGA") == null ? "": rs.getString("FECHA_ENTREGA"));
        	solRequerimiento.setDscEsfuerzo(rs.getString("DSC_ESFUERZO") == null ? "": rs.getString("DSC_ESFUERZO"));
        	solRequerimiento.setNroDocRel(rs.getString("NRO_DOC_REL") == null ? "": rs.getString("NRO_DOC_REL"));
        	solRequerimiento.setDscServicio(rs.getString("DSC_SERVICIO") == null ? "": rs.getString("DSC_SERVICIO"));
        	solRequerimiento.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	solRequerimiento.setDscEstado(rs.getString("DSC_EST_REQ") == null ? "": rs.getString("DSC_EST_REQ"));
        	//ALQD,21/04/09. SETEANDO LAS NUEVAS COLUMNAS
        	solRequerimiento.setUsuResponsable(rs.getString("NMB_USUARIO_RES") == null ? "": rs.getString("NMB_USUARIO_RES"));
        	solRequerimiento.setCantApoyo(rs.getString("CANT_APOYO") == null ? "": rs.getString("CANT_APOYO"));
        	return solRequerimiento;
        }
    }
}
