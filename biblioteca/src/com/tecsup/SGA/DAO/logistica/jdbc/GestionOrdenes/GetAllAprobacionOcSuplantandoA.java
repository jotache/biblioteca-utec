package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllDetalleByOrden.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;

import com.tecsup.SGA.modelo.DetalleAprobacion;
import com.tecsup.SGA.modelo.DetalleOrden;

public class GetAllAprobacionOcSuplantandoA extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".PKG_LOG_ORDEN.SP_SEL_APROB_OC_BY_SUPLENTE";
	
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String E_C_TIPO = "E_C_TIPO";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllAprobacionOcSuplantandoA(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codUsuario, String tipoAprobacion) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	inputs.put(E_C_TIPO, tipoAprobacion);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	DetalleAprobacion detalleAprobacion = new DetalleAprobacion();
        
        	detalleAprobacion.setCodUsuario(rs.getString("CODUSUARIO") == null ? "": rs.getString("CODUSUARIO"));
        	detalleAprobacion.setDscAprobador(rs.getString("APROBADOR") == null ? "": rs.getString("APROBADOR"));
        	detalleAprobacion.setObsEstado(rs.getString("OBSESTADO") == null ? "": rs.getString("OBSESTADO"));
        	return detalleAprobacion;
        }
    }

}
