package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertResponsableTrabajo extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_atencion_req.sp_ins_resp_trabajo";
	
	private static final String E_C_CODREQ = "E_C_CODREQ";
	private static final String E_C_CODRESP = "E_C_CODRESP";
	private static final String E_C_CODESF = "E_C_CODESF";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public InsertResponsableTrabajo(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODRESP, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODESF, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codRequerimiento, String codResp,String codEsf, String usuario) {
	
	Map inputs = new HashMap();
	System.out.println("1.-"+codRequerimiento+"<<");
	System.out.println("2.-"+codResp+"<<");
	System.out.println("3.-"+codEsf+"<<");
	System.out.println("4.-"+usuario+"<<");
	
	inputs.put(E_C_CODREQ, codRequerimiento);
	inputs.put(E_C_CODRESP, codResp);
	inputs.put(E_C_CODESF, codEsf);
	inputs.put(E_C_CODUSUARIO, usuario);
	
	
	return super.execute(inputs);		
	}	
}
