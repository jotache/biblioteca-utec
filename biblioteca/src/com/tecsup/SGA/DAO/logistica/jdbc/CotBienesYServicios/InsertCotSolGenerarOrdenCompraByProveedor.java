package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertCotSolGenerarOrdenCompraByProveedor extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_ins_gen_oc_prov";
	/*E_C_CODCOTI IN CHAR,
E_C_CODPROV IN CHAR,
E_V_CADCODDET IN VARCHAR2,
E_V_CADDSCDET IN VARCHAR2,
E_C_CANTIDAD IN CHAR,
E_C_CODUSUARIO IN CHAR,*/
	private static final String E_C_CODCOTI = "E_C_CODCOTI"; 
	private static final String E_C_CODPROV = "E_C_CODPROV"; 
	private static final String E_V_CADCODDET = "E_V_CADCODDET";
	private static final String E_V_CADDSCDET = "E_V_CADDSCDET";
	private static final String E_C_CANTIDAD = "E_C_CANTIDAD";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertCotSolGenerarOrdenCompraByProveedor(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODPROV, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_CADCODDET, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CADDSCDET, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CANTIDAD, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codCotizacion, String codProveedor, String cadCodCotizacionDet,
			String cadDscCotizacionDet, String cantidad, String codUsuario) {
    	
		System.out.println("codCotizacion: "+codCotizacion);
		System.out.println("codProveedor: "+codProveedor);
		System.out.println("cadCodCotizacionDet: "+cadCodCotizacionDet);
		System.out.println("cadDscCotizacionDet: "+cadDscCotizacionDet );
		System.out.println("cantidad: "+cantidad);
		System.out.println("codUsuario: "+codUsuario);
   	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOTI, codCotizacion);
    	inputs.put(E_C_CODPROV, codProveedor);
    	inputs.put(E_V_CADCODDET, cadCodCotizacionDet);
    	inputs.put(E_V_CADDSCDET, cadDscCotizacionDet);
    	inputs.put(E_C_CANTIDAD, cantidad);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	    	
        return super.execute(inputs);
    }
}
