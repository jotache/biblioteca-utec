package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertPresupuesto extends StoredProcedure {

	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 			+ ".PKG_LOG_MANTTO_CONFIG.SP_INS_PRESUPUESTO";
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_ANIO = "E_C_ANIO";
	private static final String E_C_CADENADATOS = "E_C_CADENADATOS";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String E_V_NROREGISTROS = "E_V_NROREGISTROS";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertPresupuesto(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_ANIO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CADENADATOS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NROREGISTROS, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	public Map execute(String sede, String anio, String cadenaDatos,
			String codUsuario, String nroReg){
		
		/*
		System.out.println("*** INI "+  SPROC_NAME + " ****");
		System.out.println("E_C_SEDE:"+sede);
		System.out.println("E_C_ANIO:"+anio);
		System.out.println("E_C_CADENADATOS:"+cadenaDatos);
		System.out.println("E_V_CODUSUARIO:"+codUsuario);
		System.out.println("E_V_NROREGISTROS:"+nroReg);
		System.out.println("*** FIN "+  SPROC_NAME + " ****");
		*/
		
		Map inputs = new HashMap();				
		inputs.put(E_C_SEDE, sede);
		inputs.put(E_C_ANIO, anio);
		inputs.put(E_C_CADENADATOS, cadenaDatos);
		inputs.put(E_V_CODUSUARIO, codUsuario);
		inputs.put(E_V_NROREGISTROS, nroReg);				
		return super.execute(inputs);	
	}
}
