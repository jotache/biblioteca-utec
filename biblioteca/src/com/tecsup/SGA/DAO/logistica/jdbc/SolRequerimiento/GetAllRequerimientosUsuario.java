package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;

/**
 * PROCEDURE SP_SEL_BNDJ_REQ_USU_SOL(
E_C_SEDE    IN CHAR,
E_C_TIPOREQ IN CHAR,
E_V_NROREQ  IN VARCHAR,
E_C_FECREQ_INI  IN CHAR,
E_C_FECREQ_FIN  IN CHAR,
E_C_CECO_REQ    IN VARCHAR,
E_C_ESTREQ      IN CHAR,
E_C_USUSOL      IN CHAR,
S_C_RECORDSET OUT CUR_RECORDSET)
*/
public class GetAllRequerimientosUsuario extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
					+ ".pkg_log_sol_requerimientos.sp_sel_bndj_req_usu_sol";
	private static Log log = LogFactory.getLog(GetAllRequerimientosUsuario.class);
	private static final String SEDE = "E_C_SEDE"; 
	private static final String TIPOREQ = "E_C_TIPOREQ"; 
	private static final String NROREQ = "E_V_NROREQ";
	private static final String FECREQ_INI = "E_C_FECREQ_INI";
	private static final String FECREQ_FIN = "E_C_FECREQ_FIN";
	private static final String CECO_REQ = "E_C_CECO_REQ";
	private static final String ESTREQ = "E_C_ESTREQ";
	private static final String USUSOL = "E_C_USUSOL";
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllRequerimientosUsuario(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(TIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(NROREQ, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(FECREQ_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(FECREQ_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(CECO_REQ, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(ESTREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(USUSOL, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String tipoReq, String nroReq
    		, String fecIni, String fecFin, String cecoReq
    		, String estadoReq, String usuSolicitante) {
    	
    	log.info("	Inicio " + SPROC_NAME);
    	log.info("codSede: "+codSede);
    	log.info("tipoReq: "+tipoReq);
    	log.info("nroReq: "+nroReq);
    	log.info("fecIni: "+fecIni);
    	log.info("fecFin: "+fecFin);
    	log.info("cecoReq: "+cecoReq);
    	log.info("estadoReq: "+estadoReq);
    	log.info("usuSolicitante: "+usuSolicitante);
    	log.info("	Fin " + SPROC_NAME);
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(SEDE, codSede);
    	inputs.put(TIPOREQ, tipoReq);
    	inputs.put(NROREQ, nroReq);
    	inputs.put(FECREQ_INI, fecIni);
    	inputs.put(FECREQ_FIN, fecFin);
    	inputs.put(CECO_REQ, cecoReq);
    	inputs.put(ESTREQ, estadoReq);
    	inputs.put(USUSOL, usuSolicitante);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	SolRequerimiento solRequerimiento = new SolRequerimiento();
        	
        	solRequerimiento.setIdRequerimiento(rs.getString("CODIGO") == null ? "": rs.getString("CODIGO"));
        	solRequerimiento.setTipoRequerimiento(rs.getString("COD_TIPO_REQ") == null ? "": rs.getString("COD_TIPO_REQ"));
        	solRequerimiento.setSubTipoRequerimiento(rs.getString("COD_STIP_REQ") == null ? "": rs.getString("COD_STIP_REQ"));
        	solRequerimiento.setNroRequerimiento(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
        	solRequerimiento.setFechaEmision(rs.getString("FEC_REQ") == null ? "": rs.getString("FEC_REQ"));
        	solRequerimiento.setCecoSolicitante(rs.getString("COD_CENTRO_COSTO_SOL") == null ? "": rs.getString("COD_CENTRO_COSTO_SOL"));
        	solRequerimiento.setDscCecoSolicitante(rs.getString("DSC_CENTRO_COSTO_SOL") == null ? "": rs.getString("DSC_CENTRO_COSTO_SOL"));
        	solRequerimiento.setDscGrupoServicio(rs.getString("GRP_SERVICIO") == null ? "": rs.getString("GRP_SERVICIO"));
        	solRequerimiento.setDscSubTipoRequerimiento(rs.getString("DSC_STIP_REQ") == null ? "": rs.getString("DSC_STIP_REQ"));
        	solRequerimiento.setDscEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
        	solRequerimiento.setDscDetalleEstado(rs.getString("DSC_DETALLE_ESTADO") == null ? "": rs.getString("DSC_DETALLE_ESTADO"));
        	solRequerimiento.setTotalRequerimiento(rs.getString("MTO_TOTAL") == null ? "": rs.getString("MTO_TOTAL"));
        	solRequerimiento.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	solRequerimiento.setIndiceDevolucion(rs.getString("IND_PERMITE_DEV") == null ? "": rs.getString("IND_PERMITE_DEV"));
        	
            return solRequerimiento;
        }
    }
}
