package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Cotizacion;

public class GetEnvioCorreoProveedor extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_envio_correo_prov";
	/*E_C_COD_COTI IN CHAR*/
	
	private static final String E_C_COD_COTI = "E_C_COD_COTI";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetEnvioCorreoProveedor(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_COTI, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codCotizacion) {
    	
    	System.out.println("codCotizacion: "+codCotizacion);
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_COTI, codCotizacion);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Cotizacion cotizacion = new Cotizacion();  
        	
        	cotizacion.setEmail(rs.getString("CORREO") == null ? "": rs.getString("CORREO"));
        	cotizacion.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
        	cotizacion.setNroCotizacion(rs.getString("NRO_COTIZACION") == null ? "": rs.getString("NRO_COTIZACION"));
        	cotizacion.setDscTipoCotizacion(rs.getString("TIPO_COTIZACION") == null ? "": rs.getString("TIPO_COTIZACION"));
        	System.out.println("Correo: "+cotizacion.getEmail());
            return cotizacion;
        }
    }

}
