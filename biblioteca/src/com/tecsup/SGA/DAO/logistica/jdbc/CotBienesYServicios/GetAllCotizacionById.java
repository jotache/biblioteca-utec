package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Cotizacion;

public class GetAllCotizacionById extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_cotizacion_by_id";
	
	private static final String E_C_COD_COTI = "E_C_COD_COTI";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllCotizacionById(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_COTI, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codCotizacion) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_COTI, codCotizacion);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Cotizacion cotizacionDetalle = new Cotizacion();
        	cotizacionDetalle.setTipoCoti(rs.getString("TIPO_COTI") == null ? "": rs.getString("TIPO_COTI"));
        	cotizacionDetalle.setSubTipoCoti(rs.getString("SUB_TIPO_COTI") == null ? "": rs.getString("SUB_TIPO_COTI"));
        	cotizacionDetalle.setFechaInicio(rs.getString("FEC_INICIO") == null ? "": rs.getString("FEC_INICIO"));
        	cotizacionDetalle.setHoraInicio(rs.getString("SCOT_HORA_INICIO") == null ? "": rs.getString("SCOT_HORA_INICIO"));
        	cotizacionDetalle.setFechaFin(rs.getString("FEC_FIN") == null ? "": rs.getString("FEC_FIN"));
        	cotizacionDetalle.setHoraFin(rs.getString("SCOT_HORA_FIN") == null ? "": rs.getString("SCOT_HORA_FIN"));
        	cotizacionDetalle.setDscTipoPago(rs.getString("DSC_TIPO_PAGO") == null ? "": rs.getString("DSC_TIPO_PAGO"));
        	cotizacionDetalle.setNroCoti(rs.getString("NRO_COTIZACION") == null ? "": rs.getString("NRO_COTIZACION"));
        	//ALQD,06/02/09. LEYENDO EL NOMBRE DEL COMPRADOR (ANEXO)
        	cotizacionDetalle.setUsuResponsable(rs.getString("NOM_COMPRADOR") == null ? "": rs.getString("NOM_COMPRADOR"));
        	//ALQD,16/04/09. LEYENDO EL GRUPO DE SERVICIO
        	cotizacionDetalle.setCodGruServicio(rs.getString("COD_GRUSERVICIO") == null ? "": rs.getString("COD_GRUSERVICIO"));
            return cotizacionDetalle;
        }
    }
}
