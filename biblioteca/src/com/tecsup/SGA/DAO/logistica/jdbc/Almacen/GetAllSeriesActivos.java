package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.NumeroSeries;

public class GetAllSeriesActivos extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_series_activo_by_docpag";
	
	
	private static final String E_C_COD_DOC_PAG = "E_C_COD_DOC_PAG";
	private static final String E_C_COD_BIEN = "E_C_COD_BIEN";
	private static final String E_C_COD_DOC_PAG_DET = "E_C_COD_DOC_PAG_DET";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllSeriesActivos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_DOC_PAG, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_BIEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_DOC_PAG_DET, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codDocPago,String codBien,String codDetalle) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_DOC_PAG, codDocPago);
    	inputs.put(E_C_COD_BIEN, codBien);
    	inputs.put(E_C_COD_DOC_PAG_DET, codDetalle);
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	NumeroSeries numeroSeries= new NumeroSeries();
 
        	numeroSeries.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
        	numeroSeries.setCodIngreso(rs.getString("COD_INGRESO") == null ? "": rs.getString("COD_INGRESO"));
        	numeroSeries.setCodDocPago(rs.getString("COD_DOC_PAGO") == null ? "": rs.getString("COD_DOC_PAGO"));
        	numeroSeries.setNroSerie(rs.getString("NRO_SERIE") == null ? "": rs.getString("NRO_SERIE"));
        	numeroSeries.setValorCompra(rs.getString("VALOR_COMPRA") == null ? "": rs.getString("VALOR_COMPRA"));
        	
         	return numeroSeries;
        }
    }
}
