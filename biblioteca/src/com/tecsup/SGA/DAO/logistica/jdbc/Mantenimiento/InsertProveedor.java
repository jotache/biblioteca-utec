package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	E_C_COD_TIPO_CALIF IN VARCHAR2,
	E_C_COD_TIPO_PROV IN VARCHAR2,
	E_V_TIPO_DOC IN VARCHAR2,
	E_V_NRO_DOC IN VARCHAR2,
	E_V_RAZON_SOCIAL IN VARCHAR2,
	E_V_RAZON_SOCIAL_CORTO IN VARCHAR2,
	E_V_EMAIL IN VARCHAR2,
	E_V_DIRECCION IN VARCHAR2,
	E_C_COD_DPTO IN VARCHAR2,
	E_C_COD_PROV IN VARCHAR2,
	E_C_COD_DSTO IN VARCHAR2,
	E_V_ATENCION_A IN VARCHAR2,
	E_C_COD_GIRO IN VARCHAR2,
	E_C_COD_ESTADO IN VARCHAR2,
	E_V_BANCO IN VARCHAR2,
	E_V_TIPO_CUENTA IN VARCHAR2,
	E_V_NRO_CUENTA IN VARCHAR2,
	E_C_COD_MONEDA IN VARCHAR2,
	E_C_USU_CREA IN VARCHAR2,
	S_V_RETVAL IN OUT VARCHAR2);
 */
//ALQD,03/07/09.AGREGANDO DOS NUEVOS PARAMETROS
public class InsertProveedor extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_mantto_config.sp_ins_proveedor";
		private static final String E_C_COD_TIPO_CALIF = "E_C_COD_TIPO_CALIF";
		private static final String E_C_COD_TIPO_PROV = "E_C_COD_TIPO_PROV";
		private static final String E_V_TIPO_DOC = "E_V_TIPO_DOC";
		private static final String E_V_NRO_DOC = "E_V_NRO_DOC";
		private static final String E_V_RAZON_SOCIAL = "E_V_RAZON_SOCIAL";
		private static final String E_V_RAZON_SOCIAL_CORTO = "E_V_RAZON_SOCIAL_CORTO";
		private static final String E_V_EMAIL = "E_V_EMAIL";
		private static final String E_V_DIRECCION = "E_V_DIRECCION";
		private static final String E_C_COD_DPTO = "E_C_COD_DPTO";
		private static final String E_C_COD_PROV = "E_C_COD_PROV";
		private static final String E_C_COD_DSTO = "E_C_COD_DSTO";
		private static final String E_V_ATENCION_A = "E_V_ATENCION_A";
		private static final String E_C_COD_GIRO = "E_C_COD_GIRO";
		private static final String E_C_COD_ESTADO = "E_C_COD_ESTADO";
		private static final String E_V_BANCO = "E_V_BANCO";
		private static final String E_V_TIPO_CUENTA = "E_V_TIPO_CUENTA";
		private static final String E_V_NRO_CUENTA = "E_V_NRO_CUENTA";
		private static final String E_C_COD_MONEDA = "E_C_COD_MONEDA";
		private static final String E_C_USU_CREA = "E_C_USU_CREA";
		private static final String E_C_PAIS = "E_C_PAIS";
		private static final String E_C_TELEFONO = "E_C_TELEFONO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
		public InsertProveedor(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_TIPO_CALIF, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_TIPO_PROV, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TIPO_DOC, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NRO_DOC, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_RAZON_SOCIAL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_RAZON_SOCIAL_CORTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_EMAIL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_DIRECCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_DPTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_PROV, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_DSTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_ATENCION_A, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_GIRO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_ESTADO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_BANCO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_TIPO_CUENTA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NRO_CUENTA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_MONEDA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_USU_CREA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_PAIS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_TELEFONO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
		
		public Map execute(String codTipoCalif, String codTipoProv, String tipoDoc
		,String nroDoc, String razSoc,String razSocCorto,String email,String direc,String codDpto,String codProv
		,String codDsto,String atencion,String codGiro,String codEstado,String banco,String tipCta,String nroCta
		,String codMon,String usuario,String pais,String telefono) {
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_COD_TIPO_CALIF, codTipoCalif);
		inputs.put(E_C_COD_TIPO_PROV, codTipoProv);
		inputs.put(E_V_TIPO_DOC, tipoDoc);
		inputs.put(E_V_NRO_DOC, nroDoc);
		inputs.put(E_V_RAZON_SOCIAL, razSoc);
		inputs.put(E_V_RAZON_SOCIAL_CORTO, razSocCorto);
		inputs.put(E_V_EMAIL, email);
		inputs.put(E_V_DIRECCION, direc);
		inputs.put(E_C_COD_DPTO, codDpto);
		inputs.put(E_C_COD_PROV, codProv);
		inputs.put(E_C_COD_DSTO, codDsto);
		inputs.put(E_V_ATENCION_A, atencion);
		inputs.put(E_C_COD_GIRO, codGiro);
		inputs.put(E_C_COD_ESTADO, codEstado);
		inputs.put(E_V_BANCO, banco);
		inputs.put(E_V_TIPO_CUENTA, tipCta);
		inputs.put(E_V_NRO_CUENTA, nroCta);
		inputs.put(E_C_COD_MONEDA, codMon);
		inputs.put(E_C_USU_CREA, usuario);
		inputs.put(E_C_PAIS, pais);
		inputs.put(E_C_TELEFONO, telefono);
		
		return super.execute(inputs);
		
		}
	
}
