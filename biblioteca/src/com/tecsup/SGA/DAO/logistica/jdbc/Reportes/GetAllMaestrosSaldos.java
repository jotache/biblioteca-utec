package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;
//ALQD,22/09/09.SE AGREGA UN PARAMETRO MAS PARA SALDO > 0
public class GetAllMaestrosSaldos extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllMaestrosSaldos.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA
	+ ".pkg_log_reportes.sp_sel_maestros_saldos";
/* E_C_SEDE IN CHAR,
  E_C_TIPO_BIEN IN CHAR,
  E_C_PERIODO IN CHAR,
  E_C_COD_FAM IN CHAR*/
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN";
	private static final String E_C_PERIODO = "E_C_PERIODO";
	private static final String E_C_COD_FAM = "E_C_COD_FAM";
	private static final String E_C_CON_SALDO = "E_C_CON_SALDO";
	
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllMaestrosSaldos(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_PERIODO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_FAM, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CON_SALDO, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoBien, String periodo,
			String codTipoFamilia, String conSaldo) {
	
		log.info("**** INI "+ SPROC_NAME + " *****");
		log.info("E_C_SEDE: "+codSede);
		log.info("E_C_TIPO_BIEN: "+codTipoBien);
		log.info("E_C_PERIODO: "+periodo);
		log.info("E_C_COD_FAM: "+codTipoFamilia);
		log.info("E_C_CON_SALDO: "+conSaldo);
		log.info("**** FIN "+ SPROC_NAME + " *****");
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_PERIODO, periodo);
	inputs.put(E_C_COD_FAM, codTipoFamilia);
	inputs.put(E_C_CON_SALDO, conSaldo);
	
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	
	ReportesLogistica reportesLogistica = new ReportesLogistica();

    reportesLogistica.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
    
    reportesLogistica.setCodFamilia(rs.getString("COD_FAMILIA") == null ? "": rs.getString("COD_FAMILIA"));
    reportesLogistica.setCodSubFamilia(rs.getString("COD_SUB_FAMILIA") == null ? "": rs.getString("COD_SUB_FAMILIA"));
    
	reportesLogistica.setDscFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	
	reportesLogistica.setSaldoFisico(rs.getString("SALDO_FISICO") == null ? "": rs.getString("SALDO_FISICO"));
	reportesLogistica.setSaldoDisponible(rs.getString("SALDO_DISPONIBLE") == null ? "": rs.getString("SALDO_DISPONIBLE"));
	
	reportesLogistica.setCostoUnitario(rs.getString("COSTO_UNITARIO") == null ? "": rs.getString("COSTO_UNITARIO"));
	
	reportesLogistica.setImporte(rs.getString("IMPORTE") == null ? "": rs.getString("IMPORTE"));
	reportesLogistica.setComprador(rs.getString("COMPRADOR") == null ? "": rs.getString("COMPRADOR"));
	reportesLogistica.setFecUltimoDespacho(rs.getString("FEC_ULTIMO_DESPACHO") == null ? "": rs.getString("FEC_ULTIMO_DESPACHO"));
	reportesLogistica.setFecUltimoOC(rs.getString("FEC_ULTIMO_OC") == null ? "": rs.getString("FEC_ULTIMO_OC"));
	
	return reportesLogistica;
	}
}

}
