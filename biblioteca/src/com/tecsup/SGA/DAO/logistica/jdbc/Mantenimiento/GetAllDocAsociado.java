package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DocumentoAsociado;

public class GetAllDocAsociado extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_doc_pago.sp_sel_doc_asociado";
	private static final String E_C_COD_DOCUMENTO = "E_C_COD_DOCUMENTO";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
	
    public GetAllDocAsociado(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_DOCUMENTO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codDoc) {
    	
    	Map inputs = new HashMap();
    	System.out.println("codDoc: "+codDoc);
    	inputs.put(E_C_COD_DOCUMENTO, codDoc);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	DocumentoAsociado curso = new DocumentoAsociado();
        	
        	curso.setDpdo_id(rs.getString("DPDO_ID")== null ? "" : rs.getString("DPDO_ID"));
        	curso.setDescripcion(rs.getString("DPDO_DESCRIPCION")== null ? "" : rs.getString("DPDO_DESCRIPCION"));
        	curso.setNumero(rs.getString("DPDO_NUMERO")== null ? "" : rs.getString("DPDO_NUMERO"));
        	curso.setMonto(rs.getString("DPDO_MONTO")== null ? "" : rs.getString("DPDO_MONTO"));
        	curso.setFecha(rs.getString("DPDO_FECHA")== null ? "" : rs.getString("DPDO_FECHA"));
        	//ALQD,11/06/09.AGREGANDO 2 NUEVAS COLUMNAS
        	curso.setCodTipMoneda(rs.getString("CODMONEDA")== null ? "" : rs.getString("CODMONEDA"));
        	curso.setDesTipMoneda(rs.getString("DESMONEDA")== null ? "" : rs.getString("DESMONEDA"));
        	curso.setImpTipCamSoles(rs.getString("IMPTC_SOLES")== null ? "" : rs.getString("IMPTC_SOLES"));
        	//ALQD,06/07/09.AGREGANDO NUEVA COLUMNA
        	curso.setMonSoles(rs.getString("MONTO_SOLES")== null ? "" : rs.getString("MONTO_SOLES"));
        	//ALQD,21/07/09.AGREGANDO TRES NUEVAS COLUMNAS
        	curso.setCodProvee(rs.getString("COD_PROVE")== null ? "" : rs.getString("COD_PROVE"));
        	curso.setNomProvee(rs.getString("NOM_PROVE")== null ? "" : rs.getString("NOM_PROVE"));
        	curso.setRucProvee(rs.getString("RUC_PROVE")== null ? "" : rs.getString("RUC_PROVE"));
       
        	return curso;
        }
    }
}
