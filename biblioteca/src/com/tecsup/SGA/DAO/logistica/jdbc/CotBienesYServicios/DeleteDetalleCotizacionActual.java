package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
 * PROCEDURE SP_DEL_DET_COTI_ACTUAL(
	E_C_CODCOTI IN CHAR,
	E_C_CODCOTIDET IN CHAR,
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */
public class DeleteDetalleCotizacionActual extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_del_det_coti_actual";

	private static final String E_C_CODCOTI = "E_C_CODCOTI"; 
	private static final String E_C_CODCOTIDET = "E_C_CODCOTIDET"; 
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteDetalleCotizacionActual(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODCOTIDET, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codCotizacion, String codDetCotizacion, String codUsuario) {
    	
   	
    	Map inputs = new HashMap();
    	System.out.println("..codCotizacion>>"+codCotizacion+">>");
    	System.out.println("..codDetCotizacion>>"+codDetCotizacion+">>");
    	System.out.println("..codUsuario>>"+codUsuario+">>");
    	
    	inputs.put(E_C_CODCOTI, codCotizacion);
    	inputs.put(E_C_CODCOTIDET, codDetCotizacion);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	    	
        return super.execute(inputs);
    }
}
