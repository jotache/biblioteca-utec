package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.CotizacionDetalle;
/*
 * 	PROCEDURE SP_SEL_DET_COTI_ACTUAL(
	E_C_CODCOTIZACION IN CHAR,
	E_C_CODTIPOCOT IN CHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllDetalleCotizacionActual extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllDetalleCotizacionActual.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_det_coti_actual";
	
	private static final String E_C_CODCOTIZACION = "E_C_CODCOTIZACION";
	private static final String E_C_CODTIPOCOT = "E_C_CODTIPOCOT";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetalleCotizacionActual(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCOTIZACION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOCOT, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codCotizacion, String codTipoCotizacion) {
    	
    	Map inputs = new HashMap();
    	log.info("***** INI " + SPROC_NAME  + "*****");
    	log.info("codCotizacion:"+codCotizacion);
    	log.info("codTipoCotizacion:"+codTipoCotizacion);
    	log.info("***** FIN " + SPROC_NAME  + "*****");
    	inputs.put(E_C_CODCOTIZACION, codCotizacion);
    	inputs.put(E_C_CODTIPOCOT, codTipoCotizacion);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionDetalle cotizacionDetalle = new CotizacionDetalle();
        	/*
        	 *AS CODIGO_COT
             ,AS CODIGO_DETALLE
             ,AS CODIGO_BIEN
             ,AS NRO_REQUERIMIENTO
             ,AS USU_SOLICITANTE
             ,AS VALOR1
             ,AS VALOR2
             ,AS VALOR3
             ,AS FECHA_ENTREGA
             ,AS NRO_PROVEEDORES
             ,AS DESCRIPCION
             ,AS DESCRIPCION_COT
        	 */
        	cotizacionDetalle.setCodigo(rs.getString("CODIGO_COT") == null ? "": rs.getString("CODIGO_COT"));
        	cotizacionDetalle.setCodDetalle(rs.getString("CODIGO_DETALLE") == null ? "": rs.getString("CODIGO_DETALLE"));
        	cotizacionDetalle.setCodBien(rs.getString("CODIGO_BIEN") == null ? "": rs.getString("CODIGO_BIEN"));
        	cotizacionDetalle.setNroRequerimiento(rs.getString("NRO_REQUERIMIENTO") == null ? "": rs.getString("NRO_REQUERIMIENTO"));
        	cotizacionDetalle.setUsuSolicitante(rs.getString("USU_SOLICITANTE") == null ? "": rs.getString("USU_SOLICITANTE"));
        	cotizacionDetalle.setProducto(rs.getString("VALOR1") == null ? "": rs.getString("VALOR1"));
        	cotizacionDetalle.setUnidadMedida(rs.getString("VALOR2") == null ? "": rs.getString("VALOR2"));
        	cotizacionDetalle.setCantidad(rs.getString("VALOR3") == null ? "": rs.getString("VALOR3"));
        	cotizacionDetalle.setFechaEntrega(rs.getString("FECHA_ENTREGA") == null ? "": rs.getString("FECHA_ENTREGA"));
        	cotizacionDetalle.setNroProveedores(rs.getString("NRO_PROVEEDORES") == null ? "": rs.getString("NRO_PROVEEDORES"));
        	cotizacionDetalle.setDescripcion(rs.getString("DESCRIPCION") == null ? "": rs.getString("DESCRIPCION"));
        	cotizacionDetalle.setDescripcionCot(rs.getString("DESCRIPCION_COT") == null ? "": rs.getString("DESCRIPCION_COT"));
        	
            return cotizacionDetalle;
        }
    }
}
