package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;

public class GetAllProveedoresByItem extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.SP_SEL_PROV_X_ITEM";
		
	private static final String E_C_CODCOTI = "E_C_CODCOTI"; 
	private static final String E_C_CODDETCOTI = "E_C_CODDETCOTI"; 
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllProveedoresByItem(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODDETCOTI, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new ProvDetMapper()));
        compile();
    }

    public Map execute(String codCotizacion, String codCotizacionDet) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOTI, codCotizacion);
    	inputs.put(E_C_CODDETCOTI, codCotizacionDet);
        return super.execute(inputs);
    }
    
    final class ProvDetMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionProveedor cotizacionProveedor = new CotizacionProveedor();
        	
        	cotizacionProveedor.setCodProv(rs.getString("COD_PROV") == null ? "": rs.getString("COD_PROV"));
        	cotizacionProveedor.setRazonSocial(rs.getString("DSC_PROV") == null ? "": rs.getString("DSC_PROV"));
        	cotizacionProveedor.setDscTipoPersona(rs.getString("DSC_TIPO_PERSONA") == null ? "": rs.getString("DSC_TIPO_PERSONA"));
        	cotizacionProveedor.setDscCalificacion(rs.getString("DSC_CALIFICACION") == null ? "": rs.getString("DSC_CALIFICACION"));
        	cotizacionProveedor.setIndEnvioCorreo(rs.getString("IND_CORREO") == null ? "": rs.getString("IND_CORREO"));
        	
            return cotizacionProveedor;
        }
    }
}
