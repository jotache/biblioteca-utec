package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetTipoServicioConCotizacion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_cotizacion.sp_sel_tipo_serv_con_coti";
	
	/*E_C_COD_SEDE IN CHAR,
E_C_COD_USUARIO IN CHAR,
E_C_COD_PERFIL IN CHAR,
E_C_COD_FAMILIA IN CHAR*/
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String E_C_COD_PERFIL = "E_C_COD_PERFIL";
	private static final String E_C_COD_FAMILIA = "E_C_COD_FAMILIA";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public GetTipoServicioConCotizacion(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_PERFIL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_FAMILIA, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codSede, String codUsuario, String codPerfil, String codGrupoServ) {
	System.out.println("codSede: "+codSede);
	System.out.println("codUsuario: "+codUsuario);
	System.out.println("codPerfil: "+codPerfil);
	System.out.println("codGrupoServ: "+codGrupoServ);
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_SEDE, codSede);	
	inputs.put(E_C_COD_USUARIO, codUsuario);
	inputs.put(E_C_COD_PERFIL, codPerfil);
	inputs.put(E_C_COD_FAMILIA, codGrupoServ);
	
	return super.execute(inputs);
	
	}

}
