package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteProductoByItem extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.SP_DEL_DET_PROD_X_ITEM";

	private static final String E_C_COD_REQ = "E_C_COD_REQ"; 
	private static final String E_C_COD_REQ_DET = "E_C_COD_REQ_DET"; 
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteProductoByItem(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_REQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_COD_REQ_DET, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codReq, String codReqDet, String codUsuario) {
    	
   	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_REQ, codReq);
    	inputs.put(E_C_COD_REQ_DET, codReqDet);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	    	
        return super.execute(inputs);
    }
}
