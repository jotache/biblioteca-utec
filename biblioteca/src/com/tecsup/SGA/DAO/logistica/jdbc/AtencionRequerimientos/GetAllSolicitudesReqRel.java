package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.SolRequerimiento;
/*
 * PROCEDURE SP_SEL_REQ_RELACIONADOS(
	E_V_COD_REQ IN VARCHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllSolicitudesReqRel extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_req_relacionados";
	
	private static final String E_V_COD_REQ = "E_V_COD_REQ";	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";	
    
    public GetAllSolicitudesReqRel(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_REQ, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codRequerimiento) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_COD_REQ, codRequerimiento);    	
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SolRequerimiento solRequerimiento= new SolRequerimiento();
        	/*
			   AS COD_REQ,
	           AS NRO_REQ, 
	           AS FEC_EMISION_REQ, 
	           AS COD_TIPO_REQ, 
	           AS DES_TIPO_REQ,
	           AS COD_ESTADO_REQ,
	           AS DES_ESTADO_REQ
        	 */            
        	solRequerimiento.setIdRequerimiento(rs.getString("COD_REQ") == null ? "": rs.getString("COD_REQ"));
        	solRequerimiento.setNroRequerimiento(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
        	solRequerimiento.setFechaEmision(rs.getString("FEC_EMISION_REQ") == null ? "": rs.getString("FEC_EMISION_REQ"));
        	solRequerimiento.setTipoRequerimiento(rs.getString("COD_TIPO_REQ") == null ? "": rs.getString("COD_TIPO_REQ"));
        	solRequerimiento.setDscTipoRequerimiento(rs.getString("DES_TIPO_REQ") == null ? "": rs.getString("DES_TIPO_REQ"));
        	solRequerimiento.setCodEstado(rs.getString("COD_ESTADO_REQ") == null ? "": rs.getString("COD_ESTADO_REQ"));
        	solRequerimiento.setDscEstado(rs.getString("DES_ESTADO_REQ") == null ? "": rs.getString("DES_ESTADO_REQ"));
        	
        	return solRequerimiento;
        }
    }
}
