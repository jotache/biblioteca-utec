package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.logistica.GestionOrdenesDAO;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.UpdateCotizacionActual;
//ALQD,22/10/08. AGREGANDO ESTE IMPORT
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllAprobadorByOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllBandejaConsultaOrdenes;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllCondicionByOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllDetalleByOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllOrdenById;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetEnvioCorreoOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetSolicitantesProductosOC;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.UpdateAprobarOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.UpdateCerrarOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.UpdateRechazarOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.UpdateSuplenteAprobacionOc;
//ALQD,09/01/09. AGREGANDO IMPORT
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllOC_pendienteXentregar;
//ALQD,15/07/09. AGREGANDO NUEVOS IMPORTS
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllAprobacionOcSuplantadoPor;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllAprobacionOcSuplantandoA;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.DetalleAprobacion;
import com.tecsup.SGA.modelo.DetalleOrden;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public class GestionOrdenesDAOJdbc extends JdbcDaoSupport implements GestionOrdenesDAO{

	public List<CotizacionProveedor> GetAllBandejaConsultaOrdenes(String codSede, String codUsuario, String codPerfil, String nroOrden, 
			String fechaInicio, String fechaFin,String codEstado,
    		String rucProvee, String nomProvee, String codComprador, String nomComprador, String codTipoOrden) {
	GetAllBandejaConsultaOrdenes getAllBandejaConsultaOrdenes = new GetAllBandejaConsultaOrdenes(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllBandejaConsultaOrdenes.execute(codSede, codUsuario, codPerfil, 
				nroOrden, fechaInicio,
				fechaFin, codEstado, rucProvee, nomProvee, codComprador, nomComprador, codTipoOrden);
		if(!inputs.isEmpty()){
			return (List<CotizacionProveedor>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public List<DetalleOrden> GetAllDetalleByOrden(String codOrden) {
		GetAllDetalleByOrden getAllDetalleByOrden = new GetAllDetalleByOrden(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllDetalleByOrden.execute(codOrden);
		if(!inputs.isEmpty()){
			return (List<DetalleOrden>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//GetAllCondicionByOrden
	public List<DetalleOrden> GetAllCondicionByOrden(String codOrden) {
		GetAllCondicionByOrden getAllCondicionByOrden = new GetAllCondicionByOrden(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllCondicionByOrden.execute(codOrden);
		if(!inputs.isEmpty()){
			return (List<DetalleOrden>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//ALQD,19/02/09.NUEVO PARAMETRO
	public String UpdateRechazarOrden(String codOrden, String codUsuario,
			String desComentario){
		
		UpdateRechazarOrden updateRechazarOrden = new UpdateRechazarOrden(this.getDataSource());
		
		HashMap inputs = (HashMap)updateRechazarOrden.execute(codOrden, codUsuario,
				desComentario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	//ALQD,19/02/09.NUEVO PARAMETRO
	public String UpdateAprobarOrden(String codOrden, String codUsuario,
			String indInversion){
		
		UpdateAprobarOrden updateAprobarOrden = new UpdateAprobarOrden(this.getDataSource());
		
		HashMap inputs = (HashMap)updateAprobarOrden.execute(codOrden, codUsuario,
				indInversion);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	//
	public List<DetalleOrden> GetAllOrdenById(String codOrden) {
		GetAllOrdenById getAllOrdenById = new GetAllOrdenById(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllOrdenById.execute(codOrden);
		if(!inputs.isEmpty()){
			return (List<DetalleOrden>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public String UpdateCerrarOrden(String codOrden, String codUsuario){
		
	UpdateCerrarOrden updateCerrarOrden = new UpdateCerrarOrden(this.getDataSource());
		
		HashMap inputs = (HashMap)updateCerrarOrden.execute(codOrden, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	public List<OrdenesAprobadas> GetEnvioCorreoOrden(String codOrden) {
		GetEnvioCorreoOrden getEnvioCorreoOrden = new GetEnvioCorreoOrden(this.getDataSource());
		
		HashMap inputs = (HashMap)getEnvioCorreoOrden.execute(codOrden);
		if(!inputs.isEmpty()){
			return (List<OrdenesAprobadas>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	
	public List getSolicitantesProdOrdComp(String codOrden, String codProd) {
		GetSolicitantesProductosOC datos = new GetSolicitantesProductosOC(this.getDataSource());
		
		System.out.println("codOrden:"+codOrden);
		System.out.println("codProd:"+codProd);
		
		HashMap inputs = (HashMap)datos.execute(codOrden,codProd);
		List<DetalleOrden> listaResult = null;
		
		if (codProd.equals("0"))
			listaResult = (List<DetalleOrden>) inputs.get("S_R_PRODUCTO");
		else
			listaResult = (List<DetalleOrden>) inputs.get("S_R_SOLICITANTE");
		
		if (listaResult!=null)
			System.out.println("Lista NO nula:" + listaResult.size());
		else
			System.out.println("Lista NULA");
	
		
		if(!inputs.isEmpty()){
			return (listaResult);
		}
		
		return null;
	}

//ALQD,22/10/08. AGREGANDO METODO PARA LA NUEVA PROPIEDAD
	public List<DetalleOrden> GetAllAprobadorByOC(String codOrden) {
		GetAllAprobadorByOrden datos = new GetAllAprobadorByOrden(this.getDataSource());
		
		HashMap inputs = (HashMap)datos.execute(codOrden);
		if(!inputs.isEmpty()){
			return (List<DetalleOrden>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//ALQD,09/01/09. AGREGANDO METODO PARA REPORTE # 25
	public List<DetalleOrden> GetAllOC_pendienteXentregar(String codComprador) {
		GetAllOC_pendienteXentregar datos = new GetAllOC_pendienteXentregar(this.getDataSource());
		
		HashMap inputs = (HashMap)datos.execute(codComprador);
		if(!inputs.isEmpty()){
			return (List<DetalleOrden>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//ALQD,15/07/09.NUEVOS METODOS PARA CONSULTAR,GRABAR Y ELIMINAR LOS SUPLENTES Y SUPLANTADOS
	public List<DetalleAprobacion> GetAllAprobacionOcSuplantandoA(String codUsuario
			, String tipoAprobacion) {
		GetAllAprobacionOcSuplantandoA datos = new GetAllAprobacionOcSuplantandoA(this.getDataSource());
		
		HashMap inputs = (HashMap)datos.execute(codUsuario, tipoAprobacion);
		if(!inputs.isEmpty()){
			return (List<DetalleAprobacion>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	public List<DetalleAprobacion> GetAllAprobacionOcSuplantadoPor(String codUsuario
			, String tipoAprobacion) {
		GetAllAprobacionOcSuplantadoPor datos = new GetAllAprobacionOcSuplantadoPor(this.getDataSource());
		
		HashMap inputs = (HashMap)datos.execute(codUsuario, tipoAprobacion);
		if(!inputs.isEmpty()){
			return (List<DetalleAprobacion>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//ALQD,16/07/09.NUEVO METODO PARA ACTUALIZAR LOS SUPLENTES DE APROBACION
	public String UpdateSuplenteAprobacionOc(String codPrincipal, String codSede
			, String cadAprobacion) {
		UpdateSuplenteAprobacionOc updateSuplenteAprobacionOc = new UpdateSuplenteAprobacionOc(this.getDataSource());
		HashMap inputs = (HashMap)updateSuplenteAprobacionOc.execute(codPrincipal
				, codSede, cadAprobacion);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
}
