package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Logistica;

public class GetAllTablaDetalle extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_comun.sp_sel_tabla_detalle_log";
		
		private static final String E_C_TTDE_PADRE_ID = "E_C_TTDE_PADRE_ID"; 
		private static final String E_C_COD_ID = "E_C_COD_ID"; 
		private static final String E_C_COD_TABLA_ID = "E_C_COD_TABLA_ID";
		private static final String E_V_CODIGO = "E_V_CODIGO"; 
		private static final String E_V_TTDE_DESCRIPCION = "E_V_TTDE_DESCRIPCION"; 
		private static final String E_C_TIPO = "E_C_TIPO";
		private static final String S_C_RECORDSET = "S_C_RECORDSET";
	    
	    public GetAllTablaDetalle(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        declareParameter(new SqlParameter(E_C_TTDE_PADRE_ID, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_COD_ID, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_C_COD_TABLA_ID, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_V_CODIGO, OracleTypes.CHAR));
	        declareParameter(new SqlParameter(E_V_TTDE_DESCRIPCION, OracleTypes.VARCHAR));
	        declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
	        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
	        compile();
	    }

	    public Map execute(String padre_Id, String codId
	    	,String codTabla, String codigo, String descripcion, String tipo) {
	    	
	    	Map inputs = new HashMap();
	    	
	    	System.out.println("padre_Id:"+padre_Id);
	    	System.out.println("codId:"+codId);
	    	System.out.println("codTabla:"+codTabla);
	    	System.out.println("codigo:"+codigo);
	    	System.out.println("descripcion:"+descripcion);
	    	System.out.println("tipo:"+tipo);
	    	
	    	inputs.put(E_C_TTDE_PADRE_ID, padre_Id);
	    	inputs.put(E_C_COD_ID, codId);
	    	inputs.put(E_C_COD_TABLA_ID, codTabla);
	    	inputs.put(E_V_CODIGO, codigo);
	    	inputs.put(E_V_TTDE_DESCRIPCION, descripcion);
	    	inputs.put(E_C_TIPO, tipo);
	        return super.execute(inputs);
	    }
	    
	    final class CursosMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	Logistica curso = new Logistica();
	        		        	
	        	curso.setCodSecuencial(rs.getString("CODIGOSECUENCIAL")== null ? "" : rs.getString("CODIGOSECUENCIAL"));
	        	curso.setCodPadre(rs.getString("CODIGOPADRE")== null ? "" : rs.getString("CODIGOPADRE"));
	        	curso.setCodigo(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
	        	curso.setDescripcion(rs.getString("DESCRIPCION")== null ? "" : rs.getString("DESCRIPCION"));
	        	curso.setDscPadre(rs.getString("DESC_PADRE")== null ? "" : rs.getString("DESC_PADRE"));
	        	curso.setValor1(rs.getString("VALOR1")== null ? "" : rs.getString("VALOR1"));
	        	curso.setValor2(rs.getString("VALOR2")== null ? "" : rs.getString("VALOR2"));
	        	curso.setValor3(rs.getString("VALOR3")== null ? "" : rs.getString("VALOR3"));
	            return curso;
	        }
	    }
		
}
