package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.NotaSeguimiento;
/*
 * PROCEDURE SP_SEL_NOTA_SEG_X_REQ(
	E_V_COD_REQ IN VARCHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllNotasSeguimientoRequerimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_nota_seg_x_req";
	
	private static final String E_V_COD_REQ = "E_V_COD_REQ";
	private static final String E_V_COD_NOTA = "E_V_COD_NOTA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllNotasSeguimientoRequerimiento(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_COD_NOTA, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codRequerimiento,String codNota) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_COD_REQ, codRequerimiento);
    	inputs.put(E_V_COD_NOTA, codNota);
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	NotaSeguimiento nota = new NotaSeguimiento();        	
        	/*
			AS COD_REQ,
          	AS COD_NOTA,
          	AS DES_NOTA,
          	AS FEC_REG,
          	AS COD_USUARIO
        	 */
        	nota.setCodRequerimiento(rs.getString("COD_REQ") == null ? "": rs.getString("COD_REQ"));
        	nota.setCodNota(rs.getString("COD_NOTA") == null ? "": rs.getString("COD_NOTA"));        	
        	nota.setDscNota(rs.getString("DES_NOTA") == null ? "": rs.getString("DES_NOTA"));
        	nota.setFechaRegistro(rs.getString("FEC_REG") == null ? "": rs.getString("FEC_REG"));
        	
        	       	
        	return nota;
        }
    }
}
