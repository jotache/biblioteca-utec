package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Almacen;

public class GetAllDatosOrden extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_datos_orden";
	
		/*E_C_COD_ORDEN IN CHAR*/
	
	private static final String E_C_COD_ORDEN = "E_C_COD_ORDEN";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDatosOrden(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_ORDEN, OracleTypes.CHAR));
       
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codOrden) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_ORDEN, codOrden);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Almacen almacen= new Almacen();
        	
        	almacen.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
        	almacen.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	almacen.setFecEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	almacen.setNroProveedor(rs.getString("NRO_PROVEEDOR") == null ? "": rs.getString("NRO_PROVEEDOR"));
        	almacen.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
        	almacen.setDscAtencion(rs.getString("ATENCION_A") == null ? "0": rs.getString("ATENCION_A"));
         	
        	almacen.setMontoTotal(rs.getString("MONTO") == null ? "": rs.getString("MONTO"));
        	almacen.setDscComprador(rs.getString("DSC_COMPRADOR") == null ? "": rs.getString("DSC_COMPRADOR"));
         	         	
        	return almacen;
        }
    }
}
