package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_ACT_ENVIAR_COTI(
	E_C_CODCOTI IN CHAR,	
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */

public class EnviarCotizacionUsuario extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_cotizacion.sp_act_enviar_coti";
		
		private static final String E_C_CODCOTI = "E_C_CODCOTI";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public EnviarCotizacionUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);		
		declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codCotizacion, String codUsuario) {
		
		Map inputs = new HashMap();
		System.out.println("1.-"+codCotizacion+"<<");
		System.out.println("2.-"+codUsuario+"<<");
		
		inputs.put(E_C_CODCOTI, codCotizacion);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		
		return super.execute(inputs);		
	}	
}
