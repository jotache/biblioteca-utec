package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento.GetAllTablaDetalle.CursosMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class GetAllFlujosAprobacion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_mantto_config.sp_sel_flujos_aprobacion";
	
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE"; 
	private static final String E_C_COD_TIPO_FLUJO = "E_C_COD_TIPO_FLUJO"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllFlujosAprobacion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_TIPO_FLUJO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codSede, String codFlujo) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_C_COD_TIPO_FLUJO, codFlujo);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	TipoTablaDetalle tipoTablaDetalle = new TipoTablaDetalle();
        	tipoTablaDetalle.setCodTipoTabla(rs.getString("CODIGO_PADRE"));
        	tipoTablaDetalle.setCodTipoTablaDetalle(rs.getString("CODIGO_DETALLE"));
        	tipoTablaDetalle.setDescripcion(rs.getString("DESCRIPCION"));
        	tipoTablaDetalle.setDscValor1(rs.getString("VALOR1"));
        	tipoTablaDetalle.setDscValor2(rs.getString("VALOR2"));
        	tipoTablaDetalle.setDscValor3(rs.getString("VALOR3"));
        	tipoTablaDetalle.setDscValor4(rs.getString("VALOR4"));
        	tipoTablaDetalle.setDscValor5(rs.getString("VALOR5"));
        	tipoTablaDetalle.setDscValor6(rs.getString("desc_padre_hijo"));
            return tipoTablaDetalle;
        }
    }
}
