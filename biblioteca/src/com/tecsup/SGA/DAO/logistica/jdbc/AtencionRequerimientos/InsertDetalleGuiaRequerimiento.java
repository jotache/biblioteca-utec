package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_INS_DET_GUIA_REQ(
	E_C_CODGUIA IN CHAR,
	E_C_CODREQ IN CHAR,
	E_C_FEC_GUIA IN CHAR,
	E_V_OBSERVACION IN VARCHAR2,
	E_V_CAD_CODDETREQ IN VARCHAR2,--Cuando es Bien Codigosd Cancatenados 1|2|, en caso sea serv solo va el cod 1
	E_V_CAD_CANTSOL IN VARCHAR2,
	E_V_CAD_PRECIOS IN VARCHAR2,
	E_V_CAD_CANTENT IN VARCHAR2,
	E_C_CANT_REG IN CHAR,
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2);
 */

public class InsertDetalleGuiaRequerimiento extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_atencion_req.sp_ins_det_guia_req";
		
		private static final String E_C_CODGUIA = "E_C_CODGUIA";
		private static final String E_C_CODREQ = "E_C_CODREQ";
		private static final String E_C_FEC_GUIA = "E_C_FEC_GUIA";
		private static final String E_V_OBSERVACION = "E_V_OBSERVACION";
		private static final String E_V_CAD_CODDETREQ = "E_V_CAD_CODDETREQ";
		private static final String E_V_CAD_CANTSOL = "E_V_CAD_CANTSOL";
		private static final String E_V_CAD_PRECIOS = "E_V_CAD_PRECIOS";
		private static final String E_V_CAD_CANTENT = "E_V_CAD_CANTENT";
		private static final String E_C_CANT_REG = "E_C_CANT_REG";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public InsertDetalleGuiaRequerimiento(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODGUIA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FEC_GUIA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_OBSERVACION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CAD_CODDETREQ, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CAD_CANTSOL, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CAD_PRECIOS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CAD_CANTENT, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CANT_REG, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codGuia, String codRequerimiento, String fechaGuia, 
			String observacion, String cadenaCodDetalle, String cadenaCantidadSolicitada,
			String cadenaPrecios, String cadenaCantidadEntregada, String cantidad, String codUsuario) {
		
		Map inputs = new HashMap();
		System.out.println("InsertBienesByProveedor");
		System.out.println("1.-"+codGuia+"<<");
		System.out.println("2.-"+codRequerimiento+"<<");
		System.out.println("3.-"+fechaGuia+"<<");
		System.out.println("4.-"+observacion+"<<");
		System.out.println("5.-"+cadenaCodDetalle+"<<");
		System.out.println("6.-"+cadenaCantidadSolicitada+"<<");
		System.out.println("7.-"+cadenaPrecios+"<<");
		System.out.println("8.-"+cadenaCantidadEntregada+"<<");
		System.out.println("9.-"+cantidad+"<<");
		System.out.println("10.-"+codUsuario+"<<");
		
		inputs.put(E_C_CODGUIA, codGuia);
		inputs.put(E_C_CODREQ, codRequerimiento);
		inputs.put(E_C_FEC_GUIA, fechaGuia);
		inputs.put(E_V_OBSERVACION, observacion);
		inputs.put(E_V_CAD_CODDETREQ, cadenaCodDetalle);
		inputs.put(E_V_CAD_CANTSOL, cadenaCantidadSolicitada);
		inputs.put(E_V_CAD_PRECIOS, cadenaPrecios);		
		inputs.put(E_V_CAD_CANTENT, cadenaCantidadEntregada);
		inputs.put(E_C_CANT_REG, cantidad);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		
		return super.execute(inputs);
		
	}	
}
