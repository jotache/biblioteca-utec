package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Almacen;

public class GetAllBienesRecepcion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_bienes_recepcion";
	
		/*E_C_SEDE IN CHAR,
E_V_NRO_ORDEN IN VARCHAR2,
E_C_FEC_EMI_ORDEN_INI IN CHAR,
E_C_FEC_EMI_ORDEN_FIN IN CHAR,
E_V_RUC_PROV IN VARCHAR2,
E_V_DSC_PROV IN VARCHAR2,
E_V_DSC_COMP IN VARCHAR2,
E_V_NRO_GUIA IN VARCHAR2,
E_C_FEC_EMI_GUIA_INI IN CHAR,
E_C_FEC_EMI_GUIA_FIN IN CHA,*/
	
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_V_NRO_ORDEN = "E_V_NRO_ORDEN";
	private static final String E_C_FEC_EMI_ORDEN_INI = "E_C_FEC_EMI_ORDEN_INI";
	private static final String E_C_FEC_EMI_ORDEN_FIN = "E_C_FEC_EMI_ORDEN_FIN";
	private static final String E_V_RUC_PROV = "E_V_RUC_PROV";
	private static final String E_V_DSC_PROV = "E_V_DSC_PROV";
	private static final String E_V_DSC_COMP = "E_V_DSC_COMP";
	private static final String E_V_NRO_GUIA = "E_V_NRO_GUIA";
	private static final String E_C_FEC_EMI_GUIA_INI = "E_C_FEC_EMI_GUIA_INI";
	private static final String E_C_FEC_EMI_GUIA_FIN = "E_C_FEC_EMI_GUIA_FIN";
	//ALQD,23/01/09. NUEVA CONSTANTE Y PARAMETRO EN LA BASE DE DATOS
	private static final String E_C_IND_ING_X_CONFIRMAR = "E_C_IND_ING_X_CONFIRMAR";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllBienesRecepcion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NRO_ORDEN, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_ORDEN_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_ORDEN_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_RUC_PROV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DSC_PROV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DSC_COMP, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NRO_GUIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_GUIA_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_GUIA_FIN, OracleTypes.CHAR));
        //ALQD,23/01/09. NUEVO PARAMETRO DE BUSQUEDA
        declareParameter(new SqlParameter(E_C_IND_ING_X_CONFIRMAR, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }
//ALQD,23/01/09. NUEVO PARAMETRO DE BUSQUEDA
    public Map execute(String codSede, String nroOrden, String fecEmiOrdIni, String fecEmiOrdFin, 
    		 String nroRucProveedor, String dscProveedor, String dscComprador, String nroGuia, 
    		 String fecEmiGuiaIni, String fecEmiGuiaFin, String indIngPorConfirmar) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_SEDE, codSede);
    	inputs.put(E_V_NRO_ORDEN, nroOrden);
    	inputs.put(E_C_FEC_EMI_ORDEN_INI, fecEmiOrdIni);
    	inputs.put(E_C_FEC_EMI_ORDEN_FIN, fecEmiOrdFin);
    	inputs.put(E_V_RUC_PROV, nroRucProveedor);
    	inputs.put(E_V_DSC_PROV, dscProveedor);
    	inputs.put(E_V_DSC_COMP, dscComprador);
    	inputs.put(E_V_NRO_GUIA, nroGuia);
    	inputs.put(E_C_FEC_EMI_GUIA_INI, fecEmiGuiaIni);
    	inputs.put(E_C_FEC_EMI_GUIA_FIN, fecEmiGuiaFin);
    	inputs.put(E_C_IND_ING_X_CONFIRMAR, indIngPorConfirmar);
    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Almacen almacen= new Almacen();
        	
        	almacen.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
        	almacen.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	almacen.setFecEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	almacen.setCodProveedor(rs.getString("COD_PROVEEDOR") == null ? "": rs.getString("COD_PROVEEDOR"));
        	almacen.setDscRazonSocial(rs.getString("RAZON_SOCIAL") == null ? "": rs.getString("RAZON_SOCIAL"));
        	almacen.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "0": rs.getString("DSC_MONEDA"));
         	
        	almacen.setMontoTotal(rs.getString("MONTO_TOTAL") == null ? "": rs.getString("MONTO_TOTAL"));
        	almacen.setDscComprador(rs.getString("DSC_COMPRADOR") == null ? "": rs.getString("DSC_COMPRADOR"));
         	         	
        	return almacen;
        }
    }
}
