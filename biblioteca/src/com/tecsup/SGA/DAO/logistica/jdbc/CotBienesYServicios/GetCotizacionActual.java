package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Cotizacion;
/*
 * PROCEDURE SP_SEL_COTI_ACTUAL(
E_C_CODSEDE IN CHAR,
E_C_CODTIPOCOT IN CHAR,
E_C_CODSUBTIPOCOT IN CHAR,
S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
//ALQD,13/08/09.AGREGANDO NUEVO PARAMETRO
public class GetCotizacionActual extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_coti_actual";
	/*E_C_CODSEDE IN CHAR,
E_C_CODTIPOCOT IN CHAR,
E_C_CODSUBTIPOCOT IN CHAR,
E_C_COD_USUARIO IN CHAR*/
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String E_C_CODTIPOCOT = "E_C_CODTIPOCOT";
	private static final String E_C_CODSUBTIPOCOT = "E_C_CODSUBTIPOCOT";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO"; 
	private static final String E_C_IND_INVERSION = "E_C_IND_INVERSION";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetCotizacionActual(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOCOT, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSUBTIPOCOT, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_IND_INVERSION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String codTipoCotizacion, String codSubTipoCotizacion
    		, String codUsuario, String indInversion) {
    	
    	System.out.println("codSede: "+codSede);
    	System.out.println("codTipoCotizacion: "+codTipoCotizacion);
    	System.out.println("codSubTipoCotizacion: "+codSubTipoCotizacion);
    	System.out.println("codUsuario: "+codUsuario);
    	System.out.println("indInversion: "+indInversion);
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_C_CODTIPOCOT, codTipoCotizacion);
    	inputs.put(E_C_CODSUBTIPOCOT, codSubTipoCotizacion);
    	inputs.put(E_C_COD_USUARIO, codUsuario);
    	inputs.put(E_C_IND_INVERSION, indInversion);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Cotizacion cotizacion = new Cotizacion();        	
        	/*
        	 	SC.SCOT_ID AS CODIGO
			  , SC.SCOT_SEDE AS SEDE
			  , SC.TIPT_TIPO_COTIZACION AS TIPO_COTIZACION
			  , SC.TIPT_SUB_TIPO_COTIZACION AS SUB_TIPO_COTIZACION
			  , SC.SCOT_NRO_COTIZACION AS NRO_COTIZACION
			  , TO_CHAR(SC.SCOT_FEC_EMISION,'DD/MM/YYYY') AS FECHA_EMISION
			  , SC.TIPT_TIPO_PAGO AS COD_TIPO_PAGO
			  , SC.SCOT_FEC_INICIO AS FECHA_INICIO
			  , SC.SCOT_HORA_INICIO AS HORA_INICIO
			  , SC.SCOT_FEC_FIN AS FECHA_FIN
			  , SC.SCOT_HORA_FIN AS HORA_FIN
			  , SC.TIPT_ESTADO AS ESTADO
			  , SC.SCOT_FEC_PUBLICACION AS FECHA_PUBLICACION
			  , SC.SCOT_FEC_CIERRE AS FECHA_CIERRE
			  , SC.SCOT_TOTAL_ASIG_SISTEMA AS ASIGANDO_SISTEMA
			  , SC.SCOT_TOTAL_ASIG_USUARIO AS ASIGNADO_USUARIO
			  , SC.SCOT_USU_RESPONSABLE AS USU_RESPONSABLE
        	 */
        	cotizacion.setCodigo(rs.getString("CODIGO") == null ? "": rs.getString("CODIGO"));
        	cotizacion.setSede(rs.getString("SEDE") == null ? "": rs.getString("SEDE"));
        	cotizacion.setCodTipoCotizacion(rs.getString("TIPO_COTIZACION") == null ? "": rs.getString("TIPO_COTIZACION"));
        	cotizacion.setCodSubTipoCotizacion(rs.getString("SUB_TIPO_COTIZACION") == null ? "": rs.getString("SUB_TIPO_COTIZACION"));
        	cotizacion.setNroCotizacion(rs.getString("NRO_COTIZACION") == null ? "": rs.getString("NRO_COTIZACION"));
        	cotizacion.setFechaEmision(rs.getString("FECHA_EMISION") == null ? "": rs.getString("FECHA_EMISION"));
        	cotizacion.setCodTipoPago(rs.getString("COD_TIPO_PAGO") == null ? "": rs.getString("COD_TIPO_PAGO"));
        	cotizacion.setFechaInicio(rs.getString("FECHA_INICIO") == null ? "": rs.getString("FECHA_INICIO"));
        	cotizacion.setHoraInicio(rs.getString("HORA_INICIO") == null ? "": rs.getString("HORA_INICIO"));
        	cotizacion.setFechaFin(rs.getString("FECHA_FIN") == null ? "": rs.getString("FECHA_FIN"));
        	cotizacion.setHoraFin(rs.getString("HORA_FIN") == null ? "": rs.getString("HORA_FIN"));
        	cotizacion.setEstado(rs.getString("ESTADO") == null ? "": rs.getString("ESTADO"));
        	cotizacion.setFechaPublicacion(rs.getString("FECHA_PUBLICACION") == null ? "": rs.getString("FECHA_PUBLICACION"));
        	cotizacion.setFechaCierre(rs.getString("FECHA_CIERRE") == null ? "": rs.getString("FECHA_CIERRE"));
        	cotizacion.setTotalAsigSistema(rs.getString("ASIGNADO_SISTEMA") == null ? "": rs.getString("ASIGNADO_SISTEMA"));
        	cotizacion.setTotalAsigUsuario(rs.getString("ASIGNADO_USUARIO") == null ? "": rs.getString("ASIGNADO_USUARIO"));
        	cotizacion.setUsuResponsable(rs.getString("USU_RESPONSABLE") == null ? "": rs.getString("USU_RESPONSABLE"));
        	
            return cotizacion;
        }
    }
}
