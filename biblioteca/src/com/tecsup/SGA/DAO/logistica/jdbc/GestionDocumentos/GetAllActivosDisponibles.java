package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ActivosDisponibles;

public class GetAllActivosDisponibles extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_comun.sp_sel_activos_disponibles";
	
	private static final String E_C_COD_BIEN = "E_C_COD_BIEN"; 
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllActivosDisponibles(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_BIEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codBien,String codSede) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_COD_BIEN, codBien);
    	inputs.put(E_C_COD_SEDE, codSede);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	ActivosDisponibles curso = new ActivosDisponibles();
        	
        	curso.setCodBienActivo(rs.getString("COD_BIEN_ACTIVO")== null ? "" : rs.getString("COD_BIEN_ACTIVO"));
        	curso.setNroSerie(rs.getString("NRO_SERIE")== null ? "" : rs.getString("NRO_SERIE"));
        	curso.setNroOrden(rs.getString("NRO_ORDEN")== null ? "" : rs.getString("NRO_ORDEN"));
        	curso.setValorCompra(rs.getString("VALOR_COMPRA")== null ? "" : rs.getString("VALOR_COMPRA"));
            return curso;
        }
    }
}
