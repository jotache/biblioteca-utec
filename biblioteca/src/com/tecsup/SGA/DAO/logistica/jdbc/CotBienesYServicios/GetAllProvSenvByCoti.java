package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Proveedor;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;

public class GetAllProvSenvByCoti extends StoredProcedure{

	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".PKG_LOG_COTIZACION.sp_sel_prov_senv_by_coti";
	//ALQD,01/06/09. AGREGANDO UN NUEVO PARAMETRO
	private static final String E_C_COD_COTI = "E_C_COD_COTI";
	private static final String E_C_COD_PROV = "E_C_COD_PROV";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllProvSenvByCoti(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_COTI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_PROV, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    //ALQD,01/06/09. A�ADIENDO NUEVO PARAMETRO
    public Map execute(String codCotizacion, String codProveedor) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_COTI, codCotizacion);
    	inputs.put(E_C_COD_PROV, codProveedor);
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Proveedor obj = new Proveedor();
        	
        	obj.setCodUnico(rs.getString("COD_PROV") == null ? "": rs.getString("COD_PROV"));
        	obj.setRazSoc(rs.getString("PROV_RAZON_SOCIAL") == null ? "": rs.getString("PROV_RAZON_SOCIAL"));
        	//ALQD,01/06/09. LEYENDO NUEVA COLUMNA
        	obj.setCodTipoProveedor(rs.getString("TIPO_PROV") == null ? "": rs.getString("TIPO_PROV"));
        	//ALQD,04/06/09. LEYENDO NUEVA COLUMNA
        	obj.setOtroCostos(rs.getString("OTRO_COSTOS") == null ? "": rs.getString("OTRO_COSTOS"));
            return obj;
        }
    }
}
