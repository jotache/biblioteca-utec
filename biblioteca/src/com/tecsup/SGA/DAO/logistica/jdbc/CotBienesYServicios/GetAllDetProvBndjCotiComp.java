package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.CotizacionDetalleBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionAdjunto;

public class GetAllDetProvBndjCotiComp extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".PKG_LOG_COTIZACION.sp_sel_det_prov_bndj_coti_comp";
		
	private static final String E_C_CODCOTI = "E_C_CODCOTI"; 
	private static final String E_C_CODCOTIDET = "E_C_CODCOTIDET";  
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetProvBndjCotiComp(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCOTIDET, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new DocumentoMapper()));
        compile();
    }

    public Map execute(String codCotizacion, String codCotizacionDet) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOTI, codCotizacion);
    	inputs.put(E_C_CODCOTIDET, codCotizacionDet);
        return super.execute(inputs);
    }
    
    final class DocumentoMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionDetalleBean objCast = new CotizacionDetalleBean();
        	
        	objCast.setCodProv(rs.getString("COD_PROV") == null ? "": rs.getString("COD_PROV"));
        	objCast.setRazonSocial(rs.getString("RAZON_SOCIAL") == null ? "": rs.getString("RAZON_SOCIAL"));
        	objCast.setPrecioUnitario(rs.getString("PRECIO_UNITARIO") == null ? "": rs.getString("PRECIO_UNITARIO"));
        	objCast.setFlgChecked(rs.getString("IND_ASIG") == null ? "0": rs.getString("IND_ASIG"));
        	//descomentar esto y cambiar el nombre OBS si es necesario
        	objCast.setObs(rs.getString("OBS_DET_COTIZACION") == null ? "": rs.getString("OBS_DET_COTIZACION"));
        	objCast.setObsProv(rs.getString("OBS_DET_PROVEEDOR") == null ? "": rs.getString("OBS_DET_PROVEEDOR"));
        	//ALQD,04/06/09.AGREGANDO NUEVA PROPIEDAD
        	objCast.setOtroCosto(rs.getString("OTRO_COSTOS") == null ? "": rs.getString("OTRO_COSTOS"));
        	
            return objCast;
        }
    }
}
