package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public class GetAllOrdenesAprobadas extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_doc_pago.sp_sel_ordenes_aprob";
//ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA NUM_FACTURA	
		/*E_C_CODSEDE IN CHAR,
		E_C_CODUSUARIO IN CHAR,
		E_C_PERFIL IN CHAR,
		E_V_NRO_ORDEN IN VARCHAR2,
		E_C_FEC_EMI_INI IN CHAR,
		E_C_FEC_EMI_FIN IN CHAR,
		E_V_RUC_PROV IN VARCHAR2,
		E_V_NOM_PROV IN VARCHAR2,
		E_V_COD_COMP IN VARCHAR2,
		E_V_NOM_COMP IN VARCHAR2,
		E_C_TIPO_ORDEN IN CHAR,
		E_C_IND_SOLO_CAJA_CHICA IN CHAR,
		E_C_NUM_FACTURA IN CHAR,
		E_C_IND_SIN_ENVIO IN CHAR,*/
	
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String E_C_PERFIL = "E_C_PERFIL";
	private static final String E_V_NRO_ORDEN = "E_V_NRO_ORDEN";
	private static final String E_C_FEC_EMI_INI = "E_C_FEC_EMI_INI";
	private static final String E_C_FEC_EMI_FIN = "E_C_FEC_EMI_FIN";
	private static final String E_V_RUC_PROV = "E_V_RUC_PROV";
	private static final String E_V_NOM_PROV = "E_V_NOM_PROV";
	private static final String E_V_COD_COMP = "E_V_COD_COMP";
	private static final String E_V_NOM_COMP = "E_V_NOM_COMP";
	private static final String E_C_TIPO_ORDEN = "E_C_TIPO_ORDEN";
	private static final String E_C_IND_SOLO_CAJA_CHICA = "E_C_IND_SOLO_CAJA_CHICA";
	private static final String E_C_IND_SIN_ENVIO = "E_C_IND_SIN_ENVIO";
	private static final String E_C_NUM_FACTURA = "E_C_NUMFACTURA";
	private static final String E_C_IND_ING_X_CONFIRMAR="E_C_IND_ING_X_CONFIRMAR";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllOrdenesAprobadas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_PERFIL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NRO_ORDEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_RUC_PROV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_PROV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_COMP, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_COMP, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO_ORDEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_IND_SOLO_CAJA_CHICA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_IND_SIN_ENVIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NUM_FACTURA, OracleTypes.CHAR));
        //ALQD,22/01/09. NUEVO FILTRO DE BUSQUEDA
        declareParameter(new SqlParameter(E_C_IND_ING_X_CONFIRMAR, OracleTypes.CHAR));
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }
    //ALQD,22/01/09. NUEVO FILTRO DE BUSQUEDA
    public Map execute(String codSede, String codUsuario, String codPerfil, String nroOrden, String fecEmisionInicio,
    	String fecEmisionFin, String nroRucProve,String nombreProve, String codComprador, String nomComprador
    	, String codTipoOrden, String indSoloCajaChica, String indSinEnvio, 
    	String numFactura, String indIngPorConfirmar) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	inputs.put(E_C_PERFIL, codPerfil);
    	inputs.put(E_V_NRO_ORDEN, nroOrden);
    	inputs.put(E_C_FEC_EMI_INI, fecEmisionInicio);
    	inputs.put(E_C_FEC_EMI_FIN, fecEmisionFin);
    	inputs.put(E_V_RUC_PROV, nroRucProve);
    	inputs.put(E_V_NOM_PROV, nombreProve);
    	inputs.put(E_V_COD_COMP, codComprador);
    	inputs.put(E_V_NOM_COMP, nomComprador);
    	inputs.put(E_C_TIPO_ORDEN, codTipoOrden);
    	inputs.put(E_C_IND_SOLO_CAJA_CHICA, indSoloCajaChica);
    	inputs.put(E_C_IND_SIN_ENVIO, indSinEnvio);
    	inputs.put(E_C_NUM_FACTURA, numFactura);
    	inputs.put(E_C_IND_ING_X_CONFIRMAR, indIngPorConfirmar);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	OrdenesAprobadas ordenesAprobadas= new OrdenesAprobadas();
        	double monto=0;
            double montoFacturado=0;
            double montoSaldo=0;
             
         	ordenesAprobadas.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
         	ordenesAprobadas.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
         	ordenesAprobadas.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
         	ordenesAprobadas.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
         	ordenesAprobadas.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
         	ordenesAprobadas.setMonto(rs.getString("MONTO") == null ? "0": rs.getString("MONTO"));
         	ordenesAprobadas.setMontoFacturado(rs.getString("MONTO_FACTURADO") == null ? "0": rs.getString("MONTO_FACTURADO"));
         	monto=Double.valueOf(ordenesAprobadas.getMonto());
         	System.out.println("MontoOrden: "+monto);
         	montoFacturado=Double.valueOf(ordenesAprobadas.getMontoFacturado());
         	System.out.println("MontoFacturado: "+montoFacturado);
         	montoSaldo=monto - montoFacturado;
         	System.out.println("MontoSaldo: "+montoSaldo);
         	ordenesAprobadas.setMontoSaldo(String.valueOf(montoSaldo));
         	ordenesAprobadas.setDscComprador(rs.getString("COMPRADOR") == null ? "": rs.getString("COMPRADOR"));
         	ordenesAprobadas.setCodTipoOrden(rs.getString("TIPO_ORDEN") == null ? "": rs.getString("TIPO_ORDEN"));
         	ordenesAprobadas.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
         	ordenesAprobadas.setDscEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
         	ordenesAprobadas.setIndDocPago(rs.getString("IND_GEN_DOC_PAGO") == null ? "": rs.getString("IND_GEN_DOC_PAGO"));
         	
        	return ordenesAprobadas;
        }
    }
}
