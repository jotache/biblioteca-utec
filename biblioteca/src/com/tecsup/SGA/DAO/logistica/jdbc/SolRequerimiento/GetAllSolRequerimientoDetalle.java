package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;

public class GetAllSolRequerimientoDetalle extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
					+ ".pkg_log_sol_requerimientos.sp_sel_det_sol_req";
	
	private static final String E_C_CODSOLREQ = "E_C_CODSOLREQ"; 
	private static final String E_C_CODDETSOLREQ = "E_C_CODDETSOLREQ";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllSolRequerimientoDetalle(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSOLREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODDETSOLREQ, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSolReq, String codDetSolReq) {
    	
    	System.out.println("codSolReq>>"+codSolReq+"<<");
    	System.out.println("codDetSolReq>>"+codDetSolReq+"<<");    	
    
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSOLREQ, codSolReq);
    	inputs.put(E_C_CODDETSOLREQ, codDetSolReq);    	
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	SolRequerimientoDetalle solRequerimientoDetalle = new SolRequerimientoDetalle();        	
        	
        	solRequerimientoDetalle.setCodigo(rs.getString("CODIGO") == null ? "": rs.getString("CODIGO"));
        	solRequerimientoDetalle.setNombreServicio(rs.getString("NOMBRE_SERVICIO") == null ? "": rs.getString("NOMBRE_SERVICIO"));
        	solRequerimientoDetalle.setDescripcion(rs.getString("DESCRIPCION") == null ? "": rs.getString("DESCRIPCION"));
        	solRequerimientoDetalle.setFechaEntrega(rs.getString("FECHA_ENTREGA") == null ? "": rs.getString("FECHA_ENTREGA"));
        	solRequerimientoDetalle.setCodigoBien(rs.getString("CODIGO_BIEN") == null ? "": rs.getString("CODIGO_BIEN"));
        	solRequerimientoDetalle.setProducto(rs.getString("PRODUCTO") == null ? "": rs.getString("PRODUCTO"));
        	solRequerimientoDetalle.setUnidadMedida(rs.getString("UNIDAD_MEDIDA") == null ? "": rs.getString("UNIDAD_MEDIDA"));
        	solRequerimientoDetalle.setCantidad(rs.getString("CANTIDAD") == null ? "": rs.getString("CANTIDAD"));
        	solRequerimientoDetalle.setPrecioUnitario(rs.getString("PRECIO_UNITARIO") == null ? "": rs.getString("PRECIO_UNITARIO"));
        	solRequerimientoDetalle.setTotal(rs.getString("TOTAL") == null ? "": rs.getString("TOTAL"));
        	solRequerimientoDetalle.setTipoGasto(rs.getString("TIPO_GASTO") == null ? "": rs.getString("TIPO_GASTO"));
        	solRequerimientoDetalle.setCantidadDevuelta(rs.getString("CANT_DEV") == null ? "": rs.getString("CANT_DEV"));
        	solRequerimientoDetalle.setMaxDevolucion(rs.getString("MAX_DEVOLVER") == null ? "": rs.getString("MAX_DEVOLVER"));
        	solRequerimientoDetalle.setTotalDevuelto(rs.getString("TOTAL_DEVUELTO") == null ? "": rs.getString("TOTAL_DEVUELTO"));
        	solRequerimientoDetalle.setIndiceAsignarSerie(rs.getString("IND_ASIGNAR_SERIE") == null ? "": rs.getString("IND_ASIGNAR_SERIE")); 
        	solRequerimientoDetalle.setCodDevolucionDet(rs.getString("CODIGO_DET_DEV") == null ? "": rs.getString("CODIGO_DET_DEV"));
        	//ALQD,19/08/08. INICIALIZANDO NUEVA PROPIEDAD
        	solRequerimientoDetalle.setCantDecimales(rs.getString("CANT_DEC") == null ? "": rs.getString("CANT_DEC"));
        	//ALQD,29/10/08. A�ADIENDO DOS PROPIEDADES MAS
        	solRequerimientoDetalle.setTotCantRecibida(rs.getString("TOT_RECIBIDA") == null ? "": rs.getString("TOT_RECIBIDA"));
        	solRequerimientoDetalle.setTotCantDevuelta(rs.getString("TOT_DEVUELTA") == null ? "": rs.getString("TOT_DEVUELTA"));
        	
        	return solRequerimientoDetalle;
        }
    }
}
