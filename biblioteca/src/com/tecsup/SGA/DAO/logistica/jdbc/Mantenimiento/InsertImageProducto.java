package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertImageProducto extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_mantto_config.sp_ins_imagen_bien";
			
	private static final String E_C_CODBIEN = "E_C_CODBIEN";
	private static final String E_V_IMAGEN = "E_V_IMAGEN";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertImageProducto(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODBIEN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_IMAGEN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codBien, String imagen,String usuario) {
	
	Map inputs = new HashMap();
	System.out.println("1.-"+codBien);
	System.out.println("2.-"+imagen);
	System.out.println("3.-"+usuario);
	inputs.put(E_C_CODBIEN, codBien);
	inputs.put(E_V_IMAGEN, imagen);
	inputs.put(E_C_CODUSUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
