package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.AsignarResponsable;

public class GetAllAsigResp extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + 
".pkg_log_sol_requerimientos.sp_sel_asig_resp";
	
	private static final String E_C_INDTIPO = "E_C_INDTIPO"; 
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_CECO = "E_C_CECO";
	private static final String E_V_NOMBRE = "E_V_NOMBRE";
	private static final String E_V_APEPAT = "E_V_APEPAT";
	private static final String E_V_APEMAT = "E_V_APEMAT";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllAsigResp(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_INDTIPO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CECO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOMBRE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_APEPAT, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_APEMAT, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String tipo, String sede, String ceco
    		,String nombre,String apPaterno,String apMaterno) {
    	
    	Map inputs = new HashMap();
    	
    	System.out.println("1.- "+tipo);
    	System.out.println("2.- "+sede);
    	System.out.println("3.- "+ceco);
    	System.out.println("4.- "+nombre);
    	System.out.println("5.- "+apPaterno);
    	System.out.println("6.- "+apMaterno);
    	inputs.put(E_C_INDTIPO, tipo);
    	inputs.put(E_C_SEDE, sede);
    	inputs.put(E_C_CECO, ceco);
    	inputs.put(E_V_NOMBRE, nombre);
    	inputs.put(E_V_APEPAT, apPaterno);
    	inputs.put(E_V_APEMAT, apMaterno);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	AsignarResponsable curso = new AsignarResponsable();
        	
        	curso.setCodigo(rs.getString("CODIGO"));
        	curso.setNombreUsu(rs.getString("NOMBRE_USUARIO"));
        	curso.setValor1(rs.getString("VALOR1"));
        	curso.setValor2(rs.getString("VALOR2"));
        
            return curso;
        }
    }
}
