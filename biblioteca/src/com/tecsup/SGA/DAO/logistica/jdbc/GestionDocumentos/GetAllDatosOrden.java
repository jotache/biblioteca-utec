package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public class GetAllDatosOrden extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_doc_pago.sp_sel_datos_orden";
	
	//E_C_COD_ORDEN IN CHAR
	
	private static final String E_C_COD_ORDEN = "E_C_COD_ORDEN";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDatosOrden(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_ORDEN, OracleTypes.CHAR));
                        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codOrden) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_ORDEN, codOrden);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	OrdenesAprobadas ordenesAprobadas= new OrdenesAprobadas();
        	
        	ordenesAprobadas.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
        	ordenesAprobadas.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	ordenesAprobadas.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	ordenesAprobadas.setMonto(rs.getString("MONTO") == null ? "0": rs.getString("MONTO"));
        	ordenesAprobadas.setCodProveedor(rs.getString("COD_PROV") == null ? "0": rs.getString("COD_PROV"));
        	ordenesAprobadas.setDscProveedor(rs.getString("NRO_DOC_PROV") == null ? "": rs.getString("NRO_DOC_PROV"));
        	ordenesAprobadas.setDscRazonSocial(rs.getString("RAZON_SOCIAL") == null ? "": rs.getString("RAZON_SOCIAL"));
        	        	
        	ordenesAprobadas.setMontoFacturado(rs.getString("MONTO_FACT_VAL") == null ? "0": rs.getString("MONTO_FACT_VAL"));
        	ordenesAprobadas.setMontoSaldoVal(rs.getString("MONTO_SALDO_VAL") == null ? "0": rs.getString("MONTO_SALDO_VAL"));
        	ordenesAprobadas.setMontoSaldo(rs.getString("MONTO_SALDO") == null ? "0": rs.getString("MONTO_SALDO"));
        	ordenesAprobadas.setDscMoneda(rs.getString("MONEDA") == null ? "0": rs.getString("MONEDA"));
        	
        	ordenesAprobadas.setIndImportacion(rs.getString("IND_IMPORTACION") == null ? "0": rs.getString("IND_IMPORTACION"));
        	ordenesAprobadas.setImporteOtros(rs.getString("IMPORTE_OTROS") == null ? "0": rs.getString("IMPORTE_OTROS"));
        	//ALQD,27/05/09.LEYENDO NUEVA PROPIEDAD IND_AFECTOIGV
        	ordenesAprobadas.setIndAfectoIGV(rs.getString("IND_AFECTOIGV") == null ? "0": rs.getString("IND_AFECTOIGV"));
        	return ordenesAprobadas;
        }
    }
}
