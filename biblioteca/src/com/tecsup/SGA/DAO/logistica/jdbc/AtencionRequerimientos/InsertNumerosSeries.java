package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertNumerosSeries extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_sol_requerimientos.sp_ins_activos_det_dev";
		/*
		 *  E_C_COD_COD_DEV IN CHAR,
			E_C_COD_COD_DEV_DET IN CHAR,
			E_C_COD_BIEN IN CHAR,
			E_V_CAD_COD_INGRESOS IN VARCHAR2,
			E_V_CANTIDAD IN VARCHAR2,
			E_C_COD_USUARIO IN CHAR,
			S_V_RETVAL IN OUT VARCHAR2)
		 */
	private static final String E_C_COD_COD_DEV = "E_C_COD_COD_DEV";
	private static final String E_C_COD_COD_DEV_DET = "E_C_COD_COD_DEV_DET";
	private static final String E_C_COD_BIEN = "E_C_COD_BIEN";
	private static final String E_V_CAD_COD_INGRESOS = "E_V_CAD_COD_INGRESOS";
	private static final String E_V_CANTIDAD = "E_V_CANTIDAD";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";		
	
	public InsertNumerosSeries(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_COD_DEV, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_COD_DEV_DET, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_BIEN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_COD_INGRESOS, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CANTIDAD, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codDev, String codDevDet,
			String codBien, String cadCodIng,String cantidad,
			 String codUsuario) {
		
		Map inputs = new HashMap();
		inputs.put(E_C_COD_COD_DEV, codDev);
		inputs.put(E_C_COD_COD_DEV_DET, codDevDet);
		inputs.put(E_C_COD_BIEN, codBien);
		inputs.put(E_V_CAD_COD_INGRESOS, cadCodIng);
		inputs.put(E_V_CANTIDAD, cantidad);
		inputs.put(E_C_COD_USUARIO, codUsuario);
		
		return super.execute(inputs);
	}
}
