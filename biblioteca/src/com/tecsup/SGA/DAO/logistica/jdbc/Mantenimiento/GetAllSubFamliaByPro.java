package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Descriptores;

public class GetAllSubFamliaByPro extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_mantto_config.sp_sel_subfam_by_prov";
	
	private static final String E_C_CODPROV = "E_C_CODPROV"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllSubFamliaByPro(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPROV, OracleTypes.CHAR));
         declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codProv) {
    	
    	Map inputs = new HashMap();
    	System.out.println("codProv: "+codProv);
    	inputs.put(E_C_CODPROV, codProv);
       return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Descriptores curso = new Descriptores();
        
        	curso.setCodRelacion(rs.getString("COD_RELACION")== null ? "" : rs.getString("COD_RELACION"));
        	curso.setCodTipoBien(rs.getString("COD_TIPOBIEN")== null ? "" : rs.getString("COD_TIPOBIEN"));
        	curso.setDscTipoBien(rs.getString("DSC_TIPOBIEN")== null ? "" : rs.getString("DSC_TIPOBIEN"));
        	curso.setCodFamilia(rs.getString("COD_FAMILIA")== null ? "" : rs.getString("COD_FAMILIA"));
        	curso.setDscFamilia(rs.getString("DSC_FAMILIA")== null ? "" : rs.getString("DSC_FAMILIA"));
        	curso.setCodSFamilia(rs.getString("COD_SFAMILIA")== null ? "" : rs.getString("COD_SFAMILIA"));
        	curso.setDscSFamilia(rs.getString("DSC_SFAMILIA")== null ? "" : rs.getString("DSC_SFAMILIA"));
            return curso;
        }
    }
}
