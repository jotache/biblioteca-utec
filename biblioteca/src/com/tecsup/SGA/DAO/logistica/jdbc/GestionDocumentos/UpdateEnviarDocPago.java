package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.driver.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;

public class UpdateEnviarDocPago extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + 
".pkg_log_doc_pago.sp_act_enviar_doc_pago";
	/*E_C_COD_DOC_PAGO IN CHAR,
E_C_CODUSUARIO IN CHAR*/
	private static final String E_C_COD_DOC_PAGO = "E_C_COD_DOC_PAGO";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
    
    public UpdateEnviarDocPago(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_DOC_PAGO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
    	compile();
    }
             
    public Map execute(String codDocPago,String codUsuario) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_COD_DOC_PAGO, codDocPago);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	
        return super.execute(inputs);
    }
    
    
    }


