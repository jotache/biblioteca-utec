package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;

public class GetAllBandejaAtencionRequerimientos extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_bndj_aten_req";
	//ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA
	/*E_C_CODSEDE IN CHAR,
	  E_C_CODTIPOREQ IN CHAR,
	E_C_CODSUBTIPOREQ IN CHAR,
	E_C_NRO_REQ IN CHAR,
	E_C_FEC_INI IN CHAR,
	E_C_FEC_FIN IN CHAR,
	E_C_CECO IN CHAR,
	E_V_NOM_USU_SOL IN VARCHAR2,
	E_V_COD_ESTADO IN CHAR*/
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String E_C_CODTIPOREQ = "E_C_CODTIPOREQ";
	private static final String E_C_CODSUBTIPOREQ = "E_C_CODSUBTIPOREQ";
	private static final String E_C_NRO_REQ = "E_C_NRO_REQ";
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_CECO = "E_C_CECO";
	private static final String E_V_NOM_USU_SOL = "E_V_NOM_USU_SOL";
	private static final String E_V_COD_ESTADO = "E_V_COD_ESTADO";
	private static final String E_C_NRO_SALIDA= "E_C_NRO_SALIDA";
	//ALQD,23/01/09. NUEVO PARAMETRO DE FILTRO
	private static final String E_C_IND_SAL_X_CONFIRMAR= "E_C_IND_SAL_X_CONFIRMAR";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllBandejaAtencionRequerimientos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSUBTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NRO_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CECO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOM_USU_SOL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_ESTADO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_NRO_SALIDA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_IND_SAL_X_CONFIRMAR, OracleTypes.CHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }
//ALQD,23/01/09. NUEVO PARAMETRO DE FILTRO
    public Map execute(String codSede, String codTipoReq, String codSubTipoReq, String nroReq, String fecInicio,
    		String fecFin, String codCeco,String nombreUsuarioSol, String codEstado, String nroGuia,
    		String indSalPorConfirmar) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_C_CODTIPOREQ, codTipoReq);
    	inputs.put(E_C_CODSUBTIPOREQ, codSubTipoReq);
    	inputs.put(E_C_NRO_REQ, nroReq);
    	inputs.put(E_C_FEC_INI, fecInicio);
    	inputs.put(E_C_FEC_FIN, fecFin);
    	inputs.put(E_C_CECO, codCeco);
    	inputs.put(E_V_NOM_USU_SOL, nombreUsuarioSol);
    	inputs.put(E_V_COD_ESTADO, codEstado);
    	inputs.put(E_C_NRO_SALIDA, nroGuia);
    	inputs.put(E_C_IND_SAL_X_CONFIRMAR, indSalPorConfirmar);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	SolRequerimiento solRequerimiento= new SolRequerimiento();
        
        	solRequerimiento.setIdRequerimiento(rs.getString("COD_REQ") == null ? "": rs.getString("COD_REQ"));
        	solRequerimiento.setNroRequerimiento(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
        	solRequerimiento.setFechaEmision(rs.getString("FECHA_REQ") == null ? "": rs.getString("FECHA_REQ"));
        	solRequerimiento.setDscCecoSolicitante(rs.getString("DSC_CENTRO_COSTO") == null ? "": rs.getString("DSC_CENTRO_COSTO"));
        	solRequerimiento.setUsuSolicitante(rs.getString("NMB_USUARIO_SOL") == null ? "Sin Orden": rs.getString("NMB_USUARIO_SOL"));
        	solRequerimiento.setTotalRequerimiento(rs.getString("MONTO") == null ? "": rs.getString("MONTO"));
        	solRequerimiento.setDscEstado(rs.getString("DSC_ESTADO") == null ? "Sin Orden": rs.getString("DSC_ESTADO"));
        	solRequerimiento.setIndiceCerrar(rs.getString("IND_CERRAR") == null ? "Sin Orden": rs.getString("IND_CERRAR")); 
        	return solRequerimiento;
        }
    }
}
