package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.modelo.NumeroSeries;
import com.tecsup.SGA.common.CommonConstants;

public class GetAllNumerosSeries extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_sol_requerimientos.sp_sel_activos_det_dev";
	
	private static final String E_C_COD_REQ = "E_C_COD_REQ";
	private static final String E_C_COD_REQ_DET = "E_C_COD_REQ_DET";
	private static final String E_C_COD_COD_DEV = "E_C_COD_COD_DEV";
	private static final String E_C_COD_COD_DEV_DET = "E_C_COD_COD_DEV_DET";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllNumerosSeries(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_REQ_DET, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_COD_DEV, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_COD_DEV_DET, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codReq,String codReqDet,String codDev,String codDevDet) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_REQ, codReq);
    	inputs.put(E_C_COD_REQ_DET, codReqDet);  
    	inputs.put(E_C_COD_COD_DEV, codDev);  
    	inputs.put(E_C_COD_COD_DEV_DET, codDevDet);  
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	NumeroSeries objCast= new NumeroSeries();       
        	objCast.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
        	objCast.setCodIngreso(rs.getString("COD_INGRESO") == null ? "": rs.getString("COD_INGRESO"));
        	objCast.setNroSerie(rs.getString("NRO_SERIE") == null ? "": rs.getString("NRO_SERIE"));
        	objCast.setIndSel(rs.getString("IND_SELECCIONADO") == null ? "": rs.getString("IND_SELECCIONADO"));
        	System.out.println("indice: "+objCast.getIndSel());
        	return objCast;
        }
    }
}
