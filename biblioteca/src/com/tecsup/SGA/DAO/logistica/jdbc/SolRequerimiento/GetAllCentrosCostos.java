package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CentrosCostos;

public class GetAllCentrosCostos extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_GENERAL + ".pkg_gen_comun.sp_sel_centros_costo_filtrado";
	
	private static final String V_C_SEDE = "V_C_SEDE"; 
	private static final String V_V_DESCRIPCION = "V_V_DESCRIPCION";
		private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllCentrosCostos(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(V_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(V_V_DESCRIPCION, OracleTypes.CHAR));
         declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codSede,String descripcion) {
    	
    	Map inputs = new HashMap();
    	
    	System.out.println("1.- "+codSede);
    	inputs.put(V_C_SEDE, codSede);
    	inputs.put(V_V_DESCRIPCION, descripcion);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CentrosCostos curso = new CentrosCostos();
        
        	curso.setSede(rs.getString("SEDE"));
        	curso.setCodSeco(rs.getString("CODIGO_CECO"));
        	curso.setDescripcion(rs.getString("DESCRIPCION"));
        	curso.setEstado(rs.getString("ESTADO"));
        
            return curso;
        }
    }
}
