package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
 * PROCEDURE SP_DEL_NOTA_SEG_X_REQ(
	E_V_COD_REQ IN VARCHAR,
	E_V_COD_NOTA IN VARCHAR,
	E_V_COD_USUARIO IN VARCHAR,
	S_V_RETVAL IN OUT VARCHAR2)  
 */
public class DeleteNotaSeguimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_del_nota_seg_x_req";

	private static final String E_V_COD_REQ = "E_V_COD_REQ"; 
	private static final String E_V_COD_NOTA = "E_V_COD_NOTA";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteNotaSeguimiento(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_COD_REQ, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_NOTA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codRequerimiento, String codNota, String codUsuario) {
    	
   	
    	Map inputs = new HashMap();    	
    	
    	inputs.put(E_V_COD_REQ, codRequerimiento);
    	inputs.put(E_V_COD_NOTA, codNota);
    	inputs.put(E_V_COD_USUARIO, codUsuario);
    	    	
        return super.execute(inputs);
    }
}