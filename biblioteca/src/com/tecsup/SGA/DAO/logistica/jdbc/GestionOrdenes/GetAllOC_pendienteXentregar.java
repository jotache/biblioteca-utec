package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllDetalleByOrden.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;

import com.tecsup.SGA.modelo.DetOrdenPendienteEntrega;
import com.tecsup.SGA.modelo.DetalleAprobacion;
import com.tecsup.SGA.modelo.DetalleOrden;

public class GetAllOC_pendienteXentregar extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_oc_pendiente_entrega";
	
	private static final String E_C_CODCOMPRADOR = "E_C_CODCOMPRADOR";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllOC_pendienteXentregar(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCOMPRADOR, OracleTypes.CHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codComprador) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOMPRADOR, codComprador);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	DetOrdenPendienteEntrega detOCPenEntrega = new DetOrdenPendienteEntrega();
        
        	detOCPenEntrega.setNomComprador(rs.getString("NOMCOMPRADOR") == null ? "": rs.getString("NOMCOMPRADOR"));
        	detOCPenEntrega.setNumOrdCompra(rs.getString("ORDE_NRO_ORDEN") == null ? "": rs.getString("ORDE_NRO_ORDEN"));
        	detOCPenEntrega.setDesTipoPago(rs.getString("TIPOPAGO") == null ? "": rs.getString("TIPOPAGO"));
        	detOCPenEntrega.setFecAprobacion(rs.getString("ORDE_FEC_APROBADO") == null ? "": rs.getString("ORDE_FEC_APROBADO"));
        	detOCPenEntrega.setMoneda(rs.getString("MONEDA") == null ? "": rs.getString("MONEDA"));
        	detOCPenEntrega.setSubTotal(rs.getString("ORDE_SUB_TOTAL") == null ? "": rs.getString("ORDE_SUB_TOTAL"));
        	detOCPenEntrega.setTotIGV(rs.getString("ORDE_IGV") == null ? "": rs.getString("ORDE_IGV"));
        	detOCPenEntrega.setTotalOC(rs.getString("ORDE_TOTAL") == null ? "": rs.getString("ORDE_TOTAL"));
        	detOCPenEntrega.setNomProducto(rs.getString("BIEN_NOMBRE") == null ? "": rs.getString("BIEN_NOMBRE"));
        	detOCPenEntrega.setCanTotal(rs.getString("SCPR_CANTIDAD_TOTAL") == null ? "": rs.getString("SCPR_CANTIDAD_TOTAL"));
        	detOCPenEntrega.setCanSaldo(rs.getString("SCPR_CANTIDAD_SALDO") == null ? "": rs.getString("SCPR_CANTIDAD_SALDO"));
        	detOCPenEntrega.setNomProveedor(rs.getString("PROV_RAZON_SOCIAL") == null ? "": rs.getString("PROV_RAZON_SOCIAL"));
        	return detOCPenEntrega;
        }
    }

}
