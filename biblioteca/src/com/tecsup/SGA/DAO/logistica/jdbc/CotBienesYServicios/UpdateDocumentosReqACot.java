package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateDocumentosReqACot extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_cotizacion.sp_act_doc_req_to_cot";
	
	private static final String E_C_COD_COT = "E_C_COD_COT";
	private static final String E_C_COD_COT_DET = "E_C_COD_COT_DET";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateDocumentosReqACot(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_COT, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_COT_DET, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codCotizacion, String codCotizacionDetalle, String codUsuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_COT, codCotizacion);
	inputs.put(E_C_COD_COT_DET, codCotizacionDetalle);
	inputs.put(E_C_COD_USUARIO, codUsuario);
		
	return super.execute(inputs);
	
	}
}
