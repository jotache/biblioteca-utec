package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_ACT_ENVIAR_DEV_BIENES(
	E_C_COD_DEV IN CHAR,
	E_C_COD_USUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */

public class EnviarSolDevolucionUsuario extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_sol_requerimientos.sp_act_enviar_dev_bienes";
		
		private static final String E_C_COD_DEV = "E_C_COD_DEV";
		private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";				
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public EnviarSolDevolucionUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_DEV, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));				
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codDevolucion, String codUsuario) {
		
		Map inputs = new HashMap();
		System.out.println("1.-"+codDevolucion+"<<");
		System.out.println("2.-"+codUsuario+"<<");				
		
		inputs.put(E_C_COD_DEV, codDevolucion);
		inputs.put(E_C_COD_USUARIO, codUsuario);		
		
		return super.execute(inputs);
		
	}	
}
