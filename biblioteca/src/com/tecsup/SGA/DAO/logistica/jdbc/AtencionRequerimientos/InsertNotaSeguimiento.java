package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertNotaSeguimiento extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ". pkg_log_atencion_req.sp_ins_nota_seg_x_req";
	private static final String E_V_COD_REQ = "E_V_COD_REQ";
	private static final String E_V_DES_NOTA = "E_V_DES_NOTA";
	private static final String E_C_COD_PERFIL = "E_C_COD_PERFIL";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertNotaSeguimiento(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_V_COD_REQ, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DES_NOTA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_PERFIL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codReq, String desNota, String codPerfil,String usuario) {
	
	Map inputs = new HashMap();
	System.out.println("llego a grabar");
	System.out.println("1.- "+codReq);
	System.out.println("2.- "+desNota);
	System.out.println("3.- "+codPerfil);
	System.out.println("4.- "+usuario);
	inputs.put(E_V_COD_REQ, codReq);
	inputs.put(E_V_DES_NOTA, desNota);
	inputs.put(E_C_COD_PERFIL, codPerfil);
	inputs.put(E_V_COD_USUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
