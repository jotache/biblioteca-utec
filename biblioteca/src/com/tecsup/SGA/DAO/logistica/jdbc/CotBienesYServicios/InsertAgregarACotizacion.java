package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertAgregarACotizacion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_ins_agregar_a_cotizacion";
	/*E_C_CODUSUARIO IN VARCHAR2,
E_C_CADENACODIGOS IN VARCHAR2,
E_C_CANTCODIGOS IN VARCHAR2,--'codreq|coddetreq$codreq|coddetrq$'
S_V_RETVAL IN OUT VARCHAR2)*/

	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO"; 
	private static final String E_C_CADENACODIGOS = "E_C_CADENACODIGOS"; 
	private static final String E_C_CANTCODIGOS = "E_C_CANTCODIGOS";
		
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertAgregarACotizacion(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CADENACODIGOS, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CANTCODIGOS, OracleTypes.VARCHAR));
				
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codUsuario, String cadenaCodigos, String cantCodigos) {
    	
    	System.out.println("codUsuario: "+codUsuario);
    	System.out.println("cadenaCodigos: "+cadenaCodigos);
    	System.out.println("cantCodigos: "+cantCodigos);
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	inputs.put(E_C_CADENACODIGOS, cadenaCodigos);
    	inputs.put(E_C_CANTCODIGOS, cantCodigos);
    	    	
        return super.execute(inputs);
    }
}
