package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllDetalleByOrden.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;

import com.tecsup.SGA.modelo.DetalleAprobacion;
import com.tecsup.SGA.modelo.DetalleOrden;

public class GetAllAprobadorByOrden extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_orden.sp_sel_aprob_orden_by_id";
	
	private static final String E_C_CODORDEN = "E_C_CODORDEN";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllAprobadorByOrden(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODORDEN, OracleTypes.CHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codOrden) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODORDEN, codOrden);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	DetalleAprobacion detalleAprobacion = new DetalleAprobacion();
        
        	detalleAprobacion.setCodUsuario(rs.getString("CODUSUARIO") == null ? "": rs.getString("CODUSUARIO"));
        	detalleAprobacion.setDscAprobador(rs.getString("APROBADOR") == null ? "": rs.getString("APROBADOR"));
        	detalleAprobacion.setFecAprobacion(rs.getString("FECAPROBACION") == null ? "": rs.getString("FECAPROBACION"));
        	//ALQD,19/02/09.LEYENDO NUEVA PROPIEDAD
        	detalleAprobacion.setObsEstado(rs.getString("OBSESTADO") == null ? "": rs.getString("OBSESTADO"));
        	return detalleAprobacion;
        }
    }

}
