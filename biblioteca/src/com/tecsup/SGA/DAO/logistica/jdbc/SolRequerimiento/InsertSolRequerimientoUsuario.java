package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertSolRequerimientoUsuario extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_sol_requerimientos.sp_ins_sol_req";
		
		private static final String E_C_CODREQ = "E_C_CODREQ";
		private static final String E_C_CECO = "E_C_CECO";
		private static final String E_C_SEDE = "E_C_SEDE";
		private static final String E_C_USU_SOL = "E_C_USU_SOL";
		private static final String E_C_TIPO_REQ = "E_C_TIPO_REQ";
		private static final String E_C_SUB_TIPO_REQ = "E_C_SUB_TIPO_REQ";
		private static final String E_C_COD_REQ_ORI = "E_C_COD_REQ_ORI";
		private static final String E_C_IND_INVERSION = "E_C_IND_INVERSION";
		private static final String E_C_IND_PARASTOCK= "E_C_IND_PARASTOCK";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public InsertSolRequerimientoUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CECO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_USU_SOL, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_TIPO_REQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_SUB_TIPO_REQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_COD_REQ_ORI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_IND_INVERSION, OracleTypes.CHAR));		
		declareParameter(new SqlParameter(E_C_IND_PARASTOCK, OracleTypes.CHAR));		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codRequerimiento, String codCentroCosto, String sede, 
			String usuarioSolicita, String tipoRequerimiento, String subTipoRequerimiento, String codRequerimientoOri,
			String indInversion, String indParaStock) {
		
		Map inputs = new HashMap();		
		
		inputs.put(E_C_CODREQ, codRequerimiento);
		inputs.put(E_C_CECO, codCentroCosto);
		inputs.put(E_C_SEDE, sede);
		inputs.put(E_C_USU_SOL, usuarioSolicita);
		inputs.put(E_C_TIPO_REQ, tipoRequerimiento);
		inputs.put(E_C_SUB_TIPO_REQ, subTipoRequerimiento);
		inputs.put(E_C_COD_REQ_ORI, codRequerimientoOri);
		inputs.put(E_C_IND_INVERSION, indInversion);
		inputs.put(E_C_IND_PARASTOCK, indParaStock);
		
		return super.execute(inputs);
		
	}	
}
