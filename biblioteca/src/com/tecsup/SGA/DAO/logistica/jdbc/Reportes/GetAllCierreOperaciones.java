package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllCierreOperaciones extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_cierre_operaciones";
/* E_C_SEDE IN CHAR,
  E_C_TIPO_REQ IN CHAR,
  E_C_TIPO_BIEN IN CHAR,
  E_C_TIPO_SERVICIO IN CHAR,
  E_C_FEC_INI IN CHAR,
  E_C_FEC_FIN IN CHAR,
  E_C_COD_CECO IN CHAR,
  E_C_TIPO_GASTO IN CHAR*/
	
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_REQ = "E_C_TIPO_REQ"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN";
	private static final String E_C_TIPO_SERVICIO = "E_C_TIPO_SERVICIO"; 
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_COD_CECO = "E_C_COD_CECO";
	private static final String E_C_TIPO_GASTO = "E_C_TIPO_GASTO";
	
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllCierreOperaciones(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_REQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_SERVICIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_CECO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_GASTO, OracleTypes.CHAR));
		
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoReq, String codTipoBien, String codTipoServicio,
			String fecInicio, String fecFin, String codCentroCosto, String codTipoGasto) {
	
	System.out.println("codSede: "+codSede);
	System.out.println("codTipoReq: "+codTipoReq);
	System.out.println("codTipoBien: "+codTipoBien);
	System.out.println("codTipoServicio: "+codTipoServicio);
	System.out.println("fecInicio: "+fecInicio);
	System.out.println("fecFin: "+fecFin);
	System.out.println("codCentroCosto: "+codCentroCosto);
	System.out.println("codTipoGasto: "+codTipoGasto);
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_REQ, codTipoReq);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_TIPO_SERVICIO, codTipoServicio);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_COD_CECO, codCentroCosto);
	inputs.put(E_C_TIPO_GASTO, codTipoGasto);
		
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
    //12
    reportesLogistica.setDscTipoReq(rs.getString("DSC_TIPO_REQ") == null ? "": rs.getString("DSC_TIPO_REQ"));
    reportesLogistica.setDscTipoBienServ(rs.getString("DSC_TIPO_BIEN_SERV") == null ? "": rs.getString("DSC_TIPO_BIEN_SERV"));
	reportesLogistica.setCodCeco(rs.getString("COD_CECO") == null ? "": rs.getString("COD_CECO"));
	reportesLogistica.setDscCeco(rs.getString("DSC_CECO") == null ? "": rs.getString("DSC_CECO"));
	reportesLogistica.setResponsableCeco(rs.getString("RESP_CECO") == null ? "": rs.getString("RESP_CECO"));
	reportesLogistica.setDscTipoGasto(rs.getString("DSC_TIPO_GASTO") == null ? "": rs.getString("DSC_TIPO_GASTO"));
	
	reportesLogistica.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
	reportesLogistica.setTotalCargado(rs.getString("TOTAL_CARGADO") == null ? "": rs.getString("TOTAL_CARGADO"));
	
	reportesLogistica.setCantReqAtendidos(rs.getString("CANT_REQ_ATENDIDOS") == null ? "": rs.getString("CANT_REQ_ATENDIDOS"));
	
	reportesLogistica.setCantReqPorCerrar(rs.getString("CANT_REQ_X_CERRAR") == null ? "": rs.getString("CANT_REQ_X_CERRAR"));
	reportesLogistica.setTipoCambio(rs.getString("TIPO_CAMBIO") == null ? "": rs.getString("TIPO_CAMBIO"));
	reportesLogistica.setImporteSoles(rs.getString("IMPORTE_SOLES") == null ? "": rs.getString("IMPORTE_SOLES"));
	
	reportesLogistica.setCantAtenInt(rs.getString("CANT_ATEN_INT") == null ? "": rs.getString("CANT_ATEN_INT"));
	
			
	return reportesLogistica;
	}
}

}
