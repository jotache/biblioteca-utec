package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;

public class GetAllCotizacionByProveedor extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_cond_x_coti_x_prov";
	/*E_C_CODCOTIZACION IN CHAR,
	E_C_CODPROVEEDOR IN CHAR*/
	private static final String E_C_CODCOTIZACION = "E_C_CODCOTIZACION";
	private static final String E_C_CODPROVEEDOR = "E_C_CODPROVEEDOR";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllCotizacionByProveedor(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCOTIZACION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODPROVEEDOR, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codCotizacion, String codProveedor) {    	
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOTIZACION, codCotizacion);
    	inputs.put(E_C_CODPROVEEDOR, codProveedor);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
        
        	cotizacionProveedor.setCodCoti(rs.getString("COD_COTIZACION") == null ? "": rs.getString("COD_COTIZACION"));
        	cotizacionProveedor.setCodCotiDet(rs.getString("COD_DETALLE") == null ? "": rs.getString("COD_DETALLE"));
        	cotizacionProveedor.setCodProv(rs.getString("COD_PROVEEDOR") == null ? "": rs.getString("COD_PROVEEDOR"));
        	cotizacionProveedor.setDetalleCondicion(rs.getString("COD_TIPOCOND") == null ? "": rs.getString("COD_TIPOCOND"));
        	cotizacionProveedor.setDescripcion(rs.getString("DESCRIPCION") == null ? "": rs.getString("DESCRIPCION"));
        	cotizacionProveedor.setDscDetalleCoti(rs.getString("DETALLE_COTI") == null ? "": rs.getString("DETALLE_COTI"));
        	cotizacionProveedor.setDscDetalleProv(rs.getString("DETALLE_PROV") == null ? "": rs.getString("DETALLE_PROV"));
        	cotizacionProveedor.setDscDetalleCotiFin(rs.getString("DETALLE_COTI_FIN") == null ? "": rs.getString("DETALLE_COTI_FIN"));
        	
        	return cotizacionProveedor;
        }
    }
}
