package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActualizarFechas extends StoredProcedure {

	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
			+ ".pkg_log_cotizacion.SP_ACT_COTI_VIGENCIA";

	private static final String E_C_CODCOTI = "E_C_CODCOTI"; 
	private static final String E_C_USUARIO = "E_C_USUARIO";
	private static final String E_C_FECINI = "E_C_FECINI";
	private static final String E_C_HORAINI = "E_C_HORAINI";
	private static final String E_C_FECFIN = "E_C_FECFIN";
	private static final String E_C_HORAFIN = "E_C_HORAFIN";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActualizarFechas(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_USUARIO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FECINI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_HORAINI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FECFIN, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_HORAFIN, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codCotizacion, String codUsuario,
			String fechaIni, String horaIni, String fechaFin, String horaFin) {
    	
	   	
    	Map inputs = new HashMap();
    	System.out.println("--------- pkg_log_cotizacion.SP_ACT_COTI_VIGENCIA ---------");
    	System.out.println("codCotizacion: "+codCotizacion);
    	System.out.println("codUsuario: "+codUsuario);
    	System.out.println("fechaIni: "+fechaIni);
    	System.out.println("horaIni: "+horaIni);
    	System.out.println("fechaFin: "+fechaFin);
    	System.out.println("horaFin: "+horaFin);
    	System.out.println("--------- FIN pkg_log_cotizacion.SP_ACT_COTI_VIGENCIA ---------");
    	inputs.put(E_C_CODCOTI, codCotizacion);
    	inputs.put(E_C_USUARIO, codUsuario);
    	inputs.put(E_C_FECINI, fechaIni);
    	inputs.put(E_C_HORAINI, horaIni);
    	inputs.put(E_C_FECFIN, fechaFin);
    	inputs.put(E_C_HORAFIN, horaFin);
    	    	
        return super.execute(inputs);
    }
}

