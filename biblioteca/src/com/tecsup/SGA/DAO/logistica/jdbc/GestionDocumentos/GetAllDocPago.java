package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DocPago;

public class GetAllDocPago extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_doc_pago.sp_sel_doc_pago";
	
	private static final String E_V_ORDE_ID = "E_V_ORDE_ID";
	private static final String E_V_DOCP_ID = "E_V_DOCP_ID";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDocPago(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_ORDE_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DOCP_ID, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }
             
    public Map execute(String ordenId,String docId) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_V_ORDE_ID, ordenId);
    	inputs.put(E_V_DOCP_ID, docId);
    	
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	DocPago curso = new DocPago();
        	
        	curso.setFactura(rs.getString("NRO_FACTURA")== null ? "" : rs.getString("NRO_FACTURA"));
        	curso.setFecEmiFactura(rs.getString("FEC_EMI_FACTURA")== null ? "" : rs.getString("FEC_EMI_FACTURA"));
        	curso.setFecVctoFactura(rs.getString("FEC_VCTO_FACTURA")== null ? "" : rs.getString("FEC_VCTO_FACTURA"));
        	curso.setNroGuia(rs.getString("NRO_GUIA")== null ? "" : rs.getString("NRO_GUIA"));
        	curso.setFecEmiGuia(rs.getString("FEC_EMI_GUIA")== null ? "" : rs.getString("FEC_EMI_GUIA"));
        	curso.setNroOrden(rs.getString("NRO_ORDEN")== null ? "" : rs.getString("NRO_ORDEN"));
        	curso.setFecEmiOrden(rs.getString("FEC_EMI_ORDEN")== null ? "" : rs.getString("FEC_EMI_ORDEN"));
        	curso.setCodMoneda(rs.getString("COD_MONEDA")== null ? "" : rs.getString("COD_MONEDA"));
        	curso.setSigMoneda(rs.getString("SIGNO_MONEDA")== null ? "" : rs.getString("SIGNO_MONEDA"));
        	curso.setMontoOrden(rs.getString("MONTO_ORDEN")== null ? "" : rs.getString("MONTO_ORDEN"));
        	curso.setFecGuiaRecep(rs.getString("FEC_RECEPCION")== null ? "" : rs.getString("FEC_RECEPCION"));
        	curso.setSubTotal(rs.getString("SUBTOTAL")== null ? "" : rs.getString("SUBTOTAL"));
        	curso.setIgv(rs.getString("IGV")== null ? "" : rs.getString("IGV"));
        	curso.setTotalFacturado(rs.getString("TOTAL_FACTURADO")== null ? "" : rs.getString("TOTAL_FACTURADO"));
        	//ALQD,13/11/08. SETEANDO LAS NUEVAS PROPIEDADES
        	curso.setNroInterno(rs.getString("NRO_INTERNO")== null ? "" : rs.getString("NRO_INTERNO"));
        	curso.setFecCierre(rs.getString("FEC_CIERRE")== null ? "" : rs.getString("FEC_CIERRE"));
        	curso.setEstIngreso(rs.getString("EST_INGRESO")== null ? "" : rs.getString("EST_INGRESO"));
        	curso.setNomProveedor(rs.getString("NOM_PROVEEDOR")== null ? "" : rs.getString("NOM_PROVEEDOR"));
        	//ALQD,12/06/09.LEYENDO NUEVA COLUMNA
        	curso.setIndImportacion(rs.getString("IND_IMPORTACION")== null ? "" : rs.getString("IND_IMPORTACION"));
        	//ALQD,01/07/09.LEYENDO NUEVA COLUMNA
        	curso.setImpOtros(rs.getString("IMPORTE_OTROS")== null ? "" : rs.getString("IMPORTE_OTROS"));
        	return curso;
        }
    }
}
