package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Almacen;
import com.tecsup.SGA.modelo.Devolucion;

public class GetDatosDevolucion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_datos_dev";
	
	private static final String E_C_COD_DEVOLUCION = "E_C_COD_DEVOLUCION";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetDatosDevolucion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_DEVOLUCION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codDevolucion) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_DEVOLUCION, codDevolucion);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Devolucion devolucion= new Devolucion();        	
        	
        	devolucion.setCodDevolucion(rs.getString("COD_DEVOLUCION") == null ? "": rs.getString("COD_DEVOLUCION"));
        	devolucion.setNroDevolucion(rs.getString("NRO_DEVOLUCION") == null ? "": rs.getString("NRO_DEVOLUCION"));
        	devolucion.setFechaDevolucion(rs.getString("FEC_DEVOLUCION") == null ? "": rs.getString("FEC_DEVOLUCION"));
        	devolucion.setDscCentroCosto(rs.getString("DSC_CECO") == null ? "": rs.getString("DSC_CECO"));
        	devolucion.setUsuSolicitante(rs.getString("USU_SOLICITANTE") == null ? "": rs.getString("USU_SOLICITANTE"));
        	devolucion.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
        	devolucion.setCodSubTipoRequerimiento(rs.getString("COD_SUB_TIPO_REQUERIMIENTO") == null ? "": rs.getString("COD_SUB_TIPO_REQUERIMIENTO"));
        	devolucion.setCodReqAsociado(rs.getString("COD_REQ_ASOCIADO") == null ? "": rs.getString("COD_REQ_ASOCIADO"));
        	devolucion.setNroReqAsociado(rs.getString("NRO_REQ_ASOCIADO") == null ? "": rs.getString("NRO_REQ_ASOCIADO"));
        	devolucion.setMotivoDevolucion(rs.getString("MOTIVO_DEVOLUCION") == null ? "": rs.getString("MOTIVO_DEVOLUCION"));
        	devolucion.setObservacionDevolucion(rs.getString("OBSERVACION_DEVOLUCION") == null ? "": rs.getString("OBSERVACION_DEVOLUCION"));
        	devolucion.setCodRequerimiento(rs.getString("COD_REQUERIMIENTO") == null ? "": rs.getString("COD_REQUERIMIENTO"));
        	devolucion.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	
        	return devolucion;
        }
    }
}
