package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetEstadoAlmacen extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_comun.sp_sel_estado_almacen";
	/*
	 * PROCEDURE SP_SEL_ESTADO_ALMACEN(
		E_C_SEDE IN CHAR,
		S_V_RETVAL IN OUT VARCHAR2);
	 */
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public GetEstadoAlmacen(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codSede) {
		Map inputs = new HashMap();
		
		inputs.put(E_C_SEDE, codSede);
		return super.execute(inputs);
	}
}
