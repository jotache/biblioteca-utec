package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public class GetAllAprobarDocumentosPago extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_doc_pago.sp_sel_doc_pagos";
	
		/*E_V_DOCP_NRO_DOCUMENTO IN VARCHAR,
E_V_DOCP_FEC_EMI_INI IN VARCHAR,�    
E_V_DOCP_FEC_EMI_FIN IN VARCHAR,
E_V_PROV_NRO_DOC IN VARCHAR,
E_V_PROV_RAZON_SOCIAL IN VARCHAR,
E_V_TIPT_ESTADO_DOCUMENTO IN VARCHAR,
E_V_ORDE_NRO_ORDEN IN VARCHAR,w 
E_V_ORDE_FEC_EMI_INI IN VARCHAR,
E_V_ORDE_FEC_EMI_FIN IN VARCHAR,*/
	
	private static final String E_V_DOCP_NRO_DOCUMENTO = "E_V_DOCP_NRO_DOCUMENTO";
	private static final String E_V_DOCP_FEC_EMI_INI = "E_V_DOCP_FEC_EMI_INI";
	private static final String E_V_DOCP_FEC_EMI_FIN = "E_V_DOCP_FEC_EMI_FIN";
	private static final String E_V_PROV_NRO_DOC = "E_V_PROV_NRO_DOC";
	private static final String E_V_PROV_RAZON_SOCIAL = "E_V_PROV_RAZON_SOCIAL";
	private static final String E_V_TIPT_ESTADO_DOCUMENTO = "E_V_TIPT_ESTADO_DOCUMENTO";
	private static final String E_V_ORDE_NRO_ORDEN = "E_V_ORDE_NRO_ORDEN";
	private static final String E_V_ORDE_FEC_EMI_INI = "E_V_ORDE_FEC_EMI_INI";
	private static final String E_V_ORDE_FEC_EMI_FIN = "E_V_ORDE_FEC_EMI_FIN";
	private static final String E_C_PERFIL = "E_C_PERFIL";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllAprobarDocumentosPago(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_DOCP_NRO_DOCUMENTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DOCP_FEC_EMI_INI, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DOCP_FEC_EMI_FIN, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_PROV_NRO_DOC, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_PROV_RAZON_SOCIAL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_TIPT_ESTADO_DOCUMENTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_ORDE_NRO_ORDEN, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_ORDE_FEC_EMI_INI, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_ORDE_FEC_EMI_FIN, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_PERFIL, OracleTypes.VARCHAR));                 
        declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String nroDocumento, String fecEmiIni1, String fecEmiIni2, String nroDocProveedor, 
    		 String razonSocialProveedor, String codTipoEstado, String nroOrden, String fecEmiFin1, 
    		 String fecEmiFin2,String perfil, String codSede, String codUsuario) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_DOCP_NRO_DOCUMENTO, nroDocumento);
    	inputs.put(E_V_DOCP_FEC_EMI_INI, fecEmiIni1);
    	inputs.put(E_V_DOCP_FEC_EMI_FIN, fecEmiIni2);
    	inputs.put(E_V_PROV_NRO_DOC, nroDocProveedor);
    	inputs.put(E_V_PROV_RAZON_SOCIAL, razonSocialProveedor);
    	inputs.put(E_V_TIPT_ESTADO_DOCUMENTO, codTipoEstado);
    	inputs.put(E_V_ORDE_NRO_ORDEN, nroOrden);
    	inputs.put(E_V_ORDE_FEC_EMI_INI, fecEmiFin1);
    	inputs.put(E_V_ORDE_FEC_EMI_FIN, fecEmiFin2);
    	inputs.put(E_C_PERFIL, perfil);
    	inputs.put(E_V_COD_USUARIO, codUsuario);
    	inputs.put(E_C_COD_SEDE, codSede);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	OrdenesAprobadas ordenesAprobadas= new OrdenesAprobadas();
        	
         	ordenesAprobadas.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
         	ordenesAprobadas.setCodDocumento(rs.getString("COD_DOCUMENTO") == null ? "": rs.getString("COD_DOCUMENTO"));
         	ordenesAprobadas.setNroDocumento(rs.getString("NRO_DOCUMENTO") == null ? "": rs.getString("NRO_DOCUMENTO"));
         	ordenesAprobadas.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
         	ordenesAprobadas.setCodProveedor(rs.getString("COD_PROVEEDOR") == null ? "": rs.getString("COD_PROVEEDOR"));
         	ordenesAprobadas.setDscProveedor(rs.getString("NOM_PROVEEDOR") == null ? "": rs.getString("NOM_PROVEEDOR"));
         	ordenesAprobadas.setCodMoneda(rs.getString("COD_MONEDA") == null ? "0": rs.getString("COD_MONEDA"));
         	ordenesAprobadas.setDscMoneda(rs.getString("SIGNO_MONEDA") == null ? "0": rs.getString("SIGNO_MONEDA"));
         	
         	ordenesAprobadas.setMonto(rs.getString("TOTAL_DOCUMENTO") == null ? "": rs.getString("TOTAL_DOCUMENTO"));
         	ordenesAprobadas.setFechaVencimiento(rs.getString("FEC_VENCIMIENTO") == null ? "": rs.getString("FEC_VENCIMIENTO"));
         	ordenesAprobadas.setCodEstado(rs.getString("COD_ESTADO_DOC") == null ? "": rs.getString("COD_ESTADO_DOC"));
         	ordenesAprobadas.setDscEstado(rs.getString("NOM_ESTADO_DOC") == null ? "": rs.getString("NOM_ESTADO_DOC"));
         	ordenesAprobadas.setDscDetalleEstado(rs.getString("NOM_DET_ESTADO") == null ? "": rs.getString("NOM_DET_ESTADO"));
         	ordenesAprobadas.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
         	
        	return ordenesAprobadas;
        }
    }
}
