package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DelSeriesActivos extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_almacen.sp_del_series_activo_by_docpag";
	
	private static final String E_C_COD_DOC_PAG = "E_C_COD_DOC_PAG";
	private static final String E_C_COD_BIEN = "E_C_COD_BIEN";
	private static final String E_C_COD_ING_BIEN = "E_C_COD_ING_BIEN";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DelSeriesActivos(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_COD_DOC_PAG, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_ING_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codDocPago, String codBien,String codIngBien,String codUsuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_DOC_PAG, codDocPago);
	inputs.put(E_C_COD_BIEN, codBien);
	inputs.put(E_C_COD_ING_BIEN, codIngBien);
	inputs.put(E_C_COD_USUARIO, codUsuario);
	return super.execute(inputs);
	
	}
}
