package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllAcumuladoSalidaByArticulo extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_acu_sal_articulo";
/* E_C_SEDE IN CHAR,
  E_C_TIPO_BIEN IN CHAR,
  E_C_FEC_INI IN CHAR,
  E_C_FEC_FIN IN CHAR,
  E_C_COD_FAMILIA IN CHAR,
  E_C_COD_BIEN IN CHAR,
  E_C_COD_CECO IN CHAR,
  E_C_TIPO_GASTO IN CHAR*/
	
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN"; 
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_COD_FAMILIA = "E_C_COD_FAMILIA"; 
	private static final String E_C_COD_BIEN = "E_C_COD_BIEN";
	private static final String E_C_COD_CECO = "E_C_COD_CECO";
	private static final String E_C_TIPO_GASTO = "E_C_TIPO_GASTO";
	
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllAcumuladoSalidaByArticulo(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_FAMILIA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_CECO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_GASTO, OracleTypes.CHAR));
		
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoBien, 
			String fecInicio, String fecFin, String codTipoFamilia, String codBien, 
			String codCentroCosto, String codTipoGasto) {
	
	System.out.println("codSede: "+codSede);
	System.out.println("codTipoBien: "+codTipoBien);
	System.out.println("fecInicio: "+fecInicio);
	System.out.println("fecFin: "+fecFin);
	System.out.println("codTipoFamilia: "+codTipoFamilia);
	System.out.println("codBien: "+codBien);
	System.out.println("codCentroCosto: "+codCentroCosto);
	System.out.println("codTipoGasto: "+codTipoGasto);
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_COD_FAMILIA, codTipoFamilia);
	inputs.put(E_C_COD_BIEN, codBien);
	inputs.put(E_C_COD_CECO, codCentroCosto);
	inputs.put(E_C_TIPO_GASTO, codTipoGasto);
		
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
    //15
    reportesLogistica.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
    reportesLogistica.setCodFamilia(rs.getString("COD_FAMILIA") == null ? "": rs.getString("COD_FAMILIA"));
	reportesLogistica.setDscFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
	
	reportesLogistica.setCodCeco(rs.getString("COD_CECO") == null ? "": rs.getString("COD_CECO"));
	reportesLogistica.setDscCeco(rs.getString("DSC_CECO") == null ? "": rs.getString("DSC_CECO"));
	
	reportesLogistica.setResponsableCeco(rs.getString("DSC_RESPONSABLE_CECO") == null ? "": rs.getString("DSC_RESPONSABLE_CECO"));
	
	reportesLogistica.setCodTipoGasto(rs.getString("COD_TIPO_GASTO") == null ? "": rs.getString("COD_TIPO_GASTO"));
	reportesLogistica.setDscTipoGasto(rs.getString("DSC_TIPO_GASTO") == null ? "": rs.getString("DSC_TIPO_GASTO"));
	reportesLogistica.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
	reportesLogistica.setTotalCargado(rs.getString("TOTAL_CARGADO") == null ? "": rs.getString("TOTAL_CARGADO"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	reportesLogistica.setCantConsumida(rs.getString("CANT_CONSUMIDA") == null ? "": rs.getString("CANT_CONSUMIDA"));
	reportesLogistica.setPrecioPromedio(rs.getString("PRECIO_PROMEDIO") == null ? "": rs.getString("PRECIO_PROMEDIO"));
				
	return reportesLogistica;
	}
}


}
