package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CatalogoProducto;

public class GetAllProducto extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
				+ ".pkg_log_mantto_config.sp_sel_catalogo";
	private static final String E_C_COD_CATAGO = "E_C_COD_CATAGO";
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE"; 
	private static final String E_V_COD_FAMILIA = "E_V_COD_FAMILIA"; 
	private static final String E_V_COD_SUB_FAMILIA = "E_V_COD_SUB_FAMILIA";
	private static final String E_V_CODIGO_PRODUCTO = "E_V_CODIGO_PRODUCTO"; 
	private static final String E_V_NOM_PRODUCTO = "E_V_NOM_PRODUCTO"; 
	private static final String E_V_COD_TIPO_BIEN = "E_V_COD_TIPO_BIEN";
	private static final String E_V_NRO_SERIE = "E_V_NRO_SERIE";
	private static final String E_V_DESCRIPTOR = "E_V_DESCRIPTOR";
	private static final String E_V_ESTADO = "E_V_ESTADO";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllProducto(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_CATAGO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_COD_FAMILIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_SUB_FAMILIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_CODIGO_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NOM_PRODUCTO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_COD_TIPO_BIEN, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_NRO_SERIE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DESCRIPTOR, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_ESTADO, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codCatago,String codSede, String codFam
    	,String codSubFam, String codPro, String nomPro, String codTipo
    	,String nroSerie, String descriptor,String estado) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_COD_CATAGO, codCatago);
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_V_COD_FAMILIA, codFam);
    	inputs.put(E_V_COD_SUB_FAMILIA, codSubFam);
    	inputs.put(E_V_CODIGO_PRODUCTO, codPro);
    	inputs.put(E_V_NOM_PRODUCTO, nomPro);
    	inputs.put(E_V_COD_TIPO_BIEN, codTipo);
    	inputs.put(E_V_NRO_SERIE, nroSerie);
    	inputs.put(E_V_DESCRIPTOR, descriptor);
    	inputs.put(E_V_ESTADO, estado);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	CatalogoProducto curso = new CatalogoProducto();
        	String des = "";
        	curso.setCodProducto(rs.getString("COD_PRODUCTO") == null ? "": rs.getString("COD_PRODUCTO"));
        	curso.setCodFamilia(rs.getString("COD_FAMILIA") == null ? "": rs.getString("COD_FAMILIA"));
        	curso.setFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
        	curso.setCodSubFamilia(rs.getString("COD_SUB_FAMILIA") == null ? "": rs.getString("COD_SUB_FAMILIA"));
        	curso.setSubFamilia(rs.getString("DSC_SUB_FAMILIA") == null ? "": rs.getString("DSC_SUB_FAMILIA"));
        	curso.setNomProducto(rs.getString("NOM_PRODUCTO") == null ? "": rs.getString("NOM_PRODUCTO"));
        	curso.setNomUnidad(rs.getString("NOM_UNIDAD") == null ? "": rs.getString("NOM_UNIDAD"));
        	curso.setStockActual(rs.getString("STOCK_ACTUAL") == null ? "": rs.getString("STOCK_ACTUAL"));
        	curso.setStockDisponible(rs.getString("STOCK_DIPONIBLE") == null ? "": rs.getString("STOCK_DIPONIBLE"));
        	curso.setPrecio(rs.getString("PRECIO") == null ? "": rs.getString("PRECIO"));
        	curso.setCondicion(rs.getString("CONDICION") == null ? "": rs.getString("CONDICION"));
        	des=rs.getString("DESCRIPCION") == null ? "": rs.getString("DESCRIPCION");
        	curso.setDescripcion(des.trim());
        	curso.setUnidadMedida(rs.getString("UNIDAD_MEDIDA") == null ? "": rs.getString("UNIDAD_MEDIDA"));
        	curso.setTipoBien(rs.getString("TIPO_BIEN") == null ? "": rs.getString("TIPO_BIEN"));
        	curso.setCodUbicacion(rs.getString("COD_UBICACION") == null ? "": rs.getString("COD_UBICACION"));
        	curso.setUbicacionFila(rs.getString("UBICACION_FILA") == null ? "": rs.getString("UBICACION_FILA"));
        	curso.setUbicacionColum(rs.getString("UBICACION_COLUM") == null ? "": rs.getString("UBICACION_COLUM"));
        	curso.setStockMin(rs.getString("STOCK_MINIMO") == null ? "": rs.getString("STOCK_MINIMO"));
        	curso.setStockMax(rs.getString("STOCK_MAXIMO") == null ? "": rs.getString("STOCK_MAXIMO"));
        	curso.setNroDiasAtencion(rs.getString("NRO_DIAS_ATENCION") == null ? "": rs.getString("NRO_DIAS_ATENCION"));
        	curso.setMarca(rs.getString("MARCA") == null ? "": rs.getString("MARCA"));
        	curso.setModelo(rs.getString("MODELO") == null ? "": rs.getString("MODELO"));
        	curso.setRutaImagen(rs.getString("RUTA_IMAGEN") == null ? "": rs.getString("RUTA_IMAGEN"));
        	curso.setPrecioReferencial(rs.getString("PRECIO_REFERENCIAL") == null ? "": rs.getString("PRECIO_REFERENCIAL"));
        	curso.setInversion(rs.getString("IND_INVERSION") == null ? "": rs.getString("IND_INVERSION"));
        	curso.setStockMostrar(rs.getString("STOCK_MOSTRAR") == null ? "": rs.getString("STOCK_MOSTRAR"));
        	curso.setEstado(rs.getString("ESTADO") == null ? "": rs.getString("ESTADO"));
        	//ALQD,23/02/09.NUEVA PROPIEDAD
        	curso.setArtPromocional(rs.getString("IND_PROMOCIONAL") == null ? "": rs.getString("IND_PROMOCIONAL"));
        	curso.setDesEstado(rs.getString("DESESTADO") == null ? "": rs.getString("DESESTADO"));
        	//ALQD,21/05/09.NUEVA PROPIEDAD
        	curso.setArtNoAfecto(rs.getString("ART_NOAFECTO") == null ? "": rs.getString("ART_NOAFECTO"));
        	return curso;
        }
    }
}
