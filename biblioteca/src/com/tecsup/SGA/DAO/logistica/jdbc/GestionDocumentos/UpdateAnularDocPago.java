package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateAnularDocPago extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_doc_pago.sp_act_anular_doc_pago";
	/*  E_V_COD_DOC_PAGO IN VARCHAR,
E_V_DOCP_USU_MODI IN VARCHAR*/
	
	private static final String E_V_COD_DOC_PAGO = "E_V_COD_DOC_PAGO";
	private static final String E_V_DOCP_USU_MODI = "E_V_DOCP_USU_MODI";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateAnularDocPago(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_V_COD_DOC_PAGO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DOCP_USU_MODI, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codDocumento,	String codUsuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_V_COD_DOC_PAGO, codDocumento);
	inputs.put(E_V_DOCP_USU_MODI, codUsuario);
	
	return super.execute(inputs);
	
	}
}
