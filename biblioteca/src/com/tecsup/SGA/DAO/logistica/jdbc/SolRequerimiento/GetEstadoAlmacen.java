package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetEstadoAlmacen extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + 
".pkg_log_comun.sp_sel_estado_almacen";
	/*E_C_SEDE IN CHAR*/
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String S_V_RETVAL = "S_V_RETVAL";
		
		public GetEstadoAlmacen(DataSource dataSource) {
			super(dataSource, SPROC_NAME);
			declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
			declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
			compile();
		}
			
		public Map execute(String codSede) {
			
			Map inputs = new HashMap();
			System.out.println("1.-"+codSede+"<<");
			
			inputs.put(E_C_SEDE, codSede);
			
			return super.execute(inputs);		
		}
}
