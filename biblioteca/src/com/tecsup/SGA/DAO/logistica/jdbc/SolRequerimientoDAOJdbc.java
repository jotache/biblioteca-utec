package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.DAO.logistica.SolRequerimientoDAO;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetFechasProcesosRequerimiento;
import com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento.*;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class SolRequerimientoDAOJdbc extends JdbcDaoSupport implements SolRequerimientoDAO {
	
	public List<SolRequerimiento> getAllSolReqUsuario(String codSede,
			String tipoReq, String nroReq, String fecIni, String fecFin,
			String cecoReq, String estadoReq, String usuSolicitante) {
		
		GetAllRequerimientosUsuario getAllRequerimientosUsuario = new GetAllRequerimientosUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllRequerimientosUsuario.execute(codSede, tipoReq, nroReq, fecIni, fecFin
				, cecoReq, estadoReq, usuSolicitante);
		if (!outputs.isEmpty())
		{
			return (List<SolRequerimiento>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	public List<SolRequerimiento> getSolReqUsuario(String codSolReq) {
		
		GetSolRequerimientoUsuario getSolRequerimientoUsuario = new GetSolRequerimientoUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)getSolRequerimientoUsuario.execute(codSolReq);
		
		if (!outputs.isEmpty()){
			return (List<SolRequerimiento>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}	
	public List<SolRequerimientoDetalle> getAllSolReqDetalle(String codSolReq, String codDetSolReq) {
		
		GetAllSolRequerimientoDetalle getAllSolRequerimientoDetalle = new GetAllSolRequerimientoDetalle(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSolRequerimientoDetalle.execute(codSolReq, codDetSolReq);
		
		if (!outputs.isEmpty())
		{
			return (List<SolRequerimientoDetalle>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	public String InsertSolRequerimientoUsuario(String codRequerimiento, String codCentroCosto, String sede, 
			String usuarioSolicita, String tipoRequerimiento, String subTipoRequerimiento, 
			String codRequerimientoOri, String indInversion, String indParaStock){
		
		InsertSolRequerimientoUsuario insertSolRequerimientoUsuario = new InsertSolRequerimientoUsuario(this.getDataSource());

		HashMap inputs = (HashMap)insertSolRequerimientoUsuario.execute(codRequerimiento, codCentroCosto, sede, usuarioSolicita, tipoRequerimiento, subTipoRequerimiento, codRequerimientoOri, indInversion, indParaStock);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public String DeleteSolRequerimientoDetalle(String codReq, String codDetSolReq, String codUsuario){
		
		DeleteSolRequerimientoDetalle deleteSolRequerimientoDetalle = new DeleteSolRequerimientoDetalle(this.getDataSource());
		
		HashMap inputs = (HashMap)deleteSolRequerimientoDetalle.execute(codReq, codDetSolReq, codUsuario);		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertSolRequerimientoDetalle(String codRequerimiento, String codDetSolReq, String nombre, 
			String descripcion, String codBien, String cantidad, String tipoGasto, String fecEntrega, String codUsuario){
		
		InsertSolRequerimientoDetalle insertSolRequerimientoDetalle = new InsertSolRequerimientoDetalle(this.getDataSource());		

		HashMap inputs = (HashMap)insertSolRequerimientoDetalle.execute(codRequerimiento, codDetSolReq, nombre, descripcion, codBien, cantidad, tipoGasto, fecEntrega, codUsuario);		
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
  
	public String InsertDocRelDetSolReqAdjunto(String codReque, String codDetalleReq,
			String codDocAdjunto, String rutaDoc, String codTipoAdjunto, String codUsuario)
	{
		InsertDocRelDetSolReqAdjunto InsertDocRelDetSolReqAdjunto = new InsertDocRelDetSolReqAdjunto(this.getDataSource());
			
			HashMap inputs = (HashMap)InsertDocRelDetSolReqAdjunto.execute(codReque, codDetalleReq,
					codDocAdjunto, rutaDoc, codTipoAdjunto, codUsuario);
			if (!inputs.isEmpty())
			{
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
	}
	
	public List<SolRequerimientoAdjunto> getAllRequerimientosAdjunto(
			String codReq, String CodDetReq, String CodAdj) {
		GetAllSolRequerimientoAdjunto getAllSolRequerimientoAdjunto 
						= new GetAllSolRequerimientoAdjunto(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSolRequerimientoAdjunto.execute(codReq, CodDetReq, CodAdj);
		
		if (!outputs.isEmpty())
		{
			return (List<SolRequerimientoAdjunto>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String deleteRequerimientosAdjunto(String codReq, String codDetReq,
			String codAdjunto, String codUsuario) {
		DeleteSolRequerimientoAdjunto deleteSolRequerimientoAdjunto = new DeleteSolRequerimientoAdjunto(this.getDataSource());
		HashMap inputs = (HashMap)deleteSolRequerimientoAdjunto.execute(codReq, codDetReq, codAdjunto, codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	//
	public List<SolRequerimiento> GetAllAproRequerimientoUsuario(String codSUsuario,
			String codPerfil, String tipoReq, String nroReq, String fechaInicio,
			String fechaFin, String codCentroCosto, String codEstado, String indGrupo
			//CCORDOVA - SEDE
    		, String codSede) {
		
		GetAllAproRequerimientoUsuario getAllAproRequerimientoUsuario = new GetAllAproRequerimientoUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllAproRequerimientoUsuario.execute(codSUsuario, codPerfil, tipoReq, 
				nroReq, fechaInicio, fechaFin, codCentroCosto, codEstado, indGrupo
				//CCORDOVA - SEDE
				, codSede);
		if (!outputs.isEmpty())
		{
			return (List<SolRequerimiento>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	
	public List GetAllAsigResp(String tipo, String sede, String ceco
    		,String nombre,String apPaterno,String apMaterno){
		GetAllAsigResp GetAllAsigResp = new GetAllAsigResp(this.getDataSource());
		HashMap outputs= (HashMap)GetAllAsigResp.execute(tipo, sede, ceco, nombre, apPaterno, apMaterno);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String InsertResponsable(String codRequerimiento, String codResp, String usuario,String codEsfuerzo){
		InsertResponsable InsertResponsable = new InsertResponsable(this.getDataSource()); 
		HashMap inputs = (HashMap)InsertResponsable.execute(codRequerimiento, codResp, usuario,codEsfuerzo);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllDetalle(String codResp){
		GetAllDetalle GetAllDetalle = new GetAllDetalle(this.getDataSource());
		HashMap outputs = (HashMap)GetAllDetalle.execute(codResp);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public String EnviarSolRequerimientoUsuario(String codRequerimiento, String codUsuario){
		
		EnviarSolRequerimientoUsuario enviarSolRequerimientoUsuario = new EnviarSolRequerimientoUsuario(this.getDataSource());

		HashMap inputs = (HashMap)enviarSolRequerimientoUsuario.execute(codRequerimiento, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	//ALQD,20/01/10.GUARDANDO TRES CAMPOS MAS: CCOSTO, INDINVERSION E INDPARASTOCK
	public String AprobarSolRequerimientoUsuario(String codRequerimiento
		,String codUsuario,String indicadorAccion,String cadenaCantidad
		,String codPerfil,String codCenCosto,String indInversion,String indParaStock){
		
		AprobarSolRequerimientoUsuario aprobarSolRequerimientoUsuario = new AprobarSolRequerimientoUsuario(this.getDataSource());		

		HashMap inputs = (HashMap)aprobarSolRequerimientoUsuario.execute(
			codRequerimiento,codUsuario,indicadorAccion,cadenaCantidad,codPerfil
			,codCenCosto,indInversion,indParaStock);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public String RechazarSolRequerimientoUsuario(String codRequerimiento, String codUsuario){
		
		RechazarSolRequerimientoUsuario rechazarSolRequerimientoUsuario = new RechazarSolRequerimientoUsuario(this.getDataSource());

		HashMap inputs = (HashMap)rechazarSolRequerimientoUsuario.execute(codRequerimiento, codUsuario);		
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	//
	public String DeleteSolRequerimiento(String codReq, String codUsuario) {
		DeleteSolRequerimiento deleteSolRequerimiento = new DeleteSolRequerimiento(this.getDataSource());
		HashMap inputs = (HashMap)deleteSolRequerimiento.execute(codReq, codUsuario);
		if (!inputs.isEmpty())
		{
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertResponsableTrabajo(String codRequerimiento, String codResp,String codEsf, String usuario){
		InsertResponsableTrabajo InsertResponsableTrabajo=new InsertResponsableTrabajo(this.getDataSource());
		HashMap inputs = (HashMap)InsertResponsableTrabajo.execute(codRequerimiento, codResp, codEsf, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertSolDevolucion(String codDevolucion, String codRequerimiento, 
			String cadenaCodigo, String cadenaCantidad, String cantidad, 
			String motivoDevolucion, String codUsuario){
		
		InsertSolDevolucion insertSolDevolucion = new InsertSolDevolucion(this.getDataSource());

		HashMap inputs = (HashMap)insertSolDevolucion.execute(codDevolucion, codRequerimiento, 
				cadenaCodigo, cadenaCantidad, cantidad, motivoDevolucion, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	
	public String EnviarSolDevolucionUsuario(String codDevolucion, String codUsuario){
		
		EnviarSolDevolucionUsuario enviarSolDevolucionUsuario = new EnviarSolDevolucionUsuario(this.getDataSource());		

		HashMap inputs = (HashMap)enviarSolDevolucionUsuario.execute(codDevolucion, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public String GetEstadoAlmacen(String codSede){
		
		GetEstadoAlmacen getEstadoAlmacen = new GetEstadoAlmacen(this.getDataSource());		

		HashMap inputs = (HashMap)getEstadoAlmacen.execute(codSede);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	
	public List GetAllCentrosCostos(String codSede,String descripcion){
		GetAllCentrosCostos GetAllCentrosCostos = new GetAllCentrosCostos(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllCentrosCostos.execute(codSede,descripcion);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
//ALQD,31/10/08. A�ADIENDO NUEVO METODO PARA OBTENER EL INDEMPLOGISTICA	
	public String GetIndEmpLogistica(String codUsuario){
		
		GetIndEmpLogistica getIndEmpLogistica = new GetIndEmpLogistica(this.getDataSource());		

		HashMap inputs = (HashMap)getIndEmpLogistica.execute(codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	//ALQD,27/01/09. NUEVA FUNCION, RECUPERA REQS PENDIENTE DE ENTREGA
	public List<ReportesLogistica> GetAllSolReqAprPenEntUsuario(String codUsuario){
		GetAllSolReqAprPenEntregaByUsuario GetAllSolReqAprPenEntUsuario = new GetAllSolReqAprPenEntregaByUsuario(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllSolReqAprPenEntUsuario.execute(codUsuario);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//ALQD,18/01/10. NUEVO METODO PARA CONSULTAR REQS APROBADOS
	public List<ReportesLogistica> GetAllSolReqAprobados(String codUsuario){
		GetAllSolReqAprobadoDetalle GetAllSolReqAprobados = new GetAllSolReqAprobadoDetalle(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllSolReqAprobados.execute(codUsuario);
		if(!outputs.isEmpty()){
			return (List<ReportesLogistica>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List<SolRequerimiento> getFlujoProcesosLogistica(String sede,
			String fecIni, String fecFin, String tipoInversion,
			String codUsuario, String nomComprador) {
		GetFechasProcesosRequerimiento getData = new GetFechasProcesosRequerimiento(this.getDataSource());
		HashMap outputs = (HashMap)getData.execute(sede, fecIni, fecFin, tipoInversion, codUsuario, nomComprador);
		if(!outputs.isEmpty()){
			return (List<SolRequerimiento>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
}
