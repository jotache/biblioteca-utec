package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllProgramaCronogramaPago extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_prog_pagos";
/* E_C_SEDE IN CHAR,
  E_C_FEC_INI IN CHAR,
  E_C_FEC_FIN IN CHAR,
  E_C_IND_VAL_VENC IN CHAR,
  E_C_COD_TIPO_ORDEN IN CHAR,
  E_C_COD_TIPO_PAGO IN CHAR,
  E_C_COD_EST_COMP IN CHAR,
  E_V_DSC_PROVEEDOR IN CHAR*/
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_IND_VAL_VENC = "E_C_IND_VAL_VENC";
	private static final String E_C_COD_TIPO_ORDEN = "E_C_COD_TIPO_ORDEN";
	private static final String E_C_COD_TIPO_PAGO = "E_C_COD_TIPO_PAGO";
	private static final String E_C_COD_EST_COMP = "E_C_COD_EST_COMP";
	private static final String E_V_DSC_PROVEEDOR = "E_V_DSC_PROVEEDOR";

	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllProgramaCronogramaPago(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_IND_VAL_VENC, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_TIPO_ORDEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_TIPO_PAGO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_EST_COMP, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_DSC_PROVEEDOR, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String fecInicio , String fecFin, String indiceValidado, 
			String codTipoOrden, String codTipoPago, String codEstadoComp , String dscProveedor) {
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_IND_VAL_VENC, indiceValidado);
	inputs.put(E_C_COD_TIPO_ORDEN, codTipoOrden);
	inputs.put(E_C_COD_TIPO_PAGO, codTipoPago);
	inputs.put(E_C_COD_EST_COMP, codEstadoComp);
	inputs.put(E_V_DSC_PROVEEDOR, dscProveedor);
		
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
	
	//14
	reportesLogistica.setDscTipoPeriodo(rs.getString("DSC_TIPO_PERIODO") == null ? "": rs.getString("DSC_TIPO_PERIODO"));
	reportesLogistica.setDscTipoOrden(rs.getString("DSC_TIPO_ORDEN") == null ? "": rs.getString("DSC_TIPO_ORDEN"));
	reportesLogistica.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
	reportesLogistica.setFechaAprobaReq(rs.getString("FEC_APROBACION") == null ? "": rs.getString("FEC_APROBACION"));
	reportesLogistica.setDscTipoPago(rs.getString("DSC_TIPO_PAGO") == null ? "": rs.getString("DSC_TIPO_PAGO"));
	reportesLogistica.setDscEstadoOrden(rs.getString("DSC_EST_ORDEN") == null ? "": rs.getString("DSC_EST_ORDEN"));
	reportesLogistica.setNroComprobante(rs.getString("NRO_COMPROBANTE") == null ? "": rs.getString("NRO_COMPROBANTE"));
	reportesLogistica.setRucProveedor(rs.getString("RUC_PROVEEDOR") == null ? "": rs.getString("RUC_PROVEEDOR"));
	reportesLogistica.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
	reportesLogistica.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
	reportesLogistica.setFechaRecepcion(rs.getString("FEC_RECEPCION") == null ? "": rs.getString("FEC_RECEPCION"));
	reportesLogistica.setFechaVencimiento(rs.getString("FEC_VENCIMIENTO") == null ? "": rs.getString("FEC_VENCIMIENTO"));
	reportesLogistica.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
	reportesLogistica.setDscImporte(rs.getString("DSC_IMPORTE") == null ? "": rs.getString("DSC_IMPORTE"));
	reportesLogistica.setDscEstDocPago(rs.getString("DSC_EST_CTA") == null ? "": rs.getString("DSC_EST_CTA"));
	return reportesLogistica;
	}
}

}
