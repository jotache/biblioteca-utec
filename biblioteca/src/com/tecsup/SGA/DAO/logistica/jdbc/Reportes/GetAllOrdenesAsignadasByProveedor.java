package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllOrdenesAsignadasByProveedor extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_ordenes_asig_prov";
/* E_C_SEDE IN CHAR,
  E_C_TIPO_REQ IN CHAR,
  E_C_TIPO_BIEN IN CHAR,
  E_C_TIPO_SERV IN CHAR,
  E_C_FEC_INI IN CHAR,
  E_C_FEC_FIN IN CHAR,
  E_C_IND_LOC_EXT IN CHAR,
  E_C_COD_PROVEEDOR IN CHAR,
  E_C_COD_MONEDA IN CHAR,
  E_C_COD_TIPO_PAGO IN CHAR*/
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_REQ = "E_C_TIPO_REQ"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN"; 
	private static final String E_C_TIPO_SERV = "E_C_TIPO_SERV";
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_IND_LOC_EXT = "E_C_IND_LOC_EXT";
	private static final String E_C_COD_PROVEEDOR = "E_C_COD_PROVEEDOR";
	private static final String E_C_COD_MONEDA = "E_C_COD_MONEDA";
	private static final String E_C_COD_TIPO_PAGO = "E_C_COD_TIPO_PAGO";

	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllOrdenesAsignadasByProveedor(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_REQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_SERV, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_IND_LOC_EXT, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_PROVEEDOR, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_MONEDA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_TIPO_PAGO, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoReq, String codTipoBien, String codTipoServicio, 
			String fecInicio , String fecFin, String indiceLocalidad, String codProveedor
			, String codTipoMoneda, String codTipoPago) {
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_REQ, codTipoReq);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_TIPO_SERV, codTipoServicio);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_IND_LOC_EXT, indiceLocalidad);
	inputs.put(E_C_COD_PROVEEDOR, codProveedor);
	inputs.put(E_C_COD_MONEDA, codTipoMoneda);
	inputs.put(E_C_COD_TIPO_PAGO, codTipoPago);
	
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
	//17
	reportesLogistica.setDscTipoOrden(rs.getString("DSC_TIPO_ORDEN") == null ? "": rs.getString("DSC_TIPO_ORDEN"));
	reportesLogistica.setDscTipoBienServ(rs.getString("DSC_TIPO_BIEN_SERV") == null ? "": rs.getString("DSC_TIPO_BIEN_SERV"));
	reportesLogistica.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
	reportesLogistica.setFechaAprobaReq(rs.getString("FEC_APROB") == null ? "": rs.getString("FEC_APROB"));
	reportesLogistica.setFechaCierre(rs.getString("FEC_CIERRE") == null ? "": rs.getString("FEC_CIERRE"));
	reportesLogistica.setRucProveedor(rs.getString("RUC_PROV") == null ? "": rs.getString("RUC_PROV"));
	reportesLogistica.setIndiceLocal(rs.getString("IND_LOCAL_EXT") == null ? "": rs.getString("IND_LOCAL_EXT"));
	reportesLogistica.setDscRazonSocial(rs.getString("DSC_RAZON_SOCIAL") == null ? "": rs.getString("DSC_RAZON_SOCIAL"));
	reportesLogistica.setDscBienServicio(rs.getString("DSC_BIEN_SERV") == null ? "": rs.getString("DSC_BIEN_SERV"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	reportesLogistica.setCantidad(rs.getString("CANTIDAD") == null ? "": rs.getString("CANTIDAD"));
	reportesLogistica.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
	reportesLogistica.setCostoUnitario(rs.getString("COSTO_UNITARIO") == null ? "": rs.getString("COSTO_UNITARIO"));
	reportesLogistica.setImporteBienServ(rs.getString("IMPORTE_BIEN_SERV") == null ? "": rs.getString("IMPORTE_BIEN_SERV"));
	reportesLogistica.setDscEstadoOC(rs.getString("DSC_EST_OC") == null ? "": rs.getString("DSC_EST_OC"));
	reportesLogistica.setDscCondicionFinal1(rs.getString("DSC_COND_FINAL_UNO") == null ? "": rs.getString("DSC_COND_FINAL_UNO"));
	reportesLogistica.setDscTipoPago(rs.getString("DSC_TIPO_PAGO") == null ? "": rs.getString("DSC_TIPO_PAGO"));
	
	return reportesLogistica;
	}
}

}
