package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public class GetAllDocPagosAsociados extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_doc_pago.sp_sel_doc_pago_asociados";
	
	//E_C_COD_ORDEN IN CHAR
	
	private static final String E_V_ORDE_ID = "E_V_ORDE_ID";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDocPagosAsociados(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_ORDE_ID, OracleTypes.CHAR));
                        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codOrden) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_ORDE_ID, codOrden);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	OrdenesAprobadas ordenesAprobadas= new OrdenesAprobadas();
        	
        	ordenesAprobadas.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
        	ordenesAprobadas.setCodDocumento(rs.getString("COD_DOC_PAGO") == null ? "": rs.getString("COD_DOC_PAGO"));
        	ordenesAprobadas.setNroDocumento(rs.getString("NRO_DOC") == null ? "": rs.getString("NRO_DOC"));
        	ordenesAprobadas.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	ordenesAprobadas.setFechaVencimiento(rs.getString("FEC_VENCIMIENTO") == null ? "": rs.getString("FEC_VENCIMIENTO"));
        	
        	ordenesAprobadas.setMonto(rs.getString("TOTAL_DOC_PAGO") == null ? "0": rs.getString("TOTAL_DOC_PAGO"));
        	
        	ordenesAprobadas.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	ordenesAprobadas.setDscEstado(rs.getString("NOM_ESTADO") == null ? "": rs.getString("NOM_ESTADO"));
        	ordenesAprobadas.setDscNroGuia(rs.getString("NRO_GUIA") == null ? "": rs.getString("NRO_GUIA"));
        	ordenesAprobadas.setDscDetalleEstado(rs.getString("DETALLE_ESTADO") == null ? "": rs.getString("DETALLE_ESTADO"));
        	
        	return ordenesAprobadas;
        }
    }
}
