package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetIndEmpLogistica extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + 
".pkg_log_comun.sp_sel_emplogistica";
	/*E_C_SEDE IN CHAR*/
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO"; 
	private static final String S_V_RETVAL = "S_V_RETVAL";
		
		public GetIndEmpLogistica(DataSource dataSource) {
			super(dataSource, SPROC_NAME);
			declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
			declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
			compile();
		}
			
		public Map execute(String codUsuario) {
			
			Map inputs = new HashMap();
			System.out.println("1.-"+codUsuario+"<<");
			
			inputs.put(E_C_CODUSUARIO, codUsuario);
			
			return super.execute(inputs);		
		}
}
