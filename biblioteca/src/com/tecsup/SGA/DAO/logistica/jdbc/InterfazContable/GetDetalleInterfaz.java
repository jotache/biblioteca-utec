package com.tecsup.SGA.DAO.logistica.jdbc.InterfazContable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.InterfazContable;
import com.tecsup.SGA.modelo.InterfazContableDetalle;

public class GetDetalleInterfaz extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_inter_contable.sp_sel_detalle_interfaz";
	/*E_C_COD_SEDE IN VARCHAR2,
E_C_MES IN VARCHAR2,
E_C_ANHO IN VARCHAR2,
E_C_TIPO_INTERFAZ VARCHAR2*/
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_C_MES = "E_C_MES";
	private static final String E_C_ANHO = "E_C_ANHO";
	private static final String E_C_TIPO_INTERFAZ = "E_C_TIPO_INTERFAZ";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetDetalleInterfaz(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_MES, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_ANHO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO_INTERFAZ, OracleTypes.VARCHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String codMes, String codAnio, String codTipoInterfaz) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_C_MES, codMes);
    	inputs.put(E_C_ANHO, codAnio);
    	inputs.put(E_C_TIPO_INTERFAZ, codTipoInterfaz);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	InterfazContableDetalle interfazContable= new InterfazContableDetalle();
        //48
        	interfazContable.setDsubdia(rs.getString("DSUBDIA") == null ? "": rs.getString("DSUBDIA"));
        	interfazContable.setDcompro(rs.getString("DCOMPRO") == null ? "": rs.getString("DCOMPRO"));
        	interfazContable.setDsecue(rs.getString("DSECUE") == null ? "": rs.getString("DSECUE"));
        	interfazContable.setDfeccom(rs.getString("DFECCOM") == null ? "": rs.getString("DFECCOM"));
        	interfazContable.setDcuenta(rs.getString("DCUENTA") == null ? "": rs.getString("DCUENTA"));
        	interfazContable.setDcodane(rs.getString("DCODANE") == null ? "": rs.getString("DCODANE"));
        	interfazContable.setDcencos(rs.getString("DCENCOS") == null ? "": rs.getString("DCENCOS"));
        	interfazContable.setDcodmon(rs.getString("DCODMON") == null ? "": rs.getString("DCODMON"));
        	interfazContable.setDdh(rs.getString("DDH") == null ? "": rs.getString("DDH"));
        	interfazContable.setDimport(rs.getString("DIMPORT") == null ? "": rs.getString("DIMPORT"));
        	interfazContable.setDtipdoc(rs.getString("DTIPDOC") == null ? "": rs.getString("DTIPDOC"));
        	interfazContable.setDnumdoc(rs.getString("DNUMDOC") == null ? "": rs.getString("DNUMDOC"));
        	interfazContable.setDfecdoc(rs.getString("DFECDOC") == null ? "": rs.getString("DFECDOC"));
        	interfazContable.setDfecven(rs.getString("DFECVEN") == null ? "": rs.getString("DFECVEN"));
        	interfazContable.setDarea(rs.getString("DAREA") == null ? "": rs.getString("DAREA"));
        	interfazContable.setDflag(rs.getString("DFLAG") == null ? "": rs.getString("DFLAG"));
        	interfazContable.setDdate(rs.getString("DDATE") == null ? "": rs.getString("DDATE"));
        	interfazContable.setDxglosa(rs.getString("DXGLOSA") == null ? "": rs.getString("DXGLOSA"));
        	interfazContable.setDusimpor(rs.getString("DUSIMPOR") == null ? "": rs.getString("DUSIMPOR"));
        	interfazContable.setDmnimpor(rs.getString("DMNIMPOR") == null ? "": rs.getString("DMNIMPOR"));
        	interfazContable.setDcodarc(rs.getString("DCODARC") == null ? "": rs.getString("DCODARC"));
        	interfazContable.setDfeccom2(rs.getString("DFECCOM2") == null ? "": rs.getString("DFECCOM2"));
        	interfazContable.setDfecdoc2(rs.getString("DFECDOC2") == null ? "": rs.getString("DFECDOC2"));
        	interfazContable.setDfecven2(rs.getString("DFECVEN2") == null ? "": rs.getString("DFECVEN2"));
        	interfazContable.setDcodane2(rs.getString("DCODANE2") == null ? "": rs.getString("DCODANE2"));
        	interfazContable.setDvanexo(rs.getString("DVANEXO") == null ? "": rs.getString("DVANEXO"));
        	interfazContable.setDvanexo2(rs.getString("DVANEXO2") == null ? "": rs.getString("DVANEXO2"));
        	interfazContable.setDtipcam(rs.getString("DTIPCAM") == null ? "": rs.getString("DTIPCAM"));
        	interfazContable.setDcantid(rs.getString("DCANTID") == null ? "": rs.getString("DCANTID"));
        	interfazContable.setDtipdor(rs.getString("DTIPDOR") == null ? "": rs.getString("DTIPDOR"));
        	interfazContable.setDnumdor(rs.getString("DNUMDOR") == null ? "": rs.getString("DNUMDOR"));
        	interfazContable.setDfecdo2(rs.getString("DFECDO2") == null ? "": rs.getString("DFECDO2"));
        	interfazContable.setDtiptas(rs.getString("DTIPTAS") == null ? "": rs.getString("DTIPTAS"));
        	
        	interfazContable.setDimptas(rs.getString("DIMPTAS") == null ? "": rs.getString("DIMPTAS"));
        	interfazContable.setDimpbmn(rs.getString("DIMPBMN") == null ? "": rs.getString("DIMPBMN"));
        	interfazContable.setDimpbus(rs.getString("DIMPBUS") == null ? "": rs.getString("DIMPBUS"));
        	interfazContable.setDinacom(rs.getString("DINACOM") == null ? "": rs.getString("DINACOM"));
        	interfazContable.setDigvcom(rs.getString("DIGVCOM") == null ? "": rs.getString("DIGVCOM"));
        	interfazContable.setDrete(rs.getString("DRETE") == null ? "": rs.getString("DRETE"));
        	interfazContable.setDporre(rs.getString("DPORRE") == null ? "": rs.getString("DPORRE"));
        	interfazContable.setDmedpag(rs.getString("DMEDPAG") == null ? "": rs.getString("DMEDPAG"));
        	interfazContable.setDmoncom(rs.getString("DMONCOM") == null ? "": rs.getString("DMONCOM"));
        	interfazContable.setDcolcom(rs.getString("DCOLCOM") == null ? "": rs.getString("DCOLCOM"));
        	interfazContable.setDbascom(rs.getString("DBASCOM") == null ? "": rs.getString("DBASCOM"));
        	interfazContable.setDtpconv(rs.getString("DTPCONV") == null ? "": rs.getString("DTPCONV"));
        	interfazContable.setDflgcom(rs.getString("DFLGCOM") == null ? "": rs.getString("DFLGCOM"));
        	interfazContable.setDtipaco(rs.getString("DTIPACO") == null ? "": rs.getString("DTIPACO"));
        	interfazContable.setDanecom(rs.getString("DANECOM") == null ? "": rs.getString("DANECOM"));
        	
        	        	
        	return interfazContable;
        }
    }

}
