package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.logistica.AlmacenDAO;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.ActAprobarInventario;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.ActEnviarInventario;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.ActRechazarInventario;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllBienesRecepcion;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllBienesDevolucion;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllDatosGuiaRemision;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllDatosOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllDetalleDevolucion;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllDetalleGuiaRemision;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllGuiaRemisionAsociadas;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllImpresionTickets;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllInventario;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetDatosDevolucion;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetEstadoAlmacen;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.InsertInventario;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.UpdateCerrarGuiaRemision;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.UpdateGuiaRemision;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.ActConfirmarDev;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.ActRechazarDev;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.ActAbrirAlmacen;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.ActCerrarAlmacen;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.InsertSeriesActivo;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.DelSeriesActivos;
import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllSeriesActivos;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos.UpdateAnularDocPago;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Almacen;
import com.tecsup.SGA.modelo.Devolucion;
import com.tecsup.SGA.modelo.DevolucionDetalle;
import com.tecsup.SGA.modelo.GuiaRemision;
import com.tecsup.SGA.modelo.Producto;

public class AlmacenDAOJdbc extends JdbcDaoSupport implements AlmacenDAO{
//ALQD,23/01/09. NUEVO PARAMETRO DE BUSQUEDA
	public List<Almacen> GetAllBienesRecepcion(String codSede, String nroOrden, String fecEmiOrdIni,
			String fecEmiOrdFin, 
   		 String nroRucProveedor, String dscProveedor, String dscComprador, String nroGuia, 
		 String fecEmiGuiaIni, String fecEmiGuiaFin, String indIngPorConfirmar){
		GetAllBienesRecepcion getAllBienesRecepcion = new GetAllBienesRecepcion(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllBienesRecepcion.execute(codSede, nroOrden, fecEmiOrdIni, 
			fecEmiOrdFin, nroRucProveedor, dscProveedor, dscComprador, nroGuia, fecEmiGuiaIni, 
			fecEmiGuiaFin, indIngPorConfirmar);
		if(!outputs.isEmpty()){
			return (List<Almacen>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List GetAllBienesDevolucion(String codSede, String fecDevIni, String fecDevFin, String codCeco, 
   		 String dsvUsu, String codEstado){
		GetAllBienesDevolucion GetAllBienesDevolucion = new GetAllBienesDevolucion(this.getDataSource());
		HashMap outputs = (HashMap)GetAllBienesDevolucion.execute(codSede, fecDevIni, fecDevFin, codCeco, dsvUsu, codEstado);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}
	
	//
	public List<Almacen> GetAllDatosOrden(String codOrden){
		GetAllDatosOrden getAllDatosOrden = new GetAllDatosOrden(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDatosOrden.execute(codOrden);
		if(!outputs.isEmpty()){
			return (List<Almacen>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public List<GuiaRemision> GetAllGuiaRemisionAsociadas(String codOrden){
		GetAllGuiaRemisionAsociadas getAllGuiaRemisionAsociadas = new GetAllGuiaRemisionAsociadas(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllGuiaRemisionAsociadas.execute(codOrden);
		if(!outputs.isEmpty()){
			return (List<GuiaRemision>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public List<GuiaRemision> GetAllDatosGuiaRemision(String codDocPago){
		GetAllDatosGuiaRemision getAllDatosGuiaRemision = new GetAllDatosGuiaRemision(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDatosGuiaRemision.execute(codDocPago);
		if(!outputs.isEmpty()){
			return (List<GuiaRemision>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public List<GuiaRemision> GetAllDetalleGuiaRemision(String codDocPago){
		GetAllDetalleGuiaRemision getAllDetalleGuiaRemision = new GetAllDetalleGuiaRemision(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetalleGuiaRemision.execute(codDocPago);
		if(!outputs.isEmpty()){
			return (List<GuiaRemision>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public String UpdateGuiaRemision(String codDocPago, String  cadCodDetDocPag,String cadCantidad, 
			String cantidad, String codUsuario){
		
		UpdateGuiaRemision updateGuiaRemision = new UpdateGuiaRemision(this.getDataSource());
		
		HashMap inputs = (HashMap)updateGuiaRemision.execute(codDocPago, cadCodDetDocPag, cadCantidad,
				cantidad, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	//
	public String UpdateCerrarGuiaRemision(String codDocPago, String codUsuario){
		
		UpdateCerrarGuiaRemision updateCerrarGuiaRemision = new UpdateCerrarGuiaRemision(this.getDataSource());
		
		HashMap inputs = (HashMap)updateCerrarGuiaRemision.execute(codDocPago, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String ActConfirmarDev(String codReqDev, String codUsuario,String observacion){
		ActConfirmarDev ActConfirmarDev = new ActConfirmarDev(this.getDataSource());
		HashMap inputs = (HashMap)ActConfirmarDev.execute(codReqDev, codUsuario, observacion);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String ActRechazarDev(String codReqDev, String codUsuario,String observacion){
		ActRechazarDev ActRechazarDev = new ActRechazarDev(this.getDataSource());
		HashMap inputs = (HashMap)ActRechazarDev.execute(codReqDev, codUsuario, observacion);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String ActAbrirAlmacen(String sede){
		ActAbrirAlmacen ActAbrirAlmacen = new ActAbrirAlmacen(this.getDataSource());
		HashMap inputs = (HashMap)ActAbrirAlmacen.execute(sede);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;
	}
	
	public String ActCerrarAlmacen(String sede){
		ActCerrarAlmacen ActCerrarAlmacen = new ActCerrarAlmacen(this.getDataSource());
		HashMap inputs = (HashMap)ActCerrarAlmacen.execute(sede);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;
	}
	
	public List<Devolucion> GetDatosDevolucion(String codDevolucion){
		
		GetDatosDevolucion getDatosDevolucion = new GetDatosDevolucion(this.getDataSource());		
		
		HashMap outputs = (HashMap)getDatosDevolucion.execute(codDevolucion);
		
		if(!outputs.isEmpty()){
			return (List<Devolucion>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	public List<DevolucionDetalle> GetAllDetalleDevolucion(String codDevolucion){
		
		GetAllDetalleDevolucion getAllDetalleDevolucion = new GetAllDetalleDevolucion(this.getDataSource());				
		
		HashMap outputs = (HashMap)getAllDetalleDevolucion.execute(codDevolucion);
		
		if(!outputs.isEmpty()){
			return (List<DevolucionDetalle>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String InsertSeriesActivo(String codDocPago, String codBien,String valorCompra,
			String cadCodIng,String cadNroSerie,String cantidad,String codUsuario,String codDetalle){
		InsertSeriesActivo InsertSeriesActivo = new InsertSeriesActivo(this.getDataSource());
		HashMap inputs = (HashMap)InsertSeriesActivo.execute(codDocPago, codBien, valorCompra, cadCodIng, cadNroSerie, cantidad, codUsuario,codDetalle);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;
	}
	
	
	public String DelSeriesActivos(String codDocPago, String codBien,String codIngBien,String codUsuario){
		DelSeriesActivos DelSeriesActivos = new DelSeriesActivos(this.getDataSource());
		HashMap inputs = (HashMap)DelSeriesActivos.execute(codDocPago, codBien, codIngBien, codUsuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllSeriesActivos(String codDocPago,String codBien,String codDetalle){
		GetAllSeriesActivos GetAllSeriesActivos = new GetAllSeriesActivos(this.getDataSource());
		HashMap output = (HashMap)GetAllSeriesActivos.execute(codDocPago, codBien,codDetalle);
		if(!output.isEmpty()){
		return (List)output.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllImpresionTickets(String codSede){
		
		GetAllImpresionTickets getAllImpresionTickets = new GetAllImpresionTickets(this.getDataSource());
		
		HashMap output = (HashMap)getAllImpresionTickets.execute(codSede);
		
		if(!output.isEmpty()){
			return (List)output.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List getAllInventario(String codSede, String respuesta){
		
		GetAllInventario getAllInventario = new GetAllInventario(this.getDataSource());
		
		HashMap output = (HashMap)getAllInventario.execute(codSede, respuesta);
		
		if(!output.isEmpty()){
			respuesta = (String)output.get("S_V_RETVAL");
			System.out.println(">>"+respuesta);
			if(respuesta.equals("-1")){
				Producto producto = new Producto();
				producto.setCodigo("-1");
				List lista = null;
				lista.set(0, producto);
				return lista;
			}
			else{
				return (List)output.get("S_C_RECORDSET");
			}
		}
		return null;
	}
	
	public String insertInventario(String codSede, String cadCodBienes, String cadCanBienes, 
			String cantidad, String codUsuario){
		
		InsertInventario insertInventario = new InsertInventario(this.getDataSource());
		
		HashMap inputs = (HashMap)insertInventario.execute(codSede, cadCodBienes, cadCanBienes, cantidad, codUsuario);		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	
	public String actEnviarInventario(String codSede, String codUsuario){
		
		ActEnviarInventario actEnviarInventario = new ActEnviarInventario(this.getDataSource());		
		
		HashMap inputs = (HashMap)actEnviarInventario.execute(codSede, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String actRechazarInventario(String codSede, String codUsuario){
		
		ActRechazarInventario actRechazarInventario = new ActRechazarInventario(this.getDataSource());				
		
		HashMap inputs = (HashMap)actRechazarInventario.execute(codSede, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String actAprobarInventario(String codSede, String codUsuario){
		
		ActAprobarInventario actAprobarInventario = new ActAprobarInventario(this.getDataSource());						
		
		HashMap inputs = (HashMap)actAprobarInventario.execute(codSede, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String getEstadoAlmacen(String codSede){
		
		GetEstadoAlmacen getEstadoAlmacen = new GetEstadoAlmacen (this.getDataSource());								
		
		HashMap inputs = (HashMap)getEstadoAlmacen.execute(codSede);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
}
