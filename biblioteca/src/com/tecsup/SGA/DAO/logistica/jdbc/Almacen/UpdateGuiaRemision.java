package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateGuiaRemision extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_almacen.sp_act_guia_rem";
	/*  E_C_COD_DOC_PAG IN CHAR,
E_V_CAD_COD_DET_DOC_PAG IN VARCHAR2,
E_V_CAD_CANT_ENT IN VARCHAR2,
E_C_CANTIDAD IN CHAR,
E_C_COD_USUARIO IN CHAR*/
	
	private static final String E_C_COD_DOC_PAG = "E_C_COD_DOC_PAG";
	private static final String E_V_CAD_COD_DET_DOC_PAG = "E_V_CAD_COD_DET_DOC_PAG";
	private static final String E_V_CAD_CANT_ENT = "E_V_CAD_CANT_ENT";
	private static final String E_C_CANTIDAD = "E_C_CANTIDAD";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateGuiaRemision(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_COD_DOC_PAG, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CAD_COD_DET_DOC_PAG, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_CANT_ENT, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CANTIDAD, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codDocPago, String  cadCodDetDocPag,String cadCantidad, String cantidad, 
			String codUsuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_DOC_PAG, codDocPago);
	inputs.put(E_V_CAD_COD_DET_DOC_PAG, cadCodDetDocPag);
	inputs.put(E_V_CAD_CANT_ENT, cadCantidad);
	inputs.put(E_C_CANTIDAD, cantidad);
	inputs.put(E_C_COD_USUARIO, codUsuario);
	
	return super.execute(inputs);
	
	}
}
