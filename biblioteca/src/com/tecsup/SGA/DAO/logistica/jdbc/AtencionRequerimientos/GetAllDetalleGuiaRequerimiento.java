package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
/*
 * PROCEDURE SP_SEL_DET_GUIA_REQ(
	E_C_CODGUIA IN CHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllDetalleGuiaRequerimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_det_guia_req";
	
	private static final String E_C_CODGUIA = "E_C_CODGUIA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetalleGuiaRequerimiento(DataSource dataSource) {
    	    	            	
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODGUIA, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
                     
    }

    public Map execute(String codGuia) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODGUIA, codGuia);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	double canEntr=0;
        	//float oto=0;
        	double oto=0;
        	double producto=0;
        	GuiaDetalle guiaDetalle= new GuiaDetalle();
        	/*
        	 * 	AS COD_REQ
   				AS COD_REQ_DET
   				AS COD_GUIA_DET
   				AS COD_BIEN
   				AS DSC_BIEN
   				AS CANT_SOL
   				AS PRECIO_UNITARIO
   				AS CANTIDAD_ENT
   				AS IND_ACT --En caso sea 1 se muestra el icono de agregar.
        	 */
        	guiaDetalle.setCodRequerimiento(rs.getString("COD_REQ") == null ? "": rs.getString("COD_REQ"));
        	guiaDetalle.setCodReqDetalle(rs.getString("COD_REQ_DET") == null ? "": rs.getString("COD_REQ_DET"));
        	guiaDetalle.setCodGuiaDetalle(rs.getString("COD_GUIA_DET") == null ? "": rs.getString("COD_GUIA_DET"));
        	guiaDetalle.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
        	guiaDetalle.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
        	guiaDetalle.setCantidadSolicitada(rs.getString("CANT_SOL") == null ? "": rs.getString("CANT_SOL"));
        	guiaDetalle.setPrecioUnitario(rs.getString("PRECIO_UNITARIO") == null ? "": rs.getString("PRECIO_UNITARIO"));
        	guiaDetalle.setCantidad(rs.getString("CANTIDAD_ENT") == null ? "": rs.getString("CANTIDAD_ENT"));        	        	        
        	guiaDetalle.setIndicador(rs.getString("IND_ACT") == null ? "": rs.getString("IND_ACT"));
        		//oto = Double.valueOf(guiaDetalle.getPrecioUnitario().trim());//  rs.getFloat(guiaDetalle.getPrecioUnitario());
        		oto = Double.valueOf(guiaDetalle.getPrecioUnitario());
        	    canEntr= Double.valueOf(guiaDetalle.getCantidad().trim());
        	    producto=canEntr*oto;
        	guiaDetalle.setTot(String.valueOf(producto));
        	guiaDetalle.setCantidadPendiente(rs.getString("CANT_PENDIENTE") == null ? "": rs.getString("CANT_PENDIENTE"));
        	guiaDetalle.setCantidadDisponible(rs.getString("CANT_DISPONIBLE") == null ? "": rs.getString("CANT_DISPONIBLE"));        	
        	//MODIFICADO RNAPA 22/08/2008
        	double canDis = Double.valueOf(guiaDetalle.getCantidadDisponible()).doubleValue();
        	double canSol = Double.valueOf(guiaDetalle.getCantidadSolicitada()).doubleValue();
        	double cantPent =  Double.valueOf(guiaDetalle.getCantidadPendiente());
        	//DecimalFormat df = new DecimalFormat("0.000");
        	
        	//if(canDis >= canSol){
        	if(canDis >= cantPent && cantPent>0){
        		//guiaDetalle.setCantidadSeleccionada(df.format(cantPent));
        		guiaDetalle.setCantidadSeleccionada(guiaDetalle.getCantidadPendiente());
        	} else{
        		if (canDis<cantPent && cantPent>0){
        			//guiaDetalle.setCantidadSeleccionada(df.format(canDis));
        			guiaDetalle.setCantidadSeleccionada(guiaDetalle.getCantidadDisponible());
        		}else
        			guiaDetalle.setCantidadSeleccionada("0");
        	}
        	
        	//jhpr 2008-09-25
        	guiaDetalle.setUnidadMedida(rs.getString("UNIDAD") == null ? "": rs.getString("UNIDAD"));
        	
        	//***************************
        	return guiaDetalle;
        }
    }
}
