package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SubFamilia;

public class GetAllSubFamilias extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_comun.sp_sel_subfamilias";
	
	private static final String E_V_BIEN = "E_V_BIEN"; 
	private static final String E_V_FAMILIA = "E_V_FAMILIA"; 
	private static final String E_C_TIPO = "E_C_TIPO";
	private static final String E_V_CODSUBFAMILIA = "E_V_CODSUBFAMILIA";
	private static final String E_V_DSCFAMILIA = "E_V_DSCFAMILIA";
	private static final String E_V_DSCSUBFAMILIA = "E_V_DSCSUBFAMILIA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllSubFamilias(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_BIEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_FAMILIA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_CODSUBFAMILIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DSCFAMILIA, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DSCSUBFAMILIA, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String bien, String familia, String tipo,String codSubFamilia,String dscFamilia,String dscSubFamilia) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_V_BIEN, bien);
    	inputs.put(E_V_FAMILIA, familia);
    	inputs.put(E_C_TIPO, tipo);
    	inputs.put(E_V_CODSUBFAMILIA, codSubFamilia);
    	inputs.put(E_V_DSCFAMILIA, dscFamilia);
    	inputs.put(E_V_DSCSUBFAMILIA, dscSubFamilia);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	SubFamilia curso = new SubFamilia();
        	
        	curso.setCodUnico(rs.getString("CODIGOUNICO")== null ? "" : rs.getString("CODIGOUNICO"));
        	curso.setCodGenerado(rs.getString("CODIGOGENERADO")== null ? "" : rs.getString("CODIGOGENERADO"));
        	curso.setDesSubFam(rs.getString("DESCSUBFAMILIA")== null ? "" : rs.getString("DESCSUBFAMILIA"));
        	curso.setCodBien(rs.getString("CODBIEN")== null ? "" : rs.getString("CODBIEN"));
        	curso.setDesBien(rs.getString("DESCBIEN")== null ? "" : rs.getString("DESCBIEN"));
        	curso.setCodFam(rs.getString("CODFAMILIA")== null ? "" : rs.getString("CODFAMILIA"));
        	curso.setDesFam(rs.getString("DESCFAMILIA")== null ? "" : rs.getString("DESCFAMILIA"));
            return curso;
        }
    }
}
