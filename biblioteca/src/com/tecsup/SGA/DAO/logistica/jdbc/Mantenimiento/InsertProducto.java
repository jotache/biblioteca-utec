package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
//ALQD,23/02/09.NUEVO PROPIEDAD
//ALQD,24/02/09.NUEVO PARAMETRO PARA ACTUALIZAR PRECIO REF.
//ALQD,20/05/09.NUEVA PROPIEDAD NO AFECTO
public class InsertProducto extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_mantto_config.sp_ins_bien";
	private static final String E_C_CODBIEN = "E_C_CODBIEN";
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_FAMILIA = "E_C_FAMILIA";
	private static final String E_C_SUBFAMILIA = "E_C_SUBFAMILIA";
	private static final String E_V_NPRODUCTO = "E_V_NPRODUCTO";
	private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
	private static final String E_C_UNIDAD = "E_C_UNIDAD";
	private static final String E_C_TIPOBIEN = "E_C_TIPOBIEN";
	private static final String E_C_UBICACION = "E_C_UBICACION";
	private static final String E_V_UBIFILA = "E_V_UBIFILA";
	private static final String E_V_UBICOLUM = "E_V_UBICOLUM";
	private static final String E_V_STOCKMIN = "E_V_STOCKMIN";
	private static final String E_V_STOCKMAX = "E_V_STOCKMAX";
	private static final String E_V_NRODIASATENCION = "E_V_NRODIASATENCION";
	private static final String E_V_MARCA = "E_V_MARCA";
	private static final String E_V_MODELO = "E_V_MODELO";
	private static final String E_V_USUARIO = "E_V_USUARIO";
	private static final String E_C_IND_INVERSION = "E_C_IND_INVERSION";
	private static final String E_V_STOCK_MOSTRAR = "E_V_STOCK_MOSTRAR";
	private static final String E_V_ESTADO = "E_V_ESTADO";
	private static final String E_C_IND_PROMOCIONAL = "E_C_IND_PROMOCIONAL";
	private static final String E_V_PREUNIREFERENCIAL = "E_V_PREUNIREFERENCIAL";
	private static final String E_C_NO_AFECTO = "E_C_NO_AFECTO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertProducto(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODBIEN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FAMILIA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_SUBFAMILIA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NPRODUCTO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_UNIDAD, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPOBIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_UBICACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_UBIFILA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_UBICOLUM, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_STOCKMIN, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_STOCKMAX, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_NRODIASATENCION, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_MARCA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_MODELO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_USUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_IND_INVERSION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_STOCK_MOSTRAR, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_ESTADO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_IND_PROMOCIONAL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_PREUNIREFERENCIAL, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_NO_AFECTO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	//ALQD,23/02/09.NUEVO PARAMETRO
	//ALQD,24/02/09.NUEVO PARAMETRO PARA ACTUALIZAR PRECIO REF.
	//ALQD,20/05/09.NUEVA PROPIEDAD NO AFECTO
	public Map execute(String codBien, String sede, String familia
	,String subFamilia, String nroProducto,String decrip,String unidad,String tipoBien,String ubicacion
	,String ubiFila,String ubiColum,String stockMin,String stockMax,String nroDiasAten,String marca,String modelo,String usuario,
	String inversion,String stockMostrar,String estado,String artPromocional,
	String preURef,String artNoAfecto) {
	
	Map inputs = new HashMap();
	System.out.println("------ INSERT ------");
	System.out.println("codBien: "+codBien);
	System.out.println("sede: "+sede);
	System.out.println("familia: "+familia);
	System.out.println("subFamilia: "+subFamilia);
	System.out.println("nroProducto: "+nroProducto);
	System.out.println("decrip: "+decrip);
	System.out.println("unidad: "+unidad);
	System.out.println("tipoBien: "+tipoBien);
	System.out.println("ubicacion: "+ubicacion);
	System.out.println("ubiFila: "+ubiFila);
	System.out.println("ubiColum: "+ubiColum);
	System.out.println("stockMin: "+stockMin);
	System.out.println("stockMax: "+stockMax);
	System.out.println("nroDiasAten: "+nroDiasAten);
	System.out.println("marca: "+marca);
	System.out.println("modelo: "+modelo);
	System.out.println("usuario: "+usuario);
	System.out.println("inversion: "+inversion);
	System.out.println("stockMostrar: "+stockMostrar);
	System.out.println("artPromocional: "+artPromocional);
	System.out.println("preURef: "+preURef);
	System.out.println("artNoAfecto: "+artNoAfecto);
	inputs.put(E_C_CODBIEN, codBien);
	inputs.put(E_C_SEDE, sede);
	inputs.put(E_C_FAMILIA, familia);
	inputs.put(E_C_SUBFAMILIA, subFamilia);
	inputs.put(E_V_NPRODUCTO, nroProducto);
	inputs.put(E_V_DESCRIPCION, decrip);
	inputs.put(E_C_UNIDAD, unidad);
	inputs.put(E_C_TIPOBIEN, tipoBien);
	inputs.put(E_C_UBICACION, ubicacion);
	inputs.put(E_V_UBIFILA, ubiFila);
	inputs.put(E_V_UBICOLUM, ubiColum);
	inputs.put(E_V_STOCKMIN, stockMin);
	inputs.put(E_V_STOCKMAX, stockMax);
	inputs.put(E_V_NRODIASATENCION, nroDiasAten);
	inputs.put(E_V_MARCA, marca);
	inputs.put(E_V_MODELO, modelo);
	inputs.put(E_V_USUARIO, usuario);
	inputs.put(E_C_IND_INVERSION, inversion);
	inputs.put(E_V_STOCK_MOSTRAR, stockMostrar);
	inputs.put(E_V_ESTADO, estado);
	inputs.put(E_C_IND_PROMOCIONAL, artPromocional);
	inputs.put(E_V_PREUNIREFERENCIAL, preURef);
	inputs.put(E_C_NO_AFECTO, artNoAfecto);
	
	return super.execute(inputs);
	
	}
}
