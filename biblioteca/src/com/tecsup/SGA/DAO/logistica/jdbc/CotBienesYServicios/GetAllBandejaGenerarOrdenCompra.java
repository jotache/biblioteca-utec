package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;

public class GetAllBandejaGenerarOrdenCompra extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_bndj_gen_oc";
	
	private static final String E_C_CODCOTI = "E_C_CODCOTI";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllBandejaGenerarOrdenCompra(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codCotizacion) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOTI, codCotizacion);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
        
        	cotizacionProveedor.setCodProv(rs.getString("COD_PROV") == null ? "": rs.getString("COD_PROV"));
        	cotizacionProveedor.setRazonSocial(rs.getString("RAZON_SOCIAL") == null ? "": rs.getString("RAZON_SOCIAL"));
        	cotizacionProveedor.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
        	cotizacionProveedor.setDscMonto(rs.getString("MONTO") == null ? "": rs.getString("MONTO"));
        	cotizacionProveedor.setDscNroOrden(rs.getString("NRO_ORDEN") == null ? "Sin Orden": rs.getString("NRO_ORDEN"));
        	cotizacionProveedor.setDscEstadoOrden(rs.getString("EST_ORDEN") == null ? "": rs.getString("EST_ORDEN"));
        	//ALQD,11/02/09.NUEVA PROPIEDAD
        	cotizacionProveedor.setTipGasOrden(rs.getString("TIPGAS_ORDEN") == null ? "": rs.getString("TIPGAS_ORDEN"));
        	//ALQD,25/05/09.AFECTO
        	cotizacionProveedor.setAfectoIGV(rs.getString("AFECTO_IGV") == null ? "": rs.getString("AFECTO_IGV"));
        	return cotizacionProveedor;
        }
    }
}
