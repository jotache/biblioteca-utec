package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
/*
 * E_C_CODPROV IN CHAR, --CODIGO DEL PROVEEDOR
E_C_CODSFAM IN CHAR, --CODIGO DE LA SUB FAMILIA
E_C_CODUSUARIO IN CHAR, --CODIGO DEL USUARIO
S_V_RETVAL IN OUT VARCHAR2);
 */
public class InsertSubFamiliaByPro extends StoredProcedure{
			private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_mantto_config.sp_ins_subfam_by_prov";
		private static final String E_C_CODPROV = "E_C_CODPROV";
		private static final String E_C_CODSFAM = "E_C_CODSFAM";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
		public InsertSubFamiliaByPro(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODPROV, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODSFAM, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
		
		public Map execute(String codPro, String codSFam,String usuario) {
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODPROV, codPro);
		inputs.put(E_C_CODSFAM, codSFam);
		inputs.put(E_C_CODUSUARIO, usuario);
		
		return super.execute(inputs);
		
		}
}
