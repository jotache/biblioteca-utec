package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllSalidaAlmacen extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllSalidaAlmacen.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_sal_almacen";
	
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN"; 
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_COD_CECO = "E_C_COD_CECO";
	private static final String E_C_COD_TIPO_GASTO = "E_C_COD_TIPO_GASTO";

	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllSalidaAlmacen(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_CECO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_TIPO_GASTO, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoBien, String fecInicio
	, String fecFin, String codCentroCosto, String codTipoGasto) {
	
		log.info("****** INI " + SPROC_NAME + " *****");
		log.info("E_C_SEDE: "+codSede);
		log.info("E_C_TIPO_BIEN: "+codTipoBien);
		log.info("E_C_FEC_INI: "+fecInicio);
		log.info("E_C_FEC_FIN: "+fecFin);
		log.info("E_C_COD_CECO: "+codCentroCosto);
		log.info("E_C_COD_TIPO_GASTO: "+codTipoGasto);
		log.info("****** Fin " + SPROC_NAME + " *****");
	
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_COD_CECO, codCentroCosto);
	inputs.put(E_C_COD_TIPO_GASTO, codTipoGasto);
	
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
	
	reportesLogistica.setNroGuia(rs.getString("NRO_GUIA_SALIDA") == null ? "": rs.getString("NRO_GUIA_SALIDA"));
	reportesLogistica.setFechaEntrega(rs.getString("FEC_ENTREGA") == null ? "": rs.getString("FEC_ENTREGA"));
	reportesLogistica.setDscCeco(rs.getString("DSC_CECO") == null ? "": rs.getString("DSC_CECO"));
	reportesLogistica.setDscTipoGasto(rs.getString("DSC_TIPO_GASTO") == null ? "": rs.getString("DSC_TIPO_GASTO"));
	reportesLogistica.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
	reportesLogistica.setDscFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	reportesLogistica.setCantidad(rs.getString("CANT") == null ? "": rs.getString("CANT"));
	reportesLogistica.setCostoUnitario(rs.getString("COSTO_UNITARIO") == null ? "": rs.getString("COSTO_UNITARIO"));
	reportesLogistica.setImporte(rs.getString("IMPORTE") == null ? "": rs.getString("IMPORTE"));
	reportesLogistica.setNroRequerimiento(rs.getString("NRO_REQUERIMIENTO") == null ? "": rs.getString("NRO_REQUERIMIENTO"));
	reportesLogistica.setFechaAprobaReq(rs.getString("FEC_APROB_REQ") == null ? "": rs.getString("FEC_APROB_REQ"));
	reportesLogistica.setCantidadAtender(rs.getString("CANT_ATENDER") == null ? "": rs.getString("CANT_ATENDER"));
	
	return reportesLogistica;
	}
}

}
