package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
//ALQD,20/04/09. NRO_GUIA Y FEC_GUIA SERAN DE ENTRADA Y SALIDA.
/*
 * PROCEDURE SP_INS_GUIA_MANT(
	E_C_COD_REQ IN CHAR,
	E_C_COD_REQ_DET IN CHAR,
	E_C_COD_GUIA IN CHAR,
	E_C_COD_GUIA_DET IN CHAR,
	E_C_NRO_GUIA IN OUT CHAR,
	E_C_FEC_GUIA IN OUR CHAR,
	E_V_OBSERVACION IN VARCHAR2,
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */
public class InsertGuiaMantenimiento extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_atencion_req.sp_ins_guia_mant";
		
	private static final String E_C_COD_REQ = "E_C_COD_REQ";
	private static final String E_C_COD_REQ_DET = "E_C_COD_REQ_DET";
	private static final String E_C_COD_GUIA = "E_C_COD_GUIA";
	private static final String E_C_COD_GUIA_DET = "E_C_COD_GUIA_DET";
	private static final String E_C_NRO_GUIA = "E_C_NRO_GUIA";
	private static final String E_C_FEC_GUIA = "E_C_FEC_GUIA";
	private static final String E_V_OBSERVACION = "E_V_OBSERVACION";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";		
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertGuiaMantenimiento(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_REQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_REQ_DET, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_GUIA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_GUIA_DET, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_NRO_GUIA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_FEC_GUIA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_OBSERVACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codRequerimiento, String codReqDetalle, 
			String codGuia, String codGuiaDetalle, String nroGuia, 
			String fechaGuia, String observacion, String codUsuario) {
		
		System.out.println("InsertGuiaRequerimiento");
		System.out.println("1>>"+codRequerimiento+"<<");
		System.out.println("2>>"+codReqDetalle+"<<");
		System.out.println("3>>"+codGuia+"<<");
		System.out.println("4>>"+codGuiaDetalle+"<<");
		System.out.println("5>>"+nroGuia+"<<");
		System.out.println("6>>"+fechaGuia+"<<");
		System.out.println("7>>"+observacion+"<<");
		System.out.println("8>>"+codUsuario+"<<");
		
		Map inputs = new HashMap();
		inputs.put(E_C_COD_REQ, codRequerimiento);
		inputs.put(E_C_COD_REQ_DET, codReqDetalle);
		inputs.put(E_C_COD_GUIA, codGuia);
		inputs.put(E_C_COD_GUIA_DET, codGuiaDetalle);
		inputs.put(E_C_NRO_GUIA, nroGuia);
		inputs.put(E_C_FEC_GUIA, fechaGuia);
		inputs.put(E_V_OBSERVACION, observacion);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		
		return super.execute(inputs);
	}
}
