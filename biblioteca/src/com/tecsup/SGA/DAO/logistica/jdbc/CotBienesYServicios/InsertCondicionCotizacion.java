package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertCondicionCotizacion extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_cotizacion.sp_ins_condicion_x_cotizacion";
		
	private static final String E_C_CODIGOID = "E_C_CODIGOID";
	private static final String E_C_CODCOTIZACION = "E_C_CODCOTIZACION";
	private static final String E_C_CODTIPOCONDICION = "E_C_CODTIPOCONDICION";
	private static final String E_V_DETALLEINICIAL = "E_V_DETALLEINICIAL";
	private static final String E_C_IND_SEL = "E_C_IND_SEL";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertCondicionCotizacion(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODIGOID, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODCOTIZACION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODTIPOCONDICION, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_DETALLEINICIAL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_IND_SEL, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codId, String codCotizacion, String codTipoCondicion,String detalleInicial,
			String indSel,String usuario) {
	
	Map inputs = new HashMap();
	inputs.put(E_C_CODIGOID, codId);
	inputs.put(E_C_CODCOTIZACION, codCotizacion);
	inputs.put(E_C_CODTIPOCONDICION, codTipoCondicion);
	inputs.put(E_V_DETALLEINICIAL, detalleInicial);
	inputs.put(E_C_IND_SEL, indSel);
	inputs.put(E_V_CODUSUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
