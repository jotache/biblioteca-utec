package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.CotizacionDetalle;
/*
 * 	PROCEDURE SP_SEL_BIENES_BY_PROVEEDOR(
	E_C_CODPROVEEDOR IN CHAR,
	E_C_CODCOTIZACION IN CHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllBienesByProveedor extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_bienes_by_proveedor";
	
	private static final String E_C_CODPROVEEDOR = "E_C_CODPROVEEDOR";
	private static final String E_C_CODCOTIZACION = "E_C_CODCOTIZACION";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllBienesByProveedor(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODPROVEEDOR, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCOTIZACION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codProveedor, String codCotizacion) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODPROVEEDOR, codProveedor);
    	inputs.put(E_C_CODCOTIZACION, codCotizacion);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionDetalle cotizacionDetalle = new CotizacionDetalle();
        	/*
				AS COD_COTI
				AS COD_COTI_DET
				AS DESCRIPCION
				AS CANTIDAD
				AS MONEDA
				AS PRECIO_UNITARIO
				//aumentado
				AS DSC_COTIZACION
       			AS DSC_PROVEEDOR
       			AS DSC_UNIDAD
        	 */
        	cotizacionDetalle.setCodigo(rs.getString("COD_COTI") == null ? "": rs.getString("COD_COTI"));        	
        	cotizacionDetalle.setCodBien(rs.getString("COD_COTI_DET") == null ? "": rs.getString("COD_COTI_DET"));        	
        	cotizacionDetalle.setProducto(rs.getString("DESCRIPCION") == null ? "": rs.getString("DESCRIPCION"));        	
        	cotizacionDetalle.setCantidad(rs.getString("CANTIDAD") == null ? "": rs.getString("CANTIDAD"));
        	cotizacionDetalle.setMoneda(rs.getString("MONEDA") == null ? "": rs.getString("MONEDA"));
        	cotizacionDetalle.setPrecioUnitario(rs.getString("PRECIO_UNITARIO") == null ? "": rs.getString("PRECIO_UNITARIO"));
        	cotizacionDetalle.setDescripcionCot(rs.getString("DSC_COTIZACION") == null ? "": rs.getString("DSC_COTIZACION"));
        	cotizacionDetalle.setDescripcionPro(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
        	cotizacionDetalle.setCodMoneda(rs.getString("COD_MONEDA") == null ? "": rs.getString("COD_MONEDA"));
        	cotizacionDetalle.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
        	//ALQD,16/06/09.LEYENDO NUEVA COLUMNA
        	cotizacionDetalle.setAfectoIGV(rs.getString("AFECTO_IGV") == null ? "": rs.getString("AFECTO_IGV"));
            return cotizacionDetalle;
        }
    }
}
