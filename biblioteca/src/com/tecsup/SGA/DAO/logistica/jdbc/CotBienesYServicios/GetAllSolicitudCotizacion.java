package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCondicionCotizacion.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Cotizacion;

public class GetAllSolicitudCotizacion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.SP_SEL_BNDJ_SOL_COTI";
	
	private static Log log = LogFactory.getLog(GetAllSolicitudCotizacion.class);
	
	/*E_C_CODSEDE IN CHAR,
	E_V_NRO_COTI IN VARCHAR2,
	E_C_FEC_INI IN CHAR,
	E_C_FEC_FIN IN CHAR,
	E_C_CODESTADO IN CHAR,
	E_C_CODTIPOREQ IN CHAR,
	E_C_CODSUBTIPOREQ IN CHAR,
	E_C_CODTIPOPAGO IN CHAR,
	E_V_RUCPROV IN VARCHAR2,
	E_V_DSCPROV IN VARCHAR2,
	--VARIABLES DEL TIPO DE USUARIO LOGEADO
	E_C_CODPERFIL IN CHAR,
	E_V_CODUSUARIO IN CHAR,*/
	
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String E_V_NRO_COTI = "E_V_NRO_COTI";
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_CODESTADO = "E_C_CODESTADO";
	private static final String E_C_CODTIPOREQ = "E_C_CODTIPOREQ";
	private static final String E_C_CODSUBTIPOREQ = "E_C_CODSUBTIPOREQ";
	private static final String E_C_CODTIPOPAGO = "E_C_CODTIPOPAGO";
	private static final String E_V_RUCPROV = "E_V_RUCPROV";
	private static final String E_V_DSCPROV = "E_V_DSCPROV";
	private static final String E_C_CODPERFIL = "E_C_CODPERFIL";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String E_C_CODGRUPO = "E_C_CODGRUPO";
	//ALQD,30/01/09. NUEVO PARAMETRO PARA FILTRAR COTs SIN OC
	private static final String E_C_INDCOTSINOC = "E_C_INDCOTSINOC";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
	
    
    public GetAllSolicitudCotizacion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NRO_COTI, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODESTADO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSUBTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODTIPOPAGO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_RUCPROV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_DSCPROV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODPERFIL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODGRUPO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_INDCOTSINOC, OracleTypes.CHAR));
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }
//ALQD,30/01/09. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA
    public Map execute(String codSede, String nroCotizacion, String fechaInicio, String fechaFin, 
    		String codEstado, String codTipoReq, String codSubTipoReq, String codTipoPago, String rucProveedor,
    		String dscProveedor, String codPerfil, String codUsuario, String codGrupoServ,
    		String indCotSinOC){
    	
    	Map inputs = new HashMap();
    	log.info(" Inicio: " + SPROC_NAME);
    	log.info("E_C_CODSEDE: "+codSede);
    	log.info("E_V_NRO_COTI: "+nroCotizacion);
    	log.info("E_C_FEC_INI: "+fechaInicio);
    	log.info("E_C_FEC_FIN: "+fechaFin);
    	log.info("E_C_CODESTADO: "+codEstado);
    	log.info("E_C_CODTIPOREQ: "+codTipoReq);
    	log.info("E_C_CODSUBTIPOREQ: "+codSubTipoReq);
    	log.info("E_C_CODTIPOPAGO: "+codTipoPago);
    	log.info("E_V_RUCPROV: "+rucProveedor);
    	log.info("E_V_DSCPROV: "+dscProveedor);
    	log.info("E_C_CODPERFIL: "+codPerfil);
    	log.info("E_V_CODUSUARIO: "+codUsuario);
    	log.info("E_C_CODGRUPO: "+codGrupoServ);
    	log.info("E_C_INDCOTSINOC: "+indCotSinOC);
    	log.info(" Fin: " + SPROC_NAME);
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_V_NRO_COTI, nroCotizacion);
    	inputs.put(E_C_FEC_INI, fechaInicio);
    	inputs.put(E_C_FEC_FIN, fechaFin);
    	inputs.put(E_C_CODESTADO, codEstado);
    	inputs.put(E_C_CODTIPOREQ, codTipoReq);
    	inputs.put(E_C_CODSUBTIPOREQ, codSubTipoReq);
    	inputs.put(E_C_CODTIPOPAGO, codTipoPago);
    	inputs.put(E_V_RUCPROV, rucProveedor);
    	inputs.put(E_V_DSCPROV, dscProveedor);
    	inputs.put(E_C_CODPERFIL, codPerfil);
    	inputs.put(E_V_CODUSUARIO, codUsuario);
    	inputs.put(E_C_CODGRUPO, codGrupoServ);
    	inputs.put(E_C_INDCOTSINOC, indCotSinOC);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Cotizacion cotizacion= new Cotizacion();
        
        	cotizacion.setCodigo(rs.getString("COD_COTI") == null ? "": rs.getString("COD_COTI"));
        	cotizacion.setNroCotizacion(rs.getString("NRO_COTI") == null ? "": rs.getString("NRO_COTI"));
        	cotizacion.setFechaEmision(rs.getString("FEC_COTI") == null ? "": rs.getString("FEC_COTI"));
        	cotizacion.setFechaInicio(rs.getString("FEC_INI") == null ? "": rs.getString("FEC_INI"));
        	cotizacion.setFechaFin(rs.getString("FEC_FIN") == null ? "": rs.getString("FEC_FIN"));
        	cotizacion.setDscTipoPago(rs.getString("DSC_TIPO_PAGO") == null ? "": rs.getString("DSC_TIPO_PAGO"));
        	cotizacion.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	cotizacion.setDscEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
        	cotizacion.setCodOrdenCompra(rs.getString("COD_ORDEN_COMP") == null ? "": rs.getString("COD_ORDEN_COMP"));
        	cotizacion.setNroOrdenCompra(rs.getString("NRO_ORDEN_COMP") == null ? "": rs.getString("NRO_ORDEN_COMP"));
        	cotizacion.setCodTipoPago(rs.getString("COD_TIPO_PAGO") == null ? "": rs.getString("COD_TIPO_PAGO"));       	
        	return cotizacion;
        }
    }
}
