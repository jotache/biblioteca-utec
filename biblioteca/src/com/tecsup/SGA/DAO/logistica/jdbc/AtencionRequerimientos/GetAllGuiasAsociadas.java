package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
/*
 * PROCEDURE SP_SEL_DET_GUIA_ASOCIADAS(
	E_C_CODREQ IN CHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetAllGuiasAsociadas extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_det_guia_asociadas";
	
	private static final String E_C_CODREQ = "E_C_CODREQ";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllGuiasAsociadas(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codRequerimiento) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODREQ, codRequerimiento);
     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Guia guia= new Guia();
        	/*
        	 * 	AS COD_GUIA
     			AS NRO_GUIA
     			AS FEC_EMISION
     			AS COD_ESTADO
     			AS DSC_ESTADO
        	 */
        	guia.setCodGuia(rs.getString("COD_GUIA") == null ? "": rs.getString("COD_GUIA"));
        	guia.setNroGuia(rs.getString("NRO_GUIA") == null ? "": rs.getString("NRO_GUIA"));
        	guia.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	guia.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	guia.setEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));        	
        	       	
        	return guia;
        }
    }
}
