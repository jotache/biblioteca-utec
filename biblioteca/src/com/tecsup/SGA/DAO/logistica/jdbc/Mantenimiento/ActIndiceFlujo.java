package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActIndiceFlujo extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ". pkg_log_mantto_config.sp_act_indices_detalle";
	
	private static final String E_C_COD_TABLA = "E_C_COD_TABLA";
	private static final String E_V_CAD_COD_DET = "E_V_CAD_COD_DET";
	private static final String E_C_CAD_IND_DET = "E_C_CAD_IND_DET";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActIndiceFlujo(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_TABLA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CAD_COD_DET, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_CAD_IND_DET, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codTabla, String codDetalle, String indicador) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_TABLA, codTabla);
	inputs.put(E_V_CAD_COD_DET, codDetalle);
	inputs.put(E_C_CAD_IND_DET, indicador);
	
	return super.execute(inputs);
	
	}
}
