package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DetalleDocPago;

public class GetAllDetDocPagosAsociados extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + 
	".pkg_log_doc_pago.sp_sel_det_doc_pago";
		//E_V_ORDE_ID IN VARCHAR
		private static final String E_V_ORDE_ID = "E_V_ORDE_ID"; 
		private static final String S_C_RECORDSET = "S_C_RECORDSET";
	    
	    public GetAllDetDocPagosAsociados(DataSource dataSource) {
	        super(dataSource, SPROC_NAME);
	        declareParameter(new SqlParameter(E_V_ORDE_ID, OracleTypes.VARCHAR));
	        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
	        compile();
	    }

	    public Map execute(String codOrden) {
	    	
	    	Map inputs = new HashMap();
	    	inputs.put(E_V_ORDE_ID, codOrden);
	        return super.execute(inputs);
	    }
	    
	    final class CursosMapper implements RowMapper {
	        
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	DetalleDocPago curso = new DetalleDocPago();
	        	float producto=0;
	        	float precioUni=0;
	        	float cantidad=0;
	        	curso.setCodCotizacion(rs.getString("COD_COTIZACION")== null ? "" : rs.getString("COD_COTIZACION"));
	        	curso.setCodProveedor(rs.getString("COD_PROVEEDOR")== null ? "" : rs.getString("COD_PROVEEDOR"));
	        	curso.setCodDetCotProv(rs.getString("COD_DET_COT_PROV")== null ? "" : rs.getString("COD_DET_COT_PROV"));
	        	curso.setCodReq(rs.getString("COD_REQ")== null ? "" : rs.getString("COD_REQ"));
	        	curso.setCodReqDet(rs.getString("COD_REQ_DET")== null ? "" : rs.getString("COD_REQ_DET"));
	        	curso.setCodItem(rs.getString("COD_ITEM")== null ? "" : rs.getString("COD_ITEM"));
	        	curso.setNomItem(rs.getString("NOM_ITEM")== null ? "" : rs.getString("NOM_ITEM"));
	        	curso.setCantidad(rs.getString("CANT_SOLICITADA")== null ? "" : rs.getString("CANT_SOLICITADA"));
	        	curso.setPreUni(rs.getString("PRECIO_UNI_FINAL")== null ? "" : rs.getString("PRECIO_UNI_FINAL"));
	        	
	        	//jhpr 2008-09-25
	        	curso.setUnidadMedida(rs.getString("UNIDAD")== null ? "" : rs.getString("UNIDAD"));
	        	
	        	precioUni=Float.valueOf(curso.getPreUni().trim());
	         	cantidad=Float.valueOf(curso.getCantidad().trim());
	         	producto=precioUni*cantidad;
	         	curso.setDscSubTotal(String.valueOf(producto));	        	
	         	return curso;
	        }
	    }
}
