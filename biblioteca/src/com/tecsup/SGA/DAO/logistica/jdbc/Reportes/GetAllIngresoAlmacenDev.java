package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.ReporteIngresoAlmacenDevBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllIngresoAlmacenDev extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.SP_SEL_ING_ALMACEN_DEV";
/*
 *   E_C_SEDE IN CHAR,
  E_C_TIPO_BIEN IN CHAR,
  E_C_FEC_INI IN CHAR,
  E_C_FEC_FIN IN CHAR,
  E_C_COD_FAM IN CHAR,
  S_C_RECORDSET OUT CUR_RECORDSET)
 * */
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN"; 
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_COD_FAM = "E_C_COD_FAM";
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllIngresoAlmacenDev(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_FAM, OracleTypes.CHAR));
	
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoBien, String fecInicio
	, String fecFin, String codTipoFamilia) {
	
	System.out.println("codSede: "+codSede);
	System.out.println("codTipoBien: "+codTipoBien);
	System.out.println("fecInicio: "+fecInicio);
	System.out.println("fecFin: "+fecFin);
	System.out.println("codTipoFamilia: "+codTipoFamilia);
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_COD_FAM, codTipoFamilia);
	
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ReporteIngresoAlmacenDevBean reportesLogistica = new ReporteIngresoAlmacenDevBean();
	
	reportesLogistica.setNroDevolucion(rs.getString("NRO_DEVOLUCION") == null ? "": rs.getString("NRO_DEVOLUCION"));
	reportesLogistica.setFecDevolucion(rs.getString("FEC_DEVOLUCION") == null ? "": rs.getString("FEC_DEVOLUCION"));
	reportesLogistica.setNroGuiaSalida(rs.getString("NRO_GUIA_SALIDA") == null ? "": rs.getString("NRO_GUIA_SALIDA"));
	reportesLogistica.setFecGuiaSalida(rs.getString("FEC_GUIA_SALIDA") == null ? "": rs.getString("FEC_GUIA_SALIDA"));
	reportesLogistica.setUsuSolicitante(rs.getString("USU_SOLICITANTE") == null ? "": rs.getString("USU_SOLICITANTE"));
	reportesLogistica.setDscTipoBien(rs.getString("DSC_TIPO_BIEN") == null ? "": rs.getString("DSC_TIPO_BIEN"));
	reportesLogistica.setDscFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));	
	reportesLogistica.setCant(rs.getString("CANT") == null ? "": rs.getString("CANT"));
	reportesLogistica.setCostoUnitario(rs.getString("COSTO_UNITARIO") == null ? "": rs.getString("COSTO_UNITARIO"));
	reportesLogistica.setImporte(rs.getString("IMPORTE") == null ? "": rs.getString("IMPORTE"));
	reportesLogistica.setCantxEntregar(rs.getString("CANT_X_ENTREGAR") == null ? "": rs.getString("CANT_X_ENTREGAR"));
	reportesLogistica.setDscEstadoOrden(rs.getString("DSC_EST_ORDEN") == null ? "": rs.getString("DSC_EST_ORDEN"));
	
	return reportesLogistica;
	}
}

}
