package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.DetalleOrden;

public class GetAllOrdenById extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_orden.sp_sel_orden_by_id";
	
	private static final String E_C_CODORDEN = "E_C_CODORDEN";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllOrdenById(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODORDEN, OracleTypes.CHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codOrden) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODORDEN, codOrden);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	DetalleOrden detalleOrden= new DetalleOrden();
        
        	detalleOrden.setDscNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	detalleOrden.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	detalleOrden.setDscNroDocumento(rs.getString("NRO_DOCUMENTO") == null ? "": rs.getString("NRO_DOCUMENTO"));
        	detalleOrden.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
        	detalleOrden.setDscAtencionA(rs.getString("ATENCION_A") == null ? "": rs.getString("ATENCION_A"));
        	detalleOrden.setDscComprador(rs.getString("COMPRADOR") == null ? "": rs.getString("COMPRADOR"));
        	detalleOrden.setDscMonto(rs.getString("MONTO") == null ? "": rs.getString("MONTO"));
        	detalleOrden.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	detalleOrden.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
        	detalleOrden.setSubTotal1(rs.getString("SUB_TOTAL") == null ? "": rs.getString("SUB_TOTAL"));
        	detalleOrden.setIgv(rs.getString("IGV") == null ? "": rs.getString("IGV"));
        	//ALQD,18/08/08. A�adiendo nuevas propiedades para invocar la consulta de las cotizaciones
        	detalleOrden.setCodCotizacion(rs.getString("SCOT_ID")==null ? "": rs.getString("SCOT_ID"));
        	detalleOrden.setCodEstCotizacion(rs.getString("CODESTADO_COT")==null ? "": rs.getString("CODESTADO_COT"));
        	detalleOrden.setCodTipoReq(rs.getString("CODTIPOREQ")==null ? "": rs.getString("CODTIPOREQ"));
        	
        	detalleOrden.setIndImportacion(rs.getString("IND_IMPORTACION") == null ? "": rs.getString("IND_IMPORTACION"));
        	detalleOrden.setImporteOtros(rs.getString("IMPORTE_OTROS") == null ? "": rs.getString("IMPORTE_OTROS"));
        	
        	//jhpr 2008-09-25
        	detalleOrden.setDescEstadoOC(rs.getString("DESESTADO") == null ? "": rs.getString("DESESTADO"));        	
        	detalleOrden.setTipoDePago(rs.getString("TIPO_PAGO") == null ? "": rs.getString("TIPO_PAGO"));
        	//ALQD,28/01/09. LEYENDO NUEVA PROPIEDAD DE CENCOSTOS
        	detalleOrden.setCadCenCosto(rs.getString("COD_CENCOS") == null ? "": rs.getString("COD_CENCOS"));
        	//ALQD,05/02/09.LEYENDO NUMCOTIZACION
        	detalleOrden.setNumCotizacion(rs.getString("NUMCOTIZACION") == null ? "": rs.getString("NUMCOTIZACION"));
        	//ALQD,19/02/09.LEYENDO NUEVAS PROPIEDADES
        	detalleOrden.setIndInversion(rs.getString("IND_INVERSION") == null ? "": rs.getString("IND_INVERSION"));
        	detalleOrden.setCodOrdenTipoGasto(rs.getString("COD_TIPGASTO") == null ? "": rs.getString("COD_TIPGASTO"));
        	detalleOrden.setDesOrdenTipoGasto(rs.getString("DES_TIPGASTO") == null ? "": rs.getString("DES_TIPGASTO"));
        	//ALQD,24/02/09.LEYENDO NUEVA PROPIEDAD
        	detalleOrden.setDesTipPago(rs.getString("DES_TIPO_PAGO") == null ? "": rs.getString("DES_TIPO_PAGO"));
        	//ALQD,25/05/09.LEYENDO NUEVA PROPIEDAD
        	detalleOrden.setCodSede(rs.getString("SEDE") == null ? "": rs.getString("SEDE"));
        	//ALQD,07/07/09.LEYENDO DOS NUEVAS COLUMNAS
        	detalleOrden.setPais(rs.getString("PAIS") == null ? "": rs.getString("PAIS"));
        	detalleOrden.setTelefono(rs.getString("TELEFONO") == null ? "": rs.getString("TELEFONO"));
        	//ALQD,08/07/09.LEYENDO DOS NUEVAS COLUMNAS
        	detalleOrden.setEmailProv(rs.getString("EMAIL_PROV") == null ? "": rs.getString("EMAIL_PROV"));
        	detalleOrden.setEmailComprador(rs.getString("EMAIL_COMPRADOR") == null ? "": rs.getString("EMAIL_COMPRADOR"));
        	detalleOrden.setFechaAprobacion(rs.getString("FEC_APROBACION") == null ? "": rs.getString("FEC_APROBACION"));
        	return detalleOrden;
        }
    }
}
