package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteFamiliaByProveedor extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_mantto_config.sp_del_subfam_by_prov";
	private static final String E_C_COD_FAM_PROV = "E_C_COD_FAM_PROV";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteFamiliaByProveedor(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_FAM_PROV, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codFamProv, String usuario) {
	
	Map inputs = new HashMap();
	System.out.println("codFamProv: "+codFamProv);
	System.out.println("usuario: "+usuario);
	inputs.put(E_C_COD_FAM_PROV, codFamProv);
	inputs.put(E_C_COD_USUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
