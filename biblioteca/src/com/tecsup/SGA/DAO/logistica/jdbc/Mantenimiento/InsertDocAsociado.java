package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertDocAsociado extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ". pkg_log_doc_pago.sp_ins_doc_asociado";
	/*E_C_COD_DOCUMENTO IN CHAR,
E_C_COD_DOCUMENTO_PAGO IN CHAR,
E_V_NRO_DOCUMENTO IN VARCHAR,
E_C_FEC_DOCUMENTO IN CHAR,
E_V_MONTO_DOCUMENTO IN VARCHAR,
E_V_DSC_DOCUMENTO IN VARCHAR,
E_V_COD_USUARIO IN VARCHAR,
S_V_RETVAL IN OUT VARCHAR2);
	 * */
	//ALQD,11/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,30/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,21/07/09.AGREGANDO 3 NUEVOS PARAMETROS
	private static final String E_C_COD_DOCUMENTO = "E_C_COD_DOCUMENTO";
	private static final String E_C_COD_DOCUMENTO_PAGO = "E_C_COD_DOCUMENTO_PAGO";
	private static final String E_V_NRO_DOCUMENTO = "E_V_NRO_DOCUMENTO";
	private static final String E_C_FEC_DOCUMENTO = "E_C_FEC_DOCUMENTO"; 
	private static final String E_V_MONTO_DOCUMENTO = "E_V_MONTO_DOCUMENTO";
	private static final String E_V_DSC_DOCUMENTO = "E_V_DSC_DOCUMENTO";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String E_V_CODTIPMONEDA = "E_V_CODTIPMONEDA";
	private static final String E_V_TC_SOLES= "E_V_TC_SOLES";
	private static final String E_C_COD_PROVEEDOR= "E_C_COD_PROVEEDOR";
	private static final String E_V_NOM_PROVEEDOR= "E_V_NOM_PROVEEDOR";
	private static final String E_V_RUC_PROVEEDOR= "E_V_RUC_PROVEEDOR";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertDocAsociado(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_DOCUMENTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_DOCUMENTO_PAGO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NRO_DOCUMENTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_DOCUMENTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_MONTO_DOCUMENTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_DSC_DOCUMENTO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CODTIPMONEDA, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_TC_SOLES, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_PROVEEDOR, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_NOM_PROVEEDOR, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_RUC_PROVEEDOR, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codDocuemnto, String codDocPago
			, String nroDocumento,String fecDocuemnto
			, String montoDocumento,String dscDoc,String codUsuario
			, String codTipMoneda, String impTipCamSoles, String codProvee
			, String nomProvee, String rucProvee) {
	
	Map inputs = new HashMap();
	
	System.out.println("1.- "+codDocuemnto);
	System.out.println("2.- "+codDocPago);
	System.out.println("3.- "+nroDocumento);
	System.out.println("4.- "+fecDocuemnto);
	System.out.println("5.- "+montoDocumento);
	System.out.println("6.- "+dscDoc);
	System.out.println("7.- "+codUsuario);
	System.out.println("8.- "+codTipMoneda);
	System.out.println("9.- "+impTipCamSoles);
	System.out.println("10.- "+codProvee);
	System.out.println("11.- "+nomProvee);
	System.out.println("12.- "+rucProvee);
	
	inputs.put(E_C_COD_DOCUMENTO, codDocuemnto);
	inputs.put(E_C_COD_DOCUMENTO_PAGO, codDocPago);
	inputs.put(E_V_NRO_DOCUMENTO, nroDocumento);
	inputs.put(E_C_FEC_DOCUMENTO, fecDocuemnto);
	inputs.put(E_V_MONTO_DOCUMENTO, montoDocumento);
	inputs.put(E_V_DSC_DOCUMENTO, dscDoc);
	inputs.put(E_V_COD_USUARIO, codUsuario);
	inputs.put(E_V_CODTIPMONEDA, codTipMoneda);
	inputs.put(E_V_TC_SOLES, impTipCamSoles);
	inputs.put(E_C_COD_PROVEEDOR, codProvee);
	inputs.put(E_V_NOM_PROVEEDOR, nomProvee);
	inputs.put(E_V_RUC_PROVEEDOR, rucProvee);
	
	return super.execute(inputs);
	
	}
}
