package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
/*
 * E_C_CODBIEN IN CHAR, --CODIGO DEL BIEN
E_C_CODDESC IN CHAR, --CODIGO DEL DESCRIPTOR
E_C_CODUSUARIO IN CHAR, --CODIGO DEL USUARIO
S_V_RETVAL IN OUT VARCHAR2) AS 
 */
public class InsDescriptoresByBien extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_mantto_config.sp_ins_descriptores_by_bien";
	private static final String E_C_CODBIEN = "E_C_CODBIEN";
	private static final String E_C_CODDESC = "E_C_CODDESC";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsDescriptoresByBien(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODBIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODDESC, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codBien, String codDes,String usuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODBIEN, codBien);
	inputs.put(E_C_CODDESC, codDes);
	inputs.put(E_C_CODUSUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
