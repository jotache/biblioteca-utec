package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertDocRelItem extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.SP_INS_DOC_REL_COMP_X_ITEM";
	/*E_C_CODCOTIZACION IN CHAR,
	E_C_CODETCOTIZACION IN CHAR,
	E_C_TIPOADJUNTO IN CHAR,
	E_C_COD_PROV IN CHAR,
	E_V_NOMBREADJUNTO IN VARCHAR2,
	E_V_NOMBREGENERADO IN VARCHAR2,
	E_V_CODUSUARIO IN VARCHAR2,*/
	private static final String E_C_CODCOTIZACION = "E_C_CODCOTIZACION"; 
	private static final String E_C_CODETCOTIZACION = "E_C_CODETCOTIZACION"; 
	private static final String E_C_TIPOADJUNTO = "E_C_TIPOADJUNTO";
	private static final String E_C_COD_PROV = "E_C_COD_PROV";
	private static final String E_V_NOMBREADJUNTO = "E_V_NOMBREADJUNTO";
	private static final String E_V_NOMBREGENERADO = "E_V_NOMBREGENERADO";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertDocRelItem(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODCOTIZACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODETCOTIZACION, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_TIPOADJUNTO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_COD_PROV, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_NOMBREADJUNTO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_NOMBREGENERADO, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
	
	public Map execute(String codCotizacion, String codCotizacionDet, String codTipoAdjunto,
			String codProveedor, String nombreAdjunto, String nombreGenerado, String codUsuario) {
    	
   	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOTIZACION, codCotizacion);
    	inputs.put(E_C_CODETCOTIZACION, codCotizacionDet);
    	inputs.put(E_C_TIPOADJUNTO, codTipoAdjunto);
    	inputs.put(E_C_COD_PROV, codProveedor);
    	inputs.put(E_V_NOMBREADJUNTO, nombreAdjunto);
    	inputs.put(E_V_NOMBREGENERADO, nombreGenerado);
    	inputs.put(E_V_CODUSUARIO, codUsuario);
    	    	
        return super.execute(inputs);
    }
}
