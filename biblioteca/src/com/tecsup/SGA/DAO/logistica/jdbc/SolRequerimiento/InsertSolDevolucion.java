package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertSolDevolucion extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_sol_requerimientos.sp_ins_dev_bienes";
		
		private static final String E_C_COD_DEV = "E_C_COD_DEV";
		private static final String E_C_COD_REQ = "E_C_COD_REQ";
		private static final String E_V_CAD_COD_REQ_DET = "E_V_CAD_COD_REQ_DET";
		private static final String E_V_CAD_CANT_DEV = "E_V_CAD_CANT_DEV";
		private static final String E_C_CAN_DET = "E_C_CAN_DET";
		private static final String E_V_MOTIVO_DEV = "E_V_MOTIVO_DEV";
		private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public InsertSolDevolucion(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_DEV, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_COD_REQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_CAD_COD_REQ_DET, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_CAD_CANT_DEV, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CAN_DET, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_MOTIVO_DEV, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codDevolucion, String codRequerimiento, String cadenaCodigo, 
			String cadenaCantidad, String cantidad, String motivoDevolucion, String codUsuario) {
		
		Map inputs = new HashMap();
		System.out.println("1.-"+codDevolucion+"<<");
		System.out.println("2.-"+codRequerimiento+"<<");
		System.out.println("3.-"+cadenaCodigo+"<<");
		System.out.println("4.-"+cadenaCantidad+"<<");
		System.out.println("5.-"+cantidad+"<<");
		System.out.println("6.-"+motivoDevolucion+"<<");
		System.out.println("7.-"+codUsuario+"<<");
		
		inputs.put(E_C_COD_DEV, codDevolucion);
		inputs.put(E_C_COD_REQ, codRequerimiento);
		inputs.put(E_V_CAD_COD_REQ_DET, cadenaCodigo);
		inputs.put(E_V_CAD_CANT_DEV, cadenaCantidad);
		inputs.put(E_C_CAN_DET, cantidad);
		inputs.put(E_V_MOTIVO_DEV, motivoDevolucion);
		inputs.put(E_C_COD_USUARIO, codUsuario);		
		
		return super.execute(inputs);
		
	}	
}
