package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.bean.CotizacionDetalleBean;
import com.tecsup.SGA.bean.ProductoxCotizacionBean;
import com.tecsup.SGA.bean.ProveedorBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionAdjunto;

public class GetAllGrpProvBndjCotiComp extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".PKG_LOG_COTIZACION.sp_sel_grp_prov_bndj_coti_comp";
		
	private static final String E_C_CODCOTI = "E_C_CODCOTI"; 
	private static final String E_C_CADDET = "E_C_CADDET";  
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllGrpProvBndjCotiComp(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CADDET, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ObjMapper()));
        compile();
    }

    public Map execute(String codCotizacion, String codCadenaDet) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODCOTI, codCotizacion);
    	inputs.put(E_C_CADDET, codCadenaDet);
        return super.execute(inputs);
    }
    
    final class ObjMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ProveedorBean objCast = new ProveedorBean();
        	
        	objCast.setCodProv(rs.getString("COD_PROV") == null ? "": rs.getString("COD_PROV"));
        	objCast.setNroDoc(rs.getString("NRO_DOC") == null ? "": rs.getString("NRO_DOC"));
        	objCast.setRazonSocial(rs.getString("RAZON_SOCIAL") == null ? "": rs.getString("RAZON_SOCIAL"));        	
        	//ALQD,04/06/09.LEYENDO EL TIPO DE PROVEEDOR
        	objCast.setTipoProveedor(rs.getString("TIPO_PROVEEDOR") == null ? "": rs.getString("TIPO_PROVEEDOR"));
            return objCast;
        }
    }
}
