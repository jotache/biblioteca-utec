package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Proveedor;

public class GetAllProveedor extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_comun.sp_sel_proveedor";
	
	private static final String E_V_COD_PROV = "E_V_COD_PROV"; 
	private static final String E_V_RUC = "E_V_RUC"; 
	private static final String E_V_RAZON_SOCIAL = "E_V_RAZON_SOCIAL";
	private static final String E_C_TIPO_CALIFICACION = "E_C_TIPO_CALIFICACION";
	private static final String E_C_ESTADO = "E_C_ESTADO";
	private static final String E_C_TIPO_PROV = "E_C_TIPO_PROV";	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllProveedor(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_COD_PROV, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_RUC, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_RAZON_SOCIAL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO_CALIFICACION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_ESTADO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO_PROV, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codProv, String ruc, String raSoc
    		,String tipoCalif,String estado, String tipoProveedor) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_V_COD_PROV, codProv);
    	inputs.put(E_V_RUC, ruc);
    	inputs.put(E_V_RAZON_SOCIAL, raSoc);
    	inputs.put(E_C_TIPO_CALIFICACION, tipoCalif);
    	inputs.put(E_C_ESTADO, estado);
    	inputs.put(E_C_TIPO_PROV, tipoProveedor);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Proveedor curso = new Proveedor();
        	
        	curso.setCodUnico(rs.getString("CODIGOUNICO")== null ? "" : rs.getString("CODIGOUNICO"));
        	curso.setCodTipoCalificacion(rs.getString("COD_TIPO_CALIFICACION")== null ? "" : rs.getString("COD_TIPO_CALIFICACION"));
        	curso.setDescCalificacion(rs.getString("DESC_CALIFICACION")== null ? "" : rs.getString("DESC_CALIFICACION"));
        	curso.setCodTipoProveedor(rs.getString("COD_TIPO_PROVEEDOR")== null ? "" : rs.getString("COD_TIPO_PROVEEDOR"));
        	curso.setTipoDoc(rs.getString("TIPO_DOC")== null ? "" : rs.getString("TIPO_DOC"));
        	curso.setNroDco(rs.getString("NRO_DOC")== null ? "" : rs.getString("NRO_DOC"));
        	curso.setRazSoc(rs.getString("RAZON_SOCIAL")== null ? "" : rs.getString("RAZON_SOCIAL"));
        	curso.setRazSocCorto(rs.getString("RAZON_SOCIAL_CORTO")== null ? "" : rs.getString("RAZON_SOCIAL_CORTO"));
        	curso.setCorreo(rs.getString("CORREO")== null ? "" : rs.getString("CORREO"));
        	curso.setDirec(rs.getString("DIRECCION")== null ? "" : rs.getString("DIRECCION"));
        	curso.setDpto(rs.getString("COD_DPTO")== null ? "" : rs.getString("COD_DPTO"));
        	curso.setProv(rs.getString("COD_PROV")== null ? "" : rs.getString("COD_PROV"));
        	curso.setDtrito(rs.getString("COD_DSTO")== null ? "" : rs.getString("COD_DSTO"));
        	curso.setAtencionA(rs.getString("ATENCION_A")== null ? "" : rs.getString("ATENCION_A"));
        	curso.setCodTipGiro(rs.getString("COD_TIPO_GIRO")== null ? "" : rs.getString("COD_TIPO_GIRO"));
        	curso.setDescGiro(rs.getString("DESC_GIRO")== null ? "" : rs.getString("DESC_GIRO"));
        	curso.setCodEstado(rs.getString("COD_ESTADO")== null ? "" : rs.getString("COD_ESTADO"));
        	curso.setNomBanco(rs.getString("NOM_BANCO")== null ? "" : rs.getString("NOM_BANCO"));
        	curso.setTipCta(rs.getString("TIPO_CUENTA")== null ? "" : rs.getString("TIPO_CUENTA"));
        	curso.setNroCta(rs.getString("NRO_CUENTA")== null ? "" : rs.getString("NRO_CUENTA"));
        	curso.setCodTipoMoneda(rs.getString("COD_TIPO_MONEDA")== null ? "" : rs.getString("COD_TIPO_MONEDA"));
        	//ALQD,03/07/09.LEYENDO DOS NUEVAS COLUMNAS
        	curso.setPais(rs.getString("PAIS")== null ? "" : rs.getString("PAIS"));
        	curso.setTelefono(rs.getString("TELEFONO")== null ? "" : rs.getString("TELEFONO"));
            return curso;
        }
    }
}
