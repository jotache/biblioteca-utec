package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateDetProvBndjCotiComp extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_cotizacion.SP_ACT_DET_PROV_BNDJ_COTI_COMP";
		
	private static final String E_C_CODCOTI = "E_C_CODCOTI";
	private static final String E_V_CADCODDET = "E_V_CADCODDET";
	private static final String E_V_CADCODPROV = "E_V_CADCODPROV";
	private static final String E_V_CADPRECIOS = "E_V_CADPRECIOS";
	private static final String E_C_CANTIDAD = "E_C_CANTIDAD";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	
	public UpdateDetProvBndjCotiComp(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CADCODDET, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CADCODPROV, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CADPRECIOS, OracleTypes.VARCHAR));	
	declareParameter(new SqlParameter(E_C_CANTIDAD, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String txhCodCotizacion,
			String cadCodDet, String cadCodProv, String cadPrecios, String cantidad, String usuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_CODCOTI, txhCodCotizacion);
	inputs.put(E_V_CADCODDET, cadCodDet);
	inputs.put(E_V_CADCODPROV, cadCodProv);
	inputs.put(E_V_CADPRECIOS, cadPrecios);	
	inputs.put(E_C_CANTIDAD, cantidad);
	inputs.put(E_C_CODUSUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
