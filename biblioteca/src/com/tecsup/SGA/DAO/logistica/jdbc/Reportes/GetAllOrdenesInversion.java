/**
 * 
 */
package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.Reportes.GetAllKardezValorizado.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

/**
 * @author jpalomino
 *
 */
public class GetAllOrdenesInversion extends StoredProcedure {

	private static Log log = LogFactory.getLog(GetAllOrdenesInversion.class);
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		+ ".pkg_log_reportes.SP_SEL_OC_INVERSION";
	private static final String E_C_SEDE = "E_C_SEDE"; 	
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";	
	
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllOrdenesInversion(DataSource dataSource){
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));		
		declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));			
		
		declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
		compile();
	}
	
	
	public Map execute(String codSede, String fecInicio, String fecFin) {
			
		log.info(SPROC_NAME+" --- INI");
		log.info("E_C_SEDE:"+codSede);	
		log.info("E_C_FEC_INI:"+fecInicio);
		log.info("E_C_FEC_FIN:"+fecFin);				
		log.info(SPROC_NAME+" --- FIN");
			
		Map inputs = new HashMap();			
		inputs.put(E_C_SEDE, codSede);			
		inputs.put(E_C_FEC_INI, fecInicio);
		inputs.put(E_C_FEC_FIN, fecFin);					
			
		return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
		
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			
		ReportesLogistica reportesLogistica = new ReportesLogistica();		
		reportesLogistica.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
		reportesLogistica.setDscTipoOrden(rs.getString("TIPO_PAGO") == null ? "": rs.getString("TIPO_PAGO"));
		reportesLogistica.setNomSolicitante(rs.getString("NOMCOMPRADOR") == null ? "": rs.getString("NOMCOMPRADOR"));
		reportesLogistica.setCodCeco(rs.getString("CODCENCOS") == null ? "": rs.getString("CODCENCOS"));		
		reportesLogistica.setDscCeco(rs.getString("DESCENCOS") == null ? "": rs.getString("DESCENCOS"));
		reportesLogistica.setFecAtencion(rs.getString("FEC_APROB") == null ? "": rs.getString("FEC_APROB"));		
		reportesLogistica.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
		reportesLogistica.setCantidad(rs.getString("CANTIDAD") == null ? "": rs.getString("CANTIDAD"));
		reportesLogistica.setPrecioUnitario(rs.getString("P_UNIT") == null ? "": rs.getString("P_UNIT"));
		reportesLogistica.setDscMoneda(rs.getString("MONEDA") == null ? "": rs.getString("MONEDA"));
		reportesLogistica.setDscProveedor(rs.getString("RAZ_SOCIAL") == null ? "": rs.getString("RAZ_SOCIAL"));
				
		return reportesLogistica;
		}
	}
	
}
