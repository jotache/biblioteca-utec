package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento.GetAllProducto.CursosMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Producto;

public class GetAllDocCatalogo extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_mantto_config.sp_sel_documentos_catalogo";
	private static final String E_C_CODBIEN = "E_C_CODBIEN";
	private static final String E_C_CODADJ = "E_C_CODADJ"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
	
    public GetAllDocCatalogo(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODBIEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODADJ, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codBien,String codAdj) {
    	
    	Map inputs = new HashMap();
    	System.out.println("codBien: "+codBien);
    	inputs.put(E_C_CODBIEN, codBien);
    	inputs.put(E_C_CODADJ, codAdj);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Producto curso = new Producto();
        	System.out.println("data: "+rs.getString("DSC_TIPO_ADJUNTO"));
        	curso.setCodigo(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
        	curso.setCodTipoAdj(rs.getString("COD_TIPO_ADJUNTO")== null ? "" : rs.getString("COD_TIPO_ADJUNTO"));
        	curso.setDscTipAdj(rs.getString("DSC_TIPO_ADJUNTO")== null ? "" : rs.getString("DSC_TIPO_ADJUNTO"));
        	curso.setRutaAdj(rs.getString("RUTA_ADJUNTO")== null ? "" : rs.getString("RUTA_ADJUNTO"));
           System.out.println("llena: "+curso.getDscTipAdj());
        	return curso;
        }
    }
}
