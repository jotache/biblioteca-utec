package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SubFamilia;

public class GetAllAtencionSolicitudes extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_mantto_config.sp_sel_atencion_solicitudes";
	
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_C_TIPO_REQ = "E_C_TIPO_REQ"; 
	private static final String E_C_SUB_TIPO_REQ = "E_C_SUB_TIPO_REQ"; 
	private static final String E_V_COD_RESPONSABLE = "E_V_COD_RESPONSABLE";
	private static final String E_V_NOM_RESPONSABLE = "E_V_NOM_RESPONSABLE";
	private static final String E_C_FAMILIA = "E_C_FAMILIA";
	private static final String E_C_SUBFAMILIA = "E_C_SUBFAMILIA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllAtencionSolicitudes(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_SUB_TIPO_REQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_COD_RESPONSABLE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOM_RESPONSABLE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FAMILIA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_SUBFAMILIA, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codSede,String tipoReq, String subtipoReq, String codResponsable
    		,String nomResponsable,String familia,String subfamilia) {
    	
    	Map inputs = new HashMap();
    	System.out.println("0.-"+codSede);
    	System.out.println("1.-"+tipoReq);
    	System.out.println("2.-"+subtipoReq);
    	System.out.println("3.-"+codResponsable);
    	System.out.println("4.-"+nomResponsable);
    	System.out.println("5.-"+familia);
    	System.out.println("6.-"+subfamilia);
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_C_TIPO_REQ, tipoReq);
    	inputs.put(E_C_SUB_TIPO_REQ, subtipoReq);
    	inputs.put(E_V_COD_RESPONSABLE, codResponsable);
    	inputs.put(E_V_NOM_RESPONSABLE, nomResponsable);
    	inputs.put(E_C_FAMILIA, familia);
    	inputs.put(E_C_SUBFAMILIA, subfamilia);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	SubFamilia curso = new SubFamilia();
        	
        	curso.setCodUnico(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
        	curso.setCodGenerado(rs.getString("COD_PADRE")== null ? "" : rs.getString("COD_PADRE"));
        	curso.setDesFam(rs.getString("DSC_PADRE")== null ? "" : rs.getString("DSC_PADRE"));
        	curso.setDesBien(rs.getString("COD_HIJO")== null ? "" : rs.getString("COD_HIJO"));
        	curso.setDesSubFam(rs.getString("DSC_HIJO")== null ? "" : rs.getString("DSC_HIJO")); 
        	curso.setCodResponsable(rs.getString("COD_RESPONSABLE")== null ? "" : rs.getString("COD_RESPONSABLE"));
        	curso.setResponsable(rs.getString("RESPONSABLE")== null ? "" : rs.getString("RESPONSABLE"));
        
            return curso;
        }
    }
}
