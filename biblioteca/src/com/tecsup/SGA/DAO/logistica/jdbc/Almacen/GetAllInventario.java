package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.BienesDevolucion;
import com.tecsup.SGA.modelo.Producto;

public class GetAllInventario extends StoredProcedure{
	/*
	 * PROCEDURE SP_SEL_INVENTARIO(
		E_C_COD_SEDE IN CHAR,
		S_V_RETVAL IN OUT CHAR,
		S_C_RECORDSET IN OUT CUR_RECORDSET);
	 */
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_inventario";
	
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllInventario(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String respuesta) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(S_V_RETVAL, respuesta);
    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {        	
        	Producto bien= new Producto();
        	/*
          AS DSC_FAMILIA
          AS DSC_SUB_FAMILIA
          AS DSC_UBICACION
          AS COD_BIEN
          AS NOM_BIEN
          AS DSC_UNIDAD
          AS CANTIDAD_CAT
          AS CANT_INV
          AS CANT_DIF
        	 */
        	bien.setDscFamilia(rs.getString("DSC_FAMILIA") == null ? "": rs.getString("DSC_FAMILIA"));
        	bien.setDscSubFamilia(rs.getString("DSC_SUB_FAMILIA") == null ? "": rs.getString("DSC_SUB_FAMILIA"));
        	bien.setDscUbicacion(rs.getString("DSC_UBICACION") == null ? "": rs.getString("DSC_UBICACION"));
        	bien.setCodProducto(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
        	bien.setNomProducto(rs.getString("NOM_BIEN") == null ? "": rs.getString("NOM_BIEN"));
        	bien.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
        	bien.setCantidad(rs.getString("CANTIDAD_CAT") == null ? "": rs.getString("CANTIDAD_CAT"));
        	bien.setCantidadInventario(rs.getString("CANT_INV") == null ? "": rs.getString("CANT_INV"));
        	bien.setCantidadDiferencia(rs.getString("CANT_DIF") == null ? "": rs.getString("CANT_DIF"));
//ALQD,23/12/08. AGREGANDO DOS NUEVAS PROPIEDADES: P.UNITARIO Y TOT.DIF.
        	bien.setPreUnitario(rs.getString("P_UNITARIO") == null ? "": rs.getString("P_UNITARIO"));
        	bien.setTotDiferencia(rs.getString("TOT_DIF") == null ? "": rs.getString("TOT_DIF"));
        	return bien;
        }
    }
}
