package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.Almacen.GetAllGuiaRemisionAsociadas.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.GuiaRemision;

public class GetAllDatosGuiaRemision extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_datos_guia_rem";
	
		/*E_C_COD_DOC_PAG IN CHAR*/
	
	private static final String E_C_COD_DOC_PAG = "E_C_COD_DOC_PAG";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDatosGuiaRemision(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_DOC_PAG, OracleTypes.CHAR));
       
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codDocPago) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_DOC_PAG, codDocPago);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	GuiaRemision guiaRemision= new GuiaRemision();
        	
        	guiaRemision.setCodCtaPago(rs.getString("COD_CTA_PAG") == null ? "": rs.getString("COD_CTA_PAG"));
        	guiaRemision.setNroGuia(rs.getString("NRO_GUIA") == null ? "": rs.getString("NRO_GUIA"));
        	guiaRemision.setFecEmisionGuia(rs.getString("FEC_EMI_GUIA") == null ? "": rs.getString("FEC_EMI_GUIA"));
        	guiaRemision.setNroCtaPago(rs.getString("NRO_CTA_PAG") == null ? "": rs.getString("NRO_CTA_PAG"));
        	guiaRemision.setFecEmisionCta(rs.getString("FEC_EMI_CTA") == null ? "": rs.getString("FEC_EMI_CTA"));
        	guiaRemision.setDscMontoCta(rs.getString("MONTO_CTA") == null ? "0": rs.getString("MONTO_CTA"));
         	guiaRemision.setFecVencimiento(rs.getString("FEC_VENCIMIENTO") == null ? "": rs.getString("FEC_VENCIMIENTO"));
         	guiaRemision.setFecRecepcion(rs.getString("FEC_RECEPCION") == null ? "": rs.getString("FEC_RECEPCION"));
         	
        	return guiaRemision;
        }
    }
}
