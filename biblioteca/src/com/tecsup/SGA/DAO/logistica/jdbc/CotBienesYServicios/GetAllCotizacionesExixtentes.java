package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Cotizacion;

//ALQD,07/08/09.A�ADIENDO NUEVO PARAMETRO
public class GetAllCotizacionesExixtentes extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_cotizaciones_existentes";
		
	/*E_C_CODSEDE IN CHAR,
E_C_TIPOCOTIZACION IN CHAR,
E_V_NROCOTIZACION IN VARCHAR2,
E_C_FEC_INI IN CHAR,
E_C_FEC_FIN IN CHAR,
E_C_TIPO_PAGO IN CHAR,
S_C_RECORDSET IN OUT CUR_RECORDSET)*/
	
	private static final String E_C_CODSEDE = "E_C_CODSEDE"; 
	private static final String E_C_TIPOCOTIZACION = "E_C_TIPOCOTIZACION"; 
	private static final String E_C_SUB_TIPOCOTIZACION = "E_C_SUB_TIPOCOTIZACION";
	private static final String E_V_NROCOTIZACION = "E_V_NROCOTIZACION";
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_TIPO_PAGO = "E_C_TIPO_PAGO";
	private static final String E_C_INDINVERSION = "E_C_INDINVERSION";
	
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllCotizacionesExixtentes(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPOCOTIZACION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_SUB_TIPOCOTIZACION, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NROCOTIZACION, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO_PAGO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_INDINVERSION, OracleTypes.VARCHAR));
                
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

	//ALQD,07/08/09.A�ADIENDO NUEVO PARAMETRO
    public Map execute(String codSede, String codTipoCotizacion, String nroCotizacion
    		, String fechaIni, String fechaFin, String codTipoPago
    		, String codSubTipoCotizacion, String indInversion) {
    	
    	System.out.println("GetAllCotizacionesExixtentes");
    	System.out.println("codSede: "+codSede);
    	System.out.println("codTipoCotizacion: "+codTipoCotizacion);
    	System.out.println("codSubTipoCotizacion: "+codSubTipoCotizacion);
    	System.out.println("nroCotizacion: "+nroCotizacion);
    	System.out.println("fechaIni: "+fechaIni);
    	System.out.println("fechaFin: "+fechaFin);
    	System.out.println("codTipoPago: "+codTipoPago);
    	System.out.println("indInversion: "+indInversion);
    	    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_C_TIPOCOTIZACION, codTipoCotizacion);
    	inputs.put(E_C_SUB_TIPOCOTIZACION, codSubTipoCotizacion);
    	inputs.put(E_V_NROCOTIZACION, nroCotizacion);
    	inputs.put(E_C_FEC_INI, fechaIni);
    	inputs.put(E_C_FEC_FIN, fechaFin);
    	inputs.put(E_C_TIPO_PAGO, codTipoPago);
    	inputs.put(E_C_INDINVERSION, indInversion);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Cotizacion cotizacion= new Cotizacion();
        	
        	cotizacion.setCodigo(rs.getString("COD_COTIZACION") == null ? "": rs.getString("COD_COTIZACION"));
        	cotizacion.setNroCotizacion(rs.getString("NRO_COTIZACION") == null ? "": rs.getString("NRO_COTIZACION"));
        	cotizacion.setFechaEmision(rs.getString("FECHA_COTIZACION") == null ? "": rs.getString("FECHA_COTIZACION"));
        	cotizacion.setFechaInicio(rs.getString("FECHA_INI_VIG") == null ? "": rs.getString("FECHA_INI_VIG"));
        	cotizacion.setFechaFin(rs.getString("FECHA_INI_FIN") == null ? "": rs.getString("FECHA_INI_FIN"));
        	cotizacion.setDscTipoGasto(rs.getString("DSC_TIPO_GASTO") == null ? "": rs.getString("DSC_TIPO_GASTO"));
        	
            return cotizacion;
        }
    }
}
