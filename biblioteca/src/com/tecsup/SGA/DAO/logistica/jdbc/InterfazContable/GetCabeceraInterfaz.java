package com.tecsup.SGA.DAO.logistica.jdbc.InterfazContable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.InterfazContable;
import com.tecsup.SGA.modelo.InterfazContableCabecera;

public class GetCabeceraInterfaz extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_inter_contable.sp_sel_cabecera_interfaz";
	/*E_C_COD_SEDE IN VARCHAR2,
E_C_MES IN VARCHAR2,
E_C_ANHO IN VARCHAR2,
E_C_TIPO_INTERFAZ VARCHAR2*/
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_C_MES = "E_C_MES";
	private static final String E_C_ANHO = "E_C_ANHO";
	private static final String E_C_TIPO_INTERFAZ = "E_C_TIPO_INTERFAZ";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetCabeceraInterfaz(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_MES, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_ANHO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO_INTERFAZ, OracleTypes.VARCHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String codMes, String codAnio, String codTipoInterfaz) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_C_MES, codMes);
    	inputs.put(E_C_ANHO, codAnio);
    	inputs.put(E_C_TIPO_INTERFAZ, codTipoInterfaz);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	InterfazContableCabecera interfaz= new InterfazContableCabecera();
        
        	interfaz.setCsubdia(rs.getString("CSUBDIA") == null ? "": rs.getString("CSUBDIA"));
        	interfaz.setCcompro(rs.getString("CCOMPRO") == null ? "": rs.getString("CCOMPRO"));
        	interfaz.setCfeccom(rs.getString("CFECCOM") == null ? "": rs.getString("CFECCOM"));
        	interfaz.setCcodmon(rs.getString("CCODMON") == null ? "": rs.getString("CCODMON"));
        	interfaz.setCsitua(rs.getString("CSITUA") == null ? "": rs.getString("CSITUA"));
        	interfaz.setCtipcam(rs.getString("CTIPCAM") == null ? "": rs.getString("CTIPCAM"));
        	interfaz.setCglosa(rs.getString("CGLOSA") == null ? "": rs.getString("CGLOSA"));
        	interfaz.setCtotal(rs.getString("CTOTAL") == null ? "": rs.getString("CTOTAL"));
        	interfaz.setCtipo(rs.getString("CTIPO") == null ? "": rs.getString("CTIPO"));
        	interfaz.setCflag(rs.getString("CFLAG") == null ? "": rs.getString("CFLAG"));
        	interfaz.setCdate(rs.getString("CDATE") == null ? "": rs.getString("CDATE"));
        	interfaz.setChora(rs.getString("CHORA") == null ? "": rs.getString("CHORA"));
        	interfaz.setCuser(rs.getString("CUSER") == null ? "": rs.getString("CUSER"));
        	interfaz.setCfeccam(rs.getString("CFECCAM") == null ? "": rs.getString("CFECCAM"));
        	interfaz.setCorig(rs.getString("CORIG") == null ? "": rs.getString("CORIG"));
        	interfaz.setCform(rs.getString("CFORM") == null ? "": rs.getString("CFORM"));
        	interfaz.setCtipcom(rs.getString("CTIPCOM") == null ? "": rs.getString("CTIPCOM"));
        	interfaz.setCextor(rs.getString("CEXTOR") == null ? "": rs.getString("CEXTOR"));
        	interfaz.setCfeccom2(rs.getString("CFECCOM2") == null ? "": rs.getString("CFECCOM2"));
        	interfaz.setCfeccam2(rs.getString("CFECCAM2") == null ? "": rs.getString("CFECCAM2"));
        	interfaz.setCopcion(rs.getString("COPCION") == null ? "": rs.getString("COPCION"));
        	        	
        	return interfaz;
        }
    }

}
