package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
 * PROCEDURE SP_DEL_GUIA_REQ(
	E_C_CODGUIA IN CHAR,
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */
public class DeleteGuiaRequerimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_del_guia_req";

	private static final String E_C_CODGUIA = "E_C_CODGUIA"; 
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteGuiaRequerimiento(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODGUIA, OracleTypes.CHAR));		
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codGuia, String codUsuario) {
    	
   	
    	Map inputs = new HashMap();    	
    	
    	inputs.put(E_C_CODGUIA, codGuia);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	    	
        return super.execute(inputs);
    }
}