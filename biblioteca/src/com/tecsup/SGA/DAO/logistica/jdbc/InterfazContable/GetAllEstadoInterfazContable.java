package com.tecsup.SGA.DAO.logistica.jdbc.InterfazContable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.InterfazContable;

public class GetAllEstadoInterfazContable extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_inter_contable.sp_sel_est_interfaz";
	/*E_C_COD_SEDE IN VARCHAR2,
E_C_MES IN VARCHAR2,
E_C_ANHO IN VARCHAR2,
E_C_TIPO_INTERFAZ VARCHAR2*/
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_C_MES = "E_C_MES";
	private static final String E_C_ANHO = "E_C_ANHO";
	private static final String E_C_TIPO_INTERFAZ = "E_C_TIPO_INTERFAZ";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllEstadoInterfazContable(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_MES, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_ANHO, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_TIPO_INTERFAZ, OracleTypes.VARCHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String codMes, String codAnio, String codTipoInterfaz) {
    	
    	Map inputs = new HashMap();
    	System.out.println(">>codSede<<"+codSede+">>codMes<<"+codMes+">>codAnio<<"+codAnio+">>codTipoInterfaz<<"+codTipoInterfaz);
    	inputs.put(E_C_COD_SEDE, codSede);
    	inputs.put(E_C_MES, codMes);
    	inputs.put(E_C_ANHO, codAnio);
    	inputs.put(E_C_TIPO_INTERFAZ, codTipoInterfaz);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	InterfazContable interfazContable= new InterfazContable();
        
        	interfazContable.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	interfazContable.setDscEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
        	        	
        	return interfazContable;
        }
    }

}
