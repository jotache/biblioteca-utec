package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertResponsable extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_sol_requerimientos.sp_act_responsable";
	
	private static final String E_C_COD_REQ = "E_C_COD_REQ";
	private static final String E_C_COD_RESP = "E_C_COD_RESP";
	private static final String E_C_COD_USUARIO = "E_C_COD_USUARIO";
	private static final String E_C_COD_ESFUERZO = "E_C_COD_ESFUERZO";
	private static final String S_V_RETVAL = "S_V_RETVAL";

	
	public InsertResponsable(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_REQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_RESP, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_USUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_ESFUERZO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codRequerimiento, String codResp, String usuario,String codEsfuerzo) {
	
	Map inputs = new HashMap();
	System.out.println("1.-"+codRequerimiento+"<<");
	System.out.println("2.-"+codResp+"<<");
	System.out.println("3.-"+usuario+"<<");
	System.out.println("3.-"+codEsfuerzo+"<<");

	inputs.put(E_C_COD_REQ, codRequerimiento);
	inputs.put(E_C_COD_RESP, codResp);
	inputs.put(E_C_COD_USUARIO, usuario);
	inputs.put(E_C_COD_ESFUERZO, codEsfuerzo);
	
	return super.execute(inputs);		
	}	
}
