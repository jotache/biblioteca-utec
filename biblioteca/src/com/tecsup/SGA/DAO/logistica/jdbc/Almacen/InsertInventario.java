package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertInventario extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_almacen.sp_ins_inventario";
	/*
	 PROCEDURE SP_INS_INVENTARIO(
	E_C_COD_SEDE IN CHAR,
	E_V_CAD_COD_BIENES IN VARCHAR2,
	E_V_CAD_CANT_BIENES IN VARCHAR2,
	E_V_CANTIDAD_REG IN VARCHAR2,
	E_V_COD_USUARIO IN VARCHAR2,
	S_V_RETVAL IN OUT CHAR);
	 */
	
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String E_V_CAD_COD_BIENES = "E_V_CAD_COD_BIENES";
	private static final String E_V_CAD_CANT_BIENES = "E_V_CAD_CANT_BIENES";
	private static final String E_V_CANTIDAD_REG = "E_V_CANTIDAD_REG";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertInventario(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_V_CAD_COD_BIENES, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_CANT_BIENES, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CANTIDAD_REG, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.CHAR));
	compile();
	}
	
	public Map execute(String codSede, String cadCodBienes, String cadCanBienes,
			String cantidad, String codUsuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_SEDE, codSede);
	inputs.put(E_V_CAD_COD_BIENES, cadCodBienes);
	inputs.put(E_V_CAD_CANT_BIENES, cadCanBienes);
	inputs.put(E_V_CANTIDAD_REG, cantidad);
	inputs.put(E_V_COD_USUARIO, codUsuario);
	
	return super.execute(inputs);
	
	}
}
