package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento.GetAllSolReqAprobadoDetalle.LogisticaMapper;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllSolReqAprobadoDetalle extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_req_aprobados";
	
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO"; 
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllSolReqAprobadoDetalle(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codUsuario) {
    	
    	System.out.println("E_C_CODUSUARIO: "+codUsuario);
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	ReportesLogistica reportesLogistica = new ReportesLogistica();
        	reportesLogistica.setCodSede(rs.getString("SEDE") == null ? "": rs.getString("SEDE"));
        	reportesLogistica.setDscTipoReq(rs.getString("DSC_TIPO_REQ") == null ? "": rs.getString("DSC_TIPO_REQ"));
        	reportesLogistica.setNroRequerimiento(rs.getString("NRO_REQ") == null ? "": rs.getString("NRO_REQ"));
        	reportesLogistica.setFechaAprobaReq(rs.getString("FEC_APROB") == null ? "": rs.getString("FEC_APROB"));
        	reportesLogistica.setDscUsuarioSol(rs.getString("DSC_USU_SOLIC") == null ? "": rs.getString("DSC_USU_SOLIC"));
        	reportesLogistica.setCodCeco(rs.getString("COD_CECO") == null ? "": rs.getString("COD_CECO"));
        	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
        	reportesLogistica.setDscBien(rs.getString("DSC_BIEN_SERV") == null ? "": rs.getString("DSC_BIEN_SERV"));
        	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
        	reportesLogistica.setCantRequerimiento(rs.getString("CANT_REQ_SOL") == null ? "": rs.getString("CANT_REQ_SOL"));
        	reportesLogistica.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
        	reportesLogistica.setPrecioUnitario(rs.getString("PRECIO_UNITARIO") == null ? "": rs.getString("PRECIO_UNITARIO"));
        	reportesLogistica.setDscTipoGasto(rs.getString("DSC_TIPO_GASTO") == null ? "": rs.getString("DSC_TIPO_GASTO"));
        	reportesLogistica.setCantPorEntregar(rs.getString("CANT_REQ_PEN") == null ? "": rs.getString("CANT_REQ_PEN"));
        	reportesLogistica.setCantReqDevuelto(rs.getString("CANT_REQ_DEV") == null ? "": rs.getString("CANT_REQ_DEV"));
        	reportesLogistica.setDscTipoCotizacion(rs.getString("PARA_STOCK") == null ? "": rs.getString("PARA_STOCK"));
        	
        	return reportesLogistica;
        }
    }
}
