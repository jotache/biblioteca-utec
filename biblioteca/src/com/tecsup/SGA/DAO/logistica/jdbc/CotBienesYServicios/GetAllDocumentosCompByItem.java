package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionAdjunto;

public class GetAllDocumentosCompByItem extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".PKG_LOG_COTIZACION.SP_SEL_DOC_REL_COMP_X_ITEM";
		
	private static final String E_C_COD_COT = "E_C_COD_COT"; 
	private static final String E_C_COD_COT_DET = "E_C_COD_COT_DET";
	private static final String E_C_COD_PROV = "E_C_COD_PROV";  
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDocumentosCompByItem(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_COT, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_COT_DET, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_PROV, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new DocumentoMapper()));
        compile();
    }

    public Map execute(String codCotizacion, String codCotizacionDet, String codProveedor) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_COT, codCotizacion);
    	inputs.put(E_C_COD_COT_DET, codCotizacionDet);
    	inputs.put(E_C_COD_PROV, codProveedor);
        return super.execute(inputs);
    }
    
    final class DocumentoMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionAdjunto cotizacionAdjunto = new CotizacionAdjunto();
        	
        	cotizacionAdjunto.setCodAdjunto(rs.getString("CODIGO") == null ? "": rs.getString("CODIGO"));
        	cotizacionAdjunto.setCodTipoAdjunto(rs.getString("COD_TIP_ADJ") == null ? "": rs.getString("COD_TIP_ADJ"));
        	cotizacionAdjunto.setDscTipoAdjunto(rs.getString("DSC_TIP_ADJ") == null ? "": rs.getString("DSC_TIP_ADJ"));
        	cotizacionAdjunto.setRutaAdjunto(rs.getString("RUTA_ADJ") == null ? "": rs.getString("RUTA_ADJ"));
        	
            return cotizacionAdjunto;
        }
    }
}
