package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public class GetEnvioCorreoOrden extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_orden.sp_sel_envio_correo_orden";
	/*E_C_COD_ORDEN IN CHAR*/
	
	private static final String E_C_COD_ORDEN = "E_C_COD_ORDEN";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetEnvioCorreoOrden(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_ORDEN, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codOrden) {
    	
    	System.out.println("codOrden: "+codOrden);
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_ORDEN, codOrden);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	OrdenesAprobadas ordenesAprobadas = new OrdenesAprobadas();  
        	
        	ordenesAprobadas.setEmailProveedor(rs.getString("CORREO") == null ? "": rs.getString("CORREO"));
        	ordenesAprobadas.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
        	ordenesAprobadas.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	ordenesAprobadas.setDscTipoOrden(rs.getString("TIPO_ORDEN") == null ? "": rs.getString("TIPO_ORDEN"));
        	
            return ordenesAprobadas;
        }
    }

}
