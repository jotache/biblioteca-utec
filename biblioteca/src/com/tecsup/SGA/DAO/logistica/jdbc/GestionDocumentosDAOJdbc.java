package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.List;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos.*;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllBandejaConsultaOrdenes;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.UpdateRechazarOrden;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import java.util.HashMap;
import com.tecsup.SGA.DAO.logistica.GestionDocumentosDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.DetalleDocPago;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public class GestionDocumentosDAOJdbc extends JdbcDaoSupport implements GestionDocumentosDAO {

	public List GetAllActivosDisponibles(String codBien,String codSede){
		GetAllActivosDisponibles GetAllActivosDisponibles = new GetAllActivosDisponibles(this.getDataSource());
		HashMap outputs = (HashMap)GetAllActivosDisponibles.execute(codBien,codSede);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO PARA BUSCAR
	//ALQD,22/01/09. A�ADIENDO NUEVO FILTRO DE BUSQUEDA
	public List<OrdenesAprobadas> GetAllOrdenesAprobadas(String codSede, String codUsuario, String codPerfil, String nroOrden,
			String fecEmisionInicio, String fecEmisionFin, String nroRucProve,String nombreProve, 
			String codComprador, String nomComprador, String codTipoOrden, 
			String indSoloCajaChica, String indSinEnvio, String numFactura,
			String indIngPorConfirmar){
		GetAllOrdenesAprobadas GetAllOrdenesAprobadas = new GetAllOrdenesAprobadas(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllOrdenesAprobadas.execute(codSede, codUsuario, codPerfil, nroOrden,
		fecEmisionInicio, fecEmisionFin, nroRucProve, nombreProve, codComprador, nomComprador, codTipoOrden, 
		indSoloCajaChica, indSinEnvio, numFactura, indIngPorConfirmar);
		if(!outputs.isEmpty()){
			return (List<OrdenesAprobadas>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	//
	public List<OrdenesAprobadas> GetAllDatosOrden(String codOrden){
		GetAllDatosOrden getAllDatosOrden = new GetAllDatosOrden(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDatosOrden.execute(codOrden);
		if(!outputs.isEmpty()){
			return (List<OrdenesAprobadas>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public List<OrdenesAprobadas> GetAllAprobarDocumentosPago(String nroDocumento, String fecEmiIni1, String fecEmiIni2, String nroDocProveedor, 
   		 String razonSocialProveedor, String codTipoEstado, String nroOrden, String fecEmiFin1, 
		 String fecEmiFin2,String perfil, String codSede, String codUsuario){
		GetAllAprobarDocumentosPago GetAllAprobarDocumentosPago = new GetAllAprobarDocumentosPago(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllAprobarDocumentosPago.execute( nroDocumento,  fecEmiIni1, 
				fecEmiIni2,  nroDocProveedor, 
		   		  razonSocialProveedor,  codTipoEstado,  nroOrden,  fecEmiFin1, 
				  fecEmiFin2,perfil, codSede, codUsuario);
				
				if(!outputs.isEmpty()){
			return (List<OrdenesAprobadas>)outputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	//
	public List<OrdenesAprobadas> GetAllDocPagosAsociados(String codOrden){
		GetAllDocPagosAsociados getAllDocPagosAsociados = new GetAllDocPagosAsociados(this.getDataSource());
			
			HashMap outputs = (HashMap)getAllDocPagosAsociados.execute(codOrden);
					
					if(!outputs.isEmpty()){
				return (List<OrdenesAprobadas>)outputs.get(CommonConstants.RET_CURSOR);
			}
			return null;
		}
	
	public List GetAllDetalleDocPago(String docId){
		GetAllDetalleDocPago GetAllDetalleDocPago = new GetAllDetalleDocPago(this.getDataSource());
		HashMap outputs = (HashMap)GetAllDetalleDocPago.execute(docId);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String ActValidarDocPago(String codDocPago,String usuario){
		ActValidarDocPago ActValidarDocPago = new ActValidarDocPago(this.getDataSource());
		HashMap inputs = (HashMap)ActValidarDocPago.execute(codDocPago, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String ActRechazarDocPago(String codDocPago,String usuario){
		ActRechazarDocPago ActRechazarDocPago = new ActRechazarDocPago(this.getDataSource());
		HashMap inputs = (HashMap)ActRechazarDocPago.execute(codDocPago, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllDocPago(String ordenId,String docId){
		GetAllDocPago GetAllDocPago = new GetAllDocPago(this.getDataSource());
		HashMap outputs = (HashMap)GetAllDocPago.execute(ordenId, docId);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//GetAllDocumentosPagosAsociados
	public List<DetalleDocPago> GetAllDetDocPagosAsociados(String codOrden){
		GetAllDetDocPagosAsociados getAllDetDocPagosAsociados = new GetAllDetDocPagosAsociados(this.getDataSource());
			
			HashMap outputs = (HashMap)getAllDetDocPagosAsociados.execute(codOrden);
					
					if(!outputs.isEmpty()){
				return (List<DetalleDocPago>)outputs.get(CommonConstants.RET_CURSOR);
			}
			return null;
		}
	//UpdateDocumentoPago
	public String UpdateDocumentoPago(String codOrden, String nroDocumento, String fecEmisionDoc, 
			String fecVencimientoDoc, String nroGuia, String fecEmisionGuia, String cadCantidad, 
			String cadObservaciones, String codUsuario, String fecGuiaRecepcion){
		
		UpdateDocumentoPago updateDocumentoPago = new UpdateDocumentoPago(this.getDataSource());
		
		HashMap inputs = (HashMap)updateDocumentoPago.execute(codOrden, nroDocumento, fecEmisionDoc,
		fecVencimientoDoc, nroGuia, fecEmisionGuia, cadCantidad, cadObservaciones, codUsuario, fecGuiaRecepcion);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	//
	public String UpdateAnularDocPago(String codDocumento, String codUsuario){
		
		UpdateAnularDocPago updateAnularDocPago = new UpdateAnularDocPago(this.getDataSource());
		
		HashMap inputs = (HashMap)updateAnularDocPago.execute(codDocumento, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}//
	
	public String UpdateEnviarDocPago(String codDocumentoPago, String codUsuario){
		
		UpdateEnviarDocPago updateEnviarDocPago = new UpdateEnviarDocPago(this.getDataSource());
		
		HashMap inputs = (HashMap)updateEnviarDocPago.execute(codDocumentoPago, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
}
