package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotPendientes;
import com.tecsup.SGA.modelo.SolRequerimiento;

public class GetAllCotBienesYServicios extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_sel_cons_det_req";
		
	private static Log log = LogFactory.getLog(GetAllCotBienesYServicios.class);
	
	/*E_C_CODTIPOREQ IN CHAR,-- BIEN O SERVICIO
	E_C_CODSUBTIPOREQ IN CHAR,--EN EL CASO DE BIENES SI ES CONSUMIBLE O ACTIVO, EN EL CASO DE SERVICIOS EL TIPO DE SERVICIO
	E_V_NROREQUERIMIENTO IN CHAR,
	E_C_CODFAMILIA IN CHAR,
	E_C_CODSUBFAMILIA IN CHAR,
	E_C_GRUPOSERVICIO IN CHAR,
	E_C_SEDE IN CHAR,
	E_C_CODCECO IN CHAR,
	E_V_USUSOLICIATNTE IN VARCHAR2,
	E_C_COD_PERFIL IN VARCHAR2*/
	
	private static final String E_C_CODTIPOREQ = "E_C_CODTIPOREQ"; 
	private static final String E_C_CODSUBTIPOREQ = "E_C_CODSUBTIPOREQ"; 
	private static final String E_V_NROREQUERIMIENTO = "E_V_NROREQUERIMIENTO";
	private static final String E_C_CODFAMILIA = "E_C_CODFAMILIA";
	private static final String E_C_CODSUBFAMILIA = "E_C_CODSUBFAMILIA";
	private static final String E_C_GRUPOSERVICIO = "E_C_GRUPOSERVICIO";
	private static final String E_C_SEDE = "E_C_SEDE";
	private static final String E_C_CODCECO = "E_C_CODCECO";
	private static final String E_V_USUSOLICIATNTE = "E_V_USUSOLICIATNTE";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String E_C_COD_PERFIL = "E_C_COD_PERFIL";
	private static final String E_C_IND_INVERSION = "E_C_IND_INVERSION";
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetAllCotBienesYServicios(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSUBTIPOREQ, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NROREQUERIMIENTO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODFAMILIA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODSUBFAMILIA, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_GRUPOSERVICIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODCECO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_USUSOLICIATNTE, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_PERFIL, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_C_IND_INVERSION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codTipoReq, String codSubTipoReq, String nroReq
    		, String codFamilia, String codSubFamilia, String grupoServicio
    		, String sede, String codCeco, String usuSolicitante, String codUsuario, String codPerfil,
    		String indInversion) {
    	
    	log.info("******* INI " + SPROC_NAME + " ***********");
    	log.info("E_C_CODTIPOREQ: "+codTipoReq);
    	log.info("E_C_CODSUBTIPOREQ: "+codSubTipoReq);
    	log.info("E_V_NROREQUERIMIENTO: "+nroReq);
    	log.info("E_C_CODFAMILIA: "+codFamilia);
    	log.info("E_C_CODSUBFAMILIA: "+codSubFamilia);
    	log.info("E_C_GRUPOSERVICIO: "+grupoServicio);
    	log.info("E_C_SEDE: "+sede);
    	log.info("E_C_CODCECO: "+codCeco);
    	log.info("E_V_USUSOLICIATNTE: "+usuSolicitante);
    	log.info("E_C_CODUSUARIO: "+codUsuario);
    	log.info("E_C_COD_PERFIL: "+codPerfil);
    	log.info("E_C_IND_INVERSION: "+indInversion);
    	log.info("******* INI " + SPROC_NAME + " ***********");
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODTIPOREQ, codTipoReq);
    	inputs.put(E_C_CODSUBTIPOREQ, codSubTipoReq);
    	inputs.put(E_V_NROREQUERIMIENTO, nroReq);
    	inputs.put(E_C_CODFAMILIA, codFamilia);
    	inputs.put(E_C_CODSUBFAMILIA, codSubFamilia);
    	inputs.put(E_C_GRUPOSERVICIO, grupoServicio);
    	inputs.put(E_C_SEDE, sede);
    	inputs.put(E_C_CODCECO, codCeco);
    	inputs.put(E_V_USUSOLICIATNTE, usuSolicitante);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	inputs.put(E_C_COD_PERFIL, codPerfil);
    	inputs.put(E_C_IND_INVERSION, indInversion);
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotPendientes cotPendientes= new CotPendientes();
        	
        	cotPendientes.setCodBien(rs.getString("CODIGO_BIEN") == null ? "": rs.getString("CODIGO_BIEN"));
        	cotPendientes.setNroReq(rs.getString("NRO_REQUERIMIENTO") == null ? "": rs.getString("NRO_REQUERIMIENTO"));
        	cotPendientes.setUsuSolicitante(rs.getString("USU_SOLICITANTE") == null ? "": rs.getString("USU_SOLICITANTE"));
        	cotPendientes.setValor1(rs.getString("VALOR1") == null ? "": rs.getString("VALOR1"));
        	cotPendientes.setValor2(rs.getString("VALOR2") == null ? "": rs.getString("VALOR2"));
        	cotPendientes.setValor3(rs.getString("VALOR3") == null ? "": rs.getString("VALOR3"));
        	cotPendientes.setFechaEntrega(rs.getString("FECHA_ENTREGA") == null ? "": rs.getString("FECHA_ENTREGA")); 
        	cotPendientes.setCodInvolucrados(rs.getString("CODIGOS_INVOLUCRADOS") == null ? "": rs.getString("CODIGOS_INVOLUCRADOS")); 
        	cotPendientes.setNroProveedores(rs.getString("NRO_PROVEEDORES") == null ? "": rs.getString("NRO_PROVEEDORES"));  
        	cotPendientes.setDscDetalle(rs.getString("DESCRIPCION_DETALLE") == null ? "": rs.getString("DESCRIPCION_DETALLE"));
        	//ALQD,03/03/09.NUEVA PROPIEDAD NOM_SERVICIO
        	cotPendientes.setNomServicio(rs.getString("NOM_SERVICIO") == null ? "": rs.getString("NOM_SERVICIO"));
        	cotPendientes.setDesCeco(rs.getString("CCOSTO") == null ? "": rs.getString("CCOSTO"));
            return cotPendientes;
        }
    }
}
