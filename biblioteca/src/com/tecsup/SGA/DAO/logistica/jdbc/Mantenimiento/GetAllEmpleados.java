package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Empleados;

public class GetAllEmpleados extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_comun.sp_sel_empleados";
	
	private static final String E_C_TIPO_PERSONAL = "E_C_TIPO_PERSONAL"; 
	private static final String E_V_NOMBRE = "E_V_NOMBRE"; 
	private static final String E_V_APEPAT = "E_V_APEPAT";
	private static final String E_V_APEMAT = "E_V_APEMAT";
	private static final String E_C_COD_SEDE = "E_C_COD_SEDE";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllEmpleados(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_TIPO_PERSONAL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOMBRE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_APEPAT, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_APEMAT, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_SEDE, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String tipoPer, String nombre, String apPat
    		,String apMat, String codSede) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_TIPO_PERSONAL, tipoPer);
    	inputs.put(E_V_NOMBRE, nombre);
    	inputs.put(E_V_APEPAT, apPat);
    	inputs.put(E_V_APEMAT, apMat);
    	inputs.put(E_C_COD_SEDE, codSede);
        return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Empleados curso = new Empleados();
        	
        	curso.setCodigo(rs.getString("CODIGO")== null ? "" : rs.getString("CODIGO"));
        	curso.setNombre(rs.getString("NOMBRE")== null ? "" : rs.getString("NOMBRE"));
        	curso.setTipoPersonal(rs.getString("TIPO_PERSONAL")== null ? "" : rs.getString("TIPO_PERSONAL"));
        	curso.setDepartamento(rs.getString("DEPARTAMENTO")== null ? "" : rs.getString("DEPARTAMENTO"));
            return curso;
        }
    }
}
