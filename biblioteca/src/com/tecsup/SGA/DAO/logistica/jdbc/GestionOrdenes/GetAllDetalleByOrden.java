package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

import com.tecsup.SGA.modelo.DetalleOrden;

public class GetAllDetalleByOrden extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_orden.sp_sel_detalle_orden";
	
	private static final String E_C_CODORDEN = "E_C_CODORDEN";
	
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetalleByOrden(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODORDEN, OracleTypes.CHAR));
                
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codOrden) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODORDEN, codOrden);
    	    	     	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	DetalleOrden detalleOrden= new DetalleOrden();
//ALQD,12/11/08. A�ADIENDO EL CODIGO DEL BIEN
        	detalleOrden.setCodBien(rs.getString("BIEN_ID") == null ? "": rs.getString("BIEN_ID"));
        	detalleOrden.setDescripcion(rs.getString("DESCRIPCION") == null ? "": rs.getString("DESCRIPCION"));
        	detalleOrden.setCantidad(rs.getString("CANTIDAD") == null ? "": rs.getString("CANTIDAD"));
        	detalleOrden.setPrecio(rs.getString("PRECIO") == null ? "": rs.getString("PRECIO"));
        	detalleOrden.setSubtotal(rs.getString("SUBTOTAL") == null ? "": rs.getString("SUBTOTAL"));
        	detalleOrden.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
        	detalleOrden.setComentario(rs.getString("COMENTARIO") == null ? "": rs.getString("COMENTARIO"));
        	//ALQD,06/07/09.LEYENDO NUEVA COLUMNA
        	detalleOrden.setProrrateo(rs.getString("TOT_PRORRATEO")== null ? "" : rs.getString("TOT_PRORRATEO"));
        	return detalleOrden;
        }
    }
}
