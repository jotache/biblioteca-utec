package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.logistica.InterfazContableDAO;
import com.tecsup.SGA.DAO.logistica.jdbc.InterfazContable.*;

public class InterfazContableDAOJdbc extends JdbcDaoSupport implements InterfazContableDAO{

	public List GetAllEstadoInterfazContable(String codSede, String codMes, String codAnio, String codTipoInterfaz){
			
		GetAllEstadoInterfazContable getAllEstadoInterfazContable = new GetAllEstadoInterfazContable(this.getDataSource());
			
			HashMap outputs = (HashMap)getAllEstadoInterfazContable.execute(codSede, codMes, codAnio,
					codTipoInterfaz);
			if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
			}
			return null;
		}
	
	public String InsertInterfazContable(String codSede, String codMes, String codAnio, String codTipoInterfaz,
			String codUsuario){
		InsertInterfazContable insertInterfazContable = new InsertInterfazContable(this.getDataSource());
		HashMap inputs = (HashMap)insertInterfazContable.execute(codSede, codMes, codAnio, codTipoInterfaz,
				codUsuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetCabeceraInterfaz(String codSede, String codMes, String codAnio, String codTipoInterfaz){
		
		GetCabeceraInterfaz getCabeceraInterfaz = new GetCabeceraInterfaz(this.getDataSource());
			
			HashMap outputs = (HashMap)getCabeceraInterfaz.execute(codSede, codMes, codAnio,
					codTipoInterfaz);
			if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
			}
			return null;
		}
	
	public List GetDetalleInterfaz(String codSede, String codMes, String codAnio, String codTipoInterfaz){
		
		GetDetalleInterfaz getDetalleInterfaz = new GetDetalleInterfaz(this.getDataSource());
			
			HashMap outputs = (HashMap)getDetalleInterfaz.execute(codSede, codMes, codAnio,
					codTipoInterfaz);
			if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
			}
			return null;
		}
	
	public List GetResultadoInterfazContable(String codSede, String codMes, String codAnio, String codTipoInterfaz){
		
		GetResultadoInterfazContable getResultadoInterfazContable = new GetResultadoInterfazContable(this.getDataSource());
			
			HashMap outputs = (HashMap)getResultadoInterfazContable.execute(codSede, codMes, codAnio,
					codTipoInterfaz);
			if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
			}
			return null;
		}
}
