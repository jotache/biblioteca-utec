package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateNotaSeguimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_atencion_req.sp_act_notas_seguimiento";
	
		private static final String E_V_COD_REQ = "E_V_COD_REQ";
		private static final String E_V_COD_NOTA = "E_V_COD_NOTA";
		private static final String E_V_DES_NOTA = "E_V_DES_NOTA";
		private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
		public UpdateNotaSeguimiento(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_V_COD_REQ, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_NOTA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_DES_NOTA, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
		
		public Map execute(String codReq, String codNota,String desNota,String usuario) {
		
		Map inputs = new HashMap();
		System.out.println("entro a modificar");
		System.out.println("1.- "+codReq);
		System.out.println("2.- "+codNota);
		System.out.println("3.- "+desNota);
		System.out.println("4.- "+usuario);
		inputs.put(E_V_COD_REQ, codReq);
		inputs.put(E_V_COD_NOTA, codNota);
		inputs.put(E_V_DES_NOTA, desNota);
		inputs.put(E_V_COD_USUARIO, usuario);
		return super.execute(inputs);
		
		}
}
