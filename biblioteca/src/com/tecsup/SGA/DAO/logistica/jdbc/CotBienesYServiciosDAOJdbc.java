package com.tecsup.SGA.DAO.logistica.jdbc;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.tecsup.SGA.DAO.logistica.CotBienesYServiciosDAO;
import com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos.GetAllRespByActivo;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.ActualizarFechas;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.DeleteBandejaPedido;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.DeleteDetalleCotizacionActual;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.EnviarCotizacionByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllBandejaGenerarOrdenCompra;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.EnviarCotizacionUsuario;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCotizacionByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCondicionesByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllDetProvBndjCotiComp;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllGrpProvBndjCotiComp;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllItmProvBndjCotiComp;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllProvSenvByCoti;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllSolicitudCotizacion;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCotBienesYServicios;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCondicionCotizacion;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCotizacionesExixtentes;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllDetalleCotizacionActual;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetCotizacionActual;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetEnvioCorreoProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetStrGruposBndjCotiComp;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetTipoServicioConCotizacion;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertBienesByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertCondicionCotizacion;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertAgregarACotizacion;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllProveedoresByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.DeleteProveedorByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllProveedores;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertCondicionesByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertCotSolGenerarOrdenCompraByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertDetalleCotizacionExistente;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertProveedorByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllProductosByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.DeleteProductoByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllDocumentosByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllDocumentosCompByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.DeleteDocRelItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.InsertDocRelItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.UpdateCerrarCotizacion;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.UpdateCotizacionActual;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.UpdateDetProvBndjCotiComp;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.UpdateDocumentosReqACot;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.UpdateProveedorByItem;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.DeleteCondicionCotizacion;
import com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento.EnviarSolRequerimientoUsuario;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllBienesByProveedor;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetAllCotizacionById;
import com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes.GetAllAprobadorByOrden;
import com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios.GetResumenPendientePorCotizar;
import com.tecsup.SGA.bean.ProductoByItemBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotPendientes;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.CotizacionAdjunto;
import com.tecsup.SGA.modelo.CotizacionDetalle;
import com.tecsup.SGA.modelo.ResumenPendientePorCotizar;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;


public class CotBienesYServiciosDAOJdbc extends JdbcDaoSupport implements CotBienesYServiciosDAO {

	public List<CotPendientes> GetAllCotBienesYServicios(String codTipoReq,	String codSubTipoReq, 
			String nroReq, String codFamilia, String codSubFamilia, String grupoServicio, 
			String sede, String codCeco, String usuSolicitante, String codUsuario, String codPerfil,String indInversion) {
		
		GetAllCotBienesYServicios getAllCotBienesYServicios = new GetAllCotBienesYServicios(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCotBienesYServicios.execute(codTipoReq, codSubTipoReq, nroReq, 
			codFamilia, codSubFamilia, grupoServicio, sede, codCeco, usuSolicitante, codUsuario, codPerfil,indInversion);
		if (!outputs.isEmpty())
		{
			return (List<CotPendientes>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	
	public List GetAllCondicionCotizacion(String codCotizacion){
		GetAllCondicionCotizacion GetAllCondicionCotizacion = new GetAllCondicionCotizacion(this.getDataSource());
		HashMap outputs = (HashMap)GetAllCondicionCotizacion.execute(codCotizacion);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//InsertAgregarACotizacion
	public String InsertAgregarACotizacion(String codUsuario, String cadenaCodigos, String cantCodigos){
		InsertAgregarACotizacion insertAgregarACotizacion = new InsertAgregarACotizacion(this.getDataSource()); 
		HashMap inputs = (HashMap)insertAgregarACotizacion.execute(codUsuario, cadenaCodigos, cantCodigos);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteItemBandejaProducto(String codUsuario, String cadenaCodigos, String cantCodigos){
		DeleteBandejaPedido delete = new DeleteBandejaPedido(this.getDataSource()); 
		HashMap inputs = (HashMap)delete.execute(codUsuario, cadenaCodigos, cantCodigos);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertCondicionCotizacion(String codId, String codCotizacion, String codTipoCondicion,String detalleInicial,
			String indSel,String usuario){
		InsertCondicionCotizacion InsertCondicionCotizacion = new InsertCondicionCotizacion(this.getDataSource());
		HashMap inputs = (HashMap)InsertCondicionCotizacion.execute(codId
				, codCotizacion, codTipoCondicion, detalleInicial, indSel, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List<CotizacionProveedor> getAllProveedoresByItem(
			String codCotizacion, String codCotizacionDet) {
		GetAllProveedoresByItem GetAllProveedoresByItem = new GetAllProveedoresByItem(this.getDataSource());
		
		HashMap inputs = (HashMap)GetAllProveedoresByItem.execute(codCotizacion, codCotizacionDet);
		if(!inputs.isEmpty()){
			return (List<CotizacionProveedor>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String deleteProveedorByItem(String codCotizacion,
			String codCotizacionDet, String codProveedor, String codUsuario) {
		
		DeleteProveedorByItem deleteProveedorByItem = new DeleteProveedorByItem(this.getDataSource());
		HashMap inputs = (HashMap)deleteProveedorByItem.execute(codCotizacion, codCotizacionDet
				, codProveedor, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public List<CotizacionProveedor> getAllProveedores(String ruc,String razonSocial) {
		GetAllProveedores getAllProveedores = new GetAllProveedores(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllProveedores.execute(ruc, razonSocial);
		if(!inputs.isEmpty()){
			return (List<CotizacionProveedor>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}

	//ALQD,13/07/09.AGREGANDO NUEVO PARAMETRO
	public List<Cotizacion> GetCotizacionActual(String codSede, String codTipoCotizacion
			, String codSubTipoCotizacion, String codUsuario, String indInversion) {
		
		GetCotizacionActual getCotizacionActual = new GetCotizacionActual(this.getDataSource());		
		
		HashMap outputs = (HashMap)getCotizacionActual.execute(codSede, codTipoCotizacion
				, codSubTipoCotizacion, codUsuario, indInversion);
		
		if(!outputs.isEmpty()){
			return (List<Cotizacion>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	
	public String UpdateCotizacionActual(String codCotizacion, String usuario, String codTipoPago, 
			String fechaInicio, String horaInicio, String fechaFin, String horaFin, 
			String cadenaDescripcion, String cadenaCodDetalle, String cantidad){
		
		UpdateCotizacionActual updateCotizacionActual = new UpdateCotizacionActual(this.getDataSource());
		
		HashMap inputs = (HashMap)updateCotizacionActual.execute(codCotizacion, usuario, codTipoPago, 
				fechaInicio, horaInicio, fechaFin, horaFin, cadenaDescripcion, cadenaCodDetalle, cantidad);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List<CotizacionDetalle> GetAllDetalleCotizacionActual(String codCotizacion, String codTipoCotizacion) {
		
		GetAllDetalleCotizacionActual getAllDetalleCotizacionActual = new GetAllDetalleCotizacionActual(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetalleCotizacionActual.execute(codCotizacion, codTipoCotizacion);		
		
		if(!outputs.isEmpty()){
			return (List<CotizacionDetalle>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	public String DeleteDetalleCotizacionActual(String codCotizacion, String codDetCotizacion, String codUsuario) {
		DeleteDetalleCotizacionActual deleteDetalleCotizacionActual = new DeleteDetalleCotizacionActual(this.getDataSource());		
		
		HashMap inputs = (HashMap)deleteDetalleCotizacionActual.execute(codCotizacion, codDetCotizacion, codUsuario);		
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public String insertProveedorByItem(String codCotizacion,
			String codCotizacionDet, String codProveedor, String codUsuario) {
		
		InsertProveedorByItem insertProveedorByItem = new InsertProveedorByItem(this.getDataSource());
		HashMap inputs = (HashMap)insertProveedorByItem.execute(codCotizacion, codCotizacionDet
				, codProveedor, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public String deleteProductosByItem(String codReq, String codReqDet,
			String codUsuario) {
		DeleteProductoByItem deleteProductoByItem = new DeleteProductoByItem(this.getDataSource());
		HashMap inputs = (HashMap)deleteProductoByItem.execute(codReq, codReqDet, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public List<ProductoByItemBean> getAllProductosByItem(String codCotizacion,
			String codCotizacionDet) {
		GetAllProductosByItem getAllProductosByItem = new GetAllProductosByItem(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllProductosByItem.execute(codCotizacion, codCotizacionDet);
		if(!inputs.isEmpty()){
			return (List<ProductoByItemBean>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<SolRequerimientoAdjunto> getAllDocumentosByItem(
			String codCotizacion, String codCotizacionDet) {
		GetAllDocumentosByItem getAllDocumentosByItem = new GetAllDocumentosByItem(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllDocumentosByItem.execute(codCotizacion, codCotizacionDet);
		if(!inputs.isEmpty()){
			return (List<SolRequerimientoAdjunto>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public List<CotizacionAdjunto> getAllDocumentosCompByItem(
			String codCotizacion, String codCotizacionDet, String codProveedor) {
		GetAllDocumentosCompByItem getAllDocumentosCompByItem = new GetAllDocumentosCompByItem(this.getDataSource());
		
		HashMap inputs = (HashMap)getAllDocumentosCompByItem.execute(codCotizacion, codCotizacionDet, codProveedor);
		if(!inputs.isEmpty()){
			return (List<CotizacionAdjunto>)inputs.get(CommonConstants.RET_CURSOR);
		}
		return null;
	}
	
	public String deleteDocumentoByItem(String codCotizacion,
			String codCotizacionDet, String codAdjunto, String codUsuario) {
		DeleteDocRelItem deleteDocRelItem = new DeleteDocRelItem(this.getDataSource());
		HashMap inputs = (HashMap)deleteDocRelItem.execute(codCotizacion, codCotizacionDet
				, codAdjunto, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	public String insertDocumentoByItem(String codCotizacion, String codCotizacionDet, String codTipoAdjunto,
			String codProveedor, String nombreAdjunto, String nombreGenerado, String codUsuario) {
		InsertDocRelItem insertDocRelItem = new InsertDocRelItem(this.getDataSource());
		HashMap inputs = (HashMap)insertDocRelItem.execute(codCotizacion, codCotizacionDet, codTipoAdjunto
				,codProveedor , nombreAdjunto, nombreGenerado, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	//ALQD,07/08/09.A�ADIENDO NUEVO PARAMETRO
	public List<Cotizacion> GetAllCotizacionesExixtentes(String codSede,
			String codTipoCotizacion, String nroCotizacion, String fechaIni, String fechaFin,
			String codTipoPago, String codSubTipoCotizacion, String indInversion) {
		
		GetAllCotizacionesExixtentes getAllCotizacionesExixtentes = new GetAllCotizacionesExixtentes(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCotizacionesExixtentes.execute(codSede
				, codTipoCotizacion, nroCotizacion, fechaIni, fechaFin, codTipoPago
				, codSubTipoCotizacion, indInversion);
		if (!outputs.isEmpty())
		{
			return (List<Cotizacion>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	//
	public String UpdateDocumentosReqACot(String codCotizacion, String codCotizacionDetalle, String codUsuario){
		
		UpdateDocumentosReqACot updateDocumentosReqACot = new UpdateDocumentosReqACot(this.getDataSource());
		
		HashMap inputs = (HashMap)updateDocumentosReqACot.execute(codCotizacion, codCotizacionDetalle,
				codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	//
	public String UpdateProveedorByItem(String codCotizacion, String codCotizacionDetalle, String cadProveedores,
			String cadIndicadores, String cantidad, String codUsuario){
			
		UpdateProveedorByItem updateProveedorByItem = new UpdateProveedorByItem(this.getDataSource());
			
			HashMap inputs = (HashMap)updateProveedorByItem.execute(codCotizacion, codCotizacionDetalle, 
					cadProveedores, cadIndicadores, cantidad, codUsuario);
			
			if(!inputs.isEmpty()){
				return (String)inputs.get("S_V_RETVAL");
			}
			return null;
		}
	//GelAllSolicitudCotizacion
	//ALQD,30/01/09. NUEVO PARAMETRO PARA FILTRAR COTs SIN OC
	public List<Cotizacion> GetAllSolicitudCotizacion(String codSede, String nroCotizacion, 
			String fechaInicio, String fechaFin, String codEstado, String codTipoReq, String codSubTipoReq,
			String codTipoPago, String rucProveedor, String dscProveedor, String codPerfil, String codUsuario,
			String codGrupoServ, String indCotSinOC) {
		
		GetAllSolicitudCotizacion getAllSolicitudCotizacion = new GetAllSolicitudCotizacion(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllSolicitudCotizacion.execute(codSede, nroCotizacion,
				fechaInicio, fechaFin, codEstado, codTipoReq, codSubTipoReq, codTipoPago, rucProveedor, 
				dscProveedor, codPerfil, codUsuario, codGrupoServ, indCotSinOC);
		if (!outputs.isEmpty())
		{
			return (List<Cotizacion>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	//
	public List<CotizacionProveedor> GetAllBandejaGenerarOrdenCompra(String codCotizacion) {
		
		GetAllBandejaGenerarOrdenCompra getAllBandejaGenerarOrdenCompra = new GetAllBandejaGenerarOrdenCompra(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllBandejaGenerarOrdenCompra.execute(codCotizacion);
		if (!outputs.isEmpty())
		{
			return (List<CotizacionProveedor>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
	
	public List<CotizacionProveedor> GetAllCotizacionByProveedor(String codCotizacion, String codProveedor) {
		
		GetAllCotizacionByProveedor getAllCotizacionByProveedor = new GetAllCotizacionByProveedor(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCotizacionByProveedor.execute(codCotizacion, codProveedor);
		if (!outputs.isEmpty())
		{
			return (List<CotizacionProveedor>)outputs.get(CommonConstants.RET_CURSOR);
		}
		
		return null;
	}
   //InsertCotSolGenerarOrdenCompraByProveedor
	public String InsertCotSolGenerarOrdenCompraByProveedor(String codCotizacion, String codProveedor,
			String cadCodCotizacionDet,	String cadDscCotizacionDet, String cantidad, String codUsuario) {
		
		InsertCotSolGenerarOrdenCompraByProveedor insertCotSolGenerarOrdenCompraByProveedor = 
			new InsertCotSolGenerarOrdenCompraByProveedor(this.getDataSource());
		HashMap inputs = (HashMap)insertCotSolGenerarOrdenCompraByProveedor.execute(codCotizacion,
				codProveedor, cadCodCotizacionDet, cadDscCotizacionDet, cantidad, codUsuario);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get(CommonConstants.RET_STRING);
		}
		return null;
	}
	
	
	public String EnviarCotizacionUsuario(String codCotizacion, String codUsuario){
		
		EnviarCotizacionUsuario enviarCotizacionUsuario = new EnviarCotizacionUsuario(this.getDataSource());		

		HashMap inputs = (HashMap)enviarCotizacionUsuario.execute(codCotizacion, codUsuario);		
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public List<CotizacionDetalle> GetAllBienesByProveedor(String codProveedor, String codCotizacion) {
		
		GetAllBienesByProveedor getAllBienesByProveedor = new GetAllBienesByProveedor(this.getDataSource());		
		
		HashMap outputs = (HashMap)getAllBienesByProveedor.execute(codProveedor, codCotizacion);						
		
		if(!outputs.isEmpty()){
			return (List<CotizacionDetalle>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	public List<CotizacionDetalle> GetAllCondicionesByProveedor( String codCotizacion, String codProveedor) {
		
		GetAllCondicionesByProveedor getAllCondicionesByProveedor = new GetAllCondicionesByProveedor(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllCondicionesByProveedor.execute(codCotizacion, codProveedor);								
		
		if(!outputs.isEmpty()){
			return (List<CotizacionDetalle>)outputs.get("S_C_RECORDSET");
		}
		return null;		
	}
	public String InsertBienesByProveedor(String codCotizacion, String codProveedor
			, String cadenaCodCotDetalle, String cadenaPrecios, String cadenaDescripcion
			, String cantidad, String codUsuario, String codMoneda, String otroCostos) {
		
		InsertBienesByProveedor insertBienesByProveedor = new InsertBienesByProveedor(this.getDataSource());
		
		HashMap inputs = (HashMap)insertBienesByProveedor.execute(codCotizacion
				, codProveedor, cadenaCodCotDetalle, cadenaPrecios, cadenaDescripcion
				, cantidad, codUsuario, codMoneda, otroCostos);		
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public String InsertCondicionesByProveedor(String codCotizacion, String codProveedor, String codDetalle, 
			String dscDetalle, String codUsuario) {
		
		InsertCondicionesByProveedor insertCondicionesByProveedor = new InsertCondicionesByProveedor(this.getDataSource());
		
		HashMap inputs = (HashMap)insertCondicionesByProveedor.execute(codCotizacion, codProveedor, codDetalle, dscDetalle, codUsuario);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}
	public String EnviarCotizacionByProveedor(String codCotizacion, String codProveedor){
		
		EnviarCotizacionByProveedor enviarCotizacionByProveedor = new EnviarCotizacionByProveedor(this.getDataSource());
				

		HashMap inputs = (HashMap)enviarCotizacionByProveedor.execute(codCotizacion, codProveedor);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}

	public List getAllDetProvBndjCotiComp(String codCotizacion,
			String codCotizacionDet) {
		GetAllDetProvBndjCotiComp getAllDetProvBndjCotiComp = new GetAllDetProvBndjCotiComp(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllDetProvBndjCotiComp.execute( codCotizacion,
				 codCotizacionDet);								
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public List getAllItmProvBndjCotiComp(String codCotizacion, String tknActual) {
		GetAllItmProvBndjCotiComp getAllItmProvBndjCotiComp = new GetAllItmProvBndjCotiComp(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllItmProvBndjCotiComp.execute(  codCotizacion,  tknActual);								
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public String getStrGruposBndjCotiComp(String codCotizacion) {
		GetStrGruposBndjCotiComp getStrGruposBndjCotiComp = new GetStrGruposBndjCotiComp(this.getDataSource());
		
		HashMap inputs = (HashMap)getStrGruposBndjCotiComp.execute(codCotizacion);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}

	public List getAllGrpProvBndjCotiComp(String codCotizacion, String tknActual) {
		GetAllGrpProvBndjCotiComp getAllGrpProvBndjCotiComp = new GetAllGrpProvBndjCotiComp(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllGrpProvBndjCotiComp.execute(  codCotizacion,  tknActual);								
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	public String UpdateDetProvBndjCotiComp(String txhCodCotizacion,
			String cadCodDet, String cadCodProv, String cadPrecios, String cantidad, String usuario) {
		UpdateDetProvBndjCotiComp updateDetProvBndjCotiComp = new UpdateDetProvBndjCotiComp(this.getDataSource());
		
		HashMap inputs = (HashMap)updateDetProvBndjCotiComp.execute( txhCodCotizacion,
				 cadCodDet,  cadCodProv,  cadPrecios, cantidad,  usuario);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}

	public String UpdateCerrarCotizacion(String txhCodCotizacion, String usuario) {
		UpdateCerrarCotizacion updateCerrarCotizacion = new UpdateCerrarCotizacion(this.getDataSource());
		
		HashMap inputs = (HashMap)updateCerrarCotizacion.execute( txhCodCotizacion,usuario);				
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}		
		return null;
	}

	public List getAllRespByActivo(String codGuia, String codBien) {
		GetAllRespByActivo getAllRespByActivo = new GetAllRespByActivo(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllRespByActivo.execute( codGuia,  codBien);								
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}

	//ALQD,01/06/09.A�ADIENDO NUEVO PARAMETRO
	public List getAllProvSenvByCoti(String txhCodCotizacion, String codProveedor) {
		GetAllProvSenvByCoti getAllProvSenvByCoti = new GetAllProvSenvByCoti(this.getDataSource());
		
		HashMap outputs = (HashMap)getAllProvSenvByCoti.execute(txhCodCotizacion, codProveedor);								
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String DeleteCondicionCotizacion(String codCotizacion,  String codUsuario){
		DeleteCondicionCotizacion DeleteCondicionCotizacion = new DeleteCondicionCotizacion(this.getDataSource());
		HashMap inputs = (HashMap)DeleteCondicionCotizacion.execute(codCotizacion, codUsuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetEnvioCorreoProveedor(String codCotizacion) {
		GetEnvioCorreoProveedor getEnvioCorreoProveedor = new GetEnvioCorreoProveedor(this.getDataSource());
		
		HashMap outputs = (HashMap)getEnvioCorreoProveedor.execute(codCotizacion);								
		
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}//
	
	public String GetTipoServicioConCotizacion(String codSede, String codUsuario, String codPerfil, String codGrupoServ){
		GetTipoServicioConCotizacion getTipoServicioConCotizacion = new GetTipoServicioConCotizacion(this.getDataSource());
		HashMap inputs = (HashMap)getTipoServicioConCotizacion.execute(codSede, codUsuario, codPerfil, codGrupoServ);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertDetalleCotizacionExistente(String codUsuario, String codCotizacion
			, String cadCodigos, String cantCodigos){
		InsertDetalleCotizacionExistente insertDetalleCotizacionExistente = new InsertDetalleCotizacionExistente(this.getDataSource());
		
		HashMap inputs = (HashMap)insertDetalleCotizacionExistente.execute(codUsuario
				, codCotizacion, cadCodigos, cantCodigos);
		
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllCotizacionById(String codCotizacion){
		GetAllCotizacionById GetAllCotizacionById = new GetAllCotizacionById(this.getDataSource());
		HashMap outputs = (HashMap)GetAllCotizacionById.execute(codCotizacion);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}

	public List GetAllCotizacionAutById(String codCotizacion) {
		GetAllAprobadorByOrden getAllAprobadorByOrden = new GetAllAprobadorByOrden(this.getDataSource());
		HashMap outputs = (HashMap)getAllAprobadorByOrden.execute(codCotizacion);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		
		return null;
	}

	
	public String actualizarFechas(String codCotizacion, String codUsuario,
			String fechaIni, String horaIni, String fechaFin, String horaFin) {
		ActualizarFechas actFechas = new ActualizarFechas(this.getDataSource());
		HashMap inputs = (HashMap)actFechas.execute(codCotizacion, codUsuario,fechaIni,horaIni,fechaFin,horaFin);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	//ALQD,12/08/09.NUEVO METODO PARA RECUPERAR LA CANTIDAD DE PENDIENTES POR TIPO
	public List<ResumenPendientePorCotizar> GetResumenPendientePorCotizar(String codSede) {
		GetResumenPendientePorCotizar getResumenPendientePorcotizar = new GetResumenPendientePorCotizar(this.getDataSource());
		HashMap outputs = (HashMap)getResumenPendientePorcotizar.execute(codSede);
		if(!outputs.isEmpty()){
			return (List<ResumenPendientePorCotizar>)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
}