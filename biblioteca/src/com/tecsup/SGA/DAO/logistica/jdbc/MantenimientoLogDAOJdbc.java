package com.tecsup.SGA.DAO.logistica.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento.*;
import com.tecsup.SGA.DAO.logistica.MantenimientoLogDAO;
import com.tecsup.SGA.modelo.PresupuestoCentroCosto;

public class MantenimientoLogDAOJdbc extends JdbcDaoSupport implements MantenimientoLogDAO{
	public List GetAllTablaDetalle(String padre_Id, String codId
	    	,String codTabla, String codigo, String descripcion, String tipo)
	{
		com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento.GetAllTablaDetalle GetAllTablaDetalle = new GetAllTablaDetalle(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllTablaDetalle.execute(padre_Id, codId, codTabla, codigo, descripcion, tipo);
		if (!outputs.isEmpty())
		{
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllSubFamilias(String bien, String familia, String tipo,String codSubFamilia,String dscFamilia,String dscSubFamilia){
		
		GetAllSubFamilias GetAllSubFamilias = new GetAllSubFamilias(this.getDataSource());
		
		HashMap outputs = (HashMap)GetAllSubFamilias.execute(bien, familia, tipo, codSubFamilia, dscFamilia, dscSubFamilia);
		if(!outputs.isEmpty()){
		return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllProveedor(String codProv, String ruc, String raSoc
    		,String tipoCalif,String estado, String tipoProveedor){
		GetAllProveedor GetAllProveedor = new GetAllProveedor(this.getDataSource());
		HashMap outputs = (HashMap)GetAllProveedor.execute(codProv, ruc, raSoc, tipoCalif, estado, tipoProveedor);
		
		if(!outputs.isEmpty()){
		return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	//ALQD,03/07/09.AGREGANDO DOS NUEVOS PARAMETROS
	public String InsertProveedor(String codTipoCalif,String codTipoProv,String tipoDoc
			,String nroDoc,String razSoc,String razSocCorto,String email,String direc,String codDpto,String codProv
			,String codDsto,String atencion,String codGiro,String codEstado,String banco,String tipCta,String nroCta
			,String codMon,String usuario,String pais,String telefono){
		InsertProveedor InsertProveedor = new InsertProveedor(this.getDataSource());
		HashMap inputs = (HashMap)InsertProveedor.execute(codTipoCalif, codTipoProv, tipoDoc
				, nroDoc, razSoc, razSocCorto, email, direc, codDpto, codProv, codDsto
				, atencion, codGiro, codEstado, banco, tipCta, nroCta, codMon, usuario
				, pais, telefono);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsertTablaDetalle(String codTipoId, String codHijo, String descripcion
			,String valor3, String valor4,String valor5,String usuario){
		InsertTablaDetalle InsertTablaDetalle = new InsertTablaDetalle(this.getDataSource());
		HashMap inputs = (HashMap)InsertTablaDetalle.execute(codTipoId, codHijo
				, descripcion, valor3, valor4, valor5, usuario);
		if(!inputs.isEmpty()){
		return (String)inputs.get("S_V_RETVAL");
		}
		
		return null;
	}
	
	public String DeleteProveedor(String codPro, String usuario){
		DeleteProveedor DeleteProveedor = new DeleteProveedor(this.getDataSource());
		HashMap inputs = (HashMap)DeleteProveedor.execute(codPro, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteProducto(String codPro, String usuario){
		DeleteProducto DeleteProducto = new DeleteProducto(this.getDataSource());
		HashMap inputs = (HashMap)DeleteProducto.execute(codPro, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteFamiliaByProveedor(String codFamProv, String usuario){
		DeleteFamiliaByProveedor DeleteFamiliaByProveedor = new DeleteFamiliaByProveedor(this.getDataSource());
		HashMap inputs = (HashMap)DeleteFamiliaByProveedor.execute(codFamProv, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteTablaDetalle(String tipId,String codUnico, String usuario){
		DeleteTablaDetalle DeleteTablaDetalle = new DeleteTablaDetalle(this.getDataSource());
		HashMap inputs = (HashMap)DeleteTablaDetalle.execute(tipId, codUnico, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String UpdateTablaDetalle(String codTipoId, String codHijo,String codReg,String codGen, String descripcion
			,String valor3, String valor4,String valor5,String usuario){
		
		UpdateTablaDetalle UpdateTablaDetalle = new UpdateTablaDetalle(this.getDataSource());
		HashMap inputs = (HashMap)UpdateTablaDetalle.execute(codTipoId
				, codHijo, codReg, codGen, descripcion, valor3, valor4, valor5, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	//ALQD,03/07/09.AGREGANDO DOS NUEVOS PARAMETROS
	public String UpdateProveedor(String codProveedor, String codTipCal,String codTipProv,String tipDoc, String nroDoc
			,String razSoc, String razSocCorto,String email,String direc,String codDpto,String codProv,String codDsto
			,String atencionA,String codGiro,String codEstado,String banco,String tipCta,String nroCta,String codMoneda
			,String usuario,String pais,String telefono){
		UpdateProveedor UpdateProveedor = new UpdateProveedor(this.getDataSource());
		HashMap inputs = (HashMap)UpdateProveedor.execute(codProveedor
				, codTipCal, codTipProv, tipDoc, nroDoc, razSoc, razSocCorto
				, email, direc, codDpto, codProv, codDsto, atencionA, codGiro
				, codEstado, banco, tipCta, nroCta, codMoneda, usuario, pais
				, telefono);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	public List GetAllProducto(String codCatago,String codSede, String codFam
	    	,String codSubFam, String codPro, String nomPro, String codTipo
	    	,String nroSerie, String descriptor,String estado){
		GetAllProducto GetAllProducto = new GetAllProducto(this.getDataSource());
		HashMap outputs = (HashMap)GetAllProducto.execute(codCatago
				, codSede, codFam, codSubFam, codPro, nomPro, codTipo, nroSerie, descriptor,estado);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	//ALQD,23/02/09.NUEVO PARAMETRO
	//ALQD,24/02/09.NUEVO PARAMETRO PARA ACTUALIZAR PRECIO REF.
	//ALQD,20/05/09.NUEVA PROPIEDAD NO AFECTO
	public String InsertProducto(String codBien, String sede, String familia
			,String subFamilia, String nroProducto,String decrip,String unidad,
			String tipoBien,String ubicacion,String ubiFila,String ubiColum,
			String stockMin,String stockMax,String nroDiasAten,String marca,
			String modelo,String usuario,String inversion,String stockMostrar,
			String estado,String artPromocional,String preURef,String artNoAfecto){
		InsertProducto InsertProducto = new InsertProducto(this.getDataSource());
	HashMap inputs = (HashMap)InsertProducto.execute(codBien, sede, familia, subFamilia, nroProducto, decrip
			, unidad, tipoBien, ubicacion, ubiFila, ubiColum, stockMin, stockMax, nroDiasAten, marca
			, modelo, usuario,inversion,stockMostrar,estado,artPromocional,preURef
			, artNoAfecto);
	if(!inputs.isEmpty()){
		return (String)inputs.get("S_V_RETVAL");
	}
	return null;
	}
	
	public String InsertImageProducto(String codBien, String imagen,String usuario){
		
		InsertImageProducto InsertImageProducto = new InsertImageProducto(this.getDataSource());
		HashMap inputs = (HashMap)InsertImageProducto.execute(codBien, imagen, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	public String InsertDocProducto(String codBien, String tipoDoc,String nomDoc,String usuario){
		InsertDocProducto InsertDocProducto = new InsertDocProducto(this.getDataSource());
		HashMap inputs = (HashMap)InsertDocProducto.execute(codBien, tipoDoc, nomDoc, usuario);
		if(!inputs.isEmpty()){
		return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllDocCatalogo(String codBien,String codAdj){
		GetAllDocCatalogo GetAllDocCatalogo = new GetAllDocCatalogo(this.getDataSource());
		HashMap inputs= (HashMap)GetAllDocCatalogo.execute(codBien, codAdj);
		if(!inputs.isEmpty()){
			return (List)inputs.get("S_C_RECORDSET");
		}
		return null;	
	}
	
	public String DeleteDocProducto(String codBien,String codAdj, String usuario){
		DeleteDocProducto DeleteDocProducto = new DeleteDocProducto(this.getDataSource());
		HashMap inputs = (HashMap)DeleteDocProducto.execute(codBien, codAdj, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	//ALQD,20/08/09.ELIMINA FOTO DEL PRODUCTO
	public String DeleteFotoProducto(String codBien, String usuario){
		DeleteFotoProducto DeleteFotoProducto = new DeleteFotoProducto(this.getDataSource());
		HashMap inputs = (HashMap)DeleteFotoProducto.execute(codBien, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List GetAllAtencionSolicitudes(String codSede,String tipoReq, String subtipoReq, String codResponsable
    		,String nomResponsable,String familia,String subfamilia){
		GetAllAtencionSolicitudes GetAllAtencionSolicitudes = new GetAllAtencionSolicitudes(this.getDataSource());
		HashMap outputs = (HashMap)GetAllAtencionSolicitudes.execute(codSede,tipoReq, subtipoReq, codResponsable, nomResponsable, familia, subfamilia);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	public String InsertAtencionSolicitante(String tipoReq, String codigo, String codReq,String usuario,String codSede){
		InsertAtencionSolicitante InsertAtencionSolicitante = new InsertAtencionSolicitante(this.getDataSource());
		HashMap inputs = (HashMap)InsertAtencionSolicitante.execute(tipoReq, codigo, codReq, usuario,codSede);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public List GetAllEmpleados(String tipoPer, String nombre, String apPat
    		,String apMat, String codSede){
		GetAllEmpleados GetAllEmpleados = new GetAllEmpleados(this.getDataSource());
		HashMap inputs = (HashMap)GetAllEmpleados.execute(tipoPer, nombre, apPat, apMat, codSede);
		if(!inputs.isEmpty()){
			return (List)inputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String InsertFlujo(String codTabla, String codDetalle, String indicador,String usuario,String codSede){
		InsertFlujo InsertFlujo = new InsertFlujo(this.getDataSource());
		HashMap inputs = (HashMap)InsertFlujo.execute(codTabla, codDetalle, indicador, usuario,codSede);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	public String DeleteAtencionSolicitudes(String tipReq,String codigo,String codRes, String usuario,String codSede){
		DeleteAtencionSolicitudes DeleteAtencionSolicitudes = new DeleteAtencionSolicitudes(this.getDataSource());
		HashMap inputs = (HashMap)DeleteAtencionSolicitudes.execute(tipReq, codigo, codRes, usuario,codSede);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String InsDescriptoresByBien(String codBien, String codDes,String usuario){
		InsDescriptoresByBien InsDescriptoresByBien = new  InsDescriptoresByBien(this.getDataSource());
		HashMap inputs = (HashMap)InsDescriptoresByBien.execute(codBien, codDes, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
		
	public List GetAllDescriptoresByBien(String codBien){
		GetAllDescriptoresByBien GetAllDescriptoresByBien = new GetAllDescriptoresByBien(this.getDataSource());
		HashMap outputs = (HashMap)GetAllDescriptoresByBien.execute(codBien);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List GetAllSubFamliaByPro(String codProv){
		GetAllSubFamliaByPro GetAllSubFamliaByPro = new GetAllSubFamliaByPro(this.getDataSource());
		HashMap outputs = (HashMap)GetAllSubFamliaByPro.execute(codProv);
		if(!outputs.isEmpty()){
			return (List)outputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public String InsertSubFamiliaByPro(String codPro, String codSFam,String usuario){
		InsertSubFamiliaByPro InsertSubFamiliaByPro = new InsertSubFamiliaByPro(this.getDataSource());
		HashMap inputs = (HashMap)InsertSubFamiliaByPro.execute(codPro, codSFam, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteDescriptoresByBien(String codBien,String codDet, String usuario){
		DeleteDescriptoresByBien DeleteDescriptoresByBien = new DeleteDescriptoresByBien(this.getDataSource());
		HashMap inputs = (HashMap)DeleteDescriptoresByBien.execute(codBien, codDet, usuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

	public String ActIndiceFlujo(String codTabla, String codDetalle, String indicador){
		ActIndiceFlujo ActIndiceFlujo = new ActIndiceFlujo(this.getDataSource());
		HashMap inputs = (HashMap)ActIndiceFlujo.execute(codTabla, codDetalle, indicador);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllFlujosAprobacion(String codSede, String codFlujo){
		GetAllFlujosAprobacion GetAllFlujosAprobacion=new GetAllFlujosAprobacion(this.getDataSource());
		
		HashMap inputs = (HashMap)GetAllFlujosAprobacion.execute(codSede, codFlujo);
		if(!inputs.isEmpty()){
			return (List)inputs.get("S_C_RECORDSET");
		}
		return null;
	}//
	public String GetSedeByUsuario(String codUsuario){
		GetSedeByUsuario getSedeByUsuario = new GetSedeByUsuario(this.getDataSource());
		HashMap inputs = (HashMap)getSedeByUsuario.execute(codUsuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	//ALQD,11/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,30/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,21/07/09.AGREGANDO 3 NUEVOS PARAMETROS
	public String InsertDocAsociado(String codDocuemnto, String codDocPago
			, String nroDocumento,String fecDocuemnto
			, String montoDocumento,String dscDoc,String codUsuario
			, String codTipMoneda, String impTipCamSoles, String codProvee
			, String nomProvee, String rucProvee){
		InsertDocAsociado insertDocAsociado = new InsertDocAsociado(this.getDataSource());
		HashMap inputs = (HashMap)insertDocAsociado.execute(codDocuemnto
				, codDocPago, nroDocumento, fecDocuemnto, montoDocumento, dscDoc
				, codUsuario, codTipMoneda, impTipCamSoles, codProvee, nomProvee
				, rucProvee);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public String DeleteDocAsociado(String codDocumento, String codUsuario){
		DeleteDocAsociado deleteDocAsociado = new DeleteDocAsociado(this.getDataSource());
		HashMap inputs = (HashMap)deleteDocAsociado.execute(codDocumento, codUsuario);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}
	
	public List GetAllDocAsociado(String codDoc){
		GetAllDocAsociado getAllDocAsociado = new GetAllDocAsociado(this.getDataSource()); 
		HashMap inputs = (HashMap)getAllDocAsociado.execute(codDoc);
		if(!inputs.isEmpty()){
			return (List)inputs.get("S_C_RECORDSET");
		}
		return null;
	}
	
	public List<PresupuestoCentroCosto> listaPresupuestosCenCos(String anio, String sede) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
			
		String sql = " Select P.PRESUP_ID id,P.ANNO anio,c.CODTECSUP CODCENCOS,'' responsable,P.MONEDA codmoneda," +
					 " (select det1.ttde_descripcion from general.gen_tipo_tabla_detalle det1 where det1.ttde_id=p.moneda and det1.tipt_id='0065') desmoneda," +
					 " C.DESCRIPCION,P.IMPORTE,NVL(P.CONTROL,0) AS CONTROL,c.SEDE " +
					 " FROM LOG_PRESUPUESTO P right join GENERAL.GEN_V_CENTROS_COSTO C on p.codcencos = c.CODTECSUP and " +					 
					 " p.estregistro='A' and p.anno=? and p.sede=?"+
					 " Where c.SEDE=? Order by c.CODTECSUP" ;
					 		
		final Object[] args = new Object[] {anio,sede,sede};
		
		return jdbcTemplate.query(sql, args, new PresupuestosRowMapper());
	}
	
	final class PresupuestosRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			PresupuestoCentroCosto presupuesto = new PresupuestoCentroCosto();
			presupuesto.setId(rs.getString("id")==null?"":rs.getString("id"));
			presupuesto.setAnio(rs.getString("anio")==null?"":rs.getString("anio"));
			presupuesto.setCodCencos(rs.getString("codcencos")==null?"":rs.getString("codcencos"));
			presupuesto.setCodMoneda(rs.getString("codmoneda")==null?"":rs.getString("codmoneda"));
			presupuesto.setDesMoneda(rs.getString("desmoneda")==null?"":rs.getString("desmoneda"));
			presupuesto.setDescripcion(rs.getString("descripcion")==null?"":rs.getString("descripcion"));
			presupuesto.setResponsableCencos(rs.getString("responsable")==null?"":rs.getString("responsable"));
			presupuesto.setFlgControl(rs.getString("control")==null?"":rs.getString("control"));
			presupuesto.setImporte(rs.getString("importe")==null?"":rs.getString("importe"));
			return presupuesto;
		}
		
	}

	public String InsertPresupuesto(String sede, String anio,String cadenaDatos, String codUsuario, String nroReg) {
		InsertPresupuesto insercion = new InsertPresupuesto(this.getDataSource());
		HashMap inputs = (HashMap)insercion.execute(sede, anio, cadenaDatos, codUsuario, nroReg);
		if(!inputs.isEmpty()){
			return (String)inputs.get("S_V_RETVAL");
		}
		return null;
	}

}
