package com.tecsup.SGA.DAO.logistica.jdbc.GestionOrdenes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;

public class GetAllBandejaConsultaOrdenes extends StoredProcedure{
	private static Log log = LogFactory.getLog(GetAllBandejaConsultaOrdenes.class);
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_orden.sp_sel_ordenes";
	/*E_C_CODSEDE IN CHAR
	  E_C_PERFIL IN CHAR,
	E_V_NRO_ORDEN IN VARCHAR2,
	E_C_FEC_EMI_INI IN CHAR,
	E_C_FEC_EMI_FIN IN CHAR,
	E_C_COD_ESTADO IN CHAR,
	E_V_RUC_PROV IN VARCHAR2,
	E_V_NOM_PROV IN VARCHAR2,
	E_V_COD_COMP IN VARCHAR2,
	E_V_NOM_COMP IN VARCHAR2,
	E_C_TIPO_ORDEN IN CHAR*/
	
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String E_C_PERFIL = "E_C_PERFIL";
	private static final String E_V_NRO_ORDEN = "E_V_NRO_ORDEN";
	private static final String E_C_FEC_EMI_INI = "E_C_FEC_EMI_INI";
	private static final String E_C_FEC_EMI_FIN = "E_C_FEC_EMI_FIN";
	private static final String E_C_COD_ESTADO = "E_C_COD_ESTADO";
	private static final String E_V_RUC_PROV = "E_V_RUC_PROV";
	private static final String E_V_NOM_PROV = "E_V_NOM_PROV";
	private static final String E_V_COD_COMP = "E_V_COD_COMP";
	private static final String E_V_NOM_COMP = "E_V_NOM_COMP";
	private static final String E_C_TIPO_ORDEN = "E_C_TIPO_ORDEN";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllBandejaConsultaOrdenes(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_PERFIL, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NRO_ORDEN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_INI, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_FEC_EMI_FIN, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_COD_ESTADO, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_RUC_PROV, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOM_PROV, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_COD_COMP, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_V_NOM_COMP, OracleTypes.CHAR));
        declareParameter(new SqlParameter(E_C_TIPO_ORDEN, OracleTypes.CHAR));
        
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSede, String codUsuario, String codPerfil, String nroOrden, String fechaInicio, String fechaFin,String codEstado,
    		String rucProvee, String nomProvee, String codComprador, String nomComprador, String codTipoOrden) {
    	
    	log.info(" **** INI " + SPROC_NAME);
    	log.info("E_C_CODSEDE:" + codSede);
    	log.info("E_C_CODUSUARIO:" + codUsuario);
    	log.info("E_C_PERFIL:" + codPerfil);
    	log.info("E_V_NRO_ORDEN:" + nroOrden);
    	log.info("E_C_FEC_EMI_INI:" + fechaInicio);
    	log.info("E_C_FEC_EMI_FIN:" + fechaFin);
    	log.info("E_C_COD_ESTADO:" + codEstado);
    	log.info("E_V_RUC_PROV:" + rucProvee);
    	log.info("E_V_NOM_PROV:" + nomProvee);
    	log.info("E_V_COD_COMP:" + codComprador);
    	log.info("E_V_NOM_COMP:" + nomComprador);
    	log.info("E_C_TIPO_ORDEN:" + nomComprador);
    	log.info(" **** FIN " + SPROC_NAME);
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSEDE, codSede);
    	inputs.put(E_C_CODUSUARIO, codUsuario);
    	inputs.put(E_C_PERFIL, codPerfil);
    	inputs.put(E_V_NRO_ORDEN, nroOrden);
    	inputs.put(E_C_FEC_EMI_INI, fechaInicio);
    	inputs.put(E_C_FEC_EMI_FIN, fechaFin);
    	inputs.put(E_C_COD_ESTADO, codEstado);
    	inputs.put(E_V_RUC_PROV, rucProvee);
    	inputs.put(E_V_NOM_PROV, nomProvee);
    	inputs.put(E_V_COD_COMP, codComprador);
    	inputs.put(E_V_NOM_COMP, nomComprador);
    	inputs.put(E_C_TIPO_ORDEN, codTipoOrden);
    	     	
    	
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	//ALQD,24/02/09.SE A�ADE NUEVA COLUMNA DE INFORMACION
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionProveedor cotizacionProveedor= new CotizacionProveedor();
        
        	cotizacionProveedor.setCodOrden(rs.getString("COD_ORDEN") == null ? "": rs.getString("COD_ORDEN"));
        	cotizacionProveedor.setDscNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	cotizacionProveedor.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	cotizacionProveedor.setDscProveedor(rs.getString("DSC_PROVEEDOR") == null ? "": rs.getString("DSC_PROVEEDOR"));
        	cotizacionProveedor.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "Sin Orden": rs.getString("DSC_MONEDA"));
        	cotizacionProveedor.setDscMonto(rs.getString("MONTO") == null ? "": rs.getString("MONTO"));
        	cotizacionProveedor.setDescripcion(rs.getString("COMPRADOR") == null ? "": rs.getString("COMPRADOR"));
        	cotizacionProveedor.setDscTipoOrden(rs.getString("TIPO_ORDEN") == null ? "": rs.getString("TIPO_ORDEN"));
        	cotizacionProveedor.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	cotizacionProveedor.setDscEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
        	cotizacionProveedor.setDetalleCondicion(rs.getString("DETALLE_ESTADO") == null ? "": rs.getString("DETALLE_ESTADO"));
        	cotizacionProveedor.setIndiceAprobacion(rs.getString("IND_APROBADOR") == null ? "": rs.getString("IND_APROBADOR"));
        	cotizacionProveedor.setCajaChica(rs.getString("CAJA_CHICA") == null ? "": rs.getString("CAJA_CHICA"));
        	return cotizacionProveedor;
        }
    }
}
