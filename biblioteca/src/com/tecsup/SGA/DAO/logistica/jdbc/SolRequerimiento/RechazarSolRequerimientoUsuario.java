package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_ACT_RECHZ_SOLREQ(
	E_C_CODREQ IN CHAR,
	E_C_CODUSUARIO IN CHAR,
	S_V_RETVAL IN OUT VARCHAR2)
 */

public class RechazarSolRequerimientoUsuario extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_sol_requerimientos.sp_act_rechz_solreq";
		
		private static final String E_C_CODREQ = "E_C_CODREQ";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";				
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public RechazarSolRequerimientoUsuario(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));				
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codRequerimiento, String codUsuario) {
		
		Map inputs = new HashMap();
		System.out.println("1.-"+codRequerimiento+"<<");
		System.out.println("2.-"+codUsuario+"<<");				
		
		inputs.put(E_C_CODREQ, codRequerimiento);
		inputs.put(E_C_CODUSUARIO, codUsuario);		
		
		return super.execute(inputs);
		
	}	
}
