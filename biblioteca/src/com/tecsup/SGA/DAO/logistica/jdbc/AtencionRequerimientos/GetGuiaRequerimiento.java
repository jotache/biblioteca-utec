package com.tecsup.SGA.DAO.logistica.jdbc.AtencionRequerimientos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CondicionCotizacion;
import com.tecsup.SGA.modelo.Guia;
/*
 * PROCEDURE SP_SEL_GUIA_REQ(
	E_C_CODGUIA IN CHAR,
	S_C_RECORDSET IN OUT CUR_RECORDSET)
 */
public class GetGuiaRequerimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_atencion_req.sp_sel_guia_req";
	
	private static final String E_C_CODGUIA = "E_C_CODGUIA";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetGuiaRequerimiento(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODGUIA, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codGuia) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODGUIA, codGuia);
     	
    	System.out.println("codGuia>>"+codGuia);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	Guia guia= new Guia();
        	/*
        	 * 	AS COD_GUIA
				AS NRO_GUIA
				AS FEC_EMISION
				AS DSC_ESTADO
				AS COD_REQ_DET
			    AS NRO_FACT
			    AS FEC_EMI_FACT
			    AS NRO_ORDEN
			    AS OBSERVACION
        	 */
        	guia.setCodGuia(rs.getString("COD_GUIA") == null ? "": rs.getString("COD_GUIA"));
        	guia.setNroGuia(rs.getString("NRO_GUIA") == null ? "": rs.getString("NRO_GUIA"));
        	guia.setFechaEmision(rs.getString("FEC_EMISION") == null ? "": rs.getString("FEC_EMISION"));
        	guia.setEstado(rs.getString("DSC_ESTADO") == null ? "": rs.getString("DSC_ESTADO"));
        	guia.setCodReqDetalle(rs.getString("COD_REQ_DET") == null ? "": rs.getString("COD_REQ_DET"));
        	guia.setNroFactura(rs.getString("NRO_FACT") == null ? "": rs.getString("NRO_FACT"));
        	guia.setFecEmiFactura(rs.getString("FEC_EMI_FACT") == null ? "": rs.getString("FEC_EMI_FACT"));
        	guia.setNroOrden(rs.getString("NRO_ORDEN") == null ? "": rs.getString("NRO_ORDEN"));
        	guia.setObservacion(rs.getString("OBSERVACION") == null ? "": rs.getString("OBSERVACION"));
        	guia.setCodEstado(rs.getString("COD_ESTADO") == null ? "": rs.getString("COD_ESTADO"));
        	
        	guia.setSolicitante(rs.getString("SOLICITANTE") == null ? "": rs.getString("SOLICITANTE"));
        	guia.setLugarTrabajo(rs.getString("LUGARTRABAJO") == null ? "": rs.getString("LUGARTRABAJO"));
        	
        	//ALQD,13/11/08. NUEVAS PROPIEDADES
        	guia.setNroRequerimiento(rs.getString("NRO_REQUERIMIENTO") == null ? "": rs.getString("NRO_REQUERIMIENTO"));
        	guia.setFecAprRequerimiento(rs.getString("FECAPR_REQUERIMIENTO") == null ? "": rs.getString("FECAPR_REQUERIMIENTO"));
        	guia.setNomCenCosto(rs.getString("NOM_CENCOSTO") == null ? "": rs.getString("NOM_CENCOSTO"));
        	guia.setFecCierre(rs.getString("FEC_CIERRE") == null ? "": rs.getString("FEC_CIERRE"));
        	return guia;
        }
    }
}
