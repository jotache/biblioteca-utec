package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Descriptores;

public class GetAllDescriptoresByBien extends StoredProcedure{
private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA + ".pkg_log_mantto_config.sp_sel_descriptores_by_bien";
	
	private static final String E_C_CODBIEN = "E_C_CODBIEN"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDescriptoresByBien(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODBIEN, OracleTypes.CHAR));
         declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new CursosMapper()));
        compile();
    }

    public Map execute(String codBien) {
    	
    	Map inputs = new HashMap();
    	inputs.put(E_C_CODBIEN, codBien);
       return super.execute(inputs);
    }
    
    final class CursosMapper implements RowMapper {
        
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	Descriptores curso = new Descriptores();
        	
        	curso.setCodBien(rs.getString("COD_BIEN")== null ? "" : rs.getString("COD_BIEN"));
        	curso.setCodBienDet(rs.getString("COD_BIEN_DET")== null ? "" : rs.getString("COD_BIEN_DET"));
        	curso.setCodDescriptor(rs.getString("COD_DESCRIPTOR")== null ? "" : rs.getString("COD_DESCRIPTOR"));
        	curso.setDscDescriptor(rs.getString("DSC_DESCRIPTOR")== null ? "" : rs.getString("DSC_DESCRIPTOR"));
            return curso;
        }
    }
}
