package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.SolRequerimiento;

public class GetSolRequerimientoUsuario extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
					+ ".pkg_log_sol_requerimientos.sp_sel_sol_req";
	
	private static final String E_C_CODSOLREQ = "E_C_CODSOLREQ";
	private static final String RECORDSET = "S_C_RECORDSET";
    
    public GetSolRequerimientoUsuario(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_CODSOLREQ, OracleTypes.CHAR));        
        declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codSolReq) {
    	
    	System.out.println("codSolReq:"+codSolReq+"<<");
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSOLREQ, codSolReq);    	
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	SolRequerimiento solRequerimiento = new SolRequerimiento();        	
        	
        	solRequerimiento.setIdRequerimiento(rs.getString("CODIGO") == null ? "": rs.getString("CODIGO"));
        	solRequerimiento.setNroRequerimiento(rs.getString("NRO_REQUERIMIENTO") == null ? "": rs.getString("NRO_REQUERIMIENTO"));
        	solRequerimiento.setFechaEmision(rs.getString("FECHA_EMISION") == null ? "": rs.getString("FECHA_EMISION"));
        	solRequerimiento.setCecoSolicitante(rs.getString("CECO_SOLICITANTE") == null ? "": rs.getString("CECO_SOLICITANTE"));
        	solRequerimiento.setUsuCrea(rs.getString("COD_USU_SOLICITANTE") == null ? "": rs.getString("COD_USU_SOLICITANTE"));
        	solRequerimiento.setUsuSolicitante(rs.getString("NOM_USU_SOLICITANTE") == null ? "": rs.getString("NOM_USU_SOLICITANTE"));
        	solRequerimiento.setTipoRequerimiento(rs.getString("TIPO_REQUERIMIENTO") == null ? "": rs.getString("TIPO_REQUERIMIENTO"));
        	solRequerimiento.setSubTipoRequerimiento(rs.getString("SUB_TIPO_REQUERIMIENTO") == null ? "": rs.getString("SUB_TIPO_REQUERIMIENTO"));        	
        	solRequerimiento.setEstadoReg(rs.getString("ESTADO_REQ") == null ? "": rs.getString("ESTADO_REQ"));
        	solRequerimiento.setUsuResponsable(rs.getString("COD_USU_REP") == null ? "": rs.getString("COD_USU_REP"));        	
        	solRequerimiento.setDscDetalleEstado(rs.getString("DETALLE_ESTADO") == null ? "": rs.getString("DETALLE_ESTADO"));
        	solRequerimiento.setCodRequerimientoOri(rs.getString("CODIGO_ORI") == null ? "": rs.getString("CODIGO_ORI"));
        	solRequerimiento.setNroRequerimientoOri(rs.getString("NRO_REQUERIMIENTO_ORI") == null ? "": rs.getString("NRO_REQUERIMIENTO_ORI"));
        	solRequerimiento.setDscCecoSolicitante(rs.getString("DSC_CECO_SOLICITANTE") == null ? "": rs.getString("DSC_CECO_SOLICITANTE"));
        	solRequerimiento.setDscTipoRequerimiento(rs.getString("DSC_TIPO_REQUERIMIENTO") == null ? "": rs.getString("DSC_TIPO_REQUERIMIENTO"));
        	solRequerimiento.setCodDevolucion(rs.getString("COD_REQ_DEVOLUCION") == null ? "": rs.getString("COD_REQ_DEVOLUCION"));
        	solRequerimiento.setNroDevolucion(rs.getString("NRO_DEVOLUCION") == null ? "": rs.getString("NRO_DEVOLUCION"));
        	solRequerimiento.setFechaDevolucion(rs.getString("FEC_DEVOLUCION") == null ? "": rs.getString("FEC_DEVOLUCION"));
        	solRequerimiento.setMotivoDevolucion(rs.getString("MOTIVO_DEVOLUCION") == null ? "": rs.getString("MOTIVO_DEVOLUCION"));
        	solRequerimiento.setUsuAsignado(rs.getString("DSC_USU_ASIG_RESP") == null ? "": rs.getString("DSC_USU_ASIG_RESP"));
        	solRequerimiento.setIndInversion(rs.getString("IND_INVERSION") == null ? "": rs.getString("IND_INVERSION"));
        	solRequerimiento.setIndParaStock(rs.getString("IND_STOCK") == null ? "": rs.getString("IND_STOCK"));
        	solRequerimiento.setIndEmpLogistica(rs.getString("IND_EMPLOGISTICA") == null ? "": rs.getString("IND_EMPLOGISTICA"));
            return solRequerimiento;
        }
    }
}
