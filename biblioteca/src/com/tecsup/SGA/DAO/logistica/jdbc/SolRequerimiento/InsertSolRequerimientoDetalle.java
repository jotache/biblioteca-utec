package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

/*
	PROCEDURE SP_INS_DET_SOL_REQ(
	E_C_CODREQ IN CHAR,
	E_C_CODDETSOLREQ IN CHAR,
	E_V_NOMBRE IN VARCHAR2,
	E_V_DESCRIPCION IN VARCHAR2,
	E_C_CODBIEN IN CHAR,
	E_V_CANTIDAD VARCHAR2,
	E_C_TIPOGASTO CHAR,
	E_C_FECENTREGA CHAR,
	E_C_CODUSUARIO CHAR,
	S_V_RETVAL OUT VARCHAR2)
 */

public class InsertSolRequerimientoDetalle extends StoredProcedure{

		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_sol_requerimientos.sp_ins_det_sol_req";
		
		private static final String E_C_CODREQ = "E_C_CODREQ";
		private static final String E_C_CODDETSOLREQ = "E_C_CODDETSOLREQ";
		private static final String E_V_NOMBRE = "E_V_NOMBRE";
		private static final String E_V_DESCRIPCION = "E_V_DESCRIPCION";
		private static final String E_C_CODBIEN = "E_C_CODBIEN";
		private static final String E_V_CANTIDAD = "E_V_CANTIDAD";
		private static final String E_C_TIPOGASTO = "E_C_TIPOGASTO";
		private static final String E_C_FECENTREGA = "E_C_FECENTREGA";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
	public InsertSolRequerimientoDetalle(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODDETSOLREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_NOMBRE, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_V_DESCRIPCION, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODBIEN, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_CANTIDAD, OracleTypes.VARCHAR));		
		declareParameter(new SqlParameter(E_C_TIPOGASTO, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_FECENTREGA, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
	}
		
	public Map execute(String codRequerimiento, String codDetSolReq, String nombre, 
			String descripcion, String codBien, String cantidad,
			String tipoGasto, String fecEntrega, String codUsuario) {
		
		Map inputs = new HashMap();
		System.out.println("1.-"+codRequerimiento+"<<");
		System.out.println("2.-"+codDetSolReq+"<<");
		System.out.println("3.-"+nombre+"<<");
		System.out.println("4.-"+descripcion+"<<");
		System.out.println("5.-"+codBien+"<<");
		System.out.println("6.-"+cantidad+"<<");
		System.out.println("7.-"+tipoGasto+"<<");
		System.out.println("8.-"+fecEntrega+"<<");
		System.out.println("9.-"+codUsuario+"<<");
		
		inputs.put(E_C_CODREQ, codRequerimiento);
		inputs.put(E_C_CODDETSOLREQ, codDetSolReq);
		inputs.put(E_V_NOMBRE, nombre);
		inputs.put(E_V_DESCRIPCION, descripcion);
		inputs.put(E_C_CODBIEN, codBien);
		inputs.put(E_V_CANTIDAD, cantidad);
		inputs.put(E_C_TIPOGASTO, tipoGasto);
		inputs.put(E_C_FECENTREGA, fecEntrega);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		
		return super.execute(inputs);		
	}	
}
