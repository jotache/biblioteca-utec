package com.tecsup.SGA.DAO.logistica.jdbc.Almacen;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Almacen;
import com.tecsup.SGA.modelo.Devolucion;
import com.tecsup.SGA.modelo.DevolucionDetalle;

public class GetAllDetalleDevolucion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_almacen.sp_sel_detalle_dev";
	
	private static final String E_C_COD_DEVOLUCION = "E_C_COD_DEVOLUCION";
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllDetalleDevolucion(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_C_COD_DEVOLUCION, OracleTypes.CHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
        compile();
    }

    public Map execute(String codDevolucion) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_COD_DEVOLUCION, codDevolucion);
    	
        return super.execute(inputs);
    }
    
    final class LogisticaMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	DevolucionDetalle devDetalle= new DevolucionDetalle();        	
        	
        	devDetalle.setCodDevolucionDetalle(rs.getString("COD_DEV_DET") == null ? "": rs.getString("COD_DEV_DET"));
        	devDetalle.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
        	devDetalle.setDscBien(rs.getString("DSC_BIEN") == null ? "": rs.getString("DSC_BIEN"));
        	devDetalle.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
        	devDetalle.setCantidad(rs.getString("CANTIDAD") == null ? "": rs.getString("CANTIDAD"));
        	devDetalle.setCodRequerimientoDetalle(rs.getString("COD_REQ_DET") == null ? "": rs.getString("COD_REQ_DET"));
        	devDetalle.setIndiceNumeroSerie(rs.getString("IND_VER_NRO_SERIE") == null ? "": rs.getString("IND_VER_NRO_SERIE"));
        	
        	return devDetalle;
        }
    }
}
