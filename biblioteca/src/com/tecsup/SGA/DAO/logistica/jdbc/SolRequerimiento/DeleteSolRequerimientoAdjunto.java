package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteSolRequerimientoAdjunto extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_sol_requerimientos.SP_DEL_DOC_REL_DET_SOL_REQ";

	private static final String E_C_CODREQ = "E_C_CODREQ"; 
	private static final String E_C_CODDETSOLREQ = "E_C_CODDETSOLREQ"; 
	private static final String E_C_CODDOCADJ = "E_C_CODDOCADJ";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteSolRequerimientoAdjunto(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODDETSOLREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODDOCADJ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.VARCHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codReque, String codDetalleReq, String codDocAdjunto, String codUsuario) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODREQ, codReque);
    	inputs.put(E_C_CODDETSOLREQ, codDetalleReq);
    	inputs.put(E_C_CODDOCADJ, codDocAdjunto);
    	inputs.put(E_V_CODUSUARIO, codUsuario);
    	
        return super.execute(inputs);
    }
	
}
