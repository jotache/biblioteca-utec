package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class ActRechazarDocPago extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ". pkg_log_doc_pago.sp_act_rechazar_doc_pago";

	private static final String E_C_COD_DOC_PAGO = "E_C_COD_DOC_PAGO";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public ActRechazarDocPago(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_COD_DOC_PAGO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codDocPago,String usuario) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_COD_DOC_PAGO, codDocPago);
	inputs.put(E_C_CODUSUARIO, usuario);
	
	return super.execute(inputs);
	
	}
}
