package com.tecsup.SGA.DAO.logistica.jdbc.GestionDocumentos;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class UpdateDocumentoPago extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
 	+ ".pkg_log_doc_pago.sp_ins_doc_pago";
	/*  E_V_ORDE_ID IN VARCHAR,
		E_V_DOCP_NRO_DOCUMENTO IN VARCHAR,
		E_V_DOCP_FEC_EMI_DOC IN VARCHAR,
		E_V_DOCP_FEC_VCTO_DOC IN VARCHAR,
		E_V_PROV_NRO_GUIA IN VARCHAR,
		E_V_DOCP_FEC_EMI_GUIA IN VARCHAR,
		E_V_CAD_COD_CANTIDAD IN VARCHAR,
		E_V_CAD_OBSERVACIONES IN VARCHAR,
		E_V_COD_USUARIO IN VARCHAR*/
	
	private static final String E_V_ORDE_ID = "E_V_ORDE_ID";
	private static final String E_V_DOCP_NRO_DOCUMENTO = "E_V_DOCP_NRO_DOCUMENTO";
	private static final String E_V_DOCP_FEC_EMI_DOC = "E_V_DOCP_FEC_EMI_DOC";
	private static final String E_V_DOCP_FEC_VCTO_DOC = "E_V_DOCP_FEC_VCTO_DOC";
	private static final String E_V_PROV_NRO_GUIA = "E_V_PROV_NRO_GUIA";
	private static final String E_V_DOCP_FEC_EMI_GUIA = "E_V_DOCP_FEC_EMI_GUIA";
	private static final String E_V_CAD_COD_CANTIDAD = "E_V_CAD_COD_CANTIDAD";
	private static final String E_V_CAD_OBSERVACIONES = "E_V_CAD_OBSERVACIONES";
	private static final String E_V_COD_USUARIO = "E_V_COD_USUARIO";
	private static final String E_V_DOCP_FEC_RECEPCION = "E_V_DOCP_FEC_RECEPCION";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public UpdateDocumentoPago(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	
	declareParameter(new SqlParameter(E_V_ORDE_ID, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DOCP_NRO_DOCUMENTO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DOCP_FEC_EMI_DOC, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DOCP_FEC_VCTO_DOC, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_PROV_NRO_GUIA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DOCP_FEC_EMI_GUIA, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_COD_CANTIDAD, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_CAD_OBSERVACIONES, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_COD_USUARIO, OracleTypes.VARCHAR));
	declareParameter(new SqlParameter(E_V_DOCP_FEC_RECEPCION, OracleTypes.VARCHAR));
	
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String codOrden, String nroDocumento, String fecEmisionDoc, String fecVencimientoDoc, 
			String nroGuia, String fecEmisionGuia, String cadCantidad, String cadObservaciones, 
			String codUsuario, String fecGuiaRecepcion) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_V_ORDE_ID, codOrden);
	inputs.put(E_V_DOCP_NRO_DOCUMENTO, nroDocumento);
	inputs.put(E_V_DOCP_FEC_EMI_DOC, fecEmisionDoc);
	inputs.put(E_V_DOCP_FEC_VCTO_DOC, fecVencimientoDoc);
	inputs.put(E_V_PROV_NRO_GUIA, nroGuia);
	inputs.put(E_V_DOCP_FEC_EMI_GUIA, fecEmisionGuia);
	inputs.put(E_V_CAD_COD_CANTIDAD, cadCantidad);
	inputs.put(E_V_CAD_OBSERVACIONES, cadObservaciones);
	inputs.put(E_V_COD_USUARIO, codUsuario);
	inputs.put(E_V_DOCP_FEC_RECEPCION, fecGuiaRecepcion);
				
	return super.execute(inputs);
	
	}
}
