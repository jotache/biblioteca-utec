package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteCondicionCotizacion extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_cotizacion.sp_del_condicion_x_cotizacion";

	private static final String E_C_COD_COTI = "E_C_COD_COTI"; 
	private static final String E_V_COD_USU = "E_V_COD_USU";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteCondicionCotizacion(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_COD_COTI, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_COD_USU, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codCotizacion,  String codUsuario) {
    	
   	
    	Map inputs = new HashMap();
    	System.out.println("1.- "+codCotizacion);
    	System.out.println("2.- "+codUsuario);
    	inputs.put(E_C_COD_COTI, codCotizacion);
    	inputs.put(E_V_COD_USU, codUsuario);
    	    	
        return super.execute(inputs);
    }
}
