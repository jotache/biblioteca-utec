package com.tecsup.SGA.DAO.logistica.jdbc.Mantenimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class InsertAtencionSolicitante extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_mantto_config.sp_ins_atencion_solicitudes";
	private static final String E_C_TIPOREQ = "E_C_TIPOREQ";
	private static final String E_C_CODIGO = "E_C_CODIGO";
	private static final String E_C_CODRESP = "E_C_CODRESP";
	private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
	private static final String E_C_CODSEDE = "E_C_CODSEDE";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public InsertAtencionSolicitante(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_TIPOREQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODIGO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODRESP, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_CODSEDE, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute(String tipoReq, String codigo, String codReq,String usuario,String codSede) {
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_TIPOREQ, tipoReq);
	inputs.put(E_C_CODIGO, codigo);
	inputs.put(E_C_CODRESP, codReq);
	inputs.put(E_C_CODUSUARIO, usuario);
	inputs.put(E_C_CODSEDE, codSede);
	
	return super.execute(inputs);
	
	}
}
