package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class GetStrGruposBndjCotiComp extends StoredProcedure{
		private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	 	+ ".pkg_log_cotizacion.SP_SEL_GRUPOS_BNDJ_COTI_COMP";
		
	private static final String E_C_CODCOTI = "E_C_CODCOTI";
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public GetStrGruposBndjCotiComp(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_CODCOTI, OracleTypes.CHAR));
	declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
	compile();
	}
	
	public Map execute( String codCotizacion) {
	
	Map inputs = new HashMap();
	inputs.put(E_C_CODCOTI, codCotizacion);	
	return super.execute(inputs);
	
	}
}
