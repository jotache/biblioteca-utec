package com.tecsup.SGA.DAO.logistica.jdbc.CotBienesYServicios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CotizacionProveedor;

public class GetAllProveedores extends StoredProcedure{
	
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".PKG_LOG_COMUN.SP_SEL_PROV_BY_RUC";
		
	private static final String E_V_RUC = "E_V_RUC"; 
	private static final String E_V_PROV = "E_V_PROV"; 
	private static final String S_C_RECORDSET = "S_C_RECORDSET";
    
    public GetAllProveedores(DataSource dataSource) {
        super(dataSource, SPROC_NAME);
        declareParameter(new SqlParameter(E_V_RUC, OracleTypes.VARCHAR));
        declareParameter(new SqlParameter(E_V_PROV, OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter(S_C_RECORDSET, OracleTypes.CURSOR, new ProveedorMapper()));
        compile();
    }

    public Map execute(String ruc, String razonSocial) {
    	
    	Map inputs = new HashMap();
    	
    	inputs.put(E_V_RUC, ruc);
    	inputs.put(E_V_PROV, razonSocial);
        return super.execute(inputs);
    }
    
    final class ProveedorMapper implements RowMapper {
    	
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        	
        	CotizacionProveedor cotizacionProveedor = new CotizacionProveedor();
        	
        	cotizacionProveedor.setCodProv(rs.getString("CODIGO") == null ? "": rs.getString("CODIGO"));
        	cotizacionProveedor.setRazonSocial(rs.getString("RAZON_SOCIAL") == null ? "": rs.getString("RAZON_SOCIAL"));
        	cotizacionProveedor.setDscTipoPersona(rs.getString("DSC_TIPO_PERSONA") == null ? "": rs.getString("DSC_TIPO_PERSONA"));
        	cotizacionProveedor.setDscCalificacion(rs.getString("DSC_TIPO_CALIFIACION") == null ? "": rs.getString("DSC_TIPO_CALIFIACION"));
        	
            return cotizacionProveedor;
        }
    }
}
