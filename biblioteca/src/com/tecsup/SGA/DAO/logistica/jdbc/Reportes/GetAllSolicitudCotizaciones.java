package com.tecsup.SGA.DAO.logistica.jdbc.Reportes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class GetAllSolicitudCotizaciones extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_reportes.sp_sel_sol_cotizacion";
	
	private static final String E_C_SEDE = "E_C_SEDE"; 
	private static final String E_C_TIPO_REQ = "E_C_TIPO_REQ"; 
	private static final String E_C_TIPO_BIEN = "E_C_TIPO_BIEN";
	private static final String E_C_TIPO_SERV = "E_C_TIPO_SERV";
	private static final String E_C_FEC_INI = "E_C_FEC_INI";
	private static final String E_C_FEC_FIN = "E_C_FEC_FIN";
	private static final String E_C_COD_PROVEEDOR = "E_C_COD_PROVEEDOR"; 
		
	private static final String RECORDSET = "S_C_RECORDSET";
	
	public GetAllSolicitudCotizaciones(DataSource dataSource) {
	super(dataSource, SPROC_NAME);
	declareParameter(new SqlParameter(E_C_SEDE, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_REQ, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_BIEN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_TIPO_SERV, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_INI, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_FEC_FIN, OracleTypes.CHAR));
	declareParameter(new SqlParameter(E_C_COD_PROVEEDOR, OracleTypes.CHAR));
			
	declareParameter(new SqlOutParameter(RECORDSET, OracleTypes.CURSOR, new LogisticaMapper()));
	compile();
	}
	
	public Map execute(String codSede, String codTipoReq, String codTipoBien, String codTipoServicio, 
			String fecInicio, String fecFin, String codProveedor) {
	
	System.out.println("codSede: "+codSede);
	System.out.println("codTipoReq: "+codTipoReq);
	System.out.println("codTipoBien: "+codTipoBien);
	System.out.println("codTipoServicio: "+codTipoServicio);
	System.out.println("fecInicio: "+fecInicio);
	System.out.println("fecFin: "+fecFin);
	System.out.println("codProveedor: "+codProveedor);
	
	Map inputs = new HashMap();
	
	inputs.put(E_C_SEDE, codSede);
	inputs.put(E_C_TIPO_REQ, codTipoReq);
	inputs.put(E_C_TIPO_BIEN, codTipoBien);
	inputs.put(E_C_TIPO_SERV, codTipoServicio);
	inputs.put(E_C_FEC_INI, fecInicio);
	inputs.put(E_C_FEC_FIN, fecFin);
	inputs.put(E_C_COD_PROVEEDOR, codProveedor);
			
	return super.execute(inputs);
	}
	
	final class LogisticaMapper implements RowMapper {
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	ReportesLogistica reportesLogistica = new ReportesLogistica();
    //16
    reportesLogistica.setDscTipoCotizacion(rs.getString("DSC_TIPO_COTI") == null ? "": rs.getString("DSC_TIPO_COTI"));
    reportesLogistica.setDscTipoBienServ(rs.getString("DSC_TIPO_BIEN_SERV") == null ? "": rs.getString("DSC_TIPO_BIEN_SERV"));
	reportesLogistica.setDscTipoPago(rs.getString("DSC_TIPO_PAGO") == null ? "": rs.getString("DSC_TIPO_PAGO"));
	reportesLogistica.setNroCotizacion(rs.getString("NRO_COTIZACION") == null ? "": rs.getString("NRO_COTIZACION"));
	reportesLogistica.setFecEnvioCotizacion(rs.getString("FEC_ENV_COTI") == null ? "": rs.getString("FEC_ENV_COTI"));	
	reportesLogistica.setFecInicioVigencia(rs.getString("FEC_INI_VIG") == null ? "": rs.getString("FEC_INI_VIG"));
	reportesLogistica.setFecFinVigencia(rs.getString("FEC_FIN_VIG") == null ? "": rs.getString("FEC_FIN_VIG"));	
	reportesLogistica.setDscEstadoCotizacion(rs.getString("DSC_EST_COTI") == null ? "": rs.getString("DSC_EST_COTI"));	
	reportesLogistica.setCodBien(rs.getString("COD_BIEN") == null ? "": rs.getString("COD_BIEN"));
	reportesLogistica.setDscBienServicio(rs.getString("DSC_BIEN_SERV") == null ? "": rs.getString("DSC_BIEN_SERV"));
	reportesLogistica.setDscUnidad(rs.getString("DSC_UNIDAD") == null ? "": rs.getString("DSC_UNIDAD"));
	reportesLogistica.setCantidad(rs.getString("CANTIDAD") == null ? "": rs.getString("CANTIDAD"));
	reportesLogistica.setDscMoneda(rs.getString("DSC_MONEDA") == null ? "": rs.getString("DSC_MONEDA"));
	reportesLogistica.setCotiGanador(rs.getString("COTI_GANADOR") == null ? "": rs.getString("COTI_GANADOR"));
	reportesLogistica.setDscProveedorGanador(rs.getString("PROV_GANADOR") == null ? "": rs.getString("PROV_GANADOR"));
	reportesLogistica.setListadoCotizaciones(rs.getString("LISTADO_COTIZACIONES") == null ? "": rs.getString("LISTADO_COTIZACIONES")); 
	
	return reportesLogistica;
	}
}

}
