package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteSolRequerimientoDetalle extends StoredProcedure{
			
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
		 	+ ".pkg_log_sol_requerimientos.sp_del_det_sol_req";
	/*
	 * PROCEDURE SP_DEL_DET_SOL_REQ(
		E_C_CODREQ IN CHAR,
		E_C_CODDETSOLREQ IN CHAR,
		E_C_CODUSUARIO CHAR,
		S_V_RETVAL OUT VARCHAR2)
	 */
		private static final String E_C_CODREQ = "E_C_CODREQ";
		private static final String E_C_CODDETSOLREQ = "E_C_CODDETSOLREQ";
		private static final String E_C_CODUSUARIO = "E_C_CODUSUARIO";
		private static final String S_V_RETVAL = "S_V_RETVAL";
		
		public DeleteSolRequerimientoDetalle(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_C_CODDETSOLREQ, OracleTypes.VARCHAR));
		declareParameter(new SqlParameter(E_C_CODUSUARIO, OracleTypes.CHAR));
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
		
		public Map execute(String codReq,String codDetSolReq, String codUsuario) {
		
		Map inputs = new HashMap();
		
		inputs.put(E_C_CODREQ, codReq);
		inputs.put(E_C_CODDETSOLREQ, codDetSolReq);
		inputs.put(E_C_CODUSUARIO, codUsuario);
		return super.execute(inputs);
		
		}
}
