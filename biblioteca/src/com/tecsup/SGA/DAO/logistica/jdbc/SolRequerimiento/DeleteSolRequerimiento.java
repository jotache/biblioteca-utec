package com.tecsup.SGA.DAO.logistica.jdbc.SolRequerimiento;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.tecsup.SGA.common.CommonConstants;

public class DeleteSolRequerimiento extends StoredProcedure{
	private static final String SPROC_NAME = CommonConstants.ESQ_LOGISTICA 
	+ ".pkg_log_sol_requerimientos.SP_DEL_SOL_REQ";
	
	private static final String E_C_CODSOLREQ = "E_C_CODSOLREQ";
	private static final String E_V_CODUSUARIO = "E_V_CODUSUARIO";
	
	private static final String S_V_RETVAL = "S_V_RETVAL";
	
	public DeleteSolRequerimiento(DataSource dataSource) {
		super(dataSource, SPROC_NAME);
		declareParameter(new SqlParameter(E_C_CODSOLREQ, OracleTypes.CHAR));
		declareParameter(new SqlParameter(E_V_CODUSUARIO, OracleTypes.CHAR));
		
		declareParameter(new SqlOutParameter(S_V_RETVAL, OracleTypes.VARCHAR));
		compile();
		}
	public Map execute(String codReque, String codUsuario) {
    	
		System.out.println("codReque: "+codReque);
		System.out.println("codUsuario: "+codUsuario);
		
    	Map inputs = new HashMap();
    	
    	inputs.put(E_C_CODSOLREQ, codReque);
    	inputs.put(E_V_CODUSUARIO, codUsuario);
    	
        return super.execute(inputs);
    }
}
