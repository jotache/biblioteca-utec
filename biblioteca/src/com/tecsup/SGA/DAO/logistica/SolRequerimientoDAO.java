package com.tecsup.SGA.DAO.logistica;

import java.util.*;

import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.modelo.ReportesLogistica;

public interface SolRequerimientoDAO {
	
	
	/**
	 * @param codSede
	 * @param tipoReq
	 * @param nroReq
	 * @param fecIni
	 * @param fecFin
	 * @param cecoReq
	 * @param estadoReq
	 * @param usuSolicitante
	 * @return Devuelve las solicitudes de requerimiento de un usuario.
	 */
	public List<SolRequerimiento> getAllSolReqUsuario(String codSede, String tipoReq, String nroReq
    		, String fecIni, String fecFin, String cecoReq
    		, String estadoReq, String usuSolicitante);
	public String InsertDocRelDetSolReqAdjunto(String codReque, String codDetalleReq,
			String codDocAdjunto, String rutaDoc, String codTipoAdjunto, String codUsuario);
	
	public List<SolRequerimiento> getSolReqUsuario(String codSolReq);
	
	public List<SolRequerimientoDetalle> getAllSolReqDetalle(String codSolReq, String codDetSolReq);

	//ALQD,31/10/08. AGREGANDO PARAMETRO INDPARASTOCK
	public String InsertSolRequerimientoUsuario(String codRequerimiento, String codCentroCosto, String sede, 
			String usuarioSolicita, String tipoRequerimiento, String subTipoRequerimiento, String codRequerimientoOri,
			String indInversion,String indParaStock);
	
	public String DeleteSolRequerimientoDetalle(String codReq, String codDetSolReq, String codUsuario);
	
	public String InsertSolRequerimientoDetalle(String codRequerimiento, String codDetSolReq, String nombre, 
			String descripcion, String codBien, String cantidad, String tipoGasto, String fecEntrega, String codUsuario);
	
	/**
	 * @param codReq
	 * @param CodDetReq
	 * @param CodAdj
	 * @return Lista de documentos relacionados a un detalle.
	 */
	public List<SolRequerimientoAdjunto> getAllRequerimientosAdjunto(String codReq, String CodDetReq, String CodAdj);
	
	
	/**
	 * @param codReq
	 * @param codDetReq
	 * @param codAdjunto
	 * @return Valor de exito o erro de operacion.
	 */
	public String deleteRequerimientosAdjunto(String codReq, String codDetReq, String codAdjunto, String codUsuario);
	
	public List<SolRequerimiento> GetAllAproRequerimientoUsuario(String codSUsuario,
			String codPerfil, String tipoReq, String nroReq, String fechaInicio,
			String fechaFin, String codCentroCosto, String codEstado, String indGrupo
			, String codSede);
	
	public List GetAllAsigResp(String tipo, String sede, String ceco
    		,String nombre,String apPaterno,String apMaterno);
	public String InsertResponsable(String codRequerimiento, String codResp, String usuario,String codEsfuerzo);
	
	public List GetAllDetalle(String codResp);
	
	public String EnviarSolRequerimientoUsuario(String codRequerimiento, String codUsuario);
	
	//ALQD,20/01/10.GUARDANDO TRES CAMPOS MAS: CCOSTO, INDINVERSION E INDPARASTOCK
	public String AprobarSolRequerimientoUsuario(String codRequerimiento,String codUsuario
		,String indicadorAccion,String cadenaCantidad,String codPerfil
		,String codCenCosto,String indInversion,String indParaStock);
	
	public String RechazarSolRequerimientoUsuario(String codRequerimiento, String codUsuario);
	
	public String DeleteSolRequerimiento(String codReq, String codUsuario);
	
	public String InsertResponsableTrabajo(String codRequerimiento, String codResp,String codEsf, String usuario);
	
	public String InsertSolDevolucion(String codDevolucion, String codRequerimiento, 
			String cadenaCodigo, String cadenaCantidad, String cantidad, 
			String motivoDevolucion, String codUsuario);
	
	public String EnviarSolDevolucionUsuario(String codDevolucion, String codUsuario);
	
	public String GetEstadoAlmacen(String codSede);
	
	public List GetAllCentrosCostos(String codSede,String descripcion);
	
	public String GetIndEmpLogistica(String codUsuario);
	//ALQD,27/01/09. NUEVA FUNCION QUE RECUPERA LOS REQS PENDIENTE DE ENTREGA
	public List<ReportesLogistica> GetAllSolReqAprPenEntUsuario(String codUsuario);
	//ALQD,18/01/10. NUEVO METODO PARA CONSULTAR REQS APROBADOS
	public List<ReportesLogistica> GetAllSolReqAprobados(String codUsuario);

	/**
	 * JHPR 15/09/2010 Reporte de Flujo de Procesos de Logistica
	 * @param sede
	 * @param fecIni
	 * @param fecFin
	 * @param tipoInversion
	 * @param codUsuario
	 * @param nomComprador
	 * @return List<SolRequerimiento>
	 */
	public List<SolRequerimiento> getFlujoProcesosLogistica(String sede, String fecIni, String fecFin, String tipoInversion, String codUsuario,String nomComprador);
}
