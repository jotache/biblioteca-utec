package com.tecsup.SGA.DAO.logistica;

import java.util.List;

import com.tecsup.SGA.modelo.DetalleDocPago;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public interface GestionDocumentosDAO {
	public List GetAllActivosDisponibles(String codBien,String codSede);
	//ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO PARA BUSCAR
	//ALQD,22/01/09. A�ADIENDO NUEVO PARAMETRO PARA FILTRAR
	public List<OrdenesAprobadas> GetAllOrdenesAprobadas(String codSede, String codUsuario, String codPerfil, String nroOrden,
			String fecEmisionInicio, String fecEmisionFin, String nroRucProve,String nombreProve, 
			String codComprador, String nomComprador, String codTipoOrden, 
			String indSoloCajaChica, String indSinEnvio, String numFactura,
			String indIngresoPorConfirmar);
	
	public List<OrdenesAprobadas> GetAllDatosOrden(String codOrden);
	
	public List<OrdenesAprobadas> GetAllAprobarDocumentosPago(String nroDocumento, String fecEmiIni1, String fecEmiIni2, String nroDocProveedor, 
	   		 String razonSocialProveedor, String codTipoEstado, String nroOrden, String fecEmiFin1, 
			 String fecEmiFin2,String perfil, String codSede, String codUsuario);
	
	public List<OrdenesAprobadas> GetAllDocPagosAsociados(String codOrden);
	
	public List GetAllDetalleDocPago(String docId);
	
	public String ActValidarDocPago(String codDocPago,String usuario);
	
	public String ActRechazarDocPago(String codDocPago,String usuario);
	
	public List GetAllDocPago(String ordenId,String docId);
	
	public List<DetalleDocPago> GetAllDetDocPagosAsociados(String codOrden);
	
	public String UpdateDocumentoPago(String codOrden, String nroDocumento, String fecEmisionDoc, 
			String fecVencimientoDoc, String nroGuia, String fecEmisionGuia, String cadCantidad, 
			String cadObservaciones, String codUsuario, String fecGuiaRecepcion);
	
	public String UpdateAnularDocPago(String codDocumento, String codUsuario);
	
	public String UpdateEnviarDocPago(String codDocumentoPago, String codUsuario);
}
