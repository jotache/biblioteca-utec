package com.tecsup.SGA.DAO.logistica;

import java.util.List;

public interface LogisticaDAO {

	
	public List GetAllTablaDetalle(String padre_Id, String codId
    	,String codTabla, String codigo, String descripcion, String tipo);

	public List GetAllSubFamilias(String bien, String familia, String tipo);
	
	public List GetAllProveedor(String codProv, String ruc, String raSoc
			,String tipoCalif,String estado);
	
	public String InsertProveedor(String codTipoCalif, String codTipoProv, String tipoDoc
			,String nroDoc, String razSoc,String razSocCorto,String email,String direc,String codDpto,String codProv
			,String codDsto,String atencion,String codGiro,String codEstado,String banco,String tipCta,String nroCta
			,String codMon,String usuario);
	
	public String InsertTablaDetalle(String codTipoId, String codHijo, String descripcion
			,String valor3, String valor4,String valor5,String usuario);

}
