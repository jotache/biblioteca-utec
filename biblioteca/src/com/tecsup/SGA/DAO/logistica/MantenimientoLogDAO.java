package com.tecsup.SGA.DAO.logistica;

import java.util.List;

import com.tecsup.SGA.modelo.PresupuestoCentroCosto;

public interface MantenimientoLogDAO {

	public List GetAllTablaDetalle(String padre_Id, String codId
    	,String codTabla, String codigo, String descripcion, String tipo);

	public List GetAllSubFamilias(String bien, String familia, String tipo,String codSubFamilia,String dscFamilia,String dscSubFamilia);
	
	public List GetAllProveedor(String codProv, String ruc, String raSoc
			,String tipoCalif,String estado, String tipoProveedor);
	
	//ALQD,03/07/09.AGREGANDO DOS NUEVOS PARAMETROS
	public String InsertProveedor(String codTipoCalif,String codTipoProv,String tipoDoc
			,String nroDoc,String razSoc,String razSocCorto,String email,String direc,String codDpto,String codProv
			,String codDsto,String atencion,String codGiro,String codEstado,String banco,String tipCta,String nroCta
			,String codMon,String usuario,String pais,String telefono);
	
	public String InsertTablaDetalle(String codTipoId, String codHijo, String descripcion
			,String valor3, String valor4,String valor5,String usuario);
	
	public String DeleteProveedor(String codPro, String usuario);
	
	public String DeleteProducto(String codPro, String usuario);
	
	public String DeleteFamiliaByProveedor(String codFamProv, String usuario);

	public String DeleteTablaDetalle(String tipId,String codUnico, String usuario);
	
	public String UpdateTablaDetalle(String codTipoId, String codHijo,String codReg,String codGen, String descripcion
			,String valor3, String valor4,String valor5,String usuario);
	
	//ALQD,03/07/09.AGREGANDO DOS NUEVOS PARAMETROS
	public String UpdateProveedor(String codProveedor, String codTipCal,String codTipProv,String tipDoc, String nroDoc
			,String razSoc, String razSocCorto,String email,String direc,String codDpto,String codProv,String codDsto
			,String atencionA,String codGiro,String codEstado,String banco,String tipCta,String nroCta,String codMoneda
			,String usuario,String pais,String telefono);
	
	public List GetAllProducto(String codCatago,String codSede, String codFam
	    	,String codSubFam, String codPro, String nomPro, String codTipo
	    	,String nroSerie, String descriptor,String estado);
	//ALQD,23/02/09.NUEVO PARAMETRO
	//ALQD,24/02/09.NUEVO PARAMETRO PARA ACTUALIZAR PRECIO REF.
	//ALQD,20/05/09.NUEVA PROPIEDAD NO AFECTO
	public String InsertProducto(String codBien, String sede, String familia
			,String subFamilia, String nroProducto,String decrip,String unidad,
			String tipoBien,String ubicacion,String ubiFila,String ubiColum,
			String stockMin,String stockMax,String nroDiasAten,String marca,
			String modelo,String usuario,String inversion,String stockMostrar,
			String estado,String artPromocional,String preURef,String artNoAfecto);

	public String InsertImageProducto(String codBien, String imagen,String usuario);
	
	public String InsertDocProducto(String codBien, String tipoDoc,String nomDoc,String usuario);
	
	public List GetAllDocCatalogo(String codBien,String codAdj);
	
	public String DeleteDocProducto(String codBien,String codAdj, String usuario);
	
	//ALQD,20/08/09.ELIMINA FOTO DEL PRODUCTO
	public String DeleteFotoProducto(String codBien, String usuario);

	public List GetAllAtencionSolicitudes(String codSede,String tipoReq, String subtipoReq, String codResponsable
    		,String nomResponsable,String familia,String subfamilia);
	
	public String InsertAtencionSolicitante(String tipoReq, String codigo, String codReq,String usuario,String codSede);
	
	public List GetAllEmpleados(String tipoPer, String nombre, String apPat
    		,String apMat, String codSede);
	
	public String InsertFlujo(String codTabla, String codDetalle, String indicador,String usuario,String codSede);
	
	public String DeleteAtencionSolicitudes(String tipReq,String codigo,String codRes, String usuario,String codSede);

	public String InsDescriptoresByBien(String codBien, String codDes,String usuario);

	public List GetAllDescriptoresByBien(String codBien);
	
	public List GetAllSubFamliaByPro(String codProv);
	
	public String InsertSubFamiliaByPro(String codPro, String codSFam,String usuario);
	
	public String DeleteDescriptoresByBien(String codBien,String codDet, String usuario);

	public String ActIndiceFlujo(String codTabla, String codDetalle, String indicador);

	public List GetAllFlujosAprobacion(String codSede, String codFlujo);
	
	public String GetSedeByUsuario(String codUsuario);
	//ALQD,11/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,30/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,21/07/09.AGREGANDO 3 NUEVOS PARAMETROS
	public String InsertDocAsociado(String codDocuemnto, String codDocPago
			, String nroDocumento,String fecDocuemnto
			, String montoDocumento,String dscDoc,String codUsuario
			, String codTipMoneda, String impTipCamSoles, String codProvee
			, String nomProvee, String rucProvee);
	
	public String DeleteDocAsociado(String codDocumento, String codUsuario);
	
	public List GetAllDocAsociado(String codDoc);
	
	public List<PresupuestoCentroCosto> listaPresupuestosCenCos(String anio, String sede);
	
	public String InsertPresupuesto(String sede, String anio, String cadenaDatos,
			String codUsuario, String nroReg);
}
