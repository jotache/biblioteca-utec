package com.tecsup.SGA.DAO;

import java.util.*;
import com.tecsup.SGA.modelo.CentroCosto;
public interface CentroCostoDAO {
	
	/**
	 * @param sede
	 * @return Retorna los centros de costo de una determinada sede.
	 */
	public List<CentroCosto> GetAllCentrosCosto(String sede, String perfil, String codUsuario);
}
