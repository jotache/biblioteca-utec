package com.tecsup.SGA.DAO;

import java.sql.SQLException;
import java.util.List;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.modelo.UsuarioSeguriBib;



public interface SeguridadDAO {
	/**
	 * @param usuario
	 * @param clave
	 * @return Vaklida usuario y pass y devuelve codigo de error
	 */
	public int getValidaUsuario(String usuario, String clave);
	
	public int getValidaUsuarioCeditec(String usuario, String clave);
	/**
	 * @param usuario
	 * @param sistema
	 * @return Opciones de usuario por sistema
	 */
	public List<SeguridadBean> getOpciones(String usuario, String sistema);
	
	public List<SeguridadBean> getOpcionesCeditec(String usuario, String sistema);
	
	/**
	 * @param codUsuario
	 * @return Obtiene los datos del usuario (idUsuario, Nombre)
	 */
	public UsuarioSeguridad getDatosUsuario(String codUsuario);
	
	//JHPR 2008-04-09 Para permitir el cambio de contraseņa.
	public int GrabarFirma(String usuario, String clave);
	
	public int GrabarFirmaCeditec(String usuario, String clave);
	
	//JHPR: 2008-04-11 Registrar inicio de sesion en "seguridad.seg_log"
	public int RegistrarSesion(boolean ingreso, String estacioncliente, String usuario,	String sistema);
	
	/**
	 * @param usuario
	 * @param sistema
	 * @return CACT: 2008-04-15 - Retorna los perfiles de un usuario para un determinado sistema
	 */
	public List<UsuarioPerfil> getPerfiles(String usuario, String sistema);
	
	public UsuarioSeguriBib obtenerUsuarioSeguriBiblio(String codsujeto,String idUsuario);
	
	/**
	 * Obtiene los datos del usuario logeado en SGA-Biblioteca para usarlo como pasarela a otras bibliotecas
	 * @param username
	 * @return
	 */
	public UsuarioSeguridad getDatosUsuarioBiblio(String username);
	public UsuarioSeguridad getDatosUsuarioBiblioCeditec(String username);
	
	public boolean esUsuarioTecsup(String username);
	public UsuarioSeguridad ObtenerEstadoUsuario(UsuarioSeguridad user);
}
