package com.tecsup.SGA.DAO;

import java.util.*;

public interface MaestroDAO {

	public List getAllDepartamento();
	
	public List getAllProvincia(String departamento);
	
	public List getAllDistrito(String provincia);
	
}
