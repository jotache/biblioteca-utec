package com.tecsup.SGA.modelo;

public class CursoEvaluador implements java.io.Serializable{
	
	private static final long serialVersionUID = -20394229229043695L;
	
	private String codCursoEjec;
	private String codPeriodo;
	private String periodo; //periodo
	private String evaluador; //nombre
		
	private String codCurso;
	private String curso;
	
	private String codProducto;
	private String producto;
	
	private String codEspecialidad;
	private String especialidad;
	
	private String sistEval;
	
	/*Alumnos*/
	//TipoSesion
	private String codTipoSesion;
	private String dscTipoSesion;
	
	private String ciclo;
	
	private String codSeccion;
	private String dscSeccion;
	
	private String codAlumno;
	private String nombre;
	private String nroFaltas;
	
	private String porcentaje; //inasistencias
	
	private String cadComponentes;
	
	private String codEvaluador;
	private String ambiente;
	private String tipoSemana;
	private String fecha;
	private String horaInicio;
	private String horaFin;
	
	@Override
	public String toString() {
		return "CursoEvaluador [ambiente=" + ambiente + ", ciclo=" + ciclo
				+ ", codCurso=" + codCurso + ", codCursoEjec=" + codCursoEjec
				+ ", codPeriodo=" + codPeriodo + ", codProducto=" + codProducto
				+ ", codSeccion=" + codSeccion + ", codTipoSesion="
				+ codTipoSesion + ", curso=" + curso + ", dscSeccion="
				+ dscSeccion + ", dscTipoSesion=" + dscTipoSesion
				+ ", especialidad=" + especialidad + ", codEvaluador=" + codEvaluador + ", evaluador=" + evaluador
				+ ", fecha=" + fecha + ", horaFin=" + horaFin + ", horaInicio="
				+ horaInicio + ", periodo=" + periodo + ", sistEval="
				+ sistEval + ", tipoSemana=" + tipoSemana + "]";
	}
	public String getCodCursoEjec() {
		return codCursoEjec;
	}
	
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public void setCodCursoEjec(String codCursoEjec) {
		this.codCursoEjec = codCursoEjec;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getAmbiente() {
		return ambiente;
	}
	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}
	public String getTipoSemana() {
		return tipoSemana;
	}
	public void setTipoSemana(String tipoSemana) {
		this.tipoSemana = tipoSemana;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getCadComponentes() {
		return cadComponentes;
	}
	public void setCadComponentes(String cadComponentes) {
		this.cadComponentes = cadComponentes;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getEvaluador() {
		return evaluador;
	}
	public void setEvaluador(String evaluador) {
		this.evaluador = evaluador;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getSistEval() {
		return sistEval;
	}
	public void setSistEval(String sistEval) {
		this.sistEval = sistEval;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNroFaltas() {
		return nroFaltas;
	}
	public void setNroFaltas(String nroFaltas) {
		this.nroFaltas = nroFaltas;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getDscSeccion() {
		return dscSeccion;
	}
	public void setDscSeccion(String dscSeccion) {
		this.dscSeccion = dscSeccion;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodTipoSesion() {
		return codTipoSesion;
	}
	public void setCodTipoSesion(String codTipoSesion) {
		this.codTipoSesion = codTipoSesion;
	}
	public String getDscTipoSesion() {
		return dscTipoSesion;
	}
	public void setDscTipoSesion(String dscTipoSesion) {
		this.dscTipoSesion = dscTipoSesion;
	}
	
}
