package com.tecsup.SGA.modelo;

public class ProgramaComponentes implements java.io.Serializable{
 
	private String codigoID;
	private String codPrograma;
	private String codCurso;
	private String codCiclo;
	private String codEspecialidad;
	private String codProducto;
	private String codEtapa;
	private String codComponente;
	private String usuCrea;
	private String usuModi;
	private String fechaCrea;
	private String fechaModi;
	private String estReg;
	private String comDescripcion;
	private String flag;
	
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getCodComponente() {
		return codComponente;
	}
	public void setCodComponente(String codComponente) {
		this.codComponente = codComponente;
	}
	public String getUsuCrea() {
		return usuCrea;
	}
	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getFechaCrea() {
		return fechaCrea;
	}
	public void setFechaCrea(String fechaCrea) {
		this.fechaCrea = fechaCrea;
	}
	public String getFechaModi() {
		return fechaModi;
	}
	public void setFechaModi(String fechaModi) {
		this.fechaModi = fechaModi;
	}
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	public String getComDescripcion() {
		return comDescripcion;
	}
	public void setComDescripcion(String comDescripcion) {
		this.comDescripcion = comDescripcion;
	}
	public String getCodigoID() {
		return codigoID;
	}
	public void setCodigoID(String codigoID) {
		this.codigoID = codigoID;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
}
