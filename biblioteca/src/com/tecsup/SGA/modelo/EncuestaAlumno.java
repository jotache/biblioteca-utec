package com.tecsup.SGA.modelo;

public class EncuestaAlumno implements java.io.Serializable{

	private String primerNombre;
	private String segundoNombre;
	private String apellidoMaterno;
	private String apellidoPaterno;
	private String codTipoDoc;
	private String nroTipoDoc;
	private String email;
	private String direccion;
	private String telfCasa;
	private String telfCelular;
	private String codEmpresa;
	private String nombreEmpresa;
	private String nombreArea;
	private String nombreCargo;
	private String correoEmpresa;
	private String nombreContacto;
	private String telfContacto;
	
	private String codPerfil;
	
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getCodTipoDoc() {
		return codTipoDoc;
	}
	public void setCodTipoDoc(String codTipoDoc) {
		this.codTipoDoc = codTipoDoc;
	}
	public String getNroTipoDoc() {
		return nroTipoDoc;
	}
	public void setNroTipoDoc(String nroTipoDoc) {
		this.nroTipoDoc = nroTipoDoc;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelfCasa() {
		return telfCasa;
	}
	public void setTelfCasa(String telfCasa) {
		this.telfCasa = telfCasa;
	}
	public String getTelfCelular() {
		return telfCelular;
	}
	public void setTelfCelular(String telfCelular) {
		this.telfCelular = telfCelular;
	}
	public String getCodEmpresa() {
		return codEmpresa;
	}
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getNombreArea() {
		return nombreArea;
	}
	public void setNombreArea(String nombreArea) {
		this.nombreArea = nombreArea;
	}
	public String getNombreCargo() {
		return nombreCargo;
	}
	public void setNombreCargo(String nombreCargo) {
		this.nombreCargo = nombreCargo;
	}
	public String getCorreoEmpresa() {
		return correoEmpresa;
	}
	public void setCorreoEmpresa(String correoEmpresa) {
		this.correoEmpresa = correoEmpresa;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getTelfContacto() {
		return telfContacto;
	}
	public void setTelfContacto(String telfContacto) {
		this.telfContacto = telfContacto;
	}
}
