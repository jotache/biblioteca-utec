package com.tecsup.SGA.modelo;

public class ParametrosEspeciales implements java.io.Serializable  {
	private String id;
	private String codPeriodo;
	private String codTabla;
	private String valor1;
	private String valor2;
	private String valor3;
	private String valor4;
	private String tipo;
	private String usuCrea;
	private String fecModi;
	private String usuModi;
	private String estReg;
	
	private String codId;
	
	private String descripcionPeriodo;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodTabla() {
		return codTabla;
	}
	public void setCodTabla(String codTabla) {
		this.codTabla = codTabla;
	}
	public String getValor1() {
		return valor1;
	}
	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}
	public String getValor2() {
		return valor2;
	}
	public void setValor2(String valor2) {
		this.valor2 = valor2;
	}
	public String getValor3() {
		return valor3;
	}
	public void setValor3(String valor3) {
		this.valor3 = valor3;
	}
	public String getValor4() {
		return valor4;
	}
	public void setValor4(String valor4) {
		this.valor4 = valor4;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getUsuCrea() {
		return usuCrea;
	}
	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}
	public String getFecModi() {
		return fecModi;
	}
	public void setFecModi(String fecModi) {
		this.fecModi = fecModi;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	public String getDescripcionPeriodo() {
		return descripcionPeriodo;
	}
	public void setDescripcionPeriodo(String descripcionPeriodo) {
		this.descripcionPeriodo = descripcionPeriodo;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	
	
}
