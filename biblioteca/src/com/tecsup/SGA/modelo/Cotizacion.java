package com.tecsup.SGA.modelo;

public class Cotizacion {
	/*
	SC.SCOT_ID AS CODIGO
  , SC.SCOT_SEDE AS SEDE
  , SC.TIPT_TIPO_COTIZACION AS TIPO_COTIZACION
  , SC.TIPT_SUB_TIPO_COTIZACION AS SUB_TIPO_COTIZACION
  , SC.SCOT_NRO_COTIZACION AS NRO_COTIZACION
  , TO_CHAR(SC.SCOT_FEC_EMISION,'DD/MM/YYYY') AS FECHA_EMISION
  , SC.TIPT_TIPO_PAGO AS COD_TIPO_PAGO
  , SC.SCOT_FEC_INICIO AS FECHA_INICIO
  , SC.SCOT_HORA_INICIO AS HORA_INICIO
  , SC.SCOT_FEC_FIN AS FECHA_FIN
  , SC.SCOT_HORA_FIN AS HORA_FIN
  , SC.TIPT_ESTADO AS ESTADO
  , SC.SCOT_FEC_PUBLICACION AS FECHA_PUBLICACION
  , SC.SCOT_FEC_CIERRE AS FECHA_CIERRE
  , SC.SCOT_TOTAL_ASIG_SISTEMA AS ASIGANDO_SISTEMA
  , SC.SCOT_TOTAL_ASIG_USUARIO AS ASIGNADO_USUARIO
  , SC.SCOT_USU_RESPONSABLE AS USU_RESPONSABLE
	 */
	private String codigo;
	private String sede;
	private String codTipoCotizacion;
	private String codSubTipoCotizacion;
	private String nroCotizacion;
	private String fechaEmision;
	private String codTipoPago;
	private String fechaInicio;
	private String horaInicio;
	private String fechaFin;
	private String horaFin;
	private String estado;
	private String fechaPublicacion;
	private String fechaCierre;
	private String totalAsigSistema;
	private String totalAsigUsuario;
	private String usuResponsable;	
	
	private String dscTipoGasto;
	
	private String codOrdenCompra;
	private String nroOrdenCompra;	
	private String dscEstado;
	private String dscTipoPago;
	private String codEstado;
	private String dscProveedor;
	private String email;
	private String dscTipoCotizacion;
	
	private String tipoCoti;
	private String subTipoCoti;
	private String nroCoti;
	//ALQD,16/04/09.A�ADIENDO NUEVA PROPIEDA GRUPO SERVICIO
	private String codGruServicio;
	
	public String getCodGruServicio() {
		return codGruServicio;
	}
	public void setCodGruServicio(String codGruServicio) {
		this.codGruServicio=codGruServicio;
	}
	public String getDscTipoGasto() {
		return dscTipoGasto;
	}
	public void setDscTipoGasto(String dscTipoGasto) {
		this.dscTipoGasto = dscTipoGasto;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getCodTipoCotizacion() {
		return codTipoCotizacion;
	}
	public void setCodTipoCotizacion(String codTipoCotizacion) {
		this.codTipoCotizacion = codTipoCotizacion;
	}
	public String getCodSubTipoCotizacion() {
		return codSubTipoCotizacion;
	}
	public void setCodSubTipoCotizacion(String codSubTipoCotizacion) {
		this.codSubTipoCotizacion = codSubTipoCotizacion;
	}
	public String getNroCotizacion() {
		return nroCotizacion;
	}
	public void setNroCotizacion(String nroCotizacion) {
		this.nroCotizacion = nroCotizacion;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getCodTipoPago() {
		return codTipoPago;
	}
	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public String getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public String getTotalAsigSistema() {
		return totalAsigSistema;
	}
	public void setTotalAsigSistema(String totalAsigSistema) {
		this.totalAsigSistema = totalAsigSistema;
	}
	public String getTotalAsigUsuario() {
		return totalAsigUsuario;
	}
	public void setTotalAsigUsuario(String totalAsigUsuario) {
		this.totalAsigUsuario = totalAsigUsuario;
	}
	public String getUsuResponsable() {
		return usuResponsable;
	}
	public void setUsuResponsable(String usuResponsable) {
		this.usuResponsable = usuResponsable;
	}
	public String getCodOrdenCompra() {
		return codOrdenCompra;
	}
	public void setCodOrdenCompra(String codOrdenCompra) {
		this.codOrdenCompra = codOrdenCompra;
	}
	public String getNroOrdenCompra() {
		return nroOrdenCompra;
	}
	public void setNroOrdenCompra(String nroOrdenCompra) {
		this.nroOrdenCompra = nroOrdenCompra;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getDscTipoPago() {
		return dscTipoPago;
	}
	public void setDscTipoPago(String dscTipoPago) {
		this.dscTipoPago = dscTipoPago;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDscProveedor() {
		return dscProveedor;
	}
	public void setDscProveedor(String dscProveedor) {
		this.dscProveedor = dscProveedor;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDscTipoCotizacion() {
		return dscTipoCotizacion;
	}
	public void setDscTipoCotizacion(String dscTipoCotizacion) {
		this.dscTipoCotizacion = dscTipoCotizacion;
	}
	public String getTipoCoti() {
		return tipoCoti;
	}
	public void setTipoCoti(String tipoCoti) {
		this.tipoCoti = tipoCoti;
	}
	public String getSubTipoCoti() {
		return subTipoCoti;
	}
	public void setSubTipoCoti(String subTipoCoti) {
		this.subTipoCoti = subTipoCoti;
	}
	public String getNroCoti() {
		return nroCoti;
	}
	public void setNroCoti(String nroCoti) {
		this.nroCoti = nroCoti;
	}
	
	
}
