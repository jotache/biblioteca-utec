package com.tecsup.SGA.modelo;

public class DocPago {

	private String factura;
	private String fecEmiFactura;
	private String fecVctoFactura;
	private String nroGuia;
	private String fecEmiGuia;
	private String nroOrden;
	private String fecEmiOrden;
	private String codMoneda;
	private String sigMoneda;
	private String montoOrden;
	private String fecGuiaRecep;
	private String subTotal;
	private String igv;
	private String totalFacturado;
	//ALQD,13/11/08. A�ADIENDO NUEVAS PROPIEDADES
	private String nroInterno;
	private String fecCierre;
	private String estIngreso;
	private String nomProveedor;
	//ALQD,12/06/09.AGREGAR NUEVA PROPIEDAD
	private String indImportacion;
	//ALQD,01/07/09.AGREGANDO NUEVA PROPIEDAD
	private String impOtros;
	
	public String getImpOtros() {
		return impOtros;
	}
	public void setImpOtros(String impOtros) {
		this.impOtros = impOtros;
	}
	public String getIndImportacion() {
		return indImportacion;
	}
	public void setIndImportacion(String indImportacion) {
		this.indImportacion = indImportacion;
	}
	public String getNroInterno() {
		return nroInterno;
	}
	public void setNroInterno(String nroInterno) {
		this.nroInterno=nroInterno;
	}
	public String getFecCierre() {
		return fecCierre;
	}
	public void setFecCierre(String fecCierre) {
		this.fecCierre=fecCierre;
	}
	public String getEstIngreso() {
		return estIngreso;
	}
	public void setEstIngreso(String estIngreso) {
		this.estIngreso=estIngreso;
	}
	public String getNomProveedor() {
		return nomProveedor;
	}
	public void setNomProveedor(String nomProveedor) {
		this.nomProveedor=nomProveedor;
	}
	public String getFactura() {
		return factura;
	}
	public void setFactura(String factura) {
		this.factura = factura;
	}
	public String getFecEmiFactura() {
		return fecEmiFactura;
	}
	public void setFecEmiFactura(String fecEmiFactura) {
		this.fecEmiFactura = fecEmiFactura;
	}
	public String getFecVctoFactura() {
		return fecVctoFactura;
	}
	public void setFecVctoFactura(String fecVctoFactura) {
		this.fecVctoFactura = fecVctoFactura;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getFecEmiGuia() {
		return fecEmiGuia;
	}
	public void setFecEmiGuia(String fecEmiGuia) {
		this.fecEmiGuia = fecEmiGuia;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFecEmiOrden() {
		return fecEmiOrden;
	}
	public void setFecEmiOrden(String fecEmiOrden) {
		this.fecEmiOrden = fecEmiOrden;
	}
	public String getCodMoneda() {
		return codMoneda;
	}
	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}
	public String getSigMoneda() {
		return sigMoneda;
	}
	public void setSigMoneda(String sigMoneda) {
		this.sigMoneda = sigMoneda;
	}
	public String getMontoOrden() {
		return montoOrden;
	}
	public void setMontoOrden(String montoOrden) {
		this.montoOrden = montoOrden;
	}
	public String getFecGuiaRecep() {
		return fecGuiaRecep;
	}
	public void setFecGuiaRecep(String fecGuiaRecep) {
		this.fecGuiaRecep = fecGuiaRecep;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getIgv() {
		return igv;
	}
	public void setIgv(String igv) {
		this.igv = igv;
	}
	public String getTotalFacturado() {
		return totalFacturado;
	}
	public void setTotalFacturado(String totalFacturado) {
		this.totalFacturado = totalFacturado;
	}
	
}
