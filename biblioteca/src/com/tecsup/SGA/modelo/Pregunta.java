package com.tecsup.SGA.modelo;

public class Pregunta implements java.io.Serializable{
	private String codGrupo;
	private String codPregunta;
	private String nomPregunta;
	private String codTipoPregunta;
	private String nomTipoPregunta;
	private String indObligatorio;
	private String indUnica;
	private String pesoPregunta;
	private String indAlternativa;
	private String alternativasRel;
	private String desAlternativa;
	
	public String getDesAlternativa() {
		return desAlternativa;
	}
	public void setDesAlternativa(String desAlternativa) {
		this.desAlternativa = desAlternativa;
	}
	public String getCodGrupo() {
		return codGrupo;
	}
	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}
	public String getCodPregunta() {
		return codPregunta;
	}
	public void setCodPregunta(String codPregunta) {
		this.codPregunta = codPregunta;
	}
	public String getNomPregunta() {
		return nomPregunta;
	}
	public void setNomPregunta(String nomPregunta) {
		this.nomPregunta = nomPregunta;
	}
	public String getCodTipoPregunta() {
		return codTipoPregunta;
	}
	public void setCodTipoPregunta(String codTipoPregunta) {
		this.codTipoPregunta = codTipoPregunta;
	}
	public String getNomTipoPregunta() {
		return nomTipoPregunta;
	}
	public void setNomTipoPregunta(String nomTipoPregunta) {
		this.nomTipoPregunta = nomTipoPregunta;
	}
	public String getIndObligatorio() {
		return indObligatorio;
	}
	public void setIndObligatorio(String indObligatorio) {
		this.indObligatorio = indObligatorio;
	}
	public String getIndUnica() {
		return indUnica;
	}
	public void setIndUnica(String indUnica) {
		this.indUnica = indUnica;
	}
	public String getPesoPregunta() {
		return pesoPregunta;
	}
	public void setPesoPregunta(String pesoPregunta) {
		this.pesoPregunta = pesoPregunta;
	}
	public String getIndAlternativa() {
		return indAlternativa;
	}
	public void setIndAlternativa(String indAlternativa) {
		this.indAlternativa = indAlternativa;
	}
	public String getAlternativasRel() {
		return alternativasRel;
	}
	public void setAlternativasRel(String alternativasRel) {
		this.alternativasRel = alternativasRel;
	}

		
	
	
	
	
	
		
}
