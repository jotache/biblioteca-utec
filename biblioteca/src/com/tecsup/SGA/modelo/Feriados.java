package com.tecsup.SGA.modelo;

public class Feriados implements java.io.Serializable{
	private String mes;
	private String dia;
	private String anio;
	private String codigoSecuencial;
	private String descripcion;
	
	private String codMes;
	
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getCodigoSecuencial() {
		return codigoSecuencial;
	}
	public void setCodigoSecuencial(String codigoSecuencial) {
		this.codigoSecuencial = codigoSecuencial;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodMes() {
		return codMes;
	}
	public void setCodMes(String codMes) {
		this.codMes = codMes;
	}
	
}
