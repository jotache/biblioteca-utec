package com.tecsup.SGA.modelo;

public class ResumenPendientePorCotizar {
	private String cantPendConsNormal;
	private String cantPendConsInversion;
	private String cantPendActivo;
	private String cantPendServGeneral;
	private String cantPendServMantto;
	private String cantPendServOtro;
	
	public String getCantPendConsNormal() {
		return cantPendConsNormal;
	}
	public void setCantPendConsNormal(String cantPendConsNormal) {
		this.cantPendConsNormal = cantPendConsNormal;
	}
	public String getCantPendConsInversion() {
		return cantPendConsInversion;
	}
	public void setCantPendConsInversion(String cantPendConsInversion) {
		this.cantPendConsInversion = cantPendConsInversion;
	}
	public String getCantPendActivo() {
		return cantPendActivo;
	}
	public void setCantPendActivo(String cantPendActivo) {
		this.cantPendActivo= cantPendActivo;
	}
	public String getCantPendServGeneral() {
		return cantPendServGeneral;
	}
	public void setCantPendServGeneral(String cantPendServGeneral) {
		this.cantPendServGeneral= cantPendServGeneral;
	}
	public String getCantPendServMantto() {
		return cantPendServMantto;
	}
	public void setCantPendServMantto(String cantPendServMantto) {
		this.cantPendServMantto= cantPendServMantto;
	}
	public String getCantPendServOtro() {
		return cantPendServOtro;
	}
	public void setCantPendServOtro(String cantPendServOtro) {
		this.cantPendServOtro= cantPendServOtro;
	}
}
