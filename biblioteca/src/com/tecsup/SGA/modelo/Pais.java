package com.tecsup.SGA.modelo;

public class Pais implements java.io.Serializable{
 
	private String paisId;
	private String paisCodigo;
	private String paisDescripcion;
	private String flag;
	private String usuCrea;
	
	public String getUsuCrea() {
		return usuCrea;
	}
	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getPaisId() {
		return paisId;
	}
	public void setPaisId(String paisId) {
		this.paisId = paisId;
	}
	public String getPaisCodigo() {
		return paisCodigo;
	}
	public void setPaisCodigo(String paisCodigo) {
		this.paisCodigo = paisCodigo;
	}
	public String getPaisDescripcion() {
		return paisDescripcion;
	}
	public void setPaisDescripcion(String paisDescripcion) {
		this.paisDescripcion = paisDescripcion;
	}
}
