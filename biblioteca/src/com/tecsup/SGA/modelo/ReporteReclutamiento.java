package com.tecsup.SGA.modelo;

public class ReporteReclutamiento implements java.io.Serializable{

	private String codProceso;
	private String dscTipoProceso;
	private String dscProceso;
	private String dscUniFuncional;
	private String dscEstado;
	private String fecInicio;
	private String fecFin;
	private String numPostulantes;
	private String numSeleccionados;
	private String indTieneEncuesta;
	private String fecProceso;
	public String getFecProceso() {
		return fecProceso;
	}
	public void setFecProceso(String fecProceso) {
		this.fecProceso = fecProceso;
	}
	private String codPostulante;
	private String dscPostulante;
	private String dscEstadoCV;
	
	private String codOferta;
	private String dscOferta;
	
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getDscTipoProceso() {
		return dscTipoProceso;
	}
	public void setDscTipoProceso(String dscTipoProceso) {
		this.dscTipoProceso = dscTipoProceso;
	}
	public String getDscProceso() {
		return dscProceso;
	}
	public void setDscProceso(String dscProceso) {
		this.dscProceso = dscProceso;
	}
	public String getDscUniFuncional() {
		return dscUniFuncional;
	}
	public void setDscUniFuncional(String dscUniFuncional) {
		this.dscUniFuncional = dscUniFuncional;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecFin() {
		return fecFin;
	}
	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}
	public String getNumPostulantes() {
		return numPostulantes;
	}
	public void setNumPostulantes(String numPostulantes) {
		this.numPostulantes = numPostulantes;
	}
	public String getNumSeleccionados() {
		return numSeleccionados;
	}
	public void setNumSeleccionados(String numSeleccionados) {
		this.numSeleccionados = numSeleccionados;
	}
	public String getIndTieneEncuesta() {
		return indTieneEncuesta;
	}
	public void setIndTieneEncuesta(String indTieneEncuesta) {
		this.indTieneEncuesta = indTieneEncuesta;
	}
	public String getCodPostulante() {
		return codPostulante;
	}
	public void setCodPostulante(String codPostulante) {
		this.codPostulante = codPostulante;
	}
	public String getDscPostulante() {
		return dscPostulante;
	}
	public void setDscPostulante(String dscPostulante) {
		this.dscPostulante = dscPostulante;
	}
	public String getDscEstadoCV() {
		return dscEstadoCV;
	}
	public void setDscEstadoCV(String dscEstadoCV) {
		this.dscEstadoCV = dscEstadoCV;
	}
	public String getCodOferta() {
		return codOferta;
	}
	public void setCodOferta(String codOferta) {
		this.codOferta = codOferta;
	}
	public String getDscOferta() {
		return dscOferta;
	}
	public void setDscOferta(String dscOferta) {
		this.dscOferta = dscOferta;
	}
}
