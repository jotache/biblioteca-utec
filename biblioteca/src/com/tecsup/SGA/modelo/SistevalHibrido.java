package com.tecsup.SGA.modelo;

public class SistevalHibrido implements java.io.Serializable{
	private String codSec;
	private String descripcion;
	private String codTipoEvaluacion;
	private String descTipoEvaluacion;	
	private String peso;		
	private String fechaRegistro;
	private String nroProductos;
	private String cadEvaluacion;
	private String usuario;
	private String codGenerado;
	//****************************
	private String lista;
	
	public String getLista() {
		return lista;
	}
	public void setLista(String lista) {
		this.lista = lista;
	}
	public String getCodGenerado() {
		return codGenerado;
	}
	public void setCodGenerado(String codGenerado) {
		this.codGenerado = codGenerado;
	}
	public String getCadEvaluacion() {
		return cadEvaluacion;
	}
	public void setCadEvaluacion(String cadEvaluacion) {
		this.cadEvaluacion = cadEvaluacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCodSec() {
		return codSec;
	}
	public void setCodSec(String codSec) {
		this.codSec = codSec;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodTipoEvaluacion() {
		return codTipoEvaluacion;
	}
	public void setCodTipoEvaluacion(String codTipoEvaluacion) {
		this.codTipoEvaluacion = codTipoEvaluacion;
	}
	public String getDescTipoEvaluacion() {
		return descTipoEvaluacion;
	}
	public void setDescTipoEvaluacion(String descTipoEvaluacion) {
		this.descTipoEvaluacion = descTipoEvaluacion;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getNroProductos() {
		return nroProductos;
	}
	public void setNroProductos(String nroProductos) {
		this.nroProductos = nroProductos;
	}			
}
