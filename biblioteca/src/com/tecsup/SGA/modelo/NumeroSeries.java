package com.tecsup.SGA.modelo;

public class NumeroSeries implements java.io.Serializable{

	private String codBien;
	private String codIngreso;
	private String codDocPago;
	private String nroSerie;
	private String valorCompra;
	private String indSel;
	
	public String getIndSel() {
		return indSel;
	}
	public void setIndSel(String indSel) {
		this.indSel = indSel;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getCodIngreso() {
		return codIngreso;
	}
	public void setCodIngreso(String codIngreso) {
		this.codIngreso = codIngreso;
	}
	public String getCodDocPago() {
		return codDocPago;
	}
	public void setCodDocPago(String codDocPago) {
		this.codDocPago = codDocPago;
	}
	public String getNroSerie() {
		return nroSerie;
	}
	public void setNroSerie(String nroSerie) {
		this.nroSerie = nroSerie;
	}
	public String getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(String valorCompra) {
		this.valorCompra = valorCompra;
	}
	
	
}
