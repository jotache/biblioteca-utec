package com.tecsup.SGA.modelo;

public class Evaluacion implements java.io.Serializable {
	/*	ATRIBUTOS	*/
	String codEvaluacion;
	String codProceso;
	String codPostulante;
	String codEtapa;
	String codUsuario;
	String codTipoEvaluacion;
	String codCalificaion;
	String comentario;
	String codEstadoEvento;
	String fecEstadoEvento;
	String codCalificacionFinEtapa;
	String fecCalificacionFinEtapa;
	String comentarioFinEtapa;
	String flagEstadoActual;
	String codRelacion;
	String nombreEvaluador;
	String nombreProceso;
	String desTipoEvaluacion;
	/*	GETTERS Y SETTERS	*/

	public String getCodEvaluacion() {
		return codEvaluacion;
	}
	public void setCodEvaluacion(String codEvaluacion) {
		this.codEvaluacion = codEvaluacion;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getCodPostulante() {
		return codPostulante;
	}
	public void setCodPostulante(String codPostulante) {
		this.codPostulante = codPostulante;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodTipoEvaluacion() {
		return codTipoEvaluacion;
	}
	public void setCodTipoEvaluacion(String codTipoEvaluacion) {
		this.codTipoEvaluacion = codTipoEvaluacion;
	}
	public String getCodCalificaion() {
		return codCalificaion;
	}
	public void setCodCalificaion(String codCalificaion) {
		this.codCalificaion = codCalificaion;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getCodEstadoEvento() {
		return codEstadoEvento;
	}
	public void setCodEstadoEvento(String codEstadoEvento) {
		this.codEstadoEvento = codEstadoEvento;
	}
	public String getFecEstadoEvento() {
		return fecEstadoEvento;
	}
	public void setFecEstadoEvento(String fecEstadoEvento) {
		this.fecEstadoEvento = fecEstadoEvento;
	}
	public String getCodCalificacionFinEtapa() {
		return codCalificacionFinEtapa;
	}
	public void setCodCalificacionFinEtapa(String codCalificacionFinEtapa) {
		this.codCalificacionFinEtapa = codCalificacionFinEtapa;
	}
	public String getFecCalificacionFinEtapa() {
		return fecCalificacionFinEtapa;
	}
	public void setFecCalificacionFinEtapa(String fecCalificacionFinEtapa) {
		this.fecCalificacionFinEtapa = fecCalificacionFinEtapa;
	}
	public String getComentarioFinEtapa() {
		return comentarioFinEtapa;
	}
	public void setComentarioFinEtapa(String comentarioFinEtapa) {
		this.comentarioFinEtapa = comentarioFinEtapa;
	}
	public String getFlagEstadoActual() {
		return flagEstadoActual;
	}
	public void setFlagEstadoActual(String flagEstadoActual) {
		this.flagEstadoActual = flagEstadoActual;
	}
	public String getCodRelacion() {
		return codRelacion;
	}
	public void setCodRelacion(String codRelacion) {
		this.codRelacion = codRelacion;
	}
	public String getNombreEvaluador() {
		return nombreEvaluador;
	}
	public void setNombreEvaluador(String nombreEvaluador) {
		this.nombreEvaluador = nombreEvaluador;
	}
	public String getNombreProceso() {
		return nombreProceso;
	}
	public void setNombreProceso(String nombreProceso) {
		this.nombreProceso = nombreProceso;
	}
	public String getDesTipoEvaluacion() {
		return desTipoEvaluacion;
	}
	public void setDesTipoEvaluacion(String desTipoEvaluacion) {
		this.desTipoEvaluacion = desTipoEvaluacion;
	}
	
}
