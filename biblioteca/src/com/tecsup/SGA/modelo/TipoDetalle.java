package com.tecsup.SGA.modelo;

import java.util.List;
import java.util.ArrayList;

public class TipoDetalle implements java.io.Serializable{
	
	private String codTipoTipo;
	private String denominacionTipo;
	private String descripcionTipo;
	private String mantenimientoTipo;
	private String sistemaTipo;
	private String codEtapa;
	private String codEva;
	
	public String getCodTipoTipo() {
		return codTipoTipo;
	}
	public void setCodTipoTipo(String codTipoTipo) {
		this.codTipoTipo = codTipoTipo;
	}
	public String getDenominacionTipo() {
		return denominacionTipo;
	}
	public void setDenominacionTipo(String denominacionTipo) {
		this.denominacionTipo = denominacionTipo;
	}
	public String getDescripcionTipo() {
		return descripcionTipo;
	}
	public void setDescripcionTipo(String descripcionTipo) {
		this.descripcionTipo = descripcionTipo;
	}
	public String getMantenimientoTipo() {
		return mantenimientoTipo;
	}
	public void setMantenimientoTipo(String mantenimientoTipo) {
		this.mantenimientoTipo = mantenimientoTipo;
	}
	public String getSistemaTipo() {
		return sistemaTipo;
	}
	public void setSistemaTipo(String sistemaTipo) {
		this.sistemaTipo = sistemaTipo;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getCodEva() {
		return codEva;
	}
	public void setCodEva(String codEva) {
		this.codEva = codEva;
	}
	


}
