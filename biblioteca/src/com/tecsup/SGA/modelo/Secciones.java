package com.tecsup.SGA.modelo;

public class Secciones implements java.io.Serializable{

	public String codUnico;
	public String codGrupo;
	public String descripcionGrupo;
	public String codSeccion;
	public String descripcionSeccion;	
	public String fechaPublicacion;
	public String fechaVigencia;
	public String tema;
	public String descripcionCorta;
	public String descripcionLarga;
	public String url;
	public String imagen;
	public String flag;
	public String codTipo;
	//********napa
	public String tipoMaterial;
	public String archivo;
	public String nomAdjunto;
	
	public String documentoNov; //novedades
	public String extDocNov;
	
	public String getNomAdjunto() {
		return nomAdjunto;
	}
	public void setNomAdjunto(String nomAdjunto) {
		this.nomAdjunto = nomAdjunto;
	}
	public String getTipoMaterial() {
		return tipoMaterial;
	}
	public void setTipoMaterial(String tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getCodGrupo() {
		return codGrupo;
	}
	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}
	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}
	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public String getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public String getFechaVigencia() {
		return fechaVigencia;
	}
	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	public String getDescripcionCorta() {
		return descripcionCorta;
	}
	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}
	public String getDescripcionLarga() {
		return descripcionLarga;
	}
	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getCodTipo() {
		return codTipo;
	}
	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
	}
	
	public String getDocumentoNov() {
		return documentoNov;
	}
	public void setDocumentoNov(String documentoNov) {
		this.documentoNov = documentoNov;
	}
	public String getExtDocNov() {
		return extDocNov;
	}
	public void setExtDocNov(String extDocNov) {
		this.extDocNov = extDocNov;
	}
	
	
}
