package com.tecsup.SGA.modelo;

public class Alternativa {

	private String codAlternativa;
	private String etiAlternativa;
	private String desAlternativa;
	private String peso;
	
	public String getCodAlternativa() {
		return codAlternativa;
	}
	public void setCodAlternativa(String codAlternativa) {
		this.codAlternativa = codAlternativa;
	}
	public String getEtiAlternativa() {
		return etiAlternativa;
	}
	public void setEtiAlternativa(String etiAlternativa) {
		this.etiAlternativa = etiAlternativa;
	}
	public String getDesAlternativa() {
		return desAlternativa;
	}
	public void setDesAlternativa(String desAlternativa) {
		this.desAlternativa = desAlternativa;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	
}
