package com.tecsup.SGA.modelo;

public class SolRequerimiento implements java.io.Serializable{
	private String idRequerimiento;
	private String tipoRequerimiento;
	private String dscTipoRequerimiento; //Descripcion del tipo de Requerimientpo
	private String subTipoRequerimiento;
	private String dscSubTipoRequerimiento; //Descripcion del subtipo de Requerimiento
	private String grupoServicio;
	private String dscGrupoServicio;
	private String nroRequerimiento;
	private String fechaEmision;
	private String moneda;
	private String usuSolicitante;	
	private String cecoSolicitante;
	private String dscCecoSolicitante;
	private String cecoAfectado;
	private String codEstado;
	private String dscEstado;
	private String dscDetalleEstado;
	private String totalRequerimiento;
	private String indReqAprobado;
	private String fecAprobacion;
	private String fecCierre;
	private String fecRechazo;
	private String usuResponsable;
	private String idOrigen;
	private String idOrigenDevolucion;
	private String motivoDevolucion;
	private String observacionDevolucion;
	private String usuCrea;
	private String fecCrea;
	private String usuModi;
	private String fecModi;
	private String estadoReg;
	//**********************
	private String sede;
	private String usuAsignado;
	private String dscTipoServicio;
	private String nombreServicio;
	private String fechaEntrega;
	private String dscEsfuerzo;
	private String nroDocRel;
	//**********************
	private String codRequerimientoOri;
	private String nroRequerimientoOri;
	private String codDevolucion;
	private String nroDevolucion;
	private String fechaDevolucion;
	
	private String cadenaCodigo;
	private String cadenaCantidad;
	private String cantidad;
	private String indiceEnvio;
	private String indiceCerrar;
	private String indiceDevolucion;
	private String dscServicio;
	
	private String indInversion;
	//ALQD,30/10/08. INDICADOR DE PEDIDO PARA STOCK y PERSONAL DE LOGISTICA
	private String indParaStock;
	private String indEmpLogistica;
	//ALQD,21/04/09. A�ADIENDO PROPIEDAd PARA REQ. DE TIPO SERVICIO DE MANTTO.
	private String  cantApoyo;
	private String nroItem;
	private String unidMed;
	private String familia;
	private String fecEnvio;
	private String fecEstimada;
	private String fecAsignado;
	private String tipoPago;
	private String fecGenOC;
	private String fecJefeLog;
	private String fecDirAdmin;
	private String fecDirCencos;
	private String comprador;
	private String fecDirGeneral;
	private String fecRecepcion;
	
	public String getCantApoyo() {
		return cantApoyo;
	}
	public void setCantApoyo(String cantApoyo) {
		this.cantApoyo=cantApoyo;
	}
	public String getIndParaStock() {
		return indParaStock;
	}
	public void setIndParaStock(String indParaStock) {
		this.indParaStock = indParaStock;
	}
	public String getIndEmpLogistica() {
		return indEmpLogistica;
	}
	public void setIndEmpLogistica(String indEmpLogistica) {
		this.indEmpLogistica = indEmpLogistica;
	}
	public String getIndInversion() {
		return indInversion;
	}
	public void setIndInversion(String indInversion) {
		this.indInversion = indInversion;
	}
	public String getDscServicio() {
		return dscServicio;
	}
	public void setDscServicio(String dscServicio) {
		this.dscServicio = dscServicio;
	}
	public String getIndiceEnvio() {
		return indiceEnvio;
	}
	public void setIndiceEnvio(String indiceEnvio) {
		this.indiceEnvio = indiceEnvio;
	}
	public String getCadenaCodigo() {
		return cadenaCodigo;
	}
	public void setCadenaCodigo(String cadenaCodigo) {
		this.cadenaCodigo = cadenaCodigo;
	}
	public String getCadenaCantidad() {
		return cadenaCantidad;
	}
	public void setCadenaCantidad(String cadenaCantidad) {
		this.cadenaCantidad = cadenaCantidad;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getCodDevolucion() {
		return codDevolucion;
	}
	public void setCodDevolucion(String codDevolucion) {
		this.codDevolucion = codDevolucion;
	}
	public String getNroDevolucion() {
		return nroDevolucion;
	}
	public void setNroDevolucion(String nroDevolucion) {
		this.nroDevolucion = nroDevolucion;
	}
	public String getFechaDevolucion() {
		return fechaDevolucion;
	}
	public void setFechaDevolucion(String fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	public String getNroRequerimientoOri() {
		return nroRequerimientoOri;
	}
	public void setNroRequerimientoOri(String nroRequerimientoOri) {
		this.nroRequerimientoOri = nroRequerimientoOri;
	}
	public String getCodRequerimientoOri() {
		return codRequerimientoOri;
	}
	public void setCodRequerimientoOri(String codRequerimientoOri) {
		this.codRequerimientoOri = codRequerimientoOri;
	}
	public String getNroDocRel() {
		return nroDocRel;
	}
	public void setNroDocRel(String nroDocRel) {
		this.nroDocRel = nroDocRel;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getDscEsfuerzo() {
		return dscEsfuerzo;
	}
	public void setDscEsfuerzo(String dscEsfuerzo) {
		this.dscEsfuerzo = dscEsfuerzo;
	}
	public String getNombreServicio() {
		return nombreServicio;
	}
	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}
	public String getDscTipoServicio() {
		return dscTipoServicio;
	}
	public void setDscTipoServicio(String dscTipoServicio) {
		this.dscTipoServicio = dscTipoServicio;
	}
	public String getUsuAsignado() {
		return usuAsignado;
	}
	public void setUsuAsignado(String usuAsignado) {
		this.usuAsignado = usuAsignado;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getDscDetalleEstado() {
		return dscDetalleEstado;
	}
	public void setDscDetalleEstado(String dscDetalleEstado) {
		this.dscDetalleEstado = dscDetalleEstado;
	}
	public String getTotalRequerimiento() {
		return totalRequerimiento;
	}
	public void setTotalRequerimiento(String totalRequerimiento) {
		this.totalRequerimiento = totalRequerimiento;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getDscCecoSolicitante() {
		return dscCecoSolicitante;
	}
	public void setDscCecoSolicitante(String dscCecoSolicitante) {
		this.dscCecoSolicitante = dscCecoSolicitante;
	}
	public String getIdRequerimiento() {
		return idRequerimiento;
	}
	public void setIdRequerimiento(String idRequerimiento) {
		this.idRequerimiento = idRequerimiento;
	}
	public String getTipoRequerimiento() {
		return tipoRequerimiento;
	}
	public void setTipoRequerimiento(String tipoRequerimiento) {
		this.tipoRequerimiento = tipoRequerimiento;
	}
	public String getSubTipoRequerimiento() {
		return subTipoRequerimiento;
	}
	public void setSubTipoRequerimiento(String subTipoRequerimiento) {
		this.subTipoRequerimiento = subTipoRequerimiento;
	}
	public String getNroRequerimiento() {
		return nroRequerimiento;
	}
	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getCecoSolicitante() {
		return cecoSolicitante;
	}
	public void setCecoSolicitante(String cecoSolicitante) {
		this.cecoSolicitante = cecoSolicitante;
	}
	public String getCecoAfectado() {
		return cecoAfectado;
	}
	public void setCecoAfectado(String cecoAfectado) {
		this.cecoAfectado = cecoAfectado;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getIndReqAprobado() {
		return indReqAprobado;
	}
	public void setIndReqAprobado(String indReqAprobado) {
		this.indReqAprobado = indReqAprobado;
	}
	public String getFecAprobacion() {
		return fecAprobacion;
	}
	public void setFecAprobacion(String fecAprobacion) {
		this.fecAprobacion = fecAprobacion;
	}
	public String getFecCierre() {
		return fecCierre;
	}
	public void setFecCierre(String fecCierre) {
		this.fecCierre = fecCierre;
	}
	public String getFecRechazo() {
		return fecRechazo;
	}
	public void setFecRechazo(String fecRechazo) {
		this.fecRechazo = fecRechazo;
	}
	public String getUsuResponsable() {
		return usuResponsable;
	}
	public void setUsuResponsable(String usuResponsable) {
		this.usuResponsable = usuResponsable;
	}
	public String getIdOrigen() {
		return idOrigen;
	}
	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
	}
	public String getIdOrigenDevolucion() {
		return idOrigenDevolucion;
	}
	public void setIdOrigenDevolucion(String idOrigenDevolucion) {
		this.idOrigenDevolucion = idOrigenDevolucion;
	}
	public String getMotivoDevolucion() {
		return motivoDevolucion;
	}
	public void setMotivoDevolucion(String motivoDevolucion) {
		this.motivoDevolucion = motivoDevolucion;
	}
	public String getObservacionDevolucion() {
		return observacionDevolucion;
	}
	public void setObservacionDevolucion(String observacionDevolucion) {
		this.observacionDevolucion = observacionDevolucion;
	}
	public String getUsuCrea() {
		return usuCrea;
	}
	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}
	public String getFecCrea() {
		return fecCrea;
	}
	public void setFecCrea(String fecCrea) {
		this.fecCrea = fecCrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getFecModi() {
		return fecModi;
	}
	public void setFecModi(String fecModi) {
		this.fecModi = fecModi;
	}
	public String getEstadoReg() {
		return estadoReg;
	}
	public void setEstadoReg(String estadoReg) {
		this.estadoReg = estadoReg;
	}	
	
	public String getDscTipoRequerimiento() {
		return dscTipoRequerimiento;
	}
	public void setDscTipoRequerimiento(String dscTipoRequerimiento) {
		this.dscTipoRequerimiento = dscTipoRequerimiento;
	}
	public String getDscSubTipoRequerimiento() {
		return dscSubTipoRequerimiento;
	}
	public void setDscSubTipoRequerimiento(String dscSubTipoRequerimiento) {
		this.dscSubTipoRequerimiento = dscSubTipoRequerimiento;
	}
	public String getGrupoServicio() {
		return grupoServicio;
	}
	public void setGrupoServicio(String grupoServicio) {
		this.grupoServicio = grupoServicio;
	}
	public String getDscGrupoServicio() {
		return dscGrupoServicio;
	}
	public void setDscGrupoServicio(String dscGrupoServicio) {
		this.dscGrupoServicio = dscGrupoServicio;
	}
	public SolRequerimiento()
	{
		this.idRequerimiento = "";
		this.tipoRequerimiento = "";
		this.subTipoRequerimiento = "";
		this.nroRequerimiento = "";
		this.fechaEmision = "";
		this.moneda = "";
		this.usuSolicitante = "";
		this.cecoSolicitante = "";
		this.cecoAfectado = "";
		this.codEstado = "";
		this.indReqAprobado = "";
		this.fecAprobacion = "";
		this.fecCierre = "";
		this.fecRechazo = "";
		this.usuResponsable = "";
		this.idOrigen = "";
		this.idOrigenDevolucion = "";
		this.motivoDevolucion = "";
		this.observacionDevolucion = "";
		this.usuCrea = "";
		this.fecCrea = "";
		this.usuModi = "";
		this.fecModi = "";
		this.estadoReg = "";
	}
	public String getIndiceCerrar() {
		return indiceCerrar;
	}
	public void setIndiceCerrar(String indiceCerrar) {
		this.indiceCerrar = indiceCerrar;
	}
	public String getIndiceDevolucion() {
		return indiceDevolucion;
	}
	public void setIndiceDevolucion(String indiceDevolucion) {
		this.indiceDevolucion = indiceDevolucion;
	}
	public String getNroItem() {
		return nroItem;
	}
	public void setNroItem(String nroItem) {
		this.nroItem = nroItem;
	}
	public String getUnidMed() {
		return unidMed;
	}
	public void setUnidMed(String unidMed) {
		this.unidMed = unidMed;
	}
	public String getFamilia() {
		return familia;
	}
	public void setFamilia(String familia) {
		this.familia = familia;
	}
	public String getFecEnvio() {
		return fecEnvio;
	}
	public void setFecEnvio(String fecEnvio) {
		this.fecEnvio = fecEnvio;
	}
	public String getFecEstimada() {
		return fecEstimada;
	}
	public void setFecEstimada(String fecEstimada) {
		this.fecEstimada = fecEstimada;
	}
	public String getFecAsignado() {
		return fecAsignado;
	}
	public void setFecAsignado(String fecAsignado) {
		this.fecAsignado = fecAsignado;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getFecGenOC() {
		return fecGenOC;
	}
	public void setFecGenOC(String fecGenOC) {
		this.fecGenOC = fecGenOC;
	}
	public String getFecJefeLog() {
		return fecJefeLog;
	}
	public void setFecJefeLog(String fecJefeLog) {
		this.fecJefeLog = fecJefeLog;
	}
	public String getFecDirAdmin() {
		return fecDirAdmin;
	}
	public void setFecDirAdmin(String fecDirAdmin) {
		this.fecDirAdmin = fecDirAdmin;
	}
	public String getFecDirCencos() {
		return fecDirCencos;
	}
	public void setFecDirCencos(String fecDirCencos) {
		this.fecDirCencos = fecDirCencos;
	}
	public String getComprador() {
		return comprador;
	}
	public void setComprador(String comprador) {
		this.comprador = comprador;
	}
	public String getFecDirGeneral() {
		return fecDirGeneral;
	}
	public void setFecDirGeneral(String fecDirGeneral) {
		this.fecDirGeneral = fecDirGeneral;
	}
	public String getFecRecepcion() {
		return fecRecepcion;
	}
	public void setFecRecepcion(String fecRecepcion) {
		this.fecRecepcion = fecRecepcion;
	}
}
