package com.tecsup.SGA.modelo;

public class ActivosDisponibles {

	private String codBienActivo;
	private String nroSerie;
	private String nroOrden;
	private String valorCompra;
	
	public String getCodBienActivo() {
		return codBienActivo;
	}
	public void setCodBienActivo(String codBienActivo) {
		this.codBienActivo = codBienActivo;
	}
	public String getNroSerie() {
		return nroSerie;
	}
	public void setNroSerie(String nroSerie) {
		this.nroSerie = nroSerie;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(String valorCompra) {
		this.valorCompra = valorCompra;
	}
	
	
}
