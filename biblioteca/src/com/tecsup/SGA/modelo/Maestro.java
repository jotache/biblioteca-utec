package com.tecsup.SGA.modelo;

public class Maestro implements java.io.Serializable{
	private String codigo;
	private String dscValor1;
	
		
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDscValor1() {
		return dscValor1;
	}
	public void setDscValor1(String dscValor1) {
		this.dscValor1 = dscValor1;
	}

}
