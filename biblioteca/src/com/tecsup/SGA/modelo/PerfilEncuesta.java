package com.tecsup.SGA.modelo;

public class PerfilEncuesta implements java.io.Serializable{
	
	private String codTipoEncuesta;
	private String codEncuesta;
	private String codPerfil;
	private String codProducto;
	private String codPrograma;
	private String codCiclo;
	private String codDepartamento;
	private String codCurso;
	private String codTipoPersonal;
	private String codTipoDocente;
	private String añoIniEgresado;
	private String añoFinEgresado;
	
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getCodDepartamento() {
		return codDepartamento;
	}
	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodTipoPersonal() {
		return codTipoPersonal;
	}
	public void setCodTipoPersonal(String codTipoPersonal) {
		this.codTipoPersonal = codTipoPersonal;
	}
	public String getCodTipoDocente() {
		return codTipoDocente;
	}
	public void setCodTipoDocente(String codTipoDocente) {
		this.codTipoDocente = codTipoDocente;
	}
	public String getAñoIniEgresado() {
		return añoIniEgresado;
	}
	public void setAñoIniEgresado(String añoIniEgresado) {
		this.añoIniEgresado = añoIniEgresado;
	}
	public String getAñoFinEgresado() {
		return añoFinEgresado;
	}
	public void setAñoFinEgresado(String añoFinEgresado) {
		this.añoFinEgresado = añoFinEgresado;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}	
}
