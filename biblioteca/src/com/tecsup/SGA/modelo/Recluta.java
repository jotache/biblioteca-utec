package com.tecsup.SGA.modelo;

import java.util.*;

public class Recluta implements java.io.Serializable{
	private String codRecluta;
	private String nombre;
	private String apepat;
	private String apemat;
	private String fecnac;
	private String fecreg;
	private String email;
	private String emailAlterno;
	private String clave;
	private String sexo; //1 femenino / 2 masculino
	private String nacionalidad;//codigo 0-peruano / 1-otro
	private String ruc;
	private String anioExpLaboral;
	private String estadoCivil; //codigo estado civil
	private String dni;
	private String direccion;
	private String departamento;
	private String provincia;
	private String distrito;
	private String pais; //pais de residencia
	private String telef; //telef domicilio
	private String telefAdicional;
	private String telefMovil;
	private String codPostal;
	private String postuladoAntes; //char 1 (Si-No)
	private String trabajadoAntes; //char 1 (Si-No)
	private String familiaTecsup; //char 1 (Si-No)
	private String familiaNombres;
	private String expDocente; //char 1 (Si-No)
	private String expDocenteAnios;
	private String dispoViaje; //char 1 (Si-No)}
	private String sedePrefTrabajo;//codigo sede pref trabajo
	private String interesEn;
	private String pretensionEconomica;
	private String dispoTrabajar;
	private String dedicacion;
	private String perfil;
	
	/*Estudios Secundarios*/
	private String colegio1;
	private String colegio2;
	private String anioInicio1;
	private String anioInicio2;
	private String anioFin1;
	private String anioFin2;
	
	/*Idiomas*/
	private String codEstudioIdioma; //codigo del estudio
	
	private String idioma;
	private String nivel;
	private String grado;
	
	/*Estudios Superiores*/
	private String codEstudioSup; //codigo del estudio
	
	private String gradoAcad;
	private String areaEstudio;
	private String institucion;
	private String otraInstitucion;	
	private String ciclo;
	private String merito;
	private String inicio; //(mm/aaaa)
	private String termino; //(mm/aaaa)
	
	/*Exp Laboral*/
	private String codExpLaboral; //codigo del estudio
	
	private String organizacion;
	private String puesto;
	private String otroPuesto;
	private String fecIni;
	private String fecFin;
	private String referencia;
	private String telefRef;
	private String funciones;
	
	
	/*Archivo Adjunto*/
	private String foto;
	private String cv;
	private String fotoOriginal;
	private String cvOriginal;
	private String infFinal;
	private String infFinalGen;
	
	/*Listado Ofertas*/
	private String codOferta;
	private String oferta;
	private String area;
	private String fechaPubli;
	private String fechaCierre;
	private String nroVacantes;
	private String funcionesOferta;
	
	/*Lista de Interes */
	private String codTTDInteres;
	private String codInteres;
	private String descInteres;
	
	/*Agregado napa*/
	private String moneda;
	private String tipoPago;
	private String puestoPostula;
	
	
	/**
	 * @return the puestoPostula
	 */
	public String getPuestoPostula() {
		return puestoPostula;
	}
	/**
	 * @param puestoPostula the puestoPostula to set
	 */
	public void setPuestoPostula(String puestoPostula) {
		this.puestoPostula = puestoPostula;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getOferta() {
		return oferta;
	}
	public void setOferta(String oferta) {
		this.oferta = oferta;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getFechaPubli() {
		return fechaPubli;
	}
	public void setFechaPubli(String fechaPubli) {
		this.fechaPubli = fechaPubli;
	}
	public String getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public String getNroVacantes() {
		return nroVacantes;
	}
	public void setNroVacantes(String nroVacantes) {
		this.nroVacantes = nroVacantes;
	}
	public String getFuncionesOferta() {
		return funcionesOferta;
	}
	public void setFuncionesOferta(String funcionesOferta) {
		this.funcionesOferta = funcionesOferta;
	}
	public String getColegio1() {
		return colegio1;
	}
	public void setColegio1(String colegio1) {
		this.colegio1 = colegio1;
	}
	public String getColegio2() {
		return colegio2;
	}
	public void setColegio2(String colegio2) {
		this.colegio2 = colegio2;
	}
	public String getAnioInicio1() {
		return anioInicio1;
	}
	public void setAnioInicio1(String anioInicio1) {
		this.anioInicio1 = anioInicio1;
	}
	public String getAnioInicio2() {
		return anioInicio2;
	}
	public void setAnioInicio2(String anioInicio2) {
		this.anioInicio2 = anioInicio2;
	}
	public String getAnioFin1() {
		return anioFin1;
	}
	public void setAnioFin1(String anioFin1) {
		this.anioFin1 = anioFin1;
	}
	public String getAnioFin2() {
		return anioFin2;
	}
	public void setAnioFin2(String anioFin2) {
		this.anioFin2 = anioFin2;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getGrado() {
		return grado;
	}
	public void setGrado(String grado) {
		this.grado = grado;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getAnioExpLaboral() {
		return anioExpLaboral;
	}
	public void setAnioExpLaboral(String anioExpLaboral) {
		this.anioExpLaboral = anioExpLaboral;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTelef() {
		return telef;
	}
	public void setTelef(String telef) {
		this.telef = telef;
	}
	public String getTelefAdicional() {
		return telefAdicional;
	}
	public void setTelefAdicional(String telefAdicional) {
		this.telefAdicional = telefAdicional;
	}
	public String getTelefMovil() {
		return telefMovil;
	}
	public void setTelefMovil(String telefMovil) {
		this.telefMovil = telefMovil;
	}
	public String getCodPostal() {
		return codPostal;
	}
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}
	public String getPostuladoAntes() {
		return postuladoAntes;
	}
	public void setPostuladoAntes(String postuladoAntes) {
		this.postuladoAntes = postuladoAntes;
	}
	public String getTrabajadoAntes() {
		return trabajadoAntes;
	}
	public void setTrabajadoAntes(String trabajadoAntes) {
		this.trabajadoAntes = trabajadoAntes;
	}
	public String getFamiliaTecsup() {
		return familiaTecsup;
	}
	public void setFamiliaTecsup(String familiaTecsup) {
		this.familiaTecsup = familiaTecsup;
	}
	public String getFamiliaNombres() {
		return familiaNombres;
	}
	public void setFamiliaNombres(String familiaNombres) {
		this.familiaNombres = familiaNombres;
	}
	public String getExpDocente() {
		return expDocente;
	}
	public void setExpDocente(String expDocente) {
		this.expDocente = expDocente;
	}
	public String getExpDocenteAnios() {
		return expDocenteAnios;
	}
	public void setExpDocenteAnios(String expDocenteAnios) {
		this.expDocenteAnios = expDocenteAnios;
	}
	public String getDispoViaje() {
		return dispoViaje;
	}
	public void setDispoViaje(String dispoViaje) {
		this.dispoViaje = dispoViaje;
	}
	public String getSedePrefTrabajo() {
		return sedePrefTrabajo;
	}
	public void setSedePrefTrabajo(String sedePrefTrabajo) {
		this.sedePrefTrabajo = sedePrefTrabajo;
	}
	public String getInteresEn() {
		return interesEn;
	}
	public void setInteresEn(String interesEn) {
		this.interesEn = interesEn;
	}
	public String getDispoTrabajar() {
		return dispoTrabajar;
	}
	public void setDispoTrabajar(String dispoTrabajar) {
		this.dispoTrabajar = dispoTrabajar;
	}
	public String getDedicacion() {
		return dedicacion;
	}
	public void setDedicacion(String dedicacion) {
		this.dedicacion = dedicacion;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getEmailAlterno() {
		return emailAlterno;
	}
	public void setEmailAlterno(String emailAlterno) {
		this.emailAlterno = emailAlterno;
	}
	public String getCodRecluta() {
		return codRecluta;
	}
	public void setCodRecluta(String codRecluta) {
		this.codRecluta = codRecluta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApepat() {
		return apepat;
	}
	public void setApepat(String apepat) {
		this.apepat = apepat;
	}
	public String getApemat() {
		return apemat;
	}
	public void setApemat(String apemat) {
		this.apemat = apemat;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getFecnac() {
		return fecnac;
	}
	public void setFecnac(String fecnac) {
		this.fecnac = fecnac;
	}
	public String getFecreg() {
		return fecreg;
	}
	public void setFecreg(String fecreg) {
		this.fecreg = fecreg;
	}
	public String getPretensionEconomica() {
		return pretensionEconomica;
	}
	public void setPretensionEconomica(String pretensionEconomica) {
		this.pretensionEconomica = pretensionEconomica;
	}
	public String getCodEstudioIdioma() {
		return codEstudioIdioma;
	}
	public void setCodEstudioIdioma(String codEstudioIdioma) {
		this.codEstudioIdioma = codEstudioIdioma;
	}
	public String getGradoAcad() {
		return gradoAcad;
	}
	public void setGradoAcad(String gradoAcad) {
		this.gradoAcad = gradoAcad;
	}
	public String getAreaEstudio() {
		return areaEstudio;
	}
	public void setAreaEstudio(String areaEstudio) {
		this.areaEstudio = areaEstudio;
	}
	public String getInstitucion() {
		return institucion;
	}
	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}
	public String getOtraInstitucion() {
		return otraInstitucion;
	}
	public void setOtraInstitucion(String otraInstitucion) {
		this.otraInstitucion = otraInstitucion;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getMerito() {
		return merito;
	}
	public void setMerito(String merito) {
		this.merito = merito;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getTermino() {
		return termino;
	}
	public void setTermino(String termino) {
		this.termino = termino;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	public String getOrganizacion() {
		return organizacion;
	}
	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public String getOtroPuesto() {
		return otroPuesto;
	}
	public void setOtroPuesto(String otroPuesto) {
		this.otroPuesto = otroPuesto;
	}
	public String getFecIni() {
		return fecIni;
	}
	public void setFecIni(String fecIni) {
		this.fecIni = fecIni;
	}
	public String getFecFin() {
		return fecFin;
	}
	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getTelefRef() {
		return telefRef;
	}
	public void setTelefRef(String telefRef) {
		this.telefRef = telefRef;
	}
	public String getFunciones() {
		return funciones;
	}
	public void setFunciones(String funciones) {
		this.funciones = funciones;
	}
	public String getCodExpLaboral() {
		return codExpLaboral;
	}
	public void setCodExpLaboral(String codExpLaboral) {
		this.codExpLaboral = codExpLaboral;
	}
	public String getCodEstudioSup() {
		return codEstudioSup;
	}
	public void setCodEstudioSup(String codEstudioSup) {
		this.codEstudioSup = codEstudioSup;
	}
	public String getCodOferta() {
		return codOferta;
	}
	public void setCodOferta(String codOferta) {
		this.codOferta = codOferta;
	}
	public String getCodTTDInteres() {
		return codTTDInteres;
	}
	public void setCodTTDInteres(String codTTDInteres) {
		this.codTTDInteres = codTTDInteres;
	}
	public String getCodInteres() {
		return codInteres;
	}
	public void setCodInteres(String codInteres) {
		this.codInteres = codInteres;
	}
	public String getDescInteres() {
		return descInteres;
	}
	public void setDescInteres(String descInteres) {
		this.descInteres = descInteres;
	}
	public String getFotoOriginal() {
		return fotoOriginal;
	}
	public void setFotoOriginal(String fotoOriginal) {
		this.fotoOriginal = fotoOriginal;
	}
	public String getCvOriginal() {
		return cvOriginal;
	}
	public void setCvOriginal(String cvOriginal) {
		this.cvOriginal = cvOriginal;
	}
	public String getInfFinal() {
		return infFinal;
	}
	public void setInfFinal(String infFinal) {
		this.infFinal = infFinal;
	}
	public String getInfFinalGen() {
		return infFinalGen;
	}
	public void setInfFinalGen(String infFinalGen) {
		this.infFinalGen = infFinalGen;
	}	
}
