package com.tecsup.SGA.modelo;

public class Alumno implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5925840984313162098L;
	private String codAlumno;
	private String codCarnet;
	public String getCodCarnet() {
		return codCarnet;
	}
	public void setCodCarnet(String codCarnet) {
		this.codCarnet = codCarnet;
	}
	private String nombreAlumno;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombre;
	private String notaExterna;
	private String notaTecsup;
	private String correo;
	//valores para registrar en nota externa
	private String codPeriodo;
	private String codProducto;
	private String codEspecialidad;
	private String codEtapa;
	private String codPrograma;
	private String codCiclo;
	private String codCurso;
	private String dscPeriodo;
	private String nivelAlumno;
	private String especialidad;
	private String secciones;
	private String flagNp; //jhpr 2008-8-5   
	
	private String tipo;//TIPO 0 : NOTA EXTERNO/ 1 : CICLO EN LA EMPRESA (CAT)
	private String estado;
	
	private String id; // JHPR: 2008-05-05 Registro en bloque de notas externas.
	private String vecesLlevaUnCurso; // JHPR: 2008-05-20 Veces que lleva un curso X.
	private String sede;
	private String nombreCurso;//jhpr 2009-08-04 nombre del curso (externo)
	
	private String flgDatosPer;
	private String edadActual;
	
	public String getVecesLlevaUnCurso() {
		return vecesLlevaUnCurso;
	}
	public void setVecesLlevaUnCurso(String vecesLlevaUnCurso) {
		this.vecesLlevaUnCurso = vecesLlevaUnCurso;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	//******************************
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNotaExterna() {
		return notaExterna;
	}
	public void setNotaExterna(String notaExterna) {
		this.notaExterna = notaExterna;
	}
	public String getNotaTecsup() {
		return notaTecsup;
	}
	public void setNotaTecsup(String notaTecsup) {
		this.notaTecsup = notaTecsup;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getDscPeriodo() {
		return dscPeriodo;
	}
	public void setDscPeriodo(String dscPeriodo) {
		this.dscPeriodo = dscPeriodo;
	}
	public String getNivelAlumno() {
		return nivelAlumno;
	}
	public void setNivelAlumno(String nivelAlumno) {
		this.nivelAlumno = nivelAlumno;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public String getSecciones() {
		return secciones;
	}
	public void setSecciones(String secciones) {
		this.secciones = secciones;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFlagNp() {
		return flagNp;
	}
	public void setFlagNp(String flagNp) {
		this.flagNp = flagNp;
	}
	/**
	 * @return the sede
	 */
	public String getSede() {
		return sede;
	}
	/**
	 * @param sede the sede to set
	 */
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	public String getFlgDatosPer() {
		return flgDatosPer;
	}
	public void setFlgDatosPer(String flgDatosPer) {
		this.flgDatosPer = flgDatosPer;
	}
	public String getEdadActual() {
		return edadActual;
	}
	public void setEdadActual(String edadActual) {
		this.edadActual = edadActual;
	}
	@Override
	public String toString() {
		return "Alumno [codAlumno=" + codAlumno + ", codCarnet=" + codCarnet
				+ ", nombreAlumno=" + nombreAlumno + ", apellidoPaterno="
				+ apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno
				+ ", nombre=" + nombre + ", notaExterna=" + notaExterna
				+ ", notaTecsup=" + notaTecsup + ", correo=" + correo
				+ ", codPeriodo=" + codPeriodo + ", codProducto=" + codProducto
				+ ", codEspecialidad=" + codEspecialidad + ", codEtapa="
				+ codEtapa + ", codPrograma=" + codPrograma + ", codCiclo="
				+ codCiclo + ", codCurso=" + codCurso + ", dscPeriodo="
				+ dscPeriodo + ", nivelAlumno=" + nivelAlumno
				+ ", especialidad=" + especialidad + ", secciones=" + secciones
				+ ", flagNp=" + flagNp + ", tipo=" + tipo + ", estado="
				+ estado + ", id=" + id + ", vecesLlevaUnCurso="
				+ vecesLlevaUnCurso + ", sede=" + sede + ", nombreCurso="
				+ nombreCurso + ", flgDatosPer=" + flgDatosPer
				+ ", edadActual=" + edadActual + "]";
	}	
	
	
}
