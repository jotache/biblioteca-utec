package com.tecsup.SGA.modelo;

public class ParametrosReservados {
/*	TTDE.TTDE_VALOR1 AS EST_MAX_HRAS_RESER_DIA,
    TTDE.TTDE_VALOR2 AS EST_MINUTOS_TOLERANCIA,
    TTDE.TTDE_VALOR3 AS EST_DIAS_PENALIDAD,
    TTDE.TTDE_VALOR4 AS INT_MAX_HRAS_RESER_DIA,
    TTDE.TTDE_VALOR5 AS INT_MINUTOS_TOLERANCIA,
    TTDE.TTDE_VALOR6 AS INT_DIAS_PENALIDAD,
    TTDE.TTDE_VALOR7 AS MAT_DIAS_PENALIDAD
    TTDE.TTDE_VALOR8 AS CANT_MOSTRAR_WEB,
    TTDE.TTDE_VALOR9 AS AUXILIAR
 */
	private String codSec;
	private String esMaDi; //SALAS: Nro. M�x.  horas<br />para reserva al d�a
	private String esMiTo; //SALAS: Nro. De Minutos De Tolerancia
	private String esDiPe; //SALAS: Nro. De D�as  Penalidad
	private String inMaDi; //INTERNET: Nro. M&aacute;x. De Horas Para  Reservar Al D&iacute;a
	private String inMiTo; //INTERNET: Nro. De Minutos   Tolerancia
	private String inDiPe; //INTERNET: Nro. De D&iacute;as  Penalidad
	private String biNroDiPe; //MAT BIBLIO: Nro. De D�as Penalidad
	private String cantMostrarWeb; //REG A VISUALIZAR: Nro. M�x. A Visualizar
	
	private String maxHrResPoli; // POLIDEPORTIVO: Maximo de horas al dia de reserva de ambiente polideportivo
	private String minTolePoli; // POLIDEPORTIVO: Minutos de tolerancia (ambiente polideportivo)
	private String codTipoUsuario;
	private String nomTipoUsuario;
	private String codParametroUsuario;
	
	public String getCantMostrarWeb() {
		return cantMostrarWeb;
	}
	public void setCantMostrarWeb(String cantMostrarWeb) {
		this.cantMostrarWeb = cantMostrarWeb;
	}

	public String getEsMaDi() {
		return esMaDi;
	}
	public void setEsMaDi(String esMaDi) {
		this.esMaDi = esMaDi;
	}
	public String getEsMiTo() {
		return esMiTo;
	}
	public void setEsMiTo(String esMiTo) {
		this.esMiTo = esMiTo;
	}
	public String getEsDiPe() {
		return esDiPe;
	}
	public void setEsDiPe(String esDiPe) {
		this.esDiPe = esDiPe;
	}
	public String getInMaDi() {
		return inMaDi;
	}
	public void setInMaDi(String inMaDi) {
		this.inMaDi = inMaDi;
	}
	public String getInMiTo() {
		return inMiTo;
	}
	public void setInMiTo(String inMiTo) {
		this.inMiTo = inMiTo;
	}
	public String getInDiPe() {
		return inDiPe;
	}
	public void setInDiPe(String inDiPe) {
		this.inDiPe = inDiPe;
	}
	public String getBiNroDiPe() {
		return biNroDiPe;
	}
	public void setBiNroDiPe(String biNroDiPe) {
		this.biNroDiPe = biNroDiPe;
	}
	public String getCodSec() {
		return codSec;
	}
	public void setCodSec(String codSec) {
		this.codSec = codSec;
	}
	public String getMaxHrResPoli() {
		return maxHrResPoli;
	}
	public void setMaxHrResPoli(String maxHrResPoli) {
		this.maxHrResPoli = maxHrResPoli;
	}
	public String getMinTolePoli() {
		return minTolePoli;
	}
	public void setMinTolePoli(String minTolePoli) {
		this.minTolePoli = minTolePoli;
	}
	public String getCodTipoUsuario() {
		return codTipoUsuario;
	}
	public void setCodTipoUsuario(String codTipoUsuario) {
		this.codTipoUsuario = codTipoUsuario;
	}
	public String getNomTipoUsuario() {
		return nomTipoUsuario;
	}
	public void setNomTipoUsuario(String nomTipoUsuario) {
		this.nomTipoUsuario = nomTipoUsuario;
	}
	public String getCodParametroUsuario() {
		return codParametroUsuario;
	}
	public void setCodParametroUsuario(String codParametroUsuario) {
		this.codParametroUsuario = codParametroUsuario;
	}
	
	
}
