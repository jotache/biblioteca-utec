package com.tecsup.SGA.modelo;

public class Evaluador implements java.io.Serializable{
	private String codRegEvaluador;
	private String codProceso;
	private String codTipoEvaluacion;
	private String dscTipoEvaluacion;
	private String codEvaluador;
	private String dscEvaluador;
	private String correo;
	private String ordenEvaluacion;
	
	public String getCodRegEvaluador() {
		return codRegEvaluador;
	}
	public void setCodRegEvaluador(String codRegEvaluador) {
		this.codRegEvaluador = codRegEvaluador;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getCodTipoEvaluacion() {
		return codTipoEvaluacion;
	}
	public void setCodTipoEvaluacion(String codTipoEvaluacion) {
		this.codTipoEvaluacion = codTipoEvaluacion;
	}
	public String getDscTipoEvaluacion() {
		return dscTipoEvaluacion;
	}
	public void setDscTipoEvaluacion(String dscTipoEvaluacion) {
		this.dscTipoEvaluacion = dscTipoEvaluacion;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getDscEvaluador() {
		return dscEvaluador;
	}
	public void setDscEvaluador(String dscEvaluador) {
		this.dscEvaluador = dscEvaluador;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getOrdenEvaluacion() {
		return ordenEvaluacion;
	}
	public void setOrdenEvaluacion(String ordenEvaluacion) {
		this.ordenEvaluacion = ordenEvaluacion;
	}
	
	
}
