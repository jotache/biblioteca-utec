package com.tecsup.SGA.modelo;

public class Curso {
private String codCurso;
private String nomCurso;
private String descripcion;
private String codProducto;
private String codEspecialidad;
private String codCiclo;
private String codSisteVal;
private String codPrograma;
private String codEvaluador;
private String codPeriodo;
private String situacionRegistro;
private String cargo;
private String nomPeriodo;
private String nomSede;

public String getCargo() {
	return cargo;
}
public void setCargo(String cargo) {
	this.cargo = cargo;
}

//********************************
private String seleccion;

/*Agregado*/
private String dscSistEval;
private String dscEvaluador;

public String getCodCurso() {
	return codCurso;
}
public void setCodCurso(String codCurso) {
	this.codCurso = codCurso;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getCodProducto() {
	return codProducto;
}
public void setCodProducto(String codProducto) {
	this.codProducto = codProducto;
}
public String getCodEspecialidad() {
	return codEspecialidad;
}
public void setCodEspecialidad(String codEspecialidad) {
	this.codEspecialidad = codEspecialidad;
}
public String getCodCiclo() {
	return codCiclo;
}
public void setCodCiclo(String codCiclo) {
	this.codCiclo = codCiclo;
}
public String getCodSisteVal() {
	return codSisteVal;
}
public void setCodSisteVal(String codSisteVal) {
	this.codSisteVal = codSisteVal;
}
public String getCodPrograma() {
	return codPrograma;
}
public void setCodPrograma(String codPrograma) {
	this.codPrograma = codPrograma;
}
public String getCodEvaluador() {
	return codEvaluador;
}
public void setCodEvaluador(String codEvaluador) {
	this.codEvaluador = codEvaluador;
}
public String getCodPeriodo() {
	return codPeriodo;
}
public void setCodPeriodo(String codPeriodo) {
	this.codPeriodo = codPeriodo;
}
public String getSituacionRegistro() {
	return situacionRegistro;
}
public void setSituacionRegistro(String situacionRegistro) {
	this.situacionRegistro = situacionRegistro;
}


public String getDscSistEval() {
	return dscSistEval;
}
public void setDscSistEval(String dscSistEval) {
	this.dscSistEval = dscSistEval;
}
public String getDscEvaluador() {
	return dscEvaluador;
}
public void setDscEvaluador(String dscEvaluador) {
	this.dscEvaluador = dscEvaluador;
}
public String getSeleccion() {
	return seleccion;
}
public void setSeleccion(String seleccion) {
	this.seleccion = seleccion;
}
public String getNomCurso() {
	return nomCurso;
}
public void setNomCurso(String nomCurso) {
	this.nomCurso = nomCurso;
}
public String getNomPeriodo() {
	return nomPeriodo;
}
public void setNomPeriodo(String nomPeriodo) {
	this.nomPeriodo = nomPeriodo;
}
public String getNomSede() {
	return nomSede;
}
public void setNomSede(String nomSede) {
	this.nomSede = nomSede;
}
}
