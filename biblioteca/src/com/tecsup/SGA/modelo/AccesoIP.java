package com.tecsup.SGA.modelo;

import java.io.Serializable;

public class AccesoIP implements Serializable{

	private static final long serialVersionUID = 4989819839855394566L;
	
	private String numIP;
	private String usuCreacion;
	private String feCreacion;
	
	public String getNumIP() {
		return numIP;
	}
	public void setNumIP(String numIP) {
		this.numIP = numIP;
	}
	public String getUsuCreacion() {
		return usuCreacion;
	}
	public void setUsuCreacion(String usuCreacion) {
		this.usuCreacion = usuCreacion;
	}
	public String getFeCreacion() {
		return feCreacion;
	}
	public void setFeCreacion(String feCreacion) {
		this.feCreacion = feCreacion;
	}
	
	
	
}
