package com.tecsup.SGA.modelo;

public class CondicionCotizacion {
	
	private String codCondicion;
	private String codTipoCondicion;
	private String desTipoCondicion;
	private String detalleInicial;
	private String detalleFinal;
	private String indSeleccion;
	
	public String getCodCondicion() {
		return codCondicion;
	}
	public void setCodCondicion(String codCondicion) {
		this.codCondicion = codCondicion;
	}
	public String getCodTipoCondicion() {
		return codTipoCondicion;
	}
	public void setCodTipoCondicion(String codTipoCondicion) {
		this.codTipoCondicion = codTipoCondicion;
	}
	public String getDesTipoCondicion() {
		return desTipoCondicion;
	}
	public void setDesTipoCondicion(String desTipoCondicion) {
		this.desTipoCondicion = desTipoCondicion;
	}
	public String getDetalleInicial() {
		return detalleInicial;
	}
	public void setDetalleInicial(String detalleInicial) {
		this.detalleInicial = detalleInicial;
	}
	public String getDetalleFinal() {
		return detalleFinal;
	}
	public void setDetalleFinal(String detalleFinal) {
		this.detalleFinal = detalleFinal;
	}
	public String getIndSeleccion() {
		return indSeleccion;
	}
	public void setIndSeleccion(String indSeleccion) {
		this.indSeleccion = indSeleccion;
	}
	
}
