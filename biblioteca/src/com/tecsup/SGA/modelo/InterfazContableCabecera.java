package com.tecsup.SGA.modelo;

public class InterfazContableCabecera {

	private String csubdia;
	private String ccompro;
	private String cfeccom;
	private String ccodmon;
	private String csitua;
	private String ctipcam;
	private String cglosa;
	private String ctotal;
	private String ctipo;
	private String cflag;
	private String cdate;
	private String chora;
	private String cuser;
	private String cfeccam;
	private String corig;
	private String cform;
	private String ctipcom;
	private String cextor;
	private String cfeccom2;
	private String cfeccam2;
	private String copcion;
	
	public String getCsubdia() {
		return csubdia;
	}
	public void setCsubdia(String csubdia) {
		this.csubdia = csubdia;
	}
	public String getCcompro() {
		return ccompro;
	}
	public void setCcompro(String ccompro) {
		this.ccompro = ccompro;
	}
	public String getCfeccom() {
		return cfeccom;
	}
	public void setCfeccom(String cfeccom) {
		this.cfeccom = cfeccom;
	}
	public String getCcodmon() {
		return ccodmon;
	}
	public void setCcodmon(String ccodmon) {
		this.ccodmon = ccodmon;
	}
	public String getCsitua() {
		return csitua;
	}
	public void setCsitua(String csitua) {
		this.csitua = csitua;
	}
	public String getCtipcam() {
		return ctipcam;
	}
	public void setCtipcam(String ctipcam) {
		this.ctipcam = ctipcam;
	}
	public String getCglosa() {
		return cglosa;
	}
	public void setCglosa(String cglosa) {
		this.cglosa = cglosa;
	}
	public String getCtotal() {
		return ctotal;
	}
	public void setCtotal(String ctotal) {
		this.ctotal = ctotal;
	}
	public String getCtipo() {
		return ctipo;
	}
	public void setCtipo(String ctipo) {
		this.ctipo = ctipo;
	}
	public String getCflag() {
		return cflag;
	}
	public void setCflag(String cflag) {
		this.cflag = cflag;
	}
	public String getCdate() {
		return cdate;
	}
	public void setCdate(String cdate) {
		this.cdate = cdate;
	}
	public String getChora() {
		return chora;
	}
	public void setChora(String chora) {
		this.chora = chora;
	}
	public String getCuser() {
		return cuser;
	}
	public void setCuser(String cuser) {
		this.cuser = cuser;
	}
	public String getCfeccam() {
		return cfeccam;
	}
	public void setCfeccam(String cfeccam) {
		this.cfeccam = cfeccam;
	}
	public String getCorig() {
		return corig;
	}
	public void setCorig(String corig) {
		this.corig = corig;
	}
	public String getCform() {
		return cform;
	}
	public void setCform(String cform) {
		this.cform = cform;
	}
	public String getCtipcom() {
		return ctipcom;
	}
	public void setCtipcom(String ctipcom) {
		this.ctipcom = ctipcom;
	}
	public String getCextor() {
		return cextor;
	}
	public void setCextor(String cextor) {
		this.cextor = cextor;
	}
	public String getCfeccom2() {
		return cfeccom2;
	}
	public void setCfeccom2(String cfeccom2) {
		this.cfeccom2 = cfeccom2;
	}
	public String getCfeccam2() {
		return cfeccam2;
	}
	public void setCfeccam2(String cfeccam2) {
		this.cfeccam2 = cfeccam2;
	}
	public String getCopcion() {
		return copcion;
	}
	public void setCopcion(String copcion) {
		this.copcion = copcion;
	}
}
