package com.tecsup.SGA.modelo;

public class HibridoCurso implements java.io.Serializable{
	
	private String codGenerado;
	private String codCurso;
	private String codigo;
	private String cadena;
	private String nroRegistros;
	
	public String getCadena() {
		return cadena;
	}
	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
	public String getNroRegistros() {
		return nroRegistros;
	}
	public void setNroRegistros(String nroRegistros) {
		this.nroRegistros = nroRegistros;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodGenerado() {
		return codGenerado;
	}
	public void setCodGenerado(String codGenerado) {
		this.codGenerado = codGenerado;
	}
		

}
