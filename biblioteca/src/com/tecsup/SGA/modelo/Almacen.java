package com.tecsup.SGA.modelo;

public class Almacen {

	private String codOrden;
	private String nroOrden;
	private String fecEmision;
	private String codProveedor;
	private String dscRazonSocial;
	private String dscMoneda;
	private String montoTotal;
	private String dscComprador;
	
	private String nroProveedor;
	private String dscProveedor;
	private String dscAtencion;
	
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFecEmision() {
		return fecEmision;
	}
	public void setFecEmision(String fecEmision) {
		this.fecEmision = fecEmision;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public String getDscRazonSocial() {
		return dscRazonSocial;
	}
	public void setDscRazonSocial(String dscRazonSocial) {
		this.dscRazonSocial = dscRazonSocial;
	}
	public String getDscMoneda() {
		return dscMoneda;
	}
	public void setDscMoneda(String dscMoneda) {
		this.dscMoneda = dscMoneda;
	}
	public String getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(String montoTotal) {
		this.montoTotal = montoTotal;
	}
	public String getDscComprador() {
		return dscComprador;
	}
	public void setDscComprador(String dscComprador) {
		this.dscComprador = dscComprador;
	}
	public String getNroProveedor() {
		return nroProveedor;
	}
	public void setNroProveedor(String nroProveedor) {
		this.nroProveedor = nroProveedor;
	}
	public String getDscProveedor() {
		return dscProveedor;
	}
	public void setDscProveedor(String dscProveedor) {
		this.dscProveedor = dscProveedor;
	}
	public String getDscAtencion() {
		return dscAtencion;
	}
	public void setDscAtencion(String dscAtencion) {
		this.dscAtencion = dscAtencion;
	}
	
}
