package com.tecsup.SGA.modelo;

public class CentrosCostos {
	private String sede;
	private String codSeco;
	private String descripcion;
	private String estado;
	
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getCodSeco() {
		return codSeco;
	}
	public void setCodSeco(String codSeco) {
		this.codSeco = codSeco;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
