package com.tecsup.SGA.modelo;

import java.util.List;

public class Examen implements java.io.Serializable{
	
	private String codigo;
	private String descripcion;
	
	private String codAlumno;
	private String nombre;
	private String nota;
	private String flagNP;
	private String flagAN;
	private String flagSU; //JHPR 2008-7-11
	private String codCurso;
	private String codEvaluacion; //tabla eva_examenes evlu_id / codigo es tipo exam
	private List resultado;
	
	//JHPR: 2008-05-02 Para obtener el alias de los tipos de ex�menes (Ej: Examen 1 = EX1)
	private String alias;
	
	private String flagIndExamRecu;
	private String flagIndExamSub;
	private String flagIndExamPar;
	private String flagRC;//1 POS:RECUPERACION
	private String flagPosSub;//1 POS:SUBSANCAION	
	private String flagPosPar;//1 POS:EXAMEN PARCIAL/FINAL
	
	private String tipoExamen;
	private String codSeccion;
	private String desSeccion;
	private String nomCurso;
	private String examenNp;
	private String tipoExamenNp;
	private String codCarnet;
	private String tieneNotaSubsa; //registra nota de subsanacion registrada
	/**
	 * @return the codCarnet
	 */
	public String getCodCarnet() {
		return codCarnet;
	}
	/**
	 * @param codCarnet the codCarnet to set
	 */
	public void setCodCarnet(String codCarnet) {
		this.codCarnet = codCarnet;
	}
	public String getNomCurso() {
		return nomCurso;
	}
	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}
	public String getTipoExamen() {
		return tipoExamen;
	}
	public void setTipoExamen(String tipoExamen) {
		this.tipoExamen = tipoExamen;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public String getFlagNP() {
		return flagNP;
	}
	public void setFlagNP(String flagNP) {
		this.flagNP = flagNP;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodEvaluacion() {
		return codEvaluacion;
	}
	public void setCodEvaluacion(String codEvaluacion) {
		this.codEvaluacion = codEvaluacion;
	}
	public List getResultado() {
		return resultado;
	}
	public void setResultado(List resultado) {
		this.resultado = resultado;
	}
	public String getFlagAN() {
		return flagAN;
	}
	public void setFlagAN(String flagAN) {
		this.flagAN = flagAN;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getFlagIndExamRecu() {
		return flagIndExamRecu;
	}
	public void setFlagIndExamRecu(String flagIndExamRecu) {
		this.flagIndExamRecu = flagIndExamRecu;
	}
	public String getFlagIndExamSub() {
		return flagIndExamSub;
	}
	public void setFlagIndExamSub(String flagIndExamSub) {
		this.flagIndExamSub = flagIndExamSub;
	}
	public String getFlagIndExamPar() {
		return flagIndExamPar;
	}
	public void setFlagIndExamPar(String flagIndExamPar) {
		this.flagIndExamPar = flagIndExamPar;
	}
	public String getFlagRC() {
		return flagRC;
	}
	public void setFlagRC(String flagRC) {
		this.flagRC = flagRC;
	}
	public String getFlagPosSub() {
		return flagPosSub;
	}
	public void setFlagPosSub(String flagPosSub) {
		this.flagPosSub = flagPosSub;
	}
	public String getFlagPosPar() {
		return flagPosPar;
	}
	public void setFlagPosPar(String flagPosPar) {
		this.flagPosPar = flagPosPar;
	}
	public String getFlagSU() {
		return flagSU;
	}
	public void setFlagSU(String flagSU) {
		this.flagSU = flagSU;
	}
	public String getDesSeccion() {
		return desSeccion;
	}
	public void setDesSeccion(String desSeccion) {
		this.desSeccion = desSeccion;
	}
	public String getExamenNp() {
		return examenNp;
	}
	public void setExamenNp(String examenNp) {
		this.examenNp = examenNp;
	}
	public String getTipoExamenNp() {
		return tipoExamenNp;
	}
	public void setTipoExamenNp(String tipoExamenNp) {
		this.tipoExamenNp = tipoExamenNp;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getTieneNotaSubsa() {
		return tieneNotaSubsa;
	}
	public void setTieneNotaSubsa(String tieneNotaSubsa) {
		this.tieneNotaSubsa = tieneNotaSubsa;
	}
	
	
	
		
}
