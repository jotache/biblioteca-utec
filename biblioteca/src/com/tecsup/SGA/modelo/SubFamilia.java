package com.tecsup.SGA.modelo;

public class SubFamilia implements java.io.Serializable{
	private String codUnico;
	private String codGenerado;
	private String desSubFam;
	private String codBien;
	private String desBien;
	private String codFam;
	private String desFam;
	private String responsable;
	private String codResponsable;
	
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getCodGenerado() {
		return codGenerado;
	}
	public void setCodGenerado(String codGenerado) {
		this.codGenerado = codGenerado;
	}
	public String getDesSubFam() {
		return desSubFam;
	}
	public void setDesSubFam(String desSubFam) {
		this.desSubFam = desSubFam;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getDesBien() {
		return desBien;
	}
	public void setDesBien(String desBien) {
		this.desBien = desBien;
	}
	public String getCodFam() {
		return codFam;
	}
	public void setCodFam(String codFam) {
		this.codFam = codFam;
	}
	public String getDesFam() {
		return desFam;
	}
	public void setDesFam(String desFam) {
		this.desFam = desFam;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	
}
