package com.tecsup.SGA.modelo;

public class EncuestaEmpresa implements java.io.Serializable{
	
	private String codEmpresa;
	private String nombreEmpresa;
	
	public String getCodEmpresa() {
		return codEmpresa;
	}
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

}
