package com.tecsup.SGA.modelo;

public class CursosHibrido implements java.io.Serializable{
	
	private String codCurso;
	private String descripcion;
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
		
}
