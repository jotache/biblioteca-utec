package com.tecsup.SGA.modelo;

public class Usuario implements java.io.Serializable {

	private String codUsuario;
	private String nomUsuario;
	private String codPerfil;
	private String dscPerfil;
	
	private String codPersonal;
	private String nombrePersonal;
	private String codTipoPersonal;
	private String nombreTipoPersonal;
	//atributo para encuesta
	private String tieneEncuesta;
	
	
	public String getTieneEncuesta() {
		return tieneEncuesta;
	}
	public void setTieneEncuesta(String tieneEncuesta) {
		this.tieneEncuesta = tieneEncuesta;
	}
	public String getCodPersonal() {
		return codPersonal;
	}
	public void setCodPersonal(String codPersonal) {
		this.codPersonal = codPersonal;
	}
	public String getNombrePersonal() {
		return nombrePersonal;
	}
	public void setNombrePersonal(String nombrePersonal) {
		this.nombrePersonal = nombrePersonal;
	}
	public String getCodTipoPersonal() {
		return codTipoPersonal;
	}
	public void setCodTipoPersonal(String codTipoPersonal) {
		this.codTipoPersonal = codTipoPersonal;
	}
	public String getNombreTipoPersonal() {
		return nombreTipoPersonal;
	}
	public void setNombreTipoPersonal(String nombreTipoPersonal) {
		this.nombreTipoPersonal = nombreTipoPersonal;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getDscPerfil() {
		return dscPerfil;
	}
	public void setDscPerfil(String dscPerfil) {
		this.dscPerfil = dscPerfil;
	}
	
	
}
