package com.tecsup.SGA.modelo;

public class BuzonSugerencia implements java.io.Serializable{

	private String mes;
	private String anio;
	private String tipoSugerencia;
	private String estadoSugerencia;
	
	private String codSugerencia;
	private String fechaCrea;
	private String codTipoSugerencia;
	private String estado;
	private String descripTipoSugerencia;
	private String descripcion;
	private String codCalificacion;
	private String descripCalificacion;
	private String codigoMaterial;
	private String descripCodMaterial;
	private String codUsuarioCrea;
	private String descripcionUsuario;
	private String respuesta;
	
	private String email;
	private String usuario;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getDescripTipoSugerencia() {
		return descripTipoSugerencia;
	}
	public void setDescripTipoSugerencia(String descripTipoSugerencia) {
		this.descripTipoSugerencia = descripTipoSugerencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getCodCalificacion() {
		return codCalificacion;
	}
	public void setCodCalificacion(String codCalificacion) {
		this.codCalificacion = codCalificacion;
	}
	public String getDescripCalificacion() {
		return descripCalificacion;
	}
	public void setDescripCalificacion(String descripCalificacion) {
		this.descripCalificacion = descripCalificacion;
	}
	public String getCodigoMaterial() {
		return codigoMaterial;
	}
	public void setCodigoMaterial(String codigoMaterial) {
		this.codigoMaterial = codigoMaterial;
	}
	public String getDescripCodMaterial() {
		return descripCodMaterial;
	}
	public void setDescripCodMaterial(String descripCodMaterial) {
		this.descripCodMaterial = descripCodMaterial;
	}
	public String getCodUsuarioCrea() {
		return codUsuarioCrea;
	}
	public void setCodUsuarioCrea(String codUsuarioCrea) {
		this.codUsuarioCrea = codUsuarioCrea;
	}
	public String getCodSugerencia() {
		return codSugerencia;
	}
	public void setCodSugerencia(String codSugerencia) {
		this.codSugerencia = codSugerencia;
	}
	public String getFechaCrea() {
		return fechaCrea;
	}
	public void setFechaCrea(String fechaCrea) {
		this.fechaCrea = fechaCrea;
	}
	public String getCodTipoSugerencia() {
		return codTipoSugerencia;
	}
	public void setCodTipoSugerencia(String codTipoSugerencia) {
		this.codTipoSugerencia = codTipoSugerencia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getTipoSugerencia() {
		return tipoSugerencia;
	}
	public void setTipoSugerencia(String tipoSugerencia) {
		this.tipoSugerencia = tipoSugerencia;
	}
	public String getEstadoSugerencia() {
		return estadoSugerencia;
	}
	public void setEstadoSugerencia(String estadoSugerencia) {
		this.estadoSugerencia = estadoSugerencia;
	}
	public String getDescripcionUsuario() {
		return descripcionUsuario;
	}
	public void setDescripcionUsuario(String descripcionUsuario) {
		this.descripcionUsuario = descripcionUsuario;
	}
}
