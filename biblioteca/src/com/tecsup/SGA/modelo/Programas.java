package com.tecsup.SGA.modelo;

public class Programas {
private String codPrograma;
private String codProducto;
private String descripcion;
private String sitRegistro;

public String getCodPrograma() {
	return codPrograma;
}
public void setCodPrograma(String codPrograma) {
	this.codPrograma = codPrograma;
}
public String getCodProducto() {
	return codProducto;
}
public void setCodProducto(String codProducto) {
	this.codProducto = codProducto;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getSitRegistro() {
	return sitRegistro;
}
public void setSitRegistro(String sitRegistro) {
	this.sitRegistro = sitRegistro;
}
}
