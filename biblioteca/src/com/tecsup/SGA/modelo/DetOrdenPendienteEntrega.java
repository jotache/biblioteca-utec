package com.tecsup.SGA.modelo;

public class DetOrdenPendienteEntrega {
	private String numOrdCompra;
	private String desTipoPago;
	private String fecAprobacion;
	private String nomComprador;
	private String simMoneda;
	private String subTotal;
	private String totIGV;
	private String totTotal;
	private String nomProducto;
	private String canTotal;
	private String canSaldo;
	private String nomProveedor;
	
	public String getNumOrdCompra() {
		return numOrdCompra;
	}
	public void setNumOrdCompra(String numOrdCompra) {
		this.numOrdCompra = numOrdCompra;
	}
	public String getDesTipoPago() {
		return desTipoPago;
	}
	public void setDesTipoPago(String desTipoPago) {
		this.desTipoPago = desTipoPago;
	}
	public String getFecAprobacion() {
		return fecAprobacion;
	}
	public void setFecAprobacion(String fecAprobacion) {
		this.fecAprobacion = fecAprobacion;
	}
	public String getNomComprador() {
		return nomComprador;
	}
	public void setNomComprador(String nomComprador) {
		this.nomComprador = nomComprador;
	}
	public String getMoneda() {
		return simMoneda;
	}
	public void setMoneda(String moneda) {
		this.simMoneda = moneda;
	}
	public String getSubTotal(){
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getTotIGV() {
		return totIGV;
	}
	public void setTotIGV(String totIGV) {
		this.totIGV = totIGV;
	}
	public String getTotalOC() {
		return totTotal;
	}
	public void setTotalOC(String totalOC) {
		this.totTotal = totalOC;
	}
	public String getNomProducto() {
		return nomProducto;
	}
	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}
	public String getCanTotal() {
		return canTotal;
	}
	public void setCanTotal(String canTotal) {
		this.canTotal = canTotal;
	}
	public String getCanSaldo() {
		return canSaldo;
	}
	public void setCanSaldo(String canSaldo) {
		this.canSaldo = canSaldo;
	}
	public String getNomProveedor() {
		return nomProveedor;
	}
	public void setNomProveedor(String nomProveedor) {
		this.nomProveedor = nomProveedor;
	}

}
