package com.tecsup.SGA.modelo;

import java.io.Serializable;

public class PresupuestoCentroCosto implements Serializable {
	
	private static final long serialVersionUID = 4742541562686907669L;
	public PresupuestoCentroCosto(){}
	
	private String id;
	private String anio;
	private String codCencos;
	private String codMoneda;
	private String desMoneda;
	private String descripcion;
	private String responsableCencos;
	private String importe;
	private String flgControl;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getCodCencos() {
		return codCencos;
	}
	public void setCodCencos(String codCencos) {
		this.codCencos = codCencos;
	}
	public String getCodMoneda() {
		return codMoneda;
	}
	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getFlgControl() {
		return flgControl;
	}
	public void setFlgControl(String flgControl) {
		this.flgControl = flgControl;
	}
	public String getDesMoneda() {
		return desMoneda;
	}
	public void setDesMoneda(String desMoneda) {
		this.desMoneda = desMoneda;
	}
	public String getResponsableCencos() {
		return responsableCencos;
	}
	public void setResponsableCencos(String responsableCencos) {
		this.responsableCencos = responsableCencos;
	}		
}
