package com.tecsup.SGA.modelo;

public class Grupo implements java.io.Serializable{
	private String codGrupo;
	private String nomGrupo;
	private String codFormato;
	private String nomFormato;
	private String pesoGrupo;
	private String indAlternativa;
	private String desAlternativa;
	private String preguntasRel;
	private String alternativasRel;
	
	public String getCodGrupo() {
		return codGrupo;
	}
	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}
	public String getNomGrupo() {
		return nomGrupo;
	}
	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}
	public String getCodFormato() {
		return codFormato;
	}
	public void setCodFormato(String codFormato) {
		this.codFormato = codFormato;
	}
	public String getNomFormato() {
		return nomFormato;
	}
	public void setNomFormato(String nomFormato) {
		this.nomFormato = nomFormato;
	}
	public String getPesoGrupo() {
		return pesoGrupo;
	}
	public void setPesoGrupo(String pesoGrupo) {
		this.pesoGrupo = pesoGrupo;
	}
	public String getIndAlternativa() {
		return indAlternativa;
	}
	public void setIndAlternativa(String indAlternativa) {
		this.indAlternativa = indAlternativa;
	}
	public String getDesAlternativa() {
		return desAlternativa;
	}
	public void setDesAlternativa(String desAlternativa) {
		this.desAlternativa = desAlternativa;
	}
	public String getPreguntasRel() {
		return preguntasRel;
	}
	public void setPreguntasRel(String preguntasRel) {
		this.preguntasRel = preguntasRel;
	}
	public String getAlternativasRel() {
		return alternativasRel;
	}
	public void setAlternativasRel(String alternativasRel) {
		this.alternativasRel = alternativasRel;
	}
	
	
	
	
	
	
		
}
