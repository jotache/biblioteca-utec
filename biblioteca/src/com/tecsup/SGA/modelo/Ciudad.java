package com.tecsup.SGA.modelo;

public class Ciudad implements java.io.Serializable{
	
	private String paisId;
	private String ciudadId;
	private String ciudadDescripcion;
	private String ciudadCodigo;
	private String estReg;
	private String usuCrea;
	private String paisDescripcion;
	
	public String getPaisId() {
		return paisId;
	}
	public void setPaisId(String paisId) {
		this.paisId = paisId;
	}
	public String getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(String ciudadId) {
		this.ciudadId = ciudadId;
	}
	public String getCiudadDescripcion() {
		return ciudadDescripcion;
	}
	public void setCiudadDescripcion(String ciudadDescripcion) {
		this.ciudadDescripcion = ciudadDescripcion;
	}
	public String getCiudadCodigo() {
		return ciudadCodigo;
	}
	public void setCiudadCodigo(String ciudadCodigo) {
		this.ciudadCodigo = ciudadCodigo;
	}
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	public String getUsuCrea() {
		return usuCrea;
	}
	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}
	public String getPaisDescripcion() {
		return paisDescripcion;
	}
	public void setPaisDescripcion(String paisDescripcion) {
		this.paisDescripcion = paisDescripcion;
	}

}
