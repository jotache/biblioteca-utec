package com.tecsup.SGA.modelo;
import java.util.List;
public class HorasXalumnos implements java.io.Serializable{
private String cantHoras;
private String horas;

public String getCantHoras() {
	return cantHoras;
}

public void setCantHoras(String cantHoras) {
	this.cantHoras = cantHoras;
}

public String getHoras() {
	return horas;
}

public void setHoras(String horas) {
	this.horas = horas;
}
}
