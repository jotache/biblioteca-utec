package com.tecsup.SGA.modelo;

public class InterfazContableDetalle {
	
	private String dsubdia;
	private String dcompro;
	private String dsecue;
	private String dfeccom;
	private String dcuenta;
	private String dcodane;
	private String dcencos;
	private String dcodmon;
	private String ddh;
	private String dimport;
	private String dtipdoc;
	private String dnumdoc;
	private String dfecdoc;
	private String dfecven;
	private String darea;
	private String dflag;
	private String ddate;
	private String dxglosa;
	private String dusimpor;
	private String dmnimpor;
	private String dcodarc;
	private String dfeccom2;
	private String dfecdoc2;
	private String dfecven2;
	private String dcodane2;
	private String dvanexo;
	private String dvanexo2;
	private String dtipcam;
	private String dcantid;
	private String dtipdor;
	private String dfecdo2;
	private String dtiptas;
	private String dimptas;
	private String dimpbmn;
	private String dimpbus;
	private String dinacom;
	private String digvcom;
	private String drete;
	private String dporre;
	private String dmedpag;
	private String dmoncom;
	private String dcolcom;
	private String dbascom;
	private String dtpconv;
	private String dflgcom;
	private String dtipaco;
	private String danecom;
	private String dnumdor;
	
	public String getDnumdor() {
		return dnumdor;
	}
	public void setDnumdor(String dnumdor) {
		this.dnumdor = dnumdor;
	}
	public String getDsubdia() {
		return dsubdia;
	}
	public void setDsubdia(String dsubdia) {
		this.dsubdia = dsubdia;
	}
	public String getDcompro() {
		return dcompro;
	}
	public void setDcompro(String dcompro) {
		this.dcompro = dcompro;
	}
	public String getDsecue() {
		return dsecue;
	}
	public void setDsecue(String dsecue) {
		this.dsecue = dsecue;
	}
	public String getDfeccom() {
		return dfeccom;
	}
	public void setDfeccom(String dfeccom) {
		this.dfeccom = dfeccom;
	}
	public String getDcuenta() {
		return dcuenta;
	}
	public void setDcuenta(String dcuenta) {
		this.dcuenta = dcuenta;
	}
	public String getDcodane() {
		return dcodane;
	}
	public void setDcodane(String dcodane) {
		this.dcodane = dcodane;
	}
	public String getDcencos() {
		return dcencos;
	}
	public void setDcencos(String dcencos) {
		this.dcencos = dcencos;
	}
	public String getDcodmon() {
		return dcodmon;
	}
	public void setDcodmon(String dcodmon) {
		this.dcodmon = dcodmon;
	}
	public String getDdh() {
		return ddh;
	}
	public void setDdh(String ddh) {
		this.ddh = ddh;
	}
	public String getDimport() {
		return dimport;
	}
	public void setDimport(String dimport) {
		this.dimport = dimport;
	}
	public String getDtipdoc() {
		return dtipdoc;
	}
	public void setDtipdoc(String dtipdoc) {
		this.dtipdoc = dtipdoc;
	}
	public String getDnumdoc() {
		return dnumdoc;
	}
	public void setDnumdoc(String dnumdoc) {
		this.dnumdoc = dnumdoc;
	}
	public String getDfecdoc() {
		return dfecdoc;
	}
	public void setDfecdoc(String dfecdoc) {
		this.dfecdoc = dfecdoc;
	}
	public String getDfecven() {
		return dfecven;
	}
	public void setDfecven(String dfecven) {
		this.dfecven = dfecven;
	}
	public String getDarea() {
		return darea;
	}
	public void setDarea(String darea) {
		this.darea = darea;
	}
	public String getDflag() {
		return dflag;
	}
	public void setDflag(String dflag) {
		this.dflag = dflag;
	}
	public String getDdate() {
		return ddate;
	}
	public void setDdate(String ddate) {
		this.ddate = ddate;
	}
	public String getDxglosa() {
		return dxglosa;
	}
	public void setDxglosa(String dxglosa) {
		this.dxglosa = dxglosa;
	}
	public String getDusimpor() {
		return dusimpor;
	}
	public void setDusimpor(String dusimpor) {
		this.dusimpor = dusimpor;
	}
	public String getDmnimpor() {
		return dmnimpor;
	}
	public void setDmnimpor(String dmnimpor) {
		this.dmnimpor = dmnimpor;
	}
	public String getDcodarc() {
		return dcodarc;
	}
	public void setDcodarc(String dcodarc) {
		this.dcodarc = dcodarc;
	}
	public String getDfeccom2() {
		return dfeccom2;
	}
	public void setDfeccom2(String dfeccom2) {
		this.dfeccom2 = dfeccom2;
	}
	public String getDfecdoc2() {
		return dfecdoc2;
	}
	public void setDfecdoc2(String dfecdoc2) {
		this.dfecdoc2 = dfecdoc2;
	}
	public String getDfecven2() {
		return dfecven2;
	}
	public void setDfecven2(String dfecven2) {
		this.dfecven2 = dfecven2;
	}
	public String getDcodane2() {
		return dcodane2;
	}
	public void setDcodane2(String dcodane2) {
		this.dcodane2 = dcodane2;
	}
	public String getDvanexo() {
		return dvanexo;
	}
	public void setDvanexo(String dvanexo) {
		this.dvanexo = dvanexo;
	}
	public String getDvanexo2() {
		return dvanexo2;
	}
	public void setDvanexo2(String dvanexo2) {
		this.dvanexo2 = dvanexo2;
	}
	public String getDtipcam() {
		return dtipcam;
	}
	public void setDtipcam(String dtipcam) {
		this.dtipcam = dtipcam;
	}
	public String getDcantid() {
		return dcantid;
	}
	public void setDcantid(String dcantid) {
		this.dcantid = dcantid;
	}
	public String getDtipdor() {
		return dtipdor;
	}
	public void setDtipdor(String dtipdor) {
		this.dtipdor = dtipdor;
	}
	public String getDfecdo2() {
		return dfecdo2;
	}
	public void setDfecdo2(String dfecdo2) {
		this.dfecdo2 = dfecdo2;
	}
	public String getDtiptas() {
		return dtiptas;
	}
	public void setDtiptas(String dtiptas) {
		this.dtiptas = dtiptas;
	}
	public String getDimptas() {
		return dimptas;
	}
	public void setDimptas(String dimptas) {
		this.dimptas = dimptas;
	}
	public String getDimpbmn() {
		return dimpbmn;
	}
	public void setDimpbmn(String dimpbmn) {
		this.dimpbmn = dimpbmn;
	}
	public String getDimpbus() {
		return dimpbus;
	}
	public void setDimpbus(String dimpbus) {
		this.dimpbus = dimpbus;
	}
	public String getDinacom() {
		return dinacom;
	}
	public void setDinacom(String dinacom) {
		this.dinacom = dinacom;
	}
	public String getDigvcom() {
		return digvcom;
	}
	public void setDigvcom(String digvcom) {
		this.digvcom = digvcom;
	}
	public String getDrete() {
		return drete;
	}
	public void setDrete(String drete) {
		this.drete = drete;
	}
	public String getDporre() {
		return dporre;
	}
	public void setDporre(String dporre) {
		this.dporre = dporre;
	}
	public String getDmedpag() {
		return dmedpag;
	}
	public void setDmedpag(String dmedpag) {
		this.dmedpag = dmedpag;
	}
	public String getDmoncom() {
		return dmoncom;
	}
	public void setDmoncom(String dmoncom) {
		this.dmoncom = dmoncom;
	}
	public String getDcolcom() {
		return dcolcom;
	}
	public void setDcolcom(String dcolcom) {
		this.dcolcom = dcolcom;
	}
	public String getDbascom() {
		return dbascom;
	}
	public void setDbascom(String dbascom) {
		this.dbascom = dbascom;
	}
	public String getDtpconv() {
		return dtpconv;
	}
	public void setDtpconv(String dtpconv) {
		this.dtpconv = dtpconv;
	}
	public String getDflgcom() {
		return dflgcom;
	}
	public void setDflgcom(String dflgcom) {
		this.dflgcom = dflgcom;
	}
	public String getDtipaco() {
		return dtipaco;
	}
	public void setDtipaco(String dtipaco) {
		this.dtipaco = dtipaco;
	}
	public String getDanecom() {
		return danecom;
	}
	public void setDanecom(String danecom) {
		this.danecom = danecom;
	}

	
}