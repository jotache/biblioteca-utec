package com.tecsup.SGA.modelo;


public class CentroCosto {
	private String codSede; 
	private String codTecsup;
	private String descripcion;
	private String estado;
	private String codSecuencial;
	
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodTecsup() {
		return codTecsup;
	}
	public void setCodTecsup(String codTecsup) {
		this.codTecsup = codTecsup;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodSecuencial() {
		return codSecuencial;
	}
	public void setCodSecuencial(String codSecuencial) {
		this.codSecuencial = codSecuencial;
	}	
}
