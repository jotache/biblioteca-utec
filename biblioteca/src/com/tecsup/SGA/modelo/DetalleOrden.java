package com.tecsup.SGA.modelo;

public class DetalleOrden implements java.io.Serializable{

	private String descripcion;
	private String cantidad;
	private String precio;
	private String subtotal;
	private String dscCondicion;
	
	private String dscNroOrden;
	private String fechaEmision;
	private String fechaAprobacion;
	private String dscNroDocumento;
	
	private String dscProveedor;
	private String dscAtencionA;
	private String dscComprador;
	private String dscMonto;
	private String codEstado;
	private String dscMoneda;
	private String subTotal1;
	private String igv;
	private String dscUnidad;
	//ALQD,18/08/08. A�adiendo nuevas propiedas para lanzar cotizaciones
	private String codCotizacion;
	private String codEstCotizacion;
	private String codTipoReq;
	
	private String indImportacion;
	private String importeOtros;
	private String descEstadoOC; //jhpr 2008-09-25
	private String tipoDePago;
	private String comentario; //ALMACENA LAS NOTAS INGRESADAS POR LOS USUARIOS
	
	private String codBien;
	//ALQD,28/01/09. A�ADIENDO NUEVA PROPIEDAD CADENA DE CENCOS
	private String cadCenCosto;
	//ALQD,05/02/09. NUEVA PROPIEDAD
	private String numCotizacion;
	//ALQD,19/02/09. NUEVAS PROPIEDADES
	private String indInversion;
	private String codOrdenTipoGasto;
	private String desOrdenTipoGasto;
	//ALQD,24/02/09. NUEVA PROPIEDAD
	private String desTipPago;
	//ALQD,25/05/09. NUEVA PROPIEDAD SEDE
	private String codSede;
	//ALQD,06/07/09.NUEVA PROPIEDAD
	private String prorrateo;
	//ALQD,07/07/09.NUEVAS PROPIEDADES
	private String pais;
	private String telefono;
	//ALQD,08/07/09.NUEVAS PROPIEDADES
	private String emailProv;
	private String emailComprador;
	
	public String getEmailProv() {
		return emailProv;
	}
	public void setEmailProv(String emailProv) {
		this.emailProv=emailProv;
	}
	public String getEmailComprador() {
		return emailComprador;
	}
	public void setEmailComprador(String emailComprador) {
		this.emailComprador=emailComprador;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais=pais;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono=telefono;
	}
	public String getProrrateo() {
		return prorrateo;
	}
	public void setProrrateo(String prorrateo) {
		this.prorrateo = prorrateo;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede=codSede;
	}
	public String getDesTipPago() {
		return desTipPago;
	}
	public void setDesTipPago(String desTipPago) {
		this.desTipPago=desTipPago;
	}
	public String getIndInversion() {
		return indInversion;
	}
	public void setIndInversion(String indInversion) {
		this.indInversion=indInversion;
	}
	public String getCodOrdenTipoGasto() {
		return codOrdenTipoGasto;
	}
	public void setCodOrdenTipoGasto(String codOrdenTipoGasto) {
		this.codOrdenTipoGasto=codOrdenTipoGasto;
	}
	public String getDesOrdenTipoGasto() {
		return desOrdenTipoGasto;
	}
	public void setDesOrdenTipoGasto(String desOrdenTipoGasto) {
		this.desOrdenTipoGasto=desOrdenTipoGasto;
	}
	public String getNumCotizacion() {
		return numCotizacion;
	}
	public void setNumCotizacion(String numCotizacion) {
		this.numCotizacion=numCotizacion;
	}
	public String getCadCenCosto() {
		return cadCenCosto;
	}
	public void setCadCenCosto(String cadCenCosto) {
		this.cadCenCosto=cadCenCosto;
	}
	/**
	 * @return the codBien
	 */
	public String getCodBien() {
		return codBien;
	}
	/**
	 * @param codBien the codBien to set
	 */
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	private String indTermino;
	private String nomCencos;
	private String usuSolicitante;
	
	
	/**
	 * @return the nomCencos
	 */
	public String getNomCencos() {
		return nomCencos;
	}
	/**
	 * @param nomCencos the nomCencos to set
	 */
	public void setNomCencos(String nomCencos) {
		this.nomCencos = nomCencos;
	}
	/**
	 * @return the usuSolicitante
	 */
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	/**
	 * @param usuSolicitante the usuSolicitante to set
	 */
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	/**
	 * @return the indTermino
	 */
	public String getIndTermino() {
		return indTermino;
	}
	/**
	 * @param indTermino the indTermino to set
	 */
	public void setIndTermino(String indTermino) {
		this.indTermino = indTermino;
	}
	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}
	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	/**
	 * @return the tipoDePago
	 */
	public String getTipoDePago() {
		return tipoDePago;
	}
	/**
	 * @param tipoDePago the tipoDePago to set
	 */
	public void setTipoDePago(String tipoDePago) {
		this.tipoDePago = tipoDePago;
	}
	/**
	 * @return the descEstadoOC
	 */
	public String getDescEstadoOC() {
		return descEstadoOC;
	}
	/**
	 * @param descEstadoOC the descEstadoOC to set
	 */
	public void setDescEstadoOC(String descEstadoOC) {
		this.descEstadoOC = descEstadoOC;
	}
	public String getIndImportacion() {
		return indImportacion;
	}
	public void setIndImportacion(String indImportacion) {
		this.indImportacion = indImportacion;
	}
	public String getImporteOtros() {
		return importeOtros;
	}
	public void setImporteOtros(String importeOtros) {
		this.importeOtros = importeOtros;
	}
	public String getDscMoneda() {
		return dscMoneda;
	}
	public void setDscMoneda(String dscMoneda) {
		this.dscMoneda = dscMoneda;
	}
	public String getSubTotal1() {
		return subTotal1;
	}
	public void setSubTotal1(String subTotal1) {
		this.subTotal1 = subTotal1;
	}
	public String getDscNroOrden() {
		return dscNroOrden;
	}
	public void setDscNroOrden(String dscNroOrden) {
		this.dscNroOrden = dscNroOrden;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getDscNroDocumento() {
		return dscNroDocumento;
	}
	public void setDscNroDocumento(String dscNroDocumento) {
		this.dscNroDocumento = dscNroDocumento;
	}
	public String getDscProveedor() {
		return dscProveedor;
	}
	public void setDscProveedor(String dscProveedor) {
		this.dscProveedor = dscProveedor;
	}
	public String getDscAtencionA() {
		return dscAtencionA;
	}
	public void setDscAtencionA(String dscAtencionA) {
		this.dscAtencionA = dscAtencionA;
	}
	public String getDscComprador() {
		return dscComprador;
	}
	public void setDscComprador(String dscComprador) {
		this.dscComprador = dscComprador;
	}
	public String getDscMonto() {
		return dscMonto;
	}
	public void setDscMonto(String dscMonto) {
		this.dscMonto = dscMonto;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getDscCondicion() {
		return dscCondicion;
	}
	public void setDscCondicion(String dscCondicion) {
		this.dscCondicion = dscCondicion;
	}
	public String getIgv() {
		return igv;
	}
	public void setIgv(String igv) {
		this.igv = igv;
	}
	public String getDscUnidad() {
		return dscUnidad;
	}
	public void setDscUnidad(String dscUnidad) {
		this.dscUnidad = dscUnidad;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}

	public void setCodCotizacion(String cotizacionID) {
		this.codCotizacion = cotizacionID;
	}

	public String getCodEstCotizacion() {
		return codEstCotizacion;
	}

	public void setCodEstCotizacion(String codEstado) {
		this.codEstCotizacion = codEstado;
	}
	public String getCodTipoReq() {
		return codTipoReq;
	}
	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}
	public String getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(String fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
}
