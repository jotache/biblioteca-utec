package com.tecsup.SGA.modelo;

public class Exclusion implements java.io.Serializable{
	private String codExclusion; 
	private String codAlumno;
	private String nomAlumno;
	private String codCurso;
	private String dscCurso;
	private String flagCurso;
	private String flagSemestre;
	private String usuario;
	
	public String getCodExclusion() {
		return codExclusion;
	}
	public void setCodExclusion(String codExclusion) {
		this.codExclusion = codExclusion;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNomAlumno() {
		return nomAlumno;
	}
	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getDscCurso() {
		return dscCurso;
	}
	public void setDscCurso(String dscCurso) {
		this.dscCurso = dscCurso;
	}
	public String getFlagCurso() {
		return flagCurso;
	}
	public void setFlagCurso(String flagCurso) {
		this.flagCurso = flagCurso;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getFlagSemestre() {
		return flagSemestre;
	}
	public void setFlagSemestre(String flagSemestre) {
		this.flagSemestre = flagSemestre;
	}
	
	
}
