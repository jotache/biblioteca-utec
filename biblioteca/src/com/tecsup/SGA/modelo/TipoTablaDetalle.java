package com.tecsup.SGA.modelo;

public class TipoTablaDetalle implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codTipoTabla;
	private String codTipoTablaDetalle;
	private String descripcion;
	private String dscValor1;
	private String dscValor2;
	private String dscValor3;
	private String dscValor4;
	private String dscValor5;
	private String dscValor6;
	private String dscValor7;
	private String dscValor8;
	private String dscValor9;
	private String dscValor10;
	private String usuario;
	private String estReg;
	private String codDetalle;
	private String tipDes;
	private String cadCod;
	private String nroReg;
	private String tipo;
	private String descripTipoSala;
	private String tipoLista;
	private String codTipoMat;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCadCod() {
		return cadCod;
	}
	public void setCadCod(String cadCod) {
		this.cadCod = cadCod;
	}
	public String getNroReg() {
		return nroReg;
	}
	public void setNroReg(String nroReg) {
		this.nroReg = nroReg;
	}
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	public String getCodTipoTabla() {
		return codTipoTabla;
	}
	public void setCodTipoTabla(String codTipoTabla) {
		this.codTipoTabla = codTipoTabla;
	}
	public String getCodTipoTablaDetalle() {
		return codTipoTablaDetalle;
	}
	public void setCodTipoTablaDetalle(String codTipoTablaDetalle) {
		this.codTipoTablaDetalle = codTipoTablaDetalle;
	}
	public String getDscValor1() {
		return dscValor1;
	}
	public void setDscValor1(String dscValor1) {
		this.dscValor1 = dscValor1;
	}
	public String getDscValor2() {
		return dscValor2;
	}
	public void setDscValor2(String dscValor2) {
		this.dscValor2 = dscValor2;
	}
	public String getDscValor3() {
		return dscValor3;
	}
	public void setDscValor3(String dscValor3) {
		this.dscValor3 = dscValor3;
	}
	public String getDscValor4() {
		return dscValor4;
	}
	public void setDscValor4(String dscValor4) {
		this.dscValor4 = dscValor4;
	}
	public String getDscValor5() {
		return dscValor5;
	}
	public void setDscValor5(String dscValor5) {
		this.dscValor5 = dscValor5;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDscValor6() {
		return dscValor6;
	}
	public void setDscValor6(String dscValor6) {
		this.dscValor6 = dscValor6;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getTipDes() {
		return tipDes;
	}
	public void setTipDes(String tipDes) {
		this.tipDes = tipDes;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDscValor7() {
		return dscValor7;
	}
	public void setDscValor7(String dscValor7) {
		this.dscValor7 = dscValor7;
	}
	public String getDscValor8() {
		return dscValor8;
	}
	public void setDscValor8(String dscValor8) {
		this.dscValor8 = dscValor8;
	}
	public String getDscValor9() {
		return dscValor9;
	}
	public void setDscValor9(String dscValor9) {
		this.dscValor9 = dscValor9;
	}
	public String getDscValor10() {
		return dscValor10;
	}
	public void setDscValor10(String dscValor10) {
		this.dscValor10 = dscValor10;
	}
	public String getTipoLista() {
		return tipoLista;
	}
	public void setTipoLista(String tipoLista) {
		this.tipoLista = tipoLista;
	}
	public String getDescripTipoSala() {
		return descripTipoSala;
	}
	public void setDescripTipoSala(String descripTipoSala) {
		this.descripTipoSala = descripTipoSala;
	}
	public String getCodTipoMat() {
		return codTipoMat;
	}
	public void setCodTipoMat(String codTipoMat) {
		this.codTipoMat = codTipoMat;
	}
	
}
