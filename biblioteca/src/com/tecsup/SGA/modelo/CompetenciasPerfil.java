package com.tecsup.SGA.modelo;

import java.util.List;
public class CompetenciasPerfil implements java.io.Serializable {
	private String codAlumno;
	private String nomAlumno;
	private String codCompetencia;
	private String dscCompetencia;
	private String notaIni;
	private String codEvalIni;
	private String notaFin;	
	private String codEvalFin;
	private String codTipoOperacion;
	private String flagTipoEval;
	private List<CompetenciasPerfil> listaCompetencias;
	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNomAlumno() {
		return nomAlumno;
	}
	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	public String getCodCompetencia() {
		return codCompetencia;
	}
	public void setCodCompetencia(String codCompetencia) {
		this.codCompetencia = codCompetencia;
	}
	public String getDscCompetencia() {
		return dscCompetencia;
	}
	public void setDscCompetencia(String dscCompetencia) {
		this.dscCompetencia = dscCompetencia;
	}
	public String getNotaIni() {
		return notaIni;
	}
	public void setNotaIni(String notaIni) {
		this.notaIni = notaIni;
	}
	public String getNotaFin() {
		return notaFin;
	}
	public void setNotaFin(String notaFin) {
		this.notaFin = notaFin;
	}
	public List getListaCompetencias() {
		return listaCompetencias;
	}
	public void setListaCompetencias(List listaCompetencias) {
		this.listaCompetencias = listaCompetencias;
	}
	public String getCodEvalIni() {
		return codEvalIni;
	}
	public void setCodEvalIni(String codEvalIni) {
		this.codEvalIni = codEvalIni;
	}
	public String getCodEvalFin() {
		return codEvalFin;
	}
	public void setCodEvalFin(String codEvalFin) {
		this.codEvalFin = codEvalFin;
	}
	public String getCodTipoOperacion() {
		return codTipoOperacion;
	}
	public void setCodTipoOperacion(String codTipoOperacion) {
		this.codTipoOperacion = codTipoOperacion;
	}
	public String getFlagTipoEval() {
		return flagTipoEval;
	}
	public void setFlagTipoEval(String flagTipoEval) {
		this.flagTipoEval = flagTipoEval;
	}
	
	
}
