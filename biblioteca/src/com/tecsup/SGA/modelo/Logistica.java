package com.tecsup.SGA.modelo;

public class Logistica implements java.io.Serializable{
	private String codSecuencial;
	private String codPadre;
	private String  codigo;
	private String  descripcion;
	private String dscPadre;
	private String valor1;
	private String valor2;
	private String valor3;
	public String getCodSecuencial() {
		return codSecuencial;
	}
	public void setCodSecuencial(String codSecuencial) {
		this.codSecuencial = codSecuencial;
	}
	public String getCodPadre() {
		return codPadre;
	}
	public void setCodPadre(String codPadre) {
		this.codPadre = codPadre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getValor1() {
		return valor1;
	}
	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}
	public String getValor2() {
		return valor2;
	}
	public void setValor2(String valor2) {
		this.valor2 = valor2;
	}
	public String getValor3() {
		return valor3;
	}
	public void setValor3(String valor3) {
		this.valor3 = valor3;
	}
	public String getDscPadre() {
		return dscPadre;
	}
	public void setDscPadre(String dscPadre) {
		this.dscPadre = dscPadre;
	}
	
	
}
