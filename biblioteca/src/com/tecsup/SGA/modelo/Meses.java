package com.tecsup.SGA.modelo;

public class Meses implements java.io.Serializable{
 
	public String idMeses;
	public String descripcionMeses;
	
	public String getIdMeses() {
		return idMeses;
	}
	public void setIdMeses(String idMeses) {
		this.idMeses = idMeses;
	}
	public String getDescripcionMeses() {
		return descripcionMeses;
	}
	public void setDescripcionMeses(String descripcionMeses) {
		this.descripcionMeses = descripcionMeses;
	}
}
