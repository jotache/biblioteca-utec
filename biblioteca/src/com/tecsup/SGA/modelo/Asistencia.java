package com.tecsup.SGA.modelo;

public class Asistencia implements java.io.Serializable{
	
	private String codAlumno;
	private String nombre;
	private String tipoAsistencia;
	private String flagSancion;
	
	private String fecha;
	private String nroSesion;
	private String codAsistencia;
	
	private String nroFaltas;
	private String porcentaje;

	/*04/2008 - Asistencia Programada*/
	private String flgProg;
	
	private String tipoSesion;
	private String codCurso;
	private String codSeccion;
	
	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoAsistencia() {
		return tipoAsistencia;
	}
	public void setTipoAsistencia(String tipoAsistencia) {
		this.tipoAsistencia = tipoAsistencia;
	}
	public String getFlagSancion() {
		return flagSancion;
	}
	public void setFlagSancion(String flagSancion) {
		this.flagSancion = flagSancion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNroSesion() {
		return nroSesion;
	}
	public void setNroSesion(String nroSesion) {
		this.nroSesion = nroSesion;
	}
	public String getCodAsistencia() {
		return codAsistencia;
	}
	public void setCodAsistencia(String codAsistencia) {
		this.codAsistencia = codAsistencia;
	}
	public String getNroFaltas() {
		return nroFaltas;
	}
	public void setNroFaltas(String nroFaltas) {
		this.nroFaltas = nroFaltas;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getFlgProg() {
		return flgProg;
	}
	public void setFlgProg(String flgProg) {
		this.flgProg = flgProg;
	}
	public String getTipoSesion() {
		return tipoSesion;
	}
	public void setTipoSesion(String tipoSesion) {
		this.tipoSesion = tipoSesion;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	
}
