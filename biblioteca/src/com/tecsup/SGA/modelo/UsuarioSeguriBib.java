//JHPR 2008-06-18
package com.tecsup.SGA.modelo;

import java.io.Serializable;

public class UsuarioSeguriBib implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codsujeto ;
	private String usuario;
	private String clave;
			
	public String getCodsujeto() {
		return codsujeto;
	}
	public void setCodsujeto(String codsujeto) {
		this.codsujeto = codsujeto;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	@Override
	public String toString() {
		return "UsuarioSeguriBib [codsujeto=" + codsujeto + ", usuario="
				+ usuario + ", clave=" + clave + "]";
	}
	
	
}
