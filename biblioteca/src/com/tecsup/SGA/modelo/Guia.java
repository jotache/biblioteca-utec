package com.tecsup.SGA.modelo;

public class Guia implements java.io.Serializable{
	/*
	 * AS COD_GUIA
	AS NRO_GUIA
	AS FEC_EMISION
	AS DSC_ESTADO
	AS COD_REQ_DET
    AS NRO_FACT
    AS FEC_EMI_FACT
    AS NRO_ORDEN
    AS OBSERVACION
	 */	
	private String codGuia;
	private String nroGuia;
	private String fechaEmision;
	private String codEstado;
	private String estado;
	private String codReqDetalle;
	private String nroFactura;
	private String fecEmiFactura;
	private String nroOrden;
	private String observacion;	
	
	private String solicitante;//jhpr 2008-09-26
	private String lugarTrabajo;
	
	//ALQD,13/11/08. NUEVAS PROPIEDADES
	private String nroRequerimiento;
	private String fecAprRequerimiento;
	private String nomCenCosto;
	private String fecCierre;
	public String getNroRequerimiento() {
		return nroRequerimiento;
	}
	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}
	public String getFecAprRequerimiento() {
		return fecAprRequerimiento;
	}
	public void setFecAprRequerimiento(String fecAprRequerimiento) {
		this.fecAprRequerimiento = fecAprRequerimiento;
	}
	public String getNomCenCosto() {
		return nomCenCosto;
	}
	public void setNomCenCosto(String nomCenCosto) {
		this.nomCenCosto = nomCenCosto;
	}
	public String getFecCierre() {
		return fecCierre;
	}
	public void setFecCierre(String fecCierre) {
		this.fecCierre = fecCierre;
	}
	/**
	 * @return the solicitante
	 */
	public String getSolicitante() {
		return solicitante;
	}
	/**
	 * @param solicitante the solicitante to set
	 */
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}
	/**
	 * @return the lugarTrabajo
	 */
	public String getLugarTrabajo() {
		return lugarTrabajo;
	}
	/**
	 * @param lugarTrabajo the lugarTrabajo to set
	 */
	public void setLugarTrabajo(String lugarTrabajo) {
		this.lugarTrabajo = lugarTrabajo;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getCodReqDetalle() {
		return codReqDetalle;
	}
	public void setCodReqDetalle(String codReqDetalle) {
		this.codReqDetalle = codReqDetalle;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getFecEmiFactura() {
		return fecEmiFactura;
	}
	public void setFecEmiFactura(String fecEmiFactura) {
		this.fecEmiFactura = fecEmiFactura;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getCodGuia() {
		return codGuia;
	}
	public void setCodGuia(String codGuia) {
		this.codGuia = codGuia;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
		
}
