package com.tecsup.SGA.modelo;

public class FormacionEmpresa implements java.io.Serializable{
	private String codAlumno;
	private String nombreAlumno;
	private String aPaterno;
	private String aMaterno;
	private String nombre;
	private String flagPracticaInicial;
	private String flagPasantia;
	private String flagPracticaPreProf;
	private String codPeriodo;
	private String codProducto;
	private String codEspecialidad;
	private String codCurso;
	private String tipoPractica;
	private String nombreEmpresa;
	private String fechaIni;
	private String fechaFin;
	private String cantidaHoras;
	private String codEstado;
	private String codGrabar;
	private String nombreNuevoArchivo;
	private String nombreArchivo;
	private String codId;
	private String observacion;
	private String nota;
	private String horaIni;
	private String horaFin;
	private String indSabado;
	private String indDomingo;
	private String ciclo;
	
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public String getHoraIni() {
		return horaIni;
	}
	public void setHoraIni(String horaIni) {
		this.horaIni = horaIni;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getIndSabado() {
		return indSabado;
	}
	public void setIndSabado(String indSabado) {
		this.indSabado = indSabado;
	}
	public String getIndDomingo() {
		return indDomingo;
	}
	public void setIndDomingo(String indDomingo) {
		this.indDomingo = indDomingo;
	}
	public String getNombreNuevoArchivo() {
		return nombreNuevoArchivo;
	}
	public void setNombreNuevoArchivo(String nombreNuevoArchivo) {
		this.nombreNuevoArchivo = nombreNuevoArchivo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getCodGrabar() {
		return codGrabar;
	}
	public void setCodGrabar(String codGrabar) {
		this.codGrabar = codGrabar;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public String getAPaterno() {
		return aPaterno;
	}
	public void setAPaterno(String paterno) {
		aPaterno = paterno;
	}
	public String getAMaterno() {
		return aMaterno;
	}
	public void setAMaterno(String materno) {
		aMaterno = materno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFlagPracticaInicial() {
		return flagPracticaInicial;
	}
	public void setFlagPracticaInicial(String flagPracticaInicial) {
		this.flagPracticaInicial = flagPracticaInicial;
	}
	public String getFlagPasantia() {
		return flagPasantia;
	}
	public void setFlagPasantia(String flagPasantia) {
		this.flagPasantia = flagPasantia;
	}
	public String getFlagPracticaPreProf() {
		return flagPracticaPreProf;
	}
	public void setFlagPracticaPreProf(String flagPracticaPreProf) {
		this.flagPracticaPreProf = flagPracticaPreProf;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodEspecialidad() {
		return codEspecialidad;
	}
	public void setCodEspecialidad(String codEspecialidad) {
		this.codEspecialidad = codEspecialidad;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getTipoPractica() {
		return tipoPractica;
	}
	public void setTipoPractica(String tipoPractica) {
		this.tipoPractica = tipoPractica;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getCantidaHoras() {
		return cantidaHoras;
	}
	public void setCantidaHoras(String cantidaHoras) {
		this.cantidaHoras = cantidaHoras;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	

}
