package com.tecsup.SGA.modelo;

public class SolRequerimientoAdjunto {
	private String codReq; 
	private String codDetReq;
	private String codAdjunto;
	private String codTipoAdjunto;
	private String dscTipoAdjunto;
	private String nombreAdjunto;
	private String rutaFisica;
	
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getCodDetReq() {
		return codDetReq;
	}
	public void setCodDetReq(String codDetReq) {
		this.codDetReq = codDetReq;
	}
	public String getCodAdjunto() {
		return codAdjunto;
	}
	public void setCodAdjunto(String codAdjunto) {
		this.codAdjunto = codAdjunto;
	}
	public String getCodTipoAdjunto() {
		return codTipoAdjunto;
	}
	public void setCodTipoAdjunto(String codTipoAdjunto) {
		this.codTipoAdjunto = codTipoAdjunto;
	}
	public String getDscTipoAdjunto() {
		return dscTipoAdjunto;
	}
	public void setDscTipoAdjunto(String dscTipoAdjunto) {
		this.dscTipoAdjunto = dscTipoAdjunto;
	}
	public String getNombreAdjunto() {
		return nombreAdjunto;
	}
	public void setNombreAdjunto(String nombreAdjunto) {
		this.nombreAdjunto = nombreAdjunto;
	}
	public String getRutaFisica() {
		return rutaFisica;
	}
	public void setRutaFisica(String rutaFisica) {
		this.rutaFisica = rutaFisica;
	}
	
	
}
