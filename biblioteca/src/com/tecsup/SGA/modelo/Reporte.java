package com.tecsup.SGA.modelo;


public class Reporte implements java.io.Serializable{

    private static final long serialVersionUID = 2639851516286703316L;

    private String fechaReg;

    /**********************/
    //Catalogo Fichas
    private String codigoMaterial;
    private String codigo;
    private String dscMaterial;
    private String tipoMaterial;
    private String tituloMaterial;
    private String nroIngresos;
    private String autor;
    private String descriptores;

    /**********************/
    //Material Faltante
    private String fecPrestamo;

    /**********************/
    //Material Adquirido
    private String nroVolumenes;
    private String procedencia;
    private String moneda;
    private String precio;
    private String total;

    /**********************/
    //Material Retirado
    private String fechaRetiro;
    private String añoPublicacion;

    /**********************/
    //Material Mas Solicitado

    private String nroPrestamosUtec;
    private String nroPrestamosTecsup;
    private String nroPrestamosTotal;

    private String dewey;
    private String totalEjemplares;
    private String nombreEditorial;

    /**********************/
    //Material Prest, Dev x Dia y Hora
    private String fechaPresDev;
    private String diaSem;
    private String hora;
    private String nroPrestSala;
    private String nroPrestCasa;
    private String totalPrestamos;
    private String devoluciones;
    private String totalMovimientos;
    private String fechaDevReal; //JHPR 2008-7-3 Fecha devolución programada

    /**********************/
    //Buzon de Sugerencias
    private String tipoSugerencia;
    private String detalle;
    private String calificacion;
    private String orientadoA;
    private String sugerencia;

    /**********************/
    //Usuarios Sancion
    private String estado;
    private String codUsuario;
    private String codTipoUsuario;
    private String usuario;
    private String nomUsuario;
    private String tipoUsuario;
    private String tipoResultSancion;
    private String fechaFinSancion; //JHPR 2008-08-27
    private String fechaFinSuspension; //ESBC 2009-12-31

    /**********************/
    //Estad Ciclo Esp
    private String nroUsuarios;
    private String ciclo;
    private String especialidad;

    private String codDeweyGenerico;
    private String nomDeweyGenerico;
    private String codDewey;
    private String nomDewey;
    private String nomMaterial;
    private String codTipoMaterial;
    private String nomTipoMaterial;
    private String idioma;
    //reporte mov dias
    private String diaLunes;
    private String diaMartes;
    private String diaMiercoles;
    private String diaJueves;
    private String diaViernes;
    private String diaSabado;
    private String diaDomingo;

    private String sancion;
    private String idSancion;


    //reporte de reserva de salas
    private String sala;
    private String fechaReserva;
    private String horaIniProg;
    private String horaFinProg;
    private String horaIniEjec;
    private String horaFinEjec;
    private String estadoReservaSala;
    private String codEstadoReserva;

    private String tipoVideo;
    
    
    /**
    * @return the codEstadoReserva
    */
    public String getCodEstadoReserva() {
        return codEstadoReserva;
    }
    /**
    * @param codEstadoReserva the codEstadoReserva to set
    */
    public void setCodEstadoReserva(String codEstadoReserva) {
        this.codEstadoReserva = codEstadoReserva;
    }
    /**
    * @return the estadoReservaSala
    */
    public String getEstadoReservaSala() {
        return estadoReservaSala;
    }
    /**
    * @param estadoReservaSala the estadoReservaSala to set
    */
    public void setEstadoReservaSala(String estadoReservaSala) {
        this.estadoReservaSala = estadoReservaSala;
    }
    /**
    * @return the sala
    */
    public String getSala() {
        return sala;
    }
    /**
    * @param sala the sala to set
    */
    public void setSala(String sala) {
        this.sala = sala;
    }
    /**
    * @return the fechaReserva
    */
    public String getFechaReserva() {
        return fechaReserva;
    }
    /**
    * @param fechaReserva the fechaReserva to set
    */
    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }
    /**
    * @return the horaIniProg
    */
    public String getHoraIniProg() {
        return horaIniProg;
    }
    /**
    * @param horaIniProg the horaIniProg to set
    */
    public void setHoraIniProg(String horaIniProg) {
        this.horaIniProg = horaIniProg;
    }
    /**
    * @return the horaFinProg
    */
    public String getHoraFinProg() {
        return horaFinProg;
    }
    /**
    * @param horaFinProg the horaFinProg to set
    */
    public void setHoraFinProg(String horaFinProg) {
        this.horaFinProg = horaFinProg;
    }
    /**
    * @return the horaIniEjec
    */
    public String getHoraIniEjec() {
        return horaIniEjec;
    }
    /**
    * @param horaIniEjec the horaIniEjec to set
    */
    public void setHoraIniEjec(String horaIniEjec) {
        this.horaIniEjec = horaIniEjec;
    }
    /**
    * @return the horaFinEjec
    */
    public String getHoraFinEjec() {
        return horaFinEjec;
    }
    /**
    * @param horaFinEjec the horaFinEjec to set
    */
    public void setHoraFinEjec(String horaFinEjec) {
        this.horaFinEjec = horaFinEjec;
    }


    /**
    * @return the sancion
    */
    public String getSancion() {
        return sancion;
    }
    /**
    * @param sancion the sancion to set
    */
    public void setSancion(String sancion) {
        this.sancion = sancion;
    }
    /**
    * @return the idSancion
    */
    public String getIdSancion() {
        return idSancion;
    }
    /**
    * @param idSancion the idSancion to set
    */
    public void setIdSancion(String idSancion) {
        this.idSancion = idSancion;
    }
    public String getDiaLunes() {
        return diaLunes;
    }
    public void setDiaLunes(String diaLunes) {
        this.diaLunes = diaLunes;
    }
    public String getDiaMartes() {
        return diaMartes;
    }
    public void setDiaMartes(String diaMartes) {
        this.diaMartes = diaMartes;
    }
    public String getDiaMiercoles() {
        return diaMiercoles;
    }
    public void setDiaMiercoles(String diaMiercoles) {
        this.diaMiercoles = diaMiercoles;
    }
    public String getDiaJueves() {
        return diaJueves;
    }
    public void setDiaJueves(String diaJueves) {
        this.diaJueves = diaJueves;
    }
    public String getDiaViernes() {
        return diaViernes;
    }
    public void setDiaViernes(String diaViernes) {
        this.diaViernes = diaViernes;
    }
    public String getDiaSabado() {
        return diaSabado;
    }
    public void setDiaSabado(String diaSabado) {
        this.diaSabado = diaSabado;
    }
    public String getDiaDomingo() {
        return diaDomingo;
    }
    public void setDiaDomingo(String diaDomingo) {
        this.diaDomingo = diaDomingo;
    }
    public String getFechaReg() {
        return fechaReg;
    }
    public void setFechaReg(String fechaReg) {
        this.fechaReg = fechaReg;
    }
    public String getTipoSugerencia() {
        return tipoSugerencia;
    }
    public void setTipoSugerencia(String tipoSugerencia) {
        this.tipoSugerencia = tipoSugerencia;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    public String getCalificacion() {
        return calificacion;
    }
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }
    public String getOrientadoA() {
        return orientadoA;
    }
    public void setOrientadoA(String orientadoA) {
        this.orientadoA = orientadoA;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    public String getCodigoMaterial() {
        return codigoMaterial;
    }
    public void setCodigoMaterial(String codigoMaterial) {
        this.codigoMaterial = codigoMaterial;
    }
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getTipoMaterial() {
        return tipoMaterial;
    }
    public void setTipoMaterial(String tipoMaterial) {
        this.tipoMaterial = tipoMaterial;
    }
    public String getTituloMaterial() {
        return tituloMaterial;
    }
    public void setTituloMaterial(String tituloMaterial) {
        this.tituloMaterial = tituloMaterial;
    }
    public String getNroIngresos() {
        return nroIngresos;
    }
    public void setNroIngresos(String nroIngresos) {
        this.nroIngresos = nroIngresos;
    }
    public String getAutor() {
        return autor;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }
    public String getFecPrestamo() {
        return fecPrestamo;
    }
    public void setFecPrestamo(String fecPrestamo) {
        this.fecPrestamo = fecPrestamo;
    }
    public String getTipoUsuario() {
        return tipoUsuario;
    }
    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    public String getNroVolumenes() {
        return nroVolumenes;
    }
    public void setNroVolumenes(String nroVolumenes) {
        this.nroVolumenes = nroVolumenes;
    }
    public String getProcedencia() {
        return procedencia;
    }
    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }
    public String getMoneda() {
        return moneda;
    }
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
    public String getPrecio() {
        return precio;
    }
    public void setPrecio(String precio) {
        this.precio = precio;
    }
    public String getTotal() {
        return total;
    }
    public void setTotal(String total) {
        this.total = total;
    }
    public String getFechaRetiro() {
        return fechaRetiro;
    }
    public void setFechaRetiro(String fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }
    public String getNroPrestamosTecsup() {
        return nroPrestamosTecsup;
    }
    public void setNroPrestamosTecsup(String nroPrestamosTecsup) {
        this.nroPrestamosTecsup = nroPrestamosTecsup;
    }
    public String getNroPrestamosUtec() {
        return nroPrestamosUtec;
    }
    public void setNroPrestamosUtec(String nroPrestamosUtec) {
        this.nroPrestamosUtec = nroPrestamosUtec;
    }
    public String getNroPrestamosTotal() {
        return nroPrestamosTotal;
    }
    public void setNroPrestamosTotal(String nroPrestamosTotal) {
        this.nroPrestamosTotal = nroPrestamosTotal;
    }
    public String getDewey() {
        return dewey;
    }
    public void setDewey(String dewey) {
        this.dewey = dewey;
    }
    public String getFechaPresDev() {
        return fechaPresDev;
    }
    public void setFechaPresDev(String fechaPresDev) {
        this.fechaPresDev = fechaPresDev;
    }
    public String getDiaSem() {
        return diaSem;
    }
    public void setDiaSem(String diaSem) {
        this.diaSem = diaSem;
    }
    public String getHora() {
        return hora;
    }
    public void setHora(String hora) {
        this.hora = hora;
    }
    public String getNroPrestSala() {
        return nroPrestSala;
    }
    public void setNroPrestSala(String nroPrestSala) {
        this.nroPrestSala = nroPrestSala;
    }
    public String getNroPrestCasa() {
        return nroPrestCasa;
    }
    public void setNroPrestCasa(String nroPrestCasa) {
        this.nroPrestCasa = nroPrestCasa;
    }
    public String getTotalPrestamos() {
        return totalPrestamos;
    }
    public void setTotalPrestamos(String totalPrestamos) {
        this.totalPrestamos = totalPrestamos;
    }
    public String getDevoluciones() {
        return devoluciones;
    }
    public void setDevoluciones(String devoluciones) {
        this.devoluciones = devoluciones;
    }
    public String getTotalMovimientos() {
        return totalMovimientos;
    }
    public void setTotalMovimientos(String totalMovimientos) {
        this.totalMovimientos = totalMovimientos;
    }
    public String getTipoResultSancion() {
        return tipoResultSancion;
    }
    public void setTipoResultSancion(String tipoResultSancion) {
        this.tipoResultSancion = tipoResultSancion;
    }
    public String getNroUsuarios() {
        return nroUsuarios;
    }
    public void setNroUsuarios(String nroUsuarios) {
        this.nroUsuarios = nroUsuarios;
    }
    public String getCiclo() {
        return ciclo;
    }
    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }
    public String getEspecialidad() {
        return especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    public String getDscMaterial() {
        return dscMaterial;
    }
    public void setDscMaterial(String dscMaterial) {
        this.dscMaterial = dscMaterial;
    }
    public String getCodUsuario() {
        return codUsuario;
    }
    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }
    public String getCodTipoUsuario() {
        return codTipoUsuario;
    }
    public void setCodTipoUsuario(String codTipoUsuario) {
        this.codTipoUsuario = codTipoUsuario;
    }
    public String getNomUsuario() {
        return nomUsuario;
    }
    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }
    public String getCodDeweyGenerico() {
        return codDeweyGenerico;
    }
    public void setCodDeweyGenerico(String codDeweyGenerico) {
        this.codDeweyGenerico = codDeweyGenerico;
    }
    public String getNomDeweyGenerico() {
        return nomDeweyGenerico;
    }
    public void setNomDeweyGenerico(String nomDeweyGenerico) {
        this.nomDeweyGenerico = nomDeweyGenerico;
    }
    public String getCodDewey() {
        return codDewey;
    }
    public void setCodDewey(String codDewey) {
        this.codDewey = codDewey;
    }
    public String getNomDewey() {
        return nomDewey;
    }
    public void setNomDewey(String nomDewey) {
        this.nomDewey = nomDewey;
    }
    public String getNomMaterial() {
        return nomMaterial;
    }
    public void setNomMaterial(String nomMaterial) {
        this.nomMaterial = nomMaterial;
    }
    public String getCodTipoMaterial() {
        return codTipoMaterial;
    }
    public void setCodTipoMaterial(String codTipoMaterial) {
        this.codTipoMaterial = codTipoMaterial;
    }
    public String getNomTipoMaterial() {
        return nomTipoMaterial;
    }
    public void setNomTipoMaterial(String nomTipoMaterial) {
        this.nomTipoMaterial = nomTipoMaterial;
    }
    public String getIdioma() {
        return idioma;
    }
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
    public String getDescriptores() {
        return descriptores;
    }
    public void setDescriptores(String descriptores) {
        this.descriptores = descriptores;
    }
    public String getTotalEjemplares() {
        return totalEjemplares;
    }
    public void setTotalEjemplares(String totalEjemplares) {
        this.totalEjemplares = totalEjemplares;
    }
    public String getAñoPublicacion() {
        return añoPublicacion;
    }
    public void setAñoPublicacion(String añoPublicacion) {
        this.añoPublicacion = añoPublicacion;
    }
    public String getNombreEditorial() {
        return nombreEditorial;
    }
    public void setNombreEditorial(String nombreEditorial) {
        this.nombreEditorial = nombreEditorial;
    }
    public String getFechaDevReal() {
        return fechaDevReal;
    }
    public void setFechaDevReal(String fechaDevReal) {
        this.fechaDevReal = fechaDevReal;
    }
    public String getFechaFinSancion() {
        return fechaFinSancion;
    }
    public void setFechaFinSancion(String fechaFinSancion) {
        this.fechaFinSancion = fechaFinSancion;
    }
    public String getSugerencia() {
        return sugerencia;
    }
    public void setSugerencia(String sugerencia) {
        this.sugerencia = sugerencia;
    }
    public String getFechaFinSuspension() {
        return fechaFinSuspension;
    }
    public void setFechaFinSuspension(String fechaFinSuspension) {
        this.fechaFinSuspension = fechaFinSuspension;
    }
	public String getTipoVideo() {
		return tipoVideo;
	}
	public void setTipoVideo(String tipoVideo) {
		this.tipoVideo = tipoVideo;
	}



}
