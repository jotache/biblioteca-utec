package com.tecsup.SGA.modelo;

public class Proveedor implements java.io.Serializable{
    
    private String codUnico;
	private String codTipoCalificacion;
	private String descCalificacion;
	private String codTipoProveedor;
	private String tipoDoc;
	private String nroDco;
	private String razSoc;
	private String razSocCorto;
	private String correo;
	private String direc;
	private String dpto;
	private String prov;
	private String dtrito;
	private String atencionA;
	private String codTipGiro;
	private String descGiro;
	private String codEstado;
	private String nomBanco;
	private String tipCta;
	private String nroCta;
	private String codTipoMoneda;
	//ALQD,02/06/09.NUEVA PROPIEDAD
	private String otroCostos;
	//ALQD,03/07/09.NUEVAS PROPIEDADES
	private String pais;
	private String telefono;
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getOtroCostos(){
		return otroCostos;
	}
	public void setOtroCostos(String otroCostos){
		this.otroCostos = otroCostos;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getCodTipoCalificacion() {
		return codTipoCalificacion;
	}
	public void setCodTipoCalificacion(String codTipoCalificacion) {
		this.codTipoCalificacion = codTipoCalificacion;
	}
	public String getDescCalificacion() {
		return descCalificacion;
	}
	public void setDescCalificacion(String descCalificacion) {
		this.descCalificacion = descCalificacion;
	}
	public String getCodTipoProveedor() {
		return codTipoProveedor;
	}
	public void setCodTipoProveedor(String codTipoProveedor) {
		this.codTipoProveedor = codTipoProveedor;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getNroDco() {
		return nroDco;
	}
	public void setNroDco(String nroDco) {
		this.nroDco = nroDco;
	}
	public String getRazSoc() {
		return razSoc;
	}
	public void setRazSoc(String razSoc) {
		this.razSoc = razSoc;
	}
	public String getRazSocCorto() {
		return razSocCorto;
	}
	public void setRazSocCorto(String razSocCorto) {
		this.razSocCorto = razSocCorto;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getDirec() {
		return direc;
	}
	public void setDirec(String direc) {
		this.direc = direc;
	}
	public String getDpto() {
		return dpto;
	}
	public void setDpto(String dpto) {
		this.dpto = dpto;
	}
	public String getProv() {
		return prov;
	}
	public void setProv(String prov) {
		this.prov = prov;
	}
	public String getDtrito() {
		return dtrito;
	}
	public void setDtrito(String dtrito) {
		this.dtrito = dtrito;
	}
	public String getAtencionA() {
		return atencionA;
	}
	public void setAtencionA(String atencionA) {
		this.atencionA = atencionA;
	}
	public String getCodTipGiro() {
		return codTipGiro;
	}
	public void setCodTipGiro(String codTipGiro) {
		this.codTipGiro = codTipGiro;
	}
	public String getDescGiro() {
		return descGiro;
	}
	public void setDescGiro(String descGiro) {
		this.descGiro = descGiro;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getNomBanco() {
		return nomBanco;
	}
	public void setNomBanco(String nomBanco) {
		this.nomBanco = nomBanco;
	}
	public String getTipCta() {
		return tipCta;
	}
	public void setTipCta(String tipCta) {
		this.tipCta = tipCta;
	}
	public String getNroCta() {
		return nroCta;
	}
	public void setNroCta(String nroCta) {
		this.nroCta = nroCta;
	}
	public String getCodTipoMoneda() {
		return codTipoMoneda;
	}
	public void setCodTipoMoneda(String codTipoMoneda) {
		this.codTipoMoneda = codTipoMoneda;
	}
	
	
	
}
