package com.tecsup.SGA.modelo;

public class InterfazContableResultado {
	/*
	 *  SELECT '09' AS CD
      , '1' AS TD
      , '1' AS NUMERO
      , TO_CHAR(INTER_DET.DDATE,'DD/MM/YYYY') AS FECHA
      , TRIM(TD_GLOSA_DET.TTDE_VALOR1) AS DESCRIPCION
      , INTER_DET.DCUENTA AS CODIGO_CUENTA
      , INTER_DET.DCENCOS AS CONTABLE_CECO
      , SUM(CASE WHEN INTER_DET.DDH = 'D' THEN INTER_DET.DIMPORT ELSE 0 END) AS DEBE
      , SUM(CASE WHEN INTER_DET.DDH = 'H' THEN INTER_DET.DIMPORT ELSE 0 END) AS HABER
	 */
	private String cd;
	private String td;
	private String numero;
	private String fecha;
	private String descripcion;
	private String codigoCuenta;
	private String contableCeco;
	private String debe;
	private String haber;
	private String sumDebe;
	private String sumHaber;

	public String getSumDebe() {
		return sumDebe;
	}
	public void setSumDebe(String sumDebe) {
		this.sumDebe = sumDebe;
	}
	public String getSumHaber() {
		return sumHaber;
	}
	public void setSumHaber(String sumHaber) {
		this.sumHaber = sumHaber;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getTd() {
		return td;
	}
	public void setTd(String td) {
		this.td = td;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigoCuenta() {
		return codigoCuenta;
	}
	public void setCodigoCuenta(String codigoCuenta) {
		this.codigoCuenta = codigoCuenta;
	}
	public String getContableCeco() {
		return contableCeco;
	}
	public void setContableCeco(String contableCeco) {
		this.contableCeco = contableCeco;
	}
	public String getDebe() {
		return debe;
	}
	public void setDebe(String debe) {
		this.debe = debe;
	}
	public String getHaber() {
		return haber;
	}
	public void setHaber(String haber) {
		this.haber = haber;
	}
	
	
	
}
