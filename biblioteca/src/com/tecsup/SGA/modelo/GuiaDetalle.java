package com.tecsup.SGA.modelo;

public class GuiaDetalle implements java.io.Serializable{
	/*
	 * 	AS COD_REQ
			AS COD_REQ_DET
			AS COD_GUIA_DET
			AS DSC_BIEN
			AS CANT_SOL
			AS PRECIO_UNITARIO
			AS CANTIDAD_ENT
			AS IND_ACT --En caso sea 1 se muestra el icono de agregar.
	 */	
	private String codGuia;
	private String codGuiaDetalle;
	private String nroGuia;
	private String codRequerimiento;
	private String codReqDetalle;
	private String codEstado;
	private String dscEstado;
	private String dscGuiaDetalle;
	private String codDocPago;
	private String nroDocPago;
	private String fechaDocPago;
	private String codOrden;
	private String fechaOrden;
	private String nroOrden;
	private String codBien;
	private String dscBien;
	private String cantidadSolicitada;
	private String precioUnitario;
	private String cantidad;
	private String indicador;
	//cadenas paar ingresar el detalle de la guia
	private String fechaGuia;
	private String observacion;
	private String cadenaCodDetalle;
	private String cadenaCantidadSolicitada;
	private String cadenaPrecios;
	private String cadenaCantidadEntregada;	
	private String tot;
	private String cantidadPendiente;
	private String cantidadDisponible;
	
	private String cantidadSeleccionada;
	private String unidadMedida; //jhpr 2008-09-25
	
	/**
	 * @return the unidadMedida
	 */
	public String getUnidadMedida() {
		return unidadMedida;
	}
	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getCantidadDisponible() {
		return cantidadDisponible;
	}
	public void setCantidadDisponible(String cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}
	public String getCantidadPendiente() {
		return cantidadPendiente;
	}
	public void setCantidadPendiente(String cantidadPendiente) {
		this.cantidadPendiente = cantidadPendiente;
	}
	public String getTot() {
		return tot;
	}
	public void setTot(String tot) {
		this.tot = tot;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getDscGuiaDetalle() {
		return dscGuiaDetalle;
	}
	public void setDscGuiaDetalle(String dscGuiaDetalle) {
		this.dscGuiaDetalle = dscGuiaDetalle;
	}
	public String getCodDocPago() {
		return codDocPago;
	}
	public void setCodDocPago(String codDocPago) {
		this.codDocPago = codDocPago;
	}
	public String getNroDocPago() {
		return nroDocPago;
	}
	public void setNroDocPago(String nroDocPago) {
		this.nroDocPago = nroDocPago;
	}
	public String getFechaDocPago() {
		return fechaDocPago;
	}
	public void setFechaDocPago(String fechaDocPago) {
		this.fechaDocPago = fechaDocPago;
	}
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getFechaOrden() {
		return fechaOrden;
	}
	public void setFechaOrden(String fechaOrden) {
		this.fechaOrden = fechaOrden;
	}
	public String getCodGuia() {
		return codGuia;
	}
	public void setCodGuia(String codGuia) {
		this.codGuia = codGuia;
	}
	public String getCadenaCodDetalle() {
		return cadenaCodDetalle;
	}
	public void setCadenaCodDetalle(String cadenaCodDetalle) {
		this.cadenaCodDetalle = cadenaCodDetalle;
	}
	public String getCadenaCantidadSolicitada() {
		return cadenaCantidadSolicitada;
	}
	public void setCadenaCantidadSolicitada(String cadenaCantidadSolicitada) {
		this.cadenaCantidadSolicitada = cadenaCantidadSolicitada;
	}
	public String getCadenaPrecios() {
		return cadenaPrecios;
	}
	public void setCadenaPrecios(String cadenaPrecios) {
		this.cadenaPrecios = cadenaPrecios;
	}
	public String getCadenaCantidadEntregada() {
		return cadenaCantidadEntregada;
	}
	public void setCadenaCantidadEntregada(String cadenaCantidadEntregada) {
		this.cadenaCantidadEntregada = cadenaCantidadEntregada;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getCodReqDetalle() {
		return codReqDetalle;
	}
	public void setCodReqDetalle(String codReqDetalle) {
		this.codReqDetalle = codReqDetalle;
	}
	public String getCodGuiaDetalle() {
		return codGuiaDetalle;
	}
	public void setCodGuiaDetalle(String codGuiaDetalle) {
		this.codGuiaDetalle = codGuiaDetalle;
	}
	public String getDscBien() {
		return dscBien;
	}
	public void setDscBien(String dscBien) {
		this.dscBien = dscBien;
	}
	public String getCantidadSolicitada() {
		return cantidadSolicitada;
	}
	public void setCantidadSolicitada(String cantidadSolicitada) {
		this.cantidadSolicitada = cantidadSolicitada;
	}
	public String getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getIndicador() {
		return indicador;
	}
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	public String getFechaGuia() {
		return fechaGuia;
	}
	public void setFechaGuia(String fechaGuia) {
		this.fechaGuia = fechaGuia;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getCantidadSeleccionada() {
		return cantidadSeleccionada;
	}
	public void setCantidadSeleccionada(String cantidadSeleccionada) {
		this.cantidadSeleccionada = cantidadSeleccionada;
	}
	
		
}
