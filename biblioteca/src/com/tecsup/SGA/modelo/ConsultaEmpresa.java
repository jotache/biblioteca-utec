package com.tecsup.SGA.modelo;

public class ConsultaEmpresa {
	private String codId;
	private String nombreEmpresa;
	private String fechaInicio;
	private String fechaFin;
	private String cantHoras;
	private String flag;
	private String observacion;
	private String nota;
	private String horarioIni;
	private String horariofin;
	private String indSabado;
	private String indDomingo;

	
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public String getHorarioIni() {
		return horarioIni;
	}
	public void setHorarioIni(String horarioIni) {
		this.horarioIni = horarioIni;
	}
	public String getHorariofin() {
		return horariofin;
	}
	public void setHorariofin(String horariofin) {
		this.horariofin = horariofin;
	}
	public String getIndSabado() {
		return indSabado;
	}
	public void setIndSabado(String indSabado) {
		this.indSabado = indSabado;
	}
	public String getIndDomingo() {
		return indDomingo;
	}
	public void setIndDomingo(String indDomingo) {
		this.indDomingo = indDomingo;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getCantHoras() {
		return cantHoras;
	}
	public void setCantHoras(String cantHoras) {
		this.cantHoras = cantHoras;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
}
