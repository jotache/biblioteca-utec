package com.tecsup.SGA.modelo;

public class CotizacionDetalle {
	/*
	 *AS CODIGO_COT
     ,AS CODIGO_DETALLE
     ,AS CODIGO_BIEN
     ,AS NRO_REQUERIMIENTO
     ,AS USU_SOLICITANTE
     ,AS VALOR1
     ,AS VALOR2
     ,AS VALOR3
     ,AS FECHA_ENTREGA
     ,AS NRO_PROVEEDORES
     ,AS DESCRIPCION
	 */	
	private String codigo;
	private String codDetalle;
	private String codBien;
	private String nroRequerimiento;
	private String usuSolicitante;
	private String producto;
	private String unidadMedida;
	private String cantidad;
	private String fechaEntrega;
	private String nroProveedores;
	private String descripcion;
	private String descripcionCot;
	private String descripcionPro;
	private String dscUnidad;
	private String codMoneda;
	private String moneda;
	private String precioUnitario;
	private String codProveedor;
	private String codTipoCondicion;
	private String detalleCotizacion;
	private String detalleProveedor;
	
	private String cadenaCodCotDetalle;
	private String cadenaPrecios;
	private String cadenaDescripcion;
	//ALQD,02/06/09.NUEVA PROPIEDAD
	private String otroCostos;
	//ALQD,16/06/09.AFECTO_IGV
	private String afectoIGV;
	
	public String getAfectoIGV() {
		return afectoIGV;
	}
	public void setAfectoIGV(String afectoIGV) {
		this.afectoIGV=afectoIGV;
	}
	public String getOtroCostos(){
		return otroCostos;
	}
	public void setOtroCostos(String otroCostos){
		this.otroCostos=otroCostos;
	}
	public String getCadenaCodCotDetalle() {
		return cadenaCodCotDetalle;
	}
	public void setCadenaCodCotDetalle(String cadenaCodCotDetalle) {
		this.cadenaCodCotDetalle = cadenaCodCotDetalle;
	}
	public String getCadenaPrecios() {
		return cadenaPrecios;
	}
	public void setCadenaPrecios(String cadenaPrecios) {
		this.cadenaPrecios = cadenaPrecios;
	}
	public String getCadenaDescripcion() {
		return cadenaDescripcion;
	}
	public void setCadenaDescripcion(String cadenaDescripcion) {
		this.cadenaDescripcion = cadenaDescripcion;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public String getCodTipoCondicion() {
		return codTipoCondicion;
	}
	public void setCodTipoCondicion(String codTipoCondicion) {
		this.codTipoCondicion = codTipoCondicion;
	}
	public String getDetalleCotizacion() {
		return detalleCotizacion;
	}
	public void setDetalleCotizacion(String detalleCotizacion) {
		this.detalleCotizacion = detalleCotizacion;
	}
	public String getDetalleProveedor() {
		return detalleProveedor;
	}
	public void setDetalleProveedor(String detalleProveedor) {
		this.detalleProveedor = detalleProveedor;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public String getDescripcionCot() {
		return descripcionCot;
	}
	public void setDescripcionCot(String descripcionCot) {
		this.descripcionCot = descripcionCot;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getNroRequerimiento() {
		return nroRequerimiento;
	}
	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}	
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getNroProveedores() {
		return nroProveedores;
	}
	public void setNroProveedores(String nroProveedores) {
		this.nroProveedores = nroProveedores;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getDescripcionPro() {
		return descripcionPro;
	}
	public void setDescripcionPro(String descripcionPro) {
		this.descripcionPro = descripcionPro;
	}
	public String getCodMoneda() {
		return codMoneda;
	}
	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}
	public String getDscUnidad() {
		return dscUnidad;
	}
	public void setDscUnidad(String dscUnidad) {
		this.dscUnidad = dscUnidad;
	}
	
}
