package com.tecsup.SGA.modelo;

public class MaterialAdjunto {
private String codUnico;
private String titulo;
private String archivo;

public String getCodUnico() {
	return codUnico;
}
public void setCodUnico(String codUnico) {
	this.codUnico = codUnico;
}
public String getTitulo() {
	return titulo;
}
public void setTitulo(String titulo) {
	this.titulo = titulo;
}
public String getArchivo() {
	return archivo;
}
public void setArchivo(String archivo) {
	this.archivo = archivo;
}
}
