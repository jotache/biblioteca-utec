package com.tecsup.SGA.modelo;

public class OrdenesAprobadas {

	private String codOrden;
	private String nroOrden;
	private String fechaEmision;
	private String dscProveedor;
	private String dscMoneda;
	private String monto;
	private String montoFacturado;
	private String montoSaldo;
	private String dscComprador;
	private String codTipoOrden;
	private String codEstado;
	private String dscEstado;
	
	private String codProveedor;
	private String dscRazonSocial;
	private String montoSaldoVal;
	private String codDocumento;
	private String nroDocumento;
	private String codMoneda;
	private String dscDetalleEstado;
	private String fechaVencimiento;
	private String dscNroGuia;
	private String indDocPago;
	private String emailProveedor;
	private String DscTipoOrden;
	
	private String indImportacion;
	private String importeOtros;
	//ALQD,27/05/09.NUEVA PROPIEDAD
	private String indAfectoIGV;
	
	public String getIndAfectoIGV() {
		return indAfectoIGV;
	}
	public void setIndAfectoIGV(String indAfectoIGV) {
		this.indAfectoIGV=indAfectoIGV;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public String getDscRazonSocial() {
		return dscRazonSocial;
	}
	public void setDscRazonSocial(String dscRazonSocial) {
		this.dscRazonSocial = dscRazonSocial;
	}
	public String getMontoSaldoVal() {
		return montoSaldoVal;
	}
	public void setMontoSaldoVal(String montoSaldoVal) {
		this.montoSaldoVal = montoSaldoVal;
	}
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getDscProveedor() {
		return dscProveedor;
	}
	public void setDscProveedor(String dscProveedor) {
		this.dscProveedor = dscProveedor;
	}
	public String getDscMoneda() {
		return dscMoneda;
	}
	public void setDscMoneda(String dscMoneda) {
		this.dscMoneda = dscMoneda;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getMontoFacturado() {
		return montoFacturado;
	}
	public void setMontoFacturado(String montoFacturado) {
		this.montoFacturado = montoFacturado;
	}
	public String getMontoSaldo() {
		return montoSaldo;
	}
	public void setMontoSaldo(String montoSaldo) {
		this.montoSaldo = montoSaldo;
	}
	public String getDscComprador() {
		return dscComprador;
	}
	public void setDscComprador(String dscComprador) {
		this.dscComprador = dscComprador;
	}
	public String getCodTipoOrden() {
		return codTipoOrden;
	}
	public void setCodTipoOrden(String codTipoOrden) {
		this.codTipoOrden = codTipoOrden;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getCodDocumento() {
		return codDocumento;
	}
	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}
	public String getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getCodMoneda() {
		return codMoneda;
	}
	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}
	public String getDscDetalleEstado() {
		return dscDetalleEstado;
	}
	public void setDscDetalleEstado(String dscDetalleEstado) {
		this.dscDetalleEstado = dscDetalleEstado;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getDscNroGuia() {
		return dscNroGuia;
	}
	public void setDscNroGuia(String dscNroGuia) {
		this.dscNroGuia = dscNroGuia;
	}
	public String getIndDocPago() {
		return indDocPago;
	}
	public void setIndDocPago(String indDocPago) {
		this.indDocPago = indDocPago;
	}
	public String getEmailProveedor() {
		return emailProveedor;
	}
	public void setEmailProveedor(String emailProveedor) {
		this.emailProveedor = emailProveedor;
	}
	public String getDscTipoOrden() {
		return DscTipoOrden;
	}
	public void setDscTipoOrden(String dscTipoOrden) {
		DscTipoOrden = dscTipoOrden;
	}
	public String getIndImportacion() {
		return indImportacion;
	}
	public void setIndImportacion(String indImportacion) {
		this.indImportacion = indImportacion;
	}
	public String getImporteOtros() {
		return importeOtros;
	}
	public void setImporteOtros(String importeOtros) {
		this.importeOtros = importeOtros;
	}
	

}
