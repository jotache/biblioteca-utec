package com.tecsup.SGA.modelo;

import java.io.Serializable;

public class ProcesoAreasNivelEstudios implements Serializable {

	
	private static final long serialVersionUID = 4977738661876221017L;

	public ProcesoAreasNivelEstudios(){}
	
	private String id;
	private String codNivelEstudio;
	private String descripcionNivelEstudio;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodNivelEstudio() {
		return codNivelEstudio;
	}
	public void setCodNivelEstudio(String codNivelEstudio) {
		this.codNivelEstudio = codNivelEstudio;
	}
	public String getDescripcionNivelEstudio() {
		return descripcionNivelEstudio;
	}
	public void setDescripcionNivelEstudio(String descripcionNivelEstudio) {
		this.descripcionNivelEstudio = descripcionNivelEstudio;
	}
	
	
}
