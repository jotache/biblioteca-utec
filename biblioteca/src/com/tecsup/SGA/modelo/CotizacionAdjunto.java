package com.tecsup.SGA.modelo;

public class CotizacionAdjunto {
	private String codCotizacion;
	private String codCotizacionDet;
	private String codAdjunto;
	private String codTipoAdjunto;
	private String dscTipoAdjunto;
	private String nomAdjunto;
	private String rutaAdjunto;
	private String codTipoUsuario;
	private String dscTipoUsuario;
	private String codProveedor;
	
	
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCodCotizacionDet() {
		return codCotizacionDet;
	}
	public void setCodCotizacionDet(String codCotizacionDet) {
		this.codCotizacionDet = codCotizacionDet;
	}
	public String getCodAdjunto() {
		return codAdjunto;
	}
	public void setCodAdjunto(String codAdjunto) {
		this.codAdjunto = codAdjunto;
	}
	public String getCodTipoAdjunto() {
		return codTipoAdjunto;
	}
	public void setCodTipoAdjunto(String codTipoAdjunto) {
		this.codTipoAdjunto = codTipoAdjunto;
	}
	public String getDscTipoAdjunto() {
		return dscTipoAdjunto;
	}
	public void setDscTipoAdjunto(String dscTipoAdjunto) {
		this.dscTipoAdjunto = dscTipoAdjunto;
	}
	public String getNomAdjunto() {
		return nomAdjunto;
	}
	public void setNomAdjunto(String nomAdjunto) {
		this.nomAdjunto = nomAdjunto;
	}
	public String getRutaAdjunto() {
		return rutaAdjunto;
	}
	public void setRutaAdjunto(String rutaAdjunto) {
		this.rutaAdjunto = rutaAdjunto;
	}
	public String getCodTipoUsuario() {
		return codTipoUsuario;
	}
	public void setCodTipoUsuario(String codTipoUsuario) {
		this.codTipoUsuario = codTipoUsuario;
	}
	public String getDscTipoUsuario() {
		return dscTipoUsuario;
	}
	public void setDscTipoUsuario(String dscTipoUsuario) {
		this.dscTipoUsuario = dscTipoUsuario;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	
	
}
