package com.tecsup.SGA.modelo;

public class GuiaRemision {
	
	private String codCtaPago;
	private String nroGuia;
	private String fecEmisionGuia;
	private String nroCtaPago;
	private String fecEmisionCta;
	private String dscMontoCta;
	private String codCtaPagina;
	private String dscEstado;
	private String fecVencimiento;
	
	private String codDocPago;
	private String codDocPagoDet;
	private String codBien;
	private String dscBien;
	private String canSolicitada;
	private String dscPrecioUnitario;
	private String canFacturada;
	private String canEntregada;
	private String dscObservacion;
	private String indActivo;
	private String dscSubTotal;
	private String indModificacion;
	private String precioCompra;
	private String fecRecepcion;
	private String unidadMedida; //jhpr 2008-09-25
	
	/**
	 * @return the unidadMedida
	 */
	public String getUnidadMedida() {
		return unidadMedida;
	}
	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getFecRecepcion() {
		return fecRecepcion;
	}
	public void setFecRecepcion(String fecRecepcion) {
		this.fecRecepcion = fecRecepcion;
	}
	public String getCodCtaPago() {
		return codCtaPago;
	}
	public void setCodCtaPago(String codCtaPago) {
		this.codCtaPago = codCtaPago;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getFecEmisionGuia() {
		return fecEmisionGuia;
	}
	public void setFecEmisionGuia(String fecEmisionGuia) {
		this.fecEmisionGuia = fecEmisionGuia;
	}
	public String getNroCtaPago() {
		return nroCtaPago;
	}
	public void setNroCtaPago(String nroCtaPago) {
		this.nroCtaPago = nroCtaPago;
	}
	public String getFecEmisionCta() {
		return fecEmisionCta;
	}
	public void setFecEmisionCta(String fecEmisionCta) {
		this.fecEmisionCta = fecEmisionCta;
	}
	public String getDscMontoCta() {
		return dscMontoCta;
	}
	public void setDscMontoCta(String dscMontoCta) {
		this.dscMontoCta = dscMontoCta;
	}
	public String getCodCtaPagina() {
		return codCtaPagina;
	}
	public void setCodCtaPagina(String codCtaPagina) {
		this.codCtaPagina = codCtaPagina;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getFecVencimiento() {
		return fecVencimiento;
	}
	public void setFecVencimiento(String fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}
	public String getCodDocPago() {
		return codDocPago;
	}
	public void setCodDocPago(String codDocPago) {
		this.codDocPago = codDocPago;
	}
	public String getCodDocPagoDet() {
		return codDocPagoDet;
	}
	public void setCodDocPagoDet(String codDocPagoDet) {
		this.codDocPagoDet = codDocPagoDet;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getDscBien() {
		return dscBien;
	}
	public void setDscBien(String dscBien) {
		this.dscBien = dscBien;
	}
	public String getCanSolicitada() {
		return canSolicitada;
	}
	public void setCanSolicitada(String canSolicitada) {
		this.canSolicitada = canSolicitada;
	}
	public String getDscPrecioUnitario() {
		return dscPrecioUnitario;
	}
	public void setDscPrecioUnitario(String dscPrecioUnitario) {
		this.dscPrecioUnitario = dscPrecioUnitario;
	}
	public String getCanFacturada() {
		return canFacturada;
	}
	public void setCanFacturada(String canFacturada) {
		this.canFacturada = canFacturada;
	}
	public String getCanEntregada() {
		return canEntregada;
	}
	public void setCanEntregada(String canEntregada) {
		this.canEntregada = canEntregada;
	}
	public String getDscObservacion() {
		return dscObservacion;
	}
	public void setDscObservacion(String dscObservacion) {
		this.dscObservacion = dscObservacion;
	}
	public String getIndActivo() {
		return indActivo;
	}
	public void setIndActivo(String indActivo) {
		this.indActivo = indActivo;
	}
	public String getDscSubTotal() {
		return dscSubTotal;
	}
	public void setDscSubTotal(String dscSubTotal) {
		this.dscSubTotal = dscSubTotal;
	}
	public String getIndModificacion() {
		return indModificacion;
	}
	public void setIndModificacion(String indModificacion) {
		this.indModificacion = indModificacion;
	}
	public String getPrecioCompra() {
		return precioCompra;
	}
	public void setPrecioCompra(String precioCompra) {
		this.precioCompra = precioCompra;
	}
	

}
