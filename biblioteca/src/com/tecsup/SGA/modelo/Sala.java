package com.tecsup.SGA.modelo;

public class Sala implements java.io.Serializable {
  
  /**
	 * 
	 */
	private static final long serialVersionUID = -4920406606452238131L;
	private String codTipoSala;
  private String codTipSala;
  private String capacidadMax;
  private String codReserva;
  private String codSala;
  private String nombreSala;
  private String fechaReserva;
  private String idHoraIni;
  private String horaIni;
  private String idHoraFin;
  private String horaFin;
  private String usuReserva; // login del usuario a quien se le reserva la sala
  private String codEstado;
  private String estado;
  private String fechaInicio;
  private String fechaFin;
  
  private String vacantes;
  private String fechaBD;
  private String usuario;
  
  private String codUsuario;
  private String flagDisponibilidad;
  private String reservas;
  private String loginUsuario;
  
  private String usuCrea; // codigo usuario, puede ser el administrador
  private String sede;
  private String estadoPendiente;
  private String nombreUsuario;
  private String dias;
  
  /**
   * @return the estadoPendiente
   */
  public String getEstadoPendiente() {
    return estadoPendiente;
  }
  
  /**
   * @param estadoPendiente
   *          the estadoPendiente to set
   */
  public void setEstadoPendiente(String estadoPendiente) {
    this.estadoPendiente = estadoPendiente;
  }
  
  /**
   * @return the estadoReservado
   */
  public String getEstadoReservado() {
    return estadoReservado;
  }
  
  /**
   * @param estadoReservado
   *          the estadoReservado to set
   */
  public void setEstadoReservado(String estadoReservado) {
    this.estadoReservado = estadoReservado;
  }
  
  /**
   * @return the estadoEjecutado
   */
  public String getEstadoEjecutado() {
    return estadoEjecutado;
  }
  
  /**
   * @param estadoEjecutado
   *          the estadoEjecutado to set
   */
  public void setEstadoEjecutado(String estadoEjecutado) {
    this.estadoEjecutado = estadoEjecutado;
  }
  
  private String estadoReservado;
  private String estadoEjecutado;
  
  public String getLoginUsuario() {
    return loginUsuario;
  }
  
  public void setLoginUsuario(String loginUsuario) {
    this.loginUsuario = loginUsuario;
  }
  
  public String getReservas() {
    return reservas;
  }
  
  public void setReservas(String reservas) {
    this.reservas = reservas;
  }
  
  public String getUsuario() {
    return usuario;
  }
  
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
  
  public String getVacantes() {
    return vacantes;
  }
  
  public void setVacantes(String vacantes) {
    this.vacantes = vacantes;
  }
  
  public String getFechaBD() {
    return fechaBD;
  }
  
  public void setFechaBD(String fechaBD) {
    this.fechaBD = fechaBD;
  }
  
  public String getCodTipoSala() {
    return codTipoSala;
  }
  
  public void setCodTipoSala(String codTipoSala) {
    this.codTipoSala = codTipoSala;
  }
  
  public String getCapacidadMax() {
    return capacidadMax;
  }
  
  public void setCapacidadMax(String capacidadMax) {
    this.capacidadMax = capacidadMax;
  }
  
  public String getCodReserva() {
    return codReserva;
  }
  
  public void setCodReserva(String codReserva) {
    this.codReserva = codReserva;
  }
  
  public String getFechaReserva() {
    return fechaReserva;
  }
  
  public void setFechaReserva(String fechaReserva) {
    this.fechaReserva = fechaReserva;
  }
  
  public String getHoraIni() {
    return horaIni;
  }
  
  public void setHoraIni(String horaIni) {
    this.horaIni = horaIni;
  }
  
  public String getHoraFin() {
    return horaFin;
  }
  
  public void setHoraFin(String horaFin) {
    this.horaFin = horaFin;
  }
  
  public String getUsuReserva() {
    return usuReserva;
  }
  
  public void setUsuReserva(String usuReserva) {
    this.usuReserva = usuReserva;
  }
  
  public String getCodEstado() {
    return codEstado;
  }
  
  public void setCodEstado(String codEstado) {
    this.codEstado = codEstado;
  }
  
  public String getEstado() {
    return estado;
  }
  
  public void setEstado(String estado) {
    this.estado = estado;
  }
  
  public String getNombreSala() {
    return nombreSala;
  }
  
  public void setNombreSala(String nombreSala) {
    this.nombreSala = nombreSala;
  }
  
  public String getFechaInicio() {
    return fechaInicio;
  }
  
  public void setFechaInicio(String fechaInicio) {
    this.fechaInicio = fechaInicio;
  }
  
  public String getFechaFin() {
    return fechaFin;
  }
  
  public void setFechaFin(String fechaFin) {
    this.fechaFin = fechaFin;
  }
  
  public String getCodUsuario() {
    return codUsuario;
  }
  
  public void setCodUsuario(String codUsuario) {
    this.codUsuario = codUsuario;
  }
  
  public String getFlagDisponibilidad() {
    return flagDisponibilidad;
  }
  
  public void setFlagDisponibilidad(String flagDisponibilidad) {
    this.flagDisponibilidad = flagDisponibilidad;
  }
  
  public String getCodSala() {
    return codSala;
  }
  
  public void setCodSala(String codSala) {
    this.codSala = codSala;
  }
  
  public String getUsuCrea() {
    return usuCrea;
  }
  
  public void setUsuCrea(String usuCrea) {
    this.usuCrea = usuCrea;
  }
  
  /**
   * @return the sede
   */
  public String getSede() {
    return sede;
  }
  
  /**
   * @param sede
   *          the sede to set
   */
  public void setSede(String sede) {
    this.sede = sede;
  }
  
  public String getIdHoraIni() {
    return idHoraIni;
  }
  
  public void setIdHoraIni(String idHoraIni) {
    this.idHoraIni = idHoraIni;
  }
  
  public String getIdHoraFin() {
    return idHoraFin;
  }
  
  public void setIdHoraFin(String idHoraFin) {
    this.idHoraFin = idHoraFin;
  }
  
  public String getNombreUsuario() {
    return nombreUsuario;
  }
  
  public void setNombreUsuario(String nombreUsuario) {
    this.nombreUsuario = nombreUsuario;
  }
  
  @Override
  public String toString() {
    return "Sala [codTipoSala=" + codTipoSala + ", capacidadMax="
        + capacidadMax + ", codReserva=" + codReserva + ", codSala=" + codSala
        + ", nombreSala=" + nombreSala + ", fechaReserva=" + fechaReserva
        + ", idHoraIni=" + idHoraIni + ", horaIni=" + horaIni + ", idHoraFin="
        + idHoraFin + ", horaFin=" + horaFin + ", usuReserva=" + usuReserva
        + ", codEstado=" + codEstado + ", estado=" + estado + ", fechaInicio="
        + fechaInicio + ", fechaFin=" + fechaFin + ", vacantes=" + vacantes
        + ", fechaBD=" + fechaBD + ", usuario=" + usuario + ", codUsuario="
        + codUsuario + ", flagDisponibilidad=" + flagDisponibilidad
        + ", reservas=" + reservas + ", loginUsuario=" + loginUsuario
        + ", usuCrea=" + usuCrea + ", sede=" + sede + ", estadoPendiente="
        + estadoPendiente + ", nombreUsuario=" + nombreUsuario
        + ", estadoReservado=" + estadoReservado + ", estadoEjecutado="
        + estadoEjecutado + "]";
  }
  
  public String getCodTipSala() {
    return codTipSala;
  }
  
  public void setCodTipSala(String codTipSala) {
    this.codTipSala = codTipSala;
  }

public String getDias() {
	return dias;
}

public void setDias(String dias) {
	this.dias = dias;
}
  
}
