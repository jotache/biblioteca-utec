package com.tecsup.SGA.modelo;

public class InterfazContable {

	private String codEstado;
	private String dscEstado;
		
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
}
