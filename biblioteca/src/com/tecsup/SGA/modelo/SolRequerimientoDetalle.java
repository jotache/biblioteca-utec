package com.tecsup.SGA.modelo;

public class SolRequerimientoDetalle {
	 
	private String codRequerimiento;
	//******************************
	private String codigo;
	private String nombreServicio;
	private String descripcion;
	private String fechaEntrega;
	private String codigoBien;
	private String producto;
	private String unidadMedida;
	private String cantidad;
	private String precioUnitario;
	private String total;
	private String tipoGasto;
	//PARA DEVOLUCION
	private String cantidadDevuelta;
	private String maxDevolucion;
	private String totalDevuelto;
	private String indiceAsignarSerie;
	private String codDevolucionDet;
	//ALQD,19/08/08. PARA VALIDAR CANT. DE DECIMALES
	private String cantDecimales;
	//ALQD,29/10/08. MOSTRANDO LA CANTIDAD RECIBIDA Y DEVUELTA
	private String totCantRecibida;
	private String totCantDevuelta;
	
	public String getTotalDevuelto() {
		return totalDevuelto;
	}
	public void setTotalDevuelto(String totalDevuelto) {
		this.totalDevuelto = totalDevuelto;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombreServicio() {
		return nombreServicio;
	}
	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getCodigoBien() {
		return codigoBien;
	}
	public void setCodigoBien(String codigoBien) {
		this.codigoBien = codigoBien;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTipoGasto() {
		return tipoGasto;
	}
	public void setTipoGasto(String tipoGasto) {
		this.tipoGasto = tipoGasto;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getMaxDevolucion() {
		return maxDevolucion;
	}
	public void setMaxDevolucion(String maxDevolucion) {
		this.maxDevolucion = maxDevolucion;
	}
	public String getCantidadDevuelta() {
		return cantidadDevuelta;
	}
	public void setCantidadDevuelta(String cantidadDevuelta) {
		this.cantidadDevuelta = cantidadDevuelta;
	}
	public String getIndiceAsignarSerie() {
		return indiceAsignarSerie;
	}
	public void setIndiceAsignarSerie(String indiceAsignarSerie) {
		this.indiceAsignarSerie = indiceAsignarSerie;
	}
	public String getCodDevolucionDet() {
		return codDevolucionDet;
	}
	public void setCodDevolucionDet(String codDevolucionDet) {
		this.codDevolucionDet = codDevolucionDet;
	}
	public String getCantDecimales() {
		return cantDecimales;
	}
	public void setCantDecimales(String CantDecimales) {
		this.cantDecimales= CantDecimales;
	}
	public String getTotCantRecibida() {
		return totCantRecibida;
	}
	public void setTotCantRecibida(String TotCantRecibida) {
		this.totCantRecibida= TotCantRecibida;
	}
	public String getTotCantDevuelta() {
		return totCantDevuelta;
	}
	public void setTotCantDevuelta(String TotCantDevuelta) {
		this.totCantDevuelta= TotCantDevuelta;
	}
}