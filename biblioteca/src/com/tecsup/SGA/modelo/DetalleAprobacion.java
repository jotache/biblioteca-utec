package com.tecsup.SGA.modelo;

public class DetalleAprobacion implements java.io.Serializable{
	private String codUsuario;
	private String dscAprobador;
	private String fecAprobacion;
	//ALQD,19/02/09. NUEVA PROPIEDAD
	private String obsEstado;
	
	public String getObsEstado() {
		return obsEstado;
	}
	public void setObsEstado(String obsEstado) {
		this.obsEstado = obsEstado;
	}
	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getDscAprobador() {
		return dscAprobador;
	}

	public void setDscAprobador(String dscAprobador) {
		this.dscAprobador = dscAprobador;
	}
	public String getFecAprobacion() {
		return fecAprobacion;
	}
	public void setFecAprobacion(String fecAprobacion) {
		this.fecAprobacion = fecAprobacion;
	}

}
