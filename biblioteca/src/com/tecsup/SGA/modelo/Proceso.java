package com.tecsup.SGA.modelo;

import java.util.List;
import java.util.ArrayList;

public class Proceso implements java.io.Serializable {
	
	private String codProceso;
	private String dscProceso;
	private String codUnidadFuncional;
	private String dscUnidadFuncional;
	
	private String flagEtapa;
	private String codEtapa;
	private String dscEtapa;
	
	private List listAreasInteres;
	private String codAreasInteres;
	private String codSexo;
	private String codAreaEstudio;
	private String codGradoAcedemico;
	private String codInstitucionAcademica;
	private String salarioIni;
	private String salarioFin;
	private String codTipoEdad;
	private String edad;
	private String codTipoEdad1;
	private String edad1;
	private String codDedicacion;
	private String codInteresado;
	
	private String flagDisponibilidadViajar;
	private String anhosExperienciaDocencia;
	private String anhosExperiencia;
	
	private String codProcRevAsociado;
	
	private String fecRegistro;
	
	private String codOfertaAsociada;
	private String dscOfertaAsociada;
	private String usuario;
	private String codTipoMoneda;
	private String puestoPostula;
	private List listAreasEstudios;
	private String codAreasEstudios;
	
	private List listAreasNivelEstudios;
	private String codAreasNivelEstudios;
	private List<Evaluador> listaEvaluadores;	
	
	public List getListlistInstitucionesEdu() {
		return listlistInstitucionesEdu;
	}

	public void setListlistInstitucionesEdu(List listlistInstitucionesEdu) {
		this.listlistInstitucionesEdu = listlistInstitucionesEdu;
	}

	public String getCodInsEdu() {
		return codInsEdu;
	}

	public void setCodInsEdu(String codInsEdu) {
		this.codInsEdu = codInsEdu;
	}

	private List listlistInstitucionesEdu;
	private String codInsEdu;
	
	public String getPuestoPostula() {
		return puestoPostula;
	}
	
	public void setPuestoPostula(String puestoPostula) {
		this.puestoPostula = puestoPostula;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getDscOfertaAsociada() {
		return dscOfertaAsociada;
	}
	public void setDscOfertaAsociada(String dscOfertaAsociada) {
		this.dscOfertaAsociada = dscOfertaAsociada;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getDscProceso() {
		return dscProceso;
	}
	public void setDscProceso(String dscProceso) {
		this.dscProceso = dscProceso;
	}
	public String getCodUnidadFuncional() {
		return codUnidadFuncional;
	}
	public void setCodUnidadFuncional(String codUnidadFuncional) {
		this.codUnidadFuncional = codUnidadFuncional;
	}
	public String getDscUnidadFuncional() {
		return dscUnidadFuncional;
	}
	public void setDscUnidadFuncional(String dscUnidadFuncional) {
		this.dscUnidadFuncional = dscUnidadFuncional;
	}
	public String getFecRegistro() {
		return fecRegistro;
	}
	public void setFecRegistro(String fecRegistro) {
		this.fecRegistro = fecRegistro;
	}
	public String getCodOfertaAsociada() {
		return codOfertaAsociada;
	}
	public void setCodOfertaAsociada(String codOfertaAsociada) {
		this.codOfertaAsociada = codOfertaAsociada;
	}
	public String getFlagEtapa() {
		return flagEtapa;
	}
	public void setFlagEtapa(String flagEtapa) {
		this.flagEtapa = flagEtapa;
	}
	
	
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getDscEtapa() {
		return dscEtapa;
	}
	public void setDscEtapa(String dscEtapa) {
		this.dscEtapa = dscEtapa;
	}
	public List getListAreasInteres() {
		return listAreasInteres;
	}
	public void setListAreasInteres(List listAreasInteres) {
		this.listAreasInteres = listAreasInteres;
	}
	public String getCodSexo() {
		return codSexo;
	}
	public void setCodSexo(String codSexo) {
		this.codSexo = codSexo;
	}
	public String getCodAreaEstudio() {
		return codAreaEstudio;
	}
	public void setCodAreaEstudio(String codAreaEstudio) {
		this.codAreaEstudio = codAreaEstudio;
	}
	public String getCodGradoAcedemico() {
		return codGradoAcedemico;
	}
	public void setCodGradoAcedemico(String codGradoAcedemico) {
		this.codGradoAcedemico = codGradoAcedemico;
	}
	public String getCodInstitucionAcademica() {
		return codInstitucionAcademica;
	}
	public void setCodInstitucionAcademica(String codInstitucionAcademica) {
		this.codInstitucionAcademica = codInstitucionAcademica;
	}
	public String getSalarioIni() {
		return salarioIni;
	}
	public void setSalarioIni(String salarioIni) {
		this.salarioIni = salarioIni;
	}
	public String getSalarioFin() {
		return salarioFin;
	}
	public void setSalarioFin(String salarioFin) {
		this.salarioFin = salarioFin;
	}
	public String getCodTipoEdad() {
		return codTipoEdad;
	}
	public void setCodTipoEdad(String codTipoEdad) {
		this.codTipoEdad = codTipoEdad;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getCodDedicacion() {
		return codDedicacion;
	}
	public void setCodDedicacion(String codDedicacion) {
		this.codDedicacion = codDedicacion;
	}
	public String getCodInteresado() {
		return codInteresado;
	}
	public void setCodInteresado(String codInteresado) {
		this.codInteresado = codInteresado;
	}
	public String getFlagDisponibilidadViajar() {
		return flagDisponibilidadViajar;
	}
	public void setFlagDisponibilidadViajar(String flagDisponibilidadViajar) {
		this.flagDisponibilidadViajar = flagDisponibilidadViajar;
	}
	public String getAnhosExperienciaDocencia() {
		return anhosExperienciaDocencia;
	}
	public void setAnhosExperienciaDocencia(String anhosExperienciaDocencia) {
		this.anhosExperienciaDocencia = anhosExperienciaDocencia;
	}
	public String getAnhosExperiencia() {
		return anhosExperiencia;
	}
	public void setAnhosExperiencia(String anhosExperiencia) {
		this.anhosExperiencia = anhosExperiencia;
	}
	public String getCodProcRevAsociado() {
		return codProcRevAsociado;
	}
	public void setCodProcRevAsociado(String codProcRevAsociado) {
		this.codProcRevAsociado = codProcRevAsociado;
	}
	public String getCodAreasInteres() {
		return codAreasInteres;
	}
	public void setCodAreasInteres(String codAreasInteres) {
		this.codAreasInteres = codAreasInteres;
	}
	
	
	/*	CONSTRUCTORES	*/
	
	public String getCodTipoEdad1() {
		return codTipoEdad1;
	}
	public void setCodTipoEdad1(String codTipoEdad1) {
		this.codTipoEdad1 = codTipoEdad1;
	}
	public String getEdad1() {
		return edad1;
	}
	public void setEdad1(String edad1) {
		this.edad1 = edad1;
	}
	public Proceso()
	{
		this.codProceso = "";
		this.dscProceso = "";
		this.codUnidadFuncional = "";
		this.dscUnidadFuncional = "";		
		this.flagEtapa = "";
		this.codEtapa = "";
		this.dscEtapa = "";		
		this.listAreasInteres = new ArrayList();
		this.codAreasInteres = "";
		this.codSexo = "";
		this.codAreaEstudio = "";
		this.codGradoAcedemico = "";
		this.codInstitucionAcademica = "";
		this.salarioIni = "";
		this.salarioFin = "";
		this.codTipoEdad = "";
		this.edad = "";
		this.codTipoEdad1 = "";
		this.edad1 = "";
		this.codDedicacion = "";
		this.codInteresado = "";		
		this.flagDisponibilidadViajar = "";
		this.anhosExperienciaDocencia = "";
		this.anhosExperiencia = "";		
		this.codProcRevAsociado = "";		
		this.fecRegistro = "";
		this.codOfertaAsociada = "";
		this.dscOfertaAsociada = "";
		this.usuario = "";
		this.listAreasEstudios = new ArrayList();
	}
	public String getCodTipoMoneda() {
		return codTipoMoneda;
	}
	public void setCodTipoMoneda(String codTipoMoneda) {
		this.codTipoMoneda = codTipoMoneda;
	}

	public List getListAreasEstudios() {
		return listAreasEstudios;
	}

	public void setListAreasEstudios(List listAreasEstudios) {
		this.listAreasEstudios = listAreasEstudios;
	}

	public String getCodAreasEstudios() {
		return codAreasEstudios;
	}

	public void setCodAreasEstudios(String codAreasEstudios) {
		this.codAreasEstudios = codAreasEstudios;
	}

	public List getListAreasNivelEstudios() {
		return listAreasNivelEstudios;
	}

	public void setListAreasNivelEstudios(List listAreasNivelEstudios) {
		this.listAreasNivelEstudios = listAreasNivelEstudios;
	}

	public String getCodAreasNivelEstudios() {
		return codAreasNivelEstudios;
	}

	public void setCodAreasNivelEstudios(String codAreasNivelEstudios) {
		this.codAreasNivelEstudios = codAreasNivelEstudios;
	}

	public List<Evaluador> getListaEvaluadores() {
		return listaEvaluadores;
	}

	public void setListaEvaluadores(List<Evaluador> listaEvaluadores) {
		this.listaEvaluadores = listaEvaluadores;
	}
	
}
