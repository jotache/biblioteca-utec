package com.tecsup.SGA.modelo;

public class CotPendientes implements java.io.Serializable{

	private String codTipoReq;
	private String codSubTipoReq;
	private String nroReq;
	private String codFamilia;
	private String codSubFamilia;
	private String dscGrupoServicio;
	private String sede;
	private String codCeco;
	private String desCeco;
	private String usuSolicitante;
	/************************/
	private String codBien;
	private String valor1;
	private String valor2;
	private String valor3;
	private String fechaEntrega;
	private String codInvolucrados;
	private String nroProveedores;
	private String dscDetalle;
	//ALQD,03/03/09.NUEVA PROPIEDAD NOM_SERVICIO
	private String nomServicio;
	
	public String getNomServicio() {
		return nomServicio;
	}
	public void setNomServicio(String nomServicio) {
		this.nomServicio = nomServicio;
	}
	public String getDscDetalle() {
		return dscDetalle;
	}
	public void setDscDetalle(String dscDetalle) {
		this.dscDetalle = dscDetalle;
	}
	public String getCodTipoReq() {
		return codTipoReq;
	}
	public void setCodTipoReq(String codTipoReq) {
		this.codTipoReq = codTipoReq;
	}
	public String getCodSubTipoReq() {
		return codSubTipoReq;
	}
	public void setCodSubTipoReq(String codSubTipoReq) {
		this.codSubTipoReq = codSubTipoReq;
	}
	public String getNroReq() {
		return nroReq;
	}
	public void setNroReq(String nroReq) {
		this.nroReq = nroReq;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	public String getDscGrupoServicio() {
		return dscGrupoServicio;
	}
	public void setDscGrupoServicio(String dscGrupoServicio) {
		this.dscGrupoServicio = dscGrupoServicio;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getCodCeco() {
		return codCeco;
	}
	public void setCodCeco(String codCeco) {
		this.codCeco = codCeco;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getValor1() {
		return valor1;
	}
	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}
	public String getValor2() {
		return valor2;
	}
	public void setValor2(String valor2) {
		this.valor2 = valor2;
	}
	public String getValor3() {
		return valor3;
	}
	public void setValor3(String valor3) {
		this.valor3 = valor3;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getCodInvolucrados() {
		return codInvolucrados;
	}
	public void setCodInvolucrados(String codInvolucrados) {
		this.codInvolucrados = codInvolucrados;
	}
	public String getNroProveedores() {
		return nroProveedores;
	}
	public void setNroProveedores(String nroProveedores) {
		this.nroProveedores = nroProveedores;
	}
	public String getDesCeco() {
		return desCeco;
	}
	public void setDesCeco(String desCeco) {
		this.desCeco = desCeco;
	}
}
