package com.tecsup.SGA.modelo;

public class DocumentoAsociado {
	private String descripcion;
	private String monto;
	private String fecha;
	private String numero;
	private String dpdo_id;
	//ALQD,11/06/09.AGREGANDO NUEVA PROPIEDAD
	private String codTipMoneda;
	private String desTipMoneda;
	//ALQD,30/06/09.AGREGANDO NUEVA PROPIEDAD
	private String impTipCamSoles;
	//ALQD,06/07/09.AGREGANDO NUEVA PROPIEDAD
	private String monSoles;
	//ALQD,21/07/09.AGREGANDO NUEVAS PROPIEDADES
	private String codProvee;
	private String nomProvee;
	private String rucProvee;

	public String getCodProvee() {
		return codProvee;
	}
	public void setCodProvee(String codProvee) {
		this.codProvee=codProvee;
	}
	public String getNomProvee() {
		return nomProvee;
	}
	public void setNomProvee(String nomProvee) {
		this.nomProvee=nomProvee;
	}
	public String getRucProvee() {
		return rucProvee;
	}
	public void setRucProvee(String rucProvee) {
		this.rucProvee=rucProvee; 
	}
	public String getMonSoles() {
		return monSoles;
	}
	public void setMonSoles(String monSoles) {
		this.monSoles=monSoles;
	}
	public String getImpTipCamSoles() {
		return impTipCamSoles;
	}
	public void setImpTipCamSoles(String impTipCamSoles) {
		this.impTipCamSoles = impTipCamSoles;
	}
	public String getCodTipMoneda() {
		return codTipMoneda;
	}
	public void setCodTipMoneda(String codTipMoneda) {
		this.codTipMoneda=codTipMoneda;
	}
	public String getDesTipMoneda() {
		return desTipMoneda;
	}
	public void setDesTipMoneda(String desTipMoneda) {
		this.desTipMoneda=desTipMoneda;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getDpdo_id() {
		return dpdo_id;
	}
	public void setDpdo_id(String dpdo_id) {
		this.dpdo_id = dpdo_id;
	}
}
