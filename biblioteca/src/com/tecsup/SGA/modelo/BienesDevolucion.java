package com.tecsup.SGA.modelo;

public class BienesDevolucion {
	
	private String codDev;
	private String codReq;
	private String nroReq;
	private String dscCeco;
	private String dscTipoBien;
	private String cantDevuelta;
	private String fecDevolucion;
	private String dscEstado;
	
	public String getCodDev() {
		return codDev;
	}
	public void setCodDev(String codDev) {
		this.codDev = codDev;
	}
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getNroReq() {
		return nroReq;
	}
	public void setNroReq(String nroReq) {
		this.nroReq = nroReq;
	}
	public String getDscCeco() {
		return dscCeco;
	}
	public void setDscCeco(String dscCeco) {
		this.dscCeco = dscCeco;
	}
	public String getDscTipoBien() {
		return dscTipoBien;
	}
	public void setDscTipoBien(String dscTipoBien) {
		this.dscTipoBien = dscTipoBien;
	}
	public String getCantDevuelta() {
		return cantDevuelta;
	}
	public void setCantDevuelta(String cantDevuelta) {
		this.cantDevuelta = cantDevuelta;
	}
	public String getFecDevolucion() {
		return fecDevolucion;
	}
	public void setFecDevolucion(String fecDevolucion) {
		this.fecDevolucion = fecDevolucion;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	
}
