//JHPR : 2008-05-05 Calculo de Nota Tecsup.
package com.tecsup.SGA.modelo;

public class ConfiguracionNotaExterna {

	private Long id;
	private Long codCursoEjec;
	private String regla;
	
	private String condicion;
	
	private String verdadero1;
	private String verdadero2;
	private String verdadero3;
	private String verdadero4;
	
	private String falso1;
	private String falso2;
	private String falso3;
	private String falso4;
	
	private String operadorVerdad1;
	private String operadorVerdad2;
	private String operadorVerdad3;
	private String operadorVerdad4;
	
	private String operadorFalso1;
	private String operadorFalso2;
	private String operadorFalso3;
	private String operadorFalso4;
	
	private String estado;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getRegla() {
		return regla;
	}
	public void setRegla(String regla) {
		this.regla = regla;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getVerdadero1() {
		return verdadero1;
	}
	public void setVerdadero1(String verdadero1) {
		this.verdadero1 = verdadero1;
	}
	public String getVerdadero2() {
		return verdadero2;
	}
	public void setVerdadero2(String verdadero2) {
		this.verdadero2 = verdadero2;
	}
	public String getVerdadero3() {
		return verdadero3;
	}
	public void setVerdadero3(String verdadero3) {
		this.verdadero3 = verdadero3;
	}
	public String getVerdadero4() {
		return verdadero4;
	}
	public void setVerdadero4(String verdadero4) {
		this.verdadero4 = verdadero4;
	}
	public String getFalso1() {
		return falso1;
	}
	public void setFalso1(String falso1) {
		this.falso1 = falso1;
	}
	public String getFalso2() {
		return falso2;
	}
	public void setFalso2(String falso2) {
		this.falso2 = falso2;
	}
	public String getFalso3() {
		return falso3;
	}
	public void setFalso3(String falso3) {
		this.falso3 = falso3;
	}
	public String getFalso4() {
		return falso4;
	}
	public void setFalso4(String falso4) {
		this.falso4 = falso4;
	}
	public String getOperadorVerdad1() {
		return operadorVerdad1;
	}
	public void setOperadorVerdad1(String operadorVerdad1) {
		this.operadorVerdad1 = operadorVerdad1;
	}
	public String getOperadorVerdad2() {
		return operadorVerdad2;
	}
	public void setOperadorVerdad2(String operadorVerdad2) {
		this.operadorVerdad2 = operadorVerdad2;
	}
	public String getOperadorVerdad3() {
		return operadorVerdad3;
	}
	public void setOperadorVerdad3(String operadorVerdad3) {
		this.operadorVerdad3 = operadorVerdad3;
	}
	public String getOperadorVerdad4() {
		return operadorVerdad4;
	}
	public void setOperadorVerdad4(String operadorVerdad4) {
		this.operadorVerdad4 = operadorVerdad4;
	}
	public String getOperadorFalso1() {
		return operadorFalso1;
	}
	public void setOperadorFalso1(String operadorFalso1) {
		this.operadorFalso1 = operadorFalso1;
	}
	public String getOperadorFalso2() {
		return operadorFalso2;
	}
	public void setOperadorFalso2(String operadorFalso2) {
		this.operadorFalso2 = operadorFalso2;
	}
	public String getOperadorFalso3() {
		return operadorFalso3;
	}
	public void setOperadorFalso3(String operadorFalso3) {
		this.operadorFalso3 = operadorFalso3;
	}
	public String getOperadorFalso4() {
		return operadorFalso4;
	}
	public void setOperadorFalso4(String operadorFalso4) {
		this.operadorFalso4 = operadorFalso4;
	}
	public Long getCodCursoEjec() {
		return codCursoEjec;
	}
	public void setCodCursoEjec(Long codCursoEjec) {
		this.codCursoEjec = codCursoEjec;
	}
}
