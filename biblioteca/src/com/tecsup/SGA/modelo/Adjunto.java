package com.tecsup.SGA.modelo;

public class Adjunto {
	private String nuevoArchivo;
	private String archivo;
	public String getNuevoArchivo() {
		return nuevoArchivo;
	}
	public void setNuevoArchivo(String nuevoArchivo) {
		this.nuevoArchivo = nuevoArchivo;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
}
