package com.tecsup.SGA.modelo;

public class DefNroEvalParciales implements java.io.Serializable {

	private String nrevId;	
	private String codCurso;
	private String sihiId;
	private String nrevPeso;
	private String nrevFlgRealizado;
	private String nrevUsuCrea;
	private String nrevFecCrea;
	private String nrevUsuModi;
	private String nrevFecModi;
	private String nreEstReg;
	
	private String nreNroEval;

	//JHPR: 2008-06-09
	private String nomEspecialidad;
	private String nomCurso;
	private String nomSeccion;
	private String nomResponsable;
	private String nomDptoResponsable;
	private String estadoCerrado;
	private String ciclo;
	private String nomTipoEvaluacion;
	
	private String nroPractDef;
	private String nroPractEje;
	private String nroAsistenc;
	private String estadoAsist;
	
	public String getEstadoAsist() {
		return estadoAsist;
	}

	public void setEstadoAsist(String estadoAsist) {
		this.estadoAsist = estadoAsist;
	}

	public String getNrevId() {
		return nrevId;
	}

	public void setNrevId(String nrevId) {
		this.nrevId = nrevId;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public String getSihiId() {
		return sihiId;
	}

	public void setSihiId(String sihiId) {
		this.sihiId = sihiId;
	}

	public String getNrevPeso() {
		return nrevPeso;
	}

	public void setNrevPeso(String nrevPeso) {
		this.nrevPeso = nrevPeso;
	}

	public String getNrevFlgRealizado() {
		return nrevFlgRealizado;
	}

	public void setNrevFlgRealizado(String nrevFlgRealizado) {
		this.nrevFlgRealizado = nrevFlgRealizado;
	}

	public String getNrevUsuCrea() {
		return nrevUsuCrea;
	}

	public void setNrevUsuCrea(String nrevUsuCrea) {
		this.nrevUsuCrea = nrevUsuCrea;
	}

	public String getNrevFecCrea() {
		return nrevFecCrea;
	}

	public void setNrevFecCrea(String nrevFecCrea) {
		this.nrevFecCrea = nrevFecCrea;
	}

	public String getNrevUsuModi() {
		return nrevUsuModi;
	}

	public void setNrevUsuModi(String nrevUsuModi) {
		this.nrevUsuModi = nrevUsuModi;
	}

	public String getNrevFecModi() {
		return nrevFecModi;
	}

	public void setNrevFecModi(String nrevFecModi) {
		this.nrevFecModi = nrevFecModi;
	}

	public String getNreEstReg() {
		return nreEstReg;
	}

	public void setNreEstReg(String nreEstReg) {
		this.nreEstReg = nreEstReg;
	}

	public String getNreNroEval() {
		return nreNroEval;
	}

	public void setNreNroEval(String nreNroEval) {
		this.nreNroEval = nreNroEval;
	}

	public String getNomEspecialidad() {
		return nomEspecialidad;
	}

	public void setNomEspecialidad(String nomEspecialidad) {
		this.nomEspecialidad = nomEspecialidad;
	}

	public String getNomCurso() {
		return nomCurso;
	}

	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}

	public String getNomSeccion() {
		return nomSeccion;
	}

	public void setNomSeccion(String nomSeccion) {
		this.nomSeccion = nomSeccion;
	}

	public String getNomResponsable() {
		return nomResponsable;
	}

	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}

	public String getEstadoCerrado() {
		return estadoCerrado;
	}

	public void setEstadoCerrado(String estadoCerrado) {
		this.estadoCerrado = estadoCerrado;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	public String getNomTipoEvaluacion() {
		return nomTipoEvaluacion;
	}

	public void setNomTipoEvaluacion(String nomTipoEvaluacion) {
		this.nomTipoEvaluacion = nomTipoEvaluacion;
	}

	public String getNomDptoResponsable() {
		return nomDptoResponsable;
	}

	public void setNomDptoResponsable(String nomDptoResponsable) {
		this.nomDptoResponsable = nomDptoResponsable;
	}

	public String getNroPractDef() {
		return nroPractDef;
	}

	public void setNroPractDef(String nroPractDef) {
		this.nroPractDef = nroPractDef;
	}

	public String getNroPractEje() {
		return nroPractEje;
	}

	public void setNroPractEje(String nroPractEje) {
		this.nroPractEje = nroPractEje;
	}

	public String getNroAsistenc() {
		return nroAsistenc;
	}

	public void setNroAsistenc(String nroAsistenc) {
		this.nroAsistenc = nroAsistenc;
	}

}
