package com.tecsup.SGA.modelo;

public class SistemaEval {

private String codSistemaEval;
private String codTipoEvalu;
private String peso;

public String getCodSistemaEval() {
	return codSistemaEval;
}
public void setCodSistemaEval(String codSistemaEval) {
	this.codSistemaEval = codSistemaEval;
}
public String getCodTipoEvalu() {
	return codTipoEvalu;
}
public void setCodTipoEvalu(String codTipoEvalu) {
	this.codTipoEvalu = codTipoEvalu;
}
public String getPeso() {
	return peso;
}
public void setPeso(String peso) {
	this.peso = peso;
}
}
