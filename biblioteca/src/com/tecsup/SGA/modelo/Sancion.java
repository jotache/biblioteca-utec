package com.tecsup.SGA.modelo;

public class Sancion {
private String codFila;
private String codColumna;
private String valorFila;
private String dscFila;
private String valorColumna;
private String dscColumna;
private String valorSancion1;
private String valorSancion2;
private String valorSancion3;
private String valorSancion4;
private String valorSancion5;
private String observacion;
private String tipoSancion;

public String getObservacion() {
	return observacion;
}
public void setObservacion(String observacion) {
	this.observacion = observacion;
}
public String getValorSancion1() {
	return valorSancion1;
}
public void setValorSancion1(String valorSancion1) {
	this.valorSancion1 = valorSancion1;
}
public String getValorSancion2() {
	return valorSancion2;
}
public void setValorSancion2(String valorSancion2) {
	this.valorSancion2 = valorSancion2;
}
public String getValorSancion3() {
	return valorSancion3;
}
public void setValorSancion3(String valorSancion3) {
	this.valorSancion3 = valorSancion3;
}
public String getValorSancion4() {
	return valorSancion4;
}
public void setValorSancion4(String valorSancion4) {
	this.valorSancion4 = valorSancion4;
}
public String getValorSancion5() {
	return valorSancion5;
}
public void setValorSancion5(String valorSancion5) {
	this.valorSancion5 = valorSancion5;
}
public String getCodFila() {
	return codFila;
}
public void setCodFila(String codFila) {
	this.codFila = codFila;
}
public String getCodColumna() {
	return codColumna;
}
public void setCodColumna(String codColumna) {
	this.codColumna = codColumna;
}
public String getValorFila() {
	return valorFila;
}
public void setValorFila(String valorFila) {
	this.valorFila = valorFila;
}
public String getValorColumna() {
	return valorColumna;
}
public void setValorColumna(String valorColumna) {
	this.valorColumna = valorColumna;
}
public String getDscColumna() {
	return dscColumna;
}
public void setDscColumna(String dscColumna) {
	this.dscColumna = dscColumna;
}
public String getDscFila() {
	return dscFila;
}
public void setDscFila(String dscFila) {
	this.dscFila = dscFila;
}
public String getTipoSancion() {
	return tipoSancion;
}
public void setTipoSancion(String tipoSancion) {
	this.tipoSancion = tipoSancion;
}
@Override
public String toString() {
	return "Sancion [codFila=" + codFila + ", codColumna=" + codColumna
			+ ", valorFila=" + valorFila + ", dscFila=" + dscFila
			+ ", valorColumna=" + valorColumna + ", dscColumna=" + dscColumna
			+ ", valorSancion1=" + valorSancion1 + ", valorSancion2="
			+ valorSancion2 + ", valorSancion3=" + valorSancion3
			+ ", valorSancion4=" + valorSancion4 + ", valorSancion5="
			+ valorSancion5 + ", observacion=" + observacion + ", tipoSancion="
			+ tipoSancion + "]";
}


}
