package com.tecsup.SGA.modelo;

public class Parciales implements java.io.Serializable{
	private String codId;
	private String peso;
	private String codEvaluador;
	private String nombreEvaluador;
	private String flag;
	private String fechaRegistro;
	private String codCurso;
	private String codTipoEvaluacion;
	private String nroEval;
	private String codCursoId;
	private String seccion;
	private String codPeriodo;
	private String flagEliminar;
	private String codPro;
	private String codEspe;
	//JHPR: 2008-06-04 Flag de estado del registro: Abierto o Cerrado
	private String flagCerrarNota;
	private String flagCerrarAsistencia;
	//JHPR: 2008-06-12 Flag realizado (para corregir que se creen registro duplicados en notas parciales - desde la pagina jsp se envia "actualizar" o "insertar" segun las notas vacias que contenga la lista, esto era impreciso)	
	private String flagRealizado;
	
	
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getCodCursoId() {
		return codCursoId;
	}
	public void setCodCursoId(String codCursoId) {
		this.codCursoId = codCursoId;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getNombreEvaluador() {
		return nombreEvaluador;
	}
	public void setNombreEvaluador(String nombreEvaluador) {
		this.nombreEvaluador = nombreEvaluador;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodTipoEvaluacion() {
		return codTipoEvaluacion;
	}
	public void setCodTipoEvaluacion(String codTipoEvaluacion) {
		this.codTipoEvaluacion = codTipoEvaluacion;
	}
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	public String getNroEval() {
		return nroEval;
	}
	public void setNroEval(String nroEval) {
		this.nroEval = nroEval;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getFlagEliminar() {
		return flagEliminar;
	}
	public void setFlagEliminar(String flagEliminar) {
		this.flagEliminar = flagEliminar;
	}
	public String getCodPro() {
		return codPro;
	}
	public void setCodPro(String codPro) {
		this.codPro = codPro;
	}
	public String getCodEspe() {
		return codEspe;
	}
	public void setCodEspe(String codEspe) {
		this.codEspe = codEspe;
	}
	public String getFlagCerrarNota() {
		return flagCerrarNota;
	}
	public void setFlagCerrarNota(String flagCerrarNota) {
		this.flagCerrarNota = flagCerrarNota;
	}
	public String getFlagRealizado() {
		return flagRealizado;
	}
	public void setFlagRealizado(String flagRealizado) {
		this.flagRealizado = flagRealizado;
	}
	public String getFlagCerrarAsistencia() {
		return flagCerrarAsistencia;
	}
	public void setFlagCerrarAsistencia(String flagCerrarAsistencia) {
		this.flagCerrarAsistencia = flagCerrarAsistencia;
	}
	
}
