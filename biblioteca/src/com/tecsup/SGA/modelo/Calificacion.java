package com.tecsup.SGA.modelo;

public class Calificacion implements java.io.Serializable{
	
	private String codCalificacion;
	private String codCalificacionId;
	private String codEtapa;
	private String codTipoEvaluacion;
	private String codCalificacionNormal;
	private String dscCalificacionNormal;
	private String codCalificacionEliminatoria;
	private String dscCalificacionEliminatoria;
	
	public String getCodCalificacion() {
		return codCalificacion;
	}
	public void setCodCalificacion(String codCalificacion) {
		this.codCalificacion = codCalificacion;
	}
	public String getCodEtapa() {
		return codEtapa;
	}
	public void setCodEtapa(String codEtapa) {
		this.codEtapa = codEtapa;
	}
	public String getCodTipoEvaluacion() {
		return codTipoEvaluacion;
	}
	public void setCodTipoEvaluacion(String codTipoEvaluacion) {
		this.codTipoEvaluacion = codTipoEvaluacion;
	}
	public String getCodCalificacionNormal() {
		return codCalificacionNormal;
	}
	public void setCodCalificacionNormal(String codCalificacionNormal) {
		this.codCalificacionNormal = codCalificacionNormal;
	}
	public String getDscCalificacionNormal() {
		return dscCalificacionNormal;
	}
	public void setDscCalificacionNormal(String dscCalificacionNormal) {
		this.dscCalificacionNormal = dscCalificacionNormal;
	}
	public String getCodCalificacionEliminatoria() {
		return codCalificacionEliminatoria;
	}
	public void setCodCalificacionEliminatoria(String codCalificacionEliminatoria) {
		this.codCalificacionEliminatoria = codCalificacionEliminatoria;
	}
	public String getDscCalificacionEliminatoria() {
		return dscCalificacionEliminatoria;
	}
	public void setDscCalificacionEliminatoria(String dscCalificacionEliminatoria) {
		this.dscCalificacionEliminatoria = dscCalificacionEliminatoria;
	}
	public String getCodCalificacionId() {
		return codCalificacionId;
	}
	public void setCodCalificacionId(String codCalificacionId) {
		this.codCalificacionId = codCalificacionId;
	}
	
	
}
