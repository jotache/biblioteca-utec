package com.tecsup.SGA.modelo;

public class CotizacionProveedor {
	private String codCoti;
	private String codCotiDet;
	private String codProv;
	private String indAsignacion;
	private String indEnvioCorreo;
	private String horaEnvioProp;	
	private String totalPropuesta;
	private String totalPrpopuestaFinal;
	private String codEstado;
	private String dscEstado;
	private String indExcede;
	private String montoExcede;
	private String detalleCondicion;
	private String fechaAprobacion;
	private String fechaRechazo;	
	private String codOrden;
	
	private String razonSocial; 
	private String codTipoPersona;
	private String dscTipoPersona;
	private String codCalificacion;
	private String dscCalificacion;
	
	private String dscMoneda;
	private String dscMonto;
	private String dscNroOrden;
	
	private String descripcion;
	private String dscDetalleCoti;
	private String dscDetalleProv;
	private String dscDetalleCotiFin;
	
	private String fechaEmision;
	private String dscProveedor;
	private String dscTipoOrden;
	private String dscEstadoOrden;
	private String indiceAprobacion;
	//ALQD,11/02/09.AGREGANDO PROPIEDAD DE TIPO GASTO ORDEN
	private String tipGasOrden;
	//ALQD,24/02/09.AGREGANDO PROPIEDAD DE TIPO DE PAGO
	private String codTipPago;
	private String desTipPago;
	private String cajaChica;
	//ALQD,25/05/09.NUEVA PROPIEDAD
	private String afectoIGV;
	
	public String getAfectoIGV() {
		return afectoIGV;
	}
	public void setAfectoIGV(String afectoIGV) {
		this.afectoIGV=afectoIGV;
	}
	public String getCodTipPago() {
		return codTipPago;
	}
	public void setCodTipPago(String codTipPago) {
		this.codTipPago=codTipPago;
	}
	public String getDesTipPago() {
		return desTipPago;
	}
	public void setDesTipPago(String desTipPago) {
		this.desTipPago=desTipPago;
	}
	public String getCajaChica() {
		return cajaChica;
	}
	public void setCajaChica(String cajaChica) {
		this.cajaChica=cajaChica;
	}
	public String getTipGasOrden() {
		return tipGasOrden;
	}
	public void setTipGasOrden(String tipGasOrden) {
		this.tipGasOrden=tipGasOrden;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getDscMoneda() {
		return dscMoneda;
	}
	public void setDscMoneda(String dscMoneda) {
		this.dscMoneda = dscMoneda;
	}
	public String getDscMonto() {
		return dscMonto;
	}
	public void setDscMonto(String dscMonto) {
		this.dscMonto = dscMonto;
	}
	public String getDscNroOrden() {
		return dscNroOrden;
	}
	public void setDscNroOrden(String dscNroOrden) {
		this.dscNroOrden = dscNroOrden;
	}
	public String getCodCoti() {
		return codCoti;
	}
	public void setCodCoti(String codCoti) {
		this.codCoti = codCoti;
	}
	public String getCodCotiDet() {
		return codCotiDet;
	}
	public void setCodCotiDet(String codCotiDet) {
		this.codCotiDet = codCotiDet;
	}
	public String getCodProv() {
		return codProv;
	}
	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}
	public String getIndAsignacion() {
		return indAsignacion;
	}
	public void setIndAsignacion(String indAsignacion) {
		this.indAsignacion = indAsignacion;
	}
	public String getIndEnvioCorreo() {
		return indEnvioCorreo;
	}
	public void setIndEnvioCorreo(String indEnvioCorreo) {
		this.indEnvioCorreo = indEnvioCorreo;
	}
	public String getHoraEnvioProp() {
		return horaEnvioProp;
	}
	public void setHoraEnvioProp(String horaEnvioProp) {
		this.horaEnvioProp = horaEnvioProp;
	}
	public String getTotalPropuesta() {
		return totalPropuesta;
	}
	public void setTotalPropuesta(String totalPropuesta) {
		this.totalPropuesta = totalPropuesta;
	}
	public String getTotalPrpopuestaFinal() {
		return totalPrpopuestaFinal;
	}
	public void setTotalPrpopuestaFinal(String totalPrpopuestaFinal) {
		this.totalPrpopuestaFinal = totalPrpopuestaFinal;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getIndExcede() {
		return indExcede;
	}
	public void setIndExcede(String indExcede) {
		this.indExcede = indExcede;
	}
	public String getMontoExcede() {
		return montoExcede;
	}
	public void setMontoExcede(String montoExcede) {
		this.montoExcede = montoExcede;
	}
	public String getDetalleCondicion() {
		return detalleCondicion;
	}
	public void setDetalleCondicion(String detalleCondicion) {
		this.detalleCondicion = detalleCondicion;
	}
	public String getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(String fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public String getFechaRechazo() {
		return fechaRechazo;
	}
	public void setFechaRechazo(String fechaRechazo) {
		this.fechaRechazo = fechaRechazo;
	}
	public String getCodOrden() {
		return codOrden;
	}
	public void setCodOrden(String codOrden) {
		this.codOrden = codOrden;
	}
	public String getCodTipoPersona() {
		return codTipoPersona;
	}
	public void setCodTipoPersona(String codTipoPersona) {
		this.codTipoPersona = codTipoPersona;
	}
	public String getDscTipoPersona() {
		return dscTipoPersona;
	}
	public void setDscTipoPersona(String dscTipoPersona) {
		this.dscTipoPersona = dscTipoPersona;
	}
	public String getCodCalificacion() {
		return codCalificacion;
	}
	public void setCodCalificacion(String codCalificacion) {
		this.codCalificacion = codCalificacion;
	}
	public String getDscCalificacion() {
		return dscCalificacion;
	}
	public void setDscCalificacion(String dscCalificacion) {
		this.dscCalificacion = dscCalificacion;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDscDetalleCoti() {
		return dscDetalleCoti;
	}
	public void setDscDetalleCoti(String dscDetalleCoti) {
		this.dscDetalleCoti = dscDetalleCoti;
	}
	public String getDscDetalleProv() {
		return dscDetalleProv;
	}
	public void setDscDetalleProv(String dscDetalleProv) {
		this.dscDetalleProv = dscDetalleProv;
	}
	public String getDscDetalleCotiFin() {
		return dscDetalleCotiFin;
	}
	public void setDscDetalleCotiFin(String dscDetalleCotiFin) {
		this.dscDetalleCotiFin = dscDetalleCotiFin;
	}
	public String getDscProveedor() {
		return dscProveedor;
	}
	public void setDscProveedor(String dscProveedor) {
		this.dscProveedor = dscProveedor;
	}
	public String getDscTipoOrden() {
		return dscTipoOrden;
	}
	public void setDscTipoOrden(String dscTipoOrden) {
		this.dscTipoOrden = dscTipoOrden;
	}
	public String getDscEstadoOrden() {
		return dscEstadoOrden;
	}
	public void setDscEstadoOrden(String dscEstadoOrden) {
		this.dscEstadoOrden = dscEstadoOrden;
	}
	public String getIndiceAprobacion() {
		return indiceAprobacion;
	}
	public void setIndiceAprobacion(String indiceAprobacion) {
		this.indiceAprobacion = indiceAprobacion;
	}	
	
}
