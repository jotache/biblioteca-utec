package com.tecsup.SGA.modelo;

public class Producto implements java.io.Serializable{
 
	 private String codProducto;
	 private String codTipoTabla;
	 private String codTipoTablaDetalle;
	 private String descripcion;
	 private String estReg;
	 private String seleccion;
	 private String codigo;
	 private String codTipoAdj;
	 private String dscTipAdj;
	 private String rutaAdj;	 
	 private String dscFamilia;
	 private String dscSubFamilia;
	 private String dscUnidad;
	 private String dscUbicacion;
	 private String nomProducto;
	 private String indPrograma;
	 private String cantidad;
	 private String cantidadInventario;
	 private String cantidadDiferencia;
//ALQD,23/12/08. A�ADIENDO DOS PROPIEDADES: P.UNITARIO Y TOTAL DIF.
	 private String preUnitario;
	 private String totDiferencia;
	public String getPreUnitario() {
		return preUnitario;
	}
	public void setPreUnitario(String preUnitario) {
		this.preUnitario=preUnitario;
	}
	public String getTotDiferencia() {
		return totDiferencia;
	}
	public void setTotDiferencia(String totDiferencia) {
		this.totDiferencia=totDiferencia;
	}
	public String getDscSubFamilia() {
		return dscSubFamilia;
	}
	public void setDscSubFamilia(String dscSubFamilia) {
		this.dscSubFamilia = dscSubFamilia;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getCantidadInventario() {
		return cantidadInventario;
	}
	public void setCantidadInventario(String cantidadInventario) {
		this.cantidadInventario = cantidadInventario;
	}
	public String getCantidadDiferencia() {
		return cantidadDiferencia;
	}
	public void setCantidadDiferencia(String cantidadDiferencia) {
		this.cantidadDiferencia = cantidadDiferencia;
	}
	public String getNomProducto() {
		return nomProducto;
	}
	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}
	public String getIndPrograma() {
		return indPrograma;
	}
	public void setIndPrograma(String indPrograma) {
		this.indPrograma = indPrograma;
	}
	public String getSeleccion() {
		return seleccion;
	}
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	public String getCodTipoTabla() {
		return codTipoTabla;
	}
	public void setCodTipoTabla(String codTipoTabla) {
		this.codTipoTabla = codTipoTabla;
	}
	public String getCodTipoTablaDetalle() {
		return codTipoTablaDetalle;
	}
	public void setCodTipoTablaDetalle(String codTipoTablaDetalle) {
		this.codTipoTablaDetalle = codTipoTablaDetalle;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodTipoAdj() {
		return codTipoAdj;
	}
	public void setCodTipoAdj(String codTipoAdj) {
		this.codTipoAdj = codTipoAdj;
	}
	public String getDscTipAdj() {
		return dscTipAdj;
	}
	public void setDscTipAdj(String dscTipAdj) {
		this.dscTipAdj = dscTipAdj;
	}
	public String getRutaAdj() {
		return rutaAdj;
	}
	public void setRutaAdj(String rutaAdj) {
		this.rutaAdj = rutaAdj;
	}
	public String getDscFamilia() {
		return dscFamilia;
	}
	public void setDscFamilia(String dscFamilia) {
		this.dscFamilia = dscFamilia;
	}
	public String getDscUnidad() {
		return dscUnidad;
	}
	public void setDscUnidad(String dscUnidad) {
		this.dscUnidad = dscUnidad;
	}
	public String getDscUbicacion() {
		return dscUbicacion;
	}
	public void setDscUbicacion(String dscUbicacion) {
		this.dscUbicacion = dscUbicacion;
	}

}
