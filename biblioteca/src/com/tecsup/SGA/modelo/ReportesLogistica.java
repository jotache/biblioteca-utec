package com.tecsup.SGA.modelo;

public class ReportesLogistica implements java.io.Serializable{
		
	private String nroGuia;
	private String fechaCierreGuia;
	private String nroOrden;
	private String fechaOrden;
	private String dscProveedor;
	private String dscTipoBien;
	private String dscFamiliaBien;
	private String codBien;
	private String dscBien;
	private String dscUnidad;
	private String cantidad;
	private String costoUnitario;
	private String importe;
	private String cantPorEntregar;
	private String dscEstadoOrden;
	
	private String fechaEntrega;
	private String dscCeco;
	private String dscTipoGasto;
	private String dscFamilia;
	private String nroRequerimiento;
	private String fechaAprobaReq;
	private String cantidadAtender;
   
	private String nroDocRelacionado;
	private String fechaDocumento;
	private String cantidadIngreso;
	private String cantidadSalida;
	private String saldo;
	private String importeIngreso;
	private String importeSalida;
	private String importeSaldo;
	
	private String saldoFisico;
	private String saldoDisponible;
	private String comprador;
	private String fecUltimoDespacho;
	private String fecUltimoOC;
	
	private String dscTipoReq;
	private String dscTipoBienServ;
	private String codCeco;
	private String responsableCeco;
	private String dscMoneda;
	private String totalCargado;
	private String cantReqAtendidos;
	private String cantReqPorCerrar;
	private String tipoCambio;
	private String importeSoles;
	
	private String codFamilia;
	private String codTipoGasto;
	private String cantConsumida;
	private String precioPromedio;

	private String dscTipoCotizacion;
	private String dscTipoPago;
	private String nroCotizacion;
	private String fecEnvioCotizacion;
	private String fecInicioVigencia;
	private String fecFinVigencia;
	private String dscEstadoCotizacion;
	private String dscBienServicio;
	private String cotiGanador;
	private String dscProveedorGanador;
	private String listadoCotizaciones;
	private String codSubFamilia;
	
	private String fecEnvioReq;
	private String dscUsuarioSol;
	private String cantRequerimiento;
	private String precioUnitario;
	private String dscImporteProrrogaReq;
	private String dscEstadoReq;
	private String fecAtencion;
	
	private String dscTipoOrden;
	private String rucProveedor;
	private String indiceLocal;
	private String dscRazonSocial;
	private String importeBienServ;
	private String fechaCierre;
	private String dscEstadoOC;
	private String dscCondicionFinal1;
	
	private String dscTipoPeriodo;
	private String nroComprobante;
	private String fechaEmision;
	private String fechaRecepcion;
	private String fechaVencimiento;
	private String dscImporte;
	private String cantAtenInt;
	private String saldoInicial;
	private String costoUnitarioInicial;
	private String importeSaldoInicial;
	private String saldoFinal;
	private String costoUnitarioFinal;
	private String importeSaldoFinal;
		
	private String dscEstDocPago;
	private String nomSolicitante;
//ALQD,28/01/09. SE A�ADE DOS PROPIEDADES MAS
	private String codSede;
	private String cantReqDevuelto;
	
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede=codSede;
	}
	public String getCantReqDevuelto(){
		return cantReqDevuelto;
	}
	public void setCantReqDevuelto(String cantReqDevuelto) {
		this.cantReqDevuelto=cantReqDevuelto;
	}
	public String getSaldoFinal() {
		return saldoFinal;
	}
	public void setSaldoFinal(String saldoFinal) {
		this.saldoFinal = saldoFinal;
	}
	public String getCostoUnitarioFinal() {
		return costoUnitarioFinal;
	}
	public void setCostoUnitarioFinal(String costoUnitarioFinal) {
		this.costoUnitarioFinal = costoUnitarioFinal;
	}
	public String getImporteSaldoFinal() {
		return importeSaldoFinal;
	}
	public void setImporteSaldoFinal(String importeSaldoFinal) {
		this.importeSaldoFinal = importeSaldoFinal;
	}
	public String getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(String saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public String getCostoUnitarioInicial() {
		return costoUnitarioInicial;
	}
	public void setCostoUnitarioInicial(String costoUnitarioInicial) {
		this.costoUnitarioInicial = costoUnitarioInicial;
	}
	public String getImporteSaldoInicial() {
		return importeSaldoInicial;
	}
	public void setImporteSaldoInicial(String importeSaldoInicial) {
		this.importeSaldoInicial = importeSaldoInicial;
	}
	public String getCantAtenInt() {
		return cantAtenInt;
	}
	public void setCantAtenInt(String cantAtenInt) {
		this.cantAtenInt = cantAtenInt;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	public String getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	public String getDscImporte() {
		return dscImporte;
	}
	public void setDscImporte(String dscImporte) {
		this.dscImporte = dscImporte;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getDscEstadoOC() {
		return dscEstadoOC;
	}
	public void setDscEstadoOC(String dscEstadoOC) {
		this.dscEstadoOC = dscEstadoOC;
	}
	public String getFecAtencion() {
		return fecAtencion;
	}
	public void setFecAtencion(String fecAtencion) {
		this.fecAtencion = fecAtencion;
	}
	public String getDscEstadoReq() {
		return dscEstadoReq;
	}
	public void setDscEstadoReq(String dscEstadoReq) {
		this.dscEstadoReq = dscEstadoReq;
	}
	public String getFecEnvioReq() {
		return fecEnvioReq;
	}
	public void setFecEnvioReq(String fecEnvioReq) {
		this.fecEnvioReq = fecEnvioReq;
	}
	public String getDscUsuarioSol() {
		return dscUsuarioSol;
	}
	public void setDscUsuarioSol(String dscUsuarioSol) {
		this.dscUsuarioSol = dscUsuarioSol;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public String getDscTipoReq() {
		return dscTipoReq;
	}
	public void setDscTipoReq(String dscTipoReq) {
		this.dscTipoReq = dscTipoReq;
	}
	public String getDscTipoBienServ() {
		return dscTipoBienServ;
	}
	public void setDscTipoBienServ(String dscTipoBienServ) {
		this.dscTipoBienServ = dscTipoBienServ;
	}
	public String getCodCeco() {
		return codCeco;
	}
	public void setCodCeco(String codCeco) {
		this.codCeco = codCeco;
	}
	public String getResponsableCeco() {
		return responsableCeco;
	}
	public void setResponsableCeco(String responsableCeco) {
		this.responsableCeco = responsableCeco;
	}
	public String getDscMoneda() {
		return dscMoneda;
	}
	public void setDscMoneda(String dscMoneda) {
		this.dscMoneda = dscMoneda;
	}
	public String getTotalCargado() {
		return totalCargado;
	}
	public void setTotalCargado(String totalCargado) {
		this.totalCargado = totalCargado;
	}
	public String getCantReqAtendidos() {
		return cantReqAtendidos;
	}
	public void setCantReqAtendidos(String cantReqAtendidos) {
		this.cantReqAtendidos = cantReqAtendidos;
	}
	public String getCantReqPorCerrar() {
		return cantReqPorCerrar;
	}
	public void setCantReqPorCerrar(String cantReqPorCerrar) {
		this.cantReqPorCerrar = cantReqPorCerrar;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getImporteSoles() {
		return importeSoles;
	}
	public void setImporteSoles(String importeSoles) {
		this.importeSoles = importeSoles;
	}
	public String getSaldoFisico() {
		return saldoFisico;
	}
	public void setSaldoFisico(String saldoFisico) {
		this.saldoFisico = saldoFisico;
	}
	public String getSaldoDisponible() {
		return saldoDisponible;
	}
	public void setSaldoDisponible(String saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}
	public String getComprador() {
		return comprador;
	}
	public void setComprador(String comprador) {
		this.comprador = comprador;
	}
	public String getFecUltimoDespacho() {
		return fecUltimoDespacho;
	}
	public void setFecUltimoDespacho(String fecUltimoDespacho) {
		this.fecUltimoDespacho = fecUltimoDespacho;
	}
	public String getFecUltimoOC() {
		return fecUltimoOC;
	}
	public void setFecUltimoOC(String fecUltimoOC) {
		this.fecUltimoOC = fecUltimoOC;
	}
	public String getNroDocRelacionado() {
		return nroDocRelacionado;
	}
	public void setNroDocRelacionado(String nroDocRelacionado) {
		this.nroDocRelacionado = nroDocRelacionado;
	}
	public String getFechaDocumento() {
		return fechaDocumento;
	}
	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	public String getCantidadIngreso() {
		return cantidadIngreso;
	}
	public void setCantidadIngreso(String cantidadIngreso) {
		this.cantidadIngreso = cantidadIngreso;
	}
	public String getCantidadSalida() {
		return cantidadSalida;
	}
	public void setCantidadSalida(String cantidadSalida) {
		this.cantidadSalida = cantidadSalida;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	public String getImporteSalida() {
		return importeSalida;
	}
	public void setImporteSalida(String importeSalida) {
		this.importeSalida = importeSalida;
	}
	public String getImporteSaldo() {
		return importeSaldo;
	}
	public void setImporteSaldo(String importeSaldo) {
		this.importeSaldo = importeSaldo;
	}
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public String getFechaCierreGuia() {
		return fechaCierreGuia;
	}
	public void setFechaCierreGuia(String fechaCierreGuia) {
		this.fechaCierreGuia = fechaCierreGuia;
	}
	public String getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getFechaOrden() {
		return fechaOrden;
	}
	public void setFechaOrden(String fechaOrden) {
		this.fechaOrden = fechaOrden;
	}
	public String getDscProveedor() {
		return dscProveedor;
	}
	public void setDscProveedor(String dscProveedor) {
		this.dscProveedor = dscProveedor;
	}
	public String getDscTipoBien() {
		return dscTipoBien;
	}
	public void setDscTipoBien(String dscTipoBien) {
		this.dscTipoBien = dscTipoBien;
	}
	public String getDscFamiliaBien() {
		return dscFamiliaBien;
	}
	public void setDscFamiliaBien(String dscFamiliaBien) {
		this.dscFamiliaBien = dscFamiliaBien;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getDscBien() {
		return dscBien;
	}
	public void setDscBien(String dscBien) {
		this.dscBien = dscBien;
	}
	public String getDscUnidad() {
		return dscUnidad;
	}
	public void setDscUnidad(String dscUnidad) {
		this.dscUnidad = dscUnidad;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getCostoUnitario() {
		return costoUnitario;
	}
	public void setCostoUnitario(String costoUnitario) {
		this.costoUnitario = costoUnitario;
	}
	public String getImporte() {
		return importe;
	}
	
	public String getCantPorEntregar() {
		return cantPorEntregar;
	}
	public void setCantPorEntregar(String cantPorEntregar) {
		this.cantPorEntregar = cantPorEntregar;
	}
	public String getDscEstadoOrden() {
		return dscEstadoOrden;
	}
	public void setDscEstadoOrden(String dscEstadoOrden) {
		this.dscEstadoOrden = dscEstadoOrden;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getDscCeco() {
		return dscCeco;
	}
	public void setDscCeco(String dscCeco) {
		this.dscCeco = dscCeco;
	}
	public String getDscTipoGasto() {
		return dscTipoGasto;
	}
	public void setDscTipoGasto(String dscTipoGasto) {
		this.dscTipoGasto = dscTipoGasto;
	}
	public String getDscFamilia() {
		return dscFamilia;
	}
	public void setDscFamilia(String dscFamilia) {
		this.dscFamilia = dscFamilia;
	}
	public String getNroRequerimiento() {
		return nroRequerimiento;
	}
	public void setNroRequerimiento(String nroRequerimiento) {
		this.nroRequerimiento = nroRequerimiento;
	}
	public String getFechaAprobaReq() {
		return fechaAprobaReq;
	}
	public void setFechaAprobaReq(String fechaAprobaReq) {
		this.fechaAprobaReq = fechaAprobaReq;
	}
	public String getCantidadAtender() {
		return cantidadAtender;
	}
	public void setCantidadAtender(String cantidadAtender) {
		this.cantidadAtender = cantidadAtender;
	}
	public String getImporteIngreso() {
		return importeIngreso;
	}
	public void setImporteIngreso(String importeIngreso) {
		this.importeIngreso = importeIngreso;
	}
	public String getCodTipoGasto() {
		return codTipoGasto;
	}
	public void setCodTipoGasto(String codTipoGasto) {
		this.codTipoGasto = codTipoGasto;
	}
	public String getCantConsumida() {
		return cantConsumida;
	}
	public void setCantConsumida(String cantConsumida) {
		this.cantConsumida = cantConsumida;
	}
	public String getPrecioPromedio() {
		return precioPromedio;
	}
	public void setPrecioPromedio(String precioPromedio) {
		this.precioPromedio = precioPromedio;
	}
	public String getDscTipoCotizacion() {
		return dscTipoCotizacion;
	}
	public void setDscTipoCotizacion(String dscTipoCotizacion) {
		this.dscTipoCotizacion = dscTipoCotizacion;
	}
	public String getDscTipoPago() {
		return dscTipoPago;
	}
	public void setDscTipoPago(String dscTipoPago) {
		this.dscTipoPago = dscTipoPago;
	}
	public String getNroCotizacion() {
		return nroCotizacion;
	}
	public void setNroCotizacion(String nroCotizacion) {
		this.nroCotizacion = nroCotizacion;
	}
	public String getFecEnvioCotizacion() {
		return fecEnvioCotizacion;
	}
	public void setFecEnvioCotizacion(String fecEnvioCotizacion) {
		this.fecEnvioCotizacion = fecEnvioCotizacion;
	}
	public String getFecInicioVigencia() {
		return fecInicioVigencia;
	}
	public void setFecInicioVigencia(String fecInicioVigencia) {
		this.fecInicioVigencia = fecInicioVigencia;
	}
	public String getFecFinVigencia() {
		return fecFinVigencia;
	}
	public void setFecFinVigencia(String fecFinVigencia) {
		this.fecFinVigencia = fecFinVigencia;
	}
	public String getDscEstadoCotizacion() {
		return dscEstadoCotizacion;
	}
	public void setDscEstadoCotizacion(String dscEstadoCotizacion) {
		this.dscEstadoCotizacion = dscEstadoCotizacion;
	}
	public String getDscBienServicio() {
		return dscBienServicio;
	}
	public void setDscBienServicio(String dscBienServicio) {
		this.dscBienServicio = dscBienServicio;
	}
	public String getCotiGanador() {
		return cotiGanador;
	}
	public void setCotiGanador(String cotiGanador) {
		this.cotiGanador = cotiGanador;
	}


	public String getDscProveedorGanador() {
		return dscProveedorGanador;
	}
	public void setDscProveedorGanador(String dscProveedorGanador) {
		this.dscProveedorGanador = dscProveedorGanador;
	}
	public String getListadoCotizaciones() {
		return listadoCotizaciones;
	}
	public void setListadoCotizaciones(String listadoCotizaciones) {
		this.listadoCotizaciones = listadoCotizaciones;
	}
	public String getCantRequerimiento() {
		return cantRequerimiento;
	}
	public void setCantRequerimiento(String cantRequerimiento) {
		this.cantRequerimiento = cantRequerimiento;
	}
	public String getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public String getDscImporteProrrogaReq() {
		return dscImporteProrrogaReq;
	}
	public void setDscImporteProrrogaReq(String dscImporteProrrogaReq) {
		this.dscImporteProrrogaReq = dscImporteProrrogaReq;
	}
	public String getRucProveedor() {
		return rucProveedor;
	}
	public void setRucProveedor(String rucProveedor) {
		this.rucProveedor = rucProveedor;
	}
	
	public String getDscRazonSocial() {
		return dscRazonSocial;
	}
	public void setDscRazonSocial(String dscRazonSocial) {
		this.dscRazonSocial = dscRazonSocial;
	}
	public String getImporteBienServ() {
		return importeBienServ;
	}
	public void setImporteBienServ(String importeBienServ) {
		this.importeBienServ = importeBienServ;
	}
	public String getDscTipoOrden() {
		return dscTipoOrden;
	}
	public void setDscTipoOrden(String dscTipoOrden) {
		this.dscTipoOrden = dscTipoOrden;
	}
	public String getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public String getIndiceLocal() {
		return indiceLocal;
	}
	public void setIndiceLocal(String indiceLocal) {
		this.indiceLocal = indiceLocal;
	}
	public String getDscCondicionFinal1() {
		return dscCondicionFinal1;
	}
	public void setDscCondicionFinal1(String dscCondicionFinal1) {
		this.dscCondicionFinal1 = dscCondicionFinal1;
	}
	public String getDscTipoPeriodo() {
		return dscTipoPeriodo;
	}
	public void setDscTipoPeriodo(String dscTipoPeriodo) {
		this.dscTipoPeriodo = dscTipoPeriodo;
	}
	public String getNroComprobante() {
		return nroComprobante;
	}
	public void setNroComprobante(String nroComprobante) {
		this.nroComprobante = nroComprobante;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getDscEstDocPago() {
		return dscEstDocPago;
	}
	public void setDscEstDocPago(String dscEstDocPago) {
		this.dscEstDocPago = dscEstDocPago;
	}
	/**
	 * @return the nomSolicitante
	 */
	public String getNomSolicitante() {
		return nomSolicitante;
	}
	/**
	 * @param nomSolicitante the nomSolicitante to set
	 */
	public void setNomSolicitante(String nomSolicitante) {
		this.nomSolicitante = nomSolicitante;
	}

}
