package com.tecsup.SGA.modelo;

import java.io.Serializable;

public class ProcesoAreasEstudios implements Serializable {
	
	private static final long serialVersionUID = -678226702065572047L;

	public ProcesoAreasEstudios(){}
	
	private String id;
	private String codArea;
	private String descripcionArea;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodArea() {
		return codArea;
	}
	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}
	public String getDescripcionArea() {
		return descripcionArea;
	}
	public void setDescripcionArea(String descripcionArea) {
		this.descripcionArea = descripcionArea;
	}
	
	
}
