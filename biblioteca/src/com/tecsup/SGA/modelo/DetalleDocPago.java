package com.tecsup.SGA.modelo;

public class DetalleDocPago {
	
	private String codCotizacion;
	private String codProveedor;
	private String codDetCotProv;
	private String codReq;
	private String codReqDet;
	private String codItem;
	private String nomItem;
	private String cantidad;
	private String preUni;
	private String subTotal;
	private String observacion;
	private String cantFacturada;
	private String dscSubTotal;
	private String unidadMedida; //jhpr 2008-09-25
	
	/**
	 * @return the unidadMedida
	 */
	public String getUnidadMedida() {
		return unidadMedida;
	}
	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getCantFacturada() {
		return cantFacturada;
	}
	public void setCantFacturada(String cantFacturada) {
		this.cantFacturada = cantFacturada;
	}
	public String getCodCotizacion() {
		return codCotizacion;
	}
	public void setCodCotizacion(String codCotizacion) {
		this.codCotizacion = codCotizacion;
	}
	public String getCodProveedor() {
		return codProveedor;
	}
	public void setCodProveedor(String codProveedor) {
		this.codProveedor = codProveedor;
	}
	public String getCodDetCotProv() {
		return codDetCotProv;
	}
	public void setCodDetCotProv(String codDetCotProv) {
		this.codDetCotProv = codDetCotProv;
	}
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getCodReqDet() {
		return codReqDet;
	}
	public void setCodReqDet(String codReqDet) {
		this.codReqDet = codReqDet;
	}
	public String getCodItem() {
		return codItem;
	}
	public void setCodItem(String codItem) {
		this.codItem = codItem;
	}
	public String getNomItem() {
		return nomItem;
	}
	public void setNomItem(String nomItem) {
		this.nomItem = nomItem;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getPreUni() {
		return preUni;
	}
	public void setPreUni(String preUni) {
		this.preUni = preUni;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getDscSubTotal() {
		return dscSubTotal;
	}
	public void setDscSubTotal(String dscSubTotal) {
		this.dscSubTotal = dscSubTotal;
	}
	
}
