package com.tecsup.SGA.modelo;

public class Reportes {

	private String dscPeriodo;
	private String codAlumno;
	private String nomAlumno;
	private String nivelCurso;
	private String especialidadCurso;
	private String secciones;
	private String nomCurso;
	private String codSeccion;
	private String codCurso;
	private String tipo;
	private String nroVez;
	private String sisEval;
	private String detSisEval;
	private String notasTeo;
	private String notasTal;
	private String notasLab;
	private String promTeo;
	private String promTal;
	private String promLab;
	private String exaParcial;
	private String exaFinal;
	private String exaCargo;
	private String proInansistencia;
	private String promFinal;
	private String exaRecu;
	private String promRecu;
	private String nroPracTeo;
	private String nroPracTal;
	private String nroPracLab;
	private String aisTeo;
	private String asisTal;
	private String asisLab;
	private String horasTeo;
	private String hoarsTal;
	private String horasLab;
	
	private String codEvaluadorTeo; //JHPR:2008-05-26
	private String dscEvaluadorTeo;
	private String codEvaluadorTall;
	private String dscEvaluadorTall;
	private String codEvaluadorLab;
	private String dscEvaluadorLab;
	
	private String ntaExt;
	private String ntaTecsup;
	private String estFinal;
	
	private String fecha;
	private String aprobados;
	private String desaprobados;
	private String anulados;
	private String nroPresentados;
	private String matriculados;
	private String promedio;
	private String codCiclo;
	private String curso;
	
	private String nivelAlumno;
	private String nroCursos;
	private String nroExamCargo;
	private String promSemestral;
	private String promAcumulado;
	private String indRecuperacion;
	private String ordMerito;
	private String indQuinto;
	private String estInicial;
	private String pce;
	private String cursosCargo;
	private String especialidadAlumno;
	private String detalle;
	private String exam1;
	private String exam2;
	private String notaCargo;
	private String subRecu;
	
	public String getSubRecu() {
		return subRecu;
	}
	public void setSubRecu(String subRecu) {
		this.subRecu = subRecu;
	}
	public String getExam1() {
		return exam1;
	}
	public void setExam1(String exam1) {
		this.exam1 = exam1;
	}
	public String getExam2() {
		return exam2;
	}
	public void setExam2(String exam2) {
		this.exam2 = exam2;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getEspecialidadAlumno() {
		return especialidadAlumno;
	}
	public void setEspecialidadAlumno(String especialidadAlumno) {
		this.especialidadAlumno = especialidadAlumno;
	}
	public String getNivelAlumno() {
		return nivelAlumno;
	}
	public void setNivelAlumno(String nivelAlumno) {
		this.nivelAlumno = nivelAlumno;
	}
	public String getNroCursos() {
		return nroCursos;
	}
	public void setNroCursos(String nroCursos) {
		this.nroCursos = nroCursos;
	}
	public String getNroExamCargo() {
		return nroExamCargo;
	}
	public void setNroExamCargo(String nroExamCargo) {
		this.nroExamCargo = nroExamCargo;
	}
	public String getPromSemestral() {
		return promSemestral;
	}
	public void setPromSemestral(String promSemestral) {
		this.promSemestral = promSemestral;
	}
	public String getPromAcumulado() {
		return promAcumulado;
	}
	public void setPromAcumulado(String promAcumulado) {
		this.promAcumulado = promAcumulado;
	}
	public String getIndRecuperacion() {
		return indRecuperacion;
	}
	public void setIndRecuperacion(String indRecuperacion) {
		this.indRecuperacion = indRecuperacion;
	}
	public String getOrdMerito() {
		return ordMerito;
	}
	public void setOrdMerito(String ordMerito) {
		this.ordMerito = ordMerito;
	}
	public String getIndQuinto() {
		return indQuinto;
	}
	public void setIndQuinto(String indQuinto) {
		this.indQuinto = indQuinto;
	}
	public String getEstInicial() {
		return estInicial;
	}
	public void setEstInicial(String estInicial) {
		this.estInicial = estInicial;
	}
	public String getPce() {
		return pce;
	}
	public void setPce(String pce) {
		this.pce = pce;
	}
	public String getCursosCargo() {
		return cursosCargo;
	}
	public void setCursosCargo(String cursosCargo) {
		this.cursosCargo = cursosCargo;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getAprobados() {
		return aprobados;
	}
	public void setAprobados(String aprobados) {
		this.aprobados = aprobados;
	}
	public String getDesaprobados() {
		return desaprobados;
	}
	public void setDesaprobados(String desaprobados) {
		this.desaprobados = desaprobados;
	}
	public String getAnulados() {
		return anulados;
	}
	public void setAnulados(String anulados) {
		this.anulados = anulados;
	}
	public String getNroPresentados() {
		return nroPresentados;
	}
	public void setNroPresentados(String nroPresentados) {
		this.nroPresentados = nroPresentados;
	}
	public String getMatriculados() {
		return matriculados;
	}
	public void setMatriculados(String matriculados) {
		this.matriculados = matriculados;
	}
	public String getPromedio() {
		return promedio;
	}
	public void setPromedio(String promedio) {
		this.promedio = promedio;
	}
	public String getNtaExt() {
		return ntaExt;
	}
	public void setNtaExt(String ntaExt) {
		this.ntaExt = ntaExt;
	}
	public String getNtaTecsup() {
		return ntaTecsup;
	}
	public void setNtaTecsup(String ntaTecsup) {
		this.ntaTecsup = ntaTecsup;
	}
	public String getDscPeriodo() {
		return dscPeriodo;
	}
	public void setDscPeriodo(String dscPeriodo) {
		this.dscPeriodo = dscPeriodo;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNivelCurso() {
		return nivelCurso;
	}
	public void setNivelCurso(String nivelCurso) {
		this.nivelCurso = nivelCurso;
	}
	public String getEspecialidadCurso() {
		return especialidadCurso;
	}
	public void setEspecialidadCurso(String especialidadCurso) {
		this.especialidadCurso = especialidadCurso;
	}
	public String getSecciones() {
		return secciones;
	}
	public void setSecciones(String secciones) {
		this.secciones = secciones;
	}
	public String getNomCurso() {
		return nomCurso;
	}
	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNroVez() {
		return nroVez;
	}
	public void setNroVez(String nroVez) {
		this.nroVez = nroVez;
	}
	public String getSisEval() {
		return sisEval;
	}
	public void setSisEval(String sisEval) {
		this.sisEval = sisEval;
	}
	public String getDetSisEval() {
		return detSisEval;
	}
	public void setDetSisEval(String detSisEval) {
		this.detSisEval = detSisEval;
	}
	public String getNotasTeo() {
		return notasTeo;
	}
	public void setNotasTeo(String notasTeo) {
		this.notasTeo = notasTeo;
	}
	public String getNotasTal() {
		return notasTal;
	}
	public void setNotasTal(String notasTal) {
		this.notasTal = notasTal;
	}
	public String getNotasLab() {
		return notasLab;
	}
	public void setNotasLab(String notasLab) {
		this.notasLab = notasLab;
	}
	public String getPromTeo() {
		return promTeo;
	}
	public void setPromTeo(String promTeo) {
		this.promTeo = promTeo;
	}
	public String getPromTal() {
		return promTal;
	}
	public void setPromTal(String promTal) {
		this.promTal = promTal;
	}
	public String getPromLab() {
		return promLab;
	}
	public void setPromLab(String promLab) {
		this.promLab = promLab;
	}
	public String getExaParcial() {
		return exaParcial;
	}
	public void setExaParcial(String exaParcial) {
		this.exaParcial = exaParcial;
	}
	public String getExaFinal() {
		return exaFinal;
	}
	public void setExaFinal(String exaFinal) {
		this.exaFinal = exaFinal;
	}
	public String getExaCargo() {
		return exaCargo;
	}
	public void setExaCargo(String exaCargo) {
		this.exaCargo = exaCargo;
	}
	public String getProInansistencia() {
		return proInansistencia;
	}
	public void setProInansistencia(String proInansistencia) {
		this.proInansistencia = proInansistencia;
	}
	public String getPromFinal() {
		return promFinal;
	}
	public void setPromFinal(String promFinal) {
		this.promFinal = promFinal;
	}
	public String getExaRecu() {
		return exaRecu;
	}
	public void setExaRecu(String exaRecu) {
		this.exaRecu = exaRecu;
	}
	public String getNroPracTeo() {
		return nroPracTeo;
	}
	public void setNroPracTeo(String nroPracTeo) {
		this.nroPracTeo = nroPracTeo;
	}
	public String getNroPracTal() {
		return nroPracTal;
	}
	public void setNroPracTal(String nroPracTal) {
		this.nroPracTal = nroPracTal;
	}
	public String getNroPracLab() {
		return nroPracLab;
	}
	public void setNroPracLab(String nroPracLab) {
		this.nroPracLab = nroPracLab;
	}
	public String getAisTeo() {
		return aisTeo;
	}
	public void setAisTeo(String aisTeo) {
		this.aisTeo = aisTeo;
	}
	public String getAsisTal() {
		return asisTal;
	}
	public void setAsisTal(String asisTal) {
		this.asisTal = asisTal;
	}
	public String getAsisLab() {
		return asisLab;
	}
	public void setAsisLab(String asisLab) {
		this.asisLab = asisLab;
	}
	public String getHorasTeo() {
		return horasTeo;
	}
	public void setHorasTeo(String horasTeo) {
		this.horasTeo = horasTeo;
	}
	public String getHoarsTal() {
		return hoarsTal;
	}
	public void setHoarsTal(String hoarsTal) {
		this.hoarsTal = hoarsTal;
	}
	public String getHorasLab() {
		return horasLab;
	}
	public void setHorasLab(String horasLab) {
		this.horasLab = horasLab;
	}
	public String getNomAlumno() {
		return nomAlumno;
	}
	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	public String getPromRecu() {
		return promRecu;
	}
	public void setPromRecu(String promRecu) {
		this.promRecu = promRecu;
	}
	public String getEstFinal() {
		return estFinal;
	}
	public void setEstFinal(String estFinal) {
		this.estFinal = estFinal;
	}
	public String getNotaCargo() {
		return notaCargo;
	}
	public void setNotaCargo(String notaCargo) {
		this.notaCargo = notaCargo;
	}
	public String getCodEvaluadorTeo() {
		return codEvaluadorTeo;
	}
	public void setCodEvaluadorTeo(String codEvaluadorTeo) {
		this.codEvaluadorTeo = codEvaluadorTeo;
	}
	public String getDscEvaluadorTeo() {
		return dscEvaluadorTeo;
	}
	public void setDscEvaluadorTeo(String dscEvaluadorTeo) {
		this.dscEvaluadorTeo = dscEvaluadorTeo;
	}
	public String getCodEvaluadorTall() {
		return codEvaluadorTall;
	}
	public void setCodEvaluadorTall(String codEvaluadorTall) {
		this.codEvaluadorTall = codEvaluadorTall;
	}
	public String getDscEvaluadorTall() {
		return dscEvaluadorTall;
	}
	public void setDscEvaluadorTall(String dscEvaluadorTall) {
		this.dscEvaluadorTall = dscEvaluadorTall;
	}
	public String getCodEvaluadorLab() {
		return codEvaluadorLab;
	}
	public void setCodEvaluadorLab(String codEvaluadorLab) {
		this.codEvaluadorLab = codEvaluadorLab;
	}
	public String getDscEvaluadorLab() {
		return dscEvaluadorLab;
	}
	public void setDscEvaluadorLab(String dscEvaluadorLab) {
		this.dscEvaluadorLab = dscEvaluadorLab;
	}
	
	
	
}
