package com.tecsup.SGA.modelo;

public class DevolucionDetalle {	
	private String codDevolucionDetalle;
	private String codBien;
	private String dscBien;
	private String dscUnidad;
	private String cantidad;
	private String codRequerimientoDetalle;
	private String indiceNumeroSerie;
	
	public String getCodDevolucionDetalle() {
		return codDevolucionDetalle;
	}
	public void setCodDevolucionDetalle(String codDevolucionDetalle) {
		this.codDevolucionDetalle = codDevolucionDetalle;
	}
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getDscBien() {
		return dscBien;
	}
	public void setDscBien(String dscBien) {
		this.dscBien = dscBien;
	}	
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getDscUnidad() {
		return dscUnidad;
	}
	public void setDscUnidad(String dscUnidad) {
		this.dscUnidad = dscUnidad;
	}
	public String getCodRequerimientoDetalle() {
		return codRequerimientoDetalle;
	}
	public void setCodRequerimientoDetalle(String codRequerimientoDetalle) {
		this.codRequerimientoDetalle = codRequerimientoDetalle;
	}
	public String getIndiceNumeroSerie() {
		return indiceNumeroSerie;
	}
	public void setIndiceNumeroSerie(String indiceNumeroSerie) {
		this.indiceNumeroSerie = indiceNumeroSerie;
	}
	
}
