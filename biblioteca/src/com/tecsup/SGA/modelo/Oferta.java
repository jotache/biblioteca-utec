package com.tecsup.SGA.modelo;

public class Oferta implements java.io.Serializable {
	/*Atributos*/
	private String codOferta;
	private String codProceso;
	private String denominacion;
	private String vacantes;
	private String descripcion;
	private String fecIniPub;
	private String fecFinPub;
	
	/*Getters y Setters*/
	public String getCodOferta() {
		return codOferta;
	}
	public void setCodOferta(String codOferta) {
		this.codOferta = codOferta;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getVacantes() {
		return vacantes;
	}
	public void setVacantes(String vacantes) {
		this.vacantes = vacantes;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFecIniPub() {
		return fecIniPub;
	}
	public void setFecIniPub(String fecIniPub) {
		this.fecIniPub = fecIniPub;
	}
	public String getFecFinPub() {
		return fecFinPub;
	}
	public void setFecFinPub(String fecFinPub) {
		this.fecFinPub = fecFinPub;
	}
	
	/*CONSTRUCTORES*/
	public Oferta()
	{
		this.codOferta = "";
		this.codProceso = "";
		this.denominacion = "";
		this.vacantes = "";
		this.descripcion = "";
		this.fecIniPub = "";
		this.fecFinPub = "";
	}
}
