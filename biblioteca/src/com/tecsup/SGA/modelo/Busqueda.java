package com.tecsup.SGA.modelo;

public class Busqueda implements java.io.Serializable{
	
	private String codUnico;
	private String titulo;
	private String tipoMaterial;
	private String descripDewey;
	private String nroEjemplares;
	private String prestaSala;
	private String prestaDomicilio;
	private String disponibilidad;
	private String reserva;
	private String texto;
	private String buscarBy;
	private String ordenarBy;
	private String flagReserva;
	
	private String descripAutor;
	private String imagen;
	private String fechaBD;
	private String isbn;
	
	private String editorial;
	private String usuarioReserva;
	private String nomUsuario;
	
	/* *********** busqueda Avanzada ********* */
	
	private int buscar1;
	private String txtTexto1;
	private int condicion1;
	private int buscar2;
	private String txtTexto2;
	private int condicion2;
	private int buscar3;
	private String txtTexto3;
	private String codTipoIdioma;
	private String codAnioInicio;
	private String codAnioFin;
	private String flagDisponibilidadSala;
	private String flagDisponibilidadDomicilio;
	private int orderBy;
	
	private String dscSede;
	
	private String codMaterial;
	private String codigoGenerado;
	private String ubicacionFisica;
	private String codSede;
	
	public String getCodMaterial() {
		return codMaterial;
	}
	public void setCodMaterial(String codMaterial) {
		this.codMaterial = codMaterial;
	}
	public String getDscSede() {
		return dscSede;
	}
	public void setDscSede(String dscSede) {
		this.dscSede = dscSede;
	}
	public int getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getTipoMaterial() {
		return tipoMaterial;
	}
	public void setTipoMaterial(String tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}
	public String getDescripDewey() {
		return descripDewey;
	}
	public void setDescripDewey(String descripDewey) {
		this.descripDewey = descripDewey;
	}
	public String getNroEjemplares() {
		return nroEjemplares;
	}
	public void setNroEjemplares(String nroEjemplares) {
		this.nroEjemplares = nroEjemplares;
	}
	public String getPrestaSala() {
		return prestaSala;
	}
	public void setPrestaSala(String prestaSala) {
		this.prestaSala = prestaSala;
	}
	public String getPrestaDomicilio() {
		return prestaDomicilio;
	}
	public void setPrestaDomicilio(String prestaDomicilio) {
		this.prestaDomicilio = prestaDomicilio;
	}
	public String getDisponibilidad() {
		return disponibilidad;
	}
	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
	public String getReserva() {
		return reserva;
	}
	public void setReserva(String reserva) {
		this.reserva = reserva;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getBuscarBy() {
		return buscarBy;
	}
	public void setBuscarBy(String buscarBy) {
		this.buscarBy = buscarBy;
	}
	public String getOrdenarBy() {
		return ordenarBy;
	}
	public void setOrdenarBy(String ordenarBy) {
		this.ordenarBy = ordenarBy;
	}
	public String getFlagReserva() {
		return flagReserva;
	}
	public void setFlagReserva(String flagReserva) {
		this.flagReserva = flagReserva;
	}
	public String getDescripAutor() {
		return descripAutor;
	}
	public void setDescripAutor(String descripAutor) {
		this.descripAutor = descripAutor;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getFechaBD() {
		return fechaBD;
	}
	public void setFechaBD(String fechaBD) {
		this.fechaBD = fechaBD;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getBuscar1() {
		return buscar1;
	}
	public void setBuscar1(int buscar1) {
		this.buscar1 = buscar1;
	}
	public String getTxtTexto1() {
		return txtTexto1;
	}
	public void setTxtTexto1(String txtTexto1) {
		this.txtTexto1 = txtTexto1;
	}
	public int getCondicion1() {
		return condicion1;
	}
	public void setCondicion1(int condicion1) {
		this.condicion1 = condicion1;
	}
	public int getBuscar2() {
		return buscar2;
	}
	public void setBuscar2(int buscar2) {
		this.buscar2 = buscar2;
	}
	public String getTxtTexto2() {
		return txtTexto2;
	}
	public void setTxtTexto2(String txtTexto2) {
		this.txtTexto2 = txtTexto2;
	}
	public int getCondicion2() {
		return condicion2;
	}
	public void setCondicion2(int condicion2) {
		this.condicion2 = condicion2;
	}
	public int getBuscar3() {
		return buscar3;
	}
	public void setBuscar3(int buscar3) {
		this.buscar3 = buscar3;
	}
	public String getTxtTexto3() {
		return txtTexto3;
	}
	public void setTxtTexto3(String txtTexto3) {
		this.txtTexto3 = txtTexto3;
	}
	public String getCodTipoIdioma() {
		return codTipoIdioma;
	}
	public void setCodTipoIdioma(String codTipoIdioma) {
		this.codTipoIdioma = codTipoIdioma;
	}
	public String getCodAnioInicio() {
		return codAnioInicio;
	}
	public void setCodAnioInicio(String codAnioInicio) {
		this.codAnioInicio = codAnioInicio;
	}
	public String getCodAnioFin() {
		return codAnioFin;
	}
	public void setCodAnioFin(String codAnioFin) {
		this.codAnioFin = codAnioFin;
	}
	public String getFlagDisponibilidadSala() {
		return flagDisponibilidadSala;
	}
	public void setFlagDisponibilidadSala(String flagDisponibilidadSala) {
		this.flagDisponibilidadSala = flagDisponibilidadSala;
	}
	public String getFlagDisponibilidadDomicilio() {
		return flagDisponibilidadDomicilio;
	}
	public void setFlagDisponibilidadDomicilio(String flagDisponibilidadDomicilio) {
		this.flagDisponibilidadDomicilio = flagDisponibilidadDomicilio;
	}
	
	public String getEditorial() {
		return editorial;
	}
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}
	public String getCodigoGenerado() {
		return codigoGenerado;
	}
	public void setCodigoGenerado(String codigoGenerado) {
		this.codigoGenerado = codigoGenerado;
	}
	public String getUbicacionFisica() {
		return ubicacionFisica;
	}
	public void setUbicacionFisica(String ubicacionFisica) {
		this.ubicacionFisica = ubicacionFisica;
	}
	public String getUsuarioReserva() {
		return usuarioReserva;
	}
	public void setUsuarioReserva(String usuarioReserva) {
		this.usuarioReserva = usuarioReserva;
	}
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}	

}
