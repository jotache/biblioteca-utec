package com.tecsup.SGA.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tecsup.SGA.bean.OpcionesApoyo;

public class UsuarioPM implements Serializable{

    private static final long serialVersionUID = 4989819839855394566L;

    private String codUsuario;
    private Integer codSujeto;
    private String nomUsuario;
    private List<AccesoIP> listaAcceso = new ArrayList<AccesoIP>();
    private List<OpcionesApoyo> listaOpciones = new ArrayList<OpcionesApoyo>();

    public String getCodUsuario() {
        return codUsuario;
    }
    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }
    public Integer getCodSujeto() {
        return codSujeto;
    }
    public void setCodSujeto(Integer codSujeto) {
        this.codSujeto = codSujeto;
    }
    public String getNomUsuario() {
        return nomUsuario;
    }
    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }
    public List<AccesoIP> getListaAcceso() {
        return listaAcceso;
    }
    public void setListaAcceso(List<AccesoIP> listaAcceso) {
        this.listaAcceso = listaAcceso;
    }
    @Override
    public String toString() {
        StringBuffer string = new StringBuffer();
        string.append("USUARIO:"+codUsuario+"\t");
        string.append("SUJETO:"+codSujeto+"\t");
        string.append("NOMBRE:"+nomUsuario+"\n");
        for (AccesoIP acceso : listaAcceso) {
            string.append("IP:"+acceso+"\n");
        }
        for(OpcionesApoyo opcion: listaOpciones){
            string.append("Opcion:" + opcion + "\n");
        }
        return string.toString();
    }
    public List<OpcionesApoyo> getListaOpciones() {
        return listaOpciones;
    }
    public void setListaOpciones(List<OpcionesApoyo> listaOpciones) {
        this.listaOpciones = listaOpciones;
    }

}
