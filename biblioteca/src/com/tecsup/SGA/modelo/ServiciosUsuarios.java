package com.tecsup.SGA.modelo;

import java.util.List;

public class ServiciosUsuarios {
	private String codSecuencial;
	private String tipoUser;
	private String prestamoSala;
	private String prestamoDomicilio;
	private String nroPrestamoSala;
	private String nroPrestamoInternet;
	private String nroPrestamoMant;
	private String diasPrestamo;
	private String sancion;
	private String reserva;
	private String cantOcurrencias;
	private String nroProrrogas;
	private List<TipoTablaDetalle> listaMaxDiasPrestamo;
	
	public String getNroProrrogas() {
		return nroProrrogas;
	}
	public void setNroProrrogas(String nroProrrogas) {
		this.nroProrrogas = nroProrrogas;
	}
	public String getCodSecuencial() {
		return codSecuencial;
	}
	public void setCodSecuencial(String codSecuencial) {
		this.codSecuencial = codSecuencial;
	}
	public String getTipoUser() {
		return tipoUser;
	}
	public void setTipoUser(String tipoUser) {
		this.tipoUser = tipoUser;
	}
	public String getPrestamoSala() {
		return prestamoSala;
	}
	public void setPrestamoSala(String prestamoSala) {
		this.prestamoSala = prestamoSala;
	}
	public String getPrestamoDomicilio() {
		return prestamoDomicilio;
	}
	public void setPrestamoDomicilio(String prestamoDomicilio) {
		this.prestamoDomicilio = prestamoDomicilio;
	}
	public String getNroPrestamoSala() {
		return nroPrestamoSala;
	}
	public void setNroPrestamoSala(String nroPrestamoSala) {
		this.nroPrestamoSala = nroPrestamoSala;
	}
	public String getNroPrestamoInternet() {
		return nroPrestamoInternet;
	}
	public void setNroPrestamoInternet(String nroPrestamoInternet) {
		this.nroPrestamoInternet = nroPrestamoInternet;
	}
	public String getNroPrestamoMant() {
		return nroPrestamoMant;
	}
	public void setNroPrestamoMant(String nroPrestamoMant) {
		this.nroPrestamoMant = nroPrestamoMant;
	}
	public String getDiasPrestamo() {
		return diasPrestamo;
	}
	public void setDiasPrestamo(String diasPrestamo) {
		this.diasPrestamo = diasPrestamo;
	}
	public String getSancion() {
		return sancion;
	}
	public void setSancion(String sancion) {
		this.sancion = sancion;
	}
	public String getReserva() {
		return reserva;
	}
	public void setReserva(String reserva) {
		this.reserva = reserva;
	}
	public String getCantOcurrencias() {
		return cantOcurrencias;
	}
	public void setCantOcurrencias(String cantOcurrencias) {
		this.cantOcurrencias = cantOcurrencias;
	}
	public List<TipoTablaDetalle> getListaMaxDiasPrestamo() {
		return listaMaxDiasPrestamo;
	}
	public void setListaMaxDiasPrestamo(List<TipoTablaDetalle> listaMaxDiasPrestamo) {
		this.listaMaxDiasPrestamo = listaMaxDiasPrestamo;
	}
}
