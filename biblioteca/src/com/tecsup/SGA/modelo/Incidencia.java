package com.tecsup.SGA.modelo;

public class Incidencia implements java.io.Serializable{
	private String codIincidencia;
	private String codAlumno;
	private String codCurso;
	private String codTipoIncidencia;
	private String dscTipoIncidencia;
	private String indTipoOpe;
	private String dscIncidencia;
	private String fecha;
	private String fechaIni;
	private String fechaFin;
	private String codUsuario;
	private String dscUsuario;
	
	public String getCodIincidencia() {
		return codIincidencia;
	}
	public void setCodIincidencia(String codIincidencia) {
		this.codIincidencia = codIincidencia;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getDscIncidencia() {
		return dscIncidencia;
	}
	public void setDscIncidencia(String dscIncidencia) {
		this.dscIncidencia = dscIncidencia;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getIndTipo() {
		return indTipoOpe;
	}
	public void setIndTipo(String indTipo) {
		this.indTipoOpe = indTipo;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getDscUsuario() {
		return dscUsuario;
	}
	public void setDscUsuario(String dscUsuario) {
		this.dscUsuario = dscUsuario;
	}
	public String getCodTipoIncidencia() {
		return codTipoIncidencia;
	}
	public void setCodTipoIncidencia(String codTipoIncidencia) {
		this.codTipoIncidencia = codTipoIncidencia;
	}
	public String getIndTipoOpe() {
		return indTipoOpe;
	}
	public void setIndTipoOpe(String indTipoOpe) {
		this.indTipoOpe = indTipoOpe;
	}
	public String getDscTipoIncidencia() {
		return dscTipoIncidencia;
	}
	public void setDscTipoIncidencia(String dscTipoIncidencia) {
		this.dscTipoIncidencia = dscTipoIncidencia;
	}
	
}
