package com.tecsup.SGA.modelo;

public class TipoProveedor {
	private	String codTipoCalif;
	private	String codTipoProv;
	private	String tipoDoc;
	private	String nroDoc;
	private	String razSoc;
	private	String razSocCorto;
	private	String email;
	private	String direc;
	private	String codDpto;
	private	String codProv;
	private	String codDsto;
	private	String atencion;
	private	String codGiro;
	private	String codEstado;
	private	String banco;
	private	String tipCta;
	private	String nroCta;
	private	String codMon;
	private	String usuario;
	//ALQD,03/07/09.NUEVAS PROPIEDADES
	private	String pais;
	private	String telefono;
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCodTipoCalif() {
		return codTipoCalif;
	}
	public void setCodTipoCalif(String codTipoCalif) {
		this.codTipoCalif = codTipoCalif;
	}
	public String getCodTipoProv() {
		return codTipoProv;
	}
	public void setCodTipoProv(String codTipoProv) {
		this.codTipoProv = codTipoProv;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getNroDoc() {
		return nroDoc;
	}
	public void setNroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
	}
	public String getRazSoc() {
		return razSoc;
	}
	public void setRazSoc(String razSoc) {
		this.razSoc = razSoc;
	}
	public String getRazSocCorto() {
		return razSocCorto;
	}
	public void setRazSocCorto(String razSocCorto) {
		this.razSocCorto = razSocCorto;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDirec() {
		return direc;
	}
	public void setDirec(String direc) {
		this.direc = direc;
	}
	public String getCodDpto() {
		return codDpto;
	}
	public void setCodDpto(String codDpto) {
		this.codDpto = codDpto;
	}
	public String getCodProv() {
		return codProv;
	}
	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}
	public String getCodDsto() {
		return codDsto;
	}
	public void setCodDsto(String codDsto) {
		this.codDsto = codDsto;
	}
	public String getAtencion() {
		return atencion;
	}
	public void setAtencion(String atencion) {
		this.atencion = atencion;
	}
	public String getCodGiro() {
		return codGiro;
	}
	public void setCodGiro(String codGiro) {
		this.codGiro = codGiro;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getTipCta() {
		return tipCta;
	}
	public void setTipCta(String tipCta) {
		this.tipCta = tipCta;
	}
	public String getNroCta() {
		return nroCta;
	}
	public void setNroCta(String nroCta) {
		this.nroCta = nroCta;
	}
	public String getCodMon() {
		return codMon;
	}
	public void setCodMon(String codMon) {
		this.codMon = codMon;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}
