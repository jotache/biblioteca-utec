package com.tecsup.SGA.modelo;

public class CatalogoProducto implements java.io.Serializable{
	
	private String codProducto;
	private String codFamilia;
	private String codSubFamilia;
	private String nomProducto;
	private String nomUnidad;
	private String stockActual;
	private String stockDisponible;
	private String precio;
	private String condicion;
	private String descripcion;
	private String unidadMedida;
	private String tipoBien;
	private String codUbicacion;
	private String ubicacionFila;
	private String ubicacionColum;
	private String stockMin;
	private String stockMax;
	private String stockDiso;
	private String stockAct;
	private String preUnitario;
	private String nroDiasAtencion;
	private String marca;
	private String modelo;
	private String rutaImagen;
	private String familia;
	private String subFamilia;
	private String precioReferencial;
	private String inversion;
	private String stockMostrar;
	private String estado;
	//ALQD,23/02/09.NUEVA PROPIEDAD
	private String artPromocional;
	private String desEstado;
	//ALQD,20/05/09.NUEVA PROPIEDAD
	private String artNoAfecto;
	
	public String getArtNoAfecto() {
		return artNoAfecto;
	}
	public void setArtNoAfecto(String artNoAfecto) {
		this.artNoAfecto=artNoAfecto;
	}
	public String getDesEstado() {
		return desEstado;
	}
	public void setDesEstado(String desEstado) {
		this.desEstado=desEstado;
	}
	public String getArtPromocional() {
		return artPromocional;
	}
	public void setArtPromocional(String artPromocional) {
		this.artPromocional=artPromocional;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getInversion() {
		return inversion;
	}
	public void setInversion(String inversion) {
		this.inversion = inversion;
	}
	public String getStockMostrar() {
		return stockMostrar;
	}
	public void setStockMostrar(String stockMostrar) {
		this.stockMostrar = stockMostrar;
	}
	public String getPrecioReferencial() {
		return precioReferencial;
	}
	public void setPrecioReferencial(String precioReferencial) {
		this.precioReferencial = precioReferencial;
	}
	public String getFamilia() {
		return familia;
	}
	public void setFamilia(String familia) {
		this.familia = familia;
	}
	public String getSubFamilia() {
		return subFamilia;
	}
	public void setSubFamilia(String subFamilia) {
		this.subFamilia = subFamilia;
	}
	public String getRutaImagen() {
		return rutaImagen;
	}
	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getCodUbicacion() {
		return codUbicacion;
	}
	public void setCodUbicacion(String codUbicacion) {
		this.codUbicacion = codUbicacion;
	}
	public String getUbicacionFila() {
		return ubicacionFila;
	}
	public void setUbicacionFila(String ubicacionFila) {
		this.ubicacionFila = ubicacionFila;
	}
	public String getUbicacionColum() {
		return ubicacionColum;
	}
	public void setUbicacionColum(String ubicacionColum) {
		this.ubicacionColum = ubicacionColum;
	}
	public String getStockMin() {
		return stockMin;
	}
	public void setStockMin(String stockMin) {
		this.stockMin = stockMin;
	}
	public String getStockMax() {
		return stockMax;
	}
	public void setStockMax(String stockMax) {
		this.stockMax = stockMax;
	}
	public String getStockDiso() {
		return stockDiso;
	}
	public void setStockDiso(String stockDiso) {
		this.stockDiso = stockDiso;
	}
	public String getStockAct() {
		return stockAct;
	}
	public void setStockAct(String stockAct) {
		this.stockAct = stockAct;
	}
	public String getPreUnitario() {
		return preUnitario;
	}
	public void setPreUnitario(String preUnitario) {
		this.preUnitario = preUnitario;
	}
	public String getNroDiasAtencion() {
		return nroDiasAtencion;
	}
	public void setNroDiasAtencion(String nroDiasAtencion) {
		this.nroDiasAtencion = nroDiasAtencion;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public String getCodSubFamilia() {
		return codSubFamilia;
	}
	public void setCodSubFamilia(String codSubFamilia) {
		this.codSubFamilia = codSubFamilia;
	}
	public String getNomProducto() {
		return nomProducto;
	}
	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}
	public String getNomUnidad() {
		return nomUnidad;
	}
	public void setNomUnidad(String nomUnidad) {
		this.nomUnidad = nomUnidad;
	}
	public String getStockActual() {
		return stockActual;
	}
	public void setStockActual(String stockActual) {
		this.stockActual = stockActual;
	}
	public String getStockDisponible() {
		return stockDisponible;
	}
	public void setStockDisponible(String stockDisponible) {
		this.stockDisponible = stockDisponible;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	
}
