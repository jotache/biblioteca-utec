package com.tecsup.SGA.modelo;

public class Especialidad implements java.io.Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String codEspecialidad;
private String codProducto;
private String descripcion;
private String situRegistro;

public String getCodEspecialidad() {
	return codEspecialidad;
}
public void setCodEspecialidad(String codEspecialidad) {
	this.codEspecialidad = codEspecialidad;
}
public String getCodProducto() {
	return codProducto;
}
public void setCodProducto(String codProducto) {
	this.codProducto = codProducto;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getSituRegistro() {
	return situRegistro;
}
public void setSituRegistro(String situRegistro) {
	this.situRegistro = situRegistro;
}
}
