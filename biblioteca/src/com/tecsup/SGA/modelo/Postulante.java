package com.tecsup.SGA.modelo;

import java.util.List;

public class Postulante implements java.io.Serializable{
	private String codPostulante;
	/*Datos Personales*/
	private String nombres;
	private String apepat;
	private String apemat;
	private String fecreg;
	private String sexo;
	private String nacionalidad;
	private String ruc;
	private String anio_exp_laboral;
	
	private String estado_civil;
	private List lista_estado_civil;
	
	private String dni;
	private String email; //Email principal
	private String email1; //Email
	private String clave; //clave
	private String clave2; //clave confirmación
	
	/*Domicilio*/
	private String domicilio;
	
	private String departamento;
	private List lista_departamento;
	
	private String provincia;
	private List lista_provincia;
	
	private String distrito;
	private List lista_distrito;
	
	private String pais; //pais de residencia
	private String telef1;
	private String telef2;
	private String telef_celular;
	private String cod_postal;
	
	/*Información Laboral*/
	private String area_interes;
	private List lista_area_interes;
	
	private String postulado_antes; //char 1 (Si-No)
	private String trabajado_antes; //char 1 (Si-No)
	private String familia_tecsup; //char 1 (Si-No)
	private String familia_nombres;
	private String exp_docente; //char 1 (Si-No)
	private String exp_docente_anios;
	private String dispo_viaje; //char 1 (Si-No)}
	
	private String sede_pref_trabajo;
	private List lista_sede_pref_trabajo;
	
	private String interes_en;
	private String lista_interes_en;
	
	private String pretension_economica;
	
	private String dispo_trabajar;
	private List lista_dispo_trabajar;
	
	/*Perfil Profesional*/
	private String perfil;

	/*Descripcion de Profesion, GRado de instrucion y Estado*/
	private String profesion;
	private String gradoInstruccion;
	private String codEstado;
	private String dscEstado;
	private String fechaModificacion;
	//private String evaluaciones;
	private List evaluaciones;
	private String ultEstadoPostulRevision;
	private String historico; //cantidad de procesos anteriores a uno especifico en los que se presento el postulante
	
	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public String getCodPostulante() {
		return codPostulante;
	}

	public void setCodPostulante(String codPostulante) {
		this.codPostulante = codPostulante;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApepat() {
		return apepat;
	}

	public void setApepat(String apepat) {
		this.apepat = apepat;
	}

	public String getApemat() {
		return apemat;
	}

	public void setApemat(String apemat) {
		this.apemat = apemat;
	}

	public String getFecreg() {
		return fecreg;
	}

	public void setFecreg(String fecreg) {
		this.fecreg = fecreg;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getAnio_exp_laboral() {
		return anio_exp_laboral;
	}

	public void setAnio_exp_laboral(String anio_exp_laboral) {
		this.anio_exp_laboral = anio_exp_laboral;
	}

	public String getEstado_civil() {
		return estado_civil;
	}

	public void setEstado_civil(String estado_civil) {
		this.estado_civil = estado_civil;
	}

	public List getLista_estado_civil() {
		return lista_estado_civil;
	}

	public void setLista_estado_civil(List lista_estado_civil) {
		this.lista_estado_civil = lista_estado_civil;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getClave2() {
		return clave2;
	}

	public void setClave2(String clave2) {
		this.clave2 = clave2;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public List getLista_departamento() {
		return lista_departamento;
	}

	public void setLista_departamento(List lista_departamento) {
		this.lista_departamento = lista_departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public List getLista_provincia() {
		return lista_provincia;
	}

	public void setLista_provincia(List lista_provincia) {
		this.lista_provincia = lista_provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public List getLista_distrito() {
		return lista_distrito;
	}

	public void setLista_distrito(List lista_distrito) {
		this.lista_distrito = lista_distrito;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getTelef1() {
		return telef1;
	}

	public void setTelef1(String telef1) {
		this.telef1 = telef1;
	}

	public String getTelef2() {
		return telef2;
	}

	public void setTelef2(String telef2) {
		this.telef2 = telef2;
	}

	public String getTelef_celular() {
		return telef_celular;
	}

	public void setTelef_celular(String telef_celular) {
		this.telef_celular = telef_celular;
	}

	public String getCod_postal() {
		return cod_postal;
	}

	public void setCod_postal(String cod_postal) {
		this.cod_postal = cod_postal;
	}

	public String getArea_interes() {
		return area_interes;
	}

	public void setArea_interes(String area_interes) {
		this.area_interes = area_interes;
	}

	public List getLista_area_interes() {
		return lista_area_interes;
	}

	public void setLista_area_interes(List lista_area_interes) {
		this.lista_area_interes = lista_area_interes;
	}

	public String getPostulado_antes() {
		return postulado_antes;
	}

	public void setPostulado_antes(String postulado_antes) {
		this.postulado_antes = postulado_antes;
	}

	public String getTrabajado_antes() {
		return trabajado_antes;
	}

	public void setTrabajado_antes(String trabajado_antes) {
		this.trabajado_antes = trabajado_antes;
	}

	public String getFamilia_tecsup() {
		return familia_tecsup;
	}

	public void setFamilia_tecsup(String familia_tecsup) {
		this.familia_tecsup = familia_tecsup;
	}

	public String getFamilia_nombres() {
		return familia_nombres;
	}

	public void setFamilia_nombres(String familia_nombres) {
		this.familia_nombres = familia_nombres;
	}

	public String getExp_docente() {
		return exp_docente;
	}

	public void setExp_docente(String exp_docente) {
		this.exp_docente = exp_docente;
	}

	public String getExp_docente_anios() {
		return exp_docente_anios;
	}

	public void setExp_docente_anios(String exp_docente_anios) {
		this.exp_docente_anios = exp_docente_anios;
	}

	public String getDispo_viaje() {
		return dispo_viaje;
	}

	public void setDispo_viaje(String dispo_viaje) {
		this.dispo_viaje = dispo_viaje;
	}

	public String getSede_pref_trabajo() {
		return sede_pref_trabajo;
	}

	public void setSede_pref_trabajo(String sede_pref_trabajo) {
		this.sede_pref_trabajo = sede_pref_trabajo;
	}

	public List getLista_sede_pref_trabajo() {
		return lista_sede_pref_trabajo;
	}

	public void setLista_sede_pref_trabajo(List lista_sede_pref_trabajo) {
		this.lista_sede_pref_trabajo = lista_sede_pref_trabajo;
	}

	public String getInteres_en() {
		return interes_en;
	}

	public void setInteres_en(String interes_en) {
		this.interes_en = interes_en;
	}

	public String getLista_interes_en() {
		return lista_interes_en;
	}

	public void setLista_interes_en(String lista_interes_en) {
		this.lista_interes_en = lista_interes_en;
	}

	public String getPretension_economica() {
		return pretension_economica;
	}

	public void setPretension_economica(String pretension_economica) {
		this.pretension_economica = pretension_economica;
	}

	public String getDispo_trabajar() {
		return dispo_trabajar;
	}

	public void setDispo_trabajar(String dispo_trabajar) {
		this.dispo_trabajar = dispo_trabajar;
	}

	public List getLista_dispo_trabajar() {
		return lista_dispo_trabajar;
	}

	public void setLista_dispo_trabajar(List lista_dispo_trabajar) {
		this.lista_dispo_trabajar = lista_dispo_trabajar;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getGradoInstruccion() {
		return gradoInstruccion;
	}

	public void setGradoInstruccion(String gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}

	public String getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}

	public String getDscEstado() {
		return dscEstado;
	}

	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public List getEvaluaciones() {
		return evaluaciones;
	}

	public void setEvaluaciones(List evaluaciones) {
		this.evaluaciones = evaluaciones;
	}

	public String getUltEstadoPostulRevision() {
		return ultEstadoPostulRevision;
	}

	public void setUltEstadoPostulRevision(String ultEstadoPostulRevision) {
		this.ultEstadoPostulRevision = ultEstadoPostulRevision;
	}
	
}
