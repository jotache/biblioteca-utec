package com.tecsup.SGA.modelo;

public class Devolucion {	
	private String codDevolucion;
	private String nroDevolucion;
	private String fechaDevolucion;
	private String dscCentroCosto;
	private String usuSolicitante;
	private String dscTipoBien;
	private String codSubTipoRequerimiento;
	private String codReqAsociado;
	private String nroReqAsociado;	
	private String motivoDevolucion;
	private String observacionDevolucion;
	private String codRequerimiento;
	private String codEstado;
	
	public String getCodDevolucion() {
		return codDevolucion;
	}
	public void setCodDevolucion(String codDevolucion) {
		this.codDevolucion = codDevolucion;
	}
	public String getNroDevolucion() {
		return nroDevolucion;
	}
	public void setNroDevolucion(String nroDevolucion) {
		this.nroDevolucion = nroDevolucion;
	}
	public String getFechaDevolucion() {
		return fechaDevolucion;
	}
	public void setFechaDevolucion(String fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	public String getDscCentroCosto() {
		return dscCentroCosto;
	}
	public void setDscCentroCosto(String dscCentroCosto) {
		this.dscCentroCosto = dscCentroCosto;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getDscTipoBien() {
		return dscTipoBien;
	}
	public void setDscTipoBien(String dscTipoBien) {
		this.dscTipoBien = dscTipoBien;
	}
	public String getCodSubTipoRequerimiento() {
		return codSubTipoRequerimiento;
	}
	public void setCodSubTipoRequerimiento(String codSubTipoRequerimiento) {
		this.codSubTipoRequerimiento = codSubTipoRequerimiento;
	}
	public String getCodReqAsociado() {
		return codReqAsociado;
	}
	public void setCodReqAsociado(String codReqAsociado) {
		this.codReqAsociado = codReqAsociado;
	}
	public String getNroReqAsociado() {
		return nroReqAsociado;
	}
	public void setNroReqAsociado(String nroReqAsociado) {
		this.nroReqAsociado = nroReqAsociado;
	}
	public String getMotivoDevolucion() {
		return motivoDevolucion;
	}
	public void setMotivoDevolucion(String motivoDevolucion) {
		this.motivoDevolucion = motivoDevolucion;
	}
	public String getObservacionDevolucion() {
		return observacionDevolucion;
	}
	public void setObservacionDevolucion(String observacionDevolucion) {
		this.observacionDevolucion = observacionDevolucion;
	}
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
}
