package com.tecsup.SGA.modelo;

public class NotaSeguimiento implements java.io.Serializable{	
	
	private String codRequerimiento;
	private String codNota;
	private String dscNota;
	private String fechaRegistro;
	
	public String getCodRequerimiento() {
		return codRequerimiento;
	}
	public void setCodRequerimiento(String codRequerimiento) {
		this.codRequerimiento = codRequerimiento;
	}
	public String getCodNota() {
		return codNota;
	}
	public void setCodNota(String codNota) {
		this.codNota = codNota;
	}
	public String getDscNota() {
		return dscNota;
	}
	public void setDscNota(String dscNota) {
		this.dscNota = dscNota;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
}
