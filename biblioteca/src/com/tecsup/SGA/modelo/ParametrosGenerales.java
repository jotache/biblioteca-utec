package com.tecsup.SGA.modelo;

public class ParametrosGenerales implements java.io.Serializable{
	
	private String descripcion;
	private String codPeriodo;
	private String codProducto;
	private String codTabla;
	private String valor;
	private String usuCrea;
	private String usuModi;
	private String estReg;
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodPeriodo() {
		return codPeriodo;
	}
	public void setCodPeriodo(String codPeriodo) {
		this.codPeriodo = codPeriodo;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodTabla() {
		return codTabla;
	}
	public void setCodTabla(String codTabla) {
		this.codTabla = codTabla;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getUsuCrea() {
		return usuCrea;
	}
	public void setUsuCrea(String usuCrea) {
		this.usuCrea = usuCrea;
	}
	public String getUsuModi() {
		return usuModi;
	}
	public void setUsuModi(String usuModi) {
		this.usuModi = usuModi;
	}
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}

}
