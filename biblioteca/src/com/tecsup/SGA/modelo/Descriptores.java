package com.tecsup.SGA.modelo;

public class Descriptores implements java.io.Serializable {
	private String codBien;
	private String codBienDet;
	private String codDescriptor;
	private String dscDescriptor;

	private String codRelacion;
	private String codTipoBien;
	private String dscTipoBien;
	private String codFamilia;
	private String dscFamilia;
	private String codSFamilia;
	private String dscSFamilia;
	
	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getCodBienDet() {
		return codBienDet;
	}
	public void setCodBienDet(String codBienDet) {
		this.codBienDet = codBienDet;
	}
	public String getCodDescriptor() {
		return codDescriptor;
	}
	public void setCodDescriptor(String codDescriptor) {
		this.codDescriptor = codDescriptor;
	}
	public String getDscDescriptor() {
		return dscDescriptor;
	}
	public void setDscDescriptor(String dscDescriptor) {
		this.dscDescriptor = dscDescriptor;
	}
	public String getCodRelacion() {
		return codRelacion;
	}
	public void setCodRelacion(String codRelacion) {
		this.codRelacion = codRelacion;
	}
	public String getCodTipoBien() {
		return codTipoBien;
	}
	public void setCodTipoBien(String codTipoBien) {
		this.codTipoBien = codTipoBien;
	}
	public String getDscTipoBien() {
		return dscTipoBien;
	}
	public void setDscTipoBien(String dscTipoBien) {
		this.dscTipoBien = dscTipoBien;
	}
	public String getCodFamilia() {
		return codFamilia;
	}
	public void setCodFamilia(String codFamilia) {
		this.codFamilia = codFamilia;
	}
	public String getDscFamilia() {
		return dscFamilia;
	}
	public void setDscFamilia(String dscFamilia) {
		this.dscFamilia = dscFamilia;
	}
	public String getCodSFamilia() {
		return codSFamilia;
	}
	public void setCodSFamilia(String codSFamilia) {
		this.codSFamilia = codSFamilia;
	}
	public String getDscSFamilia() {
		return dscSFamilia;
	}
	public void setDscSFamilia(String dscSFamilia) {
		this.dscSFamilia = dscSFamilia;
	}
	
}
