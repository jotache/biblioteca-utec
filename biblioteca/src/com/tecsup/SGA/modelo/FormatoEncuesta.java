package com.tecsup.SGA.modelo;

public class FormatoEncuesta implements java.io.Serializable{

	private String codEncuesta;
	private String nroEncuesta;
	private String nombreEncuesta;
	private String codTipoAplicacion;
	private String nomTipoAplicacion;
	private String codTipoEncuesta;
	private String nomTipoEncuesta;
	private String codEstado;
	private String nomEstado;
	
	private String obsEncuesta;
	private String indEncuestaManual;
	private String totalEncuestadosProg;
	private String totalEncuestadosReal;
	private String duracion;
	private String codSede;
	private String nomSede;
	private String codTipoServicio;
	private String nomTipoServicio;
	private String codResponsable;
	private String nomResponsable;		 
	
	private String codFormato;
	private String nomFormato;
	private String codSeccionAsignado;
	private String registrosRelacionados;
	
	private String totalEncuestados;
	private String dscEncuestados;	
	private String totalEncuestadosCrpta;
	private String fecIniAplicacion;
	private String horaIniAplicacion;
	private String fecFinAplicacion;
	private String horaFinAplicacion;
	private String codPerfil;
	private String codEncuestado;
	
	private String codProfesor;
	
	private String usuarioCreacion;
	private String usuarioResponsable;
	
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getUsuarioResponsable() {
		return usuarioResponsable;
	}
	public void setUsuarioResponsable(String usuarioResponsable) {
		this.usuarioResponsable = usuarioResponsable;
	}
	public String getCodProfesor() {
		return codProfesor;
	}
	public void setCodProfesor(String codProfesor) {
		this.codProfesor = codProfesor;
	}
	public String getCodEncuestado() {
		return codEncuestado;
	}
	public void setCodEncuestado(String codEncuestado) {
		this.codEncuestado = codEncuestado;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getHoraIniAplicacion() {
		return horaIniAplicacion;
	}
	public void setHoraIniAplicacion(String horaIniAplicacion) {
		this.horaIniAplicacion = horaIniAplicacion;
	}
	public String getHoraFinAplicacion() {
		return horaFinAplicacion;
	}
	public void setHoraFinAplicacion(String horaFinAplicacion) {
		this.horaFinAplicacion = horaFinAplicacion;
	}
	public String getTotalEncuestadosCrpta() {
		return totalEncuestadosCrpta;
	}
	public void setTotalEncuestadosCrpta(String totalEncuestadosCrpta) {
		this.totalEncuestadosCrpta = totalEncuestadosCrpta;
	}
	public String getFecIniAplicacion() {
		return fecIniAplicacion;
	}
	public void setFecIniAplicacion(String fecIniAplicacion) {
		this.fecIniAplicacion = fecIniAplicacion;
	}
	public String getFecFinAplicacion() {
		return fecFinAplicacion;
	}
	public void setFecFinAplicacion(String fecFinAplicacion) {
		this.fecFinAplicacion = fecFinAplicacion;
	}
	public String getTotalEncuestados() {
		return totalEncuestados;
	}
	public void setTotalEncuestados(String totalEncuestados) {
		this.totalEncuestados = totalEncuestados;
	}
	public String getDscEncuestados() {
		return dscEncuestados;
	}
	public void setDscEncuestados(String dscEncuestados) {
		this.dscEncuestados = dscEncuestados;
	}
	public String getCodFormato() {
		return codFormato;
	}
	public void setCodFormato(String codFormato) {
		this.codFormato = codFormato;
	}
	public String getNomFormato() {
		return nomFormato;
	}
	public void setNomFormato(String nomFormato) {
		this.nomFormato = nomFormato;
	}
	public String getCodSeccionAsignado() {
		return codSeccionAsignado;
	}
	public void setCodSeccionAsignado(String codSeccionAsignado) {
		this.codSeccionAsignado = codSeccionAsignado;
	}
	public String getRegistrosRelacionados() {
		return registrosRelacionados;
	}
	public void setRegistrosRelacionados(String registrosRelacionados) {
		this.registrosRelacionados = registrosRelacionados;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getNroEncuesta() {
		return nroEncuesta;
	}
	public void setNroEncuesta(String nroEncuesta) {
		this.nroEncuesta = nroEncuesta;
	}
	public String getNombreEncuesta() {
		return nombreEncuesta;
	}
	public void setNombreEncuesta(String nombreEncuesta) {
		this.nombreEncuesta = nombreEncuesta;
	}
	public String getCodTipoAplicacion() {
		return codTipoAplicacion;
	}
	public void setCodTipoAplicacion(String codTipoAplicacion) {
		this.codTipoAplicacion = codTipoAplicacion;
	}
	public String getNomTipoAplicacion() {
		return nomTipoAplicacion;
	}
	public void setNomTipoAplicacion(String nomTipoAplicacion) {
		this.nomTipoAplicacion = nomTipoAplicacion;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}
	public String getNomTipoEncuesta() {
		return nomTipoEncuesta;
	}
	public void setNomTipoEncuesta(String nomTipoEncuesta) {
		this.nomTipoEncuesta = nomTipoEncuesta;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getNomEstado() {
		return nomEstado;
	}
	public void setNomEstado(String nomEstado) {
		this.nomEstado = nomEstado;
	}
	public String getObsEncuesta() {
		return obsEncuesta;
	}
	public void setObsEncuesta(String obsEncuesta) {
		this.obsEncuesta = obsEncuesta;
	}
	public String getIndEncuestaManual() {
		return indEncuestaManual;
	}
	public void setIndEncuestaManual(String indEncuestaManual) {
		this.indEncuestaManual = indEncuestaManual;
	}
	public String getTotalEncuestadosProg() {
		return totalEncuestadosProg;
	}
	public void setTotalEncuestadosProg(String totalEncuestadosProg) {
		this.totalEncuestadosProg = totalEncuestadosProg;
	}
	public String getTotalEncuestadosReal() {
		return totalEncuestadosReal;
	}
	public void setTotalEncuestadosReal(String totalEncuestadosReal) {
		this.totalEncuestadosReal = totalEncuestadosReal;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getCodTipoServicio() {
		return codTipoServicio;
	}
	public void setCodTipoServicio(String codTipoServicio) {
		this.codTipoServicio = codTipoServicio;
	}
	public String getNomTipoServicio() {
		return nomTipoServicio;
	}
	public void setNomTipoServicio(String nomTipoServicio) {
		this.nomTipoServicio = nomTipoServicio;
	}
	public String getCodResponsable() {
		return codResponsable;
	}
	public void setCodResponsable(String codResponsable) {
		this.codResponsable = codResponsable;
	}
	public String getNomResponsable() {
		return nomResponsable;
	}
	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}
	public String getNomSede() {
		return nomSede;
	}
	public void setNomSede(String nomSede) {
		this.nomSede = nomSede;
	}
	
}
