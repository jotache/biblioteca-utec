package com.tecsup.SGA.modelo;

public class CursoConfig {
	
	private String codigo;
	//************************************
	private String codCurso;
	private String regla;
	private String condicion;
	private String verdadero01;
	private String verdadero02;
	private String verdadero03;
	private String verdadero04;
	private String falso01;
	private String falso02;
	private String falso03;
	private String falso04;
	private String opeVer01;
	private String opeVer02;
	private String opeVer03;
	private String opeVer04;
	private String opeFal01;
	private String opeFal02;
	private String opeFal03;
	private String opeFal04;
	//************************************
	private String resultado;
	
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getRegla() {
		return regla;
	}
	public void setRegla(String regla) {
		this.regla = regla;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getVerdadero01() {
		return verdadero01;
	}
	public void setVerdadero01(String verdadero01) {
		this.verdadero01 = verdadero01;
	}
	public String getVerdadero02() {
		return verdadero02;
	}
	public void setVerdadero02(String verdadero02) {
		this.verdadero02 = verdadero02;
	}
	public String getVerdadero03() {
		return verdadero03;
	}
	public void setVerdadero03(String verdadero03) {
		this.verdadero03 = verdadero03;
	}
	public String getVerdadero04() {
		return verdadero04;
	}
	public void setVerdadero04(String verdadero04) {
		this.verdadero04 = verdadero04;
	}
	public String getFalso01() {
		return falso01;
	}
	public void setFalso01(String falso01) {
		this.falso01 = falso01;
	}
	public String getFalso02() {
		return falso02;
	}
	public void setFalso02(String falso02) {
		this.falso02 = falso02;
	}
	public String getFalso03() {
		return falso03;
	}
	public void setFalso03(String falso03) {
		this.falso03 = falso03;
	}
	public String getFalso04() {
		return falso04;
	}
	public void setFalso04(String falso04) {
		this.falso04 = falso04;
	}
	public String getOpeVer01() {
		return opeVer01;
	}
	public void setOpeVer01(String opeVer01) {
		this.opeVer01 = opeVer01;
	}
	public String getOpeVer02() {
		return opeVer02;
	}
	public void setOpeVer02(String opeVer02) {
		this.opeVer02 = opeVer02;
	}
	public String getOpeVer03() {
		return opeVer03;
	}
	public void setOpeVer03(String opeVer03) {
		this.opeVer03 = opeVer03;
	}
	public String getOpeVer04() {
		return opeVer04;
	}
	public void setOpeVer04(String opeVer04) {
		this.opeVer04 = opeVer04;
	}
	public String getOpeFal01() {
		return opeFal01;
	}
	public void setOpeFal01(String opeFal01) {
		this.opeFal01 = opeFal01;
	}
	public String getOpeFal02() {
		return opeFal02;
	}
	public void setOpeFal02(String opeFal02) {
		this.opeFal02 = opeFal02;
	}
	public String getOpeFal03() {
		return opeFal03;
	}
	public void setOpeFal03(String opeFal03) {
		this.opeFal03 = opeFal03;
	}
	public String getOpeFal04() {
		return opeFal04;
	}
	public void setOpeFal04(String opeFal04) {
		this.opeFal04 = opeFal04;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
