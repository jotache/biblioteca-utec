package com.tecsup.SGA.bean;

public class DetalleEvalParcial implements java.io.Serializable{

	private String codAlumno;
	private String nroEvaluacion;
	private String nombre;
	private String nota;
	private String peso;
	private String prom;
	private String nroNotas;
	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNroEvaluacion() {
		return nroEvaluacion;
	}
	public void setNroEvaluacion(String nroEvaluacion) {
		this.nroEvaluacion = nroEvaluacion;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public String getProm() {
		return prom;
	}
	public void setProm(String prom) {
		this.prom = prom;
	}
	public String getNroNotas() {
		return nroNotas;
	}
	public void setNroNotas(String nroNotas) {
		this.nroNotas = nroNotas;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	
}
