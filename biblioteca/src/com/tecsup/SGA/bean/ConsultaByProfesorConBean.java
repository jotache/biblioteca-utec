package com.tecsup.SGA.bean;

import java.util.List;

public class ConsultaByProfesorConBean implements java.io.Serializable{
	
	
	private String idSeccion;
	private String codSeccion;
	private String nomSeccion;
	private String codDpto;
	private String nomDpto;
	private String codProducto;
	private String codCurso;
	private String nomCurso;
	private String codPrograma;
	private String codCiclo;
	private String nomCiclo;
	private String codSeccionAlumno;
	private String nomSeccionAlumno;
	private String idGrupo;
	private String nomGrupo;
	private String totalGrupo;
	private List listaGrupos;	
	private String anioEgresado;
	private String nroEncuestadoxGrupo;
	private String codProfesor;
	private String nomProfesor;
	private String codTipoPersonal;
	private String nomTipoPersonal;
	private String codPrograman;
	private String codTipoDocente;
	private String nomTipoDocente;
	private String codPerfil;
	private String nomPerfil;
	private String sumTotalGrupo;
	
	public String getSumTotalGrupo() {
		return sumTotalGrupo;
	}
	public void setSumTotalGrupo(String sumTotalGrupo) {
		this.sumTotalGrupo = sumTotalGrupo;
	}
	public String getCodProfesor() {
		return codProfesor;
	}
	public void setCodProfesor(String codProfesor) {
		this.codProfesor = codProfesor;
	}
	public String getNomProfesor() {
		return nomProfesor;
	}
	public void setNomProfesor(String nomProfesor) {
		this.nomProfesor = nomProfesor;
	}
	public String getNomTipoPersonal() {
		return nomTipoPersonal;
	}
	public void setNomTipoPersonal(String nomTipoPersonal) {
		this.nomTipoPersonal = nomTipoPersonal;
	}
	public String getCodPrograman() {
		return codPrograman;
	}
	public void setCodPrograman(String codPrograman) {
		this.codPrograman = codPrograman;
	}
	public String getAnioEgresado() {
		return anioEgresado;
	}
	public void setAnioEgresado(String anioEgresado) {
		this.anioEgresado = anioEgresado;
	}
	public String getNroEncuestadoxGrupo() {
		return nroEncuestadoxGrupo;
	}
	public void setNroEncuestadoxGrupo(String nroEncuestadoxGrupo) {
		this.nroEncuestadoxGrupo = nroEncuestadoxGrupo;
	}
	public String getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getNomSeccion() {
		return nomSeccion;
	}
	public void setNomSeccion(String nomSeccion) {
		this.nomSeccion = nomSeccion;
	}
	public String getCodDpto() {
		return codDpto;
	}
	public void setCodDpto(String codDpto) {
		this.codDpto = codDpto;
	}
	public String getNomDpto() {
		return nomDpto;
	}
	public void setNomDpto(String nomDpto) {
		this.nomDpto = nomDpto;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getNomCurso() {
		return nomCurso;
	}
	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getNomCiclo() {
		return nomCiclo;
	}
	public void setNomCiclo(String nomCiclo) {
		this.nomCiclo = nomCiclo;
	}
	public String getCodSeccionAlumno() {
		return codSeccionAlumno;
	}
	public void setCodSeccionAlumno(String codSeccionAlumno) {
		this.codSeccionAlumno = codSeccionAlumno;
	}
	public String getNomSeccionAlumno() {
		return nomSeccionAlumno;
	}
	public void setNomSeccionAlumno(String nomSeccionAlumno) {
		this.nomSeccionAlumno = nomSeccionAlumno;
	}
	public String getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}
	public String getNomGrupo() {
		return nomGrupo;
	}
	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}
	public String getTotalGrupo() {
		return totalGrupo;
	}
	public void setTotalGrupo(String totalGrupo) {
		this.totalGrupo = totalGrupo;
	}
	public void setListaGrupos(List listaGrupos) {
		this.listaGrupos = listaGrupos;
	}
	public List getListaGrupos() {
		return listaGrupos;
	}
	public String getCodTipoPersonal() {
		return codTipoPersonal;
	}
	public void setCodTipoPersonal(String codTipoPersonal) {
		this.codTipoPersonal = codTipoPersonal;
	}
	public String getCodTipoDocente() {
		return codTipoDocente;
	}
	public void setCodTipoDocente(String codTipoDocente) {
		this.codTipoDocente = codTipoDocente;
	}
	public String getNomTipoDocente() {
		return nomTipoDocente;
	}
	public void setNomTipoDocente(String nomTipoDocente) {
		this.nomTipoDocente = nomTipoDocente;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getNomPerfil() {
		return nomPerfil;
	}
	public void setNomPerfil(String nomPerfil) {
		this.nomPerfil = nomPerfil;
	}
		
	
}
