package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;
import com.tecsup.SGA.modelo.Postulante;

public class PostulantesDecorator extends TableDecorator {
	private Postulante lObject;
	
	public String addRowClass()
	{
		return "texto";
	}
	
	public String getChkSelPostulante()	
	{
		lObject = (Postulante) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codPostulante\" value=\"" + lObject.getCodPostulante()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + 
					lObject.getNombres() + "','" + 
					lObject.getCodEstado() + "');\" >" ;
        return str;
	}
	
	
	public String getNombreApellido()
	{
		lObject = (Postulante) getCurrentRowObject();
		String str = "<a href=\"javascript:fc_HojaVida('" + lObject.getCodPostulante() + "');\" >" + lObject.getNombres() + "</a>";
        return str;
	}
	public String getVerCV()
	{
		lObject = (Postulante) getCurrentRowObject();
		String str = "<img src=\"/biblioteca/images/iconos/buscar1.jpg\" style=\"cursor:pointer\" " +
					"onclick=\"fc_VerCV('" + lObject.getPerfil().trim() + "');\"/>";
        return str;
	}
	public String getLinkImagen(){
		
		lObject = (Postulante) getCurrentRowObject();
		String str = "<img src=\"/biblioteca/images/iconos/modificar1.jpg\" style=\"cursor:pointer\" " +
					 "onclick=\"fc_VerHistorial('" + lObject.getCodPostulante().trim() + "');\"/>";
        return str;
		
	}
}
