package com.tecsup.SGA.bean;

public class ProductoxCotizacionBean {

	private String codCoti;
	private String codCotiDet;
	private String dscFam;
	private String dscSfam;
	private String dscBien;
	private String cantidad;
	private String preUltiOrden;
	private String canUltiOrden;
	private String codUltiOrden;
	private String unidad; //jhpr 2008-09-29
	
	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}
	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	public String getCodCoti() {
		return codCoti;
	}
	public void setCodCoti(String codCoti) {
		this.codCoti = codCoti;
	}
	public String getCodCotiDet() {
		return codCotiDet;
	}
	public void setCodCotiDet(String codCotiDet) {
		this.codCotiDet = codCotiDet;
	}
	public String getDscFam() {
		return dscFam;
	}
	public void setDscFam(String dscFam) {
		this.dscFam = dscFam;
	}
	public String getDscSfam() {
		return dscSfam;
	}
	public void setDscSfam(String dscSfam) {
		this.dscSfam = dscSfam;
	}
	public String getDscBien() {
		return dscBien;
	}
	public void setDscBien(String dscBien) {
		this.dscBien = dscBien;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getPreUltiOrden() {
		return preUltiOrden;
	}
	public void setPreUltiOrden(String preUltiOrden) {
		this.preUltiOrden = preUltiOrden;
	}
	public String getCanUltiOrden() {
		return canUltiOrden;
	}
	public void setCanUltiOrden(String canUltiOrden) {
		this.canUltiOrden = canUltiOrden;
	}
	public String getCodUltiOrden() {
		return codUltiOrden;
	}
	public void setCodUltiOrden(String codUltiOrden) {
		this.codUltiOrden = codUltiOrden;
	}

	

	
	
}
