package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;
import com.tecsup.SGA.modelo.Logistica;
import com.tecsup.SGA.modelo.SubFamilia;
import com.tecsup.SGA.modelo.Proveedor;
import com.tecsup.SGA.modelo.CatalogoProducto;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.AsignarResponsable;   
import com.tecsup.SGA.modelo.Empleados;
import com.tecsup.SGA.modelo.Descriptores; 
import com.tecsup.SGA.modelo.ActivosDisponibles;
import com.tecsup.SGA.modelo.Almacen;
import com.tecsup.SGA.modelo.BienesDevolucion;
import com.tecsup.SGA.modelo.CentrosCostos;
import com.tecsup.SGA.modelo.DocumentoAsociado;

public class LogisticaDecorator extends TableDecorator{
	private Logistica lobject;
	private SubFamilia sobject;
	private Proveedor pobject;
	private CatalogoProducto cobject;
	private SolRequerimiento sol;
	private Producto eobject;
	private AsignarResponsable aobject;
	private SubFamilia subobject;
	private Empleados emobject;
	private Descriptores desobject;
	private ActivosDisponibles acobject;
	private Almacen alobject;
	private BienesDevolucion bobject;
	private CentrosCostos cenobject;
	private DocumentoAsociado odocumento;
	
	public String getTextoDefecto(){
		lobject = (Logistica) getCurrentRowObject();
		String str ="<textarea maxlength=\"4000\" lang=\"4000\" class=\"cajatexto\" style=\"HEIGHT:40px;width:320px\" name=\"codProceso\" value=\""		 
		+ "\" readonly=\"true\">"+lobject.getCodPadre()+ "</textarea>" ; 
		
		return str;
	}
	
	public String getRbtSelCentroCosto()
	{
		cenobject = (CentrosCostos) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + cenobject.getCodSeco()
					+ "\"  onclick=\"javascript:fc_SelCentroCosto('" + cenobject.getCodSeco() + "');\" >" ;
    	System.out.println(">>"+str+"<<");
		return str;
	}
	
	public String getRbtSelBienAlmacenDev()
	{
		bobject = (BienesDevolucion) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + bobject.getCodDev()
					+ "\"  onclick=\"javascript:fc_SelBienAlmacen('" + bobject.getCodDev() + "','" + bobject.getCodReq() + "');\" >" ;
    	System.out.println(">>"+str+"<<");
		return str;
	}
	public String getRbtSelBienAlmacen()
	{
		alobject = (Almacen) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + alobject.getCodOrden()
					+ "\"  onclick=\"javascript:fc_SelBienAlmacen(this.value,'" + alobject.getCodProveedor() + "','" + alobject.getCodProveedor() + "');\" >" ;
    	return str;
	}
	public String getRbtSelProductosActivos()
	{
		acobject = (ActivosDisponibles) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + acobject.getCodBienActivo()
					+ "\"  onclick=\"javascript:fc_buscarDescriptor(this.value,'" + acobject.getNroSerie() + "');\" >" ;
    	return str;
	}
	public String getRbtSelBuscarDescriptor2()
	{
		desobject = (Descriptores) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + desobject.getCodBienDet()
					+ "\"  onclick=\"javascript:fc_buscarDescriptor(this.value,'" + desobject.getCodBienDet() + "');\" >" ;
    	return str;
	}
	public String getRbtSelBuscarDescriptor()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "','" + lobject.getValor2() + "');\" >" ;
    	return str;
	}
	public String getRbtSelFamilia() 
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "', '" + lobject.getDscPadre() + "', '" + lobject.getValor1() + "','" + lobject.getCodPadre() + "','" + lobject.getCodigo() + "');\" >" ;
    	return str;
	}
	public String getRbtSelFamiliaSubFamilia()
	{
		desobject = (Descriptores) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + desobject.getCodRelacion()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + desobject.getCodRelacion() + "');\" >" ;
    	return str;
	}
	public String getRbtSelSubFamilia()
	{
		sobject = (SubFamilia) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + sobject.getCodUnico()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + sobject.getCodGenerado() + "','" + sobject.getDesSubFam() + "','" + sobject.getCodFam() +"');\" >" ;
    	return str;
	}
	public String getRbtSelUnidad()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "', '" + lobject.getValor1() + "', '" + lobject.getValor2() + "');\" >" ;
    	return str;
	}
	public String getRbtSelDescriptor()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "','" + lobject.getValor2() + "');\" >" ;
    	return str;
	}
	public String getRbtSelUbicacion()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "');\" >" ;
    	return str;
	}
	public String getRbtSelGrupoFamilia()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "', '" + lobject.getCodPadre() + "', '" + lobject.getDscPadre() + "','" + lobject.getValor1() + "');\" >" ;
    	return str;
	}
	public String getRbtSelAdjuntos()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "');\" >" ;
    	return str;
	}
	public String getRbtSelConCompra()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "');\" >" ;
    	return str;
	}
	public String getRbtSelProveedores()
	{
		pobject = (Proveedor) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + pobject.getCodUnico()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + pobject.getNroDco() + "');\" >" ;
    	return str;
	}
	public String getRbtSelCalif()
	{   int i= getListIndex();  
		lobject = (Logistica) getCurrentRowObject();//id=\"text"+i+"\" id='txhCod" + Integer.toString(i) +"'
		String strCo = "<input type='hidden' id=\"txhCod"+i+"\"  value='" + lobject.getCodSecuencial() + "'/>";
		String str = "<input type='hidden' id='txhIndice" + Integer.toString(i) +"' value='" + Integer.toString(i) + "'/>";
		str = str + strCo + "<input type=\"radio\" id=\"text"+i+"\"  name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "', '" + lobject.getCodigo()+ "','" + i+ "');\" >" ;
    	System.out.println(">>"+str+"<<");
		return str;
	}
	public String getRbtSelGastos()
	{
		lobject = (Logistica) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSecuencial()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() + "','" + lobject.getValor2() + "','" + lobject.getCodPadre() + "');\" >" ;
    	return str;
	}
	public String getRbtSelFlujo()
	{
		subobject = (SubFamilia) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + subobject.getCodResponsable()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + subobject.getCodUnico() + "','" + subobject.getCodBien() + "');\" >" ;
    	return str;
	}  
	public String getRbtSelFlujo2() 
	{
		subobject = (SubFamilia) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + subobject.getCodResponsable()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro2(this.value,'" + subobject.getCodUnico() + "','" + subobject.getCodBien() + "');\" >" ;
    	return str;
	} 
	public String getRbtSelEmpleados()
	{
		emobject = (Empleados) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + emobject.getCodigo()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + emobject.getTipoPersonal() + "','" + emobject.getNombre() + "');\" >" ;
    	return str;
	}
	public String getRbtSelCatalogo()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + cobject.getCodProducto()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + cobject.getRutaImagen() + "','" + cobject.getCondicion() + "','" + cobject.getEstado() + "');\" >" ;
    	return str;
	}
	
	public String getLnkCodProducto(){
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str = "<a href=# onclick=\"javascript:fc_verKardex('" + cobject.getCodProducto() + "');\"  >" + cobject.getCodProducto() + "</a>";
		return str;
	}
	
	/*public String getLblproducto()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<label type=\"label\" name=\"codProceso\" value=\"" + cobject.getNomProducto()
					+ "\"  onclick=\"javascript:fc_Muestra(this.value,'" + cobject.getDescripcion() + "');\" >" ;
    	return str;
	}*/
	public String getLblproducto()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + cobject.getCodProducto() +"');\" > " + cobject.getNomProducto()+
				"</td></tr></table>";
    	return str;
	}
	public String getLblproducto1()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + cobject.getCodProducto() +"');\" > " + cobject.getNomUnidad()+
				"</td></tr></table>";
    	return str;
	}
	public String getLblproducto2()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<table><tr><td onclick=\"javascript:fc_Muestra('" +  cobject.getCodProducto() +"');\" > " + cobject.getPrecio()+
				"</td></tr></table>";
    	return str;
	}
	public String getLblproducto3()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<table><tr><td onclick=\"javascript:fc_Muestra('" + cobject.getCodProducto() +"');\" > " + cobject.getStockActual()+
				"</td></tr></table>";
    	return str;
	}
	public String getLblproducto4()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" +  cobject.getCodProducto() +"');\" > " + cobject.getStockMostrar()+
				"</td></tr></table>";
    	return str;
	}
	public String getLblproducto5()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<table><tr><td onclick=\"javascript:fc_Muestra('" + cobject.getCodProducto() +"');\" > " + cobject.getNroDiasAtencion()+
				"</td></tr></table>";
    	return str;
	}
	public String getLblproducto6()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str ="<table><tr><td onclick=\"javascript:fc_Muestra('" + cobject.getCodProducto() +"');\" > " + cobject.getPrecioReferencial()+
				"</td></tr></table>";
    	return str;
	}
	//ALQD,26/06/09.SOLO SE MOSTRAR LA IMAGEN CUANDO RUTA_IMAGEN <> DE VACIO
	public String getImagen()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str;
		if(!cobject.getRutaImagen().equals("")){
			str ="<img src=\"/biblioteca/images/iconos/buscar1.jpg\" style=\"width:15px;height:15px;cursor : pointer\" " +
			"onclick=\"javascript:fc_Imagen('"+ cobject.getRutaImagen() + "');\">&nbsp; ";
		}
		else
		{
			str ="<table><tr><td>&nbsp;&nbsp;</td></tr></table>";
		}
    	return str;
	}
	//ALQD,26/06/09.SOLO SE MOSTRARA LA IMAGEN CUANDO RUTA_IMAGEN <> DE VACIO
	public String getMuestraImagen()
	{
		cobject = (CatalogoProducto) getCurrentRowObject();
		String str;
		if(!cobject.getRutaImagen().equals("")){
			str ="<img src=\"/biblioteca/images/iconos/buscar1.jpg\" style=\"cursor : pointer\" " +
			"onclick=\"javascript:fc_VerImagen('"+ cobject.getRutaImagen() + "');\">";
		}
		else{
			str ="<table><tr><td>&nbsp;&nbsp;</td></tr></table>";
		}
    	return str;
	}
	public String getAdjunto()
	{
		eobject = (Producto) getCurrentRowObject();
		String str ="<img src=\"/biblioteca/images/iconos/buscar1.jpg\" " +
		"onclick=\"javascript:fc_Adjunto('"+ eobject.getRutaAdj() + "');\">&nbsp; ";
    	return str;
	}
	public String getCheckSelRequerimientoBien()
	{   
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str="<input type=\"radio\" name=\"mad\" id=\"wild"+i+"\" value=\""
		+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + sol.getIdRequerimiento()+  "', '" + sol.getCodEstado()+  
		"','" + sol.getIndiceDevolucion()+  "','" + sol.getSubTipoRequerimiento()+  "');\" >";
		
        return str;
	}
	public String getCheckSelRequerimientoServicio()
	{   
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str="<input type=\"radio\" name=\"wild\" id=\"wild"+i+"\" value=\""
			+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + sol.getIdRequerimiento()+  "', '" + sol.getCodEstado()+ 
			"','" + sol.getIndiceDevolucion()+  "','" + sol.getSubTipoRequerimiento()+  "');\" >";
        return str;
	}
	
	public String getRbtSelDocPro()
	{
		eobject = (Producto) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + eobject.getCodigo()
					+ "\"  onclick=\"javascript:fc_seleccionaDoc(this.value,'" + eobject.getCodTipoAdj() +"');\" >" ;
    	return str;
	}
	
	public String getCheckSelARequerimientoBien()
	{  
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str="<input type=\"checkbox\" name=\"mad\" id=\"wild"+i+"\" value=\""
		+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + sol.getIdRequerimiento()+  "');\" >";
				
        return str;
	}
	public String getCheckSelARequerimientoServicio()
	{   
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str="<input type=\"checkbox\" name=\"wild\" id=\"wild"+i+"\" value=\""
			+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + sol.getIdRequerimiento()+  "');\" >";
		
        return str;
	}
	public String getImg()
	{
		aobject = (AsignarResponsable) getCurrentRowObject();
		String str ="<img src=\"/biblioteca/images/iconos/buscar1.jpg\" style=\"width:15px;height:15px;cursor : pointer\" " +
		"onclick=\"javascript:fc_Imagen('"+ aobject.getCodigo() + "');\">&nbsp; ";
    	return str;
	}        
	public String getRbtSelResponsale()
	{
		aobject = (AsignarResponsable) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + aobject.getCodigo()
					+ "\"  onclick=\"javascript:fc_seleccionaRes(this.value,'" + aobject.getNombreUsu() +"');\" >" ;
    	return str;
	}
	public String getDobleNroRequerimiento()
	{   
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getNroRequerimiento()+ "</td></tr></table>";
		
        return str;
	}
	public String getDobleFechaEmision()
	{  
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getFechaEmision()+ "</td></tr></table>";
		
        return str;
	}
	public String getDobleDscCecoSolicitante()
	{   
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getDscCecoSolicitante()+ "</td></tr></table>";
		
        return str;
	}
	public String getDobleDscSubTipoRequerimiento()
	{  
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getDscSubTipoRequerimiento()+ "</td></tr></table>";
		
        return str;
	}
	public String getDobleTotalRequerimiento()
	{  
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String a="&nbsp;";
		if(sol.getTotalRequerimiento()==null || !sol.getTotalRequerimiento().equals(""))
			a=sol.getTotalRequerimiento();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ a+ "</td></tr></table>";
		
        return str;
	}
	public String getDobleUsuSolicitante()
	{   
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getUsuSolicitante()+ "</td></tr></table>";
		
        return str;
	}
	public String getDobleDscDetalleEstado()
	{  
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getDscDetalleEstado()+ "</td></tr></table>";
		
        return str;
	}
	//dobleUsuAsignado
	public String getDobleDscTipoServicio()
	{  
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getDscTipoServicio()+ "</td></tr></table>";
		
        return str;
	}
	public String getDobleUsuAsignado()
	{  
		int i= getListIndex();
		sol= (SolRequerimiento) getCurrentRowObject();
		String str ="<table><tr><td ondblclick=\"javascript:fc_Muestra('" + sol.getIdRequerimiento() + "');\" > "
		+ sol.getUsuAsignado()+ "</td></tr></table>";
		
        return str;
	}
	
	public String getNumeroSelecciona()
	{
		odocumento = (DocumentoAsociado) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + odocumento.getFecha() +"','" + odocumento.getDpdo_id() +"','" + odocumento.getMonto() +"','" + odocumento.getNumero() +"','" + odocumento.getDescripcion() +"','" +odocumento.getCodTipMoneda()+"','"+odocumento.getImpTipCamSoles()+"');\" > " + odocumento.getNumero()+
				"</td></tr></table>";
    	return str;
	}
	
	public String getFechaSelecciona()
	{
		odocumento = (DocumentoAsociado) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + odocumento.getFecha() +"','" + odocumento.getDpdo_id() +"','" + odocumento.getMonto() +"','" + odocumento.getNumero() +"','" + odocumento.getDescripcion() +"','" +odocumento.getCodTipMoneda()+"','"+odocumento.getImpTipCamSoles()+"');\" > " + odocumento.getFecha()+
				"</td></tr></table>";
    	return str;
	}
	public String getMontoSelecciona()
	{
		odocumento = (DocumentoAsociado) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + odocumento.getFecha() +"','" + odocumento.getDpdo_id() +"','" + odocumento.getMonto() +"','" + odocumento.getNumero() +"','" + odocumento.getDescripcion() +"','" +odocumento.getCodTipMoneda()+"','"+odocumento.getImpTipCamSoles()+"');\" > " + odocumento.getMonto()+
				"</td></tr></table>";
    	return str;
	}
	public String getRbtSelDocumentoRelacionado()
	{
		odocumento = (DocumentoAsociado) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + odocumento.getDpdo_id()
					+ "\"  onclick=\"javascript:fc_SelDocumento('" + odocumento.getDpdo_id() + "');\" >" ;
		return str;
	}
//ALQD,12/06/09.A�ADIENDO 1 METODO GET
	public String getMonedaSelecciona()
	{
		odocumento = (DocumentoAsociado) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + odocumento.getFecha() +"','" + odocumento.getDpdo_id() +"','" + odocumento.getMonto() +"','" + odocumento.getNumero() +"','" + odocumento.getDescripcion() +"','" +odocumento.getCodTipMoneda()+"','"+odocumento.getImpTipCamSoles()+"');\" > " + odocumento.getDesTipMoneda()+
				"</td></tr></table>";
    	return str;
	}
	//ALQD,30/06/09.A�ADIENDO 1 METODO GET
	public String getTcSelecciona()
	{
		odocumento = (DocumentoAsociado) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + odocumento.getFecha() +"','" + odocumento.getDpdo_id() +"','" + odocumento.getMonto() +"','" + odocumento.getNumero() +"','" + odocumento.getDescripcion() +"','" +odocumento.getCodTipMoneda()+"','"+odocumento.getImpTipCamSoles()+"');\" > " + odocumento.getImpTipCamSoles()+
				"</td></tr></table>";
    	return str;
	}
	public String getDescripcionSelecciona()
	{
		odocumento = (DocumentoAsociado) getCurrentRowObject();
		String str ="<table><tr><td style=\"cursor : pointer\" onclick=\"javascript:fc_Muestra('" + odocumento.getFecha() +"','" + odocumento.getDpdo_id() +"','" + odocumento.getMonto() +"','" + odocumento.getNumero() +"','" + odocumento.getDescripcion() +"','" +odocumento.getCodTipMoneda()+"','"+odocumento.getImpTipCamSoles()+"');\" > " + odocumento.getDescripcion()+
				"</td></tr></table>";
    	return str;
	}
	
}
