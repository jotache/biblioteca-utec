package com.tecsup.SGA.bean;

public class ReporteIngresoAlmacenDevBean {
	
	private String nroDevolucion;
	private String fecDevolucion;
	private String nroGuiaSalida;
	private String fecGuiaSalida;
	private String usuSolicitante;
	private String dscTipoBien;
	private String dscFamilia;
	private String codBien;
	private String dscBien;
	private String dscUnidad;
	private String cant;
	private String costoUnitario;
	private String importe;
	private String cantxEntregar;
	private String dscEstadoOrden;
	
	public String getNroDevolucion() {
		return nroDevolucion;
	}
	public void setNroDevolucion(String nroDevolucion) {
		this.nroDevolucion = nroDevolucion;
	}
	public String getFecDevolucion() {
		return fecDevolucion;
	}
	public void setFecDevolucion(String fecDevolucion) {
		this.fecDevolucion = fecDevolucion;
	}
	public String getNroGuiaSalida() {
		return nroGuiaSalida;
	}
	public void setNroGuiaSalida(String nroGuiaSalida) {
		this.nroGuiaSalida = nroGuiaSalida;
	}
	public String getFecGuiaSalida() {
		return fecGuiaSalida;
	}
	public void setFecGuiaSalida(String fecGuiaSalida) {
		this.fecGuiaSalida = fecGuiaSalida;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getDscTipoBien() {
		return dscTipoBien;
	}
	public void setDscTipoBien(String dscTipoBien) {
		this.dscTipoBien = dscTipoBien;
	}

	public String getCodBien() {
		return codBien;
	}
	public void setCodBien(String codBien) {
		this.codBien = codBien;
	}
	public String getDscBien() {
		return dscBien;
	}
	public void setDscBien(String dscBien) {
		this.dscBien = dscBien;
	}
	public String getDscUnidad() {
		return dscUnidad;
	}
	public void setDscUnidad(String dscUnidad) {
		this.dscUnidad = dscUnidad;
	}
	public String getCant() {
		return cant;
	}
	public void setCant(String cant) {
		this.cant = cant;
	}
	public String getCostoUnitario() {
		return costoUnitario;
	}
	public void setCostoUnitario(String costoUnitario) {
		this.costoUnitario = costoUnitario;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getCantxEntregar() {
		return cantxEntregar;
	}
	public void setCantxEntregar(String cantxEntregar) {
		this.cantxEntregar = cantxEntregar;
	}
	public String getDscEstadoOrden() {
		return dscEstadoOrden;
	}
	public void setDscEstadoOrden(String dscEstadoOrden) {
		this.dscEstadoOrden = dscEstadoOrden;
	}
	public String getDscFamilia() {
		return dscFamilia;
	}
	public void setDscFamilia(String dscFamilia) {
		this.dscFamilia = dscFamilia;
	}
	
}
