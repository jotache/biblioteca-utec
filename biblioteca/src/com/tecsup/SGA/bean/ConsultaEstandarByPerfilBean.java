package com.tecsup.SGA.bean;

public class ConsultaEstandarByPerfilBean implements java.io.Serializable{	
	private String codEncuesta;
	private String idSeccion;
	private String codSeccion;
	private String nomSeccion;
	private String nomDepartamento;
	private String codProducto;
	private String codPrograma;
	private String codCurso;
	private String nomCurso;
	private String nomCiclo;
	private String nomSeccion02;
	private String codGrupo;
	private String nomGrupo;	
	private String codPregunta;
	private String nomPregunta;
	
	private String idAlternativa;
	private String codAlternativa;
	private String nomAlternativa;
	private String dscAlternativa;
	private String totalAlternativa;
	private String respuesta;
	
	private String codProfesor;
	private String nomTipoPersonal;
	private String nomProfesor;
	private String anioEgresado;
	
	private String codTipoPersonal;	
	private String codTipoDocente;
	private String nomTipoDocente;
	
	private String codPerfil;
	private String nomPerfil;
	
	private String codTipoPregunta;
	private String indPreguntaUnica;
	private String dscPreguntaUnica;
	
	private String pesoAlternativa;
	private String nroRespuestas;
	
	
	public String getPesoAlternativa() {
		return pesoAlternativa;
	}
	public void setPesoAlternativa(String pesoAlternativa) {
		this.pesoAlternativa = pesoAlternativa;
	}
	public String getNroRespuestas() {
		return nroRespuestas;
	}
	public void setNroRespuestas(String nroRespuestas) {
		this.nroRespuestas = nroRespuestas;
	}
	public String getCodTipoPregunta() {
		return codTipoPregunta;
	}
	public void setCodTipoPregunta(String codTipoPregunta) {
		this.codTipoPregunta = codTipoPregunta;
	}
	public String getIndPreguntaUnica() {
		return indPreguntaUnica;
	}
	public void setIndPreguntaUnica(String indPreguntaUnica) {
		this.indPreguntaUnica = indPreguntaUnica;
	}
	public String getDscPreguntaUnica() {
		return dscPreguntaUnica;
	}
	public void setDscPreguntaUnica(String dscPreguntaUnica) {
		this.dscPreguntaUnica = dscPreguntaUnica;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getNomPerfil() {
		return nomPerfil;
	}
	public void setNomPerfil(String nomPerfil) {
		this.nomPerfil = nomPerfil;
	}
	public String getCodTipoPersonal() {
		return codTipoPersonal;
	}
	public void setCodTipoPersonal(String codTipoPersonal) {
		this.codTipoPersonal = codTipoPersonal;
	}
	public String getCodTipoDocente() {
		return codTipoDocente;
	}
	public void setCodTipoDocente(String codTipoDocente) {
		this.codTipoDocente = codTipoDocente;
	}
	public String getNomTipoDocente() {
		return nomTipoDocente;
	}
	public void setNomTipoDocente(String nomTipoDocente) {
		this.nomTipoDocente = nomTipoDocente;
	}
	public String getAnioEgresado() {
		return anioEgresado;
	}
	public void setAnioEgresado(String anioEgresado) {
		this.anioEgresado = anioEgresado;
	}
	public String getCodProfesor() {
		return codProfesor;
	}
	public void setCodProfesor(String codProfesor) {
		this.codProfesor = codProfesor;
	}
	public String getNomTipoPersonal() {
		return nomTipoPersonal;
	}
	public void setNomTipoPersonal(String nomTipoPersonal) {
		this.nomTipoPersonal = nomTipoPersonal;
	}
	public String getNomProfesor() {
		return nomProfesor;
	}
	public void setNomProfesor(String nomProfesor) {
		this.nomProfesor = nomProfesor;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getNomSeccion() {
		return nomSeccion;
	}
	public void setNomSeccion(String nomSeccion) {
		this.nomSeccion = nomSeccion;
	}
	public String getNomDepartamento() {
		return nomDepartamento;
	}
	public void setNomDepartamento(String nomDepartamento) {
		this.nomDepartamento = nomDepartamento;
	}
	public String getNomCurso() {
		return nomCurso;
	}
	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}
	public String getNomCiclo() {
		return nomCiclo;
	}
	public void setNomCiclo(String nomCiclo) {
		this.nomCiclo = nomCiclo;
	}
	public String getNomSeccion02() {
		return nomSeccion02;
	}
	public void setNomSeccion02(String nomSeccion02) {
		this.nomSeccion02 = nomSeccion02;
	}
	public String getCodGrupo() {
		return codGrupo;
	}
	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}
	public String getNomGrupo() {
		return nomGrupo;
	}
	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}
	public String getCodPregunta() {
		return codPregunta;
	}
	public void setCodPregunta(String codPregunta) {
		this.codPregunta = codPregunta;
	}
	public String getNomPregunta() {
		return nomPregunta;
	}
	public void setNomPregunta(String nomPregunta) {
		this.nomPregunta = nomPregunta;
	}
	public String getCodAlternativa() {
		return codAlternativa;
	}
	public void setCodAlternativa(String codAlternativa) {
		this.codAlternativa = codAlternativa;
	}
	public String getNomAlternativa() {
		return nomAlternativa;
	}
	public void setNomAlternativa(String nomAlternativa) {
		this.nomAlternativa = nomAlternativa;
	}
	public String getTotalAlternativa() {
		return totalAlternativa;
	}
	public void setTotalAlternativa(String totalAlternativa) {
		this.totalAlternativa = totalAlternativa;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}	
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodEncuesta() {
		return codEncuesta;
	}
	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}
	public String getDscAlternativa() {
		return dscAlternativa;
	}
	public void setDscAlternativa(String dscAlternativa) {
		this.dscAlternativa = dscAlternativa;
	}
	public String getIdAlternativa() {
		return idAlternativa;
	}
	public void setIdAlternativa(String idAlternativa) {
		this.idAlternativa = idAlternativa;
	}	
}
