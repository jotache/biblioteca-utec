package com.tecsup.SGA.bean;

public class DatosCabeceraBean implements java.io.Serializable{
	private String codTipoAplicacion;
	private String nomTipoAplicacion;
	private String codTipoEncuesta;
	private String nomTipoEncuesta;
	private String nomServicio;
	private String nomEncuesta;
	private String nroNomEncuesta;
	private String duracion;
	private String nomProfesor;
	
	private String totEncuestados;
	private String detEncuestados;
	private String totEncuestadosCrpta;
	private String nomProducto;
	private String nomPrograma;
	
	private String codProducto;
	private String codPrograma;
	private String anioIniEgresado;
	private String anioFinEgresado;
	private String codSede;
	private String nomSede;
	
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getAnioIniEgresado() {
		return anioIniEgresado;
	}
	public void setAnioIniEgresado(String anioIniEgresado) {
		this.anioIniEgresado = anioIniEgresado;
	}
	public String getAnioFinEgresado() {
		return anioFinEgresado;
	}
	public void setAnioFinEgresado(String anioFinEgresado) {
		this.anioFinEgresado = anioFinEgresado;
	}
	public String getTotEncuestados() {
		return totEncuestados;
	}
	public void setTotEncuestados(String totEncuestados) {
		this.totEncuestados = totEncuestados;
	}
	public String getDetEncuestados() {
		return detEncuestados;
	}
	public void setDetEncuestados(String detEncuestados) {
		this.detEncuestados = detEncuestados;
	}
	public String getTotEncuestadosCrpta() {
		return totEncuestadosCrpta;
	}
	public void setTotEncuestadosCrpta(String totEncuestadosCrpta) {
		this.totEncuestadosCrpta = totEncuestadosCrpta;
	}
	public String getNomProducto() {
		return nomProducto;
	}
	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}
	public String getNomPrograma() {
		return nomPrograma;
	}
	public void setNomPrograma(String nomPrograma) {
		this.nomPrograma = nomPrograma;
	}
	public String getCodTipoAplicacion() {
		return codTipoAplicacion;
	}
	public void setCodTipoAplicacion(String codTipoAplicacion) {
		this.codTipoAplicacion = codTipoAplicacion;
	}
	public String getNomTipoAplicacion() {
		return nomTipoAplicacion;
	}
	public void setNomTipoAplicacion(String nomTipoAplicacion) {
		this.nomTipoAplicacion = nomTipoAplicacion;
	}
	public String getCodTipoEncuesta() {
		return codTipoEncuesta;
	}
	public void setCodTipoEncuesta(String codTipoEncuesta) {
		this.codTipoEncuesta = codTipoEncuesta;
	}
	public String getNomTipoEncuesta() {
		return nomTipoEncuesta;
	}
	public void setNomTipoEncuesta(String nomTipoEncuesta) {
		this.nomTipoEncuesta = nomTipoEncuesta;
	}
	public String getNomServicio() {
		return nomServicio;
	}
	public void setNomServicio(String nomServicio) {
		this.nomServicio = nomServicio;
	}
	public String getNomEncuesta() {
		return nomEncuesta;
	}
	public void setNomEncuesta(String nomEncuesta) {
		this.nomEncuesta = nomEncuesta;
	}
	public String getNroNomEncuesta() {
		return nroNomEncuesta;
	}
	public void setNroNomEncuesta(String nroNomEncuesta) {
		this.nroNomEncuesta = nroNomEncuesta;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getNomProfesor() {
		return nomProfesor;
	}
	public void setNomProfesor(String nomProfesor) {
		this.nomProfesor = nomProfesor;
	}
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getNomSede() {
		return nomSede;
	}
	public void setNomSede(String nomSede) {
		this.nomSede = nomSede;
	}
	
	
}
