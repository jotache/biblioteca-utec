package com.tecsup.SGA.bean;

public class Departamento {
	
	private String codDepartamento;	
	private String nomDepartamento;
	
	public String getCodDepartamento() {
		return codDepartamento;
	}
	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}
	public String getNomDepartamento() {
		return nomDepartamento;
	}
	public void setNomDepartamento(String nomDepartamento) {
		this.nomDepartamento = nomDepartamento;
	}	
	
}
