package com.tecsup.SGA.bean;

public class HistorialProrrogaBean {

	private String fecPrestamo;
	private String fecDevolucionProg;
	private String fecDevolucion;
	private String flagProrroga;
	private String codAsistente;
	private String nomAsistente;
	
	public String getFecPrestamo() {
		return fecPrestamo;
	}
	public void setFecPrestamo(String fecPrestamo) {
		this.fecPrestamo = fecPrestamo;
	}
	public String getFecDevolucionProg() {
		return fecDevolucionProg;
	}
	public void setFecDevolucionProg(String fecDevolucionProg) {
		this.fecDevolucionProg = fecDevolucionProg;
	}
	public String getFecDevolucion() {
		return fecDevolucion;
	}
	public void setFecDevolucion(String fecDevolucion) {
		this.fecDevolucion = fecDevolucion;
	}
	
	public String getFlagProrroga() {
		return flagProrroga;
	}
	public void setFlagProrroga(String flagProrroga) {
		this.flagProrroga = flagProrroga;
	}
	public String getCodAsistente() {
		return codAsistente;
	}
	public void setCodAsistente(String codAsistente) {
		this.codAsistente = codAsistente;
	}
	public String getNomAsistente() {
		return nomAsistente;
	}
	public void setNomAsistente(String nomAsistente) {
		this.nomAsistente = nomAsistente;
	}
	@Override
	public String toString() {
		return "FePre:"+fecPrestamo+"-FeDev:"+fecDevolucion+"-PRORROGA:"+flagProrroga;
	}

}
