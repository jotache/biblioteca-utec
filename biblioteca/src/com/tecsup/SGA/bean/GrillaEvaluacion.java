package com.tecsup.SGA.bean;

import java.util.List;

public class GrillaEvaluacion {
	private String nomCiclo;
	private String promCiclo;
	private String nroRankin;
	private String nomRankin;
	private List lstCursos;

	public String getNomCiclo() {
		return nomCiclo;
	}

	public void setNomCiclo(String nomCiclo) {
		this.nomCiclo = nomCiclo;
	}

	public String getPromCiclo() {
		return promCiclo;
	}

	public void setPromCiclo(String promCiclo) {
		this.promCiclo = promCiclo;
	}

	public String getNroRankin() {
		return nroRankin;
	}

	public void setNroRankin(String nroRankin) {
		this.nroRankin = nroRankin;
	}

	public List getLstCursos() {
		return lstCursos;
	}

	public void setLstCursos(List lstCursos) {
		this.lstCursos = lstCursos;
	}

	public String getNomRankin() {
		return nomRankin;
	}

	public void setNomRankin(String nomRankin) {
		this.nomRankin = nomRankin;
	}

}
