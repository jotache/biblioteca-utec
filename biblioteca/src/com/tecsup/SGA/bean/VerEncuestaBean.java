package com.tecsup.SGA.bean;

import java.util.List;

public class VerEncuestaBean implements java.io.Serializable {
	private String idEncuesta;
	private String idSeccion;
	private String nomSeccion;
	private String idGrupo;
	private String nomGrupo;
	private String nivelAlternativa;
	private String idPregunta;
	private String nomPregunta;
	private String idAlternativa;
	private String nomAlterntiva;
	private String desAlternativa;
	private String indObligatorio;
	private String tipoPregunta;
	private String indUnica;
	
	//lista de alternativas
	private List lstAlternativas;
	
	
	public List getLstAlternativas() {
		return lstAlternativas;
	}
	public void setLstAlternativas(List lstAlternativas) {
		this.lstAlternativas = lstAlternativas;
	}
	public String getIdEncuesta() {
		return idEncuesta;
	}
	public void setIdEncuesta(String idEncuesta) {
		this.idEncuesta = idEncuesta;
	}
	public String getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}
	public String getNomSeccion() {
		return nomSeccion;
	}
	public void setNomSeccion(String nomSeccion) {
		this.nomSeccion = nomSeccion;
	}
	public String getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}
	public String getNomGrupo() {
		return nomGrupo;
	}
	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}
	public String getNivelAlternativa() {
		return nivelAlternativa;
	}
	public void setNivelAlternativa(String nivelAlternativa) {
		this.nivelAlternativa = nivelAlternativa;
	}
	public String getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(String idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getNomPregunta() {
		return nomPregunta;
	}
	public void setNomPregunta(String nomPregunta) {
		this.nomPregunta = nomPregunta;
	}
	public String getIdAlternativa() {
		return idAlternativa;
	}
	public void setIdAlternativa(String idAlternativa) {
		this.idAlternativa = idAlternativa;
	}
	public String getNomAlterntiva() {
		return nomAlterntiva;
	}
	public void setNomAlterntiva(String nomAlterntiva) {
		this.nomAlterntiva = nomAlterntiva;
	}
	public String getDesAlternativa() {
		return desAlternativa;
	}
	public void setDesAlternativa(String desAlternativa) {
		this.desAlternativa = desAlternativa;
	}
	public String getIndObligatorio() {
		return indObligatorio;
	}
	public void setIndObligatorio(String indObligatorio) {
		this.indObligatorio = indObligatorio;
	}
	public String getTipoPregunta() {
		return tipoPregunta;
	}
	public void setTipoPregunta(String tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}
	public String getIndUnica() {
		return indUnica;
	}
	public void setIndUnica(String indUnica) {
		this.indUnica = indUnica;
	}


	
	
}
