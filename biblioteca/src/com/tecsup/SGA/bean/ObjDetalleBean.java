package com.tecsup.SGA.bean;

import java.util.List;

public class ObjDetalleBean {
	
	private List precios;
	
	private String codCoti;
	private String codCotiDet;
	private String dscFam;
	private String dscSfam;
	private String dscBien;
	private String cantidad;
	private String preUltiOrden;
	private String canUltiOrden;
	private String codUltiOrden;
	private String unidad;//jhpr 2008-09-29
	//ALQD,10/07/09.NUEVA PROPIEDAD
	private String tieneComentario;
	
	public String getTieneComentario() {
		return tieneComentario;
	}
	public void setTieneComentario(String tieneComentario) {
		this.tieneComentario = tieneComentario;
	}
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public String getPreUltiOrden() {
		return preUltiOrden;
	}

	public void setPreUltiOrden(String preUltiOrden) {
		this.preUltiOrden = preUltiOrden;
	}

	public String getCanUltiOrden() {
		return canUltiOrden;
	}

	public void setCanUltiOrden(String canUltiOrden) {
		this.canUltiOrden = canUltiOrden;
	}

	public String getCodUltiOrden() {
		return codUltiOrden;
	}

	public void setCodUltiOrden(String codUltiOrden) {
		this.codUltiOrden = codUltiOrden;
	}

	public String getCodCoti() {
		return codCoti;
	}

	public void setCodCoti(String codCoti) {
		this.codCoti = codCoti;
	}

	public String getCodCotiDet() {
		return codCotiDet;
	}

	public void setCodCotiDet(String codCotiDet) {
		this.codCotiDet = codCotiDet;
	}

	public String getDscFam() {
		return dscFam;
	}

	public void setDscFam(String dscFam) {
		this.dscFam = dscFam;
	}

	public String getDscSfam() {
		return dscSfam;
	}

	public void setDscSfam(String dscSfam) {
		this.dscSfam = dscSfam;
	}

	public String getDscBien() {
		return dscBien;
	}

	public void setDscBien(String dscBien) {
		this.dscBien = dscBien;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public List getPrecios() {
		return precios;
	}

	public void setPrecios(List precios) {
		this.precios = precios;
	}



}
