package com.tecsup.SGA.bean;

public class DescriptorxMaterialBean {
private String codUnico;
private String fecRegistro;
private String codDescriptor;
private String desDescriptor;
private String codUsuario;
private String usuario;
public String getCodUnico() {
	return codUnico;
}
public void setCodUnico(String codUnico) {
	this.codUnico = codUnico;
}
public String getFecRegistro() {
	return fecRegistro;
}
public void setFecRegistro(String fecRegistro) {
	this.fecRegistro = fecRegistro;
}
public String getCodDescriptor() {
	return codDescriptor;
}
public void setCodDescriptor(String codDescriptor) {
	this.codDescriptor = codDescriptor;
}
public String getDesDescriptor() {
	return desDescriptor;
}
public void setDesDescriptor(String desDescriptor) {
	this.desDescriptor = desDescriptor;
}
public String getCodUsuario() {
	return codUsuario;
}
public void setCodUsuario(String codUsuario) {
	this.codUsuario = codUsuario;
}
public String getUsuario() {
	return usuario;
}
public void setUsuario(String usuario) {
	this.usuario = usuario;
}

}
