package com.tecsup.SGA.bean;

public class DetalleExamenes {
	private String codigo;
	private String nombre;
	private String nota;
	private String descExamen;
	private String flgNP; // no se presento
	private String flgAN; // anulado
	private String flgSU; // susti

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getDescExamen() {
		return descExamen;
	}

	public void setDescExamen(String descExamen) {
		this.descExamen = descExamen;
	}

	public String getFlgNP() {
		return flgNP;
	}

	public void setFlgNP(String flgNP) {
		this.flgNP = flgNP;
	}

	public String getFlgAN() {
		return flgAN;
	}

	public void setFlgAN(String flgAN) {
		this.flgAN = flgAN;
	}

	public String getFlgSU() {
		return flgSU;
	}

	public void setFlgSU(String flgSU) {
		this.flgSU = flgSU;
	}

}
