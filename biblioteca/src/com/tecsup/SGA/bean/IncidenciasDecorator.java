package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;
import com.tecsup.SGA.modelo.Incidencia;
import com.tecsup.SGA.bean.IncidenciaBean;

public class IncidenciasDecorator extends TableDecorator {
	private Incidencia incidencia;
	private IncidenciaBean incidenciaBean;
	
	@Override
	public String addRowClass() {
		return "texto";
	}
	
	public String getRbtAlumnoDoc()
	{
		incidenciaBean = (IncidenciaBean) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codAlumno\"  " +
					"onclick=\"javascript:fc_selAlumno('" + incidenciaBean.getCodAlumno().trim() + "','" +
					incidenciaBean.getNomAlumno().trim() + "');\" >" ;
		return str;

	}	
	
	public String getTextAreaIncidencia()
	{
		incidencia = (Incidencia) getCurrentRowObject();
		return "<textarea style=\"width:90%\" class=\"cajatexto_1\" rows=\"3\" onKeyPress=\"return false;\" >" + incidencia.getDscIncidencia() + "</textarea>";
		//cols=\"70\" 
	}
	
	public String getImgEliminarIncidenciaDoc()
	{
		incidencia = (Incidencia) getCurrentRowObject();
		return "<img src=\"/biblioteca/images/iconos/quitar1.jpg\" style=\"cursor:hand\" alt=\"Eliminar\" " +
				" onClick=\"javascript:fc_Eliminar('" + incidencia.getCodIincidencia() + "');\" " +
				" >";
	}
	
	public String getImgModificarIncidenciaDoc()
	{
		incidencia = (Incidencia) getCurrentRowObject();
		return "<img src=\"/biblioteca/images/iconos/modificar1.jpg\" style=\"cursor:hand\" alt=\"Modificar\" " +
				" onClick=\"javascript:fc_Modificar('" + incidencia.getCodIincidencia() + "','" +
				incidencia.getFecha() + "',this,'"
				+ incidencia.getCodTipoIncidencia() + "','"
				+ incidencia.getFechaIni() + "','"
				+ incidencia.getFechaFin() + "');\" />";
	}	
}
