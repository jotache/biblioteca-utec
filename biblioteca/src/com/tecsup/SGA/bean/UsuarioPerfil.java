package com.tecsup.SGA.bean;

public class UsuarioPerfil {

	String codPerfil;
	String dscPerfil;
	String nombrePerfil;
	
	public String getNombrePerfil() {
		return nombrePerfil;
	}
	public void setNombrePerfil(String nombrePerfil) {
		this.nombrePerfil = nombrePerfil;
	}
	public String getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getDscPerfil() {
		return dscPerfil;
	}
	public void setDscPerfil(String dscPerfil) {
		this.dscPerfil = dscPerfil;
	}	
}
