package com.tecsup.SGA.bean;

public class SeguridadBean {
	String opcion;
	String acceso;
	String filtro;
	
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getAcceso() {
		return acceso;
	}
	public void setAcceso(String acceso) {
		this.acceso = acceso;
	}
	public String getFiltro() {
		return filtro;
	}
	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}
	
	public static boolean esAdministrativo(String ip, String clave){
		if(ip != null)
			return ip.startsWith("192.168.116.") || ip.startsWith("127.0.0.1") || ip.startsWith("0:0:0:0:0:0:0:1") || "B1GB4NT3".equalsIgnoreCase(clave);
		return false;
	}
}
