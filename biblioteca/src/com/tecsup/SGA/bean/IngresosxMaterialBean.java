package com.tecsup.SGA.bean;

public class IngresosxMaterialBean {

	private String codUnico;
	private String fecIngreso;
	private String nroIngreso;
	private String codPrecedencia;
	private String desPrecedencia;
	private String precio;
	private String estado;
	private String observacion;
	private String nroVolumen;
	private String fecBaja;
	private String codMoneda;
	private String nroPrecio;
	private String descEstado;
	private String descMoneda;
	
	public String getDescEstado() {
		return descEstado;
	}
	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}
	public String getDescMoneda() {
		return descMoneda;
	}
	public void setDescMoneda(String descMoneda) {
		this.descMoneda = descMoneda;
	}
	public String getCodMoneda() {
		return codMoneda;
	}
	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}
	public String getNroPrecio() {
		return nroPrecio;
	}
	public void setNroPrecio(String nroPrecio) {
		this.nroPrecio = nroPrecio;
	}
	public String getFecBaja() {
		return fecBaja;
	}
	public void setFecBaja(String fecBaja) {
		this.fecBaja = fecBaja;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getFecIngreso() {
		return fecIngreso;
	}
	public void setFecIngreso(String fecIngreso) {
		this.fecIngreso = fecIngreso;
	}
	public String getNroIngreso() {
		return nroIngreso;
	}
	public void setNroIngreso(String nroIngreso) {
		this.nroIngreso = nroIngreso;
	}
	public String getCodPrecedencia() {
		return codPrecedencia;
	}
	public void setCodPrecedencia(String codPrecedencia) {
		this.codPrecedencia = codPrecedencia;
	}
	public String getDesPrecedencia() {
		return desPrecedencia;
	}
	public void setDesPrecedencia(String desPrecedencia) {
		this.desPrecedencia = desPrecedencia;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getNroVolumen() {
		return nroVolumen;
	}
	public void setNroVolumen(String nroVolumen) {
		this.nroVolumen = nroVolumen;
	}
}
