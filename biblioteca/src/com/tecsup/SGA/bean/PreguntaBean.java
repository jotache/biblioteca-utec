package com.tecsup.SGA.bean;

import java.util.List;

public class PreguntaBean {
	private String codEncuesta;
	private String codPregunta;
	private String flgObligatorio;
	private String flgMultiple;
	private String flgCombo;//1 COMBO:0 NO COMBO
	private String desPregunta;
	
	private List lstOpciones;//OPCIONES DE LA PREGUNTA

	public String getCodEncuesta() {
		return codEncuesta;
	}

	public void setCodEncuesta(String codEncuesta) {
		this.codEncuesta = codEncuesta;
	}

	public String getCodPregunta() {
		return codPregunta;
	}

	public void setCodPregunta(String codPregunta) {
		this.codPregunta = codPregunta;
	}

	public String getFlgObligatorio() {
		return flgObligatorio;
	}

	public void setFlgObligatorio(String flgObligatorio) {
		this.flgObligatorio = flgObligatorio;
	}

	public String getFlgMultiple() {
		return flgMultiple;
	}

	public void setFlgMultiple(String flgMultiple) {
		this.flgMultiple = flgMultiple;
	}

	public List getLstOpciones() {
		return lstOpciones;
	}

	public void setLstOpciones(List lstOpciones) {
		this.lstOpciones = lstOpciones;
	}

	public String getFlgCombo() {
		return flgCombo;
	}

	public void setFlgCombo(String flgCombo) {
		this.flgCombo = flgCombo;
	}

	public String getDesPregunta() {
		return desPregunta;
	}

	public void setDesPregunta(String desPregunta) {
		this.desPregunta = desPregunta;
	}

}
