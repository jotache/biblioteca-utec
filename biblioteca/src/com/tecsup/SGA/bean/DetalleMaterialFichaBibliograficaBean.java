package com.tecsup.SGA.bean;

public class DetalleMaterialFichaBibliograficaBean {

	private String codMaterial;
	private String codTipoMaterial; //JHPR 2008-7-3
	private String nomTipoMaterial;
	private String nomMaterial;
	private String nomAutor;
	private String nomDewey;
	private String nroPags;
	private String nomEditorial;
	private String nroEdicion;
	private String fecPublicacion;
	private String nomIdioma;
	private String nroIsbn;
	private String indSala;
	private String indDomicilio;
	private String indReserva;
	private String ubicacionMaterial;
	private String nroEjemplares;
	
	private String codigoGenerado;
	private String fechaPublicacion;
	private String dscCiudad;
	private String dscIsbn;
	private String nroIngresos;
	private String mensionSeries;
	private String dscDescriptores;
	private String contenido;

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public String getCodMaterial() {
		return codMaterial;
	}

	public void setCodMaterial(String codMaterial) {
		this.codMaterial = codMaterial;
	}

	public String getNomTipoMaterial() {
		return nomTipoMaterial;
	}

	public void setNomTipoMaterial(String nomTipoMaterial) {
		this.nomTipoMaterial = nomTipoMaterial;
	}

	public String getNomMaterial() {
		return nomMaterial;
	}

	public void setNomMaterial(String nomMaterial) {
		this.nomMaterial = nomMaterial;
	}

	public String getNomAutor() {
		return nomAutor;
	}

	public void setNomAutor(String nomAutor) {
		this.nomAutor = nomAutor;
	}

	public String getNomDewey() {
		return nomDewey;
	}

	public void setNomDewey(String nomDewey) {
		this.nomDewey = nomDewey;
	}

	public String getNroPags() {
		return nroPags;
	}

	public void setNroPags(String nroPags) {
		this.nroPags = nroPags;
	}

	public String getNomEditorial() {
		return nomEditorial;
	}

	public void setNomEditorial(String nomEditorial) {
		this.nomEditorial = nomEditorial;
	}

	public String getNroEdicion() {
		return nroEdicion;
	}

	public void setNroEdicion(String nroEdicion) {
		this.nroEdicion = nroEdicion;
	}

	public String getFecPublicacion() {
		return fecPublicacion;
	}

	public void setFecPublicacion(String fecPublicacion) {
		this.fecPublicacion = fecPublicacion;
	}

	public String getNomIdioma() {
		return nomIdioma;
	}

	public void setNomIdioma(String nomIdioma) {
		this.nomIdioma = nomIdioma;
	}

	public String getNroIsbn() {
		return nroIsbn;
	}

	public void setNroIsbn(String nroIsbn) {
		this.nroIsbn = nroIsbn;
	}

	public String getIndSala() {
		return indSala;
	}

	public void setIndSala(String indSala) {
		this.indSala = indSala;
	}

	public String getIndDomicilio() {
		return indDomicilio;
	}

	public void setIndDomicilio(String indDomicilio) {
		this.indDomicilio = indDomicilio;
	}

	public String getIndReserva() {
		return indReserva;
	}

	public void setIndReserva(String indReserva) {
		this.indReserva = indReserva;
	}

	public String getUbicacionMaterial() {
		return ubicacionMaterial;
	}

	public void setUbicacionMaterial(String ubicacionMaterial) {
		this.ubicacionMaterial = ubicacionMaterial;
	}

	public String getNroEjemplares() {
		return nroEjemplares;
	}

	public void setNroEjemplares(String nroEjemplares) {
		this.nroEjemplares = nroEjemplares;
	}

	public String getCodigoGenerado() {
		return codigoGenerado;
	}

	public void setCodigoGenerado(String codigoGenerado) {
		this.codigoGenerado = codigoGenerado;
	}

	public String getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public String getDscCiudad() {
		return dscCiudad;
	}

	public void setDscCiudad(String dscCiudad) {
		this.dscCiudad = dscCiudad;
	}

	public String getDscIsbn() {
		return dscIsbn;
	}

	public void setDscIsbn(String dscIsbn) {
		this.dscIsbn = dscIsbn;
	}

	public String getNroIngresos() {
		return nroIngresos;
	}

	public void setNroIngresos(String nroIngresos) {
		this.nroIngresos = nroIngresos;
	}

	public String getMensionSeries() {
		return mensionSeries;
	}

	public void setMensionSeries(String mensionSeries) {
		this.mensionSeries = mensionSeries;
	}

	public String getDscDescriptores() {
		return dscDescriptores;
	}

	public void setDscDescriptores(String dscDescriptores) {
		this.dscDescriptores = dscDescriptores;
	}

	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}

	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}

}
