package com.tecsup.SGA.bean;

public class CotizacionesExistentesBean {
	
	private String nroCotizacion;
	private String fechaCotizacion;
	private String fechaInicioVigencia;
	private String fechaFinVigencia;
	private String dscTipoPago;
	
	public String getNroCotizacion() {
		return nroCotizacion;
	}
	public void setNroCotizacion(String nroCotizacion) {
		this.nroCotizacion = nroCotizacion;
	}
	public String getFechaCotizacion() {
		return fechaCotizacion;
	}
	public void setFechaCotizacion(String fechaCotizacion) {
		this.fechaCotizacion = fechaCotizacion;
	}
	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public String getDscTipoPago() {
		return dscTipoPago;
	}
	public void setDscTipoPago(String dscTipoPago) {
		this.dscTipoPago = dscTipoPago;
	}

}
