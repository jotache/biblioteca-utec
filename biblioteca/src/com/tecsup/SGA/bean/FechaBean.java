package com.tecsup.SGA.bean;

public class FechaBean implements java.io.Serializable {
	private String fecha;
	private String hora;
	private String minuto;
	private String horaMinuto;
	private String segundo;
	
	public String getHoraMinuto() {
		return horaMinuto;
	}
	public void setHoraMinuto(String horaMinuto) {
		this.horaMinuto = horaMinuto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getMinuto() {
		return minuto;
	}
	public void setMinuto(String minuto) {
		this.minuto = minuto;
	}
	public String getSegundo() {
		return segundo;
	}
	public void setSegundo(String segundo) {
		this.segundo = segundo;
	}
	
}
