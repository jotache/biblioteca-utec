package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.*;

public class ParametrosGeneralesDecorator extends TableDecorator{
	
	//private ParametrosGenerales pobject;
	private Producto tobject;
	private TipoTablaDetalle lobject;
	private Periodo pobject;
	private ParametrosGenerales pgobject;
	
	public String getCboSelProducto()
	{
		tobject = (Producto) getCurrentRowObject();
		String str ="<input type=\"combo\" align\"=center\" name=\"CodTipoTipo\" value=\"" + tobject.getCodProducto()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + tobject.getDescripcion() +  "');\" >" ;
        return str;
	}
	public String getCboSelPeriodo()
	{
		pobject = (Periodo) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"CodTipoTipo\" value=\"" + pobject.getCodigo()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + pobject.getTipo() +  "');\" >" ;
        return str;
	}
		
	public String getTextValor(){
		pgobject = (ParametrosGenerales) getCurrentRowObject();
		int i= getListIndex();
		String aa=pgobject.getValor();
	   	if(aa==null) aa="";
		if(i==0)
		{
			
		  	String str ="<input maxlength=\"10\"  type=\"text\" class=\"cajatexto\" readonly=\"true\" size=\"15\"  name=\"text"+i+"\" value=\"" + aa//pgobject.getValor() 
		    + "\" id=\"text"+i+"\"  " + " onblur=\"fc_ValidaFechaOnblur('text"+i+"');\" " +
		    "onkeypress=\"javascript:fc_Slash('frmMain','text"+i+"','/');\" >" +
		    " <img src=\"/biblioteca/images/iconos/calendario1.jpg\" name=\"imgFecInc1\" id=\"imgFecInc1\" alt=\"Calendario\" disabled=\"false\"  style=\'cursor:hand;\' " +
		    "align=\"absmiddle\" onclick=\"javascript:fcCalendario1();\"> ";
			//System.out.println("ta11"+">>"+str+">>");
			return str;
	    }
		else
		{	String str ="<input maxlength=\"2\" type=\"text\" class=\"cajatexto\" readonly=\"true\" size=\"40\" name=\"text"+i+"\" value=\"" + aa//mad //lobject.getValor
		   	+ "\" id=\"text"+i+"\"  " + "onblur=\"fc_ValidaNumeroOnBlur('text"+i+"');\"  onkeypress=\"fc_PermiteNumeros();\" >";
   		    //System.out.println("ta12"+">>"+str+">>");
		    return str;
		    
		}
		
	}
	
	public String getTextValor1(){
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		int i= getListIndex();
		
		if(i==0)
		{
			String str=" <input maxlength=\"10\"  type=\"text\" class=\"cajatexto\" readonly=\"true\" size=\"15\" name=\"text1"+i+"\" value=\"" + ""
			   + "\" id=\"text1"+i+"\" " + " onblur=\"fc_ValidaFechaOnblur(this.id);\" "+
			   "onkeypress=\"javascript:fc_ValidaFecha('text1"+i+"');\" >" +
		       " <img src=\"/biblioteca/images/iconos/calendario1.jpg\" name=\"imgFecInc2\" id=\"imgFecInc2\" alt=\"Calendario\" disabled=\"false\"  style=\'cursor:hand;\' " +
		       "align=\"absmiddle\" onclick=\"javascript:fcCalendario2();\"> ";
				//System.out.println("ta21"+">>"+str+">>");
				return str;
				//text1"+i+"
		}
		else
		{ String str ="<input maxlength=\"2\" type=\"text\" class=\"cajatexto\" readonly=\"true\" size=\"40\" name=\"text1"+i+"\" value=\"" + ""//mad //lobject.getValor
   	      + "\" id=\"text1"+i+"\"  " + " onblur=\"fc_ValidaNumeroOnBlur('text1"+i+"');\" onkeypress=\"fc_PermiteNumeros();\" >";
    	  //System.out.println("ta22"+">>"+str+">>");
		  return str;
		 
	    }	
		}
   
	
}
