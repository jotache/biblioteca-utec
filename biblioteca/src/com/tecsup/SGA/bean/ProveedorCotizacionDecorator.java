/**
 * 
 */
package com.tecsup.SGA.bean;

import java.util.*;
import org.displaytag.decorator.TableDecorator;
import com.tecsup.SGA.modelo.CotizacionProveedor;

/**
 * @author       CosapiSoft S.A.
 * @date         10-oct-07
 * @description  none
 */

public class ProveedorCotizacionDecorator extends TableDecorator{
	
	private CotizacionProveedor lObject;
	
	public String addRowClass()
	{
		return "texto";
	}
	
	public String getRbtSelProveedor(){
		lObject = (CotizacionProveedor) getCurrentRowObject();
		int i= getListIndex();
		
		String str ="<input type=\"radio\" name=\"rbtProveedor\" id=\"rbtProveedor"+i+"\" " +
					"onclick=\"javascript:fc_seleccionarRegistro('" + lObject.getCodProv() + "');\" >" ;
        return str;
	}
	
	public String getCheckSelBandeja(){
		lObject = (CotizacionProveedor) getCurrentRowObject();
		int i= getListIndex();
		
		String str ="<input type=\"checkbox\" id=\"chkIndEnvCorreo"+i+"\" name=\"chkIndEnvCorreo"+i+"\" " ;
		if ( lObject.getIndEnvioCorreo() != null )
		{	System.out.println("Renzo<<"+lObject.getIndEnvioCorreo()+">>Atila");
			if ( lObject.getIndEnvioCorreo().trim().equals("1")/*|| lObject.getIndEnvioCorreo().trim().equals("#")*/)
				str = str + " checked "; 
		}
		str = str + "onclick=\"javascript:fc_CambiaIndicaEnvCorreo();\" " +
				"value=\"" + lObject.getIndEnvioCorreo()+"\">" ;
		System.out.println("<<"+str+">>");
        return str;
	}
	
	
}
