package com.tecsup.SGA.bean;

public class HistorialBean {

	private String codUsuario;
	private String nomUsuario;
	private String usuario;
	private String fecPrestamo;
	private String fecPrestamoProg;
	private String fecDevolucion;
	private String nroIngreso;

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getNomUsuario() {
		return nomUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFecPrestamo() {
		return fecPrestamo;
	}

	public void setFecPrestamo(String fecPrestamo) {
		this.fecPrestamo = fecPrestamo;
	}

	public String getFecPrestamoProg() {
		return fecPrestamoProg;
	}

	public void setFecPrestamoProg(String fecPrestamoProg) {
		this.fecPrestamoProg = fecPrestamoProg;
	}

	public String getFecDevolucion() {
		return fecDevolucion;
	}

	public void setFecDevolucion(String fecDevolucion) {
		this.fecDevolucion = fecDevolucion;
	}

	public String getNroIngreso() {
		return nroIngreso;
	}

	public void setNroIngreso(String nroIngreso) {
		this.nroIngreso = nroIngreso;
	}

}
