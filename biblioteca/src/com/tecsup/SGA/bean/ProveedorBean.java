package com.tecsup.SGA.bean;

public class ProveedorBean {
private String codProv;
private String nroDoc;
private String razonSocial;
//ALQD,04/06/09.NUEVA PROPIEDAD
private String tipoProveedor;
public String getTipoProveedor() {
	return tipoProveedor;
}
public void setTipoProveedor(String tipoProveedor) {
	this.tipoProveedor=tipoProveedor;
}
public String getCodProv() {
	return codProv;
}
public void setCodProv(String codProv) {
	this.codProv = codProv;
}
public String getNroDoc() {
	return nroDoc;
}
public void setNroDoc(String nroDoc) {
	this.nroDoc = nroDoc;
}
public String getRazonSocial() {
	return razonSocial;
}
public void setRazonSocial(String razonSocial) {
	this.razonSocial = razonSocial;
}
}
