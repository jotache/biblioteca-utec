package com.tecsup.SGA.bean;

import java.util.*;

public class ProcesoEvaluadorBean implements java.io.Serializable {
	/*	ATRIBUTOS	*/
	private String CodPadre;
	private String codDetalle;
	private String descripcion;
	private String valor1;
	private List Evaluadores;
	private String codEvaluadorSel;
	private String observacion;
	private String dscEliminatoria;
	
	/*	GETTERS Y SETTERS	*/
	
	public String getCodPadre() {
		return CodPadre;
	}
	public void setCodPadre(String codPadre) {
		CodPadre = codPadre;
	}
	public String getCodDetalle() {
		return codDetalle;
	}
	public void setCodDetalle(String codDetalle) {
		this.codDetalle = codDetalle;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getValor1() {
		return valor1;
	}
	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}
	public List getEvaluadores() {
		return Evaluadores;
	}
	public void setEvaluadores(List evaluadores) {
		Evaluadores = evaluadores;
	}
	public String getCodEvaluadorSel() {
		return codEvaluadorSel;
	}
	public void setCodEvaluadorSel(String codEvaluadorSel) {
		this.codEvaluadorSel = codEvaluadorSel;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getDscEliminatoria() {
		return dscEliminatoria;
	}
	public void setDscEliminatoria(String dscEliminatoria) {
		this.dscEliminatoria = dscEliminatoria;
	}
	
	
}
