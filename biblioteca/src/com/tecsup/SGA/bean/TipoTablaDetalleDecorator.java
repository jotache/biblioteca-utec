package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.modelo.ProcesoAreasEstudios;
import com.tecsup.SGA.modelo.ProcesoAreasNivelEstudios;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.TipoDetalle;

public class TipoTablaDetalleDecorator extends TableDecorator {
	private TipoTablaDetalle lobject;
	private ProcesoAreasEstudios paeObject;
	private ProcesoAreasNivelEstudios panObject;
	private TipoDetalle tobject;
	private Producto probject;
	private Periodo peobject;

	public String getChkSelSancion()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codSancion\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value);\" >" ;        
		return str;
	}
	
	public String getChkSelDetalle()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() +  "');\" >" ;
        System.out.println("<<"+lobject.getCodTipoTablaDetalle()+">>"+lobject.getDescripcion());
		return str;
	}
	
	public String getChkSelDetalle2()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion()  + "','" + lobject.getDscValor1() + "','" + lobject.getDscValor3() + "');\" >" ;
        return str;
	}
	public String getChkSelDetalle22()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro2(this.value,'" + lobject.getDescripcion()  + "','" + lobject.getDscValor1() + "','" + lobject.getDscValor2() + "');\" >" ;
        return str;
	}
	
	public String getCboSelProducto()
	{
		probject = (Producto) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"CodTipoTipo\" value=\"" + probject.getCodProducto()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + probject.getDescripcion() +  "');\" >" ;
        return str;
	}
	public String getCboSelPeriodo()
	{
		peobject = (Periodo) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"CodTipoTipo\" value=\"" + peobject.getCodigo()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + peobject.getTipo() +  "');\" >" ;
        return str;
	}
	
	public String getRbtSelDetalle()
	{   int i= getListIndex();
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" id=\"text"+i+"\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() +  "');\" >" +
					"<input type=\"hidden\" name=\"hid"+i+"\" id=\"hid"+i+"\" value=\"" + lobject.getDescripcion()+"\" " ;
        return str;
	}	
   
	
	
	public String getRbtSelDetalle2()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro2(this.value,'" + lobject.getDescripcion() +  "');\" >" ;
        return str;
	}
	
	public String getRbtSelDetalle3()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDscValor1() +  "');\" >" ;
        return str;
	}
	
	public String getCboSelTipo()
	{
		tobject = (TipoDetalle) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"CodTipoTipo\" value=\"" + tobject.getCodTipoTipo()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + tobject.getDescripcionTipo() +  "');\" >" ;
        return str;
	}	
	
	
	public String getCboSelRevision()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"codSelRevision\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + lobject.getDescripcion() +  "');\" >" ;
        return str;
	}
	
	public String getCboSelCalificacion()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"codSelRevision\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + lobject.getDescripcion() +  "');\" >" ;
        return str;
	}
	
	
	
	public String getlistCodEvaluacion()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"list\" name=\"codEvaluacion\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ " \" >" ;
        return str;
	}


	public String getRbtSelObject()
	{
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codDetalle\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarObject(this,'" + lobject.getCodTipoTablaDetalle() + "');\" >" ;
        return str;
	}	
	
	public String getRbtSelEstObject(){
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codAreaEstudio\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarEstObject(this,'" + lobject.getCodTipoTablaDetalle() + "');\" >" ;
        return str;		
	}
	
	public String getRbtSelNivEstObject(){
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codAreaNivelEstudio\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarNivEstObject(this,'" + lobject.getCodTipoTablaDetalle() + "');\" >" ;
        return str;		
	}
	
	public String getRbtSelInstEduObject(){
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codNivelEstudio\" value=\"" + lobject.getCodTipoTablaDetalle()
					+ "\"  onclick=\"javascript:fc_seleccionarInstEduObject(this,'" + lobject.getCodTipoTablaDetalle() + "');\" >" ;
        return str;		
	}
}
