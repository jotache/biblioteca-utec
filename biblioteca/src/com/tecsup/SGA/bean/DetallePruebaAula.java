package com.tecsup.SGA.bean;

public class DetallePruebaAula {
	private String codAlumno;
	private String nombre;
	private String nroEval;
	private String codCurso;
	private String codTipoExamenParcial;
	private String nota;
	private String promedio;
	private String tipoNota; // anulado, no se presento, pendiente

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNroEval() {
		return nroEval;
	}

	public void setNroEval(String nroEval) {
		this.nroEval = nroEval;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public String getCodTipoExamenParcial() {
		return codTipoExamenParcial;
	}

	public void setCodTipoExamenParcial(String codTipoExamenParcial) {
		this.codTipoExamenParcial = codTipoExamenParcial;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getPromedio() {
		return promedio;
	}

	public void setPromedio(String promedio) {
		this.promedio = promedio;
	}

	public String getTipoNota() {
		return tipoNota;
	}

	public void setTipoNota(String tipoNota) {
		this.tipoNota = tipoNota;
	}

}
