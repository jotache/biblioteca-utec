package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;
import com.tecsup.SGA.modelo.CompetenciasPerfil;
import com.tecsup.SGA.common.CommonConstants;

public class CompetenciasDecorator extends TableDecorator {
	private CompetenciasPerfil competenciasPerfil;
	
	@Override
	public String addRowClass() {
		return "texto";
	}
	public String getRbtExcelente()
	{
		competenciasPerfil = (CompetenciasPerfil) getCurrentRowObject();
		int indice = this.getListIndex();
		String valor = "";
		String str ="<input type=\"radio\" name=\"codEval" + Integer.toString(indice) + "\"  ";
		
		if ( competenciasPerfil.getFlagTipoEval().trim().equals("1") )
		{
			valor = competenciasPerfil.getCodEvalIni().trim();			
			if ( competenciasPerfil.getCodEvalIni().trim().equals(CommonConstants.COMP_CAL_EXCELENTE) )
				str = str + " checked ";
		}
		else if ( competenciasPerfil.getFlagTipoEval().trim().equals("2") )
		{
			valor = competenciasPerfil.getCodEvalFin().trim();
			if ( competenciasPerfil.getCodEvalFin().trim().equals(CommonConstants.COMP_CAL_EXCELENTE))
				str = str + " checked ";			
		}
		
		str = str + "onclick=\"javascript:fc_selCalificacion(this,'" + 
					Integer.toString(indice) + "','" +
					CommonConstants.COMP_CAL_EXCELENTE + "');\" >" ;
		str = str + "<input type=\"hidden\" id=\"txhCodComp" + Integer.toString(indice) + "\" value=\"" + competenciasPerfil.getCodCompetencia() + "\" />";
		str = str + "<input type=\"hidden\" id=\"txhCodCal" + Integer.toString(indice) + "\" value=\"" + valor + "\" />";
		str = str + "<input type=\"hidden\" id=\"txhCodCalIni" + Integer.toString(indice) + "\" value=\"" + competenciasPerfil.getCodEvalIni() + "\" />";
		return str;
	}
	
	public String getRbtBueno()
	{
		competenciasPerfil = (CompetenciasPerfil) getCurrentRowObject();
		int indice = this.getListIndex();
		String str ="<input type=\"radio\" name=\"codEval" + Integer.toString(indice) + "\"  ";
		
		if ( competenciasPerfil.getFlagTipoEval().trim().equals("1") &&  
			competenciasPerfil.getCodEvalIni().trim().equals(CommonConstants.COMP_CAL_BUENO))
			str = str + " checked ";
		else if ( competenciasPerfil.getFlagTipoEval().trim().equals("2") &&  
			competenciasPerfil.getCodEvalFin().trim().equals(CommonConstants.COMP_CAL_BUENO))
			str = str + " checked ";
		
		str = str + "onclick=\"javascript:fc_selCalificacion(this,'" + 
					Integer.toString(indice) + "','" +
					CommonConstants.COMP_CAL_BUENO + "');\" >" ;
		return str;
	}
	
	public String getRbtMedia()
	{
		competenciasPerfil = (CompetenciasPerfil) getCurrentRowObject();
		int indice = this.getListIndex();
		String str ="<input type=\"radio\" name=\"codEval" + Integer.toString(indice) + "\"  ";
		
		if ( competenciasPerfil.getFlagTipoEval().trim().equals("1") &&  
			competenciasPerfil.getCodEvalIni().trim().equals(CommonConstants.COMP_CAL_MEDIA))
			str = str + " checked ";
		else if ( competenciasPerfil.getFlagTipoEval().trim().equals("2") &&  
			competenciasPerfil.getCodEvalFin().trim().equals(CommonConstants.COMP_CAL_MEDIA))
			str = str + " checked ";
		
		str = str + "onclick=\"javascript:fc_selCalificacion(this,'" + 
					Integer.toString(indice) + "','" +
					CommonConstants.COMP_CAL_MEDIA + "');\" >" ;
		return str;
	}

	public String getRbtBaja()
	{
		competenciasPerfil = (CompetenciasPerfil) getCurrentRowObject();
		int indice = this.getListIndex();
		String str ="<input type=\"radio\" name=\"codEval" + Integer.toString(indice) + "\"  ";
		
		if ( competenciasPerfil.getFlagTipoEval().trim().equals("1") &&  
			competenciasPerfil.getCodEvalIni().trim().equals(CommonConstants.COMP_CAL_BAJA))
			str = str + " checked ";
		else if ( competenciasPerfil.getFlagTipoEval().trim().equals("2") &&  
			competenciasPerfil.getCodEvalFin().trim().equals(CommonConstants.COMP_CAL_BAJA))
			str = str + " checked ";
		
		str = str + "onclick=\"javascript:fc_selCalificacion(this,'" + 
					Integer.toString(indice) + "','" +
					CommonConstants.COMP_CAL_BAJA + "');\" >" ;
		return str;
	}
}
