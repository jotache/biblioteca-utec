package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
import com.tecsup.SGA.modelo.Postulante;

public class EvaluacionesByEvaluadorDecorator  extends TableDecorator {
	EvaluacionesByEvaluadorBean lObject;
	
	public String getChkSelPostulante()	
	{
		lObject = (EvaluacionesByEvaluadorBean) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codPostulante\" "
					+ " onclick=\"javascript:fc_seleccionarRegistro('" +
					lObject.getCodPostulante() + "','" +
					lObject.getNomPostulante() + "','" +
					lObject.getCodTipoEvaluacion() + "','" +
					lObject.getDscTipoEvaluacion() + "','" +
					lObject.getCodProceso() + "','" +
					lObject.getCodEvalEnProceso() + "','" +
					lObject.getCodPostProceso() +
					"');\" >" ;
        return str;
	}
	
	public String getTextAreaComentario()
	{
		lObject = (EvaluacionesByEvaluadorBean) getCurrentRowObject();
		return "<textarea class=\"cajatexto_1\" rows=\"2\" cols=\"25\" onKeyPress=\"return false;\" >" + lObject.getComentario() + "</textarea>";
	}
	
	public String getVerCV()
	{
		lObject = (EvaluacionesByEvaluadorBean) getCurrentRowObject();
		String str = "<img src=\"/biblioteca/images/iconos/buscar1.jpg\" style=\"cursor:pointer\" " +
					"onclick=\"fc_VerCV('" + lObject.getCv() + "');\"/>";
        return str;
	}
	
	public String getNomPostulante()
	{
		lObject = (EvaluacionesByEvaluadorBean) getCurrentRowObject();
		String str = "<a href=\"javascript:fc_HojaVida('" + lObject.getCodPostulante() + "');\" >" + lObject.getNomPostulante() + "</a>";
		str += "&nbsp;";		
		str += "<img src=\"/biblioteca/images/reclutamiento/cv.gif\" style=\"cursor:pointer\" " +
				"onclick=\"fc_VerCV('" + lObject.getCv() + "');\"/>";
        return str;
	}
	
	public String addRowClass()
	{
		lObject = (EvaluacionesByEvaluadorBean) getCurrentRowObject();
		if ( lObject.getCodEvalEnProceso() != null)
			if ( !lObject.getCodEvalEnProceso().trim().equals("") )
				return "texto_resaltado";
		return "texto";
	}
}
