package com.tecsup.SGA.bean;

public class FormatoEncuestaBean {
	
	private String fecRegistro;
	private String idGrupo;
	private String nivelAlternativa;
	private String idPregunta;
	
	private String nomPregunta;
	private String idAlternativa;
	private String nomAlternativa;
	private String desAlternativa;
	private String indObligatorio;
	private String tipoPregunta;
	private String indUnica;
	
	public String getFecRegistro() {
		return fecRegistro;
	}
	public void setFecRegistro(String fecRegistro) {
		this.fecRegistro = fecRegistro;
	}
	public String getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}
	public String getNivelAlternativa() {
		return nivelAlternativa;
	}
	public void setNivelAlternativa(String nivelAlternativa) {
		this.nivelAlternativa = nivelAlternativa;
	}
	public String getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(String idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getNomPregunta() {
		return nomPregunta;
	}
	public void setNomPregunta(String nomPregunta) {
		this.nomPregunta = nomPregunta;
	}
	public String getIdAlternativa() {
		return idAlternativa;
	}
	public void setIdAlternativa(String idAlternativa) {
		this.idAlternativa = idAlternativa;
	}
	public String getNomAlternativa() {
		return nomAlternativa;
	}
	public void setNomAlternativa(String nomAlternativa) {
		this.nomAlternativa = nomAlternativa;
	}
	public String getDesAlternativa() {
		return desAlternativa;
	}
	public void setDesAlternativa(String desAlternativa) {
		this.desAlternativa = desAlternativa;
	}
	public String getIndObligatorio() {
		return indObligatorio;
	}
	public void setIndObligatorio(String indObligatorio) {
		this.indObligatorio = indObligatorio;
	}
	public String getTipoPregunta() {
		return tipoPregunta;
	}
	public void setTipoPregunta(String tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}
	public String getIndUnica() {
		return indUnica;
	}
	public void setIndUnica(String indUnica) {
		this.indUnica = indUnica;
	}
	
}
