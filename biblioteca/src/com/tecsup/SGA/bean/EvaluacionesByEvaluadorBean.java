package com.tecsup.SGA.bean;

import java.io.Serializable;

public class EvaluacionesByEvaluadorBean implements Serializable{

	private String codPostulante;
	private String codEvalEnProceso;
	private String codPostProceso;
	private String nomPostulante;
	private String codProceso;
	private String codTipoEvaluacion;
	private String dscProceso;
	private String dscTipoEvaluacion;
	private String comentario;
	private String comentario2;
	private String codCalificacion;
	private String dscCalificacion;
	private String dscCalificacion2;
	private String cv;
	private String dscCalificacionJefeDpto_Revision;
	private String codEvaluador;
	private String flgEstadoActual;
	private String orden;
	private String codEvaluadorAsignado;
	private String codEstado;
	private String dscEstado;
	
	public String getCodCalificacion() {
		return codCalificacion;
	}
	public void setCodCalificacion(String codCalificacion) {
		this.codCalificacion = codCalificacion;
	}
	public String getDscCalificacion2() {
		return dscCalificacion2;
	}
	public void setDscCalificacion2(String dscCalificacion2) {
		this.dscCalificacion2 = dscCalificacion2;
	}
	public String getCodPostulante() {
		return codPostulante;
	}
	public void setCodPostulante(String codPostulante) {
		this.codPostulante = codPostulante;
	}
	public String getNomPostulante() {
		return nomPostulante;
	}
	public void setNomPostulante(String nomPostulante) {
		this.nomPostulante = nomPostulante;
	}
	public String getCodProceso() {
		return codProceso;
	}
	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}
	public String getCodTipoEvaluacion() {
		return codTipoEvaluacion;
	}
	public void setCodTipoEvaluacion(String codTipoEvaluacion) {
		this.codTipoEvaluacion = codTipoEvaluacion;
	}
	public String getDscProceso() {
		return dscProceso;
	}
	public void setDscProceso(String dscProceso) {
		this.dscProceso = dscProceso;
	}
	public String getDscTipoEvaluacion() {
		return dscTipoEvaluacion;
	}
	public void setDscTipoEvaluacion(String dscTipoEvaluacion) {
		this.dscTipoEvaluacion = dscTipoEvaluacion;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getDscCalificacion() {
		return dscCalificacion;
	}
	public void setDscCalificacion(String dscCalificacion) {
		this.dscCalificacion = dscCalificacion;
	}
	public String getCodEvalEnProceso() {
		return codEvalEnProceso;
	}
	public void setCodEvalEnProceso(String codEvalEnProceso) {
		this.codEvalEnProceso = codEvalEnProceso;
	}
	public String getCodPostProceso() {
		return codPostProceso;
	}
	public void setCodPostProceso(String codPostProceso) {
		this.codPostProceso = codPostProceso;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	public String getDscCalificacionJefeDpto_Revision() {
		return dscCalificacionJefeDpto_Revision;
	}
	public void setDscCalificacionJefeDpto_Revision(
			String dscCalificacionJefeDpto_Revision) {
		this.dscCalificacionJefeDpto_Revision = dscCalificacionJefeDpto_Revision;
	}
	public String getCodEvaluador() {
		return codEvaluador;
	}
	public void setCodEvaluador(String codEvaluador) {
		this.codEvaluador = codEvaluador;
	}
	public String getFlgEstadoActual() {
		return flgEstadoActual;
	}
	public void setFlgEstadoActual(String flgEstadoActual) {
		this.flgEstadoActual = flgEstadoActual;
	}
	public String getOrden() {
		return orden;
	}
	public void setOrden(String orden) {
		this.orden = orden;
	}
	public String getCodEvaluadorAsignado() {
		return codEvaluadorAsignado;
	}
	public void setCodEvaluadorAsignado(String codEvaluadorAsignado) {
		this.codEvaluadorAsignado = codEvaluadorAsignado;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDscEstado() {
		return dscEstado;
	}
	public void setDscEstado(String dscEstado) {
		this.dscEstado = dscEstado;
	}
	public String getComentario2() {
		return comentario2;
	}
	public void setComentario2(String comentario2) {
		this.comentario2 = comentario2;
	}
	
}
