package com.tecsup.SGA.bean;

import java.io.Serializable;

public class TrazaEvaluacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String etapa;
	private String calificacion;
	
	public TrazaEvaluacion(){}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	
	
}
