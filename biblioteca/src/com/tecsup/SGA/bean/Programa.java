package com.tecsup.SGA.bean;

public class Programa {
	
	private String codPrograma;
	private String codProducto;
	private String nomProducto;
	private String indCiclo;
	
	public String getIndCiclo() {
		return indCiclo;
	}
	public void setIndCiclo(String indCiclo) {
		this.indCiclo = indCiclo;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}	
	public String getNomProducto() {
		return nomProducto;
	}
	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}
}
