package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.TipoDetalle;
import com.tecsup.SGA.modelo.TipoExamenes;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Especialidad;
import com.tecsup.SGA.modelo.FormacionEmpresa;
import com.tecsup.SGA.modelo.TipoPracticas;
public class CursoEvaluadorDecorator extends TableDecorator {
	private CursoEvaluador lobject;
	private TipoExamenes pobject;
	private Parciales mobject;
	private Evaluador cobject;
	private Especialidad eobject;
	private FormacionEmpresa fobject;
	private Curso cursoObject;
	private TipoPracticas tobject;
	private Alumno alumnoObject;
	
	public String getRbtAlumnoDoc()
	{
		alumnoObject = (Alumno) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"optCodAlumno\"  " +
					"onclick=\"javascript:fc_selAlumno('" + alumnoObject.getCodAlumno() + "','" +
					alumnoObject.getNombreAlumno() + "');\" >" ;
		return str;

	}
	
	public String getRbtSel(){
		lobject = (CursoEvaluador) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codCurso\" value=\"" + lobject.getCodCurso()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + lobject.getCodCurso() + "','" + 
					lobject.getCiclo() + "','" + lobject.getCurso() + "','" + lobject.getEspecialidad() + 
					"','" + lobject.getProducto() + "','" + lobject.getSistEval() + "','" + lobject.getCodProducto() + 
					"','" + lobject.getCodEspecialidad() + "','" + lobject.getCadComponentes() +"');\" >" ;
        return str;
	}
	public String getCboEvaluacion()
	{
		tobject = (TipoPracticas) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"codEvaluacion\" value=\"" + tobject.getCodigo()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + tobject.getDescripcion() +  "');\" >" ;
        return str;
	}
	
	public String getChkSelDetalle()
	{
		mobject = (Parciales) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + mobject.getCodId()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + mobject.getCodEvaluador() +  "','" + mobject.getFlag() + "','" + mobject.getPeso() + "','" + mobject.getFlagEliminar() + "');\" >" ;
        return str;
	}
	
	public String getCboDocente()
	{
		cobject = (Evaluador) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"codEvaluador\" value=\"" + cobject.getCodEvaluador()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + cobject.getDscEvaluador() +  "');\" >" ;
        return str;
	}
	
	public String getRbtSelCurso(){
		cursoObject = (Curso) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codCurso\" value=\"" + cursoObject.getCodCurso()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + cursoObject.getCodCurso() + "');\" >" ;
        return str;
	}
	

	
	public String getCboEspecialidad()
	{
		eobject = (Especialidad) getCurrentRowObject();
		String str ="<input type=\"combo\" name=\"codEva\" value=\"" + eobject.getCodEspecialidad()
					+ "\"  onclick=\"javascript:fc_Valor(this.value,'" + eobject.getDescripcion() +  "');\" >" ;
        return str;	
	}
	
	public String addRowClass()
	{
		
		return "texto";
	}
	
	public String getChkPracticaInicial()
	{	
		String cad1 = "";
		String cad2 = "";
		String cad3 = "";
		
		fobject = (FormacionEmpresa) getCurrentRowObject();
		cad1 ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + fobject.getCodAlumno()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + fobject.getNombreAlumno() + "');\" " +
							"  ";
							if(fobject.getFlagPracticaInicial().equalsIgnoreCase("1")){
								cad2 = " checked ";
							}
							cad3 = " disabled=\"true\" >" ;
		String str = cad1+cad2+cad3;
		return str;
	}

	public String getChkAlumno()
	{
		//JHPR 2008-06-06 masivo
		/*
		fobject = (FormacionEmpresa) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + fobject.getCodAlumno()+ "\" id=\"chk" + fobject.getCodAlumno() 
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + fobject.getNombreAlumno() + "');\" >" ;
		*/
		fobject = (FormacionEmpresa) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + fobject.getCodAlumno()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + fobject.getNombreAlumno() + "');\" >" ;

        return str;
	}
	
	public String getChkPasantia()
	{
		String cad1 = "";
		String cad2 = "";
		String cad3 = "";
		
		fobject = (FormacionEmpresa) getCurrentRowObject();
		cad1 ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + fobject.getCodAlumno()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + fobject.getNombreAlumno() + "');\" " +
							"  ";
							if(fobject.getFlagPasantia().equalsIgnoreCase("1")){
								cad2 = " checked ";
							}
							cad3 = " disabled=\"true\" >" ;
		String str = cad1+cad2+cad3;
		return str;
	}
	

	public String getChkPracticaPre()
	{
		String cad1 = "";
		String cad2 = "";
		String cad3 = "";
		
		fobject = (FormacionEmpresa) getCurrentRowObject();
		cad1 ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + fobject.getCodAlumno()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + fobject.getNombreAlumno() + "');\" " +
							"  ";
							if(fobject.getFlagPracticaPreProf().equalsIgnoreCase("1")){
								cad2 = " checked ";
							}
							cad3 = " disabled=\"true\" >" ;
		String str = cad1+cad2+cad3;
		return str;
	}
	
	public String getTextValor(){
		
		String str ="<input type=\"text\" Class=\"cajatexto\" size=\"15\"  name=\"text\" value=\"" //pgobject.getValor() 
	    + "\" id=\"text \"  " + " onblur=\"fc_ValidaFechaOnblur('text');\" " +
	    "onkeypress=\"javascript:fc_Slash('frmMain','text','/');\" >" +
	    " <img src=\"/biblioteca/images/iconos/calendario_on.gif\" id=\"imgFecInc1\" alt=\"Calendario\" style=\'cursor:hand;\' align=\"absmiddle\"> ";
//		System.out.println("ta11"+">>"+str+">>");
		return str;
	}
	
	
}


