package com.tecsup.SGA.bean;

public class ConsultaByProfesorDetBean implements java.io.Serializable{
	/*
	 * AS ID_SECCION,  
      AS COD_SECCION,
      AS NOM_SECCION,
      AS NOM_DPTO,      
      AS NOM_CURSO,
      AS NOM_CICLO,
      AS NOM_SECCION,
      AS ID_GRUPO,
      AS NOM_GRUPO,
      AS ID_PREGUNTA,
      AS DES_PREGUNTA,
      AS ID_ALTERNATIVA,
      AS NOM_ALTERNATIVA,
      AS TOTAL_ALTERNATIVA
	 */
	private String idSeccion;
	private String codSeccion;
	private String nomSeccion;
	private String codDepartamento;
	private String nomDepartamento;	
	private String codCurso;
	private String nomCurso;
	private String codCiclo;
	private String nomCiclo;
	private String codSeccionAlumno;
	private String nomSeccionAlumno;
	private String codGrupo;
	private String nomGrupo;	
	private String codPregunta;
	private String nomPregunta;
	
	private String codAlternativa;
	private String nomAlternativa;
	private String totalAlternativa;
	private String respuesta;
	
	private String codProducto;
	private String codPrograma;
	private String pesoAlternativa;
	private String nroRespuestas;
	
	public String getPesoAlternativa() {
		return pesoAlternativa;
	}
	public void setPesoAlternativa(String pesoAlternativa) {
		this.pesoAlternativa = pesoAlternativa;
	}
	public String getNroRespuestas() {
		return nroRespuestas;
	}
	public void setNroRespuestas(String nroRespuestas) {
		this.nroRespuestas = nroRespuestas;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}
	public String getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(String codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getNomSeccion() {
		return nomSeccion;
	}
	public void setNomSeccion(String nomSeccion) {
		this.nomSeccion = nomSeccion;
	}
	public String getNomDepartamento() {
		return nomDepartamento;
	}
	public void setNomDepartamento(String nomDepartamento) {
		this.nomDepartamento = nomDepartamento;
	}
	public String getNomCurso() {
		return nomCurso;
	}
	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}
	public String getNomCiclo() {
		return nomCiclo;
	}
	public void setNomCiclo(String nomCiclo) {
		this.nomCiclo = nomCiclo;
	}
	public String getNomSeccionAlumno() {
		return nomSeccionAlumno;
	}
	public void setNomSeccionAlumno(String nomSeccion02) {
		this.nomSeccionAlumno = nomSeccion02;
	}
	public String getCodGrupo() {
		return codGrupo;
	}
	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}
	public String getNomGrupo() {
		return nomGrupo;
	}
	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}
	public String getCodPregunta() {
		return codPregunta;
	}
	public void setCodPregunta(String codPregunta) {
		this.codPregunta = codPregunta;
	}
	public String getNomPregunta() {
		return nomPregunta;
	}
	public void setNomPregunta(String nomPregunta) {
		this.nomPregunta = nomPregunta;
	}
	public String getCodAlternativa() {
		return codAlternativa;
	}
	public void setCodAlternativa(String codAlternativa) {
		this.codAlternativa = codAlternativa;
	}
	public String getNomAlternativa() {
		return nomAlternativa;
	}
	public void setNomAlternativa(String nomAlternativa) {
		this.nomAlternativa = nomAlternativa;
	}
	public String getTotalAlternativa() {
		return totalAlternativa;
	}
	public void setTotalAlternativa(String totalAlternativa) {
		this.totalAlternativa = totalAlternativa;
	}
	public String getCodDepartamento() {
		return codDepartamento;
	}
	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}
	public String getCodCurso() {
		return codCurso;
	}
	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getCodSeccionAlumno() {
		return codSeccionAlumno;
	}
	public void setCodSeccionAlumno(String codSeccionAlumno) {
		this.codSeccionAlumno = codSeccionAlumno;
	}	
}
