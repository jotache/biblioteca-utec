package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.Exclusion;

public class ExclusionesDecorator extends TableDecorator{
	Exclusion exclusion;

	@Override
	public String addRowClass() {		
		return "texto";
	}
	
	public String getCodAlumno(){
		exclusion = (Exclusion) getCurrentRowObject();
		int i = this.getListIndex();
		
		String str ="<span >" + exclusion.getCodAlumno() + "</span>"
					+ "<input type=\"hidden\" id=\"txhCodExclusion" + Integer.toString(i) + "\" value=\"" + exclusion.getCodExclusion() + "\" />"
					+ "<input type=\"hidden\" id=\"txhCodAlumno" + Integer.toString(i) + "\" value=\"" + exclusion.getCodAlumno() + "\" />";
		
        return str;
	}
	
	public String getChkSementre(){
		exclusion = (Exclusion) getCurrentRowObject();
		int i = this.getListIndex();
		
		String str = "<input type=\"checkbox\" id=\"chkSem" + Integer.toString(i) + "\" ";
		if ( exclusion.getFlagCurso().trim().equals("1") ) str = str + " checked "; 
		str = str + " />";
		
        return str;
	}
	
	public String getChkAcumulado(){
		exclusion = (Exclusion) getCurrentRowObject();
		int i = this.getListIndex();
		
		String str ="<input type=\"checkbox\" id=\"chkAcum" + Integer.toString(i) + "\" ";
		if ( exclusion.getFlagSemestre().trim().equals("1") ) str = str + " checked "; 
		str = str + " />";
		
        return str;
	}
}
