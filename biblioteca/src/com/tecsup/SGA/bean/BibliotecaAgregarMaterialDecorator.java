package com.tecsup.SGA.bean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.displaytag.decorator.TableDecorator;

public class BibliotecaAgregarMaterialDecorator extends TableDecorator {
	
	private static Log log = LogFactory
	.getLog(BibliotecaAgregarMaterialDecorator.class);
	
	private MaterialesxFiltro objCurrent;

	public String getVerHistorialBandeja()
	{
		objCurrent = (MaterialesxFiltro) getCurrentRowObject();
		int i= getListIndex();
		String cadena = "<input type=\"hidden\" id=\"hidTipoMat"+i+"\"  value=\""+objCurrent.getDescTipoMat()+"\"  />"+
		"<input type=\"hidden\" id=\"hidTituloMat"+i+"\"  value=\""+objCurrent.getTitulo()+"\"  />"+
		"<img src=\"/biblioteca/images/iconos/buscar1.jpg\" " +
		" style=\"cursor: hand\" onclick=\"javascript:fc_VerHistorial('"+objCurrent.getCodUnico()+"','"+objCurrent.getCodGenerado()+"','"+i+"');\">";
		
		
        return cadena;
	}
	
	public String getDetalleMaterial()
	{
		objCurrent = (MaterialesxFiltro) getCurrentRowObject();
		
		String cadena = "<img src=\"/biblioteca/images/iconos/buscar1.jpg\" " +
		" style=\"cursor: hand\" onclick=\"javascript:fc_detalleMaterial('"+objCurrent.getCodUnico()+"');\">";
		
		
        return cadena;
	}	
	
	public String getRadioHistorialBandeja()
	{
		objCurrent = (MaterialesxFiltro) getCurrentRowObject();		
		String cadena = "<input type=\"radio\" name=\"rdo\" onclick=\"fc_seleccion('"+objCurrent.getCodUnico()+"','"+objCurrent.getCodGenerado()+"')\" >";		
				
        return cadena;
	}
	
	public String getTextAreaTitulo()
	{
		objCurrent = (MaterialesxFiltro) getCurrentRowObject();
		String cadena="<textarea style=\"width:98%\" readonly=\"true\" class=\"cajatexto_1\" rows=\"2\">" + objCurrent.getTitulo() + "</textarea>";
        return cadena;
	}
	
	public String addRowClass()
	{
		objCurrent = (MaterialesxFiltro) getCurrentRowObject();
		if ( "0".equalsIgnoreCase(objCurrent.getDispo()))			
				return "texto_rojo";
		return "texto";
	}
	
	public String getFlgReservaMaterial(){
		objCurrent = (MaterialesxFiltro) getCurrentRowObject();
		String cadena = "";
		
		if ("0".equalsIgnoreCase(objCurrent.getFlgReserva()))
			cadena = objCurrent.getReserva();
		else
			cadena = "<u onclick=\"javascript:fc_VerReservas('"+objCurrent.getCodUnico()+"')\" style=\"cursor: hand\">" + objCurrent.getReserva() + "</u>";
			
		
		
		return cadena;
	}
}


