/**
 * 
 */
package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.Recluta;

/**
 * @author       CosapiSoft S.A.
 * @date         10-oct-07
 * @description  none
 */

public class ReclutaDecorator extends TableDecorator {
	private TipoTablaDetalle lObject;
	private Recluta lObject1;
	
	/*public String getCheckSelBandeja(){
		lObject = (Proceso) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + lObject.getCodProceso() 
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value);\" >" ;
        return str;
	}*/
	
	public String getImgEliAreaInteres()
	{
		//lObject = (TipoTablaDetalle) getCurrentRowObject();
		lObject1 = (Recluta) getCurrentRowObject();
		
		String str = "<img src=\"/biblioteca/images/iconos/kitar1.jpg\" alt=\"Quitar\" style=\"cursor:hand\" " +
					" onclick = \"fc_EliAreaInteres('" + lObject1.getCodTTDInteres() + "','" + lObject1.getCodInteres() + "');\" "
					+" />";
		
		return str;
	}
}
