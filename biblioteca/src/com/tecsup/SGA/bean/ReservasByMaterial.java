package com.tecsup.SGA.bean;

public class ReservasByMaterial {
private String codReserva;
private String codMaterial;
private String codigo; //codigo generado
private String tipoMaterial;
private String titulo;
private String usuario;
private String tipoUsuario;
private String fechaReserva;
private String horaReserva;
private String flgEliminar;
public String getCodReserva() {
	return codReserva;
}
public void setCodReserva(String codReserva) {
	this.codReserva = codReserva;
}
public String getCodMaterial() {
	return codMaterial;
}
public void setCodMaterial(String codMaterial) {
	this.codMaterial = codMaterial;
}
public String getCodigo() {
	return codigo;
}
public void setCodigo(String codigo) {
	this.codigo = codigo;
}
public String getTipoMaterial() {
	return tipoMaterial;
}
public void setTipoMaterial(String tipoMaterial) {
	this.tipoMaterial = tipoMaterial;
}
public String getTitulo() {
	return titulo;
}
public void setTitulo(String titulo) {
	this.titulo = titulo;
}
public String getUsuario() {
	return usuario;
}
public void setUsuario(String usuario) {
	this.usuario = usuario;
}
public String getTipoUsuario() {
	return tipoUsuario;
}
public void setTipoUsuario(String tipoUsuario) {
	this.tipoUsuario = tipoUsuario;
}
public String getFechaReserva() {
	return fechaReserva;
}
public void setFechaReserva(String fechaReserva) {
	this.fechaReserva = fechaReserva;
}
public String getHoraReserva() {
	return horaReserva;
}
public void setHoraReserva(String horaReserva) {
	this.horaReserva = horaReserva;
}
public String getFlgEliminar() {
	return flgEliminar;
}
public void setFlgEliminar(String flgEliminar) {
	this.flgEliminar = flgEliminar;
}




}
