package com.tecsup.SGA.bean;

import java.io.Serializable;

public class OpcionesApoyo implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codOpcion;
	private String codApoyo;
	private String nomOpcion;
	private String seleccionado;
	private String nomCorto;
	
	public String getCodOpcion() {
		return codOpcion;
	}
	public void setCodOpcion(String codOpcion) {
		this.codOpcion = codOpcion;
	}
	public String getCodApoyo() {
		return codApoyo;
	}
	public void setCodApoyo(String codApoyo) {
		this.codApoyo = codApoyo;
	}
	public String getNomOpcion() {
		return nomOpcion;
	}
	public void setNomOpcion(String nomOpcion) {
		this.nomOpcion = nomOpcion;
	}
	public String getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(String seleccionado) {
		this.seleccionado = seleccionado;
	}
	@Override
	public String toString() {
		return "OpcionesApoyo [codOpcion=" + codOpcion + ", codApoyo="
				+ codApoyo + ", nomOpcion=" + nomOpcion + ", seleccionado="
				+ seleccionado + "]";
	}
	public String getNomCorto() {
		return nomCorto;
	}
	public void setNomCorto(String nomCorto) {
		this.nomCorto = nomCorto;
	}
	
	
}
