package com.tecsup.SGA.bean;

public class ObjPrecioBean {
	private String codProv;
	private String razonSocial;
	private String precioUnitario;
	private String flgChecked;
	private String obs;
	private String obsProv;
	//ALQD,04/06/09.NUEVA PROPIEDAD
	private String otroCosto;
	
	public String getOtroCosto() {
		return otroCosto;
	}
	public void setOtroCosto(String otroCosto) {
		this.otroCosto=otroCosto;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	public String getObsProv() {
		return obsProv;
	}
	public void setObsProv(String obsProv) {
		this.obsProv = obsProv;
	}
	public String getFlgChecked() {
		return flgChecked;
	}
	public void setFlgChecked(String flgChecked) {
		this.flgChecked = flgChecked;
	}
	public String getCodProv() {
		return codProv;
	}
	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

}
