package com.tecsup.SGA.bean;

import java.io.Serializable;

public class MaterialesxFiltro implements Serializable {
private String codUnico;
private String codGenerado;
private String codTipoMat;
private String descTipoMat;
private String titulo;
private String codAutor;
private String descAutor;
private String volumenes;
private String prestSala;
private String prestDomicilio;
private String reserva;
private String dispo;
private String nomSede;
private String nomEditorial;

private String dewey;

private String flgReserva;
private String fechaPublicacion;

public String getNomEditorial() {
	return nomEditorial;
}
public void setNomEditorial(String nomEditorial) {
	this.nomEditorial = nomEditorial;
}
public String getNomSede() {
	return nomSede;
}
public void setNomSede(String nomSede) {
	this.nomSede = nomSede;
}
public String getCodUnico() {
	return codUnico;
}
public void setCodUnico(String codUnico) {
	this.codUnico = codUnico;
}
public String getCodGenerado() {
	return codGenerado;
}
public void setCodGenerado(String codGenerado) {
	this.codGenerado = codGenerado;
}
public String getCodTipoMat() {
	return codTipoMat;
}
public void setCodTipoMat(String codTipoMat) {
	this.codTipoMat = codTipoMat;
}
public String getDescTipoMat() {
	return descTipoMat;
}
public void setDescTipoMat(String descTipoMat) {
	this.descTipoMat = descTipoMat;
}
public String getTitulo() {
	return titulo;
}
public void setTitulo(String titulo) {
	this.titulo = titulo;
}
public String getCodAutor() {
	return codAutor;
}
public void setCodAutor(String codAutor) {
	this.codAutor = codAutor;
}
public String getDescAutor() {
	return descAutor;
}
public void setDescAutor(String descAutor) {
	this.descAutor = descAutor;
}
public String getVolumenes() {
	return volumenes;
}
public void setVolumenes(String volumenes) {
	this.volumenes = volumenes;
}
public String getPrestSala() {
	return prestSala;
}
public void setPrestSala(String prestSala) {
	this.prestSala = prestSala;
}
public String getPrestDomicilio() {
	return prestDomicilio;
}
public void setPrestDomicilio(String prestDomicilio) {
	this.prestDomicilio = prestDomicilio;
}
public String getReserva() {
	return reserva;
}
public void setReserva(String reserva) {
	this.reserva = reserva;
}
public String getDispo() {
	return dispo;
}
public void setDispo(String dispo) {
	this.dispo = dispo;
}


public String getFlgReserva() {
	return flgReserva;
}
public void setFlgReserva(String flgReserva) {
	this.flgReserva = flgReserva;
}


public String getDewey() {
	return dewey;
}
public void setDewey(String dewey) {
	this.dewey = dewey;
}

public String getFechaPublicacion() {
	return fechaPublicacion;
}
public void setFechaPublicacion(String fechaPublicacion) {
	this.fechaPublicacion = fechaPublicacion;
}



}
