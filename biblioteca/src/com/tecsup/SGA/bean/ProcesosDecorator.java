/**
 * 
 */
package com.tecsup.SGA.bean;

import java.util.*;
import org.displaytag.decorator.TableDecorator;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.modelo.Calificacion;

/**
 * @author       CosapiSoft S.A.
 * @date         10-oct-07
 * @description  none
 */

public class ProcesosDecorator extends TableDecorator{
	
	private Proceso lObject;
	private ProcesoEvaluadorBean objEvaluador;
	
	public String addRowClass()
	{
		return "texto";
	}
	
	public String getCheckSelBandeja(){
		lObject = (Proceso) getCurrentRowObject();
		String str ="<input type=\"checkbox\" name=\"codProceso\" value=\"" + lObject.getCodProceso() 
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value);\" >" ;
        return str;
	}
	
	public String getLinkOferta()
	{
		lObject = (Proceso) getCurrentRowObject();
		String str = "<a href=\"javascript:fc_VerOferta('" + lObject.getCodProceso() + "');\" " +
							"class=\"Enlace\">" + lObject.getDscOfertaAsociada() + "</a>";
        return str;
	}
	
	public String getCboEvaluadores()
	{
		objEvaluador = (ProcesoEvaluadorBean) getCurrentRowObject();
		ArrayList listaEvaluadores = (ArrayList)objEvaluador.getEvaluadores();
		Usuario usuario = null;
		String codEvaluador = objEvaluador.getCodEvaluadorSel();
		
		String str = "<select ID=\"cboTipo\" class=\"combo_o\" " 
					+ " onChange=\"fc_CambiaEvaluador(this ,'" + objEvaluador.getCodDetalle() + "');\" " 
					+" style=\"width:200px\">";
		str = str + "<option selected value=''>--Seleccione--</option>";
		for (int i = 0; i < listaEvaluadores.size(); i++)
		{
			usuario = (Usuario)listaEvaluadores.get(i);
			if ( codEvaluador != null )
			{
				if ( codEvaluador.trim().equals(usuario.getCodUsuario().trim()) )
					str = str + "<option value='" + usuario.getCodUsuario().trim() + "' selected >" + usuario.getNomUsuario() + "</option>";
				else
					str = str + "<option value='" + usuario.getCodUsuario().trim() + "'>" + usuario.getNomUsuario() + "</option>";
			}
			else
				str = str + "<option value='" + usuario.getCodUsuario().trim() + "'>" + usuario.getNomUsuario() + "</option>";
		}
		str = str + "</select>";
		return str;
	}

	public String getCboCalificacion()
	{
		objEvaluador = (ProcesoEvaluadorBean) getCurrentRowObject();
		ArrayList listaEvaluadores = (ArrayList)objEvaluador.getEvaluadores();
		Calificacion calificacion = null;
		
		String str = "<select ID=\"cboTipoCalificacion\" class=\"combo_o\" " 
					+ " onChange=\"fc_CambiaCalificacion(this ,'" + objEvaluador.getCodDetalle() + "','" + Integer.toString(this.getListIndex()) + "');\" " 
					+" style=\"width:90%\">";
		str = str + "<option selected value=''>--Seleccione--</option>";
		for (int i = 0; i < listaEvaluadores.size(); i++)
		{
			calificacion = (Calificacion)listaEvaluadores.get(i);
			str = str + "<option value='" + calificacion.getCodCalificacionId().trim() + "'>" + calificacion.getDscCalificacionNormal().trim() + "</option>";
		}
		str = str + "</select>";
		return str;
	}
	
	public String getObservacion()
	{
		objEvaluador = (ProcesoEvaluadorBean) getCurrentRowObject();		
		String str = "<textarea rows=\"2\" cols=\"30\" id=\"txaObservacion" + Integer.toString(this.getListIndex()) + "\"";
		str = str + " onkeypress=\"javascript:void(0);\" class=\"cajatexto\" ";
		str = str + " onblur=\"javascript:void(0);\"";
		str = str + " >";
		if ( objEvaluador.getObservacion() != null ) str = str + objEvaluador.getObservacion();  
		str = str + "</textarea>";
		return str;
	}
	
}
