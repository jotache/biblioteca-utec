package com.tecsup.SGA.bean;

public class AlumnoXCodigoBean {
	
private String codUsuario;
private String tipoUsuario;
private String descTipoUsuario;
//private String flgPrestamo;
private String flgPermitido;     //flag usuario tiene permiso prestamo a domicilio
private String flgPermitidoSala; //flag usuario tiene permiso prestamo en sala
private String flgSancion;
private String fecActual;
private String nroDiasPrestCasa;
private String fecProgDev;
private String usuario;
private String flgInhabilitado;
private String fechaFinInhabilitado;
private String estado;
private String nroPrestamos;
private String limitePrestamos;

	private String nombre;
	
	private String sede;
	private String fechaFinSancion; //jhpr 2008-08-15
	
	
	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getDescTipoUsuario() {
		return descTipoUsuario;
	}

	public void setDescTipoUsuario(String descTipoUsuario) {
		this.descTipoUsuario = descTipoUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFlgSancion() {
		return flgSancion;
	}

	public void setFlgSancion(String flgSancion) {
		this.flgSancion = flgSancion;
	}

	public String getFecActual() {
		return fecActual;
	}

	public void setFecActual(String fecActual) {
		this.fecActual = fecActual;
	}

	public String getNroDiasPrestCasa() {
		return nroDiasPrestCasa;
	}

	public void setNroDiasPrestCasa(String nroDiasPrestCasa) {
		this.nroDiasPrestCasa = nroDiasPrestCasa;
	}

	public String getFecProgDev() {
		return fecProgDev;
	}

	public void setFecProgDev(String fecProgDev) {
		this.fecProgDev = fecProgDev;
	}

	public String getFlgPermitido() {
		return flgPermitido;
	}

	public void setFlgPermitido(String flgPermitido) {
		this.flgPermitido = flgPermitido;
	}

	public String getFlgPermitidoSala() {
		return flgPermitidoSala;
	}

	public void setFlgPermitidoSala(String flgPermitidoSala) {
		this.flgPermitidoSala = flgPermitidoSala;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getFechaFinSancion() {
		return fechaFinSancion;
	}

	public void setFechaFinSancion(String fechaFinSancion) {
		this.fechaFinSancion = fechaFinSancion;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFlgInhabilitado() {
		return flgInhabilitado;
	}

	public void setFlgInhabilitado(String flgInhabilitado) {
		this.flgInhabilitado = flgInhabilitado;
	}

	public String getFechaFinInhabilitado() {
		return fechaFinInhabilitado;
	}

	public void setFechaFinInhabilitado(String fechaFinInhabilitado) {
		this.fechaFinInhabilitado = fechaFinInhabilitado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNroPrestamos() {
		return nroPrestamos;
	}

	public void setNroPrestamos(String nroPrestamos) {
		this.nroPrestamos = nroPrestamos;
	}

	public String getLimitePrestamos() {
		return limitePrestamos;
	}

	public void setLimitePrestamos(String limitePrestamos) {
		this.limitePrestamos = limitePrestamos;
	}	
}
