package com.tecsup.SGA.bean;

public class FormatoBean implements java.io.Serializable{
	private String codFormato;
	private String nomFormato;
	private String idSeccion;
	
	public String getCodFormato() {
		return codFormato;
	}
	public void setCodFormato(String codFormato) {
		this.codFormato = codFormato;
	}
	public String getNomFormato() {
		return nomFormato;
	}
	public void setNomFormato(String nomFormato) {
		this.nomFormato = nomFormato;
	}
	public String getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}
		
}
