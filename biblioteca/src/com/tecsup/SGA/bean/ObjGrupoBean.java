package com.tecsup.SGA.bean;

import java.util.List;

public class ObjGrupoBean {
	
	List items;
	List cabeceras;

	public List getItems() {
		return items;
	}

	public void setItems(List items) {
		this.items = items;
	}

	public List getCabeceras() {
		return cabeceras;
	}

	public void setCabeceras(List cabeceras) {
		this.cabeceras = cabeceras;
	}
}
