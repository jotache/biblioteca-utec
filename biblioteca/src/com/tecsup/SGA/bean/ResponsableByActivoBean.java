package com.tecsup.SGA.bean;

public class ResponsableByActivoBean {
private String codBien;
private String codBienDet;
private String nroSerie;
private String codUsuResp;
private String nomUsuResp;
private String ubicacion;
private String flgBD;//1:SI VIENE DE LA BD; 0:SI NO ESTA EN LA BD

public String getCodBien() {
	return codBien;
}
public void setCodBien(String codBien) {
	this.codBien = codBien;
}
public String getCodBienDet() {
	return codBienDet;
}
public void setCodBienDet(String codBienDet) {
	this.codBienDet = codBienDet;
}
public String getNroSerie() {
	return nroSerie;
}
public void setNroSerie(String nroSerie) {
	this.nroSerie = nroSerie;
}
public String getCodUsuResp() {
	return codUsuResp;
}
public void setCodUsuResp(String codUsuResp) {
	this.codUsuResp = codUsuResp;
}
public String getNomUsuResp() {
	return nomUsuResp;
}
public void setNomUsuResp(String nomUsuResp) {
	this.nomUsuResp = nomUsuResp;
}
public String getUbicacion() {
	return ubicacion;
}
public void setUbicacion(String ubicacion) {
	this.ubicacion = ubicacion;
}
public String getFlgBD() {
	return flgBD;
}
public void setFlgBD(String flgBD) {
	this.flgBD = flgBD;
}
	
}
