/**
 * 
 */
package com.tecsup.SGA.bean;

import java.util.*;
import org.displaytag.decorator.TableDecorator;

/**
 * @author       CosapiSoft S.A.
 * @date         10-oct-07
 * @description  none
 */

public class ProductoByItemDecorator extends TableDecorator{
	
	private ProductoByItemBean lObject;
	
	public String addRowClass()
	{
		return "texto";
	}
	
	public String getRbtSelProducto(){
		lObject = (ProductoByItemBean) getCurrentRowObject();
		int indice = this.getListIndex();
		
		String str ="<input type=\"radio\" name=\"rbtProducto\" id=\"rbtProducto" + Integer.toString(indice) + "\" " +
					"onclick=\"javascript:fc_seleccionarRegistro('" + lObject.getCodReq() + "', '" + lObject.getCodReqDet() + "');\" >" ;
        return str;
	}
	
}
