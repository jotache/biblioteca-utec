package com.tecsup.SGA.bean;

public class Ciclo {
	
	private String codCiclo;
	private String nomCiclo;
	
	public String getCodCiclo() {
		return codCiclo;
	}
	public void setCodCiclo(String codCiclo) {
		this.codCiclo = codCiclo;
	}
	public String getNomCiclo() {
		return nomCiclo;
	}
	public void setNomCiclo(String nomCiclo) {
		this.nomCiclo = nomCiclo;
	}
	
}
