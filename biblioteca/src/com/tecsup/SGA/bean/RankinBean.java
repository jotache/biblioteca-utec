package com.tecsup.SGA.bean;

public class RankinBean {

	private String promedioAcumulado;
	private String rankin;
	private String descRankin;
	
	public String getPromedioAcumulado() {
		return promedioAcumulado;
	}
	public void setPromedioAcumulado(String promedioAcumulado) {
		this.promedioAcumulado = promedioAcumulado;
	}
	public String getRankin() {
		return rankin;
	}
	public void setRankin(String rankin) {
		this.rankin = rankin;
	}
	public String getDescRankin() {
		return descRankin;
	}
	public void setDescRankin(String descRankin) {
		this.descRankin = descRankin;
	}
}
