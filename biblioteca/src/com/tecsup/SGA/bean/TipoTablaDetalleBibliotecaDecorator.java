package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.*;

public class TipoTablaDetalleBibliotecaDecorator extends TableDecorator{

	private TipoTablaDetalle lobject;
	private Pais pais;
	private Ciudad ciudad;
	private BuzonSugerencia buzonSugerencia;
	private Feriados fer;
	private Secciones secciones;
	private Sala sala;
	private String dota="";
	private Busqueda busqueda;
	
	public String getRbtSelDetalle()
	{   int i= getListIndex();
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		//System.out.println("Codigo: "+lobject.getCodTipoTablaDetalle());
		String str ="<input type=\"radio\" name=\"text\" id=\"text"+i+"\" value=\"" + lobject.getDescripcion()
		+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + lobject.getCodTipoTablaDetalle() +  "'," +
				"'" + lobject.getDscValor2()+  "', 'text"+i+"','"+lobject.getDscValor3()+"');\" >" ;		
        return str;
	}
	public String getRbtSelDetalleMaterial()
	{   int i= getListIndex();
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		System.out.println("CodigoMaterial: "+lobject.getCodTipoTablaDetalle());
		String str ="<input type=\"radio\" name=\"text\" id=\"text"+i+"\" value=\"" + lobject.getDescripcion()
		+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + lobject.getCodTipoTablaDetalle() +  "'," + 
		" '" + lobject.getDscValor2()+  "','text"+i+"');\" >" ;
		//System.out.println(str);
        return str;
	}
	//***********************************************
	//	
	public String getRbtSelBibMan(){
		
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		
		String str ="<input type=\"radio\" name=\"radio\" value=\"" + lobject.getCodTipoTablaDetalle()
		+ "\"  onclick=\"javascript:fc_SeleccionarRegistro(this.value,'" + lobject.getDscValor2()+  "'," +
				" '" + lobject.getDescripcion() +  "');\" >" ;
		
        return str;
	}
	public String getTipo()
	{   int i= getListIndex();
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String str="";
		if(lobject.getDscValor3()==null){
			str="";
		}
		else{
			if(lobject.getDscValor3().equals("0001")){
				str="Persona";
			}
			if(lobject.getDscValor3().equals("0002")){
				str="Empresa";
			}
		}
        return str;
	}
	//para  consulta sala
	public String getRbtSelBibManSala(){
		
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		
		String str ="<input type=\"radio\" name=\"radio\" value=\"" + lobject.getCodTipoTablaDetalle()
		+ "\"  onclick=\"javascript:fc_SeleccionarRegistro(this.value,'" + lobject.getDscValor1()+  "'," +
		"'" + lobject.getDscValor2()+  "', '" + lobject.getDescripcion() +  "', '" +lobject.getDscValor3()+"');\" >" ;
		
        return str;
	}
	
	public String getFechaHoraInicio(){
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String sTexto = "";
		String sHora = lobject.getDscValor8();
		if(sHora==null) sHora="";
		if(!sHora.equals("") && !sHora.equals("")){
			sTexto = lobject.getDscValor4()+ " " +  sHora;
		}else{
			sTexto = lobject.getDscValor4();
		}
		return sTexto;
	}
	
	public String getFechaHoraFin(){
		lobject = (TipoTablaDetalle) getCurrentRowObject();
		String sTexto = "";
		String sHora = lobject.getDscValor9();		
		if(sHora==null) sHora="";
		if(!sHora.equals("") && !sHora.equals("")){
			sTexto = lobject.getDscValor5()+ " " +  sHora;
		}else{
			sTexto = lobject.getDscValor5();
		}
		return sTexto;
	}
	
	//para consulta seccion
	public String getRbtSelBibManSeccion(){		
		secciones = (Secciones) getCurrentRowObject();		
		String str ="<input type=\"radio\" name=\"radio\" value=\"" + secciones.getCodUnico()
		+ "\"  onclick=\"javascript:fc_SeleccionarRegistro(this.value);\" >" ;		
        return str;
	}
	//***************************************************
	public String getRbtSelDetallePais()
	{   int i= getListIndex();
		pais = (Pais) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" id=\"text"+i+"\" value=\"" + pais.getPaisDescripcion()
		+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + pais.getPaisId()+  "','" + pais.getPaisCodigo()+  "'," +
		" 'text"+i+"');\" >" ;
        return str;
	}
		
	public String getRbtSelDetalleCiudad(){
		ciudad = (Ciudad) getCurrentRowObject();
		
		String str ="<input type=\"radio\" name=\"radio\" value=\"" + ciudad.getCiudadId()
					+ "\"  onclick=\"javascript:fc_SeleccionarRegistro(this.value,'" + ciudad.getCiudadCodigo() +"','"+ciudad.getCiudadDescripcion()+"','"+ciudad.getPaisId()+"');\" >" ;
		
        return str;
	}
	//***************************************************
	public String getTextValor()
	{   lobject = (TipoTablaDetalle) getCurrentRowObject();
		int i= getListIndex();
		String peso="height=\"22px\"";
		String espacio="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		String imagen="";//<td align=right colspan="2" width="25%">&nbsp;</td>--------height=\"22px\"----border=\"2\" bordercolor=\"red\"
		String flagReservar="<img src=\"/biblioteca/images/botones/reservar.gif\" height=\"23px\" " +
				"onclick=\"javascript:fc_Reservar();\">&nbsp; ";
		if(i==0){imagen="Foto5.jpg";
		         
		        }
		else{if(i==1){ imagen="Foto6.jpg";
						flagReservar=espacio;
						//peso="";
		             }
		     else{ if(i==2){ imagen="Foto4.jpg";
		     			   
		                   }
		     	   else{ imagen="Foto3.jpg";
		     	         flagReservar=espacio;
		     	       // peso="";
		     	       }
		           } 
		     }
		String str="<table width=\"100%\" align=\"left\"> <tr> <td width=\"10%\" align=\"left\">Titulo</td>" +
				                 "<td width=\"90%\" align=\"left\" colspan=\"2\"> <input maxlength=\"50\" type=\"text\" class=\"cajatexto\"  size=\"60\" name=\"text"+i+"1\" value=\"" + lobject.getCodTipoTablaDetalle()
				                 + "\" id=\"text"+i+"1\"  " + "onblur=\"fc_ValidaSoloLetrasFinal1(this,'Titulo');\"  onkeypress=\"fc_ValidaTextoEspecial();\" ></td>" +
				                 "<td align=\"right\" colspan=\"2\" width=\"50px\" "+ peso +">"+ flagReservar +"</td>" +
				                 "<td rowspan=\"3\" width=\"15%\" valign='top'><img src=\"/biblioteca/images/"+ imagen + "\" style=\"width:100px;height:110px;\" border='1'></td>"+
				            "</tr>" +
						    "<tr><td width=\"10%\" align=\"left\">Autor</td> <td width=\"90%\" align=\"left\"> <input maxlength=\"2\" type=\"text\" class=\"cajatexto\"  size=\"60\" name=\"text"+i+"2\" value=\"" + lobject.getDscValor2()
						            + "\" id=\"text"+i+"2\"  " + "onblur=\"fc_ValidaSoloLetrasFinal1(this,'Autor');\"  onkeypress=\"fc_ValidaTextoEspecial();\" ></td>" +
						    "</tr>" +
						    "<tr><td width=\"10%\" align=\"left\">Dewey</td> <td width=\"90%\" align=\"left\"> <input maxlength=\"2\" type=\"text\" class=\"cajatexto\"  size=\"20\" name=\"text"+i+"3\" value=\"" + lobject.getDescripcion()
						            + "\" id=\"text"+i+"3\"  " + "onblur=\"fc_ValidaSoloLetrasFinal1(this,'Dewey');\"  onkeypress=\"fc_ValidaTextoEspecial();\" ></td>" +
						    "</tr>" +
						    "<tr><td colspan='5'><img src=\"/biblioteca/images/separador_n.gif\" width=\"100%\"></td>" +
						    "</tr>" +
				   "</table>";
		
	   // System.out.println(str);
		return str;
	}
	//textEnlacesInteres
	public String getTextEnlacesInteres()
	{   secciones = (Secciones) getCurrentRowObject();
		int i= getListIndex();
		String imagen="";
		
		String ruta ="";
		String archivo="";//secciones.getArchivo();
		if(!secciones.getImagen().equals("&nbsp;"))
		{	ruta=secciones.getImagen();
		    if(ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_DOC) || ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_DOCX)) ruta="doc.gif"; //excel.gif doc.gif pdf.gif
		    else{ if(ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_XLS) || ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_XLSX)) ruta="excel.gif";
		    	  else if(ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_PDF)) ruta="pdf.gif";
		    	}
		}
		
		if(!secciones.getArchivo().equals("&nbsp;"))
			archivo=secciones.getArchivo();
	    //secciones.getImagen() es donde se guarda la extension de la imagen que se guarda en archivo
		if(!secciones.getArchivo().equals("&nbsp;"))
	     imagen="<img src=\"/biblioteca/images/"+ ruta + "\" style=\"width:40px;height:40px;\" border='0'>";
		else
	     imagen="";
		
		String url="";
		if(!secciones.getUrl().equals("&nbsp;"))
		url=secciones.getUrl();
				 	
		String str="<table style=\"border:1px solid #048BBA\" bordercolor=\"red\" border=\"0\"> " +
				"<tr class=\"tabla2\"> <td width=\"1%\"><li></li></td> " +
				      "<td width=\"90%\" align=\"left\" style='cursor:hand;' onclick=\"javascript:fc_Linkear('"+ url +"');\" >&nbsp;&nbsp;&nbsp;&nbsp;" + secciones.getTema()+ "</td>" +
				      "<td>" +
				      ""+ imagen + "" +
				      "</td>" +
				/*"</tr>" +
		        "<tr class=\"tabla2\"><td colspan='3'><img src=\"/SGA/images/separador_n.gif\" width=\"100%\"></td>" +
				"</tr>" +*/
				"</table>"+
		        "<table width=\"100%\" bgcolor=\"white\"><tr height=\"2px\"><td></td></tr></table>";
		
	   // System.out.println(str);
		return str;
		
	}
	
	public String getRbtSeleccion(){
		buzonSugerencia = (BuzonSugerencia) getCurrentRowObject();
		//int i= getListIndex();
		String str="";
		String email="";
		if(buzonSugerencia.getEmail()!=null)
		   email=buzonSugerencia.getEmail();
		
		if(buzonSugerencia.getEstado().trim().equals("1")){
			str="<img src=\"/biblioteca/images/email_on.gif\" "; //email_on.gif
			//str="<img src=\"/SGA/images/botones/reservar.gif\" width=\"100%\"";
		}
		else
		{
			str ="<input type=\"radio\" name=\"radio\" value=\"" + buzonSugerencia.getCodSugerencia()//+"');\" >" ;
			+ "\"  onclick=\"javascript:fc_SeleccionarRegistro(this.value,'" + 
			buzonSugerencia.getFechaCrea() +"','"+buzonSugerencia.getDescripTipoSugerencia()+"','"+
			//buzonSugerencia.getDescripcion().trim()+"','"+buzonSugerencia.getDescripCalificacion().trim()+"','"+
			""+"','"+buzonSugerencia.getDescripCalificacion().trim()+"','"+
			buzonSugerencia.getDescripCodMaterial().trim()+"','"+buzonSugerencia.getDescripcionUsuario()+"'" +
					",'"+email+"');\" >" ;
		}
		//System.out.println(str);
        return str;
	}
	public String getMostrarComentario(){
		buzonSugerencia = (BuzonSugerencia) getCurrentRowObject();
		
		String cadena="<textarea style=\"width:90%\" readonly=\"true\" class=\"cajatexto_1\" rows=\"3\">" + buzonSugerencia.getDescripcion() + "</textarea>";	
		//System.out.println(cadena);
		return cadena;
	}
	
	public String getRbtSelferiado(){
		fer = (Feriados) getCurrentRowObject();
		
		String str ="<input type=\"radio\" name=\"radio\" value=\"" + fer.getCodigoSecuencial()
					+ "\"  onclick=\"javascript:fc_SeleccionarRegistro(this.value,'" + fer.getAnio() +"','"+ fer.getDescripcion()+"','" + fer.getDia() + "','" + fer.getCodMes() + "');\" >" ;
		
        return str;
	}
	
	public String getTextBusquedaAvanzada()
	{   lobject = (TipoTablaDetalle) getCurrentRowObject();
		int i= getListIndex();
		
		/*String ruta = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_IP +
		CommonConstants.STR_RUTA_SECCIONES+secciones.getArchivo();
		String archivo="";//secciones.getArchivo();
	    
		if((secciones.getArchivo()!=null)&&(!secciones.getArchivo().equals("")))
	     archivo="<img src=\""+ ruta + "\" style=\"width:100px;height:110px;\" border='1'>";
		else
	     archivo="";*/
		
		String peso="height=\"22px\"";
		String espacio="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		String imagen="";//<td align=right colspan="2" width="25%">&nbsp;</td>--------height=\"22px\"----border=\"2\" bordercolor=\"red\"
		String flagReservar="<img src=\"/biblioteca/images/botones/reservar.gif\" height=\"23px\" onclick=\"javascript:fc_Reservar();\">&nbsp; ";
		String verReserva="<td nowrap align=\"left\">Reservados&nbsp;:</td>"+
		  "<td align=\"left\"><input type=text size=\"3\" class=\"cajatexto_1\" value=\"3\" " +
		  "id=\"txtReservados"+i+"1\" name=\"txtReservados"+i+"1\"></td>";
		if(i==0){imagen="Foto5.jpg";
		         
		        }
		else{if(i==1){ imagen="Foto6.jpg";
						flagReservar=espacio;
						verReserva="";
		             }
		     else{ if(i==2){ imagen="Foto4.jpg";
		     			   
		                   }
		     	   else{ imagen="Foto3.jpg";
		     	         flagReservar=espacio;
		     	         verReserva="";
		     	       }
		           } 
		     }
		String str="<table width=\"100%\" align=\"left\" border=\"0\" bordercolor=\"red\"> " +
						"<tr> <td width=\"10%\" align=\"left\">Titulo</td>" +
				                 "<td width=\"40%\" align=\"left\" colspan=\"2\"> <input maxlength=\"50\" type=\"text\" class=\"cajatexto\"  size=\"60\" name=\"text"+i+"1\" value=\"" + lobject.getCodTipoTablaDetalle()
				                 + "\" id=\"text"+i+"1\"  " + "onblur=\"fc_ValidaSoloLetrasFinal1(this,'Titulo');\"  onkeypress=\"fc_ValidaTextoEspecial();\" ></td>" +
				                 "<td align=\"right\" colspan=\"2\" width=\"50px\" "+ peso +">"+ flagReservar +"</td>" +
				                 "<td rowspan=\"3\" width=\"15%\" valign='top'><img src=\"/biblioteca/images/"+ imagen + "\" style=\"width:100px;height:110px;\" border='1'></td>"+
				        "</tr>" +
						    "<tr><td width=\"10%\" align=\"left\">Autor</td> <td width=\"90%\" align=\"left\"> <input maxlength=\"2\" type=\"text\" class=\"cajatexto\"  size=\"60\" name=\"text"+i+"2\" value=\"" + lobject.getDscValor2()
						            + "\" id=\"text"+i+"2\"  " + "onblur=\"fc_ValidaSoloLetrasFinal1(this,'Autor');\"  onkeypress=\"fc_ValidaTextoEspecial();\" ></td>" +
						    "</tr>" +
						    "<tr><td width=\"10%\" align=\"left\">Clasificación</td> <td width=\"90%\" align=\"left\"> <input maxlength=\"2\" type=\"text\" class=\"cajatexto\"  size=\"20\" name=\"text"+i+"3\" value=\"" + lobject.getDescripcion()
						            + "\" id=\"text"+i+"3\"  " + "onblur=\"fc_ValidaSoloLetrasFinal1(this,'Clasificación');\"  onkeypress=\"fc_ValidaTextoEspecial();\" ></td>" +
						    "</tr>" +
						    "<tr><td colspan='5'><img src=\"/biblioteca/images/separador_n.gif\" width=\"100%\"></td>" +
						    "</tr>" +
				   "</table>";
		//class=\"tabla2\"  style=\"width:100%\" cellSpacing=\"2\" cellPadding=\"2\"
		String a="<table width=\"100%\" class=\"tabla2\" align=\"left\" border=\"0\" bordercolor='gray'>"+
			"<tr>"+
				"<td width=\"10%\" align=\"left\">Titulo:</td>" +
				"<td width=\"40%\" align=\"left\"><input type=text size=\"50\" class=\"cajatexto_1\" readonly value=\"Ingenieria: En todo su Contexto\" id=\"txtTitulo"+i+"1\" name=\"txtTitulo"+i+"1\" style=\"width:240px\">" +
				"&nbsp;<img src=\"/biblioteca/images/iconos/modificar2.jpg\" alt=\"Ver Ficha\" style=\"cursor:hand\"></td>" +							
				"<td align=right colspan=\"2\" width=\"25%\""+ peso +">"+ flagReservar +"</td>"+
				"<td rowspan=\"4\" width=\"15%\" valign='top'><img src=\"/biblioteca/images/"+ imagen + "\" style=\"width:100px;height:110px;\" border='1'></td>"+
	       "</tr>"+
	             
	        "<tr>"+
	     		"<td align=\"left\">Autor:</td>"+  
	     		"<td align=\"left\"><input type=text size=\"50\" class=\"cajatexto_1\" readonly value=\"Fernando Grillo\" id=\"txtAutor"+i+"1\"  name=\"txtAutor"+i+"1\"></td> "+
	     		"<td align=\"left\">Tipo Material:</td>"+
	     		"<td align=\"left\"><input type=text class=\"cajatexto_1\" readonly size=\"22\" id=\"txtMaterial"+i+"1\" name=\"txtMaterial"+i+"1\">&nbsp;</td>"+
	     	"</tr>"+
	     	
	     	"<tr>"+
	     		"<td align=\"left\">Clasificación:</td>"+
	     		"<td align=\"left\"><input type=text size=\"20\" class=\"cajatexto_1\" readonly value=\"\" id=\"txtDewey"+i+"1\" name=\"txtDewey"+i+"1\">" +
	     			  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nro. de Ejemplares:&nbsp;" +
	     		"<input type=text size=\"3\" class=\"cajatexto_1\" readonly value=\"6\" id=\"txtNroEjemplares"+i+"1\" name=\"txtNroEjemplares"+i+"1\"></td>"+
	     		"<td nowrap align=\"left\">Nro de Ingreso:</td>"+
	     		"<td align=\"left\"><input type=text size=\"10\" class=\"cajatexto_1\" readonly value=\"2055\" id=\"txtNroIngreso"+i+"1\" name=\"txtNroIngreso"+i+"1\"></td>"+
	     	"</tr>"+
	     	
	     	"<tr>"+
	    		"<td align=\"left\">Disponibles:</td>"+
	    		"<td align=\"left\" width=\"10%\"><input type=text size=\3\" class=\"cajatexto_1\" readonly value=\"3\" id=\"txtDisponible"+i+"1\" name=\"txtDisponible"+i+"1\">"+
	    		    "&nbsp;&nbsp;Prestados:&nbsp;&nbsp;"+
	    			"(En Sala&nbsp;<input type=text size=\"3\" class=\"cajatexto_1\" readonly value=\"2\" id=\"txtEnSala"+i+"1\" name=\"txtEnSala"+i+"1\">" +
	    			"&nbsp;Domicilio&nbsp;<input type=text size=\"3\" class=\"cajatexto_1\" readonly value=\"1\" id=\"txtDomicilio"+i+"1\" name=\"txtDomicilio"+i+"1\">)"+
	    		"</td>"+
	    		 verReserva+
	    	"</tr>"+
	    	
	    	"<tr>"+
	    		"<td colspan=\"5\" width=\"100%\"><img src=\"/biblioteca/images/separador_n.gif\" width=\"100%\">" +
	    		"</td>"+
	    	"</tr>"+
		    "</table>";
	 //   System.out.println(a);
		return a;
	}
	
	public String getTextBusquedaSimple()
	{   //lobject = (TipoTablaDetalle) getCurrentRowObject();
		busqueda=(Busqueda) getCurrentRowObject();
		int i= getListIndex();
		String ruta="";
		if(busqueda.getImagen()!=null){
			/*ruta = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_IP +
			  CommonConstants.STR_RUTA_MATERIAL_BIB_IMAGENES+busqueda.getImagen();*/
			//ruta ="\\\\"+ CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_MATERIAL_BIB_IMAGENES+busqueda.getImagen();
			ruta = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+
				CommonConstants.DIRECTORIO_BUSQUEDA+busqueda.getImagen();
		}
		//System.out.println("Ruta: "+ruta);
		String archivo="";//secciones.getArchivo();
	    String ancho="";
		if((busqueda.getImagen()!=null)&&(!busqueda.getImagen().equals("&nbsp;")))
	     {archivo="<img src=\""+ ruta + "\" style=\"width:95px;height:100px;\" border='1'>";
	      ancho="15%";}
		else
	     {archivo=" ";
	      ancho="15%";}
		String peso="height=\"22px\"";
		String espacio="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		
		String flagReservar="";
		if(!busqueda.getFlagReserva().equals("&nbsp;"))
		{ if(!busqueda.getFlagReserva().equals("0"))
			flagReservar="<img src=\"/biblioteca/images/botones/reservar.gif\" height=\"23px\" style=\"cursor:hand\" alt=\"Reservar\" " +
			"onclick=\"javascript:fc_Reservar('"+ busqueda.getCodUnico() +"','"+ busqueda.getFechaBD() +"','"+i+"1');\">&nbsp; ";
		
		}
		String verReserva="";
	    String isbn="";
		if(busqueda.getIsbn()!=null)
		   isbn=busqueda.getIsbn();
				   
		int al=Integer.valueOf(busqueda.getReserva());
		if(al>0)
		{ verReserva="<td nowrap align=\"left\">Reservados&nbsp;:</td>"+
						  "<td align=\"left\"><input type=text size=\"3\" class=\"cajatexto_1\"value=\""+ busqueda.getReserva() +"\" " +
						  "id=\"txtReservados"+i+"1\" name=\"txtReservados"+i+"1\"></td>";
		}
		else verReserva="";
		String ingreso="";
		
		if(!busqueda.getIsbn().equals("&nbsp;"))
			ingreso=busqueda.getIsbn();
			
		String a="<table style=\"border:1px solid #048BBA;table-layout:fixed;\" cellpadding=\"0\" cellspacing=\"5\" bordercolor=\"red\" border=\"0\" >" +
			"<colgroup><col width='110' /><col /><col width='110' /><col width='120' /><col width='30' /><col width='80' /><col  /><col width='100' /></colgroup>" +
			"<tr class=\"tabla3\" style=\"font-size: 11px;\">" + 
			     "<td width=\"110\" align=\"left\">CODIGO :</td>" +
			     "<td align=\"left\"><input type=\"text\" size=\"30\" class=\"cajatexto\" readonly value=\""+ busqueda.getCodigoGenerado() +"\" id=\"txtCodGenerado"+i+"1\"  name=\"txtCodGenerado"+i+"1\"></td> "+
			     //new columns
	     		"<td width=\"110\" align=\"left\">TIPO MATERIAL :</td>"+
	     		"<td width=\"140\" align=\"left\" colspan='2'><input type=text class=\"cajatexto\" readonly size=\"30\" value=\""+ busqueda.getTipoMaterial() +"\" id=\"txtMaterial"+i+"1\" name=\"txtMaterial"+i+"1\"></td>"+
	     		"<td width=\"80\" align=\"left\">SEDE :</td>"+
    			"<td align=\"left\"><input type=\"text\" size=\"32\" class=\"cajatexto\" readonly value=\""+ busqueda.getDscSede() +"\" id=\"txtSede"+i+"1\" name=\"txtSede"+i+"1\"></td>"+
    			
    			"<td width=\"100\" align=\"center\" ><img src=\"/biblioteca/images/iconos/visualizar2.jpg\" onclick=\"javascript:fc_detalleMaterial('"+ busqueda.getCodUnico() +"');\" alt=\"Ver Ficha\" style=\"cursor:hand\">" +
				"&nbsp; &nbsp;<img src=\"/biblioteca/images/iconos/adjuntar2.jpg\" onclick=\"javascript:fc_detalleAdjunto('"+ busqueda.getCodUnico() +"');\" alt=\"Ver Adjuntos\" style=\"cursor:hand\"></td>"+
    			
			"</tr>"+
			
		   "<tr class=\"tabla3\" style=\"font-size: 11px;\">"+
			    "<td align=\"left\">TITULO :</td>" +
				"<td align=\"left\" colspan='2'><textarea rows=\"3\" style=\"width:95%\" class=\"cajatexto\" onKeyPress=\"return false;\" readonly id=\"txtTitulo"+i+"1\" name=\"txtTitulo"+i+"1\">"+ busqueda.getTitulo() +"</textarea></td>" +
				
				"<td align=\"left\" colspan='2'>DESCRIPTORES:</td>"+
	     		"<td align=\"left\" colspan='2'><textarea rows=\"3\" style=\"width:95%\" class=\"cajatexto\" onKeyPress=\"return false;\" readonly  id=\"txtDescFisica"+i+"1\"  name=\"txtDescFisica"+i+"1\">"+ busqueda.getUbicacionFisica() +"</textarea></td>"+
				
	     		"<td rowspan=\"4\" valign=\"top\">" + archivo +"&nbsp;</td>"+
	       "</tr>"+
	             
	        "<tr class=\"tabla3\" style=\"font-size: 11px;\">"+
	     		"<td align=\"left\">AUTOR:</td>"+  
	     		"<td align=\"left\" colspan=\"2\"><input type=\"text\" style=\"width:95%\" size=\"60\" class=\"cajatexto\" readonly value=\""+ busqueda.getDescripAutor() +"\" id=\"txtAutor"+i+"1\"  name=\"txtAutor"+i+"1\"></td> "+
	     		
	     		"<td align=\"left\" colspan='2'>PIE IMPRENTA:</td>"+
	     		"<td align=\"left\" colspan=\"2\"><input type=\"text\" style=\"width:95%\" size=\"58\" class=\"cajatexto\" readonly  id=\"txtEditorial"+i+"1\" value=\""+ busqueda.getEditorial() +"\"  name=\"txtEditorial"+i+"1\"></td>" +
	     		
	     		//"<td align=\"left\">TIPO MATERIAL :</td>"+
	     		//"<td align=\"left\" colspan='2'><input type=text class=\"cajatexto\" readonly size=\"30\" value=\""+ busqueda.getTipoMaterial() +"\" id=\"txtMaterial"+i+"1\" name=\"txtMaterial"+i+"1\">&nbsp;</td>"+
	     	"</tr>"+
	     	
	     	"<tr class=\"tabla3\" style=\"font-size: 11px;\">"+
	     		
	     		"<td align=\"left\" rowspan=\"2\">CLASIFICACIÓN:</td>"+
	     		"<td align=\"left\" colspan='2' rowspan=\"2\"><textarea rows=\"3\" style=\"width:95%\" class=\"cajatexto\" onKeyPress=\"return false;\" readonly  id=\"txtDewey"+i+"1\"  name=\"txtDewey"+i+"1\">"+ busqueda.getDescripDewey() +"</textarea></td>"+
	     		
	     		"<td align=\"left\">DISPONIBLE: </td>"+
	    		"<td align=\"left\"><input type=\"text\" size=\"1\" style=\"text-align:center\" class=\"cajatexto\" readonly id=\"txtDisponible"+i+"1\" value=\""+ busqueda.getDisponibilidad() +"\"  name=\"txtDisponible"+i+"1\"></td>"+
	   
	    		"<td align=\"left\">PRESTADOS: </td>"+
    			"<td align=\"left\" style=\"padding-left:10px;\">En Sala&nbsp;&nbsp;&nbsp;<input type=\"text\" size=\"1\" style=\"text-align:center\" class=\"cajatexto\" readonly value=\""+ busqueda.getPrestaSala() +"\" id=\"txtEnSala"+i+"1\" name=\"txtEnSala"+i+"1\"></td>" +
    			    			
	     	"</tr>"+
	     	
	    	"<tr class=\"tabla3\" style=\"font-size: 11px;\">"+
		    	"<td align=\"left\">RESERVADOS: </td>"+
	    		"<td align=\"left\"><input type=text size=\"1\" style=\"text-align:center\" class=\"cajatexto\" readonly value=\""+ busqueda.getReserva() +"\" id=\"txtEnSala"+i+"1\" name=\"txtEnSala"+i+"1\"></td>" +
    		
	    		"<td align='left'>"+ flagReservar +"</td>"+
	    		"<td align=\"left\" style=\"padding-left:10px;\">Domicilio&nbsp;<input type=text size=\"1\" style=\"text-align:center\" class=\"cajatexto\" readonly value=\""+ busqueda.getPrestaDomicilio() +"\" id=\"txtDomicilio"+i+"1\" name=\"txtDomicilio"+i+"1\"></td>"+
	    	"</tr>"+
	    	
		    "</table>"+
		    "<table width=\"100%\" bgcolor=\"white\"><tr height=\"2px\"><td></td></tr></table>";
		
		return a;
	}//checkReservaSala
	public String getCheckReservaSala()
	{   
		sala = (Sala) getCurrentRowObject();
		
		String str ="<input type=\"checkbox\" name=\"codProceso\" id=\"chkSeleccion"+sala.getCodReserva()+
		"\" value=\"" + sala.getCodReserva() +
		"\"  onclick=\"javascript:fc_seleccionarRegistro('" + sala.getCodReserva()+  "','" + sala.getCodTipoSala()+  "', '" + 
		sala.getFechaReserva() +  "','" + sala.getHoraIni()+"','" + sala.getHoraFin()+ "'," +
		"'" + sala.getUsuReserva()+ "','" + sala.getUsuario()+ 
		"');fc_Seleccion(this.id,this.value,'" +
		sala.getCodTipoSala()+  "', '" + 
		sala.getUsuReserva() + "')\" >" ;
		
        return str;
	}
	
	public String getTextBibliotecas()//textBibliotecas
	{   secciones = (Secciones) getCurrentRowObject();
		int i= getListIndex();
		
		String url="";
		if(!secciones.getUrl().equals("&nbsp;"))
			url=secciones.getUrl();
		String al="mad";
		//System.out.println("URL: "+url);	 	
		String str="<table style=\"border:1px solid #048BBA\" bordercolor=\"red\" border=\"0\" width=\"98%\" > " +
						"<tr class=\"tabla2\"> " +
							"<td width=\"2%\">&nbsp;</td> " +	
							"<td width=\"1%\"><li></li></td> " +
						    "<td width=\"97%\" id=\"text"+i+"1\" name=\"text"+i+"1\" style='cursor:hand;' align=\"left\"  onclick=\"javascript:fc_Linkear('"+ url +"');\">" +
						            "" + secciones.getTema()+ "</td>" +
					    "</tr>" +
					   /* "<tr class=\"tabla2\">" +
					    	"<td colspan='2'>" +
					    	"<img src=\"/SGA/images/separador_n.gif\" width=\"100%\"></td>" +
						"</tr>" +*/
				   "</table>"+
				   "<table width=\"100%\" bgcolor=\"white\"><tr height=\"2px\"><td></td></tr></table>";
		//System.out.println(str);
		return str;
		
	}
	
	public String getTextOtrasFuentesInformacion()
	{   secciones = (Secciones) getCurrentRowObject();
		int i= getListIndex();
		String imagen="";
		String ruta ="";
		String url="";
		String archivo="";
		if(!secciones.getArchivo().equals("&nbsp;"))	
		 archivo=secciones.getArchivo();
		
		if(!secciones.getImagen().equals("&nbsp;"))
		{	ruta=secciones.getImagen();
			if(ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_DOC)) ruta="doc.gif"; //excel.gif doc.gif pdf.gif
		    else{ if(ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_XLS)) ruta="excel.gif";
		    	  else if(ruta.equalsIgnoreCase(CommonConstants.GSTR_EXTENSION_PDF)) ruta="pdf.gif";
		    	}
		}
			
		//secciones.getArchivo();
		    
		if(!secciones.getArchivo().equals("&nbsp;"))
		     imagen="<img src=\""+ archivo + "\" style=\"width:40px;height:40px;\" border='1'>";
		else
		    
		
		if(!secciones.getUrl().equals("&nbsp;"))
			url=secciones.getUrl();
		
		String str="<table width=\"70%\" align=\"left\"> " +
				"<tr> <td width=\"1%\"><li></li></td> " +
				      "<td width=\"90%\" style='cursor:hand;' align=\"left\" onclick=\"javascript:fc_Linkear('"+ url +"');\" >&nbsp;&nbsp;&nbsp;&nbsp;" + secciones.getTema()+ 
				      "</td>" +
				      "<td>" + imagen + "</td>"+
				"</tr>" +
				/*"<tr><td colspan='3'>" +
						    "<img src=\"/SGA/images/separador_n.gif\" width=\"100%\">" +
					 "</td>" +
				"</tr>" +*/
			"</table>"+
			"<table width=\"100%\" bgcolor=\"white\"><tr height=\"2px\"><td></td></tr></table>";
		//System.out.println(str);
		return str;
		
	}
	
	public String getTextReglamento()//textReglamento
	{   secciones = (Secciones) getCurrentRowObject();
				 	
		String cadena="<textarea style=\"width:90%; text-align:left; \" class=\"cajatexto_1\" readonly=\"true\" rows=\"18\">" + secciones.getDescripcionLarga() + "</textarea>";	
		//System.out.println(cadena);
		
		return cadena;
		
	}
	//textNovedadesInicio
	public String getTextNovedadesInicio()//textReglamento
	{   secciones = (Secciones) getCurrentRowObject();
	    String alianza="<td width=\"1%\" align=\"left\"><li></li></td>";
	    String imagen="";
	    if(!secciones.getArchivo().equals("&nbsp;"))
	    {	/*imagen = CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_IP +
	    	CommonConstants.STR_RUTA_SECCIONES+secciones.getArchivo();*/
	    	//"\\\\" + CommonConstants.SERVIDOR_NAME + CommonConstants.STR_RUTA_DOCUMENTOS
	    	/*imagen ="\\\\"+CommonConstants.SERVIDOR_NAME +
	    	CommonConstants.STR_RUTA_SECCIONES+secciones.getArchivo();*/
	    	imagen=CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+
	    		CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_NOVEDADES+secciones.getArchivo();
	    }
	    System.out.println("Ruta Imagen: "+imagen);
	    String descripcion="";
	    String panda="";
	    if(!secciones.getDescripcionGrupo().equals("&nbsp;"))
	    panda=secciones.getDescripcionGrupo();
	    
	    String archivo="";//secciones.getArchivo();
	    
	    if(!secciones.getArchivo().equals("&nbsp;"))
	     archivo="<img src=\""+ imagen + "\" style=\"width:100px;height:110px;cursor:pointer;\" border='1' onclick=\"javascript:Fc_Popup_AutoResizable2('"+ imagen+"');\" >";
	    else
	     archivo="";
	    
	    String val="";
	    if(!secciones.getDescripcionGrupo().equals("&nbsp;"))
	       val=secciones.getDescripcionGrupo();
	    int i= getListIndex();
	    if(i==0) descripcion=val;
	    
	    if(panda.equals(dota))
	    	descripcion="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	    else descripcion=val;
	    
	    String cadena="<table style=\"border:1px solid #048BBA\" bordercolor=\"red\" border=\"0\" >" +
	    		"<tr class=\"tabla2\">"+
	    			"<td width=\"2%\"></td><td colspan=\"3\" align=\"left\" >"+ descripcion+ "</td></tr>" +
    			"<tr>"+alianza+"<td align=\"left\" width=\"68%\" colspan=\"2\">" + secciones.getTema() + 
	    		"</td>"+	    		
	    		"<td rowspan=\"3\" width=\"15%\" height=\"110px\" align=\"center\">" +
	    		archivo+
	    		"</td>"+
	    "</tr>"+
	    
		"<tr> " +
		"<td width=\"2%\"></td><td valign=\"top\" align=\"left\" height=\"80px\" colspan=\"2\">"+ secciones.getDescripcionCorta() + "</td>"+		
		"</tr>"+
		
	    "<tr>"+
	           "<td colspan=\"2\"></td>"+
	    	   "<td align=\"right\" width=\"10%\" class=\"texto_vermas\" style=\"cursor:hand;\" " +
	    		"onclick=\"javascript:fc_VerMas('"+ secciones.getCodUnico()+"');\">ver mas...</td>"+				
	    "</tr>" +
	    		"</table>"+
	    		"<table width=\"100%\" bgcolor=\"white\"><tr height=\"2px\"><td></td></tr></table>";
	    dota=panda;
	return cadena;
		
	}
	public String getTextEnlacesPortal()//textBibliotecas
	{   secciones = (Secciones) getCurrentRowObject();
		int i= getListIndex();
	//textEnlacesPortal
		String str="<table width=\"10%\" border=\"2\" bordercolor=\"blue\" align=\"left\"> <tr> <td width=\"1%\"><li></li></td> " +
			"<td width=\"50%\" style='cursor:hand;' align=\"left\">&nbsp;&nbsp;&nbsp;&nbsp;" + secciones.getTema()+ "</td>" +
			"</tr>" +
			"<tr><td colspan='2'>" +
			"<img src=\"/biblioteca/images/separador_n.gif\" width=\"100%\"></td>" +
			"</tr>" +
			"</table>";
	//System.out.println(str);
	return str;
	}
	
	public String getTextBibliotecasPortal()//textBibliotecas
	{   secciones = (Secciones) getCurrentRowObject();
		int i= getListIndex();
			
		String str="<table width=\"10%\" border=\"2\" bordercolor=\"yellow\" align=\"left\"> <tr> <td width=\"1%\"><li></li></td> " +
			"<td width=\"50%\" style='cursor:hand;' align=\"left\">&nbsp;&nbsp;&nbsp;&nbsp;" + secciones.getTema()+ "</td>" +
			"</tr>" +
			"<tr><td colspan='2'>" +
			"<img src=\"/biblioteca/images/separador_n.gif\" width=\"100%\"></td>" +
			"</tr>" +
			"</table>";
	//System.out.println(str);
	return str;
	}
	
	public String getTextOtrasFuentesPortal()//textBibliotecas
	{   secciones = (Secciones) getCurrentRowObject();
		int i= getListIndex();
	
		String str="<table width=\"10%\" border=\"2\" bordercolor=\"red\" align=\"left\"> <tr> <td width=\"1%\"><li></li></td> " +
			"<td width=\"50%\" style='cursor:hand;' align=\"left\">&nbsp;&nbsp;&nbsp;&nbsp;" + secciones.getTema()+ "</td>" +
			"</tr>" +
			"<tr><td colspan='2'>" +
			"<img src=\"/biblioteca/images/separador_n.gif\" width=\"100%\"></td>" +
			"</tr>" +
			"</table>";
	//System.out.println(str);
	return str;
	}
	
	public String getRbtSelTipoMaterial()
	{   busqueda = (Busqueda) getCurrentRowObject();
		int i= getListIndex();
		
		String str ="<input type=\"radio\" name=\"text\" id=\"text"+i+"\" value=\"" + 
		busqueda.getTipoMaterial()+"|"+busqueda.getTitulo()
		+ "\"  onclick=\"javascript:fc_SeleccionarRegistro('" + busqueda.getCodUnico() +  "'," +
			"'" + busqueda.getIsbn()+  "', 'text"+i+"','" + busqueda.getFechaBD() + "','"+ busqueda.getDescripDewey() +"');\" >" ;
		//System.out.println(str);
	return str;
	}
	
}
