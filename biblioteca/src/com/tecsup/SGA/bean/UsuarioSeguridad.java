package com.tecsup.SGA.bean;

import java.util.List;

import com.tecsup.SGA.modelo.UsuarioSeguriBib;

public class UsuarioSeguridad {
	private String idUsuario;
	private String nomUsuario;
	private String apeUsuario;
	private String correo;	
	private String codUsuario;
	private String perfiles;
	private String opciones;
	private String periodo;
	private String Sede;	
	private List<UsuarioPerfil> roles;
	private String sedeOperacion;
	private String tipoUsuario;
	private String especialidad;
	private String codigoEspecialidad;
	private String ciclo;
	private boolean esTecsup;
	private String estadoActivo;
	private String opcionesApoyo;
	
	public String getCodigoEspecialidad() {
		return codigoEspecialidad;
	}

	public void setCodigoEspecialidad(String codigoEspecialidad) {
		this.codigoEspecialidad = codigoEspecialidad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	public List<UsuarioPerfil> getRoles() {
		return roles;
	}

	public void setRoles(List<UsuarioPerfil> roles) {
		this.roles = roles;
	}

	//JHPR 2008-06-18
	private UsuarioSeguriBib usuarioSeguriBib;
	
	
	public String getSede() {
		return Sede;
	}

	public void setSede(String sede) {
		Sede = sede;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomUsuario() {
		return nomUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getOpciones() {
		return opciones;
	}

	public void setOpciones(String opciones) {
		this.opciones = opciones;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(String perfiles) {
		this.perfiles = perfiles;
	}

	public UsuarioSeguriBib getUsuarioSeguriBib() {
		return usuarioSeguriBib;
	}

	public void setUsuarioSeguriBib(UsuarioSeguriBib usuarioSeguriBib) {
		this.usuarioSeguriBib = usuarioSeguriBib;
	}

	public String getSedeOperacion() {
		return sedeOperacion;
	}

	public void setSedeOperacion(String sedeOperacion) {
		this.sedeOperacion = sedeOperacion;
	}

	public String getApeUsuario() {
		return apeUsuario;
	}

	public void setApeUsuario(String apeUsuario) {
		this.apeUsuario = apeUsuario;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	} 
	
//	public String toString(){
//		return "Username: " + this.codUsuario + "\n" + 
//			"Nombre: " + this.nomUsuario + "\n" +
//			"Apellido: " + this.apeUsuario + "\n" +
//			"Correo: " + this.correo;
//	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public boolean isEsTecsup() {
		return esTecsup;
	}

	public void setEsTecsup(boolean esTecsup) {
		this.esTecsup = esTecsup;
	}

	public String getEstadoActivo() {
		return estadoActivo;
	}

	public void setEstadoActivo(String estadoActivo) {
		this.estadoActivo = estadoActivo;
	}

	public String getOpcionesApoyo() {
		return opcionesApoyo;
	}

	public void setOpcionesApoyo(String opcionesApoyo) {
		this.opcionesApoyo = opcionesApoyo;
	}

	@Override
	public String toString() {
		return "UsuarioSeguridad [idUsuario=" + idUsuario + ", nomUsuario="
				+ nomUsuario + ", apeUsuario=" + apeUsuario + ", correo="
				+ correo + ", codUsuario=" + codUsuario + ", perfiles="
				+ perfiles + ", opciones=" + opciones + ", periodo=" + periodo
				+ ", Sede=" + Sede + ", roles=" + roles + ", sedeOperacion="
				+ sedeOperacion + ", tipoUsuario=" + tipoUsuario
				+ ", especialidad=" + especialidad + ", esTecsup=" + esTecsup
				+ ", estadoActivo=" + estadoActivo + ", opcionesApoyo="
				+ opcionesApoyo + ", usuarioSeguriBib=" + usuarioSeguriBib
				+ "]";
	}
	
}
