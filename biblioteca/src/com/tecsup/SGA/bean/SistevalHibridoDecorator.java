package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.TipoDetalle;

public class SistevalHibridoDecorator extends TableDecorator {
	private SistevalHibrido lobject;
	
	public String getRbtSelDetalle()
	{
		lobject = (SistevalHibrido) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"codProceso\" value=\"" + lobject.getCodSec()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro(this.value,'" + lobject.getDescripcion() +  "');\" >" ;
        return str;
	}
}
