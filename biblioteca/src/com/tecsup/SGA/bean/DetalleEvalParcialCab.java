package com.tecsup.SGA.bean;

import java.util.List;

public class DetalleEvalParcialCab {

	private String codigo;
	private String nombreAlumno;
	private List resultado;
	
	private String nroEvaluacion; //JHPR 2008-07-01
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public List getResultado() {
		return resultado;
	}
	public void setResultado(List resultado) {
		this.resultado = resultado;
	}
	public String getNroEvaluacion() {
		return nroEvaluacion;
	}
	public void setNroEvaluacion(String nroEvaluacion) {
		this.nroEvaluacion = nroEvaluacion;
	} 
	
}
