package com.tecsup.SGA.bean;

public class IncidenciaBean implements java.io.Serializable{
	private String codAlumno;
	private String nomAlumno;
	private String totalIncidencias;
	
	
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNomAlumno() {
		return nomAlumno;
	}
	public void setNomAlumno(String nomAlumno) {
		this.nomAlumno = nomAlumno;
	}
	public String getTotalIncidencias() {
		return totalIncidencias;
	}
	public void setTotalIncidencias(String totalIncidencias) {
		this.totalIncidencias = totalIncidencias;
	}
	
	
	
}
