package com.tecsup.SGA.bean;

import java.util.List;

public class EncuestaManualBean {
		
	private String codEncuestado;
	private String fecRegistro;
	private String idGrupo;
	private String nivelAlternativa;
	private String idPregunta;
	private String nomPregunta;
	private String cadNomAlternativa;
	private String indObligatorio;
	private String rpta;
	private String tipoPregunta;
	private String indUnica;	
	private List lstPreguntas;
	
	public List getLstPreguntas() {
		return lstPreguntas;
	}
	public void setLstPreguntas(List lstPreguntas) {
		this.lstPreguntas = lstPreguntas;
	}
	public String getCodEncuestado() {
		return codEncuestado;
	}
	public void setCodEncuestado(String codEncuestado) {
		this.codEncuestado = codEncuestado;
	}
	public String getFecRegistro() {
		return fecRegistro;
	}
	public void setFecRegistro(String fecRegistro) {
		this.fecRegistro = fecRegistro;
	}
	public String getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}
	public String getNivelAlternativa() {
		return nivelAlternativa;
	}
	public void setNivelAlternativa(String nivelAlternativa) {
		this.nivelAlternativa = nivelAlternativa;
	}
	public String getIdPregunta() {
		return idPregunta;
	}
	public void setIdPregunta(String idPregunta) {
		this.idPregunta = idPregunta;
	}
	public String getNomPregunta() {
		return nomPregunta;
	}
	public void setNomPregunta(String nomPregunta) {
		this.nomPregunta = nomPregunta;
	}
	public String getCadNomAlternativa() {
		return cadNomAlternativa;
	}
	public void setCadNomAlternativa(String cadNomAlternativa) {
		this.cadNomAlternativa = cadNomAlternativa;
	}
	public String getTipoPregunta() {
		return tipoPregunta;
	}
	public void setTipoPregunta(String tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}
	public String getIndUnica() {
		return indUnica;
	}
	public void setIndUnica(String indUnica) {
		this.indUnica = indUnica;
	}
	public String getRpta() {
		return rpta;
	}
	public void setRpta(String rpta) {
		this.rpta = rpta;
	}
	public String getIndObligatorio() {
		return indObligatorio;
	}
	public void setIndObligatorio(String indObligatorio) {
		this.indObligatorio = indObligatorio;
	}
	
}
