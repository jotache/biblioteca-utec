package com.tecsup.SGA.bean;

public class ValidaReporteBean implements java.io.Serializable{
	
	private String codResultado;
	private String desResultado;
	private String idEncuesta;
	private String codProducto;
	private String codPrograma;
	
	public String getCodResultado() {
		return codResultado;
	}
	public void setCodResultado(String codResultado) {
		this.codResultado = codResultado;
	}
	public String getDesResultado() {
		return desResultado;
	}
	public void setDesResultado(String desResultado) {
		this.desResultado = desResultado;
	}
	public String getIdEncuesta() {
		return idEncuesta;
	}
	public void setIdEncuesta(String idEncuesta) {
		this.idEncuesta = idEncuesta;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getCodPrograma() {
		return codPrograma;
	}
	public void setCodPrograma(String codPrograma) {
		this.codPrograma = codPrograma;
	}
	
			
}
