package com.tecsup.SGA.bean;

public class UsuarioBean {
	
	private String nomUsuario;
	private String indTipoUsuario;
	private String codTipoUsuario;
	private String nomTipoUsuario;
	private String nomEspecialidad;
	private String nomCiclo;
	private String nomCargoPersonal;
	
	public String getNomUsuario() {
		return nomUsuario;
	}
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	public String getIndTipoUsuario() {
		return indTipoUsuario;
	}
	public void setIndTipoUsuario(String indTipoUsuario) {
		this.indTipoUsuario = indTipoUsuario;
	}
	public String getCodTipoUsuario() {
		return codTipoUsuario;
	}
	public void setCodTipoUsuario(String codTipoUsuario) {
		this.codTipoUsuario = codTipoUsuario;
	}
	public String getNomTipoUsuario() {
		return nomTipoUsuario;
	}
	public void setNomTipoUsuario(String nomTipoUsuario) {
		this.nomTipoUsuario = nomTipoUsuario;
	}
	public String getNomEspecialidad() {
		return nomEspecialidad;
	}
	public void setNomEspecialidad(String nomEspecialidad) {
		this.nomEspecialidad = nomEspecialidad;
	}
	public String getNomCiclo() {
		return nomCiclo;
	}
	public void setNomCiclo(String nomCiclo) {
		this.nomCiclo = nomCiclo;
	}
	public String getNomCargoPersonal() {
		return nomCargoPersonal;
	}
	public void setNomCargoPersonal(String nomCargoPersonal) {
		this.nomCargoPersonal = nomCargoPersonal;
	}
	
}
