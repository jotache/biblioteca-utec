package com.tecsup.SGA.bean;

import java.io.Serializable;
import java.util.List;

public class HojaVidaDatosBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public HojaVidaDatosBean(){}
	//Estudios
	private String codProfesion;
	private String codInstitucion;
	private String otraInstitucion;
	private String codGradoAcademico;
	private String ciclosEstudiados;
	private String codMerito;
	private String inicio;
	private String termino;
	private List listaProfesiones;
	private List listaInsituciones;
	private List listaMeritos;
	private List listaGradosAcademicos;
	
	//Experiencia Laboral
	private String empresa;
	private String codPuesto;
	private String otroPuesto;
	private String referencia;
	private String telefonoContacto;
	private String descripcionActividades;
	private List listaPuestos;
	
	public String getCodProfesion() {
		return codProfesion;
	}
	public void setCodProfesion(String codProfesion) {
		this.codProfesion = codProfesion;
	}
	public String getCodInstitucion() {
		return codInstitucion;
	}
	public void setCodInstitucion(String codInstitucion) {
		this.codInstitucion = codInstitucion;
	}
	public String getOtraInstitucion() {
		return otraInstitucion;
	}
	public void setOtraInstitucion(String otraInstitucion) {
		this.otraInstitucion = otraInstitucion;
	}
	public String getCodGradoAcademico() {
		return codGradoAcademico;
	}
	public void setCodGradoAcademico(String codGradoAcademico) {
		this.codGradoAcademico = codGradoAcademico;
	}
	public String getCiclosEstudiados() {
		return ciclosEstudiados;
	}
	public void setCiclosEstudiados(String ciclosEstudiados) {
		this.ciclosEstudiados = ciclosEstudiados;
	}
	public String getCodMerito() {
		return codMerito;
	}
	public void setCodMerito(String codMerito) {
		this.codMerito = codMerito;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getTermino() {
		return termino;
	}
	public void setTermino(String termino) {
		this.termino = termino;
	}
	public List getListaProfesiones() {
		return listaProfesiones;
	}
	public void setListaProfesiones(List listaProfesiones) {
		this.listaProfesiones = listaProfesiones;
	}
	public List getListaInsituciones() {
		return listaInsituciones;
	}
	public void setListaInsituciones(List listaInsituciones) {
		this.listaInsituciones = listaInsituciones;
	}
	public List getListaMeritos() {
		return listaMeritos;
	}
	public void setListaMeritos(List listaMeritos) {
		this.listaMeritos = listaMeritos;
	}
	public List getListaGradosAcademicos() {
		return listaGradosAcademicos;
	}
	public void setListaGradosAcademicos(List listaGradosAcademicos) {
		this.listaGradosAcademicos = listaGradosAcademicos;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getCodPuesto() {
		return codPuesto;
	}
	public void setCodPuesto(String codPuesto) {
		this.codPuesto = codPuesto;
	}
	public String getOtroPuesto() {
		return otroPuesto;
	}
	public void setOtroPuesto(String otroPuesto) {
		this.otroPuesto = otroPuesto;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getTelefonoContacto() {
		return telefonoContacto;
	}
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}
	public String getDescripcionActividades() {
		return descripcionActividades;
	}
	public void setDescripcionActividades(String descripcionActividades) {
		this.descripcionActividades = descripcionActividades;
	}
	public List getListaPuestos() {
		return listaPuestos;
	}
	public void setListaPuestos(List listaPuestos) {
		this.listaPuestos = listaPuestos;
	}
	
	
}
