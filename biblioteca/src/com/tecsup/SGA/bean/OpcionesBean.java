package com.tecsup.SGA.bean;

public class OpcionesBean {
	
	private String codOpcion;//ID
	private String nomOpcion;
	private String nomCodOpcionABC;
	
	public String getNomCodOpcionABC() {
		return nomCodOpcionABC;
	}
	public void setNomCodOpcionABC(String nomCodOpcionABC) {
		this.nomCodOpcionABC = nomCodOpcionABC;
	}
	public String getCodOpcion() {
		return codOpcion;
	}
	public void setCodOpcion(String codOpcion) {
		this.codOpcion = codOpcion;
	}
	public String getNomOpcion() {
		return nomOpcion;
	}
	public void setNomOpcion(String nomOpcion) {
		this.nomOpcion = nomOpcion;
	}
	
}
