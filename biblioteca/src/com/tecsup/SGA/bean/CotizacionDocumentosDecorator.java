/**
 * 
 */
package com.tecsup.SGA.bean;

import java.util.*;
import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.CotizacionAdjunto;

/**
 * @author       CosapiSoft S.A.
 * @date         10-oct-07
 * @description  none
 */

public class CotizacionDocumentosDecorator extends TableDecorator{
	
	private CotizacionAdjunto lObject;
	
	public String addRowClass()
	{
		return "texto";
	}
	
	public String getRbtSelArchivo(){
		lObject = (CotizacionAdjunto) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"documentoAdjunto\" onclick=\"javascript:fc_seleccionarRegistro('" + lObject.getCodAdjunto() + "');\" >" ;
        return str;
	}
	
	public String getImgVerArchivo()
	{
		lObject = (CotizacionAdjunto) getCurrentRowObject();
		String str = "<img src=\"/biblioteca/images/iconos/buscar1.jpg\" style=\"cursor:pointer\" " +
					"onclick=\"fc_VerDocumento('" + lObject.getRutaAdjunto().trim() + "');\"/>";
        return str;
	}
	
}
