package com.tecsup.SGA.bean;

public class SedeBean {
	private String codSede;
	private String dscSede;
	
	public String getCodSede() {
		return codSede;
	}
	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}
	public String getDscSede() {
		return dscSede;
	}
	public void setDscSede(String dscSede) {
		this.dscSede = dscSede;
	}
	
	
}
