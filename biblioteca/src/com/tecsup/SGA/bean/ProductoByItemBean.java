package com.tecsup.SGA.bean;

public class ProductoByItemBean {

	private String codReq;
	private String codReqDet;
	private String nroReq;
	private String usuSolicitante;
	private String cantidad;
	private String fechaEntrega;
	
	public String getCodReq() {
		return codReq;
	}
	public void setCodReq(String codReq) {
		this.codReq = codReq;
	}
	public String getCodReqDet() {
		return codReqDet;
	}
	public void setCodReqDet(String codReqDet) {
		this.codReqDet = codReqDet;
	}
	public String getNroReq() {
		return nroReq;
	}
	public void setNroReq(String nroReq) {
		this.nroReq = nroReq;
	}
	public String getUsuSolicitante() {
		return usuSolicitante;
	}
	public void setUsuSolicitante(String usuSolicitante) {
		this.usuSolicitante = usuSolicitante;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	
}
