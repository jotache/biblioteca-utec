package com.tecsup.SGA.bean;

import java.util.ArrayList;
import java.util.List;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class HojaVidaDatosDecorator extends TableDecorator {

	private HojaVidaDatosBean recluta; // datos "en registros" de cada estudios.
	
	public HojaVidaDatosDecorator(){}
	
	public String getExperienciasLaborales(){
		recluta = (HojaVidaDatosBean) getCurrentRowObject();
		int i= getListIndex();
		String cad = "";
		
		//Institucion
		String strPuesto="";
		List listaPuestos = (ArrayList) recluta.getListaPuestos();
		strPuesto = "<select ID=\"cboPuesto" + i + "\"" +
				" class=\"cajatexto_1_black\" "  +
				"style=\"width:315px\" >" +
				"<option selected value=''>--Seleccione--</option>" ;
			for (int x = 0; x < listaPuestos.size(); x++){
				TipoTablaDetalle tablaDet = (TipoTablaDetalle) listaPuestos.get(x);
				if (tablaDet.getCodTipoTablaDetalle().trim().equalsIgnoreCase(recluta.getCodPuesto().trim()))
					strPuesto = strPuesto + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "' selected=\"selected\" > " + tablaDet.getDescripcion().trim() + "</option>";
				else
					strPuesto = strPuesto + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "'> " + tablaDet.getDescripcion().trim() + "</option>";				
			}
			strPuesto = strPuesto + "</select>";		
		
		cad = "<table cellspacing=\"4\" cellpadding=\"0\" class=\"tabla\" style=\"margin-left: 0px; margin-top: 5px; width:100%" + 
		"border=\"0\" bordercolor='blue'>" +
		"<tr>" +
			"<td width='20%' align='left'>&nbsp;Organizaci�n/Empresa :</td>" +
			"<td colspan='2' align='left'>" +
			"<input type=\"text\" class=\"cajatexto_1_black\"  maxlength='100' size=\"60\" name=\"txtOrganizacion"+i+"\" value=\"" + recluta.getEmpresa().trim() +
	    	"\" id=\"txtOrganizacion"+i+"\"  " + " readonly=\"true\" /> " +
			"</td>" +
			"<td></td>" +				
		"</tr>" +
		"<tr>" +
			"<td width='20%' align='left'>&nbsp;Puesto :</td>" +
			"<td colspan='2' align='left'>" +	
			
			strPuesto +
			
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td width='20%' align='left'>&nbsp;Otro Puesto :</td>" +
			"<td colspan='2'>"	+
			"<input type=\"text\" class=\"cajatexto_1_black\"  maxlength='100' size=\"60\" name=\"txtOtroPuesto"+i+"\" value=\"" + recluta.getOtroPuesto().trim() +
	    	"\" id=\"txtOtroPuesto"+i+"\"  " + " readonly=\"true\" /> " +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td width='20%' align='left'>&nbsp;Fecha Inicio :</td>" +
			"<td colspan='2' align='left'>" +
			"<input type=\"text\" class=\"cajatexto_1_black\"  maxlength='10' size=\"12\" name=\"txtFechaInicio"+i+"\" value=\"" + recluta.getInicio().trim() +
	    	"\" id=\"txtFechaInicio"+i+"\"  " + " readonly=\"true\" /> " +
			"</td>" + 
		"</tr>" +
		"<tr>" +
			"<td width='20%' align='left'>&nbsp;Fecha Fin :</td>" +
			"<td colspan='2' align='left'>" +
			"<input type=\"text\" class=\"cajatexto_1_black\"  maxlength='10' size=\"12\" name=\"txtFechaFin"+i+"\" value=\"" + recluta.getTermino().trim() +
	    	"\" id=\"txtFechaFin"+i+"\"  " + " readonly=\"true\" /> " +
			"</td>" +
		"</tr>" +
		"<tr>" +
			"<td width='20%' align='left'>&nbsp;Persona de Referencia :</td>" +
			"<td width='30%' align='left'>" +
			"<input type=\"text\" class=\"cajatexto_1_black\"  maxlength='100' size=\"40\" name=\"txtPersonaRef"+i+"\" value=\"" + recluta.getReferencia().trim() +
	    	"\" id=\"txtPersonaRef"+i+"\"  " + " readonly=\"true\" /> " +
			"<td width='30%' align='right'>Telefono Contacto :</td>" +
			"<td width='20%'>&nbsp;" +
			"<input type=\"text\" class=\"cajatexto_1_black\"  maxlength='20' size=\"20\" name=\"txtTelefonoRef"+i+"\" value=\"" + recluta.getTelefonoContacto().trim() +
	    	"\" id=\"txtTelefonoRef"+i+"\"  " + " readonly=\"true\" /> " +
		"</tr>" +
		"<tr>" +
			"<td width='20%' valign='middle' align='left'>&nbsp;Descripci�n de las<br>&nbsp;Actividades del Puesto :</td>" +
			"<td colspan='3' align='left'>" +			
			"<textarea id=\"txtDesPuesto"+i+"\" class='cajatexto_1_black' onKeyPress='return false;' rows='4' cols='120'>" + recluta.getDescripcionActividades().trim() +
			"</textarea>" +
			"</td>" +
		"</tr>" +
	"</table>" ;			
	
		
		return cad;
	}
	
	public String getEstudiosSuperiores(){
		recluta = (HojaVidaDatosBean) getCurrentRowObject();
		int i= getListIndex();
				
		String cad = "";
					
		//Profesion
		List listaProfesiones = (ArrayList) recluta.getListaProfesiones();
		String strProfesion = "";
		strProfesion = "<select ID=\"cboAreaEstudio" + i + "\"" +
				" class=\"cajatexto_1_black\" "  +
			"style=\"width:200px\" >" +
			"<option selected value=''>--Seleccione--</option>" ;
			for (int x = 0; x < listaProfesiones.size(); x++){
				TipoTablaDetalle tablaDet = (TipoTablaDetalle) listaProfesiones.get(x);
				if (tablaDet.getCodTipoTablaDetalle().trim().equalsIgnoreCase(recluta.getCodProfesion().trim()))
					strProfesion = strProfesion + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "' selected=\"selected\" > " + tablaDet.getDescripcion().trim() + "</option>";
				else
					strProfesion = strProfesion + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "'> " + tablaDet.getDescripcion().trim() + "</option>";
			}
			strProfesion = strProfesion + "</select>";
		

		//Institucion
		String strInstitucion="";
		List listaInstituciones = (ArrayList) recluta.getListaInsituciones();
		strInstitucion = "<select ID=\"cboInstitucion" + i + "\"" +
				" class=\"cajatexto_1_black\" "  +
				"style=\"width:230px\" >" +
				"<option selected value=''>--Seleccione--</option>" ;
			for (int x = 0; x < listaInstituciones.size(); x++){
				TipoTablaDetalle tablaDet = (TipoTablaDetalle) listaInstituciones.get(x);
				if (tablaDet.getCodTipoTablaDetalle().trim().equalsIgnoreCase(recluta.getCodInstitucion().trim()))
					strInstitucion = strInstitucion + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "' selected=\"selected\" > " + tablaDet.getDescripcion().trim() + "</option>";
				else
					strInstitucion = strInstitucion + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "'> " + tablaDet.getDescripcion().trim() + "</option>";				
			}
			strInstitucion = strInstitucion + "</select>";
		
			
		//Grado Acad�mico
		String gradoAcademico = "";
		List listaGradosAcademicos = (ArrayList) recluta.getListaGradosAcademicos();
		gradoAcademico = "<select ID=\"cboGradoAcad" + i + "\"" +
				" class=\"cajatexto_1_black\" "  +
				"style=\"width:200px\" >" +
				"<option selected value=''>--Seleccione--</option>" ;
			for (int x = 0; x < listaGradosAcademicos.size(); x++){
				TipoTablaDetalle tablaDet = (TipoTablaDetalle) listaGradosAcademicos.get(x);
				if (tablaDet.getCodTipoTablaDetalle().trim().equalsIgnoreCase(recluta.getCodGradoAcademico().trim()))
					gradoAcademico = gradoAcademico + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "' selected=\"selected\" > " + tablaDet.getDescripcion().trim() + "</option>";
				else
					gradoAcademico = gradoAcademico + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "'> " + tablaDet.getDescripcion().trim() + "</option>";				
				
			}
		gradoAcademico = gradoAcademico + "</select>";
			
		//Merito
		String merito = "";
		List listaMeritos = (ArrayList) recluta.getListaMeritos();
		merito = "<select ID=\"cboMerito" + i + "\"" +
				" class=\"cajatexto_1_black\" "  +
				"style=\"width:120px\" >" +
				"<option selected value=''>--Seleccione--</option>" ;
			for (int x = 0; x < listaMeritos.size(); x++){
				TipoTablaDetalle tablaDet = (TipoTablaDetalle) listaMeritos.get(x);
				if (tablaDet.getCodTipoTablaDetalle().trim().equalsIgnoreCase(recluta.getCodMerito().trim()))
					merito = merito + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "' selected=\"selected\" > " + tablaDet.getDescripcion().trim() + "</option>";
				else
					merito = merito + "<option value='" + tablaDet.getCodTipoTablaDetalle() + "'> " + tablaDet.getDescripcion().trim() + "</option>";				
				
			}
		merito = merito + "</select>";
			
		cad = "<table cellspacing=\"4\" cellpadding=\"0\" class=\"tabla\" style=\"margin-left: 0px; margin-top: 5px; width:100%" + 
			"border=\"1\" bordercolor='blue'>" +
			"<tr><td width=\"20%\" align=\"left\">&nbsp;Profesi�n :</td> " +
				"<td width='30%' align=\"left\">" +
				
				strProfesion +
		 					 			
				"</td>" +
				"<td width='20%'>&nbsp;</td>" +
				"<td width='30%'>&nbsp;</td>" +
			"</tr>" +					
			"<tr> " + 
				"<td width=\"20%\" align='left'>&nbsp;Instituci�n :</td> " +				
				"<td width=\"30%\" align='left'>" +
				
				strInstitucion +			
												
			"</td>" +
			"<td width=\"20%\" class='' align='left'>Otra Instituci�n :</td>" +
			"<td width=\"30%\" align='left'>" +			
			
			"<input type=\"text\" class=\"cajatexto_1_black\"  size=\"40\" name=\"txtOtraInstitucion"+i+"\" value=\"" + recluta.getOtraInstitucion().trim() +
            	"\" id=\"txtOtraInstitucion"+i+"\"  " + " readonly=\"true\" /> " +
            	
        	"</td>" +
			"</tr>" +
			"<tr>" +
				"<td width=\"20%\" class='' align='left'>&nbsp;Grado Acad�mico :</td>" +
				"<td width=\"30%\" align='left'>" +
				
				gradoAcademico +
				
			"</td>" +
			"<td width=\"20%\" class='' align='left'>Ciclos Estudiados :</td>" +
			"<td width=\"30%\" align='left'>" +
			
			"<input type=\"text\"  maxlength=\"2\" class=\"cajatexto_1_black\"  size=\"5\" name=\"txtCiclo"+i+"\" value=\"" + recluta.getCiclosEstudiados().trim() +
        	"\" id=\"txtCiclo"+i+"\"  " + " readonly=\"true\" /> " +
        	
			"</td>" +
			"</tr>" +
			"<tr>" +
				"<td width=\"20%\" align='left'>&nbsp;M�rito :</td>" +
				"<td width=\"30%\" align='left'>" +
				
				merito + 

			"</td>" +
			"</tr>" +
			"<tr>" +
			"<td width=\"20%\" class='' align='left'>&nbsp;Inicio :</td>" +
			"<td width=\"30%\" align='left'>" +
			
			"<input type=\"text\"  maxlength=\"7\" class=\"cajatexto_1_black\"  size=\"8\" name=\"txtInicio"+i+"\" value=\"" + recluta.getInicio().trim() +
        	"\" id=\"txtInicio"+i+"\"  " + " readonly=\"true\" /> " +

			"</td>" +
			"<td width=\"20%\" class='' align='left'>T�rmino :</td>" +
			"<td width=\"30%\" align='left'>" +
			"<input type=\"text\"  maxlength=\"7\" class=\"cajatexto_1_black\"  size=\"8\" name=\"txtTermino"+i+"\" value=\"" + recluta.getTermino().trim() +
        	"\" id=\"txtTermino"+i+"\"  " + " readonly=\"true\" /> " +
				"</td>" +
			"</tr>" +
		"</table>" ;
		
		
		return cad;
	}
}
