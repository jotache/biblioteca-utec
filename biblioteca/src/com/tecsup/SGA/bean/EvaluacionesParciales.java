package com.tecsup.SGA.bean;

public class EvaluacionesParciales {
private String codigo;
private String nombre;
private String fechaEval;
private String nroEval;
private String codCurso;
private String codTipoMotivo;
private String codTipoExamenParcial;
private String nota;
private String flgSancion;
private String codSeccion;
private String idDef;
private String flgRealizado;
private String estadoCursoAlumno;

public String getCodigo() {
	return codigo;
}
public void setCodigo(String codigo) {
	this.codigo = codigo;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getFechaEval() {
	return fechaEval;
}
public void setFechaEval(String fechaEval) {
	this.fechaEval = fechaEval;
}
public String getNroEval() {
	return nroEval;
}
public void setNroEval(String nroEval) {
	this.nroEval = nroEval;
}
public String getCodCurso() {
	return codCurso;
}
public void setCodCurso(String codCurso) {
	this.codCurso = codCurso;
}
public String getCodTipoMotivo() {
	return codTipoMotivo;
}
public void setCodTipoMotivo(String codTipoMotivo) {
	this.codTipoMotivo = codTipoMotivo;
}
public String getCodTipoExamenParcial() {
	return codTipoExamenParcial;
}
public void setCodTipoExamenParcial(String codTipoExamenParcial) {
	this.codTipoExamenParcial = codTipoExamenParcial;
}
public String getNota() {
	return nota;
}
public void setNota(String nota) {
	this.nota = nota;
}
public String getFlgSancion() {
	return flgSancion;
}
public void setFlgSancion(String flgSancion) {
	this.flgSancion = flgSancion;
}
public String getCodSeccion() {
	return codSeccion;
}
public void setCodSeccion(String codSeccion) {
	this.codSeccion = codSeccion;
}
public String getIdDef() {
	return idDef;
}
public void setIdDef(String idDef) {
	this.idDef = idDef;
}
public String getFlgRealizado() {
	return flgRealizado;
}
public void setFlgRealizado(String flgRealizado) {
	this.flgRealizado = flgRealizado;
}
public String getEstadoCursoAlumno() {
	return estadoCursoAlumno;
}
public void setEstadoCursoAlumno(String estadoCursoAlumno) {
	this.estadoCursoAlumno = estadoCursoAlumno;
}


}
