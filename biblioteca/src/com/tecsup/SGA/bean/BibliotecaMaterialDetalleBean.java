package com.tecsup.SGA.bean;

public class BibliotecaMaterialDetalleBean {

	private String codUnico;
	private String codTipoMaterial;
	private String titulo;
	private String codAutor;
	private String desAutor;
	private String codDewey;
	private String desDewey;
	private String nroPaginas;
	private String nroVolumen;
	private String nroTotalVolumenes;
	private String codEditorial;
	private String nroEditorial;
	private String codPais;
	private String codCiudad;
	private String fecPublicacion;
	private String imagen;
	private String urlArchDigital;
	private String codIdioma;
	private String formato;	
	private String codTipoVideo;
	private String indPrestSala;
	private String indPrestDomicilio;
	private String indReserva;
	private String isbn;
	private String ubicacionFisica;
	private String codGenerado;
	private String desEditorial;
	private String desIdioma;
	private String desPais;
	private String desCiudad;
	private String codSede;
	
	private String prefijo;
	private String sufijo;
	private String frecuencia;
	private String codGeneradoDewey;
	private String mencionSerie;
	private String contenido;


	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public String getCodSede() {
		return codSede;
	}

	public void setCodSede(String codSede) {
		this.codSede = codSede;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getCodTipoVideo() {
		return codTipoVideo;
	}

	public void setCodTipoVideo(String codTipoVideo) {
		this.codTipoVideo = codTipoVideo;
	}

	public String getIndPrestSala() {
		return indPrestSala;
	}

	public void setIndPrestSala(String indPrestSala) {
		this.indPrestSala = indPrestSala;
	}

	public String getIndPrestDomicilio() {
		return indPrestDomicilio;
	}

	public void setIndPrestDomicilio(String indPrestDomicilio) {
		this.indPrestDomicilio = indPrestDomicilio;
	}

	public String getIndReserva() {
		return indReserva;
	}

	public void setIndReserva(String indReserva) {
		this.indReserva = indReserva;
	}

	public String getCodUnico() {
		return codUnico;
	}

	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}

	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}

	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCodAutor() {
		return codAutor;
	}

	public void setCodAutor(String codAutor) {
		this.codAutor = codAutor;
	}

	public String getDesAutor() {
		return desAutor;
	}

	public void setDesAutor(String desAutor) {
		this.desAutor = desAutor;
	}

	public String getCodDewey() {
		return codDewey;
	}

	public void setCodDewey(String codDewey) {
		this.codDewey = codDewey;
	}

	public String getDesDewey() {
		return desDewey;
	}

	public void setDesDewey(String desDewey) {
		this.desDewey = desDewey;
	}

	public String getNroPaginas() {
		return nroPaginas;
	}

	public void setNroPaginas(String nroPaginas) {
		this.nroPaginas = nroPaginas;
	}

	public String getNroVolumen() {
		return nroVolumen;
	}

	public void setNroVolumen(String nroVolumen) {
		this.nroVolumen = nroVolumen;
	}

	public String getNroTotalVolumenes() {
		return nroTotalVolumenes;
	}

	public void setNroTotalVolumenes(String nroTotalVolumenes) {
		this.nroTotalVolumenes = nroTotalVolumenes;
	}

	public String getCodEditorial() {
		return codEditorial;
	}

	public void setCodEditorial(String codEditorial) {
		this.codEditorial = codEditorial;
	}

	public String getNroEditorial() {
		return nroEditorial;
	}

	public void setNroEditorial(String nroEditorial) {
		this.nroEditorial = nroEditorial;
	}

	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getCodCiudad() {
		return codCiudad;
	}

	public void setCodCiudad(String codCiudad) {
		this.codCiudad = codCiudad;
	}

	public String getFecPublicacion() {
		return fecPublicacion;
	}

	public void setFecPublicacion(String fecPublicacion) {
		this.fecPublicacion = fecPublicacion;
	}

	public String getUrlArchDigital() {
		return urlArchDigital;
	}

	public void setUrlArchDigital(String urlArchDigital) {
		this.urlArchDigital = urlArchDigital;
	}

	public String getCodIdioma() {
		return codIdioma;
	}

	public void setCodIdioma(String codIdioma) {
		this.codIdioma = codIdioma;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getUbicacionFisica() {
		return ubicacionFisica;
	}

	public void setUbicacionFisica(String ubicacionFisica) {
		this.ubicacionFisica = ubicacionFisica;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getCodGenerado() {
		return codGenerado;
	}

	public void setCodGenerado(String codGenerado) {
		this.codGenerado = codGenerado;
	}

	public String getDesEditorial() {
		return desEditorial;
	}

	public void setDesEditorial(String desEditorial) {
		this.desEditorial = desEditorial;
	}

	public String getDesIdioma() {
		return desIdioma;
	}

	public void setDesIdioma(String desIdioma) {
		this.desIdioma = desIdioma;
	}

	public String getDesPais() {
		return desPais;
	}

	public void setDesPais(String desPais) {
		this.desPais = desPais;
	}

	public String getDesCiudad() {
		return desCiudad;
	}

	public void setDesCiudad(String desCiudad) {
		this.desCiudad = desCiudad;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	public String getSufijo() {
		return sufijo;
	}

	public void setSufijo(String sufijo) {
		this.sufijo = sufijo;
	}

	public String getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getCodGeneradoDewey() {
		return codGeneradoDewey;
	}

	public void setCodGeneradoDewey(String codGeneradoDewey) {
		this.codGeneradoDewey = codGeneradoDewey;
	}

	public String getMencionSerie() {
		return mencionSerie;
	}

	public void setMencionSerie(String mencionSerie) {
		this.mencionSerie = mencionSerie;
	}

}

