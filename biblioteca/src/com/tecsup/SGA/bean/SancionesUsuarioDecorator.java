package com.tecsup.SGA.bean;

import org.displaytag.decorator.TableDecorator;

import com.tecsup.SGA.modelo.Sancion;

public class SancionesUsuarioDecorator extends TableDecorator {
	private Sancion lObject;
	
	public String getRbtSelSancion(){
		lObject = (Sancion) getCurrentRowObject();
		String str ="<input type=\"radio\" name=\"valorSancion1\" value=\"" + lObject.getValorSancion1()
					+ "\"  onclick=\"javascript:fc_seleccionarRegistro('" + lObject.getValorSancion1() + "','" + lObject.getCodFila() + "');\" >" ;
        return str;
	}
	
	public String addRowClass()
	{		
		return "texto";
	}
}
