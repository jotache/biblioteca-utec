package com.tecsup.SGA.bean;

public class MaterialxIngresoBean { 

	
	private String codUnico;
	private String codMaterialBib;
	private String codTipoMaterial;
	private String desTipoMaterial;
	private String titulo;
	private String nroIngreso;
	private String codUsuario;
	private String nombreUsuario;
	private String tipoDeUsuario;
	private String fecPrestamo;
	private String fecDevolucionProg;
	private String fecDevolucion;
	private String flgSancion;
	private String observacion;
	private String flgPrestado;
	private String flgPrestado1;
	private String flgPrestado2;
	private String flgPrestado3;
	private String flgPrestado4;
	private String flgPrestado5;
	
	private String indProrroga;
	private String nroProrrogas;
	private String limiteProrrogas;
	
	private String flgReserva;
	private String flgReserva1;
	private String flgReserva2;
	private String flgReserva3;
	private String flgReserva4;
	private String flgReserva5;
	
	private String flgMatIndCasa; //para ver si el material puede ser prestado CASA
	private String flgMatIndCasa1;
	private String flgMatIndCasa2;
	private String flgMatIndCasa3;
	private String flgMatIndCasa4;
	private String flgMatIndCasa5;
	private String flgMatIndSala;
	private String flgMatIndSala1;
	private String flgMatIndSala2;
	private String flgMatIndSala3;
	private String flgMatIndSala4;
	private String flgMatIndSala5;
	
	private String flgLibroRetirado;
	private String flgLibroRetirado1;
	private String flgLibroRetirado2;
	private String flgLibroRetirado3;
	private String flgLibroRetirado4;
	private String flgLibroRetirado5;
	
	private String sede;
	private String sede1;
	private String sede2;
	private String sede3;
	private String sede4;
	private String sede5;
	private String usuarioReserva; //cod usuario reserva
	private String codReserva; //cod reserva
	private String codDewey;
	private String indTipoPrestamo;
	private String nomLoginUsuPrestamo;
	private String nroIngresoReal;
	private String estadoIngreso;
	private String codPrestamoMultiple;
	private String codPrestamo;
	
	public String getIndTipoPrestamo() {
		return indTipoPrestamo;
	}
	public void setIndTipoPrestamo(String indTipoPrestamo) {
		this.indTipoPrestamo = indTipoPrestamo;
	}
	public String getIndProrroga() {
		return indProrroga;
	}
	public void setIndProrroga(String indProrroga) {
		this.indProrroga = indProrroga;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getFecPrestamo() {
		return fecPrestamo;
	}
	public void setFecPrestamo(String fecPrestamo) {
		this.fecPrestamo = fecPrestamo;
	}
	public String getFecDevolucionProg() {
		return fecDevolucionProg;
	}
	public void setFecDevolucionProg(String fecDevolucionProg) {
		this.fecDevolucionProg = fecDevolucionProg;
	}
	public String getFecDevolucion() {
		return fecDevolucion;
	}
	public void setFecDevolucion(String fecDevolucion) {
		this.fecDevolucion = fecDevolucion;
	}
	public String getFlgSancion() {
		return flgSancion;
	}
	public void setFlgSancion(String flgSancion) {
		this.flgSancion = flgSancion;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getFlgPrestado() {
		return flgPrestado;
	}
	public void setFlgPrestado(String flgPrestado) {
		this.flgPrestado = flgPrestado;
	}
	public String getNroIngreso() {
		return nroIngreso;
	}
	public void setNroIngreso(String nroIngreso) {
		this.nroIngreso = nroIngreso;
	}
	public String getCodUnico() {
		return codUnico;
	}
	public void setCodUnico(String codUnico) {
		this.codUnico = codUnico;
	}
	public String getCodMaterialBib() {
		return codMaterialBib;
	}
	public void setCodMaterialBib(String codMaterialBib) {
		this.codMaterialBib = codMaterialBib;
	}
	public String getCodTipoMaterial() {
		return codTipoMaterial;
	}
	public void setCodTipoMaterial(String codTipoMaterial) {
		this.codTipoMaterial = codTipoMaterial;
	}
	public String getDesTipoMaterial() {
		return desTipoMaterial;
	}
	public void setDesTipoMaterial(String desTipoMaterial) {
		this.desTipoMaterial = desTipoMaterial;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getFlgReserva() {
		return flgReserva;
	}
	public void setFlgReserva(String flgReserva) {
		this.flgReserva = flgReserva;
	}
	public String getFlgMatIndCasa() {
		return flgMatIndCasa;
	}
	public void setFlgMatIndCasa(String flgMatIndCasa) {
		this.flgMatIndCasa = flgMatIndCasa;
	}
	public String getFlgMatIndSala() {
		return flgMatIndSala;
	}
	public void setFlgMatIndSala(String flgMatIndSala) {
		this.flgMatIndSala = flgMatIndSala;
	}
	public String getFlgLibroRetirado() {
		return flgLibroRetirado;
	}
	public void setFlgLibroRetirado(String flgLibroRetirado) {
		this.flgLibroRetirado = flgLibroRetirado;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getUsuarioReserva() {
		return usuarioReserva;
	}
	public void setUsuarioReserva(String usuarioReserva) {
		this.usuarioReserva = usuarioReserva;
	}
	public String getCodReserva() {
		return codReserva;
	}
	public void setCodReserva(String codReserva) {
		this.codReserva = codReserva;
	}
	public String getCodDewey() {
		return codDewey;
	}
	public void setCodDewey(String codDewey) {
		this.codDewey = codDewey;
	}
	public String getNomLoginUsuPrestamo() {
		return nomLoginUsuPrestamo;
	}
	public void setNomLoginUsuPrestamo(String nomLoginUsuPrestamo) {
		this.nomLoginUsuPrestamo = nomLoginUsuPrestamo;
	}
	public String getNroIngresoReal() {
		return nroIngresoReal;
	}
	public void setNroIngresoReal(String nroIngresoReal) {
		this.nroIngresoReal = nroIngresoReal;
	}
	public String getEstadoIngreso() {
		return estadoIngreso;
	}
	public void setEstadoIngreso(String estadoIngreso) {
		this.estadoIngreso = estadoIngreso;
	}
	public String getFlgLibroRetirado1() {
		return flgLibroRetirado1;
	}
	public void setFlgLibroRetirado1(String flgLibroRetirado1) {
		this.flgLibroRetirado1 = flgLibroRetirado1;
	}
	public String getFlgLibroRetirado2() {
		return flgLibroRetirado2;
	}
	public void setFlgLibroRetirado2(String flgLibroRetirado2) {
		this.flgLibroRetirado2 = flgLibroRetirado2;
	}
	public String getFlgLibroRetirado3() {
		return flgLibroRetirado3;
	}
	public void setFlgLibroRetirado3(String flgLibroRetirado3) {
		this.flgLibroRetirado3 = flgLibroRetirado3;
	}
	public String getFlgLibroRetirado4() {
		return flgLibroRetirado4;
	}
	public void setFlgLibroRetirado4(String flgLibroRetirado4) {
		this.flgLibroRetirado4 = flgLibroRetirado4;
	}
	public String getFlgLibroRetirado5() {
		return flgLibroRetirado5;
	}
	public void setFlgLibroRetirado5(String flgLibroRetirado5) {
		this.flgLibroRetirado5 = flgLibroRetirado5;
	}
	public String getFlgPrestado1() {
		return flgPrestado1;
	}
	public void setFlgPrestado1(String flgPrestado1) {
		this.flgPrestado1 = flgPrestado1;
	}
	public String getFlgPrestado2() {
		return flgPrestado2;
	}
	public void setFlgPrestado2(String flgPrestado2) {
		this.flgPrestado2 = flgPrestado2;
	}
	public String getFlgPrestado3() {
		return flgPrestado3;
	}
	public void setFlgPrestado3(String flgPrestado3) {
		this.flgPrestado3 = flgPrestado3;
	}
	public String getFlgPrestado4() {
		return flgPrestado4;
	}
	public void setFlgPrestado4(String flgPrestado4) {
		this.flgPrestado4 = flgPrestado4;
	}
	public String getFlgPrestado5() {
		return flgPrestado5;
	}
	public void setFlgPrestado5(String flgPrestado5) {
		this.flgPrestado5 = flgPrestado5;
	}
	public String getFlgReserva1() {
		return flgReserva1;
	}
	public void setFlgReserva1(String flgReserva1) {
		this.flgReserva1 = flgReserva1;
	}
	public String getFlgReserva2() {
		return flgReserva2;
	}
	public void setFlgReserva2(String flgReserva2) {
		this.flgReserva2 = flgReserva2;
	}
	public String getFlgReserva3() {
		return flgReserva3;
	}
	public void setFlgReserva3(String flgReserva3) {
		this.flgReserva3 = flgReserva3;
	}
	public String getFlgReserva4() {
		return flgReserva4;
	}
	public void setFlgReserva4(String flgReserva4) {
		this.flgReserva4 = flgReserva4;
	}
	public String getFlgReserva5() {
		return flgReserva5;
	}
	public void setFlgReserva5(String flgReserva5) {
		this.flgReserva5 = flgReserva5;
	}
	public String getSede1() {
		return sede1;
	}
	public void setSede1(String sede1) {
		this.sede1 = sede1;
	}
	public String getSede2() {
		return sede2;
	}
	public void setSede2(String sede2) {
		this.sede2 = sede2;
	}
	public String getSede3() {
		return sede3;
	}
	public void setSede3(String sede3) {
		this.sede3 = sede3;
	}
	public String getSede4() {
		return sede4;
	}
	public void setSede4(String sede4) {
		this.sede4 = sede4;
	}
	public String getSede5() {
		return sede5;
	}
	public void setSede5(String sede5) {
		this.sede5 = sede5;
	}
	public String getFlgMatIndCasa1() {
		return flgMatIndCasa1;
	}
	public void setFlgMatIndCasa1(String flgMatIndCasa1) {
		this.flgMatIndCasa1 = flgMatIndCasa1;
	}
	public String getFlgMatIndCasa2() {
		return flgMatIndCasa2;
	}
	public void setFlgMatIndCasa2(String flgMatIndCasa2) {
		this.flgMatIndCasa2 = flgMatIndCasa2;
	}
	public String getFlgMatIndCasa3() {
		return flgMatIndCasa3;
	}
	public void setFlgMatIndCasa3(String flgMatIndCasa3) {
		this.flgMatIndCasa3 = flgMatIndCasa3;
	}
	public String getFlgMatIndCasa4() {
		return flgMatIndCasa4;
	}
	public void setFlgMatIndCasa4(String flgMatIndCasa4) {
		this.flgMatIndCasa4 = flgMatIndCasa4;
	}
	public String getFlgMatIndCasa5() {
		return flgMatIndCasa5;
	}
	public void setFlgMatIndCasa5(String flgMatIndCasa5) {
		this.flgMatIndCasa5 = flgMatIndCasa5;
	}
	public String getFlgMatIndSala1() {
		return flgMatIndSala1;
	}
	public void setFlgMatIndSala1(String flgMatIndSala1) {
		this.flgMatIndSala1 = flgMatIndSala1;
	}
	public String getFlgMatIndSala2() {
		return flgMatIndSala2;
	}
	public void setFlgMatIndSala2(String flgMatIndSala2) {
		this.flgMatIndSala2 = flgMatIndSala2;
	}
	public String getFlgMatIndSala3() {
		return flgMatIndSala3;
	}
	public void setFlgMatIndSala3(String flgMatIndSala3) {
		this.flgMatIndSala3 = flgMatIndSala3;
	}
	public String getFlgMatIndSala4() {
		return flgMatIndSala4;
	}
	public void setFlgMatIndSala4(String flgMatIndSala4) {
		this.flgMatIndSala4 = flgMatIndSala4;
	}
	public String getFlgMatIndSala5() {
		return flgMatIndSala5;
	}
	public void setFlgMatIndSala5(String flgMatIndSala5) {
		this.flgMatIndSala5 = flgMatIndSala5;
	}
	public String getCodPrestamoMultiple() {
		return codPrestamoMultiple;
	}
	public void setCodPrestamoMultiple(String codPrestamoMultiple) {
		this.codPrestamoMultiple = codPrestamoMultiple;
	}
	public String getCodPrestamo() {
		return codPrestamo;
	}
	public void setCodPrestamo(String codPrestamo) {
		this.codPrestamo = codPrestamo;
	}
	public String getTipoDeUsuario() {
		return tipoDeUsuario;
	}
	public void setTipoDeUsuario(String tipoDeUsuario) {
		this.tipoDeUsuario = tipoDeUsuario;
	}
	public String getNroProrrogas() {
		return nroProrrogas;
	}
	public void setNroProrrogas(String nroProrrogas) {
		this.nroProrrogas = nroProrrogas;
	}
	public String getLimiteProrrogas() {
		return limiteProrrogas;
	}
	public void setLimiteProrrogas(String limiteProrrogas) {
		this.limiteProrrogas = limiteProrrogas;
	}

	
}
