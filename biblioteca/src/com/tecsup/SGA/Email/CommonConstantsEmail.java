package com.tecsup.SGA.Email;

public class CommonConstantsEmail {

	//JHPR: 2008-05-22 Comentado para producci�n
	//public static final String DIRECCION_RECLUTAMIENTO   = "http://opalomino:8080/SGA/loguePostulante.html";
	public static final String DIRECCION_RECLUTAMIENTO   = "http://www.tecsup.edu.pe/SGA/logueoPostulante.html";
	public static final String TECSUP_EMAIL  			 = "sistemas@tecsup.edu.pe";
	public static final String TECSUP_EMAIL_NOMBRE_REC	 = "Tecsup Reclutamiento";
	public static final String TECSUP_EMAIL_BIBLIOTECA   = "biblioteca@tecsup.edu.pe";
	public static final String TECSUP_EMAIL_ORIGEN  	 = "Sistema de Reclutamiento - Logeo Postulante.";
	public static final String TECSUP_ASUNTO_OLVIDO_PASS = "TECSUP � Olvid� Contrase�a.";
	public static final String TECSUP_ASUNTO_INSCRIPCION_POSTULANTE = "TECSUP � Registro Exitoso.";
	public static final String TECSUP_BIBLIOTECA_EMAIL_BUZON  = "CEDITEC - Atenci�n del Buz�n de Sugerencias.";
	//JHPR: 2008-05-22 Comentado para producci�n
	//public static final String DIRECCION_LOGISTICA       = "http://opalomino:8080/SGA/logeoLogistica.html";
	public static final String DIRECCION_LOGISTICA       = "http://www.tecsup.edu.pe/SGA/logeoLogistica.html";
	public static final String TECSUP_LOGISTICA_EMAIL_COTIZACION  = "TECSUP SGA - Registro de Cotizaci�n.";	
	//public static final String DESTINOS_RRHH[][]= { {"cvasquez@tecsup.edu.pe","CLAUDIA VASQUEZ"},{"larchimbaud@tecsup.edu.pe","LORENA ARCHIMBAUD"} }; 
//	public static final String DESTINOS_RRHH[][]= {
//													{"cvasquez@tecsup.edu.pe","CLAUDIA VASQUEZ"},
//													{"mtudela@tecsup.edu.pe","Miguel Tudela"} 
//												  };
	public static final String DESTINOS_RRHH[] = {"cvasquez@tecsup.edu.pe","mtudela@tecsup.edu.pe"};
	
}
