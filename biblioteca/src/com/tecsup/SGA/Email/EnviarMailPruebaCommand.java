package com.tecsup.SGA.Email;

public class EnviarMailPruebaCommand {

	private String operacion;
	private String destinoUsuario;
	private String origenUsuario;
	private String mensaje;
	private String destinoUsuarioCC;
	private String archivo;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getDestinoUsuarioCC() {
		return destinoUsuarioCC;
	}
	public void setDestinoUsuarioCC(String destinoUsuarioCC) {
		this.destinoUsuarioCC = destinoUsuarioCC;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getDestinoUsuario() {
		return destinoUsuario;
	}
	public void setDestinoUsuario(String destinoUsuario) {
		this.destinoUsuario = destinoUsuario;
	}
	public String getOrigenUsuario() {
		return origenUsuario;
	}
	public void setOrigenUsuario(String origenUsuario) {
		this.origenUsuario = origenUsuario;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
}
