package com.tecsup.SGA.Email;

import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import edu.utecsup.main.Sender;


public class EnviarMailPruebaFormController extends SimpleFormController{
	private static Log log = LogFactory.getLog(EnviarMailPruebaFormController.class);
	
	protected Object formBackingObject(HttpServletRequest request)
    throws ServletException {
    	log.info("formBackingObject:INI"); 
    	EnviarMailPruebaCommand command = new EnviarMailPruebaCommand();
    	log.info("formBackingObject:FIN");
        return command;
    }
	protected void initBinder(HttpServletRequest request,
            ServletRequestDataBinder binder) {
    	NumberFormat nf = NumberFormat.getNumberInstance();
    	binder.registerCustomEditor(Long.class,
	                  new CustomNumberEditor(Long.class, nf, true));	
    }
	public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
	throws Exception {
	return super.processFormSubmission(request, response, command, errors);
     }
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors)
    		throws Exception {
    	log.info("onSubmit:INI");
    	
    	EnviarMailPruebaCommand control =  (EnviarMailPruebaCommand) command;
    	    	
    	if(control.getOperacion().equals("ENVIAR"))
    	{ 
    	  log.info("En el enviar Correo: ");

    	  log.info(control.getDestinoUsuario().trim());
    	  log.info(control.getMensaje().trim());
    	  log.info(control.getOrigenUsuario().trim());
    	  log.info(control.getArchivo());
    	  
    	  Sender senderMail = new Sender();
//    	  senderMail.sendMail(false, control.getDestinoUsuario().trim(), "Jody", "Respuesta Inter�s", 
//    			  control.getMensaje().trim(), null, control.getArchivo());
    	      
    	  senderMail.sendMail(false, control.getDestinoUsuario().trim(), "Jody", null, null, "Respuesta Inter�s", 
    			  control.getMensaje().trim(), null, control.getArchivo());
    	}	
    	
    	log.info("onSubmit:FIN");		
	    return new ModelAndView("/enviarMailPrueba/enviarMailPrueba","control",control);		
    }

}
