package com.tecsup.SGA.Email;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import edu.utecsup.main.Sender;


public class ServletEnvioCorreo {
	private static Log log = LogFactory.getLog(ServletEnvioCorreo.class);
   	
	public ServletEnvioCorreo() {
    }
       
	  public static void EnviarCorreo(String correoDestino, String correoOrigen, String nombreDestino, String nombreOrigen,
			String asunto, String cuerpoMensaje)    
		throws Exception {
		        
//		SimpleEmail email = new SimpleEmail();
//		
//		email.setCharset("ISO-8859-1");
//		email.setHostName("127.0.0.1");
//		email.addTo(correoDestino, nombreDestino);
//		email.setFrom(correoOrigen, nombreOrigen);
//		email.setSubject(asunto);
//		email.setMsg(cuerpoMensaje);
//		email.send();
		  
		Sender senderMail = new Sender();		
		senderMail.sendMail(false, correoDestino, nombreDestino, null, null, asunto, cuerpoMensaje, nombreOrigen, null);
	    	
	}	
	
    public void enviarEmail(String asunto, String[] destino, String origen[][], String cuerpo) throws Exception
//    	throws UnsupportedEncodingException, MessagingException 
    {
//        Properties propiedades = new Properties();
//        propiedades.put("mail.smtp.host", "127.0.0.1"); // cargamos el servidor de mail
//        Session sesion = Session.getInstance(propiedades, null);
//        MimeMessage mensaje = new MimeMessage(sesion);
//        InternetAddress origenD[] = new InternetAddress[origen.length];
//        for(int i=0; i<origen.length; i++)
//            origenD[i] = new InternetAddress(origen[i][0],origen[i][1]);
//        InternetAddress destinoD[] = new InternetAddress[destino.length];
//        for(int i=0; i<destino.length; i++)
//            destinoD[i] = new InternetAddress(destino[i][0],destino[i][1]);
//       
//        mensaje.addFrom(origenD);
//        mensaje.addRecipients(Message.RecipientType.TO, destinoD);
//        mensaje.setSubject(asunto, "ISO-8859-1");
//        mensaje.setText(cuerpo, "ISO-8859-1");
//        Transport.send(mensaje);
    	
    	Sender senderMail = new Sender();    	
    	senderMail.sendMail(false, destino, null, null, null, asunto, cuerpo, CommonConstantsEmail.TECSUP_EMAIL_NOMBRE_REC, null);
    }
    
}
