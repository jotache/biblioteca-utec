package com.tecsup.SGA.Email;

public class ConstantesCuerpoMensajes {

	
	public String CuerpoOlvidoContrasena(String nombrePostulante, String correo, String pass){
		
	 String mensaje="Sr(a). " + nombrePostulante + ":" + "\n" + 
	 "\t"+ "La clave relacionada a la cuenta de correo "+ correo + " es "+ pass +  ". " +
	 "Para ingresar al sistema dar click en la siguiente direcci�n " + 
	  CommonConstantsEmail.DIRECCION_RECLUTAMIENTO + 
	  "\n" +  "\n" + "Saludos," +  "\n" + "TECSUP.";	
	
	 return mensaje;
	}
	
	public String RegistroPostulante(String nombrePostulante, String correo, String pass){
		
		 String mensaje="Sr(a). " + nombrePostulante + ":" + "\n" + 
		 "\t"+ "Usted ha sido registrado(a) exitosamente en el sistema de Reclutamiento de TECSUP," +
		 		" para poder ingresar ser� necesario proporcionar la siguiente informaci�n: " + 
		  "\n" +  "\n" +
		  "Usuario: " + correo + "\n" +
		  "Clave: " + pass + "\n" + "\n" +
		  "Para ingresar al sistema dar click en la siguiente direcci�n "+
		  CommonConstantsEmail.DIRECCION_RECLUTAMIENTO + "\n" + "\n" +
		  "Saludos," +  "\n" + "TECSUP.";	
		
		 return mensaje;
		}
	
	public String AtenderBuzon(String nombrePostulante, String correo, String mensajeEnviar){
		
		 String mensaje="Sr(a). " + nombrePostulante + ":" + "\n" + 
		 "\t"+ mensajeEnviar + 
		  "\n" +  "\n" +
		  "Usuario: " + correo + "\n" +
		  "Clave: " + mensajeEnviar + "\n" + "\n" +
		  "Para ingresar al sistema dar click en la siguiente direcci�n "+
		  CommonConstantsEmail.DIRECCION_RECLUTAMIENTO + "\n" + "\n" +
		  "Saludos," +  "\n" + "TECSUP.";	
		
		 return mensaje;
		}
	
}
