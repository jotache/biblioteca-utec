package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

//import com.tecsup.SGA.DAO.ReclutaDAO;
import com.tecsup.SGA.DAO.evaluaciones.MantenimientoDAO;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CiclosCat;
import com.tecsup.SGA.modelo.ParametrosEspeciales;
import com.tecsup.SGA.service.evaluaciones.MantenimientoManager;
import com.tecsup.SGA.web.evaluaciones.command.EvaluacionesParcialesCommand;
import com.tecsup.SGA.web.evaluaciones.command.ParametrosEspecialesCommand;
import com.tecsup.SGA.web.evaluaciones.controller.MtoParametrosEspecialesFormController;

public class MantenimientoManagerImpl implements MantenimientoManager {
	private static Log log = LogFactory.getLog(MantenimientoManagerImpl.class);
	
	MantenimientoDAO mantenimientoDAO;
	TransactionTemplate transactionTemplate; 
	
	/*SETTER*/
	public void setMantenimientoDAO(MantenimientoDAO mantenimientoDAO) {
		this.mantenimientoDAO = mantenimientoDAO;
	}
	
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	
	
	public String insertParametrosEspeciales(ParametrosEspecialesCommand control){
		
		String codPeriodo= control.getVarperiodo();
		String cadValoresForm= control.getTxtNroVisitas()+"$"+control.getTxtNroHoras2()+"$"+control.getTxtNroHoras3()+"$"+control.getTxtMinima()+"|";
		String cadValoresCat= ("".equalsIgnoreCase(control.getTknCiclos())?null:control.getTknCiclos());// "02|03";
		String cadValoresEscala= CommonConstants.PARAM_ESP_CAL_EXCELENTE+"$"+control.getTxtExelente()+"|"+
		CommonConstants.PARAM_ESP_CAL_BUENA+"$"+control.getTxtBuena()+"|"+
		CommonConstants.PARAM_ESP_CAL_MEDIA+"$"+control.getTxtMedia()+"|"+
		CommonConstants.PARAM_ESP_CAL_BAJA+"$"+control.getTxtBaja();
		String nroRegistrosCat= control.getNroRegistros();
		String codUsuario= control.getUsuario();
		/*log.info("VALORES PARA EL STORE insertParametrosEspeciales");log.info("codPeriodo>>"+codPeriodo+">>");
		log.info("cadValoresForm>>"+cadValoresForm+">>");log.info("cadValoresCat>>"+cadValoresCat+">>");
		log.info("cadValoresEscala>>"+cadValoresEscala+">>");
		log.info("nroRegistrosCat>>"+nroRegistrosCat+">>");log.info("codUsuario>>"+codUsuario+">>");
		*/
		return mantenimientoDAO.insertParametrosEspeciales(codPeriodo, cadValoresForm,
				cadValoresCat, cadValoresEscala, nroRegistrosCat, codUsuario);		
	}

	public List getLstCilos(String codPeriodo) {
		return mantenimientoDAO.getLstCilos(codPeriodo);
	}

	public List getAllParametrosEspeciales(String codPeriodo, String codTabla) {
		return mantenimientoDAO.getAllParametrosEspeciales(codPeriodo,codTabla);
	}

	public ParametrosEspecialesCommand setParametrosEspeciales(
			ParametrosEspecialesCommand command) {
		 int total = 0;
		List lstParametrosFormacion = mantenimientoDAO.getAllParametrosEspeciales(command.getVarperiodo(),"0001");
		total = total + lstParametrosFormacion.size();		
		//log.info("lstParametrosFormacion>>"+lstParametrosFormacion.size()+">>");		
		if(total==0)command.setOperacion("irRegistrar");
		else command.setOperacion("irActualizar");
		
		if(lstParametrosFormacion!=null && lstParametrosFormacion.size()>0)
		{	ParametrosEspeciales parametrosEspeciales = null;
			parametrosEspeciales = (ParametrosEspeciales)lstParametrosFormacion.get(0);
			command.setTxtNroVisitas(parametrosEspeciales.getValor1());
			command.setTxtNroHoras2(parametrosEspeciales.getValor2());
			command.setTxtNroHoras3(parametrosEspeciales.getValor3());
			command.setTxtMinima(parametrosEspeciales.getValor4());			
		}else{
			command.setTxtNroVisitas(null);
			command.setTxtNroHoras2(null);
			command.setTxtNroHoras3(null);
			command.setTxtMinima(null);			
		}
		
		//ESCALA
		List lstParametrosEscala = mantenimientoDAO.getAllParametrosEspeciales(command.getVarperiodo(),"0003");
		//log.info("lstParametrosEscala>>"+lstParametrosEscala.size()+">>");
		total = total + lstParametrosEscala.size();
		if(lstParametrosEscala !=null && lstParametrosEscala .size()>0)
		{
				ParametrosEspeciales parametrosEspeciales0 = null;				
				ParametrosEspeciales parametrosEspeciales1 = null;
				ParametrosEspeciales parametrosEspeciales2 = null;
				ParametrosEspeciales parametrosEspeciales3 = null;
				
				parametrosEspeciales0 = (ParametrosEspeciales)lstParametrosEscala.get(0);				
				llenar(parametrosEspeciales0.getValor2(),parametrosEspeciales0,command);				
				//command.setTxtExelente(parametrosEspeciales0.getValor1());
				//command.setHidExelente(parametrosEspeciales0.getId());
				
				parametrosEspeciales1 = (ParametrosEspeciales)lstParametrosEscala.get(1);
				llenar(parametrosEspeciales1.getValor2(),parametrosEspeciales1,command);				
				//command.setTxtBuena(parametrosEspeciales1.getValor1());
				//command.setHidBuena(parametrosEspeciales1.getId());
				
				parametrosEspeciales2 = (ParametrosEspeciales)lstParametrosEscala.get(2);
				llenar(parametrosEspeciales2.getValor2(),parametrosEspeciales2,command);
				//command.setTxtMedia(parametrosEspeciales2.getValor1());
				//command.setHidMedia(parametrosEspeciales2.getId());
				
				parametrosEspeciales3 = (ParametrosEspeciales)lstParametrosEscala.get(3);
				llenar(parametrosEspeciales3.getValor2(),parametrosEspeciales3,command);
				//command.setTxtBaja(parametrosEspeciales3.getValor1());
				//command.setHidBaja(parametrosEspeciales3.getId());
		}else{
			command.setTxtExelente(null);
			command.setHidExelente(null);
			command.setTxtBuena(null);
			command.setHidBuena(null);
			command.setTxtMedia(null);
			command.setHidMedia(null);	
			command.setTxtBaja(null);
			command.setHidBaja(null);			
		}
		
		//PARAMETROS CICLOS EMPRESAS
		List lstCiclosEmpresa = mantenimientoDAO.getAllParametrosEspeciales(command.getVarperiodo(),"0002");
		//log.info("lstParametrosEscala>>"+lstCiclosEmpresa.size()+">>");
		total = total + lstCiclosEmpresa.size();
		
		if(lstCiclosEmpresa !=null && lstCiclosEmpresa.size()>0)
		command.setLstCiclosSel(lstCiclosEmpresa);
		
		//CICLOS
		List lstCiclos  = this.getLstCilos(command.getVarperiodo());
		
		command.setLstCiclos(null);
		List lstResultado = null;	
		
		command.setLstCiclos(fcRestaLista(lstCiclos,lstCiclosEmpresa));		
		
		//log.info("TOTAL>"+total+">");
			
		return command;
	}

	private void llenar(String cod, ParametrosEspeciales parametrosEspeciales,ParametrosEspecialesCommand command) {
		
		int i = 0;
		
		if(cod.equalsIgnoreCase("0001"))
			i=1;
		if(cod.equalsIgnoreCase("0002"))
			i=2;		
		if(cod.equalsIgnoreCase("0003"))
			i=3;		
		if(cod.equalsIgnoreCase("0004"))
			i=4;
		
		
		switch(i){
			case 1:llenaExelente(parametrosEspeciales,command);break;
			case 2:llenaBuena(parametrosEspeciales,command);break;
			case 3:llenaMedia(parametrosEspeciales,command);break;
			case 4:llenaBaja(parametrosEspeciales,command);break;
		
		}
		
		
	}

	private void llenaExelente(ParametrosEspeciales parametrosEspeciales,ParametrosEspecialesCommand command) {
		command.setTxtExelente(parametrosEspeciales.getValor1());
		command.setHidExelente(parametrosEspeciales.getId());		
	}
	private void llenaBuena(ParametrosEspeciales parametrosEspeciales,ParametrosEspecialesCommand command) {
		command.setTxtBuena(parametrosEspeciales.getValor1());
		command.setHidBuena(parametrosEspeciales.getId());		
	}
	private void llenaMedia(ParametrosEspeciales parametrosEspeciales,ParametrosEspecialesCommand command) {
		command.setTxtMedia(parametrosEspeciales.getValor1());
		command.setHidMedia(parametrosEspeciales.getId());	
	}
	private void llenaBaja(ParametrosEspeciales parametrosEspeciales,ParametrosEspecialesCommand command) {
		command.setTxtBaja(parametrosEspeciales.getValor1());
		command.setHidBaja(parametrosEspeciales.getId());		
	}

	private List fcRestaLista(List lstCiclos, List lstCiclosEmpresa) {
		
		List lstResultado = null;
		
		if( lstCiclos!=null && lstCiclos.size()>0 )
		{	
			lstResultado = new ArrayList();
			
			CiclosCat obj = null;
			
			for(int i=0;i<lstCiclos.size();i++)
			{
				obj = (CiclosCat)lstCiclos.get(i);				
				ParametrosEspeciales obj2 = null;
				
				if(lstCiclosEmpresa !=null && lstCiclosEmpresa.size()>0)
				{
					boolean flgEncontrado = false;
					
					for(int j=0;j<lstCiclosEmpresa.size();j++)
					{
						obj2 = (ParametrosEspeciales)lstCiclosEmpresa.get(j);			
						
						if(obj.getCodCiclo().equalsIgnoreCase(obj2.getValor1()))
						flgEncontrado = true;					
						
					}
					
					if(!flgEncontrado)
						lstResultado.add(obj);
					
				}else
				{
					lstResultado = lstCiclos;
				}	

			}
			
		}
		
		return lstResultado;

	}

	public String updateParametrosEspeciales(ParametrosEspecialesCommand control){
		
		String codPeriodo= control.getVarperiodo();
		String cadValoresForm= control.getTxtNroVisitas()+"$"+control.getTxtNroHoras2()+"$"+control.getTxtNroHoras3()+"$"+control.getTxtMinima()+"|";
		String cadValoresCat= ("".equalsIgnoreCase(control.getTknCiclos())?null:control.getTknCiclos());// "02|03";
		//E_C_CADVALORESESCALA : cadena para escala = (codigo sec |valor ) = 0004$1|0005$5|0006$8|0007$10
		//String cadValoresEscala= "0005$"+control.getTxtExelente()+"|"+"0004$"+control.getTxtBuena()+"|"+"0007$"+control.getTxtMedia()+"|"+"0006$"+control.getTxtBaja();
		//String cadValoresEscala= "4$"+control.getTxtExelente()+"|"+"7$"+control.getTxtBuena()+"|"+"6$"+control.getTxtMedia()+"|"+"5$"+control.getTxtBaja();
		String cadValoresEscala= 
			control.getHidExelente()+"$"+control.getTxtExelente()	+"|"+
			control.getHidBuena()	+"$"+control.getTxtBuena()		+"|"+
			control.getHidMedia()	+"$"+control.getTxtMedia()		+"|"+
			control.getHidBaja()	+"$"+control.getTxtBaja();
		
		String nroRegistrosCat= control.getNroRegistros();
		String codUsuario= control.getUsuario();
		/*log.info("VALORES PARA EL STORE");
		log.info("codPeriodo>>"+codPeriodo+">>");
		log.info("cadValoresForm>>"+cadValoresForm+">>");
		log.info("cadValoresCat>>"+cadValoresCat+">>");
		log.info("cadValoresEscala>>"+cadValoresEscala+">>");
		log.info("nroRegistrosCat>>"+nroRegistrosCat+">>");
		log.info("codUsuario>>"+codUsuario+">>");*/
		
		return mantenimientoDAO.updateParametrosEspeciales(codPeriodo, cadValoresForm,
				cadValoresCat, cadValoresEscala, nroRegistrosCat, codUsuario);		

	}

	public List getTipoPractica() {
		return mantenimientoDAO.getTipoPractica();
	}	

	public Object insertEvaluacioneParciales(final EvaluacionesParcialesCommand control){		
		/*final String codCurso= control.getCodCurso();
		final String codSeccion= control.getCboSeccion();
		final String feEvaluacion = control.getTxtFecha();		
		final String codNroEval = control.getCboNroEvaluacion();	
		final String nroEvaluacion = control.getTxhNroEvaluacionInner();
		final String codTipoEvaluacion = control.getCboEvaluacionParcial();		
		final String cadCodAlumno= control.getHidToken();		
		final String nroRegistros= control.getHidTamanio();		
		final String codUsuario= control.getUsuario();
		
		mantenimientoDAO.insertEvaluacioneParciales(
							codCurso,codSeccion,feEvaluacion,codNroEval,nroEvaluacion,codTipoEvaluacion,cadCodAlumno,
							nroRegistros,codUsuario);
		*/				
		return transactionTemplate.execute (new TransactionCallback() {
			public Object doInTransaction(TransactionStatus ts) {
				
				try {									
					return mantenimientoDAO.insertEvaluacioneParciales(
							control.getCodCurso(),control.getCboSeccion(),control.getTxtFechaJotache(),control.getCboNroEvaluacion(),
							control.getTxhNroEvaluacionInner(),control.getCboEvaluacionParcial(),control.getHidToken(),
							control.getHidTamanio(),control.getUsuario());
										
				} catch (Exception e){
					ts.setRollbackOnly();
					log.error(e.getMessage());
				}
				return null;
				}				
			});
	}

	public Object updateEvaluacioneParciales(
			final EvaluacionesParcialesCommand control) {
		/*String codCurso= control.getCodCurso();
		String codSeccion= control.getCboSeccion();
		String feEvaluacion = control.getTxtFecha();
		String nroEvaluacion = control.getCboNroEvaluacion();
		String codTipoEvaluacion = control.getCboEvaluacionParcial();
		String cadCodAlumno= control.getHidToken();		
		String nroRegistros= control.getHidTamanio();		
		String codUsuario= control.getUsuario();
		String codtipoEval= control.getCboEvaluacionParcial();*/		
		return transactionTemplate.execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus ts) {
				try {
					return mantenimientoDAO.updateEvaluacioneParciales(control.getCodCurso(), control.getCboSeccion(), control.getTxtFechaJotache(),
							control.getCboNroEvaluacion(),control.getCboEvaluacionParcial(),control.getHidToken(), control.getHidTamanio(), control.getUsuario(), control.getCboEvaluacionParcial());		
					
				} catch (Exception e) {
					ts.setRollbackOnly();
					log.error(e.getMessage());
				}				
				return null;				
			}			
		});	
	}
	
	public List getTipoPracticaxCurso(String codPeriodo,String codCurso){
		return mantenimientoDAO.getTipoPracticaxCurso(codPeriodo,codCurso);
	}
	
	public List getAllCursosCat(String codProducto,String codCiclo){
		return mantenimientoDAO.getAllCursosCat(codProducto, codCiclo);
	}

	public List GetAllPeriodosBySede(String codSede){
		return mantenimientoDAO.GetAllPeriodosBySede(codSede);
	}
}
