package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.evaluaciones.ExclusionDAO;
import com.tecsup.SGA.DAO.evaluaciones.AdminCursosDAO;
import com.tecsup.SGA.bean.DetalleEvalParcial;
import com.tecsup.SGA.bean.DetalleEvalParcialCab;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.service.evaluaciones.GestionAdministradtivaManager;
import com.tecsup.SGA.web.evaluaciones.command.EvaluacionesParcialesCommand;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarExamenesCommand;
import com.tecsup.SGA.web.evaluaciones.controller.RegistrarExamenesFormController;
import com.tecsup.SGA.modelo.Examen;
import com.tecsup.SGA.modelo.TipoExamenes;

public class GestionAdministradtivaManagerImpl implements
		GestionAdministradtivaManager {
	private static Log log = LogFactory.getLog(GestionAdministradtivaManagerImpl.class);
	private ExclusionDAO exclusionDAO;
	private AdminCursosDAO adminCursosDAO;
	
	public void setExclusionDAO(ExclusionDAO exclusionDAO) {
		this.exclusionDAO = exclusionDAO;
	}

	public void setAdminCursosDAO(AdminCursosDAO adminCursosDAO) {
		this.adminCursosDAO = adminCursosDAO;
	}

	public List getAllExclusionesByCurso(String codCurso, String nombre,
			String apellido, String codSeccion, String codPeriodo) {
		try
		{
			return this.exclusionDAO.getAllExclusionesByCurso(codCurso, nombre, apellido, codSeccion, codPeriodo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertExclusiones(String codCurso, String cadDatos,
			String codUsuario, String codPeriodo, String codSeccion) {
		
		int numReg;
		try
		{
			StringTokenizer stDatos = new StringTokenizer(cadDatos, "|");
			numReg = stDatos.countTokens();
			
			return this.exclusionDAO.insertExclusiones(codCurso, cadDatos, Integer.toString(numReg)
					, codUsuario, codPeriodo, codSeccion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllTypesOfExams(String codPeriodo, String codCurso, String tipoExamen){
		try
		{
			//System.out.println("TOTAL NRO EXAMENES:"+this.adminCursosDAO.getAllTypesOfExams(codPeriodo, codCurso, tipoExamen).size());
			
			return this.adminCursosDAO.getAllTypesOfExams(codPeriodo, codCurso, tipoExamen);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//JHPR: 2/Abrl/08 Agregue parametro para filtrar solo curso Cargo en la Bandeja Principal del Administrador.
	public List getAllCursosByFiltros(String codPeriodo, String codProducto, String codEspecialidad, String codCiclo,
									String codEtapa, String codPrograma , String filtrarCargos){
		try
		{
			return this.adminCursosDAO.getAllCursosByFiltros(codPeriodo, codProducto, codEspecialidad, codCiclo,
															codEtapa, codPrograma,filtrarCargos);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllExamsByAlumno(String codPeriodo, String codCurso, String codSeccion, String nombre, String apellido,String tipoExamen) {
		try
		{
			//JHPR 31/3/2008 Para visualizar los alumnos con examenes de cargo.			
			return this.adminCursosDAO.getAllExamsByAlumno(codPeriodo, codCurso, codSeccion, nombre, apellido,tipoExamen);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public RegistrarExamenesCommand setConsulta(RegistrarExamenesCommand control){		
		
		String CNS_RECUPERACION = CommonConstants.EXAMEN_RECUPERACION;
		String CNS_SUBSANACION  = CommonConstants.EXAMEN_SUBSANACION;
		String CNS_PARCIAL  = CommonConstants.EXAMEN_PARCIAL;
		String CNS_FINAL  = CommonConstants.EXAMEN_FINAL;
		
		String codPeriodo = control.getCodPeriodo();
		String codCurso = control.getCodCurso();
		String codSeccion = control.getSeccion();
		String nombre = control.getNombre() == null ?"" : control.getNombre().trim();
		String apellido = control.getApellido() == null ?"" : control.getApellido().trim();
		//JHPR 31/3/2008 Para visualizar los alumnos con examenes de cargo.
		String tipoExamen = control.getModoExamen(); // Modo Examen: para tener la posibilidad de listar los examenes de cargo.
		
		//CCORDOVA - CANTIDAD DE TIPOS DE EXAMENES
		ArrayList<Examen> lstTiposExamenes = (ArrayList<Examen>)control.getListaTipoExams();
		
		//System.out.println("RegistrarExamenesCommand.lstTiposExamenes.size():"+ lstTiposExamenes.size());
		
		int cantTiposExamenes = lstTiposExamenes == null ? 0 : lstTiposExamenes.size();
		int contadorExamenes = 0;
		int cantNotasExistentes = 0;
		
		//Agregado parameto "tipoExamen".
		/*System.out.println("Gestion Administrativa:Modo Examen:"+tipoExamen);
		log.info("codPeriodo>>"+codPeriodo+">>");
		log.info("codCurso>>"+codCurso+">>");
		log.info("codSeccion>>"+codSeccion+">>");
		log.info("nombre>>"+nombre+">>");
		log.info("apellido>>"+apellido+">>");
		log.info("tipoExamen>>"+tipoExamen+">>");*/
		List A = this.adminCursosDAO.getAllExamsByAlumno(
				codPeriodo,
				codCurso,
				codSeccion,
				nombre,
				apellido,
				tipoExamen
				);
		
		log.info("A.size():" + A.size());
		
		if(A!=null && A.size()>0) {		

			Examen examen = null;
			//String tempCodAlu 	= ""	;
			//String temp2CodAlu 	= ""	;
			List lstResultado	= null	;
			List lst	= null	;
			
			lstResultado = new ArrayList();
			
			Examen obj2 = null;
			
			Examen obj1 = null;
			int i=0;
			
			while( i<A.size() )
			{	
				lst = new ArrayList();
				examen = (Examen) A.get(i);			
				
				String x = examen.getCodAlumno();
				String y = examen.getCodAlumno();
				//DetalleEvalParcial obj2 = null;
					
				obj1  = new Examen();
				
				obj1.setCodAlumno(examen.getCodAlumno());
				obj1.setCodCarnet(examen.getCodCarnet());
				obj1.setNombre(examen.getNombre());
				obj1.setNota(examen.getNota());
				
				obj1.setFlagNP(examen.getFlagNP());
				obj1.setFlagAN(examen.getFlagAN());
				//JHPR 
				obj1.setFlagSU(examen.getFlagSU());
				obj1.setFlagIndExamRecu(examen.getFlagIndExamRecu());
				obj1.setFlagIndExamSub(examen.getFlagIndExamSub());
				obj1.setFlagIndExamPar(examen.getFlagIndExamPar());
				
				obj1.setDesSeccion(examen.getDesSeccion());
				obj1.setCodSeccion(examen.getCodSeccion());
				obj1.setExamenNp(examen.getExamenNp());
				obj1.setTipoExamenNp(examen.getTipoExamenNp());
				
				obj1.setTieneNotaSubsa(examen.getTieneNotaSubsa());
				
				//System.out.println("examen.getCodAlumno():" + examen.getCodAlumno());
				
				contadorExamenes = 0;
				//CNS_RECUPERACION obtiene el codigo del examen
				//obj2.getCodigo()
				while( x.equalsIgnoreCase(y) && i<A.size() )
				{			
						
						log.info("Alumno: " + obj1.getCodAlumno());
						obj2 = new Examen();
						obj2 = (Examen)A.get(i);
						//System.out.println("	1er while obj2.getCodigo().trim():"+obj2.getCodigo().trim());
						if ( !obj2.getCodigo().trim().equals("")) // "CODTIPOEXAM"
						{
							//System.out.println("lstTiposExamenes.get(contadorExamenes).getCodigo():"+lstTiposExamenes.get(contadorExamenes).getCodigo());					
							if ( lstTiposExamenes.get(contadorExamenes).getCodigo().equals(obj2.getCodigo().trim()))
							{	
								if( CNS_RECUPERACION.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) ){								
									obj2.setFlagRC("1");
									obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_RECUPERACION);
								}
								
								if( CNS_SUBSANACION.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) ){
									obj2.setFlagPosSub("1");	
									obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_SUBSANACION);
								}
								
								//CCORDOVA - 20080502
								if( CNS_PARCIAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) ){
									obj2.setFlagPosPar("1"); 
									obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_1);
								}
								
								if ( CNS_FINAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) ){
									obj2.setFlagPosPar("1");
									obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_2);
								}
								//System.out.println("obj2.getTipoExamen:" + obj2.getTipoExamen());
								lst.add(obj2);
								//System.out.println("obj2 Agregado a lst");
							}
							else
							{	
								
								while( !lstTiposExamenes.get(contadorExamenes).getCodigo().equals(obj2.getCodigo().trim())
										&& contadorExamenes < cantTiposExamenes)
								{
									//System.out.println("		2 while contadorExamenes:"+  contadorExamenes);
									Examen objX = new Examen();//RX
									
									if( CNS_RECUPERACION.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) )
									{	objX.setFlagRC("1");
										objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_RECUPERACION);
									}
									
									if( CNS_SUBSANACION.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) )
									{	objX.setFlagPosSub("1");
									objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_SUBSANACION);
									}
									
									//CCORDOVA - 20080502
									if( CNS_PARCIAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) )
									{	
										objX.setFlagPosPar("1"); 
										objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_1);
									}
									
									if ( CNS_FINAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) ){
										objX.setFlagPosPar("1");
										objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_2);
									}
									
									lst.add(objX);
									contadorExamenes ++;
								}
							
								//CCORDOVA - 20080502
								if( CNS_RECUPERACION.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) )
								{
									obj2.setFlagRC("1");								
									obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_RECUPERACION);
								}//RX
								
								if( CNS_SUBSANACION.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) )
								{obj2.setFlagPosSub("1");
								obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_SUBSANACION);
								}
								
								if( CNS_PARCIAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo())  )
									{	obj2.setFlagPosPar("1");
										obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_1);
									}
								
								if ( CNS_FINAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) ){
									obj2.setFlagPosPar("1");
									obj2.setTipoExamen(CommonConstants.ALIAS_EXAMEN_2);
								}
								
								lst.add(obj2);
							}
						}
						else
						{
							//System.out.println("	1er while Other case");
							cantNotasExistentes = lst.size();
							if ( cantNotasExistentes < cantTiposExamenes )
							{
								
								//AQU� ENTRA CUANDO NO HAY NOTAS.
								
								for( int z = 0; z < cantTiposExamenes - cantNotasExistentes ; z++)
								{//falta el seteo
									
									Examen objX = new Examen();//RX
									
									/*System.out.println("_examen:"+lstTiposExamenes.get(0).getDescripcion()+"-"+lstTiposExamenes.get(0).getCodigo());
									System.out.println("_examen:"+lstTiposExamenes.get(1).getDescripcion()+"-"+lstTiposExamenes.get(0).getCodigo());
									*/
									//System.out.println("_examen:"+lstTiposExamenes.get(z).getDescripcion()+"-"+z+"-"+lstTiposExamenes.get(z).getAlias());
									
									//if (!(lstTiposExamenes.get(z).getCodigo().equals(CommonConstants.EXAMEN_SUBSANACION))) {
										//cambio de orden jhpr 2008-06-25
									
									if( CommonConstants.ALIAS_EXAMEN_SUBSANACION.equalsIgnoreCase(lstTiposExamenes.get(z).getAlias()) )
									//if( CNS_SUBSANACION.equalsIgnoreCase(lstTiposExamenes.get(z).getCodigo()) )
										{	objX.setFlagPosSub("1");	
											objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_SUBSANACION);
										}
										
										
										//if( CNS_RECUPERACION.equalsIgnoreCase(lstTiposExamenes.get(z).getCodigo()) )
									if( CommonConstants.ALIAS_EXAMEN_RECUPERACION.equalsIgnoreCase(lstTiposExamenes.get(z).getAlias()) )
										{	objX.setFlagRC("1");;
											objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_RECUPERACION);
										}
										
										//System.out.println("lstTiposExamenes.get(contadorExamenes).getCodigo():"+ z + "  "+lstTiposExamenes.get(contadorExamenes).getCodigo());
										
										//CCORDOVA - 20080502
										//if( CNS_PARCIAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) || CNS_FINAL.equalsIgnoreCase(lstTiposExamenes.get(contadorExamenes).getCodigo()) )
										if( CommonConstants.ALIAS_EXAMEN_1.equalsIgnoreCase(lstTiposExamenes.get(z).getAlias()) )
											{	objX.setFlagPosPar("1");
												objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_1);
											}
										
										if ( CommonConstants.ALIAS_EXAMEN_2.equalsIgnoreCase(lstTiposExamenes.get(z).getAlias()) ) {
											objX.setFlagPosPar("1");
											objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_2);
										}
										
									//}
									
									
									lst.add(objX);									
								}
							}
						}
						
						
						//System.out.println("i++ : " + i + " - A.size:" + A.size());
						
						i++;
						if( i<A.size() ){							
							y = ( (Examen)A.get(i) ).getCodAlumno();
							System.out.println("y:" + y);
							
						}
							
						contadorExamenes ++;
						
						//System.out.println("contadorExamenes:"+ contadorExamenes);
				}
				
				//System.out.println("Salio del While");
				
				cantNotasExistentes = lst.size();		
				
				//System.out.println("cantNotasExistentes:"+cantNotasExistentes);
				
				if ( cantNotasExistentes < cantTiposExamenes )
				{
					/*for( int z = 0; z < cantTiposExamenes - cantNotasExistentes ; z++)
					{
						lst.add(new Examen());
					}*/
					
					for( int z = cantNotasExistentes; z < cantTiposExamenes ; z++)
					{
						
						Examen objX = new Examen();//RX		

						if( CNS_FINAL.equalsIgnoreCase(lstTiposExamenes.get(z).getCodigo()) ){
							objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_2);
							//System.out.println("z:"+z + " Examen 2");
						}						
						
						if( CNS_PARCIAL.equalsIgnoreCase(lstTiposExamenes.get(z).getCodigo()) ){
							objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_1);
							//System.out.println("z:"+z + " Examen 1");
						}
						
						if( CNS_SUBSANACION.equalsIgnoreCase(lstTiposExamenes.get(z).getCodigo()) )
						{objX.setFlagPosSub("1");
						objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_SUBSANACION);
						//System.out.println("z:"+z + " Subsanacion");
						}
						
						if( CNS_RECUPERACION.equalsIgnoreCase(lstTiposExamenes.get(z).getCodigo()) )
						{objX.setFlagRC("1");
						objX.setTipoExamen(CommonConstants.ALIAS_EXAMEN_RECUPERACION);
						//System.out.println("z:"+z + " recuperacion");
						}											
						
						lst.add(objX);
					}
					
				}
				
				obj1.setResultado(lst);
				
				lstResultado.add(obj1);				
			
			}		
			
			//System.out.println("lstResultado.size():" + lstResultado.size());
			control.setListaNotas(lstResultado);
		}else
			control.setListaNotas(null);
					
		return control;
	}
	
	public String insertNotasExamenes(String codPeriodo, String codCurso, String codSeccion, String cadNotas, 
			String codUsuario){
		int numReg;
		try
		{
			StringTokenizer stNotas = new StringTokenizer(cadNotas, "|");
			numReg = stNotas.countTokens();
			
			return this.adminCursosDAO.insertNotasExamenes(codPeriodo, codCurso, codSeccion, cadNotas, 
															Integer.toString(numReg), codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//JHPR: 1/Abril/08 para ingresar notas de examenes de cargo.
	public String insertNotasExamenesCargo(String codPeriodo, String codCurso, String cadNotas, 
			String codUsuario,String tipoExamen){
		int numReg;
		try 
		{
			StringTokenizer stNotas = new StringTokenizer(cadNotas,"|");
			numReg = stNotas.countTokens();
			return this.adminCursosDAO.insertNotasExamenesCargo(codPeriodo, codCurso
					, cadNotas, Integer.toString(numReg), codUsuario, tipoExamen);
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	//JHPR: 2008-7-14
	public String insertNotasExamenesSubsanacion(String codPeriodo,
			String codCurso, String cadNotas, String codUsuario) {
		int numReg;
		try 
		{
			StringTokenizer stNotas = new StringTokenizer(cadNotas,"|");
			numReg = stNotas.countTokens();
			return this.adminCursosDAO.insertNotasExamenesSubsanacion(codPeriodo, codCurso
					, cadNotas, Integer.toString(numReg), codUsuario);
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 */
	public String insertNotasExamenesRecuperacion(String codPeriodo,
			String codCurso, String cadNotas, String codUsuario) {
		int numReg;
		try 
		{
			StringTokenizer stNotas = new StringTokenizer(cadNotas,"|");
			numReg = stNotas.countTokens();
			return this.adminCursosDAO.insertNotasExamenesRecuperacion(codPeriodo, codCurso
					, cadNotas, Integer.toString(numReg), codUsuario);
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String updateLevantarDI(String codCurso, String codTipoSesion,
			String codSeccion, String codAlumno, 
			String tipoIncidencia, String tipoOpe, String usuario) {
		
		try{ 
			return this.adminCursosDAO.updateLevantarDI(codCurso, codTipoSesion, codSeccion, codAlumno, tipoIncidencia, tipoOpe, usuario);
			
		}catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
}
