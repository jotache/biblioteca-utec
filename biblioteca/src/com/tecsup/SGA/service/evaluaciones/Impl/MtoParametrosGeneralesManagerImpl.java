package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.modelo.ParametrosGenerales;
import com.tecsup.SGA.service.*;
import com.tecsup.SGA.DAO.evaluaciones.*;
import com.tecsup.SGA.service.evaluaciones.*;

public class MtoParametrosGeneralesManagerImpl implements MtoParametrosGeneralesManager{
    private static Log log = LogFactory.getLog(MtoParametrosGeneralesManagerImpl.class);
	MtoParametrosGeneralesDAO mtoParametrosGeneralesDAO;
	MtoPeriodosDAO mtoPeriodosDAO;
	
	public List getAllParametrosGenerales(String codProducto, String codPeriodo )
	{
		try
		{   List we= mtoParametrosGeneralesDAO.getAllParametrosGenerales(codProducto, codPeriodo);
			//System.out.println("MtoParametrosGeneralesManagerImpl "+we.size());
			return we;//mtoParametrosGeneralesDAO.getAllParametrosGenerales(codProducto, codPeriodo);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}
	
	public List getProductos()
	{
		try
		{
			return mtoParametrosGeneralesDAO.getProductos();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}
	
	public String Insert_O_UpdateParametrosGenerales(ParametrosGenerales obj, String usuario){
	try
	{
		return this.mtoParametrosGeneralesDAO.Insert_O_UpdateParametrosGenerales(
		 obj.getCodProducto(), obj.getCodPeriodo(), obj.getCodTabla(), obj.getValor(), usuario);
	}
	catch(Exception ex){
		ex.printStackTrace();
		log.error(ex.getMessage());
	}
	
	return null;
	}

	public MtoParametrosGeneralesDAO getMtoParametrosGeneralesDAO() {
		return mtoParametrosGeneralesDAO;
	}

	public void setMtoParametrosGeneralesDAO(
			MtoParametrosGeneralesDAO mtoParametrosGeneralesDAO) {
		this.mtoParametrosGeneralesDAO = mtoParametrosGeneralesDAO;
	}
	public List getPeriodos()
	{
		try
		{
			return mtoPeriodosDAO.getPeriodos();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}

	public MtoPeriodosDAO getMtoPeriodosDAO() {
		return mtoPeriodosDAO;
	}

	public void setMtoPeriodosDAO(MtoPeriodosDAO mtoPeriodosDAO) {
		this.mtoPeriodosDAO = mtoPeriodosDAO;
	}

}
