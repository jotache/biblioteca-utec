package com.tecsup.SGA.service.evaluaciones.Impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.evaluaciones.AsistenciaDAO;
import com.tecsup.SGA.DAO.evaluaciones.CompetenciasDAO;
import com.tecsup.SGA.DAO.evaluaciones.IncidenciaDAO;
import com.tecsup.SGA.DAO.evaluaciones.OperDocenteDAO;
import com.tecsup.SGA.bean.DetalleEvalParcial;
import com.tecsup.SGA.bean.DetalleEvalParcialCab;
import com.tecsup.SGA.bean.EvaluacionesParciales;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.CompetenciasPerfil;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.DefNroEvalParciales;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.FormacionEmpresa;
import com.tecsup.SGA.modelo.HorasXalumnos;
import com.tecsup.SGA.modelo.NumeroMinimo;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.service.evaluaciones.OperDocenteManager;
import com.tecsup.SGA.web.evaluaciones.command.EvaluacionesParcialesCommand;


public class OperDocenteManagerImpl implements OperDocenteManager {
	private static Log log = LogFactory
	.getLog(OperDocenteManagerImpl.class);
	
	OperDocenteDAO operDocenteDAO;
	IncidenciaDAO incidenciaDAO;
	CompetenciasDAO competenciasDAO;
	AsistenciaDAO asistenciaDAO;
	
	/*SETTER*/
	public void setCompetenciasDAO(CompetenciasDAO competenciasDAO) {
		this.competenciasDAO = competenciasDAO;
	}
	public void setIncidenciaDAO(IncidenciaDAO incidenciaDAO) {
		this.incidenciaDAO = incidenciaDAO;
	}
	public void setOperDocenteDAO(OperDocenteDAO operDocenteDAO) {
		this.operDocenteDAO = operDocenteDAO;
	}	
	public void setAsistenciaDAO(AsistenciaDAO asistenciaDAO) {
		this.asistenciaDAO = asistenciaDAO;
	}
	
	public List<CursoEvaluador> getCursoInTime(String evaluador){
		try {
			return operDocenteDAO.getCursoInTime(evaluador);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllCursos(String periodo, String evaluador, String producto, String especialidad, String curso,
			String codEvalSeleccionado, String dscCurso){
		try {
			return operDocenteDAO.getAllCursos(periodo, evaluador, producto, especialidad, curso
						, codEvalSeleccionado, dscCurso);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllSeccionCurso(String tipoSesion, String codCurso){
		try {
			return operDocenteDAO.getAllSeccionCurso(tipoSesion, codCurso);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
		
	public List getAllIncidenciasByCurso(String codPeriodo, String codCurso, String codProducto
    		, String codEspecialidad, String codTipoSesion, String codSeccion
    		, String tipoOpe, String nombre, String apellido ) {
		try {
			return incidenciaDAO.getAllIncidenciasByCurso(codPeriodo, codCurso, codProducto
		    		, codEspecialidad, codTipoSesion, codSeccion, tipoOpe, nombre, apellido);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllTipoEvaluaciones()
	{
		try{
			return operDocenteDAO.GetAllTipoEvaluaciones();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllEvaluaciones(Parciales obj)
	{
		try
		{
			return operDocenteDAO.GetAllEvaluaciones(obj.getCodPeriodo(),obj.getCodPro(),obj.getCodEspe(), obj.getCodCurso(), obj.getSeccion(), obj.getCodEvaluador(), obj.getCodTipoEvaluacion());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertPeso(Parciales obj, String usuario)
	{
		try
		{
			return operDocenteDAO.InsertPeso(obj.getCodPeriodo(), obj.getCodId()
					, obj.getCodCurso(),obj.getCodPro(),obj.getCodEspe(), obj.getSeccion(), obj.getCodTipoEvaluacion(), obj.getPeso()
					, obj.getCodEvaluador(), obj.getFlag(), usuario);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllEvaluadores(CursoEvaluador obj,String codPeriodo)
	{
		try
		{
		return operDocenteDAO.GetAllEvaluadores(obj.getCodCurso(),codPeriodo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteEvaluaciones(Parciales obj,String numReg,String usuario)
	{
		try
		{
			return operDocenteDAO.DeleteEvaluaciones(obj.getCodPeriodo(),obj.getCodPro(),obj.getCodEspe(),obj.getCodCurso(),obj.getSeccion(), obj.getCodTipoEvaluacion(), obj.getCodEvaluador(), numReg, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllAlumnosCurso(String codPeriodo, String codCurso, 
									String codProducto, String codEspecialidad, String tipoSesion,
									String codSeccion, String nombre, String apellido){
		try {
			return operDocenteDAO.getAllAlumnosCurso(codPeriodo, codCurso, codProducto, codEspecialidad, 
									tipoSesion, codSeccion, nombre, apellido);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllIncidencias(String codPeriodo, String codAlumno, String codCurso, String tipoOpe
			, String codTipoSesion, String codSeccion) {
		try {
			return this.incidenciaDAO.getAllIncidencias(codPeriodo, codAlumno, codCurso, tipoOpe
					, codTipoSesion, codSeccion);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertIncidencia(String codIncidencia, String codCurso, String codTipoSesion, String codSeccion
    		, String codAlumno, String fecha, String fechaIni, String fechaFin
			, String dscIncidencia, String tipoIncidencia, String tipoOpe, String usuario) {
		try {
			return this.incidenciaDAO.insertIncidencia(codIncidencia, codCurso, codTipoSesion, codSeccion
		    		, codAlumno, fecha, fechaIni, fechaFin, dscIncidencia, tipoIncidencia, tipoOpe, usuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllNroEvaluacionesxParciales(String codPeriodo,String codSeccion,String codCurso,
			String codEvaluacionParcial) {
		try {
			return this.incidenciaDAO.getAllNroEvaluacionesxParciales(codPeriodo,codSeccion,codCurso,codEvaluacionParcial);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String deleteIncidencia(String codIncidencia, String codUsuario) {
		try {
			return this.incidenciaDAO.deleteIncidencia(codIncidencia, codUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<EvaluacionesParciales> getAllEvaluacionesParcialesxCurso(
			String codPeriodo,String codCurso,
				String codSeccion, String strNombre,
				String strApellido,
				String cboNroEvaluacion,
				String cboEvaluacionParcial,
				String codProducto,
				String codEspecialidad,
				String fecEvaluacion
				) {
			try {
				return this.incidenciaDAO.getAllEvaluacionesParcialesxCurso(codPeriodo,codCurso,
						 codSeccion,  strNombre,  strApellido,cboNroEvaluacion,cboEvaluacionParcial,
						 codProducto,codEspecialidad,fecEvaluacion);
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}

	public List getAllConpetenciasByCurso(String codPeriodo, String codCurso, String codSeccion,
			String nombre, String apellido, String tipoOpe, String codTipoSesion) {
		ArrayList<CompetenciasPerfil> listaAlumnos;
		ArrayList<CompetenciasPerfil> listaCompetencias;
		CompetenciasPerfil competenciasPerfil;
		NumberFormat formatter = new DecimalFormat("##.00");
    	double notaIni = 0; 
    	double notaFin = 0;
		try
		{
			listaAlumnos = (ArrayList<CompetenciasPerfil>)this.competenciasDAO.getAllCompetenciasByCurso(
						codPeriodo, codCurso, codSeccion, nombre, apellido, codTipoSesion);
			for (int i = 0 ; i < listaAlumnos.size() ; i++)
			{
				listaAlumnos.get(i).setListaCompetencias(this.competenciasDAO.getAllCompetenciasByCursoByAlumno(
							codPeriodo,	codCurso, codSeccion, listaAlumnos.get(i).getCodAlumno(), tipoOpe));

		    	notaIni = 0; 
		    	notaFin = 0;
		    	listaCompetencias = (ArrayList<CompetenciasPerfil>)listaAlumnos.get(i).getListaCompetencias();
		    	for (int j = 0 ; j < listaCompetencias.size(); j++)
		    	{
		    		listaCompetencias.get(j).setCodEvalIni("");
		    		listaCompetencias.get(j).setCodEvalFin("");
		    		
		    		notaIni = notaIni + Double.parseDouble(listaCompetencias.get(j).getNotaIni().trim().equals("") ? "0" : listaCompetencias.get(j).getNotaIni().trim());
		    		notaFin = notaFin + Double.parseDouble(listaCompetencias.get(j).getNotaFin().trim().equals("") ? "0" : listaCompetencias.get(j).getNotaFin().trim());
		    		
		    		if (!listaCompetencias.get(j).getNotaIni().trim().equals("")) 
		    			listaCompetencias.get(j).setNotaIni(
		    					formatter.format(
		    							Double.parseDouble(listaCompetencias.get(j).getNotaIni().trim())));
		    		if (!listaCompetencias.get(j).getNotaFin().trim().equals("")) 
		    			listaCompetencias.get(j).setNotaFin(
		    					formatter.format(
		    							Double.parseDouble(listaCompetencias.get(j).getNotaFin().trim())));
		    	}
		    	
		    	competenciasPerfil = new CompetenciasPerfil();
		    	competenciasPerfil.setNotaIni(notaIni > 0 ? Double.toString(notaIni) : "");
		    	competenciasPerfil.setNotaFin(notaFin > 0 ? Double.toString(notaFin) : "");
		    	competenciasPerfil.setCodEvalIni(notaIni > 0 ? formatter.format( notaIni / CommonConstants.PORCENTAJE_TOTAL * CommonConstants.NOTA_MAX_TECSUP ) : "" );//Double.toString(notaIni)
		    	competenciasPerfil.setCodEvalFin(notaFin > 0 ? formatter.format( notaFin / CommonConstants.PORCENTAJE_TOTAL * CommonConstants.NOTA_MAX_TECSUP ) : "" );//Double.toString(notaFin)
		    	
		    	listaCompetencias.add(competenciasPerfil);
		    	listaAlumnos.get(i).setListaCompetencias(listaCompetencias);
		    	
			}
			return (List)listaAlumnos;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<CompetenciasPerfil> getAllConpetenciasByAlumno(String codPeriodo, String codCurso, String codSeccion,
			String codAlumno, String tipoOpe) {
		try
		{
			return this.competenciasDAO.getAllCompetenciasByCursoByAlumno(codPeriodo, codCurso, codSeccion, codAlumno, tipoOpe);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertCompetenciasByAlumno(String codAlumno, String codCurso,
			String codPeriodo, String tipoOpe, String datosComp,
			String codUsuario, String codSeccion, String codTipoSesion) {
		int numReg;
		try
		{
			StringTokenizer stDatos = new StringTokenizer(datosComp, "|");
			numReg = stDatos.countTokens();
			return this.competenciasDAO.insertCompetenciasByAlumno(codAlumno, codCurso, codPeriodo
					, tipoOpe, Integer.toString(numReg), datosComp, codUsuario, codSeccion, codTipoSesion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getIdAsistencia(){
		try {
			return this.asistenciaDAO.getIdAsistencia();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertAsistencia(String codPeriodo, String codAsistencia, String codAlumno, String codCurso, String tipoSesion, 
			String codSeccion, String nroSesion, String fecha, String tipoAsist, String usuario, String flagProg){
		try {
			return this.asistenciaDAO.insertAsistencia(codPeriodo, codAsistencia, codAlumno, codCurso, tipoSesion,  
														codSeccion, nroSesion, fecha, tipoAsist, usuario, flagProg);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllFechaAsistenciaCurso(String codPeriodo, String codCurso, 
			String codProducto, String codEspecialidad, String tipoSesion, 
			String codSeccion) {
		try {
			return this.asistenciaDAO.getAllFechaAsistenciaCurso(codPeriodo, codCurso, 
					codProducto, codEspecialidad, tipoSesion, codSeccion);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllAlumnosXasistencia(String codPeriodo, String codAsistencia, String nombre, String apellido, String nroSesion, 
										String codCurso, 
										String codProducto, String codEspecialidad, String tipoSesion, String codSeccion) {
		try {
			return this.asistenciaDAO.getAllAlumnosXasistencia(codPeriodo, codAsistencia, nombre, apellido, nroSesion, codCurso, 
					codProducto, codEspecialidad, tipoSesion,
					codSeccion);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public NumeroMinimo GetAllNumeroMinimo(Curso obj)
	{
		try{
			return this.operDocenteDAO.GetAllNumeroMinimo(obj.getCodProducto(), obj.getCodPeriodo());
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;	
	}
	
	public List GelAllFormacionEmpresa(String codPeriodo, String codProducto
			,String codEspecialidad, String nombre, String apPaterno)
	{
		try{
			return this.operDocenteDAO.GelAllFormacionEmpresa(codPeriodo, codProducto, codEspecialidad, nombre, apPaterno);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertEmpresa(FormacionEmpresa obj,String usuario)
	{
		try
		{
			return this.operDocenteDAO.InsertEmpresa(obj.getCodPeriodo(), obj.getCodProducto()
							, obj.getCodEspecialidad(), obj.getCodCurso(), obj.getCodAlumno(), obj.getTipoPractica()
							, obj.getNombreEmpresa(),obj.getFechaIni(), obj.getFechaFin(), obj.getCantidaHoras(),
							obj.getCodEstado(),obj.getObservacion(),obj.getNota(),obj.getHoraIni()
							,obj.getHoraFin(),obj.getIndSabado(),obj.getIndDomingo(), usuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public String InsertEmpresaId(FormacionEmpresa obj, String usuario) {
		
		try
		{
			return this.operDocenteDAO.InsertEmpresaId(obj.getCodPeriodo(), obj.getCodProducto()
							, obj.getCodEspecialidad(), obj.getCodCurso(), obj.getCodAlumno(), obj.getTipoPractica()
							, obj.getNombreEmpresa(),obj.getFechaIni(), obj.getFechaFin(), obj.getCantidaHoras(),
							obj.getCodEstado(),obj.getObservacion(),obj.getNota(),obj.getHoraIni()
							,obj.getHoraFin(),obj.getIndSabado(),obj.getIndDomingo(), usuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}		
		return null;
	}	
	
	public EvaluacionesParcialesCommand setConsulta(
			EvaluacionesParcialesCommand control) {

		String codPeriodo = control.getCodPeriodo();
		String codCurso = control.getCodCurso();
		String codSeccion = control.getCboSeccion();
		String nombre = control.getTxtNombre();
		String apellido = control.getTxtApaterno();
		String codNroEval = control.getTxhNroEvaluacionInner();
		String codTipoEval = control.getCboEvaluacionParcial();
		String codProducto = control.getCodProducto();
		String codEspecialidad = control.getCodEspecialidad().equalsIgnoreCase("null")?null:control.getCodEspecialidad();
		
		List A = this.operDocenteDAO.GetAllDetalleEvalParcial(
				codPeriodo,
				codCurso,
				codSeccion,
				nombre,
				apellido,
				codNroEval,
				codTipoEval,
				codProducto,
				codEspecialidad
				);
		
		if(A!=null && A.size()>0)
		{
						
			DetalleEvalParcial detalleEvalParcial = null;
			String tempCodAlu 	= ""	;
			String temp2CodAlu 	= ""	;
			List lstResultado	= null	;
			List lst	= null	;
			
			lstResultado = new ArrayList();
			
			
			DetalleEvalParcial obj2 = null;
			
			DetalleEvalParcialCab obj1 = null;
			int i=0;
						
			while( i<A.size() )
			{	
				lst = new ArrayList();
				detalleEvalParcial = (DetalleEvalParcial) A.get(i);			
				
				String x = detalleEvalParcial.getCodAlumno();
				String y = detalleEvalParcial.getCodAlumno();
				//DetalleEvalParcial obj2 = null;
					
				obj1  = new DetalleEvalParcialCab();
				
				obj1.setCodigo(detalleEvalParcial.getCodAlumno());
				obj1.setNombreAlumno(detalleEvalParcial.getNombre());
					
				while( x.equalsIgnoreCase(y) && i<A.size() )
				{			
						obj2 = new DetalleEvalParcial();
						
						obj2 = (DetalleEvalParcial)A.get(i);
						lst.add(obj2);
						
						i++;
						if( i<A.size() )
							y = ( (DetalleEvalParcial)A.get(i) ).getCodAlumno();
												
				}
					
				obj1.setResultado(lst);
				lstResultado.add(obj1);				
			
			}
			
			control.setLstResultado(lstResultado);
		}else
			control.setLstResultado(null);
					
		return control;
	}
	
	public String AdjuntarArchivo(FormacionEmpresa obj, String usuario)
	{
		try
		{
		return this.operDocenteDAO.AdjuntarArchivo(obj.getCodProducto()
				, obj.getCodPeriodo(), obj.getCodAlumno(), obj.getCodCurso()
				, obj.getNombreNuevoArchivo(), obj.getNombreArchivo(), usuario);	
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	
	public HorasXalumnos HorasPorAlumno(FormacionEmpresa obj)
	{
		try{
			return this.operDocenteDAO.HorasPorAlumno(obj.getCodPeriodo()
					, obj.getCodEspecialidad(), obj.getCodAlumno(), obj.getTipoPractica());
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;	
	}
	
	public List EmpresaXAlumno(FormacionEmpresa obj)
	{
		try{
			return this.operDocenteDAO.EmpresaXAlumno(obj.getCodPeriodo()
					, obj.getCodEspecialidad(), obj.getCodAlumno(), obj.getTipoPractica());
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;	
	}
	
	
	public Producto ProductoxCurso(FormacionEmpresa obj)
	{
		try{
			return this.operDocenteDAO.ProductoxCurso(obj.getCodCurso());
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;	
	}
	
	public List GetAllPeriodo()
	{
		try{
			return operDocenteDAO.GetAllPeriodo();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	
	public String ModificarFormacionEmpresa(FormacionEmpresa obj,String usuario)
	{
		try{
			return operDocenteDAO.ModificarFormacionEmpresa(obj.getCodId(), obj.getCodPeriodo()
					, obj.getCodProducto(), obj.getCodEspecialidad(), obj.getCodCurso(), obj.getCodAlumno()
					, obj.getTipoPractica(), obj.getNombreEmpresa(), obj.getFechaIni(), obj.getFechaFin()
					, obj.getCantidaHoras(), obj.getCodEstado(),obj.getObservacion(),obj.getNota()
					,obj.getHoraIni(),obj.getHoraFin(),obj.getIndSabado(),obj.getIndDomingo(), usuario);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllAdjuntoEmpresa(FormacionEmpresa obj)
	{
		try{
			return operDocenteDAO.GetAllAdjuntoEmpresa(obj.getCodPeriodo()
					, obj.getCodProducto(), obj.getCodAlumno(), obj.getCodCurso());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	
	
	public List getAllSesionXCurso(String codCurso){
		try{
			return operDocenteDAO.getAllSesionXCurso(codCurso);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List getAllTipoPracticaxCursoxEval(String codPeriodo,
			String codCurso, String codEval, String codProducto,
			String codEspecialidad) {
		try{
			return operDocenteDAO.getAllTipoPracticaxCursoxEval( codPeriodo,
					 codCurso,  codEval,  codProducto,
					 codEspecialidad);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List getAllSeccionesxCursoxEval(String codPeriodo, String codCurso,
			String codEval,String codProducto,String codEspecialidad, String cboEvaluacionParcial) {
		try{
			return operDocenteDAO.getAllSeccionesxCursoxEval( codPeriodo,  codCurso,
					 codEval,codProducto , codEspecialidad,  cboEvaluacionParcial);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List getAllNroEvaxCursoxEva(String codPeriodo, String codCurso,
			String codEval, String codProducto, String codEspecialidad,
			String cboEvaluacionParcial, String cboSeccion) {
		try{
			return operDocenteDAO.getAllNroEvaxCursoxEva( codPeriodo,  codCurso,
					 codEval,  codProducto,  codEspecialidad,
					 cboEvaluacionParcial,  cboSeccion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	
	}


	// CACT : 15-04-2008 Listado de Evaluadores para los jefes de Departamento
	public List<Evaluador> getEvaluadoresByJefeDep(String codEvaluador,
			String codSede, String codPeriodo, String tipoUsuario) {
		try{
			return operDocenteDAO.getEvaluadoresByJefeDep(codEvaluador, codSede, codPeriodo, tipoUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	// CACT : 17-04-2008 Lista de Secciones por responsable de tomar asistencia
	public List<CursoEvaluador> GetAllSeccionesxCursoxResp(String codResponsable, String codCurso
			, String codTipoSesion) {
		try{
			return operDocenteDAO.GetAllSeccionesxCursoxResp(codResponsable, codCurso, codTipoSesion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String deleteAsistencia(String codCurso, String tipoSesion, String codSeccion, String nroSesion, String fecha){
		try{
			return asistenciaDAO.deleteAsistencia(codCurso, tipoSesion, codSeccion, nroSesion, fecha);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	//JHPR: 2008-06-04 Cerrar registro de notas.
	public String cerrarRegistroNotas(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario, String flagCerrado) {
		
		try {
			return operDocenteDAO.cerrarRegistroNotas(codCurso, codSeccion, tipoEvaluacion, codUsuario, flagCerrado);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	//JHPR: 2008-06-04
	public Parciales obtenerDefinicionParcial(String codigo) {
		Parciales parcial = null;
		parcial = operDocenteDAO.obtenerDefinicionParcial(codigo);
		return parcial;
	}
	
	//JHPR: 2008-06-05
	public List<Parciales> obtenerDefinicionesParciales(String codCurso,
			String codSeccion, String TipoEvaluacion,String NroEvaluacion, String Estado) {
		List<Parciales> lista = operDocenteDAO.obtenerDefinicionesParciales(codCurso, codSeccion, TipoEvaluacion,NroEvaluacion, Estado);
		return lista;
	}
	public List<DefNroEvalParciales> consultarCierreNotas(String sede,
			String codPeriodo, String codProducto, String codEspecialida) {
		
		List<DefNroEvalParciales> lista = operDocenteDAO.consultarCierreNotas(sede, codPeriodo, codProducto, codEspecialida);
		//System.out.println("consultarCierreNotas::lista.size():"+lista.size());
		return lista;
	}
	
	public List<Evaluador> obtenerEvaluadorSeccion(String codCursoEjec,
			String codSeccion) {
		List<Evaluador> lista = operDocenteDAO.obtenerEvaluadorSeccion(codCursoEjec, codSeccion);
		return lista;
	}
	public String abrirRegistroNotas(String codCurso, String codSeccion,String tipoEvaluacion, String codUsuario) {
		try {
			return operDocenteDAO.abrirRegistroNotas(codCurso, codSeccion, tipoEvaluacion, codUsuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	public String cerrarRegistroAsistencia(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario, String flagCerrado) {
		try {
			return operDocenteDAO.cerrarRegistroAsistencia(codCurso, codSeccion, tipoEvaluacion, codUsuario, flagCerrado);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	public String abrirRegistroAsistencia(String codCurso, String codSeccion,
			String tipoEvaluacion, String codUsuario) {
		try {
			return operDocenteDAO.abrirRegistroAsistencia(codCurso, codSeccion, tipoEvaluacion, codUsuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<Alumno> listarDesaprobadosXDI(String curso,
			String seccion, String nombres, String apellidos) {
		try{
			return operDocenteDAO.listarDesaprobadosXDI(curso, seccion, nombres, apellidos);
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
