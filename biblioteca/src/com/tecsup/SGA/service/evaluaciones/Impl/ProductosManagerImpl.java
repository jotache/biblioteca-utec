package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.evaluaciones.CursosHibridoDAO;
import com.tecsup.SGA.DAO.evaluaciones.ProductosDAO;
import com.tecsup.SGA.service.evaluaciones.ProductosManager;
import com.tecsup.SGA.modelo.Producto;


public class ProductosManagerImpl implements ProductosManager{
	ProductosDAO productosDAO;	
	
	public List getAllProductos(){
		try
		{
			return productosDAO.getAllProductos();			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public ProductosDAO getProductosDAO() {
		return productosDAO;
	}

	public void setProductosDAO(ProductosDAO productosDAO) {
		this.productosDAO = productosDAO;
	}			
}
