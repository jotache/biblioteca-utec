package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.tecsup.SGA.DAO.evaluaciones.AlumnoDAO;
import com.tecsup.SGA.DAO.evaluaciones.NotaExternaDAO;
import com.tecsup.SGA.service.evaluaciones.AlumnoManager;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Curso;


public class AlumnoManagerImpl implements AlumnoManager{
	AlumnoDAO alumnoDAO;
	
	public List getAllAlumnoCasoEspecial(String codPeriodo,String codProducto,String codEspecialidad,String codEtapa,String codPrograma,String codCiclo,String codCurso,String nombreAlumno, String apePaternoAlumno, String apeMaternoAlumno){
		try{
			return this.alumnoDAO.getAllAlumnoCasoEspecial(codPeriodo, codProducto, codEspecialidad, codEtapa, codPrograma, codCiclo, codCurso, nombreAlumno,  apePaternoAlumno,  apeMaternoAlumno);						
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;		
		
	}
	
	public List getAllAlumnoCasoEspecialCat(String codPeriodo,String codProducto,String codCiclo,String codCurso){
		try{
			return this.alumnoDAO.getAllAlumnoCasoEspecialCat(codPeriodo, codProducto, codCiclo, codCurso);									
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public AlumnoDAO getAlumnoDAO() {
		return alumnoDAO;
	}

	public void setAlumnoDAO(AlumnoDAO alumnoDAO) {
		this.alumnoDAO = alumnoDAO;
	}

	//JHPR 2008-06-05 Para Ingreso masivo de registros de formación de empresas.
	public List<Alumno> crearListadeAlumnos(String alumnos) {
		Alumno alumno = null;
		List<Alumno> listaAlumnos = new ArrayList<Alumno>();		
						
		try {
			int j=0;
			String [] campos = alumnos.split(",");   
			while(j<campos.length){   
				
				alumno = this.alumnoDAO.obtenerAlumno(campos[j]);
				listaAlumnos.add(alumno);
				j++;   
			}  						
				
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listaAlumnos;
	}

	public int NroPasantiasAlumno(String codAlumno) {		
		return this.alumnoDAO.NroPasantiasAlumno(codAlumno);
	}

	@Override
	public Alumno obtenerAlumno(String codAlumno) {
		return this.alumnoDAO.obtenerAlumno(codAlumno);
	}

	@Override
	public String guardarRespuestaDatosPersonales(String codAlumno,
			String respuesta) {
		try{
			return this.alumnoDAO.guardarRespuestaDatosPersonales(codAlumno, respuesta);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return "0";
	}	
	
	@Override
	public boolean debeEncuestaPFR(Integer codalumno){
		return this.alumnoDAO.debeEncuestaPFR(codalumno);
	}

	@Override
	public List<Curso> listaCursosExternos(String codPeriodo) {
		List<Curso> lista = this.alumnoDAO.listaCursosExternos(codPeriodo);
		return lista;
	}	
	
}
