package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.evaluaciones.CursosHibridoDAO;
import com.tecsup.SGA.service.evaluaciones.CursosHibridoManager;
import com.tecsup.SGA.modelo.CursosHibrido;


public class CursosHibridoManagerImpl implements CursosHibridoManager{		
	CursosHibridoDAO cursosHibridoDAO;
	
	public List getAllCursosHibrido(){
		try
		{
			return cursosHibridoDAO.getAllCursosHibrido();			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public CursosHibridoDAO getCursosHibridoDAO() {
		return cursosHibridoDAO;
	}

	public void setCursosHibridoDAO(CursosHibridoDAO cursosHibridoDAO) {
		this.cursosHibridoDAO = cursosHibridoDAO;
	}

	
}
