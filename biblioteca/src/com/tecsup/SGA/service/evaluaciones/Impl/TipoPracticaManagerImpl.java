package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;
import com.tecsup.SGA.DAO.evaluaciones.TipoPracticasDAO;
import com.tecsup.SGA.service.evaluaciones.TipoPracticaManager;
import com.tecsup.SGA.modelo.Examen;

public class TipoPracticaManagerImpl implements TipoPracticaManager{
	TipoPracticasDAO tipoPracticasDAO;
	
	public List getAllTipoPracticas(){
		try
		{
			return tipoPracticasDAO.getAllTipoPracticas();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public TipoPracticasDAO getTipoPracticasDAO() {
		return tipoPracticasDAO;
	}

	public void setTipoPracticasDAO(TipoPracticasDAO tipoPracticasDAO) {
		this.tipoPracticasDAO = tipoPracticasDAO;
	}
	
}
