package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.evaluaciones.AlumnoDAO;
import com.tecsup.SGA.DAO.evaluaciones.NotaExternaDAO;
import com.tecsup.SGA.service.evaluaciones.NotaExternaManager;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.modelo.ConfiguracionNotaExterna;

public class NotaExternaManagerImpl implements NotaExternaManager {
	NotaExternaDAO notaExternaDAO;

	private static Log log = LogFactory.getLog(NotaExternaManagerImpl.class);
	
	public NotaExternaDAO getNotaExternaDAO() {
		return notaExternaDAO;
	}

	public void setNotaExternaDAO(NotaExternaDAO notaExternaDAO) {
		this.notaExternaDAO = notaExternaDAO;
	}

	public String InsertAdjuntoCasoCat(Archivo obj, String usuario) {
		try {
			return this.notaExternaDAO.InsertAdjuntoCasoCat(obj.getPeriodo(),
					obj.getProducto(), obj.getCiclo(), obj.getNombreNuevo(),
					obj.getNombre(), usuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public List getAdjuntoCasoCatById(String periodo, String producto,
			String ciclo) {
		try {
			return this.notaExternaDAO.getAdjuntoCasoCatById(periodo, producto,
					ciclo);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String UpdateNotaExterno(Alumno obj, String usuario) {
		try {
			return this.notaExternaDAO.UpdateNotaExterno(obj.getCodPeriodo(),
					obj.getCodProducto(), obj.getCodAlumno(), obj.getCodCurso(), 
					obj.getCodCiclo(), obj.getTipo(), obj.getNotaExterna(),
					obj.getCodEspecialidad(), obj.getCodEtapa(), obj.getCodPrograma(), usuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	public String InsertNotaExterno(Alumno obj, String usuario) {
		try {
			return this.notaExternaDAO.InsertNotaExterno(obj.getCodPeriodo(),
					obj.getCodProducto(), obj.getCodAlumno(), obj.getCodCurso(), 
					obj.getCodCiclo(), obj.getTipo(), obj.getNotaExterna(),
					obj.getCodEspecialidad(), obj.getCodEtapa(), obj.getCodPrograma(), usuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllCursosNotaExterna() {
		try {
			return this.notaExternaDAO.getAllCursosNotaExterna();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public ConfiguracionNotaExterna configuracionNotaExterna(String codCursoEjec) {
		
		//JHPR 2008-05-05 Calcular Nota Tecsup.
		ConfiguracionNotaExterna configNotaExterna = null;
		configNotaExterna = notaExternaDAO.configuracionNotaExterna(codCursoEjec);
		return configNotaExterna;
	}

	//JHPR 2008-05-05 Insertar/Actualizar Nota Tecsup.
	/*public String InsertNotaExterno(String codPeriodo, String codProducto,
			String codCiclo, String codCurso, String cadenaDatos,
			String tipoReg, String usuario,String nroRegistros) {
		try {
			return this.notaExternaDAO.InsertNotaExterno(codPeriodo, codProducto, codCiclo, codCurso, 
					cadenaDatos, tipoReg, usuario,nroRegistros);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return null;
	}*/
	public String InsertNotaExterno(String codPeriodo, String codProducto,
			String cadenaDatos,
			String tipoReg, String usuario,String nroRegistros) {
		try {
			return this.notaExternaDAO.InsertNotaExterno(codPeriodo, codProducto,  
					cadenaDatos, tipoReg, usuario,nroRegistros);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return null;
	}

	public List<ConfiguracionNotaExterna> listaConfiguracionNotaExterna(String codPeriodo) {
		return notaExternaDAO.listaConfiguracionNotaExterna(codPeriodo);
	}

}
