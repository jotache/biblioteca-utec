package com.tecsup.SGA.service.evaluaciones.Impl;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.evaluaciones.MtoConfiguracionComponentesDAO;
import com.tecsup.SGA.DAO.evaluaciones.MtoPeriodosDAO;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Especialidad;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.ProgramaComponentes;
import com.tecsup.SGA.modelo.Programas;
import com.tecsup.SGA.service.evaluaciones.MtoConfiguracionComponentesManager;

public class MtoConfiguracionComponentesManagerImpl implements MtoConfiguracionComponentesManager
{ 
	private static Log log = LogFactory.getLog(MtoConfiguracionComponentesManagerImpl.class);
	MtoConfiguracionComponentesDAO mtoConfiguracionComponentesDAO;
  MtoPeriodosDAO mtoPeriodosDAO;
   
	public List getComponentes(ProgramaComponentes obj)
	{  
		try
		{   return mtoConfiguracionComponentesDAO.getComponentes(obj.getCodProducto(), obj.getCodEspecialidad(), 
				obj.getCodCiclo(), obj.getCodCurso(), obj.getCodEtapa(), obj.getCodPrograma());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}
	public String InsertComponentes(ProgramaComponentes obj, String NROREGISTROS, String usuario)
	  {
		try
		{
			return this.mtoConfiguracionComponentesDAO.InsertComponentes(
			 obj.getCodProducto(), obj.getCodEspecialidad(), obj.getCodCiclo(), obj.getCodCurso(),obj.getCodEtapa(),
			 obj.getCodPrograma(), obj.getCodComponente(), NROREGISTROS ,usuario);
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		
		return null;
	 }
	public List getEspecialidadByProducto(Producto obj){
		try
		{   return mtoConfiguracionComponentesDAO.getEspecialidadByProducto(obj.getCodProducto());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}
	public List getProgramasByProducto(Producto obj, String codEtapa){
		try
		{   return mtoConfiguracionComponentesDAO.getProgramasByProducto(obj.getCodProducto(), codEtapa);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}
	public List getCursosByProducto(Producto obj, String codPeriodo){
		
		try
		{   return mtoConfiguracionComponentesDAO.getCursosByProducto(obj.getCodProducto(), codPeriodo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}
	
	public List getCicloByCursoAndProducto(Producto obj, Curso curso){
		
		try
		{return mtoConfiguracionComponentesDAO.getCicloByCursoAndProducto(obj.getCodProducto(), 
				curso.getCodCurso());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
		
	}
			
   public List getCicloByProductoEspecialidadPrograma(Producto producto, Especialidad especialidad, 
			Programas programa, String codPeriodo){
		
		try
		{return mtoConfiguracionComponentesDAO.getCicloByProductoEspecialidadPrograma(producto.getCodProducto(), 
				especialidad.getCodEspecialidad(), programa.getCodPrograma(), codPeriodo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
		
	}
	
	public List getCursoByProductoEspecialidadCiclo(Producto producto, Programas programas, 
			Especialidad especialidad, Curso curso, String codEtapa, String codPeriodo){
		/*System.out.println(">>CodProducto<<"+producto.getCodProducto()+">>CodPrograma<<"+programas.getCodPrograma()+">>CodEspecialidad<<"+
				especialidad.getCodEspecialidad()+">>CodCiclo<<"+curso.getCodCiclo()+">>codEtapa<<"+codEtapa);*/
		try
		{return mtoConfiguracionComponentesDAO.getCursoByProductoEspecialidadCiclo(producto.getCodProducto(), programas.getCodPrograma(),
				especialidad.getCodEspecialidad(), curso.getCodCiclo(), codEtapa, codPeriodo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
		
	}
	//*******nota externa napa
	public List getCicloByProductoEspecialidadPrograma2(String codProducto, String codEspecialidad, 
			String codPrograma, String codPeriodo){
		
		try{
			return mtoConfiguracionComponentesDAO.getCicloByProductoEspecialidadPrograma(codProducto,
					codEspecialidad,codPrograma, codPeriodo);
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
	}	
	public List getCursoByProductoEspecialidadCicloExt(Producto producto, Programas programas, 
			Especialidad especialidad, Curso curso, String codEtapa, String codPeriodo){
		
		try
		{return mtoConfiguracionComponentesDAO.getCursoByProductoEspecialidadCicloExt(producto.getCodProducto(), programas.getCodPrograma(),
				especialidad.getCodEspecialidad(), curso.getCodCiclo(), codEtapa, codPeriodo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return null;
		
	}
	
	
	public MtoConfiguracionComponentesDAO getMtoConfiguracionComponentesDAO() {
		return mtoConfiguracionComponentesDAO;
	}
	public void setMtoConfiguracionComponentesDAO(
			MtoConfiguracionComponentesDAO mtoConfiguracionComponentesDAO) {
		this.mtoConfiguracionComponentesDAO = mtoConfiguracionComponentesDAO;
	}
	public MtoPeriodosDAO getMtoPeriodosDAO() {
		return mtoPeriodosDAO;
	}
	public void setMtoPeriodosDAO(MtoPeriodosDAO mtoPeriodosDAO) {
		this.mtoPeriodosDAO = mtoPeriodosDAO;
	}
	
	//JHPR: 2008-05-20 Listar Cursos de Nota Externa...
	/*public List ListarCursosNotaExternaXProductoCiclo(String codProducto,
			String codEspecialidad, String codCiclo, String codPrograma,
			String codEtapa, String codPeriodo) {
		
		System.out.println(">>CodProducto<<"+codProducto+">>CodPrograma<<"+codPrograma+">>CodEspecialidad<<"+
				codEspecialidad+">>CodCiclo<<"+codCiclo+">>codEtapa<<"+codEtapa);
		try
		{return mtoConfiguracionComponentesDAO.ListarCursosNotaExternaXProductoCiclo(codProducto, codEspecialidad, codCiclo, codPrograma, codEtapa, codPeriodo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
		
	}*/
	
}
