package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.evaluaciones.CursoConfigDAO;
import com.tecsup.SGA.DAO.evaluaciones.SistevalHibridoDAO;
import com.tecsup.SGA.service.evaluaciones.CursoConfigManager;
import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.modelo.CursoConfig;
import com.tecsup.SGA.modelo.SistevalHibrido;

public class CursoConfigManagerImpl implements CursoConfigManager{	
	CursoConfigDAO cursoConfigDAO;
	
	public CursoConfigDAO getCursoConfigDAO() {
		return cursoConfigDAO;
	}
	public void setCursoConfigDAO(CursoConfigDAO cursoConfigDAO) {
		this.cursoConfigDAO = cursoConfigDAO;
	}
	public List getCursoConfigById(String codigo){
		try
		{
			return cursoConfigDAO.getCursoConfigById(codigo);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}	
	public String InsertCursoConfig(CursoConfig obj, String usuario){
		
		try
		{   return this.cursoConfigDAO.InsertCursoConfig(obj.getCodCurso(), obj.getRegla(), obj.getCondicion(), 
				obj.getVerdadero01(), obj.getVerdadero02(), obj.getVerdadero03(), obj.getVerdadero04(),
				obj.getFalso01(), obj.getFalso02(), obj.getFalso03(), obj.getFalso04(),
				obj.getOpeVer01(), obj.getOpeVer02(), obj.getOpeVer03(), obj.getOpeVer04(),
				obj.getOpeFal01(), obj.getOpeFal02(), obj.getOpeFal03(), obj.getOpeFal04(),
				usuario);						
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;	
	}
	public String UpdateCursoConfig(CursoConfig obj, String usuario){
		try
		{   return this.cursoConfigDAO.UpdateCursoConfig(obj.getCodCurso(), obj.getRegla(), obj.getCondicion(), 
				obj.getVerdadero01(), obj.getVerdadero02(), obj.getVerdadero03(), obj.getVerdadero04(),
				obj.getFalso01(), obj.getFalso02(), obj.getFalso03(), obj.getFalso04(),
				obj.getOpeVer01(), obj.getOpeVer02(), obj.getOpeVer03(), obj.getOpeVer04(),
				obj.getOpeFal01(), obj.getOpeFal02(), obj.getOpeFal03(), obj.getOpeFal04(),
				usuario);
						
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
		
	}		
	
}
