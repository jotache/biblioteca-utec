package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.evaluaciones.HibridoCursoDAO;
import com.tecsup.SGA.DAO.evaluaciones.SistevalHibridoDAO;
import com.tecsup.SGA.service.evaluaciones.HibridoCursoManager;
import com.tecsup.SGA.modelo.HibridoCurso;
import com.tecsup.SGA.modelo.SistevalHibrido;

public class HibridoCursoManagerImpl implements HibridoCursoManager{	
	HibridoCursoDAO hibridoCursoDAO;
	
	public HibridoCursoDAO getHibridoCursoDAO() {
		return hibridoCursoDAO;
	}
	
	public void setHibridoCursoDAO(HibridoCursoDAO hibridoCursoDAO) {
		this.hibridoCursoDAO = hibridoCursoDAO;
	}
	
	public List getHibridoCursoById(String codigo){
		try{
			return hibridoCursoDAO.getHibridoCursoById(codigo);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertHibridoCurso(HibridoCurso obj, String usuario){
		try
		{   return this.hibridoCursoDAO.InsertHibridoCurso(obj.getCodigo(), obj.getCadena(), obj.getNroRegistros(), usuario);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
		
	}
	
	public String UpdateHibridoCurso(HibridoCurso obj, String usuario){
		try
		{   return this.hibridoCursoDAO.UpdateHibridoCurso(obj.getCodigo(), obj.getCadena(), obj.getNroRegistros(), usuario);			
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}		
}
