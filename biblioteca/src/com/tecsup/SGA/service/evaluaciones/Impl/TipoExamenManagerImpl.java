package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;
import com.tecsup.SGA.DAO.evaluaciones.TipoExamenesDAO;
import com.tecsup.SGA.service.evaluaciones.TipoExamenManager;
import com.tecsup.SGA.modelo.Examen;

public class TipoExamenManagerImpl implements TipoExamenManager{
	TipoExamenesDAO tipoExamenesDAO;	
	
	public List getAllTipoExamenes(){
		try
		{
			return tipoExamenesDAO.getAllTipoExamenes();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public TipoExamenesDAO getTipoExamenesDAO() {
		return tipoExamenesDAO;
	}

	public void setTipoExamenesDAO(TipoExamenesDAO tipoExamenesDAO) {
		this.tipoExamenesDAO = tipoExamenesDAO;
	}

	
}
