package com.tecsup.SGA.service.evaluaciones.Impl;

import com.tecsup.SGA.bean.FechaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Periodo;
import com.tecsup.SGA.service.evaluaciones.ComunManager;
import com.tecsup.SGA.DAO.ComunDAO;
import com.tecsup.SGA.DAO.evaluaciones.MtoPeriodosDAO;

import java.util.*;

import com.tecsup.SGA.bean.SedeBean;

public class ComunManagerImpl implements ComunManager {
	private ComunDAO comunDAO;
	private MtoPeriodosDAO mtoPeriodosDAO;
	
	public FechaBean getFecha() {
		try
		{
			return this.comunDAO.getFecha();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<SedeBean> GetSedesByEvaluador(String codEvaluador) {
		try
		{
			return this.comunDAO.GetSedesByEvaluador(codEvaluador);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List<Periodo> getPeriodoById(String codSede) {
		try
		{
			return this.mtoPeriodosDAO.getPeriodoById(codSede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<Periodo> getPeriodoBySede(String sede) {
		try
		{
			return this.mtoPeriodosDAO.getPeriodoBySede(sede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public void setComunDAO(ComunDAO comunDAO) {
		this.comunDAO = comunDAO;
	}

	public void setMtoPeriodosDAO(MtoPeriodosDAO mtoPeriodosDAO) {
		this.mtoPeriodosDAO = mtoPeriodosDAO;
	}

	/**
	 * JHPR 2008-7-3
	 */
	public UsuarioSeguridad getUsuarioById(String tipoBusqueda, String dato) {
		try {
			return this.comunDAO.getUsuarioById(tipoBusqueda, dato);
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
		return null;
	}

	public List<Alumno> getAlumnos(String apellPaterno, String apellMaterno, String nombre1, String codCarnet) {
		try
		{
			return this.comunDAO.getAlumnos(apellPaterno, apellMaterno, nombre1, codCarnet);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public String getPeriodoByUsuario(String usuario) {
		try
		{
			return this.mtoPeriodosDAO.getPeriodoByUsuario(usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Periodo> getPeriodoBySedeUsuario(String sede, String codUsuario) {
		try
		{
			return this.mtoPeriodosDAO.getPeriodoBySedeUsuario(sede,codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
}
