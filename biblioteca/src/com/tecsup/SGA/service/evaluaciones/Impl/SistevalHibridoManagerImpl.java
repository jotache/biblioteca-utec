package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.List;
import com.tecsup.SGA.DAO.evaluaciones.SistevalHibridoDAO;
import com.tecsup.SGA.service.evaluaciones.SistevalHibridoManager;
import com.tecsup.SGA.modelo.SistevalHibrido;

public class SistevalHibridoManagerImpl implements SistevalHibridoManager{
	SistevalHibridoDAO sistevalHibridoDAO;
	//UnidadFuncionalDAO unidadFuncionalDAO;
	public List getAllSistevalHibrido(String codigo){
		try
		{
			return sistevalHibridoDAO.getAllSistevalHibrido(codigo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List getSistevalHibridoById(String codigo){
		try
		{
			return sistevalHibridoDAO.getSistevalHibridoById(codigo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String InsertProductoHibrido(SistevalHibrido obj, String usuario){
		try
		{   return this.sistevalHibridoDAO.InsertSistevalHibrido(obj.getCodSec(), obj.getDescripcion(), obj.getCadEvaluacion(), obj.getNroProductos(), usuario);			
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
		
	}
	public String UpdateProductoHibrido(SistevalHibrido obj, String usuario){
		try
		{   return this.sistevalHibridoDAO.UpdateSistevalHibrido(obj.getCodSec(), obj.getDescripcion(), obj.getCadEvaluacion(), obj.getNroProductos(), usuario);			
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
		
	}
	public String DeleteProductoHibrido(SistevalHibrido obj,String usuModi){
		try
		{
			return this.sistevalHibridoDAO.DeleteSistevalHibrido(obj.getCodSec(),usuModi);					
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}
	public SistevalHibridoDAO getSistevalHibridoDAO() {
		return sistevalHibridoDAO;
	}
	public void setSistevalHibridoDAO(SistevalHibridoDAO sistevalHibridoDAO) {
		this.sistevalHibridoDAO = sistevalHibridoDAO;
	}

	
}
