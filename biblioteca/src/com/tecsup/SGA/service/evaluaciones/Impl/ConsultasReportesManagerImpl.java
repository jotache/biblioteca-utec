package com.tecsup.SGA.service.evaluaciones.Impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.evaluaciones.ConsultasReportesDAO;
import com.tecsup.SGA.bean.DetalleExamenes;
import com.tecsup.SGA.bean.DetallePruebaAula;
import com.tecsup.SGA.bean.GrillaEvaluacion;
import com.tecsup.SGA.bean.InformacionGeneral;
import com.tecsup.SGA.bean.RankinBean;
import com.tecsup.SGA.common.Ciclos;
import com.tecsup.SGA.service.evaluaciones.ConsultasReportesManager;
import com.tecsup.SGA.web.evaluaciones.command.ConsultarEvaluacionesCommand;

public class ConsultasReportesManagerImpl implements ConsultasReportesManager {
	
	private static Log log = LogFactory
	.getLog(ConsultasReportesManagerImpl.class);
	
	ConsultasReportesDAO consultasReportesDAO;
	
	public void setConsultasReportesDAO(ConsultasReportesDAO consultasReportesDAO) {
		this.consultasReportesDAO = consultasReportesDAO;
	}
	
	//CONSULTA PARA LA GRILLA DE CONSULTA DE SISTEMA DE EVALUACIONES
	public ConsultarEvaluacionesCommand getAllInformacionGeneral(
			ConsultarEvaluacionesCommand command) {
		try {
			String codPeriodo = command.getCboPeriodo();//getCodPeriodo();
			String codProducto = command.getCboProducto();
			String codAlumno = command.getCodAlumno();
			String codCiclo = command.getCboCiclo();
			String codEspecialidad = command.getCodEspecialidad();
			/*log.info("PARAMETROS DEL STORE getAllInformacionGeneral GRILLA DE CONSULTA POR PERIODO BANDEJA PRINICPAL");
			log.info("codPeriodo>>"+codPeriodo+">>");log.info("codProducto>>"+codProducto+">>");
			log.info("codAlumno>>"+codAlumno+">>");	log.info("codCiclo>>"+codCiclo+">>");*/
			
			List A = consultasReportesDAO.getAllInformacionGeneral(codPeriodo,codProducto,codAlumno,codCiclo);
			//log.info("TAMA�O DE LA LISTA getAllInformacionGeneral:A"+A.size()+":");
			
			///////////////////////*************************************
			/*log.info("PARAMETROS PARA EL STORED getObtenerRankinProm");
			log.info("codAlumno|"+codAlumno+"|");
			log.info("codProducto|"+codProducto+"|");
			log.info("codEspecialidad|"+codEspecialidad+"|");
			log.info("codPeriodo|"+codPeriodo+"|");*/
			//ULTIMA MODIFICACION DATOS DEL RANKIN Y PROMEDIO
			List lstDatos = consultasReportesDAO.getObtenerRankinProm(
					codAlumno,
					codProducto,
					codEspecialidad,
					codPeriodo
					);
			
			if(lstDatos!=null && lstDatos.size()>0)
			{
				RankinBean rankinBean = (RankinBean)lstDatos.get(0);
				command.setTxtPromAcumulado(rankinBean.getPromedioAcumulado());
				command.setTxtRankinCicloNro(rankinBean.getRankin());
				command.setTxtRankinCicloDesc(rankinBean.getDescRankin());
			}else{
				command.setTxtPromAcumulado("");
				command.setTxtRankinCicloNro("");
				command.setTxtRankinCicloDesc("");
				
			}
			
			
			if(A!=null && A.size()>0)
			{							
				InformacionGeneral informacionGeneral = null;	
				
				List lstResultado = null	;
				List lst = null	;
				InformacionGeneral obj2 = null;
				GrillaEvaluacion obj1 = null;
				lstResultado = new ArrayList();				
				
				
				//CONTADOR
				int i=0;
				while( i<A.size() )
					
				{
					//log.info("1");
					lst = new ArrayList();
					//CASTING
					informacionGeneral = (InformacionGeneral) A.get(i);			
					//QUIEBRE
					String x = informacionGeneral.getCodCiclo()==null?"":informacionGeneral.getCodCiclo();
					String y = informacionGeneral.getCodCiclo()==null?"":informacionGeneral.getCodCiclo();					
						
					obj1  = new GrillaEvaluacion();					
					obj1.setNomCiclo(Ciclos.getCicloLatinToRomano(informacionGeneral.getCodCiclo()));
					//obj1.setPromCiclo(informacionGeneral.getPromedio());
					obj1.setPromCiclo(informacionGeneral.getPromedioPeriodo());
					obj1.setNroRankin(informacionGeneral.getNroRankin());
					obj1.setNomRankin(informacionGeneral.getDesRankin());
					
						
					while( x.equalsIgnoreCase(y) && i<A.size() )
					{		log.info("2");
							obj2 = new InformacionGeneral();							
							obj2 = (InformacionGeneral)A.get(i);
							lst.add(obj2);							
							i++;
							if( i<A.size() )
							y = ( (InformacionGeneral)A.get(i) ).getCodCiclo();
							//y = ( (InformacionGeneral)A.get(i) ).getCodAlumno();
							
							
													
					}					
					obj1.setLstCursos(lst);					
					lstResultado.add(obj1);								
				}

				command.setLstResultado(lstResultado);
			}else
				command.setLstResultado(null);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return command;
	}

	public ConsultarEvaluacionesCommand setCabecera(
			ConsultarEvaluacionesCommand command) {
		try {
			String codPeriodo = command.getCodPeriodo();
			String codProducto = command.getCboProducto();
			String codAlumno = command.getCodAlumno();
			String codCiclo = command.getCboCiclo();
			/*log.info("PARAMETROS DEL STORE getAllInformacionGeneralDatos");
			log.info("codPeriodo>>"+codPeriodo+">>");
			log.info("codProducto>>"+codProducto+">>");
			log.info("codAlumno>>"+codAlumno+">>");
			log.info("codCiclo>>"+codCiclo+">>");*/
			
			List lst = consultasReportesDAO.getAllInformacionGeneralDatos(codPeriodo,codProducto,codAlumno,codCiclo);
			//log.info("TAMA�O LISTA getAllInformacionGeneralDatos"+lst.size()+">");
			if( lst!=null && lst.size()>0 )
			{
				InformacionGeneral obj = (InformacionGeneral) lst.get(0);
				command.setTxtAlumno(obj.getNomAlumno());
				command.setTxtPromAcumulado(obj.getPromAcumulado());
				command.setTxtRankinCicloNro(obj.getNroRankin());
				command.setTxtRankinDes(obj.getDesRankin());
				command.setTxtEspecialidad(obj.getDesEspecialidad());
				command.setTxtPromedio(obj.getPromedio());
				command.setTxtRankinCicloNro(obj.getNroRankin());
				command.setTxtRankinCicloDesc(obj.getDesRankin());			
			}			

			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return command;
	}

	public void setConsultarEvaluacionesDetalle(ConsultarEvaluacionesCommand command) {
		String codPeriodo 	= command.getCodPeriodo();
		String codProducto 	= command.getCboProducto();
		String codAlumno 	= command.getCodAlumno();
		String codCiclo 	= command.getCboCiclo();
		String codCurso 	= command.getTxtCurso();
		
		/*log.info("PARAMETROS DEL STORE getAllInformacionGeneralDatos x");
		log.info("codPeriodo>>"+codPeriodo+">>");
		log.info("codProducto>>"+codProducto+">>");
		log.info("codAlumno>>"+codAlumno+">>");
		log.info("codCiclo>>"+codCiclo+">>");
		log.info("codCurso>>"+codCurso+">>");*/
		
		//***************************************************

		List lst = consultasReportesDAO.getAllDetalleInformacionGral(codPeriodo,codProducto,codAlumno,codCiclo,codCurso);
		//log.info("TAMA�O DE LA LIST getAllDetalleInformacionGral>"+lst.size()+">");
    	
		if( lst!=null && lst.size()>0 )
		{
			InformacionGeneral obj = (InformacionGeneral) lst.get(0);
			command.setTxtAlumno(obj.getNomAlumno());
			command.setTxtProducto(obj.getDesProducto());
			command.setTxtEspecialidad(obj.getDesEspecialidad());
			command.setTxtCiclo(codCiclo);
			command.setTxtCurso(obj.getDesCurso());
			command.setTxtPromedio(obj.getPromedio());			
		}else{
			command.setTxtAlumno("");
			command.setTxtProducto("");
			command.setTxtEspecialidad("");
			command.setTxtCiclo("");
			command.setTxtCurso("");
			command.setTxtPromedio("");			
		}
		
		
		//LISTA DE ASISTENCIA 
		List lstAsistencia = consultasReportesDAO.getAllDetalleAsistencia(codPeriodo,codProducto,codAlumno,codCiclo,codCurso);
		//log.info("TAMA�O DE LA LISTA lstAsistencia >"+lstAsistencia.size());
		if(lstAsistencia!=null && lstAsistencia.size()>0){
			command.setLstAsistencia(lstAsistencia);			
		}else{
			command.setLstAsistencia(null);
		}		
		
		
		//LISTA DE PRUEBAS DE AULA //codAlumno = "20020984";//codCurso="1";		
		List lstPruebasAulas = consultasReportesDAO.getAllDetallePruebaAula(codPeriodo,codProducto,codAlumno,codCiclo,codCurso);
		//log.info("TAMA�O DE LA LISTA lstPruebasAulas >"+lstPruebasAulas.size());		
		if(lstPruebasAulas!=null && lstPruebasAulas.size()>0)
		{
			command.setLstPruebaAula(lstPruebasAulas);
			command.setPromedioAula(  ((DetallePruebaAula)lstPruebasAulas.get(0)).getPromedio() );
			//log.info("prom aula>>>"+((DetallePruebaAula)lstPruebasAulas.get(0)).getPromedio());
		}else{
			command.setLstPruebaAula(null);
			command.setPromedioAula("");			
		}
		
		//LISTA PRUEBA DE LA BORATORIO		
		List lstPruebasLab = consultasReportesDAO.getAllDetallePruebaLab(codPeriodo,codProducto,codAlumno,codCiclo,codCurso);
		//log.info("TAMA�O DE LA LISTA lstPruebasLab>"+lstPruebasLab.size());		
		if(lstPruebasLab!=null && lstPruebasLab.size()>0)
		{
			command.setLstPruebaLaboratorio(lstPruebasLab);
			command.setPromedioLab(  ((DetallePruebaAula)lstPruebasLab.get(0)).getPromedio() );
			//log.info("prom lab>>>"+((DetallePruebaAula)lstPruebasLab.get(0)).getPromedio());
		}else{
			command.setLstPruebaLaboratorio(null);
			command.setPromedioLab("");			
		}
		
		List lstExamenes = consultasReportesDAO.getAllDetalleExamenes(codPeriodo,codProducto,codAlumno,codCiclo,codCurso);
		//log.info("TAMA�O DE LA LISTA lstExamenes>"+lstExamenes.size());		
		if(lstExamenes!=null && lstExamenes.size()>0)
		{
			command.setLstExamenes(lstExamenes);
			command.setCargo(((DetalleExamenes)lstExamenes.get(0)).getDescExamen() );
		}else{
			command.setLstExamenes(null);
			command.setCargo("");			
		}
		
		//SI SE NECESITA LAS NOTAS DE TALLER SOLO DESCOMENTAR Y REFRLEJAR EN EL JSP
		List lstTaller = consultasReportesDAO.getAllDetallePruebaTaller(codPeriodo,codProducto,codAlumno,codCiclo,codCurso);
		//log.info("TAMA�O DE LA LISTA lstTaller>"+lstTaller.size());
		if(lstTaller!=null && lstTaller.size()>0)
		{	
			command.setLstPruebaTaller(lstTaller);
			command.setPromedioTaller(((DetallePruebaAula)lstTaller.get(0)).getPromedio());
		}else{
			command.setLstPruebaTaller(null);
			command.setPromedioTaller("");			
		}
		
	}


	public void setInformacion(ConsultarEvaluacionesCommand command) {
		
	}

	public List getProductosxAlumno(String codPeriodo, String codAlumno) {
		return consultasReportesDAO.getProductosxAlumno(codPeriodo,codAlumno);
		}

	public ConsultarEvaluacionesCommand setConsultaDetalle(
			ConsultarEvaluacionesCommand control) {
		
		List lst = consultasReportesDAO.getDatosxProductoxAlumno(control.getCodPeriodo(),control.getCboProducto(),control.getCodAlumno());
		//log.info("TAMA�O LISTA getDatosxProductoxAlumno"+lst.size()+">");
		
		if( lst!=null && lst.size()>0 )
		{
			InformacionGeneral obj = (InformacionGeneral) lst.get(0);
			control.setTxtAlumno(obj.getNomAlumno());			
			control.setTxtPromAcumulado(obj.getPromAcumulado());
			control.setTxtRankinNro(obj.getNroRankin());
			control.setTxtRankinDes(obj.getDesRankin());
			control.setTxtEspecialidad(obj.getDesEspecialidad());			
			control.setCodEspecialidad(obj.getCodEspecialidad());
			//log.info("seter>>"+control.getCodEspecialidad()+">>");
			//control.setTxtPromedio(obj.getPromedio());
			//control.setTxtRankinCicloNro(obj.getNroRankin());
			//control.setTxtRankinCicloDesc(obj.getDesRankin());
			
		}	
		
		return control;
		
	}
	
	public List GetAllRepNotasAvance(String codPeriodo,String sede,String codProducto){
		try {
			return this.consultasReportesDAO.GetAllRepNotasAvance(codPeriodo, sede, codProducto);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllEstadoFinalAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad){
		try{
			return this.consultasReportesDAO.GetAllEstadoFinalAlumno(codPeriodo, codSede, codProducto, codEspecialidad);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllRepNotasFinal(String codPeriodo,String sede,String codProducto){
		try{
			return this.consultasReportesDAO.GetAllRepNotasFinal(codPeriodo, sede, codProducto);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllRepestExamenes(String codPeriodo,String sede,String codProducto,String codEspe){
		try{
			return this.consultasReportesDAO.GetAllRepestExamenes(codPeriodo, sede, codProducto, codEspe);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;		
	}
	
	public List GetAllCursosAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad,String usuario){
		try{
			return this.consultasReportesDAO.GetAllCursosAlumno(codPeriodo, codSede, codProducto, codEspecialidad, usuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllPeriodoxProducto(String codAlumno, String codPeriodo) {
		return this.consultasReportesDAO.getAllPeriodoxProducto(codAlumno,codPeriodo);
	}

	public List getObtenerRankinProm(String codAlumno, String codProducto,
			String codEspecialidad, String codPeriodo) {
		
		return this.consultasReportesDAO.getObtenerRankinProm(codAlumno,codProducto,
				codEspecialidad,codPeriodo);
	}

}
