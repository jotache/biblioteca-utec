package com.tecsup.SGA.service.evaluaciones.Impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.ArrayList;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.CompetenciasPerfil;
import com.tecsup.SGA.DAO.evaluaciones.CompetenciasDAO;
import com.tecsup.SGA.service.evaluaciones.PerfilAlumnoManager;

public class PerfilAlumnoManagerImpl implements PerfilAlumnoManager {

	CompetenciasDAO competenciasDAO;
	
	public List<CompetenciasPerfil> getBandejaTutor(String codPeriodo,
			String codEvaluador, String nomAlumno, String apeAlumno) {
		
		CompetenciasPerfil competenciasPerfil;
		ArrayList<CompetenciasPerfil> arrAlumnosByTutor;
		ArrayList<CompetenciasPerfil> arrConsolidadoAlum;
		NumberFormat formatter = new DecimalFormat("##.00");
		
		double notaIni = 0;
		double notaFin = 0;
		try
		{
			arrAlumnosByTutor = (ArrayList<CompetenciasPerfil>)competenciasDAO.getAllAlumnosByTutor(
																		codPeriodo, codEvaluador
																		, nomAlumno, apeAlumno);
			for(int i = 0; i < arrAlumnosByTutor.size() ; i++)
			{
				arrConsolidadoAlum = (ArrayList<CompetenciasPerfil>)competenciasDAO.getAllComsByAlumno(
														codPeriodo, codEvaluador, arrAlumnosByTutor.get(i).getCodAlumno());
				notaIni = 0;
				notaFin = 0;
				
				for(int j = 0; j < arrConsolidadoAlum.size(); j++)
				{
		    		notaIni = notaIni + Double.parseDouble(arrConsolidadoAlum.get(j).getNotaIni().trim().equals("") ? "0" : arrConsolidadoAlum.get(j).getNotaIni().trim());
		    		notaFin = notaFin + Double.parseDouble(arrConsolidadoAlum.get(j).getNotaFin().trim().equals("") ? "0" : arrConsolidadoAlum.get(j).getNotaFin().trim());
		    		
		    		if (!arrConsolidadoAlum.get(j).getNotaIni().trim().equals("")) 
		    			arrConsolidadoAlum.get(j).setNotaIni(
		    					formatter.format(
		    							Double.parseDouble(arrConsolidadoAlum.get(j).getNotaIni().trim())));
		    		if (!arrConsolidadoAlum.get(j).getNotaFin().trim().equals("")) 
		    			arrConsolidadoAlum.get(j).setNotaFin(
		    					formatter.format(
		    							Double.parseDouble(arrConsolidadoAlum.get(j).getNotaFin().trim())));
				}
				
		    	competenciasPerfil = new CompetenciasPerfil();
		    	competenciasPerfil.setNotaIni(notaIni > 0 ? formatter.format( notaIni ) : "");
		    	competenciasPerfil.setNotaFin(notaFin > 0 ? formatter.format( notaFin ) : "");
		    	
		    	arrConsolidadoAlum.add(competenciasPerfil);
		    	arrAlumnosByTutor.get(i).setListaCompetencias(arrConsolidadoAlum);
			}
			
			return arrAlumnosByTutor;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<CompetenciasPerfil> getDetalleAlumno(String codPeriodo, String codEvaluador
												, String codAlumno, String tipoEval, String tipoOpe)
	{
		ArrayList<CompetenciasPerfil> arrPerfilesConceptoByAlumno = new ArrayList<CompetenciasPerfil>();
		ArrayList<CompetenciasPerfil> arrConceptoPerfil;
		CompetenciasPerfil perfil;
		try
		{
			ArrayList<CompetenciasPerfil> arrConsolidadoAlum  = (ArrayList<CompetenciasPerfil>)competenciasDAO.getAllComsByAlumno(
					codPeriodo, codEvaluador, codAlumno);
			
			for (int i = 0; i < arrConsolidadoAlum.size(); i++)
			{
				perfil = arrConsolidadoAlum.get(i);
				perfil.setFlagTipoEval(tipoEval);
				perfil.setCodEvalFin("");
				perfil.setCodEvalIni("");
				perfil.setCodTipoOperacion("");
				
				arrConceptoPerfil = (ArrayList<CompetenciasPerfil>)this.competenciasDAO.getAllCompPerfilByAlumno(codPeriodo, codEvaluador
								, codAlumno, perfil.getCodCompetencia(), tipoOpe);
				if ( arrConceptoPerfil.size() > 0 )
				{
					arrPerfilesConceptoByAlumno.add(perfil);
					for(int j = 0 ; j < arrConceptoPerfil.size() ; j++)
					{
						perfil = arrConceptoPerfil.get(j);
						perfil.setFlagTipoEval(tipoEval);
						perfil.setCodTipoOperacion(tipoOpe);
						arrPerfilesConceptoByAlumno.add(perfil);
					}
				}
			}
		
			return arrPerfilesConceptoByAlumno;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public void setCompetenciasDAO(CompetenciasDAO competenciasDAO) {
		this.competenciasDAO = competenciasDAO;
	}	
}
