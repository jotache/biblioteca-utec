package com.tecsup.SGA.service.evaluaciones;

import java.util.List;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.web.evaluaciones.command.RegistrarExamenesCommand;

public interface GestionAdministradtivaManager {
	
	/**
	 * @param codCurso
	 * @param nombre
	 * @param apellido
	 * @param codSeccion
	 * @return Lista de exclusiones por curso.
	 */
	public List getAllExclusionesByCurso(String codCurso, String nombre, String apellido
			, String codSeccion, String codPeriodo);
	
	/**
	 * @param codCurso
	 * @param cadDatos
	 * @param codUsuario
	 * @return Inserta las exclusiones por curso de manera masiva.
	 */
	public String insertExclusiones(String codCurso, String cadDatos
			, String codUsuario, String codPeriodo, String codSeccion);
	
	/**
	 * @return Lista de tipos ex�menes
	 */
	public List getAllTypesOfExams(String codPeriodo, String codCurso, String tipoExamen);
	
	/**
	 * @param codPeriodo
	 * @param codProducto
	 * @param codEspecialidad
	 * @param codCiclo
	 * @return Lista de cursos por producto, especialidad y ciclo, segun el codigo del periodo vigente.
	 */
	//JHPR: 2/Abrl/08 Agregue parametro para filtrar solo curso Cargo en la Bandeja Principal del Administrador.
	public List getAllCursosByFiltros(String codPeriodo, String codProducto, String codEspecialidad, String codCiclo,
									String codEtapa, String codPrograma,String filtrarCargos);
	
	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codSeccion
	 * @param nombre
	 * @param apellido
	 * @return Lista de las notas (x alumnos) por curso y seccion
	 */
	//JHPR 31/3/2008 Para visualizar los alumnos con examenes de cargo.
	public List getAllExamsByAlumno(String codPeriodo, String codCurso, String codSeccion, String nombre, String apellido,String tipoExamen);
	
	public RegistrarExamenesCommand setConsulta(
			RegistrarExamenesCommand control);
	
	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codSeccion
	 * @param cadNotas
	 * @param nroRegistros
	 * @param codUsuario
	 * @return Guarda las notas de los alumnos
	 */
	public String insertNotasExamenes(String codPeriodo, String codCurso, String codSeccion, String cadNotas, 
			String codUsuario);
	
	//JHPR: 1/Abril/08 Ingresar notas de examenes de cargo.
	public String insertNotasExamenesCargo(String codPeriodo, String codCurso, String cadNotas, 
			String codUsuario,String tipoExamen);
	
	//JHPR 2008-7-14
	public String insertNotasExamenesSubsanacion(String codPeriodo, String codCurso, String cadNotas, 
			String codUsuario);
	
	public String insertNotasExamenesRecuperacion(String codPeriodo, String codCurso, String cadNotas,String codUsuario);

	public String updateLevantarDI(String codCurso, String codTipoSesion,
			String codSeccion, String codAlumno, String tipoIncidencia, String tipoOpe, String usuario);
}
