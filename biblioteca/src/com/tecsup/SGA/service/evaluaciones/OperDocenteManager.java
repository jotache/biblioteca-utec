package com.tecsup.SGA.service.evaluaciones;

import java.util.List;

import com.tecsup.SGA.bean.EvaluacionesParciales;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.CompetenciasPerfil;
import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.CursoEvaluador;
import com.tecsup.SGA.modelo.DefNroEvalParciales;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.FormacionEmpresa;
import com.tecsup.SGA.modelo.HorasXalumnos;
import com.tecsup.SGA.modelo.NumeroMinimo;
import com.tecsup.SGA.modelo.Parciales;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.web.evaluaciones.command.EvaluacionesParcialesCommand;

public interface OperDocenteManager {
	
	/**
	 * @param evaluador
	 * @return Lista de cursos en ejecucion por docente
	 */
	public List<CursoEvaluador> getCursoInTime(String evaluador);
	
	/**
	 * @param periodo
	 * @param evaluador
	 * @param producto
	 * @param especialidad
	 * @param curso
	 * @param codEvalSeleccionado
	 * @param dscCurso
	 * @return Lista de cursos por evaluador y periodo vigente
	 */
	public List getAllCursos(String periodo, String evaluador, String producto, String especialidad, String curso,
			String codEvalSeleccionado, String dscCurso);
	
	/**
	 * @param tipoSesion
	 * @param codCurso
	 * @return
	 */
	public List getAllSeccionCurso(String tipoSesion, String codCurso);

	/**
	 * @param codCurso
	 * @param codSeccion
	 * @param nombre
	 * @param apellido
	 * @return Lista de Alumnos por curso
	 */
	public List getAllAlumnosCurso(String codPeriodo, String codCurso, 
									String codProducto, String codEspecialidad, String tipoSesion,
									String codSeccion, String nombre, String apellido);
	
	/*Metodos de llamado a Incidencias DAO*/
	
	/**
	 * @param codPeriodo
	 * @param codAlumno
	 * @param codCurso
	 * @param tipoOpe
	 * @param codTipoSesion
	 * @param codSeccion
	 * @return Lista de incidencias por alumno por curso
	 */
	public List getAllIncidencias(String codPeriodo, String codAlumno, String codCurso, String tipoOpe
			, String codTipoSesion, String codSeccion);


	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codProducto
	 * @param codEspecialidad
	 * @param codTipoSesion
	 * @param codSeccion
	 * @param tipoOpe
	 * @param nombre
	 * @param apellido
	 * @return Retorna Lista de incidencias por curso
	 */
	public List getAllIncidenciasByCurso(String codPeriodo, String codCurso, String codProducto
    		, String codEspecialidad, String codTipoSesion, String codSeccion
    		, String tipoOpe, String nombre, String apellido);
	
	/**
	 * @param codIncidencia
	 * @param codAlumno
	 * @param codCurso
	 * @param dscIncidencia
	 * @param tipoIncidencia
	 * @param tipoOpe
	 * @param fecha
	 * @param fechaIni
	 * @param fechaFin
	 * @param usuario
	 * @param codTipoSesion
	 * @param codSeccion  
	 * @return Retorna el codigo de la incidencia ingresada
	 */
	public String insertIncidencia( String codIncidencia, String codCurso, String codTipoSesion, String codSeccion
    		, String codAlumno, String fecha, String fechaIni, String fechaFin
			, String dscIncidencia, String tipoIncidencia, String tipoOpe
			, String usuario);

	/**RETORNA EL NUMERO DE EVALUACIONES X EVALUACION PARCIAL
	 * @param codCurso
	 * @param cboEvaluacionParcial
	 * @return
	 */
	public List getAllNroEvaluacionesxParciales(String codPeriodo,String codSeccion,String codCurso,
			String codEvaluacionParcial);
	
	/**
	 * @param codIncidencia
	 * @param codUsuario
	 * @return Borra incidencia
	 */
	public String deleteIncidencia(String codIncidencia, String codUsuario);
	

	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codSeccion
	 * @param nombre
	 * @param apellido
	 * @param tipoOpe
	 * @param codTipoSesion
	 * @return Lista las competencias por curso con la nota inicial y final por competecnias.
	 */
	public List getAllConpetenciasByCurso(String codPeriodo, String codCurso, String codSeccion, String nombre
			, String apellido, String tipoOpe, String codTipoSesion);

	/**
	 * @param codCurso
	 * @param codSeccion
	 * @param codAlumno
	 * @param tipoOpe
	 * @return Lista de competencias por alumno por curso.
	 */
	public List<CompetenciasPerfil> getAllConpetenciasByAlumno(String codPeriodo, String codCurso, String codSeccion
			, String codAlumno, String tipoOpe);
	
	/**
	 * @param codAlumno
	 * @param codCurso
	 * @param codPeriodo
	 * @param tipoOpe
	 * @param datosComp
	 * @param codUsuario
	 * @return Insert de forma masiva la evaluaciones de las competencias para un alumno
	 */
	public String insertCompetenciasByAlumno(String codAlumno,String codCurso,String codPeriodo,String tipoOpe,
    		String datosComp,String codUsuario, String codSeccion, String codTipoSesion); 
    		
	public List GetAllTipoEvaluaciones();
	
	public List GetAllEvaluaciones(Parciales obj);
	
	public String InsertPeso(Parciales obj, String usuario);
	
	public List GetAllEvaluadores(CursoEvaluador obj,String codPeriodo);
	
	public String DeleteEvaluaciones(Parciales obj,String numReg,String usuario);

	/**
	 * @param codCurso
	 * @param codSeccion
	 * @param strNombre
	 * @param strApellido
	 * @return
	 */
	public List<EvaluacionesParciales> getAllEvaluacionesParcialesxCurso(String codPeriodo,String codCurso,
			String codSeccion, String strNombre, String strApellido, 
			String cboNroEvaluacion,String cboEvaluacionParcial,String codProducto,String codEspecialidad,String fecEvaluacion);
			
	/**
	 * @return Retorna Id para grabar la asistencia de los alumnos
	 */
	public String getIdAsistencia();
	

	/**
	 * @param codPeriodo
	 * @param codAsistencia
	 * @param codAlumno
	 * @param codCurso
	 * @param tipoSesion
	 * @param codSeccion
	 * @param nroSesion
	 * @param fecha
	 * @param tipoAsist
	 * @param usuario
	 * @param flagProg
	 * @return Graba asistencia
	 */
	public String insertAsistencia(String codPeriodo, String codAsistencia, String codAlumno, String codCurso, String tipoSesion, 
			String codSeccion, String nroSesion, String fecha, String tipoAsist, String usuario, String flagProg);

	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codProducto
	 * @param codEspecialidad
	 * @param tipoSesion
	 * @param codSeccion
	 * @return Fechas registradas para la asistencia
	 */
	public List getAllFechaAsistenciaCurso(String codPeriodo, String codCurso, 
			String codProducto, String codEspecialidad, String tipoSesion, String codSeccion);
	
	
	/**
	 * @param codPeriodo
	 * @param codAsistencia
	 * @param nombre
	 * @param apellido
	 * @param nroSesion
	 * @param codCurso
	 * @param codSeccion
	 * @return Asistencia de los alumnos por curso, seccion, nrosesion, nombre y apellido
	 */
	public List getAllAlumnosXasistencia(String codPeriodo, String codAsistencia, String nombre, String apellido, String nroSesion,
										String codCurso, String codProducto, String codEspecialidad, String tipoSesion, 
										String codSeccion);
										
	public NumeroMinimo GetAllNumeroMinimo(Curso obj);
	
	public List GelAllFormacionEmpresa(String codPeriodo, String codProducto
			,String codEspecialidad, String nombre, String apPaterno);
	
	public String InsertEmpresa(FormacionEmpresa obj, String usuario);
	
	public String InsertEmpresaId(FormacionEmpresa obj, String usuario);	
	/**
	 * @param control
	 * @return
	 */
	public EvaluacionesParcialesCommand setConsulta(
			EvaluacionesParcialesCommand control);
	public String AdjuntarArchivo(FormacionEmpresa obj, String usuario);
	
	public HorasXalumnos HorasPorAlumno(FormacionEmpresa obj);
	
	public List EmpresaXAlumno(FormacionEmpresa obj);
	
	public Producto ProductoxCurso(FormacionEmpresa obj);
	
	public List GetAllPeriodo();
	
	public String ModificarFormacionEmpresa(FormacionEmpresa obj,String usuario);
	
	public List GetAllAdjuntoEmpresa(FormacionEmpresa obj);
	
	//TipoSesioXCurso
	public List getAllSesionXCurso(String codCurso);

	/**METODO PARA LISTAR LAS PRACTICAS DADO UN CURSO Y UN EVALUADOR
	 * @param codPeriodo
	 * @param codCurso
	 * @param codEval
	 * @param codProducto
	 * @param codEspecialidad
	 * @return
	 */
	public List getAllTipoPracticaxCursoxEval(String codPeriodo,
			String codCurso, String codEval, String codProducto,
			String codEspecialidad);

	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @param codEval
	 * @param codEspecialidad
	 * @param cboEvaluacionParcial
	 * @return
	 */
	public List getAllSeccionesxCursoxEval(String codPeriodo, String codCurso,
			String codEval,String codProducto ,String codEspecialidad, String cboEvaluacionParcial);

	/**METODO PARA MOSTRAR LOS NUMERO DE EVELUACIONES POR CURSO Y POR EVAL
	 * @param codPeriodo
	 * @param codCurso
	 * @param codEval
	 * @param codProducto
	 * @param codEspecialidad
	 * @param cboEvaluacionParcial
	 * @param cboSeccion
	 * @return
	 */
	public List getAllNroEvaxCursoxEva(String codPeriodo, String codCurso,
			String codEval, String codProducto, String codEspecialidad,
			String cboEvaluacionParcial, String cboSeccion);
	
	/**
	 * @param codEvaluador
	 * @param codSede
	 * @param codPeriodo
	 * @param tipoUsuario
	 * @return Listado de Evaluadores para los jefes de Departamento
	 */
	// CACT : 15-04-2008 Listado de Evaluadores para los jefes de Departamento
	public List<Evaluador> getEvaluadoresByJefeDep(String codEvaluador, String codSede, String codPeriodo, String tipoUsuario);
	
	/**
	 * @param codResponsable
	 * @param codCurso
	 * @param codTipoSesion
	 * @return Lista de secciones por responsable de tomar asistencia
	 */
	// CACT : 17-04-2008 Lista de Secciones por responsable de tomar asistencia
	public List<CursoEvaluador> GetAllSeccionesxCursoxResp(String codResponsable, String codCurso, String codTipoSesion);
	
	/**
	 * @param codCurso
	 * @param tipoSesion
	 * @param codSeccion
	 * @param nroSesion
	 * @param fecha
	 * @return Elimina una fecha de asistencia siempre y cuando sea programada
	 */
	public String deleteAsistencia(String codCurso, String tipoSesion, String codSeccion, String nroSesion, String fecha);
	
	//JHPR: 2008-06-04 Cerrar registro de notas.
	public String cerrarRegistroNotas(String codCurso, String codSeccion, String tipoEvaluacion,String codUsuario,String flagCerrado);
	
	//JHPR: 2008-06-04 Obtener Definición de Parcial
	public Parciales obtenerDefinicionParcial(String codigo);
	
	//JHPR: 2008-06-05 Obtener Definiciones Parciales
	public List<Parciales> obtenerDefinicionesParciales(String codCurso,String codSeccion,String TipoEvaluacion,String NroEvaluacion,String Estado);
	
	//JHPR: 2008-06-09 
	public List<DefNroEvalParciales> consultarCierreNotas(String sede,String codPeriodo,String codProducto,String codEspecialida);
	
	public List<Evaluador> obtenerEvaluadorSeccion(String codCursoEjec,String codSeccion);
	public String abrirRegistroNotas(String codCurso, String codSeccion,String tipoEvaluacion, String codUsuario);
	public String cerrarRegistroAsistencia(String codCurso, String codSeccion, String tipoEvaluacion,String codUsuario,String flagCerrado);
	public String abrirRegistroAsistencia(String codCurso, String codSeccion,String tipoEvaluacion, String codUsuario);
	public List<Alumno> listarDesaprobadosXDI(String curso,String seccion,String nombres,String apellidos);
}
