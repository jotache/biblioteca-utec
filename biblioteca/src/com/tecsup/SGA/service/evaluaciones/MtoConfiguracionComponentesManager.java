package com.tecsup.SGA.service.evaluaciones;
import java.util.List;

import com.tecsup.SGA.modelo.Curso;
import com.tecsup.SGA.modelo.Especialidad;
import com.tecsup.SGA.modelo.Producto;
import com.tecsup.SGA.modelo.ProgramaComponentes;
import com.tecsup.SGA.modelo.Programas;

public interface MtoConfiguracionComponentesManager {
 
	public List getComponentes(ProgramaComponentes obj);
	public String InsertComponentes(ProgramaComponentes obj, String NROREGISTROS, String usuario);
	public List getEspecialidadByProducto(Producto obj);
	public List getProgramasByProducto(Producto obj, String codEtapa);
	public List getCursosByProducto(Producto obj, String codPeriodo);
	public List getCicloByCursoAndProducto(Producto obj, Curso curso);
	public List getCicloByProductoEspecialidadPrograma(Producto producto, Especialidad especialidad, 
			Programas programa, String codPeriodo);
	public List getCursoByProductoEspecialidadCiclo(Producto producto, Programas programas, 
			Especialidad especialidad, Curso curso, String codEtapa, String codPeriodo);
	//nota externa
	public List getCicloByProductoEspecialidadPrograma2(String codProducto, String codEspecialidad,
			String codPrograma, String codPeriodo);
	
	public List getCursoByProductoEspecialidadCicloExt(Producto producto, Programas programas, 
			Especialidad especialidad, Curso curso, String codEtapa, String codPeriodo);
	
	//JHPR: 2008-05-20 Listar Cursos de Nota Externa...
	/*public List ListarCursosNotaExternaXProductoCiclo(String codProducto,String codEspecialidad,
			String codCiclo, String codPrograma,String codEtapa, String codPeriodo);
			*/
}
