package com.tecsup.SGA.service.evaluaciones;

import com.tecsup.SGA.bean.FechaBean;
import com.tecsup.SGA.bean.UsuarioSeguridad;

import java.util.*;

import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Periodo;

public interface ComunManager {
	/**
	 * @return Objecto fecha con lo datos de la base de datos
	 */
	public FechaBean getFecha();
	
	/**
	 * @param codEvaluador
	 * @return LIsta de Sedes por Evaluador
	 */
	public List<SedeBean> GetSedesByEvaluador(String codEvaluador);
	
	/**
	 * @param sede
	 * @return Saca el periodo Activo por Sede.
	 */
	
	public List<Periodo> getPeriodoBySede(String sede);
	public List<Periodo> getPeriodoBySedeUsuario(String sede, String codUsuario);
	
	
	public String getPeriodoByUsuario(String usuario);
	
	
	/**
	 * @param sede
	 * @return Saca el periodo Activo por Sede.
	 */
	public List<Periodo> getPeriodoById(String codSede);
	
	/**
	 * JHPR 2008-7-3
	 * @param tipoBusqueda => Por CODSUJETO o Por USERNAME
	 * @param dato => el codsujeto o username
	 * @return Retorna obj UsuarioSeguridad
	 */
	public UsuarioSeguridad getUsuarioById(String tipoBusqueda, String dato);

	
	/**
	 * JHPR 2008-09-05
	 * Busqueda de Alumnos por datos personales (nombre y apellidos)
	 * @param apellPaterno
	 * @param apellMaterno
	 * @param nombre1
	 * @return
	 */
	public List<Alumno> getAlumnos(String apellPaterno, String apellMaterno, String nombre1,String codCarnet);
	
}
