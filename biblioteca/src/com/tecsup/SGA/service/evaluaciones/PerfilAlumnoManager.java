package com.tecsup.SGA.service.evaluaciones;

import java.util.List;
import com.tecsup.SGA.modelo.CompetenciasPerfil;

public interface PerfilAlumnoManager {
	
	/**
	 * @param codPeriodo
	 * @param codEvaluador
	 * @param nomAlumno
	 * @param apeAlumno
	 * @return Lista de alumnos por evaluador
	 */
	public List<CompetenciasPerfil> getBandejaTutor(String codPeriodo, String codEvaluador
			, String nomAlumno, String apeAlumno);
	
	
	/**
	 * @param codPeriodo
	 * @param codEvaluador
	 * @param codAlumno
	 * @param tipoEval
	 * @param tipoOpe
	 * @return
	 */
	public List<CompetenciasPerfil> getDetalleAlumno(String codPeriodo, String codEvaluador
						, String codAlumno, String tipoEval, String tipoOpe);
	
	
}
