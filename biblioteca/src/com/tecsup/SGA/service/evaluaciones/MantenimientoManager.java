package com.tecsup.SGA.service.evaluaciones;

import java.util.List;

import com.tecsup.SGA.web.evaluaciones.command.EvaluacionesParcialesCommand;
import com.tecsup.SGA.web.evaluaciones.command.ParametrosEspecialesCommand;

public interface MantenimientoManager {

	public String insertParametrosEspeciales(ParametrosEspecialesCommand control);

	/**
	 * @param codPeriodo
	 * @return
	 */
	public List getLstCilos(String codPeriodo);

	/**
	 * @param codPeriodo
	 * @param codTabla
	 * @return
	 */
	public List getAllParametrosEspeciales(String codPeriodo,String codTabla);

	/**
	 * @param command
	 * @return
	 */
	public ParametrosEspecialesCommand setParametrosEspeciales(
			ParametrosEspecialesCommand command);

	/**
	 * @param control
	 * @return
	 */
	public String updateParametrosEspeciales(ParametrosEspecialesCommand control);

	/**
	 * @param codPeriodo
	 * @param codTabla
	 * @return
	 */
	public List getTipoPractica();

	/**metodo para almacenar las evaluaciones parciales.
	 * @param control
	 * @return
	 */
	public Object insertEvaluacioneParciales(
			EvaluacionesParcialesCommand control);

	/**
	 * @param control
	 * @return
	 */
	public Object updateEvaluacioneParciales(
			EvaluacionesParcialesCommand control);
	
	/**
	 * @param codPeriodo
	 * @param codCurso
	 * @return
	 */
	public List getTipoPracticaxCurso(String codPeriodo,String codCurso);
	public List getAllCursosCat(String codProducto,String codCiclo);
	public List GetAllPeriodosBySede(String codSede);
}
