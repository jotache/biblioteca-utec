package com.tecsup.SGA.service.evaluaciones;

import java.util.List;

import com.tecsup.SGA.web.evaluaciones.command.ConsultarEvaluacionesCommand;

public interface ConsultasReportesManager {

	ConsultarEvaluacionesCommand getAllInformacionGeneral(
			ConsultarEvaluacionesCommand command);

	ConsultarEvaluacionesCommand setCabecera(
			ConsultarEvaluacionesCommand command);

	void setInformacion(ConsultarEvaluacionesCommand command);

	void setConsultarEvaluacionesDetalle(ConsultarEvaluacionesCommand command);

	List getProductosxAlumno(String codPeriodo, String codAlumno);

	ConsultarEvaluacionesCommand setConsultaDetalle(
			ConsultarEvaluacionesCommand control);
	
	/**METODO QUE RETORNA LOS PERIDOS EN FUNCION DE ALUMNO Y EL PRODUCTO
	 * USADO PARA LISTAR COMBO PERIODOS
	 * @return
	 */
	List getAllPeriodoxProducto(String codAlumno,String codPeriodo);
	
	/**Obtener el Rankin y promedio
	 * @param codAlumno
	 * @param codProducto
	 * @param codEspecialidad
	 * @param codPeriodo
	 * @return
	 */
	List getObtenerRankinProm(String codAlumno,String codProducto,String codEspecialidad,String codPeriodo);
	
	//Eddy Dominguez - 22/04/2008
	public List GetAllRepNotasAvance(String codPeriodo,String sede,String codProducto);
	
	public List GetAllEstadoFinalAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad);
	
	public List GetAllRepNotasFinal(String codPeriodo,String sede,String codProducto);
	
	public List GetAllRepestExamenes(String codPeriodo,String sede,String codProducto,String codEspe);

	public List GetAllCursosAlumno(String codPeriodo, String codSede,
			String codProducto, String codEspecialidad,String usuario);
}
