package com.tecsup.SGA.service.evaluaciones;
import java.util.List;

import com.tecsup.SGA.modelo.HibridoCurso;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public interface HibridoCursoManager {
	public List getHibridoCursoById(String codigo);
	
	public String InsertHibridoCurso(HibridoCurso obj, String usuario);
	
	public String UpdateHibridoCurso(HibridoCurso obj, String usuario);
	
}
