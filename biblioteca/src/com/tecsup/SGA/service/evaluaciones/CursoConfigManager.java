package com.tecsup.SGA.service.evaluaciones;
import java.util.List;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.CursoConfig;

public interface CursoConfigManager {	
	public List getCursoConfigById(String codigo);
	public String InsertCursoConfig(CursoConfig obj, String usuario);
	public String UpdateCursoConfig(CursoConfig obj, String usuario);	
}
