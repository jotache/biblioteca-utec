package com.tecsup.SGA.service.evaluaciones;

import java.util.List;
import com.tecsup.SGA.modelo.ParametrosGenerales;

public interface MtoParametrosGeneralesManager {

	//public List getParametrosGenerales(String codProducto, String codPeriodo);
	public List getAllParametrosGenerales(String codProducto, String codPeriodo );
	public List getProductos();
	public List getPeriodos();
	public String Insert_O_UpdateParametrosGenerales(ParametrosGenerales obj, String usuario);
	
}
