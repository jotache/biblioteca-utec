package com.tecsup.SGA.service.evaluaciones;
import java.util.List;
import com.tecsup.SGA.modelo.Archivo;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.ConfiguracionNotaExterna;


public interface NotaExternaManager {
	
	public String UpdateNotaExterno(Alumno obj,String usuario);
	public String InsertNotaExterno(Alumno obj,String usuario);
	public String InsertAdjuntoCasoCat(Archivo obj,String usuario);
	public List getAdjuntoCasoCatById(String periodo,String producto,String ciclo);	
	public List getAllCursosNotaExterna();
	
	//JHPR 2008-05-05 Calculo de Nota Tecsup.
	public ConfiguracionNotaExterna configuracionNotaExterna(String codCursoEjec);
	/*public String InsertNotaExterno(String codPeriodo,String codProducto,String codCiclo,String codCurso,String cadenaDatos,
			String tipoReg, String usuario,String nroRegistros);*/
	public String InsertNotaExterno(String codPeriodo,String codProducto,String cadenaDatos,
			String tipoReg, String usuario,String nroRegistros);
	
	public List<ConfiguracionNotaExterna> listaConfiguracionNotaExterna(String codPeriodo);
}
