package com.tecsup.SGA.service.evaluaciones;
import java.util.List;
import com.tecsup.SGA.modelo.SistevalHibrido;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public interface SistevalHibridoManager {
	public List getAllSistevalHibrido(String codigo);
	public List getSistevalHibridoById(String codigo);
	public String InsertProductoHibrido(SistevalHibrido obj, String usuario);
	public String UpdateProductoHibrido(SistevalHibrido obj, String usuario);
	public String DeleteProductoHibrido(SistevalHibrido obj,String usuModi);	
}
