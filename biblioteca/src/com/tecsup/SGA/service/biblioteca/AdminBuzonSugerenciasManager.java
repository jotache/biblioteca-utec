 package com.tecsup.SGA.service.biblioteca;

import java.util.List;

import com.tecsup.SGA.modelo.BuzonSugerencia;

public interface AdminBuzonSugerenciasManager {

	public List GetAllSugerencias(BuzonSugerencia obj);
	public String UpdateAtenderBuzonSugerencia(BuzonSugerencia obj);
	public String InsertSugerencia(BuzonSugerencia obj);
	public BuzonSugerencia obtenerSugerencia(String id); //JHPR 2008-7-30
}
