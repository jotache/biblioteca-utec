package com.tecsup.SGA.service.biblioteca;

import java.util.List;

import com.tecsup.SGA.bean.OpcionesApoyo;
import com.tecsup.SGA.modelo.AccesoIP;
import com.tecsup.SGA.modelo.BuzonSugerencia;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.ParametrosReservados;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.modelo.UsuarioPM;

public interface BiblioMantenimientoManager {

	public List getAllTablaDetalleBiblioteca(TipoTablaDetalle obj);
	public String InsertTablaDetalleBiblioteca(TipoTablaDetalle obj);
	public String UpdateAndDeleteTablaDetalleBiblioteca(TipoTablaDetalle obj);
	public List getAllPaises(Pais obj, String tipoOrden);
	public String InsertPaises(Pais obj, String usuario);
	public List getCiudadByPais(Ciudad obj, String tipoOrden);
	public String insertCiudadByPais(Ciudad obj);
	public List GetAllFeriados(String codTablaId, String mes, String anio);
	public String InsertFeriados(String codTablaId, String dis, String mes
			, String anio, String descripcion,String usucrea);
	public String updateDeletePais(Pais obj);
	public String updateDeleteCiudadbyPais(Ciudad obj);
	public String ActualizarFeriado(String codTablaId, String id, String dis, String mes
			, String anio, String descripcion,String usucrea, String flag);
	public ServiciosUsuarios GetAllServicioUsuario(String tipId, String valor1);
	public String InsertServicioUsuario(String tiptId, String valor1, String valor2,String valor3,
			String valor4, String valor5, String valor6, String valor7, String valor8,
			String valor9,String valor10,String usuCrea,String cadenaDatos, String tamanoDatos);
	public List Sanciones();
	public String UpdateSanciones(TipoTablaDetalle obj);
	public String UpdateGrupos(String codTabla, String cadena, String nroRegistros, String codUsuario);
	public List getSeccionesByFiltros(Secciones obj);
	public String insertSecciones(Secciones seccion, String usuario, String tipo);

	public String InsertParametrosReservados(String codTablaId, String valor1, String valor2,
			String valor3, String valor4,String valor5,String valor6,
			String valor7,String valor8,String valor9,String usuario);
	public ParametrosReservados GetAllParametrosGenerales(String tipId);
	public String getNombreArchivoSeccion();
	public TipoTablaDetalle GetTipoTablaDetalle(String codPadre, String codHijo);
	public List<UsuarioPM> GetAllUsuariosAcceso();
	public List<AccesoIP> GetAllAccesosLista(Integer codUsuario);
	public void InsertAccesoIP(Integer codUsuario, String numIP, String usuCreacion);
	public void DeleteAccesoIP(Integer codUsuario, String numIP);
	public void DeleteAccesosIP(Integer codPersona);
	public void DeleteOpcionesApoyo(Integer codPersona);
	public void DeleteRolesApoyo(String codUsuario);
	public List<ParametrosReservados> GetListParametrosGenerales();
	public String UpdateParametrosGenerales(String cadenaDatos,String usuario,String nroRegistros);
	public List<TipoTablaDetalle> listaMaximoDiasPrestamos(String codTipoUsuario);
	public List<OpcionesApoyo> listaDeOpcionesPorApoyo(String codApoyo);
	public String updateOpcionesApoyo (String codApoyo, String cadena, String nroRegistros, String codUsuario);
	public List<Sala> listaHorarioSalas(String sede);
	public List<TipoTablaDetalle> listaHorasBiblioteca(String sede);
	public String UpdateHorarioSalas(String cadenaDatos,String usuario,String nroRegistros);
}
