package com.tecsup.SGA.service.biblioteca;

import java.util.List;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Busqueda;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public interface PortalWebBibliotecaManager {
	
	public List getMaximaCapacidadSalas(Sala obj);
	public List getReservaSalas(Sala obj);
	public String insertReservaSalas(Sala obj);
	public String DeleteReservaSalas(String cadena, String nroRegistros, String usuCrea);
	public List GetSeccionesPortalWeb(Secciones obj,TipoTablaDetalle obj2);
	public List GetBusquedaSimple(Busqueda obj);
	public String InsertReservaByMaterial(String codTipoMaterial, String fechaReserva, String usuCre);
	public List GetBusquedaAvanzada(Busqueda obj);
	public String AplicarReservaSalas(String codUsuarioReserva,  
			String codSala, String codReserva, String codUsuario, String estado);
	public String GetVerificarReservaUsuario(String codUsuario);
	public Sala getReservaSalaById(String codRecurso, String codSequencia);
	public String terminarReservaSalas(String codRecurso, String codSecuencia,String usuario);
	public String sedeUsuario(String username);
	public List<Alumno> getUsuarios(String apellPaterno, String apellMaterno, String nombre1,String usuario);
	public List<Sala> ListarHorasAtencionPorSede(String sede);
	public List<Sala> ListarRecursosAtencion(String fecReserva, String sede, String tipoSala);
	
	public AlumnoXCodigoBean obtenerUsuarioAtencion(String usuario);
	public String insertarAtencion(String codRecurso,String codUsuarioAtencion,String fecha, String horaIni,String horaFin,String codUsuarioActualiza,String sede);
	public List<Sala> listarAtenciones(String fecha, String sede);
	public List<Sala> listarAtencionesPorPC(String fecha,String sede,String codPC);
	public List<Sala> listarAtencionesPorPCHistorico(String sede, String codPC);
	public String actualizarAtencion(String codAtencion, String tipoOperacion,String codUsuarioActualiza);
}
