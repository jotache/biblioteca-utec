package com.tecsup.SGA.service.biblioteca;

import java.util.List;

import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.modelo.Sancion;


public interface BiblioProcesoSancionesManager {


	/**INSERTA UNA SANCION
	 * @param nroIngreso
	 * @param usuario
	 * @return
	 */
	public String insertSancion(String nroIngreso,String fecDevolucion,String usuario,String sede, String guardaSancion);
	
	/**
	 * @param codMaterial
	 * @param fecReserva
	 * @param codUsuarioReserva
	 * @param codUsuario
	 * @return
	 */
	public String insertReservaMaterial(
			String codMaterial,
			String fecReserva,
			String codUsuarioReserva,
			String codUsuario);
	
	/**
	 * @param codUsuario
	 * @param recCodigo
	 * @param renSecuencia
	 * @param usuario
	 * @param estadoReserva
	 * @return
	 */
	public String insertSancionReservaSalas(
			String codUsuario,
			String recCodigo,
			String renSecuencia,
			String usuario,
			String estadoReserva);

	public List<Sancion> getSancionesUsuario(String codUsuario) ;
	public List<Sancion> getSancionesUsuarioPorPrestamoMaterial(String codUsuario) ;
	public String updateSancionUsuario(String codUsuario, String idSancion,String codUsuarioAccion);
	public String insertarInhabilitaciones(String codUsuario,String cadSansiones, String nroReg, String fecIni, String fecFin,String obsInhabi,String codUsuarioAccion);
	
	public List<MaterialxIngresoBean> listaBloqueLibrosPrestados(String codBloque);		
	public String actualizarPrestamoBloque(String cadenaDatos,String nroReg,String usuarioPrestamo,String usuarioSistema);	
}
