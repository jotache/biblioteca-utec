package com.tecsup.SGA.service.biblioteca.Impl;

import java.util.*;

import com.tecsup.SGA.DAO.biblioteca.BiblioReportesDAO;
import com.tecsup.SGA.service.biblioteca.BiblioReportesManager;

public class BiblioReportesManagerImpl implements BiblioReportesManager{
 
	BiblioReportesDAO biblioReportesDAO;
	
	public void setBiblioReportesDAO(BiblioReportesDAO biblioReportesDAO) {
		this.biblioReportesDAO = biblioReportesDAO;
	}

	public List getReporteBuzonSugerencia (String periodo, String codTipoMaterial)
	{	
		try {
			return biblioReportesDAO.getReporteBuzonSugerencia(periodo, codTipoMaterial);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteCatalogoFichas(String sede){
		try {
			return biblioReportesDAO.getReporteCatalogoFichas(sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteMatFaltante(String fecIni, String fecFin,String sede){
		try {
			return biblioReportesDAO.getReporteMatFaltante(fecIni, fecFin,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteMatAdquiridos(String fecIni, String fecFin, String tipoCambio,String sede, String procedencia){
		try {
			return biblioReportesDAO.getReporteMatAdquiridos(fecIni, fecFin, tipoCambio,sede, procedencia);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteMatRetirados(String fecIni, String fecFin,String sede){
		try {
			return biblioReportesDAO.getReporteMatRetirados(fecIni, fecFin,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteMatMasSolicitado(String fecIni, String fecFin, String nroMax,String sede){
		try {
			return biblioReportesDAO.getReporteMatMasSolicitado(fecIni, fecFin, nroMax,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteMovPrestDevDiaHra(String fecIni, String fecFin,String sede){
		try {
			return biblioReportesDAO.getReporteMovPrestDevDiaHra(fecIni, fecFin,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteResumenMovDia(String fecIni, String fecFin,String sede){
		try {
			return biblioReportesDAO.getReporteResumenMovDia(fecIni, fecFin,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteUsuariosSancion(String periodo1,String periodo2, String tipo,String sede,String tipoUsuario){
		try {
			return biblioReportesDAO.getReporteUsuariosSancion(periodo1,periodo2, tipo,sede,tipoUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteEstadTipoUsuario(String fecIni, String fecFin,String sede){
		try {
			return biblioReportesDAO.getReporteEstadTipoUsuario(fecIni, fecFin,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;	
	}
	
	public List getReporteEstadCicloEsp(String fecIni, String fecFin,String sede){
		try {
			return biblioReportesDAO.getReporteEstadCicloEsp(fecIni, fecFin, sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public List getReporteUsuarioIndividual(String codUsuario, String username,String sede) {
		try {
			return biblioReportesDAO.getReporteUsuarioIndividual(codUsuario, username,sede);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public List getReporteReservaCicloEsp(String fecIni, String fecFin,String sede) {
		try {
			return biblioReportesDAO.getReporteReservaCicloEsp(fecIni, fecFin,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public List getReporteResumenResDia(String fecIni, String fecFin,String sede) {
		try {
			return biblioReportesDAO.getReporteResumenResDia(fecIni, fecFin,sede);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReporteEtiquetas(String fecIni, String fecFin,String sede,String tipoRep, String nroIngresoIni, String nroIngresoFin) {
		try {
			return biblioReportesDAO.getReporteEtiquetas(fecIni, fecFin,sede, tipoRep, nroIngresoIni, nroIngresoFin);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
}
