package com.tecsup.SGA.service.biblioteca.Impl;

import java.util.List;

import org.jfree.util.Log;

import com.tecsup.SGA.DAO.biblioteca.PortalWebBibliotecaDAO;
import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.modelo.Alumno;
import com.tecsup.SGA.modelo.Busqueda;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.PortalWebBibliotecaManager;

public class PortalWebBibliotecaManagerImpl implements PortalWebBibliotecaManager{

	PortalWebBibliotecaDAO portalWebBibliotecaDAO;
	
	public PortalWebBibliotecaDAO getPortalWebBibliotecaDAO() {
		return portalWebBibliotecaDAO;
	}

	public void setPortalWebBibliotecaDAO(
			PortalWebBibliotecaDAO portalWebBibliotecaDAO) {
		this.portalWebBibliotecaDAO = portalWebBibliotecaDAO;
	}

	public List getMaximaCapacidadSalas(Sala obj)
	{	
		try
		{   List we= portalWebBibliotecaDAO.getMaximaCapacidadSalas(obj.getCodTipoSala());
			
			return we;//obj.getDscValor3():codigo tabala padre, obj.getTipo():tipo de orden
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReservaSalas(Sala obj)
	{	
		try		
		{   List we= portalWebBibliotecaDAO.getReservaSalas(obj.getCodTipoSala(),obj.getNombreSala(), obj.getFechaInicio(),
				obj.getFechaFin(), obj.getHoraIni(), obj.getHoraFin(), obj.getFlagDisponibilidad(), obj.getCodUsuario(),obj.getEstadoPendiente(),obj.getEstadoReservado(),obj.getEstadoEjecutado(),obj.getSede());
			//,String estadoPendiente,String estadoReservado,String estadoEjecutado
			return we;//obj.getDscValor3():codigo tabala padre, obj.getTipo():tipo de orden
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertReservaSalas(Sala obj) {
		try
		{   	return this.portalWebBibliotecaDAO.insertReservaSalas(obj.getCodTipoSala(),obj.getFechaReserva(), 
				obj.getHoraIni(), obj.getHoraFin(), obj.getUsuReserva(), obj.getUsuCrea());
				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	public String DeleteReservaSalas(String cadena, String nroRegistros, String usuCrea) {
		try
		{   	return this.portalWebBibliotecaDAO.DeleteReservaSalas(cadena, nroRegistros, usuCrea);
				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	public List GetSeccionesPortalWeb(Secciones obj, TipoTablaDetalle obj2)
	{	
		try
		{   List we= portalWebBibliotecaDAO.GetSeccionesPortalWeb(obj.getCodTipo(), obj2.getCodTipoTablaDetalle());
			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetBusquedaSimple(Busqueda obj)
	{	
		try
		{   List we= portalWebBibliotecaDAO.GetBusquedaSimple(obj.getTipoMaterial(), obj.getTexto()
				, obj.getBuscarBy(), obj.getOrdenarBy(),obj.getPrestaSala(), obj.getPrestaDomicilio(),obj.getCodSede());
			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertReservaByMaterial(String codTipoMaterial, String fechaReserva, String usuCre) {
		try
		{   	return this.portalWebBibliotecaDAO.InsertReservaByMaterial(codTipoMaterial, fechaReserva, 
				usuCre);
				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	/*public List GetBusquedaAvanzada(String codTipoMaterial, String codTipoIdioma, String codAnioInicio, 
			String codAnioFin, String txtDisponibiblidadSala, String txtDisponibilidadCasa , String buscar1,
			String txtTexto1, String condicion1, String buscar2, String txtTexto2, String condicion2,
			String buscar3, String txtTexto3, String ordenarBy)*/
	
	public List GetBusquedaAvanzada(Busqueda obj)
	{	
		try
		{   List we=portalWebBibliotecaDAO.GetBusquedaAvanzada(obj.getTipoMaterial(), obj.getCodTipoIdioma(),
				obj.getCodAnioInicio(), obj.getCodAnioFin(), obj.getFlagDisponibilidadSala(), obj.getFlagDisponibilidadDomicilio(),
				obj.getBuscar1(), obj.getTxtTexto1(), obj.getCondicion1(), obj.getBuscar2(), obj.getTxtTexto2(),
				obj.getCondicion2(), obj.getBuscar3(), obj.getTxtTexto3(), obj.getOrderBy(),obj.getCodSede());
				//buscar2, txtTexto2, condicion2, buscar3, txtTexto3, ordenarBy);
			//System.out.println("GetBusquedaSimple "+we.size());
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String AplicarReservaSalas(String codUsuarioReserva, 
			String codSala, String codReserva, String codUsuario, String estado){
		try{
			return this.portalWebBibliotecaDAO.AplicarReservaSalas(codUsuarioReserva, 
					codSala, codReserva, codUsuario, estado);			
				
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	//public List GetVerificarReservaUsuario(String codUsuario)
	public String GetVerificarReservaUsuario(String codUsuario)
	{	
		try
		{   //List we=portalWebBibliotecaDAO.GetVerificarReservaUsuario(codUsuario);
			return portalWebBibliotecaDAO.GetVerificarReservaUsuario(codUsuario);
			//return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public Sala getReservaSalaById(String codRecurso, String codSequencia) {
		try {
			return this.portalWebBibliotecaDAO.getReservaSalaById(codRecurso, codSequencia);
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public String terminarReservaSalas(String codRecurso, String codSecuencia,String usuario) {
		try
		{   	return this.portalWebBibliotecaDAO.terminarReservaSalas(codRecurso, codSecuencia, usuario);
			
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String sedeUsuario(String username) {
		try {   
			return this.portalWebBibliotecaDAO.sedeUsuario(username);			
		}
		catch(Exception ex){ex.printStackTrace();}		
		return null;
	}

	public List<Alumno> getUsuarios(String apellPaterno, String apellMaterno, String nombre1, String usuario) {
		try
		{
			return this.portalWebBibliotecaDAO.getUsuarios(apellPaterno, apellMaterno, nombre1, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List<Sala> ListarHorasAtencionPorSede(String sede) {		
		try{
			return this.portalWebBibliotecaDAO.ListarHorasAtencionPorSede(sede);
		}catch (Exception ex){
			ex.printStackTrace();
		}
		return null;		
	}

	public List<Sala> ListarRecursosAtencion(String fecReserva, String sede, String tipoSala) {
		try{
			return this.portalWebBibliotecaDAO.ListarRecursosAtencion(fecReserva, sede, tipoSala);
		}catch (Exception ex){
			ex.printStackTrace();			
		}
		return null;
	}

	public AlumnoXCodigoBean obtenerUsuarioAtencion(String usuario) {
		try {
			return this.portalWebBibliotecaDAO.obtenerUsuarioAtencion(usuario);
		}catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public String insertarAtencion(String codRecurso,String codUsuarioAtencion,String fecha, String horaIni,String horaFin,String codUsuarioActualiza,String sede) {
		try{
		   	return this.portalWebBibliotecaDAO.insertarAtencion(codRecurso, codUsuarioAtencion, fecha, horaIni, horaFin,codUsuarioActualiza,sede);
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public List<Sala> listarAtenciones(String fecha, String sede) {
		try{
			return this.portalWebBibliotecaDAO.listarAtenciones(fecha, sede);
		}catch (Exception ex){
			ex.printStackTrace();			
		}
		return null;
	}

	public List<Sala> listarAtencionesPorPC(String fecha, String sede,
			String codPC) {
		try{
			return this.portalWebBibliotecaDAO.listarAtencionesPorPC(fecha, sede, codPC);
		}catch (Exception ex){
			ex.printStackTrace();			
		}
		return null;
	}

	public String actualizarAtencion(String codAtencion, String tipoOperacion,
			String codUsuarioActualiza) {
		try{
		   	return this.portalWebBibliotecaDAO.actualizarAtencion(codAtencion, tipoOperacion, codUsuarioActualiza);
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public List<Sala> listarAtencionesPorPCHistorico(String sede, String codPC) {
		try{
			return this.portalWebBibliotecaDAO.listarAtencionesPorPCHistorico(sede, codPC);
		}catch (Exception ex){
			ex.printStackTrace();			
		}
		return null;
	}
}
