package com.tecsup.SGA.service.biblioteca.Impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.tecsup.SGA.DAO.biblioteca.BiblioGestionMaterialDAO;
import com.tecsup.SGA.DAO.biblioteca.BiblioMantenimientoDAO;
import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.bean.BibliotecaMaterialDetalleBean;
import com.tecsup.SGA.bean.DetalleMaterialFichaBibliograficaBean;
import com.tecsup.SGA.bean.HistorialProrrogaBean;
import com.tecsup.SGA.bean.IngresosxMaterialBean;
import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.biblioteca.BiblioGestionMaterialManager;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialCommand;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialRegistrarIngresoCommand;
import com.tecsup.SGA.web.biblioteca.controller.AdminRegistrarMaterialFormController;



public class BiblioGestionMaterialManagerImpl implements BiblioGestionMaterialManager{
	private static Log log = LogFactory.getLog(BiblioGestionMaterialManagerImpl.class);
	
	BiblioGestionMaterialDAO biblioGestionMaterialDAO;
	TransactionTemplate transactionTemplate;
	
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}

	public void setBiblioGestionMaterialDAO(
			BiblioGestionMaterialDAO biblioGestionMaterialDAO) {
		this.biblioGestionMaterialDAO = biblioGestionMaterialDAO;
	}

	public List getAllMaterialesxFiltro(String txhCodigoUnico,String txtCodigo, String txtNroIngreso,
			String cboTipoMaterial, String cboBuscarPor, String txtTitulo,
			String cboIdioma, String cboAnioIni, String cboAnioFin,
			String txtFechaReservaIni, String txtFechaReservaFin,String sede) {

		try
		{   List we= biblioGestionMaterialDAO.getAllMaterialesxFiltro( txhCodigoUnico,txtCodigo,  txtNroIngreso,
				 cboTipoMaterial,  cboBuscarPor,  txtTitulo,
				 cboIdioma,  cboAnioIni,  cboAnioFin,
				 txtFechaReservaIni,  txtFechaReservaFin,sede);			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
		
	}

	public AdminRegistrarMaterialCommand getAllMaterialDetalle(
			AdminRegistrarMaterialCommand control) {
		
		List lstResultado = biblioGestionMaterialDAO.getAllMaterialDetalle(control.getTxhCodUnico());
		BibliotecaMaterialDetalleBean bean = null;
		
		if(lstResultado!=null && lstResultado.size()>0)
		{
			bean = (BibliotecaMaterialDetalleBean) lstResultado.get(0);
			control.setCboTipoMaterial(bean.getCodTipoMaterial());
			//control.setTxtCodigo(bean.getCodUnico());
			control.setTxtTitulo(bean.getTitulo());
			control.setTxtAutor(bean.getDesAutor());
			control.setTxhCodigoAutor(bean.getCodAutor());
			control.setTxtDewey(bean.getDesDewey());
			control.setTxhCodigoDewey(bean.getCodDewey());//cambio
			control.setTxhCodGeneradoDewey(bean.getCodGeneradoDewey());
			control.setTxtNroPag(bean.getNroPaginas());
			control.setTxtNroVolumen(bean.getNroVolumen());
			control.setTxtTotalVolumen(bean.getNroTotalVolumenes());
			control.setCboEditorial(bean.getCodEditorial());
			control.setTxtNroEdicion(bean.getNroEditorial());
			control.setCboPais(bean.getCodPais());
			control.setCboCiudad(bean.getCodCiudad());
			control.setTxtFecPublicacion(bean.getFecPublicacion());
			control.setTxtUrlArchivoDig(bean.getUrlArchDigital());
			control.setCboIdioma(bean.getCodIdioma());
			control.setTxtIsbn(bean.getIsbn());
			control.setTxtFormato(bean.getFormato());
			control.setCboTipoVideo(bean.getCodTipoVideo());
			//control.setCboProcedencia(bean.get);
			//control.setCboPrecio(bean.get)			
			control.setChkTipoPrestamoSala(bean.getIndPrestSala());
			control.setChkTipoPrestamoDomicilio(bean.getIndPrestDomicilio());
			control.setRdoAplicaReserva(bean.getIndReserva());
			control.setTxtUbicacionFisica(bean.getUbicacionFisica());
			control.setTxtCodigo(bean.getCodGenerado());
			control.setTxhDesEditorial(bean.getDesEditorial());
			control.setTxhDesIdioma(bean.getDesIdioma());
			control.setTxhDesPais(bean.getDesPais());
			control.setTxhDesCiudad(bean.getDesCiudad());			
			control.setNomImagen(bean.getImagen());
			
			control.setCboSede(bean.getCodSede());
			control.setTxtPrefijo(bean.getPrefijo());
			control.setTxtSufijo(bean.getSufijo());
			control.setTxtIdDewey(control.getTxtCodigo());
			control.setTxtFrecuencia(bean.getFrecuencia());
			control.setMencionSerie(bean.getMencionSerie());
			control.setTxtContenido(bean.getContenido());

			
			if(bean.getImagen()!=null)			
				control.setPathImagen(CommonConstants.GSTR_HTTP+CommonConstants.SERVIDOR_PUERTO_WEB+CommonConstants.DIRECTORIO_PROY+CommonConstants.DIRECTORIO_IMAGENES+bean.getImagen());
			else
				control.setPathImagen(null);
						
		}
			
		return control;
	}

	public List getAllIngresosxMaterial(String txhCodMaterial,String txhCodUnico) {
		try
		{   List we= biblioGestionMaterialDAO.getAllIngresosxMaterial(txhCodMaterial,txhCodUnico);			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllDescriptorxMaterial(String txhCodUnico) {
		try
		{   List we= biblioGestionMaterialDAO.getAllDescriptorxMaterial(txhCodUnico);			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllArchAdjuntoxMaterial(String txhCodUnico) {
		try
		{   List we= biblioGestionMaterialDAO.getAllArchAdjuntoxMaterial(txhCodUnico);			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String InsertArchivoAdjuntoxMaterial(String txhCodigoUnico,
			String txtTitulo, String nomArchivo, String usuario) {
		try
		{  					
			return this.biblioGestionMaterialDAO.InsertArchivoAdjuntoxMaterial(
					 txhCodigoUnico,
					 txtTitulo,
					 nomArchivo,
					 usuario);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String deleteArchAdjuntoxMaterial(String txhCodigoUnico,
			String usuario) {
		try
		{  					
			return this.biblioGestionMaterialDAO.deleteArchAdjuntoxMaterial(
					 txhCodigoUnico,usuario);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String insertDescriptorxMaterial(String codMaterial, String cadena,
			String nroRegistros, String usuario) {
		try
		{  					
			return this.biblioGestionMaterialDAO.insertDescriptorxMaterial(
					 codMaterial,  cadena,
					 nroRegistros,  usuario);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String deleteDescriptorxMaterial(String txhCodigoUnico,
			String usuario) {
		try
		{  					
			return this.biblioGestionMaterialDAO.deleteDescriptorxMaterial(
					txhCodigoUnico,  usuario);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String insertIngresosxMaterial(String txhCodigoUnico,
			String txtFecIngreso, String txtFecBaja, String cboProcedencia,
			String cboMoneda, String txtPrecio, String cboEstado,
			String txtObservacion, String usuario) {
		try
		{  					
			return this.biblioGestionMaterialDAO.insertIngresosxMaterial( txhCodigoUnico,
					 txtFecIngreso,  txtFecBaja,  cboProcedencia,
					 cboMoneda,  txtPrecio,  cboEstado,
					 txtObservacion,  usuario);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	
	public String updateIngresosxMaterial(String codUnico, String fechaIngreso, String fechaBaja,
			String codProcedencia, String codMoneda, String precio, String codEstado, 
			String observaciones, String usuario) {
		try
		{  					
			return this.biblioGestionMaterialDAO.updateIngresosxMaterial(codUnico, fechaIngreso, fechaBaja,
					codProcedencia, codMoneda, precio, codEstado, observaciones, usuario);			
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	public String deleteIngresosxMaterial(String codUnico, String usuario) {
		try
		{  					
			return this.biblioGestionMaterialDAO.deleteIngresosxMaterial(codUnico, usuario);			
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public AdminRegistrarMaterialRegistrarIngresoCommand getIngresoxMaterial(
			AdminRegistrarMaterialRegistrarIngresoCommand control) {
		List lstMaterial =  this.biblioGestionMaterialDAO.getAllIngresosxMaterial(control.getTxhCodigoMaterial(),control.getTxhCodigoUnico());
		if(lstMaterial!=null && lstMaterial.size()>0)
		{
			IngresosxMaterialBean bean = (IngresosxMaterialBean)lstMaterial.get(0);
			control.setTxtIngreso(bean.getNroIngreso());
			control.setTxtFecIngreso(bean.getFecIngreso());
			control.setNroVolumen(bean.getNroVolumen());
			control.setCboProcedencia(bean.getCodPrecedencia());
			control.setCboMoneda(bean.getCodMoneda());
			control.setTxtPrecio(bean.getNroPrecio());
			control.setCboEstado(bean.getEstado());
			control.setTxtObservacion(bean.getObservacion());
			
		}	
		return control;
	}

	public String insertMaterial(String tipMaterial, String titulo,
			String codAutor, String codDewey, String nroPaginas,
			String codEditorial, String nroEditorial, String codPais,
			String codCiudad, String fecPublicacion, String urlArchDigital,
			String codIdioma, String isbn, String formato, String codTipoVideo,
			String codProcedencia, String codMoneda, String precio,
			String indPrestamoSala, String indPrestamoCasa, String indReserva,
			String observaciones, String imagen, String usuario,String ubicacionFisica,String codSede
			,String codGenerado,String prefijo,String sufijo,String frecuencia,String mencionSerie, String contenido) {
		try
		{  					
			return this.biblioGestionMaterialDAO.insertMaterial( tipMaterial,  titulo,
					 codAutor,  codDewey,  nroPaginas,
					 codEditorial,  nroEditorial,  codPais,
					 codCiudad,  fecPublicacion,  urlArchDigital,
					 codIdioma,  isbn,  formato,  codTipoVideo,
					 codProcedencia,  codMoneda,  precio,
					 indPrestamoSala,  indPrestamoCasa,  indReserva,
					 observaciones,  imagen,  usuario,ubicacionFisica,codSede
					 , codGenerado, prefijo, sufijo, frecuencia, mencionSerie, contenido);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String updateMaterial(String codMaterial, String tipMaterial,
			String titulo, String codAutor, String codDewey, String nroPaginas,
			String codEditorial, String nroEditorial, String codPais,
			String codCiudad, String fecPublicacion, String urlArchDigital,
			String codIdioma, String isbn, String formato, String codTipoVideo,
			String indPrestamoSala, String indPrestamoCasa, String indReserva,
			String imagen, String usuario,String ubicacionFisica,String codSede
			,String codGenerado,String prefijo,String sufijo,String frecuencia,String mencionSerie,String contenido) {
		try
		{  					
			return this.biblioGestionMaterialDAO.updateMaterial( codMaterial,  tipMaterial,
					 titulo,  codAutor,  codDewey,  nroPaginas,
					 codEditorial,  nroEditorial,  codPais,
					 codCiudad,  fecPublicacion,  urlArchDigital,
					 codIdioma,  isbn,  formato,  codTipoVideo,
					 indPrestamoSala,  indPrestamoCasa,  indReserva,
					 imagen,  usuario, ubicacionFisica,codSede
					 , codGenerado, prefijo, sufijo, frecuencia, mencionSerie, contenido);			
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	public String deleteMaterial(String codMaterial, String usuario) {
		try
		{
			return this.biblioGestionMaterialDAO.deleteMaterial(codMaterial,usuario);			
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}	

	public List getAllHistorialPrestamoxMaterial(String txhCodUnico) {
		try
		{   List we= biblioGestionMaterialDAO.getAllHistorialPrestamoxMaterial(txhCodUnico);			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllHistorialProrrogaxMaterial(String txhNroIngreso, String txhCodUsuario,String sede) {
		try
		{   List prorrogas = new ArrayList();
			List we= biblioGestionMaterialDAO.getAllHistorialProrrogaxMaterial(txhNroIngreso, txhCodUsuario,sede);			
			Iterator i = we.iterator();
			while (i.hasNext()) {
				HistorialProrrogaBean bean = (HistorialProrrogaBean) i.next();
				//if(!"PENDIENTE".equals(bean.getFecDevolucion()))
				prorrogas.add(bean);
				log.info("Prestamo: "+bean);
				if(!"1".equals(bean.getFlagProrroga())) break;
			}
			return prorrogas;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public ServiciosUsuarios getAllServicioByCodUsuario(String codUsuario) {
		try {
			return biblioGestionMaterialDAO.getAllServicioByCodUsuario(codUsuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public boolean getPermisoParaProrroga(String codUsuario, Integer totalProrrogas) {
		try {
			ServiciosUsuarios su = biblioGestionMaterialDAO.getAllServicioByCodUsuario(codUsuario);
			log.info("getPermisoParaProrroga: Total:"+totalProrrogas+" - Limite:"+su.getNroProrrogas());
			return !(Integer.parseInt(su.getNroProrrogas()) <= totalProrrogas);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}
	
	public AlumnoXCodigoBean getAlumnoXCodigo(String codigo,String indTipoPrestamo,String tipoBusquedaCodUsu,String sede,String numIngreso){
		try
		{   List we= biblioGestionMaterialDAO.getAlumnoXCodigo(codigo,indTipoPrestamo,tipoBusquedaCodUsu,sede,numIngreso);
			if(we!=null && we.size()>0)
				return (AlumnoXCodigoBean)we.get(0);
			else 
				return null;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}		
		return null;
	}

	public MaterialxIngresoBean getMaterialxIngreso(String codigo,String tipo,String sede,String codUsuario) {
		try
		{   List we= biblioGestionMaterialDAO.getMaterialxIngreso(codigo,tipo,sede,codUsuario);
			if(we!=null && we.size()>0)
				return (MaterialxIngresoBean)we.get(0);
			else 
				return null;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}		
		return null;
	}

	public String insertDevolucion(String codPrestamo, String usuPrestamo,
			String flgSancion, String usuCreacion) {
		try
		{  					
			return this.biblioGestionMaterialDAO.insertDevolucion( codPrestamo,  usuPrestamo,
					 flgSancion,  usuCreacion);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public Object insertPrestamoxAlumno(final String codMaterial,final String nroIngreso,
			final String codUsuPrestamo,final String indProrroga, final String usuCreacion,final String indPrestamo, final String indReserva,
			final String fecPrestamo, final Long codMultiple) {
		 	
		log.info("insertPrestamoxAlumno:inicio");
		return this.transactionTemplate.execute(new TransactionCallback(){

			
			public Object doInTransaction(TransactionStatus ts) {
				try{						
					return biblioGestionMaterialDAO.insertPrestamoxAlumno( codMaterial,  nroIngreso,
							 codUsuPrestamo,  indProrroga,  usuCreacion,indPrestamo, indReserva, 
								 fecPrestamo, codMultiple);
				}catch(Exception e){
					ts.setRollbackOnly();
					log.error(e.getMessage());
				}
				log.info("insertPrestamoxAlumno:fin");
				return null;
			}
			
		});
						
	}

	public List getAllDetalleMaterialFichaBibliografica(
			String codUnico) {
		
		List lstResultado = this.biblioGestionMaterialDAO.
		getAllDetalleMaterialFichaBibliografica(codUnico);
					
		return lstResultado;
	}
	
	public List GetAllReservaByAlumno(String codUsuario,String nroIngreso){
		try{
			return this.biblioGestionMaterialDAO.GetAllReservaByAlumno(codUsuario,nroIngreso);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}

	public List getAllReservaByMaterial(String codMaterial){
		try{
			return this.biblioGestionMaterialDAO.getAllReservaByMaterial(codMaterial);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}
	
	public String deleteReservasMaterial(String cadena, String usuCrea){
		int numReg;
		try
		{  
			StringTokenizer stDatos = new StringTokenizer(cadena, "|");
			numReg = stDatos.countTokens();
			
			return this.biblioGestionMaterialDAO.deleteReservasMaterial(cadena, Integer.toString(numReg), usuCrea);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public List getAllDatosUsuarioBib(String codUsuario) {
		try{
			return this.biblioGestionMaterialDAO.getAllDatosUsuarioBib(codUsuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}

	//jhpr 2008-08-15
	public List getUsuariosXCodigo(String codigo, String tipo) {
		try {
			return this.biblioGestionMaterialDAO.getUsuariosXCodigo(codigo, tipo);
		} catch (Exception ex){ex.printStackTrace();}
		return null;
	}

	public List<SedeBean> GetSedesBibPorUsuario(String codUsuario) {
		try
		{
			return this.biblioGestionMaterialDAO.GetSedesBibPorUsuario(codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public int getCantISBNPorSede(String isbn, String sede,Long codMaterial) {
		return this.biblioGestionMaterialDAO.getCantISBNPorSede(isbn, sede,codMaterial);
	}

	public String getEstadoIngreso(String codIngreso) {
		return this.biblioGestionMaterialDAO.getEstadoIngreso(codIngreso);
	}

	public boolean categoriaDuplicada(String codMaterial, String codGenerado) {
		try {
			return this.biblioGestionMaterialDAO.categoriaDuplicada(codMaterial, codGenerado);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}

	public Long obtenerCodigoPrestamoMultiple() {
		return this.biblioGestionMaterialDAO.obtenerCodigoPrestamoMultiple();
	}

	public List<MaterialxIngresoBean> getListaLibrosDeudaXUsuario(
			String codUsuario) {
		try
		{
			return this.biblioGestionMaterialDAO.getListaLibrosDeudaXUsuario(codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<MaterialxIngresoBean> getListaLibrosPrestadosXUsuario(
			String codUsuario) {
		try
		{
			return this.biblioGestionMaterialDAO.getListaLibrosPrestadosXUsuario(codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
}
