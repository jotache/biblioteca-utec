package com.tecsup.SGA.service.biblioteca.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.biblioteca.AdminBuzonSugerenciasDAO;
import com.tecsup.SGA.modelo.BuzonSugerencia;
import com.tecsup.SGA.service.biblioteca.AdminBuzonSugerenciasManager;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;

public class AdminBuzonSugerenciasManagerImpl implements AdminBuzonSugerenciasManager{
 
	AdminBuzonSugerenciasDAO adminBuzonSugerenciasDAO;
	
	public List GetAllSugerencias(BuzonSugerencia obj)
	{	
		try
		{    
			List we= adminBuzonSugerenciasDAO.GetAllSugerencias(obj.getMes(), obj.getAnio(), 
				obj.getCodTipoSugerencia(), obj.getEstadoSugerencia());
			
			
			return we;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String UpdateAtenderBuzonSugerencia(BuzonSugerencia obj) {
		try
		{   
					
			return this.adminBuzonSugerenciasDAO.UpdateAtenderBuzonSugerencia(obj.getCodSugerencia(),
					obj.getCodCalificacion(), obj.getCodigoMaterial(), obj.getRespuesta());
							
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	public String InsertSugerencia(BuzonSugerencia obj){
		try{
			return this.adminBuzonSugerenciasDAO.InsertSugerencia(obj.getCodTipoSugerencia(), obj.getDescripcion(), obj.getEmail(), obj.getUsuario());
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public AdminBuzonSugerenciasDAO getAdminBuzonSugerenciasDAO() {
		return adminBuzonSugerenciasDAO;
	}

	public void setAdminBuzonSugerenciasDAO(
			AdminBuzonSugerenciasDAO adminBuzonSugerenciasDAO) {
		this.adminBuzonSugerenciasDAO = adminBuzonSugerenciasDAO;
	}

	public BuzonSugerencia obtenerSugerencia(String id) {
		// //JHPR 2008-7-30
		return this.adminBuzonSugerenciasDAO.obtenerSugerencia(id);
	}
}
