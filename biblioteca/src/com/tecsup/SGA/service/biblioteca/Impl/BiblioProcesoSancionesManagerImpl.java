package com.tecsup.SGA.service.biblioteca.Impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.biblioteca.BiblioProcesosSancionesDAO;

import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Sancion;
import com.tecsup.SGA.service.biblioteca.BiblioProcesoSancionesManager;




public class BiblioProcesoSancionesManagerImpl implements BiblioProcesoSancionesManager{
	private static Log log = LogFactory.getLog(BiblioProcesoSancionesManagerImpl.class);
	
	BiblioProcesosSancionesDAO biblioProcesosSancionesDAO;
	
	public void setBiblioProcesosSancionesDAO(
			BiblioProcesosSancionesDAO biblioProcesosSancionesDAO) {
		this.biblioProcesosSancionesDAO = biblioProcesosSancionesDAO;
	}

	public String insertSancion(String nroIngreso,String fecDevolucion, String usuario,String sede, String guardaSancion) {
		try
		{  
			
			return this.biblioProcesosSancionesDAO.insertSancion(
					 nroIngreso,fecDevolucion,usuario,sede, guardaSancion);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String insertReservaMaterial(String codMaterial, String fecReserva,
			String codUsuarioReserva, String codUsuario) {
		try
		{  
			
			return this.biblioProcesosSancionesDAO.insertReservaMaterial(codMaterial, fecReserva, codUsuarioReserva, codUsuario);	
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String insertSancionReservaSalas(String codUsuario,
			String recCodigo, String renSecuencia, String usuario,
			String estadoReserva) {
		try
		{  
			
			return this.biblioProcesosSancionesDAO.insertSancionReservaSalas(codUsuario, recCodigo, renSecuencia, usuario, estadoReserva);	
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public List<Sancion> getSancionesUsuario(String codUsuario) {
		try
		{
			return this.biblioProcesosSancionesDAO.getSancionesUsuario(codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String updateSancionUsuario(String codUsuario, String idSancion,String codUsuarioAccion) {
		try {
			return this.biblioProcesosSancionesDAO.updateSancionUsuario(codUsuario, idSancion, codUsuarioAccion);
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public String insertarInhabilitaciones(String codUsuario,
			String cadSansiones, String nroReg, String fecIni, String fecFin,String obsInhabi,
			String codUsuarioAccion) {
		try {
			return this.biblioProcesosSancionesDAO.insertarInhabilitaciones(codUsuario, cadSansiones, nroReg, fecIni, fecFin, obsInhabi, codUsuarioAccion);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public List<MaterialxIngresoBean> listaBloqueLibrosPrestados(
			String codBloque) {
		try{
			return this.biblioProcesosSancionesDAO.listaBloqueLibrosPrestados(codBloque);
		}catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public List<Sancion> getSancionesUsuarioPorPrestamoMaterial(
			String codUsuario) {
		List<Sancion> lstResult = null;
		try
		{
			List<Sancion> lista = biblioProcesosSancionesDAO.getSancionesUsuario(codUsuario);
			lstResult = new ArrayList<Sancion>();
			for(Sancion s : lista){
				if(s.getTipoSancion().equals(CommonConstants.COD_TIP_SANCION_X_PRESTAMO_MAT)){
					lstResult.add(s);
					log.info("Sancion:" + s);
				}
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return lstResult;
	}

	public String actualizarPrestamoBloque(String cadenaDatos, String nroReg,
			String usuarioPrestamo, String usuarioSistema) {
		try
		{  
			
			return this.biblioProcesosSancionesDAO
						.actualizarPrestamoBloque(cadenaDatos, nroReg, usuarioPrestamo, usuarioSistema);			
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	

	
}
