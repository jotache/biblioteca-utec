package com.tecsup.SGA.service.biblioteca.Impl;

import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;

import com.tecsup.SGA.DAO.TablaDetalleDAO;
import com.tecsup.SGA.DAO.biblioteca.BiblioMantenimientoDAO;
import com.tecsup.SGA.modelo.BuzonSugerencia;
import com.tecsup.SGA.bean.OpcionesApoyo;
import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.AccesoIP;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Examen;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.ParametrosReservados;
import com.tecsup.SGA.modelo.Sala;
import com.tecsup.SGA.modelo.Sancion;
import com.tecsup.SGA.modelo.Secciones;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.UsuarioPM;
import com.tecsup.SGA.service.biblioteca.BiblioMantenimientoManager;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;

public class BiblioMantenimientoManagerImpl implements BiblioMantenimientoManager{

    BiblioMantenimientoDAO biblioMantenimientoDAO;
    TablaDetalleDAO tablaDetalleDAO;

    public TablaDetalleDAO getTablaDetalleDAO() {
        return tablaDetalleDAO;
    }

    public void setTablaDetalleDAO(TablaDetalleDAO tablaDetalleDAO) {
        this.tablaDetalleDAO = tablaDetalleDAO;
    }

    public List getAllTablaDetalleBiblioteca(TipoTablaDetalle obj)
    {
        try
        {   List we= biblioMantenimientoDAO.getAllTablaDetalleBiblioteca(obj.getDscValor3(), obj.getCodTipoTabla(),
                obj.getCodTipoTablaDetalle(),obj.getCodDetalle(), obj.getDescripcion(), obj.getTipo(),obj.getDscValor4(),obj.getDscValor5(),obj.getDscValor6(),obj.getTipoLista());

            return we;//obj.getDscValor3():codigo tabala padre, obj.getTipo():tipo de orden
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    //String valor3, String valor4 , String valor5
    public String InsertTablaDetalleBiblioteca(TipoTablaDetalle obj) {
        try
        {       return this.biblioMantenimientoDAO.insertTablaDetalleBiblioteca(obj.getCodTipoTabla(),
                obj.getCodTipoTablaDetalle(), obj.getDescripcion(), obj.getUsuario(), obj.getDscValor3(),
                obj.getDscValor4(), obj.getDscValor5(),obj.getDscValor6());

        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }

    public String UpdateAndDeleteTablaDetalleBiblioteca(TipoTablaDetalle obj) {
        try
        {      return this.biblioMantenimientoDAO.UpdateAndDeleteTablaDetalleBiblioteca(obj.getCodTipoTabla(),
            obj.getCodTipoTablaDetalle(), obj.getDscValor1(), obj.getDscValor2(), obj.getDescripcion(),
            obj.getEstReg(), obj.getUsuario(), obj.getDscValor3(), obj.getDscValor4(), obj.getDscValor5(),obj.getDscValor6());

        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }
    //public List getAllPaises(String codigo, String descripcion, String tiopoOrden)

    public List getAllPaises(Pais obj, String tipoOrden)
    {
        try
        {   List we= biblioMantenimientoDAO.getAllPaises(obj.getPaisCodigo(),
                obj.getPaisDescripcion(), tipoOrden);
            return we;//obj.getDscValor3():codigo tabala padre, obj.getTipo():tipo de orden
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    //public String insertPaises(    String descripcion, String usuCrea)

    public String InsertPaises(Pais obj, String usuario) {
        try
        {

            return this.biblioMantenimientoDAO.insertPaises(obj.getPaisDescripcion(), usuario);

        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }
    //public List getCiudadByPais(String codigoPais, String codigoId, String descripcion, String tipoOrden)
    public List getCiudadByPais(Ciudad obj, String tipoOrden)
    {
        try
        {   List we= biblioMantenimientoDAO.getCiudadByPais(obj.getPaisId(), obj.getCiudadCodigo(),
                obj.getCiudadDescripcion(), tipoOrden);

            return we;//obj.getDscValor3():codigo tabala padre, obj.getTipo():tipo de orden
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }//public String insertCiudadByPais(String codigoPais, String descripcion, String usuCrea)

    public String insertCiudadByPais(Ciudad obj) {
        try
        {
            return this.biblioMantenimientoDAO.insertCiudadByPais(obj.getPaisId(), obj.getCiudadDescripcion()
                    , obj.getUsuCrea());

        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }

    public List GetAllFeriados(String codTablaId, String mes, String anio){
    try{
        return this.biblioMantenimientoDAO.GetAllFeriados(codTablaId, mes, anio);
    }
    catch(Exception ex){ex.printStackTrace();}
        return null;
    }//public String updateDeletePais(String paisId, String descripcion, String Flag, String usuCrea)

    public List Sanciones(){
        TipoTablaDetalle obj1 = new TipoTablaDetalle();
        TipoTablaDetalle obj2 = new TipoTablaDetalle();
        List resultado=new ArrayList();
        try{
            List ocurrencias = this.tablaDetalleDAO.getAllTablaDetalle(CommonConstants.TIPT_OCURRENCIAS, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);
            List sanciones = this.tablaDetalleDAO.getAllTablaDetalle(CommonConstants.TIPT_LISTA_OCURRENCIAS, "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_COD);
            int indice=0;
            for(int i=0;i<5;i++){
                Sancion sancion=new Sancion();
                obj1=(TipoTablaDetalle)ocurrencias.get(i);
                sancion.setDscFila(obj1.getDescripcion());
                sancion.setCodFila(obj1.getCodTipoTablaDetalle());
                for(int j=0;j<5;j++){
                    obj2=(TipoTablaDetalle)sanciones.get(indice);
                    sancion.setValorFila(obj2.getDscValor2());
                    switch (j) {
                        case 0:sancion.setValorSancion1(obj2.getDscValor5());break;
                        case 1:sancion.setValorSancion2(obj2.getDscValor5());break;
                        case 2:sancion.setValorSancion3(obj2.getDscValor5());break;
                        case 3:sancion.setValorSancion4(obj2.getDscValor5());break;
                        case 4:sancion.setValorSancion5(obj2.getDscValor5());break;
                    }
                    indice++;
                }               
                resultado.add(sancion);
            }
            return resultado;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public String UpdateSanciones(TipoTablaDetalle obj){
        try{

            return this.biblioMantenimientoDAO.UpdateSanciones(obj.getDscValor1(), obj.getDscValor2(), obj.getUsuario(), obj.getDscValor3());
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }
    public String updateDeletePais(Pais obj) {
        try
        {

            return this.biblioMantenimientoDAO.updateDeletePais(obj.getPaisId(), obj.getPaisDescripcion(),
                    obj.getFlag(), obj.getUsuCrea());

        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }

    public String updateDeleteCiudadbyPais(Ciudad obj) {
        try
        {

            return this.biblioMantenimientoDAO.updateDeleteCiudadbyPais(obj.getPaisId(), obj.getCiudadId(),
                    obj.getCiudadDescripcion(), obj.getEstReg(), obj.getUsuCrea());

        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }

    public String InsertFeriados(String codTablaId, String dis, String mes
            , String anio, String descripcion,String usucrea){
        try{
        return this.biblioMantenimientoDAO.InsertFeriados(codTablaId, dis, mes, anio, descripcion, usucrea);
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    public String ActualizarFeriado(String codTablaId, String id, String dis, String mes
            , String anio, String descripcion,String usucrea, String flag){
        try{
            return this.biblioMantenimientoDAO.ActualizarFeriado(codTablaId, id, dis, mes, anio, descripcion, usucrea, flag);
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }
   

    public String InsertServicioUsuario(String tiptId, String valor1, String valor2,String valor3,
            String valor4, String valor5, String valor6, String valor7, String valor8,
            String valor9,String valor10,String usuCrea,String cadenaDatos, String tamanoDatos){
        try{
            return this.biblioMantenimientoDAO.InsertServicioUsuario(tiptId, valor1, valor2, valor3
                    , valor4, valor5, valor6, valor7, valor8, valor9, valor10, usuCrea,cadenaDatos, tamanoDatos);
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    public ServiciosUsuarios GetAllServicioUsuario(String tipId, String valor1){
        try{
            return this.biblioMantenimientoDAO.GetAllServicioUsuario(tipId, valor1);
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    //public List getSeleccionByFiltros(String codigoUnico,String codSeccion, String codGrupo, String codTipo)
    public List getSeccionesByFiltros(Secciones obj)
    {
        try
        {   List we= biblioMantenimientoDAO.getSeccionesByFiltros(obj.getCodUnico(), obj.getCodSeccion(),
                obj.getCodGrupo(), obj.getCodTipo());
            System.out.println("getSeccionesByFiltros "+we.size());
            return we;//obj.getDscValor3():codigo tabala padre, obj.getTipo():tipo de orden
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    public String UpdateGrupos(String codTabla, String cadena, String nroRegistros, String codUsuario){
        try{
            //System.out.println("UpdateGrupos implemets-------------->");
            return this.biblioMantenimientoDAO.UpdateGrupos(codTabla, cadena, nroRegistros, codUsuario);
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }
    public String insertSecciones(Secciones seccion, String usuario, String tipo){
        try{
            return this.biblioMantenimientoDAO.insertSecciones(seccion.getCodUnico(),
                    seccion.getCodSeccion(), seccion.getTipoMaterial(), seccion.getCodGrupo(),
                    seccion.getTema(), seccion.getDescripcionCorta(), seccion.getDescripcionLarga(),
                    seccion.getUrl(), seccion.getArchivo(), seccion.getFechaPublicacion(), seccion.getFechaVigencia(),
                    seccion.getFlag(), usuario, tipo, seccion.getNomAdjunto());

        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }

    public String InsertParametrosReservados(String codTablaId, String valor1, String valor2
            , String valor3, String valor4,String valor5,String valor6
            ,String valor7,String valor8,String valor9,String usuario){
        try{
            return this.biblioMantenimientoDAO.InsertParametrosReservados(codTablaId
                    , valor1, valor2, valor3, valor4, valor5, valor6, valor7,valor8,valor9, usuario);
        }
        catch(Exception ex){
            ex.printStackTrace();
            }
        return null;
    }

    public ParametrosReservados GetAllParametrosGenerales(String tipId){
        try{
            return this.biblioMantenimientoDAO.GetAllParametrosGenerales(tipId);
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    public List<UsuarioPM> GetAllUsuariosAcceso(){
        try{
            return this.biblioMantenimientoDAO.GetAllUsuariosAcceso();
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    public List<AccesoIP> GetAllAccesosLista(Integer codUsuario){
        try{
            return this.biblioMantenimientoDAO.GetAllAccesosLista(codUsuario);
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    public void InsertAccesoIP(Integer codUsuario, String numIP, String usuCreacion) {

        this.biblioMantenimientoDAO.InsertAccesoIP(codUsuario, numIP, usuCreacion);

    }

    public void DeleteAccesoIP(Integer codUsuario, String numIP) {

        this.biblioMantenimientoDAO.DeleteAccesoIP(codUsuario, numIP);

    }

    public void DeleteAccesosIP(Integer codPersona) {

        this.biblioMantenimientoDAO.DeleteAccesosIP(codPersona);
    }

    public void DeleteOpcionesApoyo(Integer codPersona) {

        this.biblioMantenimientoDAO.DeleteOpcionesApoyo(codPersona);
    }

    public void DeleteRolesApoyo(String codUsuario) {

        this.biblioMantenimientoDAO.DeleteRolesApoyo(codUsuario);
    }

    public String getNombreArchivoSeccion(){
        try{
            return this.biblioMantenimientoDAO.GetNombreArchivoSeccion();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public BiblioMantenimientoDAO getBiblioMantenimientoDAO() {
        return biblioMantenimientoDAO;
    }

    public void setBiblioMantenimientoDAO(
            BiblioMantenimientoDAO biblioMantenimientoDAO) {
        this.biblioMantenimientoDAO = biblioMantenimientoDAO;
    }

    public TipoTablaDetalle GetTipoTablaDetalle(String codPadre, String codHijo) {

        return this.biblioMantenimientoDAO.GetTipoTablaDetalle(codPadre, codHijo);
    }

    public List<ParametrosReservados> GetListParametrosGenerales() {
        try{
            return biblioMantenimientoDAO.GetListParametrosGenerales();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;

    }

    public String UpdateParametrosGenerales(String cadenaDatos, String usuario,
            String nroRegistros) {
        try{
            return this.biblioMantenimientoDAO.UpdateParametrosGenerales(cadenaDatos, usuario, nroRegistros);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public List<TipoTablaDetalle> listaMaximoDiasPrestamos(String codTipoUsuario) {
        try{
            List<TipoTablaDetalle> lista = biblioMantenimientoDAO.listaMaximoDiasPrestamos(codTipoUsuario);
//            Log.info("lista.size manager: " + lista.size());
            return lista;
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public List<OpcionesApoyo> listaDeOpcionesPorApoyo(String codApoyo) {
        try{
            List<OpcionesApoyo> lista = biblioMantenimientoDAO.listaDeOpcionesPorApoyo(codApoyo);
            return lista;
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public String updateOpcionesApoyo(String codApoyo, String cadena,
            String nroRegistros, String codUsuario) {
        try
        {
            return this.biblioMantenimientoDAO.updateOpcionesApoyo(codApoyo, cadena, nroRegistros, codUsuario);
        }
        catch(Exception ex){ex.printStackTrace();}

        return null;
    }

	@Override
	public List<Sala> listaHorarioSalas(String sede) {
		 try{
	            return this.biblioMantenimientoDAO.listaHorarioSalas(sede);
	        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
	}

	@Override
	public List<TipoTablaDetalle> listaHorasBiblioteca(String sede) {
		try{
            return this.biblioMantenimientoDAO.listaHorasBiblioteca(sede);
        }
	    catch(Exception ex){ex.printStackTrace();}
	    return null;
	}

	@Override
	public String UpdateHorarioSalas(String cadenaDatos, String usuario,
			String nroRegistros) {
		try{
            return this.biblioMantenimientoDAO.UpdateHorarioSalas(cadenaDatos, usuario, nroRegistros);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
	}

}
