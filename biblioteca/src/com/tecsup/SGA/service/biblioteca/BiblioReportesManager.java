 package com.tecsup.SGA.service.biblioteca;

import java.util.*;

public interface BiblioReportesManager {

	public List getReporteBuzonSugerencia (String periodo, String codTipoMaterial);
	public List getReporteCatalogoFichas(String sede);
	public List getReporteMatFaltante(String fecIni, String fecFin,String sede);
	public List getReporteMatAdquiridos(String fecIni, String fecFin, String tipoCambio,String sede,String procedencia);
	public List getReporteMatRetirados(String fecIni, String fecFin, String sede);	
	public List getReporteMatMasSolicitado(String fecIni, String fecFin, String nroMax,String sede);
	public List getReporteMovPrestDevDiaHra(String fecIni, String fecFin,String sede);
	public List getReporteResumenMovDia(String fecIni, String fecFin,String sede);
	public List getReporteUsuariosSancion(String periodo1,String periodo2, String tipo,String sede,String tipoUsuario);
	public List getReporteEstadTipoUsuario(String fecIni, String fecFin,String sede);
	public List getReporteEstadCicloEsp(String fecIni, String fecFin,String sede);
	public List getReporteUsuarioIndividual(String codUsuario,String username,String sede);//JHPR 2008-7-3
	public List getReporteReservaCicloEsp(String fecIni, String fecFin,String sede);
	public List getReporteResumenResDia(String fecIni, String fecFin,String sede);
	public List getReporteEtiquetas(String fecIni, String fecFin,String sede, String tipoRep, String nroIngresoIni, String nroIngresoFin);
}
