package com.tecsup.SGA.service.biblioteca;

import java.util.List;

import com.tecsup.SGA.bean.AlumnoXCodigoBean;
import com.tecsup.SGA.bean.HistorialProrrogaBean;
import com.tecsup.SGA.bean.MaterialxIngresoBean;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.modelo.Ciudad;
import com.tecsup.SGA.modelo.Pais;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.ServiciosUsuarios;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialCommand;
import com.tecsup.SGA.web.biblioteca.command.AdminRegistrarMaterialRegistrarIngresoCommand;

/**
 * @author rochoa
 *
 */
public interface BiblioGestionMaterialManager {

	/**METODO PARA LA CONSULTA
	 * @param txtCodigo
	 * @param txtNroIngreso
	 * @param cboTipoMaterial
	 * @param cboBuscarPor
	 * @param txtTitulo
	 * @param cboIdioma
	 * @param cboAnioIni
	 * @param cboAnioFin
	 * @param txtFechaReservaIni
	 * @param txtFechaReservaFin
	 * @return
	 */
	List getAllMaterialesxFiltro(String txhCodigoUnico,String txtCodigo, String txtNroIngreso,
			String cboTipoMaterial, String cboBuscarPor, String txtTitulo,
			String cboIdioma, String cboAnioIni, String cboAnioFin,
			String txtFechaReservaIni, String txtFechaReservaFin,String sede);

	/**METODO QUE RETORNA LOS DETALLES DEL MATERIAL BIBLIOGRAFICO DENTRO DE UN OBJETO COMMAND
	 * DADO UN CODIGO UNICO.
	 * @param control
	 * @return
	 */
	AdminRegistrarMaterialCommand getAllMaterialDetalle(
			AdminRegistrarMaterialCommand control);

	/**RETORNA TODOS LOS INGRESOS POR MATERIAL
	 * @param txhCodUnico
	 * @return
	 */
	List getAllIngresosxMaterial(String txhCodMaterial,String txhCodUnico);

	/**RETORNA LA LISTA DE DESCRIPTORES X MATERIAL BIBLIOGRAFICO.
	 * @param txhCodUnico
	 * @return
	 */
	List getAllDescriptorxMaterial(String txhCodUnico);

	/**RETORNA LA LISTA DE ARCHIVOS ADJUNTOS X MATERIAL BIBLIOGRAFICO.
	 * @param txhCodUnico
	 * @return
	 */
	List getAllArchAdjuntoxMaterial(String txhCodUnico);

	/**INSERTA LOS DATOS DEL MATERIAL ADJUNTO
	 * @param txhCodigoUnico
	 * @param txtTitulo
	 * @param nomArchivo
	 * @param usuario
	 * @return
	 */
	String InsertArchivoAdjuntoxMaterial(String txhCodigoUnico,
			String txtTitulo, String nomArchivo, String usuario);

	/**METODO QUE ELIMINA UN ARCHIVO ADJUNTO POR CODIGO DE MATERIAL
	 * @param txhCodigoUnico
	 * @param usuario
	 * @return
	 */
	String deleteArchAdjuntoxMaterial(String txhCodigoUnico, String usuario);

	/**METODO PARA INSERTAR DESCRIPTORES A LOS MATERIALES
	 * @param codMaterial
	 * @param cadena
	 * @param nroRegistros
	 * @param usuario
	 * @return
	 */
	String insertDescriptorxMaterial(String codMaterial,String cadena, String nroRegistros, String usuario);

	/**ELIMINAR DESCRIPTORES POR MATERIAL
	 * @param txhCodigoUnicoDocumento
	 * @param usuario
	 * @return
	 */
	String deleteDescriptorxMaterial(String txhCodigoUnicoDocumento,
			String usuario);

	/**INSERTAR INGRESOS DEL MATERIAL
	 * @param txhCodigoUnico
	 * @param txtFecIngreso
	 * @param txtFecBaja
	 * @param cboProcedencia
	 * @param cboMoneda
	 * @param txtPrecio
	 * @param cboEstado
	 * @param txtObservacion
	 * @param usuario
	 * @return
	 */
	String insertIngresosxMaterial(String txhCodigoUnico, String txtFecIngreso,
			String txtFecBaja, String cboProcedencia, String cboMoneda,
			String txtPrecio, String cboEstado, String txtObservacion,
			String usuario);
	
	public String updateIngresosxMaterial(String codUnico, String fechaIngreso, String fechaBaja,
			String codProcedencia, String codMoneda, String precio, String codEstado, 
			String observaciones, String usuario);
	
	public String deleteIngresosxMaterial(String codUnico, String usuario);

	/**METODO QUE RETORNA EL INGRESO DE UN MATERIAL EN UN OBJETO COMMAND
	 * @param codigoMaterial,codigoUnico del registro
	 * @return
	 */
	AdminRegistrarMaterialRegistrarIngresoCommand getIngresoxMaterial(
			AdminRegistrarMaterialRegistrarIngresoCommand control);
	
	/**METODO QUE REGISTRA UN MATERIAL BIBLIOGRAFICO
	 * @param tipMaterial
	 * @param titulo
	 * @param codAutor
	 * @param codDewey
	 * @param nroPaginas
	 * @param codEditorial
	 * @param nroEditorial
	 * @param codPais
	 * @param codCiudad
	 * @param fecPublicacion
	 * @param urlArchDigital
	 * @param codIdioma
	 * @param isbn
	 * @param formato
	 * @param codTipoVideo
	 * @param codProcedencia
	 * @param codMoneda
	 * @param precio
	 * @param indPrestamoSala
	 * @param indPrestamoCasa
	 * @param indReserva
	 * @param observaciones
	 * @param imagen
	 * @param usuario
	 * @return
	 */
	public String insertMaterial(
			String tipMaterial,
			String titulo,
			String codAutor,
			String codDewey,
			String nroPaginas,
			String codEditorial,
			String nroEditorial,
			String codPais,
			String codCiudad,
			String fecPublicacion,
			String urlArchDigital,
			String codIdioma,
			String isbn,
			String formato,
			String codTipoVideo,			
			String codProcedencia,
			String codMoneda,
			String precio,
			String indPrestamoSala,
			String indPrestamoCasa,
			String indReserva,
			String observaciones,
			String imagen,			
			String usuario,
			String ubicacionFisica,
			String codSede,String codGenerado,String prefijo,String sufijo,String frecuencia,String mencionSerie, String contenido
	);
	
	/**ACTUALIZA EL MATERIAL BIBLIOGRAFICO
	 * @param codMaterial
	 * @param tipMaterial
	 * @param titulo
	 * @param codAutor
	 * @param codDewey
	 * @param nroPaginas
	 * @param codEditorial
	 * @param nroEditorial
	 * @param codPais
	 * @param codCiudad
	 * @param fecPublicacion
	 * @param urlArchDigital
	 * @param codIdioma
	 * @param isbn
	 * @param formato
	 * @param codTipoVideo
	 * @param indPrestamoSala
	 * @param indPrestamoCasa
	 * @param indReserva
	 * @param imagen
	 * @param usuario
	 * @return
	 */
	public String updateMaterial(
			String codMaterial,
			String tipMaterial,
			String titulo,
			String codAutor,
			String codDewey,
			String nroPaginas,
			String codEditorial,
			String nroEditorial,
			String codPais,
			String codCiudad,
			String fecPublicacion,
			String urlArchDigital,
			String codIdioma,
			String isbn,
			String formato,
			String codTipoVideo,
			String indPrestamoSala,
			String indPrestamoCasa,
			String indReserva,			
			String imagen,			
			String usuario,
			String ubicacionFisica,
			String codSede,
			String codGenerado,
			String prefijo,
			String sufijo,
			String frecuencia,
			String mencionSerie,
			String contenido
	);
	
	/**
	 * @param codMaterial
	 * @param usuario
	 * @return
	 */
	public String deleteMaterial(String codMaterial, String usuario);
	
	/**OBTIENE EL HISTORIAL DEL PRESTAMO DEL MATERIAL
	 * @param txhCodUnico
	 * @return
	 */
	public List getAllHistorialPrestamoxMaterial(String txhCodUnico) ;
	
	/**OBTIENE EL HISTORIAL DE PRORROGAS DEL MATERIAL PARA UN USUARIO
	 * @param txhNroIngreso
	 * @param txhCodUsuario
	 * @return
	 */
	public List getAllHistorialProrrogaxMaterial(String txhNroIngreso, String txhCodUsuario,String sede) ;
	
	public ServiciosUsuarios getAllServicioByCodUsuario(String codUsuario);
	
	public boolean getPermisoParaProrroga(String codUsuario, Integer totalProrrogas);
	
	/**RETORNA LOS DATOS DEL ALUMNO DADO SU CODIGO EN UN OBJETO AlumnoXCodigoBean
	 * @param codigo
	 * @return
	 */
	public AlumnoXCodigoBean getAlumnoXCodigo(String codigo,String indTipoPrestamo,String tipoBusquedaCodUsu,String sede,String numIngreso);
	
	/**RETORNA MATERIAL DADO SU CODIGO DE INGRESO
	 * @param codigo
	 * @return
	 */
	public MaterialxIngresoBean getMaterialxIngreso(String codigo,String tipo,String sede,String codUsuario);
	
	/**INSERTAR LAS DEVOLUCIONES DE LOS MATERIALES 
	 * @param codPrestamo
	 * @param usuPrestamo
	 * @param flgSancion
	 * @param usuCreacion
	 */
	public String insertDevolucion(String codPrestamo,
			String usuPrestamo, String flgSancion, String usuCreacion);
	
	/**INSERTAR LOS PRESTAMOS POR ALUMNO
	 * @param codMaterial
	 * @param nroIngreso
	 * @param codUsuPrestamo
	 * @param indProrroga
	 * @param usuCreacion
	 * @param indPrestamo
	 * @param indReserva
	 * @return
	 */
	
	public Object insertPrestamoxAlumno(String codMaterial,
			String nroIngreso,
			String codUsuPrestamo,
			String indProrroga,
			String usuCreacion,
			String indPrestamo,
			String indReserva,			
			String fecPrestamo,Long codMultiple);

	/**
	 * @param control
	 * @return
	 */
	public List getAllDetalleMaterialFichaBibliografica(
			String codUnico);
	
	public List GetAllReservaByAlumno(String codUsuario,String nroIngreso);
	
	
	/**
	 * @param codMaterial
	 * @return lista de reservas por material
	 */
	public List getAllReservaByMaterial(String codMaterial);
	
	/**Elimina las reservas de un material
	 * @param cadena
	 * @param nroReg
	 * @param usuCrea
	 * @return
	 */
	public String deleteReservasMaterial(String cadena, String usuCrea);
	
	/**
	 * @param codUsuario
	 * @return
	 */
	public List getAllDatosUsuarioBib(String codUsuario);
	
	//jhpr 2008-08-15
	public List getUsuariosXCodigo(String codigo,String tipo);
	public List<SedeBean> GetSedesBibPorUsuario(String codUsuario) ;
	public int getCantISBNPorSede(String isbn, String sede,Long codMaterial);
	public String getEstadoIngreso(String nCodIngreso);
	public boolean categoriaDuplicada(String codMaterial, String codGenerado);
	
	public Long obtenerCodigoPrestamoMultiple();
	//lista los libros que adeuda un usuario
	public List<MaterialxIngresoBean> getListaLibrosDeudaXUsuario(String codUsuario);
	public List<MaterialxIngresoBean> getListaLibrosPrestadosXUsuario(String codUsuario);
}
