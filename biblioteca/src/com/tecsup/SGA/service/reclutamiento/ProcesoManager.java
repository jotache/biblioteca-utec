package com.tecsup.SGA.service.reclutamiento;

import com.tecsup.SGA.modelo.Evaluacion;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.Oferta;

import java.util.*;

public interface ProcesoManager {
	public List getAllProcesos(String tipoEtapa, String unidadFuncional, String fecIni, String fecFin );
	/*Grabar y Editar*/
	public String grabarProceso(Proceso proceso);	
	public Proceso getProceso(String idProceso);
	public String deleteProceso(String codProceso, String usuario);
	public Oferta getOferta(String codOferta);
	public String grabarOferta(Oferta oferta, String usuario);
	public List getAllPostulantes(String codProceso, String codEtapa, String nombres, String apellidos,String orden);
	
	public String insertPostulanteByProceso(String codPostulantes, String codProc, String tipo
			, String codEtapa, String estadoEvento, String usuario);
	
	public List getUsuarioByTipo(String tipo, String Sistema);
	
	public String insertEvaluadorByProceso(String codProceso, String codTipoEvaluacion			
			, String codEvaluador, String usuario);
	
	public String updateEvaluadorByProceso(String codRegEval, String codProceso, String codTipoEvaluacion
			, String codEvaluador, String usuario);
	
	public List getEvaluadorsByProceso(String codProceso);
	
	public String deletePostulanteByProceso(String codPostulantes, String codProceso
			, String tipo, String usuario);
	
	public List getAllEvaluacionesByPostulante(String codEtapa, String codProceso, String codPostulante);
	
	public List getAllProcesosParaRevision(String codEstado, String codProceso);
	
	/**
	 * @param codProceso
	 * @param codPostulante
	 * @return Retorna la calificacion final de un postulante.
	 */
	public Evaluacion getCalificacionFinal(String codProceso, String codPostulante);
	
	/**
	 * @param codPostulante
	 * @param archivo
	 * @param archivoGen
	 * @param codUsuario
	 * @return Actualiza el nombre del informe final del postulante.
	 */
	public String setInformeFinalPostulante(String codPostulante,String archivo,String archivoGen,String codUsuario);
	
	/**
	 * 
	 * @param codProceso
	 * @param usuario
	 * @return
	 */
	public String cargarPostulantesXProceso(String codProceso, String usuario);
	
	public List getTrazaEvaluaciones(String codProceso, String codPostulante);
	public Evaluador getEvaluador(String codProceso,String codTipoEvaluacion,String etapa);
	
	public List<Evaluador> getEvaluadoresAsignadosXProceso(String codProceso);
	
}
