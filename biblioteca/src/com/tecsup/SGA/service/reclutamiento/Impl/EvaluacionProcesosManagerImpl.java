package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.tecsup.SGA.bean.EvaluacionesByEvaluadorBean;
import com.tecsup.SGA.modelo.Evaluacion;
import com.tecsup.SGA.modelo.Postulante;

import com.tecsup.SGA.service.reclutamiento.EvaluacionProcesosManager;
import com.tecsup.SGA.DAO.reclutamiento.ProcesoEvaluacionesDAO;

public class EvaluacionProcesosManagerImpl implements EvaluacionProcesosManager{
	private ProcesoEvaluacionesDAO procesoEvaluacionesDAO;
	
	public String insertEvaluacionPostulante(String codPostulantes,
			String codProc, String codEtapa,
			String tipoEvaluacion, String estadoEvento, String comentario,
			String calificacion, String calificacionEtapa,
			String comentarioFinEtapa, String indCambioEstado, String usuario) {
		int numReg = 0;
		try
		{
			StringTokenizer st = new StringTokenizer(codPostulantes,"|");
			numReg = st.countTokens();
			if ( numReg > 0 )
			{
				return this.procesoEvaluacionesDAO.insertEvaluacionPostulante(codPostulantes, Integer.toString(numReg)
					, codProc, codEtapa, tipoEvaluacion, estadoEvento, comentario, calificacion
					, calificacionEtapa, comentarioFinEtapa, indCambioEstado, usuario);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		return null;
	}
	
	public List getAllEvaluacionesByEvaluador(String codEtapa,
			String postEstado, String rrhhEstado, String codEvaluador,String codProceso) {		
		try
		{
			return this.procesoEvaluacionesDAO.getAllEvaluacionesByEvaluador(codEtapa, postEstado
					, rrhhEstado, codEvaluador,codProceso);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String updateEvaluacionPostulante(String codPostulantesProceso,
			String codEvaluaciones, String codEstadoEvento, String comentario, String codCalificacion,
			String codCalificacionFinEtapa, String comenFinEtapa, String indCambioEstado, String usuario) {
		int numReg = 0;
		try
		{
			StringTokenizer stPostulantes = new StringTokenizer(codPostulantesProceso,"|");
			numReg = stPostulantes.countTokens();
			return this.procesoEvaluacionesDAO.updateEvaluacionPostulante(codPostulantesProceso, codEvaluaciones
					, Integer.toString(numReg), codEstadoEvento, comentario, codCalificacion, codCalificacionFinEtapa
					, comenFinEtapa, indCambioEstado, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public Evaluacion getEvaluacionById(String codEvaluacion) {
		
		try
		{
			ArrayList<Evaluacion> arrEvaluaciones = (ArrayList<Evaluacion>)this.procesoEvaluacionesDAO.getEvaluacionById(codEvaluacion);
			if ( arrEvaluaciones.size() > 0 )
				return arrEvaluaciones.get(0);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public void setProcesoEvaluacionesDAO(
			ProcesoEvaluacionesDAO procesoEvaluacionesDAO) {
		this.procesoEvaluacionesDAO = procesoEvaluacionesDAO;
	}

	public List getAllEvaluacionesByEvaluadorCbo(String codEtapa,
			String postEstado, String rrhhEstado, String codEvaluador) {
		try
		{
			return this.procesoEvaluacionesDAO.getAllEvaluacionesByEvaluadorCbo(codEtapa, postEstado
					, rrhhEstado, codEvaluador);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String updateEvaluacionPostulanteEnvio(String codPostulantes,
			String codProc, String codEtapa,
			String estadoEvento, String usuario) {
		int numReg = 0;
		try
		{
			StringTokenizer st = new StringTokenizer(codPostulantes,"|");
			numReg = st.countTokens();
			if ( numReg > 0 )
			{
				return this.procesoEvaluacionesDAO.updateEvaluacionPostulanteEnvio(codPostulantes, Integer.toString(numReg), codProc, codEtapa, estadoEvento, usuario);								
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		return null;
	}

	public List<Postulante> getPostulantesEnviadosJefeDpto(
			String cadenaCodPostulantes, String codProceso, String codEtapa) {
		
		return procesoEvaluacionesDAO.getPostulantesEnviadosJefeDpto(cadenaCodPostulantes, codProceso,codEtapa);
	}

	public List<EvaluacionesByEvaluadorBean> getEvaluacionesSeleccionPorPostulante(
			String codProceso, String codPostulante) {
		
		try{		
			return this.procesoEvaluacionesDAO.getEvaluacionesSeleccionPorPostulante(codProceso, codPostulante);
		}
		catch(Exception ex){		
			ex.printStackTrace();
		}
		return null; 		
	}

	public List<EvaluacionesByEvaluadorBean> getEvaluacionesPostulantesSeleccionPorTipoEvaluacion(
			String codProceso, String codTipoEvaluacion) {
		try{		
			return this.procesoEvaluacionesDAO.getEvaluacionesPostulantesSeleccionPorTipoEvaluacion(codProceso, codTipoEvaluacion);
		}
		catch(Exception ex){		
			ex.printStackTrace();
		}
		return null;
	}

	public List<EvaluacionesByEvaluadorBean> getEvaluacionesRevisadas(
			String codProceso, String codEvaluador) {
		List<EvaluacionesByEvaluadorBean> evaluaciones = new ArrayList<EvaluacionesByEvaluadorBean>();
		try {
			evaluaciones = procesoEvaluacionesDAO.getEvaluacionesRevisadas(codProceso, codEvaluador);		
			return evaluaciones;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}	

}
