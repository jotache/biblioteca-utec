package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.List;
import com.tecsup.SGA.DAO.TablaDAO;
import com.tecsup.SGA.service.reclutamiento.TablaManager;

public class TablaManagerImpl implements TablaManager{
	
	TablaDAO tablaDAO;
	
	public void setTablaDAO(TablaDAO tablaDAO) {
		this.tablaDAO = tablaDAO;
	}

	public List getAllTabla(String mantenimientoTipo, String sistemaTipo)
	{
		try
		{
			return tablaDAO.getAllTabla(mantenimientoTipo, sistemaTipo);
	}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	
	
}
