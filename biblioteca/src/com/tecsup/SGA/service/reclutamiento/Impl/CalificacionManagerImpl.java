package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.List;
import com.tecsup.SGA.modelo.Calificacion;
import com.tecsup.SGA.service.reclutamiento.CalificacionManager;
import com.tecsup.SGA.DAO.reclutamiento.CalificacionDAO;
public class CalificacionManagerImpl implements CalificacionManager{
	CalificacionDAO calificacionDAO;
	
	public List getAllCalificacion(String codEtapa, String codTipoEvaluacion) {
		try
		{
			return this.calificacionDAO.getAllCalificacion(codEtapa, codTipoEvaluacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public void setCalificacionDAO(CalificacionDAO calificacionDAO) {
		this.calificacionDAO = calificacionDAO;
	}
	
	public String InsertCalificacion(Calificacion obj, String usuario, String estReg)
	{
		try
		{
			return this.calificacionDAO.InsertCalificacion(obj.getCodEtapa()
					, obj.getCodTipoEvaluacion(),obj.getCodCalificacionNormal(), obj.getCodCalificacionEliminatoria(), usuario, estReg);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String UpdateCalificacion(Calificacion obj, String usuario, String estReg)
	{
		try
		{
			return this.calificacionDAO.UpdateCalificacion(obj.getCodEtapa()
					, obj.getCodTipoEvaluacion(), obj.getCodCalificacionNormal(), obj.getCodCalificacionEliminatoria(), usuario, estReg);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();	
		}
		return null;
	}
	
	public String DeleteCalificaciones(Calificacion obj, String usuario)
	{
		try
		{
			return this.calificacionDAO.DeleteCalificaciones(obj.getCodEtapa(), obj.getCodTipoEvaluacion(), usuario);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();	
		}
		return null;
	}
	
	
}
