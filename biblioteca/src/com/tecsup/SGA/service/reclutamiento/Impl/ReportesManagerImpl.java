package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.reclutamiento.ReportesDAO;
import com.tecsup.SGA.service.reclutamiento.ReportesManager;

public class ReportesManagerImpl implements ReportesManager{
	
	ReportesDAO reportesDAO;
	
	public List GetAllProcesoEstado(String codTipoProceso, String codUniFunci, String dscProceso,String fecIni,
			String fecFin){
		try
		{
			return reportesDAO.GetAllProcesoEstado(codTipoProceso, codUniFunci, dscProceso, fecIni, fecFin);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List GetAllProcesoPostulante(String codTipoProceso, String codUniFunci, String dscProceso,String nombre,
			String apePaterno, String apeMaterno,String fecha1,String fecha2){
		try
		{
			return reportesDAO.GetAllProcesoPostulante(codTipoProceso, codUniFunci, dscProceso, nombre, 
					apePaterno, apeMaterno,fecha1,fecha2);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllOferta(String codTipoProceso, String codUniFunci, String dscProceso,String fecIni,
			String fecFin){
		try
		{
			return reportesDAO.GetAllOferta(codTipoProceso, codUniFunci, dscProceso, fecIni, fecFin);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public ReportesDAO getReportesDAO() {
		return reportesDAO;
	}

	public void setReportesDAO(ReportesDAO reportesDAO) {
		this.reportesDAO = reportesDAO;
	}
}
