package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.*;

import com.tecsup.SGA.common.CommonConstants;
import com.tecsup.SGA.modelo.Recluta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.service.reclutamiento.*;
import com.tecsup.SGA.DAO.*;
import com.tecsup.SGA.DAO.reclutamiento.ReclutaDAO;

public class ReclutaManagerImpl  implements ReclutaManager {
	ReclutaDAO reclutaDAO;
	MaestroDAO maestroDAO;
	TablaDetalleDAO tablaDetalleDAO;
	
	
	public String getAllRecluta(String email, String clave) {
		try {
			return reclutaDAO.getAllRecluta(email, clave);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateReclutaClave(String email, String clave_anterior, String clave_nueva){
		try {
			return reclutaDAO.updateReclutaClave(email, clave_anterior, clave_nueva);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public Recluta getAllDatosPersonaRecluta(String idRec){
		try {
			return reclutaDAO.getAllDatosPersonaRecluta(idRec);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertReclutaDatos(String nombre, String apepat, String apemat, String clave, String fecnac, 
			String dni, String ruc, String sexo, String estadoCivil, String codNacionalidad, 
			String email, String email1, String aniosExpLaboral, String direccion, String codDpto, 
			String codProv, String codDist, String pais, String codPostal, String telefCasa, 
			String telefAdicional, String telefCel, String codPostuladoAntes, String codTrabAntes,
			String expDocencia, String aniosExpDocencia, String familiaAntes, String familiaNombres,  
			String dispoViaje, String sedePref, String codInteresado, String pretension, String codDedicacion, 
			String codDispoTrab, String perfil, String codAreaInteres, String nroRegAreaInt, 
			String codMoneda, String tipoPago,String puestoPostula){
		
		try {
			return reclutaDAO.insertReclutaDatos(nombre, apepat, apemat, clave, fecnac, dni, ruc, sexo, 
												estadoCivil, codNacionalidad, email, email1, aniosExpLaboral, 
												direccion, codDpto, codProv, codDist, pais, codPostal, telefCasa, 
												telefAdicional, telefCel, codPostuladoAntes, codTrabAntes, 
												expDocencia, aniosExpDocencia, familiaAntes, familiaNombres, 
												dispoViaje, sedePref, codInteresado, pretension, codDedicacion, 
												codDispoTrab, perfil, codAreaInteres, nroRegAreaInt, codMoneda, tipoPago,puestoPostula);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateReclutaDatos(String idRec, String nombre, String apepat, String apemat, String fecnac, 
			String dni, String ruc, String sexo, String estadoCivil, String codNacionalidad, 
			String email, String email1, String aniosExpLaboral, String direccion, String codDpto, 
			String codProv, String codDist, String pais, String codPostal, String telefCasa, 
			String telefAdicional, String telefCel, String codPostuladoAntes, String codTrabAntes,
			String expDocencia, String aniosExpDocencia, String familiaAntes, String familiaNombres, 
			String dispoViaje, String sedePref, String codInteresado, String pretension, String codDedicacion, 
			String codDispoTrab, String perfil, String codMoneda, String tipoPago,String codAreaInteres,String puestoPostula,String codUsuario){
		
		try {			
			return reclutaDAO.updateReclutaDatos(idRec, nombre, apepat, apemat, fecnac, dni, ruc, sexo, 
												estadoCivil, codNacionalidad, email, email1, aniosExpLaboral, 
												direccion, codDpto, codProv, codDist, pais, codPostal, telefCasa, 
												telefAdicional, telefCel, codPostuladoAntes, codTrabAntes, 
												expDocencia, aniosExpDocencia, familiaAntes, familiaNombres, 
												dispoViaje, sedePref, codInteresado, pretension, codDedicacion, 
												codDispoTrab, perfil, codMoneda, tipoPago,codAreaInteres,puestoPostula,codUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReclutaIdiomas(String idRec){
		try {
			return reclutaDAO.getReclutaIdiomas(idRec);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReclutaSuperior(String idRec){
		try {
			return reclutaDAO.getReclutaSuperior(idRec);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReclutaExpLaboral(String idRec){
		try {
			return reclutaDAO.getReclutaExpLaboral(idRec);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertReclutaExpLaboral(String idRec, String empresa, String codPuesto, String otroPuesto, String fecIni,
			String fecFin, String referencia, String telef, String funciones,String codUsuario){
		try {
			return reclutaDAO.insertReclutaExpLaboral(idRec, empresa, codPuesto, otroPuesto, fecIni, fecFin, 
														referencia, telef, funciones,codUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateReclutaExpLaboral(	String idRec, String codExpLab, String empresa, String codPuesto, String otroPuesto, String fecIni,
			String fecFin, String referencia, String telef, String funciones,String codUsuario){
		try {
			return reclutaDAO.updateReclutaExpLaboral(idRec, codExpLab, empresa, codPuesto, otroPuesto, fecIni, fecFin, referencia, telef, funciones,codUsuario );
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	
	public String updateReclutaArchAdjunto(String idRec, String cv, String foto,String codUsuario){
		try {
			return reclutaDAO.updateReclutaArchAdjunto(idRec, cv, foto,codUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateReclutaEstSec(	String idRec, String colegio1, String colegio2, String anioIni1, String anioFin1,
										String anioIni2, String anioFin2,String codUsuario){
		try {
			return reclutaDAO.updateReclutaEstSec(idRec, colegio1, colegio2, anioIni1, anioFin1, anioIni2, anioFin2,codUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateReclutaIdiomas(String idRec, String cadena, String nroReg,String codUsuario){
		try {
			return reclutaDAO.updateReclutaIdiomas(idRec, cadena, nroReg,codUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateReclutaEstSup(String idRec, String cadena, String nroReg,String codUsuario){
		try {
			return reclutaDAO.updateReclutaEstSup(idRec, cadena, nroReg,codUsuario);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllOfertas(){
		try {
			return reclutaDAO.getAllOfertas();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertPostularOferta(String idRec, String codOferta){
		try {
			return reclutaDAO.insertPostularOferta(idRec, codOferta);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	//Maestro
	public List getAllDepartamento(){
		try {
			return maestroDAO.getAllDepartamento();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllProvincia(String departamento){
		try {
			return maestroDAO.getAllProvincia(departamento);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllDistrito(String provincia){
		try {
			return maestroDAO.getAllDistrito(provincia);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertReclutaAreaInteres (String idRec, String cadena, String nroReg){
		try {
			return reclutaDAO.insertReclutaAreaInteres(idRec, cadena, nroReg);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReclutaAreaInteres(String idRec){
		//System.out.println("Inicializando areas de interes");
		
		List resultado = new ArrayList();
		
		try {			
			//obteniendo cabeceras de �reas de interes.
			List areas = tablaDetalleDAO.getAllTablaDetalle("0005", "", "", "", "", "", "", "", CommonConstants.TIPO_ORDEN_DSC);			
			if (areas!=null){
				Recluta recluta = null;
				TipoTablaDetalle tipoTablaCab = null;
				for (int i=0;i<areas.size();i++){
					tipoTablaCab = (TipoTablaDetalle) areas.get(i);
					recluta = new Recluta();
					recluta.setTermino("C");//indicamos que es cabecera.
					recluta.setCodTTDInteres("");
		        	recluta.setCodInteres("");
		        	recluta.setArea(tipoTablaCab.getCodTipoTablaDetalle());
		        	recluta.setDescInteres(tipoTablaCab.getDescripcion());
		        	resultado.add(recluta);
		        	List detalles = this.reclutaDAO.getReclutaAreaInteres(idRec,tipoTablaCab.getCodTipoTablaDetalle());
		        	if (detalles!=null) {
		        		for (int y=0;y<detalles.size();y++){
		        			//recluta = new Recluta();
		        			recluta = (Recluta) detalles.get(y);
		        			recluta.setTermino("D"); // indicamos que es detalle de area
		        			resultado.add(recluta);
		        		}
		        	}
				}
			}
			
			//return reclutaDAO.getReclutaAreaInteres(idRec);
			return resultado;
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getReclutaAreaInteresEspecial(String idRec, String tipoArea){
		//System.out.println("ReclutaManagerImpl.getReclutaAreaInteresEspecial::idRec:"+idRec);
		//System.out.println("ReclutaManagerImpl.getReclutaAreaInteresEspecial::tipoArea:"+tipoArea);
		int numReg = 0;
		try {
			if (tipoArea != null ){
				StringTokenizer stDatos = new StringTokenizer(tipoArea, ",");
				numReg = stDatos.countTokens();
			}
			return reclutaDAO.getReclutaAreaInteresEspecial(idRec, tipoArea, Integer.toString(numReg));
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	/*SETTER*/
	public void setReclutaDAO(ReclutaDAO reclutaDAO) {
		this.reclutaDAO = reclutaDAO;
	}
	
	public void setMaestroDAO(MaestroDAO maestroDAO) {
		this.maestroDAO = maestroDAO;
	}

	public void setTablaDetalleDAO(TablaDetalleDAO tablaDetalleDAO) {
		this.tablaDetalleDAO = tablaDetalleDAO;
	}
}
