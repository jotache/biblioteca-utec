package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.List;

import com.tecsup.SGA.service.*;
import com.tecsup.SGA.service.reclutamiento.PostulanteManager;
import com.tecsup.SGA.DAO.reclutamiento.PostulanteDao;
public class PostulanteManagerImpl implements PostulanteManager{
	PostulanteDao postulanteDao;
		
	public PostulanteDao getPostulanteDao() {
		return postulanteDao;
	}

	public void setPostulanteDao(PostulanteDao postulanteDao) {
		this.postulanteDao = postulanteDao;
	}


	public List getAllPostulante(String nombres, String apellidos,
			String areasInteres, String profesion, String egresado, String grado, 
			String codProceso, String codEtapa) {
		try
		{
			return postulanteDao.getAllPostulante(nombres, apellidos, areasInteres, profesion
					, egresado, grado, codProceso, codEtapa);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}//
	
	public List GetPostulanteByEmail(String emailPostulante) {
		try
		{
			return postulanteDao.GetPostulanteByEmail(emailPostulante);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
