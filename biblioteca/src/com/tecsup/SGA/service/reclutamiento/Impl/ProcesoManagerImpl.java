package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.*;

import org.jfree.util.Log;

import com.tecsup.SGA.modelo.Evaluacion;
import com.tecsup.SGA.modelo.Evaluador;
import com.tecsup.SGA.modelo.Proceso;
import com.tecsup.SGA.modelo.Oferta;
import com.tecsup.SGA.service.reclutamiento.ProcesoManager;
import com.tecsup.SGA.DAO.reclutamiento.ProcesoDAO;
import com.tecsup.SGA.DAO.reclutamiento.ProcesoEvaluacionesDAO;
import com.tecsup.SGA.common.CommonConstants;


public class ProcesoManagerImpl implements ProcesoManager{
	ProcesoDAO procesoDAO;
	ProcesoEvaluacionesDAO procesoEvaluacionesDAO;
		
	public void setProcesoEvaluacionesDAO(
			ProcesoEvaluacionesDAO procesoEvaluacionesDAO) {
		this.procesoEvaluacionesDAO = procesoEvaluacionesDAO;
	}

	public void setProcesoDAO(ProcesoDAO procesoDAO) {
		this.procesoDAO = procesoDAO;
	}

	public List getAllProcesos(String tipoEtapa, String unidadFuncional,
			String fecIni, String fecFin) {
		try
		{
			return procesoDAO.getAllProceso(unidadFuncional, fecIni, fecFin, tipoEtapa);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String grabarProceso(Proceso proceso)
	{
		//System.out.println("grabarProceso 0");
		int nroAreaInteres;
		int nroAreasEstudios;
		int nroAreasNivEstudios;
		int nroInstEduc;
		try
		{
			if (proceso.getCodAreasInteres()==null) proceso.setCodAreasInteres("");
			if (proceso.getCodAreasEstudios()==null) proceso.setCodAreasEstudios("");
			if (proceso.getCodAreasNivelEstudios()==null) proceso.setCodAreasNivelEstudios("");
			if (proceso.getCodInsEdu()==null) proceso.setCodInsEdu("");
						
			StringTokenizer st = new StringTokenizer(proceso.getCodAreasInteres(),"|");			
			StringTokenizer stEst = new StringTokenizer(proceso.getCodAreasEstudios(),"|");			
			StringTokenizer stNivEst = new StringTokenizer(proceso.getCodAreasNivelEstudios(),"|");			
			StringTokenizer stInstEduc = new StringTokenizer(proceso.getCodInsEdu() ,"|");
			
			nroAreaInteres = st.countTokens();			
			nroAreasEstudios = stEst.countTokens();			
			nroAreasNivEstudios = stNivEst.countTokens();			
			nroInstEduc = stInstEduc.countTokens();
			
			//Log.info("proceso.getCodProceso():"+proceso.getCodProceso());
			if ( proceso.getCodProceso().trim().equals(""))
			{
				//System.out.println("grabarProceso 10");
				return procesoDAO.insertProceso(proceso.getCodEtapa(), proceso.getCodProcRevAsociado()
						, proceso.getDscProceso(), proceso.getCodUnidadFuncional()
						, proceso.getCodAreasInteres(), Integer.toString(nroAreaInteres)
						, proceso.getCodAreaEstudio(), proceso.getCodInstitucionAcademica()
						, proceso.getCodGradoAcedemico(), proceso.getSalarioIni(), proceso.getSalarioFin()
						, proceso.getCodTipoEdad(), proceso.getEdad(), proceso.getCodSexo()
						, proceso.getCodDedicacion(), proceso.getCodInteresado(), proceso.getFlagDisponibilidadViajar()
						, proceso.getAnhosExperiencia(), proceso.getAnhosExperienciaDocencia()
						, proceso.getUsuario(), proceso.getCodTipoEdad1(), proceso.getEdad1()
						, proceso.getCodTipoMoneda(),proceso.getPuestoPostula()
						, proceso.getCodAreasEstudios(), Integer.toString(nroAreasEstudios)
						, proceso.getCodAreasNivelEstudios(), Integer.toString(nroAreasNivEstudios)
						, proceso.getCodInsEdu(),Integer.toString(nroInstEduc));				
			}
			else
			{
				return procesoDAO.updateProceso(proceso.getCodProceso(), proceso.getCodEtapa()
						, proceso.getCodProcRevAsociado(), proceso.getDscProceso(), proceso.getCodUnidadFuncional()
						, proceso.getCodAreasInteres(), Integer.toString(nroAreaInteres)
						, proceso.getCodAreaEstudio(), proceso.getCodInstitucionAcademica()
						, proceso.getCodGradoAcedemico(), proceso.getSalarioIni(), proceso.getSalarioFin()
						, proceso.getCodTipoEdad(), proceso.getEdad(), proceso.getCodSexo()
						, proceso.getCodDedicacion(), proceso.getCodInteresado(), proceso.getFlagDisponibilidadViajar()
						, proceso.getAnhosExperiencia(), proceso.getAnhosExperienciaDocencia()
						, proceso.getUsuario(), proceso.getCodTipoEdad1(), proceso.getEdad1()
						, proceso.getCodTipoMoneda(),proceso.getPuestoPostula()
						, proceso.getCodAreasEstudios(), Integer.toString(nroAreasEstudios)
						, proceso.getCodAreasNivelEstudios(), Integer.toString(nroAreasNivEstudios)
						, proceso.getCodInsEdu(),Integer.toString(nroInstEduc));
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public Proceso getProceso(String idProceso)
	{
		Proceso proceso;
		try
		{
			proceso = procesoDAO.getProceso(idProceso);
			
			if ( proceso != null){
												
				proceso.setListaEvaluadores(procesoDAO.getEvaluadores(idProceso,"",proceso.getCodEtapa()));
				proceso.setListAreasInteres(procesoDAO.getAreasInteresByProceso(idProceso));
				//proceso.setListAreasEstudios(procesoDAO.getAreasEstudiosXProceso(idProceso));
				//proceso.setListAreasNivelEstudios(procesoDAO.getAreasNivelEstudiosXProceso(idProceso));
				proceso.setListAreasEstudios(procesoDAO.getDetallesFiltroProceso(idProceso, "0002"));
				proceso.setListAreasNivelEstudios(procesoDAO.getDetallesFiltroProceso(idProceso, "0003"));
				proceso.setListlistInstitucionesEdu(procesoDAO.getDetallesFiltroProceso(idProceso, "0127"));
			}
			return proceso;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String deleteProceso(String codProceso, String usuario)
	{
		try
		{
			return procesoDAO.deleteProceso(codProceso, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;		
	}
	
	public Oferta getOferta(String codOferta) {
		try
		{
			return procesoDAO.getOfertaById(codOferta);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;	
	}
	
	public String grabarOferta(Oferta oferta, String usuario) {
		try
		{
			if ( oferta.getCodOferta().trim().equals("") )
			{
				return procesoDAO.insertOferta(oferta.getCodProceso(), oferta.getFecIniPub()
						, oferta.getFecFinPub(), oferta.getDescripcion(), oferta.getDenominacion()
						, oferta.getVacantes(), usuario);
			}
			else
			{
				return procesoDAO.updateOferta(oferta.getCodOferta(), oferta.getCodProceso(), oferta.getFecIniPub()
						, oferta.getFecFinPub(), oferta.getDescripcion(), oferta.getDenominacion()
						, oferta.getVacantes(), usuario);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllPostulantes(String codProceso, String codEtapa,
			String nombres, String apellidos,String orden) {
		try
		{
			return this.procesoDAO.getAllPostulantes(codProceso, codEtapa, nombres, apellidos,orden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String insertPostulanteByProceso(String codPostulantes, String codProc, String tipo
			, String codEtapa, String estadoEvento, String usuario) {
		int numReg = 0;
		String respuesta;
		try
		{
			StringTokenizer st = new StringTokenizer(codPostulantes,"|");
			numReg = st.countTokens();
			respuesta = this.procesoDAO.insertPostulanteByProceso(codPostulantes, Integer.toString(numReg) 
						, codProc, tipo);
			/*if ( respuesta.trim().equals("0"))
			{
				respuesta = this.procesoEvaluacionesDAO.insertEvaluacionPostulante(codPostulantes, Integer.toString(numReg)
						, codProc, codEtapa, "", estadoEvento, "", "", "", ""
						, CommonConstants.EVAL_ACT_ESTADO , usuario);
			}*/
			return respuesta;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public List getUsuarioByTipo(String tipo, String sistema) {
		try
		{
			return this.procesoDAO.getUsuarioByTipo(tipo, sistema);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertEvaluadorByProceso(String codProceso,
			String codTipoEvaluacion, String codEvaluador, String usuario) {
		try
		{
			return this.procesoDAO.insertEvaluadorByProceso(codProceso, codTipoEvaluacion, codEvaluador, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateEvaluadorByProceso(String codRegEval,
			String codProceso, String codTipoEvaluacion, String codEvaluador,
			String usuario) {
		try
		{
			return this.procesoDAO.updateEvaluadorByProceso(codRegEval, codProceso, codTipoEvaluacion
					, codEvaluador, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getEvaluadorsByProceso(String codProceso) {
		try
		{
			return this.procesoDAO.getEvaluadoresByProceso(codProceso);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String deletePostulanteByProceso(String codPostulantes, String codProceso, String tipo, String usuario) {
		int numReg = 0;
		try
		{
			numReg = new StringTokenizer(codPostulantes,"|").countTokens();			
			return this.procesoDAO.deletePostulanteByProceso(codPostulantes, Integer.toString(numReg), codProceso, tipo, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllEvaluacionesByPostulante(String codEtapa, String codProceso,
			String codPostulante) {
		try
		{
			return this.procesoDAO.getAllEvaluacionByPostulante(codEtapa, codProceso, codPostulante);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllProcesosParaRevision(String codEstado, String codProceso) {
		try
		{
			return this.procesoDAO.getAllProcesosParaRevision(codEstado, codProceso);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public Evaluacion getCalificacionFinal(String codProceso, String codPostulante)
	{
		try
		{
			return this.procesoDAO.getCalificacionFinal(codProceso, codPostulante);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String setInformeFinalPostulante(String codPostulante,
			String archivo, String archivoGen, String codUsuario) {
		try
		{
			return this.procesoDAO.updateInformeFinalPostulante(codPostulante, archivo, archivoGen, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String cargarPostulantesXProceso(String codProceso, String usuario) {
		try
			{
				return procesoDAO.cargarPostulantesXProceso(codProceso, usuario);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return null;	
	}

	public List getTrazaEvaluaciones(String codProceso, String codPostulante) {
		try
		{
			return procesoDAO.getTrazaEvaluaciones(codProceso, codPostulante);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public Evaluador getEvaluador(String codProceso, String codTipoEvaluacion,String etapa) {
		List<Evaluador> evaluadores = new ArrayList<Evaluador>();
		try {
			evaluadores = procesoDAO.getEvaluadores(codProceso, codTipoEvaluacion,etapa);
			if (evaluadores!=null){
				return evaluadores.get(0);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public List<Evaluador> getEvaluadoresAsignadosXProceso(String codProceso) {		
		List<Evaluador> evaluadores = new ArrayList<Evaluador>();
		try {
			evaluadores = procesoDAO.getEvaluadoresAsignadosXProceso(codProceso);			
			return evaluadores;
						
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

}
