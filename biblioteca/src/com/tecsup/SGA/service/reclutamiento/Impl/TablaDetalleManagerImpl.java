package com.tecsup.SGA.service.reclutamiento.Impl;

import java.util.ArrayList;
import java.util.List;
import com.tecsup.SGA.DAO.TablaDetalleDAO;
import com.tecsup.SGA.DAO.UnidadFuncionalDAO;
import com.tecsup.SGA.service.reclutamiento.TablaDetalleManager;
import com.tecsup.SGA.modelo.TipoTablaDetalle;

public class TablaDetalleManagerImpl implements TablaDetalleManager{
	TablaDetalleDAO tablaDetalleDAO;
	UnidadFuncionalDAO unidadFuncionalDAO;

	public List getAllTablaDetalle(String codPadre, String codDetalle, String descripcion
			, String valor1, String valor2, String valor3, String valor4
			, String valor5, String tipo )
	{
		try
		{
			return tablaDetalleDAO.getAllTablaDetalle(codPadre, codDetalle, descripcion, valor1
												, valor2, valor3, valor4, valor5, tipo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List getAllUnidadFuncional()
	{
		try
		{
			return this.unidadFuncionalDAO.getAllUnidadFuncinal();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllInstitucion(String codGiroInsti)
	{
		try
		{
			return tablaDetalleDAO.getAllInstitucion(codGiroInsti);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertTablaDetalle(TipoTablaDetalle obj , String usuario) {
		try
		{   	return this.tablaDetalleDAO.InsertTablaDetalle(obj.getCodTipoTabla()
					, obj.getDescripcion(), obj.getDscValor1(), obj.getDscValor2()
					, obj.getDscValor3(), obj.getDscValor4(), obj.getDscValor5()
					, usuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	public void setTablaDetalleDAO(TablaDetalleDAO tablaDetalleDAO) {
		this.tablaDetalleDAO = tablaDetalleDAO;
	}
	public void setUnidadFuncionalDAO(UnidadFuncionalDAO unidadFuncionalDAO) {
		this.unidadFuncionalDAO = unidadFuncionalDAO;
	}
	
	public String UpdateTablaDetalle(TipoTablaDetalle obj,String usuario){
		try
		{
			return this.tablaDetalleDAO.UpdateTablaDetalle(obj.getCodTipoTabla(),
					obj.getCodDetalle(),obj.getDescripcion(),obj.getDscValor1(),obj.getDscValor2(), obj.getDscValor3()
					, obj.getDscValor4(), obj.getDscValor5(),usuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}
	
	public String DeleteTablaDetalle(TipoTablaDetalle obj, String usuario){
		try
		{
			return this.tablaDetalleDAO.DeleteTablaDetalle(obj.getCodTipoTabla()
					,obj.getCadCod(),obj.getNroReg(), usuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}
	
	public String InsertarTablaDetalle(TipoTablaDetalle obj, String usuario){
		try
		{
			return this.tablaDetalleDAO.InsertarTablaDetalle(obj.getCodTipoTabla()
					, obj.getDescripcion(), obj.getDscValor1(), obj.getDscValor2(), usuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}
	
	public String EliminarTablaDetalle(TipoTablaDetalle obj,String usuario){
		try
		{
			return this.tablaDetalleDAO.EliminarTablaDetalle(obj.getCodTipoTabla()
					, obj.getDscValor1(),obj.getCadCod(),obj.getNroReg(), usuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}
	
	public String ActualizarTablaDetalle(TipoTablaDetalle obj,String usuario){
		try
		{
			return this.tablaDetalleDAO.ActualizarTablaDetalle(obj.getCodTipoTabla()
					, obj.getCodDetalle(), obj.getDescripcion(), obj.getDscValor1(), obj.getDscValor2(), obj.getTipo(), usuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}
	
	/* JHPR 2008-07-08
	 * (non-Javadoc)
	 * @see com.tecsup.SGA.service.reclutamiento.TablaDetalleManager#getAllAreasInteresReclutamiento(java.lang.String)
	 */
	public List getAllAreasInteresReclutamiento(String codCabeceras, String codDetalles,String tipo) {
		
		List areas = tablaDetalleDAO.getAllTablaDetalle(codCabeceras, "", "", "", "", "", "", "", tipo);		
		List<TipoTablaDetalle> resultado = new ArrayList<TipoTablaDetalle>();
		if (areas!=null) {
			TipoTablaDetalle tipoTablaCab = null;
			for (int i=0;i<areas.size();i++){				
				tipoTablaCab = (TipoTablaDetalle) areas.get(i);
				tipoTablaCab.setTipo("C");// indicamos que es cabecera.
				resultado.add(tipoTablaCab);
				TipoTablaDetalle tipoTablaDet = null;				
				List detallesxArea = tablaDetalleDAO.getAllTablaDetalle(codDetalles, "", "", tipoTablaCab.getCodTipoTablaDetalle(), "", "", "", "", tipo);
				if (detallesxArea!=null) {
					for (int y=0;y<detallesxArea.size();y++){
						tipoTablaDet = (TipoTablaDetalle) detallesxArea.get(y);
						tipoTablaDet.setTipo("D"); // indicamos que es detalle.
						resultado.add(tipoTablaDet);
					}
				}
			}			
		}
		return resultado;
	}
	
}
