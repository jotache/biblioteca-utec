package com.tecsup.SGA.service.reclutamiento;

import java.util.List;
import com.tecsup.SGA.modelo.Calificacion;
public interface CalificacionManager {
	public List getAllCalificacion(String codEtapa, String codTipoEvaluacion);
	
	public String InsertCalificacion(Calificacion obj, String usuario, String estReg);

	public String UpdateCalificacion(Calificacion obj, String usuario, String estReg);

	public String DeleteCalificaciones(Calificacion obj, String usuario);
}
