package com.tecsup.SGA.service.reclutamiento;

import java.util.List;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.TipoDetalle;
import com.tecsup.SGA.web.reclutamiento.command.TiposConsultaCommand;
public interface TablaDetalleManager {
	public List getAllTablaDetalle(String codPadre, String codDetalle, String descripcion
			, String valor1, String valor2, String valor3, String valor4
			, String valor5, String tipo );
	
	public List getAllAreasInteresReclutamiento(String codCabeceras, String codDetalles,String tipo);
	
	public List getAllUnidadFuncional();
	
	/**METODO PARA RETORNAR LAS INTITUCIONES
	 * @param codGiroInsti
	 * @return
	 */
	public List getAllInstitucion(String codGiroInsti);
	
	public String InsertTablaDetalle(TipoTablaDetalle obj, String usuario);

	public String UpdateTablaDetalle(TipoTablaDetalle obj,String usuario);
	
	public String DeleteTablaDetalle(TipoTablaDetalle obj,String usuario);

	public String InsertarTablaDetalle(TipoTablaDetalle obj,String usuario);
	
	public String EliminarTablaDetalle(TipoTablaDetalle obj,String usuario);
	
	public String ActualizarTablaDetalle(TipoTablaDetalle obj,String usuario);

}
