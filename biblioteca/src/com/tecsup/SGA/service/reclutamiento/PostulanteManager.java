
package com.tecsup.SGA.service.reclutamiento;

import java.util.List;

public interface PostulanteManager {
	/**
	 * @param nombres
	 * @param apellidos
	 * @param areasInteres
	 * @param profesion
	 * @param egresado
	 * @param grado
	 * @param codProceso
	 * @param codEtapa
	 * @return Listado de postulantes para un proceso de revisi�n.
	 */
	public List getAllPostulante(String nombres, String apellidos, String areasInteres
			, String profesion, String egresado, String grado
			, String codProceso, String codEtapa); 
	
	public List GetPostulanteByEmail(String emailPostulante);
}
