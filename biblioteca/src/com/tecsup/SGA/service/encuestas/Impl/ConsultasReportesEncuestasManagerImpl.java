package com.tecsup.SGA.service.encuestas.Impl;

import java.math.BigDecimal;
import java.util.*;



import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.tecsup.SGA.DAO.encuestas.ConsultasReportesEncuestasDAO;
import com.tecsup.SGA.bean.ConsultaByProfesorConBean;
import com.tecsup.SGA.service.encuestas.ConsultasReportesEncuestasManager;


 
public class ConsultasReportesEncuestasManagerImpl implements ConsultasReportesEncuestasManager{
	ConsultasReportesEncuestasDAO consultasReportesEncuestasDAO;
	
	public void setConsultasReportesEncuestasDAO(
			ConsultasReportesEncuestasDAO consultasReportesEncuestasDAO) {
		this.consultasReportesEncuestasDAO = consultasReportesEncuestasDAO;
	}
	
	private static Log log = LogFactory.getLog(ConsultasReportesEncuestasManagerImpl.class);
	
	public List getAllProfConsolidadaPFR(String codEncuesta,
			String codProfesor) { 

		try{	List lstResultado	= null	;
				List lstTemp 		= consultasReportesEncuestasDAO.getAllProfConsolidadaPFR(codEncuesta, codProfesor);

				if( lstTemp!=null && lstTemp.size()>0 )
				{
					ConsultaByProfesorConBean obj = null;//OBJETO CASTING														
					List lst = null	;						
					lstResultado = new ArrayList();				
					ConsultaByProfesorConBean obj2 = null;						
					int i=0;							
					
					while( i<lstTemp.size() )
					{
						lst = new ArrayList();
						obj = (ConsultaByProfesorConBean) lstTemp.get(i);		
						
						String xCodSeccion = obj.getCodSeccion();
						String yCodSeccion = obj.getCodSeccion();
						String xCodProducto = obj.getCodProducto();
						String yCodProducto = obj.getCodProducto();
						String xCodCurso = obj.getCodCurso();
						String yCodCurso = obj.getCodCurso();
						String xCodSeccionAlumno = obj.getCodSeccionAlumno();
						String yCodSeccionAlumno = obj.getCodSeccionAlumno();
						
						String xCodGrupo = obj.getIdGrupo();
						String yCodGrupo = obj.getIdGrupo();
												
						int k = 0;
						BigDecimal sum = new BigDecimal("0");
						
						//COMPARANDO LOS QUIEBRES
						while( 	
								xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
								xCodProducto.equalsIgnoreCase(yCodProducto) &&
								xCodCurso.equalsIgnoreCase(yCodCurso) &&								
								xCodSeccionAlumno.equalsIgnoreCase(yCodSeccionAlumno) &&
								i<lstTemp.size()								
							 )
						{
							obj2 = new ConsultaByProfesorConBean();							
							obj2 = (ConsultaByProfesorConBean)lstTemp.get(i);
							//lst.add(obj2);
							i++;
							k++;
							
							if( i<lstTemp.size() ){
								ConsultaByProfesorConBean ObjNext = ( (ConsultaByProfesorConBean)lstTemp.get(i) );
								yCodSeccionAlumno = ObjNext.getCodSeccionAlumno();
								yCodSeccion = ObjNext.getCodSeccion();
								yCodCurso = ObjNext.getCodCurso();
								yCodProducto = ObjNext.getCodProducto();
								yCodGrupo = ObjNext.getIdGrupo();
								String numero = obj2.getTotalGrupo().replace(',', '.');
								sum = sum.add(new BigDecimal(numero) );
								
								if(  
										/*xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
										xCodTipoPersonal.equalsIgnoreCase(yCodTipoPersonal) &&
										xCodTipoDocente.equalsIgnoreCase(yCodTipoDocente) &&*/
										!xCodGrupo.equalsIgnoreCase(yCodGrupo) 
										
								  ){
									log.info(">>3>>");
									obj2.setSumTotalGrupo(sum.toString());
									lst.add(obj2);
									xCodGrupo = yCodGrupo;
									sum = new BigDecimal("0");
								}else if(
										
										!xCodSeccion.equalsIgnoreCase(yCodSeccion) ||
										!xCodProducto.equalsIgnoreCase(yCodProducto) ||
										!xCodCurso.equalsIgnoreCase(yCodCurso) ||								
										!xCodSeccionAlumno.equalsIgnoreCase(yCodSeccionAlumno) 
										
								){
									log.info(">>6>>");
									obj2.setSumTotalGrupo(sum.toString());
									lst.add(obj2);
									xCodGrupo = yCodGrupo;
									sum = new BigDecimal("0");								
								}
								
							}else{
								log.info(">>4>>");
								String numero = obj2.getTotalGrupo().replace(',', '.');
								sum = sum.add( new BigDecimal(numero) );
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								sum = new BigDecimal("0");
							}	
						}
								
						obj.setListaGrupos(lst);
						lstResultado.add(obj);				
					}							
				}			
					
				return lstResultado;

			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
				return null;
				
	}

	public List getAllProfConsolidadaPCC(String codEncuesta, String codProfesor) {
	
		try{	List lstResultado	= null	;
				List lstTemp 		= consultasReportesEncuestasDAO.getAllProfConsolidadaPCC(codEncuesta, codProfesor);
		
				if( lstTemp!=null && lstTemp.size()>0 )
				{
					ConsultaByProfesorConBean obj = null;//OBJETO CASTING														
					List lst = null	;						
					lstResultado = new ArrayList();				
					ConsultaByProfesorConBean obj2 = null;						
					int i=0;							
					
					while( i<lstTemp.size() )
					{
						lst = new ArrayList();
						obj = (ConsultaByProfesorConBean) lstTemp.get(i);		
						
						String xCodSeccion = obj.getCodSeccion();
						String yCodSeccion = obj.getCodSeccion();
						String xCodProducto = obj.getCodProducto();
						String yCodProducto = obj.getCodProducto();
						String xCodCurso = obj.getCodCurso();
						String yCodCurso = obj.getCodCurso();
						//String xCodSeccionAlumno = obj.getCodSeccionAlumno();
						//String yCodSeccionAlumno = obj.getCodSeccionAlumno();
						String xCodGrupo = obj.getIdGrupo();
						String yCodGrupo = obj.getIdGrupo();
						
						int k = 0;
						BigDecimal sum = new BigDecimal("0");
												
						//HASTA CURSO QUIEBRE						
						while( 	
								xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
								xCodProducto.equalsIgnoreCase(yCodProducto) &&
								xCodCurso.equalsIgnoreCase(yCodCurso) &&								
								//xCodSeccionAlumno.equalsIgnoreCase(yCodSeccionAlumno) &&
								i<lstTemp.size()								
							 )
						{
							obj2 = new ConsultaByProfesorConBean();							
							obj2 = (ConsultaByProfesorConBean)lstTemp.get(i);
							//lst.add(obj2);	
							i++;
							k++;
							
							
							if( i<lstTemp.size() ){
								ConsultaByProfesorConBean ObjNext = ( (ConsultaByProfesorConBean)lstTemp.get(i) );
								//yCodSeccionAlumno = ObjNext.getCodSeccionAlumno();
								yCodSeccion = ObjNext.getCodSeccion();
								yCodCurso = ObjNext.getCodCurso();
								yCodProducto = ObjNext.getCodProducto();
								yCodGrupo = ObjNext.getIdGrupo();
								String numero = obj2.getTotalGrupo().replace(',', '.');
								sum = sum.add(new BigDecimal(numero) );
								
								if(  
										/*xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
										xCodTipoPersonal.equalsIgnoreCase(yCodTipoPersonal) &&
										xCodTipoDocente.equalsIgnoreCase(yCodTipoDocente) &&*/
										!xCodGrupo.equalsIgnoreCase(yCodGrupo) 
										
								  ){
									log.info(">>3>>");
									obj2.setSumTotalGrupo(sum.toString());
									lst.add(obj2);
									xCodGrupo = yCodGrupo;
									sum = new BigDecimal("0");
								}else if(										
										!xCodSeccion.equalsIgnoreCase(yCodSeccion) ||
										!xCodProducto.equalsIgnoreCase(yCodProducto) ||
										!xCodCurso.equalsIgnoreCase(yCodCurso) 
								){
									log.info(">>6>>");
									obj2.setSumTotalGrupo(sum.toString());
									lst.add(obj2);
									xCodGrupo = yCodGrupo;
									sum = new BigDecimal("0");								
								}
								
							}else{
								log.info(">>4>>");
								String numero = obj2.getTotalGrupo().replace(',', '.');
								sum = sum.add( new BigDecimal(numero) );
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								sum = new BigDecimal("0");
							}	
						}
								
						obj.setListaGrupos(lst);
						lstResultado.add(obj);				
					}							
				}			
					
				return lstResultado;
		
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		return null;
		
	}

	public List getAllEstandarConsEgresado(String codEncuesta) {
		try{	List lstResultado	= null	;
		List lstTemp 		= consultasReportesEncuestasDAO.getAllEstandarConsEgresado(codEncuesta);

		if( lstTemp!=null && lstTemp.size()>0 )
		{
			ConsultaByProfesorConBean obj = null;//OBJETO CASTING														
			List lst = null	;						
			lstResultado = new ArrayList();				
			ConsultaByProfesorConBean obj2 = null;						
			int i=0;							
			
			while( i<lstTemp.size() )
			{
				lst = new ArrayList();
				obj = (ConsultaByProfesorConBean) lstTemp.get(i);		
				
				String xCodSeccion = obj.getCodSeccion();
				String yCodSeccion = obj.getCodSeccion();
				String xAnioEgresado = obj.getAnioEgresado();
				String yAnioEgresado = obj.getAnioEgresado();
				
				String xCodGrupo = obj.getIdGrupo();
				String yCodGrupo = obj.getIdGrupo();
				
				//VALORES DE INICIO
				int k = 0;
				BigDecimal sum = new BigDecimal("0");
				
				//HASTA CURSO QUIEBRE						
				while( 	
						xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
						xAnioEgresado.equalsIgnoreCase(yAnioEgresado) &&
						i<lstTemp.size()								
					 )
				{
					obj2 = new ConsultaByProfesorConBean();							
					obj2 = (ConsultaByProfesorConBean)lstTemp.get(i);
					//lst.add(obj2);	
					i++;
					k++;
					
					if( i<lstTemp.size() ){
						ConsultaByProfesorConBean ObjNext = ( (ConsultaByProfesorConBean)lstTemp.get(i) );
						yCodSeccion = ObjNext.getCodSeccion();
						yAnioEgresado = ObjNext.getAnioEgresado();
						
						yCodGrupo = ObjNext.getIdGrupo();
						String numero = obj2.getTotalGrupo().replace(',', '.');
						sum = sum.add(new BigDecimal(numero) );
						
						if(
								!xCodGrupo.equalsIgnoreCase(yCodGrupo) 
								
						  ){
							log.info(">>3>>");
							obj2.setSumTotalGrupo(sum.toString());
							lst.add(obj2);
							xCodGrupo = yCodGrupo;
							sum = new BigDecimal("0");
						}else if(
								!xCodSeccion.equalsIgnoreCase(yCodSeccion) ||
								!xAnioEgresado.equalsIgnoreCase(yAnioEgresado)
						){
							log.info(">>6>>");
							obj2.setSumTotalGrupo(sum.toString());
							lst.add(obj2);
							xCodGrupo = yCodGrupo;
							sum = new BigDecimal("0");								
						}

					}else{
						log.info(">>4>>");
						String numero = obj2.getTotalGrupo().replace(',', '.');
						sum = sum.add( new BigDecimal(numero) );
						obj2.setSumTotalGrupo(sum.toString());
						lst.add(obj2);
						sum = new BigDecimal("0");
					}
				}
						
				obj.setListaGrupos(lst);
				lstResultado.add(obj);				
			}							
		}			
			
		return lstResultado;

	}
	catch(Exception ex)
	{
		ex.printStackTrace();
	}
		return null;
	}

	public List getAllEstandarConsAlumno(String codEncuesta) {
	try{
		 List lstResultado	= null	;
		 List lstTemp 		= consultasReportesEncuestasDAO.getAllEstandarConsAlumno(codEncuesta);
		 log.info(">>getAllEstandarConsAlumno");
		if( lstTemp!=null && lstTemp.size()>0 )
		{
			ConsultaByProfesorConBean obj = null;//OBJETO CASTING														
			List lst = null	;						
			lstResultado = new ArrayList();				
			ConsultaByProfesorConBean obj2 = null;						
			int i=0;							
			
			while( i<lstTemp.size() )
			{
				lst = new ArrayList();
				obj = (ConsultaByProfesorConBean) lstTemp.get(i);		
				
				String xCodSeccion = obj.getCodSeccion();
				String yCodSeccion = obj.getCodSeccion();
				String xCodProfesor = obj.getCodProfesor();
				String yCodProfesor = obj.getCodProfesor();
				String xCodDpto = obj.getCodDpto();
				String yCodDpto = obj.getCodDpto();				
				String xCodProducto = obj.getCodProducto();
				String yCodProducto= obj.getCodProducto();
				String xCodCurso = obj.getCodCurso();
				String yCodCurso = obj.getCodCurso();
				
				String xCodGrupo = obj.getIdGrupo();
				String yCodGrupo = obj.getIdGrupo();
				
				int k = 0;
				BigDecimal sum = new BigDecimal("0");
				//HASTA CURSO QUIEBRE
				while(
						xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
						xCodProfesor.equalsIgnoreCase(yCodProfesor) &&
						xCodDpto.equalsIgnoreCase(yCodDpto) &&
						xCodProducto.equalsIgnoreCase(yCodProducto) &&
						xCodCurso.equalsIgnoreCase(yCodCurso) &&
						i<lstTemp.size()								
					 )
				{
					obj2 = new ConsultaByProfesorConBean();							
					obj2 = (ConsultaByProfesorConBean)lstTemp.get(i);
					//lst.add(obj2);	
					i++;
					
					k++;
					if( i<lstTemp.size() ){
						
							ConsultaByProfesorConBean ObjNext = ( (ConsultaByProfesorConBean)lstTemp.get(i) );
							yCodSeccion = ObjNext.getCodSeccion();
							yCodProfesor = ObjNext.getCodProfesor();
							yCodDpto = ObjNext.getCodDpto();
							yCodProducto = ObjNext.getCodProducto();
							yCodCurso = ObjNext.getCodCurso();
							yCodGrupo = ObjNext.getIdGrupo();							
							
							
							String numero = obj2.getTotalGrupo().replace(',', '.');
							sum = sum.add( new BigDecimal(numero) );
							
							if(
									xCodGrupo.equalsIgnoreCase(yCodGrupo) &&
									xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
									xCodProfesor.equalsIgnoreCase(yCodProfesor) &&
									xCodDpto.equalsIgnoreCase(yCodDpto) &&
									xCodProducto.equalsIgnoreCase(yCodProducto) &&
									xCodCurso.equalsIgnoreCase(yCodCurso)		
							){
														
								
							}else if(	!xCodGrupo.equalsIgnoreCase(yCodGrupo) &&
									xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
									xCodProfesor.equalsIgnoreCase(yCodProfesor) &&
									xCodDpto.equalsIgnoreCase(yCodDpto) &&
									xCodProducto.equalsIgnoreCase(yCodProducto) &&
									xCodCurso.equalsIgnoreCase(yCodCurso)								
									
							){
								
								
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								xCodGrupo = yCodGrupo;
								sum = new BigDecimal("0");
							}else if(
									!xCodSeccion.equalsIgnoreCase(yCodSeccion) ||
									!xCodProfesor.equalsIgnoreCase(yCodProfesor) ||
									!xCodDpto.equalsIgnoreCase(yCodDpto) ||
									!xCodProducto.equalsIgnoreCase(yCodProducto) ||
									!xCodCurso.equalsIgnoreCase(yCodCurso)								
									
							){							
								
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								xCodGrupo = yCodGrupo;
								sum = new BigDecimal("0");								
							}
						
						
					}else{
							
						String numero = obj2.getTotalGrupo().replace(',', '.');
						sum = sum.add( new BigDecimal(numero) );
						obj2.setSumTotalGrupo(sum.toString());
						lst.add(obj2);
						sum = new BigDecimal("0");
					}	
				}
						
				obj.setListaGrupos(lst);
				lstResultado.add(obj);				
			}							
		}			
			
		return lstResultado;

	   }
	   catch(Exception ex)
	   {
		 ex.printStackTrace();
	   }
			return null;
	}

	public List getAllEstandarConsPersonal(String codEncuesta) {
		try{ 
			 List lstResultado	= null	;
			 List lstTemp 		= consultasReportesEncuestasDAO.getAllEstandarConsPersonal(codEncuesta);
			 log.info(">>getAllEstandarConsPersonal");
			if( lstTemp!=null && lstTemp.size()>0 )
			{
				ConsultaByProfesorConBean obj = null;//OBJETO CASTING														
				List lst = null	;						
				lstResultado = new ArrayList();				
				ConsultaByProfesorConBean obj2 = null;						
				int i=0;							
				
				while( i<lstTemp.size() )
				{
					lst = new ArrayList();
					obj = (ConsultaByProfesorConBean) lstTemp.get(i);		
					
					String xCodSeccion = obj.getCodSeccion();
					String yCodSeccion = obj.getCodSeccion();
					String xCodTipoPersonal = obj.getCodTipoPersonal();
					String yCodTipoPersonal = obj.getCodTipoPersonal();					
					String xCodTipoDocente = obj.getCodTipoDocente();
					String yCodTipoDocente = obj.getCodTipoDocente();				
					String xCodGrupo = obj.getIdGrupo();
					String yCodGrupo = obj.getIdGrupo();
					/*String xCodCurso = obj.getCodCurso();
					String yCodCurso = obj.getCodCurso();*/
					
					//HASTA CURSO QUIEBRE
					//VALORES DE INICIO
					int k = 0;
					BigDecimal sum = new BigDecimal("0");
					
					while(
							xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
							xCodTipoPersonal.equalsIgnoreCase(yCodTipoPersonal) &&
							xCodTipoDocente.equalsIgnoreCase(yCodTipoDocente) &&
							//xCodGrupo.equalsIgnoreCase(yCodGrupo) &&
							i<lstTemp.size()								
						 )
					{	
						
						obj2 = new ConsultaByProfesorConBean();							
						obj2 = (ConsultaByProfesorConBean)lstTemp.get(i);
						
						/*if(!xCodGrupo.equalsIgnoreCase(yCodGrupo) || k ==0 )
						{lst.add(obj2); xCodGrupo = yCodGrupo; }*/
						
						i++;						
						k++;
						
						if( i<lstTemp.size() ){
							
							ConsultaByProfesorConBean ObjNext = ( (ConsultaByProfesorConBean)lstTemp.get(i) );
							yCodSeccion = ObjNext.getCodSeccion();
							yCodTipoPersonal = ObjNext.getCodTipoPersonal();
							yCodTipoDocente = ObjNext.getCodTipoDocente();
							yCodGrupo = ObjNext.getIdGrupo();
							
							String numero = obj2.getTotalGrupo().replace(',', '.');
							sum = sum.add(new BigDecimal(numero) );
							//SI ES DIFERENTE AL FUTURO COMPARAR
							
							if(  									
									!xCodGrupo.equalsIgnoreCase(yCodGrupo) &&
									xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
									xCodTipoPersonal.equalsIgnoreCase(yCodTipoPersonal) &&
									xCodTipoDocente.equalsIgnoreCase(yCodTipoDocente)									
							  ){								
								
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								xCodGrupo = yCodGrupo;
								sum = new BigDecimal("0");
							}else if(
									!xCodSeccion.equalsIgnoreCase(yCodSeccion) ||
									!xCodTipoPersonal.equalsIgnoreCase(yCodTipoPersonal) ||
									!xCodTipoDocente.equalsIgnoreCase(yCodTipoDocente) 	
							){
								
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								xCodGrupo = yCodGrupo;
								sum = new BigDecimal("0");								
							}
							
							
						}else{
							
							String numero = obj2.getTotalGrupo().replace(',', '.');
							sum = sum.add( new BigDecimal(numero) );
							obj2.setSumTotalGrupo(sum.toString());
							lst.add(obj2);
							sum = new BigDecimal("0");
						}
					}
					log.info("**********");		
					obj.setListaGrupos(lst);
					lstResultado.add(obj);				
				}							
			}			
				
			return lstResultado;

		   }
		   catch(Exception ex)
		   {
			 ex.printStackTrace();
		   }
				return null;
	}

	public List getAllEstandarConsServicio(String codEncuesta) {
		try{ 
			 List lstResultado	= null	;
			 List lstTemp 		= consultasReportesEncuestasDAO.getAllEstandarConsServicio(codEncuesta);
			 log.info(">>getAllEstandarConsServicio");
			if( lstTemp!=null && lstTemp.size()>0 )
			{
				ConsultaByProfesorConBean obj = null;//OBJETO CASTING														
				List lst = null	;						
				lstResultado = new ArrayList();				
				ConsultaByProfesorConBean obj2 = null;						
				int i=0;							
				
				while( i<lstTemp.size() )
				{
					lst = new ArrayList();
					obj = (ConsultaByProfesorConBean) lstTemp.get(i);		
					
					String xCodSeccion = obj.getCodSeccion();
					String yCodSeccion = obj.getCodSeccion();
					String xCodPerfil = obj.getCodPerfil();
					String yCodPerfil = obj.getCodPerfil();					
					//String xCodTipoDocente = obj.getCodTipoDocente();
					//String yCodTipoDocente = obj.getCodTipoDocente();
					String xCodGrupo = obj.getIdGrupo();
					String yCodGrupo = obj.getIdGrupo();

					int k = 0;
					BigDecimal sum = new BigDecimal("0");
					//HASTA CURSO QUIEBRE
					while(
							xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
							xCodPerfil.equalsIgnoreCase(yCodPerfil) &&							
							i<lstTemp.size()								
						 )
					{
						obj2 = new ConsultaByProfesorConBean();							
						obj2 = (ConsultaByProfesorConBean)lstTemp.get(i);
						//lst.add(obj2);	
						i++;
						
						if( i<lstTemp.size() ){
							ConsultaByProfesorConBean ObjNext = ( (ConsultaByProfesorConBean)lstTemp.get(i) );
							yCodSeccion = ObjNext.getCodSeccion();
							yCodPerfil = ObjNext.getCodPerfil();
							
							yCodGrupo = ObjNext.getIdGrupo();
							String numero = obj2.getTotalGrupo().replace(',', '.');
							sum = sum.add(new BigDecimal(numero) );
							
							if(  	xCodSeccion.equalsIgnoreCase(yCodSeccion) &&
									xCodPerfil.equalsIgnoreCase(yCodPerfil) &&	
									!xCodGrupo.equalsIgnoreCase(yCodGrupo) 
									
							  ){
								log.info(">>3>>");
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								xCodGrupo = yCodGrupo;
								sum = new BigDecimal("0");
							}else if(									
									!xCodSeccion.equalsIgnoreCase(yCodSeccion) ||
									!xCodPerfil.equalsIgnoreCase(yCodPerfil)
							){
								log.info(">>6>>");
								obj2.setSumTotalGrupo(sum.toString());
								lst.add(obj2);
								xCodGrupo = yCodGrupo;
								sum = new BigDecimal("0");								
							}
						}else{
							log.info(">>4>>");
							String numero = obj2.getTotalGrupo().replace(',', '.');
							sum = sum.add( new BigDecimal(numero) );
							obj2.setSumTotalGrupo(sum.toString());
							lst.add(obj2);
							sum = new BigDecimal("0");
						}	
					}
							
					obj.setListaGrupos(lst);
					lstResultado.add(obj);				
				}							
			}			
				
			return lstResultado;

		   }
		   catch(Exception ex)
		   {
			 ex.printStackTrace();
		   }
				return null;
	}
	
	public List getAllValidaRptaReporte(String tipoReporte, String codEncuesta,String codResponsable) {
		return consultasReportesEncuestasDAO.getAllValidaRptaReporte( tipoReporte,  codEncuesta, codResponsable) ;
	}
	
	public List GetAllConsultaByProfesorDetPRFCer(String codEncuesta, String codProfesor) {
		try
		{				
			return consultasReportesEncuestasDAO.GetAllConsultaByProfesorDetPRFCer(codEncuesta, codProfesor);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllConsultaByProfesorDetPRFAbi(String codEncuesta, String codProfesor) {
		try{				
			return consultasReportesEncuestasDAO.GetAllConsultaByProfesorDetPFRAbi(codEncuesta, codProfesor);							
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllConsultaByProfesorDetPCCCer(String codEncuesta, String codProfesor) {
		try
		{				
			return consultasReportesEncuestasDAO.GetAllConsultaByProfesorDetPCCCer(codEncuesta, codProfesor);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllConsultaByProfesorDetPCCAbi(String codEncuesta, String codProfesor) {
		try{				
			return consultasReportesEncuestasDAO.GetAllConsultaByProfesorDetPCCAbi(codEncuesta, codProfesor);										
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}	 
	public List GetAllEstandarDetalleAlumnoCer(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetalleAlumnoCer(codEncuesta);													
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllEstandarDetalleAlumnoAbi(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetalleAlumnoAbi(codEncuesta);																
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllEstandarDetalleEgresadoCer(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetalleEgresadoCer(codEncuesta);																
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllEstandarDetalleEgresadoAbi(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetalleEgresadoAbi(codEncuesta);																			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllEstandarDetallePersonalCer(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetallePersonalCer(codEncuesta);																			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllEstandarDetallePersonalAbi(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetallePersonalAbi(codEncuesta);																			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllEstandarDetalleServicioCer(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetalleServicioCer(codEncuesta);																						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllEstandarDetalleServicioAbi(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllEstandarDetalleServicioAbi(codEncuesta);																						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllMixtaDetalleCerrada(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllMixtaDetalleCerrada(codEncuesta);																									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllMixtaDetalleAbierta(String codEncuesta) {
		try{				
			return consultasReportesEncuestasDAO.GetAllMixtaDetalleAbierta(codEncuesta);																												
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
