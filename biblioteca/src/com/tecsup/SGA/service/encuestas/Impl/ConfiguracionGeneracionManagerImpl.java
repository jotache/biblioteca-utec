package com.tecsup.SGA.service.encuestas.Impl;

import java.util.*;



import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.bean.DetalleEvalParcial;
import com.tecsup.SGA.bean.DetalleEvalParcialCab;
import com.tecsup.SGA.bean.EncuestaManualBean;
import com.tecsup.SGA.bean.FormatoEncuestaBean;
import com.tecsup.SGA.bean.OpcionesBean;
import com.tecsup.SGA.bean.PreguntaBean;
import com.tecsup.SGA.bean.VerEncuestaBean;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;
import com.tecsup.SGA.web.encuestas.controller.AplicacionSeguimientoRegistroManualFormController;

 
public class ConfiguracionGeneracionManagerImpl implements ConfiguracionGeneracionManager{
	ConfiguracionGeneracionDAO configuracionGeneracionDAO;
	ConfiguracionGeneracionManager configuracionGeneracionManager;
	
	private static Log log = LogFactory.getLog(ConfiguracionGeneracionManagerImpl.class);

	public void setConfiguracionGeneracionManager(ConfiguracionGeneracionManager configuracionGeneracionManager) {
		this.configuracionGeneracionManager = configuracionGeneracionManager;
	}
	public void setConfiguracionGeneracionDAO(ConfiguracionGeneracionDAO configuracionGeneracionDAO) {
		this.configuracionGeneracionDAO = configuracionGeneracionDAO;
	}
	
	public String insertFormato(String tipoAplicacion, String tipoServicio,
			String tipoEncuesta, String numero, String nombre, String duracion,
			String observacion, String codSede, String usuCrea) {
		try
		{  					
			return configuracionGeneracionDAO.insertFormato(tipoAplicacion, tipoServicio, tipoEncuesta, numero, nombre, duracion, observacion, codSede, usuCrea);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public String obtenertxhCodEncuestadoByEncuestado(String nroEncuesta){
		try {
			
			return configuracionGeneracionDAO.obtenertxhCodEncuestadoByEncuestado(nroEncuesta);
			
		} catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	
	public String obtenerCodEstadoEncuesta(String nroEncuesta){
		try {
			
			return configuracionGeneracionDAO.obtenerCodEstadoEncuesta(nroEncuesta);
			
		} catch (Exception ex){ex.printStackTrace();}
		
		return null;
	}
	
	public String updateFormato(String idEncuesta, String tipoAplicacion,
			String tipoServicio, String tipoEncuesta, String numero,
			String nombre, String duracion, String observacion, String codSede,
			String usuCrea) {
		try
		{  					
			return configuracionGeneracionDAO.updateFormato(idEncuesta, tipoAplicacion, tipoServicio, tipoEncuesta, numero, nombre, duracion, observacion, codSede, usuCrea);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
		
	}
	
	public List<TipoTablaDetalle> GetAllConfiguracionPerfil( String codTabla, String codDetalle){
		try {
			return configuracionGeneracionDAO.GetAllConfiguracionPerfil(codTabla, codDetalle);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public AplicacionDatosGeneralesCommand getFormato(
			AplicacionDatosGeneralesCommand control) {
		try
		{  					
			 List lstFormato = this.getAllFormato(
					null,
					null,
					null,
					null,				
					control.getTxhCodigo(),"",""
					);
			
			if( lstFormato != null &&  lstFormato.size() > 0 ){
				
					FormatoEncuesta obj = (FormatoEncuesta) lstFormato.get(0);
					control.setTxhCodigo(obj.getCodEncuesta());
					control.setCboTipoAplicacion(obj.getCodTipoAplicacion());
					control.setCboTipoEncuesta(obj.getCodTipoEncuesta());
					control.setCboSede(obj.getCodSede());
					control.setTxtCodigo(obj.getNroEncuesta());
					control.setTxtNombre(obj.getNombreEncuesta());
					control.setTxtDuracion(obj.getDuracion());
					control.setCboTipoServicio(obj.getCodTipoServicio());
					control.setTxaObservacion(obj.getObsEncuesta());
					control.setTxhCodEstado(obj.getCodEstado());
			}else{
				control.setTxhCodigo(null);
				control.setCboTipoAplicacion(null);
				control.setCboTipoEncuesta(null);
				control.setCboSede(null);
				control.setTxtCodigo(null);
				control.setTxtNombre(null);
				control.setTxtDuracion(null);
				control.setCboTipoServicio(null);
				control.setTxaObservacion(null);
				control.setTxhCodEstado(null);
			}
			
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return control;
	}
			
	public List<FormatoEncuesta> getAllFormato(String codTipoAplicacion, 
			String codTipoServicio, String codTipoEncuesta, String codTipoEstado, String nombreEncuesta,
			String codResponsable, String nroEncuesta)
	{
		try
		{
		
		log.info("VALOR_BUSCADO_CONTROLLER_IMPLE: " + nombreEncuesta +" =============================================================================");
			
		 ArrayList<FormatoEncuesta> arrSolicitudes = (ArrayList<FormatoEncuesta>)this.
		 configuracionGeneracionDAO.getAllFormato(codTipoAplicacion, codTipoServicio, 
				 codTipoEncuesta, codTipoEstado, nombreEncuesta, codResponsable, nroEncuesta);		 
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	
	}

	public List getAllAlternativa(String codEncuesta,String codGrupo,String codPregunta,String codAlternativa) {
		try
		{				
			return configuracionGeneracionDAO.getAllAlternativa(codEncuesta,codGrupo,codPregunta,codAlternativa);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String insertAlternativa(String codEncuesta, String codGrupo,
			String codPregunta, String alternativa, String descripcion,
			String peso, String usuCrea) {
		try
		{				
			return configuracionGeneracionDAO.insertAlternativa(codEncuesta, codGrupo, codPregunta, alternativa, descripcion, peso, usuCrea);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String updateAlternativa(
			String codEncuesta,
			String codAlternativa,
			String codGrupo,
			String codPregunta,
			String alternativa,
			String descripcion,
			String peso,			
			String usuCrea){
		try
		{				
			return configuracionGeneracionDAO.updateAlternativa(
					 codEncuesta,
					 codAlternativa,
					 codGrupo,
					 codPregunta,
					 alternativa,
					 descripcion,
					 peso,			
					 usuCrea
					);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String deleteAlternativa(String codEncuesta, String codAlternativa,
			String usuario) {
		try
		{				
			return configuracionGeneracionDAO.deleteAlternativa(
					 codEncuesta,
					 codAlternativa,
					 usuario
					);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String insertGruposByEncuesta(String codEncuesta, String codSeccion,
			String nomGrupo, String peso, String indAlternativa, String usuCrea) {
		try
		{				
			return configuracionGeneracionDAO.insertGruposByEncuesta(
					codEncuesta,
					codSeccion,
					nomGrupo,
					peso,
					indAlternativa,
					usuCrea);				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String deleteGruposByEncuesta(String codEncuesta, String codGrupo,
			String usuario) {
		try
		{				
			return configuracionGeneracionDAO.deleteGruposByEncuesta( codEncuesta,  codGrupo,
					 usuario);				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllGruposByEncuesta(String codEncuesta, String codGrupo) {
		try
		{				
			return configuracionGeneracionDAO.getAllGruposByEncuesta( codEncuesta,  codGrupo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllPreguntasByGrupo(String codEncuesta, String codGrupo,
			String codPregunta) {
		try
		{				
			return configuracionGeneracionDAO.getAllPreguntasByGrupo(  codEncuesta,  codGrupo,
					 codPregunta);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String deletePreguntasByGrupo(String codEncuesta,
			String codPregunta, String usuario) {
		try
		{				
			return configuracionGeneracionDAO.deletePreguntasByGrupo(
					 codEncuesta,
					 codPregunta,
					 usuario);				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String insertPreguntasByGrupo(String codEncuesta, String codGrupo,
			String pregunta, String tipoPregunta, String indObligatorio,
			String indUnica, String peso, String indAlternativa, String usuCrea) {
		try
		{				
			return configuracionGeneracionDAO.insertPreguntasByGrupo(
					codEncuesta,
					codGrupo,
					pregunta,
					tipoPregunta,
					indObligatorio,
					indUnica,
					peso,
					indAlternativa,
					usuCrea);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String updatePreguntasByGrupo(String codEncuesta, String codGrupo,
			String codPregunta, String pregunta, String tipoPregunta,
			String indObligatorio, String indUnica, String peso,
			String indAlternativa, String usuCrea) {
		try
		{				
			return configuracionGeneracionDAO.updatePreguntasByGrupo(
					codEncuesta,
					codGrupo,
					codPregunta,
					pregunta, 
					tipoPregunta, 
					indObligatorio, 
					indUnica, 
					peso,
					indAlternativa,
					usuCrea);		

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List getAllFormatoEncuesta(String codEncuesta) {
		try
		{				
			return configuracionGeneracionDAO.getAllFormatoEncuesta(codEncuesta);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String updateGruposByEncuesta(String codEncuesta, String codSeccion,
			String codGrupo, String nomGrupo, String peso,
			String indAlternativa, String usuCrea) {
		try
		{				
			return configuracionGeneracionDAO.updateGruposByEncuesta(
					 codEncuesta,  codSeccion,
					 codGrupo,  nomGrupo,  peso,
					 indAlternativa,  usuCrea);		

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<FormatoEncuesta> GetAllEncuestaByFormato(String codFormatoEncuesta)
	{try
		{
		 ArrayList<FormatoEncuesta> arrSolicitudes = (ArrayList<FormatoEncuesta>)this.
		 configuracionGeneracionDAO.GetAllEncuestaByFormato(codFormatoEncuesta);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	
	}
	
	public String UpdateConfiguracionPerfil(String codEncuesta, String codResponsable,
			String indicadorManual, String usuario) {
		try{  					
			return configuracionGeneracionDAO.UpdateConfiguracionPerfil(codEncuesta, codResponsable, indicadorManual, usuario);				
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	
	public String UpdateEncuestaByFormato(String codFormatoEncuesta, String cadCodSeccion, String nroTotalReg, 
			String codUsuario) {
		try
		{  					
			return configuracionGeneracionDAO.UpdateEncuestaByFormato(codFormatoEncuesta, cadCodSeccion, nroTotalReg, codUsuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
		
	}
	/**/
	public List<Usuario> GetAllDatosPersonal(String dscPrimerNombre, String dscSegundoNombre, String dscApellidoPat, 
			String dscApellidoMat, String codTipoPersonal)
	{try
		{
		 ArrayList<Usuario> arrSolicitudes = (ArrayList<Usuario>)this.
		 configuracionGeneracionDAO.GetAllDatosPersonal(dscPrimerNombre, dscSegundoNombre, dscApellidoPat, dscApellidoMat, 
				 codTipoPersonal);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List cargaFormularioEncuesta(String codEncuesta) {
		try
		{
			List lstPreguntas =null;
			List resultado = null;
			PreguntaBean pregunta = null;
			FormatoEncuestaBean preguntaBD = null;
			OpcionesBean opcion = null;
			List lstOpciones = null;
			
			lstPreguntas = configuracionGeneracionDAO.GetAllAgregarFormatoManual(codEncuesta);
			if( lstPreguntas!=null && lstPreguntas.size()>0 )
			{
				int i=0;

				resultado = new ArrayList();
				
				while( i<lstPreguntas.size() )
				{
					  pregunta = new PreguntaBean();
					  lstOpciones = new ArrayList();
					  
					  
					  preguntaBD = (FormatoEncuestaBean) lstPreguntas.get(i);
					  String x = preguntaBD.getIdPregunta();
					  String y = preguntaBD.getIdPregunta();
					  
					  pregunta.setCodPregunta(x);
					  pregunta.setCodEncuesta(codEncuesta);
					  pregunta.setFlgMultiple( ((preguntaBD.getIndUnica().equalsIgnoreCase("1"))?"0":"1") );
					  pregunta.setFlgObligatorio(preguntaBD.getIndObligatorio());
					  pregunta.setFlgCombo(preguntaBD.getTipoPregunta().equalsIgnoreCase("0002")?"1":"0");
					  pregunta.setDesPregunta(preguntaBD.getNomPregunta());
						
					  while( x.equalsIgnoreCase(y) && i<lstPreguntas.size() )
					  {
						  	preguntaBD = (FormatoEncuestaBean)lstPreguntas.get(i);	
						  	
						  	opcion = new OpcionesBean();							
						  	opcion.setCodOpcion(preguntaBD.getIdAlternativa());
						  	opcion.setNomCodOpcionABC(preguntaBD.getNomAlternativa());
						  	opcion.setNomOpcion(preguntaBD.getDesAlternativa());
							
						  	lstOpciones.add(opcion);
								
							i++;
							if( i<lstPreguntas.size() )
								y = ( (FormatoEncuestaBean)lstPreguntas.get(i) ).getIdPregunta();
														
					  }
					  
					  pregunta.setLstOpciones(lstOpciones);
					  
					  log.info("***********Pregunta***********");
					  log.info("CodPregunta>>"+pregunta.getCodPregunta()+">>");
					  log.info("FlgMultiple>>"+pregunta.getFlgMultiple()+">>");
					  log.info("FlgCombo>>"+pregunta.getFlgCombo()+">>");
					  log.info("FlgObligatorio>>"+pregunta.getFlgObligatorio()+">>");
					  log.info("NUM ALTERN>>"+pregunta.getLstOpciones()==null?"0":pregunta.getLstOpciones().size()+">>");
					  log.info("DesPregunta>>"+pregunta.getDesPregunta()+">>");
					  
					  					  
					  resultado.add(pregunta);	
				
				 }					
					
					
			}
			
			return resultado;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	/*
	 * PERFILES
	 */
	

	public List GetAllAgregarFormatoManual(String codEncuesta) {
		try{				
			return configuracionGeneracionDAO.GetAllAgregarFormatoManual(codEncuesta);																								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	

	public String insertAgregarFormatoManual(String codEncuesta,
			String cadIdPreguntaCerrada, String cadIdRespuestaCerrada,
			String nroIdPreguntaCerrada, String cadIdPreguntaAbierta,
			String cadIdRespuestaAbierta, String nroIdPreguntaAbierta,
			String usuCrea, String indRegistro) {
		try{  					
			return configuracionGeneracionDAO.insertAgregarFormatoManual(
					codEncuesta,
					cadIdPreguntaCerrada,
					cadIdRespuestaCerrada,
					nroIdPreguntaCerrada,
					cadIdPreguntaAbierta,
					cadIdRespuestaAbierta,
					nroIdPreguntaAbierta,
					usuCrea,
					indRegistro);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;
	}

	public List getAllEncuestaManual(String codEncuesta) {
		try{				
			return configuracionGeneracionDAO.getAllEncuestaManual(codEncuesta);																								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List getPreparaGrillaRespuestaxEncuestado(String codEncuesta) {
		
		List lstResultado	= null	;
		List lstTemp = configuracionGeneracionManager.getAllEncuestaManual(codEncuesta);
		

		if( lstTemp!=null && lstTemp.size()>0 )
		{						
			EncuestaManualBean obj = null;
			String tempCodAlu 	= ""	;
			String temp2CodAlu 	= ""	;
			
			List lst	= null	;
			
			lstResultado = new ArrayList();
			
			
			EncuestaManualBean obj2 = null;			
			EncuestaManualBean obj1 = null;
			
			int i=0;
						
			while( i<lstTemp.size() )
			{	
				lst = new ArrayList();
				obj = (EncuestaManualBean) lstTemp.get(i);			
				
				String x = obj.getCodEncuestado();
				String y = obj.getCodEncuestado();				
				obj1  = new EncuestaManualBean();
				
				obj1.setCodEncuestado(obj.getCodEncuestado());
					
				while( x.equalsIgnoreCase(y) && i<lstTemp.size() )
				{			
					obj2 = new EncuestaManualBean();
					
					obj2 = (EncuestaManualBean)lstTemp.get(i);
					lst.add(obj2);
					
					i++;
					if( i<lstTemp.size() )
					y = ( (EncuestaManualBean)lstTemp.get(i) ).getCodEncuestado();												
				}
					
				obj1.setLstPreguntas(lst);
				lstResultado.add(obj1);			
			}						
		}		
		return lstResultado;
	}
	public String deleteEncuestaManual(String codEncuesta, String codPerfil,
			String epdec_id, String codUsuario) {
		try
		{				
			return configuracionGeneracionDAO.deleteEncuestaManual(
					 codEncuesta,codPerfil,epdec_id,codUsuario
					);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String updateGeneraEncuesta(String codEncuesta, String usuCrea) {
		try
		{				
			return configuracionGeneracionDAO.updateGeneraEncuesta(codEncuesta, usuCrea);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertCopiaFormatoEncuesta(String codEncuesta) {
		try{				
			return configuracionGeneracionDAO.InsertCopiaFormatoEncuesta(codEncuesta);							
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	public String DeleteEncuestaFormato(String codEncuesta, String codUsuario) {
		try{				
			return configuracionGeneracionDAO.DeleteEncuestaFormato(codEncuesta, codUsuario);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllEncuestasPublicadas(String codUsuario) {
		try
		{				
			return configuracionGeneracionDAO.GetAllEncuestasPublicadas(codUsuario);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
		
	/*public List getAllVerEncuesta(String codEncuesta) {

	try{	List lstResultado	= null	;
			List lstTemp 		= configuracionGeneracionDAO.getAllVerEncuesta(codEncuesta);

			if( lstTemp!=null && lstTemp.size()>0 )
			{
				VerEncuestaBean obj = null;
				String tempCodAlu 	= "";
				String temp2CodAlu 	= "";				
				List lst = null	;
				
				lstResultado = new ArrayList();				
				VerEncuestaBean obj2 = null;			
				List listAltFinal = new ArrayList();
				
				int i=0;							
				while( i<lstTemp.size() )
				{
					lst = new ArrayList();
					obj = (VerEncuestaBean) lstTemp.get(i);		
					
					String x = obj.getIdPregunta();
					String y = obj.getIdPregunta();
					
					//log.info(i+"tamanio de una lista creada>>"+lst.size()+">");					
					
					while( x.equalsIgnoreCase(y) && i<lstTemp.size() )
					{
						obj2 = new VerEncuestaBean();							
						obj2 = (VerEncuestaBean)lstTemp.get(i);
						lst.add(obj2);	i++;
						
						if( i<lstTemp.size() )
						y = ( (VerEncuestaBean)lstTemp.get(i) ).getIdPregunta();												
					}

					
					obj.setLstAlternativas(lst);
					
					
					//******NUEVO CAMBIO***************
					

					if( i<lstTemp.size() ){
						VerEncuestaBean objFuturo = (VerEncuestaBean)lstTemp.get(i) ;
						
						String gActual = obj.getIdGrupo();
						String gFuturo = objFuturo.getIdGrupo();
						
						//if( !gActual.equalsIgnoreCase(gFuturo) && obj.getNivelAlternativa().equalsIgnoreCase("3") ){
						if( !gActual.equalsIgnoreCase(gFuturo) ){
							if( listAltFinal.size()>0 ){
								obj.setLstAlternativas(listAltFinal);								
								listAltFinal = new ArrayList();
								//log.info("a");
							}
						}else if( !obj.getNivelAlternativa().equalsIgnoreCase("3") ){
								
								if( listAltFinal.size()==0 ){
									listAltFinal = obj.getLstAlternativas();									
									//log.info("b");
								}
						}
						
						//SI HA CAMBIADO DE GRUPO RESETEAR LA LISTA TEMPORAL
						if( !gActual.equalsIgnoreCase(gFuturo) ){
							listAltFinal = new ArrayList();
						}
						
					}else if(i == lstTemp.size()){
						VerEncuestaBean objAnterior = (VerEncuestaBean)lstTemp.get(i-2) ;
						String gAct = obj.getIdGrupo();
						String gAnt = objAnterior.getIdGrupo();

						if( gAct.equalsIgnoreCase(gAnt) ){
							//log.info("c");
								//if( listAltFinal.size()>0 && obj.getNivelAlternativa().equalsIgnoreCase("3") ){
								if( listAltFinal.size()>0 ){
									obj.setLstAlternativas(listAltFinal);
									listAltFinal = new ArrayList();
									//log.info("d");
								}
						}
					}
					log.info("********PREGUNTA*********");
					log.info("grupo>>"+obj.getIdGrupo()+">>");
					log.info("NomPregunta>>"+obj.getNomPregunta()+">>");
					log.info("NivelAlternativa>>"+obj.getNivelAlternativa()+">>");
					log.info("Numero de alternativas>>"+obj.getLstAlternativas().size()+">>");
					//***************FIN NUVEO CAMBIO******************
					lstResultado.add(obj);				
				}							
			}			
			
			return lstResultado;

	}
	catch(Exception ex)
	{
		ex.printStackTrace();
	}
		return null;
	}*/
	
	public List getAllVerEncuesta(String codEncuesta) { 

		try{	List lstResultado	= null	;
				List lstTemp 		= configuracionGeneracionDAO.getAllVerEncuesta(codEncuesta);

				if( lstTemp!=null && lstTemp.size()>0 )
				{
					VerEncuestaBean obj = null;
					String tempCodAlu 	= "";
					String temp2CodAlu 	= "";				
					List lst = null	;
					
					lstResultado = new ArrayList();				
					VerEncuestaBean obj2 = null;			

					
					int i=0;							
					while( i<lstTemp.size() )
					{
						lst = new ArrayList();
						obj = (VerEncuestaBean) lstTemp.get(i);		
						
						String x = obj.getIdPregunta();
						String y = obj.getIdPregunta();
						
						/***/
						String a = obj.getIdGrupo();
						String b = obj.getIdGrupo();
						/***/
						
						
						while( x.equalsIgnoreCase(y) && i<lstTemp.size() )
						{
							obj2 = new VerEncuestaBean();							
							obj2 = (VerEncuestaBean)lstTemp.get(i);
							lst.add(obj2);	i++;
							
							if( i<lstTemp.size() )
							y = ( (VerEncuestaBean)lstTemp.get(i) ).getIdPregunta();												
						}
							
						obj.setLstAlternativas(lst);
						lstResultado.add(obj);				
					}							
				}			
				
				return lstResultado;

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
			return null;
		}	
	
	public String insertRespuestaByEncuestado(String codEncuesta,
			String codPerfil, String epdecId, String cadIdPreguntaCerrada,
			String cadIdRespuestaCerrada, String nroIdPreguntaCerrada,
			String cadIdPreguntaAbierta, String cadIdRespuestaAbierta,
			String nroIdPreguntaAbierta, String usuCrea) {
		try
		{				
			return configuracionGeneracionDAO.insertRespuestaByEncuestado(codEncuesta, codPerfil, epdecId, cadIdPreguntaCerrada, cadIdRespuestaCerrada, nroIdPreguntaCerrada, cadIdPreguntaAbierta, cadIdRespuestaAbierta, nroIdPreguntaAbierta, usuCrea);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//public DatosCabeceraBean getAllDatosCabeceraEncuesta(String codEncuesta, String codProfesor,String codProducto,String codPrograma) {
	public DatosCabeceraBean getAllDatosCabeceraEncuesta(String codEncuesta, String codProfesor) {	
			
		try{				
			 //List lst = configuracionGeneracionDAO.getAllDatosCabeceraEncuesta( codEncuesta,codProfesor,codProducto,codPrograma);
			List lst = configuracionGeneracionDAO.getAllDatosCabeceraEncuesta( codEncuesta,codProfesor);
			 
			 if(lst!=null && lst.size()>0 && lst.get(0)!=null)
				 return (DatosCabeceraBean)lst.get(0);
			 else 
				 return null;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List getAllEncFormatoBusqueda(String codTipoAplicacion,
			String codTipoServicio, String codTipoEncuesta,
			String codTipoEstado, String nombreEncuesta, String codResponsable,
			String nroEncuesta, String nomEncuesta) {
		try{				
			 //List lst = configuracionGeneracionDAO.getAllDatosCabeceraEncuesta( codEncuesta,codProfesor,codProducto,codPrograma);
			return configuracionGeneracionDAO.getAllEncFormatoBusqueda(  codTipoAplicacion,
					 codTipoServicio,  codTipoEncuesta,
					 codTipoEstado,  nombreEncuesta,  codResponsable,
					 nroEncuesta,  nomEncuesta);
				 
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return null;
	}
	public String marcarRespuestasAbiertas(String nroEncuesta,
			String codPerfilEnc,String codRespAbierta) {
		
		try {
			return configuracionGeneracionDAO.marcarRespuestasAbiertas(nroEncuesta, codPerfilEnc, codRespAbierta);
		} 
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
