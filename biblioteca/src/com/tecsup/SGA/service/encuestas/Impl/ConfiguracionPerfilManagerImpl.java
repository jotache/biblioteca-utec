package com.tecsup.SGA.service.encuestas.Impl;

import java.util.*;



import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.encuestas.ConfiguracionGeneracionDAO;
import com.tecsup.SGA.DAO.encuestas.ConfiguracionPerfilDAO;
import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.bean.DetalleEvalParcial;
import com.tecsup.SGA.bean.DetalleEvalParcialCab;
import com.tecsup.SGA.bean.EncuestaManualBean;
import com.tecsup.SGA.bean.FormatoEncuestaBean;
import com.tecsup.SGA.bean.OpcionesBean;
import com.tecsup.SGA.bean.PreguntaBean;
import com.tecsup.SGA.bean.VerEncuestaBean;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.service.encuestas.ConfiguracionGeneracionManager;
import com.tecsup.SGA.service.encuestas.ConfiguracionPerfilManager;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;
import com.tecsup.SGA.web.encuestas.controller.AplicacionSeguimientoRegistroManualFormController;

public class ConfiguracionPerfilManagerImpl implements ConfiguracionPerfilManager{
	
	ConfiguracionPerfilDAO configuracionPerfilDAO;	

	public void setConfiguracionPerfilDAO(
			ConfiguracionPerfilDAO configuracionPerfilDAO) {
		this.configuracionPerfilDAO = configuracionPerfilDAO;
	}
	public List GetFechaHoraSistema() {
		try
		{				
			return configuracionPerfilDAO.GetFechaHoraSistema();			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String updateTerminarEncuesta(String idEncuesta, String usuCrea) {
		try
		{				
			return configuracionPerfilDAO.updateTerminarEncuesta(idEncuesta, usuCrea);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllPerfilesByEncuesta(String codEncuesta){
		try{				
			return configuracionPerfilDAO.GetAllPerfilesByEncuesta(codEncuesta);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllProductos(String codProducto, String codProductoNo){
		try{				
			return configuracionPerfilDAO.GetAllProductos(codProducto, codProductoNo);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllProgramas(String codProducto, String codPrograma, String nomPrograma){
		try{				
			return configuracionPerfilDAO.GetAllProgramas(codProducto, codPrograma, nomPrograma);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllDepartamentos(String codDepartamento, String nomDepartamento){
		try{				
			return configuracionPerfilDAO.GetAllDepartamentos(codDepartamento, nomDepartamento);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllCursos(String codCurso, String nomCurso, String codProducto, 
			String codPrograma, String codDepartamento, String codCiclo){
		try{				
			return configuracionPerfilDAO.GetAllCursos(codCurso, nomCurso, codProducto, codPrograma, codDepartamento, codCiclo);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllCiclos(String codPrograma, String codCiclo){
		try{				
			return configuracionPerfilDAO.GetAllCiclos(codPrograma, codCiclo);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String InsertPerfilEncuesta(PerfilEncuesta obj, String usuario) {
		try{  					
			return configuracionPerfilDAO.InsertPerfilEncuesta(obj.getCodTipoEncuesta(), obj.getCodEncuesta(), 
					obj.getCodPerfil(), obj.getCodProducto(), obj.getCodPrograma(), obj.getCodCiclo(), 
					obj.getCodDepartamento(), obj.getCodCurso(), obj.getCodTipoPersonal(), obj.getCodTipoDocente(), 
					obj.getAñoIniEgresado(), obj.getAñoFinEgresado(), usuario);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;

		
	}
	public List GetAllPerfilEncuesta(String codEncuesta, String codPerfil){
		try{				
			return configuracionPerfilDAO.GetAllPerfilEncuesta(codEncuesta, codPerfil);												
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllTipoPersonal(String codTipoPersonal, String nomTipoPersonal){
		try{				
			return configuracionPerfilDAO.GetAllTipoPersonal(codTipoPersonal, nomTipoPersonal);															
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllTipoDocente(String codTipoPersonal, String codTipoDocente, String nomTipoDocente){
		try{				
			return configuracionPerfilDAO.GetAllTipoDocente(codTipoPersonal, codTipoDocente, nomTipoDocente);																		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllProfesorPFR(String codEncuesta, String codPerfil){
		try{				
			return configuracionPerfilDAO.GetAllProfesorPFR(codEncuesta, codPerfil);																		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllProfesorPCC(String codEncuesta, String codPerfil){
		try{				
			return configuracionPerfilDAO.GetAllProfesorPCC(codEncuesta, codPerfil);																					
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllAmbitoGeneral(String codEncuesta, String codPerfil){
		try{				
			return configuracionPerfilDAO.GetAllAmbitoGeneral(codEncuesta, codPerfil);																								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String AplicarEncuesta(String codEncuesta, String fechaInicio, 
			String horaInicio, String fechaFin, String horaFin, String usuario) {
		try{  					
			return configuracionPerfilDAO.AplicarEncuesta(codEncuesta, 
					fechaInicio, horaInicio, fechaFin, horaFin, usuario);				
		}
		catch(Exception ex){ex.printStackTrace();}
		
		return null;		
	}
	public List GetAmpliarProgByEncuesta(String codEncuesta) {
		try
		{				
			return configuracionPerfilDAO.GetAmpliarProgByEncuesta(codEncuesta);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String AmpliarProgramacionEncuesta(String codEncuesta, String fechaInicio, 
			String horaInicio, String fechaFin, String horaFin, String codUsuario) {
		try{  					
			return configuracionPerfilDAO.AmpliarProgramacionEncuesta(codEncuesta, 
					fechaInicio, horaInicio, fechaFin, horaFin, codUsuario);
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;		
	}
	public String deleteAmbitoProfesor(String codEncuesta, String codPerfil, String codProfesor, 
			String codProducto, String codPrograma, String codCentroCosto, 
			String codCurso, String codCiclo, String codSeccion, String codUsuario) {
		try{				
			return configuracionPerfilDAO.deleteAmbitoProfesor(codEncuesta, codPerfil, codProfesor, 
					codProducto, codPrograma, codCentroCosto, codCurso, codCiclo, codSeccion, codUsuario);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	public String obtener_correo(String codUsuario) {
		
		try {
			return configuracionPerfilDAO.obtener_correo(codUsuario);
		} 
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return null;
	}
}
