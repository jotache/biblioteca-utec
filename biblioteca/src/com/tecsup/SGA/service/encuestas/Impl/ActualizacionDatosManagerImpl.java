package com.tecsup.SGA.service.encuestas.Impl;

import java.util.ArrayList;
import java.util.List;

import com.tecsup.SGA.DAO.encuestas.ActualizacionDatosDAO;
import com.tecsup.SGA.modelo.EncuestaAlumno;
import com.tecsup.SGA.modelo.EncuestaEmpresa;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.service.encuestas.ActualizacionDatosManager;

public class ActualizacionDatosManagerImpl implements ActualizacionDatosManager{

	ActualizacionDatosDAO actualizacionDatosDAO;
	
	public List<EncuestaAlumno> GetAllEncuestaAlumno(String codAlumno)
	{try
		{
		 ArrayList<EncuestaAlumno> arrSolicitudes = (ArrayList<EncuestaAlumno>)this.
		 actualizacionDatosDAO.GetAllEncuestaAlumno(codAlumno);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public String UpdateEncuestaAlumno(String codAlumno, EncuestaAlumno encuestaAlumno,String codUsuario) {
		try{  					
			return actualizacionDatosDAO.UpdateEncuestaAlumno(codAlumno, encuestaAlumno, codUsuario);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}//
	
	public List<EncuestaEmpresa> GetAllEncuestaEmpresa(String nombreEmpresa) {
		try{  					
			return actualizacionDatosDAO.GetAllEncuestaEmpresa(nombreEmpresa);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}//
	
	public List<EncuestaAlumno> GetVerificarAlumno(String codAlumno)
	{try
		{
		 ArrayList<EncuestaAlumno> arrSolicitudes = (ArrayList<EncuestaAlumno>)this.
		 actualizacionDatosDAO.GetVerificarAlumno(codAlumno);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<Usuario> GetVerificarTieneEncuesta(String codUsuario){
		try{
			ArrayList<Usuario> lista = (ArrayList<Usuario>)this.
			actualizacionDatosDAO.GetVerificarTieneEncuesta(codUsuario);
			
			return lista;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public ActualizacionDatosDAO getActualizacionDatosDAO() {
		return actualizacionDatosDAO;
	}

	public void setActualizacionDatosDAO(ActualizacionDatosDAO actualizacionDatosDAO) {
		this.actualizacionDatosDAO = actualizacionDatosDAO;
	}
}
