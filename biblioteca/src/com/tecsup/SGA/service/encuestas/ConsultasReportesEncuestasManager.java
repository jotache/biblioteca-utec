package com.tecsup.SGA.service.encuestas;

import java.util.List;
import com.tecsup.SGA.web.encuestas.command.AplicacionDatosGeneralesCommand;
import com.tecsup.SGA.bean.DatosCabeceraBean;
import com.tecsup.SGA.modelo.PerfilEncuesta;
import com.tecsup.SGA.modelo.TipoTablaDetalle;
import com.tecsup.SGA.modelo.FormatoEncuesta;
import com.tecsup.SGA.modelo.Usuario;
public interface ConsultasReportesEncuestasManager {
	
	/**REPORTE CONSOLIDADO PFR IMPLEMENTA LOGICA DEL NEGOCIO PARA EL PINTADO DE LOS REPORTES
	 * @param codEncuesta
	 * @param codProfesor
	 * @return
	 */
	public List getAllProfConsolidadaPFR(String codEncuesta,
			String codProfesor);
	
	/**REPORTE CONSOLIDADO PCC IMPLEMENTA LOGICA DEL NEGOCIO PARA EL PINTADO DE LOS REPORTES
	 * @param codEncuesta
	 * @param codProfesor
	 * @return
	 */
	public List getAllProfConsolidadaPCC(String codEncuesta,
			String codProfesor);
	
	/**REPORTE STANDAR PARA EGRESADOS IMPLEMENTA LOGICA DEL NEGOCIO PARA EL PINTADO DE LOS REPORTES
	 * @param codEncuesta
	 * @return
	 */
	public List getAllEstandarConsEgresado(String codEncuesta) ;
	
	/**REPORTE STANDAR PARA ALUMNO IMPLEMENTA LOGICA DEL NEGOCIO PARA EL PINTADO DE LOS REPORTES
	 * @param codEncuesta
	 * @return
	 */
	public List getAllEstandarConsAlumno(String codEncuesta);
	
	/**REPORTE STANDAR PARA PERSONL IMPLEMENTA LOGICA DEL NEGOCIO PARA EL PINTADO DE LOS REPORTES
	 * @param codEncuesta
	 * @return
	 */
	public List getAllEstandarConsPersonal(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List getAllEstandarConsServicio(String codEncuesta);
	
	public List getAllValidaRptaReporte(String tipoReporte,String codEncuesta,String codResponsable);
	
	/**
	 * @param codEncuesta
	 * @param codProfesor
	 * @return
	 */
	public List GetAllConsultaByProfesorDetPRFCer(String codEncuesta, String codProfesor);
	
	/**
	 * @param codEncuesta
	 * @param codProfesor
	 * @return
	 */
	public List GetAllConsultaByProfesorDetPRFAbi(String codEncuesta, String codProfesor);
	
	/**
	 * @param codEncuesta
	 * @param codProfesor
	 * @return
	 */
	public List GetAllConsultaByProfesorDetPCCCer(String codEncuesta, String codProfesor);
	
	/**
	 * @param codEncuesta
	 * @param codProfesor
	 * @return
	 */
	public List GetAllConsultaByProfesorDetPCCAbi(String codEncuesta, String codProfesor);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetalleAlumnoCer(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetalleAlumnoAbi(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetalleEgresadoCer(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetalleEgresadoAbi(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetallePersonalCer(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetallePersonalAbi(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetalleServicioCer(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllEstandarDetalleServicioAbi(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllMixtaDetalleCerrada(String codEncuesta);
	
	/**
	 * @param codEncuesta
	 * @return
	 */
	public List GetAllMixtaDetalleAbierta(String codEncuesta);
}

