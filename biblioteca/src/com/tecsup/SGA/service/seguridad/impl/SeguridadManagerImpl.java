package com.tecsup.SGA.service.seguridad.impl;

import java.sql.SQLException;
import java.util.List;

import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.modelo.CentroCosto;
import com.tecsup.SGA.modelo.UsuarioSeguriBib;
import com.tecsup.SGA.service.seguridad.SeguridadManager;
import com.tecsup.SGA.DAO.SeguridadDAO;
import com.tecsup.SGA.DAO.SedeDAO;
import com.tecsup.SGA.DAO.CentroCostoDAO;

public class SeguridadManagerImpl implements SeguridadManager {
	private SeguridadDAO seguridadDAO;
	private SedeDAO sedeDAO;
	private CentroCostoDAO centroCostoDAO;
	
	public int getValidaUsuario(String usuario, String clave) {
		try
		{
			return this.seguridadDAO.getValidaUsuario(usuario, clave);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return 0;
	
	}

	public int getValidaUsuarioCeditec(String usuario, String clave) {
		try
		{
			return this.seguridadDAO.getValidaUsuarioCeditec(usuario, clave);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return 0;
	}
	
	public List<SeguridadBean> getOpciones(String usuario, String sistema) {
		try
		{
			return this.seguridadDAO.getOpciones(usuario, sistema);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<SeguridadBean> getOpcionesCeditec(String usuario, String sistema) {
		try
		{
			return this.seguridadDAO.getOpcionesCeditec(usuario, sistema);
		}
		catch(Exception ex){		
			ex.printStackTrace();
		}
		return null;
	}
	
	public UsuarioSeguridad getDatosUsuario(String codUsuario) {
		try
		{
			return this.seguridadDAO.getDatosUsuario(codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List<SedeBean> getAllSedes() {
		try
		{
			return this.sedeDAO.GetAllSedes();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<CentroCosto> GetAllCentrosCosto(String sede, String perfil, String codUsuario) {
		try
		{
			return this.centroCostoDAO.GetAllCentrosCosto(sede,perfil,codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public void setSeguridadDAO(SeguridadDAO seguridadDAO) {
		this.seguridadDAO = seguridadDAO;
	}
	public void setSedeDAO(SedeDAO sedeDAO) {
		this.sedeDAO = sedeDAO;
	}
	public void setCentroCostoDAO(CentroCostoDAO centroCostoDAO) {
		this.centroCostoDAO = centroCostoDAO;
	}

	//JHPR 2008-04-09 Para permitir el cambio de contraseņa.
	public int GrabarFirma(String usuario, String clave) {
		try {
			return this.seguridadDAO.GrabarFirma(usuario, clave);
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
		return 0;
	}

	//JHPR: 2008-04-11 Registrar inicio de sesion en "seguridad.seg_log"
	public int RegistrarSesion(boolean ingreso, String estacioncliente, String usuario,
			String sistema) {
		
		try {
			return this.seguridadDAO.RegistrarSesion(ingreso,estacioncliente, usuario, sistema);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}	
	
	public List<UsuarioPerfil> getPerfiles(String usuario, String sistema) {
		try
		{
			return this.seguridadDAO.getPerfiles(usuario, sistema);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public UsuarioSeguriBib obtenerUsuarioSeguriBiblio(String codsujeto,String idUsuario) {
		try
		{
			return this.seguridadDAO.obtenerUsuarioSeguriBiblio(codsujeto,idUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public UsuarioSeguridad getDatosUsuarioBiblio(String username) {
		try
		{
			return this.seguridadDAO.getDatosUsuarioBiblio(username);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public UsuarioSeguridad getDatosUsuarioBiblioUTEC(String username) {
		try
		{
			return this.seguridadDAO.getDatosUsuarioBiblioCeditec(username);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public int GrabarFirmaCeditec(String usuario, String clave) {
		try {
			return this.seguridadDAO.GrabarFirmaCeditec(usuario, clave);
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
		return 0;
	}

	public boolean esUsuarioTecsup(String username) {
		try {
			return this.seguridadDAO.esUsuarioTecsup(username);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}

	public UsuarioSeguridad ObtenerEstadoUsuario(UsuarioSeguridad user) {
		try {
			return this.seguridadDAO.ObtenerEstadoUsuario(user);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}




}
