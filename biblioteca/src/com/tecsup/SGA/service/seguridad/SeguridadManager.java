package com.tecsup.SGA.service.seguridad;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.tecsup.SGA.bean.SeguridadBean;
import com.tecsup.SGA.bean.UsuarioPerfil;
import com.tecsup.SGA.bean.UsuarioSeguridad;
import com.tecsup.SGA.bean.SedeBean;
import com.tecsup.SGA.modelo.CentroCosto;
import com.tecsup.SGA.modelo.Usuario;
import com.tecsup.SGA.modelo.UsuarioSeguriBib;

public interface SeguridadManager {
	/**
	 * @param usuario
	 * @param clave
	 * @return
	 */
	public int getValidaUsuario(String usuario, String clave);
	
	public int getValidaUsuarioCeditec(String usuario, String clave);
	/**
	 * @param usuario
	 * @param sistema
	 * @return Opciones de usuario por sistema
	 */
	public List<SeguridadBean> getOpciones(String usuario, String sistema);
	
	public List<SeguridadBean> getOpcionesCeditec(String usuario, String sistema);
	
	/**
	 * @param codUsuario
	 * @return Obtiene los datos del usuario (idUsuario, Nombre)
	 */
	public UsuarioSeguridad getDatosUsuario(String codUsuario);
	
	/**
	 * @return Lista de sedes de TECSUP
	 */
	public List<SedeBean> getAllSedes();
	
	/**
	 * @param sede
	 * @return Retorna los centros de costo de una determinada sede.
	 */
	public List<CentroCosto> GetAllCentrosCosto(String sede, String perfil, String codUsuario);
	
	//JHPR 2008-04-09 Para permitir el cambio de contraseņa.
	public int GrabarFirma(String usuario, String clave);
	
	public int GrabarFirmaCeditec(String usuario, String clave);

	//JHPR: 2008-04-11 Registrar inicio de sesion en "seguridad.seg_log"
	public int RegistrarSesion(boolean ingreso, String estacioncliente, String usuario, String sistema);
	
	/**
	 * @param usuario
	 * @param sistema
	 * @return CACT: 2008-04-15 - Retorna los perfiles de un usuario para un determinado sistema
	 */
	public List<UsuarioPerfil> getPerfiles(String usuario, String sistema);
	
	public UsuarioSeguriBib obtenerUsuarioSeguriBiblio(String codsujeto,String idUsuario);
	
	/**
	 * Datos del usuarios logeado con mas datos para acceso a otras bibliotecas.
	 * @param username
	 * @return
	 */
	public UsuarioSeguridad getDatosUsuarioBiblio(String username);
	public UsuarioSeguridad getDatosUsuarioBiblioUTEC(String username);
	
	public boolean esUsuarioTecsup(String username);
	public UsuarioSeguridad ObtenerEstadoUsuario(UsuarioSeguridad user);
}
