package com.tecsup.SGA.service.logistica;

import java.util.List;

import com.tecsup.SGA.modelo.Almacen;
import com.tecsup.SGA.modelo.Devolucion;
import com.tecsup.SGA.modelo.DevolucionDetalle;
import com.tecsup.SGA.modelo.GuiaRemision;

public interface AlmacenManager {
//ALQD,23/01/09. NUEVO PARAMETRO DE BUSQUEDA
	public List<Almacen> GetAllBienesRecepcion(String codSede, String nroOrden, String fecEmiOrdIni,
			String fecEmiOrdFin, 
   		 String nroRucProveedor, String dscProveedor, String dscComprador, String nroGuia, 
		 String fecEmiGuiaIni, String fecEmiGuiaFin, String indIngPorConfirmar);
	
	public List GetAllBienesDevolucion(String codSede, String fecDevIni, String fecDevFin, String codCeco, 
	   		 String dsvUsu, String codEstado);
	
	public List<Almacen> GetAllDatosOrden(String codOrden);
	
	public List<GuiaRemision> GetAllGuiaRemisionAsociadas(String codOrden);
	
	public List<GuiaRemision> GetAllDatosGuiaRemision(String codDocPago);
	
	public List<GuiaRemision> GetAllDetalleGuiaRemision(String codDocPago);
	
	public String UpdateGuiaRemision(String codDocPago, String  cadCodDetDocPag,String cadCantidad, 
			String cantidad, String codUsuario);
	
	public String UpdateCerrarGuiaRemision(String codDocPago, String codUsuario);
	
	public String ActConfirmarDev(String codReqDev, String codUsuario,String observacion);
	
	public String ActRechazarDev(String codReqDev, String codUsuario,String observacion);
	
	public String ActAbrirAlmacen(String sede);
	
	public String ActCerrarAlmacen(String sede);
	
	public List<Devolucion> GetDatosDevolucion(String codDevolucion);
	
	public List<DevolucionDetalle> GetAllDetalleDevolucion(String codDevolucion);
	

	public String InsertSeriesActivo(String codDocPago, String codBien,String valorCompra,
			String cadCodIng,String cadNroSerie,String cantidad,String codUsuario,String codDetalle);
	
	public String DelSeriesActivos(String codDocPago, String codBien,String codIngBien,String codUsuario);
	
	public List GetAllSeriesActivos(String codDocPago,String codBien,String codDetalle);
	
	public List getAllImpresionTickets(String codSede);
	
	public List getAllInventario(String codSede, String respuesta);	
	
	public String insertInventario(String codSede, String cadCodBienes, String cadCanBienes, 
			String cantidad, String codUsuario);
	
	public String actEnviarInventario(String codSede, String codUsuario);
	
	public String actRechazarInventario(String codSede, String codUsuario);
	
	public String actAprobarInventario(String codSede, String codUsuario);
	
	public String getEstadoAlmacen(String codSede);
}
