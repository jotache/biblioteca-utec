package com.tecsup.SGA.service.logistica.Impl;

import java.util.List;
import java.util.ArrayList;
import java.text.*;

import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.modelo.SolRequerimientoDetalle;
import com.tecsup.SGA.service.logistica.SolRequerimientoManager;
import com.tecsup.SGA.DAO.logistica.SolRequerimientoDAO;
import com.tecsup.SGA.modelo.ReportesLogistica;

public class SolRequerimientoManagerImpl implements SolRequerimientoManager {

	SolRequerimientoDAO solRequerimientoDAO;
	
	
	public List<SolRequerimiento> getAllSolReqUsuario(String codSede,
			String tipoReq, String nroReq, String fecIni, String fecFin,
			String cecoReq, String estadoReq, String usuSolicitante) {
		NumberFormat formatter = new DecimalFormat("#########.00");
		try
		{
			ArrayList<SolRequerimiento> arrSolicitudes = (ArrayList<SolRequerimiento>)this.solRequerimientoDAO.getAllSolReqUsuario(
					codSede, tipoReq, nroReq, fecIni, fecFin, cecoReq, estadoReq, usuSolicitante);
			
			if ( arrSolicitudes != null )
				for( int i = 0; i < arrSolicitudes.size(); i++)
		    		if (!arrSolicitudes.get(i).getTotalRequerimiento().trim().equals("")) 
		    			arrSolicitudes.get(i).setTotalRequerimiento(formatter.format(Double.parseDouble(arrSolicitudes.get(i).getTotalRequerimiento().trim())));
			
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<SolRequerimiento> getSolReqUsuario(String codSolReq){
		try
		{
			ArrayList<SolRequerimiento> solReqUsuario = (ArrayList<SolRequerimiento>)this.solRequerimientoDAO.getSolReqUsuario(codSolReq);			
			
			return solReqUsuario;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String InsertDocRelDetSolReqAdjunto(String codReque, String codDetalleReq,
	String codDocAdjunto, String rutaDoc, String codTipoAdjunto, String codUsuario){
		try
		{   	return this.solRequerimientoDAO.InsertDocRelDetSolReqAdjunto(codReque, codDetalleReq, 
				codDocAdjunto, rutaDoc, codTipoAdjunto, codUsuario);
				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	 
	public List<SolRequerimientoDetalle> getAllSolReqDetalle(String codSolReq, String codDetSolReq){
		
		try
		{
			ArrayList<SolRequerimientoDetalle> detalleSolicitud = (ArrayList<SolRequerimientoDetalle>)this.solRequerimientoDAO.getAllSolReqDetalle(codSolReq, codDetSolReq);
		
			return detalleSolicitud;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}		
		
		return null;
	}
	
	public String InsertSolRequerimientoUsuario(SolRequerimiento obj){
		try{
			return solRequerimientoDAO.InsertSolRequerimientoUsuario(obj.getIdRequerimiento(), obj.getCecoSolicitante(), 
					obj.getSede(), obj.getUsuSolicitante(), obj.getTipoRequerimiento(), obj.getSubTipoRequerimiento(), 
					obj.getCodRequerimientoOri(), obj.getIndInversion(), obj.getIndParaStock());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}	
	
	public String DeleteSolRequerimientoDetalle(String codReq, String codDetSolReq, String codUsuario){	
		try{
			return this.solRequerimientoDAO.DeleteSolRequerimientoDetalle(codReq, codDetSolReq, codUsuario);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
		
	public String InsertSolRequerimientoDetalle(SolRequerimientoDetalle obj, String codUsuario){
		try{
			return solRequerimientoDAO.InsertSolRequerimientoDetalle(obj.getCodRequerimiento(), 
					obj.getCodigo(), obj.getNombreServicio(), obj.getDescripcion(), obj.getCodigoBien(), 
					obj.getCantidad(), obj.getTipoGasto(), obj.getFechaEntrega(), codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public void setSolRequerimientoDAO(SolRequerimientoDAO solRequerimientoDAO) {
		this.solRequerimientoDAO = solRequerimientoDAO;
	}
	
	public List<SolRequerimientoAdjunto> getAllRequerimientosAdjunto(
			String codReq, String CodDetReq, String CodAdj) {
		try{
			return solRequerimientoDAO.getAllRequerimientosAdjunto(codReq, CodDetReq, CodAdj);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String deleteRequerimientosAdjunto(String codReq, String codDetReq,
			String codAdjunto, String codUsuario) {
		try{
			return solRequerimientoDAO.deleteRequerimientosAdjunto(codReq, codDetReq, codAdjunto, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	/**/
	public List<SolRequerimiento> GetAllAproRequerimientoUsuario(String codSUsuario,
			String codPerfil, String tipoReq, String nroReq, String fechaInicio,
			String fechaFin, String codCentroCosto, String codEstado, String indGrupo
			//ccordova
			,String codSede) {
		NumberFormat formatter = new DecimalFormat("#########.00");
		try
		{
			ArrayList<SolRequerimiento> arrSolicitudes = (ArrayList<SolRequerimiento>)this.solRequerimientoDAO.GetAllAproRequerimientoUsuario(codSUsuario, 
					codPerfil, tipoReq, nroReq, fechaInicio, fechaFin, codCentroCosto, codEstado, indGrupo
					, codSede);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllAsigResp(String tipo, String sede, String ceco
    		,String nombre,String apPaterno,String apMaterno){
		try{
			return this.solRequerimientoDAO.GetAllAsigResp(tipo, sede, ceco, nombre, apPaterno, apMaterno);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String InsertResponsable(String codRequerimiento, String codResp, String usuario,String codEsfuerzo){
		try{
			return this.solRequerimientoDAO.InsertResponsable(codRequerimiento, codResp, usuario,codEsfuerzo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllDetalle(String codResp){
		try{
			return this.solRequerimientoDAO.GetAllDetalle(codResp);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String EnviarSolRequerimientoUsuario(String codRequerimiento, String codUsuario){
		try{
			return solRequerimientoDAO.EnviarSolRequerimientoUsuario(codRequerimiento, codUsuario);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,20/01/10.GUARDANDO TRES CAMPOS MAS: CCOSTO, INDINVERSION E INDPARASTOCK
	public String AprobarSolRequerimientoUsuario(String codRequerimiento
		,String codUsuario,String indicadorAccion,String cadenaCantidad
		,String codPerfil,String codCenCosto,String indInversion,String indParaStock){	
		try{
			return solRequerimientoDAO.AprobarSolRequerimientoUsuario(codRequerimiento
				,codUsuario,indicadorAccion,cadenaCantidad,codPerfil,codCenCosto
				,indInversion,indParaStock);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String RechazarSolRequerimientoUsuario(String codRequerimiento, String codUsuario){
		try{
			return solRequerimientoDAO.RechazarSolRequerimientoUsuario(codRequerimiento, codUsuario);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}	
	public String DeleteSolRequerimiento(String codReq, String codUsuario){
		try{
			return this.solRequerimientoDAO.DeleteSolRequerimiento(codReq, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertResponsableTrabajo(String codRequerimiento, String codResp,String codEsf, String usuario){
		try{
			return this.solRequerimientoDAO.InsertResponsableTrabajo(codRequerimiento, codResp, codEsf, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertSolDevolucion(SolRequerimiento obj, String codUsuario){
		try{
			return solRequerimientoDAO.InsertSolDevolucion(obj.getCodDevolucion(), obj.getIdRequerimiento(), 
					obj.getCadenaCodigo(), obj.getCadenaCantidad(), obj.getCantidad(), 
					obj.getMotivoDevolucion(), codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String EnviarSolDevolucionUsuario(String codDevolucion, String codUsuario){
		try{
			return solRequerimientoDAO.EnviarSolDevolucionUsuario(codDevolucion, codUsuario);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String GetEstadoAlmacen(String codSede){
		try{
			return solRequerimientoDAO.GetEstadoAlmacen(codSede);				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllCentrosCostos(String codSede,String descripcion){
		try{
		return this.solRequerimientoDAO.GetAllCentrosCostos(codSede,descripcion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String GetIndEmpLogistica(String codUsuario){
		try{
			return solRequerimientoDAO.GetIndEmpLogistica(codUsuario);				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,27/01/09. NUEVA FUNCION QUE OBTENDRA REQS PENDIENTE DE ENTREGA
	public List<ReportesLogistica> GetAllSolReqAprPenEntUsuario(String codUsuario){
		try{
			return this.solRequerimientoDAO.GetAllSolReqAprPenEntUsuario(codUsuario);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return null;
	}
	//ALQD,18/01/10. NUEVO METODO PARA CONSULTAR REQS APROBADOS
	public List<ReportesLogistica> GetAllSolReqAprobados(String codUsuario){
		try{
			return this.solRequerimientoDAO.GetAllSolReqAprobados(codUsuario);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return null;
	}

	public List<SolRequerimiento> getFlujoProcesosLogistica(String sede,
			String fecIni, String fecFin, String tipoInversion,
			String codUsuario, String nomComprador) {
		try{
			return this.solRequerimientoDAO.getFlujoProcesosLogistica(sede, fecIni, fecFin, tipoInversion, codUsuario, nomComprador);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return null;
	}

}
