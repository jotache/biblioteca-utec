package com.tecsup.SGA.service.logistica.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.logistica.ReportesLogisticaDAO;
import com.tecsup.SGA.modelo.ReportesLogistica;
import com.tecsup.SGA.service.logistica.ReportesLogisticaManager;

public class ReportesLogisticaManagerImpl implements ReportesLogisticaManager{

	ReportesLogisticaDAO reportesLogisticaDAO;

	public List<ReportesLogistica> GetAllIngresoAlmacen(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia) {
		try{
			return reportesLogisticaDAO.GetAllIngresoAlmacen(codSede, codTipoBien, fecInicio, fecFin,
					codTipoFamilia);
					
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<ReportesLogistica> GetAllSalidaAlmacen(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codCentroCosto, String codTipoGasto){
				try{
					return reportesLogisticaDAO.GetAllSalidaAlmacen(codSede, codTipoBien, fecInicio,
							fecFin, codCentroCosto, codTipoGasto);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	
	public List<ReportesLogistica> GetAllKardezValorizado(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia, String codBien){
				try{
					return reportesLogisticaDAO.GetAllKardezValorizado(codSede, codTipoBien, fecInicio,
							fecFin, codTipoFamilia, codBien);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	//ALQD,22/09/09.SE AGREGA UN PARAMETRO MAS PARA SALDO > 0
	public List<ReportesLogistica> GetAllMaestrosSaldos(String codSede, String codTipoBien, String periodo
			, String codTipoFamilia, String conSaldo){
				try{
					return reportesLogisticaDAO.GetAllMaestrosSaldos(codSede, codTipoBien, periodo,
							codTipoFamilia, conSaldo);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List<ReportesLogistica> GetAllCierreOperaciones(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,	String fecInicio, String fecFin,
			String codCentroCosto, String codTipoGasto){
				try{
					return reportesLogisticaDAO.GetAllCierreOperaciones(codSede, codTipoReq, codTipoBien,
							codTipoServicio, fecInicio, fecFin, codCentroCosto, codTipoGasto);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List<ReportesLogistica> GetAllAcumuladoSalidaByArticulo(String codSede, String codTipoBien,
			String fecInicio, String fecFin, String codTipoFamilia, String codBien,
			String codCentroCosto, String codTipoGasto){
				try{
					return reportesLogisticaDAO.GetAllAcumuladoSalidaByArticulo(codSede, codTipoBien,
							fecInicio, fecFin, codTipoFamilia, codBien, codCentroCosto, codTipoGasto);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List<ReportesLogistica> GetAllSolicitudCotizaciones(String codSede, String codTipoReq, String codTipoBien, String codTipoServicio,
			String fecInicio, String fecFin, String codProveedor){
				try{
					return reportesLogisticaDAO.GetAllSolicitudCotizaciones(codSede, codTipoReq,
							codTipoBien, codTipoServicio, fecInicio, fecFin, codProveedor);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List<ReportesLogistica> GetAllRequerimientoValorizado(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,
			String fecInicio , String fecFin, String usuSolicitado, String codEstadoReq){
				try{
					return reportesLogisticaDAO.GetAllRequerimientoValorizado(codSede, codTipoReq,
							codTipoBien, codTipoServicio, fecInicio, fecFin, usuSolicitado, codEstadoReq);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List<ReportesLogistica> GetAllOrdenesAsignadasByProveedor(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,
			String fecInicio , String fecFin, String indiceLocalidad, String codProveedor
			, String codTipoMoneda, String codTipoPago){
				try{
					return reportesLogisticaDAO.GetAllOrdenesAsignadasByProveedor(codSede, codTipoReq,
						 codTipoBien, codTipoServicio, fecInicio, fecFin, indiceLocalidad, codProveedor,
						 codTipoMoneda, codTipoPago);
						
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List<ReportesLogistica> GetAllProgramaCronogramaPago(String codSede, String fecInicio,
			String fecFin, String indiceValidado,
			String codTipoOrden, String codTipoPago, String codEstadoComp , String dscProveedor){
				try{
					return reportesLogisticaDAO.GetAllProgramaCronogramaPago(codSede, fecInicio, fecFin,
							indiceValidado, codTipoOrden, codTipoPago, codEstadoComp, dscProveedor);
							
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List getAllIngresoAlmacenDev(String codSede, String codTipoBien,
			String fecInicio, String fecFin, String codTipoFamilia) {
		try{
			return reportesLogisticaDAO.getAllIngresoAlmacenDev( codSede,  codTipoBien,
					 fecInicio,  fecFin,  codTipoFamilia);
					
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public ReportesLogisticaDAO getReportesLogisticaDAO() {
		return reportesLogisticaDAO;
	}

	public void setReportesLogisticaDAO(ReportesLogisticaDAO reportesLogisticaDAO) {
		this.reportesLogisticaDAO = reportesLogisticaDAO;
	}

	public List<ReportesLogistica> GetAllOrdenesInversion(String codSede,
			String fecInicio, String fecFin) {
		try{
			return reportesLogisticaDAO.GetAllOrdenesInversion(codSede, fecInicio, fecFin);
					
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
}
