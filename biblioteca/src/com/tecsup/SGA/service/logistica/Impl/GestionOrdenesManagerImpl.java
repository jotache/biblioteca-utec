package com.tecsup.SGA.service.logistica.Impl;

import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;

import com.tecsup.SGA.DAO.logistica.GestionOrdenesDAO;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.DetalleAprobacion;
import com.tecsup.SGA.modelo.DetalleOrden;
import com.tecsup.SGA.modelo.OrdenesAprobadas;
import com.tecsup.SGA.service.logistica.GestionOrdenesManager;

public class GestionOrdenesManagerImpl implements GestionOrdenesManager{

	GestionOrdenesDAO gestionOrdenesDAO;

	public List<CotizacionProveedor> GetAllBandejaConsultaOrdenes(String codSede, String codUsuario, String codPerfil, String nroOrden, 
			String fechaInicio, String fechaFin,String codEstado,
	    	String rucProvee, String nomProvee, String codComprador, String nomComprador, String codTipoOrden) {
		try{
			return gestionOrdenesDAO.GetAllBandejaConsultaOrdenes(codSede, codUsuario, codPerfil, nroOrden, fechaInicio,
					fechaFin, codEstado, rucProvee, nomProvee, codComprador, nomComprador, codTipoOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List<DetalleOrden> GetAllDetalleByOrden(String codOrden) {
		try{
			return gestionOrdenesDAO.GetAllDetalleByOrden(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List<DetalleOrden> GetAllCondicionByOrden(String codOrden) {
		try{
			return gestionOrdenesDAO.GetAllCondicionByOrden(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,19/02/09.AGREGANDO NUEVO PARAMETRO
	public String UpdateRechazarOrden(String codOrden, String codUsuario,
			String desComentario){
		try{
			return gestionOrdenesDAO.UpdateRechazarOrden(codOrden, codUsuario,
					desComentario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,19/02/09.AGREGANDO NUEVO PARAMETRO
	public String UpdateAprobarOrden(String codOrden, String codUsuario,
			String indInversion){
		try{
			return gestionOrdenesDAO.UpdateAprobarOrden(codOrden, codUsuario,
					indInversion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public List<DetalleOrden> GetAllOrdenById(String codOrden) {
		try{
			return gestionOrdenesDAO.GetAllOrdenById(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public String UpdateCerrarOrden(String codOrden, String codUsuario){
		try{
			return gestionOrdenesDAO.UpdateCerrarOrden(codOrden, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public List<OrdenesAprobadas> GetEnvioCorreoOrden(String codOrden) {
		try{
			return gestionOrdenesDAO.GetEnvioCorreoOrden(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public GestionOrdenesDAO getGestionOrdenesDAO() {
		return gestionOrdenesDAO;
	}

	public void setGestionOrdenesDAO(GestionOrdenesDAO gestionOrdenesDAO) {
		this.gestionOrdenesDAO = gestionOrdenesDAO;
	}
	
	public List getSolicitantesOrdenCompra(String codOrden) {
		Log.info("codOrden:"+codOrden);
		List resultado = new ArrayList();
		try {
			List <DetalleOrden> prods = this.gestionOrdenesDAO.getSolicitantesProdOrdComp(codOrden, "0");			
			if (prods!=null){
				DetalleOrden detOrden = null;
				for (DetalleOrden det : prods){
					detOrden = new DetalleOrden();
					detOrden.setIndTermino("C");
					detOrden.setDescripcion(det.getDescripcion());
					detOrden.setDscUnidad(det.getDscUnidad());
					detOrden.setCantidad(det.getCantidad());
					resultado.add(detOrden);
					List<DetalleOrden> detalle = this.gestionOrdenesDAO.getSolicitantesProdOrdComp(codOrden, det.getCodBien());
					if (detalle!=null) {
						for (DetalleOrden det2:detalle){
							Log.info("getCodBien:"+det.getCodBien());
							det2.setIndTermino("D");
							resultado.add(det2);
						}
					}
				}
			}
			
			return resultado;
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
//ALQD,22/10/08. IMPLEMENTANDO LA NUEVA PROPIEDAD AGREGADA.
	public List GetAllAprobadorByOC(String codOrden) {
		Log.info("codOrden:"+codOrden);
		try{
			return gestionOrdenesDAO.GetAllAprobadorByOC(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,09/01/09. IMPL. NUEVO METODO PARA REPORTE # 25
	public List GetAllOC_pendienteXentregar(String codComprador) {
		Log.info("codComprador: "+codComprador);
		try{
			return gestionOrdenesDAO.GetAllOC_pendienteXentregar(codComprador);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,15/07/09.NUEVOS METODOS PARA CONSULTAR,GRABAR Y ELIMINAR LOS SUPLENTES Y SUPLANTADOS
	public List<DetalleAprobacion> GetAllAprobacionOcSuplantandoA(String codUsuario
			, String tipoAprobacion) {
		Log.info("codUsuario: "+codUsuario);
		try{
			return gestionOrdenesDAO.GetAllAprobacionOcSuplantandoA(codUsuario, tipoAprobacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List<DetalleAprobacion> GetAllAprobacionOcSuplantadoPor(String codUsuario
			, String tipoAprobacion) {
		Log.info("codUsuario: "+codUsuario);
		try{
			return gestionOrdenesDAO.GetAllAprobacionOcSuplantadoPor(codUsuario, tipoAprobacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,16/07/09.NUEVO METODO PARA ACTUALIZAR LOS SUPLENTES DE APROBACION
	public String UpdateSuplenteAprobacionOc(String codPrincipal, String codSede
			, String cadAprobacion) {
		try{
			return gestionOrdenesDAO.UpdateSuplenteAprobacionOc(codPrincipal
					, codSede, cadAprobacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

}
