package com.tecsup.SGA.service.logistica.Impl;
import java.util.List;

import com.tecsup.SGA.DAO.logistica.GestionDocumentosDAO;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.DetalleDocPago;
import com.tecsup.SGA.modelo.OrdenesAprobadas;
import com.tecsup.SGA.service.logistica.GestionDocumentosManager;
public class GestionDocumentosManagerImpl implements GestionDocumentosManager{
	
	GestionDocumentosDAO gestionDocumentosDAO;
	
	public GestionDocumentosDAO getGestionDocumentosDAO() {
		return gestionDocumentosDAO;
	}
	public void setGestionDocumentosDAO(GestionDocumentosDAO gestionDocumentosDAO) {
		this.gestionDocumentosDAO = gestionDocumentosDAO;
	}

	public List GetAllActivosDisponibles(String codBien,String codSede){
		try{
			return this.gestionDocumentosDAO.GetAllActivosDisponibles(codBien,codSede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,22/01/09. A�ADIENDO NUEVO PARAMETRO PARA FILTRAR
	public List<OrdenesAprobadas> GetAllOrdenesAprobadas(String codSede, String codUsuario, String codPerfil, String nroOrden,
			String fecEmisionInicio, String fecEmisionFin, String nroRucProve,String nombreProve, 
			String codComprador, String nomComprador, String codTipoOrden, 
			String indSoloCajaChica, String indSinEnvio, String numFactura,
			String indIngresoPorConfirmar) {
		try{
			return gestionDocumentosDAO.GetAllOrdenesAprobadas(codSede, codUsuario, codPerfil, nroOrden,
					fecEmisionInicio, fecEmisionFin, nroRucProve, nombreProve, codComprador, nomComprador,
					codTipoOrden, indSoloCajaChica, indSinEnvio, numFactura, indIngresoPorConfirmar);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//
	public List<OrdenesAprobadas> GetAllDatosOrden(String codOrden) {
		try{
			return gestionDocumentosDAO.GetAllDatosOrden(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<OrdenesAprobadas> GetAllAprobarDocumentosPago(String nroDocumento, String fecEmiIni1, String fecEmiIni2, String nroDocProveedor, 
			 String razonSocialProveedor, String codTipoEstado, String nroOrden, String fecEmiFin1, 
			 String fecEmiFin2,String perfil, String codSede, String codUsuario) {
		try{
			return gestionDocumentosDAO.GetAllAprobarDocumentosPago(nroDocumento, fecEmiIni1, fecEmiIni2,
					nroDocProveedor, razonSocialProveedor, codTipoEstado, nroOrden, fecEmiFin1, 
					fecEmiFin2,perfil, codSede, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//public List<OrdenesAprobadas> GetAllDocPagosAsociados(String codOrden)
	public List<OrdenesAprobadas> GetAllDocPagosAsociados(String codOrden){
		try{
			return gestionDocumentosDAO.GetAllDocPagosAsociados(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllDetalleDocPago(String docId){
		try{
			return this.gestionDocumentosDAO.GetAllDetalleDocPago(docId);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String ActValidarDocPago(String codDocPago,String usuario){
		try{
			return this.gestionDocumentosDAO.ActValidarDocPago(codDocPago, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String ActRechazarDocPago(String codDocPago,String usuario){
		try{
			return this.gestionDocumentosDAO.ActRechazarDocPago(codDocPago, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllDocPago(String ordenId,String docId){
		try{
			return this.gestionDocumentosDAO.GetAllDocPago(ordenId, docId);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public List<DetalleDocPago> GetAllDetDocPagosAsociados(String codOrden){
		try{
			return gestionDocumentosDAO.GetAllDetDocPagosAsociados(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String UpdateDocumentoPago(String codOrden, String nroDocumento, String fecEmisionDoc, 
			String fecVencimientoDoc, String nroGuia, String fecEmisionGuia, String cadCantidad, 
			String cadObservaciones, String codUsuario, String fecGuiaRecepcion){
		try{
			return gestionDocumentosDAO.UpdateDocumentoPago(codOrden, nroDocumento, fecEmisionDoc,
			fecVencimientoDoc, nroGuia, fecEmisionGuia, cadCantidad, cadObservaciones, codUsuario, fecGuiaRecepcion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public String UpdateAnularDocPago(String codDocumento, String codUsuario){
		try{
			return gestionDocumentosDAO.UpdateAnularDocPago(codDocumento, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String UpdateEnviarDocPago(String codDocumentoPago, String codUsuario){
		try{
			return gestionDocumentosDAO.UpdateEnviarDocPago(codDocumentoPago, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
