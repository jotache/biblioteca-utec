package com.tecsup.SGA.service.logistica.Impl;

import java.util.ArrayList;
import java.util.List;

import com.tecsup.SGA.DAO.logistica.AtencionRequerimientosDAO;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.CotizacionDetalle;
import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.NotaSeguimiento;
import com.tecsup.SGA.modelo.SolRequerimiento;
import com.tecsup.SGA.service.logistica.AtencionRequerimientosManager;

public class AtencionRequerimientosManagerImpl implements AtencionRequerimientosManager{
	
	AtencionRequerimientosDAO atencionRequerimientosDAO;
//ALQD,23/01/09. NUEVO PARAMETRO DE FILTRO
	public List<SolRequerimiento> GetAllBandejaAtencionRequerimientos(String codSede, String codTipoReq,
			String codSubTipoReq, String nroReq, String fecInicio, String fecFin,
			String codCeco,String nombreUsuarioSol, String codEstado, String nroGuia,
			String indSalPorConfirmar)
	{try
		{
		 ArrayList<SolRequerimiento> arrSolicitudes = (ArrayList<SolRequerimiento>)this.
		 atencionRequerimientosDAO.GetAllBandejaAtencionRequerimientos(codSede,
		 codTipoReq, codSubTipoReq, nroReq, fecInicio, fecFin, codCeco, 
		 nombreUsuarioSol, codEstado, nroGuia, indSalPorConfirmar);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	
	}
	//ALQD,17/04/09. NUEVO PARAMETRO DE FILTRO
	public List<SolRequerimiento> GetAllBandejaAtencionReqMan(SolRequerimiento obj, String codSede, 
			String codUsuario, String codPerfil, String codEstado){
		try{
			return atencionRequerimientosDAO.GetAllBandejaAtencionReqMan(codSede, obj.getTipoRequerimiento(), 
					obj.getSubTipoRequerimiento(), obj.getNroRequerimiento(), 
					obj.getCecoSolicitante(), obj.getUsuSolicitante(), codUsuario, codPerfil,
					codEstado);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String UpdateCerrarRequerimiento(String codRequerimiento, String codUsuario){
		try{
			return atencionRequerimientosDAO.UpdateCerrarRequerimiento(codRequerimiento, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<Guia> GetAllGuiasAsociadas(String codRequerimiento){
		try{
			return atencionRequerimientosDAO.GetAllGuiasAsociadas(codRequerimiento);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<Guia> GetGuiaRequerimiento(String codGuia){
		try{
			return atencionRequerimientosDAO.GetGuiaRequerimiento(codGuia);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<GuiaDetalle> GetAllDetalleGuiaRequerimiento(String codGuia){
		try{
			return atencionRequerimientosDAO.GetAllDetalleGuiaRequerimiento(codGuia);												
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertGuiaRequerimiento(String codRequerimiento, String codUsuario){

		try{
			return atencionRequerimientosDAO.InsertGuiaRequerimiento(codRequerimiento, codUsuario);					
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteGuiaRequerimiento(String codGuia, String codUsuario) {
		try{
			return atencionRequerimientosDAO.DeleteGuiaRequerimiento(codGuia, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertDetalleGuiaRequerimiento(GuiaDetalle obj, String codUsuario){

		try{
			return atencionRequerimientosDAO.InsertDetalleGuiaRequerimiento(obj.getCodGuia(), 
					obj.getCodRequerimiento(), obj.getFechaGuia(), obj.getObservacion(), 
					obj.getCadenaCodDetalle(), obj.getCadenaCantidadSolicitada(), 
					obj.getCadenaPrecios(), obj.getCadenaCantidadEntregada(), obj.getCantidad(), codUsuario);								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<NotaSeguimiento> GetAllNotasSeguimientoRequerimiento(String codRequerimiento,String codNota){
		try{
			return atencionRequerimientosDAO.GetAllNotasSeguimientoRequerimiento(codRequerimiento,codNota);															
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertNotaSeguimiento(String codReq, String desNota, String codPerfil,String usuario){
		try{
			return this.atencionRequerimientosDAO.InsertNotaSeguimiento(codReq, desNota, codPerfil, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String UpdateNotaSeguimiento(String codReq, String codNota,String desNota,String usuario){
		try{
			return this.atencionRequerimientosDAO.UpdateNotaSeguimiento(codReq, codNota, desNota, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteNotaSeguimiento(String codRequerimiento, String codNota, String codUsuario) {
		try{
			return atencionRequerimientosDAO.DeleteNotaSeguimiento(codRequerimiento, codNota, codUsuario);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<GuiaDetalle> GetGuiaMantenimiento(String codRequerimiento){
		try{
			return atencionRequerimientosDAO.GetGuiaMantenimiento(codRequerimiento);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertGuiaMantenimiento(GuiaDetalle obj, String codUsuario){

		try{
			return atencionRequerimientosDAO.InsertGuiaMantenimiento(obj.getCodRequerimiento(),
					obj.getCodReqDetalle(), obj.getCodGuia(), obj.getCodGuiaDetalle(), 
					obj.getNroGuia(), obj.getFechaGuia(), obj.getObservacion(), codUsuario);
								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String RegistrarConformidad(String codGuia, String codUsuario){

		try{
			return atencionRequerimientosDAO.RegistrarConformidad(codGuia, codUsuario);								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String RegistrarConformidadMantenimiento(String codRequerimiento, String codUsuario){

		try{
			return atencionRequerimientosDAO.RegistrarConformidadMantenimiento(codRequerimiento, codUsuario);								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<SolRequerimiento> GetAllSolicitudesReqRel(String codRequerimiento){
		try{
			return atencionRequerimientosDAO.GetAllSolicitudesReqRel(codRequerimiento);															
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public AtencionRequerimientosDAO getAtencionRequerimientosDAO() {
		return atencionRequerimientosDAO;
	}

	public void setAtencionRequerimientosDAO(
			AtencionRequerimientosDAO atencionRequerimientosDAO) {
		this.atencionRequerimientosDAO = atencionRequerimientosDAO;
	}
	public String InsertRespByActivo(String codGuia, String codBien,
			String cadCodBienDet, String cadCodUsuResp,
			String cadCodDescUbicacion, String cantidad, String codUsuario) {
		try{
			return atencionRequerimientosDAO.InsertRespByActivo( codGuia,  codBien,
					 cadCodBienDet,  cadCodUsuResp,
					 cadCodDescUbicacion,  cantidad,  codUsuario);								
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String deleteRespByActivo(String codGuia, String codBien,
			String codBienDet, String usuario) {
		try{
			return atencionRequerimientosDAO.deleteRespByActivo( codGuia,  codBien,
					codBienDet,  usuario);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllNumerosSeries(String codReq,String codReqDet,String codDev,String codDevDet){
		try{
			return atencionRequerimientosDAO.GetAllNumerosSeries(codReq, codReqDet, codDev, codDevDet);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertNumerosSeries(String codDev, String codDevDet,
			String codBien, String cadCodIng,String cantidad,
			 String codUsuario){
		try{
		return atencionRequerimientosDAO.InsertNumerosSeries(codDev, codDevDet, codBien, cadCodIng, cantidad, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

}
