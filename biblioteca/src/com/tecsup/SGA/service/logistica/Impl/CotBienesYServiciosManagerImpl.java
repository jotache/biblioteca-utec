package com.tecsup.SGA.service.logistica.Impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.DAO.logistica.CotBienesYServiciosDAO;
import com.tecsup.SGA.bean.CotizacionDetalleBean;
import com.tecsup.SGA.bean.ObjDetalleBean;
import com.tecsup.SGA.bean.ObjGrupoBean;
import com.tecsup.SGA.bean.ObjPrecioBean;
import com.tecsup.SGA.bean.ProductoByItemBean;
import com.tecsup.SGA.bean.ProductoxCotizacionBean;
import com.tecsup.SGA.modelo.CotPendientes;
import com.tecsup.SGA.modelo.CotizacionAdjunto;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.CotizacionDetalle;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.ResumenPendientePorCotizar;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.service.logistica.CotBienesYServiciosManager;
import com.tecsup.SGA.web.logistica.controller.CotizacionBienesProveedorFormController;

public class CotBienesYServiciosManagerImpl implements CotBienesYServiciosManager {
	private static Log log = LogFactory.getLog(CotizacionBienesProveedorFormController.class);

	CotBienesYServiciosDAO cotBienesYServiciosDAO;

	public List<CotPendientes> GetAllCotBienesYServicios(String codTipoReq,	String codSubTipoReq, 
			String nroReq, String codFamilia, String codSubFamilia,	String grupoServicio, 
			String sede, String codCeco, String usuSolicitante, String codUsuario, String codPerfil,String indInversion) {
		NumberFormat formatter = new DecimalFormat("#########.00");
		try
		{
		 ArrayList<CotPendientes> arrSolicitudes = (ArrayList<CotPendientes>)this.cotBienesYServiciosDAO.GetAllCotBienesYServicios(codTipoReq,
			codSubTipoReq, nroReq, codFamilia, codSubFamilia, grupoServicio, sede, codCeco, 
			usuSolicitante, codUsuario, codPerfil, indInversion);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllCondicionCotizacion(String codCotizacion){
		try{
			return cotBienesYServiciosDAO.GetAllCondicionCotizacion(codCotizacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertCondicionCotizacion(String codId, String codCotizacion, String codTipoCondicion,String detalleInicial,
			String indSel,String usuario){
		try{
			return cotBienesYServiciosDAO.InsertCondicionCotizacion(codId
					, codCotizacion, codTipoCondicion, detalleInicial, indSel, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertAgregarACotizacion(String codUsuario, String cadenaCodigos, String cantCodigos) {
		try{
			return cotBienesYServiciosDAO.InsertAgregarACotizacion(codUsuario, cadenaCodigos, cantCodigos);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteItemBandejaProducto(String codUsuario, String cadenaCodigos, String cantCodigos) {
		try{
			return cotBienesYServiciosDAO.DeleteItemBandejaProducto(codUsuario, cadenaCodigos, cantCodigos);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<CotizacionProveedor> getAllProveedoresByItem(
			String codCotizacion, String codCotizacionDet) {
		try{
			return cotBienesYServiciosDAO.getAllProveedoresByItem(codCotizacion, codCotizacionDet);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//ALQD,13/07/09.AGREGANDO NUEVO PARAMETRO
	public List GetCotizacionActual(String codSede, String codTipoCotizacion
			, String codSubTipoCotizacion, String codUsuario, String indInversion){
		try{
			return cotBienesYServiciosDAO.GetCotizacionActual(codSede, codTipoCotizacion
					, codSubTipoCotizacion, codUsuario, indInversion);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String UpdateCotizacionActual(Cotizacion obj, String cadenaDescripcion, String cadenaCodDetalle, String cantidad){
		try{
			return cotBienesYServiciosDAO.UpdateCotizacionActual(obj.getCodigo(), obj.getUsuResponsable(), obj.getCodTipoPago(), 
					obj.getFechaInicio(), obj.getHoraInicio(), obj.getFechaFin(), obj.getHoraFin(), 
					cadenaDescripcion, cadenaCodDetalle, cantidad);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllDetalleCotizacionActual(String codCotizacion, String codTipoCotizacion){
		try{
			return cotBienesYServiciosDAO.GetAllDetalleCotizacionActual(codCotizacion, codTipoCotizacion);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteDetalleCotizacionActual(String codCotizacion, String codDetCotizacion, String codUsuario) {
		try{
			return cotBienesYServiciosDAO.DeleteDetalleCotizacionActual(codCotizacion, codDetCotizacion, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String deleteProveedorByItem(String codCotizacion,
			String codCotizacionDet, String codProveedor, String codUsuario) {
		try{
			return cotBienesYServiciosDAO.deleteProveedorByItem(codCotizacion, codCotizacionDet
					, codProveedor, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<CotizacionProveedor> getAllProveedores(String ruc,
			String razonSocial) {
		try{
			return cotBienesYServiciosDAO.getAllProveedores(ruc, razonSocial);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertProveedorByItem(String codCotizacion,
			String codCotizacionDet, String codProveedor, String codUsuario) {
		try{
			return cotBienesYServiciosDAO.insertProveedorByItem(codCotizacion
					, codCotizacionDet, codProveedor, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String deleteProductosByItem(String codReq, String codReqDet,
			String codUsuario) {
		try{
			return cotBienesYServiciosDAO.deleteProductosByItem(codReq, codReqDet, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<ProductoByItemBean> getAllProductosByItem(String codCotizacion,
			String codCotizacionDet) {
		try{
			return cotBienesYServiciosDAO.getAllProductosByItem(codCotizacion, codCotizacionDet);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String deleteDocumentoByItem(String codCotizacion,
			String codCotizacionDet, String codAdjunto, String codUsuario) {
		try{
			return cotBienesYServiciosDAO.deleteDocumentoByItem(codCotizacion, codCotizacionDet
					, codAdjunto, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertDocumentoByItem(String codCotizacion,
			String codCotizacionDet, String codTipoAdjunto, String codProveedor,
			String nombreAdjunto, String nombreGenerado, String codUsuario) {
		try{
			return cotBienesYServiciosDAO.insertDocumentoByItem(codCotizacion, codCotizacionDet
					, codTipoAdjunto, codProveedor, nombreAdjunto, nombreGenerado, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<SolRequerimientoAdjunto> getAllDocumentosByItem(
			String codCotizacion, String codCotizacionDet) {
		try{
			return cotBienesYServiciosDAO.getAllDocumentosByItem(codCotizacion, codCotizacionDet);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<CotizacionAdjunto> getAllDocumentosCompByItem(
			String codCotizacion, String codCotizacionDet, String codProveedor) {
		try{
			return cotBienesYServiciosDAO.getAllDocumentosCompByItem(codCotizacion, codCotizacionDet, codProveedor);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//ALQD,07/08/09.A�ADIENDO NUEVO PARAMETRO
	public List<Cotizacion> GetAllCotizacionesExixtentes(String codSede
			, String codTipoCotizacion, String nroCotizacion, String fechaIni
			, String fechaFin, String codTipoPago, String codSubTipoCotizacion
			, String indInversion){
		NumberFormat formatter = new DecimalFormat("#########.00");
		try
		{
		 ArrayList<Cotizacion> arrSolicitudes = (ArrayList<Cotizacion>)this.cotBienesYServiciosDAO.
		 GetAllCotizacionesExixtentes(codSede, codTipoCotizacion, nroCotizacion, fechaIni
				 , fechaFin, codTipoPago, codSubTipoCotizacion, indInversion);
					
			return arrSolicitudes;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//public String UpdateDocumentosReqACot(String codCotizacion, String codCotizacionDetalle, String codUsuario)
	public String UpdateDocumentosReqACot(String codCotizacion, String codCotizacionDetalle, String codUsuario){
		try{
			return cotBienesYServiciosDAO.UpdateDocumentosReqACot(codCotizacion, codCotizacionDetalle, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	/**/
	public String UpdateProveedorByItem(String codCotizacion, String codCotizacionDetalle, String cadProveedores,
			String cadIndicadores, String cantidad, String codUsuario){
		try{
			return cotBienesYServiciosDAO.UpdateProveedorByItem(codCotizacion, codCotizacionDetalle, 
					cadProveedores, cadIndicadores, cantidad, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String EnviarCotizacionUsuario(String codCotizacion, String codUsuario){
		try{
			return cotBienesYServiciosDAO.EnviarCotizacionUsuario(codCotizacion, codUsuario);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetAllBienesByProveedor(String codProveedor, String codCotizacion){
		try{
			return cotBienesYServiciosDAO.GetAllBienesByProveedor(codProveedor, codCotizacion);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllCondicionesByProveedor( String codCotizacion, String codProveedor){
		try{
			return cotBienesYServiciosDAO.GetAllCondicionesByProveedor(codCotizacion, codProveedor);												
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,30/01/09. NUEVO PARAMETRO
	public List<Cotizacion> GetAllSolicitudCotizacion(String codSede, String nroCotizacion, 
			String fechaInicio, String fechaFin, String codEstado, String codTipoReq, String codSubTipoReq,
			String codTipoPago, String rucProveedor, String dscProveedor, String codPerfil, String codUsuario,
			String codGrupoServ, String indCotSinOC)
			
	{
				NumberFormat formatter = new DecimalFormat("#########.00");
				try
				{
				 ArrayList<Cotizacion> arrSolicitudes = (ArrayList<Cotizacion>)this.cotBienesYServiciosDAO.
				 GetAllSolicitudCotizacion(codSede, nroCotizacion, fechaInicio, fechaFin, codEstado,
						 codTipoReq, codSubTipoReq, codTipoPago, rucProveedor, dscProveedor, codPerfil, 
						 codUsuario, codGrupoServ, indCotSinOC);
					return arrSolicitudes;
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public List<CotizacionProveedor> GetAllBandejaGenerarOrdenCompra(String codCotizacion)
		
	{
				NumberFormat formatter = new DecimalFormat("#########.00");
				try
				{
				 ArrayList<CotizacionProveedor> arrSolicitudes = (ArrayList<CotizacionProveedor>)
				 this.cotBienesYServiciosDAO.GetAllBandejaGenerarOrdenCompra(codCotizacion);
				
					return arrSolicitudes;
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	public List<CotizacionProveedor> GetAllCotizacionByProveedor(String codCotizacion, String codProveedor)
	
	{
				NumberFormat formatter = new DecimalFormat("#########.00");
				try
				{
				 ArrayList<CotizacionProveedor> arrSolicitudes = (ArrayList<CotizacionProveedor>)
				 this.cotBienesYServiciosDAO.GetAllCotizacionByProveedor(codCotizacion, codProveedor);
				
					return arrSolicitudes;
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				return null;
			}
	
	public String InsertCotSolGenerarOrdenCompraByProveedor(String codCotizacion, String codProveedor,
			String cadCodCotizacionDet,	String cadDscCotizacionDet, String cantidad, String codUsuario){
	try{
		return cotBienesYServiciosDAO.InsertCotSolGenerarOrdenCompraByProveedor(codCotizacion, codProveedor,
				cadCodCotizacionDet, cadDscCotizacionDet, cantidad, codUsuario);
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
	}
	return null;
	}
	//ALQD,02/06/09.PASANDO UN NUEVO PARAMETRO OTROCOSTO
	public String InsertBienesByProveedor( CotizacionDetalle obj, String codUsuario){

		try{
			return cotBienesYServiciosDAO.InsertBienesByProveedor(obj.getCodigo()
					, obj.getCodProveedor(), obj.getCadenaCodCotDetalle()
					, obj.getCadenaPrecios(), obj.getCadenaDescripcion()
					, obj.getCantidad(), codUsuario, obj.getCodMoneda()
					, obj.getOtroCostos());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String InsertCondicionesByProveedor(CotizacionDetalle obj, String codUsuario){

		try{
			return cotBienesYServiciosDAO.InsertCondicionesByProveedor(obj.getCodigo(), obj.getCodProveedor(), 
					obj.getCodDetalle(), obj.getDescripcion(), codUsuario);		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String EnviarCotizacionByProveedor(String codCotizacion, String codProveedor){
		try{
			return cotBienesYServiciosDAO.EnviarCotizacionByProveedor(codCotizacion, codProveedor);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public CotBienesYServiciosDAO getCotBienesYServiciosDAO() {
		return cotBienesYServiciosDAO;
	}

	public void setCotBienesYServiciosDAO(
			CotBienesYServiciosDAO cotBienesYServiciosDAO) {
		this.cotBienesYServiciosDAO = cotBienesYServiciosDAO;
	}

	public List getLstDetalleCotizacion(String codCotizacion){
		log.info("codCotizacion>>"+codCotizacion+">>");
		String strGrupos = this.getStrGruposBndjCotiComp(codCotizacion);//GRUPOS DE COTIZACION
		log.info("OUTPUT 1<"+strGrupos+">");
		List lstGeneral  = new ArrayList();//RESULTADO FINAL
		List lstPrecios = null;//PRECIOS
		List lstItems = null;//Items
		
		StringTokenizer tkn = new StringTokenizer(strGrupos,"$"); 
		
		//*********************GRUPOS********************************************
		ObjGrupoBean objGrupo = null;
		while(tkn.hasMoreTokens())
		{
			
			String tknActual = tkn.nextToken();//TOKEN  |1|2|
			objGrupo= new ObjGrupoBean();//NUEVO GRUPO
			
			List lstCabecera = this.getAllGrpProvBndjCotiComp(codCotizacion,tknActual);
			if(lstCabecera!=null && lstCabecera.size()>0)
			{	log.info("tam lstCabecera<"+lstCabecera.size()+">");
				objGrupo.setCabeceras(lstCabecera);
			}	
			
			log.info("******************TKN ACTUAL***********>"+tknActual+">");
			List lstDetalleGrupoConsulta = this.getAllItmProvBndjCotiComp(codCotizacion,tknActual);
			
			//*********************ITEMS DE LOS GRUPOS*********************************************
			if(lstDetalleGrupoConsulta!=null && lstDetalleGrupoConsulta.size()>0)
			{	
				ObjDetalleBean objDetalle = null;
				log.info("tam lstDetalleGrupo<"+lstDetalleGrupoConsulta.size()+">");
				lstItems = new ArrayList();
				ProductoxCotizacionBean objCast =null;
				for(int i=0;i<lstDetalleGrupoConsulta.size();i++)
				{
					objDetalle = new ObjDetalleBean();//NUVO ITEM
						objCast = (ProductoxCotizacionBean)lstDetalleGrupoConsulta.get(i);//OBJ CASTEADO DEL SP

					objDetalle.setCantidad(objCast.getCantidad());
					objDetalle.setCodCoti(objCast.getCodCoti());
					objDetalle.setCodCotiDet(objCast.getCodCotiDet());
					objDetalle.setDscBien(objCast.getDscBien());
					objDetalle.setDscFam(objCast.getDscFam());
					objDetalle.setDscSfam(objCast.getDscSfam());
					objDetalle.setPreUltiOrden(objCast.getPreUltiOrden());
					objDetalle.setCanUltiOrden(objCast.getCanUltiOrden());
					objDetalle.setCodUltiOrden(objCast.getCodUltiOrden());
								
					objDetalle.setUnidad(objCast.getUnidad());//jhpr 2008-09-19
					//ALQD,10/07/09.NUEVA PROPIEDAD
					objDetalle.setTieneComentario("0");
					
					String codCotizacionDet = objCast.getCodCotiDet();
					List lstPreciosConsulta = this.getAllDetProvBndjCotiComp(codCotizacion,codCotizacionDet);
					//*********************PRECIOS DE LOS ITEMS***********************************************
					if(lstPreciosConsulta!=null && lstPreciosConsulta.size()>0)
					{
						ObjPrecioBean objPrecioBean = null;
						CotizacionDetalleBean objCast2 =null;
						lstPrecios = new ArrayList();//LISTA DE PRECIOS PARA EL OBJETO GENERAL
						log.info("tam lstPrecios<"+lstPreciosConsulta.size()+">");

						for(int j=0;j<lstPreciosConsulta.size();j++)
						{
							objPrecioBean = new ObjPrecioBean();//NUEVO OBJ PRECIO DEL ITEM
							objCast2 = (CotizacionDetalleBean)lstPreciosConsulta.get(j);//OBJ PRECIO CASTEADO
							objPrecioBean.setCodProv(objCast2.getCodProv());
							objPrecioBean.setPrecioUnitario(objCast2.getPrecioUnitario());
							objPrecioBean.setRazonSocial(objCast2.getRazonSocial());
							objPrecioBean.setFlgChecked(objCast2.getFlgChecked());
							objPrecioBean.setObs(objCast2.getObs());
							objPrecioBean.setObsProv(objCast2.getObsProv());
							//ALQD,04/06/09.LEYENDO NUEVA PROPIEDAD
							objPrecioBean.setOtroCosto(objCast2.getOtroCosto());
							//ALQD,10/07/09.SETEANDO NUEVA PROPIEDAD
							if(objPrecioBean.getObs()!="" || objPrecioBean.getObsProv()!="")
								objDetalle.setTieneComentario("1");

							//objPrecioBean = objCast2; 
							lstPrecios.add(objPrecioBean);
						}
						objDetalle.setPrecios(lstPrecios);//ADICIONAN LOS PRECIOS AL ITEM
					}
					//*********************PRECIOS DE LOS ITEMS***********************************************					
					lstItems.add(objDetalle);
				}
				//*********************ITEMS DE LOS GRUPOS*********************************************
				objGrupo.setItems(lstItems);				
			}
			
			lstGeneral.add(objGrupo);//A LA LISTA GLOBAL
			
		}
		

		return lstGeneral;
	}
	
	public String DeleteCondicionCotizacion(String codCotizacion,  String codUsuario){
		try{
			return cotBienesYServiciosDAO.DeleteCondicionCotizacion(codCotizacion, codUsuario);									
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return null;
	}
	

	private List getAllDetProvBndjCotiComp(String codCotizacion,
			String codCotizacionDet) {
		return cotBienesYServiciosDAO.getAllDetProvBndjCotiComp( codCotizacion,
				 codCotizacionDet);
	}

	private List getAllItmProvBndjCotiComp(String codCotizacion,
			String tknActual) {
		return cotBienesYServiciosDAO.getAllItmProvBndjCotiComp( codCotizacion,tknActual);
	}

	private String getStrGruposBndjCotiComp(String codCotizacion) {
		return cotBienesYServiciosDAO.getStrGruposBndjCotiComp( codCotizacion);
	}
	
	private List getAllGrpProvBndjCotiComp(String codCotizacion,
			String tknActual) {
		return cotBienesYServiciosDAO.getAllGrpProvBndjCotiComp( codCotizacion,tknActual);		
	}

	public String UpdateDetProvBndjCotiComp(String txhCodCotizacion,
			String cadCodDet, String cadCodProv, String cadPrecios, String cantidad, String usuario) {
		
		return cotBienesYServiciosDAO.UpdateDetProvBndjCotiComp( txhCodCotizacion,
				 cadCodDet,  cadCodProv, cadPrecios, cantidad,  usuario);
		
	}

	public List GetEnvioCorreoProveedor(String codCotizacion){
		try{
			return cotBienesYServiciosDAO.GetEnvioCorreoProveedor(codCotizacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String GetTipoServicioConCotizacion(String codSede, String codUsuario, String codPerfil,
			String codGrupoServ) {
		try{	
		return cotBienesYServiciosDAO.GetTipoServicioConCotizacion(codSede, codUsuario, codPerfil, 
				codGrupoServ);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public String UpdateCerrarCotizacion(String txhCodCotizacion, String usuario) {
		return cotBienesYServiciosDAO.UpdateCerrarCotizacion( txhCodCotizacion,usuario);
	}

	public List getAllRespByActivo(String codGuia, String codBien) {
		return cotBienesYServiciosDAO.getAllRespByActivo(codGuia,codBien);
	}

	//ALQD,01/06/09. A�ADIENDO NUEVO PARAMETRO
	public List getAllProvSenvByCoti(String txhCodCotizacion, String codProveedor) {
		return cotBienesYServiciosDAO.getAllProvSenvByCoti(txhCodCotizacion, codProveedor);
	}

	public String InsertDetalleCotizacionExistente(String codUsuario, String codCotizacion
			, String cadCodigos, String cantCodigos) {
		try{	
			return cotBienesYServiciosDAO.InsertDetalleCotizacionExistente(codUsuario
					, codCotizacion, cadCodigos, cantCodigos);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public List GetAllCotizacionById(String codCotizacion){
		try{
			return cotBienesYServiciosDAO.GetAllCotizacionById(codCotizacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List getLstDetalleAprobacion(String codCotizacion) {
		try{
			return cotBienesYServiciosDAO.GetAllCotizacionAutById(codCotizacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String actualizarFechas(String codCotizacion, String codUsuario,
			String fechaIni, String horaIni, String fechaFin, String horaFin) {
		try{
			return cotBienesYServiciosDAO.actualizarFechas(codCotizacion, codUsuario, fechaIni, horaIni, fechaFin, horaFin);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,12/08/09.NUEVO METODO PARA RECUPERAR LA CANTIDAD DE PENDIENTES POR TIPO
	public List<ResumenPendientePorCotizar> GetResumenPendientePorCotizar(String codSede) {
		try{
			return cotBienesYServiciosDAO.GetResumenPendientePorCotizar(codSede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
