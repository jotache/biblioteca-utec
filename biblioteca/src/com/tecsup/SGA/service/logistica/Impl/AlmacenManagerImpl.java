package com.tecsup.SGA.service.logistica.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.logistica.AlmacenDAO;
import com.tecsup.SGA.modelo.Almacen;
import com.tecsup.SGA.modelo.Devolucion;
import com.tecsup.SGA.modelo.DevolucionDetalle;
import com.tecsup.SGA.modelo.GuiaRemision;
import com.tecsup.SGA.service.logistica.AlmacenManager;

public class AlmacenManagerImpl implements AlmacenManager{

	AlmacenDAO almacenDAO;
	//ALQD,23/10/09. NUEVO PARAMETRO DE BUSQUEDA
	public List<Almacen> GetAllBienesRecepcion(String codSede, String nroOrden, String fecEmiOrdIni,
			String fecEmiOrdFin, 
   		 String nroRucProveedor, String dscProveedor, String dscComprador, String nroGuia, 
		 String fecEmiGuiaIni, String fecEmiGuiaFin, String indIngPorConfirmar) {
		try{
			return almacenDAO.GetAllBienesRecepcion(codSede, nroOrden, fecEmiOrdIni, fecEmiOrdFin, 
					nroRucProveedor, dscProveedor, dscComprador, nroGuia, fecEmiGuiaIni, fecEmiGuiaFin,
					indIngPorConfirmar);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllBienesDevolucion(String codSede, String fecDevIni, String fecDevFin, String codCeco, 
	   		 String dsvUsu, String codEstado){
		try{
			return this.almacenDAO.GetAllBienesDevolucion(codSede, fecDevIni, fecDevFin, codCeco, dsvUsu, codEstado);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//
	public List<Almacen> GetAllDatosOrden(String codOrden) {
		try{
			return almacenDAO.GetAllDatosOrden(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public List<GuiaRemision> GetAllGuiaRemisionAsociadas(String codOrden) {
		try{
			return almacenDAO.GetAllGuiaRemisionAsociadas(codOrden);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public List<GuiaRemision> GetAllDatosGuiaRemision(String codDocPago) {
		try{
			return almacenDAO.GetAllDatosGuiaRemision(codDocPago);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public List<GuiaRemision> GetAllDetalleGuiaRemision(String codDocPago) {
		try{
			return almacenDAO.GetAllDetalleGuiaRemision(codDocPago);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//
	public String UpdateGuiaRemision(String codDocPago, String  cadCodDetDocPag,String cadCantidad, 
			String cantidad, String codUsuario){
		try{
			return almacenDAO.UpdateGuiaRemision(codDocPago, cadCodDetDocPag, cadCantidad, cantidad,
					codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//)
	public String UpdateCerrarGuiaRemision(String codDocPago, String codUsuario){
		try{
			return almacenDAO.UpdateCerrarGuiaRemision(codDocPago, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String ActConfirmarDev(String codReqDev, String codUsuario,String observacion){
		try{
			return this.almacenDAO.ActConfirmarDev(codReqDev, codUsuario, observacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String ActRechazarDev(String codReqDev, String codUsuario,String observacion){
		try{
			return this.almacenDAO.ActRechazarDev(codReqDev, codUsuario, observacion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String ActAbrirAlmacen(String sede){
		try{
			return this.almacenDAO.ActAbrirAlmacen(sede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String ActCerrarAlmacen(String sede){
		try{
			return this.almacenDAO.ActCerrarAlmacen(sede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<Devolucion> GetDatosDevolucion(String codDevolucion) {
		try{
			return almacenDAO.GetDatosDevolucion(codDevolucion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<DevolucionDetalle> GetAllDetalleDevolucion(String codDevolucion) {
		try{
			return almacenDAO.GetAllDetalleDevolucion(codDevolucion);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	

	public String InsertSeriesActivo(String codDocPago, String codBien,String valorCompra,
			String cadCodIng,String cadNroSerie,String cantidad,String codUsuario,String codDetalle){
		try{
			return almacenDAO.InsertSeriesActivo(codDocPago, codBien, valorCompra, cadCodIng, cadNroSerie, cantidad, codUsuario,codDetalle);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DelSeriesActivos(String codDocPago, String codBien,String codIngBien,String codUsuario){
		try{
			return almacenDAO.DelSeriesActivos(codDocPago, codBien, codIngBien, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllSeriesActivos(String codDocPago,String codBien,String codDetalle){
		try{
			return almacenDAO.GetAllSeriesActivos(codDocPago, codBien,codDetalle);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllImpresionTickets(String codSede){
		try{
			return almacenDAO.getAllImpresionTickets(codSede);			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public List getAllInventario(String codSede, String respuesta){
		try{
			return almacenDAO.getAllInventario(codSede, respuesta);						
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String insertInventario(String codSede, String cadCodBienes, String cadCanBienes, 
			String cantidad, String codUsuario){
		try{
			return almacenDAO.insertInventario(codSede, cadCodBienes, cadCanBienes, cantidad, codUsuario);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String actEnviarInventario(String codSede, String codUsuario){
		try{
			return this.almacenDAO.actEnviarInventario(codSede, codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String actRechazarInventario(String codSede, String codUsuario){
		try{
			return this.almacenDAO.actRechazarInventario(codSede, codUsuario);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String actAprobarInventario(String codSede, String codUsuario){
		try{
			return this.almacenDAO.actAprobarInventario(codSede, codUsuario);						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getEstadoAlmacen(String codSede){
		try{
			return this.almacenDAO.getEstadoAlmacen(codSede);				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public AlmacenDAO getAlmacenDAO() {
		return almacenDAO;
	}

	public void setAlmacenDAO(AlmacenDAO almacenDAO) {
		this.almacenDAO = almacenDAO;
	}
}
