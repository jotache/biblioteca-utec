package com.tecsup.SGA.service.logistica.Impl;

import java.util.List;

import com.tecsup.SGA.service.logistica.DetalleManager;
import com.tecsup.SGA.DAO.logistica.MantenimientoLogDAO;
import com.tecsup.SGA.modelo.PresupuestoCentroCosto;
import com.tecsup.SGA.modelo.TipoLogistica;
import com.tecsup.SGA.modelo.TipoProveedor;

public class DetalleManagerImpl implements DetalleManager{
	MantenimientoLogDAO mantenimientoLogDAO;

	public MantenimientoLogDAO getMantenimientoLogDAO() {
		return mantenimientoLogDAO;
	}

	public void setMantenimientoLogDAO(MantenimientoLogDAO mantenimientoLogDAO) {
		this.mantenimientoLogDAO = mantenimientoLogDAO;
	}

	public List GetAllTablaDetalle(TipoLogistica obj )
	{
		try
		{
			return mantenimientoLogDAO.GetAllTablaDetalle(obj.getCodPadre(), obj.getCodId(), obj.getCodTabla()
					, obj.getCodigo(), obj.getDescripcion(), obj.getTipo());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllSubFamilias(String bien, String familia, String tipo,String codSubFamilia,String dscFamilia,String dscSubFamilia){
		try{
		return 	mantenimientoLogDAO.GetAllSubFamilias(bien, familia, tipo, codSubFamilia, dscFamilia, dscSubFamilia);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllProveedor(String codProv, String ruc, String raSoc
    		,String tipoCalif,String estado, String tipoProveedor){
		try{
			return mantenimientoLogDAO.GetAllProveedor(codProv, ruc, raSoc, tipoCalif, estado, tipoProveedor);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,03/07/09.AGREGANDO DOS NUEVOS PARAMETROS
	public String InsertProveedor(TipoProveedor obj,String usuario){
		try{
			return mantenimientoLogDAO.InsertProveedor(obj.getCodTipoCalif(), obj.getCodTipoProv()
					,  obj.getTipoDoc(), obj.getNroDoc(), obj.getRazSoc(), obj.getRazSocCorto(), obj.getEmail()
					, obj.getDirec(), obj.getCodDpto(), obj.getCodProv(), obj.getCodDsto(), obj.getAtencion()
					, obj.getCodGiro(), obj.getCodEstado(), obj.getBanco(), obj.getTipCta(), obj.getNroCta()
					, obj.getCodMon(), usuario, obj.getPais(), obj.getTelefono());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertTablaDetalle(String codTipoId, String codHijo, String descripcion
			,String valor3, String valor4,String valor5,String usuario){
		try{
			return mantenimientoLogDAO.InsertTablaDetalle(codTipoId, codHijo, descripcion
					, valor3, valor4, valor5, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteProveedor(String codPro, String usuario){
		try{
			return this.mantenimientoLogDAO.DeleteProveedor(codPro, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteProducto(String codPro, String usuario){
		try{
			return this.mantenimientoLogDAO.DeleteProducto(codPro, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteFamiliaByProveedor(String codFamProv, String usuario){
		try{
			return this.mantenimientoLogDAO.DeleteFamiliaByProveedor(codFamProv, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteTablaDetalle(String tipId,String codUnico, String usuario){
	try{
		return this.mantenimientoLogDAO.DeleteTablaDetalle(tipId, codUnico, usuario);
	}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;	
	}
	
	public String UpdateTablaDetalle(String codTipoId, String codHijo,String codReg,String codGen, String descripcion
			,String valor3, String valor4,String valor5,String usuario){
		try{
			return this.mantenimientoLogDAO.UpdateTablaDetalle(codTipoId
					, codHijo, codReg, codGen, descripcion, valor3, valor4, valor5, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,03/07/09.AGREGANDO DOS NUEVOS PARAMETROS
	public String UpdateProveedor(String codProveedor, String codTipCal,String codTipProv,String tipDoc, String nroDoc
			,String razSoc, String razSocCorto,String email,String direc,String codDpto,String codProv,String codDsto
			,String atencionA,String codGiro,String codEstado,String banco,String tipCta,String nroCta,String codMoneda
			,String usuario,String pais,String telefono){
		try{
			return this.mantenimientoLogDAO.UpdateProveedor(codProveedor, codTipCal
					, codTipProv, tipDoc, nroDoc, razSoc, razSocCorto, email, direc
					, codDpto, codProv, codDsto, atencionA, codGiro, codEstado, banco
					, tipCta, nroCta, codMoneda, usuario, pais, telefono);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllProducto(String codCatago,String codSede, String codFam
	    	,String codSubFam, String codPro, String nomPro, String codTipo
	    	,String nroSerie, String descriptor,String estado){
		try{
			return this.mantenimientoLogDAO.GetAllProducto(codCatago,codSede, codFam, codSubFam
					, codPro, nomPro, codTipo, nroSerie, descriptor,estado);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,23/02/09.NUEVO PARAMETRO
	//ALQD,24/02/09.NUEVO PARAMETRO PARA ACTUALIZAR PRECIO REF.
	//ALQD,20/05/09.NUEVA PROPIEDAD NO AFECTO
	public String InsertProducto(String codBien, String sede, String familia
			,String subFamilia, String nroProducto,String decrip,String unidad,String tipoBien,String ubicacion
			,String ubiFila,String ubiColum,String stockMin,String stockMax,
			String nroDiasAten,String marca,String modelo,String usuario,
			String inversion,String stockMostrar,String estado,String artPromocional,
			String preURef, String artNoAfecto){
		try{
			return this.mantenimientoLogDAO.InsertProducto(codBien, sede, familia, subFamilia
					, nroProducto, decrip, unidad, tipoBien, ubicacion, ubiFila, ubiColum
					, stockMin,stockMax, nroDiasAten, marca, modelo, usuario, inversion,
					stockMostrar,estado,artPromocional,preURef,artNoAfecto);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertImageProducto(String codBien, String imagen,String usuario){
		try{
		return this.mantenimientoLogDAO.InsertImageProducto(codBien, imagen, usuario);	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertDocProducto(String codBien, String tipoDoc,String nomDoc,String usuario){
		
		try{
			return this.mantenimientoLogDAO.InsertDocProducto(codBien, tipoDoc, nomDoc, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllDocCatalogo(String codBien,String codAdj){
		try{
			return this.mantenimientoLogDAO.GetAllDocCatalogo(codBien, codAdj);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String DeleteDocProducto(String codBien,String codAdj, String usuario){
		try{
			return this.mantenimientoLogDAO.DeleteDocProducto(codBien, codAdj, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	//ALQD,20/08/09.ELIMINA FOTO DEL PRODUCTO
	public String DeleteFotoProducto(String codBien, String usuario){
		try{
			return this.mantenimientoLogDAO.DeleteFotoProducto(codBien, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public List GetAllAtencionSolicitudes(String codSede,String tipoReq, String subtipoReq, String codResponsable
    		,String nomResponsable,String familia,String subfamilia){
		try{
			return this.mantenimientoLogDAO.GetAllAtencionSolicitudes(codSede,tipoReq, subtipoReq, codResponsable, nomResponsable, familia, subfamilia);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
   
	public String InsertAtencionSolicitante(String tipoReq, String codigo, String codReq,String usuario,String codSede){
		try{
			return this.mantenimientoLogDAO.InsertAtencionSolicitante(tipoReq, codigo, codReq, usuario,codSede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllEmpleados(String tipoPer, String nombre, String apPat
    		,String apMat, String codSede){
		try{
			return this.mantenimientoLogDAO.GetAllEmpleados(tipoPer, nombre, apPat, apMat, codSede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String InsertFlujo(String codTabla, String codDetalle, String indicador,String usuario,String codSede){
		try{
			return this.mantenimientoLogDAO.InsertFlujo(codTabla, codDetalle, indicador, usuario, codSede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String DeleteAtencionSolicitudes(String tipReq,String codigo,String codRes, String usuario,String codSede){
		try{
			return this.mantenimientoLogDAO.DeleteAtencionSolicitudes(tipReq, codigo, codRes, usuario,codSede);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String InsDescriptoresByBien(String codBien, String codDes,String usuario){
		try{
			return this.mantenimientoLogDAO.InsDescriptoresByBien(codBien, codDes, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllDescriptoresByBien(String codBien){
		try{
			return this.mantenimientoLogDAO.GetAllDescriptoresByBien(codBien);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllSubFamliaByPro(String codProv){
		try{
			return this.mantenimientoLogDAO.GetAllSubFamliaByPro(codProv);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public String InsertSubFamiliaByPro(String codPro, String codSFam,String usuario){
		try{
			return this.mantenimientoLogDAO.InsertSubFamiliaByPro(codPro, codSFam, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteDescriptoresByBien(String codBien,String codDet, String usuario){
		try{
			return this.mantenimientoLogDAO.DeleteDescriptoresByBien(codBien, codDet, usuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String ActIndiceFlujo(String codTabla, String codDetalle, String indicador){
		try{
			return this.mantenimientoLogDAO.ActIndiceFlujo(codTabla, codDetalle, indicador);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllFlujosAprobacion(String codSede, String codFlujo){
		try{
			return this.mantenimientoLogDAO.GetAllFlujosAprobacion(codSede, codFlujo);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}//
	
	public String GetSedeByUsuario(String codUsuario){
		try{
			return this.mantenimientoLogDAO.GetSedeByUsuario(codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	//ALQD,11/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,30/06/09.AGREGANDO 1 NUEVO PARAMETRO
	//ALQD,21/07/09.AGREGANDO 3 NUEVOS PARAMETROS
	public String InsertDocAsociado(String codDocuemnto, String codDocPago
			, String nroDocumento,String fecDocuemnto
			, String montoDocumento,String dscDoc,String codUsuario
			, String codTipMoneda, String impTipCamSoles, String codProvee
			, String nomProvee, String rucProvee){
		try{
			return this.mantenimientoLogDAO.InsertDocAsociado(codDocuemnto
					, codDocPago, nroDocumento, fecDocuemnto, montoDocumento
					, dscDoc, codUsuario, codTipMoneda, impTipCamSoles, codProvee
					, nomProvee, rucProvee);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public String DeleteDocAsociado(String codDocumento, String codUsuario){
		try{
			return this.mantenimientoLogDAO.DeleteDocAsociado( codDocumento,  codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetAllDocAsociado(String codDoc){
		try{
			return this.mantenimientoLogDAO.GetAllDocAsociado(codDoc);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	
	}

	public List<PresupuestoCentroCosto> listaPresupuestosCenCos(String anio, String sede) {
		try {
			return this.mantenimientoLogDAO.listaPresupuestosCenCos(anio, sede);
		}catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public String InsertPresupuesto(String sede, String anio,
			String cadenaDatos, String codUsuario, String nroReg) {
		try{
			return mantenimientoLogDAO.InsertPresupuesto(sede, anio, cadenaDatos, codUsuario, nroReg);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
