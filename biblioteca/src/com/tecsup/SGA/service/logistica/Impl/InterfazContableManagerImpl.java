package com.tecsup.SGA.service.logistica.Impl;

import java.util.List;

import com.tecsup.SGA.DAO.logistica.InterfazContableDAO;
import com.tecsup.SGA.service.logistica.InterfazContableManager;

public class InterfazContableManagerImpl implements InterfazContableManager{

	InterfazContableDAO interfazContableDAO;
	
	public List GetAllEstadoInterfazContable(String codSede, String codMes, String codAnio, 
			String codTipoInterfaz){
		try{
			return interfazContableDAO.GetAllEstadoInterfazContable(codSede, codMes, codAnio, codTipoInterfaz);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}

	public String InsertInterfazContable(String codSede, String codMes, String codAnio, String codTipoInterfaz,
			String codUsuario){
		try{
			return this.interfazContableDAO.InsertInterfazContable(codSede, codMes, codAnio, codTipoInterfaz,
					codUsuario);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public List GetCabeceraInterfaz(String codSede, String codMes, String codAnio, 
			String codTipoInterfaz){
		try{
			return interfazContableDAO.GetCabeceraInterfaz(codSede, codMes, codAnio, codTipoInterfaz);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetDetalleInterfaz(String codSede, String codMes, String codAnio, 
			String codTipoInterfaz){
		try{
			return interfazContableDAO.GetDetalleInterfaz(codSede, codMes, codAnio, codTipoInterfaz);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	public List GetResultadoInterfazContable(String codSede, String codMes, String codAnio, 
			String codTipoInterfaz){
		try{
			return interfazContableDAO.GetResultadoInterfazContable(codSede, codMes, codAnio, codTipoInterfaz);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public InterfazContableDAO getInterfazContableDAO() {
		return interfazContableDAO;
	}

	public void setInterfazContableDAO(InterfazContableDAO interfazContableDAO) {
		this.interfazContableDAO = interfazContableDAO;
	}
}
