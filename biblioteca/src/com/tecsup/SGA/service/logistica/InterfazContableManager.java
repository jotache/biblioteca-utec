package com.tecsup.SGA.service.logistica;

import java.util.List;

public interface InterfazContableManager {

	public List GetAllEstadoInterfazContable(String codSede, String codMes, String codAnio, 
			String codTipoInterfaz);
	
	public String InsertInterfazContable(String codSede, String codMes, String codAnio, String codTipoInterfaz,
			String codUsuario);
	
	public List GetCabeceraInterfaz(String codSede, String codMes, String codAnio, String codTipoInterfaz);
	
	public List GetDetalleInterfaz(String codSede, String codMes, String codAnio, String codTipoInterfaz);
	
	public List GetResultadoInterfazContable(String codSede, String codMes, String codAnio, String codTipoInterfaz);
}
