package com.tecsup.SGA.service.logistica;

import java.util.List;

import com.tecsup.SGA.bean.ProductoByItemBean;
import com.tecsup.SGA.modelo.CotPendientes;
import com.tecsup.SGA.modelo.CotizacionAdjunto;
import com.tecsup.SGA.modelo.CotizacionDetalle;
import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.SolRequerimientoAdjunto;
import com.tecsup.SGA.modelo.Cotizacion;
import com.tecsup.SGA.modelo.ResumenPendientePorCotizar;

public interface CotBienesYServiciosManager {

	public List<CotPendientes> GetAllCotBienesYServicios(String codTipoReq,	String codSubTipoReq, 
			String nroReq, String codFamilia, String codSubFamilia,	String grupoServicio, 
			String sede, String codCeco, String usuSolicitante, String codUsuario, String codPerfil,String indInversion);
	public List GetAllCondicionCotizacion(String codCotizacion);
	
	public String InsertCondicionCotizacion(String codId, String codCotizacion, String codTipoCondicion,String detalleInicial,
			String indSel,String usuario);
	
	public String InsertAgregarACotizacion(String codUsuario, String cadenaCodigos, String cantCodigos);
	
	public String DeleteItemBandejaProducto(String codUsuario, String cadenaCodigos, String cantCodigos);
	
	//ALQD,13/07/09.AGREGANDO NUEVO PARAMETRO
	public List GetCotizacionActual(String codSede, String codTipoCotizacion
			, String codSubTipoCotizacion, String codUsuario, String indInversion);
	
	public String UpdateCotizacionActual(Cotizacion obj, String cadenaDescripcion, String cadenaCodDetalle, String cantidad);
	
	public List GetAllDetalleCotizacionActual(String codCotizacion, String codTipoCotizacion);
	
	public String DeleteDetalleCotizacionActual(String codCotizacion, String codDetCotizacion, String codUsuario);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @return Lista de Proveedores por detalle de cotizacion
	 */
	public List<CotizacionProveedor> getAllProveedoresByItem(String codCotizacion, String codCotizacionDet);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @param codProveedor
	 * @param codUsuario
	 * @return Elimina un proveedor por item.
	 */
	public String deleteProveedorByItem(String codCotizacion, String codCotizacionDet
			, String codProveedor, String codUsuario);
	
	/**
	 * @param ruc
	 * @param razonSocial
	 * @return Lista de Proveedores.
	 */
	public List<CotizacionProveedor> getAllProveedores(String ruc, String razonSocial);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @param codProveedor
	 * @param codUsuario
	 * @return Inserta un proveedor.
	 */
	public String insertProveedorByItem(String codCotizacion, String codCotizacionDet
			, String codProveedor, String codUsuario);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @return Lista de productos por item.
	 */
	public List<ProductoByItemBean> getAllProductosByItem(String codCotizacion, String codCotizacionDet);
	
	/**
	 * @param codReq
	 * @param codReqDet
	 * @param codUsuario
	 * @return Borra la relacione entre el detalle del requerimiento con el detalle dela cotizacion.
	 */
	public String deleteProductosByItem(String codReq, String codReqDet, String codUsuario);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @return Lista de documentos relacionados los item de los requerimientos asociados.
	 */
	public List<SolRequerimientoAdjunto> getAllDocumentosByItem(String codCotizacion, String codCotizacionDet);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @return Lista de documentos relacionados directamente al detalle de la cotizacion
	 */
	public List<CotizacionAdjunto> getAllDocumentosCompByItem(String codCotizacion, String codCotizacionDet
			, String codProveedor);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @param codTipoAdjunto
	 * @param nombreAdjunto
	 * @param nombreGenerado
	 * @param codUsuario
	 * @return Insertar documentos relacionados al detalle de una cotizacion
	 */
	public String insertDocumentoByItem(String codCotizacion, String codCotizacionDet, String codTipoAdjunto,
			String codProveedor, String nombreAdjunto, String nombreGenerado, String codUsuario);
	
	/**
	 * @param codCotizacion
	 * @param codCotizacionDet
	 * @param codAdjunto
	 * @param codUsuario
	 * @return Elimina un documento relacionado al detalle de una cotizacion
	 */
	public String deleteDocumentoByItem(String codCotizacion, String codCotizacionDet, String codAdjunto
			, String codUsuario);

	//ALQD,07/08/09.A�ADIENDO NUEVO PARAMETRO
	public List<Cotizacion> GetAllCotizacionesExixtentes(String codSede
			, String codTipoCotizacion, String nroCotizacion, String fechaIni
			, String fechaFin, String codTipoPago, String codSubTipoCotizacion
			, String indInversion);
	
	public String UpdateDocumentosReqACot(String codCotizacion, String codCotizacionDetalle, String codUsuario);
	
	public String UpdateProveedorByItem(String codCotizacion, String codCotizacionDetalle, String cadProveedores,
			String cadIndicadores, String cantidad, String codUsuario);
	
	public String EnviarCotizacionUsuario(String codCotizacion, String codUsuario);
	
	public List GetAllBienesByProveedor(String codProveedor, String codCotizacion);
	
	public List GetAllCondicionesByProveedor( String codCotizacion, String codProveedor);
	
	public String InsertBienesByProveedor( CotizacionDetalle obj, String codUsuario);
	
	public String InsertCondicionesByProveedor(CotizacionDetalle obj, String codUsuario);
	
	public String EnviarCotizacionByProveedor(String codCotizacion, String codProveedor);
	//ALQD,30/01/09. NUEVO PARAMETRO PARA FILTRAR COTs SIN OC
	public List<Cotizacion> GetAllSolicitudCotizacion(String codSede, String nroCotizacion, 
		String fechaInicio, String fechaFin, String codEstado, String codTipoReq, String codSubTipoReq,
		String codTipoPago, String rucProveedor, String dscProveedor, String codPerfil, String codUsuario,
		String codGrupoServ, String indCotSinOC);
	
	public List<CotizacionProveedor> GetAllBandejaGenerarOrdenCompra(String codCotizacion);
	
	public List<CotizacionProveedor> GetAllCotizacionByProveedor(String codCotizacion, String codProveedor);
	
	public String InsertCotSolGenerarOrdenCompraByProveedor(String codCotizacion, String codProveedor,
			String cadCodCotizacionDet,	String cadDscCotizacionDet, String cantidad, String codUsuario);
	
	/**METODO QUE RETORNA EL DETALLE DE LA COTIZACION DADO UN CODIGO DE COTIZACION
	 * @param string
	 * @return
	 */
	public List getLstDetalleCotizacion(String string);
	
	/**
	 * 
	 * @param string
	 * @return
	 */
	public List getLstDetalleAprobacion(String string);
	
	/**METODO QUE ACTIALIZA EL DETALLE DE UNA COTIZACION
	 * @param txhCodCotizacion
	 * @param cadCodDet
	 * @param cadCodProv
	 * @param cantidad
	 * @param usuario
	 * @return
	 */
	public String UpdateDetProvBndjCotiComp(String txhCodCotizacion,
			String cadCodDet, String cadCodProv, String cadPrecios, String cantidad, String usuario);
	/**METODO ENCARGADO DE CERRAR LA COTIZACION
	 * @param txhCodCotizacion
	 * @param usuario
	 * @return
	 */
	public String UpdateCerrarCotizacion(String txhCodCotizacion, String usuario);

	/**RETORNA UNA LISTA DE DETALLE DE UN CONJUNTO DE PRODUCTOS
	 * @param codGuia
	 * @param codBien
	 * @return
	 */
	public List getAllRespByActivo(String codGuia, String codBien);

	/**METODO QUE LISTA LOS PROVEEDORES POR COTIZACION
	 * @param txhCodCotizacion
	 * @return
	 */
	//ALQD,01/06/09. A�ADIENDO NUEVO PARAMETRO
	public List getAllProvSenvByCoti(String txhCodCotizacion,String codProveedor);
	
	public String DeleteCondicionCotizacion(String codCotizacion,  String codUsuario);
	
	public List GetEnvioCorreoProveedor(String codCotizacion);
	
	public String GetTipoServicioConCotizacion(String codSede, String codUsuario, String codPerfil,
			String codGrupoServ);

	public String InsertDetalleCotizacionExistente(String codUsuario, String codCotizacion
			, String cadCodigos, String cantCodigos);
	
	public List GetAllCotizacionById(String codCotizacion);

	public String actualizarFechas(String codCotizacion,String codUsuario,String fechaIni,String horaIni,String fechaFin,String horaFin);
	
	//ALQD,12/08/09.NUEVO METODO PARA RECUPERAR LA CANTIDAD DE PENDIENTES POR TIPO
	public List<ResumenPendientePorCotizar> GetResumenPendientePorCotizar(String codSede);
	
}
