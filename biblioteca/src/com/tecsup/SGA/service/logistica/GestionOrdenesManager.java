package com.tecsup.SGA.service.logistica;

import java.util.List;

import com.tecsup.SGA.modelo.CotizacionProveedor;
import com.tecsup.SGA.modelo.DetalleOrden;
import com.tecsup.SGA.modelo.OrdenesAprobadas;

public interface GestionOrdenesManager {

	public List<CotizacionProveedor> GetAllBandejaConsultaOrdenes(String codSede, String codUsuario, 
			String codPerfil, 
			String nroOrden, String fechaInicio, String fechaFin,String codEstado,
	    	String rucProvee, String nomProvee, String codComprador, String nomComprador, String codTipoOrden);
	
	public List<DetalleOrden> GetAllDetalleByOrden(String codOrden);
	
	public List<DetalleOrden> GetAllCondicionByOrden(String codOrden);
	//ALQD,19/02/09.AGREGANDO NUEVO PARAMETRO
	public String UpdateRechazarOrden(String codOrden, String codUsuario,
			String desComentario);
	//ALQD,19/02/09.AGREGANDO NUEVO PARAMETRO
	public String UpdateAprobarOrden(String codOrden, String codUsuario,
			String indInversion);
	
	public List<DetalleOrden> GetAllOrdenById(String codOrden);
	
	public String UpdateCerrarOrden(String codOrden, String codUsuario);
	
	public List<OrdenesAprobadas> GetEnvioCorreoOrden(String codOrden);
	
	/**
	 * 
	 * @param codOrden
	 * @return
	 */
	public List getSolicitantesOrdenCompra(String codOrden); 
	//ALQD,21/10/08. AGREGANDO UNA PROPIEDAD MAS
	public List GetAllAprobadorByOC(String codOrden);
	//ALQD,09/01/09. NUEVO METODO QUE LISTA OCs PENDIENTE DE ENTREGA
	public List GetAllOC_pendienteXentregar(String codComprador);
	//ALQD,15/07/09.NUEVOS METODOS PARA CONSULTAR,GRABAR Y ELIMINAR LOS SUPLENTES Y SUPLANTADOS
	public List GetAllAprobacionOcSuplantandoA(String codUsuario, String tipoAprobacion);
	public List GetAllAprobacionOcSuplantadoPor(String codUsuario, String tipoAprobacion);
	//ALQD,16/07/09.NUEVO METODO PARA ACTUALIZAR LOS SUPLENTES DE APROBACION
	public String UpdateSuplenteAprobacionOc(String codPrincipal, String codSede
			, String cadAprobacion);
	
}
