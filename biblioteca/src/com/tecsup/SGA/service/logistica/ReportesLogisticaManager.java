package com.tecsup.SGA.service.logistica;

import java.util.List;

import com.tecsup.SGA.modelo.ReportesLogistica;

public interface ReportesLogisticaManager {
	
	public List<ReportesLogistica> GetAllIngresoAlmacen(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia);
	
	public List<ReportesLogistica> GetAllSalidaAlmacen(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codCentroCosto, String codTipoGasto);
	
	public List<ReportesLogistica> GetAllKardezValorizado(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia, String codBien);
	//ALQD,22/09/09.SE AGREGA UN PARAMETRO MAS PARA SALDO > 0
	public List<ReportesLogistica> GetAllMaestrosSaldos(String codSede, String codTipoBien, String periodo
			, String codTipoFamilia, String conSaldo);
	
	public List<ReportesLogistica> GetAllCierreOperaciones(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,	String fecInicio, String fecFin,
			String codCentroCosto, String codTipoGasto);
	
	public List<ReportesLogistica> GetAllAcumuladoSalidaByArticulo(String codSede, String codTipoBien,
			String fecInicio, String fecFin, String codTipoFamilia, String codBien,
			String codCentroCosto, String codTipoGasto);
	
	public List<ReportesLogistica> GetAllSolicitudCotizaciones(String codSede, String codTipoReq, String codTipoBien, String codTipoServicio,
			String fecInicio, String fecFin, String codProveedor);
	
	public List<ReportesLogistica> GetAllRequerimientoValorizado(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,
			String fecInicio , String fecFin, String usuSolicitado, String codEstadoReq);
	
	public List<ReportesLogistica> GetAllOrdenesAsignadasByProveedor(String codSede, String codTipoReq,
			String codTipoBien, String codTipoServicio,
			String fecInicio , String fecFin, String indiceLocalidad, String codProveedor
			, String codTipoMoneda, String codTipoPago);
	
	public List<ReportesLogistica> GetAllProgramaCronogramaPago(String codSede, String fecInicio,
			String fecFin, String indiceValidado,
			String codTipoOrden, String codTipoPago, String codEstadoComp , String dscProveedor);

	public List getAllIngresoAlmacenDev(String codSede, String codTipoBien, String fecInicio
			, String fecFin, String codTipoFamilia);
	
	public List<ReportesLogistica> GetAllOrdenesInversion(String codSede, String fecInicio, String fecFin);
}
