package com.tecsup.SGA.service.logistica;

import java.util.List;

import com.tecsup.SGA.modelo.Guia;
import com.tecsup.SGA.modelo.GuiaDetalle;
import com.tecsup.SGA.modelo.NotaSeguimiento;
import com.tecsup.SGA.modelo.SolRequerimiento;

public interface AtencionRequerimientosManager {
	//ALQD,13/11/08. A�ADIENDO NUEVO PARAMETRO DE BUSQUEDA
	//ALQD,23/01/09. A�ADIENDO NUEVO PARAMETRO DE FILTRO
	public List<SolRequerimiento> GetAllBandejaAtencionRequerimientos(String codSede, String codTipoReq, String codSubTipoReq,
			String nroReq, String fecInicio, String fecFin, String codCeco,String nombreUsuarioSol,
			String codEstado, String nroGuia, String indSalPorConfirmar);	
	
	//ALQD,17/04/09. NUEVO PARAMETRO DE FILTRO
	public List<SolRequerimiento> GetAllBandejaAtencionReqMan(SolRequerimiento obj, String codSede, 
			String codUsuario, String codPerfil, String codEstado);
	
	public String UpdateCerrarRequerimiento(String codRequerimiento, String codUsuario);
	
	public List<Guia> GetAllGuiasAsociadas(String codRequerimiento);
	
	public List<Guia> GetGuiaRequerimiento(String codGuia);
	
	public List<GuiaDetalle> GetAllDetalleGuiaRequerimiento(String codGuia);
	
	public String InsertGuiaRequerimiento(String codRequerimiento, String codUsuario);
	
	public String DeleteGuiaRequerimiento(String codGuia, String codUsuario);
	
	public String InsertDetalleGuiaRequerimiento(GuiaDetalle obj, String codUsuario);
	
	public List<NotaSeguimiento> GetAllNotasSeguimientoRequerimiento(String codRequerimiento,String codNota);
	
	public String InsertNotaSeguimiento(String codReq, String desNota, String codPerfil,String usuario);
	
	public String UpdateNotaSeguimiento(String codReq, String codNota,String desNota,String usuario);
	
	public String DeleteNotaSeguimiento(String codRequerimiento, String codNota, String codUsuario);
	
	public List<GuiaDetalle> GetGuiaMantenimiento(String codRequerimiento);
	
	public String InsertGuiaMantenimiento(GuiaDetalle obj, String codUsuario);
	
	public String RegistrarConformidad(String codGuia, String codUsuario);
	
	public String RegistrarConformidadMantenimiento(String codRequerimiento, String codUsuario);
	
	public List<SolRequerimiento> GetAllSolicitudesReqRel(String codRequerimiento);

	/**METODO PARA GUARDAR EL DETALLE DEL PRODUCTO POR ACTIVO
	 * @param codGuia
	 * @param codBien
	 * @param cadCodBienDet
	 * @param cadCodUsuResp
	 * @param cadCodDescUbicacion
	 * @param cantidad
	 * @param codUsuario
	 * @return
	 */
	public String InsertRespByActivo(String codGuia, String codBien,
			String cadCodBienDet, String cadCodUsuResp,
			String cadCodDescUbicacion, String cantidad, String codUsuario);

	/**METODO PARA ELIMINAR UN DETALLE DEL PRODUCTO POR ACTIVO
	 * @param codGuia
	 * @param codBien
	 * @param parameter
	 * @param usuario
	 * @return
	 */
	public String deleteRespByActivo(String codGuia, String codBien,
			String parameter, String usuario);
	
	public List GetAllNumerosSeries(String codReq,String codReqDet,String codDev,String codDevDet);
	
	public String InsertNumerosSeries(String codDev, String codDevDet,
			String codBien, String cadCodIng,String cantidad,
			 String codUsuario);
}
 