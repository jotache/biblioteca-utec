package com.tecsup.SGA.common;

/**
 * @author       CosapiSoft S.A.
 * @date         11-sep-07
 * @description  Clase para enviar los mensajes a los usuarios
 * 		
 */
public class CommonMessage {
	//MENSAJES PARA EL USUARIO
	public static final String GRABAR_EXITO = "Se grab� exitosamente.";
	public static final String GRABAR_EXITO_PRESTAMO = "GRABACION_SATISFACTORIA";
	public static final String VOLVER_A_GENERAR_ENCUESTA = "\\nDebe volver a generar la encuesta para actualizar el formato";
	public static final String GRABAR_ERROR = "Problemas al Grabar. Consulte con el Administrador.";
	public static final String CERRAR_ERROR = "Se debe asignar un proveedor para cada uno de los detalles.";	
	public static final String ELIMINAR_EXITO = "Se elimin� exit�samente.";
	public static final String ELIMINAR_ERROR = "Problemas al eliminar. Consulte con el administrador.";
	public static final String REGISTRO_DUPLICADO = "Registro Duplicado";
	public static final String TITULO_DUPLICADO = "T�tulo del material duplicado";
	public static final String ISBN_DUPLICADO = "ISBN duplicado";
	public static final String CATEGORIA_DUPLICADO = "C�digo de Clasificaci�n duplicado"; //JHPR 2008-06-09
	public static final String ALGUNOS_YA_REGISTRADOS = "Se grab� exit�samente. Algunos descriptores ya se econtraban registrados";
	public static final String PROCESO_DUPLICADO = "Los par�metros ingresados coinciden con otro proceso, este no puede ser ingresado.";
	
	//RECLUTAMIENTO
	public static final String RECLUTA_ERROR = " Hubo un error en la Operaci�n. Int�ntelo m�s tarde.";
	public static final String RECLUTA_EXISTE = " La cuenta de correo ingresada ya se encuentra registrada en el sistema.";
	public static final String RECLUTA_NO_EXISTE = " Ud. no se encuentra registrado en el sistema.";
	public static final String RECLUTA_NO_PASSWORD = " La contrase�a del usuario no es correcta.";
	public static final String NO_PASS_ACTUAL = " La clave actual no es correcta.";
	public static final String OK_CAMBIO_PASS = " La clave se modific� exit�samente.";
	public static final String DOC_FECHA_EXIST = " El DNI y fecha de nacimiento ingresados ya se encuentran registrados en el sistema.";
	public static final String DOC_EXIST = " El DNI ingresado ya se encuentra registrado en el sistema.";
	public static final String CONSULTE_ADMIN = " Consulte con el administrador";
	public static final String NO_OFERTAS = " No existen requerimientos de personal en este momento.";
	public static final String OFERTA_OK = " Su postulaci�n se ha realizado exit�samente.";
	public static final String OFERTA_ANTES = " Ud. ya postul� a la oferta seleccionada.";
	public static final String ENVIO_EMAIL = " La contrase�a se ha enviado a la cuenta de correo ingresada.";
	public static final String NO_REGISTROS = "No se encontraron coincidencias";
	public static final String LIBRO_SANCIONADO = "Sancionado";
	public static final String LIBRO_PERMITIDO = "Permitido";	
	public static final String LIBRO_NO_PRESTADO = "El libro no ha sido prestado";
	public static final String LIBRO_PRESTADO = "El alumno ya tiene un ejemplar del libro en pr�stamo.";
	public static final String USUARIO_NO_REGISTRADO = "El c�digo de usuario ingresado no se encuentra registrado en el sistema.";
	public static final String MAT_NO_ELIM = "El material seleccionado no puede ser eliminado por encontrase con pr�stamos y/o reservas activas.";
	public static final String MAX_PRESTADOS = "No se puede realizar la acci�n debido a que el usuario ya lleg� al l�mite de sus pr�stamos y/o reservas.";
	public static final String LIBRO_RETIRADO = "El c�digo de libro seleccionado est� retirado de la biblioteca.";
	public static final String LIBRO_RESERVADO_OTRO_USU = "El c�digo del libro seleccionado ha sido reservado. por otro usuario";
	public static final String LIBRO_PRESTADO_RECIEN = "El libro ha sido prestado recientemente";
	public static final String LIBRO_PRESTADO_HACE_TIEMPO = "USUARIO_ADEUDA_LIBROS_ANTERIORES";
	public static final String LIBRO_NO_REGISTRADO = "El c�digo de material ingresado no se encuentra registrado en el sistema.";
	public static final String USUARIO_SANCIONADO_LIBRO = "El Usuario se encuentra sancionado."; //JHPR 2008-06-27
	public static final String USUARIO_INHABILITADO = "El usuario se encuentra inhabilitado.";
	
	public static final String ENCUESTA_COD_FORMATO_YA_EXISTE = "Error El c�digo del formato ingresado ya existe.";
	public static final String ENCUESTA_NO_SE_PUDE_MODIFICAR = "No se puede modificar la encuesta";
	public static final String ENCUESTA_EXISTE_ALTER_O_DESCRIPCION = "Existe la alternativa o descripci�n";
	public static final String ENCUESTA_EXISTE_GRUPO_O_DESCRIPCION = "Existe el nombre del grupo para la seccion (formato)y encuesta";
	public static final String ENCUESTA_EXISTE_PREGUNTA = "Existe la pregunta para el grupo y encuesta";
	public static final String SEDE_LIMA_COD = "L";
	
	public static final String ERROR_TRANSACCION = "Se produjo un error en la transacci�n.";
	public static final String EXITO_TRANSACCION = "Transacci�n exitosa";
	public static final String ERROR_ESTADO = "El estado de la encuesta debe estar en aplicaci�n";
	public static final String ERROR_TERMINANDO = "Se produjo un error mientras se realizaba el proceso de termino";
	
	public static final String NO_HAY_RESPUESTA = "No se complet� ninguna respuesta para la encuesta.";
	
}
