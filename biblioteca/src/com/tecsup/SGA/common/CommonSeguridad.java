package com.tecsup.SGA.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.tecsup.SGA.bean.UsuarioSeguridad;


public class CommonSeguridad {
	
	private static Log log = LogFactory.getLog(CommonSeguridad.class);
	
	/*CONSTANTES DE SEGURIDAD EVALUACIONES*/	
	public static final String EVA_MANT = "EVA_MANT";  
	public static final String EVA_CARG = "EVA_CARG";
	public static final String EVA_PERF = "EVA_PERF";
	public static final String EVA_ADMI = "EVA_ADMI";
	public static final String EVA_EMPR = "EVA_EMPR";
	public static final String EVA_CONS = "EVA_CONS";
	public static final String EVA_NOTA = "EVA_NOTA";
	public static final String EVA_JDEP = "EVA_JDEP";
	public static final String EVA_DIRA = "EVA_DIRA";
	
	/*CONSTANTES DE SEGURIDAD RECLUTAMIENTO*/	
	public static final String REC_MANT = "EVA_MANT";  
	public static final String REC_REGI = "REC_REGI";
	public static final String REC_PROC = "REC_PROC";
	public static final String REC_EVAL = "REC_EVAL";
	public static final String REC_INFO = "REC_INFO";
	
	public static final String EVA_PERFIL_ESTUDIANTE = "SGA_EVA - Estudiante";
	public static final String BIB_PERFIL_ADMINISTRADOR = "SGA_BIB - Administrador";
	public static final String BIB_PERFIL_ADMINISTRADOR_TOTAL = "SGA_BIB - Administrador Total";
	
	public static final String LOG_PERFIL_JEFLOG = "SGA_LOG - LJLOG";
	
	public static int getValidaOpcion(Object obj,String opcion){		
		if(obj!=null){			
			if(((UsuarioSeguridad)obj).getOpciones().indexOf(opcion)!=-1)
				return 1;
			else
				return 0;			
		}else 
			return 0;
	}
	
	public static boolean tieneOpcion(String opciones, String Opcion)
	{
		boolean flag = false;
		if ( opciones.indexOf(Opcion) >= 0 ) flag = true;
		return flag;
	}
}
