package com.tecsup.SGA.common;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class Archivo {
	protected static Log log = LogFactory.getLog(new Archivo().getClass());
	
	public static void downloadFile(String filename, String source, HttpServletResponse response) throws IOException, FileNotFoundException {		
		log.info("downloadFile :" + source);
		try{
			File archivo = new File(source);
			response.setHeader("Content-Length", archivo.length()+"");
			response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");
			download(filename, source,response);
		}catch(FileNotFoundException e){			
			log.error(e);
		}
	}
	
	
	private static void download(String filename, String source, HttpServletResponse response) throws IOException, FileNotFoundException {		
		log.info("download :" + source);
		int data;
		ServletOutputStream out = null;
		BufferedInputStream buffer = null;
		try {
			
			buffer = new BufferedInputStream(new FileInputStream(source));
			out = response.getOutputStream();
			log.info("Inicio de descarga de archivo...");
			while ((data = buffer.read()) != -1)
				out.write(data);
			log.info("Archivo descargado ");
			buffer.close();
			out.flush();
			out.close();

		} catch (FileNotFoundException e){
			log.error("Archivo No Existe");
			throw e;
		} catch (Exception e) {
			log.error("Descarga Cancelada");
		} finally {
			if (buffer != null) {
				buffer.close();
			}
		}
	}
	
}
