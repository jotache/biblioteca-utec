package com.tecsup.SGA.common;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.bean.ConsultaByProfesorConBean;
import com.tecsup.SGA.bean.VerEncuestaBean;

public class MetodosConstants {
	
	private static Log log = LogFactory.getLog(MetodosConstants.class);
	
	public String GenerarNombreArchivoAdjuntar(String nombreTipo, String extension){
		 String nombre="";
		 String fechas=Fecha.getFechaActual();
		 fechas=fechas.replace("/", "");
		 String hora=Fecha.getHoraActual1();
		 hora=hora.replace(":", "");
		 nombre=nombreTipo+fechas+hora+"."+extension;
		 return nombre;
	}

	/**RETORNA EL MENSAJE "SE DEBE VOLVER A GENERAR LA ENCUESTA" SI 
	 * EL codEncuesta es PENDIENTE SINO RETORNA "" 
	 * @param codEncuesta
	 * @return
	 */
	public static String getMensajeEncuesta(String codEncuesta){
		 
		String mensaje="";
		
		if(!CommonConstants.COD_ENCUESTA_PENDIENTE.equalsIgnoreCase(codEncuesta))
			return CommonMessage.VOLVER_A_GENERAR_ENCUESTA; 
		else			
			return mensaje;
	}
	
	public static String getSumAlumnosxEncuestados(List lst){
		 
		String mensaje="";
		int sum = 0;
		ConsultaByProfesorConBean objCurrent = null;
		
		try{
				if(lst!=null && lst.size()>0){
					for( int i=0;i<lst.size();i++ ){
						objCurrent = (ConsultaByProfesorConBean) lst.get(i);
						log.info("parse int>"+objCurrent.getNroEncuestadoxGrupo()+">>");
						sum = sum + Integer.parseInt(objCurrent.getNroEncuestadoxGrupo());
						log.info("sali:"+i);
					}
				}
				log.info(">SUM>"+sum+">>");
				
				return ""+sum;
				
		}catch(Exception e){
			log.info("ERROR DE PARSING");
				return "0";
		}
		
	}
	
	public static String getPromedioxFila(ConsultaByProfesorConBean obj){
		
		String global="";
		
		int sum = 0;
		ConsultaByProfesorConBean objCurrent = null;
		BigDecimal sumTotal = null;
		
		try{
			/*List listaTemp = obj.getListaGrupos();
			ConsultaByProfesorConBean objGrupo = null;
			sumTotal = new BigDecimal("0");				
			//PINTA LAS COLUMNAS DINAMICAMENTE
			if(listaTemp!=null && listaTemp.size()>0)
			{
			
				for(int k=0;k<listaTemp.size();k++){
					objGrupo = (ConsultaByProfesorConBean) listaTemp.get(k);
					objGrupo.setTotalGrupo(objGrupo.getTotalGrupo().replace(",","."));
					sumTotal = sumTotal.add(  new BigDecimal( "".equalsIgnoreCase(objGrupo.getTotalGrupo())?"0":objGrupo.getTotalGrupo() ) );	
				}
			}
			
			global = (listaTemp==null || listaTemp.size()==0 )?"0":sumTotal.divide(new BigDecimal(""+listaTemp.size()),2,BigDecimal.ROUND_UP).toString();
			*/
			
			//cambio
			List listaTemp = obj.getListaGrupos();
			ConsultaByProfesorConBean objGrupo = null;
			sumTotal = new BigDecimal("0");				
			//PINTA LAS COLUMNAS DINAMICAMENTE
			if(listaTemp!=null && listaTemp.size()>0)
			{
				for(int k=0;k<listaTemp.size();k++){
					objGrupo = (ConsultaByProfesorConBean) listaTemp.get(k);
					log.info(">>"+k+">>getSumTotalGrupo>>"+objGrupo.getSumTotalGrupo()+">>");
					//objGrupo.setTotalGrupo(objGrupo.getTotalGrupo().replace(",","."));
					sumTotal = sumTotal.add(  new BigDecimal( "".equalsIgnoreCase(objGrupo.getSumTotalGrupo())?"0":objGrupo.getSumTotalGrupo() ) );	
				}
			}

			global = (listaTemp==null || listaTemp.size()==0 )?"0":sumTotal.divide(new BigDecimal(""+listaTemp.size()),2,BigDecimal.ROUND_UP).toString();
			
			
			return global;
				
		}catch(Exception e){
			log.info("ERROR DE PARSING");
				return "0";
		}
		
	}
	
	public static String getLeyenda(VerEncuestaBean obj){
		
		StringBuffer sb = new StringBuffer();
		
		for (int j = 0; j < obj.getLstAlternativas().size(); j++){
			VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
			if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){
				sb.append("<b>"+alt.getNomAlterntiva()+":</b>"+alt.getDesAlternativa()+"&nbsp;&nbsp;<br/>");
			}
		}
		
		return sb.toString();
		
	}
	
	public static String getLeyendaStandar(VerEncuestaBean obj){
		
		StringBuffer sb = new StringBuffer();
		
		for (int j = 0; j < obj.getLstAlternativas().size(); j++){
			VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
			if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){
				sb.append("<b>"+alt.getNomAlterntiva()+":</b>"+alt.getDesAlternativa()+"&nbsp;&nbsp;");
			}
		}
		
		return sb.toString();
		
	}	
	
	public static String getNomAlternativas(VerEncuestaBean obj){
		
		//StringBuffer sb = new StringBuffer();
		String sb = "";
		/*for (int j = 0; j < obj.getLstAlternativas().size(); j++){
			VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
			sb.append("<b>"+alt.getNomAlterntiva()+":</b>"+alt.getDesAlternativa()+"&nbsp;&nbsp;");			
		}*/
		
		sb = sb +"<table width=\"100%\" height=\"100%\" bordercolor=\"red\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		//sb = sb +"<table>";
		sb = sb +"<tr>";
		
		int ancho=0;
		int nroAlternativas=obj.getLstAlternativas().size();
		ancho=100/nroAlternativas;
		String cad = "";
		for (int j = 0; j < obj.getLstAlternativas().size(); j++){
				VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
				sb = sb +"<td align=\"center\" width=\""+ancho+"%\"><b>"+alt.getNomAlterntiva()+"</b></td>";
				cad = cad + alt.getNomAlterntiva();
		}
		
		sb = sb +"</tr>";
		sb = sb +"</table>";		
		
		if(cad.equalsIgnoreCase(""))
			return "";
		
		return sb.toString();
		
	}	
	
	public static String hexDigest(String message) {
		MessageDigest md;
		byte[] buffer , digest;
		String hash= "";
		
		try{
			buffer = message.getBytes("UTF-8");
			md = MessageDigest.getInstance("SHA1");
		} catch(Exception e){
			throw new RuntimeException(e);
		}
		
		md.update(buffer);
		digest = md.digest();
		
		for(byte aux : digest){
			int b  =aux & 0xff;
			String s = Integer.toHexString(b);
			if (s.length()==1) hash += "0";
			hash += s;
		}
		
		return hash;
	}

}
