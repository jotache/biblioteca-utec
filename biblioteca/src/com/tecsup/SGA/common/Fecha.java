package com.tecsup.SGA.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.tecsup.SGA.exception.FormatoInvalidoException;

/************************************************************
 * @author       CosapiSoft S.A.
 * @date         12-sep-07
 * @description  Clase para manejar las fechas
 ************************************************************
 */
public class Fecha {
	private static Log log = LogFactory.getLog(Fecha.class);
/**
 * @return fecha actual tipo Date
 */
	public static Date getCurrentDate()
	{	return new Date();
	}
	
/**
 * @return hora actual tipo Date
 */
	public static Date getCurrentTime(){
		return  new Date();
	}
	
/**
 * @param 	fechaString tipo dd/mm/yyyy
 * @return	objeto Date
 * @throws 	ParseException
 */
	public static Date getFechaStringToDate(String fechaString) throws ParseException{
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
		java.util.Date fecha = sdf.parse(fechaString);
		return fecha;
	}

/**
 * @param  fechaDate
 * @return fecha eb formato dd/mm/yyyy
 * @throws ParseException
 */
	public static String getFechaDateToString(Date fechaDate) throws ParseException{
		String fecha = fechaDate.toString().trim();	String [] v = fecha.split("-");
		String anio =v[0];String mes =v[1];String dia =v[2];fecha=dia.substring(0, 2)+"/"+mes+"/"+anio;
		
		return fecha.trim();	
	}

/**
 * @return fecha de hoy en formato dd/MM/yyyy
 */
	public static String getFechaActual(){
		Date hoy = new Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(hoy).toString();
	}
	
	public static String getFechaActualReporte(){
		Date hoy = new Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM-yyyy");
		return sdf.format(hoy).toString();
	}

	public static String getDiasPorMes(String strMes, String Anho)
	{
		int mes = Integer.parseInt(strMes);
		
		if ( mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
			return "31";
		else if ( mes == 4 || mes == 6 || mes == 9 || mes == 11)
			return "30";
		else return "28";		
	}
/**
 * @return hora actual formato hh:mm
 */
	public static String getHoraActual(){
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("hh:mm");
		return sdf.format(new Date());
	}
	public static String getHoraActual1(){
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("hh:mm:ss");
		return sdf.format(new Date());
	}
	public static String getHoraActualHH(){
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(new Date());
	}
	public static String getHoraActualPM(){
		Calendar calendario = GregorianCalendar.getInstance();
		Date fecha = calendario.getTime();
		/*java.util.Date utilDate = new java.util.Date();
		long lnMilisegundos = utilDate.getTime();		
		java.sql.Time sqlTime = new java.sql.Time(lnMilisegundos);*/

		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("hh:mm");
		return sdf.format(fecha);
	}

/**
 * @param  date
 * @return hora en de un objeto Date formato hh:mm 
 */
	public static String getHoraDateToString(Date date){
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("hh:mm");
		return sdf.format(date);
	}

/**
 * @param fechaes Date
 * @param hora en String tipo hh:mm
 * @return
 */
	public static Date getHoraStringToDate(Date fecha,String hora){
		String [] v = hora.split(":"); 
		fecha.setHours(new Integer(v[0]).intValue()); 
		fecha.setMinutes(new Integer(v[1]).intValue());
		return fecha;
	}

/**
 * @param fecha en String tipo dd/mm/yyyy
 * @return fecha para consultas a la BD tipo yyyy-mm-dd
 * @throws FormatoInvalidoException 
 */
	public static String getFechaBD(String fecha) throws FormatoInvalidoException{
		if(fecha.length()==10){
		String [] v = fecha.split("/");
		String dia =v[0];String mes =v[1];String anio =v[2];fecha=anio+"-"+mes+"-"+dia;
		}else{
			throw new FormatoInvalidoException();
		}	
		return fecha.trim();
	}

/**
 * @return fecha actual en formato yyyy-mm-dd
 * @throws ParseException 
 * @throws FormatoInvalidoException 
 */
	public static String getCurrentDateBD() throws FormatoInvalidoException, ParseException{
	
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
		String fecha =  sdf.format(Fecha.getCurrentDate()).toString();
		return Fecha.getFechaBD(fecha);
	}

/**
* Genera la fecha final apartir de un numero de dias ingresado.
* @param dias
* @param inicio
* @param fin
* @return diafinal
*/
	public static Date getSumaFechaDias(String dias,Date inicio){
		Date fin = new Date();
		int minuto = 60 * 1000;
		int hora = minuto * 60;
		int dia = hora * 24;
		long diasLong = Integer.parseInt(dias);
		long diasDeseados = dia * diasLong;
		long lInicio = inicio.getTime();
		long lFin = lInicio + diasDeseados;
		fin = new Date(lFin);
		log.info("FECHA A GUARDAR:||"+fin+"||");
		return fin;
	}

/**metodo que retorna una fecha en formato dd/mm/yyy dandole substring (0,2) metodo
 * usado por el algritmo de calculo de tiempo
 * @param  fechaDate
 * @return fecha eb formato dd/mm/yyyy
 * @throws ParseException
 */
	public static String getFechaDateToStringSubstring(Date fechaDate) throws ParseException{
		String fecha = fechaDate.toString().trim();	String [] v = fecha.split("-");
		String anio =v[0];String mes =v[1];String dia =v[2];fecha=dia.substring(0, 2)+"/"+mes+"/"+anio;
		
		return fecha.trim();	
	}
}