package com.tecsup.SGA.common;

/************************************************************
 * @author       CosapiSoft S.A.
 * @date         07-sep-07
 * @description  none
 ************************************************************
 */
public class Item {
	private String id;
	private String name;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
