/**
 * 
 */
package com.tecsup.SGA.common;

/************************************************************
 * @author       CosapiSoft S.A.
 * @date         07-sep-07
 * @description  none
 ************************************************************
 */
public class CommonConstants {
	/**/
	public static final String GOOGLEPLUS_CLIENT_ID = "903982786098-6p1dmpfhhas2a5frj49r7r3u23tdc2nu.apps.googleusercontent.com";
	public static final String GOOGLEPLUS_CLIENT_SECRET = "7LK69cd-qax9GC4-M0-WBptJ";
	
	public static final String FLG_TIPO_PRESTAMO   = "0";
	public static final String FLG_TIPO_DEVOLUCION = "1";
	
	//NOMBRE DE VARIABLES QUE RETORNAN DE BASE DE DATOS.
	public static final String RET_STRING = "S_V_RETVAL";
	public static final String RET_CURSOR = "S_C_RECORDSET";
	
	//SUB SISTEMAS
	public static final String SGA_EVA = "sga_eva";
	public static final String SGA_REC = "sga_rec";
	public static final String SGA_ENC = "sga_enc";
	public static final String SGA_BIB = "sga_bib";
	public static final String SGA_LOG = "sga_log";
	
	//ESQUEMAS DE BASE DE DATOS
	public static final String ESQ_SEGURIDAD = "SEGURIDAD";
	public static final String ESQ_GENERAL = "GENERAL";
	public static final String ESQ_LOGISTICA = "LOGISTICA";
	public static final String ESQ_RECLUTAMIENTO = "RECLUTAMIENTO";
	public static final String ESQ_PERSONAL = "PERSONAL";
	public static final String ESQ_MAESTRO = "MAESTRO";
	public static final String ESQ_EVALUACION = "EVALUACION";
	public static final String ESQ_BIBLIOTECA = "BIBLIO";
	public static final String ESQ_ENCUESTA = "ENCUESTA";
	
	//MAESTRO DE TABLAS
	public static final String TIPO_ORDEN_COD = "0";
	public static final String TIPO_ORDEN_DSC = "1";
	public static final String TIPO_ORDEN_VAL2 = "2";
	public static final String TIPO_ORDEN_VAL3 = "3";
	public static final String TIPO_ORDEN_LOG = "5";		
	
	public static final String TIPT_EST_REGISTRO = "0001";
	public static final String TIPT_PROFESION = "0002";
	public static final String TIPT_GRADO_ACADEMICO = "0003";
	public static final String TIPT_MERITO = "0004";
	public static final String TIPT_AREA_INT1 = "0005";
	public static final String TIPT_AREA_INT2 = "0006";
	public static final String TIPT_TIPO_EVA = "0007";
	public static final String TIPT_CALIFICACION = "0008";
	public static final String TIPT_DEDICACION = "0009";
	public static final String TIPT_DISPONIBILIDAD = "0010";
	public static final String TIPT_ESTADO_CIVIL = "0011";
	public static final String TIPT_ESTAPA_PROCESO = "0012";
	public static final String TIPT_SEDE_PREFERENTE = "0013";
	public static final String TIPT_EST_PROCESO_REVISION = "0014";
	public static final String TIPT_SISTEMAS_SGA = "0015";
	public static final String TIPT_EST_PROCESO_SELECCION = "0016";
	public static final String TIPT_IDIOMAS = "0017";
	public static final String TIPT_NIVEL_IDIOMAS = "0018";
	public static final String TIPT_GRADO_COMPRENSION = "0019";
	public static final String TIPT_GRADO_CARGO_PUESTO = "0020";
    public static final String TIPT_SEXO = "0021";
    public static final String TIPT_INTERES_EN = "0022";
    public static final String TIPT_SIMBOLO = "0023";
    public static final String TIPT_COMPETENCIAS = "0026";
    public static final String TIPT_CALIF_FINAL_REV = "0034";
    public static final String TIPT_TIPO_INCIDENCIA = "0032";
    public static final String TIPT_TIPO_COMPONENTES = "0031";
    public static final String TIPT_TIPO_MATERIAL = "0036";
    public static final String TIPT_DESCRIPTOR = "0037";
    public static final String TIPT_CATEGORIA_DEWEY = "0038";
    public static final String TIPT_CLASIFICACION_DEWEY = "0039";
    public static final String TIPT_AUTOR = "0040";
    public static final String TIPT_EDITORIAL = "0041";
    
    public static final String TIPT_IDIOMAS_BIBLIOTECA = "0042";
    public static final String TIPT_TIPO_SECCION = "0043";
    public static final String TIPT_GRUPO_SECCION = "0044";
    public static final String TIPT_TIPO_SALA = "0045";
    public static final String TIPT_SALA = "0046";
    public static final String TIPT_TIPO_VIDEO = "0047";
    public static final String TIPT_TIPO_PROCEDENCIA = "0048";
    public static final String TIPT_TIPO_USUARIO_BIBLIOTECA = "0049";
    public static final String TIPT_TIPO_PRESTAMO = "0050";
    public static final String TIPT_TIPO_MONEDA = "0065";    
    public static final String TIPT_TIPO_REPORTES_ENCUESTA = "0122";
    public static final String TIPT_TIPO_HORAS_BIBLIO = "0087";
    
    public static final String TIPT_OCURRENCIAS = "0078";
    public static final String TIPT_RANGO_SANCIONES = "0079";
    public static final String TIPT_LISTA_OCURRENCIAS = "0080";
    public static final String TIPT_NRO_OCURRENCIAS = "0081";
    
    public static final String TIPT_TIPO_BUSQUEDA = "0072";
    public static final String TIPT_TIPO_SUGERENCIA = "0075";
    public static final String TIPT_CALIFICACION_SUGERENCIA = "0076";
    public static final String TIPT_ESTADO_SUGERENCIA = "0077";
    public static final String TIPT_REPORTES_RECLUTAMIENTO = "0124";
    
    public static final String TIPT_PARAMETROS_GRALES = "0027";
    public static final String TIPT_PAIS = "1000";
    public static final String TIPT_CIUDAD = "2000";
    
    public static final String TIPT_TIPO_REPORTES = "0098";
    public static final String TIPT_REPORTES_MAT_BIB = "0102";
    public static final String TIPT_REPORTES_MOV_PRES_DEV_DIA = "0104";
    public static final String TIPT_REPORTES_EST_CICLO_ESP = "0105";
    public static final String TIPT_REPORTES_TIPO_USUARIOS = "0106"; //Sancionados / Deudores
    public static final String TIPT_TIPO_BUSQUEDA_Y_DEWEY = "0108"; //Sancionados / Deudores
    public static final String TIPT_TIPO_APLICACION = "0109"; //-- 0109 = Tipo Aplicaci�n 
    public static final String TIPT_TIPO_ENCUESTA = "0110"; //-- 0110 = Tipo Encuesta
    public static final String TIPT_TIPO_ESTADO_ENCUESTA = "0111"; //-- 0111 = Estados Formatos Encuesta
    public static final String TIPT_TIPO_FORMATO_ENCUESTA_01 = "0113";
    public static final String TIPT_TIPO_PREGUNTA = "0114"; //-- 0111 = tipo de preguntas para encuestas
    public static final String TIPT_REPORTES_MOV_RESERV_DEV_DIA = "0128";// tipo de movimientos estad�sticos de reservas.
    public static final String COD_TIP_SANCION_X_PRESTAMO_MAT = "0001"; //codigo sancion x prestamo de material
    
	//Globales
    public static final String TIPT_TIPO_ENCUESTA_SERVICIO = "0112"; //-- 0112 = Tipo Encuesta servicio
    public static final String TIPT_TIPO_SECCIONES_ENCUESTA = "0113"; //-- 0113 = Secciones de Encuesta 
    public static final String TIPT_TIPO_REPORTES_EVA = "0116";
    public static final String TIPT_TIPO_DOCUMENTOS = "0117"; //-- 0113 = Secciones de Encuesta
    public static final String TIPT_TIPO_ENCUESTA_PARAM_GENERALES = "0126";    
    
    //Globales
	public static final String GSTR_INSTI_EDUCATIVA = "441";
	
	public static final String GSTR_SEXO_FEMENINO = "1";
	public static final String GSTR_SEXO_MASCULINO = "2";
	
	public static final String GSTR_NAC_PERUANO = "0";
	public static final String GSTR_NAC_EXTRANJERO = "1";

	//ETAPAS DE LOS PROCESOS
	public static final String PROC_ETAPA_REVISION = "0001";
	public static final String PROC_ETAPA_SELECCION = "0002";
	
	//TIPOS DE INSERCION DE POSTULANTES
	public static final String INS_POST_BUS = "1";
	public static final String INS_POST_OTR = "2";
	
	//TIPOS DE INSERCION DE EVALUACIONES
	public static final String EVAL_ACT_ESTADO = "1";
	public static final String EVAL_NO_ACT_ESTADO = "0";	
	//ESTADOS PROCESOS EN REVISION
	public static final String EST_REV_SIN_REVISION = "0001";
	public static final String EST_REV_REV_X_RRHH = "0002";
	public static final String EST_REV_REVISADO_NO_ENVIADO = "0006";
	public static final String EST_REV_REV_X_JEFE_DPTO = "0003";
	public static final String EST_REV_PRESELECCIONADO = "0004";
	public static final String EST_REV_NO_PRESELECCIONADO = "0005";
	
	public static final String CAL_REV_FIN_PRESELECCIONADO = "0001";
	public static final String CAL_REV_FIN_NO_PRESELECCIONADO = "0002";
	public static final String CAL_REV_FIN_INACTIVO = "0003";

	//ESTADOS PROCESOS EN SELECCION
	public static final String EST_SEL_SIN_EVALUAR = "0001";
	public static final String EST_SEL_ENVIADO_EVAL = "0002";
	public static final String EST_SEL_EVALUADO = "0003";
	public static final String EST_SEL_ACEPTADO = "0004";
	public static final String EST_SEL_NO_ACEPTADO = "0005";
		
	public static final String CAL_SEL_FIN_ACEPTADO = "0001";
	public static final String CAL_SEL_FIN_NO_ACEPTADO = "0002";
	public static final String CAL_SEL_FIN_INACTIVO = "0003";
	//TIPOS DE ROLES
	public static final String ROL_JEFE_DEP = "212";
	public static final String ROL_EVALUADOR = "213";
	public static final String ROL_JEFE_EVALUADOR = "214";
	
	//Extensiones
	public static final String GSTR_EXTENSION_GIF = "gif";
	public static final String GSTR_EXTENSION_JPG = "jpg";
	public static final String GSTR_EXTENSION_PDF = "pdf";
	public static final String GSTR_EXTENSION_DOC = "doc";
	public static final String GSTR_EXTENSION_DOCX = "docx";
	public static final String GSTR_EXTENSION_XLS = "xls";
	public static final String GSTR_EXTENSION_XLSX = "xlsx";
	
	//public static final String SERVIDOR_NAME = "G-OC";
	//CAMBIO PARA COMUNICACION INTERNET
	public static final String DIRECTORIO_PROY = "file/sga/";
	public static final String DIRECTORIO_PROY_REC = "file/sga-rec/";

	//Repositorio
	public static final String DIRECTORIO_DATA = "/var/data/public/sga/";
	public static final String DIRECTORIO_DATA_PRIVADO = "/var/data/sga/";
	
	//DIRECT PARA IMAGENES DEL MATERIAL
	public static final String DIRECTORIO_IMAGENES = "upload/SGA_FILE_SERVER/SGA_MATERIAL_BIB/IMAGENES/";
	//parte de secciones
	public static final String DIRECTORIO_SECCIONES = "upload/SGA_FILE_SERVER/SGA_SECCIONES/";
	//DIRECT PARA DOCUMENTOS DEL MATERIAL
	public static final String DIRECTORIO_DOCUMENTOS = "upload/SGA_FILE_SERVER/SGA_MATERIAL_BIB/DOCUMENTOS/";
	//busqueda WEB NOVEDADES
	public static final String DIRECTORIO_BUSQUEDA= "upload/SGA_FILE_SERVER/SGA_MATERIAL_BIB/IMAGENES/";
	//portal web BUSQUEDA	
	public static final String DIRECTORIO_NOVEDADES= "upload/SGA_FILE_SERVER/SGA_SECCIONES/";
	
	public static final String DIRECTORIO_STR_RUTA_POSTULANTE= "upload/SGA_FILE_SERVER/SGA_POSTULANTE/";
	public static final String DIRECTORIO_STR_RUTA_POSTULANTE_TMP= "/var/data/sga/upload/SGA_FILE_SERVER/SGA_POSTULANTE/";
	
	public static final String DIRECTORIO_STR_RUTA_POSTULANTE_FOTO= "upload/SGA_FILE_SERVER/SGA_POSTULANTE/FOTO/";
	public static final String DIRECTORIO_STR_RUTA_POSTULANTE_FOTO_TMP= "/var/data/sga/upload/SGA_FILE_SERVER/SGA_POSTULANTE/FOTO/";
	
	public static final String DIRECTORIO_STR_RUTA_POSTULANTE_INFFIN= "upload/SGA_FILE_SERVER/SGA_POSTULANTE/INFORME_FINAL/";
	
	public static final String SERVIDOR_PUERTO_WEB = "www.tecsup.edu.pe/";
			
	public static final String SERVIDOR_NAME = "www.tecsup.edu.pe/";
	public static final String GSTR_HTTP = "http://";
	
	public static final String STR_RUTA_CARGA_OPERATIVA = "upload/SGA_FILE_SERVER/SGA_CARGA_OPERATIVA/";
	
	public static final String SERVIDOR_IP = "192.168.72.11";//"192.168.68.111";	
		
	public static final String STR_RUTA_POSTULANTE = "upload/SGA_FILE_SERVER/SGA_POSTULANTE/";
	public static final String STR_RUTA_POSTULANTE_FOTO = "upload/SGA_FILE_SERVER/SGA_POSTULANTE/FOTO/";
	public static final String STR_RUTA_POSTULANTE_INFFIN = "upload/SGA_FILE_SERVER/SGA_POSTULANTE/INFORME_FINAL/";
	public static final String STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN = "upload/SGA_FILE_SERVER/SGA_PRODUCTOS_LOG/SGA_IMAGENES_LOG/";
	public static final String STR_RUTA_CATALOGO_PRODUCTOS_DOCUMENTO = "upload/SGA_FILE_SERVER/SGA_PRODUCTOS_LOG/SGA_DOCUMENTOS_LOG/";
	public static final String STR_RUTA_ACTUALIZAR_INVENTARIO_DOCUMENTO = "upload/SGA_FILE_SERVER/SGA_ALMACEN_LOG/SGA_DOCUMENTOS_LOG/";
	public static final String FILE_NAME_CV = "CV_";
	public static final String FILE_NAME_INF = "INF_";
	public static final String FILE_NAME_IMG = "IMG_";
	public static final String FILE_NAME_EMPRESA = "EMPRESA";
	public static final String FILE_NAME_PRODUCTO = "DOC";
	public static final String FILE_NAME_INVENTARIO = "INVENTARIO";
	public static final String STR_RUTA_CATALOGO_PRODUCTOS_IMAGEN_02 = "upload/SGA_FILE_SERVER/SGA_PRODUCTOS_LOG/SGA_IMAGENES_LOG/";
	public static final String STR_RUTA_CATALOGO_PRODUCTOS_DOCUMENTO_02 = "upload/SGA_FILE_SERVER/SGA_PRODUCTOS_LOG/SGA_DOCUMENTOS_LOG/";
	public static final String STR_RUTA_ACTUALIZAR_INVENTARIO_DOCUMENTO_02 = "upload/SGA_FILE_SERVER/SGA_ALMACEN_LOG/SGA_DOCUMENTOS_LOG/";
	public static final String STR_RUTA_REGISTRAR_INVENTARIO = "upload/SGA_FILE_SERVER/SGA_ALMACEN_LOG/SGA_REGISTRAR_INVENTARIO/";
	
	//ETAPAS DE LOS PROCESOS
	public static final String E_C_TTDE_EST_REG ="A";
	public static final String E_C_TIPO = "2";
	public static final String TIPT_TIPOEVALUACION = "0033";
	
	//TIPOS OPERACIONES INCIDENCIAS
	public static final String TIPO_SESION_TEO = "2324";//"2324";
	public static final String TIPO_SESION_LAB = "2326";//"2326";
	public static final String TIPO_SESION_TAL = "2325";//"2325";
	public static final String INC_TIPO_OPE_DOC = "0";
	public static final String INC_TIPO_OPE_ADM = "1";
	public static final String INC_TIPO_SANCION = "0002";
	
	public static final String TIPT_CONCEPTOS = "0025";
	public static final String TIPT_PERFIL = "0024";
	
	//CALIFICACIONES DE COMPETECNIAS
	public static final int PORCENTAJE_TOTAL = 100;
	public static final int NOTA_MAX_TECSUP = 20;
	public static final String COMP_IND_DOC = "0";
	public static final String COMP_IND_OTR = "1";
	public static final String COMP_CAL_EXCELENTE = "0001";
	public static final String COMP_CAL_BUENO = "0002";
	public static final String COMP_CAL_MEDIA = "0003";
	public static final String COMP_CAL_BAJA = "0004";
	
	//FORMACION DE EMPRESA
	public static final String COD_CURSO = "6";
	public static final String COD_FORMACION_EMPRESA = "1";
	public static final String COD_FORMACION_EMPRESA2 = "100";
	/////
	public static final String STR_RUTA_DOCUMENTOS = "/SGA_FILE_SERVER/SGA_DOCUMENTOS/";
	public static final String STR_RUTA_DOCUMENTOS_SECCIONES = "/SGA_FILE_SERVER/SGA_SECCIONES/";
	public static final String STR_RUTA_MATERIAL_BIB = "/SGA_FILE_SERVER/SGA_MATERIAL_BIB/";
	public static final String STR_RUTA_MATERIAL_BIB_IMAGENES = "/SGA_FILE_SERVER/SGA_MATERIAL_BIB/IMAGENES/";
	public static final String STR_RUTA_MATERIAL_BIB_DOCUMENTOS = "/SGA_FILE_SERVER/SGA_MATERIAL_BIB/DOCUMENTOS/";
	public static final String STR_RUTA_SECCIONES = "/SGA_FILE_SERVER/SGA_SECCIONES/";
	public static final String FILE_NAME_DOC = "DOC_";
	public static final String STR_RUTA_DOCUMENTOS_02 = "upload/SGA_FILE_SERVER/SGA_DOCUMENTOS/";
	
	//ESCALA DE PARAMETROS ESPECIALES
	public static final String PARAM_ESP_CAL_EXCELENTE = "0001";
	public static final String PARAM_ESP_CAL_BUENA 	   = "0002";
	public static final String PARAM_ESP_CAL_MEDIA 	   = "0003";
	public static final String PARAM_ESP_CAL_BAJA 	   = "0004";
	
	//NOTA CASOS ESPECIALES
	public static final String COD_PRODUCTO_CAT = "5";	
	public static final String COD_NOTA_EXTERNO = "0";
	public static final String COD_NOTA_CICLO_EXTERNO_CAT = "1";
	
	//UPDATE & DELETE MANTENIMIENTO BIBLIOTECA
	public static final String UPDATE_MTO_BIB = "0";
	public static final String DELETE_MTO_BIB = "1";
	public static final String MTO_CATALOGO_ESTADO = "2";
	public static final String DELETE_MTO_LOG = "3";
	public static final String FAMILIA_MTO_LOG = "4";
	public static final String TIPT_FERIADO = "0073";
	public static final String TIPT_FERIADO_UTEC = "0137";
	public static final String TIPT_SERVICIO_USUARIO = "0074";
	public static final String TIPT_PARAMETROS_RESERVA = "0085";
	
	//CONSTANTES PARA LOGISTICA
	public static final String TIPT_SEDE = "0051";
	public static final String TIPT_FAMILIA = "0052";
	public static final String TIPT_SUB_FAMILIA = "0053";
	public static final String TIPT_TIPO_BIEN = "0054";
	public static final String TIPT_UNIDAD_MEDIDA = "0055";
	public static final String TIPT_DESCRIPTOR_LOG = "0056";
	public static final String TIPT_UBICACION = "0057";
	public static final String TIPT_GRUPO_SERVICIO  = "0058";
	public static final String TIPT_TIPO_SERVICIO = "0059";
	public static final String TIPT_TIPO_ADJUNTOS = "0060";
	public static final String TIPT_CONDICICONES_COMPRA = "0061";
	public static final String TIPT_TIPO_CALIFICACION = "0062";
	public static final String TIPT_TIPO_ESTADO = "0063";
	public static final String TIPT_GIRO = "0064";
	public static final String TIPT_MONEDA = "0065";
	public static final String TIPT_TIPO_PROVEEDORES= "0066";
	public static final String TIPT_PROVEEDORES = "0067";
	public static final String TIPT_TIPO_GASTOS= "0068";
	public static final String TIPT_TIPO_PERSONAL= "0069";
	public static final String TIPT_TIPO_REQUERIMIENTO = "0070";
	public static final String TIPT_TIPO_FLUJO = "0071";
	public static final String TIPT_CONDICIONES_BUSQUEDA= "0082";
	public static final String TIPT_VALOR = "-1";
	public static final String TIPT_ESTD_REQUERIMIENTO = "0086";
	public static final String TIPT_ESTD_ROLES_fLUJO = "0088";
	public static final String TIPT_TIPO_PAGO= "0090";
	public static final String TIPT_NIVEL_COMPLEJIDAD= "0091";
	public static final String TIPT_REPORTE_LOGISTICA= "0119";
	public static final String TIPT_TIPO_ESTADO_LOG= "0103";
	public static final String TIPT_TIPO_CONSTE_ESTADO_LOG = "0101";
	public static final String TIPT_ESTD_REQUERIMIENTO_ALMACEN = "0107";
	public static final String TIPT_ESTADO_REQUERIMIENTO = "0095";
	public static final String TIPT_PARAMETROS_GENERALES = "0097";
	public static final String TIPT_TIPO_INTERFAZ = "0123";
	public static final String TIPT_ESTADO_INTERFAZ = "0097";
	public static final String TIPT_ESTADO_INTERFAZ_HIJO = "0008";
	public static final String TIPT_ESTADO = "0125";
	
	//CONSTANTES PARA PRODUCTO
	public static final String TIPT_PRODUCTO_PFR = "100";
	public static final String TIPT_PRODUCTO_PCC = "3003";//"3003";
	public static final String TIPT_PRODUCTO_PCC_I = "3003";//"3003";
	public static final String TIPT_PRODUCTO_PCC_CC = "3014";//"3014";
	public static final String TIPT_PRODUCTO_CAT = "4839";
	public static final String TIPT_PRODUCTO_PAT = "4";
	public static final String TIPT_PRODUCTO_PIA = "5";
	public static final String TIPT_PRODUCTO_PE = "3011";
	public static final String TIPT_PRODUCTO_HIBRIDO = "7";
	public static final String TIPT_PRODUCTO_TECSUP_VIRTUAL = "3002";
	
	//CONSTANTE PARA EL IGV
	public static final String TTDE_IGV_ID = "0002";
	
	//CONSTANTES PATA TIPO HIBRIDO Y VIRTUAL
	public static final String CONSTE_PRODUCTO_HIBRIDO = "H";
	public static final String CONSTE_PRODUCTO_TECSUP_VIRTUAL = "V";
	
	//CONSTANTES PARA EL PRODUCTO PCC EN LAS ETAPAS: PROGRAMA_INTEGRAL Y CURSOS_CORTOS
	public static final String TIPT_ETAPA_PROGRAMA_INTEGRAL= "1";
	public static final String TIPT_ETAPA_CURSOS_CORTOS = "2";
	
	public static final String TIPT_PERIODO_ACTUAL = "2008";
	public static final Integer FECHA_BASE_MINIMO=2000; //BUZON DE SUGERENCIA
	public static final Integer FECHA_BASE_MINIMO_BUZON_BIBLIOTECA=1980; //PORTAL WEB BIBLIOTECA - BUSQUEDA AVANZADA
	public static final Integer HORA_CONSULTA_RESERVA_SALA_MINIMO_LIM=8;
	public static final Integer HORA_CONSULTA_RESERVA_SALA_MAXIMO_LIM=21;

	public static final Integer HORA_CONSULTA_RESERVA_SALA_MINIMO_AQP=10;
	public static final Integer HORA_CONSULTA_RESERVA_SALA_MAXIMO_AQP=20;
	
	
	//CONSTANTES PARA SECCIONES BIBLIOTECA PARA EL RADIO
	public static final String COD_SECCION_NOVEDADES = "0001";
	public static final String TIPT_ETAPA_VIGENTE= "1";
	public static final String TIPT_ETAPA_TODOS = "0";
	
	public static final String COD_SEC_NOV = "0001";
	public static final String COD_SEC_ENL_INT = "0002";
	public static final String COD_SEC_BIB = "0003";
	public static final String COD_SEC_FUE_INF = "0004";
	public static final String COD_SEC_REG = "0005";
	public static final String COD_SEC_HOR = "0006";	
	
	//CONSTANTES PARA REPORTES BIBLIOTECA
	public static final String REPORTE_1 = "0001"; //Consulta de Usuarios Deudores y Sancionados
	
	public static final String REPORTE_2 = "0002"; //Reporte de Material Bibliogr�fico
	
	public static final String REPORTE_2A = "0001"; //Catalogo fichas
	public static final String REPORTE_2B = "0002"; //Material Bibliogr�fico Faltante
	public static final String REPORTE_2C = "0003"; //Material Bibliogr�fico Adquiridos
	public static final String REPORTE_2D = "0004"; //Material Bibliogr�fico Retirados
	public static final String REPORTE_2E = "0005"; //Material Bibliogr�fico M�s Solicitados
	public static final String REPORTE_2G = "0006"; //Material Bibliogr�fico Etiquetas
	public static final String REPORTE_2F = "0005"; //Reporte Usuario Individuales
	
	public static final String REPORTE_3 = "0003"; //Reporte Estadistico por Ciclo y Especialidad
	
	public static final String REPORTE_3A = "0001"; //Por Tipo de Usuario
	public static final String REPORTE_3B = "0002"; //Por Ciclo y Especialidad
	
	public static final String REPORTE_4 = "0004"; //Reporte Estadistico de Movimientos del D�a
	
	public static final String REPORTE_4A = "0001"; //Por Movimiento de Pr�stamos y Devoluciones por D�a y Hora
	public static final String REPORTE_4B = "0002"; //Resumen de Movimientos por D�a y Hora
	
	public static final String REPORTE_5 = "0005"; //Reporte Estadistico del Buz�n de Sugerencias
	
	public static final String REPORTE_6 = "0006"; //Reporte de asistencia (Evaluaciones)
	public static final String REPORTE_BIB_6 = "0006"; //Reporte de usuarios individuales (Biblioteca)
	public static final String REPORTE_BIB_7 = "0007"; //Reporte de reservas por ciclo y especialidad.
	public static final String REPORTE_BIB_8 = "0008"; //Reporte de estadistico de movimientos.
	public static final String REPORTE_BIB_8A = "0001"; //Por Movimiento de Reservas y Devoluciones por D�a y Hora
	public static final String REPORTE_BIB_8B = "0002"; //Resumen de Movimientos de reservas por D�a y Hora
	
	public static final String REPORTE_7 = "0007";

	//JHPR: 2008-04-24 Reporte de Ex�menes (Parcial y Final)
	public static final String REPORTE_7A = "7A";
	public static final String REPORTE_EX_CARGO = "REPORTE_EX_CARGO";
	public static final String REPORTE_EX_SUBSANACION = "REPORTE_EX_SUBSANACION";
	public static final String REPORTE_EX_RECUPERACION = "REPORTE_EX_RECUPERACION";
	public static final String REPORTE_EVA_PERFILESALM = "REPORTE_EVA_PERFILESALM";
	//JHPR: 2008-04-28 Reporte para Notas Expeciales;
	public static final String REPORTE_7B = "7B";

	
	public static final String REPORTE_8 = "0008";
	public static final String REPORTE_9 = "0009";
	public static final String REPORTE_10 = "0010";
	
	//JHPR: 2008-06-09 Reporte de Cierre de Notas (Evaluaciones)
	public static final String REPORTE_CIERRE_NOTAS = "CIERRENOTAS";
	
	public static final String REPORTE_HIST_ALUMNO = "REP_HIST_ALUMNO";
	
	//CONSTANTES PARTA EVALUACIONES.....ESTADOS
	public static final String TIPT_FLAG_APTO= "0001";
	public static final String TIPT_FLAG_NO_APTO = "0002";
	public static final int TIPT_fLAG = 1;
	//public static final String TIPT_COD_TIPO_SESION= "2311";
	
	public static final String TIPT_BIBLIO_PORTAL_NOVEDADES= "0001";
	public static final String TIPT_BIBLIO_PORTAL_ENLACES_INTERES= "0002";
	public static final String TIPT_BIBLIO_PORTAL_BIBLIOTECAS= "0003";
	public static final String TIPT_BIBLIO_PORTAL_OTRAS_FUENTES_INFORMACION= "0004";
	public static final String TIPT_BIBLIO_PORTAL_REGLAMENTOS= "0005";
	public static final String TIPT_BIBLIO_PORTAL_HORARIO_ATENCION= "0006";

	//CONSTANTES PARA EL PORTAL BUSQUEDA SIMPLE.......
	//ESTADOS DE LOS CHECKBOX
	public static final String TIPT_FLAG_SALA= "1";
	public static final String TIPT_FLAG_DOMICILIO= "1";
	public static final String TIPT_COD_CONSULTA = "0001";
	public static final String TIPT_COD_COMENTARIO = "0002";
	public static final String TIPT_COD_SUGERENCIA = "0003";
	public static final String TIPT_COD_RECLAMO = "0004";
	
	//CONTANTES DE LOS TIPOS DE SUGERENCIAS
	public static final String COD_CON= "0001";
	public static final String COD_COM= "0002";
	public static final String COD_SUG= "0003";
	public static final String COD_REC= "0004";	
	public static final String FLAG_DISPONIBLE= "1";
	
	public static final String COD_SOL_TIPO_REQ_BIEN= "0001";
	public static final String COD_SOL_TIPO_REQ_SERVICIO= "0002";
	
	//CONSTANTE GUARDAR ARCHIVO SREQ BANDEJA EVALUADOR
	public static final String SOL_REQ_BANDEJA_EVALUADOR= "SRDR";
	//CONSTANTE PARA ACTUALIZACION DE MANTENIMIENTO DE AUTOR
	public static final String COD_PERSONA = "0001";
	public static final String COD_EMPRESA = "0002";
	
	//CONSTANTES LOGISTICA
	//Aprobaci�n; 2-> Envio a Resp. CECO.
	public static final String SOL_REQ_USUARIO_APROBAR = "1";
	public static final String SOL_REQ_USUARIO_ENVIAR_APROBAR = "2";
	
	public static final String COD_SOL_TIPO_BIEN_ACTIVO= "0001";
	public static final String COD_SOL_TIPO_BIEN_CONSUMIBLE= "0002";
	
	public static final String SOL_AREQ_GRUPOS_GENERALES= "0001";
	public static final String SOL_AREQ_GRUPOS_SERVICOS= "0003";
	public static final String SOL_AREQ_GRUPOS_MANTENIMIENTO= "0002";
	
	//CONSTANTE GUARDAR ARCHIVO SREQ BANDEJA EVALUADOR
	public static final String SOL_COTIZACION_ADJUNTO= "SCAD";
	
	public static final String GEST_DOC_ESTADO_APROBADO= "0003";
	public static final String COD_DOC_PAG_ASOCIADOS_PENDIENTE= "0001";
	public static final String COD_DOC_PAG_ESTADO_ENVIADO= "0007";
	
	public static final String COD_ESTADO_PENDIENTE= "0001";
	public static final String COD_ESTADO_RECHAZO= "0004";
	public static final String COD_ESTADO_APROBACION= "0002";
	public static final String COD_ESTADO_CERRADO= "0007";
	
	//ESTADOS REQUERIMIENTOS
	public static final String COD_REQ_ESTADO_PENDIENTE= "0001";
	public static final String COD_REQ_ESTADO_EN_APROBACION= "0002";
	public static final String COD_REQ_ESTADO_APROBADO= "0003";
	public static final String COD_REQ_ESTADO_RECHAZADO= "0004";
	public static final String COD_REQ_ESTADO_EN_ATENCION= "0005";
	public static final String COD_REQ_ESTADO_ATENCION_PARCIAL= "0006";
	public static final String COD_REQ_ESTADO_CERRADA= "0007";
	public static final String COD_REQ_ESTADO_EN_PROCESO= "0008";
	public static final String COD_REQ_ESTADO_EN_ATENDIDA= "0009";
	
	
	public static final String CONSTE_COD_PERFIL_PROVEEDOR= "LPROV";
	public static final String COTIZACION_ESTADO_CERRADA = "0003";
    public static final String COD_REVISTAS = "0001";
    public static final String COD_CAJA_CHICA = "0001";
    public static final String COD_ORDEN_COMPRA = "0002";
    public static final Integer FECHA_BASE_MAXIMO=2013; //BUZON DE SUGERENCIA
    public static final String COD_TIPO_MOVIMIENTO_GENERADA = "0001";
    //ALQD,27/02/09.AGREGANDO NUEVA CONSTANTE
    public static final String COD_JEFE_MANTTO = "LJMAN";
    public static final String COD_JEFE_LOG = "LJLOG";
    public static final String COD_ESTADO_GENERADO = "0001";

    
    //CONSTANTES CONFIGURACION GENERACION 
    public static final String COD_TIPO_APLICACION_PROGRAMAS = "0001";
    public static final String COD_TIPO_APLICACION_SERVICIO_OTROS = "0002";
    
	//JHPR 31/3/2008 Para visualizar los alumnos con examenes de cargo.
    //CCORDOVA 03/05/2008 Se cambio texto por indicador num�rico
	public static final String TIPO_EXAMEN_REGULAR_PARCIAL = "0";//"EX_REGULAR";
	public static final String TIPO_EXAMEN_REGULAR_FINAL = "4";
	public static final String TIPO_EXAMEN_CARGO = "1";//"EX_CARGO";
	//JHPR 03/05/2008 Para visualizar los alumnos con examenes EXTRAORDINARIOS.
	public static final String TIPO_EXAMEN_EXTRA = "2";//"EX_EXTRA";
	public static final String TIPO_EXAMEN_SUBSANACION = "3"; //JHPR 2008-7-10
	public static final String TIPO_EXAMEN_RECUPERACION = "5";	
	
	public static final String EXAMEN_RECUPERACION= "2331";//"2331";
	public static final String EXAMEN_SUBSANACION  = "2330";//"2330";
	public static final String EXAMEN_PARCIAL= "2328";//"2328";
	public static final String EXAMEN_FINAL= "2329";//"2329";
	
	public static final String ALIAS_EXAMEN_1= "E1";
	public static final String ALIAS_EXAMEN_2 = "E2";
	public static final String ALIAS_EXAMEN_RECUPERACION= "ER";
	public static final String ALIAS_EXAMEN_SUBSANACION= "ES";
	public static final String ALIAS_EXAMEN_CARGO= "EC";
	public static final String ALIAS_EXAMEN_EXTRAORDINARIO= "EX";

	
	public static final String CONSTE_ESTADO_ALMACEN= "1";//constante para el estado de almacen 1:abierta, 0:cerrada;
	//CONSTANTES PARA REPORTES DE LOGISTICA
	public static final String REPORTE_LOG_1 = "0001"; //Reporte de Ingresos a Almac�n
	public static final String REPORTE_LOG_2 = "0002"; //Reporte de salidas de almac�n
	public static final String REPORTE_LOG_3 = "0003"; //Reporte de Kardex Valorizado
	public static final String REPORTE_LOG_4 = "0004"; //Reporte de Maestro de Saldos
	public static final String REPORTE_LOG_5 = "0005"; //Reporte de Cierre de Operaciones
	public static final String REPORTE_LOG_6 = "0006"; //Reporte Acumulado de Salidas por Art�culo
	public static final String REPORTE_LOG_011 = "0011"; //Reporte Ordenes con cargo a presupuestos de inversi�n	
	public static final String REPORTE_LOG_7 = "0007"; //Reporte de requerimientos valorizado
	public static final String REPORTE_LOG_8 = "0008"; //Reporte de Ordenes Asignadas por Proveedo
	public static final String REPORTE_LOG_9 = "0009"; //Reporte de Solicitudes de Cotizaci�n
	public static final String REPORTE_LOG_10 = "0010"; //Reporte de Programa (Cronograma) de Pagos
	public static final String REPORTE_LOG_11 = "0011"; //Reporte de Catalogo de Productos
	public static final String REPORTE_LOG_12 = "0012"; //Reporte de bienes
	public static final String REPORTE_ENC_1 = "0001"; //Avance de encuestas por profesor consilidado
	public static final String REPORTE_ENC_2 = "0002"; //Avance de encuestas por profesor consilidado
    public static final String REPORTE_ENC_3 = "0003"; //Avance de encuestas por profesor consilidado
    public static final String REPORTE_ENC_4 = "0004"; //Avance de encuestas por profesor consilidado
    public static final String REPORTE_ENC_5 = "0005"; //Avance de encuestas por profesor consilidado
    public static final String REPORTE_ENC_6 = "0006"; //Avance de encuestas por profesor consilidado
	public static final String REPORTE_LOG_13 = "0013";
	public static final String REPORTE_LOG_14 = "0014";
	public static final String REPORTE_LOG_15 = "0015";
	public static final String REPORTE_LOG_16 = "0016";
	public static final String REPORTE_LOG_17 = "0017";
	public static final String REPORTE_LOG_18 = "0018";
	public static final String REPORTE_LOG_19 = "0019";
	public static final String REPORTE_LOG_20 = "0020";
	public static final String REPORTE_LOG_21 = "0021";
	public static final String REPORTE_LOG_22 = "0022";//reporte de interfaz contable
	public static final String REPORTE_LOG_23 = "0023";//reporte de interfaz contable
	public static final String REPORTE_LOG_24 = "0024"; //JHPR 2008-09-29 reporte de documento de pago
	public static final String REPORTE_LOG_25 = "0025"; //ALQD,09/01/09. Reporte de OCs pendiente de entrega 
	public static final String REPORTE_LOG_26 = "0026"; //ALQD,09/01/09. Reporte de Reqs pendiente de entrega
	public static final String REPORTE_LOG_29 = "0029"; //nuevo reporte
	
	/*
	 * CONSTANTES PARA ENCUESTAS
	 */
	public static final String COD_REP_BIEN_SERVICIO = "0001";
	public static final String COD_REP_DEVOLUCIONES = "0002";
	public static final String COD_REP_VALIDADO = "0";
	public static final String COD_REP_VENCIDO = "1";
	
	//ESTADO DE LA ENCUESTA
	public static final String COD_ENCUESTA_PENDIENTE = "0001";
	public static final String COD_ENCUESTA_GENERADA = "0002";
	public static final String COD_ENCUESTA_EN_APLICACION = "0003";
	public static final String COD_ENCUESTA_APLICADA = "0004";
	
	public static final String TIPT_TIPO_BIBLIOTECA_EMAIL = "0120";
	public static final String TIPT_TIPO_LOGISTICA_EMAIL = "0121";
	public static final String TIPT_TIPO_RECLUTAMIENTO_EMAIL = "0130";
	//COD TIPO APLICACION DE ENCUESTAS
	public static final String ENCUESTA_PROGRAMA = "0001"; 
	public static final String ENCUESTA_SECCION = "0002";	
	//COD TIPO DE LAS ENCUESTAS
	public static final String TIPO_ENCUESTA_ESTANDAR = "0001"; 
	public static final String TIPO_ENCUESTA_MIXTA = "0002";	
	//COD DE LOS PROGRAMA
	public static final String COD_PRODUCTO_PFR = "100";
	//public static final String COD_PRODUCTO_PFR = "83";
	
	//COD TIPO DE PERFIL DE LA ENCUESTA
	public static final String PERFIL_ANONIMO_PCC = "0000";
	public static final String PERFIL_PFR = "0001"; 
	public static final String PERFIL_PCC = "0002"; 
	public static final String PERFIL_EGRESADO = "0003"; 
	public static final String PERFIL_PERSONAL = "0004";
	public static final String ORIENTADO = "0005";
	
	//COD TIPO PERSONAL ENCUESTA
	public static final String COD_TIPO_DOCENTE = "0003";
	
	public static final Integer HORA_APLICAR_PROGRAMACION_MINIMO=9;
	public static final Integer HORA_APLICAR_PROGRAMACION_MAXIMO=21;
	
	public static final Integer HORA_INICIAL_DIA=0;
	public static final Integer HORA_FINAL_DIA=23;
	
	public static final String MONEDA_SOLES = "0001";
	public static final String SEDE_LIMA = "L";
	
	//PERFILES DE USUARIO DE ENCUESTAS
	public static final String PERFIL_ADM_SISTEMA = "0";
	public static final String PERFIL_ADM_ENCUESTA = "1";
	public static final String PERFIL_PROFESOR = "2";
	public static final String PERFIL_ALUMNO = "3";

	public static final String TIPO_REQ_BIEN = "0001";
	public static final String TIPO_REQ_SERVICIO = "0002";
	//JHPR 2008-06-04 Estados para el cierre de notas
	public static final String ESTADO_REGISTRO_NOTAS_ABIERTO="0";
	public static final String ESTADO_REGISTRO_NOTAS_CERRADO="1";
	public static final int NRO_MAXIMO_PASANTIAS = 3; //JHPR 2008-06-24
	public static final String COD_DETALLE_PARAM_GEN_ENC = "0001";
	public static final String REC_CODIGO_APTO = "0006";
}

