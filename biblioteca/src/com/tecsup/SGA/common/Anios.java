package com.tecsup.SGA.common;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tecsup.SGA.exception.FormatoInvalidoException;
import com.tecsup.SGA.modelo.Meses;


/************************************************************
 * @author       CosapiSoft S.A.
 * @date         07-sep-07
 * @description  none
 ************************************************************
 */
public class Anios {
	private static Log log = LogFactory.getLog(Anios.class);
public static List getAnios(int dimension,String anioMedio){
	List lista = new ArrayList();
	Item item =null;
	try{	
	
	Integer anio = new Integer(anioMedio.trim());
	int ini = anio.intValue() - dimension;
	
	for(int i=ini;i<ini+2*dimension;i++){
		item = new Item();
		item.setName(new Integer(i).toString());
		item.setId(new Integer(i).toString());
		lista.add(item);
	}	
	return lista;
}catch(Exception e){
	for(int i=1970;i<2008;i++){
		item = new Item();
		item.setName(new Integer(i).toString());
		item.setId(new Integer(i).toString());
		lista.add(item);
	}	
	return lista;
}
}


public static List getAniosTodos(int min){
	List lista = new ArrayList();
	Item item =null;
	try {
		String fecha = Fecha.getCurrentDateBD();
		String fe = fecha.substring(0,4);
		int y = Integer.parseInt(fe);
		y = y + 1;
		for(int i=y;i>=min;i--){
			item = new Item();
			item.setName(new Integer(i).toString());
			item.setId(new Integer(i).toString());
			lista.add(item);
		}
		return lista;
		
	}catch(Exception e){
		e.printStackTrace();
		log.error("Error inicializando a�os");
		return null;
	}
}

public static List getAniosTodosFromToDay(int max){
	List lista = new ArrayList();
	Item item =null;
	try {
		String fecha = Fecha.getCurrentDateBD();
		String fe = fecha.substring(0,4);
		
		for(int i=Integer.parseInt(fe)-1;i<Integer.parseInt(fe)+7;i++){
			item = new Item();
			item.setName(new Integer(i).toString());
			item.setId(new Integer(i).toString());
			lista.add(item);
		}
		return lista;
		
	}catch(Exception e){
		e.printStackTrace();
		log.error("Error inicializando a�os");
		return null;
	}
}

public static List getMeses(){
	
	Item item1 =null;
	List listaMonth= new ArrayList();
	for(int tk=0;tk<12;tk++){
		item1 = new Item();
		if(tk==0)
		{item1.setName("ENERO");
		item1.setId("1");}
		if(tk==1)
		{item1.setName("FEBRERO");
		item1.setId("2");}
		if(tk==2)
		{item1.setName("MARZO");
		item1.setId("3");}
		if(tk==3)
		{item1.setName("ABRIL");
		item1.setId("4");}
		if(tk==4)
		{item1.setName("MAYO");
		item1.setId("5");}
		if(tk==5)
		{item1.setName("JUNIO");
		item1.setId("6");}
		if(tk==6)
		{item1.setName("JULIO");
		item1.setId("7");}
		if(tk==7)
		{item1.setName("AGOSTO");
		item1.setId("8");}
		if(tk==8)
		{item1.setName("SETIEMBRE");
		item1.setId("9");}
		if(tk==9)
		{item1.setName("OCTUBRE");
		item1.setId("10");}
		if(tk==10)
		{item1.setName("NOVIEMBRE");
		item1.setId("11");}
		if(tk==11)
		{item1.setName("DICIEMBRE");
		item1.setId("12");}
		listaMonth.add(item1);
		
	}
    
    Item mad1= new Item();
   /* for(int i=0;i<listaMonth.size();i++)
    { mad1=(Item)listaMonth.get(i);
      System.out.println("LISTA ID MES: "+mad1.getId()+"DESCRIPCION MES: "+mad1.getName());
    }*/
	return listaMonth;
}
//se creo otro metodo getMeses02 pero con el id en formato 01
//RNapa 02/05/2008
public static List getMeses02(){
	
	Item item1 =null;
	List listaMonth= new ArrayList();
	for(int tk=0;tk<12;tk++){
		item1 = new Item();
		if(tk==0)
		{item1.setName("ENERO");
		item1.setId("01");}
		if(tk==1)
		{item1.setName("FEBRERO");
		item1.setId("02");}
		if(tk==2)
		{item1.setName("MARZO");
		item1.setId("03");}
		if(tk==3)
		{item1.setName("ABRIL");
		item1.setId("04");}
		if(tk==4)
		{item1.setName("MAYO");
		item1.setId("05");}
		if(tk==5)
		{item1.setName("JUNIO");
		item1.setId("06");}
		if(tk==6)
		{item1.setName("JULIO");
		item1.setId("07");}
		if(tk==7)
		{item1.setName("AGOSTO");
		item1.setId("08");}
		if(tk==8)
		{item1.setName("SETIEMBRE");
		item1.setId("09");}
		if(tk==9)
		{item1.setName("OCTUBRE");
		item1.setId("10");}
		if(tk==10)
		{item1.setName("NOVIEMBRE");
		item1.setId("11");}
		if(tk==11)
		{item1.setName("DICIEMBRE");
		item1.setId("12");}
		listaMonth.add(item1);
	}
    
    Item mad1= new Item();
   /* for(int i=0;i<listaMonth.size();i++)
    { mad1=(Item)listaMonth.get(i);
      System.out.println("LISTA ID MES: "+mad1.getId()+"DESCRIPCION MES: "+mad1.getName());
    }*/
	return listaMonth;
}

	public static List getHoras( int horaInicio, int horaFin){
	 List lista=new ArrayList();
	 Item item =null;
	 for(int i=horaInicio; i<=horaFin; i++ )
	 {   item = new Item();
		 if(i<10)
		 { item.setId("0"+new Integer(i).toString());
		   item.setName("0"+new Integer(i).toString()+":00");
		 }
		 else
		 { item.setId(new Integer(i).toString());
		   item.setName(new Integer(i).toString()+":00");			 
		 }	 
		 lista.add(item);
	 }
	return lista;
	}

}