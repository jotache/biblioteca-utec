package com.tecsup.SGA.common;

public class Ciclos {

	public static String getCicloLatinToRomano(String numStringLatino){
		
		int num = 0;
		
		try {num = Integer.parseInt(numStringLatino);
		}catch(Exception e){num=-1;	}
		
		String val = "-";
	switch(num)
	{
		case 1: val = "I";	break;
		case 2: val = "II";	break;
		case 3: val = "III";break;
		case 4: val = "IV";	break;
		case 5: val = "V";	break;
		case 6: val = "VI";	break;
		case 7: val = "VII";break;
		case 8: val = "VIII";break;
		case 9: val = "IX";	break;
		case 10: val = "X";	break;
	}
	
	return val;
	
	}
}
