	//Seguridad
	var mstrDebeIngClave			= "Debe ingresar la clave de su cuenta.";
	var mstrUsuarioNoReg			= "Usuario no est� registrado.";
	var mstrUsuarioNoActivo			= "Usuario no est� activo.";
	var mstrUsuarioPerBloqueo		= "Usuario est� dentro del periodo de bloqueo.";
	var mstrUsuarioMaxIntFall		= "Usuario alcanz� el n�mero m�ximo de intentos fallidos permitidos.";
	var mstrClaveIncorrecta			= "Clave no verifica la Firma registrada.";
	var mstrClaveSinVigencia		= "Clave agot� su tiempo de vigencia permitido o es igual al usuario.";
	var mstrErrorLogueo				= "Error al verificar usuario.";
	var mstrUsuarioNoAccess			= "Lo sentimos pero no tiene opciones disponibles en este sistema";  //JHPR 2008-08-25
	var mstrPorSegCambiaClave		= "Por seguridad cambia tu clave para que puedas ingresar";
	var mstrErrClaCaracMinPer		= "Tu Clave Nueva tiene menos caracteres que el m�nimo permitido. Intenta otra clave.";
	var mstrErrClaNumerMinPer		= "Tu Clave Nueva tiene menos n�meros que el m�nimo permitido. Intenta otra clave.";
	var mstrErrClaNuevaYaExis		= "Tu Clave Nueva no puede ser igual que las anteriormente ingresadas. Intenta otra clave";
	var mstrErrClaDesconocido		= "Hay un problema desconocido en tu cambio de clave. Consulta al Administrador de Sistemas.";
	var mstrErrClaActualNoEs		= "La Clave Actual que ingresaste no es correcta";
	
	var msjEnConstruccion 			= "Funcionalidad en construcci�n";
	var mstrElimino 				= "Se elimin� exitosamente.";
	var mstrNoElimino 				= "Problemas al eliminar. Consulte con el administrador.";
	var mstrNoRegistros 			= "No se encontraron coincidencias";	
	var mstrSeleccione 				= "Debe seleccionar un registro";
	var mstrSeleccioneUno 			= "Debe seleccionar s�lo un registro";
		
	var mstrIngreseNumeroCodigo		= "Debe ingresar el c�digo.";
	var mstrIngreseDescripcion		= "Debe ingresar una descripci�n.";
	var mstrIngreseIngreseValorBuscar 	= "Debe ingresar al menos un criterio de b�squeda";
	var mstrSeleccioneSituacion  	= "Debe seleccionar la situaci�n del registro.";
	var mstrSeleccioneArchivo		= "Debe seleccionar la imagen relacionada.";
	var mstrNombreErroneo			= "El nombre del archivo contiene caracteres no v�lidos.";

	var mstrSeguroGrabar 			= "�Est� seguro de grabar?";
	var mstrSeguroGrabar2 			= "�Est� seguro de validar?";
	var mstrSeguroGrabar3 			= "�Est� seguro de rechazar?";
	var mstrSeguroRealizarAccion    = "�Est� seguro de realizar esta acci�n?";
	var mstrSeguroEliminar 			= "�Est� seguro de eliminar los registros seleccionados?";
	var mstrSeguroEliminar1			= "�Est� seguro de eliminar?";
    var mstrRealiceBusqueda 		= "Debe realizar primero la b�squeda.";
    var mstrActualizo 				= "Se actualiz� exitosamente";
	var mstrSeGraboConExito			= "Se grab� exitosamente";
	var mstrProblemaGrabar 			= "Problemas al Grabar. Consulte con el administrador.";
	
	var mstrSeGraboConExito2		= "Se Rechaz� exitosamente";
	var mstrProblemaGrabar2 		= "Problemas al Rechazar. Consulte con el administrador.";
	var mstrSeGraboConExito3		= "Se Valido exitosamente";
	var mstrProblemaGrabar3 		= "Problemas al Validar. Consulte con el administrador.";
	

	var mstrErrorFormatoHora 		= "El formato del campo es HH:MM";
	var mstrNohayRegistros 			= "No se encontraron coincidencias";
	var mstrExistenRegistros		= "El dato ingresado ya se encuentra registrado.";
	var mstrErrorFechaConsulta	 	= "La fecha de consulta no puede ser mayor \nque la fecha actual";
	
	var mstrFechasObligatoria		= "Debe ingresar fecha de inicio y fin de registro.";
	var mstrErrorExpectativaSal		= "La expectativa salarial inicial debe ser menor que la final.";
	var mstrDebeSelUnidadFuncional	= "Debe seleccionar una unidad funcional.";
	var mstrDebeIngDscProceso		= "Debe ingresar la descripci�n del proceso.";
	var mstrDebeSelAreaInteres		= "Debe seleccionar un �rea de interes.";
	var mstrDebeSelAreaEstudio		= "Debe seleccionar un �rea de estudio.";
	var mstrDebeSelInstEdu			= "Debe seleccionar un Instituci�n Educativa.";
	var mstrDebeSelAreaNivelEstudio = "Debe seleccionar un nivel de estudio.";
	var mstrAreaIntRepetida			= "El �rea de interes seleccionada ya se encuentra en lista.";
	var mstrAreaEstRepetida			= "El �rea de estudio seleccionada ya se encuentra en lista.";
	var mstrAreaNivEstRepetida		= "El nivel de estudio seleccionada ya se encuentra en lista.";
	var mstrInstEduRepetida			= "La instituci�n seleccionada ya se encuentra en lista.";
	var mstrSelProcesoRev			= "Debe seleccionar un proceso de revisi�n."
	var mstrProcEnProceso			= "Uno o m�s postulantes est�n en proceso de revisi�n. El proceso no puede ser eliminado.";
		
	/*Mensajes procesos*/
	var mstrSelCalificaciones		= "Debe seleccionar una calificaci�n por cada tipo de evaluaci�n.";
	var mstrIngComentarioCalif		= "Debe ingresar un comentario para la calificaci�n.";
	
	/*Mensajes de oferta*/
	var mstrIngDscOferta			= "Debe ingresar la descripci�n de la oferta.";
	var mstrIngNroVacantes			= "Debe ingresar el n�mero de vacantes.";
	var mstrIngTextoPublicar		= "Debe ingresar el texto a publicar.";
	var mstrIngFecIni				= "Debe ingresar la fecha inicio de publicaci�n.";
	var mstrIngFecFin				= "Debe ingresar la fecha fin de publicaci�n."
	
	/*Mensajes de evaluadores por proceso*/
	var mstrConfigurarEvaluadores	= "No existen evaluaciones configuradas para este tipo de proceso.";
	var mstrSelEvaluadoresByEvaluacion	= "Debe seleccionar un evaluador por cada evaluaci�n.";
	var mstrSelPostSinRevisionNoEnviado = "Los postulantes que se deseen revisar deben estar en estado 'SIN REVISI�N' � 'REVISADO NO ENVIADO'";
	var mstrSelPostSinRevision			= "Los postulantes que se deseen enviar a Jefe de Dpto. deben estar en estado REVISADOS NO ENVIADO.";	
	var mstrSelPostSinEvaluar			= "Los postulantes que se deseen enviar a evaluar deben estar en estado SIN EVALUAR.";
	var mstrSegEnviarEvaluar			= "Seguro de enviar el(los) registro(s) seleccionado(s) a evaluar.";
	
	/*Mensajes de postulantes por proceso*/
	var mstrSelPostulante			= "Debe seleccionar al menos un postulante.";
	var mstrSelCheked				= "Debe seleccionar al menos un registro consumible.";
	var mstrSelCheked2				= "Debe seleccionar al menos un registro activo.";
	var mstrSelCheked3				= "Debe seleccionar al menos un registro general.";
	var mstrSelCheked4				= "Debe seleccionar al menos un registro otros.";
	var mstrSelCheked5				= "Debe seleccionar al menos un registro mantenimiento sin envio.";
	var mstrSelCheked6				= "Debe seleccionar al menos un registro mantenimiento con envio.";	
	var mstrSelUnoPostulante		= "Debe seleccionar s�lo un postulante.";
	var mstrSelCalificacion			= "Debe seleccionar una calificaci�n."
	var mstrSegGrabarFinal			= "�Est� seguro de grabar? Luego de grabar no se podr� modificar."
	
	/*Mensajes de Evaluacion Revision*/
	var mstrSelPostConEvaluacion	= "Todos los postulantes seleccionados deben haber sido evaluados.";
	var mstrSeguroEnviarRRHH		= "�Est� seguro de enviar los postulantes seleccionados a recursos humanos?"
	
	/*Mensajes incidencias y competencias*/
	var mstrSelCalCompetencia		= "Debe seleccionar al menos la evaluaci�n de una competencia.";
	
	//Reclutamiento
	var mstrGrabarDatos				= "Debe registrar sus datos personales, para acceder a las dem�s secciones.";
	var mstrUsuario					= "Debe ingresar un usuario.";
	var mstrClave					= "Debe ingresar la clave de su cuenta.";
	var mstrClaveNoCoincide			= "La confirmaci�n de la contrase�a no coincide.";
	var mstrOtroIdioma				= "El idioma seleccionado ya ha sido elegido.";
	var mstrExtensionFoto			= "El archivo imagen debe tener las siguientes\nextensiones : JPG o GIF ";
	var mstrExtensionCV				= "El archivo CV debe tener las siguientes\nextensiones : DOC o PDF ";
	var mstrExtensionDoc			= "El archivo debe tener la siguiente : .DOC � .DOCX";
	var mstrExtensionXLS			= "El archivo debe tener las siguientes : .XLS � .XLSX";
	var mstrSeguroEnvio				= "La clave ser� enviada al e-mail ingresado.\�Desea continuar?";
	var mstrSeguroPostular			= "�Est� seguro de postular a la oferta seleccionada?";
	var mstrExtencionInf			= "El Informe debe tener las siguientes\nextensiones : DOC, DOCX , XLS, XLSX o PDF";
	var mstrSelecSexo				= "Seleccione el Sexo";
	var mstrNoGrabaAlgunosDatos	= "Algunos datos no ser�n tomados en cuenta en la grabaci�n \npues los campos del Estudio 2 y/o Estudio 3 no est�n completos.\nDesea continuar?";
	
	//Mensajes para seccion Datos
	var mstrNombre					= "Nombre";
	var mstrApellido				= "Apellido Paterno";
	var mstrFecNac					= "Fecha de Nacimiento";
	var mstrEstado					= "Estado Civil";
	var mstrDni						= "DNI";
	var mstrEmail					= "Email";
	var mstrClave					= "Clave";
	var mstrClaveConfirm			= "Confirmaci�n de Clave"
	var mstrDirec					= "Direcci�n";
	var mstrTelef					= "Tel�fono Domicilio";
	var mstrPais					= "Pa�s";
	var mstrPretension				= "Pretensi�n Econ�mica";
	var mstrDispo					= "Disponibilidad";
	var mstrMoneda					= "Moneda";
	var mstrTipoPago				= "Debe seleccionar la frecuencia de pago.";
	
	//Mensajes para seccion Estudios
	var mstrEstNivelIdioma			= "Nivel del Idioma ";
	var mstrEstGradoIdioma			= "Grado del Idioma ";
	var mstrEstColegio1				= "Colegio 1, A�o Inicio 1 y A�o Fin 1";
	var mstrEstColegio2				= "A�o Inicio 2 y/o A�o Fin 2";
	var mstrEstGradAcad				= "Grado acad�mico del estudio superior ";
	var mstrEstAreaEstudio			= "\n�rea de estudio del estudio superior ";
	var mstrEstInstitucion			= "\nInstituci�n del estudio superior ";
	var mstrEstCiclo				= "Ciclo del estudio superior ";
	var mstrEstMerito				= "M�rito del estudio superior ";
	var mstrEstFecIni				= "Inicio del estudio superior ";
	var mstrEstFecFin				= "Fin del estudio superior ";
	var mstrNoEstSec				= "Existen datos en la secci�n estudios secundarios que no se guardar�n.";
	
	//Mensajes para seccion ExpLaboral
	var mstrExpPuesto				= "Puesto y/o especificarlo si no lo encuentra\nen la lista desplegable.";
	var mstrExpFecIni				= "Fecha inicio de la experiencia laboral ";
	
	//mensajes competencias
	var mstrDenominacion			= "Debe ingresar la denominaci�n.";
	var mstrPorcentaje				= "Debe ingresar el porcentaje.";
	var mstrCero					= "No puede ingresar porcentaje cero.";
	var mstrOtraCompetencia			= "La denominaci�n ya existe para otro registro.";
	var mstrValorMenor				= "Debe ingresar un valor menor, la suma de los % debe ser 100.";	
	var mstrMaxReg					= "No puede ingresar m�s registros.";	
	var mstrConfCalificaciones		= "Debe ingresar el peso de las calificaciones. Consulte con el administrador.";
	var mstrUsado					= "El registro ya se encuentra agregado";
	//mensajes producto hibrido
	var mstrSelExamen				= "Debe seleccionar un tipo de examen.";
	var mstrSelEvaluacion			= "Debe seleccionar un tipo de evaluaci�n.";
	var mstrSelOtro					= "Seleccione otra opci�n.";
	var mstrCodigo					= "Debe ingresar el c�digo.";
	var mstrPesoVacio				= "Debe ingresar el peso.";
	var mstrGrabarSeguro			= "�Est� seguro de grabar?";
	var mstrSelOpcion				= "Debe seleccionar una opci�n.";
	var mstrOtroCodigo				= "El valor del c�digo ya existe para otro registro.";
	var mstrSelExaEva				= "Debe ingresar un tipo de examen � evaluaci�n.";
	
	//carga Academica
	var mstrAsistencia				= "No puede registrar Asistencia.\nConsulte al Administrador.";
	var mstrIncidencia				= "No puede registrar Incidencias.\nConsulte al Administrador.";
	
	//mensajes nota externa
	var mstrOperador				= "Debe seleccionar un tipo en todos los operadores.";
	var mstrDivCero					= "El valor relacionado con el signo \"/\" no puede ser 0.";
	
	//General
	var mstrValidaCampo				= "Debe ingresar el valor correspondiente en el campo ";
	var mstrNoValor					= "Debe seleccionar su CV y/o imagen a subir."
	var mstrInforme					= "Debe seleccionar su Informe";
	/*Mensajes de Mantenimiento de Tablas*/
	var mstrIngNivel				= "Debe ingresar la denominaci�n.";
	var mstrSeleccion				= "Debe seleccionar un registro";
	var mstrSeleccionVacio			= "El registro se encuentra vacio.";
	var mstrActualizar				= "Se actualiz� exitosamente.";
	var mstrGrabar					= "Se grab� exitosamente";
	var mstrElimino					= "Se elimin� exitosamente";
	var mstrEliminar				= "�Est� seguro de eliminar?.";
	var mstrPregActu				= "�Est� seguro de editar este registro?.";
	var mstrsel						= "Debe seleccionar un curso.";
	var mstrSelProducto				= "Debe seleccionar un producto.";
	var mstrSelCiclo				= "Debe seleccionar un ciclo.";
	var mstrProbEliminar			= "Problemas al eliminar. Consulte con el administrador.";
	var mstrSeleccioneParametro		= "Debe ingresar un peso";
	var mstrSelEval					= "Debe seleccionar una evaluacion parcial";
	var mstrDoble					= "La demoninaci�n ingresada ya existe";
	var mstrProbActualizar			= "Problemas al actualizar";
	var mstrNoEliminar				= "No se puede eliminar ya que la evaluaci�n se a realizado";
	var mstrPeso					= "El peso debe ser mayor a cero";
	var mstrSeleccioneEva			= "Debe seleccionar un evaluador";
	var mstrPesoMayor				= "El peso debe ser menor a 10";
	var mstrHoras					= "Horas de pr�ctica completas";
	var mstrFechaInicio				= "Debe ingresar una fecha inicial";
	var mstrFechaFin				= "Debe ingresar una fecha de fin";
	var mstrHorasIngreso			= "Debe ingresar la cantidad de horas";
	var mstrEmpresa					= "Debe ingresar una empresa";
	var mstrEst						= "Debe seleccionar un estado";
	var mstrFecha 					= "La fecha de inicio debe ser menor a la fecha final.";
	var mstrS						= "Debe seleccionar s�lo un registro.";
	var mstrE						= "Ya existe un registro no eliminable para este tipo de evaluacion parcial.";
	//Evaluaciones - Asistencia
	var mstrSeguroAsistencia		= "�Est� seguro de grabar la asistencia?";
	var mstrFaltaAsistencia			= "Falta registrar asistencia al(los) alumno(s):\n\n";
	var mstrSaveAsistencia			= "Debe ingresar y/o grabar la fecha para registrar la asistencia";
	var mstrUnaFecha				= "Debe resgistrar la asistencia para la fecha seleccionada";
	var mstrNroAsistencia			= "Nro. Asistencia.";
	var mstrDateAlreadySaved		= "Ya se registr� la asistencia de hoy";
	var mstrNoBuscar				= "No hay registros de asistencia para realizar la b�squeda.";
	var mstrFechaMayor				= "La fecha para registrar asistencia no debe ser mayor a "
	var mstrFechaDbl				= "Ya se encuentra registrada la asistencia para la fecha ";
	var mstrLstVacia				= "No hay registros para grabar la asistencia.";
	var mstrFaltaTipoSesion			= "Seleccione el tipo de Sesi�n.";
	var mstrFaltaSeccion			= "Seleccione la Secci�n.";
	
	/*Mensajes de Parametros Generales*/
	var mstrSeleccioneParametros	= "Se deben llenar todos los parametros obligatorios";
	var mstrPromedio                = "Debe ingresar una valor mayor a 10 y como m�ximo permitido 20";
	var mstrProductoPeriodo         = "Debe seleccionar un producto y un per�odo.";
    var mstrPerfilConcepto          = "Debe seleccionar un perfil.";
    
    var mstrNoSePuedeRealizarLaAccion    = "No se puede realizar la acci�n.";
    var mstrLleneLosCampos     			 = "Debe completar todos los campos.";
    
    var mstrIngreseFecha				 = "Debe ingresar una fecha.";
    var mstrAgregarCiclos				 = "Debe agregar ciclos.";
     
    //nota esterno
    var mstrExtensionArchivo		= "El documento debe tener la \nextensi�n : DOC, DOCX, XLS � XLSX";
    var mstrExtensionArchivoGIF		= "El documento debe tener la \nextensi�n : GIF";
    var mstrExtensionArchivoDOC_o_PDF		= "El documento debe tener la \nextensi�n : DOC � PDF";
    var mstrNoArchivo				= "Debe seleccionar su documento a subir.";
    var mstrParciales			    = "El m�nimo de evaluaciones parciales es: ";
    var mstrSeleccioneParametros	= "Debe seleccionar producto.";
    
        /*Mensajes de Configuracion de Componentes*/
    var mstrSeleccioneComponentes			= "Debe selecionar todas las opciones.";
	var mstrSelEspecialidad         		= "Debe selecionar una especialidad.";
	var mstrSelPrograma             		= "Debe selecionar un programa.";
	var mstrSelCurso                		= "Debe seleccionar un curso.";
    //var mstrSelCiclo                		= "Debe seleccionar un Ciclo";
    var mstrSelModulo               		= "Debe seleccionar un m�dulo.";	
    var mstrSelEspecialidad         		= "Debe seleccionar una especialidad.";
    var mstrSeleccioneSeccion				= "Seleccione secci�n.";
    var mstrSeleccioneEvaluacionParcial		= "Seleccione evaluaci�n parcial."	;
    var mstrSeleccioneEvaluacionParcial		= "Seleccione n�mero\nde evaluaci�n parcial.";
    var mstrSeleccionePeriodo 				= "Seleccione un per�odo";
    var mstrIngreseEscalaCorrecta 			= "Ingrese un orden correcto\npara escala";
    var mstrIngreseNotaValida  				= "Ingrese nota v�lida";
    var mstrNumeroMenor						= "El n�mero de decimales debe ser menor a 5.";
    
     		/*Mensajes de Biblioteca Mantenimiento*/ 
    
    var mstrSelecCategoria                  = "Seleccione una categor�a";
    var mstrArchivoMuyGrande                = "El tama�o del archivo debe ser menor a 5MB";
    var mstrSeleccioneTitulo                = "Seleccione un t�tulo";
    var mstrIngreseNota                		= "Debe ingresar por lo menos una nota";
    var mstrFamilia               			= "Debe seleccionar una familia"; 
    var mstrSede	               			= "Debe seleccionar una sede"; 
    var mstrSedeReq							= "Debe seleccionar un tipo de req.";
    var mstrIngreseCodigoNoCorrecto			= "El c�digo ingresado no es correcto"
    var mstrNoTieneCV						= "El postulante no cuenta con CV.";
    var mstrArchivoNoExiste					= "La direcci�n ingresada no es v�lida.";
    var mstrNoTiene							= "No cuenta con documentos";
    var mstrNoTiene2						= "No cuenta con imagenes";   
        /*Mensajes de logistica-----Mantenimiento
        */
        
    var mstrCalif							= "Debe ingresar la calificaci�n.";   
    var mstrCond							= "Debe ingresar condici�n de compra.";   
    //mensajes para el mantenimiento de biblioteca
    var mstrSelPais							= "Debe seleccionar un Pa�s.";
    var mstrIngreseCiudad					= "Debe ingresar la Ciudad.";
    var mstrIngreseIdioma					= "Debe ingresar el Idioma.";
    var mstrSelSeccion						= "Debe seleccionar una Secci�n.";
    var mstrSelSala			                = "Debe seleccionar una Sala.";
    var mstrIngreseSala						= "Debe ingresar la Sala.";
    var mstrIngreseCapacidad				= "Debe ingresar la Capacidad.";
    var mstrSelGrupo						= "Debe seleccionar un Grupo.";
    var mstrNoMasCinco						= "No puede ingresar m�s de 5 grupos.";
    var mstrIngreseTipoVideo				= "Debe ingresar el Tipo de Video.";
    var mstrIngreseTipoProcedencia			= "Debe ingresar el Tipo de Procedencia.";       
    var mstrSelTipoMaterial					= "Debe seleccionar un Tipo de Material.";
    var mstrExtensionDocumento				= "El archivo documento debe tener las siguientes\nextensiones : DOC, XLS o PDF.";
    var mstrFechaPublicacion				= "Debe ingresar una fecha de Publicaci�n.";
	var mstrFechaVigencia					= "Debe ingresar una fecha de Vigencia.";
	var mstrNoEli						= "No se puede eliminar ya que el registro est� siendo utilizado";
	//********************************************
	         
          /*Mensajes de Reserva de Sala para el Portal*/
   
    var mstrSeleccioneSala                  = "Debe seleccionar una sala.";       
    var mstrFechaReserva                    = "Debe ingresar una fecha.";
    var mstrHoraInicio          	        = "Debe seleccionar una hora de inicio.";
    var mstrHoraFin          	            = "Debe seleccionar una hora de fin.";
    var mstrSeleccioneHora                  = "Debe seleccionar una hora de inicio y de fin correctamente.";
    //var mstrFechaReservaMayor               = "Debe seleccionar una fecha superior a la de hoy."; 
    var mstrFechaReservaMayor               = "La fecha final debe ser mayor o igual a la inicial";
    var mstrFechaReservaMayorIgual          = "Debe seleccionar una fecha superior o igual a la de hoy.";
    var mstrSalaLlena                       = "La sala ya no cuenta con vacantes disponibles."; 
    var mstrErrorUsuario                    = "No se puede eliminar esta reserva, consulte con el administrador.";      

        var mstrSeleccioneMaterial          = "Seleccione un tipo de Material.";
	var mstrIngreseTitulo                   = "Debe ingresar un t�tulo.";
	var mstrIngreseNroPagDuracion           = "Debe ingresar nro p�gina y/o duraci�n.";
    var mstrIngreseFecPublicacion			= "Debe ingresar un a�o de publicaci�n."
    var mstrSeleccioneIdioma                = "Seleccione un Idioma.";
    var mstrIngreseIsbn						= "Debe ingresar ISBN."
    var mstrSeleccioneSede						= "Debe seleccionar una Sede."
	var mstrSeleccioneProcedencia           = "Seleccione procedencia.";
	var mstrSeleccioneMoneda                = "Seleccione moneda.";
	var mstrIngreseCantidad				    = "Debe ingresar cantidad.";
	var mstrSeleccioneAutor				    = "Seleccione autor.";
	var mstrSeleccioneDewey				    = "Seleccione Dewey.";
	var mstrSeleccioneCriterio			    = "Seleccione un criterio de b�squeda.";
	var mstrIngreseFechaIngreso			    = "Debe ingresar la fecha de ingreso.";
	var mstrIngreseNroVolumen			    = "Debe ingresar n�mero de volumen.";
	var mstrSeleccioneEstado				= "Seleccione estado.";
	var mstrSeleccioneTipoPrestamo 			= "Seleccione tipo de pr�stamo.";
	var mstrLibroNoPrestamo					= "El libro seleccionado no est� configurado para pr�stamo.";
	var mstrLibroNoPrestamoCasa				= "El libro seleccionado no est� configurado para pr�stamo a Domicilio.";
	var mstrLibroNoPrestamoSala				= "El libro seleccionado no est� configurado para pr�stamo en Sala.";
	var mstrSeleccioneAplicaReserva 		= "Seleccione aplica reserva.";
	var mstrNoElim							= "S�lo los registros Pendientes pueden eliminarse.";
	var mstrProblemaFecha					= "La fecha de reserva no es v�lida.";/*es menor a la fecha actual*/
	var mstrProblemaHora					= "La hora de reserva no es v�lida."; /*es menor a la hora actual*/
	var mstrProblemaEntreFechas				= "La fecha de reserva es mayor a la fecha actual.";
	var mstrLibroNoDisponible				= "El material seleccionado no tiene ejemplares disponibles.";
	var mstrLibroNoActivo					= "El c�digo de libro seleccionado est� retirado de la biblioteca.";
	var mstrUsuarioNoPermitidoReservas		= "El Usuario no est� permitido para realizar reservas.";
	var mstrFechaPasada						= "La fecha u hora de reserva no son v�lidas.";
	var mstrReservaYaRealizada				= "El usuario ya registra una reserva en la fecha y hora seleccionada.";
	var mstrUsuarioSancionVigente			= "El usuario registra sanciones vigentes.";
	var mstrMaxReservadoxPersona			= "El usuario excedi� el n�mero de reservas (hrs) en el d�a."
	var mstrUsuarioInvalido					= "El usuario ingresado es invalido";
	  /*Mensajes para Atender Buzon de Sugerencias*/
	  
    var mstrSeleccioneCalificacion          = "Seleccione una Calificaci�n.";
    var mstrIngreseRespuesta         		= "Debe ingresar una respuesta.";    
    var mstrSeleccioneOrientacion       	= "Seleccione una Orientaci�n."; 
    var mstrSeleccioneTipoSugerencia       	= "Seleccione un Tipo Sugerencia.";
    var mstrLibroNoPretado 					= "El libro no ha sido prestado.";
    var mstrUsuarioNoPermitido				= "El Usuario no est� permitido para registrar el material.";
    var mstrIngreseNroIngreso 				= "Debe Ingresar un Nro de Ingreso.";
    var mstrMaterialInvalido   				= "El material No es v�lido o ha sido prestado.";
    var mstrIngreseUsuario 					= "Debe ingresar un c�digo de usuario.\no Nro. de ingreso";
    var mstrIngreseUsuarioValido  			= "Debe ingresar usuario valido.";
    var mstrUsuarioNoPermitido 				= "El usuario no esta permitido para registrar pr�stamos.";
    var mstrSeleccioneMes 					= "Seleccione un Mes.";
    var mstrSeleccioneA�o	 				= "Seleccione un A�o.";
    var mstrSeleccioneOrdenarBy	 			= "Seleccione un tipo de Orden.";
    var mstrMaterialReservado				= "El c�digo del libro seleccionado ha sido reservado."
    var mstrIngreseCodigo					= "Ingrese un c�digo para consultar";
    var mstrMaterialReservadoxPersona		= "Ud. ya tiene una reserva de este ejemplar."
    var mstrUsuarioNoPermitidoSala			= "El Usuario no est� permitido para registrar el material en Sala.";
    var mstrUsuarioNoPermitidoCasa			= "El Usuario no est� permitido para registrar el material a Domicilio.";
    var mstrPrestNoCoincideSedeMatUsu		= "El Usuario y libro seleccionado pertenecen a sedes distintas.\nNo se puede realizar el pr�stamo.";
    var mstrResNoCoincideSedeMatUsu			= "El Usuario y libro seleccionado pertenecen a sedes distintas.\nNo se puede realizar la reserva.";
      /*Menu logistica*/
    var mstrDebeSelSede						= "Debe seleccionar una sede.";
    var mstrIngreseSugerencia				= "Debe ingresar una sugerencia.";
    var mstrEnviarDatos						= "�Est� seguro de enviar los datos?";
    var mstrEstadoLogistica				    = "En estos momentos el almacen se encuentra cerrado, por favor volver a intentar en unos momentos.";
    
      /* Mensajes para busqueda Avanzada */
    
    var mstrSeleccioneCondicion1            = "Seleccione la Primera Condici�n.";
    var mstrSeleccioneCondicion2            = "Seleccione la Segunda Condici�n.";    
    var mstrTxtTexto1                    	= "El Texto 1 esta vac�o."; 
    var mstrTxtTexto2       				= "El Texto 2 esta vac�o."; 
    var mstrTxtTexto3       				= "El Texto 3 esta vac�o.";
    var mstrSeleccioneBuscar1               = "Seleccione el Primer Tipo de Ordenamiento.";
    var mstrSeleccioneBuscar2               = "Seleccione el Segundo Tipo de Ordenamiento.";
    var mstrSeleccioneBuscar3               = "Seleccione el Tercer Tipo de Ordenamiento.";
    var mstrVerificarDatos                  = "Verifique los datos para la b�squeda.";
     
    
    /*Mensajes cotizaciones Log�stica*/
    var mstrProvRepetido					= "El proveedor seleccionado ya se encuentra seleccionado.";
    var mstrSelTipoServicio					= "Debe Seleccionar un Tipo de Servicio.";
    var mstrSelTipoEvaluacion				= "Debe seleccionar un Tipo de Evaluaci�n.";
    var mstrConfAprobarRequerimiento    	= "�Est� seguro de aprobar el requerimiento?";
    var mstrSegEnviarAprobacion         	= "�Est� seguro de enviar para la aprobaci�n?";
     
    var mstrCerrarSesion					= "�Est� seguro de cerrar sesi�n?";
    var mstrSeguroModificar1 			    = "�Est� seguro de modificar el registro seleccionado?";
    var mstrSeguroAgregarCotizacion 		= "�Est� seguro de agregar el registro a Cotizaci�n?";
    var mstrSeguroAgregarCotExistente       = "�Est� seguro de agregar el registro a Cotizaci�n Existente?";
    var mstrResponsable						= "Este registro no tiene asignado un responsable.";
    var mstrErrorCopiarDoc					= "Error al copiar los Documentos.";
    var mstrExitoCopiarDoc					= "Exito al copiar los Documentos.";
    var mstrNoExiteDocAdjunto				= "No existe documento adjunto.";
    var mstrSelGrupoServicio				= "Debe Seleccionar un Grupo de Servicio.";
    var mstrNoGenerarOrden				    = "No se puede genera orden. La cotizaci�n debe estar cerrada.";
    var mstrGenerarOrden				    = "�Est� seguro de generar la orden?";  
    var mstrGuardarOrden				    = "�Est� seguro de guardar la orden?";
    var mstrExitoEnvioAlmacen            	= "La devoluci�n se envi� al almac�n correctamente.";
    var mstrErrorEnvioAlmacen           	= "Existen uno o mas detalles para los cuales no se han seleccionado los activos a ser devueltos.";     
    var mstrMotivoDevolucion            	= "Debe ingresar un motivo de devoluci�n.";
    var mstrSeguroEnviarDevolucionAlmacen   = "Est� seguro de enviar la devoluci�n al almac�n.";
    var mstrExitoEnviarSolicitud			= "La solicitud se envi� con �xito.";
     
    /*Reportes*/
    var mstrSelecTipoMat					= "Seleccione el Tipo de Reporte a generar.";
    var mstrIngreseTC						= "Debe ingresar el Tipo de Cambio";
    var mstrIngreseNroReg					= "Debe ingresar el Nro. de Registros a Mostrar.";
    var mstrUnoOmas							= "Uno o mas productos ya existen.";
    /*Mensajes de Solicitud/Aprobacion de requerimientos*/
    var mstrDevolverArticulos				= "Solo se puede devolver articulos para las solicitudes que se encuentren en estado atendido.";
    var mstrNoDevolverPendiente				= "Este registro es Pendiente y no se puede devolver."; 
    var mstrNoDevolverRechazado				= "Este registro es Rechazado y no se puede devolver.";
    var mstrNoDevolverAprobacion			= "Este registro esta en Aprobaci�n y no se puede devolver.";
    var mstrNoDevolverCerrado				= "Este registro esta Cerrado y no se puede devolver.";
    var mstrSeguroDevolverBien				= "�Est� seguro de devolver el bien?";
    var mstrErrorProcesarReq     			= "Error al procesar el requerimiento.";
    var mstrLlenarCamposDetalle 			= "Debe llenar todos los campos por cada detalle.";
    var mstrNoEnviarByNoResponsable			= "La solicitud no puede ser enviada. No hay responsables a asignar.";
    var mstrIngresaRegEnviarCot 			= "Debe ingresar registros para poder enviar la solicitud.";
    var mstrGuardarSolReq       			= "Debe grabar una Solicitud de Requerimiento.";
    var mstrSelCeco             			= "Debe seleccionar un centro de costo.";
    var mstrSelTipoReq             			= "Debe seleccionar un tipo de requerimiento.";
    var mstrConfiGrabarDatos         		= "�Esta seguro de grabar los datos?";
    var mstrIngreNombreServ         		= "Debe ingresar el Nombre del Servicio.";
    var mstrIngreFechaAten          		= "Debe ingresar la Fecha de Atenci�n.";
    var mstrIngreDescripServ         		= "Debe ingresar la Descripcion del Servicio.";
    var mstrFechaEntreMayorAct       		= "Fecha de entrega debe ser mayor o igual a la fecha actual.";
    var mstrFechaAtenMayorAct       		= "Fecha de atenci�n debe ser mayor o igual a la fecha actual.";
    var mstrGuardarDatosCabecera       		= "Se debe registrar los datos de la cabecera \nantes de proceder con esta operaci�n.";
    var mstrConfSeguroContinuar       		= "Usted no ha registrado detalle para el requerimiento, este requerimiento sera eliminado \n�Seguro de continuar?";
    var mstrExisDetallesSinActivos     		= "Existen uno � m�s detalles para los cuales no se han seleccionado los activos a ser devueltos.";
    var mstrNoDevolverCantidad         		= "No se puede devolver esta cantidad.";
    var mstrNoExisteDocAdjunto         		= "No existe documento adjunto.";
    var mstrSelTipoAdjunto         		    = "Debe seleccionar un tipo de Adjunto.";
    var mstrAproboExito         		    = "La solicitud se aprobo con �xito.";
    var mstrEnvioAprobExito        		    = "Se envio la aprobaci�n con �xito.";
    var mstrRechazoSolExito        		    = "Se rechazo la solicitud con �xito.";
    var mstrNoAsigResponsables     		    = "No se encuentra asignado los responsables.";
    var mstrCantidadMayorCero     		    = "Al menos la cantidad de un detalle debe ser mayor a cero.";
    var mstrAsignarResponsable     		    = "Debe Asignar un responsable.";
    var mstrAprobacionYaEnviada    		    = "Est� aprobaci�n ya fue enviada.";
    var mstrConfirRechazarApro    		    = "�Est� seguro de rechazar la aprobaci�n?";
    var mstrDebeIngrCantidad    		    = "Debe ingresar la cantidad.";
    var mstrCampoMayorCero    		        = "Al menos un campo debe ser mayor a cero.";
    var mstrExitoEnvioCot    		        = "La cotizaci�n se envio correctamente.";
    var mstrErrorEnvioCot    		        = "Problemas al enviar la cotizaci�n, consulte con el Administrador.";
    var mstrErrorQuitarCot    		        = "Problemas al quitar la cotizaci�n, consulte con el Administrador.";
    var mstrAsignarProvebyDetalle	        = "Se debe asignar al menos un proveedor por cada detalle.";
    var mstrLlenarCamposObli    	        = "Debe llenar los campos obligatorios.";
    var mstrNoEncontradoCotPendiente        = "No se ha encontrado una cotizaci�n pendiente.";
    var mstrSelTipoPago     			    = "Debe seleccionar un Tipo de Pago.";
    var mstrIngFechaIniVige    			    = "Debe ingresar la fecha de inicio de vigencia.";
    var mstrIngHoraIniVige    			    = "Debe ingresar la hora para la fecha de vigencia de inicio.";
    var mstrIngFechaFinVige    			    = "Debe ingresar la fecha de fin de vigencia.";
    var mstrIngHoraFinVige    			    = "Debe ingresar la hora para la fecha de vigencia de fin.";
    var mstrIngFechaAntesHora  			    = "Antes de ingresar la hora, debe ingresar la fecha.";
    var mstrValFecIniFecFinVige			    = "La fecha de vigencia inicial debe ser menor\no igual a la fecha de vigencia final.";
    var mstrConfEnviarCoti    			    = "�Est� seguro de enviar la cotizaci�n?";
    var mstrIngValAntesEnviDet 			    = "Se debe ingresar los valores de los detalles antes de proceder a enviar la cotizaci�n.";
    var mstrSelProveedor    			    = "Selecci�ne Proveedor.";
    var mstrNoExisteReg    			        = "No existen registros para grabar.";
    var mstrConfCerrar    			        = "�Est� seguro de cerrar?";
    var mstrConfEnviarPedientesCero	        = "Los registros con precio unitario igual a 0 no ser�n atendidos por los proveedores, \n�desea continuar?";
    
    /*Mensajes deGesti�n de Ordenes de Compra / Servicios*/
    var mstrAprobarDetalle			= "Se aprob� el detalle de orden exitosamente.";
    var mstrNoAprobarDetalle		= "Problemas al aprobar el detalle de orden. Consulte con el administrador.";
    var mstrRechazarDetalle			= "Se rechaz� el detalle de orden exitosamente.";
    var mstrNoRechazarDetalle		= "Problemas al rechazar el detalle de orden. Consulte con el administrador."; 
    var mstrCerrarOrden		    	= "Se cerr� la orden exitosamente.";
    var mstrNoCerrarOrden    		= "Problemas al cerrar la orden. Consulte con el administrador.";
    var mstrNoCerrarOrdenBandeja    = "No se puede cerrar esta orden. Consulte con el administrador.";
    var mstrNoAnularPago            = "No se puede anular, consulte con el administrador.";
    var mstrNoEliminarDebeGrabar    = "No se puede eliminar\nEl registro seleccionado no ha sido guardado.";
    var mstrNoSeleccioneRegRepetido = "No debe seleccionar un registro repetido.";
    var mstrLleneTodoCampos			= "Se debe llenar todos los campos obligatorios.";
    var mstrNoAprobarOrden			= "No se puede aprobar esta orden.";
    var mstrSeguroRechazar			= "�Est� seguro de rechazar la orden?";
    var mstrSegurAprobar			= "�Est� seguro de aprobar la orden?";
    
    /*Mensajes de Gesti�n de Documentos*/
    var mstrIngNroGuia			          = "Ingrese un numero de Guia.";
    var mstrIngNroFactura			      = "Ingrese un numero de Factura.";
    var mstrFecEmisionFactMenorActual	  = "La Fecha de Emisi�n de Factura no puede ser menor a la fecha Actual.";
    var mstrFecEmisionNoVacia 			  = "La Fecha Emisi�n de la Factura no puede estar vacia."; 
    var mstrFecVctoMayorFecEmision		  = "La Fecha de Vencimiento debe ser mayor a la fecha de Emisi�n.";
    var mstrFecVctoNoVacia      		  = "La Fecha de Vencimiento no puede estar vacia.";
    var mstrFecEmisionGuiaMenorActual 	  = "La Fecha de Emisi�n de la Gu�a no puede ser menor a la fecha Actual."; 
    var mstrFecEmisionGuiaNoVacia		  = "La Fecha Emisi�n de la Gu�a no puede estar vacia.";
    var mstrSeguroAgregarDocPago		  = "�Est� seguro de agregar el documento de pago?";
    var mstrSeguroAnularDocPago		      = "�Est� seguro de anular el documento de pago?";
    var mstrEnviarDocPago		          = "Se envio el documento de pago correctamente.";
    var mstrNoEnviarDocPago		          = "Ocurrio un error en el envio el documento de pago .Consulte con el administrador.";
    var mstrDuplicadoNroDocYONroGuia      = "El n�mero de documento y/o el n�mero de Gu�a ya existen para el proveedor.";
    var mstrNroFacturaRegistrado          = "El nro. de Factura ya se encuentra registrado.";
    var mstrNroGuiaRegistrado             = "El nro. de Guia ya se encuentra registrado.";
    var mstrDocPagoExcedeCantidad         = "La cantidad facturada excede a la cantidad solicitada.";
    var mstrSeguroEnviarDocPago		      = "�Est� seguro de enviar el documento de pago?";
    var mstrDocPagoEnviar                 = "El documento no puede ser enviado. El documento debe estar en estado pendiente.";
    var mstrDocPagoAnular                 = "El documento no puede ser anulado. El documento debe estar en estado pendiente o enviado."
    var mstrExcedeCantidad                = "La cantidad ingresada de uno o mas detalles excede la cantidad total del detalle de la orden de compra."; 
    var mstrExcedeCantidadDocPago         = "La cantidad facturada excede a la cantidad pendiente.";
    /*Mensajes de Almacen*/
    var mstrAbrirAlmacen				  = "Se abri� el almacen exitosamente.";
    var mstrActAbrirAlmacen				  = "Existe una actualizaci�n de inventario en proceso,\nprimero debe finalizar esta antes de proceder a abrir el almac�n.";
    var mstrProblemaAbrir				  = "Problemas al abrir almacen. Consulte con el administrador.";
    var mstrCerrarAlmacen				  = "Se cerr� el almacen exitosamente.";
    var mstrProblemaCerrar				  = "Problemas al cerrar almacen. Consulte con el administrador.";
    var mstrEnviarInventario			  = "Se envi� el inventario exitosamente.";
    var mstrActEnviarInventario			  = "No existe una actualizaci�n de inventario pendiente de envio.";
    var mstrProblemaEnviarInventario	  = "Problemas al enviar inventario. Consulte con el administrador.";
    var mstrAprobarInventario			  = "Se aprob� el inventario exitosamente.";
    var mstrActAprobarInventario		  = "No existe una actualizaci�n de inventario por aprobar.";
    var mstrProblemaAprobarInventario	  = "Problemas al aprobar inventario. Consulte con el administrador.";
    var mstrRechazarInventario			  = "Se rechaz� el inventario exitosamente.";
    var mstrActRechazarInventario		  = "No existe una actualizaci�n de inventario por rechazar.";
    var mstrProblemaRechazarInventario	  = "Problemas al rechazar inventario. Consulte con el administrador.";
    var mstrRegistrarInventario			  = "Se registro el inventario exitosamente.";
    var mstrAlmacenAbierto				  = "El almac�n se encuentra abiertos en estos momentos, no se puede generar el reporte de inventario.";
    var mstrActRegistrarInventario		  = "Existe un proceso de actualizaci�n de inventario, no se puede realizar esta acci�n.";
    var mstrProblemaRegistrarInventario	  = "Problemas al registrar inventario. Consulte con el administrador.";
   	var mstrSeguroAbrirAlmacen			  = "�Est� seguro de abrir almacen?";
   	var mstrSeguroCerrarAlmacen			  = "�Est� seguro de cerrar almacen?";
   	var mstrSeguroRegistrarInventario	  = "�Est� seguro de registrar el inventario?";
   	var mstrSeguroEnviarInventario		  = "�Est� seguro de enviar el inventario?";
   	var mstrSeguroAprobarInventario		  = "�Est� seguro de aprobar el inventario?";
   	var mstrSeguroRechazarInventario	  = "�Est� seguro de rechazar el inventario?";   	
    
    var mstrModelo						  = "Debe completar los campos marca y modelo.";
    var mstrIngrese						  = "Debe ingresar una descripci�n por cada condici�n seleccionada.";
    var mstrRegistroDoble				  = "Ya existe un registro con esos datos";
    var mstrRegistroDoble2		    	  = "Uno o m�s n�meros de serie ingresados ya se encuentran registrados.";
    var mstrSeguroGrabarGuia        	  = "�Est� seguro de guadar la guia de remisi�n?";
    var mstrSeguroCerrarGuia        	  = "�Est� seguro de cerrar la guia de remisi�n?";
    var mstrNoCantidadAsignada			  = "Uno o mas detalles no cuentan con cantidad de entrega asignada.";
    var mstrNoCantidadCorrecta			  = "Uno o mas detalles de tipo activo no cuentan con la cantidad correcta de series asignadas.";
    var mstrProblemasCerrarGuia  		  = "Problemas al cerrar la gu�a de remisi�n, por favor consulte con el administrador.";
    var mstrGuiaExcedeCantidad     	      = "La cantidad entregada excede a la cantidad facturada.";
    var mstrNoModificarGuiaRemision 	  = "No se puede modificar la gu�a seleccionada.";
    var mstrDevolucionConfirmarExito	  = "La devoluci�n se confirm� con �xito.";
    var mstrDevolucionConfirmarRechazo	  = "La devoluci�n se rechaz� con �xito.";
    var mstrSeguroRealizarDevolucion      = "�Est� seguro de realizar la devoluci�n?";
    var mstrSeguroRechazarDevolucion      = "�Est� seguro de rechazar la Devoluci�n?";
    var mstrAlmacenCerrado     			  = "Para poder realizar esta acci�n el almac�n debe estar cerrado.";
    var mstrEstadoAlmacenAbierto		  = "El almac�n ya se encuentra abierto.";
    var mstrEstadoAlmacenCerrado		  = "El almac�n ya se encuentra cerrado.";

    /*Mensajes de Atencion de Requerimientos*/
    var mstrCerrarRequerimiento		    	= "Se cerr� el requerimiento exitosamente.";
    var mstrNoCerrarRequerimiento    		= "Problemas al cerrar el requerimiento. Consulte con el administrador.";
    var mstrSeguroCerrarRequerimiento       = "�Est� seguro de cerrar el requerimiento?";
    var mstrSeguroModificarNotaSeguimiento  = "�Est� seguro de modificar la nota de seguimiento?";
    var mstrNoCerrarRegistro		    	= "No se puede cerrar este registro.";
    var mstrConformidadExitoRegistro	    = "Se registro la conformidad con �xito.";
    var mstrEstadoPendienteGuia	    		= "La gu�a se encuentra en estado pendiente.";
    var mstrConfirAgregarRegistro    		= "�Est� seguro de agregar un nuevo registro?";
    var mstrErrorResponsable      		    = "Faltan asignar responsables o hay un \nexceso de responsables en uno o mas activos.";
    var mstrSeguroConformidadRegistro	    = "Est� seguro de registrar la conformidad?";
    var mstrConformidadRegistradaAntes	    = "Ya se registro la conformidad.";
    var mstrConfirAtencionRequerimiento		= "�Est� seguro de atender el requerimiento?";
    var mstrDetaExceCantPendientes          = "Uno o mas detalles exceden la cantidad pendiente.";
    var mstrIngCantidadEntregada	        = "Debe ingresar la cantidad entregada.";
    var mstrIngFechaGuia	    	        = "Debe ingresar la fecha de la gu�a.";
    var mstrIngNumeroGuia	    	        = "Debe ingresar el n�mero de gu�a.";
    var mstrNumeroGuiaExiste    	        = "El Nro de gu�a ya existe.";
    var mstrExitoRegConformidad    	        = "Se registro la conformidad con �xito.";
    var mstrFaltanAsigResponsables 	        = "Faltan asignar responsables o hay un \nexceso de responsables en uno o mas activos.";
    var mstrStockCantEntregDispo   	        = "Una o mas cantidades entregadas exceden al stock disponible.";
    var mstrConfModificarReg    	        = "�Esta seguro de modificar el registro?";
    var mstrConfAgregarNuevoReg    	        = "�Esta seguro de agregar un nuevo registro?";
    var mstrConfRegistrarConformidad	    = "�Esta seguro de registrar la conformidad?";
    var mstrRegistrarConformidad	        = "Ya se registro la conformidad";
    var mstrNoRegistrarGuiaAtencion         = "No se puede agregar m�s Guias de Atenci�n. Ya existe una guia asociada.";
    var mstrRegistrarDocPagoRelacionado     = "Primero debe registrar el documento de pago relacionado.";
    
    /*Portal Web*/
    var mstrMustLogin				= "La opci�n requiere el acceso al Sistema Biblioteca";
        
    var mstrSeleccioneEval			= "Seleccione el Evaluador";
    var mstrIngreseAlternativa		= "Ingrese alternativa";
    var mstrIngresePeso 			= "Ingrese peso";
    
    /*ENCUESTAS*/
    var mstrSeleccioneTipoAplicacion 	= "Seleccione tipo de aplicaci�n";
    var mstrSeleccioneTipoEncuesta 		= "Seleccione tipo de encuesta";
    var mstrSeleccioneSede 				= "Seleccione sede";
    var mstrIngrCodigo					= "Ingrese c�digo";
    var mstrIngrNombre					= "Ingrese Nombre";
    var mstrIngrDuracion				= "Ingrese Duraci�n";
    var mstrIngrTipoServicio			= "Ingrese Tipo de Servicio";
    var mstrIngrGrupo					= "Ingrese Grupo";
    var mstrSeleccioneFormato 			= "Seleccione Formato";
    var mstrIngrPeso					= "Ingrese peso";
    var mstrSeleccioneAlternativa 		= "Seleccione alternativa";        
    
    var mstrIngrese3Caracteres 			= "Ingrese al menos 3 caracteres";
    var mstrIngreseValido 				= "Ingrese Criterio de busqueda v�lido";
    var mstrStock						= "El stock m�nimo debe de ser menor al stock m�ximo.";
    var mstrTienePregOrAltRelacionadas  ="No se puede eliminar el grupo tiene preguntas o alternativas relacionadas";
	var mstrTieneAltRelacionadas  		="No se puede eliminar el grupo tiene alternativas relacionadas";
	var mstrCopiar						= "�Est� seguro de copiar el formato de la encuesta?";    	
    var mstrNoModificarEncuesta  		="No se puede modificar la encuesta.";
    var mstrIngreseFiltros				="Debe ingresar el valor de al menos un filtro de busqueda.";
    
    var mstrIgv							="El campo I.G.V. es obligatorio.";
    var mstrGlosa						="El campo glosa cabecera es obligatorio.";
    var mstrExistenciaExistir			="El campo cta. Existencia por Existir es obligatorio.";
    var mstrTipoGastos					="El campo cta. Tipo Gasto Ajustes es obligatorio.";
    var mstrCentroCostos				="El campo cta. Centro Costo Ajustes es obligatorio.";
    var mstrGlosaDetalle				="El campo glosa dettale es obligatorio.";
    var mstrCurser						="El campo cuser es obligatorio.";
    var mstrUbiFila 					= "Debe ingresar un numero en el primer casillero de Ubicaci�n.";
    var mstrUbiColum					="Debe ingresar un numero en el segundo casillero de Ubicaci�n.";
    var mstrTextoDefec					="Debe ingresar un texto por defecto.";
    var mstrMaximoLetras				="Debe ingresar una cantidad menor a 4000 caracteres.";        