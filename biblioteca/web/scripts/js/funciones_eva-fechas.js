function Fc_RestaFechas(date1,dateformat1,date2,dateformat2,dias) {
	var d1=getDateFromFormat(date1,dateformat1);
	var d2=getDateFromFormat(date2,dateformat2);
	if ((d2-d1)/(1000*24*60*60) > dias) 
		return true
	else	
		return false;
}
function _isInteger(val) {
	var digits="1234567890";
	for (var i=0; i < val.length; i++) {
		if (digits.indexOf(val.charAt(i))==-1) { return false; }
		}
	return true;
	}
function _getInt(str,i,minlength,maxlength) {
	for (var x=maxlength; x>=minlength; x--) {
		var token=str.substring(i,i+x);
		if (token.length < minlength) { return null; }
		if (_isInteger(token)) { return token; }
		}
	return null;
	}
function getDateFromFormat(val,format) {
	
	val=val+"";
	format=format+"";
	var i_val=0;
	var i_format=0;
	var c="";
	var token="";
	var token2="";
	var x,y;
	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth()+1;
	var date=now.getDate();
	var hh=now.getHours();
	var mm=now.getMinutes();
	var ss=now.getSeconds();
	
	var ampm="";
	
	while (i_format < format.length) {
		// Get next token from format string
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		// Extract contents of value based on format token
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }
			if (token=="yy")   { x=2;y=2; }
			if (token=="y")    { x=2;y=4; }
			year=_getInt(val,i_val,x,y);
			if (year==null) { return 0; }
			i_val += year.length;
			if (year.length==2) {
				if (year > 70) { year=1900+(year-0); }
				else { year=2000+(year-0); }
				}
			}
		else if (token=="MMM"){
			month=0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name=MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
					month=i+1;
					if (month>12) { month -= 12; }
					i_val += month_name.length;
					break;
					}
				}
			if ((month < 1)||(month>12)){return 0;}
			}
		else if (token=="MM"||token=="M") {
			month=_getInt(val,i_val,token.length,2);
			if(month==null||(month<1)||(month>12)){return 0;}
			i_val+=month.length;}
		else if (token=="dd"||token=="d") {
			date=_getInt(val,i_val,token.length,2);
			if(date==null||(date<1)||(date>31)){return 0;}
			i_val+=date.length;}
		else if (token=="hh"||token=="h") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>12)){return 0;}
			i_val+=hh.length;}
		else if (token=="HH"||token=="H") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>23)){return 0;}
			i_val+=hh.length;}
		else if (token=="KK"||token=="K") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>11)){return 0;}
			i_val+=hh.length;}
		else if (token=="kk"||token=="k") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>24)){return 0;}
			i_val+=hh.length;hh--;}
		else if (token=="mm"||token=="m") {
			mm=_getInt(val,i_val,token.length,2);
			if(mm==null||(mm<0)||(mm>59)){return 0;}
			i_val+=mm.length;}
		else if (token=="ss"||token=="s") {
			ss=_getInt(val,i_val,token.length,2);
			if(ss==null||(ss<0)||(ss>59)){return 0;}
			i_val+=ss.length;}
		else if (token=="a") {
			if (val.substring(i_val,i_val+2).toLowerCase()=="am") {ampm="AM";}
			else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {ampm="PM";}
			else {return 0;}
			i_val+=2;}
		else {
			if (val.substring(i_val,i_val+token.length)!=token) {return 0;}
			else {i_val+=token.length;}
			}
		}
	// If there are any trailing characters left in the value, it doesn't match
	if (i_val != val.length) { return 0; }
	// Is date valid for month?
	if (month==2) {
		// Check for leap year
		if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
			if (date > 29){ return false; }
			}
		else { if (date > 28) { return false; } }
		}
	if ((month==4)||(month==6)||(month==9)||(month==11)) {
		if (date > 30) { return false; }
		}
	// Correct hours value
	if (hh<12 && ampm=="PM") { hh+=12; }
	else if (hh>11 && ampm=="AM") { hh-=12; }
	var newdate=new Date(year,month-1,date,hh,mm,ss);
	return newdate.getTime();
}
function fc_ValidaFechaIniFechaFin1(pFecIni,pFecFin){
	if (pFecIni != "" && pFecFin != "" && pFecIni.length==10 && pFecFin.length==10){				
		var dFecIni = pFecIni.substr(6,4) + "/" + pFecIni.substr(3,2) + "/" + pFecIni.substr(0,2);  
		var dFecFin = pFecFin.substr(6,4) + "/" + pFecFin.substr(3,2) + "/" + pFecFin.substr(0,2); 
		if (dFecIni >= dFecFin)
			return 1;
		else
				return 0;
	}else
		return 2;			
}
function fc_ValidaRangoFechas1(strObj1,strObj2,mensaje1, mensaje2,num){
	//Borra que la fecha de inicio sea menor o igual a la de fin
   	var Obj1 = document.all[strObj1];
   	var Obj2 = document.all[strObj2];
   	var fec1 = Obj1.value;
   	var fec2 = Obj2.value;   	
   	if (fc_ValidaFechaIniFechaFin1(fec1,fec2)==1){
   		alert("La fecha de " + mensaje2 + " debe de ser mayor a la fecha de " + mensaje1 +".");
   		if (parseInt(num)==1){
   			Obj1.value="";
   			Obj1.focus();   			
   		}
   		else{
   			Obj2.value="";
   			Obj2.focus();   			
   		}
   		return false;
   	}
  	return true;   	
 }
function fc_ValidaFechaIniFechaFin(pFecIni,pFecFin){
   	if (pFecIni != "" && pFecFin != "" && pFecIni.length==10 && pFecFin.length==10){			
			var dFecIni = pFecIni.substr(6,4) + "/" + pFecIni.substr(3,2) + "/" + pFecIni.substr(0,2);  
			var dFecFin = pFecFin.substr(6,4) + "/" + pFecFin.substr(3,2) + "/" + pFecFin.substr(0,2); 
				if (dFecIni > dFecFin)
					return 1;
				else
					return 0;
	}else
		return 2;			
}
function fc_ValidaFecha(strNameObj){ 	
	var Obj = document.all[strNameObj];
	var intEncontrado = "1234567890".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}
	if (Obj.value.length == 2 || Obj.value.length == 5){		
		Obj.value = Obj.value + "/";
	}		
}
function fc_ValidaFechaOnblur(strNameObj){ 
   var Obj = document.getElementById(strNameObj);
   var error=false;
   if (Obj.value !=""){   
		if(Obj.value.length == 8){
			var _anio = Obj.value.substring(6,8)
			Obj.value = Obj.value.substring(0,6)+'20'+_anio
		}	
		if (!isFecha(Obj.value,"dd/MM/yyyy")) error=true;
		else{
			strAnho=Obj.value.split("/")[2];
			if (strAnho<'1900') error=true;                
		}
		if (error){
			alert('Debe ingresar una fecha v�lida.');
			Obj.focus();                      
		}
   }
}
function isFecha(val,format) {
	var date=getDateFromFormat(val,format);
	if (date==0) { return false; }
	return true;
}
function fc_ValidaFechaActualRet(strNameObj,num){	
	var Obj = document.all[strNameObj];	
	var strDia,strMes,strAnho;
	var today = new Date();
	strAnho= today.getYear();
	strMes = (today.getMonth()+1);
	strDia = today.getDate();
	if (parseInt(strMes)<=9)
		strMes = '0'+strMes;
	if (parseInt(strDia)<=9)
		strDia = '0'+strDia;	
	var strFechaActual = strDia+"/"+strMes+"/"+strAnho;	
	var dFec = Obj.value.substr(6,4) + "/" + Obj.value.substr(3,2) + "/" + Obj.value.substr(0,2);	  
	var dActual = strFechaActual.substr(6,4) + "/" + strFechaActual.substr(3,2) + "/" + strFechaActual.substr(0,2);	 			
    if (Obj.value !=""){    
		if ((num=='1') && (dFec < dActual)){
			alert("Debe ingresar una fecha mayor o igual a la fecha actual");
			Obj.value="";
			Obj.focus();
			return false;
		}else if ((num=='2') & (dFec >= dActual)){		
			alert("Debe ingresar una fecha menor a la actual");
			Obj.value="";
			Obj.focus();
			return false
		}else if ((num=='3') & (dFec > dActual)){		
			alert("Debe ingresar una fecha menor o igual a la actual");
			Obj.value="";
			Obj.focus();
			return false
		}		
    }
    return true;
}
function fc_ValidaDecimal(fieldValue, NumEntero, NumDecimal){ 
	decallowed = NumDecimal;  // Numero de Decimales
	intallowed = NumEntero;  // Numero de Enteros
	if (isNaN(fieldValue) || fieldValue == ""){ 	
		return false;
	}else{	
		if (fc_ValidaDecimalFinal(fieldValue)==false) {return false;}
		
		if (fieldValue.indexOf('.') == -1) fieldValue += ".";
		dectext = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);
		inttext = fieldValue.substring(0,fieldValue.indexOf('.'));

		if (dectext.length > decallowed || inttext.length > intallowed){		
			return false;
		}
	}
	return true;
}
function fc_ValidaDecimalFinal(strCad){
	var strCadena = new String(strCad);
	if(strCad == "")
		return true;			
	var valido = "1234567890.";
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++){
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1){
			valido = strCadena.substring(i,i + 1);
			return false;
		}
	}
	return true;
}
function fc_ValidaRangoHora(strNameObj1,strNameObj2,num){
	var obj1 = document.getElementById(strNameObj1);
	var strCadena1 = obj1.value;
	var obj2 = document.getElementById(strNameObj2);
	var strCadena2 = obj2.value;
	var numHor2,numMin2,arr1,arr2;
	var band=true;
	if (strCadena1!="" & strCadena2!=""){				
		arr1=strCadena1.split(":");
		if (arr1[0].charAt(0)=='0')
			numHor1 = parseInt(arr1[0].charAt(1));
		else
			numHor1 = parseInt(arr1[0]);					
		arr2=strCadena2.split(":");	
		if (arr2[0].charAt(0)=='0')
			numHor2 = parseInt(arr2[0].charAt(1));
		else
			numHor2 = parseInt(arr2[0]);					
	 	if(numHor1>=numHor2){
			alert("La horaInicial debe ser menor a la horaFinal");
			band=false;
		}
	}	
	return band;
}
function fc_ValidaFechaIniFechaFin2(pFecIni,pFecFin){
	if (pFecIni != "" && pFecFin != "" && pFecIni.length==10 && pFecFin.length==10){				
		var dFecIni = pFecIni.substr(6,4) + "/" + pFecIni.substr(3,2) + "/" + pFecIni.substr(0,2);  
		var dFecFin = pFecFin.substr(6,4) + "/" + pFecFin.substr(3,2) + "/" + pFecFin.substr(0,2); 
		if (dFecIni >= dFecFin)
			return 1;
		else
			return 0;
	}else
		return 2;			
}
function fc_ValidaHora(strIdObj){	
	var Obj = document.all[strIdObj];
	var intEncontrado = "1234567890".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1)
		window.event.keyCode = 0;
	if (Obj.value.length == 2)
		Obj.value = Obj.value + ":";
}
function fc_ValidaHoraOnBlur(form,strNameObj,strMensaje){
	var obj = eval(form + '.'+strNameObj);
	var numHor,numMin,error;
	var strCadena = obj.value;
	error = 0;
	if(strCadena == "")
		return true;
	if (strCadena.length !=5){ error = 1;}		
	else{
		var arr=strCadena.split(":");	
		if (arr[0].charAt(0)=='0')
			numHor = parseInt(arr[0].charAt(1));
		else
			numHor = parseInt(arr[0]);
		if (arr[1].charAt(0)=='0')
			numMin = parseInt(arr[1].charAt(1));
		else
			numMin = parseInt(arr[1]);
		if ((numHor>24) || (numMin>59) || (numHor==24 & numMin!=0)){error = 1;}
	}
	if (error==1){
		alert('El Campo ' + strMensaje + ' es invalido. El formato de hora es HH:MM');
		obj.value="";
		obj.focus();
		return false;		
	}
	return true;
}