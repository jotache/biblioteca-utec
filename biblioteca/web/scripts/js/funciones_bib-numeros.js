function fc_ValidaNumeroFinalMayorCero(obj){
	var entero=parseInt(obj.value,10);	
	if(entero<=0){
		alert("Debe ingresar un n�mero mayor a cero.");
		obj.value="";
		obj.focus();
		return false;
	}
	return true;
}
function fc_PermiteNumeros(){
	if ((window.event.keyCode<48) || (window.event.keyCode>57)) {
		window.event.returnValue =0;}
}
function fc_PermiteNumerosPunto() {	
	var valido = "0123456789.() ";                     
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            window.event.keyCode = 209;
      }else{               
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);            
            if (intEncontrado == -1){                       
                 window.event.keyCode = 0;          
            }else{                        
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }
}
function fc_ValidaNumeroOnBlur(strNameObj){
	//var Obj = document.all[strNameObj];
	var Obj = document.getElementById(''+strNameObj);
	if(!Obj.value.toString().match(/^\d+$/g) && Obj.value !=''){
		Obj.value="";
		alert('Debe ingresar un n�mero correcto.');
		Obj.focus();
	}
}
function fc_ValidaNumeroFinal(strNameObj, strMensaje){
	//var Obj = document.all[strNameObj];
	var Obj = document.getElementById(''+strNameObj);
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "0123456789 ";			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++){		
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1){		
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}
function fc_ValidaDecimalOnBlur(strNameObj, NumEntero, NumDecimal){	
 	//var Obj = document.all[strNameObj];
	var Obj = document.getElementById(''+strNameObj);
 	if (Obj.value !=""){ 	
 		if (!fc_ValidaDecimal(Obj.value, NumEntero, NumDecimal)) {			
 			Obj.value="";
 			alert('Debe ingresar un valor correcto.');			
 			Obj.focus();
 		}
 		var num= Number(Obj.value)
 		Obj.value = num.toFixed(NumDecimal)
 	}
}
function fc_ValidaDecimal(fieldValue, NumEntero, NumDecimal){ 
	decallowed = NumDecimal;  // Numero de Decimales
	intallowed = NumEntero;  // Numero de Enteros
	if (isNaN(fieldValue) || fieldValue == ""){ 	
		return false;
	}else{	
		if (fc_ValidaDecimalFinal(fieldValue)==false) {return false;}
		
		if (fieldValue.indexOf('.') == -1) fieldValue += ".";
		dectext = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);
		inttext = fieldValue.substring(0,fieldValue.indexOf('.'));

		if (dectext.length > decallowed || inttext.length > intallowed){		
			return false;
		}
	}
	return true;
}
function fc_ValidaDecimalFinal(strCad){
	var strCadena = new String(strCad);
	if(strCad == "")
		return true;
				
	var valido = "1234567890.";
			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			return false;
		} 
	}	
	return true;
}