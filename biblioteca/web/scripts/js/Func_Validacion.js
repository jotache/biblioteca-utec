function fc_maxlength(obj, long){
	var strCadena = obj.value;
	if ( parseFloat(strCadena.length) > parseFloat(long) )	
		obj.value = strCadena.substring(0, long);	
}

function fc_ValidaUrl() {      
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            //convierte la � en �
            //window.event.keyCode = 209;
      }else{               
            var ch_Caracter = String.fromCharCode(window.event.keyCode);//.toUpperCase();
            var intEncontrado = " 1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_.:@&_/ ".indexOf(ch_Caracter);            
            if (intEncontrado == -1)                      
                 window.event.keyCode = 0;                                 
      }
}

 function fc_PermiteNumerosPunto() {	
	var valido = "0123456789.() ";                     
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            window.event.keyCode = 209;
      }else{               
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);            
            if (intEncontrado == -1){                       
                 window.event.keyCode = 0;          
            }else{                        
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }
}

/*************************************************************************************
Descripci�n :    Permite validar si el RUC es v�lido retornando true or false de acuerdo
				 al caso.
Autor 		:    Carlos Cordova
Modificado  :	 Ronnie Ramos
Fecha		:    07/08/2006
Empresa		:    CosapiSoft S.A.
************************************************************************************/

function ValidarRuc_Retorno(strNomRuc){		
    var suma = 0;
    var Ruc = new String(document.all[strNomRuc].value);			
    var result = false;
    if (Ruc!='99999999999' && Ruc!=''){    
		if (Ruc.length == 11){		
			Ruc = Ruc.split("");
			var strPar = new String("5,4,3,2,7,6,5,4,3,2,");
			var arrPar = new Array(10);
			arrPar = strPar.split(",");
			var caracter = parseInt(Ruc[10]);
			for(var i=0; i<10; i++){			
				suma = parseInt(suma) + parseInt(arrPar[i]) * parseInt(Ruc[i]);
			}				
			var resto = suma % 11;
			var verificador = 11 - resto;
			if (verificador==11){
					verificador = 1;
			}else if (verificador==10){			
					verificador = 0;
			}
			if (verificador!=caracter){
				alert("R.U.C. no valido.");
				document.all[strNomRuc].focus();
				result = false;
			}else {result = true;}			
		}else{			
			result = false;
			alert("R.U.C. no valido.");
			document.all[strNomRuc].focus();
		}
    }else{    
    result = true;}
    return result;
}
		
/*************************************************************************************
Descripci�n :    Permite compara dos fechas, en caso una este vacia no la cuenta
Inputs		:    
Autor 		:    Carlos Cordova
Fecha/hora	:    07/08/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaFechaIniFechaFin(pFecIni,pFecFin){
   	if (pFecIni != "" && pFecFin != "" && pFecIni.length==10 && pFecFin.length==10){			
			var dFecIni = pFecIni.substr(6,4) + "/" + pFecIni.substr(3,2) + "/" + pFecIni.substr(0,2);  
			var dFecFin = pFecFin.substr(6,4) + "/" + pFecFin.substr(3,2) + "/" + pFecFin.substr(0,2); 
				if (dFecIni > dFecFin)
					return 1;
				else
					return 0;
	}else
		return 2;			
}

/*************************************************************************************
Descripci�n :    Borra que la fecha de inicio sea menor a la de fin
Autor 		:    Arturo De La Puente Altamirano
Fecha		:    07/08/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaRangoFechas(strObj1,strObj2,mensaje1, mensaje2,num){ 
   	var Obj1 = document.all[strObj1];
   	var Obj2 = document.all[strObj2];
   	var fec1 = Obj1.value;
   	var fec2 = Obj2.value;   	
   	if (fc_ValidaFechaIniFechaFin(fec1,fec2)==1){
   		alert("La fecha de "+mensaje1+" es mayor que la fecha de "+mensaje2);
   		if (parseInt(num)==1){
   			Obj1.value="";
   			Obj1.focus();   			
   		}else{   		
   			Obj2.value="";
   			Obj2.focus();   			
   		}
   		return false;
   	}
  	return true;   	
}

/*************************************************************************************
Descripci�n :    Valida que la fecha final no sea menor o igual a la fecha de inicio
Inputs		:    
Autor 		:    Carlos Cordova
Fecha/hora	:    09/09/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaFechaIniFechaFin1(pFecIni,pFecFin){
	if (pFecIni != "" && pFecFin != "" && pFecIni.length==10 && pFecFin.length==10){				
		var dFecIni = pFecIni.substr(6,4) + "/" + pFecIni.substr(3,2) + "/" + pFecIni.substr(0,2);  
		var dFecFin = pFecFin.substr(6,4) + "/" + pFecFin.substr(3,2) + "/" + pFecFin.substr(0,2); 
		if (dFecIni >= dFecFin)
			return 1;
		else
				return 0;
	}else
		return 2;			
}

/*************************************************************************************
Descripci�n :    Borra que la fecha de inicio sea menor o igual a la de fin
Autor 		:    Carlos Cordova
Fecha		:    09/09/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaRangoFechas1(strObj1,strObj2,mensaje1, mensaje2,num){
   	var Obj1 = document.all[strObj1];
   	var Obj2 = document.all[strObj2];
   	var fec1 = Obj1.value;
   	var fec2 = Obj2.value;
   	
   	if (fc_ValidaFechaIniFechaFin1(fec1,fec2)==1){
   		alert("La fecha de " + mensaje2 + " debe de ser mayor a la fecha de " + mensaje1 +".");
   		if (parseInt(num)==1){
   			Obj1.value="";
   			Obj1.focus();   			
   		}
   		else{
   			Obj2.value="";
   			Obj2.focus();   			
   		}
   		return false;
   	}
  	return true;   	
 }


/*************************************************************************************
Descripci�n :    Permit verificar si es un numero decimal, y mandar un mensaje de error
Inputs		:    
Autor 		:    Cordova Torres
Fecha/hora	:    04/08/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaDecimalOnBlur(strNameObj, NumEntero, NumDecimal){	
 	var Obj = document.all[strNameObj];	
 	if (Obj.value !=""){ 	
 		if (!fc_ValidaDecimal(Obj.value, NumEntero, NumDecimal)) {			
 			Obj.value="";
 			alert('Debe ingresar un valor correcto.');			
 			Obj.focus();
 		}
 		var num= Number(Obj.value)
 		Obj.value = num.toFixed(NumDecimal)
 	}
 }

function fc_ValidaDecimalOnBlur2(strNameObj, NumEntero, NumDecimal){ 
 	var Obj = document.getElementById(strNameObj);
 	if (Obj.value !=""){ 	
 		if (!fc_ValidaDecimal(Obj.value, NumEntero, NumDecimal)) {			
 			Obj.value="";
 			alert('Debe ingresar un valor correcto.');			
 			Obj.focus();
 		}
 		var num= Number(Obj.value)
 		Obj.value = num.toFixed(NumDecimal)
 	}
}

/*************************************************************************************
Descripci�n :    Permite verificar si es un numero entero
Inputs		:    
Autor 		:    Julio Montalvo
Fecha/hora	:    11/09/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaNumeroOnBlur(strNameObj){
	var Obj = document.all[strNameObj];
	if(!Obj.value.toString().match(/^\d+$/g) && Obj.value !=''){
		Obj.value="";
		alert('Debe ingresar un n�mero correcto.');
		Obj.focus();
	}
}

function fc_ValidaDecimal(fieldValue, NumEntero, NumDecimal){ 
	decallowed = NumDecimal;  // Numero de Decimales
	intallowed = NumEntero;  // Numero de Enteros
	if (isNaN(fieldValue) || fieldValue == ""){ 	
		return false;
	}else{	
		if (fc_ValidaDecimalFinal(fieldValue)==false) {return false;}
		
		if (fieldValue.indexOf('.') == -1) fieldValue += ".";
		dectext = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);
		inttext = fieldValue.substring(0,fieldValue.indexOf('.'));

		if (dectext.length > decallowed || inttext.length > intallowed){		
			return false;
		}
	}
	return true;
}

/*************************************************************************************
Descripci�n :    Permite Ingresar solo numeros
Inputs		:    
Autor 		:    Ronnie Ramos
Fecha/hora	:    04/08/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaNumero(){	
	//strNameObj : Nombre de la caja de texto a validar.	
	var intEncontrado = "1234567890".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}		
}

function fc_ValidaNumeroGuion(){		
	var intEncontrado = "1234567890-.".indexOf(String.fromCharCode(window.event.keyCode));
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}		
}

/*************************************************************************************
Descripci�n :    Permite Ingresar solo numeros
Inputs		:    
Autor 		:    Ronnie Ramos
Fecha/hora	:    04/08/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaNumeroDecimal(){	
	var intEncontrado = "1234567890.".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}		
}

/*************************************************************************************
Descripci�n :    Permite Validar se hayan ingresado solo Numeros 
Inputs		:    strNameObj
Autor 		:    Ronnie Ramos
Fecha/hora	:    04/08/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaNumeroFinal(strNameObj, strMensaje){
	var Obj = document.all[strNameObj];
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "0123456789 ";			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++){		
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1){		
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}

/*************************************************************************************
Descripcion : Permite validar caracteres no validos (cortar/pegar)
			  Muestra un mensaje de error en caso de que los 
			  caracteres sean no validos
Autor		: Ronnie Ramos
Fecha/hora	: 04/08/2006
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaTextoGeneralEmail( strNameObj,strMensaje ){	
	var Obj = document.getElementById(strNameObj);
	var strCadena = new String(Obj.value);
	var s = strCadena;	
	var filter=/^[A-Za-z][A-Za-z0-9_.]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
	if (s.length == 0 ) return true;
	if (filter.test(s))	return true;
	else{
		alert("Ingrese una direccion de correo valida");
		Obj.value = fc_Trim(s);
		Obj.focus();
	}	
	return false;
}

/*************************************************************************************
Descripci�n :    Valida que el parametro sea una fecha 
Autor 		:    YVA
Fecha/hora	:    19/05/2003
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function isFecha(val,format) {
	var date=getDateFromFormat(val,format);
	if (date==0) { return false; }
	return true;
}


/*************************************************************************************
Descripci�n :    Valida fecha invocada desde evento onblur
Autor 		:    YVA
Fecha/hora	:    19/05/2003
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaFechaOnblur(strNameObj){ 
   var Obj = document.getElementById(strNameObj);
   var error=false;
   if (Obj.value !=""){   
		if(Obj.value.length == 8){
			var _anio = Obj.value.substring(6,8)
			Obj.value = Obj.value.substring(0,6)+'20'+_anio
		}
		//cambio en el formato de fecha antes dd-MM-yyyy 
		if (!isFecha(Obj.value,"dd/MM/yyyy")) error=true;
		else{
			strAnho=Obj.value.split("/")[2];

			if (strAnho<'1900') error=true;                
		}
		if (error){
			alert('Debe ingresar una fecha v�lida.');
			Obj.focus();                      
		}
   }            
}

function fc_PermiteNumeros(){
	if ((window.event.keyCode<48) || (window.event.keyCode>57)) {
		window.event.returnValue =0;}
}

/*************************************************************************************
Modulo 		:    
Descripci�n :    Permite 
Inputs		:    
Autor 		:    Tom�s Chuquillanqui Ospina
Fecha/hora	:    15/03/2001
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaFecha(strNameObj){ 	
	//strNameObj : Nombre de la caja de texto a validar.
	var Obj = document.all[strNameObj];
	var intEncontrado = "1234567890".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}
	//agregado 
	if (Obj.value.length == 2 || Obj.value.length == 5){		
		Obj.value = Obj.value + "/";
	}		
}

function fc_PermiteNumeros(){
	if ((window.event.keyCode<48) || (window.event.keyCode>57)) {
		window.event.returnValue =0;}
}

function fc_Slash(form,objeto,separador){
	var obj
	fc_PermiteNumeros();
	if (form != ''){obj= eval(form + '.'+ objeto)} 
	else {obj= eval(objeto)}
	if ((obj.value.length == 2)||(obj.value.length ==5)){obj.value = obj.value + separador;}
    if (obj.value.length = obj.maxlength-1){return;}
}

/*************************************************************************************
Descripci�n :    Valida la hora de formato (HH:MM)
Autor 		:    Arturo De La Puente Altamirano
Fecha		:    07/08/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaHoraOnBlur(form,strNameObj,strMensaje){
	var obj = eval(form + '.'+strNameObj);
	var numHor,numMin,error;
	var strCadena = obj.value;
	error = 0;
	if(strCadena == ""){
		return true;}
	if (strCadena.length !=5){ error = 1;}		
	else{
		var arr=strCadena.split(":");	
		if (arr[0].charAt(0)=='0')
			numHor = parseInt(arr[0].charAt(1));
		else
			numHor = parseInt(arr[0]);
		if (arr[1].charAt(0)=='0')
			numMin = parseInt(arr[1].charAt(1));
		else
			numMin = parseInt(arr[1]);
		if ((numHor>24) || (numMin>59) || (numHor==24 & numMin!=0)){error = 1;}
	}
	if (error==1){
		alert('El Campo ' + strMensaje + ' es invalido. El formato de hora es HH:MM');
		obj.value="";
		obj.focus();
		return false;		
	}
	return true;
}

/*************************************************************************************
Descripci�n :    Valida que fecha ingresada sea menor/mayor o igual a la actual
Autor 		:    Diego Alvarez Mere
Fecha/hora	:    02/09/2006
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaFechaActualRet(strNameObj,num){	
	var Obj = document.all[strNameObj];	
	var strDia,strMes,strAnho;
	var today = new Date();
	strAnho= today.getYear();
	strMes = (today.getMonth()+1);
	strDia = today.getDate();
	if (parseInt(strMes)<=9)
		strMes = '0'+strMes;
	if (parseInt(strDia)<=9)
		strDia = '0'+strDia;	
	var strFechaActual = strDia+"/"+strMes+"/"+strAnho;	
	var dFec = Obj.value.substr(6,4) + "/" + Obj.value.substr(3,2) + "/" + Obj.value.substr(0,2);	  
	var dActual = strFechaActual.substr(6,4) + "/" + strFechaActual.substr(3,2) + "/" + strFechaActual.substr(0,2);	 
				
    if (Obj.value !="")
    {
		if ((num=='1') && (dFec < dActual))
		{		
			alert("Debe ingresar una fecha mayor o igual a la fecha actual");
			Obj.value="";
			Obj.focus();
			return false;
		}		   
		else if ((num=='2') & (dFec >= dActual))
		{
			alert("Debe ingresar una fecha menor a la actual");
			Obj.value="";
			Obj.focus();
			return false
		}
		else if ((num=='3') & (dFec > dActual))
		{
			alert("Debe ingresar una fecha menor o igual a la actual");
			Obj.value="";
			Obj.focus();
			return false
		}		
    }
    return true;
}

function fc_ValidaSoloLetrasFinal(strNameObj,strMensaje){
/*************************************************************************************
Descripcion : Permite ingresar solo letras o espacio en blanco
			  Muestra un mensaje de error en caso de que los 
			  caracteres sean no validos
Autor		: Carolina Vega
Fecha/hora	: 15/08/2006
Empresa		: CosapiSoft S.A.
*************************************************************************************/
	var Obj = document.all[strNameObj];
	
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ.";
			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}

function fc_Trim(pstrInput) {
	var i;
	var vstrTemp = '';
	var j = 0;
	var vstrOut = pstrInput;
	
	if (pstrInput=='' || pstrInput.length <1){return ''} 

	for (i=0;i<pstrInput.length;i++)
	{
		vstrTemp = pstrInput.substr(i,1);
		if(vstrTemp==' '){j++}
		else{break}
	}
	
	if (j==pstrInput.length){return ''}
	if (j>0){vstrOut = pstrInput.substring(j, pstrInput.length-1)}
	j=0;
	for (i=vstrOut.length-1;i>=0;i--)
	{
		vstrTemp = vstrOut.substr(i,1);
		if(vstrTemp==' '){j++}
		else{break}
	}
	if (j>0){vstrOut = vstrOut.substring(0, vstrOut.length - j++)}
	
	return vstrOut;

}
 
function fc_ValidaLongitudRuc(strNameObj){
	Obj = document.all[strNameObj];
	var cad = Obj.value;
	if (cad !=""){
		if (cad.length>11)
		{		
			alert("La longitud del Nro Ruc es menor a 11");
			Obj.value="";
			Obj.focus();
		}   		
	}
}


/*************************************************************************************
Descripci�n :    Valida la hora en formato de 24h
Inputs		:    
Autor 		:    Ronnie
Modificado 	:    Julio Montalvo
Fecha/hora	:    20/09/2006
Empresa		:    CosapiSoft S.A.
Ejemplo		:	 onblur="fc_ValidaHora(this)"
*************************************************************************************/
function isHora(pstrHora, pstrFormato)
{
	if(!((Number(pstrHora.substring(0,2))>=0)&&(Number(pstrHora.substring(0,2))<24))){return false;}
	if(!((Number(pstrHora.substring(3,5))>=0)&&(Number(pstrHora.substring(3,5))<60))){return false;}
	return true;

}
function FP_ValidaHoraOnblur(strNameObj) 
{					
	var Obj = document.all[strNameObj];
	
	if (Obj.value !="")
	{ if (!isHora(Obj.value,"HH:mm") || Obj.value.replace(/ /g,'').length != 5) {
			alert('Debe ingresar una hora valida.\nEl formato de hora es: HH:mm.\n\nFormato de 24 horas.');
			Obj.value="";
			Obj.focus();
		}		
	}	
}

/*************************************************************************************
Descripci�n :    Permite validar un objeto que tiene como valor un decimal
Autor 		:    Arturo De La Puente
Modificado  :	 
Fecha		:    21/09/2006
Empresa		:    CosapiSoft S.A.
************************************************************************************/
function fc_ValidaObjDecimalOnBlur(Obj, NumEntero, NumDecimal)
{
	if (Obj.value !="")
	{
		if (!fc_ValidaDecimal(Obj.value, NumEntero, NumDecimal)) {
			Obj.value="";
			alert('Debe ingresar un valor correcto.');
			Obj.focus();				
		}
	}
}

/*************************************************************************************
Descripci�n :    Permite validar el ingreso de numeros o punto.
Autor 		:    Arturo De La Puente
Modificado  :	 
Fecha		:    21/09/2006
Empresa		:    CosapiSoft S.A.
************************************************************************************/
function fc_ValidaNumerico() {
	var ch_Caracter = String.fromCharCode(window.event.keyCode);
	var intEncontrado = "0123456789.".indexOf(ch_Caracter);
	if (intEncontrado == -1) {	
		window.event.keyCode = 0;		
	}
	else {
		window.event.keyCode = ch_Caracter.charCodeAt();
	}	
}

/*************************************************************************************
Autor		: Nohelia Vilchez
Descripcion : Permite validar caracteres
Fecha       : 07-11-06
Empresa		: CosapiSoft S.A.
*************************************************************************************/
 function fc_ValidaLetrasNumerosPunto() { 
	
	var valido = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.() ";                     
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            //convierte la � en �
            window.event.keyCode = 209;
      }
      else{          
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);            
            if (intEncontrado == -1)
            {           
                 window.event.keyCode = 0;          
            }
            else
            {
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }
}

/*************************************************************************************
Autor		: Nohelia Vilchez
Descripcion : Permite validar NUMEROS Y GUION
Fecha       : 07-11-06
Empresa		: CosapiSoft S.A.
*************************************************************************************/
 function fc_ValidaNumerosGuion() { 
	
	var valido = "0123456789- ";                     
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            //convierte la � en �
            window.event.keyCode = 209;
      }
      else{          
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);            
            if (intEncontrado == -1)
            {           
                 window.event.keyCode = 0;          
            }
            else
            {
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }
}

function fc_ValidaSoloTextoNumero(){	
	//strNameObj : Nombre de la caja de texto a validar.
	
	var valido = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";                     
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            //convierte la � en �
            window.event.keyCode = 209;
      }
      else{          
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);            
            if (intEncontrado == -1)
            {           
                 window.event.keyCode = 0;          
            }
            else
            {
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }	
}
	
/*************************************************************************************
Autor		: Ruben Ochoa Anegeles
Descripcion : comparar 2 horas en formato HH:MM ob1 es el id del primer txt y ob2 esel segundo id 
Fecha       : 05-07-07
Empresa		: CosapiSoft S.A.
*************************************************************************************/

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Valida s�lo Texto y caracteres � � � � � � � � � � � � �
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaTextoEspecial() {  
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}

function fc_ValidaTextoNumeroEspecial() {  
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = " 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Valida cantidad de Caracteres establecida, y muestra un mensaje de
			  acuerdo al id del campo
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_CantCaracter(strNameObj, cant, campo){
//strNameObj -  objeto
//cant - cantidad de caracteres

	var strCadena = strNameObj.value;
	if(strCadena!=""){
		switch(campo){
			case 'Clave':
				if (strCadena.length<cant){
					alert("La clave debe tener minimo 6 caracteres.");
					strNameObj.focus();
				}
			break;
			case 'Dni':
				if (strCadena.length<cant){
					alert("El DNI debe tener 8 caracteres.");
					strNameObj.focus();
				}
			break;
			case 'Ruc':
				if (strCadena.length<cant){
					alert("El RUC debe tener 11 caracteres.");
					strNameObj.focus();
				}
			break;
		}
	}
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Valida decimal en caso tenga el punto(.); caso contrario no se le asigna
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaDecimalSiNo(strNameObj, NumEntero, NumDecimal) 
{
	var Obj = document.all[strNameObj];
	var fieldValue = Obj.value;

	if (fieldValue !=""){
		if (!fc_ValidaDecimal(fieldValue, NumEntero, NumDecimal)) {
			Obj.value="";
			alert('Debe ingresar un valor correcto.');
			Obj.focus();				
		}
		else{
			if (fieldValue.indexOf('.') != -1){
				var num = Number(fieldValue)
				Obj.value = num.toFixed(NumDecimal)
			}
		}
	}
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Validaci�n del campo a�o (mayor a 1900)
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaAnio(strNameObj){
	var Obj = document.all[strNameObj];
	var strCadena = Obj.value;
	if (fc_Trim(strCadena)!=""){
		if (strCadena < 1900){
			return false;
		}
	}
	return true;
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Valida campo a�o inidicando si debe ser mayor o menor que otro
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaAnioMayor(strNameObj1, strNameObj2, valor){
	//valor -> 1 StrNameObj1 < strNameObj2
	//valor -> 2 StrNameObj2 > strNameObj1
	
	var Obj1 = document.all[strNameObj1];
	var Obj2 = document.all[strNameObj2];
	
	var strCadena1 = Obj1.value;
	var strCadena2 = Obj2.value;
	
	if(fc_ValidaAnio(strNameObj1) && fc_ValidaAnio(strNameObj2)){
		if(fc_Trim(strCadena1) != "" && fc_Trim(strCadena2) != "") {
			if (fc_Trim(strCadena1) > fc_Trim(strCadena2)){
				if(valor==1){
					alert("El a�o de inicio debe ser menor o igual al a�o fin ");
					Obj1.value = "";
					Obj1.focus();
					return false;
				}
				if(valor==2){
					alert("El a�o fin debe ser mayor o igual al a�o de inicio ");
					Obj2.value = "";
					Obj2.focus();
					return false;
				}
			}
			else{
				if (fc_Trim(strCadena1) == ""){
					//var mstrValidaCampo = "Debe ingresar el valor correspondiente en el campo ";
					alert(mstrValidaCampo + "A�o Inicio");
					Obj2.value();
					Obj1.focus();
					return false;
				}
			}
		}
	}
	else{
		alert('El a�o no es v�lido');
		if(valor==1){
			Obj1.value = "";
			Obj1.focus();
			return false;
		}
		if(valor==2){
			Obj2.value = "";
			Obj2.focus();
			return false;
		}
	}
	return true;
}


/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Valida campo fecha "mm/aaaa" escribiendo "/" automaticamente
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaFechaEstSup(strNameObj)
{	
	//strNameObj : Nombre de la caja de texto a validar.
	var Obj = document.all[strNameObj];
	var intEncontrado = "1234567890".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}

	if (document.all[strNameObj].value.length == 2){
		document.all[strNameObj].value = document.all[strNameObj].value + "/";
	}		
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Valida campo fecha "mm/aaaa" verificando si el mes y a�o son v�lidos
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaFechaEstSupOnBlur(strNameObj){
	var Obj = document.all[strNameObj];
	var error = false;
	
	if(Obj.value != ""){
		strAnho = Obj.value.split("/")[1];
		strMes = Obj.value.split("/")[0];
		
		if (strAnho<'1900' || strMes > 12 || strMes <= 0) error=true;
		
		if (error){
			alert('Debe ingresar una fecha valida.');
			Obj.focus();                      
			return false;
		}
	}
	return true;
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Validaci�n de un combo con una caja de texto
			  Si combo es diferente a vacio , la caja de texto se dehabilita
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_onChange(obj, strObjName){
	if (document.getElementById(obj.id).value != ""){
		document.getElementById(strObjName).disabled = true;
	}
	else{
		document.getElementById(strObjName).disabled = false;
	}
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Validaci�n de texto en may�sculas, min�sculas y n�meros
			  Si combo es diferente a vacio , la caja de texto se dehabilita
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaTextoNum() {
     //� �
     if((window.event.keyCode != 209) && (window.event.keyCode != 241)){            
       	var ch_Caracter = String.fromCharCode(window.event.keyCode);
		var intEncontrado = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(ch_Caracter);            
		if (intEncontrado == -1)
		{           
				window.event.keyCode = 0;          
		}
		else
		{
				window.event.keyCode = ch_Caracter.charCodeAt();
		}    
	}
}

/*************************************************************************************
Autor		: Gloria Medina
Descripcion : Validaci�n de texto en may�sculas, min�sculas y n�meros
			  Si combo es diferente a vacio , la caja de texto se dehabilita
Empresa		: CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaTextoEspecial1() {  
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = " abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}

function fc_ValidaSoloLetrasFinal1(strNameObj,strMensaje){
/*************************************************************************************
Descripcion : Permite ingresar solo letras o espacio en blanco
			  Muestra un mensaje de error en caso de que los 
			  caracteres sean no validos
Autor		: Carolina Vega
Fecha/hora	: 15/08/2006
Empresa		: CosapiSoft S.A.
*************************************************************************************/
	var Obj = document.all[strNameObj];
	
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����������";
			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}

/**/
function fc_ValidaTextoEspOnBlur(strNameObj,strMensaje){
/*************************************************************************************
Descripcion : Valida el texto especial
Fecha/hora	: 15/08/2006
Empresa		: CosapiSoft S.A.
*************************************************************************************/
	var Obj = document.all[strNameObj];
	var strCadena = new String(Obj.value);
	if(strCadena == ""){
		return true; 
	}
	var valido = "abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����������";
			

	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			Obj.focus();	
			return false;
		} 
	}	
	return true;
}


/*************************************************************************************
Descripci�n :    Permite verificar si es un numero entero y mayor a cero
Inputs		:    
Autor 		:    Napa
Fecha/hora	:    07/01/2007
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaNumeroFinalMayorCero(obj){
	var entero=parseInt(obj.value,10);	
	if(entero<=0){
		alert("Debe ingresar un n�mero mayor a cero.");
		obj.value="";
		obj.focus();
		return false;
	}
	return true;
}
/*
*/
function fc_ValidaTextoNumeroFinal(strNameObj,strMensaje){
	var Obj = document.all[strNameObj];
	
	var strCadena = new String(strNameObj.value);


	if(strCadena == "")
		return true;

	var valido = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		
		if(String.fromCharCode(241)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(209)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(225)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(233)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(237)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(243)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(250)== strCadena.substring(i,i+1).toLowerCase()) continue;
						 
		
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1 )
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			strNameObj.focus();	
			return false;
		} 
		
	}	
	return true; 
}

/*************************************************************************************
Descripci�n :    Valida Nota del 0 al 20, manda un mensaje de error en caso contrario
Inputs		:    
Autor 		:    Gloria Medina
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaNota(strNameObj){
	var Obj = document.all[strNameObj];
	if(Obj.value != ""){
		if (Obj.value<0 || Obj.value>20){
			alert("La nota debe estar entre 0 y 20");
			Obj.value="";
			Obj.focus();
			return false;
		}
	}
}

function fc_ValidaPromedio(strNameObj){
	
    var strCadena = document.getElementById(strNameObj);
    var error = "wild";
	if (strCadena.value!=""){
		if(strCadena.value > 10 && strCadena.value < 21){
			error="mad";
			
		}
		if (error=="wild"){
		    //strCadena.value="";
			alert(mstrPromedio);
			strCadena.focus();                      
		}
		
	}
	
}

/*************************************************************************************
Descripci�n :    Valida fecha invocada desde evento onblur
Autor 		:    Eddy Dominguez Flores
Fecha/hora	:    19/05/2003
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_FechaOnblur(strNameObj) 
{      
       //strNameObj : Nombre de la caja de texto a validar.
     var Obj = document.getElementById(strNameObj);
       var error=false;
       if (Obj.value !="")
       {
			if(Obj.value.length == 8){
				var _anio = Obj.value.substring(6,8)
				Obj.value = Obj.value.substring(0,6)+'20'+_anio
			}
			//cambio en el formato de fecha antes dd-MM-yyyy 
			if (!isFecha(Obj.value,"dd/MM/yyyy")) error=true;
			else{
				strAnho=Obj.value.split("/")[2];

				if (strAnho<'1900') error=true;                
			}
			if (error){
				Obj.value="";
				alert('Debe ingresar una fecha v�lida.');
				Obj.focus();                      
			}
       }            
}


/*************************************************************************************
Descripci�n :    Permite validar el ingreso de numeros.
Autor 		:    Eddy Dominguez Flores
Modificado  :	 
Fecha		:    28/02/2007
Empresa		:    CosapiSoft S.A.
************************************************************************************/
function fc_ValidaNumerico2() {
	var ch_Caracter = String.fromCharCode(window.event.keyCode);
	var intEncontrado = "0123456789".indexOf(ch_Caracter);
	if (intEncontrado == -1) {	
		window.event.keyCode = 0;		
	}
	else {
		window.event.keyCode = ch_Caracter.charCodeAt();
	}	
}

//*********Valdida lo mismo que la funcion fc_ValidaTextoEspecial() mas las comas***************
   function fc_ValidaTextoEspecialTodo() {  
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,,/*-@'_.;()".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}

function fc_ValidaNumerosAndLetrasFinalTodo(strNameObj,strMensaje){
//*****************************Onblur********************************************************

	var Obj = document.all[strNameObj];
	
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "0123456789abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����������,/*-@'_.;";
			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}

//*********Valdida lo mismo que la funcion fc_ValidaTextoEspecial() mas las comas***************
   function fc_ValidaTextoEspecialAndNumeroTodo() {  //OnkeyPress
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && 
      (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && 
      (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && 
      (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && 
      (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = "0123456789 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,,/*-@'_.;".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}

/*************************************************************************************
Descripci�n :    Permite Ingresar solo letras, numeros y gui�n
Inputs		:    
Autor 		:    Rorick Napa Paredes 
Fecha/hora	:    05/03/2008
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaTextoNumeroGuionEspecial() {  
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = " 0123456789-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}
function fc_ValidaTextoNumeroGuionFinal(strNameObj,strMensaje){
	var Obj = document.all[strNameObj];
	
	var strCadena = new String(strNameObj.value);


	if(strCadena == "")
		return true;

	var valido = " 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-";
			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++){				
		if(String.fromCharCode(241)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(209)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(225)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(233)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(237)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(243)== strCadena.substring(i,i+1).toLowerCase()) continue;
		if(String.fromCharCode(250)== strCadena.substring(i,i+1).toLowerCase()) continue;
						 		
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1 ){		
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' )
			strNameObj.focus();	
			return false;
		} 
		
	}	
	return true; 
}
//*************************************************************************************
function fc_ValidaFechaIniFechaFin2(pFecIni,pFecFin)
   {	if (pFecIni != "" && pFecFin != "" && pFecIni.length==10 && pFecFin.length==10)
			{	
				var dFecIni = pFecIni.substr(6,4) + "/" + pFecIni.substr(3,2) + "/" + pFecIni.substr(0,2);  
				var dFecFin = pFecFin.substr(6,4) + "/" + pFecFin.substr(3,2) + "/" + pFecFin.substr(0,2); 
				if (dFecIni >= dFecFin)
					return 1;
				else
					return 0;
			}
			else
				return 2;			
   }
   
function fc_ValidaNumeroGuionOnblur(strNameObj,strMensaje){
//*****************************Onblur********************************************************
	var Obj = document.all[strNameObj];
	//alert(Obj.value);	
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "0123456789-.";
			
	strCadena = strCadena;
	
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' );
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}

function fc_ValidaTextoNumeroEspecialPunto() {  
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = " 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}
/*
*autor : renzo
*/
function fc_ValidaNumeroLetrasGuionOnblur(strNameObj,strMensaje){
//*****************************Onblur********************************************************
	
	var Obj = document.all[strNameObj];
	//alert(Obj.value);	
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "0123456789abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����������";
			
	strCadena = strCadena;
	
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' );
			strNameObj.value = "";
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}
/*************************************************************************************    
Descripci�n :    Permite hora formato HH:MM
Inputs		:    
Autor 		:    Rorick Paul Napa Paredes
Fecha/hora	:    25/03/2008
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaHora(strIdObj) 
{	
	//strNameObj : id de la caja de texto a validar.
	var Obj = document.all[strIdObj];
	var intEncontrado = "1234567890".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}
	//agregado 
	if (Obj.value.length == 2)
	{
		Obj.value = Obj.value + ":";
	}		
}
/*************************************************************************************
Descripci�n :    Valida si la hora de inicio es menor a la final de acuerdo a sus fechas correspondientes
Autor 		:    Rorick Napa Paredes
Fecha		:    25/03/2008
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaRangoHoraFechaOnBlur(form,fec1,fec2,strNameObj1,strNameObj2,num)
{	
	var objFec1 = document.getElementById(fec1);
	var strFec1 = objFec1.value;
	var objFec2 = document.getElementById(fec2);
	var strFec2 = objFec2.value;
	var obj1 = document.getElementById(strNameObj1);
	var strCadena1 = obj1.value;
	var obj2 = document.getElementById(strNameObj2);
	var strCadena2 = obj2.value;	
	var numHor1,numMin1,numHor2,numMin2,arr1,arr2;	
	if (strCadena1!="" & strCadena2!=""){		
		arr1=strCadena1.split(":");			
		if (arr1[0].charAt(0)=='0')
			numHor1 = parseInt(arr1[0].charAt(1));
		else
			numHor1 = parseInt(arr1[0]);
		if (arr1[1].charAt(0)=='0')
			numMin1 = parseInt(arr1[1].charAt(1));
		else
			numMin1 = parseInt(arr1[1]);
		arr2=strCadena2.split(":");	
		if (arr2[0].charAt(0)=='0')
			numHor2 = parseInt(arr2[0].charAt(1));
		else
			numHor2 = parseInt(arr2[0]);
		if (arr2[1].charAt(0)=='0')
			numMin2 = parseInt(arr2[1].charAt(1));
		else
			numMin2 = parseInt(arr2[1]);
		
		if (strFec1 == strFec2){				
		 	if((numHor1>numHor2) || (numHor1==numHor2 && numMin1>=numMin2)){
				alert("La horaInicial debe ser menor a la horaFinal");
				if (num == 1){
					obj1.value="";
					obj1.focus();
				}
				else{
					obj2.value="";
					obj2.focus();
				}
			}
		}
	}	
}
/*************************************************************************************
Descripci�n :    Valida si la hora de inicio es menor a la final
Autor 		:    Rorick Napa Paredes
Fecha		:    04/05/2008
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaRangoHora(strNameObj1,strNameObj2,num){
	var obj1 = document.getElementById(strNameObj1);
	var strCadena1 = obj1.value;
	var obj2 = document.getElementById(strNameObj2);
	var strCadena2 = obj2.value;		
		
	var numHor2,numMin2,arr1,arr2;
	var band=true;
	if (strCadena1!="" & strCadena2!=""){
				
		arr1=strCadena1.split(":");
		if (arr1[0].charAt(0)=='0')
			numHor1 = parseInt(arr1[0].charAt(1));
		else
			numHor1 = parseInt(arr1[0]);			
		
		arr2=strCadena2.split(":");	
		if (arr2[0].charAt(0)=='0')
			numHor2 = parseInt(arr2[0].charAt(1));
		else
			numHor2 = parseInt(arr2[0]);		
			
	 	if(numHor1>=numHor2){
			alert("La horaInicial debe ser menor a la horaFinal");
			band=false;			
			/*if (num == 1){
				obj1.value="";
				obj1.focus();
			}
			else{
				obj2.value="";
				obj2.focus();
			}*/			
		}
		
	}	
	return band;
}
/*************************************************************************************
Descripci�n :    
Autor 		:    Gloria Medina
Fecha		:    
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_AnioAntes(obj1, obj2, valorMsg){
	var Obj1 = document.all[obj1];
	var Obj2 = document.all[obj2];

			if (Obj1.value != ""){
				if (valorMsg == 1){
					if(fc_ValidaAnio(obj1)){
						if(Obj1.value <= Obj2.value){
							alert("El a�o de inicio del Colegio 2 debe ser mayor que el a�o fin del Colegio 1");
							Obj1.value = "";
							Obj1.focus();
						}
					}
				}
				else if (valorMsg == 2){
					strAnho = Obj1.value.split("/")[1];
					
					if (Obj2.value!="") {
						if(strAnho <= Obj2.value){
							alert("El a�o de inicio ingresado debe ser mayor que el a�o fin del Colegio");
							//Obj1.value = "";
							Obj1.focus();
						}						
					}
					else{
						var Obj3 = document.all['txtAnioFin1'];
						if(strAnho <= Obj3.value){
							alert("El a�o de inicio debe ser mayor que el a�o fin del Colegio");
							//Obj1.value = "";
							Obj1.focus();
						}
					}
				}
				else if (valorMsg == 3){
					if(fc_Trim(Obj2.value) != ""){
						strAnho1 = Obj1.value.split("/")[1];
						strAnho2 = Obj2.value.split("/")[1];
						
						if(strAnho1 < strAnho2) {
							alert("El a�o de inicio ingresado debe ser mayor que el a�o de t�rmino del Estudio 1");
							//Obj1.value = "";
							Obj1.focus();
						}
						else if(strAnho1 == strAnho2) {
							strMes1 = Obj1.value.split("/")[0];
							strMes2 = Obj2.value.split("/")[0];
							if(strMes1 <= strMes2) {
								alert("El mes de inicio ingresado debe ser mayor que el mes de t�rmino del Estudio 1");
								//Obj1.value = "";
								Obj1.focus();
							}
						}
					}
					else{
						alert("Para ingresar las fechas del Estudio 2, debe completar los datos del Estudio anterior");
						Obj1.value = "";
						Obj2.focus();
					}
				}
				else if (valorMsg == 4){
					if(fc_Trim(Obj2.value) != ""){
					
						strAnho1 = Obj1.value.split("/")[1];
						strAnho2 = Obj2.value.split("/")[1];

						if(strAnho1 < strAnho2) {
							alert("El a�o de inicio ingresado debe ser mayor que el a�o de t�rmino del Estudio 2");
							//Obj1.value = "";
							Obj1.focus();
						}
						else if(strAnho1 == strAnho2) {
							strMes1 = Obj1.value.split("/")[0];
							strMes2 = Obj2.value.split("/")[0];
							if(strMes1 <= strMes2) {
								alert("El mes de inicio ingresado debe ser mayor que el mes de t�rmino del Estudio 2");
								//Obj1.value = "";
								Obj1.focus();
							}
						}
					}
					else{
						alert("Para ingresar las fechas del Estudio 3, debe completar los datos del Estudio anterior");
						Obj1.value = "";
						Obj2.focus();
					}
				}
				else if (valorMsg == 5){
					if(fc_Trim(Obj2.value) != ""){
						strAnho1 = Obj1.value.split("/")[1];
						strAnho2 = Obj2.value.split("/")[1];
						
						if(strAnho1 < strAnho2) {
							alert("El a�o de t�rmino ingresado debe ser mayor que el a�o de inicio");
							//Obj1.value = "";
							Obj1.focus();
						}
						else if(strAnho1 == strAnho2) {
							strMes1 = Obj1.value.split("/")[0];
							strMes2 = Obj2.value.split("/")[0];
							if(strMes1 < strMes2) {
								alert("El mes de t�rmino ingresado debe ser mayor que el mes de inicio");
								//Obj1.value = "";
								Obj1.focus();
							}
						}
						
					}
					else{
						alert("Ingrese la fecha de inicio");
						Obj1.value = "";
						Obj2.focus();
					}
				}
			}
		}

/*************************************************************************************    
Descripci�n :    Permite Registrar Caracteres + ", ."
Inputs		:    
Autor 		:    Renzo Anccana Llamocca
Fecha/hora	:    25/03/2008
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
function fc_ValidaNombreAutorOnblur(strNameObj,strMensaje){
//*****************************Onblur********************************************************

	var Obj = document.all[strNameObj];
	
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����������,.0123456789'-/()";
			
	strCadena = strCadena;
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' );
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}

 /*************************************************************************************    
Descripci�n :    Permite Registrar Caracteres + ", ."
Inputs		:    
Autor 		:    Renzo Anccana Llamocca
Fecha/hora	:    25/03/2008
Empresa		:    CosapiSoft S.A.
*************************************************************************************/
  function fc_ValidaNombreAutorOnkeyPress() {  //OnkeyPress
      //� � � � � � � � � � � � �             
      if((window.event.keyCode != 209) && (window.event.keyCode != 241) && 
      (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && 
      (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && 
      (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && 
      (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
        	var ch_Caracter = String.fromCharCode(window.event.keyCode);
			var intEncontrado = "����������� abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.0123456789'-/()".indexOf(ch_Caracter);            
			if (intEncontrado == -1)
			{           
					window.event.keyCode = 0;          
			}
			else
			{
					window.event.keyCode = ch_Caracter.charCodeAt();
			}    
		}
}


function fc_ValidaNroOrdenOnblur(strNameObj,strMensaje){
//*****************************Onblur********************************************************
	var Obj = document.all[strNameObj];
	//alert(Obj.value);	
	var strCadena = new String(strNameObj.value);
	if(strCadena == "")
		return true;

	var valido = "0123456789-OoCcSs";
			
	strCadena = strCadena;
	
	for (i = 0 ; i <= strCadena.length - 1; i++)
	{	
		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
		{
			valido = strCadena.substring(i,i + 1);
			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' );
			strNameObj.focus();	
			return false;
		} 
	}	
	return true;
}

/*************************************************************************************
Autor		: Nohelia Vilchez
Descripcion : Permite validar NUMEROS Y GUION
Fecha       : 07-11-06
Empresa		: CosapiSoft S.A.
*************************************************************************************/
 function fc_ValidaNroOrden() { 
	
	var valido = "0123456789-OoCcSs";                     
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            //convierte la � en �
            window.event.keyCode = 209;
      }
      else{          
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);            
            if (intEncontrado == -1)
            {           
                 window.event.keyCode = 0;          
            }
            else
            {
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }
}
 /*************************************************************************************
 Autor		: 
 Descripcion : Valida Texto para el sistema de encuestas onkeyPress
 Fecha       : 07-11-06
 Empresa		: CosapiSoft S.A.
 *************************************************************************************/
 function fc_ValidaTextoEspecialEncuestas() {  
       //� � � � � � � � � � � � �             
       if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
         	var ch_Caracter = String.fromCharCode(window.event.keyCode);
 			var intEncontrado = " 0123456789abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ.!#%&/()=?�+-<>{}[]@:;*'�������������,".indexOf(ch_Caracter);            
 			if (intEncontrado == -1)
 			{           
 					window.event.keyCode = 0;          
 			}
 			else
 			{
 					window.event.keyCode = ch_Caracter.charCodeAt();
 			}    
 		}
 }
 
 
 /*************************************************************************************
 Autor		: 
 Descripcion : Valida Texto para el sistema de encuestas on blur
 Fecha       : 07-11-06
 Empresa		: CosapiSoft S.A.
 *************************************************************************************/
 function fc_ValidaTextoEspecialEncuestasOnblur(strNameObj){
 //*****************************Onblur********************************************************
 	var Obj = document.all[strNameObj];
 		
 	var strCadena = Obj.value;//new String(strNameObj.value);
 	if(strCadena == "")
 		return true;

 	var valido = " 0123456789abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ.!#%&/()=?�+-<>{}[]@:;*'�������������,";
 			
 	strCadena = strCadena;
 	
 	for (i = 0 ; i <= strCadena.length - 1; i++)
 	{	
 		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
 		{
 			valido = strCadena.substring(i,i + 1);
 			alert ('El texto contiene caracteres no permitidos.' );
 			Obj.value = "";
 			Obj.focus();	
 			return false;
 		} 
 	}	
 	return true;
 }	
 
 /*************************************************************************************
 Autor		: Dominguez Flores Eddy Jairo
 Descripcion : Valida Texto para el sistema de encuestas on blur
 Fecha       : 05-06-08
 Empresa		: CosapiSoft S.A.
 *************************************************************************************/
 function fc_ValidaTextoNumeroEspecialDos() {  
       //� � � � � � � � � � � � �             
       if((window.event.keyCode != 209) && (window.event.keyCode != 241) && (window.event.keyCode != 225) && (window.event.keyCode != 233) && (window.event.keyCode != 237) && (window.event.keyCode != 243) && (window.event.keyCode != 250) && (window.event.keyCode != 193) && (window.event.keyCode != 201) && (window.event.keyCode != 205) && (window.event.keyCode != 211) && (window.event.keyCode != 218) && (window.event.keyCode != 252)){            
         	var ch_Caracter = String.fromCharCode(window.event.keyCode);
 			var intEncontrado = " 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ(),/-.".indexOf(ch_Caracter);            
 			if (intEncontrado == -1)
 			{           
 					window.event.keyCode = 0;          
 			}
 			else
 			{
 					window.event.keyCode = ch_Caracter.charCodeAt();
 			}    
 		}
 }
 
 function fc_ValidaNumeroLetrasGuionOnblurDos(strNameObj,strMensaje){
 //*****************************Onblur********************************************************
 	var Obj = document.all[strNameObj];
 	//alert(Obj.value);	
 	var strCadena = new String(strNameObj.value);
 	if(strCadena == "")
 		return true;

 	var valido = "0123456789abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����������(),/-.";
 			
 	strCadena = strCadena;
 	
 	for (i = 0 ; i <= strCadena.length - 1; i++)
 	{	
 		if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
 		{
 			valido = strCadena.substring(i,i + 1);
 			alert ('El Campo ' + strMensaje + ' contiene caracteres no permitidos.' );
 			strNameObj.value = "";
 			strNameObj.focus();	
 			return false;
 		} 
 	}	
 	return true;
 }
 function fc_ValidaDecimalFinal(strCad){
		var strCadena = new String(strCad);
		if(strCad == "")
			return true;
				
		var valido = "1234567890.";
				
		strCadena = strCadena;
		for (i = 0 ; i <= strCadena.length - 1; i++)
		{	
			if (valido.indexOf (strCadena.substring(i,i+1),0) == -1)
			{
				valido = strCadena.substring(i,i + 1);
				return false;
			} 
		}	
		return true;
	}
 