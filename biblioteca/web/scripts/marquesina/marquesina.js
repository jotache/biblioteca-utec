var TickerDW = new Class({
	indice: 0,
	contenedoresLlenos: 0,
	contenedorActual: 0,
	identificadorRetardo: null,	
	//constructor
	//opcion classElemento (la class de css del elemento, por defecto "elementoticker")
	//opcion retardo (numero de milisegundos con el que configurar el retardo defecto: "3000")
	//opcion classContenedorElemento (la clase css del contenedor del elemento defecto "contelementoticker")
	//opcion numElementos (el numero de elementos qe se ven a la vez defecto 3)
	initialize: function(elementos, destino, opciones){		
		this.ticker = $(destino);
		this.arrayElementos = elementos;
		if ($defined(opciones.classElemento)){
			this.classElemento = opciones.classElemento;
		}else{
			this.classElemento = "elementoticker";
		}		
		if ($defined(opciones.classContenedorElemento)){
			this.classContenedorElemento = opciones.classContenedorElemento;
		}else{
			this.classContenedorElemento = "contelementoticker";
		}		
		if ($defined(opciones.retardo)){
			this.retardo = opciones.retardo;
		}else{
			this.retardo = 3000;
		}
		if ($defined(opciones.numElementos)){
			this.numElementos = opciones.numElementos;
		}else{
			this.numElementos = 3;
		}		
		this.contenedores = new Array();		
		for (i=0; i < this.numElementos; i++){			
			this.contenedores.include(new Element ("div", {
				"class": this.classContenedorElemento
			}));			
			this.contenedores[i].inject(this.ticker);
			
		}
		
		if (this.arrayElementos.length > 0){
			//comienzo la animaci�n
			this.rotaTextos();
		}
		
	},
	crearElemento: function(){
		var nuevoTitulo = new Element ("A", {
			"href": this.arrayElementos[this.indice].url,
			"html": this.arrayElementos[this.indice].titulo
		});
		var nuevoElemento = new Element ("div", {
			"class": this.classElemento
		});
		var nuevoTexto = new Element ("span", {
			"html": this.arrayElementos[this.indice].intro
		});
		nuevoTitulo.inject(nuevoElemento);
		nuevoTexto.inject(nuevoElemento);
		nuevoElemento.addEvent("mouseover", function(){
			$clear(this.identificadorRetardo);
		}.bind(this));
		nuevoElemento.addEvent("mouseout", function(){
			this.identificadorRetardo = this.rotaTextos.delay((this.retardo / 3).toInt(), this);
		}.bind(this));
		nuevoElemento.set("opacity", 0);
		return nuevoElemento;
	},
	
	muestraNoticia: function(){
		//vamso a ver donde lo meto
		if (this.contenedoresLlenos < this.numElementos){
			//aun no se han llenado contenedores iniciales
			var nuevoElemento = this.crearElemento();
			nuevoElemento.inject(this.contenedores[this.indice]);
			nuevoElemento.fade("in")
			this.contenedoresLlenos++;
			this.indice = (this.indice + 1) % this.arrayElementos.length;
		}else{
			//est�n llenos los contenedores iniciales
			//hago espacio
			var myFx1 = new Fx.Tween(this.contenedores[this.contenedorActual]);
			myFx1.start("height", 0);
			this.contenedores[this.contenedorActual].tween("margin-top", 0);
			this.contenedorEnCola.delay(800, this);
		}
		//alert(this.arrayElementos[this.indice].titulo);
	},
	
	contenedorEnCola: function(){
		this.contenedores[this.contenedorActual].destroy();
		this.contenedores[this.contenedorActual] = new Element ("div", {
			"class": this.classContenedorElemento
		});
		var nuevoElemento = this.crearElemento();
		nuevoElemento.inject(this.contenedores[this.contenedorActual]);
		this.contenedores[this.contenedorActual].inject(this.ticker);
		nuevoElemento.fade("in")
		this.contenedorActual = (this.contenedorActual + 1) % this.contenedores.length;
		this.indice = (this.indice + 1) % this.arrayElementos.length;
	},
	
	rotaTextos: function(){
		this.muestraNoticia();
		var actRetardo;
		if (this.contenedoresLlenos < this.numElementos){	
			actRetardo = (this.retardo/10).toInt();
		}else{
			actRetardo = this.retardo;
		}
		this.identificadorRetardo = this.rotaTextos.delay(actRetardo, this);
	}
});

//para traer un contenido y meterlo en la pagina con ajax
//enlace=id.del.enlace.que.carga.ajax archivo=ruta.del.contenido destino=capaDondeMeterlo suceso=funcion-ejecutar-despues cerrar=boleano para mostrar o no un enlace de cerrar.
var contenidoAjax = new Class({
	initialize: function(enlace, archivo, destino, funcionSuceso, cerrar){
		this.url = archivo;
		this.destino = $(destino);
		this.opcionCerrar = cerrar;
		this.enlace = $(enlace);
		this.mostrando = false;
		this.resaltando = false;
		this.funcionSuceso = funcionSuceso;
		this.enlace.addEvent("click", function(eventoe){
			eventoe.stop();
			this.abreContenido();
		}.bind(this));
	},
	abreContenido: function(){
		if (!this.mostrando){
			this.destino.set("styles",{"display": "block"});
			this.destino.addClass("cargandocontenido");
			this.contenidoRequerido = new Request.HTML({
				"url": this.url,
				"update": this.destino,
				onSuccess: function(){
					this.funcionSuceso();
					this.destino.removeClass("cargandocontenido");
					if(this.opcionCerrar){
						iconoCerrar = new Element("A",{
							"class": "cerrarajax", 
							"html": "Cerrar <img align='absmiddle' src='/images/iconos/cross.png' width=16 height=16 border=0 />",
							"href": "#"
						});
						iconoCerrar.addEvent("click", function(evento){
							evento.stop();
							this.destino.empty();
							this.destino.set("styles",{"display": "none"});
							this.mostrando=false;
						}.bind(this));
						capaCerrar = new Element("div",{
							"class": "ccerrarajax"
						});
						iconoCerrar.inject(capaCerrar);
						capaCerrar.inject(this.destino, "top");
					}
					this.mostrando = true;
				}.bind(this)
			}).send();
		}else{
			if(!this.resaltando){
				this.resaltando = true;
				var myFx = new Fx.Tween(this.destino);
				myFx.start('background-color', '#F5F5F5', '#bbbbbb');
				myFx.start.delay(1000,myFx,['background-color', '#bbbbbb', '#F5F5F5']);
				this.noResaltando.delay(2000, this);
			}
		}
	},
	noResaltando: function(){
		this.resaltando = false;
	}
});
//un contenido Ajax que debe abrir un formulario que est� en otro lugar
//enlace (id del enlace a activar) , ajaxAsoc es un objeto contenidoAjax al que queremos asociar este elemento.
var ContenidoAjaxAsociado = new Class({
	initialize: function(enlace, ajaxAsoc){
		this.ajaxAsoc = ajaxAsoc;
		this.enlace = $(enlace);
		this.enlace.addEvent("click", function(ev){
			ev.stop();
			var posAsoc = this.ajaxAsoc.enlace.getPosition();
			window.scrollTo(posAsoc.x, posAsoc.y);
			this.ajaxAsoc.abreContenido();
		}.bind(this));
	}
});

