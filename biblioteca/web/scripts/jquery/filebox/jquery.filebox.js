/*
 * Creado por ebc erick benites erickpm.
 * Generador de sprites.png: http://preloaders.net/en/popular (escoger APNG con javascript y solo considerar el sprites.png) 
 */
(function($) {
				    
    $.fn.fileBox = function(options) {
                
        /* TODO: This should not override CSS. */
        var settings = {
        	text : 'Examinar...',
            width : 180,
            inline: false
        };
                
        if(options) {
            $.extend(settings, options);
        };
                        
        return this.each(function() {
            
        	var input = $(this).css({
				 'position': 'absolute',
				 'top': 0,
				 'right': 0,
				 'margin': 0,
				 'font-size': '70px', 
				 'opacity': 0,
				 'cursor': 'pointer'
			 });
			 
			 $('<span/>').css({'width': settings.width+'px', 'overflow': 'hidden'}).addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only').append('<span/>').find('span').addClass('ui-button-text').text(settings.text).end().insertAfter(input).append(input);
			 
			 $(input).hover(function(){
				$(this).parent().addClass('ui-state-hover'); 
			 }, function(){
				$(this).parent().removeClass('ui-state-hover'); 
			 });
			 
			 $(input).change(function (){
		       var fileName = $(this).val();
		       fileName = fileName.substring(fileName.lastIndexOf('\\')+1);
		       if(settings.inline)
		       	$(this).parent().find('.ui-button-text').css({'overflow': 'hidden', 'white-space': 'nowrap'}).text(fileName);
		       else
		       	$('<label/>').css({'margin-right': '10px', 'font-weight': 'bold'}).text(fileName).insertAfter($(this).parent());
		     });
        					      
        });
        

    };
    
})(jQuery);

$(function(){
	//How to use:
	//$('#fileupload').fileBox({width: 180, inline: true}); //inline: indica si el nombre del archivo estará dentro del botón o al lado
});