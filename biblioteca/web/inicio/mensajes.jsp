<%-- Success Messages --%>
<c:if test="${not empty message}">
    <div class="message">${message}</div>
    <c:remove var="message" scope="session"/>
</c:if>