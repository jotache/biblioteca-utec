<%@ include file="/taglibs.jsp"%>
<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){}
		
		function fc_Regresar()
		{
			window.location.href = "${ctx}/evaluaciones/bandeja_alumnos_tutor.html" +
								"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
								"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value;
		}
		
		function fc_selCalificacion(objRadio, indice, codCalificacion)
		{
			if ( fc_Trim(document.getElementById("txhTipoCalificacion").value) != "1" )
				if ( fc_Trim(document.getElementById("txhCodCalIni" + indice).value) == "" &&
					document.getElementById("txhCodCal" + indice).value == "")
				{
					if ( !confirm("La competencia no cuenta con una evaluación inicial. Seguro de Evaluar?") )
					{
						objRadio.checked = false;
						return false;
					}
				}
			document.getElementById("txhCodCal" + indice).value = codCalificacion;
		}

		function fc_grabar()
		{
			var cantNull = 0;
			var i = 1;
			var numSel = 0;
			cadCodCompetencias = "";
			cadCodCalificaciones = "";
			
			while (true)
			{
				objCodCompetencia = document.getElementById("txhCodComp" + i);
				objCodCalificacion = document.getElementById("txhCodCal" + i);
				
				if ( objCodCompetencia == null )
				{ 
					cantNull++;
					if ( cantNull > 1 ) break;
					i++;
					continue;					
				}
				cantNull = 0;
				cadCodCompetencias = cadCodCompetencias + objCodCompetencia.value + "|";
				
				if ( fc_Trim(objCodCalificacion.value) != "" )
				{
					numSel++;
					cadCodCalificaciones = cadCodCalificaciones + objCodCalificacion.value + "|";
				}
				else cadCodCalificaciones = cadCodCalificaciones + "0|";
				
				i++;
			}
			
			if ( numSel == 0 ) 
			{
				alert('Debe seleccionar al menos la evaluación de una competencia.');
				return false;
			}
						
			$('txhCadCodCompetencias').value = cadCodCompetencias;
			$('txhCadCodEvaluaciones').value = cadCodCalificaciones;
			
			if ( !confirm(mstrSeguroGrabar)) return false;
						
			$('txhAccion').value='GRABAR';
			$('frmMain').submit();
		}
	</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/registro_alumnos_tutor.html" >
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="indTipo" id="txhTipoCalificacion"/>
	<form:hidden path="cadCodCompetencia" id="txhCadCodCompetencias"/>
	<form:hidden path="cadCodRespuesta" id="txhCadCodEvaluaciones"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="#eae9e9" width="97%" style="margin-left:9px">
	<tr>
			<td>
				<table class="borde_tabla" style="width:97%;margin-top:6px" cellspacing="0" cellpadding="0" >
					<tr class="fondo_cabecera_azul">		 
					 	<td class="titulo_cabecera" width="100%" colspan="4">
							Registrar Perfil
						</td>		 	
					</tr>
					<tr class="fondo_dato_celeste">
						<td width="10%">&nbsp;Alumno :</td>
						<td width="80%">
							<form:input path="dscAlumno" id="txtDscAlumno" cssClass="cajatexto_1" 
									cssStyle="width:200px;" readonly="true"/>
						</td>
						<td colspan=2 width="10%">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><div style="OVERFLOW:auto; WIDTH: 100%; HEIGHT: 390px; display: ;" id="tblAlumnos">
				<display:table name="sessionScope.listPerfilesByAlumno" cellpadding="0" cellspacing="1"
					class="" style="width:97%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA"
					decorator="com.tecsup.SGA.bean.PerfilDecorator">
					<display:column property="dscCompetencia" title="Competencia" style="width:40%;text-align:left"/>
					<display:column property="rbtExcelente" title="Excelente<br/>(4)<br/>${control.dscCalificacion}" style="width:15%;text-align:center"/>
					<display:column property="rbtBueno" title="Bueno<br/>(3)<br/>${control.dscCalificacion}" style="width:15%;text-align:center"/>
					<display:column property="rbtMedia" title="Media<br/>(2)<br/>${control.dscCalificacion}" style="width:15%;text-align:center"/>
					<display:column property="rbtBaja" title="Baja<br/>(1)<br/>${control.dscCalificacion}" style="width:15%;text-align:center"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
				<% request.getSession().removeAttribute("listPerfilesByAlumno"); %>
			</div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN" align="center" width="98%">	
		<tr>
			<td align="center">			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">							
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer" onclick="javascript:fc_grabar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" style="cursor:pointer" onclick="javascript:fc_Regresar();"></a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript" language="javascript">
	var strMsg = fc_Trim(document.getElementById("txhMsg").value);
	if ( strMsg != "" )
	{
		if ( strMsg == "ERROR" )alert(mstrProblemaGrabar);
		else if ( strMsg == "OK" )alert(mstrSeGraboConExito);
		else if ( strMsg == "-99" ){
			alert('Debe ingresar el peso de las calificaciones. Consulte con el administrador.');
			fc_Regresar();
		}
	}
	document.getElementById("txhAccion").value = "";
	document.getElementById("txhMsg").value = "";
</script>