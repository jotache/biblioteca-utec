<%@ include file="/taglibs.jsp"%>
<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%	if ( request.getSession().getAttribute("usuarioSeguridad") == null ) { %>
<script type="text/javascript">	
	alert("Su sesi�n ha terminado, favor de volver a ingresar al sistema.");
	top.parent.location.href = "/SGA/logeoEvaluacion.html";	
</script>
<% } %>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script type="text/javascript" type="text/javascript">
		function onLoad(){}

		function fc_Exportar(){
			var u = "REPORTE_EVA_PERFILESALM";							
			url="?opcion=" + u +
				"&periodo=" + document.getElementById("txtPeriodo").value +
				"&nomEval=" + document.getElementById("txtEvaluador").value +
				"&codPeriodo=" + document.getElementById("txhCodPeriodo").value +
				"&nombre=" + document.getElementById("txtNombre").value +
				"&usuario=" + document.getElementById("txhCodEvaluador").value+
				"&apellido=" + document.getElementById("txtApellido").value;
				window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"Reportes","resizable=yes, menubar=yes");	
		}
		
		function fc_SelAlumno(codAlumno, nomAlumno){		
			$('txhCodAlumno').value = codAlumno;
			$('txhNomAlumno').value = nomAlumno;
		}
		
		function fc_Buscar() {		
			$('txhAccion').value = 'BUSCAR';
			$('frmMain').submit();
		}
		
		function fc_Regresar(){
			location.href =  "${ctx}/menuEvaluaciones.html";
		}
		
		function fc_RegistrarIni(){
			if ($('txhCodAlumno').value == '' ){			
				alert(mstrSeleccione)
				return false;
			}
			
			location.href = "${ctx}/evaluaciones/registro_alumnos_tutor.html" +
				"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value +
				"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value +
				"&txhCodAlumno=" + document.getElementById('txhCodAlumno').value +
				"&txhNomAlumno=" + document.getElementById('txhNomAlumno').value +
				"&txhTipoOpe=1";
		}

		function fc_RegistrarFin(){		
			if ($('txhCodAlumno').value == ''){
				alert(mstrSeleccione)
				return false;
			}
			
			location.href = "${ctx}/evaluaciones/registro_alumnos_tutor.html" +
				"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value +
				"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value +
				"&txhCodAlumno=" + document.getElementById('txhCodAlumno').value +
				"&txhNomAlumno=" + document.getElementById('txhNomAlumno').value +
				"&txhTipoOpe=2";
		}
		
		function fc_Limpiar(){
			$('txtNombre').value = '';
			$('txtApellido').value = '';
		}
		
		function fc_DetalleAlumno(){		
			if ( $('txhCodAlumno').value == '' ){
				alert(mstrSeleccione)
				return false;
			}
			
			url = "${ctx}/evaluaciones/consultarEvaluaciones.html" +
			      "?codAlu=" + document.getElementById('txhCodAlumno').value +
			      "&txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
			      "&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value +
			      "&txhIndProcTutor=1" + 
			      "&txhCodPeriodoTutor=" + document.getElementById('txhCodPeriodo').value;
			window.location.href = url;
		}
	</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/bandeja_alumnos_tutor.html" >
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="nomAlumno" id="txhNomAlumno"/>
	
	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>

	<table class="borde_tabla" style="width:97%;margin-top:6px;margin-left:9px" cellspacing="0" cellpadding="0" >

		<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%" colspan="4">
				Alumnos asignados al Tutor
			</td>		 	
		</tr>
		<tr class="fondo_dato_celeste">
			<td >&nbsp;Tutor(Evaluador) :</td>
			<td >
				<form:input id="txtEvaluador" path="dscEvaluador" cssClass="cajatexto_1" 
					readonly="true" cssStyle="width:140px"/>
			</td>
			<td >&nbsp;Periodo :</td>
			<td >
				<form:input path="dscPeriodo" id="txtPeriodo" cssClass="cajatexto_1" cssStyle="width:70px"
					readonly="true" />
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td >&nbsp;Apell. Paterno :</td>
			<td >
				<form:input path="apellido" id="txtApellido" cssClass="cajatexto" cssStyle="width:140px" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Apellido');"/>
			</td>
			<td >&nbsp;Nombre :</td>
			<td>
				<form:input path="nombre" id="txtNombre" cssClass="cajatexto" cssStyle="width:140px"
					onkeypress="javascript:fc_ValidaTextoEspecial();" 
					onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Nombre');"/>&nbsp;&nbsp;&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" align="middle" alt="Buscar" 
					style="cursor: pointer;" onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
					 style="cursor: pointer;" onclick="javascript:fc_Limpiar();" id="imgLimpiar">
				</a>
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExportar','','${ctx}/images/iconos/exportarex2.jpg',1)">
					<img src="${ctx}/images/iconos/exportarex1.jpg" alt="Exportar Excel" align="middle" 
					 	style="cursor: pointer;" onclick="javascript:fc_Exportar();" id="imgExportar">
				</a>
			</td>
		</tr>
	</table>
		<div style="OVERFLOW:auto; WIDTH: 100%; height: 400px;">					
		<table style="width: 97%;margin-left:5px;margin-top:6px;border: 1px solid #048BBA;" cellSpacing="1" 
			cellPadding="0" align="left" >
			<tr height="20px">
				<td class="headtabla" width="3%">&nbsp;Sel.</td>
				<td class="headtabla" width="5%" align="center">&nbsp;C�digo</td>
				<td class="headtabla" width="20%">&nbsp;Alumno</td>
				<c:forEach var="objDetalle" items="${control.listaCabecera}" varStatus="loop" >
					<td class="headtabla" align="center" width="8%">
						<c:out value="${objDetalle.descripcion}" /><br/>
						Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fin
					</td>
				</c:forEach>
				<td class="headtabla" align="center" width="${control.anchoColumna}">
					Promedio<br/>
					Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fin
				</td>
			</tr>
			
			<c:forEach var="objEvalComp" items="${control.listaBandeja}" varStatus="loop" >
				<c:choose>
					<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
					<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
				</c:choose>				
						
					<td align="center" >
						<input type="radio" ID="chkCodAlumno" name="gpAlumnos" 
							onclick="fc_SelAlumno('<c:out value="${objEvalComp.codAlumno}" />','<c:out value="${objEvalComp.nomAlumno}" />');">
					</td>
					<td align="center" ><c:out value="${objEvalComp.codAlumno}" /></td>
					<td style="text-align: left;">&nbsp;<c:out value="${objEvalComp.nomAlumno}" /></td>
					<c:forEach var="objCompAlum" items="${objEvalComp.listaCompetencias}" varStatus="loop" >
						<td align="center" >

							<!-- -->
							<input name="color" value="<c:out value="${objCompAlum.notaIni}" />" class="cajatexto" 
								style="width:30px; text-align: right" disabled="disabled">
 							
								&nbsp;&nbsp;
							<!--  -->							
							<input name="color" value="<c:out value="${objCompAlum.notaFin}" />" class="cajatexto" 
								style="width:30px; text-align: right" disabled="disabled">
														

						</td>
					</c:forEach>
				</tr>
			</c:forEach>
		</table>
	</div>
			
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN" align="center" height="30px" style="margin-top:6px">	
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegini','','${ctx}/images/botones/reginicial2.jpg',1)">
				<img alt="Registro Inicial" onclick="javascript:fc_RegistrarIni();" id="imgRegini" src="${ctx}/images/botones/reginicial1.jpg" style="cursor: pointer;"/></a>										
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegfin','','${ctx}/images/botones/registrofin2.jpg',1)">
				<img alt="Registro Final" onclick="javascript:fc_RegistrarFin();" id="imgRegfin" src="${ctx}/images/botones/registrofin1.jpg" style="cursor: pointer;"/></a>

				<!-- Detalle Alumno -->
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDetaAlum','','${ctx}/images/botones/detalleal2.jpg',1)">
				<img alt="Detalle Alumno" onclick="javascript:fc_DetalleAlumno();" id="imgDetaAlum" src="${ctx}/images/botones/detalleal1.jpg" style="cursor: pointer;"/></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" onclick="javascript:fc_Regresar();" id="imgRegresar" src="${ctx}/images/botones/regresar1.jpg" style="cursor: pointer;"/></a>
			</td>
		</tr>
	</table>
</form:form>