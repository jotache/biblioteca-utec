<%@ include file="/taglibs.jsp"%>
<html>
	<head>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>	
	<script language=javascript>	
	function onLoad()
	{
		objMsg = document.getElementById("txhMessage");
		if ( objMsg.value == "OK" )
		
		{	objMsg.value = "";
		
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		else if( objMsg.value == "DOBLE" ){
		objMsg.value = "";
		alert('Ya existe un registro no eliminable para este tipo de evaluacion parcial.');
		}
		}
		function fc_Grabar(){
			if(!document.getElementById("txtDscProceso").value == ""){
				if(!document.getElementById("cboDocente").value == ""){
				if(document.getElementById("txtDscProceso").value > 0){
				     if(document.getElementById("txtDscProceso").value < 11){
					document.getElementById("txhOperacion").value = "GRABAR";
					document.getElementById("frmMain").submit();
					return;
						}
						else {
						alert('El peso debe ser menor a 10');
						}
						}
					else {
					alert('El peso debe ser menor a 10');
					return;
					}
			}
			else{
			alert('Debe seleccionar un evaluador');
			return;
			}
			}
		
			else if(document.getElementById("txtDscProceso").value == ""){
			alert('Debe ingresar un peso');
			return;
			}
			
			else if(!document.getElementById("cboDocente").value == ""){
				if(!document.getElementById("txtDscProceso").value == ""){
				if(document.getElementById("txtDscProceso").value > 0){
					 if(document.getElementById("txtDscProceso").value < 11){
				document.getElementById("txhOperacion").value = "GRABAR";
				document.getElementById("frmMain").submit();
				return;
				}
				}
				else {
				alert('El peso debe ser mayor a cero');
				return;
				}
			}
			else{
			alert('Se deben llenar todos los parametros obligatorios');
			return;
			}
			}
			else if(document.getElementById("cboDocente").value == ""){
			alert('Debe ingresar un peso');
			return;
			}
			document.getElementById("txhFlag").value = "";
		}
		function fc_Valor(strcodEvaluador,strDscEvaluador){
		}
		function fc_muestra(){
		}

	function fc_Flag(){
		if(document.getElementById("txhFlag").value =="")
			document.getElementById("txhFlag").value = "1";
		else
			document.getElementById("txhFlag").value = "";
		
	}
	</script>
   	<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_agregar_peso.html">
		<form:hidden path="evaluador" id="txhEvaluador"/>
		<form:hidden path="message" id="txhMessage"/>	
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="curso" id="txhCurso"/>
		<form:hidden path="codCurso" id="txhCodCurso"/>
		<form:hidden path="flag" id="txhFlag"/>
		<form:hidden path="seccion" id="txhSeccion"/>
		<form:hidden path="codId" id="txhCodId"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEval" id="txhCodEval"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<form:hidden path="codProducto" id="txhCodProducto"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="550px" class="opc_combo"><font style="">Evaluaci�n Parcial</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	 </table>
		<table class="" style="width:98%;margin-top:5px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray' align="center"> 
			<tr>
				<td valign='top' colspan="2">
					<table class="tablaflotante"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="30px"> 
						<tr>
							<td nowrap class="" width="30%">Peso :</td>			
							<td>
							<form:input path="descripcion" id="txtDscProceso" 
								onkeypress="fc_PermiteNumeros();"
								onblur="fc_ValidaNumeroOnBlur('txtDscProceso');" 
								cssStyle="text-align:right"
								cssClass="cajatexto_o" size="10" maxlength="4"/>
							</td>
						</tr>
						<tr>
							<td class="" >Evaluador Responsable:&nbsp;&nbsp;</td>
							<td>
							<form:select  path="codEvaluador" id="cboDocente" cssClass="cajatexto" 
								cssStyle="width:180px" onchange="javascript:fc_muestra();">
							        <form:option value="">--Seleccione--</form:option>
						      	    <c:if test="${control.listEvaluadores!=null}">
						        	<form:options itemValue="codEvaluador" itemLabel="dscEvaluador" 
						        		items="${control.listEvaluadores}" />						        
						            </c:if>
	 				        </form:select>
							</td>
						</tr>
						<td>
				     	<input type="checkbox" id="com" name="com" disabled="disabled" 
							<c:if test="${control.codCurso=='2325' || control.codCurso=='2326'}"> 
								 checked 
							</c:if>							
						 	<%--if("1".equalsIgnoreCase((String)request.getAttribute("flag"))){ %> checked<%} --%> 		
							<c:if test="${control.codCurso=='2324'}">
								onclick="javascript:fc_Flag();"
							</c:if>

								/>
						</td>
						<td>
						No se podr� eliminar.&nbsp;&nbsp;</td>
				   </table>
				</td>
			</tr>
		</table>	
		<br>
		<table align="center">
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="fc_Grabar();" style="cursor:pointer;"></a></td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
			</tr>
		</table>	
	</form:form>
	<script language=javascript>
	  if(document.getElementById("txhCodCurso").value !="2324"){
		  //Si es laboratorio o taller siempre permanecer marcado.		
			document.getElementById("txhFlag").value = "1";		
	  }
	</script>
</html>
