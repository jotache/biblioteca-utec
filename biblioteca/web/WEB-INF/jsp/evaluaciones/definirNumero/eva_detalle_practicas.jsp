<%@ include file="/taglibs.jsp"%>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
		function onLoad()
		{
			document.getElementById("txhOperacion").value = "";
			document.getElementById("txhUsuario").value = "";
			cantidad = document.getElementById("txhCantidad").value;
			minimo = document.getElementById("txhMinimo").value;
			if(cantidad <= minimo){
				document.getElementById("lblMinimo").style.color = "red";
			}
	
			document.getElementById("txhFlagEliminar").value = "";
			objMsg = document.getElementById("txhMessage");
	
			if ( objMsg.value == "OK" )
			{
				objMsg.value = "";
				alert(mstrElimino);				
			} else if ( objMsg.value == "ERROR" )
			{
				objMsg.value = "";
				alert(mstrProbEliminar);			
				}
			else if(objMsg.value == "NO"){
				objMsg.value = "";
				alert('No se puede eliminar ya que la evaluaci�n se a realizado');
				document.getElementById("txhNumero").value = "";
				}
				document.getElementById("txhMessage").value = "";
			}
			
		function Fc_Agregar(){
		
			if (document.getElementById("cboSeccion").value==""){
				alert('Seleccione una Secci�n');
				return false;
			}		
		
			if ( document.getElementById("cboEvaluacion").value != "")
			{ 
			document.getElementById("txhCodigo").value = document.getElementById("cboEvaluacion").value;
			document.getElementById("txhSec").value = document.getElementById("cboSeccion").value;
			strCodCurso = document.getElementById("txhCodCurso").value;
			strCodigo = document.getElementById("txhCodigo").value;
			strSeccion = document.getElementById("txhSec").value;
			strCodPeriodo = document.getElementById("txhCodPeriodo").value;
			strcodEval = document.getElementById("txhCodEvaluador").value;
			strcodPro = document.getElementById("txhCodProducto").value;
			strcodEspe = document.getElementById("txhCodEspecialidad").value;
			strCodId = "";
			strCodEva = "";
			strPeso = "";

			Fc_Popup("${ctx}/evaluaciones/eva_agregar_peso.html?txhCodigo=" + strCodigo + "&txhCodCurso=" + strCodCurso + "&txhSec=" + strSeccion + "&txhCodPeriodo=" + strCodPeriodo + "&txhCodId=" + strCodId + "&txhPeso=" + strPeso + "&txhCodEvaluador=" + strCodEva + "&txhCodEval=" + strcodEval + "&txhCodProducto=" + strcodPro + "&txhCodEspecialidad=" + strcodEspe
				,420,180);
				}
				else{
				alert('Debe seleccionar una evaluacion parcial');
				}
			}
	
		function fc_Regresar(){
			window.location.href = "${ctx}/evaluaciones/bandejaEvaluador.html" +
									"?txhCodPeriodo=" + document.getElementById("txhCodPeriodo").value + 
									"&txhCodEvaluador=" + document.getElementById("txhCod").value;	
		}
		
	function Fc_Eliminar()
	{
	
		if (document.getElementById("cboSeccion").value==""){
			alert('Seleccione una Secci�n');
			return false;
		}		
	
		strca = document.getElementById("txhCantidad").value;
		strtot = document.getElementById("txhCant").value;
		strmin = document.getElementById("txhMinimo").value;
		strre = strca - strtot;
		
		if(strre >= strmin){	
			if ( fc_Trim(document.getElementById("txhNumero").value) == ""){			
				alert(mstrSeleccione);
				return false;
			}
			eli = document.getElementById("txhFlagEliminar").value;
			//alert(eli);
			eli = eli.replace("|","");
			document.getElementById("txhFlagEliminar").value = fc_Trim(eli);
		
			//if(document.getElementById("txhFlagEliminar").value == "SI"){
				if( confirm(mstrEliminar) ){
					document.getElementById("txhOperacion").value = "ELIMINAR";
					document.getElementById("frmMain").submit();
				}
			//}else{
				//alert(mstrNoElim);
			//}
			
		}else{
			alert('El m�nimo de evaluaciones parciales es: '  + strmin);
		}
	}

	function fc_muestra()
	{	
		document.getElementById("txhCodigo").value = document.getElementById("cboEvaluacion").value
		document.getElementById("txhOperacion").value = "DETALLE";
		document.getElementById("frmMain").submit();
	}

	function fc_muestra1()
	{	
		document.getElementById("txhCodigo").value = document.getElementById("cboEvaluacion").value
		document.getElementById("txhOperacion").value = "DETALLE1";
		document.getElementById("frmMain").submit();
	}
	
	function fc_Valor(strCodigo,strDescripcion,strFlag){
	}
	
	function fc_seleccionarRegistro(strCodId, strCodEvaluador, strFalg, strPeso, strFlagEliminar){
		
		strPes = document.getElementById("txhPeso").value;
		strCodEva = document.getElementById("txhUsuario").value;
		strFla = document.getElementById("txhFlag").value;
		strCodSel = document.getElementById("txhNumero").value;
		strCant = document.getElementById("txhCant").value;
		strFlaEli = document.getElementById("txhFlagEliminar").value;
		flag=false;
		if (strCodSel!='')
		{
			ArrCodSel = strCodSel.split("|");
			arrPeso   = strPes.split("|");
			arrCodEva = strCodEva.split("|");
			arrFla   = strFla.split("|");
			arrFlaEli = strFlaEli.split("|");
			strPes = "";
			strCodEva= "";
			strFla = "";
			strCodSel = "";
			strFlaEli = "";		
			for (i=0;i<=ArrCodSel.length-2;i++)
			{
				if (ArrCodSel[i] == strCodId){ 
				flag = true; 
				}
				else{
				strCodSel = strCodSel + ArrCodSel[i]+'|';
				strPes = strPes + arrPeso[i]+'|';
				strCodEva = strCodEva + arrCodEva[i]+'|';
				strFla = strFla + arrFla[i]+'|';
				strFlaEli = strFlaEli + arrFlaEli[i]+'|';
				}
			}
		}
		if (!flag)
		{
			strCodSel = strCodSel + strCodId + '|';
			strPes = strPes + strPeso + "|";
			strCodEva = strCodEva + strCodEvaluador + "|";
			strFla = strFla + strFalg + "|";
			strFlaEli = strFlaEli + strFlagEliminar + "|";
		}
		
		document.getElementById("txhCodId").value = strCodSel;
		document.getElementById("txhNumero").value = strCodSel;
		document.getElementById("txhPeso").value = strPes;
		document.getElementById("txhUsuario").value = strCodEva;
		document.getElementById("txhFlag").value = strFla;
		document.getElementById("txhFlagEliminar").value = strFlaEli;
		
		fc_GetNumeroSeleccionados();
	}

function fc_GetNumeroSeleccionados()
	{
		strCodSel = document.getElementById("txhNumero").value;
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			document.getElementById("txhCant").value = ArrCodSel.length - 1;
		}
		else{ 
		document.getElementById("txhCant").value = 0;
		}
	}

function Fc_Modificar(){

	if (document.getElementById("cboSeccion").value==""){
		alert('Seleccione una Secci�n');
		return false;
	}		

	var  intcant = document.getElementById("txhCant").value;
	if(intcant == 1)
	{	
		pes = document.getElementById("txhPeso").value;
		pes = pes.replace("|","");
		document.getElementById("txhPeso").value = fc_Trim(pes);
		
		codeva = document.getElementById("txhUsuario").value;
		codeva = codeva.replace("|","");
		document.getElementById("txhUsuario").value = fc_Trim(codeva);
		
		cod = document.getElementById("txhCodId").value;
		cod = cod.replace("|","");
		document.getElementById("txhCodId").value = fc_Trim(cod);
		
		flag = document.getElementById("txhFlagEliminar").value;
		flag = flag.replace("|","");
		document.getElementById("txhFlagEliminar").value = fc_Trim(flag);
		
		if(document.getElementById("txhFlagEliminar").value=="SI"){
		document.getElementById("txhFlagEliminar").value = "0";
		}
		else{
		document.getElementById("txhFlagEliminar").value = "1";
		}
		
		if ( document.getElementById("txhCodId").value != "")
		{ 
		  	document.getElementById("txhCodigo").value = document.getElementById("cboEvaluacion").value;
			document.getElementById("txhSec").value = document.getElementById("cboSeccion").value;
			strCodCurso = document.getElementById("txhCodCurso").value;
			strCodigo = document.getElementById("txhCodigo").value;
			strSeccion = document.getElementById("txhSec").value;
			strCodPeriodo = document.getElementById("txhCodPeriodo").value;
			strCodId = document.getElementById("txhCodId").value;
			strcodEval = document.getElementById("txhCodEvaluador").value;			
			strPeso = document.getElementById("txhPeso").value;
			strFlag = document.getElementById("txhFlagEliminar").value;
			strcodPro = document.getElementById("txhCodProducto").value;
		    strcodEspe = document.getElementById("txhCodEspecialidad").value;


		  Fc_Popup("${ctx}/evaluaciones/eva_agregar_peso.html?txhCodigo=" + strCodigo + "&txhCodCurso=" + strCodCurso + "&txhSec=" + strSeccion + "&txhCodPeriodo=" + strCodPeriodo + "&txhCodId=" + strCodId + "&txhPeso=" + strPeso + "&txhCodEvaluador=" + strcodEval + "&txhFlagEliminar=" + strFlag + "&txhCodProducto=" + strcodPro + "&txhCodEspecialidad=" + strcodEspe + "&txhCod=" + document.getElementById("txhUsuario").value + "&txhCodEval=" + strcodEval
			,420,180);	
					
			 }
		 else
		 {
			alert(mstrSeleccione);
		 }
	}
	else{
		alert('Debe seleccionar s�lo un registro.');
	}
	
}

</script>
	</head>

   	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_detalle_practicas.html">
		<form:hidden path="codCurso" id="txhCodCurso"/>
		<form:hidden path="codigo" id="txhCodigo"/>
		<form:hidden path="descripcion" id="txhDescripcion"/>
		<form:hidden path="numero" id="txhNumero"/>
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="flag" id="txhFlag"/>
		<form:hidden path="flagEliminar" id="txhFlagEliminar"/>
		<form:hidden path="cant" id="txhCant"/>
		<form:hidden path="cantidad" id="txhCantidad"/>
		<form:hidden path="minimo" id="txhMinimo"/>
		<form:hidden path="min" id="txhMin"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codProducto" id="txhCodProducto"/>
		<form:hidden path="codEval" id="txhCodEval"/>
		<form:hidden path="sec" id="txhSec"/>
		<form:hidden path="codId" id="txhCodId"/>
		<form:hidden path="peso" id="txhPeso"/>
		<form:hidden path="cod" id="txhCod"/> 
		<form:hidden path="usuario" id="txhUsuario"/>
		<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<form:hidden path="flagCerrarNota" id="flagCerrarNota"/>
		
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>
		
		
		<table cellpadding="2" cellspacing="0" bordercolor="#eae9e9" style="margin-left:9px;width:98%">
			<tr>
				<td>
					<table class="borde_tabla" style="width:95%;margin-top:0px" cellspacing="0" cellpadding="0" >

						<tr class="fondo_cabecera_azul">		 
						 	<td class="titulo_cabecera" width="100%" colspan="4">
								Nro. de Evaluaciones Parciales
							</td>		 	
						 </tr>

						<tr class="fondo_dato_celeste">
							<td class="" style="width: 130px">&nbsp;Per�odo Vigente :</td>
							<td style="" align="left">
								<form:input path="periodo" id="txtPeriodo" 	cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:200px" readonly="true" />
							</td>
							<td class="" style="width: 10%">Ciclo :</td>
							<td style="width: 40%" align="left">
								<form:input path="ciclo" id="txtCiclo" 	cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:20px" readonly="true" />
							</td>
						</tr>
						<tr class="fondo_dato_celeste">
							<td class="" >&nbsp;Programa :</td>
							<td align="left">
								<form:input path="producto" id="txtProducto" cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:95%" readonly="true" />
							</td>
							<td class="" >Especialidad :</td>
							<td align="left">
								<form:input path="especialidad" id="txtEspecialidad" cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:200px" readonly="true" />
							</td>
						</tr>
						<tr class="fondo_dato_celeste">
							<td class="" >&nbsp;Curso :</td>
							<td align="left">
								<form:input path="curso" id="txtCurso" 	cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:200px" readonly="true" />
							</td>
							<td class="" >Sist. Eval. :</td>
							<td align="left">
								<form:input path="sistEval" id="txtSistEval" 	cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:20px" readonly="true" />
							</td>
						</tr>
						<tr class="fondo_dato_celeste">
							<td class="" >&nbsp;Evaluador :</td>
							<td align="left">
								<form:input path="evaluador" id="txtEvaluador" 	cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:200px" readonly="true" />
							</td>
							<td class="" >&nbsp;</td>
							<td >&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr >
				<td class="opc_combo">
					<!-- Titulo de la bandeja -->
					 <table cellpadding="0" cellspacing="0" class="borde_tabla" width="95%">

						<tr class="fondo_cabecera_azul">		 
						 	<td class="titulo_cabecera" width="100%" colspan="2">
								Relaci�n de Evaluaciones Parciales
							</td>		 	
						 </tr>
						 <c:if test="${control.flagCerrarNota=='1'}">
						 <tr>
						 	<td align="center">
						 		<span class="mensaje_rojo">
						 			&nbsp;&nbsp;El Registro se encuentra Cerrado						 			
						 		</span>						 	
						 	</td>
						 </tr>
						 </c:if>
					 </table>
					<table height="2px"><tr><td></td></tr></table>
				</td>						
			</tr>
			<tr>
				<td>
					<table class="tabla" style="width:95%;margin-top:6px;height:30px" background="${ctx}/images/Evaluaciones/back.jpg" cellspacing="2" cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td class="" style="width: 130px">&nbsp;Evaluaci�n Parcial : </td>
							<td align="left">
							<form:select  path="codEvaluacion" id="cboEvaluacion" cssClass="cajatexto" 
								cssStyle="width:180px" onchange="javascript:fc_muestra();">
							        <c:if test="${control.listEvaluaciones!=null}">
						        	<form:options itemValue="codigo" itemLabel="descripcion" 
						        		items="${control.listEvaluaciones}" />						        
						            </c:if>
	 				            </form:select>
								</td>
								<td align="center">
								<td cssClass="cajatexto_o" cssStyle="width:60px">Secci�n :
								<form:select path="seccion" id="cboSeccion" cssClass="cajatexto_o" cssStyle="width:100px" onchange="javascript:fc_muestra1();">
								<c:if test="${control.listaSeccion!=null}">
								<form:options itemValue="codSeccion" itemLabel="dscSeccion" items="${control.listaSeccion}" />
								</c:if>
							</form:select>
							</td>
								<td align="right" class="" height="10px">
									<form:label path="minimo" id="lblMinimo" cssClass="texto_dentro" cssStyle="HEIGHT: 14px;width:300px" >N�mero de registros m�nimos:${control.minimo}</form:label>
								</td>
							</tr>
					 </table>
				</td>
			</tr>

			<tr>
				<td>					
					<table cellpadding="0" cellspacing="0" width="100%" border="0">
						<tr>
							<td><!-- decorator="com.tecsup.SGA.bean.EvaluacionesDecorator"-->
								<div style="overflow: auto; height: 220px;width:100%">									
									<display:table name="sessionScope.listaCursos" cellpadding="0" cellspacing="1"
										decorator="com.tecsup.SGA.bean.CursoEvaluadorDecorator" 
										requestURI="" style="border: 1px solid #048BBA;width:98%">
										<display:column property="chkSelDetalle" title="Sel." style="width:4%;text-align:center"/>
										<display:column property="nroEval" title="N�mero" style="text-align:center;width:15%"/>
										<display:column property="peso" title="Peso" style="text-align:center;width:15%"/>
										<display:column property="nombreEvaluador" title="Evaluador" style="align:left;width:66%"/>
										<display:column property="flagEliminar" title="Eliminar" style="align:left;width:66%"/>
										
										<display:setProperty name="basic.empty.showtable" value="true"  />
										<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
										<display:setProperty name="paging.banner.placement" value="bottom"/>
										<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
										<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
										<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
														
										</display:table>
								</div>
							</td>
							<td width="27px" valign="top">&nbsp;
								<!-- JHPR 2008-07-01 -->
								<c:if test="${control.flagCerrarNota=='1'}">									
									<img src= "${ctx}/images/iconos/agregar_disabled.jpg"  style="cursor:hand" id="imgAgregar">&nbsp;									
									<img src= "${ctx}/images/iconos/modificar_disabled.jpg"  style="cursor:hand" id="imgModificar">&nbsp;
									<img src="${ctx}/images/iconos/quitar_disabled.jpg" alt="Quitar" style="cursor:hand" id="imgEliminar">								
								</c:if>
								
								<c:if test="${(control.flagCerrarNota=='0' || control.flagCerrarNota==null)}">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
										<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" style="cursor:hand" id="imgAgregar">&nbsp;
									</a>									 
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificar','','${ctx}/images/iconos/modificar1.jpg',1)">
										<img src= "${ctx}/images/iconos/modificar1.jpg" onclick="javascript:Fc_Modificar();" style="cursor:hand" id="imgModificar">&nbsp;
									</a>									 
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/iconos/quitar2.jpg',1)">
										<img src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar" style="cursor:hand" onclick="javascript:Fc_Eliminar();" id="imgEliminar">
									</a>								
								</c:if>

								
							</td>							
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form:form>

