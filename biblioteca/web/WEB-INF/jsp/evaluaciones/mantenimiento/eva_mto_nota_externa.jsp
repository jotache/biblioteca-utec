<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	
	<script language=javascript>
	
	function Fc_X(obj){
		a=document.getElementById("hid"+obj);
		if(a.checked==true){			
			document.getElementById("hid"+obj).checked=false;
		}
		else{
			document.getElementById("hid"+obj).checked=true;
		}
	}
	
	function onLoad(){
	}
	function fc_Regresar(){
		parent.location.href =  "${ctx}/menuEvaluaciones.html";
	}	
	function Fc_Incluir(seleccion){		
		a=document.getElementById("hid"+seleccion);				
		if(a.checked==true){
			Fc_ModificarRegla(a.value);			
		}
		else{			
			Fc_IncluirRegla(a.value);
		}
	}
	function Fc_ModificarRegla(codProducto){
		modificar="MODIFICAR";
		codPeriodo=document.getElementById("txhCodPeriodo").value;
		codEvaluador=document.getElementById("txhCodEvaluador").value;
		Fc_Popup("${ctx}/evaluaciones/eva_mto_regla_nota_externa.html?val="+codProducto+"&operacion="+modificar+"&txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador,660,285,"");
	}
	function Fc_IncluirRegla(codProducto){
		codPeriodo=document.getElementById("txhCodPeriodo").value;
		codEvaluador=document.getElementById("txhCodEvaluador").value;
		Fc_Popup("${ctx}/evaluaciones/eva_mto_regla_nota_externa.html?val="+codProducto+"&txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador,660,285,"");
	}
	
	</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_nota_externa.html">

	<form:hidden path="seleccionCheck" id="txhSeleccionCheck"/>	
	<form:hidden path="seleccionButton" id="txhSeleccionButton"/>
	<form:hidden path="operacion" id="txhOperacion"/>
		
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="94%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>		
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="534px" class="opc_combo"><font style="">Cursos de Tipo Nota Externa</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table><tr height="1px"><td></td></tr></table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="#048BBA" style="width:99%;margin-top:10px;margin-left:7px">
		<tr>
			<td>
				<div style="overflow: auto; height: 400px;width:99%">				
				<table class="" style="WIDTH: 97.5%;margin-top:0px;margin-bottom:1px;border: 1px solid #048BBA" 
				cellSpacing="1" cellPadding="0" border="0">
					<tr height="20px">
						<td class="headtabla" width="70%">&nbsp;Producto</td>
						<td class="headtabla" width="10%">&nbsp;Semestre</td>
						<td class="headtabla" width="10%">&nbsp;Sede</td>
						<td class="headtabla" width="10%">&nbsp;Incluir Regla</td>
					</tr>					
					<c:forEach var="lista" items="${control.listCursos}" varStatus="status">												
					<!-- tr  class=""-->
						<c:choose>
							<c:when test="${status.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
							<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
						</c:choose>
																										
						<td style="text-align: left;width:70%" >&nbsp;					
							<c:out value="${lista.descripcion}" />								
						</td>						
						<td style="text-align: left;width:10%" >&nbsp;					
							<c:out value="${lista.nomPeriodo}" />								
						</td>
						<td style="text-align: left;width:10%" >&nbsp;					
							<c:out value="${lista.nomSede}" />								
						</td>
						<td align="center"  style="width:10%" >
							&nbsp;<input type="checkbox" name="check"							 				
							<c:out value="${lista.seleccion}" />
							value='<c:out value="${lista.codCurso}" />'							
							id='hid<c:out value="${lista.codCurso}" />'
							onclick='javascript:Fc_X(this.value);'>&nbsp;														
							<img src= '${ctx}/images/iconos/agregar1.jpg'
							value='<c:out value="${lista.codCurso}" />' 
							onclick='javascript:Fc_Incluir(this.value);'
							style='cursor:hand' alt='Incluir'>
						</td>							
					</tr>
					</c:forEach>
				</table>
				</div>
			</td>
		</tr>
	</table>
		
</form:form>									
</body>
</html>