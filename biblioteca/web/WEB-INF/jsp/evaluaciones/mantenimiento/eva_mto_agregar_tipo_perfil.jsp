<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script language=javascript>
    function onLoad(){
	   objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" ){
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}else{
			if(objMsg.value == "ERROR"){
			  alert(mstrProblemaGrabar);			
			}else{
				if(objMsg.value == "DUPLICADO")
		            alert(mstrExistenRegistros);
	        }
		}
	}
	function fc_Valida(){	
	  return true;
	}
	function fc_Grabar(){
		objDescripcion = $('txtDscProceso');
		if(fc_Trim(objDescripcion.value) == ''){		
		    window.opener.document.getElementById("frmMain").submit();		    
			alert('Debe ingresar una descripción.');
			return;
		}
		
		$('txhOperacion').value = 'GRABAR';
		$('frmMain').submit();
	
	}
</script>
</head>
<body leftmargin="5px" rightmargin="5px">
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_agregar_tipo_perfil.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codDetalle" id="txhCodDetalle"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>
		<!-- 
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="400px" class="opc_combo"><font style="">Agregar Perfil</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		 </table>
		 -->

		<table class="borde_tabla" style="width:98%;margin-top:5px" cellSpacing="0" cellPadding="0" align="center">

			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="1">
					Agregar Perfil
				</td>		 	
			</tr>
 
			<tr class="fondo_dato_celeste">
				<td width="30%">&nbsp;Perfil :		
				
					<form:input path="descripcion" id="txtDscProceso" maxlength="50"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'concepto');" 
					cssClass="cajatexto" size="40" />
				</td>
			</tr>
						
		</table>
			
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
			</tr>
		</table>
		<br>
</form:form>
</body>
</html>