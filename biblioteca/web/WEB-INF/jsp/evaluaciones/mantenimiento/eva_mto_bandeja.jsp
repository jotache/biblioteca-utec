<%@ include file="/taglibs.jsp"%>
<head>
<link href="../estilos/estilo_eva.css" rel="styleSheet" type="text/css">
<script type="text/javascript" src="../library/js/Func_Comunes.js"></script>
<script id="clientEventHandlersJS" language="javascript"></script>


<script language="JavaScript" type="text/JavaScript">
codPeriodo="<%=request.getAttribute("codPeriodo") %>";
codEvaluador="<%=request.getAttribute("codEvaluador") %>";

function onLoad(){	
}
	
function fc_CargaTab(param){
	form = document.forms[0];
	form.ir.value = param;
	switch(param){
		case '1':
			form.operacion.value = "inicio";
			form.submit();
			break;
		case '2':	
			form.operacion.value = "inicioCubo";
			form.submit();
			break;
		case '3':	
			form.operacion.value = "inicioEvento";
			form.submit();
			break;
		case '4':	
			form.operacion.value = "inicioHoja";
			form.submit();
			break;
		case '5':	
			form.operacion.value = "inicioParametro";
			form.submit();
			break;
	}
}

function fc_Aparece(param){			
	document.getElementById("tr_mant").style.display = "none";
	document.getElementById("tr_conf").style.display = "none";
	if (param == '1'){document.getElementById("tr_mant").style.display="";}	
	if (param == '2'){document.getElementById("tr_conf").style.display="";}	
}

function fc_muestra(param){

	var pag_tipo = $('pag_tipo');
	var pag_desc = $('pag_desc');
	var pag_sanc = $('pag_sanc');
	var pag_feri = $('pag_feri');
	var pag_serv = $('pag_serv');
	var	pag_para = $('pag_para');
	var pag_para2 = $('pag_para2');
	
	/*pag_tipo.className = "tab_sub";
	pag_desc.className = "tab_sub";
	pag_sanc.className = "tab_sub";
	pag_feri.className = "tab_sub";
	pag_serv.className = "tab_sub";
	pag_para.className = "tab_sub";
	pag_para2.className = "tab_sub";*/

	switch(param){
		case '1':
			pag_tipo.className = "tab_sub_res";	
			destino = "${ctx}/evaluaciones/eva_mto_concepto_perfil.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
			break;
		case '2':
			pag_desc.className = "tab_sub_res";						
			destino = "${ctx}/evaluaciones/eva_mto_competencias.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
			break;
		case '3':	
			pag_clas.className = "tab_sub_res";
			parent.cuerpo1.location.href = "mto_cfg_calificaciones_consulta.htm";	
			break;
		case '4':
			pag_auto.className = "tab_sub_res"	
			parent.cuerpo1.location.href = "mto_cfg_calificacion_tipo_evaluacion_consulta.htm";	
			break;
		case '5':	
			pag_edit.className = "tab_sub_res";
			parent.cuerpo1.location.href = "Consulta_Editoriales.htm";
			break;
		case '6':	
			pag_pais.className = "tab_sub_res";
			parent.cuerpo1.location.href = "Consulta_Paises.htm";	
			break;
		case '7':	
			pag_ciud.className = "tab_sub_res";
			parent.cuerpo1.location.href = "Consulta_Ciudades.htm";	
			break;
		case '8':
			pag_idio.className = "tab_sub_res";
			parent.cuerpo1.location.href = "Consulta_Idiomas.htm";	
			break;
		case '9':
			pag_sala.className = "tab_sub_res";
			parent.cuerpo1.location.href = "Consulta_Salas.htm";	
			break;
		case '10':
			pag_vide.className = "tab_sub_res";
			parent.cuerpo1.location.href = "Consulta_Tipo_Video.htm";	
			break;
		case '11':
			pag_proc.className = "tab_sub_res";
			parent.cuerpo1.location.href = "Consulta_Tipo_Procedencia.htm";	
			break;		
		case '12':
			pag_sanc.className = "tab_sub_res";	
			destino = "${ctx}/evaluaciones/eva_mto_config_parametros_grales.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
			break;
		case '14':
			pag_feri.className = "tab_sub_res";	
			destino = "${ctx}/eva/mto_parametrosEspeciales.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
			break;
		case '13':
			pag_serv.className = "tab_sub_res";
			destino = "${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;			
			
			break;
		case '16':
			pag_para.className = "tab_sub_res";	
			destino = "${ctx}/evaluaciones/eva_mto_config_componentes.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
			//destino = "${ctx}/eva/mto_menu.html?construccion=construccion";
			break;
		case '17':
			pag_para2.className = "tab_sub_res";				
			destino = "${ctx}/evaluaciones/eva_mto_nota_externa.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
			break;
	}
	
	document.getElementById("iFrame").src = destino;		
}

/*
function fc_Aparece(param){			
	td_unidad.className = "tab_vert";
	td_cubo.className = "tab_vert";
	td_evento.className = "tab_vert";
	td_hojaDiag.className = "tab_vert";
	td_parametro.className = "tab_vert";
	switch(param){
		case '1':
			td_unidad.className = "tab_vert_resaltado";
			destino = "${ctx}/mantTablaDetalle.html?operacion=UnidadesTrabajo&estado=A"
			break;
		case '2':	
			td_cubo.className = "tab_vert_resaltado";
			destino = "${ctx}/mantTablaDetalle.html?operacion=Cubos&estado=A"
			break;
		case '3':			
			td_evento.className = "tab_vert_resaltado";
			destino = "${ctx}/mantTablaDetalle.html?operacion=Eventos&estado=A"
			break;
		case '4':
			td_hojaDiag.className = "tab_vert_resaltado";
				destino = "/SGA/eva/mto_parametrosEspeciales.html"
			break;
		case '5':			
			td_parametro.className = "tab_vert_resaltado";
			destino = "${ctx}/parametros.html"
			
			break;
		default:
			td_unidad.className = "tab_vert_resaltado";
			destino = "${ctx}/mantTablaDetalle.html?operacion=UnidadesTrabajo&estado=A"
			break;

	}
	
	document.getElementById("iFrame").src = destino;		
}
*/
function fc_onclick(param){		
	var obj;	
	if (txhFila.value != '0'){
		obj = eval('tr' + String(txhFila.value));
		obj.className = "texto";
	}
	obj = eval('tr' + String(param));
	obj.className = "resaltado";
	txhFila.value = param;
}
		
function fc_filtroSituacion(param){
	form = document.forms[0];
	form.ir.value = param;
	switch(param){
		case '1':
			form.operacion.value = "listarUnidadTrabajo";
			break;
		case '2':	
			form.operacion.value = "listarCubo";
			break;
		case '3':	
			form.operacion.value = "listarEvento";
			break;
		case '4':	
			form.operacion.value = "listarHoja";
			break;
	}
	form.submit();
	return true;
}


</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoad()">
	
<form>
	<input type="hidden" name="txhFila" value="0">	
	<input type="hidden" id="ir" name="ir" value='<%=(String) request.getSession().getAttribute("Ir")%>'/>
	<input type="hidden" id="txhCodPeriodo" name="txhCodPeriodo" value='<%=(String) request.getAttribute("txhCodPeriodo")%>'/>
	<input type="hidden" id="txhCodEvaluador" name="txhCodEvaluador" value='<%=(String) request.getAttribute("txhCodEvaluador")%>'/>

	
	<table style="width:98%;height:50%;cursor:pointer;" cellspacing="0" cellpadding="0"  
	border="0" bordercolor="red"  align="center">
		<tr>
			<td valign="top" width="20%">					
				<table style="HEIGHT: 30px" cellspacing="0" cellpadding="0" border="1" bordercolor="white">
					<tr height="24px"><td>&nbsp;</td></tr>
					<tr>
						<td id="td_mantenimiento" width="12%" onclick="javascript:fc_Aparece('1');" class="Tab_vert">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmantenimiento','','${ctx}/images/Evaluaciones/Menu/mantenimientos2.jpg',1)">
						<img id="imgmantenimiento" src= "${ctx}/images/Evaluaciones/Menu/mantenimientos1.jpg" style="cursor:pointer"></a>
						</td>
					</tr>
					<tr style="DISPLAY:none"id="tr_mant" name="tr_mant" border="0">
						<td>
							<table cellspacing="0" cellpadding="0" border="0" bordercolor="white">
								<tr>
									<td id="pag_tipo" width="10%" onclick="javascript:fc_muestra('1');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgperfiles','','${ctx}/images/Evaluaciones/Menu/perfiles1.jpg',1)">
									<img id="imgperfiles" src= "${ctx}/images/Evaluaciones/Menu/perfiles2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
								<tr>
									<td id="pag_desc" width="10%" onclick="javascript:fc_muestra('2');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcompetencias','','${ctx}/images/Evaluaciones/Menu/competencias1.jpg',1)">
									<img id="imgcompetencias" src= "${ctx}/images/Evaluaciones/Menu/competencias2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>									
							</table>
						</td>
					</tr>
					<tr>
						<td id="td_configuraciones" width="12%" onclick="javascript:fc_Aparece('2');" class="tab_vert_resaltado">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgconfiguraciones','','${ctx}/images/Evaluaciones/Menu/configuraciones2.jpg',1)">
						<img id="imgconfiguraciones" src= "${ctx}/images/Evaluaciones/Menu/configuraciones1.jpg" style="cursor:pointer"></a>
						</td>
					</tr>
					<tr style="DISPLAY:none"id="tr_conf" name="tr_conf" border="0">
						<td>
							<table cellspacing="0" cellpadding="0" border="0" bordercolor="white">
								<tr>
									<td id="pag_sanc" width="10%" onclick="javascript:fc_muestra('12');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgparamgen','','${ctx}/images/Evaluaciones/Menu/parametrosg1.jpg',1)">
									<img id="imgparamgen" src= "${ctx}/images/Evaluaciones/Menu/parametrosg2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
								<tr>
									<td id="pag_feri" width="10%" onclick="javascript:fc_muestra('14');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgparamesp','','${ctx}/images/Evaluaciones/Menu/parametrosesp1.jpg',1)">
									<img id="imgparamesp" src= "${ctx}/images/Evaluaciones/Menu/parametrosesp2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
								<tr>
									<td id="pag_serv" width="10%" onclick="javascript:fc_muestra('13');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imghibrido','','${ctx}/images/Evaluaciones/Menu/hibrido1.jpg',1)">
									<img id="imghibrido" src= "${ctx}/images/Evaluaciones/Menu/hibrido2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
								<tr>
									<td id="pag_para" width="10%" onclick="javascript:fc_muestra('16');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcompo','','${ctx}/images/Evaluaciones/Menu/componentes1.jpg',1)">
									<img id="imgcompo" src= "${ctx}/images/Evaluaciones/Menu/componentes2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
								<tr>
									<td id="pag_para2" width="10%" onclick="javascript:fc_muestra('17');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgnotaext','','${ctx}/images/Evaluaciones/Menu/notaexterna1.jpg',1)">
									<img id="imgnotaext" src= "${ctx}/images/Evaluaciones/Menu/notaexterna2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="80%" valign="top">
				<table style="width:99%;background-color:white;margin-left: 5px"
					cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<iframe id="iFrame" name="iFrame" width="100%" height="500px" frameborder="0"
								src="${ctx}/eva/mto_menu.html">
							</iframe>
						</td>		
					</tr>
				</table>
			</td>
		</tr>
	</table>

</form>
</body>

