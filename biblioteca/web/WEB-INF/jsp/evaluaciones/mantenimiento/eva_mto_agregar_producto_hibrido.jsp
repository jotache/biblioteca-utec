<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>	
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script language=javascript>
	var examen = new Array();
	var practica = new Array();
	var iExa=0;
	var iPra=0;
	function onLoad(){
		estado = document.getElementById("txhEstadoCodigo");
		objMsg = document.getElementById("txhMsg");
		if(estado.value=='DESABILITADO'){			
			document.getElementById("txtCodigo").readOnly=true;
		}						
		if ( objMsg.value == "OK" ){
			codPeriodo=document.getElementById("txhCodPeriodo").value;
			codEvaluador=document.getElementById("txhCodEvaluador").value;	
			location.href="${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;			
			alert(mstrSeGraboConExito);					
		}
		else if ( objMsg.value == "IGUAL" ){
			alert('El valor del c�digo ya existe para otro registro.');
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
		}
		Fc_CrearLista();		
	}
	function fc_Regresar(){
		codPeriodo=document.getElementById("txhCodPeriodo").value;
		codEvaluador=document.getElementById("txhCodEvaluador").value;
		location.href =  "${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
	}		
	function Fc_Cancelar(){
		codPeriodo=document.getElementById("txhCodPeriodo").value;
		codEvaluador=document.getElementById("txhCodEvaluador").value;
		location.href="${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;			
	}
	function Fc_AgregarExamen(){
		if(Fc_ValidarSeleccion('1')){
			alert('Debe seleccionar un tipo de examen.');
			return;
		}
		var tr = document.createElement("<tr class='texto'>");
		tr.id =document.getElementById("cboTipoExamen").value;		
		
		var td01 = document.createElement("td"); 
		var td02 = document.createElement("<td align='center'>"); 
		var td03 = document.createElement("<td align='center'>");
		
		index=document.getElementById("cboTipoExamen").selectedIndex;
		codigo=document.getElementById("cboTipoExamen").value;
		if(Fc_ListaExamen(codigo)){
			var texto=document.getElementById("cboTipoExamen").options[index].text;
			var hidden01="<input type='hidden' value='0'>";
			var hidden02="<input type='hidden' value='"+codigo+"'>";
			var input="<input id='peso' size='10' onkeypress='fc_PermiteNumeros();' onblur=\"fc_ValidaNumeroFinal(this,'peso');fc_ValidaNumeroFinalMayorCero(this);\" class='cajatexto' maxlength='3'>";		
			var img="<img src= '${ctx}/images/iconos/quitar1.jpg' onclick='javascript:Fc_Quitar(this);' style='cursor:hand' alt='Quitar'>";	
			
			td01.appendChild(document.createTextNode(texto));
			td01.appendChild(document.createElement(hidden01));
			td01.appendChild(document.createElement(hidden02)); 
			tr.appendChild(td01); 
			td02.appendChild(document.createElement(input)); 
			tr.appendChild(td02);		 
			td03.appendChild(document.createElement(img)); 
			tr.appendChild(td03);
			
			document.getElementById("tabla").lastChild.appendChild(tr);
		}
		else{
			alert('Seleccione otra opci�n.');
		}
		return true;	
	}
	function Fc_AgregarPractica(){
		if(Fc_ValidarSeleccion('2')){
			alert('Debe seleccionar un tipo de evaluaci�n.');
			return;
		}	
		var tr = document.createElement("<tr class='texto'>");
		tr.id =document.getElementById("cboTipoPractica").value;		 
		
		var td01 = document.createElement("td"); 
		var td02 = document.createElement("<td align='center'>"); 
		var td03 = document.createElement("<td align='center'>");		
		
		index=document.getElementById("cboTipoPractica").selectedIndex;
		codigo=document.getElementById("cboTipoPractica").value;		 
		if(Fc_ListaPractica(codigo)){
			var texto=document.getElementById("cboTipoPractica").options[index].text;
			var hidden01="<input type='hidden' value='1'>";
			var hidden02="<input type='hidden' value='"+codigo+"'>";
			var input="<input id='peso' size='10' onkeypress='fc_PermiteNumeros();' onblur=\"fc_ValidaNumeroFinal(this,'peso');fc_ValidaNumeroFinalMayorCero(this);\" class='cajatexto' maxlength='3'>";
			var img="<img src= '${ctx}/images/iconos/quitar1.jpg' onclick='javascript:Fc_Quitar(this);' style='cursor:hand' alt='Quitar'>";		
			
			td01.appendChild(document.createTextNode(texto));
			td01.appendChild(document.createElement(hidden01));
			td01.appendChild(document.createElement(hidden02));
			tr.appendChild(td01); 
			td02.appendChild(document.createElement(input)); 
			tr.appendChild(td02);		 
			td03.appendChild(document.createElement(img)); 
			tr.appendChild(td03);
			
			document.getElementById("tabla").lastChild.appendChild(tr);
		}
		else{
			alert('Seleccione otra opci�n.');
		}
		return true;	
	}	
	function Fc_ListaExamen(codigo){		
		var band=true;		
		if(examen.length==0){			
			examen[0]=codigo;
			iExa=iExa+1;			
		}
		else{
			for(var i=0;i<examen.length;i++){				
				if(fc_Trim(examen[i])==codigo){					
					band=false;
				}			
			}
			if(band==true){
				examen[iExa]=codigo;
				iExa=iExa+1;				
			}
		}
		return band;
	}
	function Fc_ListaPractica(codigo){		
		var band=true;		
		if(practica.length==0){
			practica[0]=codigo;
			iPra=iPra+1;			
		}
		else{
			for(var i=0;i<practica.length;i++){				
				if(fc_Trim(practica[i])==codigo){
					band=false;
				}			
			}
			if(band==true){
				practica[iPra]=codigo;
				iPra=iPra+1;				
			}
		}
		return band;
	}
	function Fc_ValidarSeleccion(tipo){
		if(tipo==1)
			index=document.getElementById("cboTipoExamen").selectedIndex;
		else
			index=document.getElementById("cboTipoPractica").selectedIndex;
		if(index==0)
			return true;		
		else
			return false;		
	}
	function Fc_Quitar(obj){	
		while (obj.tagName!='TR') 
		   	obj = obj.parentNode;
		tab = document.getElementById('tabla');
		for (i=0; ele=tab.getElementsByTagName('tr')[i]; i++){
		    if (ele==obj)
				num=i;
		}
		QuitarEnLista(num);
		tab.deleteRow(num);		
	}
	function Fc_CrearLista(){
		var num=0;		
		tab = document.getElementById('tabla');
		for (i=0; ele=tab.getElementsByTagName('tr')[i+1]; i++){						
			num=i;
			a=tab.getElementsByTagName('input')[parseInt(num*3)];			
			b=tab.getElementsByTagName('input')[parseInt(num*3+1)];
			if(a.value=='0'){
				examen[iExa]=b.value;				
				iExa=iExa+1;				
			}
			else{
				practica[iPra]=b.value;
				iPra=iPra+1;
			}
		}	
	}
	function QuitarEnLista(num){
		cel = document.getElementById('tabla');		
		num=num-1;
		a=cel.getElementsByTagName('input')[parseInt(num*3)];
		b=cel.getElementsByTagName('input')[parseInt(num*3+1)];
		c=cel.getElementsByTagName('input')[parseInt(num*3+2)];		
		if(a.value==0){		
			for(var i=0;i<examen.length;i++){
				if(examen[i]==b.value){
					examen[i]=-1;
				}
			}
		}
		else{
			for(var i=0;i<practica.length;i++){
				if(practica[i]==b.value){
					practica[i]=-1;
				}
			}
		}
	}
	function Fc_ValidarCadena(){
		var band=true;
		cel = document.getElementById('tabla');		
     	for (i=0; ele=cel.getElementsByTagName('input')[i]; i++){     		
     		if(ele.value==""||fc_Trim(ele.value)==""){
     			ele.value="";
     			ele.focus();
	     		alert('Debe ingresar el peso.');
	     		return false;
	     	}
	    }
	    return band;
	}
	function Fc_ObtenerCadena(){				
		var st	= "";
		var nroRegistros=0;		
		cel = document.getElementById('tabla');		
     	for (i=0; ele=cel.getElementsByTagName('input')[i]; i++){     		
     		if(i==0){
     			st=st+ele.value;
     		}
     		else{
     			if(i%3==0){
     				nroRegistros=nroRegistros+1;
     				st=st+"|"+ele.value;
     			}
     			else{
     				//se pasa a entero para no ingrsar valores como 01 � 001 en la BD
     				st=st+"$"+parseInt(ele.value,10);
     			}
     		}	     	
     	}		
     	if(st!="")     	
     	document.getElementById("txhNroRegistros").value=nroRegistros+1;
		return st;		
	}
	function Fc_Grabar(){				
		objCodigo = document.getElementById("txtCodigo");
		objDenominacion = document.getElementById("txtDenominacion");		
		if (objDenominacion.value == "" || fc_Trim(objDenominacion.value)==""){
			document.getElementById("txtDenominacion").value="";
			document.getElementById("txtDenominacion").focus();
			alert('Debe ingresar la denominaci�n.');
			return;
		}
		if(objCodigo.value == "" || fc_Trim(objCodigo.value)==""){
			document.getElementById("txtCodigo").value="";
			document.getElementById("txtCodigo").focus();
			alert('Debe ingresar el c�digo.');
			return;			
		}
		if(Fc_ObtenerCadena()!=""){								
			if(Fc_ValidarCadena()){				
				if(confirm(mstrSeguroGrabar)){				
					document.getElementById("txhCadEvaluacion").value = Fc_ObtenerCadena();			
					document.getElementById("frmMain").submit();
				}				
			}									
		}
		else{
			alert('Debe ingresar un tipo de examen � evaluaci�n.');
		}		
	}	
	</script>
</head>
<body topmargin="3" leftmargin="3" rightmargin="3">
<form:form action="${ctx}/evaluaciones/eva_mto_agregar_producto_hibrido.html" commandName="control" id="frmMain">
	<form:hidden path="codDetalle" id="txhCodDetalle"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="cadEvaluacion" id="txhCadEvaluacion"/>	
	<form:hidden path="nroRegistros" id="txhNroRegistros"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="estadoCodigo" id="txhEstadoCodigo"/>
	<form:hidden path="seleccion" id="txhSeleccion"/>
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="98%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>	
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="544px" class="opc_combo"><font style="">Sistema de evaluaci�n - Producto H�brido</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table><tr height="1px"><td></td></tr></table>
	
	<table class="tabla" cellpadding="2" background="${ctx}/images/Evaluaciones/back.jpg" cellspacing="2" border="0"
	 style="margin-top:5px;margin-left:5px;width:96%">
		<tr>
			<td class="">C�digo :</td>
			<td>
				<form:input path="codigo" id="txtCodigo"
				onkeypress="fc_ValidaTextoNumeroEspecial();"
				onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'c�digo');"											 
				cssClass="cajatexto" size="15" maxlength="10"/>			
			</td>
			<td class="">Denominaci�n :</td>
			<td>			
				<form:input path="denominacion" id="txtDenominacion" 
				onkeypress="fc_ValidaTextoEspecial();"
				onblur="fc_ValidaNumeroLetrasGuionOnblur(this.id,'denominaci�n');"				 
				cssClass="cajatexto" size="35" maxlength="20"/>
			</td>
		</tr>
		<tr>
			<td class="" width="20%">Ex�menes :&nbsp;&nbsp;</td>
			<td>
				<form:select path="tipoExamen" id="cboTipoExamen" cssClass="cajatexto" cssStyle="width:180px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaExamenes!=null}">
						<form:options itemValue="codigo" itemLabel="descripcion" 
							items="${control.listaExamenes}" />
					</c:if>
				</form:select>
				&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar2','','${ctx}/images/iconos/agregar2.jpg',1)">
				&nbsp;<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_AgregarExamen();" id="imgagregar2" style="cursor:hand" alt="Agregar"></a>			
			</td>
			<td class="">Evaluaci�n Parcial :&nbsp;&nbsp;</td>
			<td>
				<form:select path="tipoPractica" id="cboTipoPractica" cssClass="cajatexto" cssStyle="width:180px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaPracticas!=null}">
						<form:options itemValue="codigo" itemLabel="descripcion" 
							items="${control.listaPracticas}" />
					</c:if>
				</form:select>
				&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_AgregarPractica();" id="imgagregar" style="cursor:hand" alt="Agregar"></a>
			</td>
		</tr>
	</table>
	<table><tr height="4px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" width="100%" class="">
		<tr>
			<td>				
				<TABLE id="tabla" class="tablagrilla" style="margin-left:5px;margin-bottom:5px;border: 1px solid #048BBA;width:96%" 
				cellSpacing="1" cellPadding="0" border="0" borderColor="white">
					
					<TR height="20px">
						<TD class="headtabla" width="70%">&nbsp;Evaluaciones</TD>
						<TD class="headtabla" width="10%" align="center">&nbsp;Peso</TD>
						<TD class="headtabla" width="20%" align="center">&nbsp;Elim</TD>
					</TR>
					<c:forEach var="lista" items="${control.listaBandeja}" varStatus="status">												
						<!--tr  class="tablagrilla"-->	
					<c:choose>
						<c:when test="${status.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
						<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>											
							<td align="left" >
								<input type='hidden' value='<c:out value="${lista.codTipoEvaluacion}" />'>
								<input type='hidden' value='<c:out value="${lista.descTipoEvaluacion}" />'>
								<c:out value="${lista.lista}" />								
							</td>
							<td align="center" >
								<input id='peso' size='10' value='<c:out value="${lista.peso}" />' onkeypress='fc_ValidaNumero();' onblur="fc_ValidaNumeroFinal(this,'peso');fc_ValidaNumeroFinalMayorCero(this);" class='cajatexto' maxlength="3">								
							</td>
							<td align="center">
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
								<img src= '${ctx}/images/iconos/quitar1.jpg' id="imgquitar" onclick='javascript:Fc_Quitar(this);' style='cursor:hand' alt='Quitar'></a>
							</td>							
						</tr>
					</c:forEach>					
				</TABLE>	
			</td>	
		</tr>			
	</table>	
	
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:Fc_Grabar();" style="cursor:pointer"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" onclick="javascript:Fc_Cancelar();" style="cursor:pointer"></a></td>
		</tr>
	</table>

</form:form>									
</body>