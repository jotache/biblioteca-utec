<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script language=javascript>
	function onLoad(){
		objMsg = document.getElementById("txhMsg");
	
		if ( objMsg.value == "OK" ){	
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();	
		}
		else if ( objMsg.value == "IGUAL" ){
			alert('La denominación ya existe para otro registro.');			
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}		
	}
	
	function Fc_Grabar(){
		objDenominacion = document.getElementById("txtDenominacion");
		objPorcentaje = document.getElementById("txtPorcentaje");
		objValorTope = document.getElementById("txhValorTope").value;
		
		document.getElementById("txhValorTope").value = objValorTope;				
		
		if (objDenominacion.value == "" || fc_Trim(objDenominacion.value)=="")
		{	document.getElementById("txtDenominacion").value="";			
			alert('Debe ingresar la denominación.');
			objDenominacion.focus();			
			return;
		}
		if( objPorcentaje.value == "" || fc_Trim(objPorcentaje.value)==""){
			document.getElementById("txtPorcentaje").value="";
			alert('Debe ingresar el porcentaje.');
			objPorcentaje.focus();			
			return;
		}
		var a=parseInt(objPorcentaje.value,10) + parseInt(objValorTope,10);		
		if(a > 100){
			alert('Debe ingresar un valor menor, la suma de los % debe ser 100.');			
			return;
		}
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txtPorcentaje").value=parseInt(objPorcentaje.value,10);
			document.getElementById("frmMain").submit();
		}
	}
</script>
</head>
<body topmargin="0" leftmargin="10" rightmargin="0">
<form:form action="${ctx}/evaluaciones/eva_mto_agregar_competencia.html" commandName="control" id="frmMain">
	<form:hidden path="codDetalle" id="txhCodDetalle"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="valorTope" id="txhValorTope"/>
				
	
		<table class="borde_tabla" style="width:98%;margin-top:5px" cellSpacing="0" cellPadding="2" border="0" bordercolor='red' height="30px">

			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="2">
					<c:if test="${control.operacion=='MODIFICAR'}">
						Modificar Competencia
					</c:if>
					<c:if test="${control.operacion=='GRABAR'}">
						Agregar Competencia
					</c:if>
				</td>		 	
			</tr>

			<tr class="fondo_dato_plomo">
				<td nowrap>Denominación:</td>			
				<td>
				<form:input path="denominacion" id="txtDenominacion"
					maxlength="200"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this.id,'denominacion');" 
					cssClass="cajatexto" size="40" />							
				</td>
			</tr>
			<tr class="fondo_dato_plomo">
				<td nowrap>Porcentaje (%):</td>			
				<td>
				<form:input path="porcentaje" id="txtPorcentaje" 
				maxlength="2"
				onkeypress="fc_PermiteNumeros();"
				onblur="fc_ValidaNumeroFinal(this,'porcentaje');fc_ValidaNumeroFinalMayorCero(this);" 
				cssClass="cajatexto" size="5" />
				</td>
			</tr>
		</table>
				
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:Fc_Grabar();"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">					
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
</form:form>
</body>
</html>