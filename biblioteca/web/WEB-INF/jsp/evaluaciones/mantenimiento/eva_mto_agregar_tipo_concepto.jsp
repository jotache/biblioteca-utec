<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script language=javascript>
        function onLoad(){
			objMsg = document.getElementById("txhMsg");
			if ( objMsg.value == "OK" )
			{
				window.opener.document.getElementById("frmMain").submit();
				
				alert(mstrSeGraboConExito);
				window.close();
			}
			else{ if ( objMsg.value == "ERROR" )
			      {  
				  alert(mstrProblemaGrabar);			
			      }
			      else{if ( objMsg.value == "DUPLICADO" )
			           alert(mstrExistenRegistros);}
			}
		}

	function fc_Grabar(){
		objDescripcion = $('txtDscProceso');
		if (fc_Trim(objDescripcion.value) == "")
		{
			alert('Debe ingresar una descripción.');
			return;
		}
		$('txhOperacion').value = 'GRABAR';
		$('frmMain').submit();
	 }

</script>	
	<body topmargin="5" leftmargin="5" rightmargin="5">
	<form:form action="${ctx}/evaluaciones/eva_mto_agregar_tipo_concepto.html" commandName="control" id="frmMain" name="frmMain">
	<form:hidden path="codDetalle" id="txhCodDetalle"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="valor1" id="txhValor1"/>
	<form:hidden path="msg" id="txhMsg"/>
		
		<table class="borde_tabla" style="width:98%;margin-top:5px" cellSpacing="0" cellPadding="0" align="center">

			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="2">
					Agregar Concepto del Perfil
				</td>		 	
			</tr>
 
			<tr class="fondo_dato_celeste">
				<td nowrap >Perfil :</td>
				<td>&nbsp;	
			    	<form:input path="descripcionNivel1" id="txtDescripcionNivel1" cssClass="cajatexto" size="40" readonly="true" />					
				</td>
			</tr>
			<tr class="fondo_dato_celeste">
				<td>Descripción :</td>
				<td>&nbsp;
					<form:input path="descripcion" id="txtDscProceso" maxlength="50"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'concepto');" 
					cssClass="cajatexto" size="40" />
				</td>
			</tr>
		</table>	
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
		<br>
	</form:form>
	</body>
</html>
