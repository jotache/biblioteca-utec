<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>

	<script language=javascript>
	function onLoad(){	
		objMsg = document.getElementById("txhMsg");		
		if ( objMsg.value == "OK" ){
			codPeriodo=document.getElementById("txhCodPeriodo").value;
			codEvaluador=document.getElementById("txhCodEvaluador").value;
			location.href ="${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
			
			alert(mstrSeGraboConExito);					
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrNoElimino);
		}
		
	}
	function fc_Regresar(){
		codPeriodo=document.getElementById("txhCodPeriodo").value;
		codEvaluador=document.getElementById("txhCodEvaluador").value;
		location.href ="${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
	}
	function Fc_Cancelar(){
		codPeriodo=document.getElementById("txhCodPeriodo").value;
		codEvaluador=document.getElementById("txhCodEvaluador").value;
		location.href ="${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;	
	}
	function Fc_Mover(side){
		var origen;//origen
		var destino;//destino
		if (side == "right")
		{ 
			origen = document.getElementById("cboCursosHibrido");
			destino = document.getElementById("cboCursosHibridoSel");
		}
		else
		{  
			origen = document.getElementById("cboCursosHibridoSel");
			destino = document.getElementById("cboCursosHibrido"); 
		}
		var idSel=-1;
		for (var i = 0; i < origen.length; i++)
		{  
			if ( origen.options[i].selected )
			{
				idSel=	i;
			}
		}
		if(idSel==-1)
		{
			alert('Debe seleccionar una opci�n.');
			return;
		}
		var NroItemsDestino=destino.length;
		var oOption = document.createElement('OPTION');
		destino.options.add(oOption);
		oOption.value = origen.options[idSel].value;
		oOption.innerText = origen.options[idSel].text;
		for(i=idSel;i < origen.length-1; i++){
			origen.options[i].value=origen.options[i+1].value;
			origen.options[i].text=origen.options[i+1].text;
		}
		origen.length=origen.length-1;
		origen.focus()
	}
	function Fc_Grabar(){
		
		form = document.forms[0];
		tkn = "";		
		objCombo = document.getElementById("cboCursosHibridoSel");
		
		form.nroRegistros.value = objCombo.options.length;
		for ( var i = 0; i < objCombo.options.length ; i++)
		{			
			tkn = tkn + objCombo.options[i].value+"|";
		}

		if(tkn.length>0)
			tkn = tkn.substring(0,tkn.length-1);	
		
		if(confirm(mstrSeguroGrabar)){
			form.tknCiclos.value = tkn;		
			form.operacion.value="GRABAR";
			form.submit();
		}
		
	}
	</script>
</head>
<body topmargin="0" leftmargin="10 rightmargin="0">
<form:form action="${ctx}/evaluaciones/eva_mto_relacionar_producto_hibrido.html" commandName="control" id="frmMain">
	<form:hidden path="codDetalle" id="txhCodDetalle"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
		
	<form:hidden path="codigo" id="txhCodigo"/>
	<form:hidden path="descripcion" id="txhDescripcion"/>
	
	<form:hidden path="nroRegistros" id="nroRegistros"/>
	<form:hidden path="tknCiclos" id="tknCiclos"/>
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="green" width="96%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="535px" class="opc_combo"><font style="">Relacionar Sist. Evaluaciones y Productos H�bridos</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table><tr height="5px"><td></td></tr></table>
	
	<table cellpadding="0" cellspacing="0" style="margin-left:4px;width:95%" class="tabla" background="${ctx}/images/Evaluaciones/back.jpg">
		<tr height="30px">
			<td class="" width="60%">&nbsp;Sistema de Evaluaciones:</td>
			<td class="" width="">
				<form:input path="cadena" id="txtCadena"								 
				cssClass="cajatexto" size="40" readonly="true" />			
			</td>			
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table class="tablaMant" bgcolor="#f2f2f2" border="1" style="margin-left:4px;width:90%" bordercolor="#7BC6DB" cellspacing="0" cellpadding="0" >
		<tr>	
			<td colspan="2" width="100%">
			<table border="0" width="717px" class="">
				<tr>
					<td class="" align="center" width="180px">Productos Definidos</td>
					<td class="">&nbsp;</td>
					<td class="" align="center"  width="180px">Productos Asignados</td>
				</tr>
				<tr>				
					<td align="center" valign="top">
						<form:select  path="cursosHibrido" id="cboCursosHibrido" cssClass="cajatexto" 
							cssStyle="height:150px;width:250px" multiple="true">
					        <c:if test="${control.listaCursosHibrido!=null}">
				        	<form:options itemValue="codCurso" itemLabel="descripcion" 
				        		items="${control.listaCursosHibrido}" />						        
				            </c:if>
						</form:select>						
					</td>
					<td align="center" valign="center">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgadelante','','${ctx}/images/iconos/avanzar2.jpg',1)">									
						<img src="${ctx}/images/iconos/avanzar.jpg" alt="Agregar" onclick="javascript:Fc_Mover('right')" id="imgadelante" style="cursor:hand"></a><br><br>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgatras','','${ctx}/images/iconos/retroceder2.jpg',1)">
						<img src="${ctx}/images/iconos/retroceder1.jpg" alt="Quitar" onclick="javascript:Fc_Mover('left')" id="imgatras" style="cursor:hand"></a><br><br>
					</td>
					<td align="center" valign="top">
						<form:select  path="cursosHibridoSel" id="cboCursosHibridoSel" cssClass="cajatexto_o" 
							cssStyle="height:150px;width:250px" multiple="true">
					        <c:if test="${control.listaCursosHibridoSel!=null}">
				        	<form:options itemValue="codCurso" itemLabel="descripcion" 
				        		items="${control.listaCursosHibridoSel}" />						        
				            </c:if>
						</form:select>						
					</td>									
				</tr>
			</table>
			</td>	
		</tr>
		
	</table>
	<br>
	<table align=center>
		<tr>
			<td><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" onclick="javascript:Fc_Grabar();" id="imggrabar" style="cursor:pointer"></a></td>
			<td><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
			<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" onclick="javascript:Fc_Cancelar();" id="imgcancelar" style="cursor:hand"></a></td>
		</tr>
	</table>
</form:form>
</body>
</html>