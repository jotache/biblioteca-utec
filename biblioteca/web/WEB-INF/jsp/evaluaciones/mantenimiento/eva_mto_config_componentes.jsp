<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script language=javascript>
function onLoad(){
  var objProducto = "<%=((request.getAttribute("Producto")==null)?"":(String)request.getAttribute("Producto"))%>";
  var objGuardar = "<%=((request.getAttribute("GUARDAR")==null)?"":(String)request.getAttribute("GUARDAR"))%>";
     
  if(objProducto!=""){ 
  fc_CambiaProducto(document.getElementById("codProducto").value);
  }
  if(objGuardar!= "")
  {  if(objGuardar == "OK"){
      objGuardar = "";
	  alert(mstrSeGraboConExito);
     }
     else{  objGuardar = "";
     alert(mstrProblemaGrabar);}
  }
}

    function fc_Regresar(){
		parent.location.href =  "${ctx}/menuEvaluaciones.html";
	}
	
   	function fc_Producto(){
    fc_BuscarByProducto(document.getElementById("codProducto").value);
   
	}
	   	    
	function fc_Grabar(){
	 nroSelec= fc_GetNumeroSeleccionados();
	 if(document.getElementById("txhTipoProducto").value =="PFR")
		{ frog=fc_ValidarPFR();
		  if(frog=="1")
		      {   if ( nroSelec != "0" )
					{ if(confirm(mstrSeguroGrabar))
									{  
									    document.getElementById("txhNroSelec").value = nroSelec;
										document.getElementById("txhOperacion").value = "GRABAR";
										document.getElementById("frmMain").submit();
									}
					}
					else alert(mstrSeleccione);
			  }
		 
	   }
	   else{ 
	        if(document.getElementById("txhTipoProducto").value =="PCC_I")
		    {   frog=fc_ValidarPCC1();
			   	if(frog=="1")
				{ if ( nroSelec != "0" )
				        {	if(confirm(mstrSeguroGrabar))
						    {  
							  document.getElementById("txhNroSelec").value = nroSelec;
							  document.getElementById("txhOperacion").value = "GRABAR";
							  document.getElementById("frmMain").submit();
							}
						}
						else alert(mstrSeleccione);
				 }
		       
		   }
		   else{ if(document.getElementById("txhTipoProducto").value =="PCC2")
			    {   frog=fc_ValidarPCC2();
				  	if(frog=="1")
					{
						 if ( nroSelec != "0" )
						    { if(confirm(mstrSeguroGrabar))
											{   document.getElementById("txhNroSelec").value = nroSelec;
												document.getElementById("txhOperacion").value = "GRABAR";
												document.getElementById("frmMain").submit();
											}
							}
							else alert(mstrSeleccione);
					 }
				   
				}
			  else{ if(document.getElementById("txhTipoProducto").value =="PCC3")
				     {  frog=fc_ValidarPCC3();
					    if(frog=="1")
						{  if ( nroSelec != "0" )
						    { if(confirm(mstrSeguroGrabar))
							     {      document.getElementById("txhNroSelec").value = nroSelec;
												document.getElementById("txhOperacion").value = "GRABAR";
												document.getElementById("frmMain").submit();
								 }
							}
						else alert(mstrSeleccione);
					    }
				       
				     }
			        else{ if(document.getElementById("txhTipoProducto").value =="CAT")
					       { frog=fc_ValidarCAT();
						     if(frog=="1")
						     { if ( nroSelec != "0" )
						            {	if(confirm(mstrSeguroGrabar)){  
													    document.getElementById("txhNroSelec").value = nroSelec;
														document.getElementById("txhOperacion").value = "GRABAR";
														document.getElementById("frmMain").submit();
													}
									}
									else alert(mstrSeleccione);
							  }
						  }
			            else{ if(document.getElementById("txhTipoProducto").value =="PAT")
					          {  frog=fc_ValidarPAT();
						         if(frog=="1")
						         {	 if ( nroSelec != "" )
								    {	if(confirm(mstrSeguroGrabar))
									        {  		    document.getElementById("txhNroSelec").value = nroSelec;
														document.getElementById("txhOperacion").value = "GRABAR";
														document.getElementById("frmMain").submit();
											}
									}
									else alert(mstrSeleccione);
							     }
						        
						      }
						     else{ if(document.getElementById("txhTipoProducto").value =="PIA")
							        {   frog=fc_ValidarPIA();
								       	if(frog=="1")
										{ if ( nroSelec != "0" )
										    { if(confirm(mstrSeguroGrabar))
										        {   document.getElementById("txhNroSelec").value = nroSelec;
													document.getElementById("txhOperacion").value = "GRABAR";
													document.getElementById("frmMain").submit();
												}
											}
											else alert(mstrSeleccione);
									    }
								   
								    }
							   
						     else{ if(document.getElementById("txhTipoProducto").value =="PROGRA_ESPECIALIZADO")
								   {   frog=fc_ValidarPE();
									  
									   if(frog=="1")
									   { if ( nroSelec != "0" )
									            {	if(confirm(mstrSeguroGrabar))
												     {  document.getElementById("txhNroSelec").value = nroSelec;
														document.getElementById("txhOperacion").value = "GRABAR";
														document.getElementById("frmMain").submit();
												     }
												}
												else alert(mstrSeleccione);
										 }
									
								   }
								  else  { if(document.getElementById("txhTipoProducto").value =="HIBRIDO")
								          {frog=fc_ValidarHIBRIDO();//alert("Nro "+nroSelec);
									    
									      if(frog=="1")
									      { if ( nroSelec != "0" )
									         {	if(confirm(mstrSeguroGrabar))
											                    {   document.getElementById("txhNroSelec").value = nroSelec;
																	document.getElementById("txhOperacion").value = "GRABAR";
																	document.getElementById("frmMain").submit();
																}
										     }
										     else alert(mstrSeleccione);
										  }
									     }//fc_ValidarPCC_CC()
									     else{ if(document.getElementById("txhTipoProducto").value =="TECSUP_VIRTUAL")
									           {    frog=fc_ValidarTECSUP_VIRTUAL();
										      
										        	if(frog=="1")
													{ if ( nroSelec != "0" )
												  	  { if(confirm(mstrSeguroGrabar))
												  	      {   document.getElementById("txhNroSelec").value = nroSelec;
																document.getElementById("txhOperacion").value = "GRABAR";
															document.getElementById("frmMain").submit();
															}
													  }
													 else alert(mstrSeleccione);
											      }
										 
									     		}
									     		else{ if(document.getElementById("txhTipoProducto").value =="PCC_CC")
									                   {  frog=fc_ValidarPCC_CC();
										      
										        	  if(frog=="1")
													  { if ( nroSelec != "0" )
												  	   { if(confirm(mstrSeguroGrabar))
												  	      {   document.getElementById("txhNroSelec").value = nroSelec;
																document.getElementById("txhOperacion").value = "GRABAR";
															document.getElementById("frmMain").submit();
															}
													   }
													   else alert(mstrSeleccione);
											          }
										 
									     		}
									     		
									     		}
									         }
								  	    } 
						        }
			                }
			            }    
		            }
		        }
		  
	        }   
	}	
	
}		
function fc_CambiaProducto(param) 
{   var alianza;
  
   if(param==document.getElementById("txhCodPFR").value)
    alianza='1';
   if(param==document.getElementById("txhCodPCC_I").value)
    alianza='2';
   if(param==document.getElementById("txhCodCAT").value)
    alianza='3';
    if(param==document.getElementById("txhCodPAT").value)
    alianza='4';
    if(param==document.getElementById("txhCodPIA").value)
    alianza='5';
    if(param==document.getElementById("txhCodPE").value)
    alianza='6';
    if(param==document.getElementById("txhCodHIBRIDO").value)
    alianza='7';
    if(param==document.getElementById("txhCodTecsupVirtual").value)
    alianza='8';
    if(param==document.getElementById("txhCodPCC_CC").value)
    alianza='9';	
	switch(alianza)
	{
		case '1':
			document.getElementById("Tabla_PFR").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PFR";
			break;
		
		case '2':
			document.getElementById("Tabla_PCC_ProgramaIntegral").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PCC_I";
			break;
		case '3':
			document.getElementById("Tabla_CAT").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "CAT";
			break;
		case '4':
			document.getElementById("Tabla_PAT").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PAT";
			break;
		case '5':
			document.getElementById("Tabla_PIA").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PIA";
			break;

		case '6':
			document.getElementById("Tabla_PROGRA_ESPECIALIZADO").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PROGRA_ESPECIALIZADO";
			break;
		case '7':
			document.getElementById("Tabla_HIBRIDO").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "HIBRIDO";
			break;
		case '8':
			document.getElementById("txhTipoProducto").value = "TECSUP_VIRTUAL";
			document.getElementById("Tabla_TECSUP_VIRTUAL").style.display = 'inline-block';
			break;//Tabla_PCC
		case '9':
			document.getElementById("Tabla_PCC_CursoCorto").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PCC_CC";
			break;
			
	}	
}

function fc_BuscarByProducto(param) 
{  
   var alianza;
  
   if(param==document.getElementById("txhCodPFR").value)
    alianza='1';
   if(param==document.getElementById("txhCodPCC_I").value)
    alianza='2';
   if(param==document.getElementById("txhCodCAT").value)
    alianza='3';
    if(param==document.getElementById("txhCodPAT").value)
    alianza='4';
    if(param==document.getElementById("txhCodPIA").value)
    alianza='5';
    if(param==document.getElementById("txhCodPE").value)
    alianza='6';
    if(param==document.getElementById("txhCodHIBRIDO").value)
    alianza='7';
    if(param==document.getElementById("txhCodTecsupVirtual").value)
    alianza='8';
    if(param==document.getElementById("txhCodPCC_CC").value)
    alianza='9';
    if(param=="-1")
    alianza='10';
    
   switch(alianza)
	{   
		case '1':
		    document.getElementById("txhOperacion").value = "PFR";
	        document.getElementById("frmMain").submit();
	        break;
		
		case '2':
			document.getElementById("txhOperacion").value = "PCC_I";
			document.getElementById("frmMain").submit();
			break;
		case '3':
			document.getElementById("txhOperacion").value = "CAT";
			document.getElementById("frmMain").submit();
			
			break;
		case '4':
			document.getElementById("txhOperacion").value = "PAT";
			document.getElementById("frmMain").submit();
			
			break;
		case '5':
			document.getElementById("txhOperacion").value = "PIA";
			document.getElementById("frmMain").submit();
			break;

		case '6':
			document.getElementById("txhOperacion").value = "ESPECIALIZACION";
			document.getElementById("frmMain").submit();
			break;
		case '7':
			document.getElementById("txhOperacion").value = "HIBRIDO";
			document.getElementById("frmMain").submit();
			break;
	    case '8':
			document.getElementById("txhOperacion").value = "TECSUP_VIRTUAL";
			document.getElementById("frmMain").submit();
			break;
		case '9':
			document.getElementById("txhOperacion").value = "PCC_CC";
			document.getElementById("frmMain").submit();
			break;
		case '10':
		    document.getElementById("txhOperacion").value = " ";
			document.getElementById("frmMain").submit();
			break;
	}	
}

    function fc_CursoPFR(){
	document.getElementById("txhOperacion").value = "CursoPFR";
	document.getElementById("frmMain").submit();
	}
	
	function fc_EspecialidadPFR(){
	document.getElementById("txhOperacion").value = "EspecialidadPFR";
	document.getElementById("frmMain").submit();
	}
	function fc_CicloPFR(){
	document.getElementById("txhOperacion").value = "CicloPFR";
	document.getElementById("frmMain").submit();
	}
	
	function fc_CursoCAT(){
	document.getElementById("txhOperacion").value = "CursoCAT";
	document.getElementById("frmMain").submit();
	}
	
	function fc_CicloCAT(){
	document.getElementById("txhOperacion").value = "CicloCAT";
	document.getElementById("frmMain").submit();
	}
	function fc_ProgramaPE(){
	document.getElementById("txhOperacion").value = "ProgramaPE";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPE(){
	document.getElementById("txhOperacion").value = "CursoPE";
	document.getElementById("frmMain").submit();
	}
	function fc_ModuloPE(){
	document.getElementById("txhOperacion").value = "ModuloPE";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPAT(){
	document.getElementById("txhOperacion").value = "CursoPAT";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPIA(){
	document.getElementById("txhOperacion").value = "CursoPIA";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoHibrido(){
	document.getElementById("txhOperacion").value = "CursoHibrido";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoTECSUPVIRTUAL(){
	document.getElementById("txhOperacion").value = "CursoTecsupVirtual";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPCC1()
	{ if(document.getElementById("codProducto").value == document.getElementById("txhCodPCC_I").value)
	  	{	document.getElementById("txhOperacion").value = "CursoPCC_I";
	  		document.getElementById("frmMain").submit();
	  	}
	  else{ if(document.getElementById("codProducto").value == document.getElementById("txhCodPCC_CC").value) 
	        { document.getElementById("txhOperacion").value = "CursoPCC_CC";
	  		  document.getElementById("frmMain").submit();
	  		}
	  }
	}
	
	function fc_ProgramaIntegralPCC1(){
	  document.getElementById("txhOperacion").value = "ProgramaIntegralPCC";
	  document.getElementById("frmMain").submit();
	}
	
function fc_ValidarPFR(){
   
   if(document.getElementById("codSelEspecialidadPFR").value!="-1") 
     {  mad="1";
        if(document.getElementById("codSelCicloPFR").value!="-1" )
          { mad="1";
            if(document.getElementById("codSelCursoPFR").value=="-1"){  
             alert('Debe seleccionar un curso.');
             mad="0";}
          }
        else{ alert('Debe seleccionar un ciclo.');
              mad="0";}
           
     }
   else{ alert('Debe selecionar una especialidad.');  
         mad="0"; }
       
    return mad;     
}	

function fc_ValidarPCC1(){
   
    if(document.getElementById("txhProgramaPCCEtapa").value == "1") 
     { if(document.getElementById("codSelProgramaPCC").value!="-1" )
       { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  
             alert('Debe seleccionar un curso.');
            mad="0";}
       }
       else{ alert('Debe selecionar un programa.');
             mad="0";}
            
         
       return mad;       
     }
    else 
     {
       mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  
             alert('Debe seleccionar un curso.');
            mad="0";}
       
       return mad;
     
     }
       
}	

function fc_ValidarPCC2(){
    
    if(document.getElementById("txhProgramaPCCEtapa").value =="1") 
     { if(document.getElementById("codSelProgramaPCC").value!="-1" )
       { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
       }
       else{ alert('Debe selecionar un programa.');
             mad="0";}
       return mad;       
     }
    else 
     { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
      
      return mad;
      }
   }	

function fc_ValidarPCC3(){
    if(document.getElementById("txhProgramaPCCEtapa").value =="1") 
     { if(document.getElementById("codSelProgramaPCC").value!="-1" )
       { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
       }
       else{ alert('Debe selecionar un programa.');
             mad="0";}
       return mad;       
     }
    else 
     { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
         return mad;
     }
  }	

function fc_ValidarCAT(){
     if(document.getElementById("codSelCursoCAT").value!="-1" )
       { mad="1";
         if(document.getElementById("codSelCicloCAT").value=="-1"){  alert('Debe seleccionar un ciclo.');
            mad="0";}
       }
       else{ alert('Debe seleccionar un curso.');
             mad="0";}
     return mad;     
}	
function fc_ValidarPAT(){
    mad="1";
         if(document.getElementById("codSelCursoPAT").value=="-1"){  alert('Debe seleccionar un ciclo.');
            mad="0";}
    return mad;     
}	

function fc_ValidarPIA(){
    mad="1";
    if(document.getElementById("codSelCursoPIA").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}	

function fc_ValidarHIBRIDO(){
    mad="1";
    if(document.getElementById("codSelCursoHibrido").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}

function fc_ValidarPE(){
   
   if(document.getElementById("codSelProgramaPE").value!="-1") 
     {  mad="1";
        if(document.getElementById("codSelModuloPE").value!="-1" )
          { mad="1";
            if(document.getElementById("codSelCursoPE").value=="-1"){  alert('Debe seleccionar un curso.');
             mad="0";}
          }
        else{ alert('Debe seleccionar un m�dulo.');
              mad="0";}
           
     }
   else{ alert('Debe selecionar un programa.');  
         mad="0"; }  
    return mad;     
}	
function fc_ValidarTECSUP_VIRTUAL(){
    mad="1";
         if(document.getElementById("codSelCursoTECSUPVIRTUAL").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}
function fc_ValidarPCC_CC(){
    mad="1";
         if(document.getElementById("codSelCursosCortosPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}

function fc_seleccion(strCodNIvel1,obj){
	strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
	flag=false;
	if (strCodSel!='')
	{	ArrCodSel = strCodSel.split("|");
		strCodSel = "";
		for (i=0;i<=ArrCodSel.length-2;i++)
		{	if (ArrCodSel[i] == strCodNIvel1)flag = true 
			else strCodSel = strCodSel + ArrCodSel[i]+'|';
			}
	}
	if (!flag)
	{
		strCodSel = strCodSel + strCodNIvel1 + '|';
	}
	
	document.getElementById("txhCodComponentesSeleccionados").value = strCodSel;
	   
}

function fc_GetNumeroSeleccionados()
	{
		strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
		
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			return ArrCodSel.length - 1;
		}
		else return 0;
	}
	
	function fc_SoloUnRegistro()
	{   
		intNumeroSeleccionados = fc_GetNumeroSeleccionados();
		
		if ( intNumeroSeleccionados == 0)	return "";	
		if ( intNumeroSeleccionados > 1)    return "";	
		
		strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
		ArrCodSel = strCodSel.split("|");
		return ArrCodSel[0]; 	
	}
	
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_config_componentes.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="tipoGrabar" id="txhTipoGrabar" />
<form:hidden path="nroSelec" id="txhNroSelec" />
<form:hidden path="codComponentesSeleccionados"	id="txhCodComponentesSeleccionados" />
<form:hidden path="tipoProducto" id="txhTipoProducto" />
<form:hidden path="descripcion" id="txhDescripcion" />
<form:hidden path="programaPCCEtapa" id="txhProgramaPCCEtapa" />
<form:hidden path="valorEtapa" id="txhValorEtapa" />
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
   
<form:hidden path="codPFR" id="txhCodPFR" />
<form:hidden path="codPCC_I" id="txhCodPCC_I" />
<form:hidden path="codPCC_CC" id="txhCodPCC_CC" />
<form:hidden path="codCAT" id="txhCodCAT" />
<form:hidden path="codPAT" id="txhCodPAT" />
<form:hidden path="codPIA" id="txhCodPIA"/>
<form:hidden path="codPE" id="txhCodPE"/>
<form:hidden path="codHIBRIDO" id="txhCodHIBRIDO" />
<form:hidden path="codTecsupVirtual" id="txhCodTecsupVirtual"/>
<form:hidden path="codEtapaProgramaIntegral" id="txhCodEtapaProgramaIntegral"/>
<form:hidden path="codEtapaCursoCorto" id="txhCodEtapaCursoCorto"/>
    
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="94%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>	
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="543px" class="opc_combo"><font style="">Configurar Componentes</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table><tr height="5px"><td></td></tr></table>

	<table cellpadding="0" cellspacing="0" class="tabla" style="width: 96%;margin-left:5px"
		border="0" background="${ctx}/images/Evaluaciones/back.jpg" bordercolor="red">
		<tr height="10px"><td></td></tr>
		<tr height="20px">
			<td class="" width="12%">&nbsp;Producto :</td>
			<td width="38%">&nbsp;<form:select path="codProducto" id="codProducto"
				cssClass="cajatexto" cssStyle="width:260px"
				onchange="javascript:fc_Producto();">
				<form:option value="-1">--Seleccione--</form:option>
				<c:if test="${control.codListaProducto!=null}">
					<form:options itemValue="codProducto" itemLabel="descripcion"
						items="${control.codListaProducto}" />
					<!-- itemValue="codProducto" es el codigo interno q se guarda
					        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
				</c:if>
				</form:select>
			</td>
			<td class="" width="12%" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sede :</td>
			<td width="38%"><form:select path="codSede" id="codSede"
				cssClass="cajatexto" cssStyle="width:150px"
				onchange="javascript:fc_Producto();">
				<c:if test="${control.listaPeriodo!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion"
						items="${control.listaPeriodo}" />
					<!-- itemValue="codProducto" es el codigo interno q se guarda
					        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
				</c:if>
				</form:select>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_PFR"
				name="Tabla_PFR" cellpadding="0" border="0" cellspacing="0" 
				align="center">
				<tr>
					<td class="" width="12%">&nbsp;Especialidad :&nbsp;&nbsp;</td>
					<td>&nbsp;<form:select path="codEspecialidadPFR"
						id="codSelEspecialidadPFR" cssClass="cajatexto"
						cssStyle="width:260px"  onchange="javascript:fc_EspecialidadPFR();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaEspecialidadPFR!=null}">
							<form:options itemValue="codEspecialidad" itemLabel="descripcion"
								items="${control.codListaEspecialidadPFR}" />
						</c:if>
					</form:select></td>
				</tr>
				<tr>
					<td class="" width="12%">&nbsp;Ciclo :&nbsp;</td>
					<td width="38%">&nbsp;<form:select path="codSelCicloPFR" id="codSelCicloPFR"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CicloPFR();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCicloPFR!=null}">
							<form:options itemValue="codCiclo" itemLabel="codCiclo"
								items="${control.codListaCicloPFR}" />
						</c:if>
					</form:select></td>
					<td class="" width="12%" align="right">&nbsp;Cursos :&nbsp;</td>
					<td width="38%"><form:select path="codSelCursoPFR" id="codSelCursoPFR"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoPFR();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPFR!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPFR}" />
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_CAT"
				name="Tabla_CAT" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td class="" width="12%">&nbsp;Ciclo:&nbsp;</td>
					<td width="38%">&nbsp;<form:select path="codSelCicloCAT"
						id="codSelCicloCAT" cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CicloCAT();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCicloCAT!=null}">
							<form:options itemValue="codCiclo" itemLabel="codCiclo"
								items="${control.codListaCicloCAT}" />
						</c:if>
					</form:select></td>
					<td class="" width="12%" align="right">&nbsp;Cursos:&nbsp;</td>
					<td width="38%"><form:select path="codSelCursoCAT" id="codSelCursoCAT"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoCAT();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoCAT!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoCAT}" />
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<!-- td colspan="2">
		<table style="display: none; width: 100%" id="Tabla_PCC"
			name="Tabla_PCC" cellpadding="0" cellspacing="0" border="0"
			align="center">
			<td width="30%">	
			<table style="display: none; width: 100%" id="Tabla_PCC_ETAPA"
				name="Tabla_PCC_ETAPA" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<td width="12%">&nbsp;Etapa:</td>
				<td align="left" width="4%"><input Type="radio" checked name="rdoEtapa"
				    <c:if test="${control.valorEtapa==control.codEtapaProgramaIntegral}"><c:out value=" checked=checked " /></c:if>
					id="gbradio1" onclick="javascript:fc_Etapa('1');"></td>
				<td align="left">&nbsp;Programa Integral&nbsp;<input Type="radio" name="rdoEtapa"
					<c:if test="${control.valorEtapa==control.codEtapaCursoCorto}"><c:out value=" checked=checked " /></c:if>
					id="gbradio2" onclick="javascript:fc_Etapa('2');">&nbsp;&nbsp;Cursos Cortos</td>
			</table>
			</td>

		</table> Tabla_PCC_PI Tabla_PCC_Curso
		</td-->
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_PCC_ProgramaIntegral" 
			    name="Tabla_PCC_ProgramaIntegral" cellpadding="0" cellspacing="0" border="0" align="left">
				<tr>
				    <td class="" width="12%">&nbsp;Programa<br> &nbsp;Integral:</td>
					<td width="38%" align="left">&nbsp;<form:select
							    path="codSelProgramaPCC" id="codSelProgramaPCC"
								cssClass="cajatexto" cssStyle="width:260px"
								onchange="javascript:fc_ProgramaIntegralPCC1();">
                                <form:option value="-1">--Seleccione--</form:option>
								<c:if test="${control.codListaProgramaPCC!=null}">
								<form:options itemValue="codPrograma" itemLabel="descripcion"
								items="${control.codListaProgramaPCC}" />
							    </c:if>
						  </form:select>
					</td>
					<td class="" align="right" width="12%">&nbsp; Cursos:&nbsp;</td>
					<td align="left" width="38%"><form:select path="codSelCursoPCC" id="codSelCursoPCC"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoPCC1();">
                        <form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPCC!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPCC}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
						        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
						</form:select>
					</td>
				</tr>
			</table>
			</td>
		</tr>           
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%"
				id="Tabla_PROGRA_ESPECIALIZADO" name="Tabla_PROGRA_ESPECIALIZADO"
				cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td class="" width="12%">&nbsp;Programa :&nbsp;&nbsp;</td>
					<td width="38%">&nbsp;<form:select path="codSelProgramaPE" id="codSelProgramaPE"
						cssClass="cajatexto" cssStyle="width:260px" onchange="javascript:fc_ProgramaPE();" >
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaProgramaPE!=null}">
							<form:options itemValue="codPrograma" itemLabel="descripcion"
								items="${control.codListaProgramaPE}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
							        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</tr>
				<tr>
					<td class="" width="12%">&nbsp;Modulo:&nbsp;&nbsp;</td>
					<td width="38%">&nbsp;<form:select path="codSelModuloPE" id="codSelModuloPE"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_ModuloPE();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaModuloPE!=null}">
							<form:options itemValue="codCiclo" itemLabel="codCiclo"
								items="${control.codListaModuloPE}" />
						</c:if>
					</form:select></td>
					<td class="" width="12%" align="right">&nbsp;Cursos:&nbsp;&nbsp;</td>
					<td width="38%"><form:select path="codSelCursoPE" id="codSelCursoPE"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoPE();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPE!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPE}" />
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_PIA"
				name="Tabla_PIA" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td class="" width="12%">&nbsp;Cursos:&nbsp;&nbsp;</td>
					<td>&nbsp;<form:select path="codSelCursoPIA" id="codSelCursoPIA"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoPIA();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPIA!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPIA}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
							        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_PAT"
				name="Tabla_PAT" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td class="" width="12%">&nbsp;Cursos :&nbsp;&nbsp;</td>
					<td>&nbsp;<form:select path="codSelCursoPAT" id="codSelCursoPAT"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoPAT();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPAT!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPAT}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
							        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_HIBRIDO"
				name="Tabla_HIBRIDO" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td class="" width="12%">&nbsp;Cursos:&nbsp;&nbsp;</td>
					<td>&nbsp;<form:select path="codSelCursoHibrido"
						id="codSelCursoHibrido" cssClass="cajatexto"
						cssStyle="width:260px" onchange="javascript:fc_CursoHibrido();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoHibrido!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoHibrido}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
								        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_TECSUP_VIRTUAL"
				name="Tabla_TECSUP_VIRTUAL" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td class="" width="12%">&nbsp;Cursos:&nbsp;&nbsp;</td>
					<td>&nbsp;<form:select path="codSelCursoTECSUPVIRTUAL" id="codSelCursoTECSUPVIRTUAL"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoTECSUPVIRTUAL();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoTECSUPVIRTUAL!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoTECSUPVIRTUAL}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
							        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<table style="display: none; width: 100%" id="Tabla_PCC_CursoCorto" 
			    name="Tabla_PCC_CursoCorto" cellpadding="0" cellspacing="0" border="0" align="left">
				<tr>		
					<td class="" align="left" width="90px">&nbsp;Cursos:&nbsp;</td>
					<td align="left"> 
					     <form:select path="codSelCursosCortosPCC" id="codSelCursosCortosPCC"
							cssClass="cajatexto" cssStyle="width:260px; margin-top: 2px"
							onchange="javascript:fc_CursoPCC1();">
	                        <form:option value="-1">--Seleccione--</form:option>
							<c:if test="${control.codListaCursoPCC!=null}">
								<form:options itemValue="codCurso" itemLabel="descripcion"
									items="${control.codListaCursoPCC}" />
							</c:if>
						</form:select>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	
	<div style="overflow: auto; height: 190px;width:99%">
	<table cellpadding="0" cellspacing="1" class="" 
		 style="border: 1px solid #048BBA;width:97%;margin-left:5px; margin-top: 10px">
		<TR height="20px">
			<TD class="headtabla" width="2%">&nbsp;Sel.</TD>
			<TD class="headtabla" width="50%">&nbsp;Componente</TD>

		</TR>
	
		<c:forEach var="objCast" items="${control.listaComponentes}" varStatus="loop">
				<c:choose>
					<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
					<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
				</c:choose>
					
				<td align="center"><input type="checkbox" name="com"
					<c:if test="${objCast.flag=='1'}"><c:out value=" checked=checked " /></c:if>
					id='com<c:out value="${objCast.codComponente}"/>'
					value='<c:out value="${objCast.codComponente}"/>'
					onclick="javascript:fc_seleccion(this.value,'<c:out value="${objCast.comDescripcion}" />');">
				</td>
				<td align="left">&nbsp;<c:out
					value="${objCast.comDescripcion}" /></td>

			</tr>
		</c:forEach>		
	</table></div>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN"
		align="center" height="30px" width="98%" style="margin-top: 10px">
		<tr>
			<td align="center">
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src="${ctx}/images/botones/grabar1.jpg" alt="Grabar" onclick="javascript:fc_Grabar();" style="cursor:pointer;" id="imggrabar">
			</a></td>
		</tr>
	</table>
</form:form>
</body>
</html>