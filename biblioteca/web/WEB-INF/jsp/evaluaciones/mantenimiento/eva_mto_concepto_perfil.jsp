<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/prototypeUtils.js" language="JavaScript;"	type="text/JavaScript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{  
				
	   document.getElementById("txhCodProcesosSeleccionados").value= " "; 
	   document.getElementById("txhCodProcesosSeleccionados2").value= " ";
		if ( document.getElementById("txhOperacion").value == "NIVEL2")
		{  
			document.getElementById("tbl_Nivel21").style.display = 'inline-block';	
            
		}
		
		var objNivel ="<%=((request.getAttribute("Nivel")==null)?"":(String)request.getAttribute("Nivel"))%>";
		var objMsg = "<%=((request.getAttribute("Msg1OK")==null)?"":(String)request.getAttribute("Msg1OK"))%>";        //document.getElementById("txhMsg");
		var objMsg2 ="<%=((request.getAttribute("Msg2OK")==null)?"":(String)request.getAttribute("Msg2OK"))%>"; //document.getElementById("txhMsg2");
      
        if(objNivel=="OK"){ document.getElementById("tbl_Nivel21").style.display = 'inline-block';}
         
		if ( objMsg == "OK" )
		{   document.getElementById("txhMsg").value="";
			objMsg = "";
			alert(mstrElimino);
				
		}		
		else if ( objMsg== "ERROR" )
		{
			alert(mstrNoElimino);			
		}

		if ( objMsg2 == "OK" )
		{
			objMsg2= "";
			document.getElementById("tbl_Nivel21").style.display = 'inline-block';
			alert(mstrElimino);
			
		}		
		else if ( objMsg2== "ERROR" )
		{
			alert(mstrNoElimino);			
		}
		
	}
	function fc_Regresar(){
		parent.location.href =  "${ctx}/menuEvaluaciones.html";
	}
	
	function fc_GetNumeroSeleccionados()
	{
		strCodSel = document.getElementById("txhCodProcesosSeleccionados").value;
		
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			return ArrCodSel.length - 1;
		}
		else return 0;
	}
	function fc_SoloUnRegistro(){
		intNumeroSeleccionados = fc_GetNumeroSeleccionados();		
		if ( intNumeroSeleccionados == 0)				
			return "";
				
		if ( intNumeroSeleccionados > 1)		
			return "";	
				
		strCodSel = document.getElementById("txhCodProcesosSeleccionados").value;
		ArrCodSel = strCodSel.split("|");
		return ArrCodSel[0];
	}
	
	function fc_GetNumeroSeleccionados2(){				
		strCodSel = $('txhCodProcesosSeleccionados2').value;
		if(strCodSel != '' ){		
			ArrCodSel = strCodSel.split("|");
			return ArrCodSel.length - 1;
		}
		else return 0;
	}
	
	function fc_SoloUnRegistro2(){	
		intNumeroSeleccionados = fc_GetNumeroSeleccionados2();
		
		if(intNumeroSeleccionados == 0)						
			return "";
			
		if(intNumeroSeleccionados > 1)		
			return "";
				
		strCodSel = document.getElementById("txhCodProcesosSeleccionados2").value;
		ArrCodSel = strCodSel.split("|");
		return ArrCodSel[0]; 	
	}
	
	function fc_MostrarConceptos(){
		tbl_Concepto.style.display = '';		
	}

     function fc_Nivel2(){
	 codSel = fc_SoloUnRegistro();
	 
	 if(codSel != "" )
	  {  document.getElementById("txhCodAreasNivelUno").value = codSel;
		 document.getElementById("txhOperacion").value = "NIVEL2";
		 document.getElementById("frmMain").submit();
	  }
	 else alert(mstrSeleccione);
	  
 		
	}

	function Fc_Agregar_Perfil(){
		Fc_Popup("${ctx}/evaluaciones/eva_mto_agregar_tipo_perfil.html?txhCodPeriodo=" +
		document.getElementById("txhCodPeriodo").value + "&txhCodEvaluador=" +
		document.getElementById("txhCodEvaluador").value ,435,144);
		//alert(document.getElementById("txhCodPeriodo").value + document.getElementById("txhCodEvaluador").value);
	}

	function fc_Agregar_Concepto(){
	    strCod = document.getElementById("txhCodAreasNivelUno").value;
	    strDescNivel1= document.getElementById("txhDscNiveluno").value;
	    Fc_Popup("${ctx}/evaluaciones/eva_mto_agregar_tipo_concepto.html?txhCodAreasNivelUno=" 
	    + strCod + "&txhDscNivelUno=" + strDescNivel1 + "&txhCodPeriodo=" +
		document.getElementById("txhCodPeriodo").value + "&txhCodEvaluador=" +
		document.getElementById("txhCodEvaluador").value,435,144,"");
		
	}
	
	function fc_Modificar_Perfil(){	
		codSel = fc_SoloUnRegistro();
		if(codSel != "" )
			{  Fc_Popup("${ctx}/evaluaciones/eva_mto_concepto_perfil_modificacion.html?txhCodDetalle=" + 
			codSel+ "&txhCodPeriodo=" +
		document.getElementById("txhCodPeriodo").value + "&txhCodEvaluador=" +
		document.getElementById("txhCodEvaluador").value,435,144,"");	
		    }
		else {
		    alert(mstrSeleccione);
		    }	
		}
		function fc_Modificar_Concepto(){
		codSel2 = fc_SoloUnRegistro2();
		 if(codSel2 != "" )
			{ strCod = document.getElementById("txhCodAreasNivelUno").value;
		      strDescNivel1= document.getElementById("txhDscNiveluno").value;
		      strDsc = document.getElementById("txhDscNivelDos").value;
		    
		      Fc_Popup("${ctx}/evaluaciones/eva_mto_concepto_concepto_modificacion.html?txhDscNivelDos=" + 
		      	strDsc + "&txhCodAreasNivelUno=" + strCod + "&txhCodAreasNivelDos=" + codSel2 + 
		      	"&txhDscNivelUno=" + strDescNivel1 + "&txhCodPeriodo=" +
			  $("txhCodPeriodo").value + "&txhCodEvaluador=" +
			  $("txhCodEvaluador").value ,435,144,"");	
			  $("txhCodAreasNivelDos").value ="";	
			  }
		    else {
		     alert(mstrSeleccione);
		     }		
		}
		
		function fc_Eliminar_Perfil(){
		    nroSelec= fc_GetNumeroSeleccionados();		   
			if(nroSelec != "" ){
				if(confirm(mstrSeguroEliminar1)){  
				    $('txhNroSelec').value = nroSelec;
					$('txhOperacion').value = 'ELIMINAR_PERFIL';
					$('frmMain').submit();
				}
			}
			else alert(mstrSeleccione);
		}
		
		function fc_seleccionarRegistro(strCodNIvel1, descripcion){
			var nivelUno = $('txhDscNiveluno');
			nivelUno.value=''+descripcion;
			
			var strCodSel = $('txhCodProcesosSeleccionados').value;
			var flag=false;
			
			if(strCodSel!=''){
				var ArrCodSel = strCodSel.split("|");
				strCodSel = "";
				for (i=0;i<=ArrCodSel.length-2;i++){
					if (ArrCodSel[i] == strCodNIvel1) 
						flag = true					
					else 
						strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
			}
			if(!flag) strCodSel = strCodSel + strCodNIvel1 + '|';
			$('txhCodProcesosSeleccionados').value = strCodSel;
		}		
		
		function fc_seleccionarRegistro2(strCodNIvel1, descripcion){		
			$('txhDscNivelDos').value = ''+descripcion;
			strCodSel = $('txhCodProcesosSeleccionados2').value;			
			flag=false;
			if(strCodSel!=''){			
				ArrCodSel = strCodSel.split("|");
				strCodSel = "";
				for (i=0;i<=ArrCodSel.length-2;i++){
					if (ArrCodSel[i] == strCodNIvel1){ flag = true }
					else strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
			}
			if(!flag)	strCodSel = strCodSel + strCodNIvel1 + '|';
			
			$('txhCodProcesosSeleccionados2').value = strCodSel;
		}
		
		function fc_Eliminar_Concepto(){
			nroSelec= fc_GetNumeroSeleccionados2();
			if( nroSelec != "" ){			
				if(confirm(mstrSeguroEliminar1)){
				    $('txhNroSelec').value = nroSelec;
				   	$('txhOperacion').value = 'ELIMINAR_CONCEPTO';
					$('frmMain').submit();
				    $('txhCodAreasNivelDos').value = '';
			   }
			}
			else alert(mstrSeleccione);			
		}
		
</script>
</head>
	<body leftmargin="5px">
	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_concepto_perfil.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="nroSelec" id="txhNroSelec"/>
	<form:hidden path="tamano" id="txhTamano"/>
	<form:hidden path="codProcesosSeleccionados" id="txhCodProcesosSeleccionados"/>
	<form:hidden path="codProcesosSeleccionados2" id="txhCodProcesosSeleccionados2"/>
	<form:hidden path="codAreasNivelUno" id="txhCodAreasNivelUno"/>
	<form:hidden path="codAreasNivelDos" id="txhCodAreasNivelDos"/>
	<form:hidden path="dscNivelUno" id="txhDscNiveluno"/>
	<form:hidden path="dscNivelDos" id="txhDscNivelDos"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="msg2" id="txhMsg2"/>
	<form:hidden path="mad" id="txhMad"/>
		
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>

	
		<table>
		<tr height="5px"><td></td>
		</tr>
		</table>
		<table cellpadding="0" cellspacing="0" ID="Table1" width="98%" border="0"  class="borde_tabla" style="margin-left:3px">

			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="2">
					Perfiles del Alumno
				</td>		 	
			</tr>

			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 180px;width:99%">
				 	<display:table name="sessionScope.listaAreasIntUno" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" style="border: 1px solid #048BBA;width:97%">
					    <display:column property="chkSelDetalle" title="Sel." style="text-align:center; width:5%"/>
						<display:column property="descripcion" title="1� Nivel" style="text-align:left;width:95%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
							
					  </display:table>
					</div>
				</td>
				<td width="27px" valign="top">
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar_Perfil();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
						<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
							id="imgModificarPerfil" onclick="javascript:fc_Modificar_Perfil();">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
						<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar" style="cursor:pointer" 
							id="imgEliminarPerfil" onclick="javascript:fc_Eliminar_Perfil();">
					</a>
				</td>
			</tr>
		</table>
		<br/>
		<table align=center>
			<tr>
				<td><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgConceptos','','${ctx}/images/botones/conceptos2.jpg',1)">
					<img alt="Conceptos" src="${ctx}/images/botones/conceptos1.jpg"  id="imgConceptos" onclick="javascript:fc_Nivel2();" style="cursor:pointer;"></a></td>
			</tr>
		</table>
		<br/>			
		<table border="0" style="display:none;margin-left:3px" id="tbl_Concepto" cellpadding="0" 
				cellspacing="0" width="80%" class="" >
			<tr>
				<td colspan="2" class="">Perfil:&nbsp;
					<form:input maxlength="10"  path="desPerfil" id="txtDesPerfil" cssClass="cajatexto" cssStyle="HEIGHT: 18px;width:150px" readonly="readonly" />				
				</td>		
			</tr>
		</table>
		<table style="display:none" id="tbl_Nivel21" cellpadding="0" cellspacing="0" width="98%" class="borde_tabla">

			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="3">
					Perfil:&nbsp;
					<form:label path="descripcionNivel1" id="txtDescripcionNivel1">${control.descripcionNivel1}</form:label>
				</td>		 	
			</tr>
			
			<tr>
				<td width="98%">
					<div style="overflow: auto; height:150px;width:99%">
						<display:table name="sessionScope.listaAreasIntDos" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator"  requestURI="" 
						style="border: 1px solid #048BBA;width:97%;margin-left:3px">					
							<display:column property="chkSelDetalle22" title="Sel"  
								style="text-align:center; width:5%"/>
							<display:column property="descripcion" title="2� Nivel"
							style="text-align:left;width:95%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
						</display:table>
					</div>
				</td>
				<td width="27px" valign="top" align="center">
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" id="imgAgregar" onclick="javascript:fc_Agregar_Concepto();" style="cursor:pointer" alt="Agregar" align="middle">
					</a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarConcepto','','${ctx}/images/iconos/actualizar2.jpg',1)">
						<img src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" id="imgModificarConcepto" onclick="javascript:fc_Modificar_Concepto();" style="cursor:pointer" align="middle">
					</a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarConcepto','','${ctx}/images/iconos/quitar2.jpg',1)">
						<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar" style="cursor:pointer" id="imgEliminarConcepto" onclick="javascript:fc_Eliminar_Concepto();" style="cursor:pointer" align="middle">
					</a>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>

</form:form>
</body>
<%request.getSession().removeAttribute("listaAreasIntDos"); %>
</html>