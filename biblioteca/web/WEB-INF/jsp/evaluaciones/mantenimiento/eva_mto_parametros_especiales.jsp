<%@ include file="/taglibs.jsp"%>
<head>
<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
<script language=javascript>
function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	fc_msgManager(mensaje);	
	return true;
}

	function fc_Regresar(){
		parent.location.href =  "${ctx}/menuEvaluaciones.html";
	}

function fc_msgManager(mensaje){	
	if(mensaje!="")
	{
		if(mensaje.length>1)           		
		alert(mensaje);				    
	}
}

	function fc_Nivel2(){
		tbl_Nivel2.style.display = '';		
		tbl_Nivel21.style.display = '';		
	}

	function Fc_Agregar(){
		Fc_Popup("Agregar_competencia.htm",350,120);	
	}

	function Fc_Agregar2(){
		Fc_Popup("Agregar_competencia.htm",350,140);	
	}
	
	function fc_validaEscala(){
		var exe = Number(fc_Trim(document.getElementById("txtExelente").value));
		var bue = Number(fc_Trim(document.getElementById("txtBuena").value));
		var med = Number(fc_Trim(document.getElementById("txtMedia").value));
		var baj = Number(fc_Trim(document.getElementById("txtBaja").value));
		
		if( exe < bue )
		{	alert('Ingrese un orden correcto\npara escala');
			document.getElementById("txtExelente").focus();
		}else{
			if( bue<med )
			{	alert('Ingrese un orden correcto\npara escala');
				document.getElementById("txtBuena").focus();
			}else{
				if( med<baj ){
					alert('Ingrese un orden correcto\npara escala');
					document.getElementById("txtMedia").focus();
				}else 
					return 1;	
			}
		}
		return 0;
		
	
	}
	
	function fc_grabar(){
		
		form = document.forms[0];
		
		if(form.operacion.value=="irActualizar" || form.operacion.value=="irRegistrar"  )
		{
				if(valida())
				{
					objCombo = document.getElementById("cboCicloCatSel");
					
					//VALIDACION DE COMBOS
					/*if(objCombo.options.length==0)
					{	alert("Debe agregar ciclos");
						return 0;
					}*/					
					
					if(!fc_validaEscala())
					return 0;
					
					if(confirm(mstrSeguroGrabar))
					{			
					tkn = "";		
							
					form.nroRegistros.value = objCombo.options.length;
			

			
					for ( var i = 0; i < objCombo.options.length ; i++)
					{				
						tkn = tkn + objCombo.options[i].value+"|";
					}	
			
					if(tkn.length>0)
						tkn = tkn.substring(0,tkn.length-1);
				
		
						
					form.tknCiclos.value = tkn;					
					form.submit();
					}
				}
			
		}else
			alert('Seleccione un per�odo');
		
	}
	

			function fc_Mover(side){
			var origen;//origen
			var destino;//destino
			if (side == "right")
			{ 
				origen = document.getElementById("cboCicloCat");
				destino = document.getElementById("cboCicloCatSel");
			}
			else
			{  
				origen = document.getElementById("cboCicloCatSel");
				destino = document.getElementById("cboCicloCat"); 
			}
			var idSel=-1;
			for (var i = 0; i < origen.length; i++)
			{  
				if ( origen.options[i].selected )
				{
					idSel=	i;
				}
			}
			if(idSel==-1)
			{
				alert('Debe Seleccionar un valor');
				return;
			}
			var NroItemsDestino=destino.length;
			var oOption = document.createElement('OPTION');
			destino.options.add(oOption);
			oOption.value = origen.options[idSel].value;
			oOption.innerText = origen.options[idSel].text;
			for(i=idSel;i < origen.length-1; i++){
				origen.options[i].value=origen.options[i+1].value;
				origen.options[i].text=origen.options[i+1].text;
			}
			origen.length=origen.length-1;
			origen.focus()
			}

function fc_cambiaPeriodo(){
	form = document.forms[0];
	if(form.varperiodo.value!="-1"){
		form.operacion.value="irConsultarPeriodo";
		form.submit();
	}else
		alert("Seleccione Periodo");
}


	function valida(){
	
	/*if(fc_Trim(document.getElementById("txtNroVisitas").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtNroVisitas").focus();return 0;}
	
	if(fc_Trim(document.getElementById("txtNroHoras2").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtNroHoras2").focus();return 0;}
	
	if(fc_Trim(document.getElementById("txtNroHoras3").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtNroHoras3").focus();return 0;}*/
	
	/*if(fc_Trim(document.getElementById("txtMinima").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtMinima").focus();return 0;}*/
	
	if(fc_Trim(document.getElementById("txtExelente").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtExelente").focus();return 0;}
	
	if(fc_Trim(document.getElementById("txtBuena").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtBuena").focus();return 0;}
	
	if(fc_Trim(document.getElementById("txtMedia").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtMedia").focus();return 0;}
	
	if(fc_Trim(document.getElementById("txtBaja").value)=="")
	{alert(mstrLleneLosCampos);document.getElementById("txtBaja").focus();return 0;}
	
	return 1;
	
	}
	
</script>
</head>
<body>
<form:form commandName="control"
	action="${ctx}/eva/mto_parametrosEspeciales.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="tknCiclos" id="tknCiclos" />
	<form:hidden path="nroRegistros" id="nroRegistros" />

	<form:hidden path="hidExelente" id="hidExelente" />
	<form:hidden path="hidBuena" id="hidBuena" />
	<form:hidden path="hidMedia" id="hidMedia" />
	<form:hidden path="hidBaja" id="hidBaja" />
	
	<form:hidden path="codPeriodo" id="codPeriodo" />
	<form:hidden path="codEvaluador" id="codEvaluador" />
	
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="94%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="margin-left: 7px">
		<tr>
			<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="525px"
				class="opc_combo"><font style="">Par�metros Especiales</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<div style="overflow: auto; height:420px;width:740px">
	<table cellpadding="0" cellspacing="0" class="tablaMant" border="1"	bordercolor="#7BC6DB" bgcolor="#f2f2f2" style="margin-top:5px;width:710px;margin-left:8px">
		<tr>
			<td>
				<table cellpadding="3" cellspacing="0" border="0" class="tablaMant" style="width:98%">
					<tr>
						<td class="">Periodo :</td>
						<td><form:select path="varperiodo" id="varperiodo"
							cssClass="cajatexto" cssStyle="width:150px"
							onchange="fc_cambiaPeriodo()">
							<form:option value="-1">--Seleccione--</form:option>
							<c:if test="${control.lstPeriodo!=null}">
								<form:options itemValue="codigo" itemLabel="nombre"
									items="${control.lstPeriodo}" />
							</c:if>
						</form:select></td>
					</tr>
					<tr>
						<td class="" colspan=3>* Formacion de Empresa (Caso PFR)</td>
					</tr>
					<tr>
						<td class="">&nbsp;</td>
						<td class="" width="30%">Total_horas=&nbsp;</td>
						<td colspan=2><form:input path="txtNroVisitas"
							id="txtNroVisitas"
							onblur="javascript:fc_ValidaNumeroOnBlur('txtNroVisitas');"
							maxlength="3" onkeypress="fc_PermiteNumeros()"
							cssClass="cajatexto" cssStyle="width:30px" />
						&nbsp;Nro_Visitas&nbsp; <form:input path="txtNroHoras2"
							id="txtNroHoras2"
							onblur="javascript:fc_ValidaNumeroOnBlur('txtNroHoras2');"
							maxlength="3" onkeypress="fc_PermiteNumeros()"
							cssClass="cajatexto" cssStyle="width:30px" />
						&nbsp;Nro_Horas_Pasant�a&nbsp; <form:input path="txtNroHoras3"
							id="txtNroHoras3"
							onblur="javascript:fc_ValidaNumeroOnBlur('txtNroHoras3');"
							maxlength="3" onkeypress="fc_PermiteNumeros()"
							cssClass="cajatexto" cssStyle="width:30px" /> &nbsp;Nro_Horas_Pr�cticas
						</td>
	
					</tr>
					<tr>
						<td class="">&nbsp;</td>
						<td class="">Total_horas Minima >=&nbsp;</td>
						<td colspan=2><form:input path="txtMinima" id="txtMinima"
							maxlength="3"
							onblur="javascript:fc_ValidaNumeroOnBlur('txtMinima');"
							onkeypress="fc_PermiteNumeros()" cssClass="cajatexto"
							cssStyle="width:30px" /> &nbsp;</td>
					</tr>
					<tr>
						<td class="" colspan=3>* Caso CAT</td>
					</tr>
	
					<tr>
						<td colspan="3">
						<table border="0" width="400px">
							<tr>
								<td class="" align="center" width="180px">Ciclos en Tecsup</td>
								<td class="">&nbsp;</td>
								<td class="" align="center" width="180px">Ciclos en la
								Empresa</td>
							</tr>
							<tr>
	
								<td align="center" valign="top"><form:select
									path="cboCicloCat" id="cboCicloCat" cssClass="Combo"
									cssStyle="height:80px;width:100px" multiple="true">
									<c:if test="${control.lstCiclos!=null}">
										<form:options itemValue="codCiclo" itemLabel="descripcion"
											items="${control.lstCiclos}" />
									</c:if>
								</form:select></td>
								<td align="center" valign="center"><a
									onmouseout="MM_swapImgRestore()"
									onmouseover="MM_swapImage('imgadelante','','${ctx}/images/iconos/avanzar2.jpg',1)">
								<img src="${ctx}/images/iconos/avanzar.jpg" alt="Agregar"
									onclick="javascript:fc_Mover('right')" id="imgadelante"
									style="cursor: hand"></a><br>
								<br>
								<a onmouseout="MM_swapImgRestore()"
									onmouseover="MM_swapImage('imgatras','','${ctx}/images/iconos/retroceder2.jpg',1)">
								<img src="${ctx}/images/iconos/retroceder1.jpg" alt="Quitar"
									onclick="javascript:fc_Mover('left')" id="imgatras"
									style="cursor: hand"></a><br>
								<br>
								</td>
								<td align="center" valign="top"><form:select
									path="cboCicloCatSel" id="cboCicloCatSel" cssClass="combo_o"
									cssStyle="height:80px;width:100px" multiple="true">
									<c:if test="${control.lstCiclosSel!=null}">
										<form:options itemValue="valor1" itemLabel="descripcionPeriodo"
											items="${control.lstCiclosSel}" />
									</c:if>
								</form:select></td>
							</tr>
	
						</table>
					</td>
				</tr>
				<tr>
					<td class="" colspan=3>* Escala para Registro de
					Perfiles/Competencia</td>
				</tr>
				<tr>
					<td width="3%">&nbsp;</td>
					<td class="" width="15%">Excelente&nbsp;</td>
					<td class="">&nbsp; <form:input path="txtExelente"
						id="txtExelente"
						onblur="javascript:fc_ValidaNumeroOnBlur('txtExelente');"
						maxlength="2" onkeypress="fc_PermiteNumeros()"
						cssClass="cajatexto" cssStyle="width:50px" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="">Buena&nbsp;</td>
					<td class="">&nbsp; <form:input path="txtBuena" id="txtBuena"
						onblur="javascript:fc_ValidaNumeroOnBlur('txtBuena');"
						maxlength="2" onkeypress="fc_PermiteNumeros()"
						cssClass="cajatexto" cssStyle="width:50px" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="">Media&nbsp;</td>
					<td class="">&nbsp; <form:input path="txtMedia" id="txtMedia"
						onblur="javascript:fc_ValidaNumeroOnBlur('txtMedia');"
						maxlength="2" onkeypress="fc_PermiteNumeros()"
						cssClass="cajatexto" cssStyle="width:50px" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="">Baja&nbsp;</td>
					<td class="">&nbsp; <form:input path="txtBaja" id="txtBaja"
						maxlength="2"
						onblur="javascript:fc_ValidaNumeroOnBlur('txtBaja');"
						onkeypress="fc_PermiteNumeros()" cssClass="cajatexto"
						cssStyle="width:50px" /></td>
				</tr>
			</table>
			</td>
		</tr>
	</table><br>
	<table border="0" width="95%" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center"><a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg"
				id="imggrabar" style="cursor: pointer" onclick="fc_grabar()"></a>
			</td>
		</tr>
	</table>
	</div>
	
</form:form>

</body>

