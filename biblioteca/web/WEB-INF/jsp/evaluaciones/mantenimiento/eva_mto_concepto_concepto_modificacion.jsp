<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>		
	<script language=javascript>
    function onLoad()
	{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();			
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "ERROR" )
		{
		alert(mstrProblemaGrabar);
		}
	}

	function fc_Modificar(){
	    
		objDescripcion = document.getElementById("txtDscProceso");
		if (fc_Trim(objDescripcion.value) == "")
		{
			alert('Debe ingresar una descripción.');
			return;
		}
		document.getElementById("txhOperacion").value = "MODIFICAR";
		document.getElementById("frmMain").submit();
	
	}
   function fc_cancelar(){
	window.opener.document.getElementById("frmMain").submit();
	}
</script>	
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form action="${ctx}/evaluaciones/eva_mto_concepto_concepto_modificacion.html" commandName="control" id="frmMain">
<form:hidden path="valor1" id="txhValor1"/>
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codDetalle" id="txhCodDetalle"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="400px" class="opc_combo"><font style="">Modificar Concepto del Perfil</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		 </table>		
		<table  style="width:98%;margin-top:5px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray' align="center"> 
			<tr>
				<td valign='top' colspan="2">
					<table class="tablaflotante" cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="30px"> 
						<tr>
							<td nowrap width="60%">Perfil:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		
						        <form:input path="descripcionNivel1" id="txtDescripcionNivel1"
								onkeypress="fc_ValidaTextoEspecial1();"
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'denominacion');" 
								 cssClass="cajatexto" size="40" readonly="true"/>
								<!-- input name="color" value="Imagen Personal" class="cajatexto_1" style="HEIGHT: 18px;width:150px" readonly -->
							</td>
						</tr>
						<tr>
							<td nowrap width="30%">Concepto:&nbsp;			
								<form:input path="descripcion" id="txtDscProceso" maxlength="50"
								onkeypress="fc_ValidaTextoEspecial1();"
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'denominacion');" 
								 cssClass="cajatexto" size="40" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Modificar();"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">					
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();fc_cancelar();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
	</form:form>
	</body>
</html>