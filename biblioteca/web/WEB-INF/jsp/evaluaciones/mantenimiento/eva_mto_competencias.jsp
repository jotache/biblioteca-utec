<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}"	scope="request" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad(){		
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "IGUAL" ){
			alert("No se puede quitar la competencia por que esta siendo utilizada.");			
		}
		else if ( objMsg.value == "ERROR" ){
			alert("Error al quitar registro.");
			window.close();
		}
	}
	function fc_Regresar(){
		parent.location.href =  "${ctx}/menuEvaluaciones.html";
	}
	function Fc_Agregar(){
		if(document.getElementById("txhValorTope").value==100){
			alert('No puede ingresar m�s registros.');
			return;
		}
		codPeriodo=document.getElementById("txhCodPeriodo").value;
		codEvaluador=document.getElementById("txhCodEvaluador").value;
		valorTope=document.getElementById("txhValorTope").value;
		Fc_Popup("${ctx}/evaluaciones/eva_mto_agregar_competencia.html?val="+valorTope+"&txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador,400,150,"");	
	}
	function Fc_Quitar(){		
		if ($('txhSeleccion').value != ''){
			if( confirm(mstrEliminar) ){
				$('txhOperacion').value = 'ELIMINAR';
				$('frmMain').submit();
			}			
		}else{
			alert('Seleccione un registro');
			return false;
		}
	}
	function Fc_Modificar(){
		if ($('txhSeleccion').value != ''){
			codPeriodo=$('txhCodPeriodo').value;
			codEvaluador=$('txhCodEvaluador').value;
			strCod = $('txhSeleccion').value
		    var val= parseInt($('txhValorTope').value)-parseInt($('txhPorcentaje').value);
		    Fc_Popup("${ctx}/evaluaciones/eva_mto_agregar_competencia.html?val=" + val + "&txhCodDetalle=" + strCod + "&txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador,400,160,"");
		}else{
	   		alert('Seleccione un registro');
	   		return false;
		}
	}		
	function fc_seleccionarRegistro(seleccion, porcentaje){		
		$('txhSeleccion').value = ''+seleccion;
		$('txhPorcentaje').value = ''+porcentaje;
	}
	</script>
</head>
<body topmargin="0" leftmargin="10" rightmargin="0">
	<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_competencias.html">
	<form:hidden path="descripcion" id="txhDescripcion" />
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="porcentaje" id="txhPorcentaje" />
	<form:hidden path="valorTope" id="txhValorTope" />
	<form:hidden path="codPeriodo" id="txhCodPeriodo" />
	<form:hidden path="codEvaluador" id="txhCodEvaluador" />
	
	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	
	
	<table cellpadding="0" cellspacing="0" ID="Table1" width="98%" >
		<tr>
			<td>
			<div style="overflow: auto; height: 250px;width:99%">
			<table cellpadding="0" cellspacing="0" ID="Table1" class="borde_tabla" width="97%" style="border: 1px solid #048BBA;">

				<tr class="fondo_cabecera_azul">		 
				 	<td class="titulo_cabecera" width="100%" colspan="3">
						Competencias del Alumno
					</td>		 	
				</tr>

				<tr height="20px">
					<td class="headtabla" width="5%">&nbsp;Sel.</td>
					<td class="headtabla" width="80%">&nbsp;Competencias</td>
					<td class="headtabla" width="15%">&nbsp;%</td>
				</tr>
				<c:forEach var="lista" items="${control.listAreasNivelUno}" varStatus="status">
				
					<c:choose>
					  <c:when test="${status.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
					  <c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>				
					
					<!-- tr class="texto"-->
						<td align="center" ><input type="radio"
							name="codProceso"
							value='<c:out value="${lista.codTipoTablaDetalle}" />'
							onclick="fc_seleccionarRegistro(this.value,'<c:out value="${lista.dscValor1}" />');">
						</td>
						<td style="text-align: left;">&nbsp; <c:out
							value="${lista.descripcion}" /></td>
						<td align="center" ><c:out
							value="${lista.dscValor1}" /></td>
					</tr>
				</c:forEach>
			</table>
			</div>
			</td>
			<td width="27px" valign="top"><a
				onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
			&nbsp;<img src="${ctx}/images/iconos/agregar1.jpg" id="imgagregar"
				onclick="javascript:Fc_Agregar();" style="cursor: pointer"
				alt="Agregar"></a> <a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imgactualizar','','${ctx}/images/iconos/actualizar2.jpg',1)">
			&nbsp;<img src="${ctx}/images/iconos/actualizar1.jpg"
				id="imgactualizar" alt="Modificar" style="cursor: pointer"
				onclick="javascript:Fc_Modificar();"></a> <a
				onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/iconos/quitar2.jpg',1)">
			&nbsp;<img src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar"
				id="imgEliminar" style="cursor: pointer"
				onclick="javascript:Fc_Quitar();"></a></td>
		</tr>
	</table>
</form:form>
</body>
</html>