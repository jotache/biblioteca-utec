<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
<head>
		
	<script language=javascript>
	function onLoad()
	{	objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "ERROR" )
		{
			alert(mstrProblemaGrabar);
		}
	}
	function fc_ValidaCero(objTxt,objCbo){
		a = document.getElementById(objTxt);
		b = document.getElementById(objCbo);
		if ( fc_Trim( a.value ) == "" ) return true;
		a.value = fc_Trim( a.value );
		if( fc_Trim( b.value ) == '/' && a.value == 0 ){
			alert('El valor relacionado con el signo \"/\" no puede ser 0.');
			document.getElementById(objCbo).value="";
		}
	}
	function Fc_Grabar(){		
		if(Fc_ValidarCampos()==true){
			alert(mstrLleneLosCampos);
			return;
		}
		else{
			if(Fc_ValidarOperadores()==true){
				alert('Debe seleccionar un tipo en todos los operadores.');
				return;
			}
			else{
				if(document.getElementById("txhAux").value=="MODIFICAR"){
					document.getElementById("txhOperacion").value="MODIFICAR";
				}
				else{
					document.getElementById("txhOperacion").value="GRABAR";
				}
				if(confirm(mstrSeguroGrabar)){
					document.getElementById("frmMain").submit();
				}
			}
		}	
	}
	function Fc_ValidarCampos(){
		var band=false;
		if(document.getElementById("txtCondicion").value==""){
			band=true;
		}
		else{
			if(document.getElementById("txtVerdadero01").value==""){
				band=true;
			}
			else{
				if(document.getElementById("txtVerdadero02").value==""){
					band=true;
				}
				else{
					if(document.getElementById("txtVerdadero03").value==""){
						band=true;
					}
					else{
						if(document.getElementById("txtVerdadero04").value==""){
							band=true;
						}
						else{
							if(document.getElementById("txtFalso01").value==""){
								band=true;
							}
							else{
								if(document.getElementById("txtFalso02").value==""){
									band=true;
								}
								else{
									if(document.getElementById("txtFalso03").value==""){
										band=true;
									}
									else{
										if(document.getElementById("txtFalso04").value==""){
											band=true;
										}														
									}
								}
							}
						}
					}
				}
			}
		}
		return band;
	}
	function Fc_ValidarOperadores(){
		var band=false;
		if(document.getElementById("cboRegla").value==""){
			band=true;
		}
		else{
			if(document.getElementById("cboOpeVer01").value==""){
				band=true;
			}
			else{
				if(document.getElementById("cboOpeVer02").value==""){
					band=true;
				}
				else{
					if(document.getElementById("cboOpeVer03").value==""){
						band=true;
					}
					else{
						if(document.getElementById("cboOpeVer04").value==""){
							band=true;
						}
						else{
							if(document.getElementById("cboOpeFal01").value==""){
								band=true;
							}
							else{
								if(document.getElementById("cboOpeFal02").value==""){
									band=true;
								}
								else{
									if(document.getElementById("cboOpeFal03").value==""){
										band=true;
									}
									else{
										if(document.getElementById("cboOpeFal04").value==""){
											band=true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return band;
	}
	</script>
</head>

<body topmargin="0" leftmargin="0 rightmargin="0">
<form:form action="${ctx}/evaluaciones/eva_mto_regla_nota_externa.html" commandName="control" id="frmMain">
	<form:hidden path="codCurso" id="txhCodCurso"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="aux" id="txhAux"/>
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="550px" class="opc_combo"><font style="">Regla Nota Externa</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	 </table>		
	<table cellpadding="2" cellspacing="2" class="tablaflotante" border="0" bordercolor="red" align="center" style="width:98%;margin-top:5px">
		<tr>
			<td style="color:#048BBA" colspan="4" >Curso con Nota Externo</td>
		</tr>
		<tr>
			<td class="texto_bold">&nbsp;</td>
			<td class="texto_bold" >Si   (Nota_Externo &nbsp;</td>
			<td colspan="2" class="texto_bold">
				<form:select path="regla" id="cboRegla" cssClass="cajatexto" cssStyle="width:50px" >
					<form:option value="">-Sel-</form:option>
					<form:option value='>'>></form:option>
					<form:option value='<'><</form:option>
					<form:option value='>='>>=</form:option>
					<form:option value='<='><=</form:option>
					<form:option value='='>=</form:option>					
				</form:select>				
				<form:input path="condicion" id="txtCondicion"
					maxlength="5" cssStyle="text-align: right;"
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtCondicion','2','1');"					 
					cssClass="cajatexto" size="5"/>&nbsp;) Entonces
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align: rigth;" class="texto_bold">&nbsp;&nbsp;&nbsp;Nota_TECSUP=</td>
			<td colspan="2" style="text-align: rigth;" class="texto_bold">
				<form:input path="verdadero01" id="txtVerdadero01"
					maxlength="5" cssStyle="text-align: right;"					 
					onkeypress="fc_PermiteNumerosPunto();"													
					onblur="fc_ValidaDecimalOnBlur('txtVerdadero01','2','1')" 
					cssClass="cajatexto" size="5"/>
				<form:select path="opeVer01" id="cboOpeVer01" cssClass="cajatexto" cssStyle="width:50px" onchange="javascript:fc_ValidaCero('txtVerdadero02','cboOpeVer01');">
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>										
				</form:select>
				<form:input path="verdadero02" id="txtVerdadero02"
					maxlength="5" cssStyle="text-align: right;"
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtVerdadero02','2','1');fc_ValidaCero('txtVerdadero02','cboOpeVer01');" 
					cssClass="cajatexto" size="5"/>
				<form:select path="opeVer02" id="cboOpeVer02" cssClass="cajatexto" cssStyle="width:50px" >
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>
				</form:select>
				<b>(</b>Nota_Externo
				<form:select path="opeVer03" id="cboOpeVer03" cssClass="cajatexto" cssStyle="width:50px" onchange="javascript:fc_ValidaCero('txtVerdadero03','cboOpeVer03');">
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>										
				</form:select>
				<form:input path="verdadero03" id="txtVerdadero03"
					maxlength="5" cssStyle="text-align: right;"
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtVerdadero03','2','1');fc_ValidaCero('txtVerdadero03','cboOpeVer03');" 
					cssClass="cajatexto" size="5"/><b>)</b>
				<form:select path="opeVer04" id="cboOpeVer04" cssClass="cajatexto" cssStyle="width:50px" onchange="javascript:fc_ValidaCero('txtVerdadero04','cboOpeVer04');">
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>										
				</form:select>
				<form:input path="verdadero04" id="txtVerdadero04"
					maxlength="5" cssStyle="text-align: right;"		 
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtVerdadero04','2','1');fc_ValidaCero('txtVerdadero04','cboOpeVer04');" 
					cssClass="cajatexto" size="5"/>
			</td>
		</tr>
		<tr>
			<td class="texto_bold">&nbsp;</td>
			<td class="texto_bold" colspan=3>Sino</td>
		</tr>
		<tr>
			<td class="texto_bold">&nbsp;</td>
			<td class="texto_bold" class="texto_bold">&nbsp;&nbsp;&nbsp;Nota_TECSUP=</td>
			<td colspan="2" class="texto_bold">
				<form:input path="falso01" id="txtFalso01"													
					maxlength="5" cssStyle="text-align:right;"
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtFalso01','2','1')" 
					cssClass="cajatexto" size="5"/>
				<form:select path="opeFal01" id="cboOpeFal01" cssClass="cajatexto" cssStyle="width:50px" onchange="javascript:fc_ValidaCero('txtFalso02','cboOpeFal01');">
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>										
				</form:select>
				<form:input path="falso02" id="txtFalso02"					
					maxlength="5" cssStyle="text-align:right;"
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtFalso02','2','1');fc_ValidaCero('txtFalso02','cboOpeFal01');" 
					cssClass="cajatexto" size="5"/>
				<form:select path="opeFal02" id="cboOpeFal02" cssClass="cajatexto" cssStyle="width:50px">
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>										
				</form:select>
				(Nota_Externo
				<form:select path="opeFal03" id="cboOpeFal03" cssClass="cajatexto" cssStyle="width:50px" onchange="javascript:fc_ValidaCero('txtFalso03','txtFalso03');">
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>										
				</form:select>
				<form:input path="falso03" id="txtFalso03"					
					maxlength="5" cssStyle="text-align:right;" 
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtFalso03','2','1');fc_ValidaCero('txtFalso03','cboOpeFal03');" 
					cssClass="cajatexto" size="5"/><b>)</b>
				<form:select path="opeFal04" id="cboOpeFal04" cssClass="cajatexto" cssStyle="width:50px" onchange="javascript:fc_ValidaCero('txtFalso04','cboOpeFal04');">
					<form:option value="">-Sel-</form:option>
					<form:option value='+'>+</form:option>
					<form:option value='-'>-</form:option>
					<form:option value='*'>*</form:option>
					<form:option value='/'>/</form:option>										
				</form:select>
				<form:input path="falso04" id="txtFalso04"					
					maxlength="5" cssStyle="text-align:right;" 
					onkeypress="fc_PermiteNumerosPunto();"								
					onblur="fc_ValidaDecimalOnBlur('txtFalso04','2','1');fc_ValidaCero('txtFalso04','cboOpeFal04');" 
					cssClass="cajatexto" size="5"/>
			</td>
		</tr>
	</table>
		<br>
	<table border="0" width="100%">
		<tr>
			<td align="center">					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" alt="Grabar" id="imggrabar" onclick="javascript:Fc_Grabar();" style="cursor:pointer;">&nbsp;</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" id="imgcancelar" onclick="javascript:window.close()" style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>	
	
	
	<table>
		<tr>
			<td class="texto_bold">*&nbsp;Si ( Nota_Externo > 16) Entonces</td>
		</tr>
		<tr>
			<td class="texto_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nota_TECSUP&nbsp;= 10.4*2 - (Nota_Externo/2 + 20)</td>
		</tr>
		<tr>
			<td class="texto_bold">&nbsp;&nbsp;Sino Nota_TECSUP&nbsp;= 9 + 2*(Nota_Externo - 8*0.2)</td>
		</tr>
	</table>
</form:form>
</body>
</html>