<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
<script language=javascript>
    function onLoad()
	{   objMsg = document.getElementById("txhMsg");
	    //alert(document.getElementById("txhMsg").value +"wa");
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
		}
	}
	
	function fc_Valida(){	
	  return true;
	}
	
	function fc_Grabar(){
		objDescripcion = document.getElementById("txtDscComponente");
		if (objDescripcion.value == ""){
		    window.opener.document.getElementById("frmMain").submit();		    
			alert("ingrese el componente");
			return;
		}
		document.getElementById("txhOperacion").value = "GRABAR";
		document.getElementById("frmMain").submit();
	
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_config_componentes_agregar.html">
<form:hidden path="codDetalle" id="txhCodDetalle"/>
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>
<table class="tabla2" style="width:100%" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray' align="center"> 
			<tr>
				<td colspan='2' class="opc_combo" style='cursor:hand;'>Agregar Componente</td>					
			</tr>
			<tr>
				<td valign='top' colspan="2">
					<table class="tabla"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="30px"> 
						<tr>
							<td nowrap class="texto_bold" width="30%">Componente:</td>			
							<td>
								<input name="color" value="Imagen Personal" class="cajatexto_1" style="HEIGHT: 18px;width:150px" readonly >
							</td>
						</tr>

						<tr>
							<td nowrap class="texto_bold" width="30%">Concepto:</td>			
							<td>
								<form:input path="descripcion" id="txtDscComponente" 
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'concepto');" 
								cssClass="cajatexto" size="40" />
							</td>
						</tr>
						
					</table>
				</td>
			</tr>
		</table>	
		<br>
		<table align=center>
			<tr>
				<td><input type=button class="boton" value="Grabar" onclick="javascript:fc_Grabar();" ></td>
				<td><input type=button class="boton" value="Cerrar" onclick="window.close();" ID="Button1" NAME="Button1"></td>
			</tr>
		</table>
		<br>
</form:form>
</body>
</html>