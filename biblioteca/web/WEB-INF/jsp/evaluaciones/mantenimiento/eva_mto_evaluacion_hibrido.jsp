<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
	}
	function fc_Regresar(){
		parent.location.href =  "${ctx}/menuEvaluaciones.html";
	}
	function Fc_RelacionarProductos(){
		if (document.getElementById("txhSeleccion").value != ""){
			strCod = document.getElementById("txhSeleccion").value;
			strDsc=document.getElementById("txhDescripcion").value;
			strCodSisteval=document.getElementById("txhCodigo").value;
			codPeriodo=document.getElementById("txhCodPeriodo").value
			codEvaluador=document.getElementById("txhCodEvaluador").value
			location.href="${ctx}/evaluaciones/eva_mto_relacionar_producto_hibrido.html?txhCodigo="+strCod+"&txhDescripcion="+strDsc+"&txhCodSisteval="+strCodSisteval+"&txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;
		}
		else{
			alert(mstrSeleccione);
			return false;
		}			
	}
	function Fc_Agregar(){
		codPeriodo=document.getElementById("txhCodPeriodo").value
		codEvaluador=document.getElementById("txhCodEvaluador").value		
		location.href="${ctx}/evaluaciones/eva_mto_agregar_producto_hibrido.html?txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;		
	}
	function Fc_Quitar(){		
		if (document.getElementById("txhSeleccion").value != ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
			}			
		}
		else{
			alert(mstrSeleccione);
			return false;
		}
	}
	function Fc_Modificar(){
		if (document.getElementById("txhSeleccion").value != ""){
			codPeriodo=document.getElementById("txhCodPeriodo").value
			codEvaluador=document.getElementById("txhCodEvaluador").value			
			strCod = document.getElementById("txhSeleccion").value;   		    
			location.href="${ctx}/evaluaciones/eva_mto_agregar_producto_hibrido.html?txhCodDetalle=" + strCod+"&txhCodPeriodo="+codPeriodo+"&txhCodEvaluador="+codEvaluador;		
		}
		else{
			alert(mstrSeleccione);
			return false;
		}
	}
	function fc_seleccionarRegistro(seleccion, descripcion, codSisteval){			
		document.getElementById("txhSeleccion").value = seleccion;
		document.getElementById("txhCodigo").value = codSisteval;
		document.getElementById("txhDescripcion").value = descripcion;				
	}			
	</script>
</head>
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_evaluacion_hibrido.html">
	<form:hidden path="seleccion" id="txhSeleccion"/>
	<form:hidden path="operacion" id="txhOperacion"/>	
	<form:hidden path="descripcion" id="txhDescripcion"/>
	<form:hidden path="codigo" id="txhCodigo"/>
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="green" width="96%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:4px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="503px" class="opc_combo"><font style="">Sistema de Evaluaciones - Producto H�brido</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table><tr height="5px"><td></td></tr></table>
	
	<table cellpadding="0" cellspacing="0" ID="Table1" width="98%" style="margin-left:4px" class="" >
		<tr>
			<td>
			<div style="overflow: auto; height: 250px;width:99%">			
				<table cellpadding="0" cellspacing="1" ID="Table1" class="" style="border: 1px solid #048BBA;width:97%">
					<tr height="20px">
						<td class="headtabla" width="5%">&nbsp;Sel.</td>
						<td class="headtabla" width="15%">&nbsp;C�digo</td>
						<td class="headtabla" width="50%">&nbsp;Denominaci�n</td>
						<td class="headtabla" width="15%">&nbsp;Fecha de Registro</td>
						<td class="headtabla" width="15%">&nbsp;Nro de Productos</td>						
					</tr>					
				 	<c:forEach var="lista" items="${control.listEvaluaciones}" varStatus="status">												
					<!-- tr  class=""-->
					<c:choose>
						<c:when test="${status.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
						<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>
					 						
						<td align="center" >
							<input type="radio" name="codProceso"						
							value='<c:out value="${lista.codGenerado}" />'							
							onclick="fc_seleccionarRegistro(this.value,'<c:out value="${lista.descripcion}" />','<c:out value="${lista.codSec}" />');">						
						</td>
						<td style="text-align: center;" >&nbsp;
							<c:out value="${lista.codSec}" />							
						</td>
						<td style="text-align: left;" >&nbsp;
							<c:out value="${lista.descripcion}" />							
						</td>
						<td style="text-align: center;" >
							<c:out value="${lista.fechaRegistro}" />
						</td>
						<td style="text-align: center;" >
							<c:out value="${lista.nroProductos}" />
						</td>
					</tr>
					</c:forEach>
				</table>
			</div>
			</td>
			<td width="27px" valign="top"><br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				&nbsp;<img src= "${ctx}/images/iconos/agregar1.jpg" id="imgagregar" onclick="javascript:Fc_Agregar();" style="cursor:hand" alt="Agregar"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgactualizar','','${ctx}/images/iconos/actualizar2.jpg',1)">
				&nbsp;<img src="${ctx}/images/iconos/actualizar1.jpg" id="imgactualizar" alt="Modificar" style="cursor:hand" onclick="javascript:Fc_Modificar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/iconos/quitar2.jpg',1)">
				&nbsp;<img  src="${ctx}/images/iconos/quitar1.jpg" id="imgEliminar" alt="Quitar" style="cursor:hand" onclick="javascript:Fc_Quitar();"></a>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN" align="center" height="30px" width="98%">	
		<tr>
			<td align="center"><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRelacionar','','${ctx}/images/botones/relacionarprod2.jpg',1)">
				<img alt="Relacionar Productos" src="${ctx}\images\botones\relacionarprod1.jpg" id="imgRelacionar" onclick="javascript:Fc_RelacionarProductos();" style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>
	<br>
</form:form>
</body>
</html>