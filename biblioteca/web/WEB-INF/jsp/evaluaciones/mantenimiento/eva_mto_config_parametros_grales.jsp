<%@ include file="/taglibs.jsp"%>
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-fechas.js" language="JavaScript;"	type="text/JavaScript"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
    function onLoad(){
	   var objLong = "<%=((request.getAttribute("Longitud")==null)?"":(String)request.getAttribute("Longitud"))%>";
	   var objMsg = "<%=((request.getAttribute("Msg1OK")==null)?"":(String)request.getAttribute("Msg1OK"))%>";
	   var objGuarda = "<%=((request.getAttribute("Msg")==null)?"":(String)request.getAttribute("Msg"))%>";
	   var objProducto = "<%=((request.getAttribute("MsgProducto")==null)?"":(String)request.getAttribute("MsgProducto"))%>";
	   var objPeriodo = "<%=((request.getAttribute("MsgPeriodo")==null)?"":(String)request.getAttribute("MsgPeriodo"))%>";
	   var objMadFrog = "<%=((request.getAttribute("WilDATilA")==null)?"":(String)request.getAttribute("WilDATilA"))%>";
	   $("txhLongitud").value= objLong;
	   
	   var Oper = "<%=((request.getAttribute("Operacion")==null)?"":(String)request.getAttribute("Operacion"))%>";
	   var objTipo = "<%=((request.getAttribute("Tipo")==null)?"":(String)request.getAttribute("Tipo"))%>";
	   
	   var estadoPeriodo="<%=((request.getAttribute("estadoPeriodo")==null)?"":(String)request.getAttribute("estadoPeriodo"))%>";
	   $("txhTipo").value= objTipo;
	   if(objMadFrog=="MAD")
	   	document.getElementById("tbl_calificaciones").style.display = 'inline-block';
	   	   
	   if((estadoPeriodo!="")&&(objLong!="")&&(objTipo!=""))
	   	ValidarPeriodo(estadoPeriodo, objTipo, objLong);
	   	   
	   if(Oper=="DETALLE"){		
		   if(objMsg == "OK"){
			  document.getElementById("tbl_calificaciones").style.display = 'inline-block';}
		   else	
              document.getElementById("tbl_TipoTablaDetalle").style.display = 'inline-block';           
	   }
	   
	   if(Oper == "REGISTRAR"){ 			  
	     if(objGuarda=="OK") 
		     alert(mstrSeGraboConExito);
	     else 
		     alert(mstrProblemaGrabar);
	   }
	    
	}
	
	function ValidarPeriodo(periodoActual, tipo, tamano){
	 var realLong=parseInt(tamano);
	    if(periodoActual.indexOf(document.getElementById("cboSelPeriodo").value,0)>-1) {
	       if(tipo=="EXISTE"){
	          if(tamano=="9"){
	           document.getElementById("imgFecInc1").disabled=false;
	           for( a=0;a<realLong;a++)
	           {  document.getElementById("text"+a).readOnly=false;
	           }
	           
		       }
		       else{ if(tamano=="7")
		       		 { 
				       document.getElementById("imgFecInc1").disabled=false;
				       for( a=0;a<realLong;a++)
	          			 {  document.getElementById("text"+a).readOnly=false;
	          			 }
				      
		             }
		       		 else{ document.getElementById("imgFecInc1").disabled=false;
		          		 	for( a=0;a<realLong;a++)
	          			 	 {  document.getElementById("text"+a).readOnly=false;
	          			  	}
		           		} 		           
			      }
	       }
	      else { if(tamano=="9"){
		           document.getElementById("imgFecInc2").disabled=false;
		           for( a=0;a<realLong;a++)
	          		   {  document.getElementById("text1"+a).readOnly=false;
	          		   }
		          }
			      else{ if(tamano=="7")
			             { document.getElementById("imgFecInc2").disabled=false;
			               for( a=0;a<realLong;a++)
	          		  		 {  document.getElementById("text1"+a).readOnly=false;
	          		   		 }
			              }
			             else
			            { document.getElementById("imgFecInc2").disabled=false;
			              for( a=0;a<realLong;a++)
	          		  		 {  document.getElementById("text1"+a).readOnly=false;
	          		   		 }
			            }
			      }
	      } 
	    }
	}
	
	function fc_Regresar(){
		parent.location.href =  "${ctx}/menuEvaluaciones.html";
	}
         
      function fc_Valor(){
        if(document.getElementById("cboSelProducto").value!="-1")
       {	document.getElementById("txhCodProducto2").value = document.getElementById("cboSelProducto").value;
        	document.getElementById("txhCodPeriodo2").value = document.getElementById("cboSelPeriodo").value;
	    	if(document.getElementById("txhCodProducto2").value==document.getElementById("txhConstePFR").value)
	    	    document.getElementById("txhTxtPFR").value="PFR";
	    	document.getElementById("txhOperacion").value = "DETALLE";
	    	document.getElementById("frmMain").submit();
	   }
	   else{
	   		document.getElementById("tbl_calificaciones").style.display = 'none';
		    document.getElementById("tbl_TipoTablaDetalle").style.display = 'none';
	    }
	}
				
	  function fc_seleccionarRegistro(strCodNIvel1, descripcion)
	  {
	   document.getElementById("txhValor").value = strCodNIvel1;
	   
	  }
	    
	   function fc_Grabar() {
	   
	   var realLong=parseInt(document.getElementById("txhLongitud").value);
	    if(document.getElementById("txhLongitud").value == "9"){	     
	      //if(document.getElementById("txhTxtPFR").value=="PFR")
		    if(document.getElementById("txhTipo").value=="EXISTE")
		      {//alert("9 y Existe");
		       if(document.getElementById("txhTxtPFR").value=="PFR")  
			     { if(fc_ValidaCampos1()==true){
			          alert(mstrLleneLosCampos);
		              return;}
		           else{  
		              var cadena="";
		          	  var valorCaja="";
			       	   for(a=0;a<realLong;a++)
		               { valorCaja=document.getElementById("text"+a).value;
		                if(fc_Trim(valorCaja)=="")
		                    valorCaja="&&";
		                 if(a==0)
		                 cadena= valorCaja + "|";
		                 else{ if(a==realLong-1)
		                       cadena= cadena + valorCaja;
		                       else cadena= cadena + valorCaja + "|";
		                  }
		               }
		            document.getElementById("txhValor0").value=cadena;
		            }
			      }
			     else { 
			       	 	 var cadena="";
		         	     var valorCaja="";
			       	   	 for(a=0;a<realLong;a++)
		              	 { valorCaja=document.getElementById("text"+a).value;
		               		 if(fc_Trim(valorCaja)=="")
		                 	   valorCaja="&&";
		                 	if(a==0)
		                 	cadena= valorCaja + "|";
		                 	else{ if(a==realLong-1)
		                       cadena= cadena + valorCaja;
		                       else cadena= cadena + valorCaja + "|";
		                  }
		               }
		            document.getElementById("txhValor0").value=cadena;  
		           }
		       }
		      else
		      {//alert("9 y No Existe");
		       
		       if(document.getElementById("txhTxtPFR").value=="PFR")
			    { if(fc_ValidaCampos2()==true){
			          alert(mstrLleneLosCampos);
		              return;}
		           else{ var cadena="";
		          		 var valorCaja="";
			       	     for(a=0;a<realLong;a++)
		              	 { valorCaja=document.getElementById("text1"+a).value;
		                 if(fc_Trim(valorCaja)=="")
		                    valorCaja="&&";
		                 if(a==0)
		                 cadena= valorCaja + "|";
		                 else{ if(a==realLong-1)
		                       cadena= cadena + valorCaja;
		                       else cadena= cadena + valorCaja + "|";
		                  }
		               }
		            document.getElementById("txhValor0").value=cadena; 
		          }
		       }
		       else{
		           var cadena="";
		           var valorCaja="";
			       	   for(a=0;a<realLong;a++)
		               { valorCaja=document.getElementById("text1"+a).value;
		                if(fc_Trim(valorCaja)=="")
		                    valorCaja="&&";
		                 if(a==0)
		                 cadena= valorCaja + "|";
		                 else{ if(a==realLong-1)
		                       cadena= cadena + valorCaja;
		                       else cadena= cadena + valorCaja + "|";
		                  }
		               }
		            document.getElementById("txhValor0").value=cadena; 
			       }
		      }
	      
	      
	     }
	    else{ if(document.getElementById("txhLongitud").value == "7")
	          {  if(document.getElementById("txhTipo").value=="EXISTE"){			      
		           var cadena="";
		           var valorCaja="";
			       for(a=0;a<realLong;a++)
		           { valorCaja=document.getElementById("text"+a).value;
		             if(fc_Trim(valorCaja)=="")
		              valorCaja="&&";
		              if(a==0)
		              cadena= valorCaja + "|";
		              else{ if(a==realLong-1)
		                    cadena= cadena + valorCaja;
		                    else cadena= cadena + valorCaja + "|";
		                  }
		               }
		            document.getElementById("txhValor0").value=cadena;
			     }
			      else{			     
				      var cadena="";
		          	  var valorCaja="";
			       	   for(a=0;a<realLong;a++)
		               { valorCaja=document.getElementById("text1"+a).value;
		                if(fc_Trim(valorCaja)=="")
		                    valorCaja="&&";
		                 if(a==0)
		                 cadena= valorCaja + "|";
		                 else{ if(a==realLong-1)
		                       cadena= cadena + valorCaja;
		                       else cadena= cadena + valorCaja + "|";
		                  }
		               }
		             document.getElementById("txhValor0").value=cadena;				     
			       }
	          
	          }
			    else{
			      if(document.getElementById("txhTipo").value=="EXISTE"){			      
			      if(fc_ValidaCampos3()==true){
			          alert(mstrLleneLosCampos);
		              return;}
		           else{
		           		 var cadena="";
		          		 var valorCaja="";
			       	  	 for(a=0;a<realLong;a++)
		              	 { valorCaja=document.getElementById("text"+a).value;
		              	   if(fc_Trim(valorCaja)=="")
		                    valorCaja="&&";
		                 if(a==0)
		                 cadena= valorCaja + "|";
		                 else{ if(a==realLong-1)
		                       cadena= cadena + valorCaja;
		                       else cadena= cadena + valorCaja + "|";
		                  }
		               }
		            document.getElementById("txhValor0").value=cadena;
			     }
			        }
			      else{			      
			      if(fc_ValidaCampos4()==true){
			       	  alert(mstrLleneLosCampos);
		              return;
		           }
			          else{ 
			          var cadena="";
		          	  var valorCaja="";
			       	   for(a=0;a<realLong;a++)
		               { valorCaja=document.getElementById("text1"+a).value;
		                if(fc_Trim(valorCaja)=="")
		                    valorCaja="&&";
		                 if(a==0)
		                 cadena= valorCaja + "|";
		                 else{ if(a==realLong-1)
		                       cadena= cadena + valorCaja;
		                       else cadena= cadena + valorCaja + "|";
		                  }
		               }
		            document.getElementById("txhValor0").value=cadena;
				    }
			       } 
			      } 
	     } 
	    if((document.getElementById("txhCodProducto2").value!="")&&(document.getElementById("txhCodPeriodo2").value!="")){
		    document.getElementById("txhOperacion").value = "REGISTRAR";	       
		   	document.getElementById("frmMain").submit();
	    }else alert('Debe seleccionar un producto y un per�odo.');
	     	   
	   }

	function fc_ValidaCampos1(){
  		var band=false;
 
  		if(document.getElementById("text0").value==""){
                       band=true;
               }
               else{
                       if(document.getElementById("text1").value==""){
                               band=true;
                       }
                       else{
                               if(document.getElementById("text2").value==""){
                                       band=true;
                               }
                               else{
                                       if(document.getElementById("text3").value==""){
                                               band=true;
                                       }
                                       else{
                                               if(document.getElementById("text4").value==""){
                                                       band=true;
                                               }
                                               else{
                                                       if(document.getElementById("text5").value==""){
                                                               band=true;
                                                       }
                                                       else{
                                                               if(document.getElementById("text6").value==""){
                                                                       band=true;
                                                               }
                                                               else{
                                                                       if(document.getElementById("text7").value==""){
                                                                               band=true;
                                                                       }
                                                                       else{
                                                                               if(document.getElementById("text8").value==""){
                                                                                       band=true;
                                                                               }                                                                                                                
                                                                       }                                
                                                               }                                                                
                                                       }                                                        
                                               }                                                
                                       }                                        
                               }                                
                       }                        
               }                
               return band;
 }
 
 function fc_ValidaCampos2(){
  var band=false;
  if(document.getElementById("text10").value==""){
                       band=true;
               }
               else{
                       if(document.getElementById("text11").value==""){
                               band=true;
                       }
                       else{
                               if(document.getElementById("text12").value==""){
                                       band=true;
                               }
                               else{
                                       if(document.getElementById("text13").value==""){
                                               band=true;
                                       }
                                       else{
                                               if(document.getElementById("text14").value==""){
                                                       band=true;
                                               }
                                               else{
                                                       if(document.getElementById("text15").value==""){
                                                               band=true;
                                                       }
                                                       else{
                                                               if(document.getElementById("text16").value==""){
                                                                       band=true;
                                                               }
                                                               else{
                                                                       if(document.getElementById("text17").value==""){
                                                                               band=true;
                                                                       }
                                                                       else{
                                                                               if(document.getElementById("text18").value==""){
                                                                                       band=true;
                                                                               }                                                                                                                
                                                                       }                                
                                                               }                                                                
                                                       }                                                        
                                               }                                                
                                       }                                        
                               }                                
                       }                        
               }                
               return band;
 }
 function fc_ValidaCampos3(){
   var band=false;
  if(document.getElementById("text0").value==""){
                       band=true;
               }
               else{
                       if(document.getElementById("text1").value==""){
                               band=true;
                       }
                       else{
                               if(document.getElementById("text2").value==""){
                                       band=true;
                               }
                               else{
                                       if(document.getElementById("text3").value==""){
                                               band=true;
                                       }
                                       else{
                                               if(document.getElementById("text4").value==""){
                                                       band=true;
                                               }
                                                                                          
                                       }                                        
                               }                                
                       }                        
               }                
               return band;
 }
 function fc_ValidaCampos4(){
  var band=false;
  if(document.getElementById("text10").value==""){
                       band=true;
               }
               else{
                       if(document.getElementById("text11").value==""){
                               band=true;
                       }
                       else{
                               if(document.getElementById("text12").value==""){
                                       band=true;
                               }
                               else{
                                       if(document.getElementById("text13").value==""){
                                               band=true;
                                       }
                                       else{
                                               if(document.getElementById("text14").value==""){
                                                       band=true;
                                               }
                                                                                          
                                       }                                        
                               }                                
                       }                        
               }
               return band;  
 }

function fc_ValidaCampos5(){
  var band=false;
 
  if(document.getElementById("text0").value==""){
                       band=true;
               }
               else{
                       if(document.getElementById("text1").value==""){
                               band=true;
                       }
                       else{
                               if(document.getElementById("text2").value==""){
                                       band=true;
                               }
                               else{
                                       if(document.getElementById("text3").value==""){
                                               band=true;
                                       }
                                       else{
                                               if(document.getElementById("text4").value==""){
                                                       band=true;
                                               }
                                               else{
                                                       if(document.getElementById("text5").value==""){
                                                               band=true;
                                                       }
                                                       else{
                                                               if(document.getElementById("text6").value==""){
                                                                       band=true;
                                                               }
                                                                                                                              
                                                       }                                                        
                                               }                                                
                                       }                                        
                               }                                
                       }                        
               }                
               return band;
 }
 
 function fc_ValidaCampos6(){
  var band=false;
  if(document.getElementById("text10").value==""){
                       band=true;
               }
               else{
                       if(document.getElementById("text11").value==""){
                               band=true;
                       }
                       else{
                               if(document.getElementById("text12").value==""){
                                       band=true;
                               }
                               else{
                                       if(document.getElementById("text13").value==""){
                                               band=true;
                                       }
                                       else{
                                               if(document.getElementById("text14").value==""){
                                                       band=true;
                                               }
                                               else{
                                                       if(document.getElementById("text15").value==""){
                                                               band=true;
                                                       }
                                                       else{
                                                               if(document.getElementById("text16").value==""){
                                                                       band=true;
                                                               }
                                                                                                                           
                                                       }                                                        
                                               }                                                
                                       }                                        
                               }                                
                       }                        
               }                
               return band;
 }

function fcCalendario1(){
   //alert("Calendario 1");
    Calendar.setup({
		inputField     :    "text0",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecInc1",
		singleClick    :    true
	});
  }
	function fcCalendario2(){
	//alert("Calendario 2");
    Calendar.setup({
		inputField     :    "text10",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecInc2",
		singleClick    :    true
	});
  }  
	   
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_mto_config_parametros_grales.html">
<form:hidden path="operacion" id="txhOperacion"/>

<form:hidden path="codProducto2" id="txhCodProducto2"/>
<form:hidden path="codPeriodo2" id="txhCodPeriodo2"/>
<form:hidden path="codTablaDetalle" id="txhCodTablaDetalle"/>
<form:hidden path="tipo" id="txhTipo"/>
<form:hidden path="valor" id="txhValor"/>
<form:hidden path="valor0" id="txhValor0"/>
<form:hidden path="longitud" id="txhLongitud"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="txtPFR" id="txhTxtPFR"/>
<form:hidden path="constePFR" id="txhConstePFR"/>
<form:hidden path="consteProductoVirtual" id="txhConsteProductoVirtual"/>
<form:hidden path="consteProductoHibrido" id="txhConsteProductoHibrido"/>
<form:hidden path="codPeriodoAperturados" id="txhCodPeriodoAperturados"/>


		<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>

		
		<table cellpadding="0" cellspacing="0" class="borde_tabla"  style="width:97%;margin-left:5px" >
			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="4">
					Par�metros Generales
				</td>		 	
			</tr>
			<tr class="fondo_dato_celeste">
				<td>Producto :</td>
				<td>&nbsp;<form:select  path="codProducto" id="cboSelProducto"  cssClass="cajatexto" 
								cssStyle="width:280px" onchange="javascript:fc_Valor();">
						        <form:option  value="-1">--Seleccione--</form:option>
					      	    <c:if test="${control.codListaProducto!=null}">
					        	<form:options itemValue="codProducto" itemLabel="descripcion" 
					        		items="${control.codListaProducto}" /><!-- itemValue="codProducto" es el codigo interno q se guarda
					        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->						        
					            </c:if>
 				            </form:select></td>
				<td>Per�odo :</td>
				<td>&nbsp;<form:select  path="codigo" id="cboSelPeriodo"  cssClass="cajatexto" 
								cssStyle="width:180px" onchange="javascript:fc_Valor();">
						         <c:if test="${control.codListaPeriodo!=null}">
					        	<form:options itemValue="codigo" itemLabel="nombre" 
					        		items="${control.codListaPeriodo}" />						        
					            </c:if>
 				            </form:select>
				</td>
			</tr>
		</table>

		<table>
		<tr height="3px"><td></td></tr></table>
        <table style="display:none" id="tbl_calificaciones" name="tbl_calificaciones" cellpadding="0"  border="1" bordercolor="#048BBA" cellspacing="0" ID="Table1" 
        style="width:97%;margin-left:5px" class="tablaMant" bgcolor="#f2f2f2">
			<tr>
				<td><!-- en este display va si se encuentra data en la BD-->
                    <display:table name="sessionScope.listaCalificaciones" cellpadding="2" cellspacing="2"
						decorator="com.tecsup.SGA.bean.ParametrosGeneralesDecorator" 
					 	requestURI="" style="width:98%;">
						<display:column property="descripcion"  title="" style="text-align:left; width:85%"/>
						<display:column property="textValor"  title="" />	
						
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
								  					
			 		</display:table>	
				</td>
				
				</tr>
		</table>
		
		<!-- aki el empieza el 2do display -->
        <table style="display:none;width:97%;margin-left:5px" id="tbl_TipoTablaDetalle" name="tbl_TipoTablaDetalle" border="1" cellpadding="0" bordercolor="#048BBA" cellspacing="0" ID="Table1"
         class="tablaMant" bgcolor="#f2f2f2">
			<tr>
				<td><!-- este display va si no existe data en la BD y s etiene q crear nueva data -->
                        <display:table name="sessionScope.listaCalificaciones2" cellpadding="2" cellspacing="2"
						decorator="com.tecsup.SGA.bean.ParametrosGeneralesDecorator" 
						 requestURI="" style="width:98%">
						<display:column property="descripcion"  title="" style="text-align:left; width:95%"/>
						<display:column property="textValor1" title="" headerClass="grilla"  />	
						
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
									  					
				 </display:table>	
				</td>	
			</tr>
		</table><br>
		<!-- aki muere el 2do display -->
		<table border="0" width="100%">
			<tr>
				<td align="center">					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Grabar();" style="cursor:pointer"></a>
				</td>
			</tr>
		</table>
</form:form>
</body>

</html>