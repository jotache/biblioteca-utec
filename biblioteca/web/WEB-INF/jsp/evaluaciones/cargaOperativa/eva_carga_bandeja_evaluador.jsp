<%@ include file="/taglibs.jsp"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script type="text/javascript" type="text/javascript">
		function onLoad(){		
		}

		function fc_Regresar(){
			location.href = "${ctx}/menuEvaluaciones.html";
		}
		
		function fc_Ir(obj){
			if(document.getElementById('txhCodCurso').value == ""){
				alert(mstrSeleccione);
				return;
			}
			var codEvaluador = "";
			objCombo = document.getElementById("cboEvaluadores");
			codEvaluador = document.getElementById("txhCodEvaluador").value;
			if ( objCombo != null )
			{
				codEvaluador = objCombo.value;
			}	
			var tipoUsuario = "<%=(String)request.getAttribute("tipoUsuario")%>";		
			switch(obj){
				case '1':
					window.location.href = "${ctx}/evaluaciones/controlAsistencia.html" + 
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
											"&txhCodEvaluador=" + codEvaluador + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value +											
											"&txhCodProducto=" + document.getElementById('txhCodProducto').value +
											"&txhTipoUsuarioBandeja=" + tipoUsuario +
											"&txhCodUsuarioBandeja=" + document.getElementById('txhCodEvaluador').value +
											"&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value;
					break;
				case '2':
					window.location.href = "${ctx}/evaluaciones/bandeja_incidencia_doc.html" + 
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
											"&txhCodEvaluador=" + codEvaluador + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value +
											"&txhCodProducto=" + document.getElementById('txhCodProducto').value +
											"&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value;
					break;
				case '3':	
									
					url = 	"${ctx}/evaluaciones/evaluaciones.html" +
							"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
							"&txhCodEvaluador=" + codEvaluador + 
							"&txhCodCurso=" + document.getElementById('txhCodCurso').value +
							"&txhCodProducto=" + document.getElementById('txhCodProducto').value +
							"&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value+
							"&txhTipoUsuarioBandeja=" + tipoUsuario+
							"&txhCodUsuarioBandeja=" + document.getElementById('txhCodEvaluador').value;					
					window.location.href = url; 
															
					break;
				case '4':
					window.location.href = "${ctx}/evaluaciones/bandeja_eva_competencias.html" + 
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value +
											"&txhCodProducto=" + document.getElementById('txhCodProducto').value +
											"&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value;
					break;
			}
		}
		
		
		
		function fc_seleccionarRegistro(strCodCurso,strCiclo,strCurso,strEspecialidad,strProducto,strSistEval,strCodProducto, strCodEspec){
		
			document.getElementById('txhCodCurso').value = strCodCurso;
			document.getElementById('txhCiclo').value = strCiclo;
			document.getElementById('txhCurso').value = strCurso;
			document.getElementById('txhEspecialidad').value = strEspecialidad;
			document.getElementById('txhProducto').value = strProducto;
			document.getElementById('txhSistEval').value = strSistEval;
			document.getElementById("txhCodProducto").value = strCodProducto;
			document.getElementById("txhCodEspecialidad").value=strCodEspec;
						
		}
		
		
		function fc_Eva(){
		if ( document.getElementById("txhCodCurso").value != "")
			{ 	
	
			if(document.getElementById("txhCiclo").value == "null"){
			document.getElementById("txhCiclo").value = " ";
			}
			if(document.getElementById("txhEspecialidad").value == "null"){
			document.getElementById("txhEspecialidad").value = " ";
			}	
			if(document.getElementById("txhCodCurso").value == "null"){
			document.getElementById("txhCodCurso").value = " ";
			}
			if(document.getElementById("txhCurso").value == "null"){
			document.getElementById("txhCurso").value = " ";
			}
			if(document.getElementById("txhProducto").value == "null"){
			document.getElementById("txhProducto").value = " ";
			}
			if(document.getElementById("txhSistEval").value == "null"){
			document.getElementById("txhSistEval").value = " ";
			}
			if(document.getElementById("txhCodPeriodo").value == "null"){
			document.getElementById("txhCodPeriodo").value = " ";
			}
			if(document.getElementById("txhCodProducto").value == "null"){
			document.getElementById("txhCodProducto").value = " ";
			}
			if(document.getElementById("txhCodEspecialidad").value == "null"){
			document.getElementById("txhCodEspecialidad").value = " ";
			}
		
			strCiclo = document.getElementById('txhCiclo').value;
			strEspecialidad = document.getElementById('txhEspecialidad').value;
			strCodCurso = document.getElementById('txhCodCurso').value;
			strCurso = document.getElementById('txhCurso').value;
			strProducto = document.getElementById('txhProducto').value;
			strSistEval = document.getElementById('txhSistEval').value;
			strCodPeriodo = document.getElementById("txhCodPeriodo").value;
			strCodProducto = document.getElementById("txhCodProducto").value;
			strcodEval= document.getElementById("txhCodEvaluador").value;
			strPeriodo = "${control.periodo}";
			strEvaluador = document.getElementById("txtEvaluador").value;
			strCodEspecialidad = document.getElementById("txhCodEspecialidad").value;
			window.location.href = ("${ctx}/evaluaciones/eva_detalle_practicas.html?txhCodCurso=" + strCodCurso + "&txhCiclo=" + 
									strCiclo + "&txhCurso=" + strCurso + "&txhEspecialidad=" + strEspecialidad + "&txhProducto=" + 
									strProducto + "&txhSistEval=" + strSistEval + "&txhPeriodo=" + strPeriodo + "&txhEval=" + 
									strEvaluador + "&txhCodPeriodo=" + strCodPeriodo + "&txhCodProducto=" + 
									strCodProducto + "&txhCodEvaluador=" + strcodEval + "&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value);
		}
		else{
			alert(mstrSeleccione);
		}
		}
		
		
		function fc_Buscar(){
			if (!document.getElementById("cboEvaluadores").value == ""){ 
				document.getElementById("txhOperacion").value="BUSCAR";	
				document.getElementById("frmMain").submit();	
			}
			else{
				alert('Seleccione el Evaluador');
				document.getElementById("cboEvaluadores").focus();
			}
		}
	</script>
</head>

<body>
	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/evaluaciones/bandejaEvaluador.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEval" id="txhCodEvaluador"/>
		<form:hidden path="codCurso" id="txhCodCurso"/>
		
		
		<form:hidden path="ciclo" id="txhCiclo"/>
		<!--
		<form:hidden path="nomEvaluador" id="txhNomEvaluador"/> 
		<form:hidden path="periodo" id="txhPeriodo"/>
		 -->

		<form:hidden path="evaluador" id="txhEval"/>
		<form:hidden path="producto" id="txhProducto"/>
		<form:hidden path="curso" id="txhCurso"/>
		<form:hidden path="especialidad" id="txhEspecialidad"/>
		<form:hidden path="sistEval" id="txhSistEval"/>
		<form:hidden path="fecha" id="txhFecha"/>
		<form:hidden path="codProducto" id="txhCodProducto"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<!-- Alcenamos la sede del evaluador -->
		<form:hidden path="codSede" id="txhCodSede"/>
		<form:hidden path="codOpciones" id="txhCodOpciones"/>
		<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>
		<!-- Titulo de la Pagina -->
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="765px" class="opc_combo"><font style="">Carga Ac�demica del Evaluador</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			 </tr>
		 </table>
		<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" style="width:98%;margin-left:8px">
			<tr>
				<td>
					<table class="tabla" background="${ctx}/images/Evaluaciones/back-sup.jpg" style="width:98%;margin-top:2px; height:65px" cellspacing="2" cellpadding="0" border="0" bordercolor="white">
						<tr>
							<td class="" height="20px" width="15%">&nbsp;Per&iacute;odo Vigente :</td>
							<td align="justify" height="20px">
								<form:input path="periodo" id="txtPeriodo" cssClass="cajatexto_1" readonly="true" cssStyle="width: 150px;"/></td>
						</tr>
						<tr>
							<td class="" height="20px" width="15%">
								<%
								if (request.getAttribute("tipoUsuario") == "0"){%>
								&nbsp;Evaluador :
								<%}else if (request.getAttribute("tipoUsuario") == "1"){%>
								&nbsp;Jefe de Departamento :
								<%}else if (request.getAttribute("tipoUsuario") == "2"){%>
								&nbsp;Director :
								<%}%>
							</td>
							<td align="left" height="20px"><form:input path="nomEvaluador" id="txtEvaluador" size="50" cssClass="cajatexto_1" readonly="true"/></td>							
						</tr>
						<%if (request.getAttribute("tipoUsuario") == "1" || 
							  request.getAttribute("tipoUsuario") == "2"){%>
						<tr>
							<td class="" height="20px" width="15%">&nbsp;Evaluadores :</td>
							<td align="left" height="20px">
								<form:select path="cboEvaluadores" id="cboEvaluadores"
									cssClass="cajatexto_o" cssStyle="width:227px">
									<form:option value="">--Seleccione--</form:option>
										<c:if test="${control.listaEvaluadores!=null}">
											<form:options itemValue="codEvaluador" itemLabel="dscEvaluador"
												items="${control.listaEvaluadores}" />
										</c:if>
									</form:select>
								&nbsp;&nbsp;&nbsp;
								&nbsp;Curso :&nbsp;&nbsp;&nbsp;
								<form:input path="txtCurso" id="txtCurso"
								cssClass="cajatexto" 
								maxlength="40" size="28"/>

								&nbsp;

								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>

							</td>


							<!-- td align="left">
								
							</td -->
						</tr>
						<%}%>
					</table>
					<!-- Titulo de la bandeja -->
					 <table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-top:10px">
						 <tr>
						 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
						 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="400px" class="opc_combo">Relaci&oacute;n de Cursos Asignados en el Per&iacute;odo</td>
						 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
						 </tr>
					 </table>
					<table style="width:100%;margin-top:2px" cellspacing="0" cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td>
								<!-- JHPR: 2008-04-24 Modifique esto: sessionScope.listaCursos por requestScope -->
								<div style="OVERFLOW:auto; WIDTH: 100%; height: 290px; display: ;" id="tblCursos">
									<display:table htmlId="myTable" name="sessionScope.listaCursos" cellpadding="0" cellspacing="1"
										decorator="com.tecsup.SGA.bean.CursoEvaluadorDecorator" style="border: 1px solid #048BBA;width:98%">
										<display:column property="rbtSel" title="Sel." /> <!-- style="width:40px;text-align:center" headerClass="grilla" class="tablagrilla" -->
										<display:column property="ciclo" title="Ciclo" style="width:60px;text-align:center"/>
										<display:column property="producto" title="Producto" style="text-align:left;width:260px"/>
										<display:column property="especialidad" title="Especialidad/Tipo" style="text-align:left;width:240px"/>
										<display:column property="curso" title="Curso" style="text-align:left;width:250px"/>
										<display:column property="sistEval" title="Sist. Eval." style="width:90px;text-align:center"/>
										
										<display:setProperty name="basic.empty.showtable" value="true"  />
										<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='6' align='center'>No se encontraron registros</td></tr>"  />
										<display:setProperty name="paging.banner.placement" value="bottom"/>
										<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
										<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
										<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'></span>" />
										<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
										<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
										<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
										<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
										<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
										<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
										<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
									</display:table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table><tr height="0px"><td></td></tr></table>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" align="center" height="21px" width="98%">	
			<tr>
				<td align="center">		
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgevalparcial','','${ctx}/images/botones/evalparcial2.jpg',1)">
					<img alt="Nro. Evaluaciones Parciales" src="${ctx}/images/botones/evalparcial1.jpg" id="imgevalparcial" onclick="javascript:fc_Eva();" style="cursor:pointer">&nbsp;</a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcontrol','','${ctx}/images/botones/controlasist2.jpg',1)">
					<img alt="Control Asistencia" src="${ctx}/images/botones/controlasist1.jpg" id="imgcontrol" onclick="javascript:fc_Ir('1');" style="cursor:pointer">&nbsp;</a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgincidencias','','${ctx}/images/botones/incidencias2.jpg',1)">
					<img alt="Incidencias" onclick="javascript:fc_Ir('2');" id="imgincidencias" src="${ctx}/images/botones/incidencias1.jpg" style="cursor:pointer">&nbsp;</a>										
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgevalu','','${ctx}/images/botones/evaluaciones2.jpg',1)">
					<img alt="Evaluaciones" onclick="javascript:fc_Ir('3');" id="imgevalu" src="${ctx}/images/botones/evaluaciones1.jpg" style="cursor:pointer">&nbsp;</a>										
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcompetencias','','${ctx}/images/botones/competencias2.jpg',1)">
					<img alt="Competencias" onclick="javascript:fc_Ir('4');" id="imgcompetencias" src="${ctx}/images/botones/competencias1.jpg" style="cursor:pointer">&nbsp;</a>										
				</td>
			</tr>
		</table>
	</form:form>	
</body>