<%@ include file="/taglibs.jsp"%>

<head>
<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
<script language="javascript">
		
	function onLoad(){	
		var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
		fc_msgManager(mensaje);
	}

	function fc_cerrar(){	
		if (confirm('�Seguro que desea Cerrar notas?')) {			
			if ($('flagCerrarNota').value=='1'){
				alert('Registro ya se encuentra cerrado.');
			} else {
				$('operacion').value='CERRAR';
				$('frmMain').submit();
			}
		}
	}

	function fc_AbrirRegistro(){
		if (confirm('�Seguro de abrir registro de notas?')) {		
			if ($('flagCerrarNota').value=='1'){
				$('operacion').value='ABRIR_REG';
				$('frmMain').submit();
			}
		}
	}

	function fc_regresar(){
		url = 	"${ctx}/evaluaciones/evaluaciones.html" +
				"?txhCodPeriodo=" + document.getElementById('codPeriodo').value + 
				"&txhCodEvaluador=" + document.getElementById('codEval').value + 
				"&txhCodCurso=" + document.getElementById('codCurso').value+
				"&txhCodProducto=" + document.getElementById('codProducto').value+
				"&txhCodEspecialidad=" + document.getElementById('codEspecialidad').value;							
		window.location.href = url;
	}

	function fc_verificaCeldas(){
		form = document.forms[0];
		var max = form.hidTamanio.value;	
			for(i=0;i<max;i++)
				fc_verificaNota(i);	
	}


	function fc_msgManager(mensaje){	
		if(mensaje!=""){
			if(mensaje.length>1)           		
			alert(mensaje);				    
		}
	}

	function fc_cambiaSeccion(){	
		$('operacion').value='irCambiaSeccion';		
		$('frmMain').submit();
	}

	function fc_cambiaNroEvaluacion(){		
		fc_buscar();
	}

	function fc_cambiaEvaluacionParcial(){						
		$('cboNroEvaluacion').value='-1';		
		$('operacion').value='irCambiaEvaluacionParcial';		
		$('frmMain').submit();	
	}

	function fc_buscar(){
		form = document.forms[0];
		form.operacion.value="irConsultar";
		if(form.cboSeccion.value=="-1"){	
			alert('Debe ingresar el peso de las calificaciones. Consulte con el administrador.');
			form.cboSeccion.focus();
		}else{
			objCombo = document.getElementById("cboNroEvaluacion");
			var txt = objCombo.options[objCombo.selectedIndex].innerText;
			document.getElementById("txhNroEvaluacionInner").value = txt;
			form.submit();
		}	
	}


	function fc_grabar(){
	
		form = document.forms[0];
		if(form .cboNroEvaluacion.value=="-1" ){
			alert('Se deben llenar todos los parametros obligatorios');
			form .cboNroEvaluacion.focus();
			return 0;
		}else if(form .cboEvaluacionParcial.value=="-1" ){
			alert('Se deben llenar todos los parametros obligatorios');
			form .cboEvaluacionParcial.focus();
			return 0;
		}
		
		var max = form.hidTamanio.value;
		var st	= "";
		var srdo= "";
		
		if( confirm("Seguro Grabar") ){
			for(i=0;i<max;i++){
				srdo = "";			
				objCodigo 	= document.getElementById("codAlu"+i);
				codigoAlu 	= objCodigo.value;			
				txtIni 		= document.getElementById("txt"+i);
				horaIni 	= txtIni.value;			
				rdo0 		= document.getElementById("RadioAN"+i);
				rdo1 		= document.getElementById("RadioNP"+i);
				rdo2 		= document.getElementById("RadioPE"+i);
				
				if(rdo0.checked){
					srdo = rdo0.value;
				}else if(rdo1.checked){
					srdo = rdo1.value;
				}else if(rdo2.checked){
					srdo = rdo2.value;
				}
				
				st=st+codigoAlu+"$"+horaIni+"$"+srdo+"|";		
			}
						
			$('operacion').value='irActualizar';
			$('hidToken').value=st;
			$('frmMain').submit();		
		}	
		
	}

	function fc_cancelar(){		
		$('operacion').value='irCancelar';	
	}

	function fc_verificaNota(i){
		rdo0 		= document.getElementById("RadioAN"+i);
		rdo1 		= document.getElementById("RadioNP"+i);
		rdo2 		= document.getElementById("RadioPE"+i);	
		if(document.getElementById("txt"+i).value=="")
			rdo2.checked = true;
		else if(document.getElementById("txt"+i).value=="0"){
			
			if(!rdo0.checked && !rdo1.checked)	
				rdo0.checked = true;			
		}
	
	}

	function fc_reset(rdo0,rdo1,rdo2){
		rdo0.checked = false;
		rdo1.checked = false;
		rdo2.checked = false;
	}

	function fc_verificaRadio(i){
		if(document.getElementById("txt"+i).value=="" || document.getElementById("txt"+i).value=="0"){
			fc_verificaNota(i);
		}else
		{
				rdo0 		= document.getElementById("RadioAN"+i);
				rdo1 		= document.getElementById("RadioNP"+i);
				rdo2 		= document.getElementById("RadioPE"+i);
				fc_reset(rdo0,rdo1,rdo2);
		}
	}

	
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">
<form:form commandName="control" action="${ctx}/evaluaciones/registroEvaluaciones.html" id="frmMain" name="frmMain">

		<form:hidden path="operacion" id="operacion" />		
		<form:hidden path="hidTamanio" id="hidTamanio"/>
		<form:hidden path="hidToken" id="hidToken"/>
		
		<form:hidden path="codPeriodo" id="codPeriodo"/>
		<form:hidden path="codEval" id="codEval"/>
		<form:hidden path="codCurso" id="codCurso"/>		
		
		<form:hidden path="codProducto" id="codProducto"/>
		<form:hidden path="codEspecialidad" id="codEspecialidad"/>
		<form:hidden path="txhNroEvaluacionInner" id="txhNroEvaluacionInner"/>
		
		<form:hidden path="flagCerrarNota" id="flagCerrarNota"/>		

		<form:hidden path="tipoUsuarioBandeja" id="txhTipoUsuarioBandeja"/>
		<form:hidden path="codUsuarioBandeja" id="txhCodUsuarioBandeja"/>

		
	<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9"
		align="center" width="98%">
		<tr>
			<td>
			<table class="borde_tabla" style="width: 100%; margin-top: 6px" cellspacing="0" cellpadding="0" 
				align="center">
				<tr class="fondo_cabecera_azul">		 
				 	<td class="titulo_cabecera" width="100%" colspan="6">
						Registro de Evaluaciones
					</td>		 	
				</tr>
				<tr class="fondo_dato_celeste">
					<td >&nbsp;Per�odo Vigente :</td>
					<td colspan=3>
					<form:input path="txtPeridoVigente" id="txtPeridoVigente"
						cssClass="cajatexto_1" cssStyle="width:120px"
						readonly="true" /></td>
					<td >&nbsp;Ciclo :</td>
					<td>
					<form:input path="txtCiclo" id="txtCiclo" cssClass="cajatexto_1"
						cssStyle="width:20px" readonly="true" />
					</td>
				</tr>
				<tr class="fondo_dato_celeste">
					<td >&nbsp;Programa :</td>
					<td colspan=3><!--<input name="color" value="Programa de Formaci�n Regular (PFR)" class="cajatexto_1" style="HEIGHT: 18px;width:250px" readonly > -->
					<form:input path="txtPrograma" id="txtPrograma"
						cssClass="cajatexto_1" cssStyle="width:250px"
						readonly="true" /></td>
					<td >&nbsp;Especialidad :</td>
					<td>
					<form:input path="txtEspecialidad" id="txtEspecialidad"
						cssClass="cajatexto_1" cssStyle="width:250px"
						readonly="true" /></td>

				</tr>
				<tr class="fondo_dato_celeste">
					<td >&nbsp;Curso :</td>
					<td colspan=3>
					<form:input path="txtCurso" id="txtCurso" cssClass="cajatexto_1"
						cssStyle="width:200px" readonly="true" /></td>
					<td >&nbsp;Sist. Eval. :</td>
					<td>
					<form:input path="txtSistEval" id="txtSistEval"
						cssClass="cajatexto_1" cssStyle="width:20px"
						readonly="true" />
					</td>
				</tr>
				<tr class="fondo_dato_celeste">
					<td >&nbsp;Profesor :</td>
					<td colspan=3>
					<form:input path="txtProfesor" id="txtProfesor"
						cssClass="cajatexto_1" cssStyle="width:200px"
						readonly="true" /></td>
					<td class="texto_bold">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<table class="tabla" style="width: 100%; margin-top: 6px"
				cellspacing="2" cellpadding="0" border="0" bordercolor="red"
				align="center" background="${ctx}/images/Evaluaciones/back.jpg">
				<tr>
					<td class="">Apell. Paterno :</td>
					<td>
					<form:input path="txtApaterno" id="txtApaterno"  onkeypress="javascript:fc_ValidaTextoEspecial();"  
						cssClass="cajatexto" cssStyle="width:140px" />
					&nbsp;</td>
					<td class="">Nombre :</td>
					<td><!--<input name="color"  style="HEIGHT: 18px;width:140px" class="cajatexto"  >-->
					<form:input path="txtNombre" id="txtNombre" cssClass="cajatexto"  onkeypress="javascript:fc_ValidaTextoEspecial();"  
						cssStyle="width:140px" /></td>

					<td class="">Tipo Evaluaci�n</td>
					<td>
					<form:select path="cboEvaluacionParcial" onchange="fc_cambiaEvaluacionParcial()" 
						id="cboEvaluacionParcial" cssClass="combo_o" cssStyle="width:120px">
						<form:option value="-1">--Seleccione--</form:option>						
						<c:if test="${control.lstEvaluacionParcial!=null}">							
							<form:options itemValue="codigo" itemLabel="descripcion"
								items="${control.lstEvaluacionParcial}" />
						</c:if>
					</form:select>
					</td>
					<td class="">Secci�n :</td>
					<td><form:select path="cboSeccion" id="cboSeccion"
						cssClass="combo_o" cssStyle="width:60px"
						onchange="fc_cambiaSeccion()">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.lstSeccion!=null}">
							<form:options itemValue="codSeccion" itemLabel="dscSeccion"
								items="${control.lstSeccion}" />
						</c:if>
					</form:select></td>
					<td class="">Nro.Eval</td>
					<td>
					<form:select path="cboNroEvaluacion" onchange="fc_cambiaNroEvaluacion()" 
						id="cboNroEvaluacion" cssClass="combo_o" cssStyle="width:50px">						
						<form:option value="-1">--Todos--</form:option>
						<!--item de prueba codigo 3 -->
						<c:if test="${control.lstNroEvaluacion!=null}">
							<form:options itemValue="nrevId" itemLabel="nreNroEval"
								items="${control.lstNroEvaluacion}" />
						</c:if>
					</form:select>
					</td><td>
					 &nbsp;&nbsp;&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align=absmiddle alt="Buscar" id="imgBuscar" style="CURSOR: hand" onclick="fc_buscar()" ></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align=absmiddle alt="Limpiar" id="imgLimpiar" style="CURSOR: hand" onclick="fc_limpiar()" ></a>
						</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr height="10px">
			<td></td>
		</tr>
		<tr>
			<td >
				<!-- Titulo de la bandeja -->
				 <table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
					 <tr>
					 	<td align="left"></td>
					 	<td>
					 		<span class="cabecera">
					 		Relaci�n de Alumnos
					 		</span>
					 		<span class="mensaje_rojo">
					 			<c:if test="${control.flagCerrarNota=='1'}">
					 				&nbsp;&nbsp;(REGISTRO CERRADO)
					 			</c:if>
					 		</span>					 		
					 	</td>
					 	<td></td>
					 </tr>
				 </table>
			</td>
		</tr>
		<tr>
			<td>			
			</td>
		<tr>
			<td>
			<TABLE style="WIDTH: 100%; margin-top: 0px; margin-bottom: 5px;border: 1px solid #048BBA" 
				cellspacing="1" cellpadding="0" >
				<TR height="25px">
					<TD class="headtabla" style="width: 10%" >&nbsp;C�digo</TD>
					<TD class="headtabla" style="width: 50%" >&nbsp;Alumno</TD>
					<TD class="headtabla" style="width: 40%" align="center">&nbsp;Notas</TD>
				</TR>
				<c:if test="${control.lstResultado!=null}">
				<!-- JHPR: 2008-04-28 Cambiando de estilo a de grillas -->							
				<c:forEach var="obj1" items="${control.lstResultado}" varStatus="loop" >
											
					<!-- PINTADO DE CABECERA -->
					<c:if test="${loop.index==0}">
					
					<c:choose>
						<c:when test="${loop.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
						<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>					
					
						<!-- tr class=<c:out value="${fondo}"/>-->
							<td align="center" style="width: 10%">&nbsp;</td>							
							<td align="left" style="width: 50%">&nbsp;</td>
							<td style="text-align: center;width: 40%">
								
								<c:set var="tam" value="0" />
								
								<c:forEach var="objy" items="${obj1.resultado}" varStatus="loopCount" >
									<c:set var="tam" value="${loopCount.index+1}" />																		
								</c:forEach>
								
								<!-- <c:if test="${tam==1}">
									<input name="color"  value = "P<c:out value="${control.txhNroEvaluacionInner}" />" class="cajatexto" style="width:30px;text-align:center;" disabled>&nbsp;&nbsp;									
								</c:if>-->
								
								<c:if test="${tam>=1}">
								<c:forEach var="objx" items="${obj1.resultado}" varStatus="loop2" >								
									<!-- input name="color"  value = "P<c:out value="${loop2.index+1}" />" class="cajatexto" style="width:30px;text-align:center;" disabled />&nbsp;&nbsp;-->
									<input name="color" value="P<c:out value="${objx.nroEvaluacion}" /> [<c:out value="${objx.peso}" />]" class="cajatexto" style="width:30px;text-align:center;" disabled />&nbsp;&nbsp;								
								</c:forEach>
								</c:if>
								
								<input name="color"  value = "Prom" class="cajatexto" style="width:30px" disabled>&nbsp;&nbsp;
							</td>
						</tr>
					</c:if>
					<!-- PINTADO DE EL CUERPO -->						
					<!-- tr class=<c:out value="${fondo}"/>-->
					<c:choose>
						<c:when test="${loop.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
						<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>	
										
						<td align="center" style="width: 10%">&nbsp;<c:out value="${obj1.codigo}"/></td>							
						<td align="left" style="width: 50%">&nbsp;<c:out value="${obj1.nombreAlumno}" /></td>	
						<td style="text-align: center;" style="width: 40%">						
							<c:forEach var="obj2" items="${obj1.resultado}" varStatus="loop" >								
								<input name="color"  value = "<c:out value="${obj2.nota}" />" class="cajatexto" style="width:30px" disabled>&nbsp;&nbsp;
								<c:set var="varProm" value="${obj2.prom}" />
							</c:forEach>
							<input name="color"  value = "<c:out value="${varProm}" />" class="cajatexto" style="width:30px" disabled>&nbsp;&nbsp;								
						</td>										
					</tr>
				</c:forEach>
				</c:if>
			</TABLE>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" align="center" height="30px" width="98%">
		<tr>
			<td align="center">
	
				<c:if test="${control.flagCerrarNota=='1' && (control.tipoUsuarioBandeja=='1' || control.tipoUsuarioBandeja=='2')}">
					<!-- Abrir Registro de Notas -->
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgabrir','','${ctx}/images/botones/abrir_reg2.png',1)">
						<img alt="Abrir Registros" src="${ctx}/images/botones/abrir_reg1.png" id="imgabrir" onclick="fc_AbrirRegistro()" style="cursor:pointer">
					</a>			
					&nbsp;
				</c:if>				

				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcerrar','','${ctx}/images/botones/cerrar_reg2.png',1)">
					<img alt="Cerrar Registros" src="${ctx}/images/botones/cerrar_reg1.png" id="imgcerrar" onclick="fc_cerrar()" style="cursor:pointer">
				</a>			
				&nbsp;&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar1','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar1" onclick="fc_regresar()" style="cursor:pointer"></a>
			</td>
		</tr>
	</table>
</form:form>

</body>

