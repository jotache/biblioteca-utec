<%@ include file="/taglibs.jsp"%>

<head>	
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter"/>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-fechas.js" type="text/JavaScript"></script>	
<script>

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	fc_msgManager(mensaje);
	fc_verificaCeldas();	
	
}

function listarAlumnosEvalParciales(){
	var fecha = document.getElementById('txtFechaJotache');	
	if(fc_Trim(fecha.value)!=""){
		fc_buscar();
	}
}

function listarAlumnosEvalFecha(){	
	//var fecha = document.getElementById('txtFechaJotache');	
	if(fc_ValidaFecha('txtFechaJotache')){
		fc_buscar();		
	}
}

function fc_permiteValEnter(posObjEvt,obj){
	var notadig = document.getElementById(obj).value;
	if (notadig.length==2){
		var nom = "txt"+posObjEvt;			
		if(Number(posObjEvt)<Number(document.getElementById("hidTamanio").value))
			document.getElementById(""+nom).focus();	
			
		return true;
	}
}

function fc_permiteNumerosValEnter(posObjEvt,obj){
	
	var notadig = document.getElementById(obj).value;
	//alert(notadig.length)
	if(window.event.keyCode ==13  ){
				
		//alert(notadig.length);
		
		var nom = "txt"+posObjEvt;	
	
		if(Number(posObjEvt)<Number(document.getElementById("hidTamanio").value))
			document.getElementById(""+nom).focus();	
			
		return true;
	}
	
	//fc_PermiteNumeros();

}

function fc_habilitaCombos(){
	/*document.getElementById("cboSeccionReg").disdabled="";
	document.getElementById("cboNroEvaluacionReg").disdabled="";
	alert("w");return 0;*/
}

function fc_verificaCeldas(){
	form = document.forms[0];
	var max = form.hidTamanio.value;		
	for(i=0;i<max;i++){
		fc_verificaNota(i);
	}
	fc_verificaActualiza(max);			
}


function fc_validaCeldas(){

	form = document.forms[0];
	var max = form.hidTamanio.value;	
	var flgValida = 1;
	
		for(i=0;i<max;i++)
		{
			if(fc_Trim(document.getElementById("txt"+i).value)=="")
				flgValida = 0;
			
		}
	return flgValida;
}

function fc_verificaTodosVacios(){
	form = document.forms[0];
	var max = form.hidTamanio.value;	
	var flgValida = 1;	
	for(i=0;i<max;i++){
		if(fc_Trim(document.getElementById("txt"+i).value)!="")
			flgValida = 0;			
	}	
	return flgValida;
}

function fc_verificaActualiza(max){
	
	flgActualiza = 0;
		
	for(i=0;i<max;i++)
	{
		txt 	= document.getElementById("txt"+i);
		horaIni = txt.value;
		
		if(fc_Trim(horaIni)!="")
			flgActualiza = 1;
					
	}
	
	if( flgActualiza == 1 )
	{	form.operacion.value="irActualizar";
		form.txtFechaJotache.readOnly					=true;
		//form.cboEvaluacionParcial.disabled	=true;
		//form.cboNroEvaluacion.disabled		=true;
		form.imgCalendar.disabled				=true;		
	}
	else
		form.operacion.value="irRegistrar";
}


function fc_msgManager(mensaje){	
	if(mensaje!="")
	{
		if(mensaje.length>1)           		
		alert(mensaje);				    
	}
}

function fc_cambiaSeccion(){
	form = document.forms[0];
	//form.hCodSeccion.value = document.getElementById("cboSeccion").value;
	form.operacion.value="irCambiaSeccion";
	form.submit();
}

function fc_cambiaNroEvaluacion(){
	form = document.forms[0];	
	if(document.getElementById("cboNroEvaluacion").value=="T")
	{	
		if(document.getElementById("cboSeccion").value!="-1")
		{			
				url = 	"${ctx}/evaluaciones/registroEvaluaciones.html" +
						"?txhCodPeriodo=" + document.getElementById('codPeriodo').value + 
						"&txhCodEvaluador=" + document.getElementById('codEval').value + 
						"&txhCodCurso=" + document.getElementById('codCurso').value+							
						"&cboSeccion=" + document.getElementById('cboSeccion').value+
						"&cboEvaluacionParcial=" + document.getElementById('cboEvaluacionParcial').value+							
						"&txhCodProducto=" + document.getElementById('codProducto').value+
						"&txhCodEspecialidad=" + document.getElementById('codEspecialidad').value+
						"&txhTipoUsuarioBandeja=" + document.getElementById('tipoUsuarioBandeja').value+
						"&txhCodUsuarioBandeja=" + document.getElementById('codUsuarioBandeja').value;
					window.location.href = url;
		}else
		{	alert('Seleccione secci�n.');
			document.getElementById("cboNroEvaluacion").value="-1";
			document.getElementById("cboSeccion").focus();
		}
				
	}else{
		fc_buscar();
	}
	//alert("irCambiaNroEvaluacion");
	//form.operacion.value="irCambiaNroEvaluacion";
	//form.submit();
}

function fc_cambiaEvaluacionParcial(){

	form = document.forms[0];
	form.operacion.value="irCambiaEvaluacionParcial";
	form.submit();
	
}

function fc_buscar(){
	form = document.forms[0];
	form.operacion.value="irConsultar";
	
	if(form.cboEvaluacionParcial.value=="-1"){
		alert('Seleccione evaluaci�n parcial.');
		form.cboEvaluacionParcial.focus();	
	}else{
		
		if(form.cboSeccion.value=="-1")
		{
			alert('Seleccione secci�n.');
			document.getElementById("cboNroEvaluacion").value="-1";
			form.cboSeccion.focus();
		}		
		else if(document.getElementById("cboNroEvaluacion").value=="-1")
		{
			alert('Seleccione evaluaci�n parcial.');
			document.getElementById("cboNroEvaluacion").focus();
		}else{
			
			var evaluacion = $('cboNroEvaluacion');
			var text_eva = evaluacion.options[evaluacion.selectedIndex].text;
			$('txhNroEvaluacionInner').value = text_eva;
			$('frmMain').submit();
			form.submit();
		}
	}
}


function fc_grabar(){

	//JHPR 2008/04/08 Desabilitando boton Grabar
	form = document.forms[0];	
			
	if(fc_Trim(form.txtFechaJotache.value)==""){
		alert('Debe ingresar una fecha.');
		form.txtFechaJotache.focus();
		return 0;
	}else if(form.cboNroEvaluacion.value=="-1" ){
		alert('Se deben llenar todos los parametros obligatorios');
		form .cboNroEvaluacion.focus();
		return 0;
	}else if(form.cboEvaluacionParcial.value=="-1" ){
		alert('Se deben llenar todos los parametros obligatorios');
		form.cboEvaluacionParcial.focus();
		return 0;
	}
			
	var max = form.hidTamanio.value;
	var st	= "";var srdo= "";
	var objCodigoAlum;
	if(max>0){
	
		if(fc_verificaTodosVacios()==1)
		{	alert('Debe ingresar por lo menos una nota');
			return 0;	
		}
	
		if( confirm(mstrSeguroGrabar) ){
			form.imggrabar.disabled=true;

			for(i=0;i<max;i++){
			
				srdo = "";			
				objCodigoAlum 	= document.getElementById("codAlu"+i);
				codigoAlu 	= objCodigoAlum.value;					
				txtIni 		= document.getElementById("txt"+i);
				horaIni 	= txtIni.value;			
				rdo0 		= document.getElementById("RadioAN"+i);
				rdo1 		= document.getElementById("RadioNP"+i);
				rdo2 		= document.getElementById("RadioPE"+i);
				codSecc		= document.getElementById("txhCodSecc"+i);
				idDef		= document.getElementById("txhIdDef"+i);	
				if(rdo0.checked){
					srdo = rdo0.value;
				}else if(rdo1.checked){
					srdo = rdo1.value;
				}else if(rdo2.checked){
					srdo = rdo2.value;
				}				
				st=st+codigoAlu+"$"+horaIni+"$"+srdo+"$"+codSecc.value+"$"+idDef.value+"|";
			}	
			form.hidToken.value=st;
			//alert(st);			
			form.submit();
		}else {
			form.imggrabar.disabled=false;
		}
	
	}else
		alert(mstrNoSePuedeRealizarLaAccion);
}

function fc_cancelar(){
	form = document.forms[0];	
	url = "${ctx}/evaluaciones/bandejaEvaluador.html?txhCodPeriodo="+
	document.getElementById('codPeriodo').value+"&txhCodEvaluador="+document.getElementById('codEval').value;
	window.location.href =url; 
}

function fc_ValidaPromedio(strNameObj){
	var strCadena = strNameObj.value;
	var flg = true;
		if (fc_Trim(strCadena)!=""){
			if(strCadena < 0 || strCadena > 20){
				flg = false;
				strNameObj.value="";				
				strNameObj.focus();
			}
			

		}
	return flg;
}


function fc_verificaNota(i){

	//VALIDA QUE LA NOTA INGRESADA SEA VALIDA
	if(fc_ValidaPromedio(document.getElementById("txt"+i)))
	{
	
		rdo0 = document.getElementById("RadioAN"+i);
		rdo1 = document.getElementById("RadioNP"+i);
		rdo2 = document.getElementById("RadioPE"+i);
	
		if(document.getElementById("txt"+i).value=="")
			rdo2.checked = true;
		else if(document.getElementById("txt"+i).value=="0")
		{		
			//if(!rdo0.checked && !rdo1.checked)	
			//	rdo0.checked = true;
			//			fc_reset(rdo0,rdo1,rdo2);
						
		}else
		{
			fc_reset(rdo0,rdo1,rdo2);
		}
	
	}else
		alert('Ingrese nota v�lida');
}

function fc_verificaNotaTxt(i,obj){

	fc_ValidaNumeroOnBlur(obj);
	
	if(fc_ValidaPromedio(document.getElementById("txt"+i)))
	{
	
		rdo0 = document.getElementById("RadioAN"+i);
		rdo1 = document.getElementById("RadioNP"+i);
		rdo2 = document.getElementById("RadioPE"+i);
	
		if(document.getElementById("txt"+i).value=="")
		{	rdo2.checked = true;
			//alert("1");
		}else if(document.getElementById("txt"+i).value=="0" )
		{	
			if(rdo0.checked== false && rdo1.checked==false){
				fc_reset(rdo0,rdo1,rdo2);
				//alert("2");	
			}else{
			
			}
			
		}else
		{	
			fc_reset(rdo0,rdo1,rdo2);
		}
	
	
	}else
		alert('Ingrese nota v�lida');
}


function fc_reset(rdo0,rdo1,rdo2){
			rdo0.checked = false;
			rdo1.checked = false;
			rdo2.checked = false;
}

function fc_validaCheked(i){
	rdo0 		= document.getElementById("RadioAN"+i);
	rdo1 		= document.getElementById("RadioNP"+i);
	rdo2 		= document.getElementById("RadioPE"+i);
	
	if(rdo0.checked || rdo1.checked)
	{	rdo2.checked =false;		
		return false;
		
	}

}

function fc_verificaRadio(i){

			rdo0 		= document.getElementById("RadioAN"+i);
			rdo1 		= document.getElementById("RadioNP"+i);
			rdo2 		= document.getElementById("RadioPE"+i);

	if(rdo0.checked || rdo1.checked)
				document.getElementById("txt"+i).value="0";			
			
	if(document.getElementById("txt"+i).value=="" || document.getElementById("txt"+i).value=="0"){
		fc_verificaNota(i);
	}else
	{			
			fc_reset(rdo0,rdo1,rdo2);
	}
}

function fc_ValidaFecha(strNameObj) 
{	
	//strNameObj : Nombre de la caja de texto a validar.
	var Obj = document.all[strNameObj];
	var intEncontrado = "1234567890/".indexOf(String.fromCharCode(window.event.keyCode));		
	if (intEncontrado == -1) {
		window.event.keyCode = 0;		
	}
	//agregado 
	if (document.all[strNameObj].value.length == 2 || document.all[strNameObj].value.length == 5)
	{
	document.all[strNameObj].value = document.all[strNameObj].value + "/";
	}		
}

function fc_limpiar(){
	document.getElementById("txtNombre").value = "";
	document.getElementById("txtApaterno").value = "";
	document.getElementById("cboSeccion").value = "-1";
	document.getElementById("cboNroEvaluacion").value = "-1";
}
function fc_Generar(){
		u = "0003";
		url="?opcion=" + u +
			"&codPeriodo=" + document.getElementById("codPeriodo").value +
			"&codCurso=" + document.getElementById("codCurso").value +
			"&codSeccion=" + document.getElementById("cboSeccion").value +
			"&nombre=" + document.getElementById("txtNombre").value +
			"&apellido=" + document.getElementById("txtApaterno").value +
			"&nroEval=" + form.cboNroEvaluacion.options[form.cboNroEvaluacion.selectedIndex].text +
			"&evalParcial=" + document.getElementById("cboEvaluacionParcial").value +
			"&codProducto=" + document.getElementById("codProducto").value +
			"&codEspecialidad=" + document.getElementById("codEspecialidad").value +
			"&periodo=" + document.getElementById("txtPeridoVigente").value +
			"&nomEval=" + document.getElementById("txtProfesor").value +
			"&curso=" + document.getElementById("txtCurso").value +
			"&especialidad=" + document.getElementById("txtEspecialidad").value +
			"&sistEval=" + document.getElementById("txtSistEval").value +
			"&tipEval=" + form.cboEvaluacionParcial.options[form.cboEvaluacionParcial.selectedIndex].text + 
			"&ciclo=" + document.getElementById("txtCiclo").value +
			"&seccion=" + form.cboSeccion.options[form.cboSeccion.selectedIndex].text + 
			"&nroAsis=" + form.cboNroEvaluacion.options[form.cboNroEvaluacion.selectedIndex].text;
		
		window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"Reportes","resizable=yes, menubar=yes");
	
		}
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/evaluaciones/evaluaciones.html">
	
		<form:hidden path="operacion" id="operacion" />		
		<form:hidden path="hidTamanio" id="hidTamanio"/>
		<form:hidden path="hidToken" id="hidToken"/>		
		<form:hidden path="codPeriodo" id="codPeriodo"/>
		<form:hidden path="codEval" id="codEval"/>
		<form:hidden path="codCurso" id="codCurso"/>
		
		<form:hidden path="codProducto" id="codProducto"/>
		<form:hidden path="codEspecialidad" id="codEspecialidad"/>		
		<form:hidden path="txhNroEvaluacionInner" id="txhNroEvaluacionInner"/>			
				
		<form:hidden path="flagCerrarNota" id="flagCerrarNota"/>
		<form:hidden path="tipoUsuarioBandeja" id="tipoUsuarioBandeja"/>
		<form:hidden path="codUsuarioBandeja" id="codUsuarioBandeja"/>		
	
	<table cellpadding="0" cellspacing="0" class="borde_tabla" style="margin-left:11px; margin-top:10px" width="95%" border="0">		 
		<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%" colspan="8">
				Evaluaciones Parciales
			</td>		 	
		</tr>
		<tr class="fondo_dato_celeste">
			<td width="15%">&nbsp;Per�odo Vigente :</td>
			<td colspan=3 width="30%">
				<form:input path="txtPeridoVigente" id="txtPeridoVigente"
				cssClass="cajatexto_1" cssStyle="width:200px"
				readonly="true" /></td>
			<td width="15%">&nbsp;Ciclo :</td>
			<td width="30%">
			<form:input path="txtCiclo" id="txtCiclo" cssClass="cajatexto_1" cssStyle="width:20px" readonly="true" /></td>
			<td colspan="2" width="10%">Sist. Eval  :<form:input path="txtSistEval" id="txtSistEval" cssClass="cajatexto_1" cssStyle="width:20px"
				readonly="true" /></td>
			
		</tr>
		<tr class="fondo_dato_celeste">
			<td >&nbsp;Programa :</td>
			<td colspan=3>
			<form:input path="txtPrograma" id="txtPrograma"
				cssClass="cajatexto_1" cssStyle="width:250px"
				readonly="true" /></td>
			<td class="">&nbsp;Especialidad : </td>
			<td class="">
			<form:input path="txtEspecialidad" id="txtEspecialidad"
				cssClass="cajatexto_1" cssStyle="width:250px"
				readonly="true" /></td>
			<td colspan="2" class="">&nbsp;</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td>&nbsp;Curso :</td>
			<td colspan=3>
			<form:input path="txtCurso" id="txtCurso" cssClass="cajatexto_1"
				cssStyle="width:200px" readonly="true" /></td>
			<td>&nbsp;Prof. Responsable : </td>
			<td>
			<form:input path="txtProfesor" id="txtProfesor"
				cssClass="cajatexto_1" cssStyle="width:200px"
				readonly="true" /></td>
			<td colspan="2" class="">&nbsp;</td>
		</tr>
	 </table>

	 <table cellpadding="0" cellspacing="0" class="borde_tabla" style="margin-left:11px; margin-top:10px" width="95%" border="0">
		<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%" colspan="10">
				B�squeda de Alumnos
			</td>		 	
		</tr>
		<tr class="fondo_dato_celeste">
			<td >Apell. Paterno :</td>
			<td>
			<form:input path="txtApaterno" id="txtApaterno"  maxlength="35" onkeypress="javascript:fc_ValidaTextoEspecial();"  
				cssClass="cajatexto" cssStyle="width:80px" />&nbsp;</td>
			<td class="">&nbsp;Nombre :</td>
			<td>
			<form:input path="txtNombre" id="txtNombre"  maxlength="35" onkeypress="javascript:fc_ValidaTextoEspecial();" cssClass="cajatexto"
				cssStyle="width:80px" /></td>
			
			<td class="">Evaluaci�n Parcial :</td>
			<td>
			<form:select path="cboEvaluacionParcial" onchange="fc_cambiaEvaluacionParcial()" 
				id="cboEvaluacionParcial" cssClass="combo_o" cssStyle="width:115px">
				<form:option value="-1">--Seleccione--</form:option>
				<c:if test="${control.lstEvaluacionParcial!=null}">
					<form:options itemValue="codigo" itemLabel="descripcion"
						items="${control.lstEvaluacionParcial}" />
				</c:if>						
			</form:select></td>					
			<td class="">Secci�n :</td>
			<td>
				<form:select path="cboSeccion" id="cboSeccion"
				cssClass="combo_o" cssStyle="width:100px"
				onchange="fc_cambiaSeccion()">
				<form:option value="-1">--Seleccione--</form:option>
				<c:if test="${control.lstSeccion!=null}">
					<form:options itemValue="codSeccion" itemLabel="dscSeccion"
						items="${control.lstSeccion}" />
				</c:if>
			</form:select>	</td>
			<td class="">Nro Eval. :</td>
			<td>
				<form:select path="cboNroEvaluacion" id="cboNroEvaluacion"
					 onchange="fc_cambiaNroEvaluacion();" 
					 cssClass="combo_o" cssStyle="width:40px">
					<form:option value="-1">-Seleccione-</form:option>
					<form:option value="T">-Todos-</form:option>						
					<!--item de prueba codigo 3 -->
					<c:if test="${control.lstNroEvaluacion!=null}">
						<form:options itemValue="nrevId" itemLabel="nreNroEval"
							items="${control.lstNroEvaluacion}" />
					</c:if>
				</form:select>				
				 &nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" style="CURSOR: hand" id="imgBuscar" onclick="fc_buscar()" title="Listar evaluciones">
					   </a>
					   <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: hand" id="imglimpiar" onclick="fc_limpiar()" title="Limpiar selecci�n">
					  </a>	
					  <a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoExcel','','/SGA/images/iconos/exportarex2.jpg',1)">
						<img src="/SGA/images/iconos/exportarex1.jpg" align="absmiddle" alt="Excel" id="icoExcel" style="cursor:hand" onclick="javascript:fc_Generar();" title="Exportar a Excel">
					</a>
			</td>
		</tr>
	 </table>

	<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" align="center" width="98%">		
		<tr height="10px"><td></td></tr>			
		<tr>
			<td>				
				 <table cellpadding="0" cellspacing="0" border="0">
					 <tr>					 
					 	<td align="left"></td>					 	
					 	<td width="380px">
					 		<span class="cabecera">
					 			Registro de Evaluaciones Parciales
					 		</span>
					 		<span class="mensaje_rojo">
					 			<c:if test="${control.flagCerrarNota=='1'}">
					 				&nbsp;&nbsp;(REGISTRO CERRADO)
					 			</c:if>
					 		</span>
					 	</td>					 						 					 	
					 	<td align="right"></td>
					 </tr>
				 </table>
			</td>
		</tr>		
		<tr>
			<td valign="top">
			<div style="overflow: auto; height: 320px;width:99%" >
			<table class="bordegrilla" style="WIDTH: 98%;"  cellSpacing="1" cellPadding="0" border="0" borderColor="blue">
				<TR height="20px">
					<TD class="headtabla" width="10%">&nbsp;C�digo</TD>
					<TD class="headtabla" width="50%">&nbsp;Alumno</TD>
					<TD class="headtabla" width="40%">&nbsp;Fecha:

					<form:input path="txtFechaJotache" maxlength="10" id="txtFechaJotache"  
						onkeypress="fc_ValidaFecha('txtFechaJotache')" 
						onblur="fc_ValidaFechaOnblur('txtFechaJotache');listarAlumnosEvalFecha();" 
						cssClass="cajatexto_o" 
						cssStyle="width:75px" 						
						/>

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="<c:out value="${ctx}"/>/images/iconos/calendario1.jpg" name="imgCalendar" id="imgCalendar"
							STYLE="width: 20px; height: 15px;CURSOR: pointer" align=absmiddle alt="Calendario">
					</a>
					<form:select path="cboSeccionReg" id="cboSeccionReg"  disabled="true" 
						cssClass="combo_o" cssStyle="width:60px"
						onchange="fc_cambiaSeccion()">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.lstSeccion!=null}">
							<form:options itemValue="codSeccion" itemLabel="dscSeccion"
								items="${control.lstSeccion}" />
						</c:if>
					</form:select>									
					<form:select path="cboNroEvaluacionReg" onchange="fc_cambiaNroEvaluacion()" 
						id="cboNroEvaluacionReg"  cssClass="combo_o"  disabled="true" cssStyle="width:50px">
						<form:option value="-1">-Seleccione-</form:option>
						<form:option value="T">-Todos-</form:option>						
						<!--item de prueba codigo 3 -->
						<c:if test="${control.lstNroEvaluacion!=null}">
							<form:options itemValue="nrevId" itemLabel="nreNroEval"
								items="${control.lstNroEvaluacion}" />
						</c:if>
					</form:select>
					
					
					
					</TD>
				</TR>

				
				<c:set var="vfondo" value="" />
				<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop" >
					<%-- 
					<c:set var="vfondo" value="" />
					<c:if test="${loop.count % 2 == 0}"><c:set var="vfondo" value="fondoceldablanco" /></c:if>
					<c:if test="${loop.count % 2 != 0}"><c:set var="vfondo" value="fondocelesteclaro" /></c:if>
					--%>
					
					<%-- --%> 
					<c:choose>
						<c:when test="${loop.count % 2 == 0}"><c:set var="vfondo" value="fondoceldablanco" /></c:when>
						<c:otherwise><c:set var="vfondo" value="fondocelesteclaro" /></c:otherwise>
					</c:choose>
					
					<%-- 
					<tr	
						<c:if test="${objCast.flgSancion!='0'}"> class="texto_resaltado" title="El Alumno se encuentra sancionado" </c:if>
						<c:if test="${objCast.flgSancion=='0'}"> class=<c:out value="${vfondo}"></c:out> </c:if>
					>
					 --%>
						
						<tr	
						<c:if test="${objCast.estadoCursoAlumno=='R'}"> class="texto_resaltado" title="El Alumno est� retirado" </c:if>
						<c:if test="${objCast.estadoCursoAlumno!='R'}"> class=<c:out value="${vfondo}"></c:out> </c:if>
						>
					
										
						<input type="hidden" id="txhIdDef<c:out value="${loop.index}"/>" name="txhIdDef<c:out value="${loop.index}"/>" value="<c:out value="${objCast.idDef}"/>"/>
						<input type="hidden" id="txhFlgRea<c:out value="${loop.index}"/>" name="txhFlgRea<c:out value="${loop.index}"/>" value="<c:out value="${objCast.flgRealizado}"/>"/>
						<input type="hidden" id="txhCodSecc<c:out value="${loop.index}"/>" name="txhCodSecc<c:out value="${loop.index}" />" value="<c:out value="${objCast.codSeccion}"/>"/>
															
						<input type="hidden" id="codAlu<c:out value="${loop.index}" />" value="<c:out value="${objCast.codigo}" />" />							
							<td align="center" ><c:out value="${objCast.codigo}" /></td>							
							<td align="left" style="text-align: left" ><c:out value="${objCast.nombre}" />&nbsp;&nbsp;&nbsp;&nbsp;<c:if test="${objCast.estadoCursoAlumno=='R'}">(RETIRADO)</c:if></td>
							<td align="center" >
							
							<input <c:if test="${objCast.estadoCursoAlumno=='R'}">disabled="disabled"</c:if> id="txt<c:out value="${loop.index}" />" name="txt<c:out value="${loop.index}" />"   class="cajatexto" value="<c:out value="${objCast.nota}" />"   
								style="width: 40px" onkeyup="fc_permiteValEnter('<c:out value="${loop.index+1}" />','txt<c:out value="${loop.index}" />')" onkeypress="fc_permiteNumerosValEnter('<c:out value="${loop.index+1}" />','txt<c:out value="${loop.index}" />')" maxlength="2"  onblur="fc_verificaNotaTxt('<c:out value="${loop.index}" />','txt<c:out value="${loop.index}" />')" >
																
								<input type=radio <c:if test="${objCast.codTipoMotivo=='0001'}"><c:out value=" checked=checked " /></c:if>
								  onclick="fc_verificaRadio('<c:out value="${loop.index}" />')"  ID="RadioAN<c:out value="${loop.index}" />" NAME="Radio<c:out value="${loop.index}" />" 	value="0001"  >AN&nbsp;								
								<input type=radio <c:if test="${objCast.codTipoMotivo=='0002'}"><c:out value=" checked=checked " /></c:if>
								 onclick="fc_verificaRadio('<c:out value="${loop.index}" />')"  ID="RadioNP<c:out value="${loop.index}" />" NAME="Radio<c:out value="${loop.index}" />" 	value="0002"  >NP&nbsp;
								<input type=radio  <c:if test="${objCast.codTipoMotivo=='0003'}"><c:out value=" checked=checked " /></c:if>  
								onclick="fc_verificaRadio('<c:out value="${loop.index}" />')"  ID="RadioPE<c:out value="${loop.index}" />" NAME="Radio<c:out value="${loop.index}" />" 	value="0003"   >PE								
							
							
							</td>										
					</tr>
				</c:forEach>
			</table>
			</div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0"
		align="center" height="20px" width="98%" style="margin-top:5px">
		<tr>
			<td align="center">
				<c:if test="${control.flagCerrarNota=='1'}">					
					<img alt="Grabar" src="${ctx}/images/botones/grabarDisabled.jpg" id="imggrabar" style="cursor:pointer">					
				</c:if>
				<c:if test="${(control.flagCerrarNota=='0' || control.flagCerrarNota==null)}">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="fc_grabar()" style="cursor:pointer">
					</a>				
				</c:if>
				&nbsp;&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" onclick="fc_cancelar()" style="cursor:pointer"></a>
				
		</tr>
	</table>
</form:form>

<script type="text/javascript">
	Calendar.setup({
	inputField     :    "txtFechaJotache",
	ifFormat       :    "%d/%m/%Y",
	daFormat       :    "%d/%m/%Y",
	button         :    "imgCalendar",
	singleClick    :    true

	});
                          
</script>

</body>

