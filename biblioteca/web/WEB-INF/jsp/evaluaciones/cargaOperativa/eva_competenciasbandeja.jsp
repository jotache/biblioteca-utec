<%@ include file="/taglibs.jsp"%>
<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){}
		
		function fc_SelAlumno(codAlumno, nomAlumno)
		{
			document.getElementById("txhCodAlumno").value = codAlumno;
			document.getElementById("txhNomAlumno").value = nomAlumno;
		}
		
		function fc_registrar(indTipo)
		{
			if ( fc_Trim(document.getElementById("txhCodAlumno").value) == "")
			{
				alert(mstrSeleccione);
				return false;
			}
			
			strRuta = "${ctx}/evaluaciones/registro_eva_competencias.html";
			strRuta = strRuta + "?txhCodPeriodo=" + document.getElementById("txhCodPeriodo").value;
			strRuta = strRuta + "&txhCodEval=" + document.getElementById("txhCodEvaluador").value;
			strRuta = strRuta + "&txhCodCurso=" + document.getElementById("txhCodCurso").value;
			strRuta = strRuta + "&txhCodAlumno=" + document.getElementById("txhCodAlumno").value;
			strRuta = strRuta + "&txhNomAlumno=" + document.getElementById("txhNomAlumno").value;
			strRuta = strRuta + "&txhTipoCalificacion=" + indTipo;
			strRuta = strRuta + "&txhCodSeccion=" + document.getElementById("cboSeccion").value;
			strRuta = strRuta + "&txhCodTipoSesion=" + document.getElementById("txhCodTipoSesionDefecto").value;
			
			window.location.href = strRuta;
		}
		
		function fc_Buscar(){
			$('txhAccion').value='BUSCAR';
			$('frmMain').submit();
		}
		
		function fc_Regresar()
		{
			location.href = "${ctx}/evaluaciones/bandejaEvaluador.html"
							+ "?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value
							+ "&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value;
		}
		
		function fc_Limpiar()
		{
			document.getElementById("txtNombre").value = "";
			document.getElementById("txtApellidos").value = "";
			document.getElementById("cboSeccion").value = "";
		}
	</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/bandeja_eva_competencias.html" >
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codCurso" id="txhCodCurso"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="nomAlumno" id="txhNomAlumno"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codTipoSesionDefecto" id="txhCodTipoSesionDefecto"/>
	
	
	<table cellpadding="2" cellspacing="0" style="margin-left:6px" width="98%">
		<tr>
			<td>
				<table class="borde_tabla" style="width:98%;margin-top:6px; height:100px" cellspacing="0" cellpadding="0"> 
					<tr class="fondo_cabecera_azul">		 
					 	<td class="titulo_cabecera" width="100%" colspan="7">
							Evaluaci�n por Competencias
						</td>		 	
					</tr>				 
					<tr class="fondo_dato_celeste">
						<td >&nbsp;Per�odo Vigente</td>
						<td colspan=3>
							<form:input cssClass="cajatexto_1" path="periodoVigente" cssStyle="width:120px" readonly="true"/>
						</td>
						<td >&nbsp;Ciclo</td>
						<td>
							<form:input cssClass="cajatexto_1" path="ciclo" cssStyle="width:20px"/>
						</td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td >&nbsp;Producto</td>
						<td colspan=3>
							<form:input path="producto" cssClass="cajatexto_1" cssStyle="width:250px" readonly="true"/>
						</td>
						<td >&nbsp;Especialidad</td>
						<td >
							<form:input path="escialidad" cssClass="cajatexto_1" cssStyle="width:250px" readonly="true"/>
						</td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td >&nbsp;Curso</td>
						<td colspan=3>
							<form:input path="curso" cssClass="cajatexto_1" cssStyle="width:200px" readonly="true"/>
						</td>
						<td  >&nbsp;Sist. Eval.</td>
						<td>
							<form:input path="sistemaEval" cssClass="cajatexto_1" cssStyle="width:20px" readonly="true"/>
						</td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td >&nbsp;Evaluador</td>
						<td colspan=3>
							<form:input path="nomEvaluador" cssClass="cajatexto_1" cssStyle="width:200px" readonly="true"/>
						</td>	
						<td >&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>	
		<tr height="10px">
			<td></td>						
		</tr>		
		<tr>
			<td>
				<table class="borde_tabla" style="width:98%;margin-top:4px;height:30px" cellspacing="0" cellpadding="0" 
					>

					<tr class="fondo_cabecera_azul">		 
					 	<td class="titulo_cabecera" width="100%" colspan="6">
							Relaci�n de Alumnos
						</td>		 	
					</tr>

					<tr class="fondo_dato_celeste">
						<td>Apell. Paterno :</td>
						<td>
							<form:input path="apellidoAlumno" id="txtApellidos" cssClass="cajatexto"
								onkeypress="javascript:fc_ValidaTextoEspecial();" 
								onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Apellido');" 
								cssStyle="width:140px"/>
						</td>
						<td >Nombre :</td>
						<td >
							<form:input path="nombreAlumno" id="txtNombre" cssClass="cajatexto"
								onkeypress="javascript:fc_ValidaTextoEspecial();" 
								onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Nombre');" 
								cssStyle="width:140px"/>
						</td>
						<td  >Secci�n :</td>
						<td>
							<form:select path="codSeccion" id="cboSeccion" cssClass="combo_o" 
								cssStyle="width:50px">
								<form:option value="">SEL</form:option>
								<c:if test="${control.listaSeccion!=null}">
								<form:options itemValue="codSeccion" itemLabel="dscSeccion" 
									items="${control.listaSeccion}" />
								</c:if>
							</form:select>&nbsp;&nbsp;&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" align="middle" alt="Buscar" 
								style="cursor: pointer;" onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
							&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer;" onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<div style="overflow: auto; height: 320px; width: 950px">
					<table style="margin-top:0px;margin-bottom:0px; width: ${control.anchoTabla}px"
						cellspacing="1" cellpadding="0" id="tblAlumnoComp" class="bordegrilla" >
						<tr >
							<td class="headtabla" width="30px">&nbsp;Sel.</td>
							<td class="headtabla" width="80px">&nbsp;C�digo</td>
							<td class="headtabla" width="220px">&nbsp;Alumno</td>
							<c:forEach var="objDetalle" items="${control.listaCompetencias}" varStatus="loop" >
								<td class="headtabla" align="center" width="${control.anchoColumna}px">
									<c:out value="${objDetalle.descripcion}" /><br/>
									(<c:out value="${objDetalle.dscValor1}" />%)<br/>
									Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fin
								</td>
							</c:forEach>
							<td class="headtabla" align="center" width="${control.anchoColumna}px">
								Puntaje/<br/>
								Nota<br/>
								Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fin
							</td>
						</tr>		
						<c:forEach var="objEvalComp" items="${control.listaBandeja}" varStatus="loop1" >
							<!-- tr class=""-->
							
							<c:choose>
							  <c:when test="${loop1.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
							  <c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
							</c:choose>
							
								<td align="center" style="width:30px"><input type="radio" ID="chkCodAlumno" name="gpAlumnos"
												onclick="fc_SelAlumno('<c:out value="${objEvalComp.codAlumno}" />','<c:out value="${objEvalComp.nomAlumno}" />');"></td>
								<td align="center" style="width:80px"><c:out value="${objEvalComp.codAlumno}" /></td>
								<td style="text-align: left; width:220px">&nbsp;<c:out value="${objEvalComp.nomAlumno}" /></td>
								<c:forEach var="objCompAlum" items="${objEvalComp.listaCompetencias}" varStatus="loop2" >
									<td align="center" >
										<input name="color" value="<c:out value="${objCompAlum.notaIni}" />" class="cajatexto" 
											style="width:28px; text-align: right" disabled="disabled">
										<span style="text-align: right; font-size: 8pt"><c:out value="${objCompAlum.codEvalIni}" /></span>
											&nbsp;&nbsp;
										<input name="color" value="<c:out value="${objCompAlum.notaFin}" />" class="cajatexto" 
											style="width:28px; text-align: right" disabled="disabled">
										<span style="text-align: right; font-size: 8pt"><c:out value="${objCompAlum.codEvalFin}" /></span>
									</td>
								</c:forEach>
							</tr>
						</c:forEach>
					</table>
					<% request.getSession().removeAttribute("listaAlumnoComp"); %>
				</div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" align="center" height="30px" width="98%">	
		<tr>
			<td align="center">										
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregini','','${ctx}/images/botones/reginicial2.jpg',1)">
				<img alt="Registro Inicial" src="${ctx}/images/botones/reginicial1.jpg" id="imgregini" onclick="javascript:fc_registrar('1');" style="cursor:pointer;"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregfin','','${ctx}/images/botones/registrofin2.jpg',1)">
				<img alt="Registro Final" src="${ctx}/images/botones/registrofin1.jpg" id="imgregfin" onclick="javascript:fc_registrar('2');" style="cursor:pointer;"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar1','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar1" onclick="javascript:fc_Regresar();" style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript" language="javascript">
	document.getElementById("txhAccion").value = "";
	document.getElementById("txhMsg").value = "";
</script>	