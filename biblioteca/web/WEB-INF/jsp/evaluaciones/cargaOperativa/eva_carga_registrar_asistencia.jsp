<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<%@page import='com.tecsup.SGA.modelo.Asistencia'%>
<%@ include file="/taglibs.jsp"%>

<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script src="${ctx}/scripts/js/funciones_eva-fechas.js" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" type="text/JavaScript"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
	/*Global*/
	
	var isIE = (window.navigator.userAgent.indexOf("MSIE")> 0);  
	if (! isIE){   		
	  HTMLElement.prototype.__defineGetter__("innerText",function () { return(this.textContent); });   
	  HTMLElement.prototype.__defineSetter__("innerText",function (txt) { this.textContent = txt; });
	}
	var x='txhAsis';
	var y=0;

	document.onkeydown=function(event){
		//Event.observe(document,'keypress',function(event){
			var key;
			if (window.event) {
				key = window.event.keyCode;     //IE
			} else {
				key = event.which;     //firefox
			}
			try{
			switch (key) {   
				case Event.KEY_RIGHT:   
					//alert('moved right');     
					break;   
				case Event.KEY_LEFT:     
					//alert('moved left');     
					break; 
				case Event.KEY_UP:     
					up();
					break;   
				case Event.KEY_DOWN:     
					down(); 
					break; 
			}
			//lee()
			}catch(e){}
		//});
		};
		//function lee(){alert(this.y)}
		function down(){
			if($('txhAsis'+(this.y+1))){
				howChecked(this.y+1);
				$('txhAsis'+(this.y+1)).focus();
				this.y += 1;
			}
		}

		function up(){
			if($('txhAsis'+(this.y-1))){
				howChecked(this.y-1);
				$('txhAsis'+(this.y-1)).focus();
				this.y -= 1;
			}
		}

		function howChecked(_y){
			try{
				if($('txhAsis'+_y).checked == true){
					setTimeout("checkIT('"+('txhAsis'+_y)+"')",10);
				}else if($('txhFalt'+_y).checked == true){
					setTimeout("checkIT('"+('txhFalt'+_y)+"')",10);
				}else if($('txhTard'+_y).checked == true){
					setTimeout("checkIT('"+('txhTard'+_y)+"')",10);
				}
			}catch(e){alert(e.description)}
		}

		function checkIT(inp){
			$(inp).focus();
			$(inp).checked = true;
		}
		
		function setRadio(_x,_y){
			this.x=_x; this.y=_y;
		}

	
	function fc_Calendar(txt, btn){
		Calendar.setup({
			inputField     :    txt,
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    btn,
			singleClick    :    true
		});
	}
	
	var fe;
	var nro;
	var fecha = "<%=((request.getAttribute("fechaActual")==null)?"":request.getAttribute("fechaActual"))%>";
	var longFechas;
	
		function onLoad(){
			if(document.getElementById("txhFe").value!=""){
				$('exportar').style.display = '';
			}
		}
		
		function fc_SeleccionaFilaBandeja_OnRowClick(pNumFila){
			longFechas = document.getElementById('tblFechas').rows.length - 1;
			for (k=0; k<longFechas ;k++){
				var a = document.getElementById('txtSel'+k);
				if(a != null){
					document.getElementById('txtSel'+k).checked = false;
				}
			}
		    document.getElementById('txtSel'+pNumFila).checked = true;
		}
		
		function fc_Seleccionar(strValor, fecha){
			document.getElementById('txtNroAsistencia').value = fc_Trim(strValor);
			document.getElementById('fechaEliminar').value = document.getElementById('txtFecha'+fc_Trim(strValor)+"_"+fecha.replace("/","").replace("/","")).value;
			document.getElementById('nroSesionEliminar').value = strValor;
		}
		
		function fc_Atras(){
			window.location.href = "${ctx}/evaluaciones/controlAsistencia.html" + 
									"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
									"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
									"&txhCodCurso=" + document.getElementById('txhCodCurso').value +
									"&txhCodProducto=" + document.getElementById('txhCodProducto').value +
									"&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value +
									"&txhTipoSesion=" + document.getElementById('txhTipoSesion').value + 
									"&txhCodSeccion=" + document.getElementById('txhCodSeccion').value;
		}
		
		function fc_Add(){			
			$('txhOperacion').value='BUSCAR_ALUM';
			$('frmMain').submit();
		}
		function fc_Agregar(){
			var objTabla = document.getElementById('tblFechas');
			var tabla = $('tblFechas');			
			var nroSesion = 1;		
				
			if ( objTabla.rows[0].cells.length > 0)
			{
				var objLastCell = objTabla.rows[0].cells[objTabla.rows[0].cells.length - 1];
				var objTdNroSesion = objLastCell.childNodes[0].rows[0].cells[1];
				
				if (! isIE)
					nroSesion = parseFloat(objTdNroSesion.textContent) + 1;
				else				
					nroSesion = parseFloat(objTdNroSesion.innerText) + 1; //IE
			}
			var cont = 0;
			var flag;

			//Busca si hay un registro de aistencia pendiente antes de a�adir otro
			for(i=0; i<nroSesion; i++){
				flag = document.getElementById('FAsist');				
				if(flag != null){
					//alert('flag encontrado');
					cont=cont+1;
				}				
			}
					
			if(cont>1){
				alert('Debe resgistrar la asistencia para la fecha seleccionada');
				return false;
			}
						
			if (/*! isIE*/ true ){						
				var elmTBODY = document.getElementById('THETR');
				var elmTR;
				var elmTD;
				var elmText;
				elmTR = elmTBODY.insertCell(-1);
				elmTR.setAttribute('id','FAsist');
				elmTR.setAttribute('width','86px');
				elmTR.innerHTML = '<table style="width:100%;border: 1px solid #048BBA;border-left:none; table-layout: fixed;" cellSpacing="0" cellPadding="0" border="0" align="left"> <tbody> <tr> <td width="18" height="26" class="tablagrillaglo" style="text-align:center;"> <input type="radio" name="codDetalle" disabled> </td> <td class="tablagrillaglo" style="text-align:center;">'+nroSesion+'</td> <td width="28" class="tablagrillaglo" style="text-align:center"> <img src="${ctx}/images/iconos/modificar_disabled.jpg" alt="Mostrar" style="vertical-align:bottom"> </td> <tr> <td colspan="2" height="27" class="tablagrillaglo" style="text-align:left;padding-left: 3px;"> <input type="text" id="txtFecha" name="txtFecha" value="'+ fecha + '" class="cajatexto" size="10" onkeypress=javascript:fc_ValidaFecha(this.id); onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10"  style="width: 50px;"/></td> <td class="tablagrillaglo" style="text-align:center;"><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage(\'imgCalendarX\',\'\',\'${ctx}/images/iconos/calendario2.jpg\',1)"> <img src="${ctx}/images/iconos/calendario1.jpg" id="imgCalendarX" name="imgCalendarX" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a> </td> </tr> </tr></tbody></table>';
				//elmTR = elmTBODY.insertCell(-1); //Una celda adicional
			}else{
				var container = objTabla.rows[0]; 
				objItem = document.createElement("<td width='86px' id='FAsist'></td>"); 
				
				objTable = document.createElement('<table style="width:100%;border: 1px solid #048BBA;border-left:none; table-layout: fixed;" cellSpacing="0" cellPadding="0" border="0" align="left"></table>'); 

				objTbody = document.createElement("<tbody></tbody>"); 

				objFil1 = document.createElement("<tr></tr>"); 
				
				objCol0 = document.createElement('<td width="18px" height="26px" class="tablagrillaglo" style="text-align:center;"></td>');
				objCol0.innerHTML = '<input type="radio" name="codDetalle" disabled>';
				
				objCol1 = document.createElement('<td class="tablagrillaglo" style="text-align:center;"></td>');
				objCol1.innerHTML = nroSesion;
				
				objCol2 = document.createElement("<td width='28px' class='tablagrillaglo' style='text-align:center'></td>");
				objCol2.innerHTML = '<img src="${ctx}/images/iconos/modificar_disabled.jpg" alt="Mostrar" style="vertical-align:bottom">';

				objFil2 = document.createElement("<tr></tr>"); 

				objCol3 = document.createElement('<td colspan="2" height="27" class="tablagrillaglo" style="text-align:left;padding-left: 3px;"></td>');
				objCol3.innerHTML = "<input type='text' id='txtFecha' name='txtFecha' value='"+  fecha + "' class='cajatexto' size='10' onkeypress=javascript:fc_ValidaFecha(this.id); onblur='javascript:fc_ValidaFechaOnblur(this.id);' maxlength='10' style='width: 50px;' />";

				objCol4 = document.createElement('<td class="tablagrillaglo" style="text-align:center;"></td>');
				objCol4.innerHTML = '<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage(\'imgCalendarX\',\'\',\'${ctx}/images/iconos/calendario2.jpg\',1)">' +
				'<img src="${ctx}/images/iconos/calendario1.jpg"  id="imgCalendarX" name="imgCalendarX" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>';
				
				
				objFil1.appendChild(objCol0);		
				objFil1.appendChild(objCol1);
				objFil1.appendChild(objCol2);
				objTbody.appendChild(objFil1);
				
				objFil2.appendChild(objCol3);
				objFil2.appendChild(objCol4);
				objTbody.appendChild(objFil2);

				objTable.appendChild(objTbody);
				
				objItem.appendChild(objTable);

				container.appendChild(objItem);

				//container.appendChild(document.createElement("<td></td>")); //una celda adicional
			}

			document.getElementById("txtFecha").focus();
		}
		
		function fc_getCadena(){
			var tbl = document.getElementById('tblAsistenciaCurso');
			var objTabla = document.getElementById('tblFechas');
			//CCORDOVA 11/06/2008
			//var nroSesion = objTabla.rows.length - 1;
			var objLastCell = objTabla.rows[0].cells[objTabla.rows[0].cells.length - 1]; 
			var objTdNroSesion = objLastCell.childNodes[0].rows[0].cells[1];
			if (! isIE)
				nroSesion = parseFloat(objTdNroSesion.textContent);
			else				
				nroSesion = parseFloat(objTdNroSesion.innerText); //IE
			
			var nroSesionConsulta = "";	
			/*if(document.getElementById('txhNroSesionConsulta').value==""){
				var tbl = document.getElementById('tblAsistenciaCurso');
				nroSesionConsulta = tbl.rows.length;			
			}
			else{
				nroSesionConsulta = document.getElementById('txhNroSesionConsulta').value;
			}*/
			var nroSesionConsulta = document.getElementById('txhNroSesionConsulta').value;
			var fechaConsulta = document.getElementById('txhFe').value;
						
			var str1 = "";
			var str2 = "";
			var codSecci = ""; //secciones
			var nombre = "";
			var cont = 0;
		
			for(i=1 ; i < tbl.rows.length ; i++){
				j = i-1;
				str1 = tbl.rows[i].cells[0].innerHTML;
				
				objAsist = document.getElementById('txhAsis'+j);
				objFalta = document.getElementById('txhFalt'+j);
				objTarde = document.getElementById('txhTard'+j);
				
				codSecci = document.getElementById('txhCodSecc'+j); //cod. seccion.				
				codAsist = document.getElementById('txhCodAsis'+j);
				//txhSancionAdm9" name="txhSancionAdm9" value="1"/>				
				var objSancionAdm = document.getElementById('txhSancionAdm'+j); 
								
				if(!(objAsist.checked) && !(objFalta.checked) && !(objTarde.checked)){
					nombre = nombre + "- " + tbl.rows[i].cells[1].innerHTML + "\n";
					cont++;
				}
				else if( ((objAsist.disabled) && (objFalta.disabled) && (objTarde.disabled)) && objSancionAdm.value=='0' ){
					alert('Debe ingresar y/o grabar la fecha para registrar la asistencia');
					return false;
				}
				else{
					if(objAsist.checked) str2 = objAsist.value;
					if(objFalta.checked) str2 = objFalta.value;
					if(objTarde.checked) str2 = objTarde.value;
				}		
				if(fc_Trim(codAsist.value) != 'null' && codAsist.value != ""){
					//UPDATE		
					document.getElementById('txhCadena').value = 
						document.getElementById('txhCadena').value + 
						str1 + '$' + str2 + '$'+ nroSesionConsulta + '$'+ document.getElementById('txtFecha'+fc_Trim(nroSesionConsulta)+"_"+fechaConsulta.replace("/","").replace("/","")).value + '$' + codSecci.value + '$_';
				}
				else{
					//INSERT
					document.getElementById('txhCadena').value = 
						document.getElementById('txhCadena').value + 
						str1 + '$' + str2 + '$'+ nroSesion + '$'+ document.getElementById('txtFecha').value + '$' + codSecci.value + '$_';	
				}
			}			
			if(cont > 0){
				alert('Falta registrar asistencia al(los) alumno(s):\n\n' + nombre);
				return false;
			}
						
			return true;
		}
		
		function fc_Buscar(obj, strValor,i,fe){
		
			if(i == "" || fe == ""){			
				try{
					i=$F('txhNro'+(parseFloat($F(strValor))-1));
					fe=$F('txhFec'+(parseFloat($F(strValor))-1));
				}catch(e){return false;}
			}
		
			//var Obj = document.all[strValor];
			var Obj = $(strValor);
			var objTabla = document.getElementById('tblFechas');
			var cant = objTabla.rows[0].cells.length - 1;
			if(cant < 0){
				alert('No hay registros de asistencia para realizar la b�squeda.');
				return false;
			}

			//Valido ingreso del nro sesion del curso para ver la asistencia
			if(Obj.value == ""){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Nro. Asistencia.');
				return false;
			}
			//alert(Obj.value);
			switch(obj){
				case '1':
					document.getElementById('txhOperacion').value = "BUSCAR_ALUM";
					break;
				case '2':
					nro = "";
					nro = parseInt(i);
					document.getElementById("txhNro").value = nro;
					document.getElementById("txhFe").value = fe;
					document.getElementById('txhOperacion').value = "BUSCAR_ASIST";
					document.getElementById('txtNroAsistencia').value = Obj.value;
					break;
				default:
					break;
			}
			document.getElementById('frmMain').submit();
		}
		
		function fc_Grabar(){		
			form = document.forms[0];
			var lst = "<%=((request.getAttribute("listaAsistenciaCurso")==null)?"":request.getAttribute("listaAsistenciaCurso"))%>"; //request.getAttribute("listaAsistenciaCurso")
			if (lst!= "[]"){
				if(fc_ValidaFechaGrabar()){
					if(fc_getCadena()){
						if(confirm('�Est� seguro de grabar la asistencia?')){
							form.imggrabar.disabled=true;
							form.imggrabarfec.disabled=true;
							document.getElementById("saveFecha").value = "0";
							document.getElementById("txhOperacion").value = "GRABAR";
							//console.log(document.getElementById('txhCadena').value);
							document.getElementById("frmMain").submit();
						}else{
							form.imggrabar.disabled=false;
							form.imggrabarfec.disabled=false;
						}
					}
				}
			}
			else{
				alert('No hay registros para grabar la asistencia.');
				return false;
			}
		}
		
		//JHPR: 1/ABRIL/2008 Nueva Funci�n para conversion a Fechas.
		function retornaFecha(datoFecha){				
			var fechaIngresadaS = datoFecha; 			
			array_fecha = fechaIngresadaS.split("/"); 			
			var dia=array_fecha[0]; 
			var mes=(array_fecha[1]-1); 
			var ano=(array_fecha[2]); 
			var fechaIngresadaD = new Date(ano,mes,dia); 			
			return fechaIngresadaD;
		}
		
		function fc_GrabarFecha(){
			var fila = document.getElementById('FAsist');
			
			if(fila == null){				
				alert('Debe ingresar y/o grabar la fecha para registrar la asistencia');
				return false;
			}
			
			if(!confirm('�Est� seguro de programar la asistencia?')){
				return false;
			}
								
			if(document.getElementById('txtFecha').value != ""){		
				if(fc_VerificaFechaRepetida()){
					document.getElementById('txtFecha').disabled = true;
					document.getElementById('txtFecha').style.background = "#f0ede7";
					
					fc_DeshabilitaRadio();
					
					
					//fc_Grabar(); //Para grabar directamente
					//Erickpm 14-10-09
					form = document.forms[0];
					var lst = "<%=((request.getAttribute("listaAsistenciaCurso")==null)?"":request.getAttribute("listaAsistenciaCurso"))%>"; //request.getAttribute("listaAsistenciaCurso")
					if (lst!= "[]"){
						if(fc_ValidaFechaGrabar()){
							if(fc_getCadena()){
								form.imggrabar.disabled=true;
								form.imggrabarfec.disabled=true;
								document.getElementById("saveFecha").value = "1";								
								document.getElementById("txhOperacion").value = "GRABAR";
								document.getElementById("frmMain").submit();
							}
						}
					}
					else{
						alert('No hay registros para grabar la asistencia.');
						return false;
					}
					//-----
			
				}
			}else{				
				alert('Debe ingresar una fecha.');
				document.getElementById('txtFecha').focus();
				return false;
			}
		}
		
		function fc_ValidaFechaGrabar(){
			var objTabla = document.getElementById('tblFechas');	
			var k = objTabla.rows.length - 2 ;
			if(document.getElementById('txtFecha'+k)!=null){
				/*if(document.getElementById('txtFecha'+k).value == fecha){
					alert(mstrDateAlreadySaved);
					return false;
				}*/
			}
			return true;
		}
		
		function fc_ValidaFechaIgual(){
			var objTabla = document.getElementById('tblFechas');	
			var k = objTabla.rows.length - 2 ;
			if(document.getElementById('txtFecha'+k)!=null){
				if(document.getElementById('txtFecha'+k).value == fecha){
					alert('Ya se registr� la asistencia de hoy');
					return false;
				}
			}
			return true;
		}
		
		function fc_DeshabilitaRadio(){
			var tbl = document.getElementById('tblAsistenciaCurso');
			for(i=1 ; i < tbl.rows.length ; i++){
				j = i-1;
				str1 = tbl.rows[i].cells[0].innerHTML;
				
				objAsist = document.getElementById('txhAsis'+j);
				objFalta = document.getElementById('txhFalt'+j);
				objTarde = document.getElementById('txhTard'+j);
				
				objAsist.disabled = false;
				objFalta.disabled = false;
				objTarde.disabled = false;
			}
		}
		
		function fc_VerificaFechaRepetida(){
			var tbl = document.getElementById('tblFechas');
			var val0 = document.getElementById('txtFecha').value;
			var val1 = "";
			
			for(i=0 ; i < tbl.rows.length - 1 ; i++){
				val1 = document.getElementById('txtFecha'+i);
				if(val1 != null){
					if(val1.value == val0){
						alert('Ya se encuentra registrada la asistencia para la fecha ' + val0);
						document.getElementById('txtFecha').value = "";
						document.getElementById('txtFecha').focus();
						return false;
					}
				}
			}
			return true;
		}
		
		function fc_Generar(){
		u = "0001";
		url="?opcion=" + u +
			"&codPeriodo=" + document.getElementById("txhCodPeriodo").value + 
			"&nroSesion=" + document.getElementById("txhNroSesionConsulta").value + 
			"&codCurso=" + document.getElementById('txhCodCurso').value +
			"&codProducto=" + document.getElementById('txhCodProducto').value +
			"&codEspecialidad=" + document.getElementById('txhCodEspecialidad').value +
			"&tipoSesion=" + document.getElementById('txhTipoSesion').value +
			"&codSeccion=" + document.getElementById('txhCodSeccion').value + 
			"&curso=" + document.getElementById('txtCurso').value + 
			"&nomEval=" + document.getElementById('txhNomEval').value +
			"&seccion=" + document.getElementById('txtSeccion').value +
			"&nroAsis=" + document.getElementById('txhNro').value +
			"&feAis=" + document.getElementById('txhFe').value +
			"&sisEval=" + document.getElementById('txtSistEval').value; 
		window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"Reportes","resizable=yes, menubar=yes");
	
		}
		
		function fc_Eliminar(){
			if(document.getElementById('fechaEliminar').value!=""){
				if (confirm(mstrEliminar)){
					document.getElementById("txhOperacion").value = "ELIMINAR";
					document.getElementById('frmMain').submit();
				}
			}
			else{
				alert(mstrSeleccione);
				return false;
			}
		}

	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/registrarAsistencia.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>		
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEval" id="txhCodEvaluador"/>
		<form:hidden path="codCurso" id="txhCodCurso"/>
		<form:hidden path="nomEval" id="txhNomEval"/>
		<form:hidden path="codSeccion" id="txhCodSeccion"/>
		<form:hidden path="nroSesionConsulta" id="txhNroSesionConsulta" />		
		<form:hidden path="codProducto" id="txhCodProducto"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<form:hidden path="tipoSesion" id="txhTipoSesion"/>		
		<form:hidden path="fe" id="txhFe"/> 
		<form:hidden path="nro" id="txhNro"/>		
		<form:hidden path="cadena" id="txhCadena"/>		
		<form:hidden path="saveFecha" id="saveFecha"/>
		<form:hidden path="fechaEliminar" id="fechaEliminar"/>
		<form:hidden path="nroSesionEliminar" id="nroSesionEliminar"/>
		
		<table cellpadding="0" cellspacing="0" border="0" align="center">
			<tr>
				<td>
<!--	background="${ctx}/images/Evaluaciones/back.jpg" -->
					<table class="borde_tabla" cellpadding="0" cellspacing="0" style="width:940px;margin-top:0px">
						<tr class="fondo_cabecera_azul">		 
						 	<td class="titulo_cabecera" width="100%" colspan="8">
								Registrar Asistencia
							</td>		 	
						</tr>
 						<tr style="background-color: #78C4DC; color: white; font-weight: bold;">
							<td width="80">&nbsp;Curso :</td>
							<td width="300"><form:input path="curso" id="txtCurso" size="70" cssClass="cajatexto_1" readonly="true"/></td>
							<td width="70">&nbsp;</td>
							<td width="100">&nbsp;Secci&oacute;n :</td>
							<td width="50"><form:input path="seccion" cssStyle="text-align:center" id="txtSeccion" size="15" cssClass="cajatexto_1" readonly="true"/></td>
							<td width="70">&nbsp;</td>
							<td width="100">Sist. Eval: </td>
							<td width="50"><form:input path="sistEval" id="txtSistEval" size="6" cssClass="cajatexto_1" readonly="true"/></td>
						</tr>
					</table>
			  	</td>
			</tr>
			<tr><td colspan="4" height="5px"></td></tr>
			<tr>
							<td align="center" colspan="4" valign="top">
							
							<!-- Cambio -->
							
							<table cellSpacing="0" cellPadding="0" align="center" border="0" bordercolor="blue" width="100%" style="table-layout: fixed;">
								<tr>
									<td valign="top" align="left" width="80">
										<table style="width:100%;border: 1px solid #048BBA;" cellSpacing="1" cellPadding="0" border="0" align="left">
											<tr>
												<td class="grilla">Nro. Asist.</td>
											</tr>
											<tr>
												<td class="grilla">Fecha</td>
											</tr>
										</table>
									</td>
									<td valign="top" align="left" width="830">
										<div style="overflow-y:none;overflow-x:auto;height:72px; width:100%;" id="div_fechas">
										<!-- style="table-layout:fixed;" -->
										<table id="tblFechas" style="table-layout:fixed;" cellSpacing="0" cellPadding="0" border="0" align="left">
											<tbody id="TBODY">
											<tr id="THETR">
											<%
											ArrayList lista = (ArrayList)request.getAttribute("listaFechas");
											
											if(lista != null){
												for(int i = 0; i < lista.size(); i++){
													Asistencia lObject = (Asistencia)lista.get(i);
											%>
												<td width="92px" id="Fila<%=i%>" align="left"><table style="width:90px;border: 1px solid #048BBA;border-left:none; table-layout:fixed;" cellSpacing="0" cellPadding="0" border="0" align="left">
														<tr>
															<td width="18px" height="26" class="tablagrillaglo" style="text-align:center;">
																<input type="radio" id="txtSel<%=i%>" name="txtSel" <%if ("0".equalsIgnoreCase(lObject.getFlgProg())){%>disabled="disabled" <%}%> onclick="javascript:fc_Seleccionar('<%=lObject.getNroSesion()%>','<%=lObject.getFecha()%>'); fc_SeleccionaFilaBandeja_OnRowClick('<%=i%>');">
															</td>
															<td class="tablagrillaglo" style="text-align:center;"><%=lObject.getNroSesion()%>
																<input type="hidden" id="txhNro<%=i%>" name="txhNro<%=i%>" value="<%=lObject.getNroSesion()%>" />
																<input type="hidden" id="txhFec<%=i%>" name="txhFec<%=i%>" value="<%=lObject.getFecha()%>" />
																<input type="hidden" id="txhFlgProg<%=i%>" name="txhFlgProg<%=i%>" value="<%=lObject.getFlgProg()%>" />
															</td>
															<td width="28px" class="tablagrillaglo" style="text-align:center;">
																<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificar<%=i%>','','${ctx}/images/iconos/modificar2.jpg',1)">
																<img src="${ctx}/images/iconos/modificar1.jpg" style="cursor:pointer;" id="imgModificar<%=i%>" name="imgModificar<%=i%>" alt="Mostrar" style="CURSOR: pointer; vertical-align:bottom"
																onclick="javascript:fc_Buscar('2','txhNro<%=i%>','<%=lObject.getNroSesion().trim()%>','<%=lObject.getFecha()%>');" style="cursor: pointer;"></a>
															</td>
														</tr>
														<tr>
															<td colspan="2" height="27" class="tablagrillaglo" style="text-align:left;padding-left: 3px;" width="50px">
																<input id="txtFecha<%=lObject.getNroSesion().trim()%>_<%=lObject.getFecha().replaceAll("/","")%>" name="txtFecha<%=i%>" value="<%=lObject.getFecha()%>" class="cajatexto" readonly size="10" 
																	onblur="javascript:fc_ValidaFechaOnblur(this.id);" onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10" style="width: 50px;">
															</td>
															<td class="tablagrillaglo" style="text-align:center;">
																
																<%if (!"0".equalsIgnoreCase(lObject.getFlgProg())){%>
																	<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar<%=i%>','','${ctx}/images/iconos/calendario2.jpg',1)">
																	<img src="${ctx}/images/iconos/calendario1.jpg"  id="imgCalendar<%=i%>" name="imgCalendar<%=i%>" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
																	<script type="text/javascript">
																		Event.observe(window,'load',function(){  
																			fc_Calendar("txtFecha<%=i%>", "imgCalendar<%=i%>");
																		},false);
																	</script>
																<%}else{%>
																		<img src="${ctx}/images/iconos/calendario1_disable.jpg" alt="Calendario" title="ASISTENCIA TOMADA" style="vertical-align:bottom">
																<%}%>
															</td>
														</tr>
													</table>
												</td>
											<%
												}
											}
											%>	
											</tr>
											</tbody>
										</table>
										</div>
									</td>
									<td valign="top" align="right" width="26">
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
										<img hspace="3" vspace="3" src= "${ctx}/images/iconos/agregar1.jpg" id="imgAgregar" onclick="javascript:fc_Add();" style="cursor:pointer;" alt="Agregar Fecha"></a><br>
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabarfec','','${ctx}/images/iconos/grabar2.jpg',1)">
										<img hspace="3" src= "${ctx}/images/iconos/grabar1.jpg" id="imggrabarfec" onclick="javascript:fc_GrabarFecha();" style="cursor:pointer;" alt="Grabar Fecha"></a><br>
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgElimFecha','','${ctx}/images/iconos/quitar2.jpg',1)">
										<img hspace="3" vspace="3" src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" id="imgElimFecha" onclick="javascript:fc_Eliminar();"></a>
									</td>
								</tr>
							</table>
							
							<!-- Fin CAmbio -->
							
						</td>
					</tr>
					<tr style="height:20px; display: none">
						<td class="opc_combo">	ax						
						</td>						
					</tr>
					<tr>
						<td>
							<table class="borde_tabla" style="width:940px;margin-top:4px;background-color: #78C4DC;" cellspacing="0" cellpadding="0">
								<tr class="fondo_cabecera_azul">		 
								 	<td class="titulo_cabecera" width="100%" colspan="8">
										Relaci�n de Alumnos
									</td>		 	
								</tr>
								<tr style="background-color: #78C4DC; color: white; font-weight: bold;">
									<td >Apell. Paterno :</td>
									<td><form:input path="apellido" id="txtApellido" cssClass="cajatexto" size="35" maxlength="20" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNumerosAndLetrasFinalTodo(this.id,'Apellido Paterno');"/>&nbsp;</td>
									<td >Nombre :</td>
									<td><form:input path="nombre" id="txtNombre" cssClass="cajatexto" size="35" maxlength="20" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNumerosAndLetrasFinalTodo(this.id,'Nombre');"/></td>
									<td >Nro. Asistencia :</td>
									<td><form:input path="nroSesion" id="txtNroAsistencia" cssClass="cajatexto" size="5" onkeypress="javascript:fc_PermiteNumeros();" onblur="javascript:fc_ValidaNumeroOnBlur(this.id);" maxlength="2"/>
									&nbsp;&nbsp;&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
									<img src="${ctx}/images/iconos/buscar1.jpg"  align="absmiddle" alt="Buscar" style="CURSOR: hand" id="imgBuscar" onclick="javascript:fc_Buscar('2','txtNroAsistencia',$F('nroSesionEliminar'),$F('fechaEliminar'));"></a>
									</td>
									<td id="exportar" style="display:none">
									<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoExcel','','/SGA/images/iconos/exportarex2.jpg',1)">
									<img src="/SGA/images/iconos/exportarex1.jpg" align="absmiddle" alt="Excel" id="icoExcel" style="cursor:hand" onclick="javascript:fc_Generar();"></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table><tr height="1px"><td></td></tr></table>

				<table cellpadding="0" cellspacing="0" border="0" align="center" height="30px" width="940">
					<tr>
						<td>
							<div style="overflow:auto;height:260px;width:100%">
								<table id="tblAsistenciaCurso" style="WIDTH:98%;margin-left:0px; border: 1px solid #048BBA" 
									cellSpacing="1" cellPadding="0" border="0" borderColor="white">
									<tr height="20px">
										<TD class="headtabla" width="10%" align="center">&nbsp;C&oacute;digo</TD>
										<TD class="headtabla" width="50%">&nbsp;Alumno</TD>
										<TD class="headtabla" width="40%" align="center">&nbsp;Asistencia</TD>
									</tr>
									<%
									ArrayList alumno = (ArrayList)request.getAttribute("listaAsistenciaCurso");
									String fondo="";
									if(alumno != null){
										for(int j = 0; j < alumno.size(); j++){
											Asistencia asist = (Asistencia)alumno.get(j);
											if( (j%2) == 0 ) fondo = "fondoceldablanco";
											else fondo = "fondocelesteclaro";
											
											//out.println(">"+asist.getFlagSancion());
											
									%>

									<tr <% if (!"0".equals(asist.getFlagSancion())){%> class="texto_resaltado" title="El Alumno se encuentra sancionado" <%}else {%> class=<%=fondo%> <%}%>>
										<td width="10%" style="text-align:center"><%=asist.getCodAlumno()%></td>
										<td width="50%" style="text-align:left"><%=asist.getNombre()%></td>
										<td width="40%" style="text-align:center">
											<input type="hidden" id="txhCodAsis<%=j%>" name="txhCodAsis<%=j%>" value="<%=asist.getCodAsistencia()%>" />
											<input type="hidden" id="txhCodSecc<%=j%>" name="txhCodSecc<%=j%>" value="<%=asist.getCodSeccion()%>"/>
											<input type="hidden" id="txhSancionAdm<%=j%>" name="txhSancionAdm<%=j%>" value="<%=asist.getFlagSancion()%>"/>
											<input type="radio" id="txhAsis<%=j%>" name="txhRadio<%=j%>" onFocus="setRadio('txhAsis',<%=j%>);" onclick="setRadio('txhAsis',<%=j%>);" onchange="setRadio('txhAsis',<%=j%>);"
											<%if (asist.getTipoAsistencia() == null || asist.getFlagSancion().equals("1")){%><c:out value=" checked=checked disabled"/><%} %>
											<%if (asist.getTipoAsistencia() == ""){%><c:out value=" checked=checked "/><%} %>

<%if(asist.getFlagSancion().equals("0")){%>

											<%if ("0001".equals(asist.getTipoAsistencia())){%>
												<c:out value=" checked=checked"/>
											<%} %>
<%} %>
											value="0001">Asistente&nbsp;&nbsp;
											
											<input type=radio id="txhFalt<%=j%>" name="txhRadio<%=j%>"  onFocus="setRadio('txhFalt',<%=j%>);" onclick="setRadio('txhFalt',<%=j%>);" onchange="setRadio('txhFalt',<%=j%>);"
											<%if (asist.getTipoAsistencia() == null || asist.getFlagSancion().equals("1")){%><c:out value=" disabled"/><%} %>
<%if(!asist.getFlagSancion().equals("0")){%>
	<c:out value=" checked=checked" />
<%}else{ %>
											<%if ("0002".equals(asist.getTipoAsistencia())){%>
												<c:out value=" checked=checked" />
											<%} %>
<%} %>
											value="0002">Faltante&nbsp;&nbsp;
											
											<input type=radio id="txhTard<%=j%>" name="txhRadio<%=j%>"  onFocus="setRadio('txhTard',<%=j%>);" onclick="setRadio('txhTard',<%=j%>);" onchange="setRadio('txhTard',<%=j%>);"
											<%if (asist.getTipoAsistencia() == null || asist.getFlagSancion().equals("1")){%><c:out value=" disabled"/><%} %>

<%if(asist.getFlagSancion().equals("0")){%>
											<%if ("0003".equals(asist.getTipoAsistencia())){%>
												<c:out value=" checked=checked" />
											<%} %>
<%} %>

											value="0003">Tardanza
										</td>
									</tr>
									<%
										}
									}
									%>	
								</table>
							</div>	
						</td>
					</tr>
				</table>

				<table><tr height="4px"><td></td></tr></table>
				<table cellpadding="0" cellspacing="0" border="0" align="center" height="30px" width="98%">	
					<tr>
						<td align="center">										
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
							<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Grabar();" style="cursor:pointer;"></a>
							&nbsp;&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
							<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" onclick="javascript:fc_Atras();" style="cursor:pointer;"></a>
						</td>
					</tr>
				</table>
	</form:form>
	
	<script type="text/javascript" language="javascript">
		
			if (document.getElementById("txhTypeMessage").value != ""){
			strMsg = document.getElementById("txhMessage").value;
				switch(document.getElementById("txhTypeMessage").value){
					case 'ERROR':
						if ( strMsg != ""){
							alert(strMsg);
						}
						break;
					case 'OK':
						if ( strMsg != ""){
							alert(strMsg);
						}
						break;
					case 'REG':
							alert(mstrNoRegistros);
						break;
				}
			}
					
			if($('txhOperacion').value=='BUSCAR_ALUM'){	
				try{
				fc_Agregar();
				}catch (e) {
					alert(e);
				}
				Event.observe(window,'load',function(){ 
					fc_Calendar("txtFecha", "imgCalendarX");
				},false);
				
			}else{
				/*if (! isIE ){						
					var elmTBODY = document.getElementById('THETR');
					elmTR = elmTBODY.insertCell(-1); //Una celda adicional
				}else{
					var objTabla = document.getElementById('tblFechas');
					var container = objTabla.rows[0]; 
					container.appendChild(document.createElement("<td></td>")); //una celda adicional
				}*/
			}
			document.getElementById("txhTypeMessage").value = "";
			document.getElementById("txhMessage").value = "";
			document.getElementById("txhCadena").value = "";
			document.getElementById("txhOperacion").value = "";

			try{
			//document.getElementById("txtFecha"+parseFloat(document.getElementById('txtNroAsistencia').value-1)).focus();
			$('div_fechas').scrollLeft += parseFloat(document.getElementById('txtNroAsistencia').value-1) * 110;
			}catch(e){
				}
			
		
	</script>
	
</body>
