<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){
			var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
			fc_msgManager(mensaje);			
		}
		function fc_msgManager(mensaje){	
			if(mensaje!=""){
				if(mensaje.length>1)      		
					alert(mensaje);				    
			}
		}
		function fc_Regresar(){
			window.location.href = "${ctx}/evaluaciones/bandejaEvaluador.html" +
									"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value +
									"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value;
		}
		function fc_cerrar(){
			if( document.getElementById('cboTipoSesion').value==""){
				alert('Seleccione el tipo de Sesi�n.');
				document.getElementById('cboTipoSesion').focus();
				return false;
			}
			else if(document.getElementById('cboSeccion').value==""){
				alert('Seleccione la Secci�n.');
				document.getElementById('cboSeccion').focus();
				return false;
			}
			
			if (confirm('�Seguro que desea Cerrar Asistencias?')) {
				form = document.forms[0];
				if (form.flagCerrarAsistencia.value=="1"){
					alert('Registro ya se encuentra cerrado.');
				} else {					
					$('txhOperacion').value='CERRAR';
					$('frmMain').submit();
				}
			}	
		}
		function fc_AbrirRegistro(){
			if (confirm('�Seguro de Abrir Registro de Asistencia?')) {
				form = document.forms[0];
				if (form.flagCerrarAsistencia.value=="1"){					
					$('txhOperacion').value='ABRIR_REG';
					$('frmMain').submit();					
				}
			}
		}
		function fc_cambiaSeccion(){			
			$('txhOperacion').value='VER_ESTADO_REGISTRO';
			$('frmMain').submit();
		}
		function fc_Registrar(){
			if (document.getElementById('flagCerrarAsistencia').value=="1"){
				alert('Registro ya se encuentra cerrado.');
				return false;
			}
			if( document.getElementById('cboTipoSesion').value==""){
				alert('Seleccione el tipo de Sesi�n.');
				document.getElementById('cboTipoSesion').focus();
				return false;
			}
			else if(document.getElementById('cboSeccion').value==""){
				alert('Seleccione la Secci�n.');
				document.getElementById('cboSeccion').focus();
				return false;
			}
			window.location.href = "${ctx}/evaluaciones/registrarAsistencia.html" + 
									"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
									"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
									"&txhCodCurso=" + document.getElementById('txhCodCurso').value + 
									"&txhCodProducto=" + document.getElementById('txhCodProducto').value +
									"&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value +
									"&txhTipoSesion=" + document.getElementById('cboTipoSesion').value +
									"&txhCodSeccion=" + document.getElementById('cboSeccion').value + 
									"&txhNomEval=" + document.getElementById('txtEval').value + 
									"&txhSeccion=" + document.getElementById('cboSeccion')[document.getElementById('cboSeccion').selectedIndex].text;
		}
		function fc_Buscar(){
			if( document.getElementById('cboTipoSesion').value==""){
				alert('Seleccione el tipo de Sesi�n.');
				document.getElementById('cboTipoSesion').focus();
				return false;
			}else if(document.getElementById('cboSeccion').value==""){
				alert('Seleccione la Secci�n.');
				document.getElementById('cboSeccion').focus();
				return false;
			}
			$('txhOperacion').value='BUSCAR';
			$('frmMain').submit();
		}
		
		function fc_limpiar(){
			document.getElementById("txtNombre").value = "";
			document.getElementById("txtApellido").value = "";
			document.getElementById("cboSeccion").value = "";
		}
		
		function fc_CambiaTipoSesion(){
			$('txhOperacion').value='SESION';
			$('frmMain').submit(); 
		}
	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/controlAsistencia.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEval" id="txhCodEvaluador"/>
		<form:hidden path="codCurso" id="txhCodCurso"/>
		
		<form:hidden path="codProducto" id="txhCodProducto"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<form:hidden path="flagCerrarAsistencia" id="flagCerrarAsistencia"/>
		<form:hidden path="tipoUsuarioBandeja" id="txhTipoUsuarioBandeja"/>
		<form:hidden path="codUsuarioBandeja" id="txhCodUsuarioBandeja"/>
		
		
		<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" style="margin-left:9px;width:98%">
			<tr>
				<td>
				<table class="borde_tabla" style="width:97%;margin-top:0px" height="110px" cellspacing="0" cellpadding="0" >
					<tr class="fondo_cabecera_azul">		 
					 	<td class="titulo_cabecera" width="100%" colspan="6">
							Control de Asistencia
						</td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td width="20%">&nbsp;Per&iacute;odo Vigente :</td>
						<td colspan=3><form:input path="periodo" id="txtPeriodo" cssStyle="text-align:left" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="" >&nbsp;Ciclo :</td>
						<td><form:input path="ciclo" cssStyle="text-align:left" id="txtCiclo" size="4" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td >&nbsp;Programa :</td>
						<td colspan=3><form:input path="programa" id="txtPrograma" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="" >&nbsp;Especialidad :</td>
						<td ><form:input path="especialidad" id="txtEspecialidad" size="50" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td >&nbsp;Curso :</td>
						<td colspan=3><form:input path="curso" id="txtCurso" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td >&nbsp;Sist. Eval. :</td>
						<td><form:input path="sistEval" id="txtSistEval" size="4" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td class="" >&nbsp;Evaluador Responsable :</td>
						<td colspan=3><form:input path="evaluador" id="txtEval" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="texto_bold" >&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr style="height:0px;">
				<td></td>
			</tr>
			
			<tr>
				<td>
					<table class="borde_tabla" style="width:97%;margin-top:0px" cellspacing="0" cellpadding="0" >

						<tr class="fondo_cabecera_azul">		 
						 	<td class="titulo_cabecera" width="100%" colspan="8">
								Relaci�n de Alumnos
							</td>		 	
						</tr>

						<tr class="fondo_dato_celeste">
							<td >&nbsp;Apell. Paterno :</td>
							<td><form:input path="apellido" id="txtApellido" cssClass="cajatexto" size="30" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Apellido Paterno');"/>&nbsp;</td>
							<td >&nbsp;Nombre :</td>
							<td><form:input path="nombre" id="txtNombre" cssClass="cajatexto" size="25" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Nombre');"/></td>
							<td >&nbsp;Tipo Sesi&oacute;n :</td>
							<td>
							<form:select path="tipoSesion" id="cboTipoSesion" cssClass="cajatexto_o" cssStyle="width:140px" onchange="javascript:fc_CambiaTipoSesion();">
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.listaTipoSesion!=null}">
								<form:options itemValue="codTipoSesion" itemLabel="dscTipoSesion" items="${control.listaTipoSesion}" />
								</c:if>
							</form:select>&nbsp;&nbsp;
							</td>
							<td >&nbsp;Secci&oacute;n :</td>
							<td>
							<form:select path="seccion" id="cboSeccion" cssClass="cajatexto_o" 
								cssStyle="width:100px" onchange="fc_cambiaSeccion()">
								<form:option value="">SEL.</form:option>
								<c:if test="${control.listaSeccion!=null}">
								<form:options itemValue="codSeccion" itemLabel="dscSeccion" items="${control.listaSeccion}" />
								</c:if>
							</form:select>
							&nbsp;&nbsp;&nbsp; 
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg"  align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: hand" onclick="javascript:fc_Buscar();"></a>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: hand" id="imglimpiar" onclick="javascript:fc_limpiar();" ></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table class="" style="width:98%;margin-top:2px" cellspacing="0" cellpadding="0" border="0" bordercolor="red">
			<tr><td align="center">
			 	<span class="mensaje_rojo">
		 			<c:if test="${control.flagCerrarAsistencia=='1'}">
		 				&nbsp;&nbsp;El Registro se encuentra Cerrado
		 			</c:if>
		 		</span>	
			</td>
			</tr>
			<tr>
				<td>
					<div style="OVERFLOW:auto; WIDTH: 99%; HEIGHT: 280px;margin-left:10px; display: ;" id="tblAlumnos">
						<display:table name="sessionScope.listaAlumnos" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.AsistenciaDecorator" 
							requestURI="" style="border: 1px solid #048BBA;width:900px">
							<display:column property="codAlumno" title="C&oacute;digo" style="width:20%;text-align:center"/>
							<display:column property="nombre" title="Alumno" style="text-align:left; width:60%"/>
							<display:column property="nroFaltas" title="Nro. Inasistencia" style="text-align:center;width:10%"/>
							<display:column property="porcentaje" title="% Inasistencia" style="text-align:center;width:10%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFound' class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
						</display:table>
					</div>
				</td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="green" align="center" height="30px" width="98%" style="margin-top:8px">	
			<tr>
				<td align="center">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregistrar','','${ctx}/images/botones/regasistencia2.jpg',1)">
					<img alt="Registrar Asistencia" src="${ctx}/images/botones/regasistencia1.jpg" id="imgregistrar" onclick="javascript:fc_Registrar();" style="cursor:pointer;"></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar" onclick="javascript:fc_Regresar();" style="cursor:pointer;"></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcerrar','','${ctx}/images/botones/cerrar_reg2.png',1)">
						<img alt="Cerrar Registros" src="${ctx}/images/botones/cerrar_reg1.png" id="imgcerrar" onclick="fc_cerrar()" style="cursor:pointer">
					</a>
					&nbsp;
					<c:if test="${control.flagCerrarAsistencia=='1' && (control.tipoUsuarioBandeja=='1' || control.tipoUsuarioBandeja=='2')}">
						<!-- Abrir Registro de Notas -->
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgabrir','','${ctx}/images/botones/abrir_reg2.png',1)">
							<img alt="Abrir Registros" src="${ctx}/images/botones/abrir_reg1.png" id="imgabrir" onclick="fc_AbrirRegistro()" style="cursor:pointer">
						</a>						
					</c:if>	

				</td>
			</tr>
		</table>
	</form:form>
	<script language="javascript">
		document.getElementById('txhOperacion').value = "";
	</script>
</body>