<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<%@page import='com.tecsup.SGA.modelo.ConsultaEmpresa' %>
<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<script src="${ctx}/scripts/js/funciones_eva-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var bandera = "";	
	bandera = "<%=((request.getAttribute("Bandera")==null)?"":(String)request.getAttribute("Bandera"))%>";	
	function onLoad(){                                            		
		objflag = document.getElementById("txhFlag").value;
	
		fc_Aparece(objflag);
		
		objMsg = document.getElementById("txhMsg");
		objMen = document.getElementById("txhMen");
		objMens = document.getElementById("txhMens");
		
		if ( objMsg.value == "OK" )
		{
		
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrSeGraboConExito);
		
		}
		else if ( objMsg.value == "ERROR" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrProblemaGrabar);			
		}
		
		if(objMen.value =="NO"){
			objMen.value = "";
			document.getElementById("txhMen").value = "";
			alert('Horas de pr�ctica completas');
		}
						
		if ( objMens.value == "OK" )
		{
			objMens.value = "";
			document.getElementById("txhMens").value = "";
			alert('Se actualiz� exitosamente.');		
		}
		else if ( objMens.value == "ERROR" )
		{
			objMens.value = "";
			document.getElementById("txhMens").value = "";
			alert('Problemas al actualizar');			
		}
		objMsg = "";
		objMen = "";
		objMens.value = "";
		 document.getElementById("txhMsg").value = "";
		 document.getElementById("txhMen").value = "";
		 document.getElementById("txhMens").value = "";	

		document.getElementById("txhEmpresa").value = "";
		document.getElementById("txhFechaIni").value = "";
		document.getElementById("txhFechaFin").value = "";
	 	document.getElementById("txhEmpresa1").value = "";
	 	document.getElementById("txhCant").value = "";
	 	document.getElementById("txhEmpresa2").value = "";
	 	document.getElementById("txhCant1").value = "";
	 	document.getElementById("txhFechaInicio1").value = "";
		document.getElementById("txhFechaFin1").value = "";
		document.getElementById("txhOperacion").value = "";
		document.getElementById("txhNota").value = "";
		document.getElementById("txhHoraIni").value = "";
		document.getElementById("txhHoraFin").value = "";
		document.getElementById("txhCHoras").value = "";
		document.getElementById("txhObservacion").value = "";
	}
	
	function fc_Regresar() {
		window.location.href = ("${ctx}/evaluaciones/eva_formacion_empresa.html" +
							"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
							"&txhCodEvaluador=" + document.getElementById('txhCodEva').value);	
	}

	function fc_levanta(){
		strcodAlumno = document.getElementById("txhcodAlumno").value;
		strcodPeriodo = document.getElementById("txhCodPeriodo").value;
		strCodEva = document.getElementById("txhCodEva").value;
		Fc_Popup("${ctx}/evaluaciones/eva_adjuntar_archivo.html?txhcodAlumno=" + strcodAlumno + "&txhCodPeriodo=" + strcodPeriodo + "&txhCodEva=" + strCodEva 
		,430,130);
	}

	function fc_Grabar(){
		fechaIni = document.getElementById("txhFechaIni").value;
		fechaFin = document.getElementById("txhFechaFin").value; 
		fechaIni1 = document.getElementById("txhFechaInicio1").value;
		fechaFin1 = document.getElementById("txhFechaFin1").value;
		if(document.getElementById("txhTipo").value == "0"){
		emp = document.getElementById("txhEmpresa").value;
		if(fc_Trim(emp)){
		if (fc_ValidaFechaIniFechaFin(fechaIni,fechaFin)==0){
			if(document.getElementById("txhFechaIni").value !==""){
				if(document.getElementById("txhFechaFin").value !==""){
				document.getElementById("txhOperacion").value = "GRABAR";
				document.getElementById("frmMain").submit();
				 } else{
				 alert('Debe ingresar una fecha de fin');
				 return false;
				 }
				}
				else{
				alert('Debe ingresar una fecha inicial');
				return false;				
				}
			}
			else{
			 	alert('La fecha de inicio debe ser menor a la fecha final.');
			 	}
			}
		else{
		alert('Debe ingresar una empresa');
		return false;
		}
	 }
	
	
	if(document.getElementById("txhTipo").value == "1"){
	emp = document.getElementById("txhEmpresa1").value;
	if(fc_Trim(emp)){
		if(document.getElementById("txhCant").value !==""){
		if(document.getElementById("txhEstado").value !==""){
		document.getElementById("txhOperacion").value = "GRABAR";
		document.getElementById("frmMain").submit();
			}
			else{
			alert('Debe seleccionar un estado');
			return false;
			}
		}
		else{
		alert('Debe ingresar la cantidad de horas');
		return false;
		}
		}
		else{
		alert('Debe ingresar una empresa');
		return false;
		}
	}
	
	if(document.getElementById("txhTipo").value == "2"){
	emp = document.getElementById("txhEmpresa2").value;
	if(fc_Trim(emp)){
	if (fc_ValidaFechaIniFechaFin(fechaIni1,fechaFin1)==0){
		if(document.getElementById("txhEmpresa2").value !==""){
			if(document.getElementById("txhCant1").value !==""){
			if(document.getElementById("txhFechaInicio1").value !==""){
				if(document.getElementById("txhFechaFin1").value !==""){
				document.getElementById("txhOperacion").value = "GRABAR";
				document.getElementById("frmMain").submit();
				 }
				 else{
				 alert('Debe ingresar una fecha de fin');
				 return false;
				 }
				}
				else{
				alert('Debe ingresar una fecha inicial');
				return false;
				}
			 }	
			else{
			alert('Debe ingresar la cantidad de horas');
			return false;
				}
			}
		else{
			alert('Debe ingresar una empresa');
			return false;
			}
			}
			else{
				alert('La fecha de inicio debe ser menor a la fecha final.');
				}
			}
			else{
			alert('Debe ingresar una empresa');
			return false;
			}
		}
		document.getElementById("txhOperacion").value = "";
		document.getElementById("txhEmpresa").value = "";
		emp = "";
	}

	function fc_Aparece(param){	
		
		switch(param){
		case '1':
			document.getElementById("txhTipo").value = "0";
				var objLong = bandera;
				if(objLong!="OK")
				{	document.getElementById("txhOperacion").value = "ACTUALIZA";
					document.getElementById("frmMain").submit();
				}
			document.getElementById("txhEmpresa").value = "";
			document.getElementById("txhFechaIni").value = "";
			document.getElementById("txhFechaFin").value = "";
			
		 	document.getElementById("txhEmpresa1").value = "";
		 	document.getElementById("txhCant").value = "";
		 	
	 		document.getElementById("txhEmpresa2").value = "";
		 	document.getElementById("txhCant1").value = "";
		 			 	
		 	document.getElementById("txhFechaInicio1").value = "";
			document.getElementById("txhFechaFin1").value = "";
		
			if(document.getElementById("txhTotemp").value < 4){
				tbl_Grabar.style.display="";
				tbl_PracticaInicial.style.display="";
			}
			if(document.getElementById("txtEmp1").value !== ""){
				tbl_PracticaInicial1.style.display="";
				tbl_Grabar.style.display="";
			}
			if(document.getElementById("txtEmp2").value !== ""){				
				tbl_PracticaInicial2.style.display="";
				tbl_Grabar.style.display="";
			}
			if(document.getElementById("txtEmp3").value !== ""){
				tbl_PracticaInicial3.style.display="";
				tbl_Grabar.style.display="";
			}
			if(document.getElementById("txtEmp4").value !== ""){
				tbl_PracticaInicial4.style.display="";
				tbl_Grabar.style.display="none";
			}
			
			tbl_Pasantia.style.display="none";
			tbl_Mod.style.display="none";
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			divPractica.style.display="none";
			tbl_Detalle2.style.display="none";
			tbl_Mo_di.style.display="none";
			
			PracticaInicial.className="Tab_off";
			Pasantia.className="Tab_on";
			PracticaPre.className="Tab_on";
			
			break;
		case '2':
			document.getElementById("txhTipo").value = "1";
			var objLong = bandera;
		
			if(objLong!="OK")
			{	document.getElementById("txhOperacion").value = "ACTUALIZA";
				document.getElementById("frmMain").submit();
			}
			
			document.getElementById("txhEmpresa").value = "";
			document.getElementById("txhFechaIni").value = "";
			document.getElementById("txhFechaFin").value = "";
		 	document.getElementById("txhEmpresa1").value = "";
		 	document.getElementById("txhCant").value = "";
		 	document.getElementById("txhEmpresa2").value = "";
		 	document.getElementById("txhCant1").value = "";
		 	document.getElementById("txhFechaInicio1").value = "";
			document.getElementById("txhFechaFin1").value = "";
			
			tbl_Grabar.style.display="";
			tbl_Modifica.style.display="none";
			tbl_PracticaInicial.style.display="none";
			tbl_PracticaInicial1.style.display="none";
			tbl_PracticaInicial2.style.display="none";
			tbl_PracticaInicial3.style.display="none";
			tbl_Pasantia.style.display="";
			tbl_Mod.style.display="none";
			divPasantia.style.display ="";
			tbl_Detalle1.style.display="";
			tbl_PracticaPre.style.display="none";
			divPractica.style.display="none";
			tbl_Detalle2.style.display="none";
			tbl_Mo_di.style.display="none";
			PracticaInicial.className="Tab_on";
			Pasantia.className="Tab_off";
			PracticaPre.className="Tab_on";
			break;
		case '3':
			document.getElementById("txhTipo").value = "2";
			var objLong = bandera;
			
			if(objLong!="OK")
			{	document.getElementById("txhOperacion").value = "ACTUALIZA";
				document.getElementById("frmMain").submit();
			}
			
			document.getElementById("txhEmpresa").value = "";
			document.getElementById("txhFechaIni").value = "";
			document.getElementById("txhFechaFin").value = "";
		 	document.getElementById("txhEmpresa1").value = "";
		 	document.getElementById("txhCant").value = "";
			document.getElementById("txhEmpresa2").value = "";
		 	document.getElementById("txhCant1").value = "";
		 	document.getElementById("txhFechaInicio1").value = "";
			document.getElementById("txhFechaFin1").value = "";
			
			tbl_Modifica.style.display="none";
			tbl_Grabar.style.display="";
			tbl_PracticaInicial.style.display="none";
			tbl_PracticaInicial1.style.display="none";
			tbl_PracticaInicial2.style.display="none";
			tbl_PracticaInicial3.style.display="none";
			tbl_Pasantia.style.display="none";
			tbl_Mod.style.display="none";
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="";
			divPractica.style.display="";
			tbl_Detalle2.style.display="";
			tbl_Mo_di.style.display="none";
			PracticaInicial.className="Tab_on";
			Pasantia.className="Tab_on";
			PracticaPre.className="Tab_off";
		
			break;
	}	
	bandera = "";
}
	function fc_Etapa(param){
	switch(param){
		case '1':
			document.getElementById("txhEstado").value = "0001";
			break;
		case '2':
			document.getElementById("txhEstado").value = "0002";
			break;
		}
	}

	function fc_Modifica(){
	fechaIni = document.getElementById("txhFechaIni").value;
	fechaFin = document.getElementById("txhFechaFin").value; 
		document.getElementById("txhCantHoras").value = document.getElementById("txhCHoras").value;
			document.getElementById("txhObser").value = document.getElementById("txhObservacion").value;
	
	emp = document.getElementById("txhEmpresa").value;
	if(fc_Trim(emp)){
	if (fc_ValidaFechaIniFechaFin(fechaIni,fechaFin)==0){
		if(document.getElementById("txhFechaIni").value !==""){
				if(document.getElementById("txhFechaFin").value !==""){
				document.getElementById("txhOperacion").value = "MODIFICA";
				document.getElementById("frmMain").submit();
				 }
				 else{
				 alert('Debe ingresar una fecha de fin');
				 return false;
				 }
				}
				else{
				alert('Debe ingresar una fecha inicial');
				return false;				
				}
				}
			else{
			 	alert('La fecha de inicio debe ser menor a la fecha final.');
			 	}
			}
		else{
		alert('Debe ingresar una empresa');
		return false;
		}
		document.getElementById("txhEmpresa").value = "";
	}	

	function fc_Modifica1(){ 
	        document.getElementById("txhCodId").value = document.getElementById("txhCodId1").value;
			document.getElementById("txhEmpresa").value = document.getElementById("txtEmp1").value;
			document.getElementById("txhFechaIni").value = document.getElementById("txtFini1").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txtFfin1").value;
			document.getElementById("txhCHoras").value = document.getElementById("txtCa1").value;
			document.getElementById("txhObservacion").value = document.getElementById("txtOb1").value;			 			
			document.getElementById("txhVez").value = "2";
	
			tbl_Modifica.style.display="";
			tbl_PracticaInicial.style.display="";
			tbl_PracticaInicial1.style.display="none";
			if(document.getElementById("txtEmp2").value !== ""){
				tbl_PracticaInicial2.style.display="";
			}
			if(document.getElementById("txtEmp3").value !== ""){
				tbl_PracticaInicial3.style.display="";
			}
			if(document.getElementById("txtEmp4").value !== ""){
				tbl_PracticaInicial4.style.display="";
			}
			tbl_Pasantia.style.display="none";			
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			tbl_Detalle2.style.display="none";
			divPractica.style.display="none";
			tbl_Mo_di.style.display="none";
			tbl_Grabar.style.display="none";
		}
		function fc_Modifica2(){ 
			document.getElementById("txhCodId").value = document.getElementById("txhCodId2").value;
			document.getElementById("txhEmpresa").value = document.getElementById("txtEmp2").value;
			document.getElementById("txhFechaIni").value = document.getElementById("txtFini2").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txtFfin2").value;
			document.getElementById("txhCHoras").value = document.getElementById("txtCa2").value;
			document.getElementById("txhObservacion").value = document.getElementById("txtOb2").value;
			document.getElementById("txhVez").value = "2";
	
			tbl_Modifica.style.display="";
			tbl_PracticaInicial.style.display="";
			if(document.getElementById("txtEmp1").value !== ""){
				tbl_PracticaInicial1.style.display="";
			}
			tbl_PracticaInicial2.style.display="none";
			if(document.getElementById("txtEmp3").value !== ""){
				tbl_PracticaInicial3.style.display="";
			}
			if(document.getElementById("txtEmp4").value !== ""){
				tbl_PracticaInicial4.style.display="";
			}
			tbl_Pasantia.style.display="none";
			
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			tbl_Detalle2.style.display="none";
			divPractica.style.display="none";
			tbl_Mo_di.style.display="none";
			tbl_Grabar.style.display="none";
		}
		function fc_Modifica3(){ 
			document.getElementById("txhCodId").value = document.getElementById("txhCodId3").value;
			document.getElementById("txhEmpresa").value = document.getElementById("txtEmp3").value;
			document.getElementById("txhFechaIni").value = document.getElementById("txtFini3").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txtFfin3").value;
			document.getElementById("txhCHoras").value = document.getElementById("txtCa3").value;
			document.getElementById("txhObservacion").value = document.getElementById("txtOb3").value;
			document.getElementById("txhVez").value = "2";
			tbl_Modifica.style.display="";
			tbl_PracticaInicial.style.display="";
			if(document.getElementById("txtEmp1").value !== ""){
				tbl_PracticaInicial1.style.display="";
			}
			if(document.getElementById("txtEmp2").value !== ""){
				tbl_PracticaInicial2.style.display="";
			}
			if(document.getElementById("txtEmp4").value !== ""){
				tbl_PracticaInicial4.style.display="";
			}
			tbl_PracticaInicial3.style.display="none";
			tbl_Pasantia.style.display="none";
			
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			tbl_Detalle2.style.display="none";
			divPractica.style.display="none";
			tbl_Mo_di.style.display="none";
			tbl_Grabar.style.display="none";
		}

		function fc_Modifica4(){ 
			document.getElementById("txhCodId").value = document.getElementById("txhCodId4").value;
			document.getElementById("txhEmpresa").value = document.getElementById("txtEmp4").value;
			document.getElementById("txhFechaIni").value = document.getElementById("txtFini4").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txtFfin4").value;
			document.getElementById("txhCHoras").value = document.getElementById("txtCa4").value;
			document.getElementById("txhObservacion").value = document.getElementById("txtOb4").value;
			document.getElementById("txhVez").value = "2";
			tbl_Modifica.style.display="";
			tbl_PracticaInicial.style.display="";
			if(document.getElementById("txtEmp1").value !== ""){
				tbl_PracticaInicial1.style.display="";
			}
			if(document.getElementById("txtEmp2").value !== ""){
				tbl_PracticaInicial2.style.display="";
			}
			if(document.getElementById("txtEmp3").value !== ""){
				tbl_PracticaInicial3.style.display="";
			}
			tbl_PracticaInicial4.style.display="none";
			tbl_Pasantia.style.display="none";			
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			tbl_Detalle2.style.display="none";
			divPractica.style.display="none";
			tbl_Mo_di.style.display="none";
			tbl_Grabar.style.display="none";
		}
		
		function fc_Modi(param1){
			tbl_Grabar.style.display="none";
			tbl_Mod.style.display="";
			document.getElementById("txhCodId").value = document.getElementById("cod"+param1).value;
			document.getElementById("txhEmpresa1").value = document.getElementById("name"+param1).value;
			document.getElementById("txhCant").value = document.getElementById("ca"+param1).value;
			document.getElementById("txhVez").value = "2";
			document.getElementById("tbl"+param1).style.display="none";
			if(document.getElementById("ra1"+param1).checked ==true){
				document.getElementById("rdoEtapa").checked = true;
				document.getElementById("txhEstado").value = "0001";
			}
			if(document.getElementById("ra2"+param1).checked ==true){
				document.getElementById("rdoEtapa1").checked = true;
				document.getElementById("txhEstado").value = "0002";
			}
		}
		
		function fc_Mod(){
			document.getElementById("txhEmpresa").value = document.getElementById("txhEmpresa1").value;
			document.getElementById("txhCantHoras").value = document.getElementById("txhCant").value;
			
			emp = document.getElementById("txhEmpresa").value;
			if(fc_Trim(emp)){
				if(document.getElementById("txhCant").value !==""){
					if(document.getElementById("txhEstado").value !==""){
						document.getElementById("txhOperacion").value = "MODIFICA";
						document.getElementById("frmMain").submit();
					}else{
						alert('Debe seleccionar un estado');
						return false;
					}
				}else{
					alert('Debe ingresar la cantidad de horas');
					return false;
				}
			}else{
				alert('Debe ingresar una empresa');
				return false;
			}
		}
		
		
		function fc_Mo(param1){
		tbl_Mod.style.display="none";
		tbl_Grabar.style.display="none";
		tbl_Mo_di.style.display="";
		document.getElementById("txhCodId").value = document.getElementById("co"+param1).value;
		document.getElementById("txhEmpresa2").value = document.getElementById("na"+param1).value;
		document.getElementById("txhFechaInicio1").value = document.getElementById("peini"+param1).value;
		document.getElementById("txhFechaFin1").value = document.getElementById("pefin"+param1).value;
		document.getElementById("txhCant1").value = document.getElementById("c"+param1).value;
		
		document.getElementById("txhHoraIni").value = document.getElementById("horaIni"+param1).value;
		document.getElementById("txhHoraFin").value = document.getElementById("horaFin"+param1).value;
		document.getElementById("txhNota").value = document.getElementById("nota"+param1).value;
		
		if(document.getElementById("sab"+param1).checked ==true){
		document.getElementById("rdoSab").checked = true;
		//document.getElementById("txhIndSabado").value = "0001"; --JHPR 2008-06-24 Comentado.
		document.getElementById("txhIndSabado").value = "1";
		}
		if(document.getElementById("dom"+param1).checked ==true){
		document.getElementById("rdoDom").checked = true;
		//document.getElementById("txhIndDomingo").value = "0001"; --JHPR 2008-06-24 Comentado.
		document.getElementById("txhIndDomingo").value = "1";
		}
		
		document.getElementById("tb"+param1).style.display="none";
		document.getElementById("txhVez").value = "2";
		}
		function fc_Mo_di(){
	
		document.getElementById("txhFechaIni").value = document.getElementById("txhFechaInicio1").value;
		document.getElementById("txhFechaFin").value = document.getElementById("txhFechaFin1").value;
		
		fechaIni = document.getElementById("txhFechaIni").value;
		fechaFin = document.getElementById("txhFechaFin").value; 
		
		document.getElementById("txhEmpresa").value = document.getElementById("txhEmpresa2").value;
		document.getElementById("txhCantHoras").value = document.getElementById("txhCant1").value;
		       
		document.getElementById("txhNot").value = document.getElementById("txhNota").value;
		document.getElementById("txhHoraInicio").value = document.getElementById("txhHoraIni").value;
		document.getElementById("txhHorafinaliza").value = document.getElementById("txhHoraFin").value;
		document.getElementById("txhSelSabado").value = document.getElementById("txhIndSabado").value;
		document.getElementById("txhSelDomingo").value = document.getElementById("txhIndDomingo").value;
		
		emp = document.getElementById("txhEmpresa").value;
	if(fc_Trim(emp)){
	if (fc_ValidaFechaIniFechaFin(fechaIni,fechaFin)==0){
		if(document.getElementById("txhFechaIni").value !==""){
				if(document.getElementById("txhFechaFin").value !==""){
				document.getElementById("txhOperacion").value = "MODIFICA";
				document.getElementById("frmMain").submit();
				 }
				 else{
				 alert('Debe ingresar una fecha de fin');
				 return false;
				 }
				}
				else{
				alert('Debe ingresar una fecha inicial');
				return false;				
				}
				}
			else{
			 	alert('La fecha de inicio debe ser menor a la fecha final.');
			 	}
			}
		else{
		alert('Debe ingresar una empresa');
		return false;
		}
		document.getElementById("txhEmpresa").value = "";
		
		}
		function Fc_VER(){
		strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>";
		if ( strRuta != "" )
		{
			if ( fc_Trim(document.getElementById("txhVer").value) != ""){
				//alert(">>"+strRuta + document.getElementById("txhVer").value+"<<");
				window.open(strRuta + document.getElementById("txhVer").value);				
			}
			else alert('No cuenta con documentos');
		}
		}
		
		function fc_Inc1(){
		if(document.getElementById("txhIndDomingo").value ==""){
		document.getElementById("txhIndDomingo").value = "1";
		}	
		else{
		document.getElementById("txhIndDomingo").value = "";
		} 
		}
		function fc_Inc(){
		if(document.getElementById("txhIndSabado").value ==""){
		document.getElementById("txhIndSabado").value = "1";
		}	
		else{
		document.getElementById("txhIndSabado").value = "";
		}
		
		}
	</script>
	</head>

   	<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_registrar_empresa.html">
	<form:hidden path="tipo" id="txhTipo"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="valida" id="txhValida"/>
	<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
	<form:hidden path="codAlumno" id="txhcodAlumno"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="men" id="txhMen"/>
	<form:hidden path="codFlagApto" id="txhFlagApto"/>
	<form:hidden path="codFlagNoApto" id="txhFlagNoApto"/>
	<form:hidden path="estado" id="txhEstado"/>
	<form:hidden path="totemp" id="txhTotemp"/>
	<form:hidden path="flag" id="txhFlag"/>
	<form:hidden path="mens" id="txhMens"/>
	<form:hidden path="codId" id="txhCodId"/>
	<form:hidden path="codId1" id="txhCodId1"/>
	<form:hidden path="codId2" id="txhCodId2"/>
	<form:hidden path="codId3" id="txhCodId3"/>
	<form:hidden path="codId4" id="txhCodId4"/>
	<form:hidden path="inf" id="txhInf"/>
	<form:hidden path="ver" id="txhVer"/>
	<form:hidden path="vez" id="txhVez"/>
	<form:hidden path="sab" id="txhSab"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEva" id="txhCodEva"/>
	<form:hidden path="cantHoras" id="txhCantHoras"/>
	<form:hidden path="indDomingo" id="txhIndDomingo"/>
	<form:hidden path="indSabado" id="txhIndSabado"/>
	 
	<form:hidden path="obser" id="txhObser"/>
	<form:hidden path="not" id="txhNot"/>
	<form:hidden path="horaInicio" id="txhHoraInicio"/>
	<form:hidden path="horafinaliza" id="txhHorafinaliza"/>
	<form:hidden path="selSabado" id="txhSelSabado"/>
	<form:hidden path="selDomingo" id="txhSelDomingo"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="745px" class="opc_combo"><font style="">Formaci�n Empresa</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
		<table class="tabla" style="width:97%;margin-top:6px; margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
		cellspacing="2" cellpadding="0" >
			<tr>
				<td width="14%">&nbsp;Alumno :&nbsp;</td>
				<td width="53%"> 
					<form:input path="cadena" id="txtCadena" cssClass="cajatexto_1" cssStyle="HEIGHT: 14px;width:250px" readonly="true" />
				</td>
				<td width="33%">Informe:&nbsp;
					<a onclick="javascript:Fc_VER()" href="#" >
					<b><c:out value="${control.inf}"/></b></a>
					<img src="${ctx}/images/iconos/adjuntar_on.gif" style="cursor:hand" onclick="javascript:fc_levanta()">
				</td>
			</tr>
		</table>
		<br>
		
		<table cellpadding="0" cellspacing="0" width="97%" border ="0" style="margin-left:9px">
		<tr>
			<td class="Tab_off" width="33%" onclick="javascript:fc_Aparece('1','PracticaInicial');" id="PracticaInicial">&nbsp;<b>Pr�ctica Inicial<b></td>
			<td class="Tab_off" width="33%" onclick="javascript:fc_Aparece('2','Pasantia');" id="Pasantia">&nbsp;<b>Pasant�a<b></td>
			<td class="Tab_off" width="33%" onclick="javascript:fc_Aparece('3','PracticaPre');" id="PracticaPre">&nbsp;<b>Pr�ctica Preprofesional<b></td>
		</tr>
		</table>
		<table class="tablaflotante" border="0" bordercolor="red" style="display:none;width:97%; margin-left:9px" 
			id="tbl_PracticaInicial" name="tbl_PracticaInicial" cellpadding="2" cellspacing="2" >
		<tr>
			<td class="">&nbsp;Empresa 
			</td>
		<tr>
		<tr>
			<td class="" width="14%">&nbsp;Nombre Empresa :&nbsp;&nbsp;</td>
			<td>
			<form:input path="empresa" id="txhEmpresa"
					onkeypress="fc_ValidaTextoNumeroEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur('txhEmpresa','empresa');" 
					cssClass="cajatexto" size="50" />
			</td>
			<td>&nbsp;</td>
			<td class="" align="right">&nbsp;Per�odo :&nbsp;&nbsp;</td>
			<td align="left" >
			<form:input path="fechaInicio" id="txhFechaIni"
					onkeypress="fc_ValidaFecha('txhFechaIni');"
					onblur="fc_ValidaFechaOnblur('txhFechaIni');" 
					cssClass="cajatexto" size="10" />
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" style='cursor:hand;' align="absmiddle"></a> - 
			<form:input path="fechaFin" id="txhFechaFin"
					onkeypress="fc_ValidaFecha('txhFechaFin');"
					onblur="fc_ValidaFechaOnblur('txhFechaFin');" 
					cssClass="cajatexto" size="10" />
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecFin" alt="Calendario" style='cursor:hand;' align="absmiddle"></a> -
					&nbsp;&nbsp;(DD-MM-AAAA)
			</td>
		</tr>
		<tr>
		<td class="" width="14%">&nbsp;Cant. Horas:&nbsp;&nbsp;</td>
		<td>
			<form:input path="caHoras" id="txhCHoras"  
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur('txhCHoras');" 
					cssClass="cajatexto" size="10" /> 
		</td>
		<td>&nbsp;</td>
		<td align="right">&nbsp;Observaciones:&nbsp;&nbsp;</td>
		<td>
			<form:input path="observacion" id="txhObservacion"					
					onkeypress="fc_ValidaTextoNumeroEspecial();"	
					onblur="fc_ValidaNumeroLetrasGuionOnblur('txhObservacion','observaciones');" 					 
					cssClass="cajatexto" size="40" />
		</td>
		
		</tr>
		</table>
		<table id="tbl_Modifica" name="tbl_Modifica" style="display:none;height:30px;width:97%; margin-left: 9px" 
			cellpadding="0" cellspacing="0" height="30px" >	
			<br>
			<tr>
				<td align="center">															
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModif','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Modificar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Modifica();" style="cursor:pointer;"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgretrocede','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgretrocede" onclick="fc_Regresar();" style="cursor:pointer;"></a>													
				</td>
			</tr>
		</table>
		
		<table class="tablaflotante" border="0" 
			style="display:none;height:30px;width:97%; margin-left:9px;" id="tbl_PracticaInicial1" name="tbl_PracticaInicial1" cellpadding="2" cellspacing="2" >
			<tr>
				<td colspan="5">&nbsp;Empresa 1</td>				
			</tr>
			<tr>
				<td width="14%">&nbsp;Nombre Empresa:&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="emp1" id="txtEmp1" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:273px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td >&nbsp;Per�odo:&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="fini1" id="txtFini1" cssClass="cajatexto" cssStyle="HEIGHT: 12px;width:58px" readonly="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<form:input path="ffin1" id="txtFfin1" cssClass="cajatexto" cssStyle="HEIGHT: 12px;width:58px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td width="14%">&nbsp;Cant. Horas:&nbsp;&nbsp;</td>
				<td> 
					<form:input path="ca1" id="txtCa1" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:73px" readonly="true" />
				</td>
				<td width="13%">&nbsp;Observaciones:</td>
				<td>
					<form:input path="ob1" id="txtOb1" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:225px" readonly="true" />
				</td>			
				<td align="right">															
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificar','','${ctx}/images/iconos/modificar2.jpg',1)">
						<img alt="Modificar" src= "${ctx}/images/iconos/modificar1.jpg" onclick="javascript:fc_Modifica1();" style="cursor:hand" id="imgModificar">&nbsp;
					</a>
				</td>
		</tr>
		</table>
		
		<!-- table><tr height="5px"><td></td></tr></table> -->
		<table class="tablaflotante" border="0" 
			style="display:none;height:30px;width:97%; margin-left:9px;" id="tbl_PracticaInicial2" name="tbl_PracticaInicial2" cellpadding="2" cellspacing="2" >
			<tr>
				<td colspan="5">&nbsp;Empresa 2</td>			
			</tr>
			<tr>
				<td width="14%">&nbsp;Nombre Empresa :&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="emp2" id="txtEmp2" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:273px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td >&nbsp;Per�odo :&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="fini2" id="txtFini2" cssClass="cajatexto" cssStyle="HEIGHT: 13px;width:58px" readonly="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<form:input path="ffin2" id="txtFfin2" cssClass="cajatexto" cssStyle="HEIGHT: 13px;width:58px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td width="14%">&nbsp;Cant. Horas:&nbsp;&nbsp;</td>
				<td> 
					<form:input path="ca2" id="txtCa2" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:73px" readonly="true" />
				</td>
				<td width="13%">&nbsp;Observaciones:</td>
				<td>
					<form:input path="ob2" id="txtOb2" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:225px" readonly="true" />
				</td>
				<td align="right">															
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificar1','','${ctx}/images/iconos/modificar2.jpg',1)">
						<img alt="Modificar" src= "${ctx}/images/iconos/modificar1.jpg" onclick="javascript:fc_Modifica2();" style="cursor:hand" id="imgModificar1">&nbsp;
					</a>
				</td>
			</tr>
		</table>
		
		<table class="tablaflotante" border="0" style="display:none;height:30px;width:97%; margin-left:9px;" id="tbl_PracticaInicial3" name="tbl_PracticaInicial3" cellpadding="2" cellspacing="2" >
			<tr>
				<td colspan="5">&nbsp;Empresa 3
				</td>
			</tr>
			<tr>
				<td width="14%">&nbsp;Nombre Empresa :&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="emp3" id="txtEmp3" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:273px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td >&nbsp;Per�odo :&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="fini3" id="txtFini3" cssClass="cajatexto" cssStyle="HEIGHT: 13px;width:58px" readonly="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<form:input path="ffin3" id="txtFfin3" cssClass="cajatexto" cssStyle="HEIGHT: 13px;width:58px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td width="14%">&nbsp;Cant. Horas:&nbsp;&nbsp;</td>
				<td> 
					<form:input path="ca3" id="txtCa3" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:73px" readonly="true" />
				</td>
				<td width="13%">&nbsp;Observaciones:</td>
				<td>
					<form:input path="ob3" id="txtOb3" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:225px" readonly="true" />
				</td>
				<td align="right">	
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModi1','','${ctx}/images/iconos/modificar2.jpg',1)">
						<img alt="Modificar" src= "${ctx}/images/iconos/modificar1.jpg" onclick="javascript:fc_Modifica3();" style="cursor:hand" id="imgModi1">&nbsp;
					</a>
				</td>
			</tr>
		</table>
		
		<table class="tablaflotante" border="0" style="display:none;height:30px;width:97%; margin-left:9px;" id="tbl_PracticaInicial4" name="tbl_PracticaInicial4" cellpadding="2" cellspacing="2" >
			<tr>
				<td colspan="5">&nbsp;Empresa 4
				</td>
			</tr>
			<tr>
				<td width="14%">&nbsp;Nombre Empresa :&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="emp4" id="txtEmp4" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:273px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td >&nbsp;Per�odo :&nbsp;&nbsp;</td>
				<td colspan="4">
					<form:input path="fini4" id="txtFini4" cssClass="cajatexto" cssStyle="HEIGHT: 13px;width:58px" readonly="true" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<form:input path="ffin4" id="txtFfin4" cssClass="cajatexto" cssStyle="HEIGHT: 13px;width:58px" readonly="true" />
				</td>
			</tr>
			<tr>
				<td width="14%">&nbsp;Cant. Horas:&nbsp;&nbsp;</td>
				<td> 
					<form:input path="ca4" id="txtCa4" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:73px" readonly="true" />
				</td>
				<td width="13%">&nbsp;Observaciones:</td>
				<td>
					<form:input path="ob4" id="txtOb4" cssClass="cajatexto" cssStyle="HEIGHT: 14px;width:225px" readonly="true" />
				</td>
				<td align="right">	
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModi1','','${ctx}/images/iconos/modificar2.jpg',1)">
						<img alt="Modificar" src= "${ctx}/images/iconos/modificar1.jpg" onclick="javascript:fc_Modifica4();" style="cursor:hand" id="imgModi1">&nbsp;
					</a>
				</td>
			</tr>
		</table>
		
		
		
		<table class="tablaflotante" border="0" style="display:none;width:97%; margin-left: 9px" 
			id="tbl_Pasantia" name="tbl_Pasantia" cellpadding="2" cellspacing="2" >
		<tr>
			<td class="" width="14%">&nbsp;Nombre Empresa :&nbsp;&nbsp;</td>
			<td>
			<form:input path="empresa1" id="txhEmpresa1"
					onkeypress="fc_ValidaTextoNumeroEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur('txhEmpresa1','empresa');" 					
					cssClass="cajatexto" size="50" />
			</td>
		</tr>
		<tr>
			<td class="" width="14%">&nbsp;Cant. Horas :&nbsp;&nbsp;</td>
			<td>
			<form:input path="cant" id="txhCant"
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur('txhCant');" 
					cssClass="cajatexto" size="10" maxlength="3"/>
			</td>
			<td class="" width="14%">&nbsp;Estado :&nbsp;&nbsp;</td>
			<td>
			  <input Type="radio" id="rdoEtapa" name="rdoEtapa" name="gbradio" onclick="javascript:fc_Etapa('1');" value="${status.value}">Apto&nbsp;&nbsp;&nbsp;&nbsp;
			  <input Type="radio" id="rdoEtapa1" name="rdoEtapa" name="gbradio" onclick="javascript:fc_Etapa('2');" value="${status.value}">No Apto
				        
			</td>
		</tr>
		</table>
		<table id="tbl_Mod" name="tbl_Mod" style="display:none;height:30px;width:97%; margin-left: 9px" 
			cellpadding="0" cellspacing="0" >	
			<tr>
				<td align="center">															
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModif','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Modificar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Mod();" style="cursor:pointer;"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgretrocede','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgretrocede" onclick="fc_Regresar();" style="cursor:pointer;"></a>													
	
				</td>
			</tr>
		</table>
		<div id="divPasantia" style="display:none">
		<table id="tbl_Detalle1" class="" name="tbl_Detalle1" 
			style="display:none; width: 100%" cellpadding="0" cellspacing="0" >
			<tr>
				<td>
					<c:forEach var="objCast" items="${control.listEmpresasPasasntia}" varStatus="loop">
						<table border="0" style="display:; width:97%; margin-left: 9px; height: 30px;" 
							id="tbl<c:out value="${loop.index}" />" class="tablaflotante" 
							name="tbl<c:out value="${loop.index}" />"  
							cellpadding="2" cellspacing="2">
							<tr>
								<td width="130px">
									<input id="cod<c:out value="${loop.index}" />" 
										name="cod<c:out value="${loop.index}" />" 
										value="${objCast.codId}"  
										class="cajatexto" readonly="true" 
										style="display:none;HEIGHT:13px;width:175px;">
									&nbsp;Empresa :
								</td>
								<td>
									<input id="name<c:out value="${loop.index}" />" name="name<c:out value="${loop.index}" />" value="${objCast.nombreEmpresa}"  class="cajatexto" readonly="true" style="HEIGHT:13px;width:265px;">
								</td>
							</tr>
							<tr>
								<td class="">&nbsp;Cantidad :</td>
								<td><input id="ca<c:out value="${loop.index}" />" name="ca<c:out value="${loop.index}" />" class="cajatexto" readonly="true" style="HEIGHT:13px;width:73px" value="${objCast.cantHoras}">
								</td>
								<td class="" width="14%">&nbsp;Estado :&nbsp;&nbsp;</td>
								<td class="tablagrillaglo"><input type="radio" readonly="true" id="ra1<c:out value="${loop.index}" />" name="ra1<c:out value="${loop.index}" />"
									<c:if test="${objCast.flag==control.codFlagApto}"><c:out value=" checked=checked " /></c:if> >Apto
								</td>
			                    <td class="tablagrillaglo"><input type="radio" readonly="true" id="ra2<c:out value="${loop.index}" />" name="ra1<c:out value="${loop.index}" />"
									<c:if test="${objCast.flag==control.codFlagNoApto}"><c:out value=" checked=checked " /></c:if> >No Apto
								</td>
								<td align="right">	
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModi1','','${ctx}/images/iconos/modificar2.jpg',1)">
									<img alt="Modificar" src= "${ctx}/images/iconos/modificar1.jpg" onclick="javascript:fc_Modi('<c:out value="${loop.index}" />');"
												 style="cursor:hand" id="imgModi1">&nbsp;</a>
								</td>
							</tr>
							</table>
					</c:forEach>
				</td>
			</tr>
		</table>
		</div>
		<table class="tablaflotante" style="display:;width:97%; margin-left: 9px" 
			id="tbl_PracticaPre" cellpadding="2" cellspacing="2">
		<tr>
			<td>&nbsp;Nombre Empresa:&nbsp;&nbsp;</td> 
			<td>
			<form:input path="empresa2" id="txhEmpresa2"			
					onkeypress="fc_ValidaTextoNumeroEspecial();"	
					onblur="fc_ValidaNumeroLetrasGuionOnblur('txhEmpresa2','empresa');" 								
					cssClass="cajatexto" size="40" />
			</td>
			<td>Per�odo:&nbsp;&nbsp;</td> 
			<td align="left">
		<form:input path="fechaInicio1" id="txhFechaInicio1"
					onkeypress="fc_ValidaFecha('txhFechaInicio1');"
					onblur="fc_ValidaFechaOnblur('txhFechaInicio1');" 
					cssClass="cajatexto" size="6" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni1','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni1" alt="Calendario" style='cursor:hand;' align="absmiddle"></a> -
			<form:input path="fechaFin1" id="txhFechaFin1"
					onkeypress="fc_ValidaFecha('txhFechaFin1');"
					onblur="fc_ValidaFechaOnblur('txhFechaFin1');" 
					cssClass="cajatexto" size="6" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin1','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin1" alt="Calendario" style='cursor:hand;' align="absmiddle"></a> -
					&nbsp;&nbsp;(DD-MM-AAAA)
			</td>
			<td>Hora Ini:&nbsp; 
				<form:input path="horaIni" id="txhHoraIni"  
						onkeypress="fc_ValidaHora('txhHoraIni');"
						onblur="fc_ValidaHoraOnBlur('frmMain','txhHoraIni','hora inicial');" 
						cssClass="cajatexto" size="3" maxlength="5"/>&nbsp;Hora Fin:&nbsp;
				<form:input path="horaFin" id="txhHoraFin"
						onkeypress="fc_ValidaHora('txhHoraFin');"
						onblur="fc_ValidaHoraOnBlur('frmMain','txhHoraFin','hora final');" 
						cssClass="cajatexto" size="3" maxlength="5"/>
			</td>
		</tr>     
		<tr>
			<td class="" width="14%">&nbsp;Cant. Horas :&nbsp;&nbsp;</td>
			<td>
			<form:input path="cant1" id="txhCant1"  
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur('txhCant1');" 
					cssClass="cajatexto" size="30" maxlength="3"/>

			</td>
			<td class="" width="7%">&nbsp;Nota :&nbsp;&nbsp;</td>
			<td>
			<form:input path="nota" id="txhNota"
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur('txhCant1');" 
					cssClass="cajatexto" size="26" maxlength="3"/>

			</td>
			<td>
			  <input Type="checkbox" id="rdoSab" name="rdoSab" name="gbradio" onclick="javascript:fc_Inc();" value="${status.value}">Inc. Sab.&nbsp;&nbsp;&nbsp;&nbsp;
			  <input Type="checkbox" id="rdoDom" name="rdoDom" name="gbradio1" onclick="javascript:fc_Inc1();" value="${status.value}">Inc. Dom.
				        
			</td>
		</table>
		<table align="center" id="tbl_Mo_di" name="tbl_Mo_di" style="display:none;height:30px;width:98%" cellpadding="0" cellspacing="0" border="0" align="center" height="30px" width="98%">	
			<tr>
				<td align="center">															
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModif','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Modificar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Mo_di();" style="cursor:pointer;"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgretrocede','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgretrocede" onclick="fc_Regresar();" style="cursor:pointer;"></a>													
	
				</td>
			</tr>
		</table>
		<div id="divPractica" style="display:none">
		<table id="tbl_Detalle2" class="" name="tbl_Detalle2" style="display:none;width:100%" cellpadding="0" cellspacing="0" >
			<tr>
				<td>
					<c:forEach var="lobjCast" items="${control.listEmpresasPractica}" varStatus="loop">
						<table id="tb<c:out value="${loop.index}" />" 
							class="tablaflotante" name="tb<c:out value="${loop.index}" />" 
							style="display:;width:97%; margin-left: 9px" cellpadding="2" cellspacing="2" 
							height="30px" >
							<tr>
								<td class="" width="130px">
								<input id="co<c:out value="${loop.index}" />" name="co<c:out value="${loop.index}" />" value="${lobjCast.codId}"  class="cajatexto" readonly="true" style="display:none;HEIGHT:13px;width:175px;">
									&nbsp;Empresa :
								</td>
								<td><input id="na<c:out value="${loop.index}" />" name="na<c:out value="${loop.index}" />" value="${lobjCast.nombreEmpresa}"  class="cajatexto" readonly="true" style="HEIGHT:13px;width:175px;">
								</td>
								<td>Per�odo :</td>
								<td><input id="peini<c:out value="${loop.index}" />" name="peini<c:out value="${loop.index}" />" class="cajatexto" readonly="true" style="HEIGHT:13px;width:73px" value="${lobjCast.fechaInicio}">
								    <input id="pefin<c:out value="${loop.index}" />" name="pefin<c:out value="${loop.index}" />" class="cajatexto" readonly="true" style="HEIGHT:13px;width:73px" value="${lobjCast.fechaFin}">
								</td>
								<td>Hora Ini. :
								<input id="horaIni<c:out value="${loop.index}" />" name="horaIni<c:out value="${loop.index}" />" class="cajatexto" readonly="true" style="HEIGHT:13px;width:73px" value="${lobjCast.horarioIni}">
								  </td>
								<td>Hora Fin. :
								<input id="horaFin<c:out value="${loop.index}" />" name="horaFin<c:out value="${loop.index}" />" class="cajatexto" readonly="true" style="HEIGHT:13px;width:73px" value="${lobjCast.horariofin}">
								</td>
								</tr>    
								<tr>
								<td>Cantidad :</td>
								<td><input id="c<c:out value="${loop.index}" />" name="c<c:out value="${loop.index}" />" class="cajatexto" readonly="true" style="HEIGHT:13px;width:73px" value="${lobjCast.cantHoras}">
								</td>
								<td>Nota :</td>
								<td><input id="nota<c:out value="${loop.index}" />" name="nota<c:out value="${loop.index}" />" class="cajatexto" readonly="true" style="HEIGHT:13px;width:73px" value="${lobjCast.nota}">
								</td>
								<td>
								<input type="checkbox" name="sab"
							<c:if test="${lobjCast.indSabado=='1'}"><c:out value=" checked=checked " /></c:if>
							id='sab<c:out value="${loop.index}"/>'
							value='<c:out value="${loop.index}"/>'>Inc. Sab.
							</td><td>
							<input type="checkbox" name="dom"
							<c:if test="${lobjCast.indDomingo=='1'}"><c:out value=" checked=checked " /></c:if>
							id='dom<c:out value="${loop.index}"/>'
							value='<c:out value="${loop.index}"/>'>Inc. Dom.
								</td>
								</tr>
								<tr>
								<td></td><td></td><td></td><td></td><td></td>
								<td align="right">	
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModi1','','${ctx}/images/iconos/modificar2.jpg',1)">
									<img alt="Modificar" src= "${ctx}/images/iconos/modificar1.jpg" onclick="javascript:fc_Mo('<c:out value="${loop.index}" />');"
												 style="cursor:hand" id="imgModi1">&nbsp;</a>
								</td>
							</tr>
							</table>
					</c:forEach>
				</td>
			</tr>
		</table>
		</div>
		<table><tr height="5px"><td></td></tr></table>
		<table align="center" style="height:30px;width:40%"  cellpadding="0" cellspacing="0" border="0" >	
			<tr>
				<td align="center" id="tbl_Grabar" name="tbl_Grabar" style="display:none;">		
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="fc_Grabar();" style="cursor:pointer;">
					</a>
				</td>
				<td align="center">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgretrocede','','${ctx}/images/botones/regresar2.jpg',1)">
						<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgretrocede" onclick="fc_Regresar();" style="cursor:pointer;">
					</a>
				</td>
			</tr>
		</table>
	</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txhFechaIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	
	Calendar.setup({
		inputField     :    "txhFechaFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaInicio1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni1",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaFin1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin1",
		singleClick    :    true
	});
</script>