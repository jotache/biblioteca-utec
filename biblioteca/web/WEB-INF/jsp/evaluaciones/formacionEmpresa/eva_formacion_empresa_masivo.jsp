<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad()
			{
	}
	
	function fc_Aparece(param){			
		DatosVeh.style.display = "none";
		OT.style.display = "none";
		
		td_DatosVehiculo.className = "Tab";
		td_OT.className = "Tab";
		switch(param){
			case '1':
				DatosVeh.style.display="";
				td_DatosVehiculo.className = "Tab_resaltado";
				break;
			case '2':
				OT.style.display="";
				td_OT.className = "Tab_resaltado";
				break;
			}	
	}
	function fc_Regresar(){
		location.href = "${ctx}/menuEvaluaciones.html";
	}
	
	function fc_Registrar(){
	
		//JHPR 2008-06-06 obtener alumnos seleccionados.
		//alert('Tama�o lista:'+document.getElementById("tamanioLista").value);		
		var max = document.getElementById("tamanioLista").value;
		var aAlumnos = new Array();
		var inputChk;
		var contador=0;
		for(i=0;i<max;i++) {
			inputChk = document.getElementById("chk"+i);
			if (inputChk.checked==true){
				aAlumnos[contador]=inputChk.value;
				contador=contador+1;
			}
		}
		
		if (contador>0) {
		// TODO: Validar que se haya seleccionado al menos un alumno
	 	//if ( document.getElementById("txhNombreAlumno").value != "") { 
			document.getElementById("txhCodEspecialidad").value = document.getElementById("cboEspecialidad").value;
			
			strNombreAlumno ="123"
			strCodAlumno = "123"
			
			strCodEspecialidad = document.getElementById("txhCodEspecialidad").value;
			strCodPeriodo = document.getElementById("txhCodPeriodo").value;
			strCodEva = document.getElementById("txhCodEva").value;
			window.location.href = ("${ctx}/evaluaciones/eva_registrar_empresa.html?txhNombreAlumno=" + strNombreAlumno + "&txhCodAlumno=" + strCodAlumno + "&txhCodEspecialidad=" + strCodEspecialidad + "&txhCodPeriodo=" + strCodPeriodo + "&txhCodEva=" + strCodEva+"&aDatosAlumno=" + aAlumnos);
		} else {
			alert(mstrSeleccione);
		}
	 }
	 
	function fc_Buscar(){
		document.getElementById("txhOperacion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
	}

	function fc_seleccionarRegistro(strCodAlumno, strNombreAlumno){
	document.getElementById("txhNombreAlumno").value = strNombreAlumno;
	document.getElementById("txhCodAlumno").value = strCodAlumno;
	}

</script>
	</head>

   	<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_formacion_empresa.html">
		<form:hidden path="operacion" id="txhOperacion"/>	
		<form:hidden path="nombreAlumno" id="txhNombreAlumno"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEva" id="txhCodEva"/>
		
		<form:hidden path="tamanioLista" id="idTamanioLista"/>
		
		
		<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>
		
		<!-- Titulo de la Pagina -->
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="753px" class="opc_combo"><font style="">Control Formaci�n Empresa</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			 </tr>
		 </table>
		
		<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" align="center" width="98%">
			<tr>
				<td>
					<table class="tabla" style="width:97%;margin-top:6px" cellspacing="2" background="${ctx}/images/Evaluaciones/back-sup.jpg"
					 cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td class="" >&nbsp;Per�odo Vigente :</td>
							<td colspan=3>
							<form:input path="periodo" id="txtPeriodo" cssClass="cajatexto_1" cssStyle="HEIGHT: 12px;width:100px" readonly="true" />
							</td>
						</tr>
						<tr style="display: none">
							<td class="" >&nbsp;Producto :</td>
							<td  align="left" colspan=3>
							<form:input path="producto" id="txtProducto" cssClass="cajatexto_1" cssStyle="HEIGHT: 12px;width:250px" readonly="true" />
							</td>
						</tr>
						<tr>
							<td class="" >&nbsp;Especialidad :</td>
							<td  align="left" colspan=3>
								<form:select  path="codEspe" id="cboEspecialidad" cssClass="cajatexto" 
								cssStyle="width:260px" >
							        <c:if test="${control.listEva!=null}">
						        	<form:options itemValue="codEspecialidad" itemLabel="descripcion" 
						        		items="${control.listEva}" />						        
						            </c:if>
	 				            </form:select>
								</td>
							<td class="">&nbsp;&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="" >&nbsp;Apellido Paterno :</td>
							<td  align="left">
							<form:input path="apellidos" id="txhApellidos"
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaNumerosAndLetrasFinalTodo('txhApellidos','apellido paterno');" 
								cssClass="cajatexto" size="46" />
								</td>
							<td class="">&nbsp; Nombre:</td>
							<td>
							<form:input path="nombre" id="txhNombre"
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaNumerosAndLetrasFinalTodo('txhNombre','nombre');" 
								cssClass="cajatexto" size="46" />&nbsp;&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:fc_Buscar();" id="imgbuscar" style="cursor:pointer;" align="center" alt="Buscar"></a></td>
							</tr>
					</table>
				</td>
			</tr>
			<tr height="15px"><td></td></tr>
			<tr style="height:20px;">
				<td>
				<!-- Titulo de la bandeja -->
					 <table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
						 <tr>
						 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
						 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="400px" class="opc_combo">Relaci�n de Alumnos</td>
						 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
						 </tr>
					 </table>
				</td>						
			</tr>
			
			<table class="bordegrilla" style="width:99%;margin-top:6px" cellspacing="0" cellpadding="0" border="0" align="center">
				<tr>
					<TD class="headtabla" width="5%">&nbsp;Sel.</TD>
					<TD class="headtabla" width="10%" align="center">&nbsp;C&oacute;digo</TD>
					<TD class="headtabla" width="53%">&nbsp;Alumno</TD>
					<TD class="headtabla" width="10%" align="center">&nbsp;Pr&aacute;ctica inicial</TD>
					<TD class="headtabla" width="10%" align="center">&nbsp;Pasant&iacute;a</TD>
					<TD class="headtabla" width="10%" align="center">&nbsp;Pr&aacute;ctica Preprofesional</TD>
				</tr>
				<c:forEach var="lista" items="${control.listAlum}" varStatus="loop">
					<c:choose>
						<c:when test="${loop.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
						<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>						
					<td align="center">
					<input type="checkbox" value="${lista.codAlumno}" id="chk<c:out value="${loop.index}"/>" />
					</td>
					<td align="center" ><c:out value="${lista.codAlumno}" /></td>
					<td align="left" ><c:out value="${lista.nombreAlumno}" /></td>
					<td align="center" ><c:out value="${lista.flagPracticaInicial}" /></td>
					<td align="center" ><c:out value="${lista.flagPasantia}" /></td>
					<td align="center" ><c:out value="${lista.flagPracticaPreProf}" /></td>
					</tr>
				</c:forEach>
			</table>
			<% request.getSession().removeAttribute("ListAlum"); %>
					
		<table cellpadding="0" cellspacing="0" border="0" align="center" height="30px" width="98%">	
			<tr>
				<td align="center">		
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregistrar','','${ctx}/images/botones/registrar2.jpg',1)">								
				<img alt="Registrar" onclick="javascript:fc_Registrar();" src="${ctx}/images/botones/registrar1.jpg" id="imgregistrar" style="cursor:pointer"/></a>
				</td>
			</tr>
		</table>									
</form:form>
