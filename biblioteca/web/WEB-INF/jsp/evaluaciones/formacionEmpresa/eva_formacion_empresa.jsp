<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad()
	{
	}
	
	function fc_Aparece(param){			
		DatosVeh.style.display = "none";
		OT.style.display = "none";		
		td_DatosVehiculo.className = "Tab";
		td_OT.className = "Tab";
		switch(param){
			case '1':
				DatosVeh.style.display="";
				td_DatosVehiculo.className = "Tab_resaltado";
				break;
			case '2':
				OT.style.display="";
				td_OT.className = "Tab_resaltado";
				break;
		}	
	}
	
	function fc_Regresar(){
		location.href = "${ctx}/menuEvaluaciones.html";
	}
	
	function fc_Registrar(){	
		var max = document.getElementById("idTamanioLista").value;
		var aAlumnos = new Array();
		var aNom = new Array();
		var inputChk;
		var inputNom;
		var contador=0;
		//JHPR: 2008-06-23 -- Formaci�n de Empresa registro masivo.		
		for(i=0;i<max;i++) {
			inputChk = document.getElementById("chk"+i);
			inputNom = document.getElementById("nom_"+i);			
			if (inputChk.checked==true){
				aAlumnos[contador]=inputChk.value;
				aNom[contador]=inputNom.value;
				contador=contador+1;
			}
		}			
		
		if (contador!=0) {
			if (contador==1) {	 			
				document.getElementById("txhCodEspecialidad").value = document.getElementById("cboEspecialidad").value;				
				strNombreAlumno = aNom[0];
				strCodAlumno = aAlumnos[0];				
				strCodEspecialidad = document.getElementById("txhCodEspecialidad").value;
				strCodPeriodo = document.getElementById("txhCodPeriodo").value;
				strCodEva = document.getElementById("txhCodEva").value;				
				window.location.href = ("${ctx}/evaluaciones/eva_registrar_empresa.html?txhNombreAlumno=" + strNombreAlumno + "&txhCodAlumno=" + strCodAlumno + "&txhCodEspecialidad=" + strCodEspecialidad + "&txhCodPeriodo=" + strCodPeriodo + "&txhCodEva=" + strCodEva);
			} else {
				document.getElementById("txhCodEspecialidad").value = document.getElementById("cboEspecialidad").value;
				strCodEspecialidad = document.getElementById("txhCodEspecialidad").value;
				strCodPeriodo = document.getElementById("txhCodPeriodo").value;
				strCodEva = document.getElementById("txhCodEva").value;
				window.location.href = ("${ctx}/evaluaciones/eva_registrar_empresa_masivo.html?txhCodEspecialidad=" + strCodEspecialidad + "&txhCodPeriodo=" + strCodPeriodo + "&txhCodEva=" + strCodEva + "&aDatosAlumno=" + aAlumnos);
			}
		} else
			alert(mstrSeleccione);
					
	 }
	 
	 
	function fc_RegistroMasivo() {
		
	}
	
	function fc_Buscar(){
	document.getElementById("txhOperacion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
	}

	function fc_seleccionarRegistro(strCodAlumno, strNombreAlumno){
	document.getElementById("txhNombreAlumno").value = strNombreAlumno;
	document.getElementById("txhCodAlumno").value = strCodAlumno;
	}

</script>
	</head>

   	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_formacion_empresa.html">
		<form:hidden path="operacion" id="txhOperacion"/>	
		<form:hidden path="nombreAlumno" id="txhNombreAlumno"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEva" id="txhCodEva"/>
		
		<form:hidden path="tamanioLista" id="idTamanioLista"/>
		
		<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>
		
		<!-- Titulo de la Pagina -->
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="753px" class="opc_combo"><font style="">Control Formaci�n Empresa</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			 </tr>
		 </table>
		
		<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" align="center" width="98%">
			<tr>
				<td>
					<table class="tabla" style="width:97%;margin-top:6px" cellspacing="2" background="${ctx}/images/Evaluaciones/back-sup.jpg"
					 cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td class="" >&nbsp;Per�odo Vigente :</td>
							<td colspan=3>
							<form:input path="periodo" id="txtPeriodo" cssClass="cajatexto_1" cssStyle="HEIGHT: 12px;width:100px" readonly="true" />
							</td>
						</tr>
						<tr style="display: none">
							<td class="" >&nbsp;Producto :</td>
							<td  align="left" colspan=3>
							<form:input path="producto" id="txtProducto" cssClass="cajatexto_1" cssStyle="HEIGHT: 12px;width:250px" readonly="true" />
							</td>
						</tr>
						<tr>
							<td class="" >&nbsp;Especialidad :</td>
							<td  align="left" colspan=3>
								<form:select  path="codEspe" id="cboEspecialidad" cssClass="cajatexto" 
								cssStyle="width:260px" >
							        <c:if test="${control.listEva!=null}">
						        	<form:options itemValue="codEspecialidad" itemLabel="descripcion" 
						        		items="${control.listEva}" />						        
						            </c:if>
	 				            </form:select>
								</td>
							<td class="">&nbsp;&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="" >&nbsp;Apellido Paterno :</td>
							<td  align="left">
							<form:input path="apellidos" id="txhApellidos"
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaNumerosAndLetrasFinalTodo('txhApellidos','apellido paterno');" 
								cssClass="cajatexto" size="46" />
							</td>
							<td class="">&nbsp; Nombre:</td>
							<td>
							<form:input path="nombre" id="txhNombre"
								onkeypress="fc_ValidaTextoEspecial();"									    
								onblur="fc_ValidaNumerosAndLetrasFinalTodo('txhNombre','nombre');" 
								cssClass="cajatexto" size="46" />&nbsp;&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:fc_Buscar();" id="imgbuscar" style="cursor:pointer;" align="center" alt="Buscar"></a></td>
							</tr>
					</table>
				</td>
			</tr>			
			<tr height="15px"><td></td></tr>
			<tr style="height:20px;">
				<td>
				<!-- Titulo de la bandeja -->
					 <table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
						 <tr>
						 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
						 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="400px" class="opc_combo"><font >Relaci�n de Alumnos</font></td>
						 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
						 </tr>
					 </table>
				</td>						
			</tr>

			
			
			
			<!-- 
			<table class="" style="width:98%;margin-top:6px" cellspacing="2" cellpadding="0" border="0" bordercolor="red" align="center">
			<tr>
				<td>
					<div style="overflow: auto; height: 230px;width:99%">
						<display:table name="sessionScope.ListAlum" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.CursoEvaluadorDecorator" 
							requestURI="" style="border: 1px solid #048BBA;width:98%">
							<display:column property="chkAlumno" title="Sel." style="width:5%;text-align:center" />
							<display:column property="codAlumno" title="C�digo" style="text-align:center;width:10%"/>
							<display:column property="nombreAlumno" title="Alumno" style="text-align:left;width:55%"/>							
							<display:column property="chkPracticaInicial"  title="Pr�ctica inicial"  style="text-align:center;width:10%"/>
							<display:column property="chkPasantia" title="Pasant�a" style="text-align:center;width:10%"/>
							<display:column property="chkPracticaPre" title="Pr�ctica Preprofesional" style="text-align:center;width:10%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
							
						</display:table>
						<% //request.getSession().removeAttribute("ListAlum"); %>
					</div>
				</td>
			</tr>
			</table> 
			-->		
		</table>
				
		<div style="height:280px; width:978px; overflow:auto;mpositioning:FlowLayout">			
		<table class="" style="width:930px;margin-left:11px;border: 1px solid #048BBA;" cellspacing="0" cellpadding="0" border="0" align="left">
			<tr>
				<TD class="headtabla" width="5%" align="center">&nbsp;Sel.</TD>
				<TD class="headtabla" width="10%" align="center">&nbsp;C&oacute;digo</TD>
				<TD class="headtabla" width="10%" align="center">&nbsp;Ciclo</TD>
				<TD class="headtabla" width="43%">&nbsp;Alumno</TD>
				<TD class="headtabla" width="10%" align="center">&nbsp;Pr&aacute;ctica inicial</TD>
				<TD class="headtabla" width="10%" align="center">&nbsp;Pasant&iacute;a</TD>
				<TD class="headtabla" width="10%" align="center">&nbsp;Pr&aacute;ctica Preprofesional</TD>
			</tr>
			<c:forEach var="lista" items="${control.listAlum}" varStatus="loop">
				<c:choose>
					<c:when test="${loop.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
					<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
				</c:choose>				
				<td align="center">
					<input type="checkbox" value="${lista.codAlumno}" id="chk<c:out value="${loop.index}"/>" />
					<input type="hidden" value="${lista.nombreAlumno}" id="nom_<c:out value="${loop.index}"/>"/>
				</td>
				<td align="center" ><c:out value="${lista.codAlumno}" /></td>
				<td align="center" ><c:out value="${lista.ciclo}" /></td>
				<td align="left" ><c:out value="${lista.nombreAlumno}" /></td>
				<td align="center" >
					<c:if test="${lista.flagPracticaInicial=='1'}">
						<img alt="" width="16" height="15" src="${ctx}/images/iconos/check_on_wb.gif" border="0" align="middle"/>
					</c:if>
					<c:if test="${lista.flagPracticaInicial!='1'}">
						&nbsp;
					</c:if>
					<%--<c:out value="${lista.flagPracticaInicial}" />--%>
				</td>
				<td align="center" >
					<c:if test="${lista.flagPasantia=='1'}">
						<img alt="" width="16" height="15" src="${ctx}/images/iconos/check_on_wb.gif" border="0" align="middle"/>
					</c:if>
					<c:if test="${lista.flagPasantia!='1'}">
						&nbsp;
					</c:if>					
					<%--<c:out value="${lista.flagPasantia}" />--%>
				</td>
				
				<td align="center" >
					<c:if test="${lista.flagPracticaPreProf=='1'}">
						<img alt="" width="16" height="15" src="${ctx}/images/iconos/check_on_wb.gif" border="0" align="middle"/>
					</c:if>
					<c:if test="${lista.flagPracticaPreProf!='1'}">
						&nbsp;
					</c:if>					
					<%--<c:out value="${lista.flagPracticaPreProf}" />--%>
				</td>
				
				</tr>
			</c:forEach>
			<% request.getSession().removeAttribute("ListAlum"); %>				
		</table>
		</div>
				
		<table cellpadding="0" cellspacing="0" border="0" align="center" height="30px" width="98%">	
			<tr>
				
				<td align="center">					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregistrar','','${ctx}/images/botones/registrar2.jpg',1)">								
						<img alt="Registrar" onclick="javascript:fc_Registrar();" src="${ctx}/images/botones/registrar1.jpg" id="imgregistrar" style="cursor:pointer"/>
					</a>
				</td>
			</tr>
		</table>									
</form:form>
