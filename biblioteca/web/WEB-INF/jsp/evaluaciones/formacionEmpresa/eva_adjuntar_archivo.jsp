<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
		var strExtensionDOC = "<%=(String)request.getAttribute("strExtensionDOC")%>";
		var strExtensionXLS = "<%=(String)request.getAttribute("strExtensionXLS")%>";
		var strExtensionDOCX = "<%=(String)request.getAttribute("strExtensionDOCX")%>";
		var strExtensionXLSX = "<%=(String)request.getAttribute("strExtensionXLSX")%>";
		var strExtensionPDF =  "<%=(String)request.getAttribute("strExtensionPDF")%>";
		function onLoad()
		{
	
		objMsg = document.getElementById("txhMessage");
			if ( objMsg.value == "OK" )
			{
				alert(mstrGrabar);
				window.opener.document.getElementById("frmMain").submit();
				window.close();
			}
			else if ( objMsg.value == "ERROR" )
			{
				alert(mstrProblemaGrabar);			
			}
		}
		
		function fc_Valida(){
			var strExtension;
			var lstrRutaArchivo;
		if(fc_Trim(document.getElementById("txtCV").value) == ""){
				alert('Debe seleccionar su Informe');
					return false;
			}
			else{
				if(fc_Trim(document.getElementById("txtCV").value) != ""){
					lstrRutaArchivo = document.getElementById("txtCV").value;
					strExtension = "";
					strExtension = lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);
				
					nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();				 
					document.getElementById("txhNomArchivo").value=nombre.substring("1");
			
				
					if (strExtension != strExtensionDOC.toUpperCase() && strExtension != strExtensionDOCX.toUpperCase() && strExtension != strExtensionXLS.toUpperCase() && strExtension != strExtensionXLSX.toUpperCase() && strExtension !=strExtensionPDF.toUpperCase() ){
						alert('El Informe debe tener las siguientes\nextensiones : DOC, DOCX, XLS, XLSX o PDF');
						return false;
					}
					else{
						document.getElementById("txhExtCv").value = strExtension;
					}
				}
			}
			return true;
		}
		
		
		
		function fc_GrabarArchivos(){
			if (fc_Valida()){
				if (confirm(mstrSeguroGrabar)){
					document.getElementById("txhOperacion").value="GRABAR";
					document.getElementById("frmMain").submit();			
				}
			}
		}

			</script>
	</head>	
	<form:form name="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_adjuntar_archivo.html" 
		enctype="multipart/form-data" method="post">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="codAlumno" id="txhCodAlumno"/>
		<form:hidden path="extCv" id="txhExtCv"/>
		<form:hidden path="nomArchivo" id="txhNomArchivo"/>
		<form:hidden path="txhPath" id="txhPath"/>
		<form:hidden path="codEva" id="txhCodEva"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<table cellpadding="0" cellspacing="0" border="0" style="margin-left:5px;margin-top:5px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="550px" class="opc_combo"><font style="">Adjuntar Archivo</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
			 </tr>
		 </table>
		<table style="width:98%;margin-top:5px" align="center" cellpadding="1" cellspacing="2" class="tablaflotante" border="0" bordercolor="red">
			<tr>		
				<td class="texto_bold" width="50%">Archivo:</td>
				<td>
					<input type="file"  name="txtCV"  class="cajatexto_1" size="50">
			
				</td>
			</tr>
		</table>
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_GrabarArchivos();" style="cursor:hand" id="imgAgregar" alt="Grabar">&nbsp;</a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img  src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();" id="imgEliminar"></a>
				</td>
			</tr>
		</table>
</form:form>


