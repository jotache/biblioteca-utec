<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;" type="text/JavaScript"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	
	
	//bandera = "<%=((request.getAttribute("Bandera")==null)?"":(String)request.getAttribute("Bandera"))%>";
	
	function onLoad(){              
		objflag = document.getElementById("txhFlag").value;		
		fc_Aparece(objflag);		
		
		objMsg = document.getElementById("txhMsg");
		objMen = document.getElementById("txhMen");
		objMens = document.getElementById("txhMens");	
			
		if ( objMsg.value == "OK" )	{			
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrSeGraboConExito);			
		} else if ( objMsg.value == "ERROR" ) {
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			alert(mstrProblemaGrabar);			
		}		
			
		if(objMen.value =="OK"){
			objMen.value = "";
			document.getElementById("txhMen").value = "";
			alert(mstrSeGraboConExito);	
		} else if (objMen.value == "ERROR"){
			objMen.value = "";
			document.getElementById("txhMen").value = "";
			alert(mstrProblemaGrabar);			
		}		
				
		if ( objMens.value == "OK" ) {
			objMens.value = "";
			document.getElementById("txhMens").value = "";
			alert('Se actualiz� exitosamente.');			
		} else if (objMens.value == "ERROR") {
			objMens.value = "";
			document.getElementById("txhMens").value = "";
			alert('Problemas al actualizar');			
		}
		objMsg = "";
		objMen = "";
		objMens.value = "";
		document.getElementById("txhMsg").value = "";
		document.getElementById("txhMen").value = "";
		document.getElementById("txhMens").value = "";			
	}
	
	function fc_Regresar() {
		window.location.href = ("${ctx}/evaluaciones/eva_formacion_empresa.html" +
							"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
							"&txhCodEvaluador=" + document.getElementById('txhCodEva').value);	
	}

	function fc_Grabar(){			
		fechaIni = document.getElementById("txhFechaIni").value;
		fechaFin = document.getElementById("txhFechaFin").value; 
		fechaIni1 = document.getElementById("txhFechaInicio1").value;
		fechaFin1 = document.getElementById("txhFechaFin1").value;
		if(document.getElementById("txhTipo").value == "0"){
			if (confirm('�Seguro de Registrar la Pr�ctica Inicial?')){
			
			emp = document.getElementById("txhEmpresa").value;
			if(fc_Trim(emp)){				
				if (fc_ValidaFechaIniFechaFin(fechaIni,fechaFin)==0){
					if(document.getElementById("txhFechaIni").value !==""){
						if(document.getElementById("txhFechaFin").value !==""){					
							document.getElementById("txhOperacion").value = "GRABAR";
							document.getElementById("frmMain").submit();
				 		}else{
				 			alert('Debe ingresar una fecha de fin');
				 			return false;
				 		}
					}else{
						alert('Debe ingresar una fecha inicial');
						return false;
					}
				}else{
			 		alert('La fecha de inicio debe ser menor a la fecha final.');
			 	}
			}else{
				alert('Debe ingresar una empresa');
				return false;
			}
			
			}
	    }
		
	
		if(document.getElementById("txhTipo").value == "1"){
			if (confirm('�Seguro de Registrar la Pasant�a?')){
			emp = document.getElementById("txhEmpresa1").value;
			if(fc_Trim(emp)){
				if(document.getElementById("txhCant").value !==""){
					if(document.getElementById("txhEstado").value !==""){
						document.getElementById("txhOperacion").value = "GRABAR";						
						document.getElementById("frmMain").submit();
					}else{
						alert('Debe seleccionar un estado');
						return false;
					}
				}else{
					alert('Debe ingresar la cantidad de horas');
					return false;
				}
			}else{
				alert('Debe ingresar una empresa');
				return false;
			}
			}
		}
		//Fin txhTipo=="1"
	
		if(document.getElementById("txhTipo").value == "2"){
			if (confirm('�Seguro de Registrar la Pr�ctica Pre-Profesional?')){
			emp = document.getElementById("txhEmpresa2").value;
			if(fc_Trim(emp)){
				if (fc_ValidaFechaIniFechaFin(fechaIni1,fechaFin1)==0){
					if(document.getElementById("txhEmpresa2").value !==""){
						if(document.getElementById("txhCant1").value !==""){
							if(document.getElementById("txhFechaInicio1").value !==""){
								if(document.getElementById("txhFechaFin1").value !==""){
									document.getElementById("txhOperacion").value = "GRABAR";
									document.getElementById("frmMain").submit();
				 				}else{
				 					alert('Debe ingresar una fecha de fin');
				 					return false;
				 				}
							}else{
								alert('Debe ingresar una fecha inicial');
								return false;
							}
			 			}else{
							alert('Debe ingresar la cantidad de horas');
							return false;
						}
					}else{
						alert('Debe ingresar una empresa');
						return false;
					}
				}else{
					alert('La fecha de inicio debe ser menor a la fecha final.');
				}
			}else{
				alert('Debe ingresar una empresa');
				return false;
			}
			}
		}
		
		//document.getElementById("txhOperacion").value = "";
		//document.getElementById("txhEmpresa").value = "";
		emp = "";
	}

	function fc_Aparece(param){			
					
		switch(param){
		case '0': //Practica Inicial
			document.getElementById("txhTipo").value = "0";			
			if(document.getElementById("txhTotemp").value < 3){
				tbl_Grabar.style.display="";
				tbl_PracticaInicial.style.display="";
			}
			tbl_Pasantia.style.display="none";
			tbl_PracticaPre.style.display="none";
			PracticaInicial.className="Tab_off";
			Pasantia.className="Tab_on";
			PracticaPre.className="Tab_on";	
			break;
		case '1': //Pasant�a
			document.getElementById("txhTipo").value = "1";							
			tbl_Grabar.style.display="";
			tbl_PracticaInicial.style.display="none";
			tbl_Pasantia.style.display="";
			tbl_PracticaPre.style.display="none";
			PracticaInicial.className="Tab_on";
			Pasantia.className="Tab_off";
			PracticaPre.className="Tab_on";
			break;
		case '2':
			document.getElementById("txhTipo").value = "2";
			tbl_Grabar.style.display="";
			tbl_PracticaInicial.style.display="none";
			tbl_Pasantia.style.display="none";
			tbl_PracticaPre.style.display="";
			PracticaInicial.className="Tab_on";
			Pasantia.className="Tab_on";
			PracticaPre.className="Tab_off";
		
			break;
	}	
	bandera = "";
}
	function fc_Etapa(param){
		switch(param){
			case '1':
				document.getElementById("txhEstado").value = "0001";
				break;
			case '2':
				document.getElementById("txhEstado").value = "0002";
				break;
		}
	}

	/*
	function fc_Modifica(){		
		fechaIni = document.getElementById("txhFechaIni").value;
		fechaFin = document.getElementById("txhFechaFin").value; 
		document.getElementById("txhCantHoras").value = document.getElementById("txhCHoras").value;
		document.getElementById("txhObser").value = document.getElementById("txhObservacion").value;	
		emp = document.getElementById("txhEmpresa").value;
		if(fc_Trim(emp)){
		if (fc_ValidaFechaIniFechaFin(fechaIni,fechaFin)==0){
			if(document.getElementById("txhFechaIni").value !==""){
				if(document.getElementById("txhFechaFin").value !==""){
				document.getElementById("txhOperacion").value = "MODIFICA";
				document.getElementById("frmMain").submit();
				 }
				 else{
				 alert('Debe ingresar una fecha de fin');
				 return false;
				 }
				}
				else{
				alert('Debe ingresar una fecha inicial');
				return false;				
				}
				}
			else{
			 	alert('La fecha de inicio debe ser menor a la fecha final.');
			 	}
			}
		else{
		alert('Debe ingresar una empresa');
		return false;
		}
		document.getElementById("txhEmpresa").value = "";
	}	

	function fc_Modifica1(){ 
	        document.getElementById("txhCodId").value = document.getElementById("txhCodId1").value;
			document.getElementById("txhEmpresa").value = document.getElementById("txtEmp1").value;
			document.getElementById("txhFechaIni").value = document.getElementById("txtFini1").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txtFfin1").value;
			document.getElementById("txhCHoras").value = document.getElementById("txtCa1").value;
			document.getElementById("txhObservacion").value = document.getElementById("txtOb1").value;						 
			document.getElementById("txhVez").value = "2";
	
			tbl_Modifica.style.display="";
			tbl_PracticaInicial.style.display="";
			tbl_PracticaInicial1.style.display="none";
			if(document.getElementById("txtEmp2").value !== ""){
			tbl_PracticaInicial2.style.display="";
			}
			if(document.getElementById("txtEmp3").value !== ""){
			tbl_PracticaInicial3.style.display="";
			}
			tbl_Pasantia.style.display="none";
			
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			tbl_Detalle2.style.display="none";
			divPractica.style.display="none";
			tbl_Mo_di.style.display="none";
			tbl_Grabar.style.display="none";
		}
		function fc_Modifica2(){ 
			document.getElementById("txhCodId").value = document.getElementById("txhCodId2").value;
			document.getElementById("txhEmpresa").value = document.getElementById("txtEmp2").value;
			document.getElementById("txhFechaIni").value = document.getElementById("txtFini2").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txtFfin2").value;
			document.getElementById("txhCHoras").value = document.getElementById("txtCa2").value;
			document.getElementById("txhObservacion").value = document.getElementById("txtOb2").value;
					document.getElementById("txhVez").value = "2";
	
			tbl_Modifica.style.display="";
			tbl_PracticaInicial.style.display="";
			if(document.getElementById("txtEmp1").value !== ""){
			tbl_PracticaInicial1.style.display="";
			}
			tbl_PracticaInicial2.style.display="none";
			if(document.getElementById("txtEmp3").value !== ""){
			tbl_PracticaInicial3.style.display="";
			}
			tbl_Pasantia.style.display="none";
			
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			tbl_Detalle2.style.display="none";
			divPractica.style.display="none";
			tbl_Mo_di.style.display="none";
			tbl_Grabar.style.display="none";
		}
		function fc_Modifica3(){ 
			document.getElementById("txhCodId").value = document.getElementById("txhCodId3").value;
			document.getElementById("txhEmpresa").value = document.getElementById("txtEmp3").value;
			document.getElementById("txhFechaIni").value = document.getElementById("txtFini3").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txtFfin3").value;
			document.getElementById("txhCHoras").value = document.getElementById("txtCa3").value;
			document.getElementById("txhObservacion").value = document.getElementById("txtOb3").value;
			document.getElementById("txhVez").value = "2";
			tbl_Modifica.style.display="";
			tbl_PracticaInicial.style.display="";
			if(document.getElementById("txtEmp1").value !== ""){
			tbl_PracticaInicial1.style.display="";
			}
			if(document.getElementById("txtEmp2").value !== ""){
			tbl_PracticaInicial2.style.display="";
			}
			tbl_PracticaInicial3.style.display="none";
			tbl_Pasantia.style.display="none";
			
			divPasantia.style.display ="none";
			tbl_Detalle1.style.display="none";
			tbl_PracticaPre.style.display="none";
			tbl_Detalle2.style.display="none";
			divPractica.style.display="none";
			tbl_Mo_di.style.display="none";
			tbl_Grabar.style.display="none";
		}
		
		function fc_Modi(param1){
			tbl_Grabar.style.display="none";
			tbl_Mod.style.display="";
			document.getElementById("txhCodId").value = document.getElementById("cod"+param1).value;
			document.getElementById("txhEmpresa1").value = document.getElementById("name"+param1).value;
			document.getElementById("txhCant").value = document.getElementById("ca"+param1).value;
			document.getElementById("txhVez").value = "2";
			document.getElementById("tbl"+param1).style.display="none";
			if(document.getElementById("ra1"+param1).checked ==true){
			//document.getElementById("rdoEtapa").checked = true;
			document.getElementById("txhEstado").value = "0001";
			}
			if(document.getElementById("ra2"+param1).checked ==true){
			//document.getElementById("rdoEtapa1").checked = true;
			document.getElementById("txhEstado").value = "0002";
			}
		}
		
		function fc_Mod(){
			document.getElementById("txhEmpresa").value = document.getElementById("txhEmpresa1").value;
			document.getElementById("txhCantHoras").value = document.getElementById("txhCant").value;
			
			emp = document.getElementById("txhEmpresa").value;
			if(fc_Trim(emp)){
			if(document.getElementById("txhCant").value !==""){
			if(document.getElementById("txhEstado").value !==""){
			document.getElementById("txhOperacion").value = "MODIFICA";
			document.getElementById("frmMain").submit();
				}
				else{
				alert('Debe seleccionar un estado');
				return false;
				}
			}
			else{
			alert('Debe ingresar la cantidad de horas');
			return false;
			}
			}
			else{
			alert('Debe ingresar una empresa');
			return false;
			}
		}
		
		
		function fc_Mo(param1){
			tbl_Mod.style.display="none";
			tbl_Grabar.style.display="none";
			tbl_Mo_di.style.display="";
			document.getElementById("txhCodId").value = document.getElementById("co"+param1).value;
			document.getElementById("txhEmpresa2").value = document.getElementById("na"+param1).value;
			document.getElementById("txhFechaInicio1").value = document.getElementById("peini"+param1).value;
			document.getElementById("txhFechaFin1").value = document.getElementById("pefin"+param1).value;
			document.getElementById("txhCant1").value = document.getElementById("c"+param1).value;
			
			document.getElementById("txhHoraIni").value = document.getElementById("horaIni"+param1).value;
			document.getElementById("txhHoraFin").value = document.getElementById("horaFin"+param1).value;
			document.getElementById("txhNota").value = document.getElementById("nota"+param1).value;
			
			if(document.getElementById("sab"+param1).checked ==true){
			document.getElementById("rdoSab").checked = true;
			document.getElementById("txhIndSabado").value = "0001";
			}
			if(document.getElementById("dom"+param1).checked ==true){
			document.getElementById("rdoDom").checked = true;
			document.getElementById("txhIndDomingo").value = "0001";
			}
			
			document.getElementById("tb"+param1).style.display="none";
			document.getElementById("txhVez").value = "2";
		}
		
		function fc_Mo_di(){
	
			document.getElementById("txhFechaIni").value = document.getElementById("txhFechaInicio1").value;
			document.getElementById("txhFechaFin").value = document.getElementById("txhFechaFin1").value;
			
			fechaIni = document.getElementById("txhFechaIni").value;
			fechaFin = document.getElementById("txhFechaFin").value; 
			
			document.getElementById("txhEmpresa").value = document.getElementById("txhEmpresa2").value;
			document.getElementById("txhCantHoras").value = document.getElementById("txhCant1").value;
			       
			document.getElementById("txhNot").value = document.getElementById("txhNota").value;
			document.getElementById("txhHoraInicio").value = document.getElementById("txhHoraIni").value;
			document.getElementById("txhHorafinaliza").value = document.getElementById("txhHoraFin").value;
			document.getElementById("txhSelSabado").value = document.getElementById("txhIndSabado").value;
			document.getElementById("txhSelDomingo").value = document.getElementById("txhIndDomingo").value;
			
			emp = document.getElementById("txhEmpresa").value;
			if(fc_Trim(emp)){
			if (fc_ValidaFechaIniFechaFin(fechaIni,fechaFin)==0){
			if(document.getElementById("txhFechaIni").value !==""){
					if(document.getElementById("txhFechaFin").value !==""){
					document.getElementById("txhOperacion").value = "MODIFICA";
					document.getElementById("frmMain").submit();
					 }
					 else{
					 alert('Debe ingresar una fecha de fin');
					 return false;
					 }
					}
					else{
					alert('Debe ingresar una fecha inicial');
					return false;				
					}
					}
				else{
				 	alert('La fecha de inicio debe ser menor a la fecha final.');
				 	}
				}
			else{
			alert('Debe ingresar una empresa');
			return false;
			}
			document.getElementById("txhEmpresa").value = "";
		
		}
		
		function Fc_VER(){
			strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>";
			if ( strRuta != "" )
			{ 
				if ( fc_Trim(document.getElementById("txhVer").value) != ""){
					//alert(">>"+strRuta + document.getElementById("txhVer").value+"<<");
					window.open(strRuta + document.getElementById("txhVer").value);
				}
				else alert('No cuenta con documentos');
			}
		}
		*/
		
		function fc_Inc1(){
			if(document.getElementById("txhIndDomingo").value =="")
				document.getElementById("txhIndDomingo").value = "1";
			else
				document.getElementById("txhIndDomingo").value = "";			
		}
		
		function fc_Inc(){
			if(document.getElementById("txhIndSabado").value =="")
				document.getElementById("txhIndSabado").value = "1";				
			else
				document.getElementById("txhIndSabado").value = "";	
		}
		
	</script>
	</head>

	<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_registrar_empresa_masivo.html">
		<form:hidden path="tipo" id="txhTipo"/>
		<form:hidden path="operacion" id="txhOperacion"/>	
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
		<form:hidden path="codEva" id="txhCodEva"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="men" id="txhMen"/>
		<form:hidden path="mens" id="txhMens"/>
		<form:hidden path="flag" id="txhFlag"/>
		<form:hidden path="codFlagApto" id="txhFlagApto"/>	
		<form:hidden path="codFlagNoApto" id="txhFlagNoApto"/>
		<form:hidden path="sab" id="txhSab"/>	
		<form:hidden path="totemp" id="txhTotemp"/>	
		<!-- JHPR 2008-06-06 -->
		<form:hidden path="codAlumnosSelec" id="idCodAlumnosSelec"/>
	 	<form:hidden path="tipoIngreso" id="tipoIngreso"/> <!-- Tipo de Ingreso: I=inserci�n, A=actualizaci�n -->	 	
	 	<form:hidden path="tipoIngreso2" id="tipoIngreso2"/>
	 	<form:hidden path="tipoIngreso3" id="tipoIngreso3"/>
	 	
	 	<form:hidden path="f1NomEmpresa" id="f1NomEmpresa"/>
	 	<form:hidden path="f1FechaIni" id="f1FechaIni"/>
	 	<form:hidden path="f1FechaFin" id="f1FechaFin"/>
	 	<form:hidden path="f1Obs" id="f1Obs"/>
	 	<form:hidden path="f1CantHrs" id="f1CantHrs"/>
	 	
	 	<form:hidden path="f2NomEmpresa" id="f2NomEmpresa"/>
	 	<form:hidden path="f2Cantidad" id="f2Cantidad"/>
	 	
	 	<form:hidden path="f3NomEmpresa" id="f3NomEmpresa"/>
	 	<form:hidden path="f3FechaIni" id="f3FechaIni"/>
	 	<form:hidden path="f3FechaFin" id="f3FechaFin"/>
	 	<form:hidden path="f3HoraIni" id="f3HoraIni"/>
	 	<form:hidden path="f3HoraFin" id="f3HoraFin"/>
	 	<form:hidden path="f3Nota" id="f3Nota"/>
	 	<form:hidden path="f3Cantidad" id="f3Cantidad"/>
					 	
	 	<form:hidden path="ids" id="ids"/>
	 	<form:hidden path="ids2" id="ids2"/>
	 	<form:hidden path="ids3" id="ids3"/>
	 
		<form:hidden path="estado" id="txhEstado"/>
		<form:hidden path="indDomingo" id="txhIndDomingo"/>
		<form:hidden path="indSabado" id="txhIndSabado"/>	 
		
		<table cellpadding="0" cellspacing="0" border="0" style="margin-left:9px;margin-top:7px">
			<tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="745px" class="opc_combo"><font style="">Formaci�n Empresa</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			</tr>
		</table>
		<table style="width:97%;margin-top:6px; margin-left:9px" cellspacing="2" cellpadding="0" bgcolor="#FFFFFF">
		  <tr>
			  <td colspan="3" class="cabecera">Alumnos seleccionados:</td>
		  </tr>
			<tr>
			  <td colspan="3">
			  	
			  	<div style="overflow: auto; height: 150px;width:50%;">
			  	
				  	<table class="bordegrilla" width="95%" border="0" cellpadding="1" cellspacing="0">
	                <tr>
	                  <td class="headtabla" width="25%" align="center">C&oacute;digo </td>
	                  <td class="headtabla" width="70%">Nombre</td>
	                  <td class="headtabla" width="3%">&nbsp;</td>
	                  <td class="headtabla" width="2%">&nbsp;</td>
	                </tr>                
	                <c:forEach var="alumno" items="${control.alumnosSeleccionados}" varStatus="loop">
	                	  <c:choose>
	                		<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
	                		<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
	                	  </c:choose>	                
		                  <td align="center"><c:out value="${alumno.codAlumno}"></c:out> </td>
		                  <td><c:out value="${alumno.nombreAlumno}"></c:out></td>
		                  <td><input type="hidden" id="txt<c:out value="${loop.index}"/>" value="<c:out value="${alumno.codAlumno}"/>"></td>
		                  <td>&nbsp;</td>
		                </tr>
	                </c:forEach>               
	              </table>
              
              </div>
              
              </td>
		  </tr>
		</table>
		<br>
		
		
		<table cellpadding="0" cellspacing="0" width="97%" border ="0" style="margin-left:9px">
		<tr>
			<td class="Tab_off" width="33%" onclick="javascript:fc_Aparece('0','PracticaInicial');" id="PracticaInicial">&nbsp;<b>Pr�ctica Inicial<b></td>
			<td class="Tab_off" width="33%" onclick="javascript:fc_Aparece('1','Pasantia');" id="Pasantia">&nbsp;<b>Pasant�a<b></td>
			<td class="Tab_off" width="33%" onclick="javascript:fc_Aparece('2','PracticaPre');" id="PracticaPre">&nbsp;<b>Pr�ctica Preprofesional<b></td>
		</tr>
		</table>
		
		<table class="tablaflotante" border="0" style="display:none;width:97%; margin-left:9px" 
			id="tbl_PracticaInicial" name="tbl_PracticaInicial" cellpadding="2" cellspacing="2" >
			<tr>
			<td>&nbsp;Empresa
			</td>
			<tr>
			<tr>
				<td width="14%">&nbsp;Nombre Empresa :&nbsp;&nbsp;</td>
				<td>
				<form:input path="empresa" id="txhEmpresa" 					
					onkeypress="fc_ValidaTextoNumeroEspecial();"	
					onblur="fc_ValidaNumeroLetrasGuionOnblur('txhEmpresa','empresa');" 			
					cssClass="cajatexto" size="50"/>
				</td>
				<td>&nbsp;</td>
				<td class="" align="right">&nbsp;Per�odo :&nbsp;&nbsp;</td>
				<td align="left">
				<form:input path="fechaInicio" id="txhFechaIni" onkeypress="fc_ValidaFecha('txhFechaIni');"
					onblur="fc_ValidaFechaOnblur('txhFechaIni');" cssClass="cajatexto" size="10" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" style='cursor:hand;' align="absmiddle">
					</a> - 
					<form:input path="fechaFin" id="txhFechaFin" onkeypress="fc_ValidaFecha('txhFechaFin');" 
						onblur="fc_ValidaFechaOnblur('txhFechaFin');" cssClass="cajatexto" size="10" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecFin" alt="Calendario" style='cursor:hand;' align="absmiddle">
					</a> - &nbsp;&nbsp;(DD-MM-AAAA)
				</td>
			</tr>
			<tr>
				<td class="" width="14%">&nbsp;Cant. Horas:&nbsp;&nbsp;</td>
				<td>
					<form:input path="caHoras" id="txhCHoras" onkeypress="fc_PermiteNumeros();"
						onblur="fc_ValidaNumeroOnBlur('txhCHoras');" cssClass="cajatexto" size="10" /> 
				</td>
				<td>&nbsp;</td>
				<td align="right">&nbsp;Observaciones:&nbsp;&nbsp;</td>
				<td>
					<form:input path="observacion" id="txhObservacion" 						
						onkeypress="fc_ValidaTextoNumeroEspecial();"	
						onblur="fc_ValidaNumeroLetrasGuionOnblur('txhObservacion','observaciones');"						
						cssClass="cajatexto" size="40" />
				</td>		
			</tr>
		</table>
		

		
		
		<!-- PASANT�A -->
		<table class="tablaflotante" border="0" style="display:none;width:97%; margin-left: 9px" 
			id="tbl_Pasantia" name="tbl_Pasantia" cellpadding="2" cellspacing="2" >
		<tr>
			<td width="14%">&nbsp;Nombre Empresa :&nbsp;&nbsp;</td>
			<td>
				<form:input path="empresa1" id="txhEmpresa1" 					
					onkeypress="fc_ValidaTextoNumeroEspecial();"	
					onblur="fc_ValidaNumeroLetrasGuionOnblur('txhEmpresa1','empresa');" 			
					cssClass="cajatexto" size="50" />
			</td>
		</tr>
		<tr>
			<td width="14%">&nbsp;Cant. Horas :&nbsp;&nbsp;</td>
			<td>
				<form:input path="cant" id="txhCant" onkeypress="fc_PermiteNumeros();" onblur="fc_ValidaNumeroOnBlur('txhCant');" cssClass="cajatexto" size="10" maxlength="3"/>
			</td>
			<td width="14%">&nbsp;Estado :&nbsp;&nbsp;</td>
			<td>
																   
			  <input Type="radio" id="rdoEtapa" name="rdoEtapa" name="gbradio" onclick="javascript:fc_Etapa('1');" <c:if test="${control.estado=='0001'}"><c:out value=" checked=checked " /></c:if> value="${status.value}" >Apto&nbsp;&nbsp;&nbsp;&nbsp;
			  <input Type="radio" id="rdoEtapa1" name="rdoEtapa" name="gbradio" onclick="javascript:fc_Etapa('2');" <c:if test="${control.estado=='0002'}"><c:out value=" checked=checked " /></c:if> value="${status.value}">No Apto
			</td>
		</tr>
		</table>
		
		
		
		<!-- PRACTICA PRE-PROFESIONAL -->
		<table class="tablaflotante" style="display:none;width:97%; margin-left: 9px" 
			id="tbl_PracticaPre" cellpadding="2" cellspacing="2">
		<tr>
			<td>&nbsp;Nombre Empresa:&nbsp;&nbsp;</td> 
			<td>
			<form:input path="empresa2" id="txhEmpresa2" 			 			
				onkeypress="fc_ValidaTextoNumeroEspecial();"	
				onblur="fc_ValidaNumeroLetrasGuionOnblur('txhEmpresa2','empresa');" 			
				cssClass="cajatexto" size="40" />
			</td>
			<td>Per�odo:&nbsp;&nbsp;</td> 
			<td align="left">
				<form:input path="fechaInicio1" id="txhFechaInicio1" onkeypress="fc_ValidaFecha('txhFechaInicio1');"
					onblur="fc_ValidaFechaOnblur('txhFechaInicio1');" cssClass="cajatexto" size="6" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni1','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni1" alt="Calendario" style='cursor:hand;' align="absmiddle">
					</a> -
				<form:input path="fechaFin1" id="txhFechaFin1" onkeypress="fc_ValidaFecha('txhFechaFin1');"
					onblur="fc_ValidaFechaOnblur('txhFechaFin1');" cssClass="cajatexto" size="6" maxlength="10"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin1','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin1" alt="Calendario" style='cursor:hand;' align="absmiddle">
					</a> -
					&nbsp;&nbsp;(DD-MM-AAAA)
			</td>
			<td>Hora Ini:&nbsp; 
			<form:input path="horaIni" id="txhHoraIni"  
					onkeypress="fc_ValidaHora('txhHoraIni');"
					onblur="fc_ValidaHoraOnBlur('frmMain','txhHoraIni','hora inicial');" 
					cssClass="cajatexto" size="3" maxlength="5"/>&nbsp;Hora Fin:&nbsp;
			<form:input path="horaFin" id="txhHoraFin"
					onkeypress="fc_ValidaHora('txhHoraFin');"
					onblur="fc_ValidaHoraOnBlur('frmMain','txhHoraFin','hora final');" 
					cssClass="cajatexto" size="3" maxlength="5"/>
			</td>
		</tr>     
		<tr>
			<td class="" width="14%">&nbsp;Cant. Horas :&nbsp;&nbsp;</td>
			<td>
			<form:input path="cant1" id="txhCant1"  
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur('txhCant1');" 
					cssClass="cajatexto" size="30" maxlength="3"/>

			</td>
			<td class="" width="7%">&nbsp;Nota :&nbsp;&nbsp;</td>
			<td>
			<form:input path="nota" id="txhNota"
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroOnBlur('txhCant1');" 
					cssClass="cajatexto" size="26" maxlength="3"/>

			</td>
			<td>
			  <input Type="checkbox" id="rdoSab" name="rdoSab" name="gbradio" onclick="javascript:fc_Inc();" <c:if test="${control.indSabado=='1'}"><c:out value=" checked=checked " /></c:if> value="${status.value}">Inc. Sab.&nbsp;&nbsp;&nbsp;&nbsp;
			  <input Type="checkbox" id="rdoDom" name="rdoDom" name="gbradio1" onclick="javascript:fc_Inc1();" <c:if test="${control.indDomingo=='1'}"><c:out value=" checked=checked " /></c:if> value="${status.value}">Inc. Dom.
				        
			</td>
		</table>
		
		
		<table><tr height="5px"><td></td></tr></table>
		<table align="center" id="tbl_Grabar" name="tbl_Grabar" style="display:none;height:30px;width:98%" cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN" align="center" height="30px" width="98%">	
			<tr>
				<td align="center">
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="fc_Grabar();" style="cursor:pointer;">
				</a>
			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgretrocede','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgretrocede" onclick="fc_Regresar();" style="cursor:pointer;"></a>													
				</td>
			</tr>
		</table>
	</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txhFechaIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	
	Calendar.setup({
		inputField     :    "txhFechaFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaInicio1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni1",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txhFechaFin1",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin1",
		singleClick    :    true
	});
</script>