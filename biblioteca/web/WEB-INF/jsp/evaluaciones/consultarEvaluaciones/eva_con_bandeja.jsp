<%@ include file="/taglibs.jsp"%>
<head>
<script language="javascript">

function onLoad(){

}
function buscarAlumno(){
	Fc_Popup("${ctx}/inicio/buscar_alumno.html?tipoRetorno=CYN&ctrlUSE=&ctrlCOD=codAlumno&ctrlNOM=txtAlumno&ventanaOrigen=eva_con_bandeja",600,400);
}

function actualizarCabecera(){
	//form = document.forms[0];
	//form.operacion.value="CABECERA";
	//form.submit();	
	$('operacion').value='CABECERA';
	$('frmMain').submit();
}

function fc_Regresar(){
	window.location.href = "${ctx}/menuEvaluaciones.html";
}

function fc_Regresar1(){
	var url = "${ctx}/evaluaciones/bandeja_alumnos_tutor.html" +
				"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoTutor').value + 
				"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value;
	window.location.href = url; 
}

function fc_cambiaProducto(){
	//form = document.forms[0];
	//form.cboPeriodo.value="-1"
	$('cboPeriodo').value='-1';
	$('operacion').value='irCambiaProducto';
	//form.operacion.value="irCambiaProducto";
	$('frmMain').submit();
	//form.submit();
}

function fc_cambiaPeriodo(){
	
	form = document.forms[0];
	if(form.cboProducto.value!="-1")
	{	//if(form.cboPeriodo.value=="T")
		//form.cboPeriodo.value="";		
		form.operacion.value="irCambiaPeriodo";
		form.submit();
	}else
		alert('Se deben llenar todos los parametros obligatorios');	
}

function fc_cambiaCiclo(){	
	
	form = document.forms[0];
	if(form.cboProducto.value!="-1"){
		if(form.cboCiclo.value=="T")
			form.cboCiclo.value="";
		
		//form.operacion.value="irCambiaCiclo";
		$('operacion').value='irCambiaCiclo';
		$('frmMain').submit();
		//form.submit();
	}else
		alert('Se deben llenar todos los parametros obligatorios');	
}

function fc_selecciona(cod,ciclo){
	document.getElementById("txhSel").value = cod;
	document.getElementById("txhCiclo").value = ciclo;
}

function fc_irDetalle(){
	codCurso = document.getElementById("txhSel").value
	codPeriodo = $F('cboPeriodo');
	codProducto = document.getElementById("cboProducto").value 
	codAlumno = document.getElementById("codAlumno").value
	codCiclo = document.getElementById("txhCiclo").value
	codEval = document.getElementById("txhCodEvaluador").value	
	codCboPeriodo = $F('cboPeriodo');
	
	if( codCurso!="" ){
		url = "${ctx}/evaluaciones/consultarEvaluacionesDetalle.html?"
		+"codCurso=" + codCurso
		+"&codPeriodo=" + codPeriodo
		+"&codProducto=" + codProducto
		+"&codAlumno=" + codAlumno
		+"&codCiclo=" + codCiclo
		+"&txhCodEvaluador=" + codEval
		+"&codCboPeriodo=" + codCboPeriodo
        +"&txhIndProcTutor=" + document.getElementById("txhIndProcedencia").value
        +"&txhCodPeriodoTutor=" + document.getElementById('txhCodPeriodoTutor').value;        
		window.location.href=url;
	}else
		alert(mstrSeleccione);
		
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" name="frmMain" action="${ctx}/evaluaciones/consultarEvaluaciones.html">

	<form:hidden path="operacion" id="operacion" />	
	<form:hidden path="codAlumno" id="codAlumno" />
	<form:hidden path="codPeriodo" id="txhCodPeriodo" />
	<form:hidden path="codEvaluador" id="txhCodEvaluador" />
	<form:hidden path="codEspecialidad" id="codEspecialidad" />
	<form:hidden path="flgBuscarAlumnos" id="flgBuscarAlumnos"/>
	<form:hidden path="indProcedencia" id="txhIndProcedencia" />
	<form:hidden path="codPeriodoTutor" id="txhCodPeriodoTutor" />
	
	<input type="hidden" id="txhSel" />
	<input type="hidden" id="txhCiclo" />
	<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="97%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif"
					<%if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("0") ) {%>
					onclick="javascript:fc_Regresar();"
					<%}else{ %>
					onclick="javascript:fc_Regresar1();"
					<%} %> 
					style="cursor: hand" alt="Regresar">
				</td>
			</tr>
		</table>
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" class="borde_tabla" style="margin-left:9px; background-color: #FFFFFF;" width="97%">
		 <tr class="fondo_cabecera_azul">
		 
		 	<td class="titulo_cabecera" width="100%">
				Informaci�n General
			</td>
		 	
		 </tr>
	 </table>
	
			<table class="tabla" style="width: 97%; margin-top: 0px;margin-left:9px"
				cellspacing="2" cellpadding="0" border="0" 
				background="${ctx}/images/Evaluaciones/back-sup.jpg">
				<tr>
					<td class="">&nbsp;Alumno :</td>
					<td align="left">
						<table width="200px" border="0">
						<tr>
							<td align="left">
								<form:input path="txtAlumno" id="txtAlumno" cssClass="cajatexto_1" cssStyle="width:250px" readonly="true" />
							</td>
							<td>
							
								
								<c:if test="${control.flgBuscarAlumnos=='1'}">								
								<!-- 1= procedencia tutor -->
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerarX','','${ctx}/images/iconos/buscara2.jpg',1)">
									<img src="${ctx}/images/iconos/buscara1.jpg" onclick="javascript:buscarAlumno();" alt="Buscar Alumno" style="cursor:hand;" id="btnGenerarX" width="20" height="17" >
								</a>
								</c:if>
							</td>
						</tr>
						</table>											
					</td>
					<td class="">&nbsp;Promedio Acumulado</td>
					<td align="left">&nbsp;<form:input path="txtPromAcumulado"
						id="txtPromAcumulado" cssClass="cajatexto_1"
						cssStyle="width:40px" readonly="true" /></td>
				</tr>
				<tr>
					<td class="">&nbsp;Ranking Acad�mico :</td>
					<td align="left">&nbsp;<form:input path="txtRankinNro"
						id="txtRankinNro" cssClass="cajatexto_1"
						cssStyle="width:40px" readonly="true" />
					&nbsp;&nbsp; <form:input path="txtRankinDes" id="txtRankinDes"
						cssClass="cajatexto_1" cssStyle="width:110px"
						readonly="true" /></td>
					<td class="texto_bold">&nbsp;</td>
					<td align="left">&nbsp;</td>
				</tr>
				<tr>
					<td align="left" colspan=4 height="15">&nbsp;</td>
				</tr>
				<tr>
					<td class="">&nbsp;Producto :</td>
					<td align="left">&nbsp;<form:select path="cboProducto"
						id="cboProducto" cssClass="Combo"
						onchange="fc_cambiaProducto()" cssStyle="width:250px">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.lstProductos!=null}">
							<form:options itemValue="codProducto" itemLabel="desProducto"
								items="${control.lstProductos}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
					        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
					<td class="">&nbsp;Especialidad :</td>
					<td align="left">&nbsp;<form:input path="txtEspecialidad"
						id="txtEspecialidad" cssClass="cajatexto_1"
						cssStyle="width:300px" readonly="true" /></td>
				</tr>
				<tr>
					<td>&nbsp;Per�odo :</td>
					<td>&nbsp;<form:select path="cboPeriodo" id="cboPeriodo" 
						cssClass="Combo" cssStyle="width:250px"
						onchange="fc_cambiaPeriodo()">
						<form:option value="-1">--Seleccione--</form:option>												
						<c:if test="${control.lstPeriodos!=null}">
							<form:options itemValue="codigo" itemLabel="nombre"
								items="${control.lstPeriodos}" />
						</c:if>
					</form:select></td>
				</tr>
			</table>
			<!--/td>
		</tr>
		<tr style="height: 10px;"><td></td></tr>
		<tr style="height: 20px;">
			<td-->
				<!-- Titulo de la bandeja -->
				 <table cellpadding="0" class="borde_tabla" cellspacing="0" width="97%" style="margin-left:9px;margin-top:6px">
					 <tr class="fondo_cabecera_azul">
		 
						 	<td class="titulo_cabecera" width="100%">
								Relaci�n de Cursos
							</td>
						 	
						 </tr>

				 </table>
			<!--/td>
		</tr-->
		<div style="overflow: auto; height: 300px; width:100%">
		<c:forEach var="objGlob" items="${control.lstResultado}" >
		
		<!--tr>
			<td-->
			<!-- TABLA GLOBAL -->
			
			&nbsp;&nbsp;&nbsp;<span class="texto_bold" >CICLO&nbsp;:<c:out value="${objGlob.nomCiclo}" /></span>
			
			<!-- div style="overflow: auto; height: 150px; width: WIDTH: 97%; margin-left:9px; margin-top: 0px; margin-bottom: 5px;"  -->
				<TABLE class="" 
					style="width: 1470px; border: 1px solid #048BBA; margin-left:9px; margin-top: 0px; margin-bottom: 5px;" cellSpacing="1" cellPadding="0">
					<TR height="20px">
						<TD class="headtabla" width="30px" align="center">&nbsp;Sel.</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;Periodo</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;C�digo</TD>
						<TD class="headtabla" width="200px" align="center">&nbsp;Curso</TD>
						<TD class="headtabla" width="70px" align="center">&nbsp;Sist. Eval.</TD>
						<TD class="headtabla" width="100px" align="center">&nbsp;Profesor</TD>
						<TD class="headtabla" width="70px" align="center">&nbsp;Secci�n</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;Promedio</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;Nro. Veces</TD>
						<!-- ccordova 2008/05/21 -->
						<TD class="headtabla" width="80px" align="center">&nbsp;Examen 1</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;Examen 2</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;Inasistencia<br>Teoria</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;Inasistencia<br>Laboratorio</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;Inasistencia<br>Taller</TD>
						<TD class="headtabla" width="80px" align="center">&nbsp;% Inasistencia</TD>
						<TD class="headtabla" width="60px" align="center">&nbsp;Nota Recup.</TD>
						<TD class="headtabla" width="60px" align="center">&nbsp;Prom. Recup.</TD>						
					</TR>
					<!-- GRILLA -->
								
					<c:forEach var="objCast" items="${objGlob.lstCursos}"
						varStatus="loop">
							<c:choose>
								<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
								<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
							</c:choose>
							<TD align="center"><INPUT type="radio"
								ID="Check" NAME="Check"
								onclick="fc_selecciona('<c:out value="${objCast.codCurso}" />','<c:out value="${objCast.codCiclo}" />')"></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.desPeriodo}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.codCurso}" /></TD>
							<TD >&nbsp;<c:out
								value="${objCast.desCurso}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.desSistEval}" /></TD>
							<TD >&nbsp;<c:out
								value="${objCast.nomEvaluador}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.desSeccion}" /></TD>
							<TD style="text-align:right">&nbsp;<c:out value="${objCast.promedio}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.nroVeces}" /></TD>
							<!-- ccordova 2008/05/21 -->
							<TD align="center">&nbsp;<c:out
								value="${objCast.exam1}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.exam2}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.inasistenciaTeo}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.inasistenciaLab}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.inasistenciaTal}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.inasistenciaPorcentaje}" /></TD>
							
							<TD align="center">&nbsp;<c:out
								value="${objCast.notaRecuperacion}" /></TD>
							<TD align="center">&nbsp;<c:out
								value="${objCast.promedioRecuperacion}" /></TD>								
								
						</tr>
					</c:forEach>
				</TABLE>
			<!-- /div -->
			<!--/td>
		</tr>
		<tr>
			<td-->
			<table class="tabla" style="width: 97%; margin-left:9px;margin-top: 6px;border: 1px solid #048BBA"
				cellspacing="2" cellpadding="0" bgcolor="#f2f2f2">
				<tr>
					<td class="texto_bold">Promedio del Ciclo</td>
					<td align="left">
					<input type="text" 
						 class="cajatexto_1"
						 style="width:40px; text-align:right"  readonly="readonly" 
						  value="<c:out value="${objGlob.promCiclo}" />"/>							
						</td>
					<td class="texto_bold" align="right">Ranking del Ciclo</td>
					<td align="center">					
					<input type="text"  class="cajatexto_1"
						 style="width:40px"  readonly="readonly"
						 value="<c:out value="${objGlob.nroRankin}" />"
						  />	
						 &nbsp;&nbsp;					
						<input type="text"  class="cajatexto_1"
						 style="width:100px" readonly="readonly"						
						 value="<c:out value="${objGlob.nomRankin}" />"/>						
						</td>
				</tr>
			</table><br><br>
	</c:forEach>
	<!-- TABLA GLOBAL -->
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN"
		align="center" height="30px" width="90%">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgconsultardetalle','','${ctx}/images/botones/consdetallecurso2.jpg',1)">
				<img src="${ctx}/images/botones/consdetallecurso1.jpg" id="imgconsultardetalle" alt="Consultar Detalle Curso" onclick="fc_irDetalle()" style="cursor:pointer;"></a></td>
		</tr>
	</table>
</form:form>
</body>

