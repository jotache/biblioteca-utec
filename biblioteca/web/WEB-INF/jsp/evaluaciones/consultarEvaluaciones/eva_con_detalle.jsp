<%@ include file="/taglibs.jsp"%>
<head>
<script type="text/javascript">

function onLoad(){
}

function fc_atras(){
	form = document.forms[0];
	a = form.cboProducto.value;
	b = form.cboCiclo.value;
	c = form.codAlumno.value;
	d = form.codEvaluador.value;
	e = form.codPeriodo.value;
	f = form.cboPeriodo.value;
	
	url="${ctx}/evaluaciones/consultarEvaluaciones.html"
	+"?codProducto="+a
	+"&codCiclo="+b
	+"&irRegresar=true"
	+"&codAlu="+c
	+"&txhCodEvaluador="+d
	+"&txhCodPeriodo="+e
	+"&codCboPeriodo="+f
	+"&txhIndProcTutor="+document.getElementById("txhIndProcedencia").value
	+"&txhCodPeriodoTutor="+document.getElementById('txhCodPeriodoTutor').value
	;
	window.location.href = url;
	
}

function fc_IncidenciaDoc()
{
	var url = "${ctx}/evaluaciones/registrar_incidencia_doc.html" + 
							"?txhCodAlumno=" + document.getElementById('codAlumno').value + 
							"&txhCodCurso=" + document.getElementById('txhCodCursoTutor').value +
							"&txhNomAlumno=" + document.getElementById('txtAlumno').value +
							"&txhCodPeriodo=" + document.getElementById('cboPeriodo').value +
							"&txhCodEvaluador=" + document.getElementById('codEvaluador').value +
							"&txhCodProducto=" + document.getElementById('cboProducto').value +
							"&txhCodEspecialidad=" +
							"&txhCodTipoSesion=&txhCodSeccion=" +
							"&txtEvaluador=" +
							"&txhCiclo=" + document.getElementById("txtCiclo").value+
					        "&txhIndProcedencia=" + document.getElementById("txhIndProcedencia").value+
					        "&txhCodPeriodoTutor=" + document.getElementById('txhCodPeriodoTutor').value;

	window.location.href = url;
}

function fc_IncidenciaAdm()
{
	var url = "${ctx}/evaluaciones/registrar_incidencia_adm.html" + 
							"?txhCodAlumno=" + document.getElementById('codAlumno').value + 
							"&txhCodCurso=" + document.getElementById('txhCodCursoTutor').value +
							"&txhNomAlumno=" + document.getElementById('txtAlumno').value +
							"&txhCodPeriodo=" + document.getElementById('cboPeriodo').value +
							"&txhCodEvaluador=" + document.getElementById('codEvaluador').value +
							"&txhCodProducto=" + document.getElementById('cboProducto').value +
							"&txhCodEspecialidad=" + 
							"&txhCodTipoSesion=" + 
							"&txhCodSeccion=" + 
							"&txhCiclo=" + document.getElementById("txtCiclo").value+
					        "&txhIndProcedencia=" + document.getElementById("txhIndProcedencia").value+
					        "&txhCodPeriodoTutor=" + document.getElementById('txhCodPeriodoTutor').value;

	window.location.href = url;
}

</script>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">
<form:form commandName="control" action="${ctx}/evaluaciones/consultarEvaluacionesDetalle.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="cboProducto" id="cboProducto" />
	<form:hidden path="cboCiclo" id="cboCiclo" />
	<form:hidden path="codAlumno" id="codAlumno" />
	<form:hidden path="codEvaluador" id="codEvaluador" />
	<form:hidden path="codPeriodo" id="codPeriodo" />
	
	<form:hidden path="cboPeriodo" id="cboPeriodo" />	
	
	<form:hidden path="indProcedencia" id="txhIndProcedencia" />
	<form:hidden path="codPeriodoTutor" id="txhCodPeriodoTutor" />
	<form:hidden path="codCursoTutor" id="txhCodCursoTutor" />
	
	<table cellpadding="0" cellspacing="0" class="borde_tabla" width="98%" style="margin-left:11px;margin-top: 5px;">
		 <tr class="fondo_cabecera_azul">
		 	<td class="titulo_cabecera" width="100%">
				Detalle del Curso
			</td>
		 </tr>
	</table>
	
	<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" align="center" width="98%">
		<tr>
			<td>
			<table class="tabla" style="width: 100%; margin-top: 0px"
				cellspacing="2" cellpadding="0" border="0" 
				align="center" background="${ctx}/images/Evaluaciones/back-sup.jpg">
				<tr>
					<td>&nbsp;Alumno :</td>
					<td align="left"><form:input path="txtAlumno"
						id="txtAlumno" cssClass="cajatexto_1" cssStyle="width:250px"
						readonly="true" /></td>
					<td>&nbsp;Departamento :</td>
					<td align="left"><form:input path="txtEspecialidad"
						id="txtEspecialidad" cssClass="cajatexto_1" cssStyle="width:300px"
						readonly="true" /></td>
				</tr>				
				<tr>
					<td>&nbsp;Producto :</td>
					<td align="left"><form:input path="txtProducto"
						id="txtProducto" cssClass="cajatexto_1" cssStyle="width:250px"
						readonly="true" /></td>
					<td>&nbsp;Curso :</td>
					<td align="left"><form:input path="txtCurso" id="txtCurso"
						cssClass="cajatexto_1" cssStyle="width:250px" readonly="true" /></td>
				</tr>


				<tr>
					<td>&nbsp;Ciclo :</td>
					<td><form:input path="txtCiclo" id="txtCiclo"
						cssClass="cajatexto_1" cssStyle="width:40px" readonly="true" /></td>
					<td>&nbsp;Promedio :</td>
					<td align="left"><form:input path="txtPromedio"
						id="txtPromedio" cssClass="cajatexto_1" cssStyle="width:50px"
						readonly="true" /></td>
				</tr>
				
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<TABLE class=""
				style="WIDTH: 100%; margin-top: 0px; margin-bottom: 5px;border: 1px solid #048BBA"
				cellSpacing="0" cellPadding="0" align="center" border="1"
				borderColor="white" bgcolor="#f2f2f2">
				<tr>
					<td WIDTH=20% valign="top">
					<TABLE align="center">
						<tr style="height: 20px;">
							<td class="tablagrilla" align="center"><b>Asistencia</b></td>
						</tr>
						<tr>
							<td>
							<TABLE class="" cellSpacing="0" cellPadding="0"
								align="center" style="border: 1px solid #048BBA;">
								<TR height="20px">
									<TD class="grilla" width="40%">&nbsp;&nbsp;Fecha&nbsp;&nbsp;</TD>
									<TD class="grilla" width="40%">&nbsp;&nbsp;Asistencia&nbsp;&nbsp;</TD>
								</TR>


								<c:forEach var="objCast" items="${control.lstAsistencia}"
									varStatus="loop">
									<tr>

										<TD class="tablagrilla" align="center">&nbsp;&nbsp;<c:out
											value="${objCast.fecha}" />&nbsp;&nbsp;</TD>
										<TD class="tablagrilla" align="center"><c:if
											test="${objCast.asistencia=='1'}">
											<INPUT type="checkbox" ID="Check" NAME="Check"
												checked="checked" disabled="disabled">&nbsp;A
											</c:if> <c:if test="${objCast.asistencia=='2'}">
											<INPUT type="checkbox" ID="Check" NAME="Check" CHECKED
												DISABLED>&nbsp;F
											</c:if> <c:if test="${objCast.asistencia=='3'}">
											<INPUT type="checkbox" ID="Check" NAME="Check" DISABLED>&nbsp;T
											</c:if></TD>


									</tr>
								</c:forEach>
							</TABLE>
							</td>
						</tr>
					</TABLE>
					</td>
					<td WIDTH=20% valign="top">
					<TABLE align="center">
						<tr style="height: 20px;">
							<td class="tablagrilla" align="center">&nbsp;&nbsp;<b>Prueba de
							Aula</b>&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td>
							<TABLE class="" cellSpacing="0" cellPadding="0"
								align="center" style="border: 1px solid #048BBA;">
								<TR height="20px">
									<TD class="grilla" width="40%">&nbsp;&nbsp;Nro.&nbsp;&nbsp;</TD>
									<TD class="grilla" width="40%">&nbsp;&nbsp;Nota&nbsp;&nbsp;</TD>
								</TR>
								<c:if test="${control.lstPruebaAula!=null}">
									<c:forEach var="objCast2" items="${control.lstPruebaAula}"
										varStatus="loop">
										<TR height="20px">
											<td class="tablagrilla" align="center">
<%-- 												<c:out value="${loop.index+1}" /> --%>
													<c:out value="${objCast2.nroEval}" />
											</td>
											<TD class="tablagrilla" align="center">
											
																				
												<c:choose>
													<c:when test="${objCast2.tipoNota=='0001'}">
														<c:set var="valorNotaPA" value="AN"></c:set>		
													</c:when>
													<c:when test="${objCast2.tipoNota=='0002'}">
														<c:set var="valorNotaPA" value="NP"></c:set>		
													</c:when>
													<c:when test="${objCast2.tipoNota=='0003'}">
														<c:set var="valorNotaPA" value="PE"></c:set>		
													</c:when>
													<c:otherwise>
														<c:set var="valorNotaPA" value="${objCast2.nota}"></c:set>
													</c:otherwise>
												</c:choose>
											
											<input
												name="color" class="cajatexto"
												style="HEIGHT: 18px; width: 30px" disabled
												value="<c:out value="${valorNotaPA}" />">&nbsp;&nbsp;</TD>
										</TR>
									</c:forEach>
									<%--
									<!-- JHPR:2008-04-15 Comentado a solicitud de la Sra. Augusta 
									<TR height="20px">
										<TD class="tablagrilla" align="center">Promedio</TD>
										<TD class="tablagrilla" align="center"><input
											name="color" class="cajatexto"
											style="HEIGHT: 18px; width: 30px" disabled
											value="<c:out value="${control.promedioAula}" />">&nbsp;&nbsp;</TD>
									</TR>
									-->
									--%>
								</c:if>

							</TABLE>
							</td>
						</tr>
					</TABLE>
					</td>
					<td WIDTH=20% valign="top">
					<TABLE align="center">
						<tr style="height: 20px;">
							<td class="tablagrilla" align="center">&nbsp;&nbsp;<b>Prueba de
							Laboratorio</b>&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td>
							<TABLE class="" cellSpacing="0" cellPadding="0"
								align="center" style="border: 1px solid #048BBA;">
								<TR height="20px">
									<TD class="grilla" width="40%">&nbsp;&nbsp;Nro.&nbsp;&nbsp;</TD>
									<TD class="grilla" width="40%">&nbsp;&nbsp;Nota&nbsp;&nbsp;</TD>
								</TR>
								<c:if test="${control.lstPruebaLaboratorio!=null}">
									<c:forEach var="objCast3"
										items="${control.lstPruebaLaboratorio}" varStatus="loop">
										<TR height="20px">
											<TD class="tablagrilla" align="center">
<%-- 												<c:out value="${loop.index+1}" /> --%>
												    <c:out value="${objCast3.nroEval}" />
											</TD>
											<TD class="tablagrilla" align="center">
											
												<c:choose>
													<c:when test="${objCast3.tipoNota=='0001'}">
														<c:set var="valorNotaPL" value="AN"></c:set>		
													</c:when>
													<c:when test="${objCast3.tipoNota=='0002'}">
														<c:set var="valorNotaPL" value="NP"></c:set>		
													</c:when>
													<c:when test="${objCast3.tipoNota=='0003'}">
														<c:set var="valorNotaPL" value="PE"></c:set>		
													</c:when>
													<c:otherwise>
														<c:set var="valorNotaPL" value="${objCast3.nota}"></c:set>
													</c:otherwise>
												</c:choose>
											
											
												<input
												name="color" class="cajatexto"
												style="HEIGHT: 18px; width: 30px" disabled
												value="<c:out value="${valorNotaPL}" />">&nbsp;&nbsp;</TD>
										</TR>
									</c:forEach>
									<%--
									<!-- JHPR:2008-04-15 Comentado a solicitud de la Sra. Augusta 
									<TR height="20px">
										<TD class="tablagrilla" align="center">Promedio</TD>
										<TD class="tablagrilla" align="center"><input
											name="color" class="cajatexto"
											style="HEIGHT: 18px; width: 30px" disabled
											value="<c:out value="${control.promedioLab}" />">&nbsp;&nbsp;</TD>
									</TR>
									-->
									--%>

								</c:if>
							</TABLE>
							</td>
						</tr>
					</TABLE>
					</td>
					<!--  -->
					<td WIDTH=20% valign="top">
					<TABLE align="center">
						<tr style="height: 20px;">
							<td class="tablagrilla" align="center">&nbsp;&nbsp;<b>Prueba de
							Taller</b>&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td>
							<TABLE class="" cellSpacing="0" cellPadding="0"
								align="center" style="border: 1px solid #048BBA;">
								<TR height="20px">
									<TD class="grilla" width="40%">&nbsp;&nbsp;Nro.&nbsp;&nbsp;</TD>
									<TD class="grilla" width="40%">&nbsp;&nbsp;Nota&nbsp;&nbsp;</TD>
								</TR>
								<c:if test="${control.lstPruebaTaller!=null}">
									<c:forEach var="objCast4"
										items="${control.lstPruebaTaller}" varStatus="loop">
										<TR height="20px">
											<TD class="tablagrilla" align="center">
<%-- 												<c:out value="${loop.index+1}" /> --%>
												<c:out value="${objCast4.nroEval}" />
											</TD>
											<TD class="tablagrilla" align="center">
																																			
												<c:choose>
													<c:when test="${objCast4.tipoNota=='0001'}">
														<c:set var="valorNotaPT" value="AN"></c:set>		
													</c:when>
													<c:when test="${objCast4.tipoNota=='0002'}">
														<c:set var="valorNotaPT" value="NP"></c:set>		
													</c:when>
													<c:when test="${objCast4.tipoNota=='0003'}">
														<c:set var="valorNotaPT" value="PE"></c:set>		
													</c:when>
													<c:otherwise>
														<c:set var="valorNotaPT" value="${objCast4.nota}"></c:set>
													</c:otherwise>
												</c:choose>
																								
												<input name="color" class="cajatexto" style="HEIGHT: 18px; width: 30px" disabled
												value="<c:out value="${valorNotaPT}" />">&nbsp;&nbsp;
											</TD>
										</TR>
									</c:forEach>
									<%--
									<!-- JHPR:2008-04-15 Comentado a solicitud de la Sra. Augusta 
									<TR height="20px">
										<TD class="tablagrilla" align="center">Promedio</TD>
										<TD class="tablagrilla" align="center"><input
											name="color" class="cajatexto"
											style="HEIGHT: 18px; width: 30px" disabled
											value="<c:out value="${control.promedioTaller}" />">&nbsp;&nbsp;</TD>
									</TR>
									-->
									--%>

								</c:if>
							</TABLE>
							</td>
						</tr>
					</TABLE>
					</td>
					<!--  -->
					
					<td WIDTH=20% valign="top">
					<TABLE align="center">
						<tr style="height: 20px;">
							<td class="tablagrilla" align="center">&nbsp;&nbsp;<b>Ex�menes</b>&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td>
							<TABLE class="" cellSpacing="0" cellPadding="0"
								align="center" style="border: 1px solid #048BBA;">
								<TR height="20px">
									<TD class="grilla" width="40%">&nbsp;&nbsp;Examen&nbsp;&nbsp;</TD>
									<TD class="grilla" width="40%">&nbsp;&nbsp;Nota&nbsp;&nbsp;</TD>
								</TR>

								<c:if test="${control.lstExamenes!=null}">
									<c:forEach var="objCast4" items="${control.lstExamenes}"
										varStatus="loop">
										<TR height="20px">
											<TD class="tablagrilla" align="left"><c:out
												value="${objCast4.descExamen}" /></TD>
											<TD class="tablagrilla" align="center">
											
												<c:set var="valorNotaEx" value="${objCast4.nota}"></c:set>												
												
												<c:if test="${objCast4.flgNP=='1'}">
													<c:set var="valorNotaEx" value="NP"></c:set>
												</c:if>
												<c:if test="${objCast4.flgAN=='1'}">
													<c:set var="valorNotaEx" value="AN"></c:set>
												</c:if>
												
												<input name="color" class="cajatexto" style="HEIGHT: 18px; width: 30px;text-align:center" disabled="disabled"
													value="<c:out value="${valorNotaEx}" />">&nbsp;&nbsp;
											</TD>
										</TR>
									</c:forEach>
									
								</c:if>

							</TABLE>
							</td>
						</tr>
					</TABLE>
					</td>
				</tr>
			</TABLE>
			</td>
		</tr>
	</table>
	
	<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" align="center" width="98%">
		<tr>
			<td>NP: No se present�; AN: Anulado; PE: Pendiente
			</td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN"
		align="center" height="30px" width="98%">
		<tr>
			<td align="center">
			<% if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("1") ){%>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgIncDoc','','${ctx}/images/botones/incidenciasdoc2.jpg',1)">
				<img alt="Incidencias Docencia" onclick="javascript:fc_IncidenciaDoc();" src="${ctx}/images/botones/incidenciasdoc1.jpg" style="cursor:pointer;" id="imgIncDoc"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgIncAdm','','${ctx}/images/botones/incidenciasadm2.jpg',1)">
				<img alt="Incidencias Administrativas" onclick="javascript:fc_IncidenciaAdm();" src="${ctx}/images/botones/incidenciasadm1.jpg" style="cursor:pointer;" id="imgIncAdm"></a>
			<% } %>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" onclick="javascript:fc_atras();" src="${ctx}/images/botones/regresar1.jpg" style="cursor:pointer;" id="imgregresar"></a>
			</td>
		</tr>
	</table>
</form:form>
</body>

