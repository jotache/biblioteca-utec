<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script language=javascript>
	function onLoad(){	
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" ){
			window.opener.document.getElementById("txhCodAlumno").value="";
			window.opener.fc_ActualizarCat();			
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
	}	
	function Fc_Grabar(){
		notaCat = document.getElementById("txtNotaCat");
		if (notaCat.value == "" || fc_Trim(notaCat.value) == "")
		{	document.getElementById("txtNotaCat").value="";
			alert(mstrLleneLosCampos);
			notaCat.focus();
			return;
		}
		else{		
			if(confirm(mstrSeguroGrabar)){			
				document.getElementById("frmMain").submit();
			}
		}		
	}
</script>
</head>
<body topmargin="0" leftmargin="10" rightmargin="0">
<form:form action="${ctx}/evaluaciones/eva_registro_nota_ciclo_externo.html" commandName="control" id="frmMain">	
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>	
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codProducto" id="txhCodProducto"/>	
	<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
	<form:hidden path="codEtapa" id="txhCodEtapa"/>
	<form:hidden path="codPrograma" id="txhCodPrograma"/>
	<form:hidden path="codCiclo" id="txhCodCiclo"/>	
	<form:hidden path="codCurso" id="txhCodCurso"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="550px" class="opc_combo">
		 	<font style="">
		 		<c:if test="${control.operacion=='GRABAR'}">
					Registrar Ciclo Externo
				</c:if>
				<c:if test="${control.operacion=='MODIFICAR'}">
					Modificar Ciclo Externo
				</c:if>		 	
		 	</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	</table>
	<table class="tablaflotante" style="width:98%;height:50px;margin-top:5px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray' align="center">	 
		<tr>
			<td width="30%">&nbsp;Alumno:</td>
			<td>
				<form:input path="alumno" id="txtAlumno"
				cssClass="cajatexto_1" cssStyle="width:250px" readonly="true"/>			
			</td>
		</tr>
		<tr>
			<td nowrap>&nbsp;Nota(%):</td>			
			<td>
				<form:input path="notaCat" id="txtNotaCat" 
				maxlength="3"
				onkeypress="fc_PermiteNumeros();"
				onblur="fc_ValidaNumeroFinal(this,'nota externo');fc_ValidaNumeroFinalMayorCero(this);" 
				cssClass="cajatexto_o" size="5"/>
			</td>
		</tr>
	</table>	
	<br>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:Fc_Grabar();" style="cursor:hand" id="imgAgregar" alt="Grabar">&nbsp;</a>	
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img  src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();"id="imgEliminar"></a>
			</td>
		</tr>
	</table>	
</form:form>
</body>
</html>