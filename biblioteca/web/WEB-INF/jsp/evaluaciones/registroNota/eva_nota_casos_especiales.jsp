<%@ include file="/taglibs.jsp"%>
<%@page import="com.tecsup.SGA.modelo.ConfiguracionNotaExterna"%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%><html>
<head>	
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" type="text/JavaScript"></script>	
	<script type="text/javascript">
	function onLoad(){	
						
		var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
		fc_msgManager(mensaje);
							
		var cat = "<%=((request.getAttribute("cat")==null)?"":(String)request.getAttribute("cat"))%>";		
		if(cat!=""){
			fc_Aparece2(cat,'Ciclo');
		}		
		fc_Producto("");					
	}	

	function fc_msgManager(mensaje){	
		if(mensaje!=""){
			if(mensaje.length>1)           		
			alert(mensaje);	    
		}
	}
	
	function fc_NP(fil){
		if(document.getElementById("txtNP" + fil).checked == true){
			document.getElementById("txt" + fil).value = "0";
			document.getElementById("lbl" + fil).value = "0";
			document.getElementById("txt" + fil).disabled = true;
		}else{
			document.getElementById("txt" + fil).value = "";
			document.getElementById("lbl" + fil).value = "";
			document.getElementById("txt" + fil).disabled = false;
			document.getElementById("txt" + fil).focus();			
		}
	}
	
	function Fc_RegistrarNotaExterno(){
		if ($("cboProducto").value==""){ //|| $("cboCicloPFR").value=="" || $("cboCursoPFR").value=="") {
			//if ($("cboProducto").value=="")
				alert("Seleccione un Producto");			
			//else if ($("cboCicloPFR").value=="")
				//alert("Seleccione un Ciclo");
			//else if ($("cboCursoPFR").value=="") 
				//alert("Seleccione un Curso");
		} else {	
			codEtapa="";
			if(confirm('�Seguro de grabar las notas?')){
				//--
				form = document.forms[0];
				var max = form.tamListAlumnos.value;
				var datos = "";
				var codigoAlu;
				var notaExterna;
				var notaTecsup;		
				var codPrograma ; 
				var codEspecialidad;
				var codEtapa;
				var id;	
				var flgnp; // flag de no se presento JHPR 2008-8-4
				var codcurso;
				var codciclo;
								
				if(max>0){
					for(i=0;i<max;i++){
						objCodigo 	= document.getElementById("val"+i);
						codigoAlu 	= objCodigo.value;					
						 
						if (fc_Trim(document.getElementById("txt"+i).value)=="") {
							notaExterna = "";
							notaTecsup = "";
						} else {
							notaExterna = document.getElementById("txt"+i).value;
							notaTecsup = document.getElementById("lbl"+i).value;
						}				
						codPrograma = document.getElementById("hidCodPrograma"+i).value;
						codEspecialidad = document.getElementById("hidCodEspecialidad"+i).value;
						codEtapa = document.getElementById("hidCodEtapa"+i).value;

						codcurso = document.getElementById("hidCodCurso"+i).value;
						codciclo = document.getElementById("hidCodCiclo"+i).value;
						
						id = document.getElementById("hidId"+i).value;
						if(document.getElementById('txtNP'+i).checked == true)
							flgnp = "1";
						else
							flgnp = "0";

						datos = datos + codigoAlu + "$" + notaExterna + "$" + notaTecsup + "$" + codPrograma + "$" + codEspecialidad + "$" + codEtapa + "$" + id + "$" + flgnp + "$" + codcurso + "$" + codciclo +  "|";
					}
								
// 					alert(datos);
					
					form.imgregistrar1.disabled=true;
					form.cadenaDatos.value = datos;
					form.txhOperacion.value = "GRABARNOTAEXTERNA";
					form.submit();				
				}
				//--
			}else{
				form.imgregistrar1.disabled=false;
			}
			
		}						
	}

	
	//JHPR: 2008-05-05 Calcular Nota Tecsup.
	//function calcularNotaTecsup(notaExterna) {
	function calcularNotaTecsup(codCursoEjec,notaExterna) {
		//obtengo lista de configuraciones de cursos externos...
		var notaTecsup = 0;
		<%
		//ConfiguracionNotaExterna configNotExt = null;
		List<ConfiguracionNotaExterna> listaConfig = new ArrayList<ConfiguracionNotaExterna>();
		//configNotExt = (request.getAttribute("config")==null?null:(ConfiguracionNotaExterna)request.getAttribute("config"));
		listaConfig = (request.getAttribute("config")==null?null:(List<ConfiguracionNotaExterna>)request.getAttribute("config"));
		if(listaConfig!=null){			
			if(listaConfig.size()>0){				 		
		%>
			var codCurso;
			<%
				for(ConfiguracionNotaExterna configNotExt:listaConfig){
					configNotExt.getCodCursoEjec();
			%>
				//trabajar con el curso externo correcto...
				codCurso="<%=(configNotExt.getCodCursoEjec())%>";
				if(codCurso==codCursoEjec){

					var regla = "<%=((configNotExt==null)?"":configNotExt.getRegla().trim())%>";
					var condicion = "<%=((configNotExt==null)?"":configNotExt.getCondicion().trim())%>";
					var verdadero1 = "<%=((configNotExt==null)?"":configNotExt.getVerdadero1().trim())%>";					
					var verdadero2 = "<%=((configNotExt==null)?"":configNotExt.getVerdadero2().trim())%>";
					var verdadero3 = "<%=((configNotExt==null)?"":configNotExt.getVerdadero3().trim())%>";
					var verdadero4 = "<%=((configNotExt==null)?"":configNotExt.getVerdadero4().trim())%>";
					var falso1 = "<%=((configNotExt==null)?"":configNotExt.getFalso1().trim())%>";
					var falso2 = "<%=((configNotExt==null)?"":configNotExt.getFalso2().trim())%>";
					var falso3 = "<%=((configNotExt==null)?"":configNotExt.getFalso3().trim())%>";
					var falso4 = "<%=((configNotExt==null)?"":configNotExt.getFalso4().trim())%>";
					var operVerdad1 = "<%=((configNotExt==null)?"":configNotExt.getOperadorVerdad1().trim())%>";
					var operVerdad2 = "<%=((configNotExt==null)?"":configNotExt.getOperadorVerdad2().trim())%>";
					var operVerdad3 = "<%=((configNotExt==null)?"":configNotExt.getOperadorVerdad3().trim())%>";
					var operVerdad4 = "<%=((configNotExt==null)?"":configNotExt.getOperadorVerdad4().trim())%>";
					var operFalso1 = "<%=((configNotExt==null)?"":configNotExt.getOperadorFalso1().trim())%>";
					var operFalso2 = "<%=((configNotExt==null)?"":configNotExt.getOperadorFalso2().trim())%>";
					var operFalso3 = "<%=((configNotExt==null)?"":configNotExt.getOperadorFalso3().trim())%>";
					var operFalso4 = "<%=((configNotExt==null)?"":configNotExt.getOperadorFalso4().trim())%>";

					var _valor1,_valor2,_valor3,_valor4;					
					var _operador1,_operador2,_operador3,_operador4;

					if (comparar(notaExterna,condicion,regla)) {
						_valor1 = parseFloat(verdadero1);		
						_valor2 = parseFloat(verdadero2);
						_valor3 = parseFloat(verdadero3);
						_valor4 = parseFloat(verdadero4);	
						_operador1 = operVerdad1;
						_operador2 = operVerdad2;
						_operador3 = operVerdad3;
						_operador4 = operVerdad4;
					} else {
						_valor1 = parseFloat(falso1);		
						_valor2 = parseFloat(falso2);
						_valor3 = parseFloat(falso3);
						_valor4 = parseFloat(falso4);	
						_operador1 = operFalso1;
						_operador2 = operFalso2;
						_operador3 = operFalso3;
						_operador4 = operFalso4;				
					}

					var notaTecsup_1 = opera(notaExterna,_valor3,_operador3);		
					notaTecsup_1 = parseFloat(notaTecsup_1).toFixed(5);
					
					var _orden_ejecucion = orden_ejecucion(_operador1,_operador2,_operador4);								
					var _orden_ejecucion_array = _orden_ejecucion.split("|");
							
					var operador_aux1 = _orden_ejecucion_array[0];
					var operador_aux2 = _orden_ejecucion_array[1];
					var operador_aux3 = _orden_ejecucion_array[2];

					if (operador_aux1 == _operador1) {			
						notaTecsup = opera(_valor1,_valor2,_operador1);			
						notaTecsup = parseFloat(notaTecsup);
						notaTecsup = notaTecsup.toFixed(5);
									
						if (operador_aux2 == _operador2) {
							notaTecsup = opera(notaTecsup,notaTecsup_1,_operador2);
							notaTecsup = opera(notaTecsup,_valor4,_operador4);
						} else {
							notaTecsup_1 = opera(notaTecsup_1,_valor4,_operador4);
							notaTecsup = opera(notaTecsup,notaTecsup_1,_operador2);
						}
					} else {
						if (operador_aux1 == _operador2) {
							notaTecsup = opera(_valor2,notaTecsup_1,_operador2);
							if (operador_aux2 == _operador1) {
								notaTecsup = opera(_valor1,notaTecsup,_operador1);
								notaTecsup = opera(notaTecsup,_valor4,_operador4);					
							} else {
								notaTecsup = opera(notaTecsup,_valor4,_operador4);
								notaTecsup = opera(_valor1,notaTecsup,_operador2);					
							}
						} else {
							notaTecsup = opera(notaTecsup_1,_valor4,_operador4);
							if (operador_aux2 == _operador2) {
								notaTecsup = opera(_valor2,notaTecsup,_operador2);
								notaTecsup = opera(_valor1,notaTecsup,_operador1);
							} else {
								notaTecsup_1 = opera(_valor1,_valor2,_operador1);
								notaTecsup = opera(notaTecsup_1,notaTecsup,_operador2);
							}
						}
					}
						
				}//Fin si es el curso externo correcto
			<%
				}
			%>
		
		<%	}
		}%>
		
		return notaTecsup.toFixed(0); 
		
	}

	//JHPR: 2008-05-05 Calcular Nota Tecsup.  	
	function comparar(notaexterna, condicion, operador) {	
		//alert('Comparar:'+notaexterna+' con '+condicion+' segun '+operador);
		var valor = false;
		
		notaexterna = parseFloat(notaexterna);
		condicion = parseFloat(condicion);
		
		switch(operador){		
			case '=':				
				if (notaexterna==condicion) valor=true				
				break;			
			case '<>':
				if (notaexterna!=condicion) valor=true;
				break;
			case '>':
				if (notaexterna>condicion) valor=true;
				break;				
			case '<':
				if (notaexterna<condicion) valor=true;
				break;								
			case '>=':
				if (notaexterna>=condicion) valor=true;
				break;
			case '<=':
				if (notaexterna<=condicion) valor=true;
				break;
		}
		//alert('comparar resultado:'+valor);
		return valor;
	}
	
	function opera(valor1,valor2,operador) {
		var resultado=0;
		valor1 = parseFloat(valor1);
		valor2 = parseFloat(valor2);
		//alert('operar:'+valor1+' '+operador+' '+valor2);
		switch(operador){
			case '+':
				resultado = (valor1 + valor2);
				break;
			case '-':
				resultado = (valor1 - valor2);
				break;
			case '*':
				resultado = (valor1 * valor2);
				break;
			default : 
				if (valor2>0) resultado = (valor1 / valor2);
		}
		//alert('operar.resultado:'+resultado);
		return resultado;
	}	
	
	function orden_ejecucion(operador1,operador2,operador3) {
		var resultado ="";
		var peso1=0;
		var peso2=0;
		var peso3=0;	
		
		switch(operador1){
			case '/':
				peso1 = 4;
				break;
			case '*':
				peso1 = 3;
				break;
			case '+':
				peso1 = 2;
				break;
			case '-':
				peso1 = 1;
				break;
		}
			
		switch(operador2) {
			case '/':
				peso2 = 4;
				break;
			case '*':
				peso2 = 3;
				break;
			case '+':
				peso2 = 2;
				break;
			case '-':
				peso2 = 1;
				break;		
		}

		switch(operador3) {
			case '/':
				peso3 = 4;
				break;
			case '*':
				peso3 = 3;
				break;
			case '+':
				peso3 = 2;
				break;
			case '-':
				peso3 = 1;
				break;		
		}		
		
		if (peso1>=peso2 && peso1>=peso3) {
			if (peso2>=peso3)
				resultado = operador1 + "|" + operador2 + "|" + operador3;
			else			
				resultado = operador1 + "|" + operador3 + "|" + operador2;
		} else if (peso2>=peso1 && peso2>=peso3) {
			if (peso1>=peso3)
				resultado = operador2 + "|" + operador1 + "|" + operador3;
			else
				resultado = operador2 + "|" + operador3 + "|" + operador1;			
		} else {
			if (peso1>=peso2) 
				resultado = operador3 + "|" + operador1 + "|" + operador2;
			else
				resultado = operador3 + "|" + operador2 + "|" + operador1;
		}			
		return resultado; 
	}
	
	function fc_permiteNumerosValEnter(posObjEvt){
		if(window.event.keyCode ==13 ){
		var nomactual = "txt" + String(posObjEvt - 1);
		var objCurso = "hidCodCurso" + String(posObjEvt - 1);
		var nom = "txt"+posObjEvt;
					
			if (parseFloat(document.getElementById(nomactual).value)<=100) {			
				var notaTecsup = calcularNotaTecsup(objCurso.value, document.getElementById(nomactual).value);
				document.getElementById("lbl"+String(posObjEvt - 1)).value = notaTecsup;			
			} else {
				document.getElementById("lbl"+String(posObjEvt - 1)).value = "";
			}
			
			if(Number(posObjEvt)<Number(document.getElementById("tamListAlumnos").value))
				document.getElementById(""+nom).focus();			
			return true;
		}	
		fc_PermiteNumeros();
				
	}
	
	function fc_verificaNotaTxt(i,obj){	
		fc_ValidaNumeroOnBlur(obj);	
			
		var txtactual = document.getElementById("txt"+String(i)).value;
		var lblactual = document.getElementById("lbl"+String(i)).value;
		
		var objCurso = document.getElementById("hidCodCurso"+String(i));
		
		if (fc_Trim(document.getElementById("txt"+String(i)).value)==""){			
			document.getElementById("lbl"+String(i)).value = "";
		} else {
			//fc_Trim(document.getElementById("lbl"+String(i)).value)=="" && 
			if (parseFloat(document.getElementById("txt"+String(i)).value)<=100){

				var notaTecsup = calcularNotaTecsup(objCurso.value, document.getElementById("txt"+String(i)).value);
				document.getElementById("lbl"+String(i)).value = notaTecsup;
			}
		}
		
		fc_ValidaPromedio(document.getElementById("txt"+i));
	}

	function fc_ValidaPromedio(strNameObj){	
		var strCadena = strNameObj.value;		
		var flg = true;	
			if (fc_Trim(strCadena)!="")
			{
				if(strCadena < 0 || strCadena > 100)
				{		
					flg = false;
					strNameObj.value="";				
					strNameObj.focus();
				}
			}
		return flg;
	}


	//onChange deacuerdo al tipo de producto seleccionado
	function fc_Producto(obj){
		var opcion=$F("cboProducto");		
		var PFR02 = $('PFR02');
		var PCC01 = $('PCC01');
		var PCC02 = $('PCC02');
		var CAT01 = $('CAT01');
		var PAT01 = $('PAT01');
		var PIA01 = $('PIA01');
		var ESP01 = $('ESP01');
		var ESP02 = $('ESP02');
		var HIB01 = $('HIB01');
		var VIR01 = $('VIR01');
		
		switch(opcion){
			case "3":								
				PFR02.style.display="none";
				PCC01.style.display="";
				PCC02.style.display="none";				
				CAT01.style.display="none";
				PAT01.style.display="none";
				PIA01.style.display="none";
				ESP01.style.display="none";
				ESP02.style.display="none";
				HIB01.style.display="none";
				$("txhOperacion").value="ETAPA02";												
				break;
			case $("txhCodPFR").value:				
				//PFR01.style.display="";
				PFR02.style.display="";
				PCC01.style.display="none";
				PCC02.style.display="none";				
				CAT01.style.display="none";
				PAT01.style.display="none";
				PIA01.style.display="none";
				ESP01.style.display="none";
				ESP02.style.display="none";
				HIB01.style.display="none";
// 				$("txhOperacion").value="PFR";
				$("txhOperacion").value='BUSCAR_CURSOS';
				break;
			case $("txhCodPCC").value:				
				//PFR01.style.display="none";
				PFR02.style.display="none";
				PCC01.style.display="none";
				PCC02.style.display="";				
				CAT01.style.display="none";
				PAT01.style.display="none";
				PIA01.style.display="none";
				ESP01.style.display="none";
				ESP02.style.display="none";
				HIB01.style.display="none";
				document.getElementById("txhOperacion").value="PCC";				
				break;
			case $("txhCodCAT").value:
				//PFR01.style.display="none";
				PFR02.style.display="none";
				PCC01.style.display="none";
				PCC02.style.display="none";				
				CAT01.style.display="";
				PAT01.style.display="none";
				PIA01.style.display="none";
				ESP01.style.display="none";
				ESP02.style.display="none";
				HIB01.style.display="none";
				$("txhOperacion").value="CAT";				
				break;
			case $("txhCodPAT").value:
				//PFR01.style.display="none";
				PFR02.style.display="none";
				PCC01.style.display="none";
				PCC02.style.display="none";				
				CAT01.style.display="none";
				PAT01.style.display="";
				PIA01.style.display="none";
				ESP01.style.display="none";
				ESP02.style.display="none";
				HIB01.style.display="none";
				$("txhOperacion").value="PAT";				
				break;
			case $("txhCodPIA").value:
				//PFR01.style.display="none";
				PFR02.style.display="none";
				PCC01.style.display="none";
				PCC02.style.display="none";				
				CAT01.style.display="none";
				PAT01.style.display="none";
				PIA01.style.display="";
				ESP01.style.display="none";
				ESP02.style.display="none";
				HIB01.style.display="none";
				$("txhOperacion").value="PIA";				
				break;
			case $("txhCodPE").value:
				//PFR01.style.display="none";
				PFR02.style.display="none";
				PCC01.style.display="none";
				PCC02.style.display="none";				
				CAT01.style.display="none";
				PAT01.style.display="none";
				PIA01.style.display="none";
				ESP01.style.display="";
				ESP02.style.display="";
				HIB01.style.display="none";
				$("txhOperacion").value="ESP";				
				break;
			case $("txhCodHIBRIDO").value:
				//PFR01.style.display="none";
				PFR02.style.display="none";
				PCC01.style.display="none";
				PCC02.style.display="none";				
				CAT01.style.display="none";
				PAT01.style.display="none";
				PIA01.style.display="none";
				ESP01.style.display="none";
				ESP02.style.display="none";			
				HIB01.style.display="";
				$("txhOperacion").value="HIB";				
				break;
			case $("txhCodTecsupVirtual").value:
				//PFR01.style.display="none";
				PFR02.style.display="none";
				PCC01.style.display="none";
				PCC02.style.display="none";				
				CAT01.style.display="none";
				PAT01.style.display="none";
				PIA01.style.display="none";
				ESP01.style.display="none";
				ESP02.style.display="none";			
				HIB01.style.display="none";
				VIR01.style.display="";
				$("txhOperacion").value="VIR";				
				break;
		}
		
		if(obj!=""){		
			$("frmMain").submit();
		}
		
		
// 		if(obj!="" && opcion!=$("txhCodPFR").value ){		
// 			$("frmMain").submit();
// 		}		
	}
	
	function Fc_CicloCat02(obj){
		document.getElementById("txhOperacion").value="CICLOCAT02";		
		document.getElementById("frmMain").submit();
	}
	function Fc_CursoCat02(obj){
		if(document.getElementById("cboCursoCat02").value!=""){
			document.getElementById("txhOperacion").value="BUSCARCAT";			
			document.getElementById("frmMain").submit();
		}
	}
	
	function Fc_Buscar(cboProducto,cboEspecialidad,cboEtapa,cboPrograma,cboCiclo,cboCurso){
		$("txhCodAlumno").value="";			
		var busqueda=document.getElementById(cboProducto).value;
		var codProducto='';	
		var codEspecialidad='';
		var codEtapa='';		
		var codPrograma='';
		var codCiclo='';
		var codCurso='';
		switch(busqueda){
			case "3":				
				codProducto=document.getElementById(cboProducto).value;				
				var codEspecialidad="";
				var codPrograma="";				
				var codCiclo="";
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}				
				codCurso=document.getElementById(cboCurso).value;				
				break;
			case document.getElementById("txhCodPFR").value:
				
				codProducto=document.getElementById(cboProducto).value;
				
				//JHPR 2008-05-20 Eliminar filtro por Especialidad.
				/*if(document.getElementById(cboEspecialidad).value==""){
					alert(mstrSelEspecialidad);
					return;
				}*/
				//codEspecialidad=document.getElementById(cboEspecialidad).value;
				codEspecialidad="0";
				
				codEtapa="";
				//JHPR: 2008-04-07 Para filtrar correctamente los alumnos y no se repitan por Programa (en pfr el programa es la especialidad)				
				codPrograma=codEspecialidad;
				
				/*if(document.getElementById(cboCiclo).value==""){
					alert('Debe seleccionar un ciclo.');
					return;
				}*/
				//codCiclo=document.getElementById(cboCiclo).value;				
				/*if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}*/
				//codCurso=document.getElementById(cboCurso).value;
				codCiclo='';
				codCurso=document.getElementById(cboCurso).value;
							
				break;
			case document.getElementById("txhCodPCC").value:				
				codProducto=document.getElementById(cboProducto).value;				
				codEspecialidad="";
			
				if(document.getElementById(cboPrograma).value==""){
					alert('Debe selecionar un programa.');
					return;
				}
				codPrograma=document.getElementById(cboPrograma).value;				
				codCiclo="";
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}				
				codCurso=document.getElementById(cboCurso).value;				
				break;
			case document.getElementById("txhCodCAT").value:
				codProducto=document.getElementById(cboProducto).value;
				codEspecialidad="";
				codEtapa="";
				codPrograma="";
				if(document.getElementById(cboCiclo).value==""){
					alert('Debe seleccionar un ciclo.');
					return;
				}
				codCiclo=document.getElementById(cboCiclo).value;
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}
				codCurso=document.getElementById(cboCurso).value;												
				break;
			case document.getElementById("txhCodPAT").value:				
				codProducto=document.getElementById(cboProducto).value;
				codEspecialidad="";
				codEtapa="";
				codPrograma="";
				codCiclo="";
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}
				codCurso=document.getElementById(cboCurso).value;												
				break;
			case document.getElementById("txhCodPIA").value:
				codProducto=document.getElementById(cboProducto).value;
				codEspecialidad="";
				codEtapa="";
				codPrograma="";
				codCiclo="";
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}
				codCurso=document.getElementById(cboCurso).value;												
				break;
			case document.getElementById("txhCodPE").value:
				codProducto=document.getElementById(cboProducto).value;
				codEspecialidad="";
				codEtapa="";
				if(document.getElementById(cboPrograma).value==""){
					alert('Debe selecionar un programa.');
					return;
				}
				codPrograma=document.getElementById(cboPrograma).value;
				if(document.getElementById(cboCiclo).value==""){
					alert('Debe seleccionar un m�dulo.');
					return;
				}
				codCiclo=document.getElementById(cboCiclo).value;
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}
				codCurso=document.getElementById(cboCurso).value;							
				break;
			case document.getElementById("txhCodHIBRIDO").value:
				codProducto=document.getElementById(cboProducto).value;
				codEspecialidad="";
				codEtapa="";
				codPrograma="";
				codCiclo="";
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}
				codCurso=document.getElementById(cboCurso).value;											
				break;
			case document.getElementById("txhCodTecsupVirtual").value:
				codProducto=document.getElementById(cboProducto).value;
				codEspecialidad="";
				codEtapa="";
				codPrograma="";
				codCiclo="";
				if(document.getElementById(cboCurso).value==""){
					alert('Debe seleccionar un curso.');
					return;
				}
				codCurso=document.getElementById(cboCurso).value;										
				break;
		}
		
		$("txhCodProducto").value=codProducto+'';		
		$("txhCodEspecialidad").value=codEspecialidad+'';		
		$("txhCodEtapa").value='';
		$("txhCodPrograma").value=codPrograma+'';
		$("txhCodCiclo").value=codCiclo+'';
		$("txhCodCurso").value=codCurso+'';
		$("txhOperacion").value='BUSCAR';
		$("frmMain").submit();				
	} 
	
	function fc_AdjuntarArchivo(){	
		var codPeriodo = $("txhCodPeriodo").value;
		var codProducto = $("txhCodProductoCat").value;
		var codCiclo = $("cboCicloCat02").value;
		var codEvaluador = $("txhCodEvaluador").value;
		if(codCiclo==""){
			alert('Debe seleccionar un ciclo.');
			return;
		}
		else{
			
			Fc_Popup("${ctx}/evaluaciones/eva_adjuntar_archivo_cat.html?txhCodPeriodo="+codPeriodo
				+"&txhCodProducto="+codProducto
				+"&txhCodCiclo="+codCiclo
				+"&txhCodEvaluador="+codEvaluador
				,430,130);
		}	
	}
	
	function Fc_RegistrarNotaCicloExterno(){
		if($("txhCodAlumno").value != ""){
			codPeriodo=$("txhCodPeriodo").value;
			codProducto=$("txhCodProductoAlumno").value;			
			codEspecialidad=$("txhCodEspecialidadAlumno").value;
			codEtapa=$("txhCodEtapaAlumno").value;
			codPrograma=$("txhCodProgramaAlumno").value;
			codCiclo=$("txhCodCicloAlumno").value;
			codCurso=$("txhCodCursoAlumno").value;			
			codAlumno=$("txhCodAlumno").value;
			nombreAlumno=$("txhNombreAlumno").value;
			notaExterno=$("txhNotaExterno").value;
			codEvaluador=$("txhCodEvaluador").value;
			if(notaExterno==""){
				operacion="GRABAR";
			}
			else{
				operacion="MODIFICAR";
			}
			
			Fc_Popup("${ctx}/evaluaciones/eva_registro_nota_ciclo_externo.html?txhCodPeriodo="+codPeriodo
				+"&txhCodProducto="+codProducto
				+"&txhCodEspecialidad="+codEspecialidad
				+"&txhCodEtapa="+codEtapa
				+"&txhCodPrograma="+codPrograma
				+"&txhCodCiclo="+codCiclo
				+"&txhCodCurso="+codCurso				
				+"&txhCodAlumno="+codAlumno
				+"&txhNombreAlumno="+nombreAlumno
				+"&txhNotaExterno="+notaExterno
				+"&txhCodEvaluador="+codEvaluador
				+"&txhOperacion="+operacion
				,400,145);		
		}
		else{
			alert(mstrSeleccione);
	   		return false;
		}		
	}	
	function fc_SeleccionarRegistro(codAlumno,seleccion, nombre){
		document.getElementById("txhCodAlumno").value=codAlumno;
		document.getElementById("txhNombreAlumno").value=nombre;		
		document.getElementById("txhNotaExterno").value=document.getElementById("hid"+seleccion).value;		
		document.getElementById("txhCodProductoAlumno").value=document.getElementById("hidCodProducto"+seleccion).value;
		document.getElementById("txhCodEspecialidadAlumno").value=document.getElementById("hidCodEspecialidad"+seleccion).value;
		document.getElementById("txhCodEtapaAlumno").value=document.getElementById("hidCodEtapa"+seleccion).value;
		document.getElementById("txhCodProgramaAlumno").value=document.getElementById("hidCodPrograma"+seleccion).value;
		document.getElementById("txhCodCicloAlumno").value=document.getElementById("hidCodCiclo"+seleccion).value;
		document.getElementById("txhCodCursoAlumno").value=document.getElementById("hidCodCurso"+seleccion).value;
											
	}
	
	function fc_SeleccionarRegistroCat(codAlumno,seleccion,nombre){		
		document.getElementById("txhCodAlumno").value=codAlumno;
		document.getElementById("txhNombreAlumno").value=nombre;		
		document.getElementById("txhNotaExterno").value=document.getElementById("hidCat"+seleccion).value;		
		document.getElementById("txhCodProductoAlumno").value=document.getElementById("txhCodProductoCat").value;		
		document.getElementById("txhCodEspecialidadAlumno").value="";
		document.getElementById("txhCodEtapaAlumno").value="";
		document.getElementById("txhCodProgramaAlumno").value="";
		document.getElementById("txhCodCicloAlumno").value=document.getElementById("cboCicloCat02").value;
		document.getElementById("txhCodCursoAlumno").value=document.getElementById("cboCursoCat02").value;
											
	}	
	function fc_Actualizar(){
		$("txhOperacion").value="BUSCAR";
		$("frmMain").submit();
	}
	function fc_ActualizarCat(){
		$("txhOperacion").value="BUSCARCAT";
		$("frmMain").submit();
	}
	function fc_ActualizarArchivoCat(){		
		$("txhOperacion").value="CICLOCAT02";		
		$("frmMain").submit();
	}
	function fc_Aparece2(param,obj_name){
		var tbl_Nota_Externo = $('tbl_Nota_Externo');
		var tbl_Ciclo = $('tbl_Ciclo');
		var BANDEJA01 = $('BANDEJA01');
		var BANDEJA02 = $('BANDEJA02');
		var Ciclo = $('Ciclo');
		var Curso = $('Curso');
		var archivo = $('archivo');
		
		switch(param){
			case '1':				
				tbl_Nota_Externo.style.display="";
				tbl_Ciclo.style.display="none";
				BANDEJA01.style.display="";												
				Ciclo.className="Tab_on"	
				Curso.className="Tab_off";								
				break;
				
			case '2':	
				tbl_Nota_Externo.style.display="none";
				tbl_Ciclo.style.display="";
				archivo.style.display="";
				if($("txhOperacion").value=="BUSCARCAT"){
					BANDEJA02.style.display="";
				}
				Ciclo.className="Tab_off"	
				Curso.className="Tab_on"
				break;	
		}	
	}
	function fc_Aparece(param,obj_name){	
		var tbl_Nota_Externo = $('tbl_Nota_Externo');
		var tbl_Ciclo = $('tbl_Ciclo');
		var BANDEJA01 = $('BANDEJA01');
		var BANDEJA02 = $('BANDEJA02');
		var Ciclo = $('Ciclo');
		var Curso = $('Curso');
		var archivo = $('archivo');		
		switch(param){
			case '1':				
				tbl_Nota_Externo.style.display="";
				tbl_Ciclo.style.display="none";																
				Ciclo.className="Tab_on"	
				Curso.className="Tab_off"
				//limpia la bandeja
				BANDEJA01.style.display="none";
				document.getElementById("tbl_bandeja01").style.width = "98%";
				//**************************
				//limpia la cabecera
				fc_ActualizaCabecera();
				//*******************		
				break;
				
			case '2':	
				tbl_Nota_Externo.style.display="none";
				tbl_Ciclo.style.display="";
				Ciclo.className="Tab_off";	
				Curso.className="Tab_on";
				archivo.style.display="none";
				//limpia la bandeja cat
				BANDEJA02.style.display="none";
				document.getElementById("tbl_bandeja02").style.width = "98%";
				//limpia la cabecer cat
				fc_ActualizaCabeceraCat();
				break;	
		}		
	}
	function fc_ActualizaCabeceraCat(){
		$("cboCicloCat02").value="";
		$("cboCursoCat02").value="";
		//*****************************************************
		//para limpiar el combo de cursos CAT
		var select = document.getElementById("cboCursoCat02");
		//numero de option en el select
		var contador=select.length-1;		
		for (var i=contador; i>0; i--){			
			var option = select.getElementsByTagName("option")[i]; //selecciono el nodo final			
			var padre = option.parentNode; //busco el padre del nodo final
			padre.removeChild(option); //elimino el nodo final
		}
		//*****************************************************		
	}
	function fc_ActualizaCabecera(){
		$("cboEspecialidadPFR").value="";		
		//$("cboCicloPFR").value="";
		//$("cboCursoPFR").value="";
		$("cboProgramaPCC").value="";		
		$("cboCursoPCC").value="";		
		$("cboCicloCAT").value="";		
		$("cboCursoCAT").value="";		
		$("cboCursoPAT").value="";		
		$("cboCursoPIA").value="";		
		$("cboProgramaESP").value="";		
		$("cboModuloESP").value="";		
		$("cboCursoESP").value="";		
		$("cboCursoHIB").value="";		
		$("cboCursoVIR").value="";				
		//PFR01.style.display="none";
		$('PFR02').style.display="none";
		$('PCC01').style.display="none";
		$('PCC02').style.display="none";				
		$('CAT01').style.display="none";
		$('PAT01').style.display="none";
		$('PIA01').style.display="none";
		$('ESP01').style.display="none";
		$('ESP02').style.display="none";			
		$('HIB01').style.display="none";
		$('VIR01').style.display="none";
		$("cboProducto").value="";
	}	
	function fc_Regresar(){
		location.href = "${ctx}/menuEvaluaciones.html";
	}
	function Fc_EspecialidadPFR(){
		//$("cboCicloPFR").value="";		
		$("txhOperacion").value="ESPECIALIDADPFR";
		$("frmMain").submit();
	}
	function Fc_CicloPFR(){
		//$("cboCursoPFR").value="";		
		$("txhOperacion").value="CICLOPFR";
		$("frmMain").submit();
	}
	function Fc_ProgramaPCC(){		
		$("txhOperacion").value="PROGRAMAPCC";
		$("frmMain").submit();
	}
	function Fc_CicloCAT(){
		$("cboCursoCAT").value="";
		$("txhOperacion").value="CICLOCAT";
		$("frmMain").submit();
	}
	function Fc_ProgramaESP(){		
		$("txhOperacion").value="PROGRAMAESP";
		$("frmMain").submit();
	}
	function Fc_ModuloESP(){		
		$("txhOperacion").value="MODULOESP";
		$("frmMain").submit();
	}
	function fc_CursoPFR(){			
		Fc_Buscar('cboProducto','cboEspecialidadPFR','','','cboCicloPFR','cboCursoPFR');
	}	
	
	function fc_VerArchivo(){	
		var srtRuta = $("txhRutaArchivo").value;
		if(srtRuta!=""){
			window.open(srtRuta);
		}
	}
	</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form id="frmMain" commandName="control" action="${ctx}/evaluaciones/eva_nota_casos_especiales.html">

	<form:hidden path="operacion" id="txhOperacion"/>	
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codProducto" id="txhCodProducto"/>
	<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
	<form:hidden path="codEtapa" id="txhCodEtapa"/>
	<form:hidden path="codPrograma" id="txhCodPrograma"/>
	<form:hidden path="codCurso" id="txhCodCurso"/>
	<form:hidden path="codCiclo" id="txhCodCiclo"/>
	
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<!-- form:hidden path="nombreAlumno" id="txhNombreAlumno"/-->	
	<form:hidden path="notaExterno" id="txhnotaExterno"/>
	
	<form:hidden path="codProductoAlumno" id="txhCodProductoAlumno"/>
	<form:hidden path="codEspecialidadAlumno" id="txhCodEspecialidadAlumno"/>
	<form:hidden path="codEtapaAlumno" id="txhCodEtapaAlumno"/>
	<form:hidden path="codProgramaAlumno" id="txhCodProgramaAlumno"/>
	<form:hidden path="codCicloAlumno" id="txhCodCicloAlumno"/>
	<form:hidden path="codCursoAlumno" id="txhCodCursoAlumno"/>	
	
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<form:hidden path="codProductoCat" id="txhCodProductoCat"/>
	
	<form:hidden path="valorEtapa" id="txhValorEtapa"/>
	<!-- copiado de renzo --> 
	<form:hidden path="codPFR" id="txhCodPFR" />
	<form:hidden path="codPCC" id="txhCodPCC" />
	<form:hidden path="codCAT" id="txhCodCAT" />
	<form:hidden path="codPAT" id="txhCodPAT" />
	<form:hidden path="codPIA" id="txhCodPIA"/>
	<form:hidden path="codPE" id="txhCodPE"/>
	<form:hidden path="codHIBRIDO" id="txhCodHIBRIDO" />
	<form:hidden path="codTecsupVirtual" id="txhCodTecsupVirtual"/>
	<form:hidden path="codEtapaProgramaIntegral" id="txhCodEtapaProgramaIntegral"/>
	<form:hidden path="codEtapaCursoCorto" id="txhCodEtapaCursoCorto"/>
	<!-- /copiado de renzo -->
	
	<form:hidden path="rutaArchivo" id="txhRutaArchivo"/>
	<!-- JHPR 2008-05-05 Calculo de Nota Tecsup (Desplazamiento con la tecla ENTER) -->
	<form:hidden path="tamListAlumnos" id="tamListAlumnos"/>
	<form:hidden path="cadenaDatos" id="cadenaDatos"/>
	
	<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>
	<!-- Titulo de la Pagina -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="768px" class="opc_combo"><font style="">Registro Nota - Casos Especiales</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
			
	<table style="margin-left:10px;width:96%" cellpadding="0" cellspacing="0" border ="0">
		<tr>
			<td width="20%" class="Tab_off" onclick="javascript:fc_Aparece('1','Curso');" id="Curso">&nbsp;<b>Curso Nota Externa<b></td>
			<td width="20%" class="Tab_on" onclick="javascript:fc_Aparece('2','Ciclo');" id="Ciclo">&nbsp;<b>Ciclo en la Empresa(CAT)<b></td>
		</tr>
	</table>
	<table height="2px"><tr><td></td></tr></table>
	<!--primera tabla-->
	<table align="center"  border="0" style="display:;width:98%" id="tbl_Nota_Externo" name="tbl_Nota_Externo" cellpadding="0" cellspacing="0" >
	<tr>		
		<td >
			<!--cabecera de la bandeja-->
			<table class="tabla" style="width:98%;margin-top:0px;"  background="${ctx}/images/Evaluaciones/back-sup.jpg" 
				cellspacing="2" cellpadding="0" border="0" bordercolor="red">
				<tr>					
					<td class="" width="15%">&nbsp;Per�odo Vigente :</td>
					<td colspan="3">
						<form:input path="periodo01" id="txtPeriodo01" cssClass="cajatexto_1" readonly="true"/>
					</td>
				</tr>

				<tr>
					<td class="" >&nbsp;Producto :</td>
					<td  align="left" colspan="3">
						<form:select path="tipoProducto" id="cboProducto" cssClass="cajatexto" cssStyle="width:300px" onchange="fc_Producto(this.value);">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listProductos!=null}">
							<form:options itemValue="codProducto" itemLabel="descripcion" 
								items="${control.listProductos}" />
						</c:if>
						</form:select>
						<%-- 
						&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
							<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
								onclick="javascript:Fc_Buscar('cboProducto','cboEspecialidadPFR','','','cboCicloPFR','cboCursoPFR');" 
								style="cursor:hand" alt="Buscar" align="center">
						</a>
						--%>
					</td>					
				</tr>
				
				<!-- 
				<tr id="PFR01" style="display:none;">
					<td class="" >&nbsp;Especialidad :</td>
					<td  align="left" colspan=3>
						<form:select path="tipoEspecialidadPFR" id="cboEspecialidadPFR" cssClass="cajatexto" cssStyle="width:250px" onchange="Fc_EspecialidadPFR();">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listEspecialidadPFR!=null}">
							<form:options itemValue="codEspecialidad" itemLabel="descripcion" 
								items="${control.listEspecialidadPFR}" />
							</c:if>						
						</form:select>
					</td>					
				</tr>
				 -->
				
				<tr id="PFR02" style="display:none;">					
					<td colspan="4">
						<table border="0" class="tabla" width="100%" cellpadding="0" cellspacing="1">							
							<tr>
								<td colspan="3">Curso:</td>
								<td align="left">
									<form:select path="tipoCursoPFR" id="cboCursoPFR" cssClass="cajatexto" cssStyle="width:300px" onchange="fc_CursoPFR();">
										<form:option value="">--Todos--</form:option>
										<c:if test="${control.listCursoPFR!=null}">
											<form:options itemValue="codCurso" itemLabel="nomCurso" 
												items="${control.listCursoPFR}" />
										</c:if>
									</form:select>
								</td>
							</tr>
							
							
							<tr><td colspan="4">B�squeda de Alumnos:</td></tr>
							<tr>
								<td width="15%" align="right">&nbsp;Nombre :</td>
								<td align="left" width="85%" colspan="3">
									<form:input path="nombreAlumno" id="nombreAlumno" maxlength="50" cssClass="cajatexto" cssStyle="width:100px;"/>
									&nbsp;&nbsp;Apellido Paterno :<form:input path="apePaternoAlumno" id="apePaternoAlumno" maxlength="50" cssClass="cajatexto" cssStyle="width:120px;"/>
									&nbsp;&nbsp;Apellido Materno :<form:input path="apeMaternoAlumno" id="apeMaternoAlumno" maxlength="50" cssClass="cajatexto" cssStyle="width:120px;"/>
									&nbsp;
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
										<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
											onclick="javascript:Fc_Buscar('cboProducto','cboEspecialidadPFR','','','cboCicloPFR','cboCursoPFR');" 
											style="cursor:hand" alt="Buscar" align="center">
									</a>
								</td>
								
							</tr>
						</table>
					</td>

					
 
					<%-- --%>
					
										
				</tr>

				<tr id="PCC01" style="display:none;">
					<!-- <td class="" width="15%" id="TDPCC01">&nbsp;Prog. Integral :</td>
					<td  align="left" width="35%" id="TDPCC02">
						<form:select path="tipoProgramaPCC" id="cboProgramaPCC" cssClass="cajatexto" cssStyle="width:250px" onchange="Fc_ProgramaPCC();">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listProgramaPCC!=null}">
								<form:options itemValue="codPrograma" itemLabel="descripcion"
									items="${control.listProgramaPCC}" />								
						    </c:if>						
						</form:select>
					</td> -->
					<td class="" width="10%">&nbsp;Cursos :</td>
					<td  align="left" width="40%">
						<form:select path="tipoCursoPCC01" id="cboCursoPCC01" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoPCC!=null}">
								<form:options itemValue="codCurso" itemLabel="descripcion"
									items="${control.listCursoPCC}" />							
							</c:if>
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','cboProgramaPCC','','cboCursoPCC01');" 
						style="cursor:hand" alt="Buscar" align="center"></a>
					</td>
					<!-- <td class="" >&nbsp;Etapa :</td>					
					<td align="left" width="4%">					
						<input type="radio" checked="true" name="rdoEtapa" value="1"
				    	<c:if test="${control.codEtapa=='1'}">
				    		<c:out value="checked=checked" />
				    	</c:if>
						id="gbradio1" onclick="javascript:Fc_Etapa(this,this.value);">
						&nbsp;Programa Integral&nbsp;
						<input type="radio" name="rdoEtapa" value="2"
						<c:if test="${control.codEtapa=='2'}">
							<c:out value="checked=checked" />
						</c:if>
						id="gbradio2" onclick="javascript:Fc_Etapa(this,this.value);">&nbsp;&nbsp;Cursos Cortos
					</td> -->
				</tr>
				<tr id="PCC02" style="display:none;">
					<td class="" width="15%" id="TDPCC01">&nbsp;Prog. Integral :</td>
					<td  align="left" width="35%" id="TDPCC02">
						<form:select path="tipoProgramaPCC" id="cboProgramaPCC" cssClass="cajatexto" cssStyle="width:250px" onchange="Fc_ProgramaPCC();">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listProgramaPCC!=null}">
								<form:options itemValue="codPrograma" itemLabel="descripcion"
									items="${control.listProgramaPCC}" />								
						    </c:if>						
						</form:select>
					</td>
					<td class="" width="10%">&nbsp;Cursos :</td>
					<td  align="left" width="40%">
						<form:select path="tipoCursoPCC02" id="cboCursoPCC02" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoPCC!=null}">
								<form:options itemValue="codCurso" itemLabel="descripcion"
									items="${control.listCursoPCC}" />							
							</c:if>
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','cboProgramaPCC','','cboCursoPCC02');" 
						style="cursor:hand" alt="Buscar" align="center"></a>
					</td>					
				</tr>
				<tr id="CAT01" style="display:none;">
					<td class="" width="15%">&nbsp;Ciclo :</td>
					<td  align="left" width="35%">
						<form:select path="tipoCicloCAT" id="cboCicloCAT" cssClass="cajatexto" cssStyle="width:250px" onchange="Fc_CicloCAT();">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCicloCAT!=null}">
							<form:options itemValue="codCiclo" itemLabel="codCiclo"
								items="${control.listCicloCAT}" />
							</c:if>
						</form:select>
					</td>
					<td class="" width="10%">&nbsp;Cursos :</td>
					<td  align="left" width="40%">
						<form:select path="tipoCursoCAT" id="cboCursoCAT" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoCAT!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.listCursoCAT}" />
							</c:if>						
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','','cboCicloCAT','cboCursoCAT');" 
						style="cursor:hand" alt="Buscar" align="center"></a>						
					</td>					
				</tr>
				<tr id="PAT01" style="display:none;">
					<td class="" >&nbsp;Cursos :</td>
					<td  align="left" colspan="3">
						<form:select path="tipoCursoPAT" id="cboCursoPAT" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoPAT!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.listCursoPAT}" />
							</c:if>
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','','','cboCursoPAT');" 
						style="cursor:hand" alt="Buscar" align="center"></a>
					</td>					
				</tr>
				<tr id="PIA01" style="display:none;">
					<td class="" >&nbsp;Cursos :</td>
					<td  align="left" colspan="3">
						<form:select path="tipoCursoPIA" id="cboCursoPIA" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoPIA!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.listCursoPIA}" />
							</c:if>						
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','','','cboCursoPIA');" 
						style="cursor:hand" alt="Buscar" align="center"></a>
					</td>					
				</tr>
				<tr id="ESP01" style="display:none;">
					<td class="" >&nbsp;Programa :</td>
					<td  align="left" colspan="3">
						<form:select path="tipoProgramaESP" id="cboProgramaESP" cssClass="cajatexto" cssStyle="width:250px" onchange="Fc_ProgramaESP();">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listProgramaESP!=null}">
							<form:options itemValue="codPrograma" itemLabel="descripcion"
								items="${control.listProgramaESP}" />
							</c:if>												
						</form:select>
					</td>					
				</tr>
				<tr id="ESP02" style="display:none;">
					<td class="" width="15%">&nbsp;Modulo :</td>
					<td  align="left" width="35%">
						<form:select path="tipoModuloESP" id="cboModuloESP" cssClass="cajatexto" cssStyle="width:250px" onchange="Fc_ModuloESP();">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listModuloESP!=null}">
							<form:options itemValue="codCiclo" itemLabel="codCiclo"
								items="${control.listModuloESP}" />
							</c:if>															
						</form:select>
					</td>
					<td class="" width="10%">&nbsp;Cursos :</td>
					<td  align="left" width="40%">
						<form:select path="tipoCursoESP" id="cboCursoESP" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoESP!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.listCursoESP}" />
							</c:if>															
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','cboProgramaESP','cboModuloESP','cboCursoESP');" 
						style="cursor:hand" alt="Buscar" align="center"></a>
					</td>					
				</tr>
				<tr id="HIB01" style="display:none;">
					<td class="" >&nbsp;Cursos :</td>
					<td  align="left" colspan="3">
						<form:select path="tipoCursoHIB" id="cboCursoHIB" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoHIB!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.listCursoHIB}" />
							</c:if>												
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','','','cboCursoHIB');" 
						style="cursor:hand" alt="Buscar" align="center"></a>
					</td>					
				</tr>
				<tr id="VIR01" style="display:none;">
					<td class="" >&nbsp;Cursos :</td>
					<td  align="left" colspan="3">
						<form:select path="tipoCursoVIR" id="cboCursoVIR" cssClass="cajatexto" cssStyle="width:250px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listCursoVIR!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.listCursoVIR}" />
							</c:if>												
						</form:select>&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">												
						<img src='${ctx}/images/iconos/buscar1.jpg' id="imgbuscar" 
						onclick="javascript:Fc_Buscar('cboProducto','','','','','cboCursoVIR');" 
						style="cursor:hand" alt="Buscar" align="center"></a>
					</td>					
				</tr>
			</table>
			<!--espacio en alto-->
			<table >
				<tr>
					<td style="height:6px"></td>
				</tr>
			</table>		
			<!--DATOS DE LA PRIMERA TABLA EN LA BANDEJA-->
			<div style="overflow: auto; height: 300px">			
			<table style="WIDTH: 100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" 
				cellSpacing="1" cellPadding="0" border="0" id="tbl_bandeja01">
				<tr height="20px">					
					<TD class="headtabla" width="10%" align="center">&nbsp;C�digo</TD>
					<TD class="headtabla" width="40%">&nbsp;Alumno</TD>
					<TD class="headtabla" width="15%" align="left">&nbsp;Curso</TD>
					<TD class="headtabla" width="5%" align="center">&nbsp;Ciclo</TD>
					
					<TD class="headtabla" width="5%" align="center">&nbsp;Veces</TD>
					 
					<TD class="headtabla" width="15%" align="center">&nbsp;Nota Externa</TD>
					<TD class="headtabla" width="10%" align="center" colspan="2">&nbsp;Nota Tecsup</TD>
					
				</tr>
				
				<tr id="BANDEJA01" style="display:none;">
					<td width="100%" colspan="6">
<!--						<table class="" width="100%" cellSpacing="1" cellPadding="0" align="center" border="1">-->
						<c:forEach varStatus="loop" var="lista" items="${control.listAlumnos}"  >																
						<!-- tr class=""-->	
							<c:choose>
								<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
								<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
							</c:choose>											
							<td style="text-align: center;" width="10%">
								<c:out value="${lista.codAlumno}" />							
							</td>
							<td style="text-align: left;" width="30%">&nbsp;
								<c:out value="${lista.nombreAlumno}" />							
							</td>
							<%-- --%>
							<td style="text-align: left;" width="25%">&nbsp;							
								<c:out value="${lista.nombreCurso}" />
							</td>							
							
							<td style="text-align: center;" width="5%">&nbsp;
								<c:out value="${lista.codCiclo}" />
							</td>
							<!--  -->						
							<td style="text-align: center;" width="5%">&nbsp;							
								<c:out value="${lista.vecesLlevaUnCurso}" />
							</td>
							 
														
							<td style="text-align: center;" width="15%">
								<!-- -->					
								<input id="txt<c:out value="${loop.index}" />" name="txt<c:out value="${loop.index}" />"   class="cajatexto" value="<c:out value="${lista.notaExterna}"  />"   
									style="width: 40px;text-align: center" onkeypress="fc_permiteNumerosValEnter('<c:out value="${loop.index+1}" />')" maxlength="3"  onblur="fc_verificaNotaTxt('<c:out value="${loop.index}" />','txt<c:out value="${loop.index}" />')" >
												
									&nbsp;&nbsp;
									<input type="checkbox" onclick="javascript:fc_NP('<c:out value="${loop.index}" />')" id="txtNP<c:out value="${loop.index}" />"
									<c:if test="${lista.flagNp=='1'}"> checked </c:if> 
									/>NP&nbsp;
																			
							</td>
																					
							<td style="text-align: center;" width="10%">
								<!--  -->								
								<input disabled id="lbl<c:out value="${loop.index}" />" name="lbl<c:out value="${loop.index}" />" 
									style="width: 40px;text-align: center" border="0" class="cajatexto" value="<c:out value="${lista.notaTecsup}" />"/>
								
							</td>
							
							<td width="0%">							 	
								<input type="hidden" 
								value='<c:out value="${lista.codProducto}" />'
								id='hidCodProducto<c:out value="${loop.index}"/>'>
								
								<input type="hidden" 
								value='<c:out value="${lista.codEspecialidad}" />'
								id='hidCodEspecialidad<c:out value="${loop.index}"/>'>							
								
								<input type="hidden" 
								value='<c:out value="${lista.codEtapa}" />'
								id='hidCodEtapa<c:out value="${loop.index}"/>'>
								
								<input type="hidden" 
								value='<c:out value="${lista.codPrograma}" />'
								id='hidCodPrograma<c:out value="${loop.index}"/>'>
								
								<input type="hidden" 
								value='<c:out value="${lista.codCiclo}" />'
								id='hidCodCiclo<c:out value="${loop.index}"/>'>
								
								<input type="hidden" 
								value='<c:out value="${lista.codCurso}" />'
								id='hidCodCurso<c:out value="${loop.index}"/>'>							
								
								<input type="hidden" 
								value='<c:out value="${lista.id}" />'
								id='hidId<c:out value="${loop.index}"/>'>							
							
								<input type="hidden" 
								value='<c:out value="${lista.codAlumno}" />'
								id='val<c:out value="${loop.index}"/>'>							
								 								 						
							 </td>
							
						</tr>				
						</c:forEach>
<!--						</table>-->
					</td>
				</tr>
				
				<c:if test="${control.tamListAlumnos=='0'}">
				<tr class="">
					<td colspan='5' class="tablagrillaglo" style="text-align: center;">No se encontraron registros</td>
				</tr>
				</c:if>
			</table>
			<c:if test="${control.tamListAlumnos>=11}">
				<script type="text/javascript">
					document.getElementById("tbl_bandeja01").style.width = "98%";					
				</script>				
			</c:if>
			</div>
			<!--el boton de registrar de la primera bandeja-->
			<table align="center" cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN" align="center" height="30px" width="98%" style="margin-top:6px">	
				<tr>
					<td align="center">																				
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" style="cursor:pointer;" id="imgregistrar1" onclick="Fc_RegistrarNotaExterno();"></a>											
					</td>
				</tr>
			</table>									
			</td>
		</tr>
	</table>
	<!--segunda tabla-->	
	<table align="center" style="display:none;height:30px;width:98%" id="tbl_Ciclo" cellpadding="0" cellspacing="0" >
		<tr>
			<td>
			<!--cabezera de la bandeja-->
			<table class="tabla" style="width:98%;margin-top:0px"  background="${ctx}/images/Evaluaciones/back-sup.jpg" cellspacing="2" cellpadding="0" border="0" bordercolor="red">		
				<tr>
					<td width="15%" >&nbsp;Per�odo Vigente :</td>
					<td width="35%">
						<form:input path="periodo02" id="txtPeriodo02" cssClass="cajatexto_1" readonly="true"/>
					</td>
					
					<td width="10%">&nbsp;Ciclo :</td>
					<td  width="15%">
						<form:select path="tipoCicloCat" id="cboCicloCat02" cssClass="cajatexto" cssStyle="width:100px" onchange="Fc_CicloCat02();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listCiclosCat!=null}">
							<form:options itemValue="id" itemLabel="valor1" 
								items="${control.listCiclosCat}" />
						</c:if>
						</form:select>
					</td>										
				</tr>
				<tr>
					<td>&nbsp;Producto :</td>
					<td  align="left">						
						<input value="PROGRAMA CATERPILLAR (CAT)" class="cajatexto" style="width:250px" readonly="readonly">																		
					</td>
					<td width="10%">&nbsp;Curso :</td>
					<td  width="15%">
						<form:select path="tipoCursoCat" id="cboCursoCat02" cssClass="cajatexto" cssStyle="width:100px" onchange="Fc_CursoCat02();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listCursosCat!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion" 
								items="${control.listCursosCat}" />
						</c:if>
						</form:select>
					</td>
					<td width="7%">&nbsp;<b>Informe :</b></td>
					<td id="archivo" style="display:none;">
						<c:if test="${control.archivo!=null}">
							<a onclick="javascript:fc_VerArchivo();" href="#" style="color: white;">
								<b>${control.archivo}</b>
							</a>									
						</c:if>
					</td>
					<td width="3%">						
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgadjuntar','','${ctx}/images/iconos/adjuntar2.jpg',1)">
						<img src='${ctx}/images/iconos/adjuntar1.jpg' id="imgadjuntar" onclick="javascript:fc_AdjuntarArchivo();" style="cursor:pointer" alt="Adjuntar Archivo"></a>
					</td>
										
				</tr>
			</table>
			<!--espacio en alto-->
			<table >			
				<tr>
					<td style="height:6px"></td>
				</tr>
			</table>
			<!--bandeja de contenido-->
			<div style="overflow: auto; height: 300px">
			<table class="" style="WIDTH: 98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" 
			cellSpacing="1" cellPadding="0" id="tbl_bandeja02" border="0" bordercolor="red">
				<TR height="20px">
					<TD class="headtabla" width="5%">&nbsp;Sel</TD>
					<TD class="headtabla" width="10%" align="center">&nbsp;C�digo</TD>
					<TD class="headtabla" width="65%">&nbsp;Alumno</TD>
					<TD class="headtabla" width="20%">&nbsp;Nota </TD>
				</TR>
				<tr id="BANDEJA02" style="display:none;">
					<td width="100%" colspan="5">
						<table class="" style="width:100%" cellSpacing="0" cellPadding="0" border="0">
							<c:forEach varStatus="loop" var="listaCat" items="${control.listAlumnosCat}" >												
							<!-- tr  class=""-->	
							
									<c:choose>
										<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
										<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
									</c:choose>							
												
									<td align="center" >
										<input type="radio" name="cat"
										value='<c:out value="${listaCat.codAlumno}" />'
										id='valCat<c:out value="${loop.index}"/>'
										onclick="fc_SeleccionarRegistroCat(this.value,'<c:out value="${loop.index}"/>','<c:out value="${listaCat.nombreAlumno}" />');">
									</td>
									<td style="text-align: center;">
										<c:out value="${listaCat.codAlumno}" />							
									</td>
									<td style="text-align: left;">&nbsp;
										<c:out value="${listaCat.nombreAlumno}" />							
									</td>
									<td style="text-align: center;">
										<input 
										type="hidden" 
										value='<c:out value="${listaCat.notaExterna}" />'
										id='hidCat<c:out value="${loop.index}"/>'>
										<c:if test="${listaCat.notaExterna!=null}">
											${listaCat.notaExterna}%
										</c:if>
														
									</td>						
							</tr>
							</c:forEach>
						</table>
					</td>
				</tr>
				<c:if test="${control.tamListAlumnosCat=='0'}">
				<tr class="">
					<td colspan='4' class="tablagrillaglo" style="text-align: center;">No se encontraron registros</td>
				</tr>					
				</c:if>			
			</table>
			<c:if test="${control.tamListAlumnosCat>=13}">
				<script type="text/javascript">
					document.getElementById("tbl_bandeja02").style.width = "98%";					
				</script>				
			</c:if>
			</div>
			<!--boton para agregar la nota cat-->
			<table align="center" cellpadding="0" cellspacing="0" border="0" bordercolor="GREEN" align="center" height="30px" width="98%">	
				<tr>
					<td align="center">																			
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregistrar2','','${ctx}/images/botones/registrar2.jpg',1)">
						<img alt="Registrar" src="${ctx}/images/botones/registrar1.jpg" id="imgregistrar2" onclick="Fc_RegistrarNotaCicloExterno();" style="cursor:pointer;">
						</a>
					</td>
				</tr>
			</table>									
			</td>
		</tr>
	</table>
</form:form>	
</body>
<!-- JHRP : 2008-05-20 : Agregu� columna Ciclo a la lista y elimin� filtro por especialidad -->
</html>
