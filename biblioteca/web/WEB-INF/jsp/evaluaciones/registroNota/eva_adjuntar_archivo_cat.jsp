<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<script language=javascript>
	var strExtensionDOC = "<%=(String)request.getAttribute("strExtensionDOC")%>";
	var strExtensionXLS = "<%=(String)request.getAttribute("strExtensionXLS")%>";	
	var strExtensionDOCX = "<%=(String)request.getAttribute("strExtensionDOCX")%>";
	var strExtensionXLSX = "<%=(String)request.getAttribute("strExtensionXLSX")%>";
	
	function onLoad()
	{	objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" ){			
			window.opener.fc_ActualizarArchivoCat();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
	}
	function fc_Valida(){
		var strExtension;
		var lstrRutaArchivo;
		if(fc_Trim(document.getElementById("txtArchivo").value)==""){
			alert('Debe seleccionar su documento a subir.');
			return false;
		}
		else{
			if(fc_Trim(document.getElementById("txtArchivo").value) != ""){
				lstrRutaArchivo = document.getElementById("txtArchivo").value;
				strExtension = "";
				strExtension = lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);
				
				//**************************
				//obtiene el nombre del archivo
				nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();				 
				document.getElementById("txhNomArchivo").value=nombre.substring("1");
				//**************************				
				if(strExtension == strExtensionDOC || strExtension == strExtensionXLS || strExtension==strExtensionDOCX || strExtension == strExtensionXLSX){
					document.getElementById("txhExtArchivo").value = strExtension;
				}
				else{
					alert('El documento debe tener la \nextensi�n : DOC, DOCX, XLS � XLSX');
						return false;
				}
			}
		
		}		
		return true;
	}
		
	function fc_Grabar(){
		if (fc_Valida()){
			if (confirm(mstrSeguroGrabar)){				
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
	}

</script>
</head>
<body topmargin="5" leftmargin="10" rightmargin="10" bgcolor="red">
<form:form  id="frmMain" action="${ctx}/evaluaciones/eva_adjuntar_archivo_cat.html" commandName="control" enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="extArchivo" id="txhExtArchivo"/>
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codProducto" id="txhCodProducto"/>
	<form:hidden path="codCiclo" id="txhCodCiclo"/>
	<form:hidden path="nomNuevoArchivo" id="txhNomNuevoArchivo"/>
	<form:hidden path="nomArchivo" id="txhNomArchivo"/>	
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px;margin-top:5px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width=220px" class="opc_combo"><font style="">Adjuntar Archivo</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	 </table>		
	<table cellpadding="1" cellspacing="2" class="tablaflotante" border="0" bordercolor="gray" style="width:400px;margin-top:5px;margin-left:10px">
		<tr>		
			<td width="50%">
				Archivo:
			</td>
			<td>				
				<input type=file name="txtArchivo" class="cajatexto" style="width:350px">
			</td>
		</tr>		
	</table>
	<br>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_Grabar();" style="cursor:hand" id="imgAgregar" alt="Grabar">&nbsp;</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img ID="Button1" NAME="Button1" src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();"></a>
			</td>		
		</tr>
	</table>		
</form:form>	
</body>
</html>



