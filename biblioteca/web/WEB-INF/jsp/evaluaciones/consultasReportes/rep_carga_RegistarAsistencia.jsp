<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Asistencia'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

</style>

<body >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2"><u><b>CONTROL ASISTENCIA</b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
	<tr >
		<td></td>
		<td class="texto_bold">Evaluador</td>
		<td align="left" class="texto">: ${model.nomEval} </td>
	</tr>
	<tr >
		<td></td>
		<td class="texto_bold">Curso</td>
		<td align="left" class="texto">: ${model.curso} </td>
	</tr>
	<tr >
		<td></td>
		<td class="texto_bold">Secci�n</td>
		<td align="left" class="texto">: ${model.seccion} </td>
	</tr>
	<tr >
		<td></td>
		<td class="texto_bold">Sist. Eval.</td>
		<td align="left" class="texto">: ${model.sistEval} </td>
	</tr>
	<tr >
		<td></td>
		<td class="texto_bold">Fecha<br>Asistencia</td>
		<td align="left" class="texto">: ${model.feAis} </td>
		<td align="left" class="texto_bold">Nro.<br>Asist.</td> 
		<td align="left" class="texto">: ${model.nroAsis} </td>
	</tr>
	<tr >
		<td></td>
	</tr>		
	<!-- Detalle de la lista -->
	<tr >
		<td></td>
		<td class="cabecera_grilla" rowspan="2">C�digo</td>
		<td class="cabecera_grilla" rowspan="2">Alumno</td>
		<td class="cabecera_grilla" colspan="3" >Asistencia</td>
	</tr>
	<tr>
		<td></td>
		<td class="cabecera_grilla" align="center"><b>A</b></td>
		<td class="cabecera_grilla" align="center"><b>F</b></td>
		<td class="cabecera_grilla" align="center"><b>T</b></td>
	</tr>
	<%
		List alumno = (List)request.getSession().getAttribute("consultaAsistencia");
		if(alumno != null){
			for(int j = 0; j < alumno.size(); j++){
				Asistencia asist = (Asistencia)alumno.get(j);
		%>
		<%if (asist.getTipoAsistencia() != null){%>
		<tr class="texto_grilla">
			<td></td>
			<td style="text-align:center; border: 1px solid DarkGray; width: 20%"><%=asist.getCodAlumno()%></td>
			<td style="text-align:left; border: 1px solid DarkGray; width: 100%"><%=asist.getNombre()%></td>
			<%if ("0001".equals(asist.getTipoAsistencia())){%>
			<td style="text-align:center; border: 1px solid DarkGray;"><b>X</b></td>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<%}%>
			<%if ("0002".equals(asist.getTipoAsistencia())){%>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<td style="text-align:center; border: 1px solid DarkGray;"><b>X</b></td>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<%}%>
			<%if ("0003".equals(asist.getTipoAsistencia())){%>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<td style="text-align:center; border: 1px solid DarkGray;"><b>X</b></td>
			<%}%>
			<%if ("".equals(asist.getTipoAsistencia())){%>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<td style="text-align:center; border: 1px solid DarkGray;"></td>
			<%}%>
		</tr>				
		<%}
			}
		}request.getSession().removeAttribute("consulta"); 
		%>	
</table>

</body>
</html>