<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function buscarAlumno(){
		Fc_Popup("${ctx}/inicio/buscar_alumno.html?tipoRetorno=U&ctrlUSE=txtUsuario&ctrlCOD=&ctrlNOM=",600,400);
	}	
	function onLoad(){
		document.getElementById("trUsuario").style.display='none';
		fc_CambiaReporte();		
	}
	function fc_Valor(){
		if($F('codReporte')=='0003'){
			$('txhOperacion').value = 'BUSCAR';
			$('frmMain').submit();
		}
		if($F('codReporte')=='0004'){
			$('txhOperacion').value = 'BUSCAR';
			$('frmMain').submit();
		}
		if($F('codReporte')=='0005'){
			$('txhOperacion').value = 'BUSCAR';
			$('frmMain').submit();
		}
		if($F('codReporte')=='0001'){
			$('txhOperacion').value = 'BUSCAR';
			$('frmMain').submit();
		}
		if($F('codReporte')=='0006'){ //JHPR
			$('txhOperacion').value = 'BUSCAR';
			$('frmMain').submit();
		}
	}
	function fc_Generar(){ //alert("0");

		var producto = $('producto');
		var pro = producto.options[producto.selectedIndex].text;
		
		//pro = frmMain.producto.options[frmMain.producto.selectedIndex].text;
		if(pro=="--Seleccione--"){
			pro="";
		}
		if($('producto').value=='-1'){
			alert('Debe seleccionar un producto.');
			return false;
		}
		if($('periodo').value==''){
			alert('Seleccione un per�odo');
			return false;
		}

		var periodo = $('periodo');
		var periodo_txt = periodo.options[periodo.selectedIndex].text;

		var especialidad = $('especialidad');
		var especialidad_txt = especialidad.options[especialidad.selectedIndex].text;

		var sede = $('sede');
		var sede_txt = sede.options[sede.selectedIndex].text;
		
		u = "0004";
		url="?opcion=" + u +
				"&codPeriodo=" + periodo.value +
				"&sede=" + sede_txt +
				"&periodo=" + periodo_txt + 
				"&producto=" + pro + 
			 	"&codSede=" + sede.value + 
				"&usuario=" + $('txhUsuario').value +
				"&codProducto=" + $F('producto');
			
		window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"","resizable=yes, menubar=yes");		
	}
		
	function fc_Generar1(){ //alert("1");
		//pro = frmMain.producto.options[frmMain.producto.selectedIndex].text;
		var producto = $('producto');
		var pro = producto.options[producto.selectedIndex].text;
		
		if(pro=="--Seleccione--"){
			pro="";
		}
		if($('producto').value=='-1'){
			alert('Debe seleccionar un producto.');
			return false;
		}
		if($('periodo').value==''){
			alert('Seleccione un per�odo');
			return false;
		}
		u = "0005";

		var periodo = $('periodo');
		var periodo_txt = periodo.options[periodo.selectedIndex].text;

		var especialidad = $('especialidad');
		var especialidad_txt = especialidad.options[especialidad.selectedIndex].text;

		var sede = $('sede');
		var sede_txt = sede.options[sede.selectedIndex].text;
		
		url="?opcion=" + u +
			"&codPeriodo=" + periodo.value +
			"&sede=" + sede_txt +
			"&periodo=" + periodo_txt + 
			"&producto=" + pro + 
			"&codSede=" + sede.value + 
			"&usuario=" + $('txhUsuario').value +
			"&codProducto=" + $F('producto');

		window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"","resizable=yes, menubar=yes");
	
	}
		
		function fc_Generar2(){ //alert("2");
			//prod = frmMain.producto.options[frmMain.producto.selectedIndex].text;
			var producto = $('producto');
			var prod = producto.options[producto.selectedIndex].text;
			
			if(prod=="--Seleccione--")
				prod="";
			
			u = "0007";
			if($('producto').value=='-1'){
				alert('Debe seleccionar un producto.');
				return false;
			}
			if($('periodo').value==''){
				alert('Seleccione un per�odo');
				return false;
			}

			var periodo = $('periodo');
			var periodo_txt = periodo.options[periodo.selectedIndex].text;

			var especialidad = $('especialidad');
			var especialidad_txt = especialidad.options[especialidad.selectedIndex].text;

			var sede = $('sede');
			var sede_txt = sede.options[sede.selectedIndex].text;
			
			url="?opcion=" + u +
				"&codPeriodo=" + $F('periodo') +
				"&sede=" + sede_txt +
				"&periodo=" + periodo_txt + 
				"&producto=" + prod +
				"&especialidad=" + especialidad_txt +
				"&codSede=" + sede.value + 
				"&codEspecialidad=" + especialidad.value +
				"&usuario=" + $('txhUsuario').value +
				"&codProducto=" + $F('producto');
	
			window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"","resizable=yes, menubar=yes");
	
		}
		
		function fc_Generar3(){
			
			var producto = $('producto');
			var prod = producto.options[producto.selectedIndex].text;			
			if(prod=="--Seleccione--")
				prod="";
			
			u = "0006";
			if($('producto').value=='-1'){
				alert('Debe seleccionar un producto.');
				return false;
			}
			if($F('periodo')==''){
				alert('Seleccione un per�odo');
				return false;
			}

			var periodo = $('periodo');
			var periodo_txt = periodo.options[periodo.selectedIndex].text;

			var especialidad = $('especialidad');
			var especialidad_txt = especialidad.options[especialidad.selectedIndex].text;

			var sede = $('sede');
			var sede_txt = sede.options[sede.selectedIndex].text;
			
			url="?opcion=" + u +
				"&codPeriodo=" + $F('periodo') +
				"&sede=" + sede_txt +
				"&periodo=" + periodo_txt + 
				"&producto=" + prod +
				"&especialidad=" + especialidad_txt + 
				"&codSede=" + sede.value + 
				"&codEspecialidad=" + especialidad.value +
				"&usuario=" + $('txhUsuario').value +
				"&codProducto=" + $F('producto');
	
			window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"","resizable=yes, menubar=yes");
		}
		
		function fc_Generar4(){ //alert("4");		
			//prod = frmMain.producto.options[frmMain.producto.selectedIndex].text;
			var producto = $('producto');
			var prod = producto.options[producto.selectedIndex].text;
			
			if(prod=="--Seleccione--")
				prod="";

			var periodo = $('periodo');
			var periodo_txt = periodo.options[periodo.selectedIndex].text;

			var especialidad = $('especialidad');
			var especialidad_txt = especialidad.options[especialidad.selectedIndex].text;

			var sede = $('sede');
			var sede_txt = sede.options[sede.selectedIndex].text;
						
			if($('txtUsuario').value!=''){			
				u = "0008";			
				url="?opcion=" + u +
				"&codPeriodo=" + $F('periodo') +
				"&sede=" + sede_txt +
				"&periodo=" + periodo_txt + 
				"&producto=" + prod +
				"&especialidad=" + especialidad_txt + 
				"&codSede=" + $F('sede') + 
				"&codEspecialidad=" + $F('especialidad') +
				"&usuario=" + $('txtUsuario').value +
				"&codProducto=" + $F('producto');
	
				window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"","resizable=yes, menubar=yes");
		 	}
			else alert("Debe ingresar un codigo de usuario." );
		}
		
	function fc_Generar6(){
		//'Reporte Historial'
		if($('txtUsuarioHistorial').value!=''){			
			u = "REP_HIST_ALUMNO";			
			url="?opcion=" + u +			
			"&usuario=" + $('txtUsuarioHistorial').value;
			window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"","resizable=yes, menubar=yes");
	 	}
		else alert("Debe ingresar un codigo de usuario.");
	}
	
	function fc_Generar5(){ 
		
		//pro = frmMain.producto.options[frmMain.producto.selectedIndex].text;
		var producto = $('producto');
		var pro = producto.options[producto.selectedIndex].text;
		
		if(pro=="--Seleccione--")
			pro="";
	
		if($('producto').value=='-1'){
			alert('Debe seleccionar un producto.');
			return false;
		}
		if($F('periodo').value==''){
			alert('Seleccione un per�odo');
			return false;
		}
		u = "CIERRENOTAS";
		var periodo = $('periodo');
		var periodo_txt = periodo.options[periodo.selectedIndex].text;

		var especialidad = $('especialidad');
		var especialidad_txt = especialidad.options[especialidad.selectedIndex].text;

		var sede = $('sede');
		var sede_txt = sede.options[sede.selectedIndex].text;
		
		url="?opcion=" + u +
			"&especialidad=" + especialidad_txt +
			"&sede=" + sede_txt +
			"&periodo=" + periodo_txt +
			"&producto=" + pro +
			"&codPeriodo=" + $F('periodo') +
			"&codSede=" + $F('sede') + 
			"&codEspecialidad=" + $F('especialidad') +			 					
			"&codProducto=" + $F('producto');

		window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"","resizable=yes, menubar=yes");
	
	}
		
				
	function fc_CambiaReporte(){
		
		$('trUsuario').style.display='none';
		var tablaReportes = $('tablaReportes');
		var genera = $('genera');
		var genera1 = $('genera1');
		var genera2 = $('genera2');
		var genera3 = $('genera3');
		var genera4 = $('genera4');
		var genera5 = $('genera5');
		var genera6 = $('genera6');		
		var tablaEspecialidad = $('tablaEspecialidad');
		var tablaEspecialidad1 = $('tablaEspecialidad1'); 
		var tablaRepHistorial = $('tablaRepHistorial');
		
		if($F('codReporte')=='0001'){ //alert("0001");
			tablaReportes.style.display='';
			genera.style.display='';
			genera1.style.display='none';
			genera2.style.display='none';
			genera3.style.display='none';
			genera4.style.display='none';
			genera5.style.display='none';
			genera6.style.display='none';			
			tablaEspecialidad.style.display='';
			tablaEspecialidad1.style.display='';
			tablaRepHistorial.style.display='none';
		}else if($F('codReporte')=='0002'){ //alert("0002");
			tablaReportes.style.display='';
			genera.style.display='none';
			genera1.style.display='';
			genera2.style.display='none';
			genera3.style.display='none';
			genera4.style.display='none';
			genera5.style.display='none';
			genera6.style.display='none';			
			tablaEspecialidad.style.display='none';
			tablaEspecialidad1.style.display='none';
			tablaRepHistorial.style.display='none';
		}else if($F('codReporte')=='0003'){ //alert("0003");
			tablaReportes.style.display='';
			genera.style.display='none';
			genera1.style.display='none';
			genera2.style.display='';
			genera3.style.display='none';
			genera4.style.display='none';
			genera5.style.display='none';
			genera6.style.display='none';			
			tablaEspecialidad.style.display='';
			tablaEspecialidad1.style.display='';
			tablaRepHistorial.style.display='none';
		}else if($F('codReporte')=='0004'){ //alert("0004");
			tablaReportes.style.display='';
			genera.style.display='none';
			genera1.style.display='none';
			genera2.style.display='none';
			genera3.style.display='';
			tablaEspecialidad.style.display='';
			tablaEspecialidad1.style.display='';
			genera4.style.display='none';
			genera5.style.display='none';
			genera6.style.display='none';			
			tablaRepHistorial.style.display='none';
		}else if($F('codReporte')=='0005'){ //alert("0005");
			tablaReportes.style.display='';
			genera.style.display='none';
			genera1.style.display='none';
			genera2.style.display='none';
			genera3.style.display='none';
			genera4.style.display='';
			genera5.style.display='none';
			genera6.style.display='none';			
			tablaEspecialidad.style.display='';
			tablaEspecialidad1.style.display='';
			$('trUsuario').style.display='inline-block';
			tablaRepHistorial.style.display='none';
		}else if($F('codReporte')=='0006') { //Reporte de Cierre de Notas.			
			tablaReportes.style.display='';
			genera.style.display='none';
			genera1.style.display='none';
			genera2.style.display='none';
			genera3.style.display='none';
			genera4.style.display='none';
			genera5.style.display='';
			genera6.style.display='none';			
			tablaEspecialidad.style.display='';
			tablaEspecialidad1.style.display='';	
			tablaRepHistorial.style.display='none';
		}else if($F('codReporte')=='0007'){ //REPORTE HISTORIAL DE ALUMNO
			tablaReportes.style.display='none';
			genera.style.display='none';
			genera1.style.display='none';
			genera2.style.display='none';
			genera3.style.display='none';
			genera4.style.display='none';
			genera5.style.display='none';
			genera6.style.display='';
			tablaRepHistorial.style.display='';
		}else{
			$('txtUsuario').value=""; 
		}
	}
	
	function fc_Regresar(){
		location.href =  "${ctx}/menuEvaluaciones.html";
	}
	
	function fc_Sede(){
		$('txhOperacion').value='PERIODO';
		$('frmMain').submit();
	}
</script>
	</head>

   	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/evaluaciones/reportes.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="usuario" id="txhUsuario"/>
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	
	<table border="0" cellpadding="0" cellspacing="0" height="50px" style="margin-left:11px; width:90%;" class="">
		<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%" >
				Consultas y Reportes
			</td>		 	
		</tr>	 
		<tr class="fondo_dato_celeste">
			<td nowrap="nowrap">&nbsp;Reportes :&nbsp;&nbsp;&nbsp;
				<form:select path="codReporte" id="codReporte" cssClass="cajatexto_o" 
								cssStyle="width:350px;" onchange="javascript:fc_CambiaReporte();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listreportes!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" items="${control.listreportes}" />
					</c:if>
				</form:select>
			</td>
		</tr>
	</table>
		 
	<table id="tablaReportes" border="0" cellpadding="2" cellspacing="2" height="60px" background="${ctx}/images/biblioteca/fondoinf.jpg"
		style="display:none; margin-left:11px;width:90%" class="tabla" >	 
		<tr>	
				<td width="9%">&nbsp;Sede:</td>
				<td width="41%"><form:select path="sede" id="sede" cssClass="cajatexto" 
					cssStyle="width:100px" onchange="javascript:fc_Sede();">
					<c:if test="${control.listSede!=null}">
					<form:options itemValue="codSede" itemLabel="dscSede" 
						items="${control.listSede}" />
					</c:if>
				</form:select>
				</td>
				
				<td width="9%">&nbsp;Periodo:</td>
				<td width="41%"><form:select  path="periodo" id="periodo"  cssClass="cajatexto" 
								cssStyle="width:100px">
								<form:option  value="">--Seleccione--</form:option>
						         <c:if test="${control.listPeriodo!=null}">
					        	<form:options itemValue="codigo" itemLabel="nombre" 
					        		items="${control.listPeriodo}" />						        
					            </c:if>
 				            </form:select></td>
		</tr>
				
		<tr>	
				<td width="9%">&nbsp;Producto:</td>
				<td width="41%"><form:select  path="producto" id="producto"  cssClass="cajatexto" 
								cssStyle="width:280px" onchange="javascript:fc_Valor();">
						        <form:option  value="-1">--Seleccione--</form:option>
					      	    <c:if test="${control.listProducto!=null}">
					        	<form:options itemValue="codProducto" itemLabel="descripcion" 
					        		items="${control.listProducto}" />						        
					            </c:if>
 				            </form:select></td>
			
				<td id="tablaEspecialidad" style="display: none" width="9%">&nbsp;Especialidad:</td>
				<td id="tablaEspecialidad1" style="display: none" width="41%">
				<form:select  path="especialidad" id="especialidad" cssClass="cajatexto" 
								cssStyle="width:260px" >
								 <form:option  value="">--Seleccione--</form:option>
							        <c:if test="${control.listEspecialidad!=null}">
						        	<form:options itemValue="codEspecialidad" itemLabel="descripcion" 
						        		items="${control.listEspecialidad}" />						        
						            </c:if>
	 				            </form:select></td>
		</tr>
		<tr id="trUsuario" style="display: none">	
			<td width="9%">&nbsp;Usuario:</td>
			<td colspan="4">
			<table width="100px" border="0">
			<tr>
				<td>
					<form:input path="dscUsuario" id="txtUsuario" cssClass="cajatexto" size="8"
			     				maxlength="10" onblur="fc_ValidaNumeroOnBlur('txtUsuario');" 
			     				onkeypress="fc_PermiteNumeros();"/>
				</td>
				<td>
				<!-- put image -->
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerarX','','${ctx}/images/iconos/buscara2.jpg',1)">
					<img src="${ctx}/images/iconos/buscara1.jpg" onclick="javascript:buscarAlumno();" alt="Buscar Alumno" style="cursor:hand;" id="btnGenerarX" width="20" height="17" >
				</a>
				</td>
			</tr>
			</table>
			</td>
			
		</tr>
		</table>
		
		
		<table id="tablaRepHistorial" border="0" cellpadding="2" cellspacing="2" height="60px" background="${ctx}/images/biblioteca/fondoinf.jpg"
				style="display:none; margin-left:11px;width:90%" class="tabla" >	 
			<tr>	
				<td width="10%">&nbsp;Usuario:</td>
				<td width="90%">
					<form:input path="dscUsuario" id="txtUsuarioHistorial" cssClass="cajatexto" size="8"
			     				maxlength="10" onblur="fc_ValidaNumeroOnBlur('txtUsuarioHistorial');" 
			     				onkeypress="fc_PermiteNumeros();"/>
				</td>
			</tr>
		</table>
		
		<table align="center">
		<tr>
		<td id="genera" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
		</td>
		<td id="genera1" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar1','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar1();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar1"></a>
		</td>
		<td id="genera2" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar2','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar2();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar2"></a>
		</td>
		<td id="genera3" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar3','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar3();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar3"></a>
		</td>
		<td id="genera4" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar4','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar4();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar4"></a>
		</td>
		<td id="genera5" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar5','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar5();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar5"></a>
		</td>
		<td id="genera6" style="display: none" align="center">
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar6','','${ctx}/images/botones/generar2.jpg',1)">
				<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar6();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar6"></a>
		</td>							
		</tr>
		</table>
	</form:form>