<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reportes'%>
<%@page import='com.tecsup.SGA.modelo.CompetenciasPerfil'%>
<%@page import="com.tecsup.SGA.modelo.TipoTablaDetalle"%> 
<%@page import='java.util.StringTokenizer'%> 
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table width="699">
<tr>
	<td style="width:5pt;"></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2" colspan="4"><u><b>REPORTE DE PERFILES DE ALUMNOS </b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	
	<td></td>	
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" colspan="2" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>

</table>

<br>
<table >
<tr>
	<td></td>
	<td class="texto_bold">Periodo</td>
	<td align="left" class="texto">: ${model.periodo} </td>
</tr>
<tr>
  <td></td>
  <td class="texto_bold">Evaluador</td>
  <td align="left" class="texto" colspan="2">: ${model.nomEval}</td>
  <td class="texto_bold">&nbsp;</td>
  <td align="left" class="texto" colspan="2">&nbsp;</td>
</tr>
</table>
<br><br><br>
<table align="center">
	<tr>
		<td></td>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
			  <td colspan="2" nowrap="nowrap"  >&nbsp;</td>
			  
			  <% 
			  List<TipoTablaDetalle> cabecera = (List<TipoTablaDetalle>) request.getSession().getAttribute("consultaPerfilesAlumnos_Cab");
			  if (cabecera.size()>0){
			  	for (TipoTablaDetalle cab:cabecera){				  		  
			  %>			  	  
				  <td colspan="2" nowrap="nowrap" class="cabecera_grilla" align="center" ><%=cab.getDescripcion().trim()%></td>
			  
			  <%
			  	}%>
			  	<td colspan="2" nowrap="nowrap" class="cabecera_grilla" align="center">Promedio</td>
			  	<%
			  }%>
			  </tr>
			<tr rowspan="2">
				<!--td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fecha </td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Per�odo</td-->
				<td class="cabecera_grilla" style="width:120px" nowrap="nowrap">C&oacute;digo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Alumno</td>
				
				<%
				if (cabecera.size()>0){
					for (TipoTablaDetalle cab:cabecera){
				%>
					<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Inicio</td>
					<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fin</td>
				
				
				<%
					}%>
					<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Inicio</td>
					<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fin</td>
					<%
				}
				%>
				
			  </tr>
			<%
				List detalle;
				List alumno = (List)request.getSession().getAttribute("consultaPerfilesAlumnos_Det");
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						//Reportes rep = (Reportes)alumno.get(j);
						CompetenciasPerfil rep = (CompetenciasPerfil) alumno.get(j);
						
				%>
				
				<tr class="texto_grilla">
					<td width="30%" style="text-align:center"><%=rep.getCodAlumno()%></td>
					<td width="30%" style="text-align:left"><%=rep.getNomAlumno()%></td>
					
					<%
					detalle = rep.getListaCompetencias();
					for (int k = 0; k < detalle.size(); k++){
						CompetenciasPerfil detAlumno = (CompetenciasPerfil) detalle.get(k);						
					%>
					
					<td width="30%" style="text-align:left"><%=detAlumno.getNotaIni()%></td>
					<td width="30%" style="text-align:center"><%=detAlumno.getNotaFin()%></td> 					
					
					<%
					}
					%>
					
				</tr>
				
				<%	}
				}
				request.getSession().removeAttribute("consultaPerfilesAlumnos_Cab");
				request.getSession().removeAttribute("consultaPerfilesAlumnos_Det"); 
				%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>