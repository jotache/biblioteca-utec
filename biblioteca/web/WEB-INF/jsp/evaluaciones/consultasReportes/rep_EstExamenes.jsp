<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reportes'%> 
<%@page import='java.util.StringTokenizer'%> 
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2"><u><b>REPORTE DE ESTAD&Iacute;STICAS POR EXAMEN</b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
<tr>
	<td></td>
	<td class="texto_bold">Sede</td>
	<td align="left" class="texto">: ${model.sede} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Periodo</td>
	<td align="left" class="texto">: ${model.periodo} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Producto</td>
	<td align="left" class="texto" colspan="2">: ${model.producto} </td>
	<td class="texto_bold">Especialidad</td>
	<td align="left" class="texto" colspan="2">: ${model.especialidad} </td>
</tr>
</table><br><br><br>
<table align="center">
	<tr>
		<td></td>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<!--td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Fecha </td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Per�odo</td-->
				<td class="cabecera_grilla" style="width:120px" nowrap="nowrap">Ciclo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Especialidad</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Curso</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Secci�n</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >CodSeccion</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Codigo Curso</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Examen </td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Aprobados</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Desaprobados</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Anulados</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >No se Presento</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Matriculados</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >% Desap.</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Promedio Nota</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Profesor</td>
				</tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consultaAsistencia");
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						Reportes rep = (Reportes)alumno.get(j);
				%>
				<%if (rep.getDscPeriodo() != null){%>
				<tr class="texto_grilla">
					<td width="30%" style="text-align:center"><%=rep.getCodCiclo()%></td>
					<td width="30%" style="text-align:left"><%=rep.getEspecialidadCurso()%></td>
					<td width="30%" style="text-align:left"><%=rep.getCurso()%></td>
					<td width="30%" style="text-align:center"><%=rep.getSecciones()%></td> 
					<td width="30%" style="text-align:center"><%=rep.getCodSeccion()%></td>
					<td width="30%" style="text-align:center"><%=rep.getCodCurso()%></td>
					<td width="30%" style="text-align:center"><%=rep.getTipo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getAprobados()%></td>
					<td width="30%" style="text-align:center"><%=rep.getDesaprobados()%></td>
					<td width="30%" style="text-align:center"><%=rep.getAnulados()%></td>
					<td width="30%" style="text-align:center"><%=rep.getNroPresentados()%></td>
					<td width="30%" style="text-align:center"><%=rep.getMatriculados()%></td>
					<td width="30%" style="text-align:center"><%=Integer.parseInt(rep.getDesaprobados())*100/ Integer.parseInt(rep.getMatriculados())%></td>
					<td width="30%" class="detalleNro" style="text-align:right"><%=rep.getPromedio()%></td>
					<td width="30%" style="text-align:left"><%=rep.getDscEvaluadorTeo()%></td>
				</tr>
				<%}
					}
				}request.getSession().removeAttribute("consultaAsistencia"); 
				%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>