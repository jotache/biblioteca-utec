<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.bean.EvaluacionesParciales'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2"><u><b>REPORTE DE EVALUACIONES PARCIALES</b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
<tr>
	<td></td>
	<td class="texto_bold">Periodo</td>
	<td align="left" class="texto">: ${model.periodo} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Evaluador</td>
	<td align="left" class="texto">: ${model.nomEval} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Curso</td>
	<td align="left" class="texto">: ${model.curso} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Especialidad</td>
	<td align="left" class="texto">: ${model.especialidad} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Sist. Eval.</td>
	<td align="left" class="texto">: ${model.sistEval} </td>
	<td class="texto_bold">Ciclo</td>
	<td align="left" class="texto">: ${model.ciclo} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">tipo Evaluacion</td>
	<td align="left" class="texto">: ${model.tipEval} </td>
	<td class="texto_bold">Seccion</td>
	<td align="left" class="texto">: ${model.seccion} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Nro. Eval.</td>
	<td align="left" class="texto">: ${model.nroAsis} </td>
</tr>
</table><br><br><br>
<table align="center">
	<tr>
		<td></td>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap"  rowspan="2">C�digo</td>
				<td class="cabecera_grilla"  style="width:260px" nowrap="nowrap"  rowspan="2">Nombre</td>
				<td class="cabecera_grilla"  style="width:260px" nowrap="nowrap"  rowspan="2">Nota</td>
				<td class="cabecera_grilla"  nowrap="nowrap" colspan="3" ></td>
			</tr>
			<tr>
				<td class="cabecera_grilla" style="width:40px" style="text-align:center;"><b>AN</b></td>
				<td class="cabecera_grilla" style="width:40px" style="text-align:center;"><b>NP</b></td>
				<td class="cabecera_grilla" style="width:40px" style="text-align:center;"><b>PE</b></td>
			</tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consultaAsistencia");
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						EvaluacionesParciales evalPar = (EvaluacionesParciales)alumno.get(j);
				%>
				<%if (evalPar.getCodigo() != null){%>
				<tr class="texto_grilla">
					<td width="30%" style="text-align:center"><%=evalPar.getCodigo()%></td>
					<td width="80%" style="text-align:left"><%=evalPar.getNombre()%></td>
					<td width="80%" style="text-align:right"><%=evalPar.getNota()%></td>
					<%if ("0001".equals(evalPar.getCodTipoMotivo())){%>
					<td width="15%" style="text-align:center"><b>X</b></td>
					<td width="15%" style="text-align:center"> </td>
					<td width="15%" style="text-align:center"> </td>
					<%}%>
					<%if ("0002".equals(evalPar.getCodTipoMotivo())){%>
					<td width="15%" style="text-align:center"> </td>
					<td width="15%" style="text-align:center"><b>X</b></td>
					<td width="15%" style="text-align:center"> </td>
					<%}%>
					<%if ("0003".equals(evalPar.getCodTipoMotivo())){%>
					<td width="15%" style="text-align:center"> </td>
					<td width="15%" style="text-align:center"> </td>
					<td width="15%" style="text-align:center"><b>X</b></td>
					<%}%>
					<%if ("".equals(evalPar.getCodTipoMotivo())){%>
					<td width="15%" style="text-align:center"> </td>
					<td width="15%" style="text-align:center"> </td>
					<td width="15%" style="text-align:center"> </td>
					<%}%>
				</tr>
				
				<%}
					}
				}request.getSession().removeAttribute("consultaAsistencia"); 
				%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>