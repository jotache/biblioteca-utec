<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Incidencia'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

</style>

<body >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2"><u><b>INCIDENCIAS POR ALUMNO</b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
	<table>	
	<tr >
		<td></td>
		<td class="texto_bold">Alumno</td>
		<td align="left" class="texto">: ${model.alumno} </td>
	</tr>
	<tr >
		<td></td>
		<td class="texto_bold">Usuario</td>
		<td align="left" class="texto">: ${model.nomEval} </td>
	</tr>
	<tr >
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr >
		<td></td>
		<td class="cabecera_grilla" style="width:80px" >Fecha</td>
		<td class="cabecera_grilla" style="width:280px" >Incidencia</td>
		<td class="cabecera_grilla" style="width:150px" >Registrado Por</td>
	</tr>
	<%
	List alumno = (List)request.getSession().getAttribute("consultaAsistencia");
	if(alumno != null){
		for(int j = 0; j < alumno.size(); j++){
			Incidencia asist = (Incidencia)alumno.get(j);
	%>
	<%if (asist.getCodIincidencia() != null){%>
	<tr class="texto_grilla">
		<td></td>
		<td style="text-align:center; border: 1px solid DarkGray;">&nbsp;<%=asist.getFecha()%></td>
		<td style="text-align:left; border: 1px solid DarkGray;"><%=asist.getDscIncidencia()%></td>
		<td style="text-align:left; border: 1px solid DarkGray;"><%=asist.getCodUsuario()%></td>
	</tr>
	
	<%}
		}
	}request.getSession().removeAttribute("consultaAsistencia"); 
	%>	
</table>
</body>
</html>