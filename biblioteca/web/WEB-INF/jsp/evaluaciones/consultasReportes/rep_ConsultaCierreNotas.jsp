<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.DefNroEvalParciales'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
	/* JHPR 2008-06-09*/
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2" colspan="2"><u><b>REPORTE DE CIERRE DE NOTAS </b></u></td>
	<!--td></td-->
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<!--td></td-->
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
<tr>
	<td></td>
	<td class="texto_bold">Sede</td>
	<td align="left" class="texto">: ${model.sede} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Periodo</td>
	<td align="left" class="texto">: ${model.periodo} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Producto</td>
	<td align="left" class="texto" colspan="2">: ${model.producto} </td>
	<td class="texto_bold">Especialidad</td>
	<td align="left" class="texto" colspan="2">: ${model.especialidad} </td>
</tr>
</table><br><br><br>
<table align="center">
	<tr>		
		<td></td>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">				
				<td class="cabecera_grilla" style="width:100px" nowrap="nowrap" >Especialidad</td>
				<td class="cabecera_grilla" style="width:40px" nowrap="nowrap" align="center">Ciclo</td>
				<td class="cabecera_grilla" style="width:100px" nowrap="nowrap" >Curso</td>
				<td class="cabecera_grilla" style="width:60px" nowrap="nowrap" >Secci�n</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Responsable</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Departamento</td>				
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Tipo Evaluaci&oacute;n </td>
				<td class="cabecera_grilla" style="width:60px" nowrap="nowrap" >Estado</td>
				
				<td class="cabecera_grilla" style="width:70px" nowrap="nowrap" >Pract. Def.</td>
				<td class="cabecera_grilla" style="width:70px" nowrap="nowrap" >Pract. Ing.</td>
				<td class="cabecera_grilla" style="width:70px" nowrap="nowrap" >Estado Asist.</td>
				<td class="cabecera_grilla" style="width:70px" nowrap="nowrap" >Asistenc.</td>				
				
			  </tr>
			<%
				List<DefNroEvalParciales> lista = (List<DefNroEvalParciales>)request.getSession().getAttribute("consultaCierreNotas");
				if(lista != null){
					for (DefNroEvalParciales defi : lista){						
						
				%>
				
				<tr class="texto_grilla">
					<td style="text-align:center"><%=defi.getNomEspecialidad()%></td>
					<td style="text-align:left"><%=defi.getCiclo() %></td>
					<td style="text-align:left"><%=defi.getNomCurso() %></td>
					<td style="text-align:left"><%=defi.getNomSeccion()%></td>
					<td style="text-align:center"><%=defi.getNomResponsable() %></td>
					<td style="text-align:center"><%=defi.getNomDptoResponsable() %></td> 
					<td style="text-align:center"><%=defi.getNomTipoEvaluacion() %></td>
					
					<td style="text-align:center">
					<%if (defi.getEstadoCerrado().toUpperCase().equals("CERRADO")) {%>
						<font color="#333333" style="font-style: italic"><%=defi.getEstadoCerrado()%></font>
					<%} else {%>
						<font color="#FF0000"><%=defi.getEstadoCerrado()%></font>
					<%}%>									
					</td>
					
					<td style="text-align:center"><%=defi.getNroPractDef() %></td>
					<td style="text-align:center"><%=defi.getNroPractEje() %></td>
					
					<td style="text-align:center">
					<%if (defi.getEstadoAsist().toUpperCase().equals("0")) {%>
						<font color="#FF0000" style="font-style: italic">Abierto</font>
					<%} else {%>
						<font color="#333333">Cerrado</font>
					<%}%>									
					</td>
					
					<td style="text-align:center"><%=defi.getNroAsistenc() %></td>										
				</tr>
				<%
					}
				}
				request.getSession().removeAttribute("consultaCierreNotas"); 
				%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>