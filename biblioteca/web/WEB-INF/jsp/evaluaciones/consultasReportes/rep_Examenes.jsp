<%@page import='java.util.List'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@page import='com.tecsup.SGA.web.evaluaciones.command.RegistrarExamenesCommand'%>
<%@page import='com.tecsup.SGA.modelo.Examen'%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td align="center"><u>Reporte De Notas de Ex&aacute;menes</u></td>
	<td colspan="5" align="left">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td align="center">

	<c:choose>
		<c:when test="${model.tipoExamen=='0'}">
			Parciales
		</c:when>
		<c:when test="${model.tipoExamen=='1'}">
			De Cargo
		</c:when>
		<c:when test="${model.tipoExamen=='2'}">
			Extraordinarios
		</c:when>
		<c:when test="${model.tipoExamen=='3'}">
			De Subsanaci�n
		</c:when>
		<c:when test="${model.tipoExamen=='4'}">
			Finales
		</c:when>
		<c:when test="${model.tipoExamen=='5'}">
			De Recuperaci�n
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>

	</td>
	<td colspan="5" align="left">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td></td>
	<td colspan="5" align="left">Usuario : ${model.usuario}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
<tr class="texto_bold">
	<td></td>
	<td>Periodo</td>
	<td align="left">: ${model.periodo} </td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td>Evaluador</td>
	<td align="left">: ${model.nomEvalSeccion} </td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td>Curso</td>
	<td align="left">: ${model.curso} </td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td>Especialidad</td>
	<td align="left">: ${model.especialidad}</td>
	<td>Ciclo</td>
	<td align="left">: ${model.ciclo} </td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td>Sist. Eval.</td>
	<td align="left">: ${model.sistEval} </td>
	<td>Seccion</td>
	<td align="left">: ${model.seccion} </td>
</tr>
</table>
<br><br><br>
<table align="center">
	<tr>
		<td></td>
		<td>
			<table border="1" align="center">
			<%	
			RegistrarExamenesCommand cmdxls = (RegistrarExamenesCommand) request.getSession().getAttribute("consultaExamenes0");
			List listaTipoExms = null;
			
			int nrocolumnas = 0;
			
			%>
			<tr >
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap"  >C�digo</td>
				<td class="cabecera_grilla"  style="width:260px" nowrap="nowrap"  >Nombre</td>
				<%
				if 	(cmdxls!=null) { 
					listaTipoExms = cmdxls.getListaTipoExams();
					if (listaTipoExms!=null){	
						for(int i = 0; i < listaTipoExms.size(); i++){
							Examen tipoexamen = (Examen) listaTipoExms.get(i);
							//tipoexamen.getDescripcion();	
						%>
						
						<%
						
							//POR EL MOMENTO NO VISUALIZAR LAS COLUMNAS DE RECUPERACION Y SUBSANACION
							String titExamen = "";
							//if (tipoexamen.getAlias().trim().equals("E1") || tipoexamen.getAlias().trim().equals("E2")) {
								nrocolumnas = nrocolumnas + 1;													
								if (tipoexamen.getAlias().trim().equals("E1")) {
									titExamen = "Examen" + "<br/>" + "1";
								} else if (tipoexamen.getAlias().trim().equals("E2")) {
									titExamen = "Examen" + "<br/>" + "2";
								} else if (tipoexamen.getAlias().trim().equals("ER")) {
									titExamen = "Examen" + "<br/>" + "Recuperaci�n";
								} else if (tipoexamen.getAlias().trim().equals("ES")) {
									titExamen = "Examen" + "<br/>" + "Subsanaci�n";
								}else if (tipoexamen.getAlias().trim().equals("EC")) {
									titExamen = "Examen" + "<br/>" + "De Cargo";
								}else if (tipoexamen.getAlias().trim().equals("EX")) {
									titExamen = "Examen" + "<br/>" + "De Extraordinario";
								}
															
						%>												
								<td class="cabecera_grilla"  style="width:200px" nowrap="nowrap" ><%=titExamen%></td>						
								<td class="cabecera_grilla" style="width:20px" style="text-align:center;"><b>NP</b></td>
								<td class="cabecera_grilla" style="width:20px" style="text-align:center;"><b>AN</b></td>												
						<%
							//}							
						}
					}
				}
				%>
				</tr>		
												
			
			<!-- something -->
			<%	
				System.out.println("nrocolumnas:"+nrocolumnas);
				//System.out.println("atributo consultaExamenes0 cargado");
				if 	(cmdxls!=null) {
					//System.out.println("atributo consultaExamenes0 No es Nulo");
					List listaNotas = cmdxls.getListaNotas();					
					//System.out.println("lista listaNotas cargada");
					if (listaNotas!=null) {
						int cont = 1;
						int contExams = 0;
						for(int j = 0; j < listaNotas.size(); j++){							
							Examen examen = (Examen) listaNotas.get(j);
							//System.out.println("item:"+examen.getCodAlumno());										
				%>
					<tr class="texto_grilla">
						<td  style="text-align:center"><%=examen.getCodAlumno()%></td>
						<td  style="text-align:left"><%=examen.getNombre()%></td>
						
						<%
						for (int x = 0; x < listaTipoExms.size(); x++){
							// por el momento no visualizar ex�menes de recuperaci�n ni subsanaci�n.
							// linea original: contExams = x;
							//Examen tipoexamen1 = (Examen) listaTipoExms.get(x);
							//if (tipoexamen1.getAlias().equals("E1") || tipoexamen1.getAlias().equals("E2")) {
								contExams = x;
							//}
						}
						for (int x = 0; x < examen.getResultado().size(); x++){
							// por el momento no visualizar ex�menes de recuperaci�n ni subsanaci�n.							
								cont = x;							
						}
						if (cont==0 && cont!=contExams) {
							System.out.println("listaTipoExms.size():"+listaTipoExms.size());
							// por el momento no visualizar ex�menes de recuperaci�n ni subsanaci�n.
							//for(int i = 0; i < listaTipoExms.size(); i++){ //valedero
							for (int i=0; i < nrocolumnas; i ++) {																								
								%>									
									<td width="20%" style="text-align:left"></td>						
									<td width="15%" style="text-align:center"></td>
									<td width="15%" style="text-align:center"></td>								
								<%																
							}
						}
						if (cont==contExams){						
							//System.out.println("previa posible caida");
							//System.out.println("examen.getResultado().size()"+examen.getResultado().size());
							int contnull=0;
							int contnotnull=0;
							for(int i = 0; i < examen.getResultado().size(); i++){
								Examen examen2 = (Examen) examen.getResultado().get(i);
								//System.out.println("examen2:Nota"+examen2.getNota());								
								if (examen2.getNota()==null && examen2.getFlagAN()==null && examen2.getFlagNP()==null){
									contnull=contnull+1;
									if (contnull<=nrocolumnas) {
								%>									
									<td width="20%" style="text-align:left"></td>						
									<td width="15%" style="text-align:center"></td>
									<td width="15%" style="text-align:center"></td>																
								<%				
									}
								} else if (examen2.getFlagAN().equals("0") && examen2.getFlagNP().equals("0") ){																
									contnull=contnull+1;
									if (contnotnull<=nrocolumnas){
									%>									
										<td width="20%" style="text-align:center"><%=(examen2.getNota()==null?"":examen2.getNota())%></td>						
										<td width="15%" style="text-align:center"></td>
										<td width="15%" style="text-align:center"></td>									
									<%									
									}
								}else if (examen2.getFlagAN().equals("1")){
									contnull=contnull+1;
									if (contnotnull<=nrocolumnas){
								%>
									<td width="20%" style="text-align:center"></td>						
									<td width="15%" style="text-align:center"></td>
									<td width="15%" style="text-align:center">X</td>								
								<%									
									}
								}else if (examen2.getFlagNP().equals("1")){
									contnull=contnull+1;
									if (contnotnull<=nrocolumnas){
								%>
									<td width="20%" style="text-align:center"></td>						
									<td width="15%" style="text-align:center">X</td>
									<td width="15%" style="text-align:center"></td>								
								<%									
									}
								}
								
							}
						}
						%>
												
					</tr>
													
				<%		}
					}
				}request.getSession().removeAttribute("consultaExamenes0"); 
				%>				
			<!-- end something -->									
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>