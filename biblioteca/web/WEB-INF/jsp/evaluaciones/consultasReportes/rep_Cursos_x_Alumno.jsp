<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reportes'%> 
<%@page import='java.util.StringTokenizer'%> 
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style >
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 8pt;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
	border: 1px solid black;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
	border: 1px solid black;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2" colspan="28"><u><b>REPORTE CURSOS POR ALUMNO</b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fechas</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
<table>
<tr>
	<td></td>
	<td class="texto_bold">C�digo</td>
	<td align="left" class="texto">: ${model.usuario} </td>
</tr>
<tr>
	<td></td>	
	<td class="texto_bold">Nombre</td>
	<td align="left" class="texto" >: ${model.nomEval} </td>
	<td class="texto_bold" >Nivel</td>
	<td align="left" class="texto">: ${model.ciclo} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Especialidad</td>
	<td align="left" class="texto" >: ${model.especialidad} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Sede</td>
	<td align="left" class="texto">: ${model.sede} </td>
	<td class="texto_bold" >Periodo</td>
	<td align="left" class="texto">: ${model.periodo} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Producto</td>
	<td align="left" class="texto" >: ${model.producto} </td>
</tr>
</table>
<br>
<table align="center">
	<tr>
		<td></td>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Periodo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nombre Del Curso</td>
				<td class="cabecera_grilla" style="width:800px" nowrap="nowrap">Ciclo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Especi Curso</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Secci�n</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Codigo Curso</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Tipo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Numvez</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Sist.Eval</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Detalle</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >PROM_AULA</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >PROM_TALLER</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >PROM_LABORA</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >EXAMEN1</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >EXAMEN2</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nota Cargo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nota Externa</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nota Externa TECSUP</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >% Inasistencia</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Promedio Final</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >SUBSANACION RECUPERACION</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Promedio Recuperaci�n</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >ESTADO FINAL</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Inasit. TEO</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Inasit. TAL</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Inasit. LAB</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >HoraxSesion.TEO</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >HoraxSesion.TAL</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >HoraxSesion.LAB</td>
				<!--
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >CodProfesor</td>
				<td class="cabecera_grilla" style="width:140px" nowrap="nowrap">Nombre Profesor</td>
				-->
			</tr>
			<%
				List alumno = (List)request.getSession().getAttribute("consultaAsistencia");
				if(alumno != null){
					for(int j = 0; j < alumno.size(); j++){
						Reportes rep = (Reportes)alumno.get(j);
							if (rep.getDscPeriodo() != null){
				%>					
				<tr class="texto_grilla">
					<td width="30%" style="text-align:center"><%=rep.getDscPeriodo()%></td>
					<td width="30%" style="text-align:left"><%=rep.getCurso()%></td>
					<td width="30%" style="text-align:center"><%=rep.getNivelCurso()%></td>
					<td width="30%" style="text-align:left"><%=rep.getEspecialidadCurso()%></td>
					<td width="30%" style="text-align:center"><%=rep.getSecciones()%></td>
					<td width="30%" style="text-align:center"><%=rep.getCodCurso()%></td>
					<td width="30%" style="text-align:center"><%=rep.getTipo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getNroVez()%></td>
					<td width="30%" style="text-align:center"><%=rep.getSisEval()%></td>
					<td width="30%" style="text-align:left"><%=rep.getDetalle()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPromTeo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPromTal()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPromLab()%></td>
					<td width="30%" style="text-align:center"><%=rep.getExam1()%></td>
					<td width="30%" style="text-align:center"><%=rep.getExam2()%></td>
					<td width="30%" style="text-align:center"><%=rep.getNotaCargo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getNtaExt()%></td>
					<td width="30%" style="text-align:center"><%=rep.getNtaTecsup()%></td>
					<td width="30%" style="text-align:center"><%=rep.getProInansistencia()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPromFinal()%></td>
					<td width="30%" style="text-align:center"><%=rep.getSubRecu()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPromRecu()%></td>
					<td width="30%" style="text-align:center"><%=rep.getEstFinal()%></td>
					<td width="30%" style="text-align:center"><%=rep.getAisTeo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getAsisTal()%></td>
					<td width="30%" style="text-align:center"><%=rep.getAsisLab()%></td>
					<td width="30%" style="text-align:center"><%=rep.getHorasTeo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getHoarsTal()%></td>
					<td width="30%" style="text-align:center"><%=rep.getHorasLab()%></td>
					<%-- 
					<td width="30%" style="text-align:right"><%=rep.getCodEvaluadorTeo()%></td>
					<td width="30%" style="text-align:left"><%=rep.getDscEvaluadorTeo()%></td>
					--%>
				
				</tr>
				
				<%}
					}
				}request.getSession().removeAttribute("consultaAsistencia"); 
				%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>