<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reportes'%>
<%@page import='java.util.StringTokenizer'%> 
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr>
	<td style="width:2pt" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td align="center" style="font-size:12pt; vertical-align: middle" rowspan="2" colspan="24"><u><b>REPORTE ESTADO FINAL DEL ALUMNO</b></u></td>
	<td></td>
	<td align="right" style="font-size:8pt">Fecha</td>
	<td colspan="2" align="left" style="font-size:8pt"><b>:</b> ${model.fechaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
	<td align="right" style="font-size:8pt">Hora</td>
	<td align="left" style="font-size:8pt"><b>:</b> ${model.horaRep}</td>
</tr>
<tr >
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
<br>
<table >
<tr>
	<td></td>
	<td class="texto_bold">Sede</td>
	<td align="left" class="texto">: ${model.sede} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Periodo</td>
	<td align="left" class="texto">: ${model.periodo} </td>
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Producto</td>
	<td align="left" class="texto">: ${model.producto} </td>
	<td class="texto_bold">Especialidad</td>
	<td align="left" class="texto">: ${model.especialidad} </td>
</tr>
</table><br><br><br>
<table align="center">
	<tr>
		<td></td>
		<td>
			<table border="1" align="center">
			<tr rowspan="2">
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Periodo</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >CodAlumno</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nivel Alumno</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Especialidad Alumno</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nombre Alumno</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Secci�n</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >N�mero Cursos</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Numero Examenes (Cargo)</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Promedio Semestral</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Promedio Acumulado</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Va a recuperacion </td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Orden de M�rito</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Quinto Superior</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Estado Inicial</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Estado Final</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >PCE</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Curso Cargo 1</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >C�digo Cargo 1</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >NumVez1</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nota C1</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Curso Cargo 2</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >C�digo Cargo 2</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >NumVez2</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nota C2</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Curso Cargo 3</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >C�digo Cargo 3</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Numvez3</td>
				<td class="cabecera_grilla" style="width:80px" nowrap="nowrap" >Nota C3</td>
			</tr>
			<%
				List reportes = (List)request.getSession().getAttribute("consultaAsistencia");
				if(reportes != null){
					for(int j = 0; j < reportes.size(); j++){
						Reportes rep = (Reportes)reportes.get(j);
				%>
				<%if (rep.getCodAlumno() != null){%>
				<tr class="texto_grilla">
					<td width="30%" style="text-align:center"><%=rep.getDscPeriodo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getCodAlumno()%></td>
					<td width="30%" style="text-align:center"><%=rep.getNivelAlumno()%></td>
					<td width="30%" style="text-align:center"><%=rep.getEspecialidadCurso()%></td>
					<td width="30%" style="text-align:left"><%=rep.getNomAlumno()%></td>
					<td width="30%" style="text-align:center"><%=rep.getSecciones()%></td>
					<td width="30%" style="text-align:right"><%=rep.getNroVez()%></td>
					<td width="30%" style="text-align:right"><%=rep.getNroExamCargo()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPromSemestral()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPromAcumulado()%></td>
					<td width="30%" style="text-align:center"><%=rep.getIndRecuperacion()%></td>
					<td width="30%" style="text-align:center"><%=rep.getOrdMerito()%></td>
					<td width="30%" style="text-align:center"><%=rep.getIndQuinto()%></td>
					<td width="30%" style="text-align:center"><%=rep.getEstInicial()%></td>
					<td width="30%" style="text-align:center"><%=rep.getEstFinal()%></td>
					<td width="30%" style="text-align:center"><%=rep.getPce()%></td>
					<%
					int nrocol = 12;
					String muestra = "";
					String data = "";
					StringTokenizer st = new StringTokenizer(rep.getCursosCargo(),"|");
					int nroNotas=st.countTokens();
					
					while ( st.hasMoreTokens())
    					{
						data= st.nextToken();
						if(data=="-1"){
							muestra="";
						}
						else{
							muestra=data;
						}
    				%>
						<td width="30%" style="text-align:center"><%=muestra%></td>
				<%}%>
					<%for(int i=0;i<nrocol - nroNotas;i++){%>
					<td width="30%" style="text-align:center"></td>
					<%}%>
					
					</tr>
				
				<%}
					}
				}request.getSession().removeAttribute("consultaAsistencia"); 
				%>	
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>