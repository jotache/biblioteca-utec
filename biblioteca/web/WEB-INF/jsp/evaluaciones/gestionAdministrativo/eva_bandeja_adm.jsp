<%@ include file="/taglibs.jsp"%>


<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
	//**************************ATILA FUNCIONES
	function onLoad(){
	  var objProducto = "<%=((request.getAttribute("Producto")==null)?"":(String)request.getAttribute("Producto"))%>";
	  var objCodigoEtapa = "<%=((request.getAttribute("CODIGOETAPA")==null)?"":(String)request.getAttribute("CODIGOETAPA"))%>";
	  if(objProducto!="")
	  		fc_CambiaProducto(document.getElementById("codProducto").value);
	}
	
   	function fc_Producto(){
    	fc_BuscarByProducto(document.getElementById("codProducto").value);
	}

   	function fc_getParametrosBusqueda(){   	   	  	   	   	
   		strParametrosBusqueda = "&prmOperacion="+document.getElementById("nombreOperacion").value+
   		"&prmCodSelCicloPfr="+document.getElementById("codSelCicloPFR").value+
   		"&prmEspecialidadPfr="+document.getElementById("codSelEspecialidadPFR").value+
   		"&prmCodProducto="+document.getElementById("codProducto").value+
   		"&prmVerCargos="+document.getElementById("txhVerCargos").value+
   		"&prmCodPeriodoVig="+document.getElementById("txhCodPeriodoVig").value;   		
   		return strParametrosBusqueda; 
   	}
   	
   	function fc_EspecialidadAdmision(){
		document.getElementById("txhOperacion").value = "CursosAdmision";
		document.getElementById("frmMain").submit();
	}
   	
	function fc_Etapa(param){		
		document.getElementById("txhValorEtapa").value = param;
		var objValorEtapa = "<%=((request.getAttribute("VALORETAPA")==null)?"":(String)request.getAttribute("VALORETAPA"))%>";
		var objValorEtapa1 = "<%=((request.getAttribute("VALORETAPA1")==null)?"":(String)request.getAttribute("VALORETAPA1"))%>";
		
		switch(param){
			case '1':	
			    Tabla_PCC_ProgramaIntegral.style.display = 'inline-block';
			    MADFROG.style.display = 'inline-block';
			    Tabla_PCC_PI.style.display = 'inline-block';
			    Tabla_PCC_Curso.style.display = 'inline-block';
			    document.getElementById("txhProgramaPCCEtapa").value ="1";
			    document.getElementById("txhValorEtapa").value ="1";
			    if((objValorEtapa1=="")||(objValorEtapa1=="DOS")){ 
			    document.getElementById("txhOperacion").value = "ProgramaIntegralPCC";
			    document.getElementById("frmMain").submit();}
			    objValorEtapa1="";			    
			    break;
			case '2':
			    Tabla_PCC_ProgramaIntegral.style.display = 'inline-block';
			    MADFROG.style.display = 'inline-block';
			    Tabla_PCC_PI.style.display = 'none';
			    Tabla_PCC_Curso.style.display = 'inline-block';
			    document.getElementById("txhProgramaPCCEtapa").value ="2";
			    document.getElementById("txhValorEtapa").value ="2";
			    document.getElementById("txhOperacion").value = "MADFROG1";			    
			    if((objValorEtapa=="")||(objValorEtapa=="UNO")){// alert("TK");
			    if(objValorEtapa=="UNO") document.getElementById("codSelCursoPCC").value="-1";
			    document.getElementById("txhOperacion").value = "CursoPCC";
			    document.getElementById("frmMain").submit();}
			    objValorEtapa="";			    
			   	break;	
			}
	}
      	   
	function fc_Grabar(){
	 nroSelec= fc_GetNumeroSeleccionados();	 
	 if(document.getElementById("txhTipoProducto").value =="PFR")
		{ frog=fc_ValidarPFR();
		  if(frog=="1")
		      {   if ( nroSelec != "0" )
					{ if(confirm(mstrSeguroGrabar))
						{  
						    document.getElementById("txhNroSelec").value = nroSelec;
							document.getElementById("txhOperacion").value = "GRABAR";
							document.getElementById("frmMain").submit();
						}
					}
					else alert(mstrSeleccione);
			  }		 
	   }else{ 
	        if(document.getElementById("txhTipoProducto").value =="PCC_I")
		    {   frog=fc_ValidarPCC1();
			   	if(frog=="1")
				{ if ( nroSelec != "0" )
				        {	if(confirm(mstrSeguroGrabar))
						    {  
							  document.getElementById("txhNroSelec").value = nroSelec;
							  document.getElementById("txhOperacion").value = "GRABAR";
							  document.getElementById("frmMain").submit();
							}
						}
						else alert(mstrSeleccione);
				 }		     
		   }
		   else{ if(document.getElementById("txhTipoProducto").value =="PCC2")
			    {   frog=fc_ValidarPCC2();				  
				    if(frog=="1")
					{
						 if ( nroSelec != "0" )
						    { if(confirm(mstrSeguroGrabar))
									{   document.getElementById("txhNroSelec").value = nroSelec;
										document.getElementById("txhOperacion").value = "GRABAR";
										document.getElementById("frmMain").submit();
									}
							}
							else alert(mstrSeleccione);
					 }
				}
			  else{ if(document.getElementById("txhTipoProducto").value =="PCC3")
				     {  frog=fc_ValidarPCC3();					  
					    if(frog=="1")
						{  if ( nroSelec != "0" )
						    { if(confirm(mstrSeguroGrabar))
							     {      document.getElementById("txhNroSelec").value = nroSelec;
												document.getElementById("txhOperacion").value = "GRABAR";
												document.getElementById("frmMain").submit();
								 }
							}
						else alert(mstrSeleccione);
					    }
				     }
			        else{ if(document.getElementById("txhTipoProducto").value =="CAT")
					       { frog=fc_ValidarCAT();						 
						     if(frog=="1")
						     { if ( nroSelec != "0" )
						            {	if(confirm("Seguro de Guardar??")){  
													    document.getElementById("txhNroSelec").value = nroSelec;
														document.getElementById("txhOperacion").value = "GRABAR";
														document.getElementById("frmMain").submit();
													}
									}
									else alert(mstrSeleccione);
							  }
						     // else  alert(mstrSeleccioneComponentes);
						   }
			            else{ if(document.getElementById("txhTipoProducto").value =="PAT")
					          {  frog=fc_ValidarPAT();
						         //alert("<<PAT>>"+frog);
						         if(frog=="1")
						         {	 if ( nroSelec != "" )
								    {	if(confirm(mstrSeguroGrabar))
									        {  		    document.getElementById("txhNroSelec").value = nroSelec;
														document.getElementById("txhOperacion").value = "GRABAR";
														document.getElementById("frmMain").submit();
											}
									}
									else alert(mstrSeleccione);
							     }
						      }
						     else{ if(document.getElementById("txhTipoProducto").value =="PIA")
							        {   frog=fc_ValidarPIA();								  
								        if(frog=="1")
										{ if ( nroSelec != "0" )
										    { if(confirm(mstrSeguroGrabar))
										        {   document.getElementById("txhNroSelec").value = nroSelec;
													document.getElementById("txhOperacion").value = "GRABAR";
													document.getElementById("frmMain").submit();
												}
											}
											else alert(mstrSeleccione);
									    }
								    }
							   
						     else{ if(document.getElementById("txhTipoProducto").value =="PROGRA_ESPECIALIZADO")
								   {   frog=fc_ValidarPE();
									   if(frog=="1")
									   { if ( nroSelec != "0" )
									            {	if(confirm(mstrSeguroGrabar))
												     {  document.getElementById("txhNroSelec").value = nroSelec;
														document.getElementById("txhOperacion").value = "GRABAR";
														document.getElementById("frmMain").submit();
												     }
												}
												else alert(mstrSeleccione);
										 }
								   }
								 else  { if(document.getElementById("txhTipoProducto").value =="HIBRIDO")
								          {frog=fc_ValidarHIBRIDO();//alert("Nro "+nroSelec);
									    
									      if(frog=="1")
									      { if ( nroSelec != "0" )
									         {	if(confirm(mstrSeguroGrabar))
											                    {   document.getElementById("txhNroSelec").value = nroSelec;
																	document.getElementById("txhOperacion").value = "GRABAR";
																	document.getElementById("frmMain").submit();
																}
										     }
										     else alert(mstrSeleccione);
										  }
									     }
									     else{ if(document.getElementById("txhTipoProducto").value =="TECSUP_VIRTUAL")
									           {    frog=fc_ValidarTECSUP_VIRTUAL();
										      
										        	if(frog=="1")
													{ if ( nroSelec != "0" )
												  	  { if(confirm(mstrSeguroGrabar))
												  	      {   document.getElementById("txhNroSelec").value = nroSelec;
																document.getElementById("txhOperacion").value = "GRABAR";
															document.getElementById("frmMain").submit();
															}
													  }
													 else alert(mstrSeleccione);
											      }
										 
									     		}
									     		else{ if(document.getElementById("txhTipoProducto").value =="PCC_CC")
									                   {  frog=fc_ValidarPCC_CC();
										      
										        	  if(frog=="1")
													  { if ( nroSelec != "0" )
												  	   { if(confirm(mstrSeguroGrabar))
												  	      {   document.getElementById("txhNroSelec").value = nroSelec;
																document.getElementById("txhOperacion").value = "GRABAR";
															document.getElementById("frmMain").submit();
															}
													   }
													   else alert(mstrSeleccione);
											          }
										 
									     		}
									     		
									     		}
									         }
								  	    }  
						        }
			                }
			            }    
		            }
		        }
		  
	        }   
	}	
	
}		
function fc_CambiaProducto(param) 
{   
	switch(param)
	{
		
	case "32666":
		document.getElementById("Tabla_BDO").style.display = 'inline-block';
		document.getElementById("txhTipoProducto").value = "BDO";
		break;
	
	case "210":
		document.getElementById("Tabla_ADMISION").style.display = 'inline-block';
		document.getElementById("txhTipoProducto").value = "ADMISION";
		break;	
	
	case document.getElementById("txhCodPFR").value:
			document.getElementById("Tabla_PFR").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PFR";
			break;
		
		case document.getElementById("txhCodPCC_I").value:
			document.getElementById("Tabla_PCC_PI").style.display = 'inline-block';
			document.getElementById("Tabla_PCC_ProgramaIntegral").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PCC_I";
			break;		
		case document.getElementById("txhCodCAT").value:
			document.getElementById("Tabla_CAT").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "CAT";
			break;
		case document.getElementById("txhCodPE").value:
			
			document.getElementById("Tabla_PROGRA_ESPECIALIZADO").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PAT";
			break;
		case document.getElementById("txhCodPIA").value:
			document.getElementById("Tabla_PIA").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PIA";
			break;

		case document.getElementById("txhCodPAT").value:
			document.getElementById("Tabla_PAT").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PROGRA_ESPECIALIZADO";
			break;
		case document.getElementById("txhCodHIBRIDO").value:
			document.getElementById("Tabla_HIBRIDO").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "HIBRIDO";
			break;
		case document.getElementById("txhCodTecsupVirtual").value:
			document.getElementById("txhTipoProducto").value = "TECSUP_VIRTUAL";
			document.getElementById("Tabla_TECSUP_VIRTUAL").style.display = 'inline-block';
			break;
	   case document.getElementById("txhCodPCC_CC").value:
	  		document.getElementById("Tabla_PCC_ProgramaIntegral").style.display = 'inline-block';
			document.getElementById("txhTipoProducto").value = "PCC_CC";
			break;
	}	
}

function fc_BuscarByProducto(param) 
{
	switch(param)
	{
		
		case "32666":
			document.getElementById("txhOperacion").value = "BDO";
			document.getElementById("frmMain").submit();
			break;
	
		case "210":
			document.getElementById("txhOperacion").value = "ADMISION";
			document.getElementById("frmMain").submit();
			break;
		
		case document.getElementById("txhCodPFR").value:
			document.getElementById("txhOperacion").value = "PFR";
	        document.getElementById("frmMain").submit();
			break;
		
		case document.getElementById("txhCodPCC_I").value:
			document.getElementById("txhOperacion").value = "PCC_I";
			document.getElementById("frmMain").submit();
			break;
		case document.getElementById("txhCodCAT").value:
			document.getElementById("txhOperacion").value = "CAT";
			document.getElementById("frmMain").submit();
			
			break;
		case document.getElementById("txhCodPAT").value:
			document.getElementById("txhOperacion").value = "PAT";
			document.getElementById("frmMain").submit();
			
			break;
		case document.getElementById("txhCodPIA").value:
			document.getElementById("txhOperacion").value = "PIA";
			document.getElementById("frmMain").submit();
			break;

		case document.getElementById("txhCodPE").value:
			document.getElementById("txhOperacion").value = "ESPECIALIZACION";
			document.getElementById("frmMain").submit();
			break;
			
		case document.getElementById("txhCodHIBRIDO").value:
			document.getElementById("txhOperacion").value = "HIBRIDO";
			document.getElementById("frmMain").submit();
			break;
			
		case document.getElementById("txhCodTecsupVirtual").value:
			document.getElementById("txhOperacion").value = "TECSUP_VIRTUAL";
			document.getElementById("frmMain").submit();
			break;
		case document.getElementById("txhCodPCC_CC").value:
			document.getElementById("txhOperacion").value = "PCC_CC";
			document.getElementById("frmMain").submit();
			break;
	}	
}

    function fc_CursoPFR(){
		document.getElementById("txhOperacion").value = "CursoPFR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_EspecialidadPFR(){
		document.getElementById("txhOperacion").value = "EspecialidadPFR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_EspecialidadBDO(){
		document.getElementById("txhOperacion").value = "EspecialidadBDO";
		document.getElementById("frmMain").submit();
	}
	
	function fc_CicloPFR(){		
		$('txhOperacion').value = 'CicloPFR';
		
		//JHPR: 2/Abril/08 Para Filtrar Solo Cursos De Cargo o Mostrar Todos
		if (document.getElementById("chkVerCargos").checked==true){ 
			document.getElementById("txhVerCargos").value = "SI";
		}else{
			document.getElementById("txhVerCargos").value = "";
		}
		//JHPR: 2/Abril/08 Validando que se haya seleccionado al menos una especialidad...
						
		//if (document.getElementById("codEspecialidadPFR").options[document.getElementById("codEspecialidadPFR").selectedIndex].value!="") {
		if($F('codSelEspecialidadPFR')!=''){
			$('frmMain').submit();
		}		
	}
		

	function fc_CicloBDO(){		
		$('txhOperacion').value = 'CicloBDO';									
		if($F('codSelEspecialidadBDO')!=''){
			$('frmMain').submit();
		}		
	}		
	
	//JHPR: 1/Abril/08 Para mostrar solamente los cursos cargos...
	/*
	function fc_CicloPFR_Cargo() {			
		var indice = document.getElementById("codSelCicloPFR").selectedIndex
		document.getElementById("codSelCicloPFR").options[indice].value;				
		if (document.getElementById("codSelCicloPFR").options[indice].value!="") {
			//document.getElementById("txhOperacion").value = "CicloPRFCargo";
			//document.getElementById("frmMain").submit();
			if (document.getElementById("chkVerCargos").checked==true) {
				document.getElementById("txhOperacion").value = "CicloPRFCargo";
				txhVerCargos
				alert("Buscar Listar Solo Cargos");			
			}else{
				alert("Buscar Listar Todos");
			}
		}
	}
	*/
	
	function fc_CursoCAT(){
	document.getElementById("txhOperacion").value = "CursoCAT";
	document.getElementById("frmMain").submit();
	}
	
	function fc_CicloCAT(){
	document.getElementById("txhOperacion").value = "CicloCAT";
	document.getElementById("frmMain").submit();
	}
	function fc_ProgramaPE(){
	document.getElementById("txhOperacion").value = "ProgramaPE";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPE(){
	document.getElementById("txhOperacion").value = "CursoPE";
	document.getElementById("frmMain").submit();
	}
	function fc_ModuloPE(){
	document.getElementById("txhOperacion").value = "ModuloPE";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPAT(){
	document.getElementById("txhOperacion").value = "CursoPAT";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPIA(){
	document.getElementById("txhOperacion").value = "CursoPIA";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoHibrido(){
	document.getElementById("txhOperacion").value = "CursoHibrido";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoTECSUPVIRTUAL(){
	document.getElementById("txhOperacion").value = "CursoTecsupVirtual";
	document.getElementById("frmMain").submit();
	}
	function fc_CursoPCC1()
	{ if(document.getElementById("codProducto").value == document.getElementById("txhCodPCC_I").value)
	  	{	document.getElementById("txhOperacion").value = "CursoPCC_I";
	  		document.getElementById("frmMain").submit();
	  	}
	  else{ if(document.getElementById("codProducto").value == document.getElementById("txhCodPCC_CC").value) 
	        { document.getElementById("txhOperacion").value = "CursoPCC_CC";
	  		  document.getElementById("frmMain").submit();
	        }
	  }
	}
	
	function fc_ProgramaANDCursoPCC1(){
	if(document.getElementById("txhProgramaPCCEtapa").value =="1")
	   { if((document.getElementById("codSelProgramaPCC").value!="-1")&&(document.getElementById("codSelCursoPCC").value!="-1"))
	       {document.getElementById("txhOperacion").value = "ProgramaANDCursoPCC1";
		    document.getElementById("frmMain").submit();
		   }
	   }
	else{ if(document.getElementById("txhProgramaPCCEtapa").value =="2")
	      {document.getElementById("txhOperacion").value = "CursoPCC1";
		    document.getElementById("frmMain").submit();
	      }
	    }   
	}
	
	function fc_ProgramaIntegralPCC1(){
	  document.getElementById("txhOperacion").value = "ProgramaIntegralPCC";
	  document.getElementById("frmMain").submit();
	}
	
function fc_ValidarPFR(){
   
   if(document.getElementById("codSelEspecialidadPFR").value!="-1") 
     {  mad="1";
        if(document.getElementById("codSelCicloPFR").value!="-1" )
          { mad="1";
            if(document.getElementById("codSelCursoPFR").value=="-1"){  alert('Debe seleccionar un curso.');
             mad="0";}
          }
        else{ alert('Debe seleccionar un ciclo.');
              mad="0";}
           
     }
   else{ alert('Debe selecionar una especialidad.');  
         mad="0"; }
       
    return mad;     
}	

function fc_ValidarPCC1(){
   
    if(document.getElementById("txhProgramaPCCEtapa").value == "1") 
     { if(document.getElementById("codSelProgramaPCC").value!="-1" )
       { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
       }
       else{ alert('Debe selecionar un programa.');
             mad="0";}
            
         
       return mad;       
     }
    else 
     {
       mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
       
       return mad;
     
     }
       
}	

function fc_ValidarPCC2(){
    
    if(document.getElementById("txhProgramaPCCEtapa").value =="1") 
     { if(document.getElementById("codSelProgramaPCC").value!="-1" )
       { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
       }
       else{ alert('Debe selecionar un programa.');
             mad="0";}
       return mad;       
     }
    else 
     { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
      
      return mad;
      }
   }	

function fc_ValidarPCC3(){
    if(document.getElementById("txhProgramaPCCEtapa").value =="1") 
     { if(document.getElementById("codSelProgramaPCC").value!="" )/**/
       { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="")/**/{  alert('Debe seleccionar un curso.');
            mad="0";}
       }
       else{ alert('Debe selecionar un programa.');
             mad="0";}
       return mad;       
     }
    else 
     { mad="1";
         if(document.getElementById("codSelCursoPCC").value=="")/**/{  alert('Debe seleccionar un curso.');
            mad="0";}
         return mad;
     }
  }	

function fc_ValidarCAT(){
     if(document.getElementById("codSelCursoCAT").value!="" )/**/
       { mad="1";
         if(document.getElementById("codSelCicloCAT").value=="")/**/{  alert('Debe seleccionar un ciclo.');
            mad="0";}
       }
       else{ alert('Debe seleccionar un curso.');
             mad="0";}
     return mad;     
}	
function fc_ValidarPAT(){
    mad="1";
         if(document.getElementById("codSelCursoPAT").value=="")/**/{  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}	

function fc_ValidarPIA(){
    mad="1";
    if(document.getElementById("codSelCursoPIA").value=="")/**/{  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}	

function fc_ValidarHIBRIDO(){
    mad="1";
    if(document.getElementById("codSelCursoHibrido").value=="")/**/{  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}

function fc_ValidarPE(){
   
   if(document.getElementById("codSelProgramaPE").value!="") /**/
     {  mad="1";
        if(document.getElementById("codSelModuloPE").value!="" )/**/
          { mad="1";
            if(document.getElementById("codSelCursoPE").value=="")/**/{  alert('Debe seleccionar un curso.');
             mad="0";}
          }
        else{ alert('Debe seleccionar un m�dulo.');
              mad="0";}
           
     }
   else{ alert('Debe selecionar un programa.');  
         mad="0"; }  
    return mad;     
}
function fc_ValidarTECSUP_VIRTUAL(){
    mad="1";
         if(document.getElementById("codSelCursoTECSUPVIRTUAL").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}
function fc_ValidarPCC_CC(){
    mad="1";
         if(document.getElementById("codSelCursoPCC").value=="-1"){  alert('Debe seleccionar un curso.');
            mad="0";}
    return mad;     
}	

function fc_seleccion(strCodNIvel1,obj){
	strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
	flag=false;
	if (strCodSel!='')
	{	ArrCodSel = strCodSel.split("|");
		strCodSel = "";
		for (i=0;i<=ArrCodSel.length-2;i++)
		{	if (ArrCodSel[i] == strCodNIvel1)flag = true 
			else strCodSel = strCodSel + ArrCodSel[i]+'|';
			}
	}
	if (!flag)
	{
		strCodSel = strCodSel + strCodNIvel1 + '|';
	}
	
	document.getElementById("txhCodComponentesSeleccionados").value = strCodSel;
	//alert("Hom "+document.getElementById("txhCodComponentesSeleccionados").value);       
}

function fc_GetNumeroSeleccionados()
	{
		strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
		
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			return ArrCodSel.length - 1;
		}
		else return 0;
	}
	
	function fc_SoloUnRegistro()
	{   
		intNumeroSeleccionados = fc_GetNumeroSeleccionados();
		
		if ( intNumeroSeleccionados == 0)	return "";	
		if ( intNumeroSeleccionados > 1)    return "";	
		
		strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
		ArrCodSel = strCodSel.split("|");
		return ArrCodSel[0]; 	
	}
	

	//*****************************************GEMA FUNCIONES
		function fc_Regresar(){
			location.href = "${ctx}/menuEvaluaciones.html";
		}
		
		function fc_Cambia(){
			document.getElementById("txhOperacion").value = "CHANGE";
			document.getElementById("frmMain").submit();
		}
		
		function fc_Ir(obj){
						
			if(document.getElementById('txhCodCurso').value == ""){
				alert(mstrSeleccione);
				return;
			}
			switch(obj){
				case 'subsa':
					
					window.location.href = "${ctx}/evaluaciones/registrarExamenesSubsanacion.html" +
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value +
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value+
											"&txhModo=3" + fc_getParametrosBusqueda();
					break;		 
				case 'extra':
					//CCORDOVA: 03/5/2008
					
					window.location.href = "${ctx}/evaluaciones/registrarExamenesCargo.html" +
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value +
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value+
											"&txhModo=2" + fc_getParametrosBusqueda();//EX_CARGO CCORDOVA
					break;				
				case 'cargo':
					//JHPR: 28/3/2008 para agregar la funcionalidad de registrar notas de examenes de cargo Y se agrego parametro "txhModo".
					window.location.href = "${ctx}/evaluaciones/registrarExamenesCargo.html" +
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value +
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value+
											"&txhModo=1" + fc_getParametrosBusqueda();//EX_CARGO CCORDOVA
					break;
			
				case '1': //examen parcial					
					window.location.href = "${ctx}/evaluaciones/registrarExamenes.html" + 
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value + 
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value+
											"&txhModo=0"+ fc_getParametrosBusqueda();//EX_REGULAR CCORDOVA
					break;
				case 'exfinal': //examen parcial
					window.location.href = "${ctx}/evaluaciones/registrarExamenesFinales.html" + 
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value + 
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value+
											"&txhModo=4"+ fc_getParametrosBusqueda();
					break;
				case 'exrecu': //examen recuperacion
					window.location.href = "${ctx}/evaluaciones/registrarExamenesRecuperacion.html" + 
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value + 
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value + 
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value+
											"&txhModo=5"+ fc_getParametrosBusqueda();
					break;											
				case '2':
					window.location.href = "${ctx}/evaluaciones/bandeja_incidencia_adm.html" +
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value +  
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value +
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value;
					break;
				case '3':					
					window.location.href = "${ctx}/evaluaciones/bandeja_exclusiones_adm.html" + 
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value + 
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value +
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value;
											//+ document.getElementById('txhCodPeriodo').value
											//+ document.getElementById('txhCodEvaluador').value 
											//document.getElementById('txhCodCurso').value;
															
					break;
				case 'levantardi':
					window.location.href = "${ctx}/evaluaciones/bandeja_levantardi_adm.html" +
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodoVig').value +  
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value +
											"&txhCodCurso=" + document.getElementById('txhCodCurso').value;
					break;
			}
		}
		
		function fc_seleccionarRegistro(strCurso){
			document.getElementById('txhCodCurso').value = fc_Trim(strCurso);
		}

	
	</script>
</head>

<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/bandeja_gest_administrativa.html">
 
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codCursoSel" id="txhCodCurso"/>
	<form:hidden path="codUsuario" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodoVig" id="txhCodPeriodoVig"/>
	<!-- nuevos -->
	<form:hidden path="tipoProducto" id="txhTipoProducto" />
	<form:hidden path="programaPCCEtapa" id="txhProgramaPCCEtapa" />
	<form:hidden path="codComponentesSeleccionados" id="txhCodComponentesSeleccionados" />
	<form:hidden path="valorEtapa" id="txhValorEtapa" />
	
	<!-- copiado de renzo --> 
	<form:hidden path="codPFR" id="txhCodPFR" />
	<form:hidden path="codPCC_I" id="txhCodPCC_I" />
	<form:hidden path="codPCC_CC" id="txhCodPCC_CC" />
	<form:hidden path="codCAT" id="txhCodCAT" />
	<form:hidden path="codPAT" id="txhCodPAT" />
	<form:hidden path="codPIA" id="txhCodPIA"/>
	<form:hidden path="codPE" id="txhCodPE"/>
	<form:hidden path="codHIBRIDO" id="txhCodHIBRIDO" />
	<form:hidden path="codTecsupVirtual" id="txhCodTecsupVirtual"/>
	<form:hidden path="codEtapaProgramaIntegral" id="txhCodEtapaProgramaIntegral"/>
	<form:hidden path="codEtapaCursoCorto" id="txhCodEtapaCursoCorto"/>
	<!-- /copiado de renzo -->
	
	<!-- Mostrar Cargos -->
	<!-- JHPR: 2/Abril/08 Para Filtrar los cursos cargos en la bandeja principal -->
	<form:hidden path="verCargos" id="txhVerCargos"/>

	<input type="hidden" name="nombreOperacion" id="nombreOperacion" value="<c:out value="${control.operacion}"/>"/>

	<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
			</tr>
		</table>
	
	<table  class="borde_tabla" style="margin-left:9px;width:94%;margin-top:6px; " cellspacing="0" cellpadding="0" >

		<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%" colspan="2">
				Gesti�n Administrativa
			</td>		 	
		</tr>

		<tr class="fondo_dato_celeste">
			<td width="20%">&nbsp;Per�odo Vigente :</td>
			<td><form:input path="dscPeriodoVig" id="txtPeriodo" cssClass="cajatexto_1" readonly="true" cssStyle="width:180px;"/></td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td >&nbsp;Producto:</td>
			<td><form:select path="codProducto" id="codProducto"
				cssClass="cajatexto" cssStyle="width:280px"
				onchange="javascript:fc_Producto(this.value);">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.codListaProducto!=null}">
					<form:options itemValue="codProducto" itemLabel="descripcion"
						items="${control.codListaProducto}" />
				</c:if>
			</form:select></td>
		</tr>
		
		<tr class="fondo_dato_celeste">
			<td colspan="2">
				<table style="display: none;" id="Tabla_ADMISION" name="Tabla_ADMISION" cellpadding="0" border="0" cellspacing="0" width="100%">
					<tr>
						<td width="20%">Tipo de Admisi�n :</td>
						<td align="left" width="80%">
							
							<form:select path="codEspecialidadAdmision" id="codSelEspecialidadAdmision" cssClass="cajatexto" 
										 cssStyle="width:200px"  onchange="javascript:fc_EspecialidadAdmision();">
								<form:option value="">--Seleccione--</form:option>
									
								<c:if test="${control.codListaTiposAdmision!=null}">
									<form:options itemValue="codEspecialidad" itemLabel="descripcion"
										items="${control.codListaTiposAdmision}" />
								</c:if>
							</form:select>

						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		
		<tr class="fondo_dato_celeste">
			<td colspan="2">
				<table style="display: none; width: 100%;" id="Tabla_BDO"
					name = "Tabla_BDO" cellpadding="0" border="0" cellspacing="0">
					<tr>
						<td width="20%">Especialidad :</td>
						<td colspan="3"><form:select path="codEspecialidadBDO"
													   id="codSelEspecialidadBDO" 
							cssClass="cajatexto"
							cssStyle="width:280px"  onchange="javascript:fc_EspecialidadBDO();">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.codListaEspecialidadBDO!=null}">
								<form:options itemValue="codEspecialidad" itemLabel="descripcion"
									items="${control.codListaEspecialidadBDO}" />
							</c:if>
						</form:select></td>
					</tr>
									
					
					<tr>
						<td >&nbsp;Ciclo :&nbsp;</td>
						<td align="left"> 							
							<form:select path="codSelCicloBDO" id="codSelCicloBDO"
							cssClass="cajatexto"  cssStyle="width:94px" onchange="javascript:fc_CicloBDO();">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.codListaCicloBDO!=null}">
								<form:options itemValue="codCiclo" itemLabel="codCiclo"
									items="${control.codListaCicloBDO}" />
							</c:if>
							</form:select>
						</td>											
					</tr>
				</table>
			</td>
		</tr>		
		
		
		<tr class="fondo_dato_celeste">
			<td colspan="2">
				<table style="display: none;" id="Tabla_PFR"
					name = "Tabla_PFR" cellpadding="0" border="0" cellspacing="0" width="100%" 
					>
					<tr>
						<td width="20%">Especialidad :</td>
						<td colspan="3"><form:select path="codEspecialidadPFR"
													   id="codSelEspecialidadPFR" 
							cssClass="cajatexto"
							cssStyle="width:280px"  onchange="javascript:fc_EspecialidadPFR();">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.codListaEspecialidadPFR!=null}">
								<form:options itemValue="codEspecialidad" itemLabel="descripcion"
									items="${control.codListaEspecialidadPFR}" />
							</c:if>
						</form:select></td>
					</tr>
					<tr>
						<td >&nbsp;Ciclo :&nbsp;</td>
						<td align="left"> 
							<form:select path="codSelCicloPFR" id="codSelCicloPFR"
							cssClass="cajatexto"  cssStyle="width:94px" onchange="javascript:fc_CicloPFR();">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.codListaCicloPFR!=null}">
								<form:options itemValue="codCiclo" itemLabel="codCiclo"
									items="${control.codListaCicloPFR}" />
							</c:if>
							</form:select>
						</td>
						<td style="display:none">&nbsp;Cursos :&nbsp;</td>
						<td style="display:none">
							<form:select path="codSelCursoPFR" id="codSelCursoPFR"
								cssClass="cajatexto" cssStyle="width:280px"
								onchange="javascript:fc_CursoPFR();">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.codListaCursoPFR!=null}">
								<form:options itemValue="codCurso" itemLabel="descripcion"
									items="${control.codListaCursoPFR}" />
							</c:if>
						</form:select>
						</td>
					</tr>	
					<tr>
						<!-- //JHPR: 1/Abril/08 Para mostrar solamente los cursos cargos...-->
						<td>�Ver Cargos?</td>
						<td colspan="3">
							<c:if test="${control.verCargos=='SI'}">
								<input type="checkbox" id="chkVerCargos" name="chkVerCargos" class="cajatexto" title="Filtrar ex�menes de Cargo"  onclick="javascript:fc_CicloPFR();" checked="checked">												
							</c:if>
							<c:if test="${control.verCargos=='NO' || control.verCargos==''}">
								<input type="checkbox" id="chkVerCargos" name="chkVerCargos" class="cajatexto" title="Filtrar ex�menes de Cargo"  onclick="javascript:fc_CicloPFR();" >												
							</c:if>
							<!-- input type="checkbox" id="chkVerCargos" name="chkVerCargos" class="cajatexto" title="Filtrar ex�menes de Cargo"  onclick="javascript:fc_CicloPFR();"-->
							<!-- form:checkbox path="verCargos" id="chkVerCargos" cssClass="cajatexto" title="Filtrar ex�menes de Cargo" onclick="javascript:fc_CicloPFR();"/-->
						</td>
					</tr>			
				</table>
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td colspan="2">
			<table style="display: none; width: 100%" id="Tabla_CAT"
				name="Tabla_CAT" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td >&nbsp;Ciclo :&nbsp;</td>
					<td align="left">&nbsp;<form:select path="codSelCicloCAT"
						id="codSelCicloCAT" cssClass="cajatexto" cssStyle="width:94px"
						onchange="javascript:fc_CicloCAT();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaCicloCAT!=null}">
							<form:options itemValue="codCiclo" itemLabel="codCiclo"
								items="${control.codListaCicloCAT}" />
						</c:if>
					</form:select>
					</td>
					<td style="display:none">&nbsp;Cursos :&nbsp;</td>
					<td style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;
						<form:select path="codSelCursoCAT" id="codSelCursoCAT"
							cssClass="cajatexto" cssStyle="width:280px"
							onchange="javascript:fc_CursoCAT();">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.codListaCursoCAT!=null}">
								<form:options itemValue="codCurso" itemLabel="descripcion"
									items="${control.codListaCursoCAT}" />
							</c:if>
						</form:select>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<!-- td colspan="2">
		<table style="display: none; width: 100%" id="Tabla_PCC"
			name="Tabla_PCC" cellpadding="0" cellspacing="0" border="0"
			align="center" >
			<td width="30%">
			<table style="display: none; width: 100%" id="Tabla_PCC_ETAPA"
				name="Tabla_PCC_ETAPA" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<td class="" width="12%">&nbsp;Etapa :</td>					
				<td align="left" width="4%"><input Type="radio" checked name="rdoEtapa"
				    <c:if test="${control.valorEtapa=='1'}"><c:out value=" checked=checked " /></c:if>
					id="gbradio1" onclick="javascript:fc_Etapa('1');"></td>
				<td align="left">&nbsp;Programa Integral&nbsp;<input Type="radio" name="rdoEtapa"
					<c:if test="${control.valorEtapa=='2'}"><c:out value=" checked=checked " /></c:if>
					id="gbradio2" onclick="javascript:fc_Etapa('2');">&nbsp;&nbsp;Cursos Cortos</td>	
					
			</table>
			</td>
		</table>
		</td-->
		
		<tr class="fondo_dato_celeste">
			<td colspan="2" style="width: 100%" id="MADFROG"
				name="MADFROG">
			<table style="display: none; width: 100%"
				id="Tabla_PCC_ProgramaIntegral" name="Tabla_PCC_ProgramaIntegral"
				cellpadding="0" cellspacing="0" border="0" align="left">
				<td align="left" style="display: none; width: 52%" id="Tabla_PCC_PI" name="Tabla_PCC_PI" width="10%" >
					<table class="tabla" cellpadding="0" cellspacing="0" border="0"	align="left" width="5%">
						<tr>										  
						  	<td >&nbsp;Programa Integral :</td>
							<td  align="left">
						    	<form:select
									path="codSelProgramaPCC" id="codSelProgramaPCC"
									cssClass="cajatexto" cssStyle="width:280px"
									onchange="javascript:fc_ProgramaIntegralPCC1();">
									<form:option value="">--Seleccione--</form:option>
									<c:if test="${control.codListaProgramaPCC!=null}">
										<form:options itemValue="codPrograma" itemLabel="descripcion"
											items="${control.codListaProgramaPCC}" />
										<!-- itemValue="codProducto" es el codigo interno q se guarda
											        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
								    </c:if>
						  		</form:select>
						  	</td>
						 </tr>					  
					</table>
				</td>
				<td width="80%">
				<table style="display:none; width: 300%" id="Tabla_PCC_Curso"
					name="Tabla_PCC_Curso" cellpadding="0" cellspacing="0" border="0"
					align="left">
					<td width="4%" style="display:none">&nbsp;Cursos :&nbsp;</td>
					<td align="left" style="display:none">&nbsp;<form:select path="codSelCursoPCC" id="codSelCursoPCC"
						cssClass="cajatexto" cssStyle="width:280px"
						onchange="javascript:fc_CursoPCC1();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPCC!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPCC}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
						        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</table>
				</td>
			</table>
			</td>
		</tr>           
		<tr class="fondo_dato_celeste">
			<td colspan="2">
			<table style="display: none; width: 100%"
				id="Tabla_PROGRA_ESPECIALIZADO" name="Tabla_PROGRA_ESPECIALIZADO"
				cellpadding="2" cellspacing="0" border="0" align="center">
				<tr>
					<td width="12%">&nbsp;Programa :&nbsp;&nbsp;</td>
					<td>&nbsp;<form:select path="codSelProgramaPE" id="codSelProgramaPE"
						cssClass="cajatexto" cssStyle="width:280px" onchange="javascript:fc_ProgramaPE();" >
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaProgramaPE!=null}">
							<form:options itemValue="codPrograma" itemLabel="descripcion"
								items="${control.codListaProgramaPE}" />
						</c:if>
					</form:select></td>
				</tr>
				<tr>
					<td width="12%" align="left">&nbsp;M&oacute;dulo :&nbsp;&nbsp;</td>
					<td align="left">&nbsp;<form:select path="codSelModuloPE" id="codSelModuloPE"
						cssClass="cajatexto" cssStyle="width:280px"
						onchange="javascript:fc_ModuloPE();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaModuloPE!=null}">
							<form:options itemValue="codCiclo" itemLabel="codCiclo"
								items="${control.codListaModuloPE}" />
						</c:if>
					</form:select></td>
					<td width="10%" align="right" style="display:none">&nbsp;Cursos5s :&nbsp;&nbsp;</td>
					<td style="display:none"><form:select path="codSelCursoPE" id="codSelCursoPE"
						cssClass="cajatexto" cssStyle="width:260px"
						onchange="javascript:fc_CursoPE();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPE!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPE}" />
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td colspan="2">
			<table style="display: none; width: 100%" id="Tabla_PIA"
				name="Tabla_PIA" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td width="12%" style="display:none">&nbsp;Cursos :&nbsp;&nbsp;</td>
					<td style="display:none"><form:select path="codSelCursoPIA" id="codSelCursoPIA"
						cssClass="cajatexto" cssStyle="width:280px"
						onchange="javascript:fc_CursoPIA();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPIA!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPIA}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
							        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td colspan="2">
			<table style="display: none; width: 100%" id="Tabla_PAT"
				name="Tabla_PAT" cellpadding="0" cellspacing="0" border="0"
				align="center">
				<tr>
					<td width="12%" style="display:none">Cursos :&nbsp;&nbsp;</td>
					<td style="display:none"><form:select path="codSelCursoPAT" id="codSelCursoPAT"
						cssClass="cajatexto" cssStyle="width:280px"
						onchange="javascript:fc_CursoPAT();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaCursoPAT!=null}">
							<form:options itemValue="codCurso" itemLabel="descripcion"
								items="${control.codListaCursoPAT}" />
							<!-- itemValue="codProducto" es el codigo interno q se guarda
							        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
						</c:if>
					</form:select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td colspan="2">
				<table style="display: none; width: 100%" id="Tabla_HIBRIDO"
					name="Tabla_HIBRIDO" cellpadding="0" cellspacing="0" border="0"
					align="center">
					<tr>
						<td width="12%" style="display:none">&nbsp;Curso8s :&nbsp;&nbsp;</td>
						<td style="display:none">&nbsp;<form:select path="codSelCursoHibrido"
							id="codSelCursoHibrido" cssClass="cajatexto"
							cssStyle="width:280px" onchange="javascript:fc_CursoHibrido();">
							<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.codListaCursoHibrido!=null}">
									<form:options itemValue="codCurso" itemLabel="descripcion"
										items="${control.codListaCursoHibrido}" />
									<!-- itemValue="codProducto" es el codigo interno q se guarda
										        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
								</c:if>
							</form:select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td colspan="2">
				<table style="display: none; width: 100%" id="Tabla_TECSUP_VIRTUAL"
					name="Tabla_TECSUP_VIRTUAL" cellpadding="0" cellspacing="0" border="0"
					align="center">
					<tr>
						<td class="" width="12%" style="display:none">&nbsp;Cursos:&nbsp;&nbsp;</td>
						<td style="display:none">&nbsp;<form:select path="codSelCursoTECSUPVIRTUAL" id="codSelCursoTECSUPVIRTUAL"
							cssClass="cajatexto" cssStyle="width:260px"
							onchange="javascript:fc_CursoTECSUPVIRTUAL();">
							<form:option value="-1">--Seleccione--</form:option>
							<c:if test="${control.codListaCursoTECSUPVIRTUAL!=null}">
								<form:options itemValue="codCurso" itemLabel="descripcion"
									items="${control.codListaCursoTECSUPVIRTUAL}" />
								<!-- itemValue="codProducto" es el codigo interno q se guarda
								        		  itemLabel="descripcion" es la descripcion a mostrar osea el valor q tiene el objeto de la clase Periodo-->
							</c:if>
							</form:select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		 
		<table ><tr><td style="height:4px"></td></tr></table>
		<table cellpadding="0" cellspacing="0" style="margin-left:9px;width:97%" class="borde_tabla">
			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="1">
					Relaci&oacute;n de Cursos Asignados en el Per&iacute;odo
				</td>		 	
			</tr>			
			<tr>
				<td>
					<table class="" style="width:100%;margin-top:6px" cellspacing="2" cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td>					
								<div style="OVERFLOW:auto; WIDTH: 99%; HEIGHT: 280px; display: ;" id="tblCursos">
									<display:table name="sessionScope.listaCursos" cellpadding="0" cellspacing="1"
										decorator="com.tecsup.SGA.bean.CursoEvaluadorDecorator" 
										requestURI="" style="border: 1px solid #048BBA;width:98%">
										<display:column property="rbtSelCurso" title="Sel." style="text-align:center; width:3%" />
										<display:column property="descripcion" title="Curso" style="text-align:left;width:37%" />
										<display:column property="dscSistEval" title="Sist. Eval." style="text-align:center;width:6%"/>
										<display:column property="dscEvaluador" title="Evaluador" style="text-align:left;width:44%"/>
										<!-- JHPR: 31/3/2008 Mostrando columna CARGO (Indica si el curso tiene alumnos que lo llevan como Cargo) -->
										<display:column property="cargo" title="�Con Cargo?" style="text-align:left;width:10%"/>
										
										<display:setProperty name="basic.empty.showtable" value="true"  />
										<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
										<display:setProperty name="paging.banner.placement" value="bottom"/>
										<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
										<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
										<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'></span>" />
										<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
										<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
										<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
										<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
										<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
										<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
										<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
									</display:table>
									<% request.getSession().removeAttribute("listaCursos"); %>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		<table cellpadding="0" cellspacing="0" border="0" align="center" style="height:28px;width:98%;margin-top:5px">	
			<tr>
				<td align="center">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgexamenesp','','${ctx}/images/botones/exparcial2n.gif',1)">
					<img alt="Ex�menes Parciales" src="${ctx}/images/botones/exparcial1n.gif" id="imgexamenesp" onclick="javascript:fc_Ir('1');" style="cursor:pointer;"></a>&nbsp;

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgexamenesf','','${ctx}/images/botones/exfinal2n.gif',1)">
					<img alt="Ex�menes Finales" src="${ctx}/images/botones/exfinal1n.gif" id="imgexamenesf" onclick="javascript:fc_Ir('exfinal');" style="cursor:pointer;"></a>&nbsp;
									
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcargo','','${ctx}/images/botones/excargo2n.gif',1)">
					<img alt="Cargo" src="${ctx}/images/botones/excargo1n.gif" id="imgcargo" onclick="javascript:fc_Ir('cargo');" style="cursor:pointer;"></a>&nbsp;

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgsubsanacion','','${ctx}/images/botones/exsubsa2n.gif',1)">
					<img alt="Subsanaci�n" src="${ctx}/images/botones/exsubsa1n.gif" id="imgsubsanacion" onclick="javascript:fc_Ir('subsa');" style="cursor:pointer;"></a>&nbsp;

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgrecup','','${ctx}/images/botones/exrecu2n.gif',1)">
					<img alt="Recuperaci�n" src="${ctx}/images/botones/exrecu1n.gif" id="imgrecup" onclick="javascript:fc_Ir('exrecu');" style="cursor:pointer;"></a>&nbsp;

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgextra','','${ctx}/images/botones/exextra2n.gif',1)">
					<img alt="Extraordinario" src="${ctx}/images/botones/exextra1n.gif" id="imgextra" onclick="javascript:fc_Ir('extra');" style="cursor:pointer;"></a>&nbsp;&nbsp;
																								
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgincidencias','','${ctx}/images/botones/incidencia2n.gif',1)">
					<img alt="Incidencias" src="${ctx}/images/botones/incidencia1n.gif" id="imgincidencias" onclick="javascript:fc_Ir('2');" style="cursor:pointer;"></a>&nbsp;

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgexcluir','','${ctx}/images/botones/excluir2n.gif',1)">
					<img alt="Excluir del Promedio" src="${ctx}/images/botones/excluir1n.gif" id="imgexcluir" onclick="javascript:fc_Ir('3');" style="cursor:pointer;"></a>

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('levantardi','','${ctx}/images/botones/levantarDI2.gif',1)">
					<img alt="Levantar D.I." src="${ctx}/images/botones/levantarDI1.gif" id="levantardi" onclick="javascript:fc_Ir('levantardi');" style="cursor:pointer;"></a>

				</td>
			</tr>
		</table>
</form:form>
<script type="text/javascript" language="javascript">
	document.getElementById("txhOperacion").value = "";
	document.getElementById("txhMsg").value = "";
</script>