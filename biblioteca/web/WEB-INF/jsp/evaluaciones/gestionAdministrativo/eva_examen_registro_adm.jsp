<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script type="text/javascript" type="text/javascript">
		function onLoad(){
			
		}		
		function fc_Generar(){
			//JHPR: 2008-04-24 Generaci�n de reporte en excel
			form = document.forms[0];
			u = "7A";
			var user = "";
			<% 
			if (request.getSession().getAttribute("usuarioSeguridad") != null) {
				UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
			%>
				user = "<%=(usuarioSeguridad.getCodUsuario())%>";
			<%					
			}
			%>									
			url="?opcion=" + u +
				"&periodo=" + document.getElementById("txtPeriodo").value +
				"&nomEval=" + document.getElementById("txtEval").value +
				"&nomEvalSeccion=" + document.getElementById("nomEvaluadorSeccion").value +				
				"&ciclo=" + document.getElementById("txtCiclo").value +
				"&curso=" + document.getElementById("txtCurso").value +
				"&especialidad=" + document.getElementById("txtEspecialidad").value +
				"&sistEval=" + document.getElementById("txtSistEval").value +			
				"&codPeriodo=" + document.getElementById("txhCodPeriodo").value +		
				"&codCurso=" + document.getElementById("txhCodCurso").value +		
				"&codSeccion=" + document.getElementById("cboSeccion").value +		
				"&nombre=" + document.getElementById("txtNombre").value +
				"&apellido=" + document.getElementById("txtApellido").value +
				"&tipoExamen=" + document.getElementById("txhModo").value+
				"&seccion=" + form.cboSeccion.options[form.cboSeccion.selectedIndex].text+
				"&usuario=" + user;
				window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"Reportes","resizable=yes, menubar=yes");	
		}
		
		function fc_Regresar(){
			window.location.href = "${ctx}/evaluaciones/bandeja_gest_administrativa.html?" + 
									"txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
									"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value+
									"&prmOperacion=" + document.getElementById("prmOperacion").value +
									"&prmCodSelCicloPfr=" + document.getElementById("prmCodSelCicloPfr").value +
									"&prmEspecialidadPfr=" + document.getElementById("prmEspecialidadPfr").value +
									"&prmCodProducto=" + document.getElementById("prmCodProducto").value +
									"&prmVerCargos=" + document.getElementById("prmVerCargos").value +
									"&prmCodPeriodoVig=" + document.getElementById("prmCodPeriodoVig").value+
									"&regreso=1";
		}
		
		function fc_NP(fil, col){		
			//alert("estado:" + document.getElementById("txtNP" + fil + "_" + col).checked);		
			if(document.getElementById("txtNP" + fil + "_" + col).checked == true){
				document.getElementById("txtNota" + fil + "_" + col).value = "0";
				document.getElementById("txtNota" + fil + "_" + col).disabled = true;
				document.getElementById("txtAN" + fil + "_" + col).checked = false;
				
				document.getElementById("txtSU" + fil + "_" + col).checked = false;
			}
			else{
				document.getElementById("txtNota" + fil + "_" + col).disabled = false;
				document.getElementById("txtNota" + fil + "_" + col).focus();
				document.getElementById("txtNota" + fil + "_" + col).value = "";
				//JHPR 2008-7-11 quitar check para guardar relacion con subsanacion
				document.getElementById("txtSU" + fil + "_" + col).checked = false;
			}
		}
		
		function fc_SU(fil, col){
			
			if(document.getElementById("txtSU" + fil + "_" + col).checked == true){
				var nombreAlumno = document.getElementById("txhNomAlumno" + fil).value;
				var mensaje = "�Seguro de Habilitar Subsanaci�n para :\n";
				mensaje = mensaje + nombreAlumno+ "?";
				if( !confirm(mensaje)){
					document.getElementById("txtSU" + fil + "_" + col).checked = false;
					return false;
				}
			}
						
			if(document.getElementById("txtSU" + fil + "_" + col).checked == true){
				document.getElementById("txtNota" + fil + "_" + col).value = "0";
				document.getElementById("txtNota" + fil + "_" + col).disabled = true;
				document.getElementById("txtNP" + fil + "_" + col).checked = true;
				document.getElementById("txtAN" + fil + "_" + col).checked = false;
				//buscar check de la otra subsanacion
				if (col==0) {
					if ( document.getElementById("txtSU" + fil + "_1") ){
						document.getElementById("txtSU" + fil + "_1").checked = false;
					}
				}else{
					if (col==1) {
						if ( document.getElementById("txtSU" + fil + "_0") ){
							document.getElementById("txtSU" + fil + "_0").checked = false;
						}						
					}	
				}								
			}else{
				document.getElementById("txtNota" + fil + "_" + col).disabled = false; //Habilita
				document.getElementById("txtNota" + fil + "_" + col).focus();
				document.getElementById("txtNota" + fil + "_" + col).value = "";
			}
		}
		
		function fc_AN(fil, col){
			if(document.getElementById("txtAN" + fil + "_" + col).checked == true){
				document.getElementById("txtNota" + fil + "_" + col).value = "0";
				document.getElementById("txtNota" + fil + "_" + col).disabled = true; //deshabilita
				document.getElementById("txtNP" + fil + "_" + col).checked = false;
				
				document.getElementById("txtSU" + fil + "_" + col).checked = false;
			}
			else{
				document.getElementById("txtNota" + fil + "_" + col).disabled = false; //Habilita
				document.getElementById("txtNota" + fil + "_" + col).focus();
				document.getElementById("txtNota" + fil + "_" + col).value = "";
			}
		}
		
		function fc_Buscar(){
			if(document.getElementById('cboSeccion').value==""){
				alert('Seleccione secci�n.');
				document.getElementById('cboSeccion').focus();
				return false;
			}
			form = document.forms[0];
			form.imggrabar.disabled=true;
			$('txhOperacion').value = 'BUSCAR';
			$('frmMain').submit();
		}
		
		function fc_Grabar(){
			if(fc_getCadena()){
				form = document.forms[0];
				if(confirm(mstrSeguroGrabar)){
					form.imggrabar.disabled=true;
					objCombo = document.getElementById("cboSeccion");
					$('txhOperacion').value = 'GRABAR';
					$('frmMain').submit();
				}else{
					form.imggrabar.disabled=false;	
				}					
			}
		}
		
		function fc_getCadena(){
			var tbl = document.getElementById('tblNotas');
			var nroExams = fc_Trim(document.getElementById('txhCantExams').value);

			for(i=1 ; i < tbl.rows.length+1 ; i++){
				j = i-1;
				str1 = fc_Trim(document.getElementById('txhCodAlumno'+j).value);
					//JHPR 2008-7-11 no considerar SU
					for(k=0; k<=nroExams-1; k++){
						str2 = fc_Trim(document.getElementById('txhType'+k).value); //Ejm: 2328,2329,etc
						str3 = fc_Trim(document.getElementById('txtNota'+j+'_'+k).value);
						//str4=indicador de NP
						//str5=indicador de AN
						if(str3==""){str3="-1"};//si no hay nota pasarle -1	
						if(document.getElementById('txtNP'+j+'_'+k).checked == true){str4 = "1";}
						else {str4 = "0";}						
						if(document.getElementById('txtAN'+j+'_'+k).checked == true){str5 = "1";}
						else {str5 = "0";}	
						
						if (str2=='2328'){ //si es Examen 1 o Examen 2 enviar el valor del check de Subanacion.
							if(document.getElementById('txtSU'+j+'_'+k).checked == true){str6 = "1";}
							else {str6 = "0";}
						}else{														
							if (str2=='2329'){
								if(document.getElementById('txtSU'+j+'_'+k).checked == true){str6 = "1";}
								else {str6 = "0";}
							}else{
								str6="0";						
							}							
						}						
						//strValores = str1 + "$" + str2 + "$" + str3 + "$" + str4 + "$" + str5;
						strValores = str1 + "$" + str2 + "$" + str3 + "$" + str4 + "$" + str5 + "$" + str6;
						document.getElementById('txhCadena').value = document.getElementById('txhCadena').value + 
							strValores + "|";
					}
			}
			
			//alert(document.getElementById('txhCadena').value);
			
			return true;
		}
		
		function fc_ValidaChk(){
			var tbl = document.getElementById('tblNotas');
			var nroExams = fc_Trim(document.getElementById('txhCantExams').value);

			if(tbl.rows.length > 1){
				for(i=1 ; i < tbl.rows.length ; i++){
					j = i-1;
					//JHPR 2008-7-11 no considerar SU
					for(k=0; k<=nroExams-1; k++){
						objFlag = document.getElementById('txtNP'+j+'_'+k);
						objFlagAN = document.getElementById('txtAN'+j+'_'+k);
						objNota = document.getElementById('txtNota'+j+'_'+k);						
						objNota.disabled = false;						
						if(objFlag.checked){
							objNota.disabled = true;
							objFlagAN.checked = false;
						}						
						if(objFlagAN.checked){
							objNota.disabled = true;
							objFlag.checked = false;
						}
					}
				}
			}	
		}
		
		function fc_limpiar(){
			$('txtNombre').value = '';
			$('txtApellido').value = '';
			$('cboSeccion').value = '';
		}

		//CCORDOVA - 20080502 
		function fc_permiteNumerosValEnter(e, strVal1, strVal2){
		
			var nom = "";
			if(e.keyCode == 13){
			
				while (true)
				{
					nom = "txtNota" + strVal1 + "_" + strVal2;
					if ( document.getElementById(nom) == null )
					{
						//Si objecto es nulo puede ser por dos casos
						//Si El indice 1 es mayor o igual que la cantidad de alumnos
						if( Number(document.getElementById("txhCantAlumnos").value) < Number(strVal1)  )
						{
							//Reseteo los contadores para ls siguiente columna.
							strVal1 = 0;
							strVal2 = Number(strVal2) + 1;  
						}
						else
						{
							//El nombre obtenido no existe en la grilla, hemos llegado a su final
							return false;
						}
					}
					else
					{
						//Si el campo existe, tenemos que validar que no este deshabilitado,
						//sino nos vamos al siguiente valor
						if ( document.getElementById(nom).disabled )
						{
							strVal1 = Number(strVal1) + 1;
						}
						else
						{
							document.getElementById(nom).select();
							document.getElementById(nom).focus();
							return true;
						}
					}
				}
			}
		}
		
	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/registrarExamenes.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
			
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEval" id="txhCodEvaluador"/>
		<form:hidden path="codCurso" id="txhCodCurso"/>
		<form:hidden path="nomEvaluadorSeccion" id="nomEvaluadorSeccion"/>
		
		<form:hidden path="cadena" id="txhCadena"/>
		<!-- JHPR: 31/3/2008 Para separar las busquedas de examenes regulares y las de cargo -->
		<form:hidden path="modoExamen" id="txhModo"/>
		
		
		<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" width="97%" style="margin-left:10px">
			<tr>
				<td>
				<table class="borde_tabla" style="width:99%;margin-top:6px" height="110px" cellspacing="0" cellpadding="0" >
					<tr class="fondo_cabecera_azul">		 
					 	<td class="titulo_cabecera" width="100%" colspan="6">
							Registro de Ex&aacute;menes Parciales
						</td>		 	
					</tr>
					<tr class="fondo_dato_celeste">
						<td width="25%">&nbsp;Per&iacute;odo Vigente :</td>
						<td colspan="3"><form:input path="periodo" id="txtPeriodo" cssStyle="text-align:left" size="8" cssClass="cajatexto_1" readonly="true"/></td>
						<td>&nbsp;Ciclo :</td>
						<td><form:input path="ciclo" cssStyle="text-align:left" id="txtCiclo" size="4" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td>&nbsp;Programa :</td>
						<td colspan="3"><form:input path="programa" id="txtPrograma" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td>&nbsp;Especialidad :</td>
						<td><form:input path="especialidad" id="txtEspecialidad" size="50" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td>&nbsp;Curso :</td>
						<td colspan="3"><form:input path="curso" id="txtCurso" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td>&nbsp;Sist. Eval. :</td>
						<td><form:input path="sistEval" id="txtSistEval" size="4" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr class="fondo_dato_celeste">
						<td>&nbsp;Evaluador :</td>
						<td colspan="3"><form:input path="evaluador" id="txtEval" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="texto_bold" >&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
						
			<tr>
				<td valign="top">
					<table class="borde_tabla" style="width:99%;margin-top:2px" cellspacing="0" cellpadding="0">
						<tr class="fondo_cabecera_azul">		 
						 	<td class="titulo_cabecera" width="100%" colspan="6">
								Relaci&oacute;n de Alumnos:
							</td>		 	
						</tr>
						<tr class="fondo_dato_celeste">
							<td>&nbsp;Apell. Paterno :</td>
							<td><form:input path="apellido" id="txtApellido" cssClass="cajatexto" size="35" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Apellido Paterno');"/>&nbsp;</td>							
							<td>&nbsp;Nombre :</td>
							<td><form:input path="nombre" id="txtNombre" cssClass="cajatexto" size="35" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Nombre');"/></td>
							<td>&nbsp;Secci&oacute;n :</td>
							<td>
								<form:select path="seccion" id="cboSeccion" cssClass="cajatexto_o" cssStyle="width:55px" onchange="javascript:fc_Buscar();">
									<form:option value="">--Seleccione--</form:option>
									<c:if test="${control.listaSeccion!=null}">
									<form:options itemValue="codSeccion" itemLabel="dscSeccion" 
										items="${control.listaSeccion}" />
									</c:if>
								</form:select>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
								<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: hand" id="imglimpiar" onclick="javascript:fc_limpiar();" ></a>
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
									<img src="${ctx}/images/iconos/exportarex1.jpg" align="absmiddle" alt="Excel" id="icoExcel" style="cursor:hand" onclick="javascript:fc_Generar();">
								</a>							
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<table id="tblNotas2" style="width:900px;margin-left:11px;border: 0px solid #048BBA;" cellspacing="1" cellpadding="0" border="0" bordercolor="#048BBA">
			<tr height="15px">
				<td class="headtabla" width="4%" align="center">&nbsp;C&oacute;digo</td>
				<td class="headtabla" width="4%" align="center">&nbsp;Cod. Carnet</td>
				<td class="headtabla" width="20%">&nbsp;Alumno</td>				
				<!-- PINTADO DE CABECERA -->
				<c:if test="${control.listaTipoExams!=null}">
					<c:forEach var="objTipo" items="${control.listaTipoExams}" varStatus="loop" >
						<!-- JHPR: 2008-07-10 No mostrar la columna Subsanaci�n -->						
						<c:if test="${objTipo.alias!='ES'}">
							<td class="headtabla" width="10%" align="center">
								<c:out value="${objTipo.descripcion}" />
								<input type="hidden" id="txhType<c:out value="${loop.index}" />" value="<c:out value="${objTipo.codigo}"/>">
							</td>						
						</c:if>
					</c:forEach>					
				</c:if>
			</tr>
		</table>
		<div style="height:280px; width:950px; overflow:auto;mpositioning:FlowLayout">
		<table id="tblNotas" style="width:900px;margin-left:11px;border: 1px solid #048BBA;" cellspacing="1" cellpadding="0" border="0" bordercolor="#048BBA">
				<!-- PINTADO DE EL CUERPO-->
				<c:if test="${control.listaNotas!=null}">
					<c:forEach var="objAlumno" items="${control.listaNotas}" varStatus="loop10" >
						<c:set var="contAlumnos" value="${loop10.index}" />
					</c:forEach>
					<input type="hidden" id="txhCantAlumnos" value="${contAlumnos}"/>						
					<c:set var="cont" value="1"/>
					<c:set var="contExams" value="0"/>
										
					<c:forEach var="obj1" items="${control.listaNotas}" varStatus="loop" >					
						<c:choose>
							<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
							<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
						</c:choose>							
							<td align="center" width="4%">&nbsp;<c:out value="${obj1.codAlumno}"/>
								<input type="hidden" id="txhCodAlumno<c:out value="${loop.index}" />" value="<c:out value="${obj1.codAlumno}"/>">
								<input type="hidden" id="txhNomAlumno<c:out value="${loop.index}" />" value="<c:out value="${obj1.nombre}"/>">
							</td>	
							<td align="center" width="4%">
								&nbsp;<strong><c:out value="${obj1.codCarnet}"/></strong>
							</td>
						
							<td align="left" width="20%">&nbsp;<c:out value="${obj1.nombre}" /></td>	
							
							
							<c:forEach var="objTipo" items="${control.listaTipoExams}" varStatus="loop5" >																
								<c:set var="contExams" value="${loop5.index}" />
							</c:forEach>
							<c:forEach var="objResultado" items="${obj1.resultado}" varStatus="loop4">								
								<c:set var="cont" value="${loop4.index}" />
							</c:forEach>

							<input type="hidden" id="txhCantExams" value="${contExams}"/>

							<%-- 
							<c:out value="Contador (cont):: ${cont}"></c:out>							
							<c:out value="Contador (contExams):: ${contExams}"></c:out>
							--%>
														
							<c:if test="${cont==0 && contExams!=0}">
								<%-- <c:out value="{cont==0}"></c:out>--%>
								<c:forEach var="objLista1" items="${control.listaTipoExams}" varStatus="loop6" >
								<!-- No mostrar columna subsanaci�n. -->
								<%-- 							
								<c:out value="CONT==0"></c:out>
								<c:out value="flagPosSub: ${objLista1.flagPosSub}"></c:out>										
								<c:out value="flagPosPar: ${objLista1.flagPosPar}"></c:out>										
								<c:out value="flagIndExamSub: ${obj1.flagIndExamSub}"></c:out>								
								<c:out value="   -> obj1.tipoExamen: ${obj1.tipoExamen}"></c:out>								
								<c:out value="   -> objLista1.tipoExamen: ${objLista1.tipoExamen}"></c:out>
								--%>
														
								<c:if test="${objLista1.tipoExamen!='ES'}">
																								
									<td  width="10%" align="center">									
										<!-- CAJA DE TEXTO -->
										<input type="text" id="txtNota<c:out value="${loop.index}" />_<c:out value="${loop6.index}" />"  value="" class="cajatexto" style="width:23px;text-align:center" maxlength="2" 
										onkeypress="javascript:fc_ValidaNumero();" 
										onkeydown="javascript:fc_permiteNumerosValEnter(event, '<c:out value="${loop.index+1}" />', '<c:out value="${loop6.index}" />')" onblur="javascript:fc_ValidaNumeroOnBlur(this.id);fc_ValidaNota(this.id);"
										<c:if test="${objLista1.flagRC=='1' && '0'==obj1.flagIndExamRecu}"> readonly="readonly" disabled="true" </c:if>
										<c:if test="${objLista1.flagPosSub=='1' && '0'==obj1.flagIndExamSub}"> readonly="readonly" disabled="true" </c:if>
										<c:if test="${objLista1.flagNP=='1' || objLista1.flagAN }"> readonly="readonly" disabled="true" </c:if>
										<c:if test="${objLista2.flagPosPar=='1' && '0'==obj1.flagIndExamPar }"> readonly="readonly" disabled="true" </c:if>
										 >&nbsp;									 
																			
										<!-- CHEK -->	 
										<input type="checkbox" id="txtNP<c:out value="${loop.index}" />_<c:out value="${loop6.index}" />" 
										onclick="javascript:fc_NP('<c:out value="${loop.index}" />','<c:out value="${loop6.index}" />');"
										<c:if test="${objLista1.flagRC=='1' && '0'==obj1.flagIndExamRecu}"> disabled="disabled" </c:if> 
										<c:if test="${objLista1.flagPosSub=='1' && '0'==obj1.flagIndExamSub}"> disabled="disabled" </c:if>
										<c:if test="${objLista2.flagPosPar=='1' && '0'==obj1.flagIndExamPar }"> disabled="disabled" </c:if>
										>NP&nbsp;
										
										<!-- CHEK -->
										<input type="checkbox" id="txtAN<c:out value="${loop.index}" />_<c:out value="${loop6.index}" />" 
										onclick="javascript:fc_AN('<c:out value="${loop.index}" />','<c:out value="${loop6.index}" />');"
										<c:if test="${objLista1.flagRC=='1' && '0'==obj1.flagIndExamRecu}"> disabled="disabled" </c:if> 
										<c:if test="${objLista1.flagPosSub=='1' && '0'==obj1.flagIndExamSub}"> disabled="disabled" </c:if>
										<c:if test="${objLista2.flagPosPar=='1' && '0'==obj1.flagIndExamPar }"> disabled="disabled" </c:if>
										>AN			
										
										<c:if test="${objLista1.tipoExamen=='E1' || objLista1.tipoExamen=='E2'}">
											<input type="checkbox" id="txtSU<c:out value="${loop.index}" />_<c:out value="${loop6.index}" />" onclick="javascript:fc_SU('<c:out value="${loop.index}" />','<c:out value="${loop6.index}" />')"
											<c:if test="${objLista2.flagSU=='1'}"> checked </c:if>
											<c:if test="${objLista1.flagPosSub=='1' && '0'==obj1.flagIndExamSub}"> disabled="disabled" </c:if>
											/>SU
										</c:if>
																
									</td>
									
								</c:if>
								
								</c:forEach>								
							</c:if>						
							<c:if test="${cont==contExams && contExams!=0}">
								<%-- <c:out value="CONT==contExams"></c:out> --%>								
								<c:forEach var="objLista2" items="${obj1.resultado}" varStatus="loop7" >									
																		
									<c:if test="${objLista2.tipoExamen!='ES'}">									
									<%--  recorre tipos de examenes y los coloca como columnas 
									<c:out value="flagRC ${objLista2.flagRC}"></c:out>
									<c:out value="flagPosSub ${objLista2.flagPosSub}"></c:out>
									<c:out value="flagNP ${objLista2.flagNP}"></c:out>
									<c:out value="flagPosPar ${objLista2.flagPosPar}"></c:out>
									<c:out value="flagAN ${objLista2.flagAN}"></c:out>
									<c:out value="flagIndExamRecu ${obj1.flagIndExamRecu}"></c:out>
									<c:out value="flagIndExamSub ${obj1.flagIndExamSub}"></c:out>
									<c:out value="flagIndExamPar ${obj1.flagIndExamPar}"></c:out>											
									<c:out value="   -> obj1.tipoExamen: ${obj1.tipoExamen}"></c:out>								
									<c:out value="   -> objLista2.tipoExamen: ${objLista2.tipoExamen}"></c:out>
									--%>
													
									<td width="10%" align="center">
										
										<!-- CAJA DE TEXTO -->
										<input type="text" id="txtNota<c:out value="${loop.index}" />_<c:out value="${loop7.index}" />"  value="${objLista2.nota}" class="cajatexto" style="width:23px; text-align:center" maxlength="2" 
											onkeypress="javascript:fc_PermiteNumeros();" 
											onkeydown="javascript:fc_permiteNumerosValEnter(event, '<c:out value="${loop.index+1}" />', '<c:out value="${loop7.index}" />')" 
											onblur="javascript:fc_ValidaNumeroOnBlur(this.id);fc_ValidaNota(this.id);"
											
										<%-- Habilitando caja de texto de nota: --%>										 
										<c:if test="${objLista2.flagRC=='1' && '0'==obj1.flagIndExamRecu }"> readonly="readonly" disabled="true" </c:if>
										<c:if test="${objLista2.flagPosSub=='1' && '0'==obj1.flagIndExamSub }"> readonly="readonly" disabled="true" </c:if>
										<c:if test="${objLista2.flagNP=='1' || objLista2.flagAN}"> disabled="true" </c:if>
										<c:if test="${objLista2.flagPosPar=='1' && '0'==obj1.flagIndExamPar }"> readonly="readonly" disabled="true"</c:if>
										>&nbsp;
										
										<!-- CHEK -->
										<input type="checkbox" id="txtNP<c:out value="${loop.index}" />_<c:out value="${loop7.index}" />" onclick="javascript:fc_NP('<c:out value="${loop.index}" />','<c:out value="${loop7.index}" />');"
										<c:if test="${objLista2.flagRC=='1' && '0'==obj1.flagIndExamRecu }"> disabled="disabled" </c:if>
										<c:if test="${objLista2.flagNP=='1'}"> checked </c:if> 
										<c:if test="${objLista2.flagPosSub=='1' && '0'==obj1.flagIndExamSub }"> disabled="disabled" </c:if> 
										<c:if test="${objLista2.flagPosPar=='1' && '0'==obj1.flagIndExamPar }"> disabled="disabled" </c:if>
										/>NP&nbsp;
										
										<!-- CHEK -->
										<input type="checkbox" id="txtAN<c:out value="${loop.index}" />_<c:out value="${loop7.index}" />" onclick="javascript:fc_AN('<c:out value="${loop.index}" />','<c:out value="${loop7.index}" />');"
										<c:if test="${objLista2.flagRC=='1' && '0'==obj1.flagIndExamRecu }"> disabled="disabled" </c:if>
										<c:if test="${objLista2.flagAN=='1'}"> checked </c:if> 
										<c:if test="${objLista2.flagPosSub=='1' && '0'==obj1.flagIndExamSub }"> disabled="disabled" </c:if> 
										<c:if test="${objLista2.flagPosPar=='1' && '0'==obj1.flagIndExamPar }"> disabled="disabled" </c:if>
										/>AN     
																														
										<c:if test="${(objLista2.tipoExamen=='E1' || objLista2.tipoExamen=='E2')}">
											<input type="checkbox" id="txtSU<c:out value="${loop.index}" />_<c:out value="${loop7.index}" />" onclick="javascript:fc_SU('<c:out value="${loop.index}" />','<c:out value="${loop7.index}" />')"
											<c:if test="${objLista2.flagSU=='1'}"> checked </c:if>
											<c:if test="${objLista2.flagPosSub=='1' && '0'==obj1.flagIndExamSub }"> disabled="disabled" </c:if>
											<c:if test="${objLista2.flagPosSub=='1' && '0'==obj1.flagIndExamSub }"> disabled="disabled" </c:if> 
											<c:if test="${objLista2.flagPosPar=='1' && '0'==obj1.flagIndExamPar }"> disabled="disabled" </c:if>											
											/>SU
										</c:if>										
										
									</td>
									</c:if>
								</c:forEach>

							</c:if>
							
						</tr>
					</c:forEach>
					

										
				</c:if>
				<c:if test="${control.listaNotas==null}">
					<c:set var="contExams" value="0"/>
					<c:forEach var="objTipo" items="${control.listaTipoExams}" varStatus="loop5" >
						<c:set var="contExams" value="${loop5.index}" />
					</c:forEach>
						<c:set var="contExams" value="${contExams+3}" />
					<tr>
						<td colspan="<c:out value="${contExams}"/>" class="tablagrillaglo" align="center">
						No se encontraron registros
						</td>
					</tr>
				</c:if>	
		</table>
		</div>
		<br>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="green" align="center" height="30px" width="98%">	
			<tr>
				<td align="center">					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_Grabar();" id="imggrabar" style="cursor:pointer;"></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar" onclick="javascript:fc_Regresar();" style="cursor:pointer;"></a>					
				</td>
			</tr>
		</table>


	<!-- Par�metros de b�squeda de la P�gina principal -->

	<form:hidden path="prmOperacion" id="prmOperacion" />
	<form:hidden path="prmCodSelCicloPfr" id="prmCodSelCicloPfr" />
	<form:hidden path="prmEspecialidadPfr" id="prmEspecialidadPfr" />
	<form:hidden path="prmCodProducto" id="prmCodProducto" />
	<form:hidden path="prmVerCargos" id="prmVerCargos" />
	<form:hidden path="prmCodPeriodoVig" id="prmCodPeriodoVig" />	
	
	</form:form>
	
	<script type="text/javascript" language="javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case 'ERROR':
					if ( strMsg != ""){
						alert(strMsg);
					}
					break;
				case 'OK':
					if ( strMsg != ""){
						alert(strMsg);
					}
					break;
			}
		}

		if(document.getElementById("txhTypeMessage").value != "NO" && document.getElementById("txhTypeMessage").value != ""){
			fc_ValidaChk();
		}

		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
		document.getElementById("txhCadena").value = "";

	</script>


</body>