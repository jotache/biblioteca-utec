<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<%@page import='com.tecsup.SGA.modelo.Examen'%>
<%@page import='com.tecsup.SGA.modelo.Asistencia'%>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_eva-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script type="text/javascript" type="text/javascript">
		function onLoad(){
		}

		function fc_Generar(){
			form = document.forms[0];
			u = 'REPORTE_EX_CARGO';
			var user = "";
			<% 
			if (request.getSession().getAttribute("usuarioSeguridad") != null) {
				UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
			%>
				user = "<%=(usuarioSeguridad.getCodUsuario())%>";
			<%}%>			
															
			url="?opcion=" + u +
				"&periodo=" + document.getElementById("txtPeriodo").value +
				"&nomEval=" + document.getElementById("txtEval").value +
				"&nomEvalSeccion=" + document.getElementById("txtEval").value +				
				"&ciclo=" + document.getElementById("txtCiclo").value +
				"&curso=" + document.getElementById("txtCurso").value +
				"&especialidad=" + document.getElementById("txtEspecialidad").value +
				"&sistEval=" + document.getElementById("txtSistEval").value +			
				"&codPeriodo=" + document.getElementById("txhCodPeriodo").value +		
				"&codCurso=" + document.getElementById("txhCodCurso").value +		
				"&codSeccion=" + 		
				"&nombre=" + document.getElementById("txtNombre").value +
				"&apellido=" + document.getElementById("txtApellido").value +
				"&tipoExamen=" + document.getElementById("txhModo").value+
				"&seccion=" + 
				"&usuario=" + user;
				window.open("${ctx}/evaluaciones/eva_reporteEva.html"+url,"Reportes","resizable=yes, menubar=yes");			
		}
		
		function fc_Regresar(){
			window.location.href = "${ctx}/evaluaciones/bandeja_gest_administrativa.html?" + 
									"txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
									"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value+
									"&prmOperacion=" + document.getElementById("prmOperacion").value +
									"&prmCodSelCicloPfr=" + document.getElementById("prmCodSelCicloPfr").value +
									"&prmEspecialidadPfr=" + document.getElementById("prmEspecialidadPfr").value +
									"&prmCodProducto=" + document.getElementById("prmCodProducto").value +
									"&prmVerCargos=" + document.getElementById("prmVerCargos").value +
									"&prmCodPeriodoVig=" + document.getElementById("prmCodPeriodoVig").value+
									"&prmCodSelCicloBdo=" + document.getElementById("prmCodSelCicloBdo").value +
									"&prmEspecialidadBdo=" + document.getElementById("prmEspecialidadBdo").value +
									
									"&regreso=1";									
		}
		
		function fc_NP(fil, col){
			if(document.getElementById("txtNP" + fil + "_" + col).checked == true){
				document.getElementById("txtNota" + fil + "_" + col).value = "0";
				document.getElementById("txtNota" + fil + "_" + col).disabled = true;
				document.getElementById("txtAN" + fil + "_" + col).checked = false;
			}
			else{
				document.getElementById("txtNota" + fil + "_" + col).disabled = false;
				document.getElementById("txtNota" + fil + "_" + col).focus();
				document.getElementById("txtNota" + fil + "_" + col).value = "";
			}
		}
		
		function fc_AN(fil, col){
			if(document.getElementById("txtAN" + fil + "_" + col).checked == true){
				document.getElementById("txtNota" + fil + "_" + col).value = "0";
				document.getElementById("txtNota" + fil + "_" + col).disabled = true;
				document.getElementById("txtNP" + fil + "_" + col).checked = false;
			}
			else{
				document.getElementById("txtNota" + fil + "_" + col).disabled = false;
				document.getElementById("txtNota" + fil + "_" + col).focus();
				document.getElementById("txtNota" + fil + "_" + col).value = "";
			}
		}
		
		function fc_Buscar(){
			/*
			if(document.getElementById('cboSeccion').value==""){
				alert(mstrSeleccioneSeccion);
				document.getElementById('cboSeccion').focus();
				return false;
			}
			*/		
			document.getElementById("txhOperacion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_Grabar(){
			if(fc_getCadena()){
				if(confirm(mstrSeguroGrabar)){
					document.getElementById("txhOperacion").value = "GRABAR";
					document.getElementById("frmMain").submit();
				}
			}
		}
		
		function fc_getCadena(){
			var tbl = document.getElementById('tblNotas');
			
			if (document.getElementById('txhCantExams')==null){
				alert('No se encontraron registros')
				return false;
			}
			
			var nroExams = fc_Trim(document.getElementById('txhCantExams').value);

			for(i=1 ; i < tbl.rows.length ; i++){
				j = i-1;
				str1 = fc_Trim(document.getElementById('txhCodAlumno'+j).value);
				
					for(k=0; k<=nroExams; k++){
						str2 = fc_Trim(document.getElementById('txhType'+k).value);
						str3 = fc_Trim(document.getElementById('txtNota'+j+'_'+k).value);
						
						if(str3==""){str3="-1"};
						
						if(document.getElementById('txtNP'+j+'_'+k).checked == true){str4 = "1";}
						else {str4 = "0";}
						
						if(document.getElementById('txtAN'+j+'_'+k).checked == true){str5 = "1";}
						else {str5 = "0";}
						
						strValores = str1 + "$" + str2 + "$" + str3 + "$" + str4 + "$" + str5;

						document.getElementById('txhCadena').value = document.getElementById('txhCadena').value + 
							strValores + "|";
					}
			}
			//alert(document.getElementById('txhCadena').value);
			return true;
		}
		
		function fc_ValidaChk(){
			var tbl = document.getElementById('tblNotas');
			var nroExams = fc_Trim(document.getElementById('txhCantExams').value);

			if(tbl.rows.length > 1){
				for(i=1 ; i < tbl.rows.length ; i++){
					j = i-1;
					for(k=0; k<=nroExams; k++){
						objFlag = document.getElementById('txtNP'+j+'_'+k);
						objFlagAN = document.getElementById('txtAN'+j+'_'+k);
						objNota = document.getElementById('txtNota'+j+'_'+k);
						
						objNota.disabled = false;
						
						if(objFlag.checked){
							objNota.disabled = true;
							objFlagAN.checked = false;
						}
						
						if(objFlagAN.checked){
							objNota.disabled = true;
							objFlag.checked = false;
						}
					}
				}
			}	
		}
		
		function fc_limpiar(){
			document.getElementById("txtNombre").value = "";
			document.getElementById("txtApellido").value = "";
			//document.getElementById("cboSeccion").value = "";
		}
		function fc_permiteNumerosValEnter(e, strVal1, strVal2){
		
			var nom = "";
			if(e.keyCode == 13){
			
				while (true)
				{
					nom = "txtNota" + strVal1 + "_" + strVal2;
					if ( document.getElementById(nom) == null )
					{
						//Si objecto es nulo puede ser por dos casos
						//Si El indice 1 es mayor o igual que la cantidad de alumnos
						if( Number(document.getElementById("txhCantAlumnos").value) < Number(strVal1)  )
						{
							//Reseteo los contadores para ls siguiente columna.
							strVal1 = 0;
							strVal2 = Number(strVal2) + 1;  
						}
						else
						{
							//El nombre obtenido no existe en la grilla, hemos llegado a su final
							return false;
						}
					}
					else
					{
						//Si el campo existe, tenemos que validar que no este deshabilitado,
						//sino nos vamos al siguiente valor
						if ( document.getElementById(nom).disabled )
						{
							strVal1 = Number(strVal1) + 1;
						}
						else
						{
							document.getElementById(nom).focus();
							return true;
						}
					}
				}
			}
		}
		
	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/registrarExamenesCargo.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
			
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEval" id="txhCodEvaluador"/>
		<form:hidden path="codCurso" id="txhCodCurso"/>
		
		<form:hidden path="cadena" id="txhCadena"/>
		<!-- JHPR: 31/3/2008 Para separar las busquedas de examenes regulares y las de cargo -->
		<form:hidden path="modoExamen" id="txhModo"/>
		<!--T�tulo de la P�gina  -->
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:8px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="765px" class="opc_combo" id="tdTitulo">
			 		<font style="">Registro de Ex&aacute;menes de Cargo</font></td>
			 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			 </tr>
		 </table>
		<table cellpadding="2" cellspacing="0" border="0" bordercolor="#eae9e9" width="97%" style="margin-left:10px">
			<tr>
				<td>
				<table class="tabla" style="width:99%;margin-top:6px" height="110px" background="${ctx}/images/Evaluaciones/back.jpg" cellspacing="2" cellpadding="0" border="0" bordercolor="red">
					<tr>
						<td class="" width="15%">&nbsp;Per&iacute;odo Vigente :</td>
						<td colspan="3"><form:input path="periodo" id="txtPeriodo" cssStyle="text-align:left" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="" >&nbsp;Ciclo</td>
						<td><form:input path="ciclo" cssStyle="text-align:left" id="txtCiclo" size="4" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr>
						<td class="" >&nbsp;Programa :</td>
						<td colspan="3"><form:input path="programa" id="txtPrograma" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="" >&nbsp;Especialidad :</td>
						<td ><form:input path="especialidad" id="txtEspecialidad" size="50" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr>
						<td class="" >&nbsp;Curso :</td>
						<td colspan="3"><form:input path="curso" id="txtCurso" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="" >&nbsp;Sist. Eval. :</td>
						<td><form:input path="sistEval" id="txtSistEval" size="4" cssClass="cajatexto_1" readonly="true"/></td>
					</tr>
					<tr>
						<td class="">&nbsp;Evaluador :</td>
						<td colspan="3"><form:input path="evaluador" id="txtEval" size="50" cssClass="cajatexto_1" readonly="true"/></td>
						<td class="texto_bold" >&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr style="height:20px;">
				<td>
					<!-- Titulo de la bandeja -->
					 <table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-top:5px">
						 <tr>
						 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
						 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="200px" class="opc_combo"><font style="">Relaci�n de Alumnos</font></td>
						 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
						 </tr>
					 </table>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table class="tabla" height="30px" background="${ctx}/images/Evaluaciones/back.jpg" style="width:99%;margin-top:2px" cellspacing="2" cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td class="" >&nbsp;Apell. Paterno :</td>
							<td><form:input path="apellido" id="txtApellido" cssClass="cajatexto" size="35" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Apellido Paterno');"/>&nbsp;</td>
							<td class="">&nbsp;Nombre :</td>
							<td><form:input path="nombre" id="txtNombre" cssClass="cajatexto" size="35" onkeypress="javascript:fc_ValidaTextoEspecial();" onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Nombre');"/></td>
							<!-- td class="" >&nbsp;Secci&oacute;n :</td-->
							<td>
							<!-- 
							<form:select path="seccion" id="cboSeccion" cssClass="cajatexto_o" cssStyle="width:40px">
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.listaSeccion!=null}">
								<form:options itemValue="codSeccion" itemLabel="dscSeccion" 
									items="${control.listaSeccion}" />
								</c:if>
							</form:select>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							 -->
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: hand" id="imglimpiar" onclick="javascript:fc_limpiar();" ></a>

							<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
								<img src="${ctx}/images/iconos/exportarex1.jpg" align="absmiddle" alt="Excel" id="icoExcel" style="cursor:hand" onclick="javascript:fc_Generar();">
							</a>

							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div style="height:200px; width:978px; overflow:auto;mpositioning:FlowLayout">
		<table id="tblNotas" style="width:950px;margin-left:11px;border: 1px solid #048BBA;" cellspacing="1" cellpadding="0" border="0" bordercolor="#048BBA">
			<tr height="20px">
				<td class="headtabla" width="5%" align="center">&nbsp;C&oacute;digo</td>
				<td class="headtabla" width="4%" align="center">&nbsp;Cod. Carnet</td>
				<td class="headtabla" width="30%">&nbsp;Alumno</td>
				<!-- PINTADO DE CABECERA -->
				<c:if test="${control.listaTipoExams!=null}">
					<c:forEach var="objTipo" items="${control.listaTipoExams}" varStatus="loop" >
						<td class="headtabla" width="10%" align="center">
							&nbsp;<c:out value="${objTipo.descripcion}" />
						<input type="hidden" id="txhType<c:out value="${loop.index}" />" value="<c:out value="${objTipo.codigo}"/>">
						</td>
					</c:forEach>
				</c:if>
				<!-- td class="grilla" width="10%">				
							Examen Cargo  -->
							<!-- JHPR: 1/Abril/2008 enviar EC para capturar el valor de tipo de cargo -->
				<!-- 
						<input type="hidden" id="txhType0" value="2328" --> 
						<!-- Examen de cargo: obtengo el tipo de Examenes de Maestra ('TIPO_EXAMENES') -->
				<!-- /td> -->
			</tr>
			
				<!-- PINTADO DE EL CUERPO-->
				<c:if test="${control.listaNotas!=null}">
					<c:forEach var="objAlumno" items="${control.listaNotas}" varStatus="loop10" >
						<c:set var="contAlumnos" value="${loop10.index}" />
					</c:forEach>
					<input type="hidden" id="txhCantAlumnos" value="${contAlumnos}"/>
						
					<c:set var="cont" value="1"/>
					<c:set var="contExams" value="0"/>
					<c:forEach var="obj1" items="${control.listaNotas}" varStatus="loop" >
						
						<c:choose>
							<c:when test="${loop.count % 2 ==0}"><tr class="fondoceldablanco"></c:when>
							<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
						</c:choose>
												
							<td align="center" >&nbsp;<c:out value="${obj1.codAlumno}"/>
							<input type="hidden" id="txhCodAlumno<c:out value="${loop.index}" />" value="<c:out value="${obj1.codAlumno}"/>">
							</td>	
							<td align="center" width="4%">
								&nbsp;<strong><c:out value="${obj1.codCarnet}"/></strong>
							</td>
							<td align="left" >&nbsp;<c:out value="${obj1.nombre}" /></td>					
							<c:forEach var="objTipo" items="${control.listaTipoExams}" varStatus="loop5" >
								<c:set var="contExams" value="${loop5.index}" />
							</c:forEach>
							<c:forEach var="objResultado" items="${obj1.resultado}" varStatus="loop4">
								<c:set var="cont" value="${loop4.index}" />
							</c:forEach>
							<input type="hidden" id="txhCantExams" value="${contExams}"/>

							<c:if test="${cont==contExams}">

								<c:forEach var="objLista2" items="${obj1.resultado}" varStatus="loop7" >
								<td width="10%" align="center">
									<input type="text" id="txtNota<c:out value="${loop.index}" />_<c:out value="${loop7.index}" />"  value="${objLista2.nota}" class="cajatexto" style="width:23px" maxlength="2" 
									onkeypress="javascript:fc_PermiteNumeros();" 
									onkeydown="javascript:fc_permiteNumerosValEnter(event, '<c:out value="${loop.index+1}" />', '<c:out value="${loop7.index}" />')"
									onblur="javascript:fc_ValidaNumeroOnBlur(this.id);fc_ValidaNota(this.id);" 
									style="text-align:center">&nbsp;
									<input type="checkbox" id="txtNP<c:out value="${loop.index}" />_<c:out value="${loop7.index}" />" onclick="javascript:fc_NP('<c:out value="${loop.index}" />','<c:out value="${loop7.index}" />');"
									<c:if test="${objLista2.flagNP=='1'}"> checked </c:if>>NP&nbsp;
									<input type="checkbox" id="txtAN<c:out value="${loop.index}" />_<c:out value="${loop7.index}" />" onclick="javascript:fc_AN('<c:out value="${loop.index}" />','<c:out value="${loop7.index}" />');"
									<c:if test="${objLista2.flagAN=='1'}"> checked </c:if>>AN
								</td>
								</c:forEach>
							</c:if>									
						</tr>
					</c:forEach>
				</c:if>
				<c:if test="${control.listaNotas==null}">
					<c:set var="contExams" value="0"/>
					<c:forEach var="objTipo" items="${control.listaTipoExams}" varStatus="loop5" >
						<c:set var="contExams" value="${loop5.index}" />
					</c:forEach>
						<c:set var="contExams" value="${contExams+3}" />
					<tr>
						<td colspan="<c:out value="${contExams}"/>" class="tablagrillaglo" align="center">
						No se encontraron registros
						</td>
					</tr>
				</c:if>	
		</table>
		</div>
		<br>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="green" align="center" height="30px" width="98%">	
			<tr>
				<td align="center">					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_Grabar();" id="imggrabar" style="cursor:pointer;"></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar" onclick="javascript:fc_Regresar();" style="cursor:pointer;"></a>					
				</td>
			</tr>
		</table>

	<!-- Par�metros de b�squeda de la P�gina principal -->
	<form:hidden path="prmOperacion" id="prmOperacion" />
	<form:hidden path="prmCodSelCicloPfr" id="prmCodSelCicloPfr" />
	<form:hidden path="prmEspecialidadPfr" id="prmEspecialidadPfr" />
	<form:hidden path="prmCodProducto" id="prmCodProducto" />
	<form:hidden path="prmVerCargos" id="prmVerCargos" />
	<form:hidden path="prmCodPeriodoVig" id="prmCodPeriodoVig" />

	<form:hidden path="prmCodSelCicloBdo" id="prmCodSelCicloBdo" />
	<form:hidden path="prmEspecialidadBdo" id="prmEspecialidadBdo" />	
	<form:hidden path="prmVerCargos" id="prmVerCargos" />
	<form:hidden path="prmCodPeriodoVig" id="prmCodPeriodoVig" />

	</form:form>
	
	<script type="text/javascript">
	
		if ( document.getElementById("txhModo").value == "1")
			document.getElementById("tdTitulo").innerHTML = "Registro de Ex&aacute;menes de Cargo";
		else document.getElementById("tdTitulo").innerHTML = "Registro de Ex&aacute;menes Extraordinario";
		
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case 'ERROR':
					if ( strMsg != ""){
						alert(strMsg);
					}
					break;
				case 'OK':
					if ( strMsg != ""){
						alert(strMsg);
					}
					break;
			}
		}
		
		if(document.getElementById("txhTypeMessage").value != "NO" && document.getElementById("txhTypeMessage").value != ""){
			fc_ValidaChk();
		}
		
		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
		document.getElementById('txhCadena').value = "";

	</script>
	
</body>