<%@ include file="/taglibs.jsp"%>
<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter"/>
	<script src="${ctx}/scripts/js/funciones_eva-fechas.js" language="JavaScript;"	type="text/JavaScript"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){}
		function fc_Grabar()
		{
			objFecha = document.getElementById("txtFecIncidencia");
			objFecAct = document.getElementById("txhFecActual");
			objInc = document.getElementById("txaDscIncidencia");
			
			if ( fc_Trim(objFecha.value) == "")
			{
				alert("Debe ingresar el campo fecha.");
				return false;
			}
			if (Fc_RestaFechas(objFecha.value,"dd/MM/yyyy",objFecAct.value,"dd/MM/yyyy") < 0 )
			{
				alert("La fecha ingresada debe ser menor o igual a la actual.");
				objFecha.focus();
				return false;
			}
			if ( fc_Trim(document.getElementById("cboTipoIncidencia").value) == "0002" )
			{
				objFecIni = document.getElementById("txtFecIni");
				objFecFin = document.getElementById("txtFecFin");
				
				if ( fc_Trim(objFecIni.value) == "")
				{
					alert("Debe ingresar el campo fecha inicio.");
					return false;
				}
				if ( fc_Trim(objFecFin.value) == "")
				{
					alert("Debe ingresar el campo fecha fin.");
					return false;
				}
				if (Fc_RestaFechas(objFecIni.value,"dd/MM/yyyy",objFecFin.value,"dd/MM/yyyy") < 0 )
				{
					alert("La fecha inicial debe ser menor que la final.");
					objFecha.focus();
					return false;
				}
			}

			if ( fc_Trim(objInc.value) == "")
			{
				alert("Debe ingresar la descripci�n de la incidencia.");
				return false;
			}

			if ( !confirm(mstrSeguroGrabar) ) return false;
						
			$('txhAccion').value='GRABAR';
			$('frmMain').submit();			
		}
		
		function fc_Eliminar(codIncidencia)
		{
			<% if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("0") ){ %>
				if ( !confirm(mstrSeguroEliminar1) ) return null;
				
				$('txhCodIncidencia').value=codIncidencia;
				$('txhAccion').value='ELIMINAR';
				$('frmMain').submit();
				
			<% } else {%>
				alert("Ud. no pruede realizar esta acci�n.");
			<% }%>
		}
		function fc_Modificar(codIncidencia, fecIncidencia, objImg, codTipoInc, fecIni, fecFin)
		{
			<% if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("0") ){ %>
				document.getElementById("txhCodIncidencia").value = codIncidencia;
				document.getElementById("txtFecIncidencia").value = fecIncidencia;
				objTd = objImg.parentNode;
				objTr = objTd.parentNode;
				objTd = objTr.cells[2];
				objTxa = objTd.firstChild;
				document.getElementById("txaDscIncidencia").value = objTxa.value;
				
				//alert(codTipoInc);
				objCbo = document.getElementById("cboTipoIncidencia");
				objCbo.value = codTipoInc;
				fc_CambiaTipoInc();
				if ( fc_Trim(objCbo.value) == document.getElementById("txhCodIncSancion").value ) 
				{
					document.getElementById("txtFecIni").value = fecIni;
					document.getElementById("txtFecFin").value = fecFin;
				}
			<% } else {%>
				alert("Ud. no pruede realizar esta acci�n.");
			<% }%>
		}
		function fc_Regresar()
		{
			window.location.href = "${ctx}/evaluaciones/bandeja_incidencia_adm.html" + 
									"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
									"&txhCodEvaluador=" + document.getElementById('txhCodEval').value + 
									"&txhCodCurso=" + document.getElementById('txhCodCurso').value +
									"&txhCodProducto=" + document.getElementById('txhCodProducto').value +
									"&txhCodEspecialidad=" + document.getElementById('txhCodEspecialidad').value;
		}
		function fc_CambiaTipoInc()
		{
			objCbo = document.getElementById("cboTipoIncidencia");
			
			if ( fc_Trim(objCbo.value) == document.getElementById("txhCodIncSancion").value )
			{
				document.getElementById("trFecha").style.display = "inline-block";
			}
			else
			{
				document.getElementById("trFecha").style.display = "none";
				document.getElementById("txtFecFin").value = "";
				document.getElementById("txtFecIni").value = "";
			}
		}
		
		function fc_Regresar1()
		{
			url = "${ctx}/evaluaciones/consultarEvaluacionesDetalle.html?"
			+"codCurso=" + document.getElementById("txhCodCurso").value
			+"&codPeriodo=" + document.getElementById("txhCodPeriodo").value
			+"&codProducto=" + document.getElementById("txhCodProducto").value
			+"&codAlumno=" + document.getElementById("txhCodAlumno").value
			+"&txhCodEvaluador=" + document.getElementById("txhCodEval").value
			+"&codCboPeriodo=" + document.getElementById("txhCodPeriodo").value
			+"&codCiclo=" + document.getElementById("txhCodCiclo").value
	        +"&txhIndProcTutor=" + document.getElementById("txhIndProcedencia").value
	        +"&txhCodPeriodoTutor=" + document.getElementById('txhCodPeriodoTutor').value;

			window.location.href=url;
		}
	</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/registrar_incidencia_adm.html" >
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="codCurso" id="txhCodCurso"/>
	<form:hidden path="codIncidencia" id="txhCodIncidencia"/>
	
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codEvaluador" id="txhCodEval"/>
	<form:hidden path="codTipoIncSancion" id="txhCodIncSancion"/>
	
	<form:hidden path="fechaActual" id="txhFecActual"/>
	
	<form:hidden path="codProducto" id="txhCodProducto"/>
	<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
	<form:hidden path="codTipoSesionDefecto" id="txhCodTipoSesion"/>
	<form:hidden path="codSeccion" id="txhCodSeccion"/>
	
	<form:hidden path="codPeriodoTutor" id="txhCodPeriodoTutor"/>
	<form:hidden path="indProcedencia" id="txhIndProcedencia"/>
	<form:hidden path="codCiclo" id="txhCodCiclo"/>
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" class="borde_tabla" style="margin-left:11px; margin-top: 10px;" width="95%">
	 	<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%">
				Registrar Incidencia Administrativa
			</td>		 	
		</tr>
		
		<tr>
			<td>
				<table class="tabla" align="center" style="width:100%;margin-top:0px" background="${ctx}/images/Evaluaciones/back.jpg"	 cellspacing="2" cellpadding="0" border="0" >
					<tr>
						<td class="" >&nbsp;Alumno</td>
						<td>
							<form:input path="nomAlumno" id="txtNombre" cssClass="cajatexto_1" readonly="true" 
								cssStyle="width:200px"/>
						</td>
						<td width="500px">&nbsp;</td>
					</tr>
					<tr>
						<td class="" style="width: 90px">&nbsp;Tipo</td>
						<td width="100px">
							<form:select path="codTipoIncidencia" id="cboTipoIncidencia" cssClass="combo_o"
								cssStyle="width:150px" onchange="javascript:fc_CambiaTipoInc();">
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.listaTipoIncidencia!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listaTipoIncidencia}" />
								</c:if>
							</form:select>
						</td>
						<td class="" >
							&nbsp;Fecha :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<form:input path="fechaIncidencia" id="txtFecIncidencia" cssClass="cajatexto"
								onkeypress="javascript:fc_ValidaFecha('txtFecIncidencia');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecIncidencia');"								  
								cssStyle="width:56px"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecInc','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecInc"
								align="absmiddle" alt="Calendario" style="cursor:pointer;<% if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("1") ){ %>display:none<% }%>"
								></a>
						</td>
					</tr>
					<tr id="trFecha" >
						<td class="">&nbsp;Fecha Inicio :</td>
						<td colspan="3">
							<form:input path="fechaIni" id="txtFecIni" cssClass="cajatexto"
								onkeypress="javascript:fc_ValidaFecha('txtFecIni');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecIni');"  
								cssStyle="width:56px"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
							&nbsp;&nbsp;
							<span class="">Fecha Fin : </span>&nbsp;&nbsp;&nbsp;
							<form:input path="fechaFin" id="txtFecFin" cssClass="cajatexto"
								onkeypress="javascript:fc_ValidaFecha('txtFecFin','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFin');"  
								cssStyle="width:56px"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
						</td>
					</tr>
					<tr>
						<td class="" >&nbsp;Incidencia</td>
						<td  align="left" class="" style="border: 0px" colspan="2">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td rowspan="2">
										<form:textarea id="txaDscIncidencia" path="dscIncidencia" cols="120" rows="4"
											onkeypress="javascript: if (this.value.length > 499) return false;"
											onblur="javascript: if (this.value.length > 499) this.value = this.value.substring(0,499);"
											cssClass="cajatexto"/>
									</td>
									<td>&nbsp;&nbsp;
										<% if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("0") ){ %>
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/grabar2.jpg',1)">
										<img alt="Grabar" src="${ctx}/images/iconos/grabar1.jpg" id="imgAgregar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
										<% } %>
									</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp;Long. Max. 500 caracteres.</td>
								</tr>
							</table>							
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<div style="overflow: auto; height:280px;width:100%" class="">
					<display:table name="sessionScope.listaIncidenciasAdm" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.IncidenciasDecorator" id="tblIncidencias" 
						style="border: 1px solid #048BBA;width:98%">
						<display:column property="fecha" title="Fecha" style="width:8%;text-align:center"/>
						<display:column property="dscTipoIncidencia" title="Tipo" style="width:17%; text-align:left"/>
						<display:column property="textAreaIncidencia" title="Incidencia" style="width:67%" />
						<display:column property="imgEliminarIncidenciaDoc" title="Eli." style="width:4%;text-align:center"/>						
						<display:column property="imgModificarIncidenciaDoc" title="Mod." style="width:4%;text-align:center"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
						<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
						<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
						<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
					</display:table>
					<script type="text/javascript">
						<%
							ArrayList lista = (ArrayList)request.getSession().getAttribute("listaIncidenciasAdm");
							if ( lista.size() > 6 ) {
						%>
							document.getElementById("tblIncidencias").style.width = '98%';
						<% } %>
					</script>
					<% request.getSession().removeAttribute("listaIncidenciasAdm"); %>
				</div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" align="center" height="30px" style="margin-top:5px" width="98%">	
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar1','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar1"
					<% if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("1") ){ %> 
					onclick="javascript:fc_Regresar1();"
					<% } else { %>
					onclick="javascript:fc_Regresar();"
					<% } %> 
				style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecIncidencia",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecInc",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
<script type="text/javascript" language="javascript">
	fc_CambiaTipoInc()
	var strMsg = fc_Trim(document.getElementById("txhMsg").value);
	if ( strMsg != "" )
	{
		if ( strMsg == "ERROR" )alert(mstrProblemaGrabar);
		else if ( strMsg == "OK" )alert(mstrSeGraboConExito);
		else if ( strMsg == "ERROR_ELI" )alert(mstrNoElimino);
		else if ( strMsg == "OK_ELI" )alert(mstrElimino);
	} 
	document.getElementById("txhAccion").value = "";
	document.getElementById("txhMsg").value = "";
	
	<% if ( ((String)request.getAttribute("IndProcedencia")).trim().equals("1") ){ %>
		document.getElementById("cboTipoIncidencia").disabled = true;
	<% }%>
									
</script>