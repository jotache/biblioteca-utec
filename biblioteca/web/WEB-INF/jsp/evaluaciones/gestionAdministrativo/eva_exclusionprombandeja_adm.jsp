<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script type="text/javascript" type="text/javascript">
	
		function onLoad(){}
		
		function fc_Buscar()
		{ 
			document.getElementById("txhAccion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_PreparaGrabar()
		{
			var i = 0;
			
			strCadCodExclusiones = "";
			strCadCodAlumnos = "";
			strCadIndSemestre = "";
			strCadIndAcumulado = "";
			
			while (true)
			{
				objCodEx = document.getElementById("txhCodExclusion" + i );
				objCodAlum = document.getElementById("txhCodAlumno" + i );
				objIndSemestre = document.getElementById("chkSem" + i );
				objIndAcumulado = document.getElementById("chkAcum" + i );
				
				if ( objCodEx == null ) break;

				if ( fc_Trim(objCodEx.value) == "" ) strCadCodExclusiones = strCadCodExclusiones + "0|";
				else strCadCodExclusiones = strCadCodExclusiones + objCodEx.value + "|";
								
				
				strCadCodAlumnos = strCadCodAlumnos + objCodAlum.value + "|";
				
				if ( objIndSemestre.checked ) strCadIndSemestre = strCadIndSemestre + "1|";
				else strCadIndSemestre = strCadIndSemestre + "0|";

				if ( objIndAcumulado.checked ) strCadIndAcumulado = strCadIndAcumulado + "1|";
				else strCadIndAcumulado = strCadIndAcumulado + "0|";
								
				i++;
			}
			
			document.getElementById("txhCadCodExclusiones").value = strCadCodExclusiones;
			document.getElementById("txhCadCodAlumnos").value = strCadCodAlumnos;
			document.getElementById("txhCadIndSemestre").value = strCadIndSemestre;
			document.getElementById("txhCadIndAcumulado").value = strCadIndAcumulado;
			
		}
		
		function fc_Grabar()
		{		
			fc_PreparaGrabar();
			
			if ( !confirm(mstrSeguroGrabar)) return false;
			
			document.getElementById("txhAccion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_Regresar()
		{
			window.location.href = "${ctx}/evaluaciones/bandeja_gest_administrativa.html?txhCodPeriodo="+
									document.getElementById('txhCodPeriodo').value + "&txhCodEval="+
									document.getElementById('txhCodEvaluador').value;
		}
		
		function fc_Limpiar()
		{
			document.getElementById("txtNombre").value = "";
			document.getElementById("txtApellidos").value = "";
			document.getElementById("cboSeccion").value = "";
		}
	</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/bandeja_exclusiones_adm.html" >
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codCurso" id="txhCodCurso"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="nomAlumno" id="txhNomAlumno"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	
	<form:hidden path="cadCodExclusiones" id="txhCadCodExclusiones"/>
	<form:hidden path="cadCodAlumnos" id="txhCadCodAlumnos"/>
	<form:hidden path="cadIndSemestre" id="txhCadIndSemestre"/>
	<form:hidden path="cadIndAcumulado" id="txhCadIndAcumulado"/>
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px;margin-top:6px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="765px" class="opc_combo"><font style="">Exclusi�n del Promedio</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table cellpadding="2" cellspacing="0" width="98%" style="margin-left:9px">
		<tr>
			<td>
				<table class="tabla" style="width:98%;margin-top:6px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0">
					<tr>
						<td>&nbsp;Per�odo Vigente :</td>
						<td colspan=3>
							<form:input cssClass="cajatexto_1" path="periodoVigente" cssStyle="width:120px" readonly="true"/>
						</td>
						<td>Ciclo :</td>
						<td>
							<form:input cssClass="cajatexto_1" path="ciclo" cssStyle="width:20px"/>
						</td>
					</tr>
					<tr>
						<td>&nbsp;Producto :</td>
						<td colspan=3>
							<form:input path="producto" cssClass="cajatexto_1" cssStyle="width:250px" readonly="true"/>
						</td>
						<td>Especialidad :</td>
						<td >
							<form:input path="escialidad" cssClass="cajatexto_1" cssStyle="width:250px" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td>&nbsp;Curso :</td>
						<td colspan=3>
							<form:input path="curso" cssClass="cajatexto_1" cssStyle="width:200px" readonly="true"/>
						</td>
						<td>Sist. Eval. :</td>
						<td>
							<form:input path="sistemaEval" cssClass="cajatexto_1" cssStyle="width:20px" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td>&nbsp;Evaluador :</td>
						<td colspan=3>
							<form:input path="nomEvaluador" cssClass="cajatexto_1" cssStyle="width:200px" readonly="true"/>
						</td>	
						<td>&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>	
		<tr style="height:15px;">
				<td></td>
		</tr>
		<tr style="height:20px;">
			<td class="">
				<!-- Titulo de la bandeja -->
				 <table cellpadding="0" cellspacing="0" border="0" bordercolor="red">
					 <tr>
					 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="200px" class="opc_combo"><font style="">Relaci�n de Alumnos</font></td>
					 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
					 </tr>
				 </table>
			</td>						
		</tr>
		<tr>
			<td>
				<table class="tabla" height="30px" background="${ctx}/images/Evaluaciones/back.jpg" style="width:98%;margin-top:6px" cellspacing="2" cellpadding="0" border="0" bordercolor="red">
					<tr>
						<td>Apell. Paterno :</td>
						<td>
							<form:input path="apellidoAlumno" id="txtApellidos" cssClass="cajatexto" cssStyle="width:140px"
								onkeypress="javascript:fc_ValidaTextoEspecial();" 
								onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Apellido');"
							/>
						</td>
						<td>Nombre :</td>
						<td >
							<form:input path="nombreAlumno" id="txtNombre" cssClass="cajatexto" cssStyle="width:140px"
								onkeypress="javascript:fc_ValidaTextoEspecial();" 
								onblur="javascript:fc_ValidaNombreAutorOnblur(this.id,'Nombre');"
							/>
						</td>
						<td>Secci�n :</td>
						<td>
							<form:select path="codSeccion" id="cboSeccion" cssClass="combo_o" 
								cssStyle="width:55px">
								<form:option value="">SEL</form:option>
								<c:if test="${control.listaSeccion!=null}">
								<form:options itemValue="codSeccion" itemLabel="dscSeccion" 
									items="${control.listaSeccion}" />
								</c:if>
							</form:select>&nbsp;&nbsp;&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" 
								style="cursor:pointer;" onclick="javascript:fc_Buscar();"></a>
							&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer;" onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<div style="overflow: auto; height: 220px" class="">
				<display:table name="sessionScope.listaBandejaExclusionesAdm" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.ExclusionesDecorator" requestURI="" class="" style="width:98%;border: 1px solid #048BBA">
					<display:column property="codAlumno" title="C�digo" style="width:20%; text-align: center"/>
					<display:column property="nomAlumno" title="Alumno" style="width:44%; text-align: left"/>
					<display:column property="chkSementre" title="Excluir Curso del Prom. Semestre" style="width:13%; text-align: center"/>
					<display:column property="chkAcumulado" title="Excluir Semestrel Prom. Acumulado" style="width:13%; text-align: center"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
				</div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" align="center" height="30px" width="98%" style="margin-top:6px">	
		<tr>
			<td align="center">										
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" alt="Grabar" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" id="imgcancelar" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript" language="javascript">
	objMsg =  document.getElementById("txhMsg");
	
	if ( objMsg.value != "" )
	{
		if ( objMsg.value == "ERROR" )alert(mstrProblemaGrabar);
		else if ( objMsg.value == "OK" )alert(mstrSeGraboConExito);
	}	
	
	document.getElementById("txhAccion").value = "";
	objMsg.value = "";
</script>