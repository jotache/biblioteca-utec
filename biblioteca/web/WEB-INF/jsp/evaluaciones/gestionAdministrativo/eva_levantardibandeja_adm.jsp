<%@ include file="/taglibs.jsp"%>
<%@page import='java.util.List'%>
<%@page import='java.util.ArrayList'%>
<head>
	<script src="${ctx}/scripts/js/funciones_eva-textos.js" type="text/JavaScript"></script>
	<script type="text/javascript" type="text/javascript">
		function onLoad(){}				
		
		function fc_Regresar()
		{
			window.location.href = "${ctx}/evaluaciones/bandeja_gest_administrativa.html?txhCodPeriodo="+
									$('txhCodPeriodo').value + "&txhCodEvaluador="+
									$('txhCodEvaluador').value;
		}
		
		function fc_Buscar(){
			$("txhAccion").value = "BUSCAR";
			$("frmMain").submit();
		}

		function fc_Grabar(){
			if ( fc_Trim($("txhCodAlumno").value) == "" ){	
				alert(mstrSeleccione);
				return false;
			}			
			$("txhAccion").value = "GRABAR";
			$("frmMain").submit();
		}
		
		function fc_selAlumno(codAlumno, nombreAlumno)
		{
			$("txhCodAlumno").value = ''+codAlumno;
			$("txhNomAlumno").value = ''+nombreAlumno;			
		}
			
		function fc_Limpiar()
		{
			$("txtNombre").value = "";
			$("txtApellidos").value = "";
			$("cboSeccion").value = "";
		}
	</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/evaluaciones/bandeja_levantardi_adm.html" >
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codCurso" id="txhCodCurso"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="nomAlumno" id="txhNomAlumno"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>

	<form:hidden path="codEspecialidad" id="txhCodEspecialidad"/>
	<form:hidden path="codProducto" id="txhCodProducto"/>
	<form:hidden path="codTipoSesionDefecto" id="txhCodTipoSesionDefecto"/>	
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top: 10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="765px" class="opc_combo"><font style="">Levantar D.I (Desaprobado por Inasistencia)</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table cellpadding="2" cellspacing="0" align="center" width="98%">
		<tr>
			<td>
				<table class="tabla" style="margin-top:6px;width:98%" background="${ctx}/images/Evaluaciones/back.jpg"
				 cellspacing="2" cellpadding="0">
					<tr>
						<td class="" >Per�odo Vigente</td>
						<td colspan=3>
							<form:input cssClass="cajatexto_1" path="periodoVigente" cssStyle="width:120px" readonly="true"/>
						</td>
						<td class="" >Ciclo</td>
						<td>
							<form:input cssClass="cajatexto_1" path="ciclo" cssStyle="width:20px"/>
						</td>
					</tr>
					<tr>
						<td class="" >Producto</td>
						<td colspan=3>
							<form:input path="producto" cssClass="cajatexto_1" cssStyle="width:250px" readonly="true"/>
						</td>
						<td class="" >Especialidad</td>
						<td >
							<form:input path="escialidad" cssClass="cajatexto_1" cssStyle="width:250px" readonly="true"/>
						</td>
	
					</tr>
					<tr>
						<td class="" >Curso</td>
						<td colspan=3>
							<form:input path="curso" cssClass="cajatexto_1" cssStyle="width:200px" readonly="true"/>
						</td>
						<td class="" >Sist. Eval.</td>
						<td>
							<form:input path="sistemaEval" cssClass="cajatexto_1" cssStyle="width:20px" readonly="true"/>
						</td>
					</tr>
					<tr>
						<td class="" >Evaluador</td>
						<td colspan=3>
							<form:input path="nomEvaluador" cssClass="cajatexto_1" cssStyle="width:200px" readonly="true"/>
						</td>	
						<td class="" >&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="height:20px;">
			<td class="opc_combo">
				<!-- Titulo de la bandeja -->
				 <table cellpadding="0" cellspacing="0" border="0" style="margin-top:6px" bordercolor="red">
					 <tr>
					 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="200px" class="opc_combo"><font style="">Relaci�n de Alumnos</font></td>
					 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
					 </tr>
				 </table>
			</td>
		</tr>
		<tr>
			<td>
				<table class="tabla" style="width:98%;margin-top:6px; height:30px" background="${ctx}/images/Evaluaciones/back.jpg" cellspacing="2" cellpadding="0" border="0" bordercolor="red">
					<tr>
						<td class="" >Apell. Paterno</td>
						<td>
							<form:input path="apellidoAlumno" id="txtApellidos" cssClass="cajatexto" cssStyle="width:140px"
								onkeypress="javascript:fc_ValidaTextoEspecial();" 
								onblur="javascript:fc_ValidaNumerosAndLetrasFinalTodo(this.id,'Apellido');"
							/>
						</td>
						<td class="" >Nombre</td>
						<td >
							<form:input path="nombreAlumno" id="txtNombre" cssClass="cajatexto" cssStyle="width:120px"
								onkeypress="javascript:fc_ValidaTextoEspecial();" 
								onblur="javascript:fc_ValidaNumerosAndLetrasFinalTodo(this.id,'Nombre');"
							/>
						</td>
						<td class="" >Secci�n</td>
						<td>
							<form:select path="codSeccion" id="cboSeccion" cssClass="combo_o" 
								cssStyle="width:100px">
								<form:option value="">SEL</form:option>
								<c:if test="${control.listaSeccion!=null}">
								<form:options itemValue="codSeccion" itemLabel="dscSeccion" 
									items="${control.listaSeccion}" />
								</c:if>
							</form:select>&nbsp;&nbsp;&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
							<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
								 style="cursor: pointer;" onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
							&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
							<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
								 style="cursor: pointer;" onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<div style="overflow: auto; height:200px">
					<display:table name="sessionScope.listaBandejaDIs" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.CursoEvaluadorDecorator" id="tblIncidencias"
						style="width: 98%;border: 1px solid #048BBA">
						<display:column property="rbtAlumnoDoc" title="Sel." style="width:3%;text-align:center"/>
						<display:column property="codCarnet" title="Carnet" style="width:13%;text-align:center"/>
						<display:column property="nombreAlumno" title="Alumno" style="width:73%; text-align:left"/>
<%--						<display:column property="totalIncidencias" title="Total Incidencias" style="width:11%;text-align:center"/> --%>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
						<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
						<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
						<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
					</display:table>
					<script type="text/javascript">
						<%
							ArrayList lista = (ArrayList)request.getSession().getAttribute("listaBandejaDIs");
							if ( lista.size() > 10) {
						%>
							document.getElementById("tblIncidencias").style.width = '98%';
						<% } %>
					</script>
					<% request.getSession().removeAttribute("listaBandejaDIs"); %>
				</div>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" align="center" height="30px" width="98%">	
		<tr>
			<td align="center">

				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgreg','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Registrar" src="${ctx}/images/botones/grabar1.jpg" id="imgreg" onclick="javascript:fc_Grabar();" style="cursor:pointer;"></a>
				 
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar1','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar1" onclick="javascript:fc_Regresar();" style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript" language="javascript">
	var strMsg = fc_Trim(document.getElementById("txhMsg").value);
	if ( strMsg != "" ){
		if ( strMsg == "ERROR" )alert(mstrProblemaGrabar);
		else if ( strMsg == "OK" )alert(mstrSeGraboConExito);		
	}
	document.getElementById("txhAccion").value = "";
	document.getElementById("txhMsg").value = "";
</script>
