<%@page import="com.tecsup.SGA.modelo.Alumno"%>
<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<head>

<link href="${ctx}/styles/marquesina.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/scripts/marquesina/mootools-1.2.2-core.js" type=text/javascript></script>
<script src="${ctx}/scripts/marquesina/mootools-1.2.2.2-more.js" type=text/javascript></script>
<script src="${ctx}/scripts/marquesina/marquesina.js" type=text/javascript></script>
<link href="${ctx}/styles/estilo_eva.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/scripts/js/Func_Comunes.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/mensajes.js" type="text/JavaScript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript" ></script>
<link href="${ctx}/scripts/jquery.colorbox/colorbox.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/scripts/jquery.colorbox/jquery.colorbox.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/jquery.colorbox/jquery.colorbox.js" type="text/JavaScript"></script>

<script src="${ctx}/scripts/jquery/jquery-ui-1.10.1.custom.min.js" type="text/JavaScript"></script>
<link href="${ctx}/styles/jquery/jquery-ui-1.10.1.custom.css" rel="stylesheet" type="text/css" />

<%Alumno oAlumnoDatos = (Alumno)request.getAttribute("AlumnoDatos"); 
%>

<script type="text/javascript">
jQuery.noConflict();


function onLoad(){
	//Debe de ingresar una opcion y periodo	
	if ( document.getElementById("txhOpcion").value != "" && document.getElementById('txhCodPeriodo') != null && document.getElementById('txhCodPeriodo').value != "")
	{
		fc_Perfiles(document.getElementById("txhOpcion").value);
	}
	document.getElementById("txhOpcion").value = "";
}

function fc_deacuerdo(){	
	document.getElementById("txhAccion").value = "ACEPTA";
	document.getElementById("frmMain").submit();
}

function fc_Perfiles(obj){
	
	switch(obj){
		case '3':			
			window.location.href = "${ctx}/evaluaciones/bandejaEvaluador.html?txhCodPeriodo="+
									document.getElementById('txhCodPeriodo').value + "&txhCodEvaluador="+
									document.getElementById('txhCodEvaluador').value + 
									"&txhCodSede=" + document.getElementById("cboSede").value;
			break;
		case '4':
			window.location.href = "${ctx}/evaluaciones/bandeja_alumnos_tutor.html" +
									"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
									"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value;
			break;
		case '5':			
			window.location.href = "${ctx}/evaluaciones/bandeja_gest_administrativa.html?txhCodPeriodo="+
									document.getElementById('txhCodPeriodo').value + "&txhCodEvaluador="+
									document.getElementById('txhCodEvaluador').value + 
									"&txhCodSede=" + document.getElementById("cboSede").value;
			break;
		case '6':
			window.location.href = "${ctx}/evaluaciones/eva_nota_casos_especiales.html?txhCodPeriodo="+
									document.getElementById('txhCodPeriodo').value + "&txhCodEvaluador="+
									document.getElementById('txhCodEvaluador').value;
			break;
		case '7':
			
			window.location.href = "${ctx}/evaluaciones/eva_formacion_empresa.html?txhCodPeriodo="+
									document.getElementById('txhCodPeriodo').value + "&txhCodEvaluador="+
									document.getElementById('txhCodEvaluador').value;
			break;
		default:
			break;
	}
}

function fc_SelOpcion(obj)
{
	switch(obj){
		case '1':
		window.location.href = "${ctx}/eva/mto_bandeja.html?txhCodPeriodo="+
									document.getElementById('txhCodPeriodo').value + "&txhCodEvaluador="+
									document.getElementById('txhCodEvaluador').value;
													
			break;
		case '8':
			url = "${ctx}/evaluaciones/consultarEvaluaciones.html?codAlu="+document.getElementById('txhCodEvaluador').value+"&txhCodPeriodo="+
									document.getElementById('txhCodPeriodo').value + "&txhCodEvaluador="+
									document.getElementById('txhCodEvaluador').value + "&txhIndProcTutor=0";					

			
// 			url = "${ctx}/confirmar_datos_personales.html?codAlu="+document.getElementById('txhCodEvaluador').value+"&txhCodPeriodo="+
// 			document.getElementById('txhCodPeriodo').value + "&txhCodEvaluador="+
// 			document.getElementById('txhCodEvaluador').value + "&txhIndProcTutor=0";
			
			window.location.href = url;
			break;
		case '9':
			url = "${ctx}/evaluaciones/reportes.html?txhCodEvaluador="+
									document.getElementById('txhCodEvaluador').value;					
			window.location.href = url;
			break;
		default:
			objSede = document.getElementById("cboSede"); 
			
			if ( objSede != null )
			{
				if ( document.getElementById("cboSede").value == "" )
				{ 
					alert(mstrDebeSelSede);
					return false;
				}
			}
			document.getElementById("txhOpcion").value = obj;
			document.getElementById("txhAccion").value = "PERIODO";
			document.getElementById("frmMain").submit();
			break;
	}
}
	</script>
</head>

<body>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/menuEvaluaciones.html">	
	<form:hidden path="codEval" id="txhCodEvaluador"/>
<%-- 	<form:hidden path="codPeriodo" id="txhCodPeriodo"/> --%>
	<form:hidden path="accion" id="txhAccion"/>
	<form:hidden path="opcion" id="txhOpcion"/>
	<table class="table-menu" cellspacing="0" cellpadding="2" border="0" 
		style="width: 98%; margin-top: 1px;margin-left:10px" >
		
		<tr bgcolor="#00A0E4" id="trComboSede">
		
<%

  if (
	  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CARG") == 1 ||
	  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_ADMI") == 1 ||
	  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_JDEP") == 1 ||
	  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1 ||
	  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_PERF") == 1 || 
	  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_NOTA") == 1 ||
	  CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_EMPR") == 1
  	) 
	{
%>
			<td colspan="2" style="FONT-SIZE: 10px;TEXT-TRANSFORM: none;COLOR: #ffffff;FONT-FAMILY: Tahoma, Verdana;TEXT-DECORATION: none;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEDE :</b>&nbsp;
				<form:select path="codSede" id="cboSede" cssClass="combo_o" 
					cssStyle="width:100px" onchange="javascript:fc_SelOpcion(0);">
					<c:if test="${control.listaSedes!=null}">
					<form:options itemValue="codSede" itemLabel="dscSede" 
						items="${control.listaSedes}" />
					</c:if>
				</form:select>
				
				&nbsp;&nbsp;
				
				<c:if test="${control.listaPeriodos!=null}">
				<form:select path="codPeriodo" id="txhCodPeriodo" cssClass="combo_o" 
					cssStyle="width:150px" >
					<form:options itemValue="codigo" itemLabel="nombre" 
						items="${control.listaPeriodos}" />
				</form:select>
				</c:if>
				
				
			<!-- /td-->
			</td>
<%
	}else{
%>
		<td colspan="2" style="FONT-SIZE: 10px;TEXT-TRANSFORM: none;COLOR: #ffffff;FONT-FAMILY: Tahoma, Verdana;TEXT-DECORATION: none;">
			<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
			
		</td>
<%} %>
						
			<td rowspan="15" align="right" height="511px" width="459px"><img
				src="${ctx}/images/Evaluaciones/opciones-eval.jpg" width="459px" height="511px"></td>
		</tr>
<!-- 
		<tr bgcolor="#00A0E4" id="trComboSede" >			

		</tr>
 -->

		<%		
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"EVA_MANT") == 1) {
		%>

		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_SelOpcion('1');" class="enlace"
				style="cursor: hand">Mantenimiento y Configuraciones</a></td>
		</tr>

		<%
		}
		%>

		<%
		if (
		CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CARG") == 1 ||
		CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_JDEP") == 1 ||
		CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1
		) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_Perfiles('3');" class="enlace"
				style="cursor: hand">Carga Acad&eacute;mica del Evaluador </a></td>
		</tr>
		<%
		}
		%>


		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_PERF") == 1) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_SelOpcion('4');" class="enlace"
				style="cursor: hand">Registro de Perfiles del Alumno </a></td>
		</tr>
		<%
		}
		%>


		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_ADMI") == 1 ||		
		CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1
		) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_SelOpcion('5');" class="enlace"
				style="cursor: hand">Gesti&oacute;n Administrativa </a></td>
		</tr>
		<%
		}
		%>


		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_NOTA") == 1) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_SelOpcion('6');" class="enlace"
				style="cursor: hand">Registro Nota - Casos Especiales</a></td>
		</tr>
		<%
		}
		%>


		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_EMPR") == 1) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_SelOpcion('7');" class="enlace"
				style="cursor: hand">Control Formaci&oacute;n de Empresa - PFR
			</a></td>
		</tr>
		<%
		}
		%>


		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CONS") == 1) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right" ><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td ><a href="javascript:fc_SelOpcion('8');" class="enlace"
				style="cursor: hand">Consultar Evaluaciones</a></td>
		</tr>
		<%
		}
		%>
		
		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_REPO") == 1) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_SelOpcion('9');" class="enlace"
				style="cursor: hand">Reportes Evaluaciones</a></td>
		</tr>
		
		
		<%
		}
		%>
		
		<%
		//if (
		//CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_CARG") == 1 ||
		//CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_JDEP") == 1 ||
		//CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "EVA_DIRA") == 1
		//) {
		%>
		
			<tr bgcolor="#00A0E4" height="45px">
			<td>&nbsp;</td>
			
			<td >
			&nbsp;
			</td>
			</tr>
		<%
		//} 
		%>			
			
		
	</table>
</form:form>



<%
String debeEncuesta = (String)request.getAttribute("hayEncuestaPFR");
String debeLeyDatos = (String)request.getAttribute("debeAceptarLey");

if(debeEncuesta.equals("S") || debeLeyDatos.equals("S")){ %>

	<%if(debeEncuesta.equals("S")){%>
	<div id="encuesta-PFR" style="padding: 20px !important; font-size: 14px; color:#0070C0; text-align: justify; line-height: 24px;
		background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAXABjAAD/2wBDAAoHBwkHBgoJCAkLCwoMDxkQDw4ODx4WFxIZJCAmJSMgIyIoLTkwKCo2KyIjMkQyNjs9QEBAJjBGS0U+Sjk/QD3/2wBDAQsLCw8NDx0QEB09KSMpPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT3/wAARCADvAPEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD2aiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKSgsF6kD60ALRUP2qAcGaPP+8Kessb/ddT9DQOzH0UUUCCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKjkdIkZ3IVVGST2oAfXP654x03RN0cknnXA/5ZRckfU9q5nxR42luC9ppTGOHo0w+8309BXDyAkkkkk9zW8KV9ZHv4HJudKdfRdv8AM6TU/iNqt4xW12WkZ6BBlvzNYMut3tw26e5mkP8AtuTVF1xTDXRGEVsj6Klg6FJWhFI1IdWkQ/eNatp4idCMsa5WnByO9NxTFUwsJ7o9M07xS3AMh/E101lrkc4G4g+4rxSG7eMjDGtrT9deIjLVlKiuh4+KyqL1ieyxyrIMoc0+uH0nxEG25euts7+O6UcjdXNKDifP18NOk9S3RRRUnOFFFFABRRRQAUUUUAFFFFABRRRQAUUUlAC1lal4h0/S8rcTgyf880+Zq57xN4uZXey018EcSTD+Q/xrjuXYsxJY9STmvRw+Ac1zVNEeTiszUHyUtX3Oyn8fksRbWXHrI+P0FRR+PLvPzWkJHoGIrlVWpVWu76nRS+E814/EN35vyO4s/GtpMQtzDJCT/EPmFdBb3MN1EJLeRZEPdTmvLFWrun31xp8wktnK+q54b61z1cBBq9PRnVQzOadqmq/E9MzivPfGPiJruRrC0bECHEjA/fPp9K1tU8Uxtoh8g7LqX5CvdfU1wbqTyeTXnxpuL94+2yehCqliHqun+ZUdagdauOtQOtbJn1EZFN1qFhirbrUDrVpnRFkFFKRikqihKVXK0UlMlov2moPCw+Y11+jeICpUM1cBnFWbe6aJgQamUEzixGFjVR7ppepJexgZG8D860K8n0LxA8EqHdyDXqFldJe2sc8Z+Vx+tcVSHKz5HG4R0JeRYooorM4gooooAKKKKACiiigAooooAK5vxjrJ03TvIhbE9xkAjqq9zXR15l4vujd+IZlzlIcRqP5/rXXg6SqVddlqcOYVnSou270MMDPJq2LK42q32eUq4yCEJBHrUKjNbF3rFzO8cVpPNDbxqEjVWx+Jr25OV0onzkIxabkygtpP/wA8Jf8Avg1KbWZI/MeJ0TOMsuBmul8S3dxaW9jbR3EquI9zMHIJ+prLe9uNRsIbQmWeZXLcjccdqxhVnOKlbQ6KlGEJOF3dGeq1Kq9KAhVipB3A4xinXCvbRO0ilCqlgGGK2ujBRbOV1HUXGtGRD+7j+THqO9anEiBlPDDIrmpcszMepOa6fwratqxhtd2PmILegrgxcVbnP0DJK6op0Xslf7tyu61C69a73+wvD1zcyafDJKLtAfmyev8AI1kaJ4dgv9QvbW8LBoB8rKcc+prhU0e9DH0+Vyaatrt0ZyTrXReG5tOi0LVEvWtxMy/uhJjceO1bEXhnQNQt57exupHvIFJZ93Gfp6Vi6L4ftL/StSnuhJ5tqDs2tgZp8yaKqYqlWptO6s10130ORZajrtNE8NabqHhyXUL6aSDypPmdTwFHt61Lq3h/Q7rwxJqmhmUeScNvJ59cg1ftFex1/wBo0lP2bT3te2l/Up3lxprfD62hje2N8H+ZRjzMZ796wtD0aXXNSjtITtB5eQ9EXua2Lrw/ZReB4NVUSfanfafm+Xr6Vq2Gg6Fo2i215rk8wluxwI2YAD6ClzKKdjn+sQo05ezbbcmlpd38l2Ryeu2NlpmoNa2N292I+HkKhRn0FZddZ4l8MWumanYtaSO9jeEbcnkdO/0NbN14Z8LaLqyQX807G4AEUOSdue5IqlUSS6lxx1OFOO8m12103OBt5zG4INenfD7WftAlsnbnG9P61xHjDQ4tA1s29uWMDoHTccke1O8HagbPxJZtnCs4Q/Q8UTSnC6M8ZThi8K5w7XR7bRRRXCfGhRRRQAUUUUAFFFFABRRRQAV5NrIJ1u8LdfOb+des15p4otTb6/OccSEOPxr0culao15HlZtFunF+ZkKtX9Jt/tGp20WPvSD8hzVVVrofDNl5d/HdzvFHEqnaS4yT9K9KtPkg2ePh6fPUS8xnimXzdbdR0jUKKt+F5nj89gFWOKMsxA5Y9sms7V4ZBqMsshRhK5KlWByK1NGjRtCvFE0ccjnBLnGBXNNJYdR9DrpuTxUperKejwNe6zG2CQHLt7VU8U3HnG+kHTkD6VvaHJbwyzJDIqoicyPxvP8AgK5zW0jNrcLDIZBtJLEYBPt7U4y5qr02QKKjThru9ThGFdh8OBjWB6ZP8q5E812/w+g8u+hcj7+41GLdqTPo8Em5yfZP8jV00f8AFcyH/barOiDHiDWf901Dpw/4raQ/7bVY0UY1/WP9015jPbrPSX+FfmZfgoY1a+/65N/On+HBjRde+rf1o8GDGq3v/XNv507w8P8AiTa79W/rVPr8jfEP36n/AG5+ZT0wf8W21D/fNM0oY+Gmpf75/pU2nD/i3N+P9s1Hpg/4tvqQ/wBs1X+ZrJ/F/wBfF+hHf/8AJK7T/roP50zxx/yL2hf9ch/IVJqAx8LbQf8ATQfzpnjj/kXtC/65D+QprderLo/xof45/kWPFH/Hl4Z+i/yFV/H3/I6WP+4n/oVWPFH/AB5eGfov8hVfx7/yOdj/ALif+hUobr5kYX4qfpP8yt8T/wDkYYP+vcfzNcrp0hi1G3cdVkUj866v4n/8jDB/17j+ZrlNNjMupWyDq0qgfnW1P4DvwVvqcb9j6DXoKWkHAFLXAfEBRRRQAUUUUAFFFFABRRRQAVzfi7SjeWa3US5lg6juVrpKaRuGD0NaUqjpzUkZVqSqwcJdTydVqVUHXA/Kt/xB4f8AsjNd2oHkE/Mv9w+3tWIor36dWNSPNE+XrUZUZ8shVQDoMfQVIq1lXerrESluAzDqx6VlzX91Kfmnf6KcCvMr5zQpy5Y+8/LYqNGTR1ePamyoJInT+8CK5AXdwhys8gP+9WhY65deYI5U85T3AwRU0s4ozdpKw3SktUZkNo8155Cjndg+wrvvDfl22q2y8BQNoqi+n29swuraQSrcDcXHY9x7UnQ5BwfWpr1lVfu7H32XYZfV+Z7yX3HUWmkXUPiaS7kQCDJbfnrUGgET65qhQjDggGsSfVrwweXLeSeXjGC1XfCNxCZr0BxxF3OK5JTjHRvU6alGoqUpSd9EtPUu+HtFu9Lvbya6QLGUIVtwOe9U/DuG0bWyOhyR+tc/ceIbpo3t2urgw5I25qnHqbRI8cNxJEj/AHlBwGqY4mjJ250dawVaak5vV2/A6PTh/wAW7v8A/fNR6YP+Lc6j/vmuf+1TrbNBHO4gbqgb5TUP2q4S3a3WeRYG+9GG+U/hXUo3Oj6q3fXeSkdFqA/4tfa/9dB/OrOuaPd+IvDWjvpiLMI0AYbgMcYrjZLq5e1Fs08hgByIyflFLaaxqOmIyWV7NCjHlVPFVyPoX9TqK0qclzKTavtqdZ4wAt38P2TFTPDt3KD06Cq3j0f8VnY/7if+hVx9xd3Fxc/aJ5nkmznexyc0lxf3V3Os1xcSSSL0ZmyRTjTasaUcBKm4Pm2Tv6s6n4n/APIww/8AXAfzNZXgqyN94qslxlY28xv+A81k3d7c30gkup3mcDAZzk4r0H4X6QUhuNTkXG/91Hn06mnL3KdjOv8A7FgOVvVK3zZ6HRSUtcR8YFFFFABRRRQAUUUUAFFFFABRRRQBzfi65xbw2yn5pG3EewrmrjSZni2TM0IYZIH3vxrvXsbdrr7VIm6VVwpbnaPauavGMtw7nksa4M5zOWGwsaNLRy3f5nIsEqtZ1amq6I46fw9MhPlzIw9+DVY6HdD7xjA/3q76xYRWN5L5aMyYK7xnBqw0cc1/azvHGWNuZCMYBI9q8XD+0nBPm1fl52CeDp30PO00QKczS59lFTi3jgXbGoUV2fyalHYz3EUQczlDsXAIqpqt0t5p14kyQrJBMFi2qFbFdHK2ruXp91zGVCMVocxHcPbng5QnLLUs9+MYh5/2qzb2QklFPA61rwaNLcaTFcAojgYZWPUetV9bqwp8kGe3kOIUJOjV23RmOzOcuxJ96ibI6Ej6GpGG1iPSkPSvPcne7PskV2FRMKtOhABIIB6e9QstUmaxZAGZDlSRUqTh/lfhv50xlpZ7OSExhsfvBlcGvQwuLqUX7u3Yp8r3HutV3WrbxSW0ghmKl8ZGDmoXWvpqNaNWKlEmLKjrUJGDVtlqNIJLiZIoULyOcBVGSTW6ZupK2pLpWmTavqMNnbqS8jYJ9B3Ne5adYxaZp8NpAMRxKFHv71h+DfCy6BZebOoa9mHzn+4P7orp65Ks+Z2R8dm2PWJqckPhX4vuLRRRWR5AUUUUAFFFFABRRRQAUUUUAFFFFADHGUYeorkZRgt7Guwrm9UtjDdNgfK/Ir53iGjJ04VFsv1NqT3Q23QQ6ZdMw3MwBwegq0rrm3yi7zbE4x2qp5pNjKiJuZ1CkZ6YoN1IGt5lgLeXH5boepFZYavCEYpbJL83czmiNC8+kQ/ZokEomOAfWs/V401HS5r0RCO8t22S7ej+9aMl4LeCP7LaMio24ITkk+9UNV1CCLTZoobUwvcHcwZsmujmi4Wk+n/DW0MJRT0ZxqRxtMBKxVc9QM12mnxoLTJY5PQEVwFraX51mUyPvgkIES57/SvS/I+zxRx4+6oz9a4MS1Gi1o9jqwVO1W9+hkX/AIfhf9+rGNm5MajrWHc2AtZE8x28pv4tvIrtboZCMOmKq31nDc2SRzx5PUHuK541eS8ZLRdT36OKkrczujmtQitzHDuldcJ8uF61SSyjFuJ7mQojHChRya3dU0OR0hNu4IC9G/xqlqOnXX2CDELNs4bbzXWqsJSZ10q0XFJS3Mm7sljgWeCQyRMccjBFLqi5W1HTKDpVx4JYtFKyRSAs/AKmoL6CWZrYJE7bVAOFzit9Em/Q6Y1LyV3tcpXFk0d8kIlJJAO5u1XxYW05aOKdjKB6fKasz+H7vUNTjBiKRlRlmOK2LDwwUSRrVEMkfbnJHsa9DL8TGFV077/mY1sXCMU3LU4iRCCR3FekeBdCsYNNi1JP31zKD87D7nsK8+uYmSeRXUqyscgjBFdz8N7/AHQXVg55QiRPoete9Uvy6GeaucsK3B+vmjuR0paSlrlPkQooooAKKKKACiiigAooooAKKKKACiiigAqtd2q3cJRuD2Poas0VFSnGpFwmrpjTscndWstrJhwVPYjoapvLL/z0b866rVk32D+3NZtloq3Vv5kzOpb7oHpXyVbLalPE+xoO6tc1crxuzn5Jph/y1f8AOsq6V5bggBnc8AdSa7seGbXOXklYemcUyTTreylIgiVcjr3/ADrSeCxFGHPU29TKFPnlY8+0LT7uHWdThubmTzIot0R4zHnmlGoatLojau+oyebHJ5Yi2jYRnGSPWtK1uIv+Et1r96mPJHO4dhWPFIn/AAgMnzrn7R0yP71ata6rfl6d1qfR0aUYJJL+Xp3Wppx3N/pes2Vu9/LcRXqZYSAfI3qPSqkc+o3smoiXUJgbInyyoALd/m9asanLH/wkGh/Ov3euR6VWspY45/EO91BBJwT7Vzu7he3RdP71vyNIpW5ra27eZOmvXOoLptrDM0P2lS0rIBkY7D0qIatfW9nqUMd24lsWyshAJdfRqp6Xbva3uiSSjarowBPqaScDZ4gnyPLYhAc9T7U+WKnyrZbf+BW/I09nC9ktP+CWYtR1GXUbFZri4kN3GWk/uID02jtik0y4u7fT9Qu/tU0rQMyqjcg47mtzRyraRakbSREBkdqwrW8k0/Q9TnhAMguWALDpk9ayVV1OaCj1S9dWSrO6S/q5LaandC60uZ7qZ/tgIkjlOcehHpXd6FI8l04Ziflrzu5W2tbrSJfPEsrv5kspbJPH6D2r0Pw2VmLzJyrKMH610Ye7xdKUdnf8LnJj4xVK9v6uSa74XtNbjLMPKuQPllUfz9a47S7G98K+KLc3kZEMjeX5q/cYH37V6dUbxpINrorL6MM19dGbSsedQx9SnB0paxelv8h46UtJS1BwhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBHLGssZRxlT1FOUBVAAwBTqKnlV+bqAVSv49yq47cVdpjoHUqehrPEUva03AqEuV3OUbQdM8x5PsEG987m28nPWojoOliHyhp9v5ZO7bt4zWzPEYnKn8KgYV8nVdSLs27o9SFWT6sypNG04sjtZQZjACsR90DpXOaNbWGo6tqYnSGdlm3Jk549vauydcggjIPXNVEsba3cvDbxRseNyoAaw9s4xkm3d7HTTqtJptkF1ZwXcPlXEKPGOikdPpUR02zNqtsbaMwA5CY4z61eYVGa89znHRMpNkNvawWkfl20SxJnO1RxWXb6Vc29xcRq0D2U8nmMHGW56jHStK6vIbOPfM2PQDqayJfE2G/d22V9Wbmt6PtpXcVe/c2pwqST5UX10XTUhaJbKEIxyRjvXZaLbC3sR8uC3OMdu1cXpGvWl5eRx3YNuM8sTlfz7V6Im3YuwjbjjHpXvZNhqnPKrVe2i1/E83MZTjaEh1FFFfSHlBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBBcW6zpg8EdDWTLG0blW6itwkAGsacmSVm9TxXi5rThZSXxM6cPJ3t0KrComFaK6dNJHu4B7A1XksrhP+WTH6c14lXCVkuZwdjrjVi9LlFhUMhCgk9AM1ca1m/54yf980yXS7qaCTbEVyp5JxXH9Wqzdoxf3M6FUit2cTfXDXVy0jnjoo9BVPyi5wOlddo506ytbhNQgEkr/dO3P4UzRrrTLL7QL633hh8ny5wPSvWhQSUVzJX/AA9T0XieVNRg3b8fQ5gxqi4Fdz4G1aS4gksZmLGEboyeu30rG0W80y0vbh7633RvnywV3bfarng1Fk8RXU1uhSAKcKewJ4rswT5Zxale+ljkx3v05qS21ud5S0UV7586FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAEcqsYyF6mooLRYuT8zepqzRWUqMJTU5K7RSk0rIKKKK1JCkIzS0UAcfr2iyQSvcQKWibkgfw/8A1q52ROuK9QIz2qpLpVlMd0lrEzepWvLxGXc8rwdj06GYuCtNXPMPKMkgRMFmOAM16J4e0YaPY7W+aeT5pGH8qlutCsrq3EXkLHt5VkGCprQiQxxKhYsVAGT3qsHgfYTcpa9icXjvbwUY6dx9FFFekecFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRTTntQA6ioGZxUbSyDoKALdFUfOl9KcJ37igC5RVYTt3pwn9aAJ6Kh88etJ54oAnoqDzxSGc9qALFFVDO1NM8nYUAXaKpCaXuKesjmgC1RUSlqkFAC0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFACEUwpmpKKAITH7U0x+1WKSgCsYjTTCTVujAoApGA+tJ5B96vYHpRgelAFMQGnCI1awPSjAoAriI04R+1TYpaAIhH7U8LinUUAJS0UUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/9k=');
		background-position: top right;
		background-repeat: no-repeat;">
		<h1 style="font-size: 22px; font-weight: bold; color: red; margin-bottom: 10px;">Encuestas PFR - 2016.II</h1>
		<!--<h2 style="font-size: 16px; font-weight: bold; color: red; margin-bottom: 20px;">De 1, 2, 3, 5 y 6 semestre</h2>-->
		<hr/>
		<p style="">Ingresa a este enlace para poder realizarla:</p>
		<p style="text-align: center; padding: 0px;"><a href="http://academico.tecsup.edu.pe/alumnos/" target="_blank" style="font-size: 18px; color: blue; display: block; line-height: 30px;">http://academico.tecsup.edu.pe/alumnos/</a></p>
		<p>Luego, ingresas tu DNI y tu c�digo de carn�, despu�s procedes con las indicaciones que te indique el sistema.</p>
		<!-- <p>Te hacemos recordar que solo tienes plazo de responder las encuestas de evaluaci�n del docente hasta el domingo <span style="color: red; font-weight: bold;">05 de junio</span>.</p> -->
		<p style="color: #00538e">Cualquier duda, no olvides acercarte a Secretar�a Docente.</p>
	</div>
	<%}%>
	
	<%if(debeLeyDatos.equals("S")){%>
	<div id="datospersonales-PFR" style="padding: 20px !important; font-size: 10px; color:#0070C0; text-align: justify; line-height: 24px;
		background-position: top right;
		background-repeat: no-repeat;">
		<h1 style="font-size: 16px; font-weight: bold; color: blue; margin-bottom: 10px;">Ley de datos Personales</h1>
		<p style="text-align: center; padding: 0px;">
			Autorizo a TECSUP N� 1 y a la Asociaci�n TECSUP N� 2, afiliadas, asociadas, personas naturales o jur�dicas con las que haya suscrito contratos o convenios a realizar el tratamiento* de mis datos personales por un plazo indeterminado con la finalidad** de promoci�n, de todo tipo de publicidad, anuncio, en cualquier medio de comunicaci�n sea f�sico, virtual, televisivo u otro; as� mismo para �l env�o de informaci�n promocional, educativa, administrativa.<br/>			
			El titular de los datos personales puede ejercer sus derechos ARCO*** escribi�ndonos al correo datospersonales@tecsup.edu.pe Sus datos personales ser�n almacenados en las bases de datos de TECSUP N� 1 y de la Asociaci�n TECSUP N� 2, respectivamente. 
			<br/>
			Ley de Protecci�n de Datos Personales concordado con su Reglamento<br/> 
			*Ley de Protecci�n de datos Personales <br/>			
			Articulo 2.- Numeral 17 Tratamiento de Datos personales .- Cualquier procedimiento t�cnico automatizado o no, que permite la recopilaci�n registro, organizaci�n, almacenamiento, conservaci�n, elaboraci�n, modificaci�n extracci�n, consulta, utilizaci�n, bloqueo, supresi�n, comunicaci�n por transferencia o por difusi�n o cualquier otra forma de procesamiento que facilite el acceso, correlaci�n o interconexi�n de los datos personales.<br/> 			
			** Art�culo 6.- Principio de finalidad. En atenci�n al principio de finalidad se considera que una finalidad est� determinada cuando haya sido expresada con claridad, sin lugar a confusi�n y cuando de manera objetiva se especifica el objeto que tendr� el tratamiento de los datos personales.<br/> 			
			***El titular de los datos personales tiene derecho al acceso, rectificaci�n, cancelaci�n, oposici�n de sus datos personales.
		</p>	
		<p style="text-align: center;">
			<input type="button" value="Aceptar" id="imgGrabar" onClick="javascript:fc_deacuerdo();">
		</p>
	</div>	
	<%}%>
	
	<style type="text/css">
		#encuesta-PFR > p > a:HOVER{
			background-color: #FFF1A8;
		}
		
		#datospersonales-PFR > p > a:HOVER{
			background-color: #FFF1A8;
		}
	</style>
	
	<script type="text/javascript">
		jQuery(function(){
			var mensaje='';
			<%if(debeLeyDatos.equals("S")){%>
				mensaje = '#datospersonales-PFR';				
			<%}else{%>
				mensaje = '#encuesta-PFR';					
			<%}%>
			
			jQuery(mensaje).dialog({
				width: '600px',
				resizable: false,
				draggable: false,
				closeOnEscape: false,
				modal: false,
				clickOut: false,
	 			showTitleBar: false,
	 			showCloseButton: false,
	 			responsive: true
			});
			jQuery('.ui-dialog-titlebar-close').hide();
			jQuery('table.table-menu a, table.table-menu img ').hide();
		});
	</script>
	
	
<% } %>		

</body>
