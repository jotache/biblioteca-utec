<%@ include file="/taglibs.jsp"%>
<head>
	<%	if ( request.getSession().getAttribute("usuarioSeguridad") == null ) { %>
	<script type="text/javascript">	
		<% if (request.getSession().getAttribute("reingreso")!=null) {%>
		alert("Por favor reingrese al sistema con su nueva clave.");
		<% }else{ %>
		alert("Su sesi�n ha terminado, favor de volver a ingresar al sistema.");
		<% } %>	
		location.href = "/SGA/logeoReclutamiento.html";
	</script>
	<%} %>

<link href="${ctx}/styles/cab.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" type="text/javascript">
		function onLoad(){
					
		}
		
		function fc_CerrarSesion(){		
			if ( confirm("�Est� seguro de cerrar sesi�n?") ){				
				form = document.forms[0];
				form.txhAccion.value="CERRAR_SESION";
				form.submit();
			}
		}
		
		function fc_CambiarClave(){					 			
			window.location.href = "${ctx}/reclutamiento/cambiaClaveReclutamiento.html";			
		}
	</script>
</head>

<body>
	<form:form name="frmMain" commandName="control" action="${ctx}/Reclutamiento.html">
		<form:hidden path="accion" id="txhAccion"/>
				
		<table cellpadding="0" cellspacing="0" width="989px" border="0" align="center" background="${ctx}/images/cabecera/cabecera.jpg">
			<tr>
            	<td height="65px" colspan="3">&nbsp;                	
                </td>                
            </tr>				
			<tr>	
				<td valign="middle" width="495">	
					&nbsp;
					<span class="textonormal">Bienvenido(a): &nbsp;
						<form:label path="codEval" cssClass="Opciones"><b>${control.codEval}</b></form:label>
					</span>
						&nbsp;
					<label onClick="fc_CerrarSesion();" style="cursor: pointer;">
                        <span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'">[Cerrar Sesi&oacute;n]</span>
					</label>
						&nbsp;&nbsp;						
					<label onclick="fc_CambiarClave();" style="cursor: pointer;">
						<span class="textomenu" onmouseover="this.className='textolink'" onmouseout="this.className='textomenu'" >[Cambiar Contrase&ntilde;a]</span>
					</label>
				</td>				
				
				<td>
														
				</td>
				<td width="5px">
				</td>
			</tr>	
		</table>
		
		<%-- 
		<iframe id="Body" name="Body" src="${ctx}/menuReclutamiento.html" scrolling="auto" frameborder="0"
		 height="530px" width="98%" style="align:center;margin-top:9px"></iframe>
		 --%>
		 
		 <table cellpadding="0" cellspacing="0" width="989px" border="0" align="center">
         <tr><td height="10px">&nbsp;</td></tr>
         <tr><td align="center">
         <iframe id="Body" name="Body" src="${ctx}/menuReclutamiento.html" scrolling="yes" frameborder="0" 
         height="532px" width="989px"></iframe>
         </td></tr>
         </table>
		 
	</form:form>	
</body>