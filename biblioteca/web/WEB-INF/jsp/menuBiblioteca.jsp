<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<head>
<script type="text/javascript">
		function onLoad(){
		}
		function fc_Regresar(){
			location.href =  "${ctx}/menuBibliotecaUsuario.html";
		}
		
		function fc_Perfiles(obj){
		objSede = document.getElementById("cboSede"); 					
		if ( objSede != null ) {					
			if ( document.getElementById("cboSede").value == "" ){						 
				alert(mstrDebeSelSede);
				return false;
			}
		}	
			
		  srtCod="A";
			switch(obj){
				case '1':							
					window.location.href = "${ctx}/biblioteca/biblio_admin_gestion_material.html?txhCodUsuario="+
						document.getElementById("txhCodUsuario").value+
						"&txhCodSede=" + document.getElementById("cboSede").value+
						"&txhSedeUsuario=" + document.getElementById("txhSedeUsuario").value;																				
					break;
				case '2':
					window.location.href = "${ctx}/biblioteca/bib_bandeja_buzon_sugerencias.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;					
					break;
				case '3': 			
					window.location.href = "${ctx}/biblioteca/bib_consulta_reserva_salas.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value
											+"&txhEstado="+srtCod
											+"&flgUsuAdmin=1"
											+"&txhCodSede=" + document.getElementById("cboSede").value
											+"&txhSedeUsuario=" + document.getElementById("txhSedeUsuario").value;
					break;					
				case '4':
					window.location.href = "${ctx}/biblioteca/bib_consulta_reportes.html?txhCodUsuario="+
											document.getElementById("txhCodUsuario").value
											+"&txhCodSede=" + document.getElementById("cboSede").value;
					
					break;
					/*window.location.href = "${ctx}/evaluaciones/bandeja_alumnos_tutor.html" +
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
											"&txhCodEvaluador=" + document.getElementById('txhCodEvaluador').value;*/
					//break;
				case '5': 
					window.location.href = "${ctx}/biblioteca/mantenimiento_configuracion_enlaces.html?txhCodUsuario="+
					                      document.getElementById("txhCodUsuario").value 
					                      +"&txhCodSede=" + document.getElementById("cboSede").value
					                      +"&txhSedeUsuario=" + document.getElementById("txhSedeUsuario").value;
					break;
				default:
					break;
			}
		}
		
	</script>
<title>CEDITEC - Centro de Documentación e Información de Tecsup</title>	
</head>
<body>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/menuBiblioteca.html">		
	<form:hidden path="codUsuario" id="txhCodUsuario"/>	
	<form:hidden path="sedeUsuario" id="txhSedeUsuario"/>
	<form:hidden path="sedeSelWork" id="txhSedeSelWork"/>

	<table cellspacing="0" cellpadding="0" border="0" width="989px" align="center" style="background-color: #00A0E4;">		
		<tr height="2px" bgcolor="#00A0E4">
			<td colspan="2" style="FONT-SIZE: 10px;TEXT-TRANSFORM: none;COLOR: #ffffff;FONT-FAMILY: Verdana;TEXT-DECORATION: none;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEDE :</b>&nbsp;
				<form:select path="codSede" id="cboSede" cssClass="combo_o" 
					cssStyle="width:100px">
					<c:if test="${control.listaSedes!=null}">
					<form:options itemValue="codSede" itemLabel="dscSede" 
						items="${control.listaSedes}" />
					</c:if>
				</form:select>
			</td>
			<!-- height="310px" -->
			<td rowspan="6" align="right"  width="463px">
			<img src="${ctx}/images/biblioteca/opciones-bib.jpg"></td>
		</tr>

		
		<%		
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_GMA") == 1) {
		%>

		<tr bgcolor="#00A0E4">
			<td width="80px" align="right" v>
				<img src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand">
			</td>			
			<td width="446px" style="height: 50px;">
				<a href="javascript:fc_Perfiles('1');" class="enlace">&nbsp;Gesti&oacute;n de Material Did&aacute;ctico</a>
			</td>
		</tr>
		<%
		}
		%>

		<%
		if (
		CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "BIB_BUZ") == 1 ) {
		%>
		<c:if test="${control.sedeUsuario=='L'}">
			<tr bgcolor="#00A0E4">
				<td width="80px" align="right" style="height: 50px;"><img
					src="${ctx}/images/iconos/ico_link.gif" ></td>
				<td width="446px" style="height: 50px;"><a href="javascript:fc_Perfiles('2');" class="enlace"
					>&nbsp;Administraci&oacute;n del Buz&oacute;n de Sugerencias</a></td>
			</tr>
		</c:if>
		<%
		}
		%>

		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "BIB_RES") == 1) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="80px" align="right" style="height: 50px;"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td width="446px" style="height: 50px;"><a href="javascript:fc_Perfiles('3');" class="enlace"
				style="cursor: hand">&nbsp;Gesti&oacute;n de Reserva de Salas</a></td>
		</tr>
		<%
		}
		%>

		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "BIB_RPT") == 1 ) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="80px" align="right" style="height: 50px;"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td width="446px" style="height: 50px;"><a href="javascript:fc_Perfiles('4');" class="enlace"
				style="cursor: hand">&nbsp;Consulta y Reportes</a></td>
		</tr>
		<%
		}
		%>

		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "BIB_ADM") == 1) {
		%>
		<c:if test="${control.sedeUsuario=='L' || control.sedeUsuario=='U'}">
			<tr bgcolor="#00A0E4">
				<td width="80px" align="right" style="height: 50px;"><img
					src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
				<td width="446px" style="height: 50px;"><a href="javascript:fc_Perfiles('5');" class="enlace"
					style="cursor: hand">&nbsp;Administraci&oacute;n del Sistema</a></td>
			</tr>
		</c:if>
		<%
		}
		%>
		
		
	</table>
</form:form>

<script type="text/javascript">	
	if (document.getElementById("txhSedeSelWork").value!=''){
		document.getElementById("cboSede").value=document.getElementById("txhSedeSelWork").value;		
	}
</script>

</body>