<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.bean.DatosCabeceraBean'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
	var refresh = "<%=(( request.getAttribute("refresh")==null)?"": request.getAttribute("refresh"))%>";	
	
	// console.log("var mensaje: " + mensaje);
	// console.log("var refresh: " + refresh);
	
	if(mensaje!="")
		alert(mensaje);	
		
	if(refresh=="TRUE")
		parent.fc_refrescar();
}

function validaTexto(nameObj,tamMax){
 	
 	fc_ValidaTextoEspecialEncuestas();
 	
	if(document.getElementById(nameObj).value.length > Number(tamMax))
	{
		str = document.getElementById(nameObj).value.substring(0,Number(tamMax));
		document.getElementById(nameObj).value=str;
		return false;
	}			
	
}


function fc_Grabar(){
	if(fc_validaGrabar()){
		
		if(confirm(mstrSeguroGrabar)){
					
			fc_generaEnvio();
			document.getElementById("operacion").value  ="GRABAR";
			// console.log("OPERACION: " + document.getElementById("operacion").value);
			//alert(document.getElementById("operacion").value);
			//alert("codEncuestado>>"+document.getElementById("txhCodEncuestado").value+">");											
			document.getElementById("frmMain").submit();
		}	
	}
}


function fc_generaEnvio(){

	var tam = document.getElementById("txhTamanio").value;
	
	var tknCodPregunta  = "";	
	var tknCodRespuesta = "";
	var numPreg = 0;
	
	var tknCodPreguntaAbierta  = "";
	var tknCodRespuestaAbierta = "";
	var numPregAbierta = 0;
	 
	for ( var i = 0; i < Number(tam); i++)
	{
			//OBJETO ACTUAL
			obj = document.getElementById("txhOpciones"+i);
			tamanioOpciones = document.getElementById("txhTamanioOpciones"+i).value;
			
			//tknCodPregunta = tknCodPregunta + document.getElementById("hidCodPregunta"+i).value + "|";
				
			if( document.getElementById("hidFlgCombo"+i).value == "1" )
			{	//COMBO PREGUNTAS CERRADAS
				numPreg = numPreg+1;
				tknCodPregunta = tknCodPregunta + document.getElementById("hidCodPregunta"+i).value + "|";
					
				if( document.getElementById("hidFlgMultiple"+i).value == "0" )
				{	//SIMPLE
					//if(fc_Trim(obj.value)!="")
					
					if(radioValue(i,tamanioOpciones)!="")
						tknCodRespuesta = tknCodRespuesta+radioValue(i,tamanioOpciones)+"|";
					else
						tknCodRespuesta = tknCodRespuesta+"x"+"|";
							
				}else
				{	//MULTIPLE
					//****************
					var countOcurrencias = 0;
					
					//alert("zzz>>>"+tamanioOpciones+">>>");
					for (var j = 0; j < tamanioOpciones; j++)
					{	
						//alert("obj>>>"+"txhOpciones"+i+""+j+">>>"+document.getElementById("txhOpciones"+i+""+j).checked);
						if ( document.getElementById("txhOpciones"+i+""+j).checked == true )
						{
								tknCodRespuesta = tknCodRespuesta+document.getElementById("txhOpciones"+i+""+j).value+"|";
								countOcurrencias = countOcurrencias + 1;
						}
					}
					
					if( countOcurrencias==0 )
						tknCodRespuesta = tknCodRespuesta+"x"+"|";
					
					//****************					
				}
				
				tknCodRespuesta = tknCodRespuesta.substring(0,tknCodRespuesta.length-1);
				tknCodRespuesta = tknCodRespuesta+"$";
				
			}else
			{	//TXT PREGUNTAS ABIERTAS
				numPregAbierta = numPregAbierta + 1;
				tknCodPreguntaAbierta = tknCodPreguntaAbierta + document.getElementById("hidCodPregunta"+i).value + "|";
			
				if(fc_Trim(obj.value)!="")
					tknCodRespuestaAbierta = tknCodRespuestaAbierta+fc_Trim(obj.value)+"|";
				else
					tknCodRespuestaAbierta = tknCodRespuestaAbierta+"x"+"|";
				
				tknCodRespuestaAbierta = tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1);	
				tknCodRespuestaAbierta = tknCodRespuestaAbierta+"$";//SEPARACION DE REGISTROS				
			}
					
	}	
		document.getElementById("tknCodPreguntaCerrada").value = tknCodPregunta.substring(0,tknCodPregunta.length-1);
		document.getElementById("tknCodRespuestaCerrada").value = tknCodRespuesta.substring(0,tknCodRespuesta.length-1);
		document.getElementById("tknNumPregCerrada").value = numPreg;

		document.getElementById("tknCodPreguntaAbierta").value = tknCodPreguntaAbierta.substring(0,tknCodPreguntaAbierta.length-1);
		document.getElementById("tknCodRespuestaAbierta").value = tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1);
		document.getElementById("tknNumPregAbierta").value = numPregAbierta;

}


/*retorna el valor de un radio si esta seleccionado sino retorna "" */
function radioValue(fila,tamanioOpciones){
	var xvalue = "";
	
	for ( var i = 0; i < Number(tamanioOpciones); i++)
	{
	 	if(document.getElementById("txhOpciones"+fila+""+i).checked == true)
	 	 xvalue = document.getElementById("txhOpciones"+fila+""+i).value;
	 	
	}
	
	return xvalue;
}



function fc_validaGrabar(){
	
	var tam = document.getElementById("txhTamanio").value;
	if(Number(tam)<1){
		alert("No hay registros para grabar");
		return 0;
	}	
	
	 for ( var i = 0; i < Number(tam); i++)
	 {
		if( document.getElementById("hidFlgObligatorio"+i).value == "1" && fc_Trim(document.getElementById("hidFlgCombo"+i).value) == "0" )
		{	//var texto = fc_Trim(document.getElementById("opc"+i).value) == "";
			if( fc_Trim(document.getElementById("txhOpciones"+i).value) == "" ){
				alert("Debe llenar todos los campos obligatorios");
				document.getElementById("txhOpciones"+i).focus();
				return 0;
			}
		}else if(  document.getElementById("hidFlgObligatorio"+i).value == "1" && fc_Trim(document.getElementById("hidFlgCombo"+i).value) == "1"  ){
			//RECORRE SUS MICROOPCIONES
			//tamanio es el tama�o de las opciones
			//i es la posicion de la fila o de la pregunta			
			tamanio = document.getElementById("txhTamanioOpciones"+i).value;
			var flgSeleccionado = fc_validaCheckedSeleccionado(i,tamanio);
			
			if( flgSeleccionado == 0 ){
				alert("Debe llenar todos los campos obligatorios");
				//document.getElementById("opc"+i).focus();
				return 0;		
			}
			
		}
	 }
			
	return 1;
}

function fc_validaCheckedSeleccionado(fila,tamanioOpciones){
	 
	 for ( var i = 0; i < Number(tamanioOpciones); i++)
	 {	 	
	 	if(document.getElementById("txhOpciones"+fila+""+i).checked == true)
	 	return 1;
	 	
	 }

	return 0;
}

function fc_funcionInnerIdTexto(id,texto){
	if(texto!="")
	document.getElementById(""+id).innerHTML = texto;	
}

</script>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">

<!-- MOTIVOS UTEC - TECSUP   - SIN LOGO SIN LOGO-->
<table style="width: 100%;" align="center">
<tr>
<td align="right" style="background-image: url('${ctx}/images/cabecera_2_encuesta.png'); background-repeat: repeat-x;">
	<img src="${ctx}/images/cabecera_encuesta_sin_logo.png" align="right" style="margin: -1px;"/>
</td>
</tr>

</table>

<%DatosCabeceraBean bean= (DatosCabeceraBean) request.getAttribute("OBJ_CABECERA"); %>
<%
if(bean==null){%>

<div style="padding-top: 100px; padding-bottom: 150px;">
<table align="center">
<tr>
<td>
<img src="${ctx}/images/encuesta_cerrada_jsp.png" align="right" style="margin: -1px;"/>
</td>
</tr>
</table>
</div>

<%} else {%>

<form:form commandName="control" id="frmMain" action="${ctx}/encuesta_anonimo/verEncuestasMixtas.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<!-- CARACTERISTICAS DE LA ENCUESTA -->
	<form:hidden path="txhCodPerfil" id="txhCodPerfil" />
	<form:hidden path="txhCodEncuestado" id="txhCodEncuestado" />
	<form:hidden path="txhCodProfesor" id="txhCodProfesor" />
	
	<input type="hidden" name="tknCodPreguntaCerrada" id="tknCodPreguntaCerrada" />
	<input type="hidden" name="tknCodRespuestaCerrada" id="tknCodRespuestaCerrada" />
	<input type="hidden" name="tknNumPregCerrada" id="tknNumPregCerrada" />
	<input type="hidden" name="tknCodPreguntaAbierta" id="tknCodPreguntaAbierta" />
	<input type="hidden" name="tknCodRespuestaAbierta" id="tknCodRespuestaAbierta" />
	<input type="hidden" name="tknNumPregAbierta" id="tknNumPregAbierta" />
	

	<%int varTamanio = 0; %>
	<%String tam = "0";%>
<%-- 	<%DatosCabeceraBean bean= (DatosCabeceraBean) request.getAttribute("OBJ_CABECERA"); %> --%>
	<%if(bean!=null ){ %>	
	<table cellpadding="0" cellspacing="2" 
			style="width:97%;margin-left: 6px;margin-top: 0px; margin-bottom:5px;border: 1px solid #048BBA">
		<TR class="tablagrilla" style="height: 20px;">
			<TD style="width: 20%;"><b>&nbsp;Encuesta :</b></TD>
			<TD style="width: 40%;">&nbsp;<%=bean.getNomEncuesta()%></TD>
			<TD style="width: 20%;"><b>&nbsp;Duracion:</b></TD>
			<TD style="width: 20%;">&nbsp;<%=bean.getDuracion()%>&nbsp;Min</TD>
		</TR>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0002".equalsIgnoreCase(bean.getCodTipoAplicacion()) ){%>
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado a :</b></TD>
			<TD>&nbsp;<%=bean.getNomServicio()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0001".equalsIgnoreCase(bean.getCodTipoAplicacion()) ){%>
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado al profesor :</b></TD>
			<TD>&nbsp;<%=bean.getNomProfesor()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
	</table>	
	<%}%>
	
	
<div style="overflow: auto; height: 450px;width:100%">
<table cellpadding="0" cellspacing="2" 
	style="width:97%;margin-left: 6px;margin-bottom:5px;border: 1px solid #048BBA">



<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{
		
		List consulta = (List) request.getAttribute("LST_RESULTADO");	
		
		varTamanio = consulta.size();
		
		String guia = "";
		String grupoVariable = "";
		String formatoVariable = "";
		VerEncuestaBean objFuturo =null;
		String grupoFutura = "";
		String formatoFutura = "";
		String preguntaVariable = "";
		String preguntaFutura = "";
		
		for (int i = 0; i < consulta.size(); i++)
		{		
			VerEncuestaBean obj = (VerEncuestaBean) consulta.get(i);			
			grupoVariable = obj.getIdGrupo();
			formatoVariable = obj.getIdSeccion();
			preguntaVariable = obj.getIdPregunta();
%>

		<!-- PINTAMOS EL INICIO FORMATO Y GRUPO -->
		
		<%
		if( i==0 ){%>
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="2">&nbsp;(*): Preguntas de caracter obligatorio.</td>
				</tr>
				<tr class="tablagrilla" style="height: 20px;">
					<td  colspan="1" style="width: 40%;">&nbsp;</td>
					<td  colspan="1" style="width: 60%;">&nbsp;</td>
				</tr>			
				<tr class="grilla" style="height: 20px;">
					<td  colspan="2" style="width: 100%;" align="left">&nbsp;<b><%=obj.getNomSeccion()%></b></td>
				</tr>			
				<tr class="tablagrilla" style="height: 20px;">
					<td bgcolor="#C8E8F0" colspan="2"><b>&nbsp;&nbsp;&nbsp;<%=obj.getNomGrupo()%></b></td>									
				</tr>
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td valign="top">
					<table width="100%" height="100%" bordercolor="red" border="0" cellpadding="0" cellspacing="0"><tr><td>
						<%for (int j = 0; j < obj.getLstAlternativas().size(); j++){
							VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
						%>	
							<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%>
							<b><%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
							<%}%>
						<%}%>
					</td></tr></table>
					</td>
				</tr>
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->				
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1"  width="<%=tam%>%;">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="blue" border="0" cellpadding="0" cellspacing="0"><tr>					
						<%
						int ancho=0;
						int nroAlternativas=obj.getLstAlternativas().size();
						ancho=100/nroAlternativas;
						for (int j = 0; j < obj.getLstAlternativas().size(); j++){
							VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
						%>
							<td align="center" width="<%=ancho%>%"><%=alt.getNomAlterntiva()%></td>
						<%}%>
					</tr></table>
					</td>
				</tr>
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->					
		<%}%>
		
		
		
		
		<!-- ************* PINTAMOS LOS REGISTROS INTERMEDIOS *************** -->
		<%if( i+1<consulta.size() )
		{	objFuturo = (VerEncuestaBean) consulta.get(i+1);
			grupoFutura = objFuturo.getIdGrupo();
			formatoFutura = objFuturo.getIdSeccion();
			preguntaFutura = objFuturo.getIdPregunta();		
			%>			
			<!--***************************** PINTA LAS PREGUNTA Y SUS PROPIEDASDES *****************************-->
			<tr>
			<td>
				<input type="hidden" id="hidFlgObligatorio<%=i%>"  value="<%=obj.getIndObligatorio()%>"  />
				<input type="hidden" id="hidFlgCombo<%=i%>"  		value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
				<input type="hidden" id="hidFlgMultiple<%=i%>"  	value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
				<input type="hidden" id="hidCodPregunta<%=i%>"  	value="<%=obj.getIdPregunta()%>"  />
				<input type="hidden" id="txhTamanioOpciones<%=i%>"  	value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  /> 
			</td>
			</tr>
			<tr class="tablagrilla">
				<!--***************** DATOS DE LA PREGUNTA ***************** -->
				<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>				
				
				<!-- ABIERTA O CERRRADA -->
				<td>
				<table width="100%" height="100%" bordercolor="black" border="0" cellpadding="0" cellspacing="0"><tr>				
				
				<%if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>			
					<td colspan="<%=obj.getLstAlternativas().size()%>">
					<textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>" 
					 onkeypress="validaTexto('txhOpciones<%=i%>','10000')"   
					 onblur="fc_ValidaTextoEspecialEncuestasOnblur('txhOpciones<%=i%>')"  
					  class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto"%>"   style="width: 98%;height:50px;"></textarea></td>
				<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>				
					<%for (int j = 0; j < obj.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);%>
				
						<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
							<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
							<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}%>
						
					<%}%>					
				<%}%>
				</tr></table>
				</td>
				<!-- FIN ABIERTA O CERRRADA -->
			</tr>
		<!--***************************** FIN PINTA LAS PREGUNTA *****************************-->			
			
				
				
				
			<%
			//PINTA LAS CABECERAS SIGUIENTES FORMATO  Y GRUPO
			if(!formatoVariable.equalsIgnoreCase(formatoFutura)){%>
				<!-- ****************PINTA FORMATO ****************-->
				<tr class="grilla" style="height: 20px;">				
					<td colspan="2" align="left">&nbsp;<b><%=objFuturo.getNomSeccion()%></b></td>										
				</tr>				
				<!-- *****************PINTA GRUPO ****************-->
				<tr class="tablagrilla" style="height: 20px;">
					<td bgcolor="#C8E8F0" colspan="2"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>								
				</tr>
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					
					<table width="100%" height="100%" bordercolor="blue" border="0" cellpadding="0" cellspacing="0">
					<tr><td>
					<%for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%> 
						<b><%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
						<%}%>
						
					<%}%>
					</td></tr>
					</table>
					</td>
				</tr>				
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
					<tr>					
					<%
					int ancho2=0;
					int nroAlternativas2=objFuturo.getLstAlternativas().size();
					ancho2=100/nroAlternativas2;
					for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<td  align="center" width="<%=ancho2%>%"><%=alt.getNomAlterntiva()%></td>
					<%}%>
					</tr></table>
					</td>
				</tr>
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->				
			<%
			}else if(!grupoVariable.equalsIgnoreCase(grupoFutura))
			{/**PINTA LAS CABECERAS SIGUIENTES GRUPO*/%>
				<!-- *****************PINTA GRUPO ****************-->
				<tr class="tablagrilla" style="height: 20px;">
					<td bgcolor="#C8E8F0" colspan="2"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>										
				</tr>				
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
					<tr><td>
					<%for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%>
						<b><%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
						<%}%>
					<%}%>
					</td></tr></table>
					</td>
				</tr>				
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->				
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
					<tr>
					<%
					int ancho3=0;
					int nroAlternativas3=objFuturo.getLstAlternativas().size();
					ancho3=100/nroAlternativas3;
					for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<td align="center" width="<%=ancho3%>%"><%=alt.getNomAlterntiva()%></td>
					<%}%>
					</tr></table>
					</td>
				</tr>
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->				
			<%						
			}else if(grupoVariable.equalsIgnoreCase(grupoFutura)){
				if(objFuturo.getNivelAlternativa().equalsIgnoreCase("2")){%>
				
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="orange" border="0" cellpadding="0" cellspacing="0">
					<tr><td>
					<%for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%><b>
						<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%> 
						<%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
						<%}%>
					<%}%>
					</td></tr></table>
					</td>
				</tr>				
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->				
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
						<table width="100%" height="100%" bordercolor="blue" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<%
						int ancho4=0;
						int nroAlternativas4=objFuturo.getLstAlternativas().size();
						ancho4=100/nroAlternativas4;
						for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
							VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
						%>
							<td align="center" width="<%=ancho4%>%"><%=alt.getNomAlterntiva()%></td>
						<%}%>
						</tr>
						</table>
					</td>
				</tr>				
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
					
				<%}
			}
			// FIN PINTA LAS CABECERAS SIGUIENTES
			
		}
		
		
		
		
		
		///*************PINTAMOS EL ULTIMO REGISTRO*************** 
		if( i+1==consulta.size()){
		%>
		<!--***************************** PINTA LAS PREGUNTA *****************************-->
			<tr>
			<td>
				<input type="hidden" id="hidFlgObligatorio<%=i%>" value="<%=obj.getIndObligatorio()%>"  />
				<input type="hidden" id="hidFlgCombo<%=i%>" value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
				<input type="hidden" id="hidFlgMultiple<%=i%>" value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
				<input type="hidden" id="hidCodPregunta<%=i%>" value="<%=obj.getIdPregunta()%>"  />
				<input type="hidden" id="txhTamanioOpciones<%=i%>" value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  />			
			</td>
			</tr>
			<tr class="tablagrilla">
				<!--***************** DATOS DE LA PREGUNTA ***************** -->
				<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>										
				
				<td>
				<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
				<tr>					

				<%				
				if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>				
				
					<td colspan="<%=obj.getLstAlternativas().size()%>">
					<textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>"  
					 onkeypress="validaTexto('txhOpciones<%=i%>','10000')"
					 onblur="fc_ValidaTextoEspecialEncuestasOnblur('txhOpciones<%=i%>')"  
					  class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto" %>" style="width:98%;height:50px;"></textarea>
					</td>
					
				<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>
				
					<%/**FOR**/				
					for (int j = 0; j < obj.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
					%>
				
						<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
							<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
							<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}%>
					
					<%}
					/**FOR**/%>
				<%}%>
				</tr>
				</table>
				</td>
			</tr>
		<!--***************************** /PINTA LAS PREGUNTA *****************************-->	
		<%}
	}/*FOR*/		
  }/*IF*/
%>
</table>

	<table width="50%" border="0" align="center" style="margin-top: 10px" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" valign="bottom">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviar','','/SGA/images/botones/enviar2.jpg',1)">
					<img src="/SGA/images/botones/enviar1.jpg" onclick="javascript:fc_Grabar();" style="cursor:pointer;" id="imgEnviar" alt="Enviar">
				</a>
			</td>
		</tr>
	</table>
</div>	
	
<!--CANTIDAD DE PREGUNTAS-->
<input type="hidden" name="txhTamanio" id="txhTamanio" value="<%=varTamanio%>" />

</form:form>

<%} %>


</body>
</html>