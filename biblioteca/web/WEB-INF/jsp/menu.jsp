<%@ include file="/taglibs.jsp"%>
<%if(request.getSession().getAttribute("usuarioSeguridad")==null){
	response.sendRedirect("/SGA/logeo.html");
}
 %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 transitional//EN" "http://www.w3.org/tr/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script type="text/javascript" type="text/javascript">
		function onLoad(){
			fc_Display();
		}
		
		function fc_Perfiles(obj){
			var tipo = document.getElementById('txhType').value;
			
			switch(obj){
				case '1':
					if(fc_Trim(tipo) == '04'){
						window.location.href = "${ctx}/reclutamiento/rec_mto_conf_inicio.html";
					};
					if(fc_Trim(tipo) == '05'){
						window.location.href = "${ctx}/eva/mto_bandeja.html";
					}
					break;
				case '2':
					if(fc_Trim(tipo) == '04'){
						window.location.href = "${ctx}/reclutamiento/logueo_reclutamiento.html";
					}
					if(fc_Trim(tipo) == '05'){
						window.location.href = "${ctx}/evaluaciones/eva_numero_eva.html";
					}
					break;
				case '3':
					if(fc_Trim(tipo) == '04'){
						window.location.href = "${ctx}/reclutamiento/procesos_bandeja.html";
					}
					if(fc_Trim(tipo) == '05'){
						window.location.href = "${ctx}/evaluaciones/bandejaEvaluador.html?txhCodPeriodo="+
											document.getElementById('txhCodPeriodo').value + "&txhCodEval="+
											document.getElementById('txhCodEval').value;
					}
					break;
				case '4':
					if(fc_Trim(tipo) == '05'){
						window.location.href = "${ctx}/evaluaciones/bandeja_alumnos_tutor.html" +
											"?txhCodPeriodo=" + document.getElementById('txhCodPeriodo').value + 
											"&txhCodEval=" + document.getElementById('txhCodEval').value;
					}
					break;
				case '5': 
					if(fc_Trim(tipo) == '05'){
						window.location.href = "${ctx}/evaluaciones/bandeja_gest_administrativa.html?txhCodPeriodo="+
											document.getElementById('txhCodPeriodo').value + "&txhCodEval="+
											document.getElementById('txhCodEval').value;
					}
					break;
				case '6':
					if(fc_Trim(tipo) == '05'){
						window.location.href = "${ctx}/evaluaciones/eva_nota_casos_especiales.html";
					}
					break;
				case '7':
				
						if(fc_Trim(tipo) == '05'){
						window.location.href = "${ctx}/evaluaciones/eva_formacion_empresa.html";
						}
				
					
					break;
				case '8':
					window.location.href = "${ctx}/evaluaciones/consultarEvaluaciones.html?codAlu=20020510";
					break;
				default:
					break;
			}
		}
		
		function fc_Display(){
			var tipo = document.getElementById('txhType').value;
			
			divBiblioteca.style.display = "none";
			divEvaluaciones.style.display = "none";
			divReclutamiento.style.display = "none";
			
			if(tipo != ""){
				switch(tipo){
					case '01':
						document.getElementById('divBiblioteca').style.display = "inline-block";
						break;
					case '02':
						document.getElementById('divEvaluaciones').style.display = "inline-block";
						break;
					case '03':
						document.getElementById('divEvaluaciones').style.display = "inline-block";
						break;
					case '04':
						document.getElementById('divReclutamiento').style.display = "inline-block";
						break;
					case '05':
						document.getElementById('divEvaluaciones').style.display = "inline-block";
						break;
				}
			}
		}
	</script>
</head>

<body>
	<form:form name="frmMain" commandName="control" action="${ctx}/menu.html">
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEval" id="txhCodEval"/>
		<form:hidden path="type" id="txhType"/>
		
		<div id="divBiblioteca" style="display:none">
		<table cellSpacing="2" align=center cellPadding="2" border="0" style="HEIGHT: 18px; WIDTH: 100%; margin-top:50px" bordercolor="green">
			<tr><td width="30%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('1');" class="enlace" style="cursor:hand">Usuario Web</a></td>
			</tr>
			<tr><td width="30%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('2');" class="enlace" style="cursor:hand">Usuario Administrador</a></td>
			</tr>
		</table>
		</div>
		
		<div id="divReclutamiento" style="display:none">
			<table cellSpacing="0" align=center cellPadding="0" border="0" style="height:18px; width:98%" bordercolor="green">
				<tr>
					<td></td>
					<td></td>	
					<td rowspan="9" align="right" height="270px" ><img src="${ctx}/images/reclutamiento/opciones-reclut.jpg"></td>																
				</tr>					
				<tr bgcolor="AD222F"><td width="10%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
					<td width="40%"><a href="javascript:fc_Perfiles('1');" class="enlace" style="cursor:hand">Mantenimiento y Configuraciones(Perfil RRHH) </a></td>
				</tr>	
				<tr bgcolor="AD222F"><td width="10%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
					<td><a href="javascript:fc_Perfiles('2');" class="enlace" style="cursor:hand">Registro de Datos Personales (Perfil Postulante)</a></td>
				</tr>
				<tr bgcolor="AD222F"><td width="10%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
					<td><a href="javascript:fc_Perfiles('3');" class="enlace" style="cursor:hand">Gestionar Procesos(Perfil RRHH)</a></td>
				</tr>	
				<tr bgcolor="AD222F"><td width="10%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
					<td><a href="javascript:fc_Perfiles('4');" class="enlace" style="cursor:hand">Evaluar Postulante (Perfil Jefe Dpto / Evaluador)</a></td>
				</tr>
				<tr bgcolor="AD222F"><td width="10%" align="right">	<img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
					<td><a href="javascript:fc_Perfiles('5');" class="enlace" style="cursor:hand">Explotación de Información(Perfil RRHH / Consulta)</a></td>
				</tr>			
			</table>
		</div>
		
		<div id="divEvaluaciones" style="display:none">
		<table  cellSpacing="0" align=center cellPadding="2" border="0" style="height:18px; width:96%; margin-top:3px" bordercolor="green">
			<tr height="2px">
				<td></td>
				<td></td>
				<td rowspan="15" align="right" height="280px" width="463px"><img src="${ctx}/images/Evaluaciones/opciones-eval.jpg"></td>
			</tr>
			<tr bgcolor="#00A0E4" height="5px">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr bgcolor="#00A0E4"><td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('1');" class="enlace" style="cursor:hand">Mantenimiento y Configuraciones (Perfil Configurador)</a></td>
			</tr>
			
			<tr bgcolor="#00A0E4"><td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('3');" class="enlace" style="cursor:hand">Carga Operativa del Evaluador (Perfil Evaluador)</a></td>
			</tr>
			<tr bgcolor="#00A0E4"><td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('4');" class="enlace" style="cursor:hand">Registro de Perfiles del Alumno (Perfil Tutor)</a></td>
			</tr>
			<tr bgcolor="#00A0E4"><td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('5');" class="enlace" style="cursor:hand">Gesti&oacute;n Administrativo (Perfil Administraci&oacute;n)</a></td>
			</tr>	
			<tr bgcolor="#00A0E4"><td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('6');" class="enlace" style="cursor:hand">Registro Nota - Casos Especiales (Perfil Administraci&oacute;n)</a></td>
			</tr>	
			<tr bgcolor="#00A0E4"><td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('7');" class="enlace" style="cursor:hand">Control Formaci&oacute;n de Empresa - PFR (Perfil Administraci&oacute;n)</a></td>
			</tr>	
			<tr bgcolor="#00A0E4"><td width="6%" align="right"><img src="${ctx}/images/iconos/ico_link.gif" style="cursor:hand"></td>
				<td><a href="javascript:fc_Perfiles('8');" class="enlace" style="cursor:hand">Consultar Evaluaciones (Perfil Estudiante)</a></td>
			</tr>	
			<tr bgcolor="#00A0E4" height="5px">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</div>
	</form:form>	
</body>