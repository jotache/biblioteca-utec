<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
.tablagrilla02
{
    BACKGROUND-COLOR: #C8E8F0;
	FONT-SIZE: 8pt;
    /*TEXT-TRANSFORM: capitalize;*/
    WIDTH: 100%;
    FONT-FAMILY: Arial;
    color: #606060;
    
}
</style>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){
		//PARA DEFINIR LOS DAOSDEL USUARIO OEL PERFIL
		fc_AsignarDatosPerfilUsuario();
		//*******************************************
		document.getElementById("txhOperacion").value="";
		document.getElementById("txhMsg").value="";
		document.getElementById("txhCodEncuesta").value="";
		fc_TipoServicio();
		var objBuscar = "<%=((request.getAttribute("iFrameBandeja")==null)?"":(String)request.getAttribute("iFrameBandeja"))%>";
		var mensaje = "<%=((request.getAttribute("mensaje")==null)?"":(String)request.getAttribute("mensaje"))%>";
		
		if(mensaje!="")
		alert(mensaje)
		
		if(objBuscar=="OK")
		{ 
		  fc_Buscar();
		}
		//fc_IntercalarColorColumnas();
	}
	
	function fc_AsignarDatosPerfilUsuario(){
		perfilUsuario = document.getElementById("txhPerfilUsuario").value;
		if(perfilUsuario != "0"){
			document.getElementById("txhCodResponsable").value=document.getElementById("txhCodUsuario").value;
			document.getElementById("responsable").value=document.getElementById("txhNomUsuario").value;			
			botonBuscarResponsable.style.display="none";
		}		
	}
	
	function fc_Terminar(){
	
		codEncuesta = document.getElementById("txhCodEncuesta").value;
				
		if(codEncuesta==""){
			alert(mstrSeleccion);
			return false;			
		}
		else{
			if( confirm("�Esta seguro de terminar la Encuesta?") ){
				document.getElementById("txhOperacion").value = "TERMINAR";
				document.getElementById("control").submit();
				
			}
		}
	}
		
	function fc_IntercalarColorColumnas(){
		tamTabla=document.getElementById("txhTamListaBandeja").value;
		//alert(tamTabla);
		for(var i=0;i<tamTabla;i++){			
			trHijo=document.getElementById("tablaBandeja").rows[i];
			//alert(trHijo.className);
			if(i%2){				
				for(var j=0;j<9;j++){
					trHijo.cells[j].className="tablagrilla02";					
				}					
			}
		}
	}
	function fc_Regresar(){
		document.location.href = "${ctx}/menuEncuesta.html"
	}
	function fc_BuscarResponsable(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		Fc_Popup("${ctx}/encuestas/busqueda_empleados.html?txhCodUsuario="+codUsuario,660,460);	
	}
	function fc_ActualizarResponsable(codResponsable, nomResponsable){
		document.getElementById("txhCodResponsable").value=codResponsable;
		document.getElementById("responsable").value=nomResponsable;
	}
	function fc_Buscar(){
		/*document.getElementById("txhOperacion").value = "BUSCAR";
		document.getElementById("frmMain").submit();*/
// 		alert(document.getElementById("codTipoEstado").value);
		
		document.getElementById("iFrameBandejaAplicacion").src="${ctx}/encuestas/bandejaAplicacionSeguimientoIframe.html?"+
	  	"txhCodUsuario="+document.getElementById("txhCodUsuario").value +
	  	"&txhParCodTipoAplicacion=" + document.getElementById("codTipoAplicacion").value +
		"&txhParCodTipoEncuesta=" + document.getElementById("codTipoEncuesta").value +
		"&txhParCodTipoServicio=" + document.getElementById("codTipoServicio").value +
		"&txhParCodTipoEstado=" + document.getElementById("codTipoEstado").value +
		"&txhParCodEncuesta=" + document.getElementById("nroEncuesta").value +
		"&txhParResponsable=" + document.getElementById("txhCodResponsable").value +
		"&txhParBandera=1" ;
	}
	function fc_Limpiar(){
		perfilUsuario = document.getElementById("txhPerfilUsuario").value;
		if(perfilUsuario == "0"){
			document.getElementById("txhCodResponsable").value="";
			document.getElementById("responsable").value="";
		}
		document.getElementById("nroEncuesta").value="";
		document.getElementById("codTipoAplicacion").value="";
		document.getElementById("codTipoEncuesta").value="";
		document.getElementById("codTipoServicio").value="";
		document.getElementById("codTipoEstado").value=document.getElementById("txhConsCodEncEnAplicacion").value;
	}	
	function fc_RegistrarManual(){ 
		codEncuesta = document.getElementById("txhCodEncuesta").value;
		indEncuesta = document.getElementById("txhIndEncuesta").value;
		if(codEncuesta==""){
			alert(mstrSeleccion);
			return false;			
		}
		if(indEncuesta=="" || indEncuesta=="0"){
			alert("Debe Seleccionar una encuesta manual.");
			return false;
		}
		codUsuario=document.getElementById("txhCodUsuario").value;
		codEncuesta=document.getElementById("txhCodEncuesta").value;
		window.location.href="${ctx}/encuestas/registroManual.html?txhCodigo="+codEncuesta+
		 "&txhUsuario="+codUsuario +
		 "&txhParCodTipoAplicacion=" + document.getElementById("codTipoAplicacion").value +
		 "&txhParCodTipoEncuesta=" + document.getElementById("codTipoEncuesta").value +
		 "&txhParCodTipoServicio=" + document.getElementById("codTipoServicio").value +
		 "&txhParCodTipoEstado=" + document.getElementById("codTipoEstado").value +
		 "&txhParCodEncuesta=" + document.getElementById("nroEncuesta").value +
		 "&txhParResponsable=" + document.getElementById("responsable").value +
		 "&txhParCodResponsable=" + document.getElementById("txhCodResponsable").value +
		 "&txhParBandera=1" ;
	
	}
	
	
	function fc_TipoAplicacion(){
		codTipoAplicacion = document.getElementById("codTipoAplicacion").value;
		if(codTipoAplicacion != document.getElementById("txhConsteAplicacionServicio").value){
			td_TipoServicio.style.display = 'none';
	  		Tab_Servicio.style.display = 'inline-block';
	  		document.getElementById("codTipoServicio").value="";
  		}
  		else
  		if(codTipoAplicacion == document.getElementById("txhConsteAplicacionServicio").value){
  			td_TipoServicio.style.display = 'inline-block';
	  		Tab_Servicio.style.display = 'none';
	  		document.getElementById("codTipoServicio").value="";
	  	}	   
	}
	function fc_TipoServicio(){
		codTipoAplicacion = document.getElementById("codTipoAplicacion").value;
		if(codTipoAplicacion != document.getElementById("txhConsteAplicacionServicio").value){
			td_TipoServicio.style.display = 'none';
	  		Tab_Servicio.style.display = 'inline-block';	  		
  		}
  		else
  		if(codTipoAplicacion == document.getElementById("txhConsteAplicacionServicio").value){
  			td_TipoServicio.style.display = 'inline-block';
	  		Tab_Servicio.style.display = 'none';	  		
	  	}	   
	}	
	function fc_SeleccionarRegistro(codEncuesta, indicador){
		//alert(codEncuesta);
		document.getElementById("txhCodEncuesta").value=codEncuesta;
		document.getElementById("txhIndEncuesta").value=indicador;
	}
	function fc_AmpliarPeriodo(){
		codEncuesta = document.getElementById("txhCodEncuesta").value;
		if(codEncuesta==""){
			alert(mstrSeleccion);
			return false;			
		}
		codUsuario=document.getElementById("txhCodUsuario").value;
		codEncuesta=document.getElementById("txhCodEncuesta").value;
		Fc_Popup("${ctx}/encuestas/ampliar_periodo_aplicacion.html?txhCodEncuesta="+codEncuesta+"&txhCodUsuario="+codUsuario,500,260);
	}
	function fc_VerEncuesta(srtCodEncuesta, srtCodTipoEncuesta){
		codUsuario=document.getElementById("txhCodUsuario").value;
		if(srtCodTipoEncuesta=="0001"){
			Fc_Popup_Resizable("${ctx}/encuestas/visualizarEncuestaEstandar.html?txhUsuario="+codUsuario+
				"&txhCodEncuesta="+srtCodEncuesta,800,600);
		}
		else
		if(srtCodTipoEncuesta=="0002"){								
			Fc_Popup_Resizable("${ctx}/encuestas/visualizarEncuestaMixta.html?txhUsuario="+codUsuario+
				"&txhCodEncuesta="+srtCodEncuesta,800,600);
		}	  	
	}
</script>
</head>
<body>
<form:form name="frmMain" commandName="control" action="${ctx}/encuestas/bandejaAplicacionSeguimiento.html" enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="constePrograma" id="txhConstePrograma"/>
	<form:hidden path="consteServicio" id="txhConsteServicio"/>
	<form:hidden path="codSeleccion" id="txhCodSeleccion"/>
	<form:hidden path="consteAplicacionPrograma" id="txhConsteAplicacionPrograma"/>
	<form:hidden path="consteAplicacionServicio" id="txhConsteAplicacionServicio"/>
	
	<form:hidden path="codResponsable" id="txhCodResponsable"/>
	<form:hidden path="tamListaBandeja" id="txhTamListaBandeja"/>
	
	<form:hidden path="codEncuesta" id="txhCodEncuesta"/>
	<form:hidden path="indEncuesta" id="txhIndEncuesta"/>
	<form:hidden path="consCodEncEnAplicacion" id="txhConsCodEncEnAplicacion"/>	
	
	<form:hidden path="nomUsuario" id="txhNomUsuario"/>
	<form:hidden path="perfilUsuario" id="txhPerfilUsuario"/>
	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>	
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Bandeja de Aplicaci�n y Seguimiento					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- CABECERA -->	       
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red">
		<tr>
			<td width="10%">Nro. Encuesta:</td>
			<td width="15%">
			   <form:input path="nroEncuesta" id="nroEncuesta" maxlength="50"
				onkeypress="fc_ValidaNombreAutorOnkeyPress();"
				onblur="fc_ValidaNombreAutorOnblur(this.id,'C�d. Encuesta');" 
				cssClass="cajatexto" size="25"/>
			</td>
			<td width="10%">Responsable:</td>
			<td width="30%">
				<form:input path="responsable" id="responsable" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();" readonly="true"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Responsable');" 
					cssClass="cajatexto" cssStyle="width: 80%;"/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscarRes','','${ctx}/images/iconos/buscar2.jpg',1)" id="botonBuscarResponsable">
				   <img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar Responsable" align="middle" 
					 style="cursor: pointer;" onclick="javascript:fc_BuscarResponsable();" id="imgBuscarRes"></a>				
			</td>
			<td width="25%">Estado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<form:select path="codTipoEstado" id="codTipoEstado" cssClass="cajatexto" 
					cssStyle="width:120px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listaTipoEstado!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.listaTipoEstado}" />
					</c:if>
			    </form:select>
			</td>
			<td width="10%">&nbsp;
			</td>
		</tr>	
		<tr>
			<td width="10%">Tipo Aplicaci�n:</td>
			<td width="15%">
				<form:select path="codTipoAplicacion" id="codTipoAplicacion" cssClass="cajatexto" 
							cssStyle="width:120px" onchange="javascript:fc_TipoAplicacion();">
							<form:option value="">--Todos--</form:option>
							<c:if test="${control.listaTipoAplicacion!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoAplicacion}" />
							</c:if>
			   </form:select>
			</td>
			<td width="10%">Tipo Encuesta:</td>
			<td width="30%">
			     <form:select path="codTipoEncuesta" id="codTipoEncuesta" cssClass="cajatexto" 
							cssStyle="width:120px">
							<form:option value="">--Todos--</form:option>
							<c:if test="${control.listaTipoEncuesta!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoEncuesta}" />
							</c:if>
			   </form:select>
			</td>
			<td ID="td_TipoServicio" style="display:none;" width="25%" valign="middle">Tipo Servicio:&nbsp;&nbsp;
				<form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto" 
					cssStyle="width:120px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listaTipoServicio!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.listaTipoServicio}" />
					</c:if>
			     </form:select>
			</td>
			<td width="25%" id="Tab_Servicio">&nbsp;</td>
			<td align="right" width="10%">
			    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
						style="cursor: pointer; 60px" onclick="javascript:fc_Limpiar();" id="imgLimpiar">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" id="imgBuscar"
						style="cursor: pointer; margin-bottom: " onclick="javascript:fc_Buscar();"></a>
		   </td>
		</tr>
	</table>
	<!-- BANDEJA -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td>
			<!-- div style="overflow: auto; height:350px;width:100%">
				<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>
						<td class="grilla" style="width: 3%">Sel</td>						
						<td class="grilla" style="width: 10%">Cod. Encuesta</td>
						<td class="grilla" style="width: 25%">Nombre</td>						
						<td class="grilla" style="width: 15%">Tipo Aplicaci�n</td>
						<td class="grilla" style="width: 10%">Tipo Encuesta</td>						
						<td class="grilla" style="width: 10%">Encuestados</td>
						<td class="grilla" style="width: 10%">Fecha Hora<br>Inicio</td>						
						<td class="grilla" style="width: 10%">Fecha Hora<br>Fin</td>
						<td class="grilla" style="width: 8%">Ind.<br>Manual</td>																			
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}"  >
					<tr>						
						<td align="center" class="tablagrilla" style="width: 3%">
							<input type="radio" name="encuesta"
								value='<c:out value="${lista.codEncuesta}" />'
								id='txhRbtEnc<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro(this.value);">								
						</td>						
						<td align="center" class="tablagrilla" style="width: 10%">
							${lista.nroEncuesta}														
						</td>
						<td align="left" class="tablagrilla" style="width: 25%">
							${lista.nombreEncuesta}														
						</td>
						<td align="left" class="tablagrilla" style="width: 15%">
							${lista.nomTipoAplicacion}														
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							${lista.nomTipoEncuesta}														
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							${lista.totalEncuestadosCrpta}														
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							${lista.fecIniAplicacion}														
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							${lista.fecFinAplicacion}														
						</td>						
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:if test='${lista.indEncuestaManual == "1"}'>							
								<img src="/SGA/images/iconos/check.gif" >							
							</c:if>																					
						</td>
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaBandeja == "0"}'>
					<tr>
						<td align="center" class="tablagrilla" colspan="9">						
							No se encontraron Registros
						</td>
					</tr>
					</c:if>
				</table>
			</div-->
			<iframe id="iFrameBandejaAplicacion" name="iFrameBandejaAplicacion" frameborder="0" height="360px" width="100%">
				</iframe>
			</td>
		</tr>
	</table>
	<!-- BOTONERIA -->
	<table width="80%" border="0" align="center" style="margin-top: 10px">
		<tr>			
			<td align="center">				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAmpliarPeriodo','','/SGA/images/botones/ampliarperiodo2.jpg',1)">
				<img src="/SGA/images/botones/ampliarperiodo1.jpg" onclick="fc_AmpliarPeriodo()" style="cursor:pointer;" id="imgAmpliarPeriodo"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegmanual','','/SGA/images/botones/regmanual2.jpg',1)">
				<img src="/SGA/images/botones/regmanual.jpg" onclick="fc_RegistrarManual()" style="cursor:pointer;" id="imgRegmanual"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgTerminar','','/SGA/images/botones/terminar2.jpg',1)">
				<img src="/SGA/images/botones/terminar1.jpg" onclick="fc_Terminar()" style="cursor:pointer;" id="imgTerminar"></a>
			</td>
		</tr>
	</table>
</form:form>
</body>
</html>