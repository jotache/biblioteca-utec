<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

	function onLoad(){
		
		var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
		var refresh = "<%=(( request.getAttribute("refresh")==null)?"": request.getAttribute("refresh"))%>";
		
		if( refresh!="" )
			fc_refrescarGrilla();
		
		if(mensaje!="")
			alert(mensaje);
	}

	function fc_Focus(index){
		var tam = document.getElementById("txhTamanio").value;
		var tama�o = tam-1;		
		if(index < tama�o){			
			index++;			
			if (window.event.keyCode==13 || window.event.keyCode==0) {
			    document.getElementById("opc"+index).focus();
			}
		}
	}

	function validaTexto(nameObj,tamMax){
	 	fc_ValidaTextoEspecialEncuestas();
		if(document.getElementById(nameObj).value.length > Number(tamMax)){
			str = document.getElementById(nameObj).value.substring(0,Number(tamMax));
			document.getElementById(nameObj).value=str;
			return false;
		}
	}

	function fc_Regresar(){	 
		parent.fc_Regresar(); 
	}

	function fc_refrescarGrilla(){
		parent.document.getElementById("iframeGrilla").contentWindow.fc_refrescar();
	}

	function fc_Procesa(pos,cadCodRespuesta,flgSimple,codEncuestado){
	
		document.getElementById("txhCodEncuestado").value = codEncuestado;
		document.getElementById("operacion").value = "ACTUALIZAR";
		
		if(flgSimple=="1"){
			processSimple(pos,cadCodRespuesta);
		}else if(flgSimple=="0"){
			processComplejo(pos,cadCodRespuesta);
		}
	}

	function processSimple(pos,cadCodRespuesta){
		//PRA PROCESSAR OBJETOS SIMPLES COMBOS,AREASDE TEXTO
		document.getElementById("opc"+pos).value = fc_Trim(cadCodRespuesta);
	}

function processComplejo(pos,cadCodRespuesta){
	//PRA PROCESSAR OBJETOS MULTISELECT
	
	ArrCodSel = cadCodRespuesta.split("|");
	cboDemo = document.getElementById("opc"+pos);
	cboDemo.value = "";//RESETEAMOS 
	
	//ASIGNAMOS
	for (i=0;i<=ArrCodSel.length-1;i++)
	{
			//alert("<"+ArrCodSel[i]+">");
			for (var j=0; j<=cboDemo.length - 1; j++){			
			   if ( cboDemo[j].value == ArrCodSel[i] ){
				  cboDemo[j].selected = true;
			   }			   			
			}		
	}
}

function fc_MuestraPregunta(pos){
	var texto = document.getElementById("hidNomPregunta"+pos).value;
	parent.fc_MuestraPregunta(texto);
}

function fc_Limpiar(){

	var tam = document.getElementById("txhTamanio").value;
	
	for ( var i = 0; i < Number(tam); i++)
	{
		document.getElementById("opc"+i).value="";
	}
	
	//RESETEAMOS LA OPERACION A GRABAR (PUEDE SER MODIFICAR)
	document.getElementById("operacion").value = "GRABAR";
	
	parent.fc_Limpiar();

}

function fc_Grabar(){

	if(fc_validaGrabar()){		
		if(confirm(mstrSeguroGrabar)){
			fc_generaEnvio();											
			document.getElementById("frmMain").submit();
		}
	}
}

function fc_generaEnvio(){

	var tam 			= document.getElementById("txhTamanio").value;
	
	var tknCodPregunta  = "";	
	var tknCodRespuesta = "";
	var numPreg = 0;
	
	var tknCodPreguntaAbierta  = "";
	var tknCodRespuestaAbierta = "";
	var numPregAbierta = 0;
	 
	for ( var i = 0; i < Number(tam); i++)
	{
			//OBJETO ACTUAL
			obj = document.getElementById("opc"+i);
			
			//tknCodPregunta = tknCodPregunta + document.getElementById("hidCodPregunta"+i).value + "|";
				
			if( document.getElementById("hidFlgCombo"+i).value == "1" )
			{	//COMBO PREGUNTAS CERRADAS
				numPreg = numPreg+1;
				tknCodPregunta = tknCodPregunta + document.getElementById("hidCodPregunta"+i).value + "|";
					
				if( document.getElementById("hidFlgMultiple"+i).value == "0" )
				{	//SIMPLE
					if(fc_Trim(obj.value)!="")
						tknCodRespuesta = tknCodRespuesta+fc_Trim(obj.value)+"|";
					else
						tknCodRespuesta = tknCodRespuesta+""+"|";
						//tknCodRespuesta = tknCodRespuesta+"x"+"|";	
				}else
				{	//MULTIPLE
					//****************
					var countOcurrencias = 0
					for (var j = 0; j < obj.length; j++)
					{
						if ( obj.options[j].selected )
						{
								tknCodRespuesta = tknCodRespuesta+obj.options[j].value+"|";
								countOcurrencias = countOcurrencias + 1;
						}
					}
					
					if( countOcurrencias==0 )
						tknCodRespuesta = tknCodRespuesta+""+"|";
						//tknCodRespuesta = tknCodRespuesta+"x"+"|";
					
					//****************					
				}
				
				tknCodRespuesta = tknCodRespuesta.substring(0,tknCodRespuesta.length-1);
				tknCodRespuesta = tknCodRespuesta+"$";
				
			}else
			{	//TXT PREGUNTAS ABIERTAS
				numPregAbierta = numPregAbierta + 1;
				tknCodPreguntaAbierta = tknCodPreguntaAbierta + document.getElementById("hidCodPregunta"+i).value + "|";
			
				if(fc_Trim(obj.value)!="")
					tknCodRespuestaAbierta = tknCodRespuestaAbierta+fc_Trim(obj.value)+"|";
				else
					tknCodRespuestaAbierta = tknCodRespuestaAbierta+""+"|";
					//tknCodRespuestaAbierta = tknCodRespuestaAbierta+"x"+"|";
				
				tknCodRespuestaAbierta = tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1);	
				tknCodRespuestaAbierta = tknCodRespuestaAbierta+"$";//SEPARACION DE REGISTROS				
			}
		
		
					
	}	
		document.getElementById("tknCodPreguntaCerrada").value = tknCodPregunta.substring(0,tknCodPregunta.length-1);
		document.getElementById("tknCodRespuestaCerrada").value = tknCodRespuesta.substring(0,tknCodRespuesta.length-1);
		document.getElementById("tknNumPregCerrada").value = numPreg;

		document.getElementById("tknCodPreguntaAbierta").value = tknCodPreguntaAbierta.substring(0,tknCodPreguntaAbierta.length-1);
		document.getElementById("tknCodRespuestaAbierta").value = tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1);
		document.getElementById("tknNumPregAbierta").value = numPregAbierta;
		
		
	//alert("tknCodPregunta>"+tknCodPregunta.substring(0,tknCodPregunta.length-1)+">");
	//alert("tknCodRespuesta>"+tknCodRespuesta.substring(0,tknCodRespuesta.length-1)+">");
	//alert("numPreg>"+numPreg+">");	
	
	//alert("tknCodPreguntaAbierta>"+tknCodPreguntaAbierta.substring(0,tknCodPreguntaAbierta.length-1)+">");
	//alert("tknCodRespuestaAbierta>"+tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1)+">");
	//alert("numPregAbierta>"+numPregAbierta+">");	

}

function fc_Actualizar(){

	/*var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);*/	
}

function fc_mostrarDatos(posSel){
/*
	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtGrupo").value =  document.getElementById("hidGrupo"+posSel).value ;	
	document.getElementById("cboFormato").value = document.getElementById("hidFormato"+posSel).value;
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	document.getElementById("cboFormato").disabled = true;
	document.getElementById("cboFormato").className = "cajatexto_1";	
	*/
}

function fc_validaGrabar(){
	
	var tam = document.getElementById("txhTamanio").value;
	if(Number(tam)<1){
		alert(mstrNoSePuedeRealizarLaAccion);
		return 0;
	}
	
	 for ( var i = 0; i < Number(tam); i++)
	 {
		if( document.getElementById("hidFlgObligatorio"+i).value == "1" && fc_Trim(document.getElementById("opc"+i).value) == "" )
		{
			alert(mstrLleneTodoCampos);
			document.getElementById("opc"+i).focus();
			return 0;
		}
	 }
			
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcci�n");
}

function fc_Sel(pos){

	/*document.getElementById("txhPosSel").value = pos;	 
	codGrupo = document.getElementById("hidCodigo"+pos).value;
	
	fc_MostrarPreguntasStandar(codGrupo);*/
}

function fc_Eliminar(){
/*	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{	
			if(confirm(mstrSeguroEliminar1))
			{
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			}
		}
	else
		alert(mstrSeleccione);*/
}

/*
function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}*/

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/registroManualOpciones.html">

	<input type="hidden" name="operacion" id="operacion" value="GRABAR" />
	<input type="hidden" name="txhCodigo" id="txhCodigo" value="${model.txhCodigo}" />
		
	<input type="hidden" name="txhUsuario" id="txhUsuario" value="${model.txhUsuario}" />
	<input type="hidden" name="txhCodEncuestado" id="txhCodEncuestado"  />
	<!-- CANTIDAD DE PREGUNTAS -->
	
	<input type="hidden" name="tknCodPreguntaCerrada" id="tknCodPreguntaCerrada" />
	<input type="hidden" name="tknCodRespuestaCerrada" id="tknCodRespuestaCerrada" />
	<input type="hidden" name="tknNumPregCerrada" id="tknNumPregCerrada" />
	
	<input type="hidden" name="tknCodPreguntaAbierta" id="tknCodPreguntaAbierta" />
	<input type="hidden" name="tknCodRespuestaAbierta" id="tknCodRespuestaAbierta" />
	<input type="hidden" name="tknNumPregAbierta" id="tknNumPregAbierta" />

	<c:set var="varTamanio" value="0" />
	<!-- **************************************************************************************************** -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px;"  
				cellspacing="0" cellpadding="0" border="0" bordercolor="blue">
	<tr>
		<td width="95%" >
			<div style="overflow:auto;" style="width:865px;height:115px;" >
				<!-- <table class="tabla2" style="width:100%" cellSpacing="1" cellPadding="0" align="center" border="1" borderColor="white"
					background="${ctx}/images/Evaluaciones/back.jpg"> -->
				<table border="0" bordercolor="blue" cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" 
					id="tablaBandeja" bgcolor="white">
					<tr>
					<c:forEach var="objPreguntaCab" items="${model.lstResultado}" varStatus="nloop">
						<!-- VARIABLE QUE ALMACENA EL NOMBRE DE LA PREGUNTA -->
						<c:set var="varTamanio" value="${nloop.index+1}" />
						<input type="hidden" id="hidNomPregunta<c:out value="${nloop.index}" />" name="hidNomPregunta<c:out value="${nloop.index}" />" value="<c:out value="${objPreguntaCab.desPregunta}" />" />						
						<td style="width: 150px;" class="grilla"  style="cursor:pointer;" onclick="fc_MuestraPregunta('<c:out value="${nloop.index}" />')" >Preg.<b><c:out value="${nloop.index+1}" /></b></td>
					</c:forEach>
					</tr>
					
					<tr id="trElementos">
					 <c:forEach var="objPregunta" items="${model.lstResultado}" varStatus="loop">
						<td valign="top" style="width: 150px;" class="tablagrilla">
						
						<!--***************** DATOS DE LA PREGUNTA ***************** -->
						<input type="hidden" id="hidFlgObligatorio<c:out value="${loop.index}" />"  value="<c:out value="${objPregunta.flgObligatorio}" />"  />
						<input type="hidden" id="hidFlgCombo<c:out value="${loop.index}" />"  		value="<c:out value="${objPregunta.flgCombo}" />"  />
						<input type="hidden" id="hidFlgMultiple<c:out value="${loop.index}" />"  	value="<c:out value="${objPregunta.flgMultiple}" />"  /> 
						<input type="hidden" id="hidCodPregunta<c:out value="${loop.index}" />"  	value="<c:out value="${objPregunta.codPregunta}" />"  /> 
						
						<c:choose>								
							<c:when test="${objPregunta.flgCombo == '1'}" >
								<c:choose>
								 <c:when test="${objPregunta.flgMultiple == '1'}" >							 		
							 		<c:choose>
								 	<c:when test="${objPregunta.flgObligatorio == '1'}" >
								 		<select id="opc<c:out value="${loop.index}" />" class="combo_o" style="width:150px;" multiple="multiple" onkeypress="fc_Focus('${loop.index}');">								 		
								 			<c:forEach var="objOpcion" items="${objPregunta.lstOpciones}" varStatus="loop">
								 				<option value="<c:out value="${objOpcion.codOpcion}" />" ><c:out value="${objOpcion.nomCodOpcionABC}" />:<c:out value="${objOpcion.nomOpcion}" /></option>
								 			</c:forEach>
										</select>
								 	</c:when>
								 	<c:otherwise>
								 		<select id="opc<c:out value="${loop.index}" />" class="combo" style="width:150px;" multiple="multiple" onkeypress="fc_Focus('${loop.index}');">								 		
								 			<c:forEach var="objOpcion" items="${objPregunta.lstOpciones}" varStatus="loop">
								 				<option value="<c:out value="${objOpcion.codOpcion}" />" ><c:out value="${objOpcion.nomCodOpcionABC}" />:<c:out value="${objOpcion.nomOpcion}" /></option>
								 			</c:forEach>
										</select>								 	
								 	</c:otherwise>
								 	</c:choose>
								 	
																		
								 </c:when>	
								 <c:otherwise>
							 		<c:choose>
								 	<c:when test="${objPregunta.flgObligatorio == '1'}" >
								 		<select id="opc<c:out value="${loop.index}" />" class="combo_o" style="width:150px;" onkeypress="fc_Focus('${loop.index}');">
								 			<option value="">--Seleccione--</option>								 		
								 			<c:forEach var="objOpcion" items="${objPregunta.lstOpciones}" varStatus="loop">								 				
								 				<option value="<c:out value="${objOpcion.codOpcion}" />" ><c:out value="${objOpcion.nomCodOpcionABC}" />:<c:out value="${objOpcion.nomOpcion}" /></option>
								 			</c:forEach>								 		
										</select>
								 	</c:when>
								 	<c:otherwise>
								 		<select id="opc<c:out value="${loop.index}" />" class="cajatexto" style="width:150px;" onkeypress="fc_Focus('${loop.index}');">
								 			<option value="">--Ninguno--</option>								 		
								 			<c:forEach var="objOpcion" items="${objPregunta.lstOpciones}" varStatus="loop">								 				
								 				<option value="<c:out value="${objOpcion.codOpcion}" />" ><c:out value="${objOpcion.nomCodOpcionABC}" />:<c:out value="${objOpcion.nomOpcion}" /></option>
								 			</c:forEach>								 		
										</select>								 	
								 	</c:otherwise>
								 	</c:choose>												
								 </c:otherwise>								
								</c:choose>								
							</c:when>							
							<c:otherwise>	
								<c:choose>
								 	<c:when test="${objPregunta.flgObligatorio == '1'}" >								 		
								 		<textarea onblur="fc_ValidaTextoEspecialEncuestasOnblur('opc<c:out value="${loop.index}" />');"   onkeypress="validaTexto('opc<c:out value="${loop.index}" />','200');fc_Focus('${loop.index}');" id="opc<c:out value="${loop.index}" />"  class="cajatexto_o" style="width:150px;height:50px;" ></textarea>
								 	</c:when>	
								 	<c:otherwise>
								 		
										<textarea onblur="fc_ValidaTextoEspecialEncuestasOnblur('opc<c:out value="${loop.index}" />')"   onkeypress="validaTexto('opc<c:out value="${loop.index}" />','200');fc_Focus('${loop.index}');"  id="opc<c:out value="${loop.index}" />"  class="cajatexto" style="width:150px;height:50px;" ></textarea> 
									</c:otherwise>
								</c:choose>							
							</c:otherwise>
					 	</c:choose>
					</td>	
				   </c:forEach>
				</tr>
				</table>
			</div>
		</td>	
		<td align="right">
			<table style="width: 100%" border="0" bordercolor="red" cellpadding="0" cellspacing="4">
				<tr>
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonGrabar','','${ctx}/images/botones/grabar_2g.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar_1g.jpg" id="botonGrabar" style="cursor:pointer" onclick="javascript:fc_Grabar();"></a>						
					</td>
				</tr>
				<tr>
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','/SGA/images/botones/Limpiar2.jpg',1)">
						<img src="/SGA/images/botones/Limpiar1.jpg" onclick="fc_Limpiar()" style="cursor:pointer;" id="imgLimpiar"></a>												
					</td>
				</tr>
				<tr>
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
						<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="botonRegresar" onclick="javascript:fc_Regresar();" style="cursor:pointer;"></a>						
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

	<!--CANTIDAD DE PREGUNTAS-->
	<input type="hidden" name="txhTamanio" id="txhTamanio" value="<c:out value="${varTamanio}" />" />
	<!-- **************************************************************************************************** -->
</form:form>
</body>


