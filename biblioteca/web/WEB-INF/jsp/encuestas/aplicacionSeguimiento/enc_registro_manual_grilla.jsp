<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
}

function fc_refrescar(){
	document.getElementById("frmMain").submit();
}

function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	/*document.getElementById("txtGrupo").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("cboFormato").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	document.getElementById("txhCodigoSel").value = "";
	document.getElementById("cboFormato").disabled = false;
	document.getElementById("cboFormato").className = "combo_o";*/
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){
				
			document.getElementById("operacion").value="irRegistrar";									
			document.getElementById("frmMain").submit();
		}
	}
}

function fc_Actualizar(posSel){

	//var posSel = document.getElementById("txhPosSel").value;
	
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_mostrarDatos(posSel){

	var tam = document.getElementById("txhTamanio").value;
	
	//alert("<"+tam+">");
		
	for ( var i = 0; i < Number(tam); i++)
	{
			//OBJETO QUE OBTIENE LOS VALUES DE LAS OPCIONES SELECCIONADAS
			//CAMBIAR POR CODIGO DE ALTERNATIVAS
			
			//obj = document.getElementById("hidCadNomAlternativa"+i);
			obj = document.getElementById("hidRpta"+posSel+""+i);			
			codEncuestado = document.getElementById("hidCodEncuestado"+posSel+""+i).value;
			
			//alert("hidRpta"+i+"<"+obj.value+">");			
			
			if( document.getElementById("hidTipoPregunta"+posSel+""+i).value == "0002" )
			{	//COMBO
				
				if( document.getElementById("hidIndUnica"+posSel+""+i).value == "1" )
				{	//SIMPLE					
					//PROCESA SIMPLE 1					
					parent.fc_Procesa(i,obj.value,1,codEncuestado);
	
				}else
				{	//MULTIPLE					
					//PROCESA COMPUESTO					
					parent.fc_Procesa(i,obj.value,0,codEncuestado);
					
				}
				
			}else
			{	//TXT
					parent.fc_Procesa(i,obj.value,1,codEncuestado);
	
			}		
				
	}
	
	
}

function fc_validaGrabar(){

/*	if(fc_Trim(document.getElementById("txtGrupo").value)=="")
	{	alert(mstrIngrGrupo);
		document.getElementById("txtGrupo").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboFormato").value)==""){
		alert(mstrSeleccioneFormato);
		document.getElementById("cboFormato").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngrPeso);
		document.getElementById("txtPeso").focus();
		return 0;
	}*/
		
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function fc_Sel(pos){

	/*document.getElementById("txhPosSel").value = pos;	 
	codGrupo = document.getElementById("hidCodigo"+pos).value;
	
	fc_MostrarPreguntasStandar(codGrupo);*/
}

function fc_Eliminar(codEncuestado){

			if(confirm(mstrSeguroEliminar1))
			{
				document.getElementById("txhCodigoCorrelativo").value = codEncuestado;				
		 		document.getElementById("txhOperacion").value="ELIMINAR";
				document.getElementById("frmMain").submit();
			}
}

/*
function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}*/

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/registroManualGrilla.html">
	
	
	<input type="hidden" name="txhOperacion" id="txhOperacion" />
	
	<input type="hidden" name="txhTamanio" id="txhTamanio" value="${model.txhTamanio}" />
	
	<!-- CODIGO DE LA ENCUESTA -->
	<input type="hidden" name="txhCodigo" id="txhCodigo" value="${model.txhCodigo}" />
	
	<!-- USUARIO QUE REALIZA LA ACCION -->
	<input type="hidden" name="txhUsuario" id="txhUsuario" value="${model.txhUsuario}" />
	
	<!-- ID DE REGISTRO A ELIMINAR -->
	<input type="hidden" name="txhCodigoCorrelativo" id="txhCodigoCorrelativo" />
	

	<!-- **************************************************************************************************** -->	
	<div style="overflow:auto;" style="width:100%;height:245px;" class="tabla">
	<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">	
		<tr>						
			<td class="grilla" style="width:5%;">Nro.</td>
			<td class="grilla" style="width:5%;">Mod.</td>
			<td class="grilla" style="width:5%;">&nbsp;Eli.</td>
			<% String tam = (String)request.getAttribute("txhTamanio");				
			   if(tam==null) tam = "0";				
			   int max = Integer.parseInt(tam);
			   for(int i=0;i<max;i++){%>			
				<td style="width:<%=85/max%>%;"  class="grilla" >Preg.<b><%=i+1%></b></td>	
				<%}%>		
			
		</tr>		
			<c:forEach var="objFila" items="${model.lstResultado}" varStatus="loop">
			<tr>
				<td  class="tablagrilla" style="width:5%;" align="center" valign="top"><c:out value="${loop.index + 1}" /></td>
				<td  class="tablagrilla" style="width:5%;" align="center" valign="top"><img onclick="fc_Actualizar('<c:out value="${loop.index}" />')" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" style="cursor:hand"></td>
				<td  class="tablagrilla" style="width:5%;" align="center" valign="top"><img onclick="fc_Eliminar('<c:out value="${objFila.codEncuestado}" />')" src="${ctx}/images/iconos/cerrar1.jpg" alt="Eliminar" style="cursor:hand"></td>
				
				<c:forEach var="objColumna" items="${objFila.lstPreguntas}" varStatus="loop2">	
			 	  <td  class="tablagrilla" valign="top" style="width:<%=85/max%>%;" >
					<input type="hidden" id="hidIndObligatorio<c:out value="${loop.index}" /><c:out value="${loop2.index}" />"  value="<c:out value="${objColumna.indObligatorio}" />"  />					
					<input type="hidden" id="hidTipoPregunta<c:out value="${loop.index}" /><c:out value="${loop2.index}" />"  		value="<c:out value="${objColumna.tipoPregunta}" />"  />
					<input type="hidden" id="hidIndUnica<c:out value="${loop.index}" /><c:out value="${loop2.index}" />"  	value="<c:out value="${objColumna.indUnica}" />"  /> 
					<input type="hidden" id="hidIdPregunta<c:out value="${loop.index}" /><c:out value="${loop2.index}" />"  	value="<c:out value="${objColumna.idPregunta}" />"  />
					<input type="hidden" id="hidCadNomAlternativa<c:out value="${loop.index}" /><c:out value="${loop2.index}" />"  	value="<c:out value="${objColumna.cadNomAlternativa}" />"  />
					<input type="hidden" id="hidCodEncuestado<c:out value="${loop.index}" /><c:out value="${loop2.index}" />"  	value="<c:out value="${objColumna.codEncuestado}" />"  />
					<input type="hidden" id="hidRpta<c:out value="${loop.index}" /><c:out value="${loop2.index}" />"  	value="<c:out value="${objColumna.rpta}" />"  />
					
					<!-- PINTAMOS LA INFORMACION -->					
					
					
					<c:if test="${objColumna.tipoPregunta=='0001'}">
						<textarea   class="cajatexto_1" readonly="readonly" style="width:150px;height:43px;"><c:out value="${objColumna.rpta}" /></textarea>					
					</c:if>
					<c:if test="${objColumna.tipoPregunta=='0002'}">
						&nbsp;<c:out value="${objColumna.cadNomAlternativa}" />
					</c:if>
									
			 	  </td>
			 	</c:forEach>			 	
			 </tr>
			</c:forEach>
	</table>
	</div>
	<!-- **************************************************************************************************** -->

	<!-- **************************************************************************************************** -->
</form:form>
</body>


