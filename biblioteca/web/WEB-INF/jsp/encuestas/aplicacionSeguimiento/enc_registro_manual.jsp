<%@page import='com.tecsup.SGA.bean.DatosCabeceraBean'%>
<%@ include file="/taglibs.jsp"%>


<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
}

function fc_RegresarMenu(){
	document.location.href = "${ctx}/menuEncuesta.html"
}

function fc_Procesa(pos,cadCodRespuesta,flgSimple,codEncuestado){	
	document.getElementById("iframeOpciones").contentWindow.fc_Procesa(pos,cadCodRespuesta,flgSimple,codEncuestado);	
}

function fc_MuestraPregunta(texto){	
	document.getElementById("txtNomPregunta").value = texto;
}

function fc_Limpiar(){
	//document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtNomPregunta").value = "";
	/*document.getElementById("txtGrupo").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("cboFormato").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	document.getElementById("txhCodigoSel").value = "";
	document.getElementById("cboFormato").disabled = false;
	document.getElementById("cboFormato").className = "combo_o";*/
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){
				
			document.getElementById("operacion").value="irRegistrar";									
			document.getElementById("frmMain").submit();
		}	
	}
}

function fc_Actualizar(){

	/*var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);*/	
}

function fc_mostrarDatos(posSel){
/*
	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtGrupo").value =  document.getElementById("hidGrupo"+posSel).value ;	
	document.getElementById("cboFormato").value = document.getElementById("hidFormato"+posSel).value;
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	document.getElementById("cboFormato").disabled = true;
	document.getElementById("cboFormato").className = "cajatexto_1";	
	*/
}

function fc_validaGrabar(){

/*	if(fc_Trim(document.getElementById("txtGrupo").value)=="")
	{	alert(mstrIngrGrupo);
		document.getElementById("txtGrupo").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboFormato").value)==""){
		alert(mstrSeleccioneFormato);
		document.getElementById("cboFormato").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngrPeso);
		document.getElementById("txtPeso").focus();
		return 0;
	}*/
		
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function fc_Sel(pos){

	/*document.getElementById("txhPosSel").value = pos;	 
	codGrupo = document.getElementById("hidCodigo"+pos).value;
	
	fc_MostrarPreguntasStandar(codGrupo);*/
}

function fc_Eliminar(){
/*	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{	
			if(confirm(mstrSeguroEliminar1))
			{
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			}
		}
	else
		alert(mstrSeleccione);*/
}

/*
function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}*/

function fc_Regresar(){
    window.location.href = "${ctx}/encuestas/bandejaAplicacionSeguimiento.html?txhCodUsuario="+
	document.getElementById("txhUsuario").value +
	"&txhParCodTipoAplicacion=" + document.getElementById("txhParCodTipoAplicacion").value +
	"&txhParCodTipoEncuesta=" + document.getElementById("txhParCodTipoEncuesta").value +
	"&txhParCodTipoServicio=" + document.getElementById("txhParCodTipoServicio").value +
	"&txhParCodTipoEstado=" + document.getElementById("txhParCodTipoEstado").value +
	"&txhParCodEncuesta=" + document.getElementById("txhParCodEncuesta").value +
	"&txhParResponsable=" + document.getElementById("txhParResponsable").value +
	"&txhParCodResponsable=" + document.getElementById("txhParCodResponsable").value +
	"&txhParBandera=" + document.getElementById("txhParBandera").value;
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/registroManual.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="txhUsuario" id="txhUsuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />

	<form:hidden path="parCodTipoAplicacion" id="txhParCodTipoAplicacion"/>
	<form:hidden path="parCodTipoEncuesta" id="txhParCodTipoEncuesta"/>
	<form:hidden path="parCodTipoServicio" id="txhParCodTipoServicio"/>
	<form:hidden path="parCodTipoEstado" id="txhParCodTipoEstado"/>
	<form:hidden path="parCodEncuesta" id="txhParCodEncuesta"/>
	<form:hidden path="parResponsable" id="txhParResponsable"/>
	<form:hidden path="parBandera" id="txhParBandera"/>
	<form:hidden path="parCodResponsable" id="txhParCodResponsable"/>	
	<!-- **************************************************************************************************** -->
	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_RegresarMenu();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Registro Manual de Encuestas					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>	
	<!-- XXXXXXXX -->
	<%DatosCabeceraBean bean= (DatosCabeceraBean) request.getAttribute("OBJ_CABECERA"); %>
	<%if(bean!=null ){ %>
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="1" cellpadding="1" border="0" bordercolor="red">
		<TR>
			<TD style="width: 15%;"><b>&nbsp;Tipo Aplicación :</b></TD>
			<TD style="width: 25%;" align="left">&nbsp;
				<input type="text" value="<%=bean.getNomTipoAplicacion()%>" size="30" class="cajatexto_1" readonly="readonly">
			</TD>
			<TD style="width: 10%;"><b>&nbsp;Tipo Encuesta :</b></TD>
			<TD style="width: 20%;">&nbsp;
				<input type="text" value="<%=bean.getNomTipoEncuesta()%>" size="30" class="cajatexto_1" readonly="readonly">
			</TD>
			<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0002".equalsIgnoreCase(bean.getCodTipoAplicacion()) ){%>
			<TD style="width: 10%;"><b>&nbsp;Aplicado a :</b></TD>
			<TD style="width: 20%;" align="left">&nbsp;<input type="text" value="<%=bean.getNomServicio()%>" size="30" class="cajatexto_1" readonly="readonly"></TD>			
			<%}else{%>
			<TD style="width: 10%;">&nbsp;</TD>
			<TD style="width: 20%;">&nbsp;</TD>
			<%}%>
		</TR>
		<TR>
			<TD style="width: 15%;"><b>&nbsp;Encuesta :</b></TD>
			<TD style="width: 25%;" align="left">&nbsp;
				<input type="text" value="<%=bean.getNomEncuesta()%>" size="30" class="cajatexto_1" readonly="readonly">
			</TD>
			<TD style="width: 10%;"><b>&nbsp;Duración :</b></TD>
			<TD style="width: 20%;" align="left">&nbsp;
				<input type="text" value="<%=bean.getDuracion()%>" size="5" class="cajatexto_1" readonly="readonly">
			&nbsp;Min</TD>
			
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0001".equalsIgnoreCase(bean.getCodTipoAplicacion()) && bean.getNomProfesor()!=null && !bean.getNomProfesor().equals("")){%>					
			<TD style="width: 10%;"><b>&nbsp;Aplicado al profesor:</b></TD>
			<TD style="width: 20%;" align="left">&nbsp;
				<input type="text" value="<%=bean.getNomProfesor()%>" size="30" class="cajatexto_1" readonly="readonly"></TD>					
		<%}else{%>		
		<TD style="width: 10%;">&nbsp;</TD>
			<TD style="width: 20%;">&nbsp;</TD>	
		<%}%>
		</TR>	
	</table>	
	<%}%>
	<!-- PREGUNTA -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="1" cellpadding="1" border="0" bordercolor="red">
		<tr>
			<td style="cursor:pointer;width: 15%;">&nbsp;Pregunta:
			</td>
			<td align="left">&nbsp;
			<textarea cols="100" rows="2" id="txtNomPregunta" name="txtNomPregunta" class="cajatexto_1" readonly="readonly" ></textarea>
			</td>
		</tr>
	</table>		

	<table class="tabla" border="0" height="125px;" bordercolor="red" cellpadding="0" cellspacing="0" style="width: 100%">
		<tr>
			<td class="tabla" valign="top">
				<iframe id="iframeOpciones" frameborder="0" height="125px;" width="100%"  src="/SGA/encuestas/registroManualOpciones.html?txhUsuario=${control.txhUsuario}&flagObligatorio=${control.flagObligatorio}&txhCodigo=${control.txhCodigo}&flagMultiple=${control.flagMultiple}">
				</iframe>
			</td>		
		</tr>
	</table>
	
	<table class="tabla" border="0" bordercolor="red" cellpadding="0" cellspacing="0" style="width: 100%" >
		<tr>
			<td class="tabla">
				<!-- div style="overflow:auto;" style="width:990px;height:200px;margin-left:8px" class="tabla"-->
				<iframe  id="iframeGrilla" scrolling="no" frameborder="0" style="width:97%;height:245px;margin-left:9px" src="/SGA/encuestas/registroManualGrilla.html?txhCodigo=${control.txhCodigo}&txhUsuario=${control.txhUsuario}">
				</iframe>
				<!--div-->
			</td>
		</tr>
	</table>

	<!-- **************************************************************************************************** -->
</form:form>
</body>


