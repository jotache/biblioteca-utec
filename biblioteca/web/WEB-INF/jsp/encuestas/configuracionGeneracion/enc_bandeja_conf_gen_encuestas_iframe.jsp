<%@ include file="/taglibs.jsp"%>
<%@ page import='java.util.List'%>
<style>
.tablagrilla02
{
    /*BACKGROUND-COLOR: #CEE3F6;*/
    BACKGROUND-COLOR: #C8E8F0;
	FONT-SIZE: 8pt;
    /*TEXT-TRANSFORM: capitalize;*/
    WIDTH: 100%;
    FONT-FAMILY: Arial;
    color: #606060;
    
}
</style>
<head>
<script language=javascript>
	function onLoad(){
		fc_IntercalarColorColumnas();
	}	
	function fc_IntercalarColorColumnas(){
		//tamTabla=document.getElementById("txhTamListaBandeja").value;
		tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');		
		numeroHijoTabla = hijosTr.length;
		//alert(numeroHijoTabla)
		
		if(numeroHijoTabla > 2){
			for(var i=1;i<numeroHijoTabla;i++){			
				trHijo=document.getElementById("tablaBandeja").rows[i];
				if(i%2){				
					for(var j=0;j<8;j++){
						trHijo.cells[j].className="tablagrilla02";					
					}					
				}
			}
		}		
	}

	function fc_SeleccionarRegistro(srtInd, codEncuesta, codEstado){
	   document.getElementById("txhCodSelec").value = codEncuesta;
	   document.getElementById("txhValTipoEncuesta").value = codEstado;
	   parent.fc_seleccionarRegistro(srtInd, codEncuesta, codEstado);
	}

	function fc_VerEncuesta(srtCodEncuesta, srtCodTipoEncuesta){
	 parent.fc_VerEncuesta(srtCodEncuesta, srtCodTipoEncuesta);
	}
</script>
</head>


<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/bandeja_conf_gen_encuestas_iframe.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="consteTipoPrograma" id="txhConsteTipoPrograma"/>
<form:hidden path="consteTipoServicios" id="txhConsteTipoServicios"/>
<form:hidden path="consteEstadoPendiente" id="txhConsteEstadoPendiente"/>
<form:hidden path="consteEstadoAplicacion" id="txhConsteEstadoAplicacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="consteAplicacionPrograma" id="txhConsteAplicacionPrograma"/>
<form:hidden path="consteAplicacionServicio" id="txhConsteAplicacionServicio"/>
<form:hidden path="consteEncuestaGenerada" id="txhConsteEncuestaGenerada"/>
<form:hidden path="consteEncuestaPendiente" id="txhConsteEncuestaPendiente"/>
<form:hidden path="consteEncuestaAplicada" id="txhConsteEncuestaAplicada"/>
<form:hidden path="valTipoEncuesta" id="txhValTipoEncuesta"/>
<form:hidden path="parCodTipoAplicacion" id="txhParCodTipoAplicacion"/>
<form:hidden path="parCodTipoEncuesta" id="txhParCodTipoEncuesta"/>
<form:hidden path="parCodTipoEstado" id="txhParCodTipoEstado"/>
<form:hidden path="parCodTipoServicio" id="txhParCodTipoServicio"/>
<form:hidden path="parBandera" id="txhParBandera"/>
<form:hidden path="tamListaBandeja" id="txhTamListaBandeja"/>

<form:hidden path="nomUsuario" id="txhNomUsuario"/>
<form:hidden path="perfilUsuario" id="txhPerfilUsuario"/>
<form:hidden path="codResponsable" id="txhCodResponsable"/>

	<!-- BANDEJA -->			
				<display:table name="sessionScope.listaBandejaConfiguracion" pagesize="14" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.EncuestaDecorator"  requestURI="" 
				style="border: 1px solid #048BBA;width:100%;" id="tablaBandeja">					
					<display:column property="rbtSelEncuesta" title="Sel"  
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:4%"/>
						
					<display:column property="nroEncuesta" title="Nro. Encuesta" headerClass="grilla" 
					class="tablagrilla" style="text-align:center;width:29%"/>
					
					<display:column property="nombreEncuesta" title="Nombre" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:29%"/>
					
					<display:column property="nomTipoAplicacion" title="Tipo Aplicación" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					
					<display:column property="nomTipoEncuesta" title="Tipo Encuesta" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					
					<display:column property="nomEstado" title="Estado" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					
					<display:column property="indEncuestaManual" title="Ind.<br>Manual" headerClass="grilla" 
					class="tablagrilla" style="text-align:center;width:4%"/>
										
					<display:column property="visualizarEncuesta" title="Ver" headerClass="grilla" 
					class="tablagrilla" style="text-align:center;width:4%"/>
					
								
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='8' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
					<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
					<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
				</display:table>			
			
</form:form>
</body>
