<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var global=0;
	function onLoad(){
	
	  if(document.getElementById("txhMsg").value == "OK")
	  {    alert(mstrSeGraboConExito);
      	   window.close();
      }
       else{   if(document.getElementById("txhMsg").value == "ERROR")
               alert(mstrProblemaGrabar);
               else if(document.getElementById("txhMsg").value == "ERROR_MODIFICAR")
                    alert(mstrNoModificarEncuesta);
       }
     
     document.getElementById("txhMsg").value="";
	}
	
	function fc_Buscar()
	{ document.getElementById("txhOperacion").value = "BUSCAR";
	  document.getElementById("frmMain").submit();
	}
	
	function fc_Limpiar(){
	 document.getElementById("dscTipoServicio").value="";
	
	}
		
	function fc_Modificar(){
	if(document.getElementById("txhCodSelec").value!=""){
		srtcodigo=document.getElementById("txhCodSelec").value;
		srtdescripcion=document.getElementById("txhDescripSelec").value;
				
		Fc_Popup("${ctx}/encuentas/tipo_formato_agregar.html?txhCodUsuario="+
		document.getElementById("txhCodUsuario").value+ "&txhCodSelec=" + srtcodigo
		+ "&txhDescripSelec=" + srtdescripcion ,450,160); 	
	
	 }
	  else alert(mstrSeleccione); 
	}
	
		
	function fc_SeleccionarRegistro(srtId, strCodNIvel1, strCodSeccionAsignado, srtRegRelacionados)
		{  // alert("Cadena: "+document.getElementById("txhCadCodDescripSelec").value);
			/*if(strCodSeccionAsignado!="")			
			{	*/
			//alert("Global: "+global);
			    if(global==0)
			     { document.getElementById("txhCadCodSelec").value=document.getElementById("txhCadCodDescripSelec").value;
			       global=global+1;
			     }
			    
			    //alert("CadCod Antes :"+document.getElementById("txhCadCodSelec").value);
		       	        
				strCodSel = document.getElementById("txhCadCodSelec").value;
				flag=false;
				if(strCodSel!='')
				{
					ArrCodSel = strCodSel.split("|");
					strCodSel = "";
				
					for (i=0;i<=ArrCodSel.length-2;i++)
					{
						if (ArrCodSel[i] == strCodNIvel1){ flag = true ; }
						else strCodSel = strCodSel + ArrCodSel[i]+'|';
					}
				}
				if(!flag) strCodSel = strCodSel + strCodNIvel1 + '|';
				
				document.getElementById("txhCadCodSelec").value = strCodSel;
				
				/***************************** Cadena de Descripcion ******************************/
				
		        
		        //alert("CadCod Despues :"+document.getElementById("txhCadCodSelec").value);
	        /*}
	        else{ //document.getElementById("text"+srtId).disabled=true;
	              alert("No se puede hacer nada");
	        }*/
	         MadFrog = document.getElementById("txhCadCodSelec").value.split("|");
	         long=MadFrog.length-1;
	         document.getElementById("txhNroTotalRegistros").value=long.toString();
	         //alert("Size: "+document.getElementById("txhNroTotalRegistros").value);
		}
	
	function fc_Aceptar(){
	document.getElementById("txhOperacion").value = "ACEPTAR";
	document.getElementById("frmMain").submit();
	}
	
	//window.opener.document.getElementById("txtNroIngreso").value=document.getElementById("txhCodMaterial").value;	
</script>
</head>
<body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/datosGenTipoFormato.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codUsuario" id="txhCodUsuario" />
<form:hidden path="cadCodSelec" id="txhCadCodSelec" />
<form:hidden path="cadCodDescripSelec" id="txhCadCodDescripSelec" />
<form:hidden path="codFormatoEncuesta" id="txhCodFormatoEncuesta" /> 
<form:hidden path="nroTotalRegistros" id="txhNroTotalRegistros" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="285px" class="opc_combo"><font style="">Consulta Tipo de Formato</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
			
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1"
	         style="width:96%; margin-left:4px; display: none;" class="tabla" height="50px" bordercolor="red">
			<tr>
				<td class="" width="20%">Tipo de Formato:</td>
				<td width="20%"><form:input path="dscTipoFormato" id="dscTipoFormato" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Tipo de Formato');" 
					cssClass="cajatexto" size="50"/></td>
				<td align="right">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;			
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
				</td>	
				
			</tr>
			
		</table>
		
	<table cellpadding="0" cellspacing="0" ID="Table1" width="98%" border="0"  class="" 
	        style="margin-left:10px; margin-top: 10px">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 190px;">
				 	<display:table name="sessionScope.listaBandejaFormato" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.EncuestaDecorator"  pagesize="5" requestURI=""
						style="border: 1px solid #048BBA;width:96%;">
					    <display:column property="rbtSelTipoFormatoEncuesta" title="Sel."  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:7%"/>
						<display:column property="nomFormato" title="Formato" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:93%"/>
						
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
					  </display:table>
					</div>
				</td>
				
			</tr>
		</table>
		
		<table width="100%" style="margin-top: 10px">
			<tr>
				<td align="center">
				    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imgGrabar01" style="cursor:pointer;" onclick="javascript:fc_Aceptar();"></a>
								
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar01','','${ctx}/images/botones/cancelar2.jpg',1)">
						<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar01" style="cursor:pointer;" onclick="window.close();"></a>
					
					
				</td>
			</tr>
		</table>
		
</form:form>

</body>
</html>