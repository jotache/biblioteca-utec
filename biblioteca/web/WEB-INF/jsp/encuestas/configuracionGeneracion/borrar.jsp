<%@ include file="/taglibs.jsp"%>

<head>
<link rel="stylesheet" type="text/css" href="../../../../styles/estilo_eva.css">
<script language="javascript">
	function onLoad(){		
		fc_MarcarIndicador();
		fc_MostrarTipoServicio();		
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OK_GRABAR" ){		
			alert(mstrGrabar);
		}		
		else
		if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);
		}
		else
		if ( objMsg.value != 'null' &&  objMsg.value != ""){
			alert(document.getElementById("txhDscMensaje").value)
		}
		document.getElementById("txhMsg").value = "";
		document.getElementById("txhOperacion").value = "";	
	}
	function fc_MarcarIndicador(){		
		if(document.getElementById("txhIndicadorManual").value=="1"){
			document.getElementById("chkTipoEncuesta").checked=true;
			document.getElementById("chkTipoEncuesta").disabled=true;
		}
		else
		if(document.getElementById("txhIndicadorManual").value=="0"){
			document.getElementById("chkTipoEncuesta").checked=false;
			document.getElementById("chkTipoEncuesta").disabled=true;			
			fc_VerificarPerfil02();
		}
	}
	function fc_MostrarTipoServicio(){
		if(document.getElementById("txhCodTipoAplicacion").value=="0002"){
			tdTipoServicio01.style.display="";
			tdTipoServicio02.style.display="";
		}
	}
	function fc_VerificarPerfil02(){
		var perfilEncuesta=document.getElementById("txhCodPerfilEncuesta").value;
		var codTipoAplicacion=document.getElementById("txhCodTipoAplicacion").value;
		//CASO PROGRAMA
		if(codTipoAplicacion=="0001" && perfilEncuesta!=""){
			perfil=perfilEncuesta.split("|");			
			tablaTab.style.display="";
			//COMO TIENE COMO MINIMO UN PERFIL ASIGNADO 
			//SE MUESTRA ORIENTADO
			fc_CambiaBandeja(document.getElementById("txhConstOrientado").value);			
			fc_OcultarPerfiles(perfil[0]);			
		
		}
		else
		//CASO SERVICIOS
		if(codTipoAplicacion=="0002" && perfilEncuesta!=""){
			tablaTab.style.display="";
			//COMO TIENE COMO MINIMO UN PERFIL ASIGNADO 
			//SE MUESTRA ORIENTADO
			fc_CambiaBandeja(document.getElementById("txhConstOrientado").value);
		}		
	}
	function fc_VerificarPerfil(){
		var perfilEncuesta=document.getElementById("txhCodPerfilEncuesta").value;
		var codTipoAplicacion=document.getElementById("txhCodTipoAplicacion").value;
		if(codTipoAplicacion=="0001" && perfilEncuesta!=""){
			perfil=perfilEncuesta.split("|");			
			tablaTab.style.display="";
			fc_CambiaBandeja(perfil[0]);						
			fc_OcultarPerfiles(perfil[0]);			
		
		}
		else{
			tablaTab.style.display="";
			//COMO NO EXISTE UN PERFIL ASIGNADO A LA ENCUESTA 
			//SE MUESTRA EL PRIMER PERFIL PFR
			fc_CambiaBandeja(document.getElementById("txhConstPerfilPFC").value);			
		}		
	}
	function fc_AgregarPerfiles(){
		if(document.getElementById("txhIndicadorManual").value!=""){
			if(document.getElementById("chkTipoEncuesta").checked!=true){
				fc_VerificarPerfil();							
			}
			else{
				alert("No puede ingresar perfiles.");
			}
		}
		else{
			alert("Debe Grabar los datos de la encuesta.");
		}
	}	
	function fc_Grabar(){
		fc_ObtenerIndicador();
		if(document.getElementById("txtResponsable").value==""){
			alert("Debe Asignar un Responsable.");
			return;
		}
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value="GRABAR";		
			document.getElementById("frmMain").submit();
		}		
	}
	function fc_ObtenerIndicador(){
		if(document.getElementById("chkTipoEncuesta").checked==true){
			document.getElementById("txhIndicadorManual").value="1";
		}
		else{
			document.getElementById("txhIndicadorManual").value="0";
		}		
	}
	function fc_RegresarBandeja(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		/*alert("CodTipoAplicacion="+document.getElementById("txhParCodTipoAplicacion").value +
		"CodTipoEncuesta="  + document.getElementById("txhParCodTipoEncuesta").value +
		"CodTipoServicio="  + document.getElementById("txhParCodTipoServicio").value +
		"CodTipoEstado="    + document.getElementById("txhParCodTipoEstado").value +
		"ParBandera="       + document.getElementById("txhParBandera").value);*/
		window.location.href = "${ctx}/encuestas/bandeja_conf_gen_encuestas.html?txhCodUsuario="+codUsuario +
		"&txhParCodTipoAplicacion="+ document.getElementById("txhParCodTipoAplicacion").value +
		"&txhParCodTipoEncuesta="  + document.getElementById("txhParCodTipoEncuesta").value +
		"&txhParCodTipoServicio="  + document.getElementById("txhParCodTipoServicio").value +
		"&txhParCodTipoEstado="    + document.getElementById("txhParCodTipoEstado").value +
		"&txhParBandera="          + document.getElementById("txhParBandera").value ;
	}	
	function fc_BuscarEmpleado(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		Fc_Popup("${ctx}/encuestas/busqueda_empleados.html?txhCodUsuario="+codUsuario,660,460);
	}
	function fc_Aplicar(){
		/*if(codEstado==document.getElementById("txhEstadoGenerada").value){
			codUsuario=document.getElementById("txhCodUsuario").value;
			codEncuesta=document.getElementById("txhCodEncuesta").value;
			Fc_Popup("${ctx}/encuestas/aplicarProgramacionEncuesta.html?txhCodEncuesta="+codEncuesta+"&txhCodUsuario="+codUsuario,480,160);			
		}
		else{
			alert("Solo se pueden aplicar encuestas generadas.");
		}*/
		codEstado=document.getElementById("txhCodEstado").value;
		//alert(codEstado);
		if(codEstado==document.getElementById("txhEstadoEnAplicacion").value){
			alert("La Encuesta ya se encuesta aplicada y programada.");
			return;
		}
		codUsuario=document.getElementById("txhCodUsuario").value;
		codEncuesta=document.getElementById("txhCodEncuesta").value;
		Fc_Popup("${ctx}/encuestas/aplicarProgramacionEncuesta.html?txhCodEncuesta="+codEncuesta+"&txhCodUsuario="+codUsuario,500,260);
	}
	function fc_TipoEncuesta(){
		if(document.getElementById("chkTipoEncuesta").checked==true){
			divRegistrarPerfiles.style.display="none";
		}
		else{
			divRegistrarPerfiles.style.display="";			
		}		
	}
	function fc_Regresar(){
		document.location.href = "${ctx}/menuEncuesta.html"
	}
	function fc_ActualizarResponsable(codResponsable, nomResponsable){		
		document.getElementById("txhCodResponsable").value=codResponsable;
		document.getElementById("txtResponsable").value=nomResponsable;
	}
	function fc_ActualizarTotalEncuestado(totalEncuestados, dscEncuestados){
		document.getElementById("txtTotalEncuestado").value=totalEncuestados
		document.getElementById("tdEncuestados").innerText=dscEncuestados;
		
	}
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/perfilProgramacion.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="dscMensaje" id="txhDscMensaje" />	
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />	
	<!-- para la BD -->
	<form:hidden path="codEncuesta" id="txhCodEncuesta" />
	<form:hidden path="codPerfilEncuesta" id="txhCodPerfilEncuesta" />	
	<!-- para grabar la cabecera -->	
	<form:hidden path="codTipoAplicacion" id="txhCodTipoAplicacion" />
	<form:hidden path="codResponsable" id="txhCodResponsable" />
	<form:hidden path="indicadorManual" id="txhIndicadorManual" />
	<form:hidden path="dscEncuestados" id="txhDscEncuestados" />
	<form:hidden path="codEstado" id="txhCodEstado" />
	<!-- CONSTANTES -->
	<form:hidden path="estadoGenerada" id="txhEstadoGenerada" />
	<form:hidden path="estadoEnAplicacion" id="txhEstadoEnAplicacion" />
	<form:hidden path="constPerfilPFC" id="txhConstPerfilPFC" />
	<form:hidden path="constPerfilPCC" id="txhConstPerfilPCC" />
	<form:hidden path="constPerfilEGRESADO" id="txhConstPerfilEGRESADO" />
	<form:hidden path="constPerfilPERSONAL" id="txhConstPerfilPERSONAL" />
	<form:hidden path="constOrientado" id="txhConstOrientado" />
	<!-- parametros a devolver al regresar -->
	<form:hidden path="parCodTipoAplicacion" id="txhParCodTipoAplicacion" />
	<form:hidden path="parCodTipoEncuesta" id="txhParCodTipoEncuesta" />
	<form:hidden path="parCodTipoServicio" id="txhParCodTipoServicio" />
	<form:hidden path="parCodTipoEstado" id="txhParCodTipoEstado" />
	<form:hidden path="parBandera" id="txhParBandera" />
	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">		 		
		 			Configuraci�n de Perfil y Programaci�n de Aplicaci�n		 								 			 		
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- cabecera -->
	<table cellpadding="0" cellspacing="0" style="margin-top: 4px; margin-left: 8px">
		<tr>
		<!--<img src="${ctx}/images/Logistica/izq1.jpg" height="34px" id="tdProducto01">-->
			<td>			
			<img src="../../../../images/Logistica/izq1.jpg" width="12" height="34" id="tdProducto01">
			</td>			
			<td bgcolor=#048BBA class="opc_combo" height="34px" style="font-size: 12px;">
				B&Uacute;SQUEDA DE ALUMNO </td>
			<td><img src="../../../../images/Logistica/der1.jpg" width="12" height="34" id="tdProducto03"></td>
		</tr>
	</table>
	<table border="0" bordercolor="blue" cellpadding="0" cellspacing="0" width="98%">
		<tr>
			<td width="95%">	
				<table class="tabla" style="width:98%;margin-left:9px" background="../../../../images/biblioteca/fondoinf.jpg" cellspacing="6" cellpadding="0" border="0" >
					<tr>
						<td width="15%">Cod. Carnet  :</td>
						<td width="15%">			
							<form:input path="tipoAplicacion" id="txtTipoAplicacion" cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>								
						</td>
						<td width="15%">Nombre :</td>
						<td width="25%">						
							<form:input path="tipoEncuesta" id="txtTipoEncuesta" cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>							
						</td>
					</tr>
					<tr>
						<td width="15%">Apellido Paterno :</td>
						<td width="15%">			
							<form:input path="codigo" id="txtCodigo" cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>								
						</td>
						<td width="15%">Apellido Materno:</td>
						<td width="25%">										
							<form:input path="nombre" id="txtNombre" cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>							
						</td>
					</tr>
					<tr>
						<td width="15%">&nbsp;</td>
						<td colspan="3">			
							<form:input path="responsable" id="txtResponsable"													 
								cssClass="cajatexto_1" cssStyle="width: 91%" readonly="true"/>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="../../../../images/botones/buscar1.gif" alt="Buscar" width="138" height="22" align="absmiddle" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_BuscarEmpleado();">
							</a>&nbsp;
						</td>
					</tr>		
			  </table>
			</td>		
		</tr>
		<tr>
			<td width="95%">	
				<table cellpadding="0" cellspacing="0" style="margin-top: 4px; margin-left: 8px">
					<tr>
					<!--<img src="${ctx}/images/Logistica/izq1.jpg" height="34px" id="tdProducto01">-->
						<td>			
						<img src="../../../../images/Logistica/izq1.jpg" width="12" height="34" id="tdProducto01">
						</td>			
						<td bgcolor=#048BBA class="opc_combo" height="34px" style="font-size: 12px;">
							RESULTADOS DE LA B&Uacute;SQUEDA</td>
						<td><img src="../../../../images/Logistica/der1.jpg" width="12" height="34" id="tdProducto03"></td>
					</tr>
				</table>			
				<table width="101%" cellpadding="6" cellspacing="1"  style="width:98%;margin-left:9px">
					<tr height="30px">
						<td class="grilla" width="5%"  >Sel.</td>
						<td class="grilla" width="15%">C�digo</td>
						<td class="grilla">Nombre</td>
					</tr>	
					<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
						<tr class="tablagrilla">
							<td align="center">
							<input type="hidden" name="hid<c:out value="${loop.index}" />" id="hid<c:out value="${loop.index}" />" value="<c:out value="${objCast.nomUsuario}" />" />
							<input type="radio" ID="Radio3" NAME="Radio" onclick="fc_selecciona('<c:out value="${objCast.codTipoUsuario}" />','<c:out value="${objCast.nomUsuario}" />')" ></td>
							<td align="center">&nbsp;<c:out value="${objCast.codTipoUsuario}" /></td>
							<td><c:out value="${objCast.nomUsuario}" /></td>
						</tr>
					</c:forEach>												
				</table>			
			</td>
		</tr>
	</table>
	


				
</form:form>
<script language="javascript">
	function fc_CambiaBandeja(strParametro){	
		switch (strParametro){
			case document.getElementById("txhConstPerfilPFC").value:
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdPFR02").bgColor="#00A2E4";
				document.getElementById("tdPFR02").style.fontSize=12;				
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der2.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPCC02").bgColor="#048BBA";
				document.getElementById("tdPCC02").style.fontSize=10;		
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
				document.getElementById("tdEGRESADOS02").style.fontSize=10;			
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#048BBA";
				document.getElementById("tdPERSONAL02").style.fontSize=10;		
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAlumnoPFR.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;				
				break;
			case document.getElementById("txhConstPerfilPCC").value:
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPFR02").bgColor="#048BBA";
				document.getElementById("tdPFR02").style.fontSize=10;
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdPCC02").bgColor="#00A2E4";
				document.getElementById("tdPCC02").style.fontSize=12;			
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der2.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
				document.getElementById("tdEGRESADOS02").style.fontSize=10;			
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#048BBA";
				document.getElementById("tdPERSONAL02").style.fontSize=10;		
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAlumnoPCC.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;
				break;
			case document.getElementById("txhConstPerfilEGRESADO").value:
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPFR02").bgColor="#048BBA";
				document.getElementById("tdPFR02").style.fontSize=10;
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPCC02").bgColor="#048BBA";
				document.getElementById("tdPCC02").style.fontSize=10;			
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#00A2E4";
				document.getElementById("tdEGRESADOS02").style.fontSize=12;				
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der2.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#048BBA";
				document.getElementById("tdPERSONAL02").style.fontSize=10;			
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;	
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAlumnoEgresado.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;				
				break;
			case document.getElementById("txhConstPerfilPERSONAL").value:						
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPFR02").bgColor="#048BBA";
				document.getElementById("tdPFR02").style.fontSize=10;				
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPCC02").bgColor="#048BBA";
				document.getElementById("tdPCC02").style.fontSize=10;			
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
				document.getElementById("tdEGRESADOS02").style.fontSize=10;
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#00A2E4";
				document.getElementById("tdPERSONAL02").style.fontSize=12;			
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der2.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilPersonalTecsup.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;
				break;
			case document.getElementById("txhConstOrientado").value:
				if(document.getElementById("txhCodPerfilEncuesta").value!=""){
					//PFR
					document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdPFR02").bgColor="#048BBA";
					document.getElementById("tdPFR02").style.fontSize=10;
					document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
					//PCC		
					document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdPCC02").bgColor="#048BBA";
					document.getElementById("tdPCC02").style.fontSize=10;			
					document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
					//EGRESADOS
					document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
					document.getElementById("tdEGRESADOS02").style.fontSize=10;			
					document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
					//PERSONAL
					document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdPERSONAL02").bgColor="#048BBA";
					document.getElementById("tdPERSONAL02").style.fontSize=10;
					document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
					//ORIENTADO
					document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq2.jpg";
					document.getElementById("tdORIENTADO02").bgColor="#00A2E4";
					document.getElementById("tdORIENTADO02").style.fontSize=12;								
					document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der2.jpg";
					//LLAMAMOS A LA PAGINA POR EL IFRAME
					fc_OrientacionPerfil();
				}
				else{
					alert("Debe registrar un perfil");
					return;
				}				
				break;			
		}				
	}
	function fc_OrientacionPerfil(){
		codEncuesta = document.getElementById("txhCodEncuesta").value;
		codUsuario = document.getElementById("txhCodUsuario").value;
		codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
		codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
		document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/orientacionPerfil.html"+
			"?txhCodEncuesta="+codEncuesta+
			"&txhCodUsuario="+codUsuario+
			"&txhCodPerfilEncuesta="+codPerfilEncuesta+
			"&txhCodTipoAplicacion="+codTipoAplicacion;
	}
	function fc_OcultarPerfiles(opcion){		
		switch(opcion){
			case document.getElementById("txhConstPerfilPFC").value:
				pesta�aPFR.style.display="";
				pesta�aPCC.style.display="none";
				pesta�aEGRESADO.style.display="none";
				pesta�aPERSONAL.style.display="none";
				break;
			case document.getElementById("txhConstPerfilPCC").value:
				pesta�aPFR.style.display="none";
				pesta�aPCC.style.display="";
				pesta�aEGRESADO.style.display="none";
				pesta�aPERSONAL.style.display="none";
				break;
			case document.getElementById("txhConstPerfilEGRESADO").value:
				pesta�aPFR.style.display="none";
				pesta�aPCC.style.display="none";
				pesta�aEGRESADO.style.display="";
				pesta�aPERSONAL.style.display="none";
				break;
			case document.getElementById("txhConstPerfilPERSONAL").value:
				pesta�aPFR.style.display="none";
				pesta�aPCC.style.display="none";
				pesta�aEGRESADO.style.display="none";
				pesta�aPERSONAL.style.display="";
				break;
		}
	}	
</script>
</body>