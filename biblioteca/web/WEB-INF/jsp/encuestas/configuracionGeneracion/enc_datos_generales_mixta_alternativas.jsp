<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);
			
	fc_ini();			
}

function fc_ini(){

	numPreg = document.getElementById("txhNumPreguntas").value
	parent.fc_SetNumPreg(numPreg);
	document.getElementById("txtPeso").value = "1";		
}

function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtAlternativa").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("txtDescripcion").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	document.getElementById("txhCodigoSel").value = "";
}


function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){
		
			document.getElementById("operacion").value="irRegistrar";						
			document.getElementById("frmMain").submit();
		}	
	}
}

function fc_validaGrabar(){
	if(fc_Trim(document.getElementById("txtAlternativa").value)=="")
	{	alert(mstrIngreseAlternativa);
		document.getElementById("txtAlternativa").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngresePeso);//mstrSelTitulo;
		document.getElementById("txtPeso").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtDescripcion").value)==""){
		alert(mstrIngreseDescripcion);//mstrSelSede;
		document.getElementById("txtDescripcion").focus();
		return 0;
	}
		
	
	return 1;
}

function fc_Actualizar(){
	var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_mostrarDatos(posSel){

	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtAlternativa").value =  document.getElementById("hidAlternativa"+posSel).value ;
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	document.getElementById("txtDescripcion").value = document.getElementById("hidDescripcion"+posSel).value;
	
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function fc_Sel(pos){
	document.getElementById("txhPosSel").value = pos;
}

function fc_Eliminar(){
	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{	
			if(confirm(mstrSeguroEliminar1))
			{
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			}
		}
	else
		alert(mstrSeleccione);
}


</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/datosGeneralesMixtaAlternativas.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario"   id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<form:hidden path="txhCodGrupo" id="txhCodGrupo" />	
	<!-- INDICADOR DE QUE PAGINA ES LLAMADA -->
	<form:hidden path="txhVieneDePreguntasMixtas" id="txhVieneDePreguntasMixtas" />	
	<!-- PARA ENCUESTAS MIXTAS CON ALTERNATIVAS POR PREGUNTAS  -->
	<form:hidden path="txhCodPregunta" id="txhCodPregunta" />
	<form:hidden path="txhCodEstado" id="txhCodEstado" />
		
	<form:hidden path="tamanioBandeja" id="txhTamanioBandeja" />
	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />	
	
	
<!-- **************************************************************************************************** -->


	<table class="tabla2" width="97%" cellSpacing="0" cellPadding="0"
			 border="0" bordercolor='gray' ID="Table2"  style="margin-left:4px; margin-top:0px;height: 18px;">			  
		<tr>
			<td colspan='3' class="" style='cursor:hand;'>ALTERNATIVAS
			</td>					
		</tr>
	</table>						
			
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:99%; margin-left:4px; height: 70px;margin-bottom: 4px;" border="0" cellspacing="2" 
		cellpadding="2" class="tabla" bordercolor="red"> 
		<tr>
			<td class=""> Alternativa:</td>
			<td>					
				<form:input path="txtAlternativa" id="txtAlternativa" 
				 onkeypress="fc_ValidaTextoEspecialEncuestas()"  onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtAlternativa')"  
				cssClass="cajatexto_o" maxlength="5" size="25" />
			</td>
			<td class=""> Peso:</td>
			<td>						
				<form:input path="txtPeso" id="txtPeso" cssClass="cajatexto_o" cssStyle=""
				maxlength="3"
			 	onblur="fc_ValidaNumeroOnBlur('txtPeso');" 
					onkeypress="fc_PermiteNumeros();"   
				size="3" />
			</td>
			<td>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/iconos/grabar2.jpg',1)">
			<img onclick="fc_Grabar()"  src="${ctx}/images/iconos/grabar1.jpg" alt="Grabar" id="imgGrabar" style="cursor:pointer">&nbsp;</a>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','${ctx}/images/iconos/cerrar2.jpg',1)">
			<img onclick="fc_Limpiar();"  src="${ctx}/images/iconos/cerrar1.jpg" alt="Cancelar" id="imgCerrar" style="cursor:pointer"></a></td>
		</tr>
		<tr>
			<td class=""> Descripción:</td>
			<td colspan="3">
			
			<form:input path="txtDescripcion" id="txtDescripcion" 
			 onkeypress="fc_ValidaTextoEspecialEncuestas()"  onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtDescripcion')"  
			maxlength="255" cssClass="cajatexto_o" size="68"/>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<!-- VARIABEL CONTADORAS DE PREGUNTAS -->
	<c:set var="numPreg" value="0" />
	<table style="width: 99%; margin-top: 0px; margin-left: 4px;" cellSpacing="0" cellPadding="0" border="0" bordercolor="blue" ID="Table12">
		<tr>
			<td style="width: 95%;">			
			<table cellpadding="0" cellspacing="1" style="width:96%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
				<tr>
					<td class="grilla" style="width: 5%;">Sel.</td>
					<td class="grilla" style="width: 15%;">Alternativa</td>
					<td class="grilla" style="width: 70%;">Descripción</td>
					<td class="grilla" style="width: 10%;" >Peso</td>
				</tr>
			</table>
			<div style="overflow: auto; height: ${control.tamanioBandeja}px;width:100%">
			<table cellpadding="0" cellspacing="1" style="width:96%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
				<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
				<c:set var="numPreg" value="${numPreg+1}" />						
				<tr  class="tablagrilla">
					<td style="width: 5%;">
						<input type="hidden" id="hidCodigo<c:out value="${loop.index}" />" 		name="hidCodigo<c:out value="${loop.index}" />" value="<c:out value="${objCast.codAlternativa}" />" />
						<input type="hidden" id="hidAlternativa<c:out value="${loop.index}" />" 	name="hidAlternativa<c:out value="${loop.index}" />"  value="<c:out value="${objCast.etiAlternativa}" />"   />
						<input type="hidden" id="hidDescripcion<c:out value="${loop.index}" />" 	name="hidDescripcion<c:out value="${loop.index}" />"  value="<c:out value="${objCast.desAlternativa}" />"  />
						<input type="hidden" id="hidPeso<c:out value="${loop.index}" />" 			name="hidPeso<c:out value="${loop.index}" />"  value="<c:out value="${objCast.peso}" />"  />
						
						<INPUT class="radio" type="radio"  name="radio" onclick="fc_Sel('<c:out value="${loop.index}" />')" >
					 </td>
					<td align="center" style="width: 15%;"><c:out value="${objCast.etiAlternativa}" /></td>
					<td style="width: 70%;" align="CENTER"><c:out value="${objCast.desAlternativa}" /></td>
					<td style="width: 10%;" align="center"><c:out value="${objCast.peso}" /></td>
				</tr>							
				</c:forEach>						
			</table>
			</div>
			</td>
			<td align="right" valign="middle">									
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmodificar','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img onclick="fc_Actualizar()"  src= "${ctx}/images/iconos/modificar1.jpg" style="cursor:pointer" id="imgmodificar">
				</a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgQuitar','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img onclick="fc_Eliminar()"  src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer" id="imgQuitar">
				</a>
			</td>
		</tr>
	</table>	
<input type="hidden" id="txhNumPreguntas" name="txhNumPreguntas" value="<c:out value="${numPreg}" />" />
<!-- **************************************************************************************************** -->
</form:form>
</body>


