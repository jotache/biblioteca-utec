<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
}

function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtAlternativa").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("txtDescripcion").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	document.getElementById("txhCodigoSel").value = "";
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){

			document.getElementById("operacion").value="irRegistrar";						
			document.getElementById("frmMain").submit();
		}
	}
}

function fc_Actualizar(){
	var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_mostrarDatos(posSel){

	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtAlternativa").value =  document.getElementById("hidAlternativa"+posSel).value ;
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	document.getElementById("txtDescripcion").value = document.getElementById("hidDescripcion"+posSel).value;
	
}

function fc_validaGrabar(){
	if(fc_Trim(document.getElementById("txtAlternativa").value)=="")
	{	alert(mstrIngreseAlternativa);
		document.getElementById("txtAlternativa").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngresePeso);//mstrSelTitulo;
		document.getElementById("txtPeso").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtDescripcion").value)==""){
		alert(mstrIngreseDescripcion);//mstrSelSede;
		document.getElementById("txtDescripcion").focus();
		return 0;
	}
		
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function fc_Sel(pos){
	document.getElementById("txhPosSel").value = pos;
}

function fc_Eliminar(){
	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{	
			if(confirm(mstrSeguroEliminar1))
			{
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			}
		}
	else
		alert(mstrSeleccione);
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/datosGeneralesAlternativas.html">
	<!--OBJETOS PERSISTENTES-->
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario"   id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	
	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />
	
	<!-- DATA DE PRUEBEA ELIMINAR NOMAS -->

	<!-- FIN DATA DE PRUEBEA ELIMINAR NOMAS -->
	
<!-- **************************************************************************************************** -->


			<table class="tabla2" style="width:80%" cellSpacing="1" cellPadding="1" border="0" bordercolor='gray' ID="Table2"  style="margin-left:3px; margin-top:3px"> 
				<tr>
					<td colspan='3' class="" style='cursor:hand;'>ETIQUETAS</td>					
				</tr>
			</table>			
			
			<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:99%; margin-left:3px" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red"> 
				<tr>
					<td class=""> Alternativa:</td>
					<td>					
						<form:input path="txtAlternativa" id="txtAlternativa" maxlength="5" cssClass="cajatexto_o" size="25" />
					</td>
					<td class=""> Peso:</td>
					<td>						
						<form:input path="txtPeso" id="txtPeso" cssClass="cajatexto_o" cssStyle=""
						maxlength="3"
					 	onblur="fc_ValidaNumeroOnBlur('txtPeso');" 
 						onkeypress="fc_PermiteNumeros();" 
						 size="3" />
					</td>
					<td>
					 	<img onclick="fc_Grabar()" src="${ctx}/images/iconos/guardar_on.gif" alt="Grabar">&nbsp;
					 	<img onclick="fc_Limpiar();" src="${ctx}/images/iconos/cerrar_on.gif"   alt="Limpiar">
					</td>
				</tr>
				<tr>
					<td class=""> Descripción:</td>
					<td colspan="3">
					
					<form:input path="txtDescripcion" id="txtDescripcion" cssClass="cajatexto_o" maxlength="255" size="68"/>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table class="tabla" style="width:100%;margin-top:8px" cellSpacing="1" cellPadding="1" border="0" bordercolor='gray' ID="Table8"> 
				<tr>
					<td>
						<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:97%;">
							<tr>
								<td class="grilla" width="4%">Sel.</td>
								<td class="grilla">Alternativa</td>
								<td class="grilla">Descripción</td>
								<td class="grilla">Peso</td>
							</tr>
												
						<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
							<tr  class="tablagrilla">
								<td width="4%">
									<input type="hidden" id="hidCodigo<c:out value="${loop.index}" />" 		name="hidCodigo<c:out value="${loop.index}" />" value="<c:out value="${objCast.codAlternativa}" />" />
									<input type="hidden" id="hidAlternativa<c:out value="${loop.index}" />" 	name="hidAlternativa<c:out value="${loop.index}" />"  value="<c:out value="${objCast.etiAlternativa}" />"   />
									<input type="hidden" id="hidDescripcion<c:out value="${loop.index}" />" 	name="hidDescripcion<c:out value="${loop.index}" />"  value="<c:out value="${objCast.desAlternativa}" />"  />
									<input type="hidden" id="hidPeso<c:out value="${loop.index}" />" 			name="hidPeso<c:out value="${loop.index}" />"  value="<c:out value="${objCast.peso}" />"  />
									
									<INPUT class="radio" type="radio"  name="radio" onclick="fc_Sel('<c:out value="${loop.index}" />')" >
								 </td>
								<td align="center"><c:out value="${objCast.etiAlternativa}" /></td>
								<td><c:out value="${objCast.desAlternativa}" /></td>
								<td align="center"><c:out value="${objCast.peso}" /></td>
							</tr>							
						</c:forEach>
							
						</table>
					</td>
					<td align="center">
						<img onclick="fc_Actualizar()" src= "${ctx}/images/iconos/modificar_on.gif" style="cursor:hand"><br>
						<img onclick="fc_Eliminar()" src="${ctx}/images/iconos/eliminar_on.gif" style="cursor:hand">
					</td>
				</tr>
			</table>	

<!-- **************************************************************************************************** -->
</form:form>
</body>


