<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
	fc_ini();	
}

function validaTexto(nameObj,tamMax){
 	
 	fc_ValidaTextoEspecialEncuestas();
 	
	if(document.getElementById(nameObj).value.length > Number(tamMax))
	{
		str = document.getElementById(nameObj).value.substring(0,Number(tamMax));
		document.getElementById(nameObj).value=str;
		return false;
	}			
	
}

function fc_ini(){
	document.getElementById("chkObligatorio").checked =true;
	document.getElementById("cboTipo").value  = "0002";
	document.getElementById("txtPeso").value = "1";
}

function fc_SetNumPreg(numAlt){
	posSel = document.getElementById("txhPosSel").value;
	document.getElementById("hidAlternativasRel"+posSel).value = numAlt;	 
}

function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtPregunta").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("chkObligatorio").checked = false;	
	document.getElementById("cboTipo").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	//document.getElementById("txhCodigoSel").value = "";
}

function fc_Actualizar(){

	var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}

function fc_mostrarDatos(posSel){

	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtPregunta").value =  document.getElementById("hidPregunta"+posSel).value ;	
	document.getElementById("cboTipo").value = document.getElementById("hidTipo"+posSel).value;
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	
	
	indice = document.getElementById("hidIndUnica"+posSel).value;
	
	if(indice=="1")
		document.getElementById("radio1").checked = true;
	else	
		document.getElementById("radio2").checked = true;
		
	valCombo = document.getElementById("hidObligatorio"+posSel).value;
	if(valCombo=="1")
		document.getElementById("chkObligatorio").checked = true;
	else	
		document.getElementById("chkObligatorio").checked = false;
	
	Fc_CambiaTipo();	
	
}


function fc_Sel(pos){
	document.getElementById("txhPosSel").value = pos;
	fc_aparece("divAlternativasMixta",'0');
}

function fc_Respuestas(){
	posSel = document.getElementById("txhPosSel").value;
	//alert(">>"+posSel+">>");
	if(posSel!="") 
		{
			indAlternativa = document.getElementById("hidIndAlternativa"+posSel).value;

			//alert(">>"+indAlternativa+">>");			

			if(indAlternativa=="1"){//cambiar por 1
				fc_mostrarAlternativasxPregunta(posSel);
			}else{
				alert("No se puede realizar esta acci�n");
			}
		}
	else
		alert(mstrSeleccione);
}

function fc_mostrarAlternativasxPregunta(posSel){
		
		codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);		
		codGrupo = document.getElementById("txhCodGrupo").value;
		codPregunta = document.getElementById("hidCodigo"+posSel).value;		
		usuario = document.getElementById("usuario").value;
		
		fc_aparece("divAlternativasMixta",'1');		
		
						
		document.getElementById("iframeAlternativasMixta").src 
		="${ctx}/encuestas/datosGeneralesMixtaAlternativas.html"+
		"?prmCodEncuesta="+codEncuesta+
		"&prmCodGrupo="+codGrupo+
		"&prmUsuario="+usuario+
		"&prmCodPregunta="+codPregunta+
		"&prmVieneDePreguntasMixtas=yes"+
		"&prmCodEstado="+document.getElementById("txhCodEstado").value+
		"&prmTamanio=70";		
		
}

function Fc_CambiaTipo()
{
	Radiotipo.style.display="none";
	if (document.getElementById("cboTipo").value=='0002')
	{
		Radiotipo.style.display="";
	}
	else
	{
		Radiotipo.style.display="none";
	}
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){	
	
			document.getElementById("operacion").value="irRegistrar";						
			document.getElementById("frmMain").submit();
		}	
	}
}

function fc_validaGrabar(){
	
	if(fc_Trim(document.getElementById("txtPregunta").value)=="")
	{	alert(mstrIngrNombre);//mstrTipoAplicacion
		document.getElementById("txtPregunta").focus();
		return 0;
	}/*else if(fc_Trim(document.getElementById("cboAlternativa").value)==""){
		alert("Seleccione alternativa");//mstrSelTitulo;
		document.getElementById("cboAlternativa").focus();
		return 0;
	}*/else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngrPeso);//mstrSelTitulo;
		document.getElementById("txtPeso").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboTipo").value)==""){
		alert(mstrSeleccioneTipoEncuesta);//mstrSelTitulo;
		document.getElementById("cboTipo").focus();
		return 0;
	}
	
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcci�n");
}

function fc_Eliminar(){
	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{		
			numAltRel = document.getElementById("hidAlternativasRel"+posSel).value;
			
			if( Number(numAltRel)>0  ){
				alert(mstrTieneAltRelacionadas);
			}else{
			  if(confirm(mstrSeguroEliminar1))
			  {
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			  }			
			}
			

		}
	else
		alert(mstrSeleccione);
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/datosGeneralesMixtaPreguntas.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<form:hidden path="txhCodGrupo" id="txhCodGrupo" />
	<form:hidden path="txhCodAlternativa" id="txhCodAlternativa" />
	<form:hidden path="txhCodEstado" id="txhCodEstado" />

	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />
	<!-- **************************************************************************************************** -->
	<table cellpadding="0" cellspacing="0" width="100%" height="100%" border="0" bordercolor="black">
		<tr>
			<td valign="top" style="width: 50%;">
			
			<table class="tabla2" width="97%" cellSpacing="0" cellPadding="0"
				border="0" bordercolor='gray' ID="Table2" style="margin-top: 0px;height: 18px;">
				<tr>
					<td align="left" style='cursor: hand;'>PREGUNTAS</td>
				</tr>
			</table>
			<table background="${ctx}/images/biblioteca/fondosup.jpg"
				style="width: 100%;height: 70px;margin-bottom: 4px;" border="0" cellspacing="2" cellpadding="2" class="tabla" bordercolor="red">
				<tr>
					<td class="">Pregunta:</td>
					<td>
						<form:textarea path="txtPregunta" id="txtPregunta"
							cssClass="cajatexto_o" cssStyle="width:150px;height:30px;"
							onkeypress="validaTexto('txtPregunta','255')" 
							onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtPregunta')"/>
					</td>
					<td colspan="4" valign="middle">
						<form:checkbox path="chkObligatorio"
							id="chkObligatorio" value="1" /> Obligatorio&nbsp;
							
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/iconos/grabar2.jpg',1)">
							<img onclick="fc_Grabar()"  src="${ctx}/images/iconos/grabar1.jpg" alt="Grabar" id="imgGrabar" style="cursor:pointer">
						</a>
						&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','${ctx}/images/iconos/cerrar2.jpg',1)">
							<img onclick="fc_Limpiar();"  src="${ctx}/images/iconos/cerrar1.jpg" alt="Limpiar" id="imgCerrar" style="cursor:pointer">
						</a>
					</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td id="">Tipo:</td>
					<td id="ComboTipo">
						<form:select path="cboTipo" id="cboTipo"
							cssClass="cajatexto_o" cssStyle="width:120px"
							onclick="Fc_CambiaTipo()">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.lstTipo!=null}">
								<form:options itemValue="codTipoTablaDetalle"
									itemLabel="descripcion" items="${control.lstTipo}" />
							</c:if>
						</form:select>
					</td>	
					<td id="CajaTipo" style="display: none;">
						<input type="text"
							class="cajatexto_1" size="8" value="Cerrada" readonly></td>
					<td id="Radiotipo" name="tipo" colspan="">
						<input type=radio id="radio1" value="1" name="radio" checked>Unico&nbsp;
						<input type=radio id="radio2" value="0" name="radio">M�ltiple</td>
					<td>Peso :&nbsp;</td>
					<td>
						<form:input path="txtPeso" id="txtPeso"
							cssClass="cajatexto_o" maxlength="3"
							onblur="fc_ValidaNumeroOnBlur('txtPeso');"
							onkeypress="fc_PermiteNumeros();" size="3" />
					</td>
				</tr>
				<tr id="no1" name="no1" style="display: none">
					<td class="texto_bold">Peso:</td>
					<td colspan="3"><input type="text" size="17" class="cajatexto"
						ID="Text8" NAME="Text8"></td>
				</tr>
			</table>
			<table style="width: 100%; margin-top: 0px" cellSpacing="0" cellPadding="0" border="0" bordercolor="blue" ID="Table12">
				<tr>
					<td style="width: 95%;">					
					<table cellpadding="0" cellspacing="1" style="width:96%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
						<tr>
							<td class="grilla" style="width: 5%;">Sel.</td>
							<td class="grilla" style="width: 40%;">Pregunta</td>
							<td class="grilla" style="width: 15%;">Alternativa</td>
							<td class="grilla" style="width: 9%;">Tipo</td>
							<td class="grilla" style="width: 8%;">Peso</td>
							<td class="grilla" style="width: 15%;">Obligatorio</td>
							<td class="grilla" style="width: 8%;">Unica</td>
						</tr>
					</table>
					<div style="overflow: auto; height: 70px;width:100%">
					<table cellpadding="0" cellspacing="1" style="width:96%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">						
						<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
						<tr class="tablagrilla">
							<td style="width: 5%;"><input type="hidden"
								value="<c:out value="${objCast.codPregunta}" />"
								id="hidCodigo<c:out value="${loop.index}" />"
								name="hidCodigo<c:out value="${loop.index}" />" /> <input
								type="hidden" value="<c:out value="${objCast.nomPregunta}" />"
								id="hidPregunta<c:out value="${loop.index}" />"
								name="hidPregunta<c:out value="${loop.index}" />" /> <input
								type="hidden"
								value="<c:out value="${objCast.codTipoPregunta}" />"
								id="hidTipo<c:out value="${loop.index}" />"
								name="hidTipo<c:out value="${loop.index}" />" /> <input
								type="hidden"
								value="<c:out value="${objCast.indObligatorio}" />"
								id="hidObligatorio<c:out value="${loop.index}" />"
								name="hidObligatorio<c:out value="${loop.index}" />" /> <input
								type="hidden" value="<c:out value="${objCast.indUnica}" />"
								id="hidIndUnica<c:out value="${loop.index}" />"
								name="hidIndUnica<c:out value="${loop.index}" />" /> <input
								type="hidden"
								value="<c:out value="${objCast.indAlternativa}" />"
								id="hidIndAlternativa<c:out value="${loop.index}" />"
								name="hidIndAlternativa<c:out value="${loop.index}" />" /> <input
								type="hidden" value="<c:out value="${objCast.pesoPregunta}" />"
								id="hidPeso<c:out value="${loop.index}" />"
								name="hidPeso<c:out value="${loop.index}" />" /> <input
								type="hidden"
								value="<c:out value="${objCast.alternativasRel}" />"
								id="hidAlternativasRel<c:out value="${loop.index}" />"
								name="hidAlternativasRel<c:out value="${loop.index}" />" /> <INPUT
								class="radio" type="radio" name="radio"
								onclick="fc_Sel('<c:out value="${loop.index}" />')">
							<td style="width: 40%;"><textarea readonly="readonly" class="cajatexto_1"
								style="width: 97%; height: 30px;"> <c:out
								value="${objCast.nomPregunta}" /> </textarea></td>
		
							<td align="CENTER" style="width: 15%;">
								<c:out value="${objCast.desAlternativa}" />
								</td>
							<td style="width: 9%;"><c:out value="${objCast.nomTipoPregunta}" /></td>
							<td style="width: 8%;" align="CENTER"><c:out value="${objCast.pesoPregunta}" /></td>
							<td style="width: 15%;" align="CENTER"><c:if
								test="${objCast.indObligatorio==1}">Si</c:if><c:if
								test="${objCast.indObligatorio!=1}">No</c:if></td>
							<td style="width: 8%;" align="CENTER"><c:if test="${objCast.indUnica==1}">Si</c:if><c:if
								test="${objCast.indUnica!=1}">No</c:if></td>
						</tr>
						</c:forEach>
					</table>
					</div>
					</td>
					<td align="right" valign="middle">
					
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmodificar','','${ctx}/images/iconos/modificar2.jpg',1)">
							<img onclick="fc_Actualizar()"   src= "${ctx}/images/iconos/modificar1.jpg" style="cursor:hand" id="imgmodificar">							
						</a>
						<br>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/iconos/quitar2.jpg',1)">
							<img onclick="fc_Eliminar()"   src="${ctx}/images/iconos/quitar1.jpg" style="cursor:hand" id="imgEliminar">
						</a>
						<br>						
						<c:if test="${ control.txhCodAlternativa=='1' }">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgA','','${ctx}/images/iconos/a2.jpg',1)">
							<img onclick="fc_Respuestas()"  src="${ctx}/images/iconos/a.jpg" style="cursor:hand" id="imgA">
						</a>
						</c:if>
					</td>
				</tr>
			</table>
			</td>
			<td style="width: 50%;" valign="top">
				<table width="100%" border="0" bordercolor="red" cellpadding="0" cellspacing="0">
					<tr>
						<td>
						<div id="divAlternativasMixta" style="display: none; width: 100%; height: 100%;">
							<iframe id="iframeAlternativasMixta" name="iframeAlternativasMixta" frameborder="0" 
								style="width: 100%; margin-left: 0px;height: 197px;">
							</iframe>
						</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<!-- **************************************************************************************************** -->
</form:form>
</body>


