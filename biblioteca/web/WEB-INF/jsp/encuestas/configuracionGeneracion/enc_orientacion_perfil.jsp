<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
	function onLoad(){		
		if(parent.document.getElementById("txhCodTipoAplicacion").value=="0001"){			
			/*alert(parent.document.getElementById("txhCodPerfilEncuesta").value);*/
			codPerfil=parent.document.getElementById("txhCodPerfilEncuesta").value.split("|");			
			document.getElementById("cboPerfil").value=codPerfil[0];
			fc_OcultaCabecera();
			fc_SeleccionPerfil();			
		}
		else{			
			fc_SeleccionSeccion();
		}
		//************************		
		//MANEJO DE MENSAJES DESPUES DE GRABAR
		objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK_QUITAR" ){
			//PARA REFRESCAR EL TOTAL DE ENCUESTADOS DEL PADRE
			totalEncuestados=document.getElementById("txhTotalEncuestados").value;
			dscEncuestados=document.getElementById("txhDscEncuestados").value;
			parent.fc_ActualizarTotalEncuestado(totalEncuestados, dscEncuestados);
			//************************
			alert(mstrElimino);			
		}		
		else
		if ( objMsg.value == "ERROR_QUITAR" ){
			alert(mstrNoElimino);
		}
		else
		if ( objMsg.value != 'null' &&  objMsg.value != ""){
			alert(document.getElementById("txhDscMensaje").value)
		}
		document.getElementById("txhMsg").value="";
		document.getElementById("txhOperacion").value="";
		document.getElementById("txhCodProfesor").value="";
		//****************************************************************************		
	}	
	function fc_OcultaCabecera(){
		tdCabecera01.style.display="none";
		tdCabecera02.style.display="none";
	}	
	function fc_SeleccionPerfil(){
		opcion=document.getElementById("cboPerfil").value;
		fc_MostrarCabecera(opcion);		
		switch(opcion){
			case '0001':
				tablaPFR.style.display="";
				tablaPCC.style.display="none";
				tablaEGRESADOPERSONAL.style.display="none";
				tdEtiqueta.innerText="ALUMNOS PFR";
				break;
			case '0002':
				tablaPFR.style.display="none";
				tablaPCC.style.display="";
				tablaEGRESADOPERSONAL.style.display="none";
				tdEtiqueta.innerText="ALUMNOS PCC";				
				break;
			case '0003':
				tablaPFR.style.display="none";
				tablaPCC.style.display="none";
				tablaEGRESADOPERSONAL.style.display="";
				tdEtiqueta.innerText="EGRESADOS";				
				break;		
			case '0004':
				tablaPFR.style.display="none";
				tablaPCC.style.display="none";
				tablaEGRESADOPERSONAL.style.display="";
				tdEtiqueta.innerText="PERSONAL TECSUP";				
				break;
		}		
	}
	function fc_SeleccionPerfilSeccion(){
		document.getElementById("txhOperacion").value="SECCION";
		fc_oculta();
		document.getElementById("frmMain").submit();
	}
	function fc_SeleccionSeccion(){
		opcion=document.getElementById("cboPerfil").value;
		fc_MostrarCabecera(opcion);
		
		tablaPFR.style.display="none";
		tablaPCC.style.display="none";
		tablaEGRESADOPERSONAL.style.display="";
		
		switch(opcion){
			case '0001':
				tdEtiqueta.innerText="ALUMNOS PFR";
				break;
			case '0002':					
				tdEtiqueta.innerText="ALUMNOS PCC";
				break;
			case '0003':					
				tdEtiqueta.innerText="EGRESADOS";
				break;
			case '0004':					
				tdEtiqueta.innerText="PERSONAL TECSUP";
				break;
		}		
	}
	function fc_MostrarCabecera(opcion){
		tablaCabecera.style.display="";		
	}
	function fc_Excel(){		
		if(parent.document.getElementById("txhCodTipoAplicacion").value=="0001"){
			codPerfilEncuesta=document.getElementById("txhCodPerfilEncuesta").value;			
			codPerfil=codPerfilEncuesta.split("|");
			switch(codPerfil[0]){
				case "0001":
					if(document.getElementById("txhTamListaProfesoresPFR").value=="0"){
						alert("No se encontraron registros.");
						return;
					}
				case "0002":
					if(document.getElementById("txhTamListaProfesoresPCC").value=="0"){
						alert("No se encontraron registros.");
						return;
					}
				case "0003":
				case "0004":
					if(document.getElementById("txhTamListaGeneral").value=="0"){
						alert("No se encontraron registros.");
						return;
					}
			}
			codPerfilEncuesta = codPerfil[0];
			codEncuesta = document.getElementById("txhCodEncuesta").value;
			url="?txhCodTipoEncuesta=0001"+
				"&txhCodTipoPerfil=" + codPerfilEncuesta + 
				"&txhCodEncuesta=" + codEncuesta;			
			window.open("${ctx}/encuestas/perfilReporte.html"+url,"Reportes","resizable=yes, menubar=yes");
			//alert("realiza la opeacion PROGRAMA>>"+codPerfil[0]+"<<");
		}
		if(parent.document.getElementById("txhCodTipoAplicacion").value=="0002"){
			seleccion=document.getElementById("cboPerfil").value;			
			if(seleccion == ""){
				alert("Debe seleccionar un tipo de Secci�n.");
				document.getElementById("cboPerfil").focus();
				return;				
			}
			if(document.getElementById("txhTamListaGeneral").value=="0"){				
				alert("No se encontraron registros.");
				return;
			}
			codEncuesta = document.getElementById("txhCodEncuesta").value;
			url="?txhCodTipoEncuesta=0002"+
				"&txhCodTipoPerfil=" + seleccion + 
				"&txhCodEncuesta=" + codEncuesta;
			window.open("${ctx}/encuestas/perfilReporte.html"+url,"Reportes","resizable=yes, menubar=yes");									
		}			
	}
	
	function fc_oculta(){
		document.getElementById("divajax").style.display = "";;		
		document.getElementById("divtabla").style.display = "none";
	}
	
	function fc_QuitarProfesor(){
		//alert("fc_QuitarProfesorPFR");	
		if (document.getElementById("txhCodProfesor").value != ""){
			if( confirm(mstrEliminar)){
				//document.getElementById("txhSeleccion").value="";
				document.getElementById("txhOperacion").value = "QUITAR";
				document.getElementById("frmMain").submit();
			}			
		}
		else{
			alert(mstrSeleccion);
			return false;
		}		
	}
	function fc_SeleccionarRegistro(codProfesor,codSeccion,codCiclo,codDepartamento,codCurso,codProducto,codPrograma){
		/*alert("codEncuesta>>"+document.getElementById("txhCodEncuesta").value+
			"/ncodPerfilEncuesta>>"+document.getElementById("txhCodPerfilEncuesta").value+
			"/ncodProfesor>>"+codProfesor+
			"/ncodSeccion>>"+codSeccion+
			"/ncodCiclo>>"+codCiclo+
			"/ncodDepartamento>>"+codDepartamento+
			"/ncodCurso>>"+codCurso+
			"/ncodProducto>>"+codProducto+
			"/ncodPrograma>>"+codPrograma);*/
		document.getElementById("txhCodProfesor").value=codProfesor;
		document.getElementById("txhCodProducto").value=codProducto;
		document.getElementById("txhCodPrograma").value=codPrograma;
		document.getElementById("txhCodCentroCosto").value=codDepartamento;
		document.getElementById("txhCodCurso").value=codCurso;
		document.getElementById("txhCodCiclo").value=codCiclo;
		document.getElementById("txhCodSeccion").value=codSeccion;
	}
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/orientacionPerfil.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="dscMensaje" id="txhDscMensaje" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	
	<form:hidden path="codEncuesta" id="txhCodEncuesta" />
	
	<form:hidden path="codPerfilEncuesta" id="txhCodPerfilEncuesta" />
	<!-- A�ADIDO PARA ELIMINAR REGISTRO -->
	<form:hidden path="codProfesor" id="txhCodProfesor" />
	<form:hidden path="codProducto" id="txhCodProducto" />
	<form:hidden path="codPrograma" id="txhCodPrograma" />
	<form:hidden path="codCentroCosto" id="txhCodCentroCosto" />
	<form:hidden path="codCurso" id="txhCodCurso" />
	<form:hidden path="codCiclo" id="txhCodCiclo" />
	<form:hidden path="codSeccion" id="txhCodSeccion" />
	<!-- /A�ADIDO PARA ELIMINAR REGISTRO -->
	<form:hidden path="tamListaProfesoresPFR" id="txhTamListaProfesoresPFR" />
	<form:hidden path="tamListaProfesoresPCC" id="txhTamListaProfesoresPCC" />
	<form:hidden path="tamListaGeneral" id="txhTamListaGeneral" />	
	
	<form:hidden path="totalEncuestados" id="txhTotalEncuestados" />
	<form:hidden path="dscEncuestados" id="txhDscEncuestados" />
	
	<form:hidden path="tamMostrar" id="txhTamMostrar" />
	<!-- TABLA CABECERA ORIENTADO -->
	<DIV id="divajax" align="center" class="texto_bold" style="display:none" ><br><br><br><br><br><br><br><br>Espere...<br><img src="${ctx}/images/ajaxloader.gif"><br></DIV>	
	<DIV id="divtabla"  align="left" >
	<table background="${ctx}/images/biblioteca/fondosup.jpg" class="tabla" style="width:97%;margin-left:9px;" 
	cellspacing="6" cellpadding="0" border="0" bordercolor="red">
		<tr>
			<td width="10%" id="tdCabecera01">Se configur�</td>
			<td width="50%" id="tdCabecera02">
				<form:select path="codPerfilSeleccion" cssClass="cajatexto_o" cssStyle="width: 150px"
					onchange="fc_SeleccionPerfilSeccion();" id="cboPerfil">
					<form:option value="">--Seleccione--</form:option>					
					<form:option value="0001">Alumnos PFR</form:option>
					<form:option value="0002">Alumnos PCC</form:option>
					<form:option value="0003">Egresados</form:option>
					<form:option value="0004">Personal TECSUP</form:option>										
				</form:select>&nbsp;Mostrar� :				
			</td>
			<!-- <td width="40%" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/iconos/grabar2.jpg',1)">
					<img src="${ctx}/images/iconos/grabar1.jpg" style="cursor:pointer;" id="imgGrabar01" onclick="fc_Grabar('1');" 
					alt="Grabar">
				</a>			
			</td> -->
		</tr>
	</table>	
	<table background="${ctx}/images/Evaluaciones/back.jpg" class="tabla" style="width:97%;margin-left:9px;display: none;" 
		cellspacing="6" cellpadding="0" border="0" bordercolor="red" id="tablaCabecera">
		<tr>
			<td width="20%" id="tdEtiqueta" colspan="2">
			</td>
			<td width="60%">&nbsp;
			</td>
			<td width="10%">Total: ${control.tamMostrar}
			</td>
			<td width="10%" align="right">	
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
					<img src="${ctx}/images/iconos/exportarex1.jpg" alt="Exportar" align="middle" style="cursor: pointer;" 
					 	onclick="javascript:fc_Excel();" id="imgExcel"></a>
			</td>				
		</tr>						
	</table>
	<!-- TABLA PFR -->
	<table cellpadding="0" cellspacing="0" id="tablaPFR" 
			style="width:97%;margin-top:6px;margin-left:9px;display: none;" border="0" bordercolor="blue">
		<tr>
			<td>
				<div style="overflow: auto; height: 220px" >
				<table cellpadding="0" cellspacing="1" id="tablaBandejaPFR"
					style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
					<tr>											
						<td class="grilla" width="4%">Sel.</td>	
						<td class="grilla" width="40%">Profesor</td>
						<td class="grilla" width="8%">Seccion</td>
						<td class="grilla" width="8%">Ciclo</td>
						<td class="grilla" width="20%">Departamento</td>
						<td class="grilla" width="20%">Curso</td>													
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaProfesoresPFR}"  >
					<tr>
						<td align="center" class="tablagrilla" style="width: 4%">
							<input type="radio" name="encuestaPFR"
								value="" id='txhRbtEncPFR<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('${lista.codProfesor}','${lista.codSeccion}','${lista.codCiclo}','${lista.codDepartamento}','${lista.codCurso}','${lista.codProducto}','${lista.codPrograma}');">
						</td>						
						<td align="left" class="tablagrilla" style="width: 40%">
							${lista.nomProfesor}														
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							${lista.nomSeccion}
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							${lista.nomCiclo}									
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							${lista.nomDepartamento}
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">						
							${lista.nomCurso}		
						</td>			
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaProfesoresPFR == "0"}'>
					<tr>
						<td align="center" class="tablagrilla" colspan="6">						
							No se encontraron Registros		
						</td>			
					</tr>
					</c:if>										
				</table>
				</div>				
			</td>
			<td width="25px" valign="middle" align="right">
				<br/>			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliProPFR','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliProPFR" onclick="javascript:fc_QuitarProfesor();">
				</a>
			</td>
		</tr>
	</table>
	<!-- TABLA PCC -->
	<table cellpadding="0" cellspacing="0" id="tablaPCC"
			style="width:97%;margin-top:6px;margin-left:9px;display: none;">
		<tr>
			<td>
				<div style="overflow: auto; height: 220px" >
				<table cellpadding="0" cellspacing="1" id="tablaBandeja"
					style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
					<tr>
						<td class="grilla" width="4%">Sel.</td>
						<td class="grilla" width="50%">Profesor</td>						
						<td class="grilla" width="23%">Departamento</td>
						<td class="grilla" width="23%">Curso</td>													
					</tr>					
					<c:forEach varStatus="loop" var="lista" items="${control.listaProfesoresPCC}"  >
					<tr>
						<td align="center" class="tablagrilla" style="width: 4%">
							<input type="radio" name="encuestaPCC"
								value="" id='txhRbtEncPCC<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('${lista.codProfesor}','${lista.codSeccion}','${lista.codCiclo}','${lista.codDepartamento}','${lista.codCurso}','${lista.codProducto}','${lista.codPrograma}');">
						</td>
						<td align="left" class="tablagrilla" style="width: 50%">
							${lista.nomProfesor}														
						</td>						
						<td align="left" class="tablagrilla" style="width: 23%">
							${lista.nomDepartamento}
						</td>
						<td align="left" class="tablagrilla" style="width: 23%">						
							${lista.nomCurso}		
						</td>			
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaProfesoresPCC == "0"}'>
					<tr>
						<td align="center" class="tablagrilla" colspan="4">						
							No se encontraron Registros		
						</td>			
					</tr>
					</c:if>					
				</table>
				</div>				
			</td>
			<td width="25px" valign="middle" align="right">
				<br/>			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliProPCC','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliProPCC" onclick="javascript:fc_QuitarProfesor();">
				</a>
			</td>
		</tr>
	</table>
	<!-- TABLA EGRESADOS PERSONAL TECSUP-->
	<table cellpadding="0" cellspacing="0" id="tablaEGRESADOPERSONAL"
			style="width:97%;margin-top:6px;margin-left:9px;display: none;">
		<tr>
			<td width="100%">
				<div style="overflow: auto; height: 200px" >
				<table cellpadding="0" cellspacing="1" id="tablaBandeja"
					style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
					<tr>
						<td class="grilla" width="60%">Nombres</td>						
						<td class="grilla" width="40%">Correos</td>																			
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaGeneral}"  >
					<tr>						
						<td align="left" class="tablagrilla" style="width: 40%">
							${lista.nombreAlumno}														
						</td>						
						<td align="left" class="tablagrilla" style="width: 20%">
							${lista.correo}
						</td>						
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaGeneral == "0"}'>
					<tr>
						<td align="center" class="tablagrilla" colspan="2">						
							No se encontraron Registros
						</td>
					</tr>
					</c:if>
				</table>
				</div>
			</td>
		</tr>
	</table>
	</DIV>	
</form:form>
</body>