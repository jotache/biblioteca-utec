<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
	function onLoad(){
		if(document.getElementById("txhIndCiclo").value=="1"){
			fc_MostrarCiclo("1");
		}
		//metodos para marcar los valores seleccionados antes de la busqueda de cursos
		if(document.getElementById("txhCadenaCodDepartamentos").value!=""){
			fc_MarcarDepartamentos();
		}		
		if(document.getElementById("txhCadenaCodCiclos").value!=""){
			fc_MarcarCiclos();
		}		
		//****************************************************************************
		//MANEJO DE MENSAJES DESPUES DE GRABAR
		objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK_GRABAR" ){
			//PARA REFRESCAR EL TOTAL DE ENCUESTADOS DEL PADRE
			totalEncuestados=document.getElementById("txhTotalEncuestados").value;
			dscEncuestados=document.getElementById("txhDscEncuestados").value;
			parent.fc_ActualizarTotalEncuestado(totalEncuestados, dscEncuestados);
			//************************
			alert(mstrGrabar);
			codTipoAplicacion=parent.document.getElementById("txhCodTipoAplicacion").value;			
			if(codTipoAplicacion==document.getElementById("txhEncuestaPrograma").value){
				parent.document.getElementById("txhCodPerfilEncuesta").value=document.getElementById("txhPerfilPFR").value;
				parent.fc_OcultarPerfiles(document.getElementById("txhPerfilPFR").value);
			}
			else
			if(codTipoAplicacion==document.getElementById("txhEncuestaSeccion").value){
				parent.document.getElementById("txhCodPerfilEncuesta").value=document.getElementById("txhCodPerfilEncuesta").value;
			}
		}		
		else
		if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);
		}
		else
		if ( objMsg.value != 'null' &&  objMsg.value != ""){
			alert(document.getElementById("txhDscMensaje").value)
		}
		document.getElementById("txhMsg").value="";
		document.getElementById("txhOperacion").value="";
		//****************************************************************************
	}	
	function fc_MarcarDepartamentos(){		
		cadenaDepartamentos = document.getElementById("txhCadenaCodDepartamentos").value;		
		tamCadenaDepartamentos = fc_GetNumeroSeleccionados("txhCadenaCodDepartamentos");		
		tamListaDepartamentos = document.getElementById("txhTamListaDepartamento").value;
		
		cadena = cadenaDepartamentos.split("|");
		for(var i=0; i < tamCadenaDepartamentos; i++){//txhDepartamento
			for(var j=0; j < tamListaDepartamentos; j++){
				codDepartamento=document.getElementById("txhDepartamento"+j).value;
				if(cadena[i]==codDepartamento){
					document.getElementById("txhDepartamento"+j).checked=true;
				}
			}			
		}
	}
	function fc_MarcarCiclos(){
		cadenaCiclos = document.getElementById("txhCadenaCodCiclos").value;		
		tamCadenaCiclos = fc_GetNumeroSeleccionados("txhCadenaCodCiclos");		
		tamListaCiclos = document.getElementById("txhTamListaCiclo").value;
		
		cadena = cadenaCiclos.split("|");
		for(var i=0; i < tamCadenaCiclos; i++){//txhDepartamento
			for(var j=0; j < tamListaCiclos; j++){
				codCiclo=document.getElementById("txhCiclo"+j).value;
				if(cadena[i]==codCiclo){
					document.getElementById("txhCiclo"+j).checked=true;
				}
			}			
		}
	}
	function fc_SeleccionPrograma(){		
		if(document.getElementById("cboPrograma").value!=""){
			document.getElementById("txhCadenaCodCiclos").value="";
			var codPrograma=document.getElementById("cboPrograma").value;
			var indicador=document.getElementById("txhPrograma"+codPrograma).value;			
			if(indicador > 0){
				document.getElementById("txhIndCiclo").value="1";
				document.getElementById("txhOperacion").value="PROGRAMA";
				document.getElementById("frmMain").submit();
			}
			else{
				document.getElementById("txhIndCiclo").value="0";
				fc_MostrarCiclo("0");
				fc_LimpiarCombo("cboCurso");
				fc_LimpiarCombo("cboCursosSel");
			}
		}
	}
	function fc_MostrarCiclo(opcion){
		if(opcion=="1"){
			tdCicloAux01.style.display="none";
			tdCicloAux02.style.display="none";
			tdCiclo01.style.display="";
			tdCiclo02.style.display="";
			tdCiclo03.style.display="";
		}
		else{
			tdCicloAux01.style.display="";
			tdCicloAux02.style.display="";
			tdCiclo01.style.display="none";
			tdCiclo02.style.display="none";
			tdCiclo03.style.display="none";
		}
	}
	function fc_BuscarCursos(){
		codPrograma=document.getElementById("cboPrograma").value;	
		codDepartamento=document.getElementById("txhCadenaCodDepartamentos").value;
		codCiclo=document.getElementById("txhCadenaCodCiclos").value;
		indCiclo=document.getElementById("txhIndCiclo").value;
		if(codPrograma==""){
			alert("Debe Seleccionar un tipo de Programa.");
			document.getElementById("cboPrograma").focus();
			return;
		}
		if(codDepartamento==""){
			alert("Debe Seleccionar al menos un tipo de Departamento.");
			return;
		}
		if(indCiclo=="1"){
			if(codCiclo==""){
				alert("Debe Seleccionar al menos un tipo de Ciclo.");
				return;
			}
		}
		document.getElementById("txhOperacion").value="BUSCAR_CURSOS";
		document.getElementById("frmMain").submit();
		
	}	
	function fc_Grabar(){
		codPrograma=document.getElementById("cboPrograma").value;
		if(codPrograma==""){
			alert("Debe Seleccionar un tipo de Programa.");
			document.getElementById("cboPrograma").focus();
			return;
		}
		fc_ObtenerDatosPFR();
		if(document.getElementById("txhCadenaCodCursos").value==""){
			alert("Debe Seleccionar al menos un curso.");
			document.getElementById("cboCurso").focus();
			return;
		}
		if(confirm(mstrSeguroGrabar)){
			fc_oculta();
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
	function fc_oculta(){
	
		document.getElementById("divajax").style.display = "";;		
		document.getElementById("divtabla").style.display = "none";
		
	}	
	function fc_ObtenerDatosPFR(){
		cadenaCurSel = fc_ObtenerCadenaCombo("cboCursosSel");
		document.getElementById("txhCadenaCodCursos").value = cadenaCurSel;
	}
	function fc_ObtenerCadenaCombo(combo){
		objCombo = document.getElementById(combo);
		cadena = "";							
		for(var i = 0; i < objCombo.options.length ; i ++){
			opcion = objCombo.options[i];
			if(i == "0"){
				cadena = opcion.value;
			}
			else{						
				cadena = cadena + "|" + opcion.value;
			}
		}
		return cadena;
	}
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/perfilAlumnoPFR.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="dscMensaje" id="txhDscMensaje" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	
	<form:hidden path="indCiclo" id="txhIndCiclo" />
	
	<form:hidden path="codEncuesta" id="txhCodEncuesta" />
	<form:hidden path="codPerfilEncuesta" id="txhCodPerfilEncuesta" />
	<form:hidden path="codTipoEncuesta" id="txhCodTipoEncuesta" />	
	
	<form:hidden path="cadenaCodDepartamentos" id="txhCadenaCodDepartamentos" />
	<form:hidden path="cadenaCodCiclos" id="txhCadenaCodCiclos" />
	<form:hidden path="cadenaCodCursos" id="txhCadenaCodCursos" />
			
	<form:hidden path="tamListaDepartamento" id="txhTamListaDepartamento" />
	<form:hidden path="tamListaCiclo" id="txhTamListaCiclo" />
	<!-- CONSTANTES -->
	<form:hidden path="encuestaPrograma" id="txhEncuestaPrograma" />
	<form:hidden path="encuestaSeccion" id="txhEncuestaSeccion" />
	<form:hidden path="perfilPFR" id="txhPerfilPFR" />
	<form:hidden path="totalEncuestados" id="txhTotalEncuestados" />
	<form:hidden path="dscEncuestados" id="txhDscEncuestados" />	
	<!-- TABLA PFR -->	
	
	<DIV id="divajax" align="center" class="texto_bold" style="display:none" ><br><br><br><br><br><br><br><br>Espere...<br><img src="${ctx}/images/ajaxloader.gif"><br>Guardando Información<br></DIV>	
	<DIV id="divtabla"  align="left" >
	<table background="${ctx}\images\biblioteca\fondosup.jpg" class="tabla" style="width:97%;margin-left:9px;" 
		cellspacing="6" cellpadding="0" border="0" bordercolor="red" id="tablaPFR">
		<tr>
			<td width="10%">Especialización:</td>
			<td colspan="3">
				<form:select path="codPrograma" id="cboPrograma" 
					cssStyle="width:260px" cssClass="cajatexto_o" onchange="fc_SeleccionPrograma();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaPrograma != null}">
						<form:options itemValue="codPrograma" itemLabel="nomProducto" 
							items="${control.listaPrograma }" />
					</c:if>
				</form:select>
				<c:forEach varStatus="loop" var="lista" items="${control.listaPrograma}"  >
					<input type="hidden" id="txhPrograma<c:out value="${lista.codPrograma}" />"
						value="<c:out value="${lista.indCiclo}" />"/>
				</c:forEach>
			</td>
			<td align="right" style="width: 40%;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/iconos/grabar2.jpg',1)">
					<img src="${ctx}/images/iconos/grabar1.jpg" style="cursor:pointer;" id="imgGrabar01" onclick="fc_Grabar();" 
					alt="Grabar">
				</a>			
			</td>
		</tr>		
		<tr>
			<td valign="top" width="10%">Departamento :			
			</td>
			<td width="30%" nowrap>
				<div style="overflow: auto; height: 97px">
				<table cellpadding="0" cellspacing="1" style="width:94%; margin-top:0px;margin-bottom:5px;border: 1px solid white;"
					id="tablaDepartamento">				
					<tr>
						<td class="grilla" width="10%">Sel.</td>
						<td class="grilla" width="90%">Departamento</td>													
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaDepartamento}"  >
					<tr>						
						<td align="center" class="tablagrilla" style="width: 10%">
							<input type="checkbox" value="${lista.codDepartamento}"
								id="txhDepartamento${loop.index}"
								name="departamento" onclick="fc_Seleccion(this.value,'txhCadenaCodDepartamentos');">
						</td>
						<td align="left" class="tablagrilla" style="width: 90%">							
							&nbsp;<c:out value="${lista.nomDepartamento}" />
						</td>													
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaDepartamento=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="2" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
					<!--<c:if test='${control.tamListaDepartamento > 3}'>
					<script type="text/javascript">
						document.getElementById("tablaDepartamento").style.width = "100%";
					</script>		 		
					</c:if>-->
				</table>				
				</div>
			</td>
			<td id="tdCicloAux01" valign="bottom">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar01','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar01" 
					style="CURSOR: pointer" onclick="javascript:fc_BuscarCursos();">
				</a>&nbsp;
			</td>
			<td id="tdCicloAux02">
			</td>
			<td valign="top" id="tdCiclo01" style="display: none;width: 10%;">Ciclo :			
			</td>
			<td id="tdCiclo02" style="display: none;width: 30%">
				<div style="overflow: auto; height: 97px">
				<table cellpadding="0" cellspacing="1" style="width:94%; margin-top:0px;margin-bottom:5px;border: 1px solid white;"
					id="tablaCiclo">				
					<tr>
						<td class="grilla" width="10%">Sel.</td>
						<td class="grilla" width="90%">Ciclo</td>													
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaCiclo}"  >
					<tr>						
						<td align="center" class="tablagrilla" style="width: 10%">
							<input type="checkbox" value="${lista.codCiclo}" name="ciclo"
								id="txhCiclo${loop.index}" onclick="fc_Seleccion(this.value,'txhCadenaCodCiclos');">							
						</td>
						<td align="left" class="tablagrilla" style="width: 90%">							
							&nbsp;<c:out value="${lista.nomCiclo}" />
						</td>													
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaCiclo=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="2" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>					
					<!--<c:if test='${control.tamListaCiclo > 3}'>
					<script type="text/javascript">						
						document.getElementById("tablaCiclo").style.width = "90%";
					</script>
					</c:if>-->
				</table>				
				</div>				
			</td>
			<td width="40%" valign="bottom" id="tdCiclo03" style="display: none;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar02','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar02" 
					style="CURSOR: pointer" onclick="javascript:fc_BuscarCursos();">
				</a>&nbsp;
			</td>
		</tr>
		<tr>
			<td valign="top" style="width: 10%">
				Cursos :			
			</td>
			<td style="width: 30%">
				Disponibles			
			</td>
			<td valign="top" style="width: 10%;">			
			</td>
			<td style="width: 30%">
				Seleccionados							
			</td>				
			<td style="width: 20%;">
			</td>
		</tr>
		<tr>
			<td valign="top" width="10%">			
			</td>
			<td width="20%">
				<form:select  path="codCurso" id="cboCurso" cssClass="cajatexto" 
					cssStyle="width:100%;height: 105px;" multiple="true">
			        <c:if test="${control.listaCurso!=null}">
		        	<form:options itemValue="codCurso" itemLabel="nomCurso" 
		        		items="${control.listaCurso}" />						        
		            </c:if>
		       </form:select>				
			</td>
			<td align="center" valign="middle" style="width: 10%;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgadelantePFR','','${ctx}/images/iconos/agregaruno2.jpg',1)">
				<img src="${ctx}/images/iconos/agregaruno1.jpg" alt="Agregar" onclick="javascript:fc_Mover('right','cboCurso','cboCursosSel')" id="imgadelantePFR" style="cursor:hand"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgatrasPFR','','${ctx}/images/iconos/quitaruno2.jpg',1)">
				<img src="${ctx}/images/iconos/quitaruno1.jpg" alt="Quitar" onclick="javascript:fc_Mover('left','cboCurso','cboCursosSel')" id="imgatrasPFR" style="cursor:hand"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagtodPFR','','${ctx}/images/iconos/agregartodos2.jpg',1)">
				<img src="${ctx}/images/iconos/agregartodos1.jpg" onclick="javascript:fc_MoverTodo('cboCurso','cboCursosSel')"  alt="Agregar Todos" style="cursor:hand" id="imgagtodPFR"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitodPFR','','${ctx}/images/iconos/quitartodos2.jpg',1)">
				<img src="${ctx}/images/iconos/quitartodos1.jpg" onclick="javascript:fc_QuitarTodo('cboCurso','cboCursosSel')" alt="Quitar Todos" style="cursor:hand" id="imgquitodPFR"></a>
			</td>
			<td width="20%">
				<form:select  path="codCursoSel" id="cboCursosSel" cssClass="cajatexto_o" 
					cssStyle="width:100%;height: 105px;font-size: 11px;" multiple="true">
			        <c:if test="${control.listaCursoSel!=null}">
		        	<form:options itemValue="codCurso" itemLabel="nomCurso" 
		        		items="${control.listaCursoSel}" />						        
		            </c:if>
		       </form:select>				
			</td>
			<td width="40%">
			</td>
		</tr>
	</table>
	</DIV>		
</form:form>
<script language="javascript">
	function fc_Mover(side, cboOrigen, cboDestino){
		var origen;//origen
		var destino;//destino
		if (side == "right")
		{ 
			origen = document.getElementById(cboOrigen);
			destino = document.getElementById(cboDestino);
		}
		else
		{  
			origen = document.getElementById(cboDestino);
			destino = document.getElementById(cboOrigen); 
		}
		var idSel=-1;
		for (var i = 0; i < origen.length; i++)
		{  
			if ( origen.options[i].selected )
			{
				idSel=	i;
			}
		}
		if(idSel==-1)
		{
			alert(mstrSeleccion);
			return;
		}
		var NroItemsDestino=destino.length;
		var oOption = document.createElement('OPTION');
		destino.options.add(oOption);
		oOption.value = origen.options[idSel].value;
		oOption.innerText = origen.options[idSel].text;
		for(i=idSel;i < origen.length-1; i++){
			origen.options[i].value=origen.options[i+1].value;
			origen.options[i].text=origen.options[i+1].text;
		}
		origen.length=origen.length-1;
		origen.focus()
	}
	function fc_MoverTodo( cboOrigen, cboDestino){
		fbox = document.getElementById(cboOrigen);
		tbox = document.getElementById(cboDestino);
		var arrFbox = new Array();
		var arrTbox = new Array();
		var arrLookup = new Array();
		var i;
		for (i = 0; i < tbox.options.length; i++){
			arrLookup[tbox.options[i].text] = tbox.options[i].value;
			arrTbox[i] = tbox.options[i].text;
		}
		var fLength = 0;
		var tLength = arrTbox.length;
		for(i = 0; i < fbox.options.length; i++){
			arrLookup[fbox.options[i].text] = fbox.options[i].value;				
			arrTbox[tLength] = fbox.options[i].text;
			tLength++;				
		}
		arrFbox.sort();
		arrTbox.sort();
		fbox.length = 0;
		tbox.length = 0;
		var c;
		for(c = 0; c < arrFbox.length; c++){
			var no = new Option();
			no.value = arrLookup[arrFbox[c]];
			no.text = arrFbox[c];
			fbox[c] = no;
		}
		for(c = 0; c < arrTbox.length; c++){
			var no = new Option();
			no.value = arrLookup[arrTbox[c]];
			no.text = arrTbox[c];
			tbox[c] = no;
		}
		document.getElementById(cboOrigen).value = fbox;
		document.getElementById(cboDestino).value = tbox;
	}
	function fc_QuitarTodo(cboOrigen, cboDestino){
		fbox = document.getElementById(cboDestino);
		tbox = document.getElementById(cboOrigen);
		var arrFbox = new Array();
		var arrTbox = new Array();
		var arrLookup = new Array();
		var i;
		for (i = 0; i < tbox.options.length; i++) 
		{
			arrLookup[tbox.options[i].text] = tbox.options[i].value;
			arrTbox[i] = tbox.options[i].text;
		}
		var fLength = 0;
		var tLength = arrTbox.length;
		for(i = 0; i < fbox.options.length; i++) 
		{
			arrLookup[fbox.options[i].text] = fbox.options[i].value;				
			arrTbox[tLength] = fbox.options[i].text;
			tLength++;				
		}
		arrFbox.sort();
		arrTbox.sort();
		fbox.length = 0;
		tbox.length = 0;
		var c;
		for(c = 0; c < arrFbox.length; c++) 
		{
			var no = new Option();
			no.value = arrLookup[arrFbox[c]];
			no.text = arrFbox[c];
			fbox[c] = no;
		}
		for(c = 0; c < arrTbox.length; c++) 
		{
			var no = new Option();
			no.value = arrLookup[arrTbox[c]];
			no.text = arrTbox[c];
			tbox[c] = no;
		}
		document.getElementById(cboDestino).value = fbox;
		document.getElementById(cboOrigen).value = tbox;
	}
	function fc_Seleccion(codigo, idCadena){
		strCodSel = document.getElementById(idCadena).value;
		flag=false;
		if (strCodSel!='')
		{	ArrCodSel = strCodSel.split("|");
			strCodSel = "";
			for (i=0;i<=ArrCodSel.length-2;i++)
			{	if (ArrCodSel[i] == codigo)flag = true 
				else strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
		}
		if (!flag)
		{
			strCodSel = strCodSel + codigo + '|';
		}
		
		document.getElementById(idCadena).value = strCodSel;
		
		//alert(document.getElementById(idCadena).value);
	}
	function fc_GetNumeroSeleccionados(idCadena){
		strCodSel = document.getElementById(idCadena).value;
		
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			return ArrCodSel.length - 1;
		}
		else return 0;
	}
	function fc_LimpiarCombo(cboSeleccion){		
		combo = document.getElementById(cboSeleccion);
		tam = combo.options.length;		
		var opcion=0;
		for(var i=0; i < tam ; i++){						
			var aBorrar = document.getElementById(cboSeleccion).options[opcion];						
			aBorrar.parentNode.removeChild(aBorrar);
		}		
	}
</script>
</body>