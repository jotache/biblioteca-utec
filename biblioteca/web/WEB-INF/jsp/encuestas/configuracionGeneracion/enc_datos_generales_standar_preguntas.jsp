<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);
		
	fc_ini();	
}

function fc_ini(){
	
	numPreg = document.getElementById("txhNumPreguntas").value
	parent.fc_SetNumPreg(numPreg);
	document.getElementById("chkObligatorio").checked =true;
	document.getElementById("cboTipo").value  = "0002";
	
}

function validaTexto(nameObj,tamMax){
	
	fc_ValidaTextoEspecialEncuestas();
	
	if(document.getElementById(nameObj).value.length > Number(tamMax))
	{
		str = document.getElementById(nameObj).value.substring(0,Number(tamMax));
		document.getElementById(nameObj).value=str;
		return false;
	}			
	
}

function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtPregunta").value = "";
	document.getElementById("chkObligatorio").checked = false;	
	document.getElementById("cboTipo").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	//document.getElementById("txhCodigoSel").value = "";
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){
		
			document.getElementById("operacion").value="irRegistrar";						
			document.getElementById("frmMain").submit();
		}	
	}
}

function fc_Actualizar(){

	var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_mostrarDatos(posSel){

	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtPregunta").value =  document.getElementById("hidPregunta"+posSel).value ;	
	document.getElementById("cboTipo").value = document.getElementById("hidTipo"+posSel).value;
	
	indice = document.getElementById("hidIndUnica"+posSel).value;
	
	if(indice=="1")
		document.getElementById("radio1").checked = true;
	else	
		document.getElementById("radio2").checked = true;
		
	valCombo = document.getElementById("hidObligatorio"+posSel).value;
	if(valCombo=="1")
		document.getElementById("chkObligatorio").checked = true;
	else	
		document.getElementById("chkObligatorio").checked = false;
		
	Fc_CambiaTipo();
	 	
	
}

function fc_validaGrabar(){
	if(fc_Trim(document.getElementById("txtPregunta").value)=="")
	{	alert(mstrIngrNombre);
		document.getElementById("txtPregunta").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboTipo").value)==""){
		alert(mstrSeleccioneTipoEncuesta);
		document.getElementById("cboTipo").focus();
		return 0;
	}
		
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function Fc_CambiaTipo()
{
	tdTipoAbierta.style.display="none";
	
	if (document.getElementById("cboTipo").value=='0002')
	{
		tdTipoAbierta.style.display="";
	}
	else
	{
		tdTipoAbierta.style.display="none";
	}
}

function fc_Sel(pos){
	document.getElementById("txhPosSel").value = pos;
}

function fc_Eliminar(){
	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{
			numAltRel = document.getElementById("hidAlternativasRel"+posSel).value;
			
			if( Number(numAltRel)>0  ){
				alert(mstrTieneAltRelacionadas);
			}else{
			
			  if(confirm(mstrSeguroEliminar1))
			  {
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			  }
			  			
			}
			

		}
	else
		alert(mstrSeleccione);
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/datosGeneralesPreguntas.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<form:hidden path="txhCodGrupo" id="txhCodGrupo" />
	<form:hidden path="txhCodEstado" id="txhCodEstado" />	

	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />

	<!-- **************************************************************************************************** -->	
	<table class="tabla2" width="99%" cellSpacing="0" cellPadding="0"
		border="0" bordercolor='gray' ID="Table2" style="margin-left: 3px;height: 18px;">
		<tr>
			<td align="left" style='cursor: hand;'>PREGUNTAS</td>
		</tr>
	</table>					
	<!-- CABECERA -->
	<table background="${ctx}/images/biblioteca/fondosup.jpg"
		style="width: 99%;height: 60px;margin-bottom: 4px;margin-left: 3px;" border="0" cellspacing="1" cellpadding="1" class="tabla" bordercolor="red">
		<tr>
			<td class="">Pregunta:</td>
			<td>
			<form:textarea path="txtPregunta"
				id="txtPregunta" cssClass="cajatexto_o"
				 onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtPregunta')"  
				cssStyle="width:300px;height:27px;" onkeypress="validaTexto('txtPregunta','255')"/>	
				
			</td>

			<td><form:checkbox path="chkObligatorio" id="chkObligatorio" 
				value="1" /> Obligatorio</td>
			<td align=right>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/iconos/grabar2.jpg',1)">
				<img onclick="fc_Grabar()" src="${ctx}/images/iconos/grabar1.jpg" alt="Grabar" style="cursor:pointer;" id="imgGrabar">&nbsp;</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','${ctx}/images/iconos/cerrar2.jpg',1)">
				<img onclick="fc_Limpiar()" src="${ctx}/images/iconos/cerrar1.jpg" alt="Cancelar" style="cursor:pointer;" id="imgCerrar">&nbsp;&nbsp;</a></td>
		<tr>
			<td class="">Tipo:</td>
			<td><form:select path="cboTipo" id="cboTipo"
				cssClass="cajatexto_o" cssStyle="width:190px"
				onclick="Fc_CambiaTipo()">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.lstTipo!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="descripcion" items="${control.lstTipo}" />
				</c:if>
			</form:select></td>
			<!-- <td colspan="2" id="tdTipoAbierta" style="display: none;"> -->
			<td colspan="2" id="tdTipoAbierta" ><input
				type=radio id="radio1" value="1" name="radio" checked>Unico&nbsp;
			<input type=radio id="radio2" value="0" name="radio" disabled="disabled" >Múltiple</td>
		</tr>
	</table>

	<!-- VARIABEL CONTADORAS DE PREGUNTAS -->
	<c:set var="numPreg" value="0" /> 
		
		
	<!-- BANDEJA -->
	<table style="width: 99%; margin-top: 0px;margin-left: 3px;" cellSpacing="0" cellPadding="0" border="0" bordercolor="blue" ID="Table12">
		<tr>
			<td style="width: 95%;">					
				<table cellpadding="0" cellspacing="1" style="width:96.5%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
					<tr>
						<td class="grilla" style="width: 5%;">Sel.</td>
						<td class="grilla" style="width: 60%;">Pregunta</td>
						<td class="grilla" style="width: 10%;">Tipo</td>
						<td class="grilla" style="width: 15%;">Obligatorio</td>
						<td class="grilla" style="width: 10%;">Unica</td>						
					</tr>
				</table>
				<div style="overflow: auto; height: 190px;width:100%">
				<table cellpadding="0" cellspacing="1" style="width:96.5%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">			
					<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
					<c:set var="numPreg" value="${numPreg+1}" /> 
					<tr class="tablagrilla">
						<td style="width: 5%;">
							<input type="hidden"
							value="<c:out value="${objCast.codPregunta}" />"
							id="hidCodigo<c:out value="${loop.index}" />"
							name="hidCodigo<c:out value="${loop.index}" />" /> 
							
							<input
							type="hidden" value="<c:out value="${objCast.nomPregunta}" />"
							id="hidPregunta<c:out value="${loop.index}" />"
							name="hidPregunta<c:out value="${loop.index}" />" /> 
							
							<input
							type="hidden"
							value="<c:out value="${objCast.codTipoPregunta}" />"
							id="hidTipo<c:out value="${loop.index}" />"
							name="hidTipo<c:out value="${loop.index}" />" /> 
							
							<input
							type="hidden"
							value="<c:out value="${objCast.indObligatorio}" />"
							id="hidObligatorio<c:out value="${loop.index}" />"
							name="hidObligatorio<c:out value="${loop.index}" />" /> 
							
							<input
							type="hidden" value="<c:out value="${objCast.indUnica}" />"
							id="hidIndUnica<c:out value="${loop.index}" />"
							name="hidIndUnica<c:out value="${loop.index}" />" />
							
							<input
							type="hidden" value="<c:out value="${objCast.alternativasRel}" />"
							id="hidAlternativasRel<c:out value="${loop.index}" />"
							name="hidAlternativasRel<c:out value="${loop.index}" />" />
														
							<INPUT
							class="radio" type="radio" name="radio"
							onclick="fc_Sel('<c:out value="${loop.index}" />')">
						</td>
						<td style="width: 60%;">
							<textarea  readonly="readonly"  class="cajatexto_1" style="width:96%;height:30px;"><c:out value="${objCast.nomPregunta}" /></textarea>
						</td>
						<td style="width: 10%;"><c:out value="${objCast.nomTipoPregunta}" /></td>							
						<td align="center" style="width: 15%;"><c:if test="${objCast.indObligatorio=='1'}">Si</c:if><c:if test="${objCast.indObligatorio!='1'}">No</c:if></td>							
						<td align="center" style="width: 10%;"><c:if test="${objCast.indUnica=='1'}">Si</c:if><c:if test="${objCast.indUnica!='1'}">No</c:if></td>							
					</tr>
				</c:forEach>
			</table>
			</div>
			</td>
			<td align="right" valign="middle">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificar1','','${ctx}/images/iconos/modificar2.jpg',1)">
					<img onclick="fc_Actualizar()" src="${ctx}/images/iconos/modificar1.jpg" style="cursor: pointer" id="imgModificar1">
				</a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgQuitar','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img onclick="fc_Eliminar()" src="${ctx}/images/iconos/quitar1.jpg" style="cursor: pointer" id="imgQuitar">
				</a>
			</td>
		</tr>
	</table>	
	
	<!-- NUMERO D PREGUNTAS RELACIONADAS -->
	<input type="hidden" id="txhNumPreguntas" name="txhNumPreguntas" value="<c:out value="${numPreg}" />" /> 	
	<!-- **************************************************************************************************** -->
</form:form>
</body>


