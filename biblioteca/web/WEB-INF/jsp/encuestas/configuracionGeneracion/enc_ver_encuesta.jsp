<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.bean.DatosCabeceraBean'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
	var refresh = "<%=(( request.getAttribute("refresh")==null)?"": request.getAttribute("refresh"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
		
	if(refresh=="TRUE")
		parent.fc_refrescar();
}

function validaTexto(nameObj,tamMax){
 	
 	fc_ValidaTextoEspecialEncuestas();
 	
	if(document.getElementById(nameObj).value.length > Number(tamMax))
	{
		str = document.getElementById(nameObj).value.substring(0,Number(tamMax));
		document.getElementById(nameObj).value=str;
		return false;
	}			
	
}


function fc_Grabar(){
	if(fc_validaGrabar()){
		
		if(confirm(mstrSeguroGrabar)){
					
			fc_generaEnvio();
			document.getElementById("operacion").value  ="GRABAR";
			//alert(document.getElementById("operacion").value);
			//alert("codEncuestado>>"+document.getElementById("txhCodEncuestado").value+">");											
			document.getElementById("frmMain").submit();
		}	
	}
}

function fc_generaEnvio(){

	var tam 			= document.getElementById("txhTamanio").value;
	
	var tknCodPregunta  = "";	
	var tknCodRespuesta = "";
	var numPreg = 0;
	
	var tknCodPreguntaAbierta  = "";
	var tknCodRespuestaAbierta = "";
	var numPregAbierta = 0;
	 
	for ( var i = 0; i < Number(tam); i++)
	{
			//OBJETO ACTUAL
			obj = document.getElementById("txhOpciones"+i);
			tamanioOpciones = document.getElementById("txhTamanioOpciones"+i).value
			
			//tknCodPregunta = tknCodPregunta + document.getElementById("hidCodPregunta"+i).value + "|";
				
			if( document.getElementById("hidFlgCombo"+i).value == "1" )
			{	//COMBO PREGUNTAS CERRADAS
				numPreg = numPreg+1;
				tknCodPregunta = tknCodPregunta + document.getElementById("hidCodPregunta"+i).value + "|";
					
				if( document.getElementById("hidFlgMultiple"+i).value == "0" )
				{	//SIMPLE
					//if(fc_Trim(obj.value)!="")
					
					if(radioValue(i,tamanioOpciones)!="")
						tknCodRespuesta = tknCodRespuesta+radioValue(i,tamanioOpciones)+"|";
					else
						tknCodRespuesta = tknCodRespuesta+"x"+"|";
							
				}else
				{	//MULTIPLE
					//****************
					var countOcurrencias = 0;			
					
					for (var j = 0; j < tamanioOpciones; j++)
					{	
						//alert("obj>>>"+"txhOpciones"+i+""+j+">>>"+document.getElementById("txhOpciones"+i+""+j).checked);
						if ( document.getElementById("txhOpciones"+i+""+j).checked == true )
						{
								tknCodRespuesta = tknCodRespuesta+document.getElementById("txhOpciones"+i+""+j).value+"|";
								countOcurrencias = countOcurrencias + 1;
						}
					}
					
					if( countOcurrencias==0 )
						tknCodRespuesta = tknCodRespuesta+"x"+"|";
					
					//****************					
				}
				
				tknCodRespuesta = tknCodRespuesta.substring(0,tknCodRespuesta.length-1);
				tknCodRespuesta = tknCodRespuesta+"$";
				
			}else
			{	//TXT PREGUNTAS ABIERTAS
				numPregAbierta = numPregAbierta + 1;
				tknCodPreguntaAbierta = tknCodPreguntaAbierta + document.getElementById("hidCodPregunta"+i).value + "|";
			
				if(fc_Trim(obj.value)!="")
					tknCodRespuestaAbierta = tknCodRespuestaAbierta+fc_Trim(obj.value)+"|";
				else
					tknCodRespuestaAbierta = tknCodRespuestaAbierta+"x"+"|";
				
				tknCodRespuestaAbierta = tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1);	
				tknCodRespuestaAbierta = tknCodRespuestaAbierta+"$";//SEPARACION DE REGISTROS				
			}
		
		
					
	}	
		document.getElementById("tknCodPreguntaCerrada").value = tknCodPregunta.substring(0,tknCodPregunta.length-1);
		document.getElementById("tknCodRespuestaCerrada").value = tknCodRespuesta.substring(0,tknCodRespuesta.length-1);
		document.getElementById("tknNumPregCerrada").value = numPreg;

		document.getElementById("tknCodPreguntaAbierta").value = tknCodPreguntaAbierta.substring(0,tknCodPreguntaAbierta.length-1);
		document.getElementById("tknCodRespuestaAbierta").value = tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1);
		document.getElementById("tknNumPregAbierta").value = numPregAbierta;
		
		
	/*alert("tknCodPreguntaCerrada>"+tknCodPregunta.substring(0,tknCodPregunta.length-1)+">");
	alert("tknCodRespuestaCerrada>"+tknCodRespuesta.substring(0,tknCodRespuesta.length-1)+">");
	alert("numPregCerrada>"+numPreg+">");	
	
	alert("tknCodPreguntaAbierta>"+tknCodPreguntaAbierta.substring(0,tknCodPreguntaAbierta.length-1)+">");
	alert("tknCodRespuestaAbierta>"+tknCodRespuestaAbierta.substring(0,tknCodRespuestaAbierta.length-1)+">");
	alert("numPregAbierta>"+numPregAbierta+">");*/	

}

/*retorna el valor de un radio si esta seleccionado sino retorna "" */
function radioValue(fila,tamanioOpciones){
	var xvalue = "";
	
	for ( var i = 0; i < Number(tamanioOpciones); i++)
	{
	 	if(document.getElementById("txhOpciones"+fila+""+i).checked == true)
	 	 xvalue = document.getElementById("txhOpciones"+fila+""+i).value;
	 	
	}
	
	return xvalue;
}


function fc_validaGrabar(){
	
	var tam = document.getElementById("txhTamanio").value;
	if(Number(tam)<1){
		alert("No hay registros para grabar");
		return 0;
	}	
	
	 for ( var i = 0; i < Number(tam); i++)
	 {
		if( document.getElementById("hidFlgObligatorio"+i).value == "1" && fc_Trim(document.getElementById("hidFlgCombo"+i).value) == "0" )
		{	//var texto = fc_Trim(document.getElementById("opc"+i).value) == "";
			if( fc_Trim(document.getElementById("txhOpciones"+i).value) == "" ){
				alert("Debe llenar todos los campos obligatorios");
				document.getElementById("txhOpciones"+i).focus();
				return 0;
			}
		}else if(  document.getElementById("hidFlgObligatorio"+i).value == "1" && fc_Trim(document.getElementById("hidFlgCombo"+i).value) == "1"  ){
			//RECORRE SUS MICROOPCIONES
			//tamanio es el tama�o de las opciones
			//i es la posicion de la fila o de la pregunta			
			tamanio = document.getElementById("txhTamanioOpciones"+i).value;
			var flgSeleccionado = fc_validaCheckedSeleccionado(i,tamanio);
			
			if( flgSeleccionado == 0 ){
				alert("Debe llenar todos los campos obligatorios");
				//document.getElementById("opc"+i).focus();
				return 0;		
			}
			
		}
	 }
			
	return 1;
}

function fc_validaCheckedSeleccionado(fila,tamanioOpciones){
	 
	 for ( var i = 0; i < Number(tamanioOpciones); i++)
	 {	 	
	 	if(document.getElementById("txhOpciones"+fila+""+i).checked == true)
	 	return 1;
	 	
	 }

	return 0;
}

function fc_funcionInnerIdTexto(id,texto){
	if(texto!="")
	document.getElementById(""+id).innerHTML = texto;	
}

</script>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/verEncuestas.html">
	
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<!-- CARACTERISTICAS DE LA ENCUESTA -->
	<form:hidden path="txhCodPerfil" id="txhCodPerfil" />
	<form:hidden path="txhCodEncuestado" id="txhCodEncuestado" />
	<form:hidden path="txhCodProfesor" id="txhCodProfesor" />
	

	<input type="hidden" name="tknCodPreguntaCerrada" id="tknCodPreguntaCerrada" />
	<input type="hidden" name="tknCodRespuestaCerrada" id="tknCodRespuestaCerrada" />
	<input type="hidden" name="tknNumPregCerrada" id="tknNumPregCerrada" />
	<input type="hidden" name="tknCodPreguntaAbierta" id="tknCodPreguntaAbierta" />
	<input type="hidden" name="tknCodRespuestaAbierta" id="tknCodRespuestaAbierta" />
	<input type="hidden" name="tknNumPregAbierta" id="tknNumPregAbierta" />
	
	<%int varTamanio = 0; %>
	
	<%DatosCabeceraBean bean= (DatosCabeceraBean) request.getAttribute("OBJ_CABECERA"); %>
	<%if(bean!=null ){ %>	
	<table cellpadding="0" cellspacing="2" 
		style="width:97%;margin-left: 6px;margin-top: 0px; margin-bottom:5px;border: 1px solid #048BBA">
		<TR class="tablagrilla" style="height: 20px;">	
			<TD style="width: 20%;"><b>&nbsp;Encuesta :</b></TD>
			<TD style="width: 40%;">&nbsp;<%=bean.getNomEncuesta()%></TD>
			<TD style="width: 20%;"><b>&nbsp;Duracion :</b></TD>
			<TD style="width: 20%;">&nbsp;<%=bean.getDuracion()%>&nbsp;Min</TD>
		</TR>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0002".equalsIgnoreCase(bean.getCodTipoAplicacion()) ){%>
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado a :</b></TD>
			<TD>&nbsp;<%=bean.getNomServicio()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0001".equalsIgnoreCase(bean.getCodTipoAplicacion()) ){%>
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado al profesor :</b></TD>
			<TD>&nbsp;<%=bean.getNomProfesor()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
	</table>	
	<%}%>
	
	
	<%int numSeccion = 0; %>	
	<table cellpadding="0" cellspacing="2" 
	style="width:97%;margin-left: 6px;margin-bottom:5px;border: 1px solid #048BBA">

	<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{
		
		List consulta = (List) request.getAttribute("LST_RESULTADO");	
		varTamanio = consulta.size();
		
		String guia = "";
		String grupoVariable = "";
		String formatoVariable = "";
		VerEncuestaBean objFuturo =null;
		String grupoFutura = "";
		String formatoFutura = "";
		String preguntaVariable = "";
		String preguntaFutura = "";
		
		for (int i = 0; i < consulta.size(); i++)	
		{		
			VerEncuestaBean obj = (VerEncuestaBean) consulta.get(i);		 	
			grupoVariable = obj.getIdGrupo();
			formatoVariable = obj.getIdSeccion();
			preguntaVariable = obj.getIdPregunta();
%>

		<!--**************** PINTAMOS EL INICIO******************* -->
		<%if( i==0 ){%>
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1" style="width: 40%;">&nbsp;(*): Preguntas de caracter obligatorio.</td>
				</tr>
				<tr class="tablagrilla" align="left" style="height: 20px;">
					<td id="td0" colspan="<%=obj.getLstAlternativas().size()%>">					
					<!-- delete -->	
					</td>
				</tr>
			</table>
			<div style="overflow: auto; height: 330px;width:100%">
			<table cellpadding="0" cellspacing="2" 
				style="width:97%;margin-left: 6px;margin-bottom:5px;border: 1px solid #048BBA">				
				<tr class="grilla" style="height: 20px;">
					<td colspan="1" style="width: 40%;" align="left">&nbsp;<b><%=obj.getNomSeccion()%></b></td>
					<td id="tdFormato<%=numSeccion%>" style="width: 60%;">
					</td>
				</tr>
				<tr class="tablagrilla" style="height: 20px;">
					<td bgcolor="#C8E8F0" colspan="2"><b>&nbsp;&nbsp;&nbsp;<%=obj.getNomGrupo()%></b></td>				
				</tr>
				<% numSeccion++; %>	
		<%}%>
		<!--**************** FIN PINTAMOS EL INICIO******************* -->
		
		
		<%if( i+1<consulta.size() )
		{	objFuturo = (VerEncuestaBean) consulta.get(i+1);
			grupoFutura = objFuturo.getIdGrupo();
			formatoFutura = objFuturo.getIdSeccion();
			preguntaFutura = objFuturo.getIdPregunta();%>
			
			<!--***************************** PINTA LAS PREGUNTA CON SUS CARACTERISTICAS *****************************-->
			<tr class="tablagrilla">
					<input type="hidden" id="hidFlgObligatorio<%=i%>"  value="<%=obj.getIndObligatorio()%>"  />
					<input type="hidden" id="hidFlgCombo<%=i%>"  		value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
					<input type="hidden" id="hidFlgMultiple<%=i%>"  	value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
					<input type="hidden" id="hidCodPregunta<%=i%>"  	value="<%=obj.getIdPregunta()%>"  />
					<input type="hidden" id="txhTamanioOpciones<%=i%>"  	value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  />
								
					<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>					 
					
					<!-- QUE TIPO DE PREGUNA ES ABIERTA O CERRADA -->
					<td>
					
					<table width="100%" height="100%" bordercolor="red" border="0" cellpadding="0" cellspacing="0"><tr>										
					<%if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>
						<td colspan="<%=obj.getLstAlternativas().size()%>">
							<textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>"    
							 onkeypress="validaTexto('txhOpciones<%=i%>','400')"
							 onblur="fc_ValidaTextoEspecialEncuestasOnblur('txhOpciones<%=i%>')"  
							class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto"%>" style="width:98%;height:50px;"></textarea>
						</td>
					<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>
				
						<%for (int j = 0; j < obj.getLstAlternativas().size(); j++)
						{
							VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);%>				
							<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
								<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
								<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}%>
					
						<%}%>
					
					<%}%>
					</tr>
					</table>
					
					</td>
										
			</tr>
			<!--***************************** FIN PINTA LAS PREGUNTA *****************************-->
				
			<%
			//PINTA LAS CABECERAS SIGUIENTES
			if(!formatoVariable.equalsIgnoreCase(formatoFutura)){%>
				<tr class="grilla" style="height: 20px;">
					<td colspan="1" align="left">&nbsp;<b><%=objFuturo.getNomSeccion()%></b></td>
					<td id="tdFormato<%=numSeccion%>">
						<!-- delete fromato -->
					</td>
					<% numSeccion++; %>					
				</tr>
				<tr class="tablagrilla" style="height: 20px;">
					<td bgcolor="#C8E8F0" colspan="2"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>			
				</tr>
			<%
			}else if(!grupoVariable.equalsIgnoreCase(grupoFutura))
			{/**PARA LOS QUIEBRES**/%>
				<tr class="tablagrilla" style="height: 20px;">
					<td bgcolor="#C8E8F0" colspan="2"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>					
				</tr>
			<%						
			}else if( formatoVariable.equalsIgnoreCase(formatoFutura) ){%>
				
			<% }%>
				<script>fc_funcionInnerIdTexto('tdFormato<%=numSeccion-1%>','<%=MetodosConstants.getNomAlternativas(obj)%>')</script>
				<script>fc_funcionInnerIdTexto('td0','<%=MetodosConstants.getLeyendaStandar(obj)%>')</script>
			<%
			//PINTA LAS CABECERAS SIGUIENTES
			
		}
		///************* FIN PINTAMOS LOS REGISTROS INTERMEDIOS ***************
		
		
		
		///*************PINTAMOS EL ULTIMO REGISTRO*************** 
		if( i+1==consulta.size()){
		%>
		
		<!--***************************** PINTA LAS PREGUNTA *****************************-->
			<tr class="tablagrilla">
					<input type="hidden" id="hidFlgObligatorio<%=i%>"  value="<%=obj.getIndObligatorio()%>"  />
					<input type="hidden" id="hidFlgCombo<%=i%>"  		value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
					<input type="hidden" id="hidFlgMultiple<%=i%>"  	value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
					<input type="hidden" id="hidCodPregunta<%=i%>"  	value="<%=obj.getIdPregunta()%>"  />
					<input type="hidden" id="txhTamanioOpciones<%=i%>"  	value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  />
								
					<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>										
					<td>
					<table width="100%" height="100%" bordercolor="red" border="0" cellpadding="0" cellspacing="0"><tr>
					<%if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>
						<td colspan="<%=obj.getLstAlternativas().size()%>">
						<textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>"    
						 onkeypress="validaTexto('txhOpciones<%=i%>','50')"   
						 onblur="fc_ValidaTextoEspecialEncuestasOnblur('txhOpciones<%=i%>')"   
						class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto"%>"   style="width:98%;height:50px;"></textarea>
						</td>
					<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>				
						<%/**FOR**/				
						for (int j = 0; j < obj.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
						%>				
							<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
							<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
							<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}%>					
						<%}
					/**FOR**/%>					
					<%}%>
					</tr>
					</table>
					<td>
				
				
			</tr>	
			<script>fc_funcionInnerIdTexto('tdFormato<%=numSeccion-1%>','<%=MetodosConstants.getNomAlternativas(obj)%>')</script>
			<script>fc_funcionInnerIdTexto('td0','<%=MetodosConstants.getLeyendaStandar(obj)%>')</script>				
		<%}
		
		
	}/*FOR*/		
  }/*IF*/
%>
</table>
</div>

	<table width="50%" border="0" align="center" style="margin-top: 10px">
		<tr>
			<td align="center">		
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviar','','/SGA/images/botones/enviar2.jpg',1)">
					<img src="/SGA/images/botones/enviar1.jpg" onclick="javascript:fc_Grabar();" style="cursor:pointer;" id="imgEnviar" alt="Enviar">
				</a>
			</td>
		</tr>
	</table>
<!--CANTIDAD DE PREGUNTAS-->
<input type="hidden" name="txhTamanio" id="txhTamanio" value="<%=varTamanio%>" />
</form:form>
</body>


