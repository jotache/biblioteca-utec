<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@page import='com.tecsup.SGA.bean.DatosCabeceraBean'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@ include file="/taglibs.jsp"%>
<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
}

function fc_funcionInnerIdTexto(id,texto){
	if(texto!="")
	document.getElementById(""+id).innerHTML = texto;	
}

</script>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/visualizarEncuestaEstandar.html">	
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<!-- CARACTERISTICAS DE LA ENCUESTA -->
	<form:hidden path="txhCodPerfil" id="txhCodPerfil" />
	<form:hidden path="txhCodEncuestado" id="txhCodEncuestado" />	

	<input type="hidden" name="tknCodPreguntaCerrada" id="tknCodPreguntaCerrada" />
	<input type="hidden" name="tknCodRespuestaCerrada" id="tknCodRespuestaCerrada" />
	<input type="hidden" name="tknNumPregCerrada" id="tknNumPregCerrada" />
	<input type="hidden" name="tknCodPreguntaAbierta" id="tknCodPreguntaAbierta" />
	<input type="hidden" name="tknCodRespuestaAbierta" id="tknCodRespuestaAbierta" />
	<input type="hidden" name="tknNumPregAbierta" id="tknNumPregAbierta" />
	
	<%int varTamanio = 0; %>
		<table width="98%" border="0" align="center" style="margin-top: 10px" cellpadding="0" cellspacing="0" bordercolor="red">
		<tr>
			<td align="right">				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','/SGA/images/iconos/cerrar2.jpg',1)">
				<img src="/SGA/images/iconos/cerrar1.jpg" onclick="javascript:window.close();" style="cursor:pointer;" id="imgCerrar" alt="Cerrar Ventana"></a>
			</td>
		</tr>
	</table>
	<%DatosCabeceraBean bean= (DatosCabeceraBean) request.getAttribute("OBJ_CABECERA"); %>
	<%if(bean!=null ){ %>		
	<table cellpadding="0" cellspacing="2" align="center" 
		style="width:98%;margin-top: 8px; margin-bottom:5px;border: 1px solid #048BBA">
		<TR class="tablagrilla" style="height: 20px;">
			<TD id="prueba" style="width: 20%;"><b>&nbsp;Tipo Aplicación :</b></TD>
			<TD style="width: 40%;">&nbsp;<%=bean.getNomTipoAplicacion()%></TD>
			<TD style="width: 20%;"><b>&nbsp;Tipo Encuesta :</b></TD>
			<TD style="width: 20%;">&nbsp;<%=bean.getNomTipoEncuesta()%></TD>
		</TR>
		<TR class="tablagrilla" style="height: 20px;">
			<TD style="width: 20%;"><b>&nbsp;Encuesta :</b></TD>
			<TD style="width: 40%;">&nbsp;<%=bean.getNomEncuesta()%></TD>
			<TD style="width: 20%;"><b>&nbsp;Duracion :</b></TD>
			<TD style="width: 20%;">&nbsp;<%=bean.getDuracion()%>&nbsp;Min</TD>
		</TR>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0002".equalsIgnoreCase(bean.getCodTipoAplicacion()) ){%>
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado a :</b></TD>
			<TD>&nbsp;<%=bean.getNomServicio()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0001".equalsIgnoreCase(bean.getCodTipoAplicacion()) && bean.getNomProfesor()!=null && !bean.getNomProfesor().equals("")){%>		
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado al profesor:</b></TD>
			<TD>&nbsp;<%=bean.getNomProfesor()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
	</table>	
	<%}%>

<c:set value="0" var="numSeccion" />
<%int numSeccion = 0; %>
	
<table cellpadding="0" cellspacing="2" align="center" 
	style="width:98%;margin-bottom:5px;border: 1px solid #048BBA">

	<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{
		
		List consulta = (List) request.getAttribute("LST_RESULTADO");	
		varTamanio = consulta.size();
		
		String guia = "";
		String grupoVariable = "";
		String formatoVariable = "";
		VerEncuestaBean objFuturo =null;
		String grupoFutura = "";
		String formatoFutura = "";
		String preguntaVariable = "";
		String preguntaFutura = "";
		
		for (int i = 0; i < consulta.size(); i++)	
		{		
			VerEncuestaBean obj = (VerEncuestaBean) consulta.get(i);			
			grupoVariable = obj.getIdGrupo();
			formatoVariable = obj.getIdSeccion();
			preguntaVariable = obj.getIdPregunta();
%>

		<!--**************** PINTAMOS EL INICIO******************* -->
		<%if( i==0 ){%>
		
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;(*): Preguntas de caracter obligatorio.</td>
				</tr>
				<tr class="tablagrilla" align="left" style="height: 20px;">
					<td id="td0" colspan="<%=obj.getLstAlternativas().size()%>">	
						<!-- delete -->						
					</td>
				</tr>
			</table>
						
			<table cellpadding="0" cellspacing="2" align="center" 
				style="width:98%;margin-bottom:5px;border: 1px solid #048BBA">				
				<tr class="grilla" style="height: 20px;">
				
					<td colspan="1" style="width: 40%;" align="left">&nbsp;<b><%=obj.getNomSeccion()%></b></td>

					<td id="tdFormato<%=numSeccion%>" style="width: 60%;">
						
					</td>
										
				</tr>
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="2" bgcolor="#C8E8F0"><b>&nbsp;&nbsp;&nbsp;<%=obj.getNomGrupo()%></b></td>				
				</tr>				
				<% numSeccion++; %>		
		<%}%>
		<!--**************** FIN PINTAMOS EL INICIO******************* -->
		
		
		<%if( i+1<consulta.size() )
		{	objFuturo = (VerEncuestaBean) consulta.get(i+1);
			grupoFutura = objFuturo.getIdGrupo();
			formatoFutura = objFuturo.getIdSeccion();
			preguntaFutura = objFuturo.getIdPregunta();%>
			
			<!--***************************** PINTA LAS PREGUNTA CON SUS CARACTERISTICAS *****************************-->
			<tr class="tablagrilla">
					<input type="hidden" id="hidFlgObligatorio<%=i%>"  value="<%=obj.getIndObligatorio()%>"  />
					<input type="hidden" id="hidFlgCombo<%=i%>"  		value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
					<input type="hidden" id="hidFlgMultiple<%=i%>"  	value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
					<input type="hidden" id="hidCodPregunta<%=i%>"  	value="<%=obj.getIdPregunta()%>"  />
					<input type="hidden" id="txhTamanioOpciones<%=i%>"  	value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  />
								
					<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>					 
					
					<!-- QUE TIPO DE PREGUNA ES ABIERTA O CERRADA -->
					<td>
					
					<table width="100%" height="100%" bordercolor="red" border="0" cellpadding="0" cellspacing="0"><tr>										
					<%if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>
						<td colspan="<%=obj.getLstAlternativas().size()%>">
							<textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>"    class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto"%>" style="width:99%;height:50px;"></textarea>
						</td>
					<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>
				
						<%for (int j = 0; j < obj.getLstAlternativas().size(); j++)
						{
							VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);%>				
							<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
								<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
								<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}%>
					
						<%}%>
					
					<%}%>
					</tr>
					</table>
					
					</td>
										
			</tr>
			<!--***************************** FIN PINTA LAS PREGUNTA *****************************-->
				
			<%
			//PINTA LAS CABECERAS SIGUIENTES
			if(!formatoVariable.equalsIgnoreCase(formatoFutura)){%>
				<tr class="grilla" style="height: 20px;">
					
					<td colspan="1" align="left">&nbsp;<b><%=objFuturo.getNomSeccion()%></b></td>
					
					
					
					<td id="tdFormato<%=numSeccion%>">
						<!-- delete fromato -->						
						

					</td>					
					<% numSeccion++; %>			
				</tr>
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="2" bgcolor="#C8E8F0"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>			
				</tr>
			<%
			}else if(!grupoVariable.equalsIgnoreCase(grupoFutura))
			{/**PARA LOS QUIEBRES**/%>
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="2" bgcolor="#C8E8F0"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>					
				</tr>
			<%						
			}else if( formatoVariable.equalsIgnoreCase(formatoFutura) ){%>
				
			<%}%>
			<script>fc_funcionInnerIdTexto('tdFormato<%=numSeccion-1%>','<%=MetodosConstants.getNomAlternativas(obj)%>')</script>
			<script>fc_funcionInnerIdTexto('td0','<%=MetodosConstants.getLeyendaStandar(obj)%>')</script>
			<%
			//PINTA LAS CABECERAS SIGUIENTES
			
		}
		///************* FIN PINTAMOS LOS REGISTROS INTERMEDIOS ***************
		
		
		
		///*************PINTAMOS EL ULTIMO REGISTRO*************** 
		if( i+1==consulta.size()){
		%>
		
		<!--***************************** PINTA LAS PREGUNTA *****************************-->
			<tr class="tablagrilla">
					<input type="hidden" id="hidFlgObligatorio<%=i%>"  value="<%=obj.getIndObligatorio()%>"  />
					<input type="hidden" id="hidFlgCombo<%=i%>"  		value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
					<input type="hidden" id="hidFlgMultiple<%=i%>"  	value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
					<input type="hidden" id="hidCodPregunta<%=i%>"  	value="<%=obj.getIdPregunta()%>"  />
					<input type="hidden" id="txhTamanioOpciones<%=i%>"  	value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  />
								
					<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>										
					<td>
					<table width="100%" height="100%" bordercolor="red" border="0" cellpadding="0" cellspacing="0"><tr>
					<%if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>
						<td colspan="<%=obj.getLstAlternativas().size()%>">
						<textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>"    class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto"%>"   style="width:99%;height:50px;"></textarea>
						</td>
					<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>				
						<%/**FOR**/				
						for (int j = 0; j < obj.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
						%>				
							<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
							<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
							<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
							<%}%>					
						<%}
					/**FOR**/%>					
					<%}%>
					</tr>
					</table>
					<td>
				
				
			</tr>
			<!-- ULTIMO REGISTRO -->
			<script>fc_funcionInnerIdTexto('tdFormato<%=numSeccion-1%>','<%=MetodosConstants.getNomAlternativas(obj)%>')</script>
			<script>fc_funcionInnerIdTexto('td0','<%=MetodosConstants.getLeyendaStandar(obj)%>')</script>					
		<%}		
		
	}/*FOR*/		
  }/*IF*/
%>
</table>	
	
<!--CANTIDAD DE PREGUNTAS-->
<input type="hidden" name="txhTamanio" id="txhTamanio" value="<%=varTamanio%>" />
</form:form>
</body>


	