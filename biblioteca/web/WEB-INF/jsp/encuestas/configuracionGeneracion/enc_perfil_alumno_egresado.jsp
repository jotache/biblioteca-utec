<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
	function onLoad(){
		objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK_GRABAR" ){
			//PARA REFRESCAR EL TOTAL DE ENCUESTADOS DEL PADRE
			totalEncuestados=document.getElementById("txhTotalEncuestados").value;
			dscEncuestados=document.getElementById("txhDscEncuestados").value;
			parent.fc_ActualizarTotalEncuestado(totalEncuestados, dscEncuestados);
			//************************		
			alert(mstrGrabar);
			//alert(parent.document.getElementById("txhCodTipoAplicacion").value);
			codTipoAplicacion=parent.document.getElementById("txhCodTipoAplicacion").value;
			if(codTipoAplicacion==document.getElementById("txhEncuestaPrograma").value){
				parent.document.getElementById("txhCodPerfilEncuesta").value=document.getElementById("txhPerfilEGRESADO").value;
				parent.fc_OcultarPerfiles(document.getElementById("txhPerfilEGRESADO").value);
			}
			else
			if(codTipoAplicacion==document.getElementById("txhEncuestaSeccion").value){
				parent.document.getElementById("txhCodPerfilEncuesta").value=document.getElementById("txhCodPerfilEncuesta").value;
			}
		}		
		else
		if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);			
		}
		else
		if ( objMsg.value != 'null' &&  objMsg.value != ""){
			alert(document.getElementById("txhDscMensaje").value)
		}
		document.getElementById("txhMsg").value="";
		document.getElementById("txhOperacion").value="";
	}	
	function fc_Grabar(){
		if(fc_ValidaA�o()){
			if(confirm(mstrSeguroGrabar)){
				fc_oculta();
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
	}
	function fc_oculta(){
	
		document.getElementById("divajax").style.display = "";;		
		document.getElementById("divtabla").style.display = "none";
		
	}	
	function fc_ValidaA�o(){
		a�oIngreso=document.getElementById("txtA�oIngreso").value;
		a�oEgreso=document.getElementById("txtA�oEgreso").value;
		if(a�oIngreso == ""){
			alert("Debe ingresar un a�o de Ingreso.");
			document.getElementById("txtA�oIngreso").focus();
			return false;
		}
		if(a�oEgreso == ""){
			alert("Debe ingresar un a�o de Egreso.");
			document.getElementById("txtA�oEgreso").focus();
			return false;
		}
		if(fc_ValidaAnio('txtA�oIngreso')!=true){
			alert('El a�o no es v�lido');
			document.getElementById("txtA�oIngreso").value="";
			document.getElementById("txtA�oIngreso").focus();
			return false;

		}
		if(fc_ValidaAnio('txtA�oEgreso')!=true){
			alert('El a�o no es v�lido');
			document.getElementById("txtA�oEgreso").value="";
			document.getElementById("txtA�oEgreso").focus();
			return false;

		}
		if(a�oIngreso > a�oEgreso){
			alert("El a�o de inicio debe ser menor o igual al a�o fin ");
			document.getElementById("txtA�oIngreso").value="";
			document.getElementById("txtA�oIngreso").focus();
			return false;
		}
		return true;
	}	
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/perfilAlumnoEgresado.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="dscMensaje" id="txhDscMensaje" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codTipoEncuesta" id="txhCodTipoEncuesta" />
	
	<form:hidden path="codEncuesta" id="txhCodEncuesta" />
	<form:hidden path="codPerfilEncuesta" id="txhCodPerfilEncuesta" />
	<!-- CONSTANTES -->
	<form:hidden path="encuestaPrograma" id="txhEncuestaPrograma" />
	<form:hidden path="encuestaSeccion" id="txhEncuestaSeccion" />
	<form:hidden path="perfilEGRESADO" id="txhPerfilEGRESADO" />
	
	<form:hidden path="totalEncuestados" id="txhTotalEncuestados" />
	<form:hidden path="dscEncuestados" id="txhDscEncuestados" />
	<!-- TABLA PFR -->	
	<DIV id="divajax" align="center" class="texto_bold" style="display:none" ><br><br><br><br><br><br><br><br>Espere...<br><img src="${ctx}/images/ajaxloader.gif"><br>Guardando Informaci�n<br></DIV>	
	<DIV id="divtabla"  align="left" >	
	<table background="${ctx}/images/biblioteca/fondosup.jpg" class="tabla" style="width:97%;margin-left:9px;"
		 cellspacing="6" cellpadding="0" border="0" bordercolor="red" id="tablaEGRESADOS">
		<tr>
			<td width="10%">Programa PFR :</td>
			<td width="80%">
				<form:select path="codProgramaEgresado" id="cboProgramaEgresado" cssStyle="width:260px" cssClass="cajatexto_o">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaProgramaEgresado != null}">
						<form:options itemValue="codPrograma" itemLabel="nomProducto"						 
							items="${control.listaProgramaEgresado }" />
					</c:if>
				</form:select>
			</td>
			<td width="10%" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar03','','${ctx}/images/iconos/grabar2.jpg',1)">
					<img src="${ctx}/images/iconos/grabar1.jpg" style="cursor:pointer;" id="imgGrabar03" onclick="fc_Grabar();" 
					alt="Grabar">
				</a>
			</td>
		</tr>
		<tr>
			<td>A�o de Egreso :
			</td>
			<td>
				<form:input path="a�oIngreso" id="txtA�oIngreso"
					onkeypress="fc_ValidaNumero();"
					onblur="fc_ValidaNumeroFinal(this,'a�o Ingreso');"
					maxlength="4" cssClass="cajatexto" size="4"/>&nbsp;-&nbsp;
				<form:input path="a�oEgreso" id="txtA�oEgreso"
					onkeypress="fc_ValidaNumero();"
					onblur="fc_ValidaNumeroFinal(this,'a�o Egreso');"
					maxlength="4" cssClass="cajatexto" size="4"/>
			</td>
		</tr>
	</table>
	</DIV>	
</form:form>
</body>