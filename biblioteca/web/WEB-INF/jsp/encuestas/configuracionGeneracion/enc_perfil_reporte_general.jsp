<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Alumno'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td><img src="${ctx}/images/logoTecsup.jpg">
	</td>
	<td colspan="7" align="center"><u>SGA - SISTEMA DE ENCUESTAS</u></td>
	<td align="left">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td>
	</td>
	<td colspan="7" align="center">${model.titulo}</td>
	<td align="left">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="7" align="left"></td>
	<td></td>
</tr>
<tr >
	<td></td>
	<td class="texto_bold">Tipo Aplicación :</td>
	<td class="texto">
		${model.tipoAplicacion}	
	</td>
	<td class="texto_bold">Tipo Encuesta :</td>
	<td class="texto">
		${model.tipoEncuesta}
	</td>
	<td class="texto_bold">Sede :</td>
	<td colspan="2" class="texto">
		${model.sede}
	</td>	
</tr>
<tr>
	<td></td>
	<td class="texto_bold">Nro. Encuesta :</td>
	<td class="texto">
		${model.nroEncuesta}
	</td>
	<td class="texto_bold">Nombre Encuesta :</td>
	<td class="texto">
		${model.nomEncuesta}
	</td>
	<td class="texto_bold">Duración :</td>
	<td colspan="2" class="texto" align="left">
		${model.duracion}&nbsp;Min.	
	</td>
</tr>
<tr>
	<td></td>
	<%
	System.out.println("codTipoAplicacion en reporte>>"+request.getSession().getAttribute("codTipoAplicacion"));
	String codigoAplicacion=(String)request.getSession().getAttribute("codTipoAplicacion");
	if(codigoAplicacion!=null){
		if(codigoAplicacion.equals("0002")){%>
			<td class="texto_bold">Tipo Servicio :</td>
			<td class="texto">
				${model.tipoServicio}
			</td>
		<%}
	}
	request.getSession().removeAttribute("codTipoAplicacion");
	%>	
	<td class="texto_bold">Indicador Manual :</td>
	<td class="texto">
	<%
	String indManual=(String)request.getSession().getAttribute("indManual");
	if(indManual!=null){
		if(indManual.equals("0")){
		%>
			No
		<%}
		else{%>
			Si
		<%}
	}
	request.getSession().removeAttribute("indManual");
	%>
	</td>	
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="40%" colspan="5">Nombres</td>				
				<td class="cabecera_grilla" width="20%" colspan="2">Correo</td>								
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
				List consulta = (List) request.getSession().getAttribute("consulta"); 
					for(int i=0;i<consulta.size();i++){
					
						Alumno obj  = (Alumno) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:left" colspan="5"><%=obj.getNombreAlumno()==null?"":obj.getNombreAlumno()%></td>						
						<td style="text-align:left" colspan="2"><%=obj.getCorreo()==null?"":obj.getCorreo()%></td>												
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>