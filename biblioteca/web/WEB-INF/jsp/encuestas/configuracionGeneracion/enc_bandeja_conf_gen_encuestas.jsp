<%@ include file="/taglibs.jsp"%>
<style>
.tablagrilla02
{
    BACKGROUND-COLOR: #CEE3F6;
	FONT-SIZE: 8pt;
    /*TEXT-TRANSFORM: capitalize;*/
    WIDTH: 100%;
    FONT-FAMILY: Arial;
    color: #606060;
    
}
</style>
<head>
<script language=javascript>
	function onLoad(){
		fc_TipoAplicacion();
		//PARA DEFINIR LOS DAOSDEL USUARIO OEL PERFIL
		
		//MENSAJES DE LAS OPERACIONES
		var msg=document.getElementById("txhMsg");
		if(msg.value=="ERROR_QUITAR"){
			alert("Problemas al eliminar registro consulte con el Administrador");
			fc_Buscar();			
		}		
		else
		if(msg.value=="OK_QUITAR"){
			alert("Se elimino el registro correctamente.");
			fc_Buscar();
		}
		else
		if(msg.value=="ESTADO_INCORRECTO"){
			alert(document.getElementById("txhDscMensaje").value);
			fc_Buscar();
		}
		else
		if(msg.value=="OK_COPIAR"){
			alert("La Encuesta se copi� con �xito.");
			fc_Buscar();
		}	
		else
		if(msg.value=="ERROR_COPIAR"){
			alert("Error al generar la copia del formato de la encuesta");
			fc_Buscar();
		}
		else
		if(msg.value=="ERROR_GENERAR_COPIA"){
			alert(document.getElementById("txhDscMensaje").value);
			fc_Buscar();
		}	
		//***************************
		document.getElementById("txhOperacion").value="";
		document.getElementById("txhMsg").value="";
		document.getElementById("txhCodSelec").value="";
		
		var objBuscar = "<%=((request.getAttribute("iFrameBandeja")==null)?"":(String)request.getAttribute("iFrameBandeja"))%>";
		
		if(objBuscar=="OK"){
			fc_Buscar();
		}
	}	
	function fc_Regresar(){
		document.location.href = "${ctx}/menuEncuesta.html"
	}
	function fc_Buscar(){
		/*document.getElementById("txhOperacion").value = "BUSCAR";
	  	document.getElementById("frmMain").submit();*/
	  	document.getElementById("txhBandera").value="1";
	  	
	  	document.getElementById("iFrameBandejaEncuesta").src="${ctx}/encuestas/bandeja_conf_gen_encuestas_iframe.html?"+
	  	"txhCodUsuario="+document.getElementById("txhCodUsuario").value +
		"&txhParCodTipoAplicacion=" + document.getElementById("codTipoAplicacion").value +
		"&txhParCodTipoEncuesta=" + document.getElementById("codTipoEncuesta").value +
		"&txhParCodTipoServicio=" + document.getElementById("codTipoServicio").value +
		"&txhParCodTipoEstado=" + document.getElementById("codTipoEstado").value +
		"&txhParBandera=" + document.getElementById("txhBandera").value ;
	}
	
	function fc_Limpiar(){
		document.getElementById("codTipoAplicacion").value="-1";
		document.getElementById("codTipoEncuesta").value="-1";
		document.getElementById("codTipoServicio").value="-1";
		document.getElementById("codTipoEstado").value="-1";
		fc_TipoAplicacion();
	}	
	function fc_Eliminar(){
		if(document.getElementById("txhCodSelec").value!=""){			
			if( confirm(mstrEliminar) ){
				document.getElementById("txhOperacion").value="QUITAR";
				document.getElementById("frmMain").submit();		
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	function fc_Copiar(){
		if(document.getElementById("txhCodSelec").value!=""){
			pendiente=document.getElementById("txhConsteEncuestaPendiente").value;
			
			tipoEncuesta=document.getElementById("txhValTipoEncuesta").value;
			
			if(tipoEncuesta!=pendiente){
				if( confirm(mstrCopiar) ){
					document.getElementById("txhOperacion").value="COPIAR";
					document.getElementById("frmMain").submit();		
				}
			}
			else{
				alert("No se puede copiar la encuesta/nEstado Actual Pendiente.");
			}
		}
		else{
			alert(mstrSeleccion);
		}
	}
	function fc_TipoAplicacion(){
	  if(document.getElementById("codTipoAplicacion").value!=document.getElementById("txhConsteAplicacionServicio").value)
	  {       td_Servicio.style.display = 'none';
	  		  td_Servicio1.style.display = 'none';
	  		  document.getElementById("codTipoServicio").value="-1"; 
	  }
	  else{ if(document.getElementById("codTipoAplicacion").value==document.getElementById("txhConsteAplicacionServicio").value)
	  		{ td_Servicio.style.display = 'inline-block';
	  		  td_Servicio1.style.display = 'inline-block';
	 		}
	  } 
	}
	
	function fc_seleccionarRegistro(srtInd, codEncuesta, codEstado){
	   //alert(srtInd+">><<"+codEncuesta+">><<"+codEstado);
	   document.getElementById("txhCodSelec").value = codEncuesta;
	   document.getElementById("txhValTipoEncuesta").value = codEstado;
	}
	
	function fc_Modificar(){//ok
	  if(document.getElementById("txhCodSelec").value!=""){
		srtCodigo=document.getElementById("txhCodSelec").value;
		srtCodUsuario=document.getElementById("txhCodUsuario").value;
				                     
		window.location.href="${ctx}/encuestas/datosGenerales.html?prmUsuario="+
		srtCodUsuario+ "&prmCodEncuesta=" + srtCodigo + 
		"&txhEstado=" + document.getElementById("txhValTipoEncuesta").value +
		"&txhParCodTipoAplicacion=" + document.getElementById("codTipoAplicacion").value +
		"&txhParCodTipoEncuesta=" + document.getElementById("codTipoEncuesta").value +
		"&txhParCodTipoServicio=" + document.getElementById("codTipoServicio").value +
		"&txhParCodTipoEstado=" + document.getElementById("codTipoEstado").value +
		"&txhParBandera=1";
		
	 }
	  else alert(mstrSeleccione);
	}
	
	function fc_Agregar(){//ok
	    srtCodigo=document.getElementById("txhCodSelec").value;
		srtCodUsuario=document.getElementById("txhCodUsuario").value;
			
		window.location.href="${ctx}/encuestas/datosGenerales.html?prmUsuario="+ srtCodUsuario +
		"&txhEstado=" + document.getElementById("txhValTipoEncuesta").value +
		"&txhParCodTipoAplicacion=" + document.getElementById("codTipoAplicacion").value +
		"&txhParCodTipoEncuesta=" + document.getElementById("codTipoEncuesta").value +
		"&txhParCodTipoServicio=" + document.getElementById("codTipoServicio").value +
		"&txhParCodTipoEstado=" + document.getElementById("codTipoEstado").value +
		"&txhParBandera=" + document.getElementById("txhBandera").value ;
	
	}
	
	function fc_Conf_Perfil(){//ok
		srtCodUsuario=document.getElementById("txhCodUsuario").value;
		srtCodigo=document.getElementById("txhCodSelec").value;
		//alert(document.getElementById("txhValTipoEncuesta").value);
		if(document.getElementById("txhCodSelec").value!=""){
		
			pendiente=document.getElementById("txhConsteEncuestaPendiente").value;
			aplicada=document.getElementById("txhConsteEncuestaAplicada").value;
			tipoEncuesta=document.getElementById("txhValTipoEncuesta").value;
			
			if(tipoEncuesta!=pendiente && tipoEncuesta!=aplicada){
				window.location.href="${ctx}/encuestas/perfilProgramacion.html?txhCodUsuario="+
				srtCodUsuario+ "&txhCodEncuesta=" + srtCodigo +
				"&txhParCodTipoAplicacion=" + document.getElementById("codTipoAplicacion").value +
				"&txhParCodTipoEncuesta=" + document.getElementById("codTipoEncuesta").value +
				"&txhParCodTipoServicio=" + document.getElementById("codTipoServicio").value +
				"&txhParCodTipoEstado=" + document.getElementById("codTipoEstado").value +
				"&txhParBandera=1";
			  }
			else alert("No se puede configurar el perfil para este registro.");
	  	}
	  	else alert(mstrSeleccione);
	}
	function fc_VerEncuesta(srtCodEncuesta, srtCodTipoEncuesta){
		codUsuario=document.getElementById("txhCodUsuario").value;
		if(srtCodTipoEncuesta=="0001"){
			Fc_Popup_Resizable("${ctx}/encuestas/visualizarEncuestaEstandar.html?txhUsuario="+codUsuario+
				"&txhCodEncuesta="+srtCodEncuesta,800,600);
		}
		else
		if(srtCodTipoEncuesta=="0002"){								
			Fc_Popup_Resizable("${ctx}/encuestas/visualizarEncuestaMixta.html?txhUsuario="+codUsuario+
				"&txhCodEncuesta="+srtCodEncuesta,800,600);
		}	  	
	}
	
	/*
	   <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2.jpg',1)">
		  <img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>
	
	*/
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/bandeja_conf_gen_encuestas.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="consteTipoPrograma" id="txhConsteTipoPrograma"/>
<form:hidden path="consteTipoServicios" id="txhConsteTipoServicios"/>
<form:hidden path="consteEstadoPendiente" id="txhConsteEstadoPendiente"/>
<form:hidden path="consteEstadoAplicacion" id="txhConsteEstadoAplicacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="consteAplicacionPrograma" id="txhConsteAplicacionPrograma"/>
<form:hidden path="consteAplicacionServicio" id="txhConsteAplicacionServicio"/>
<form:hidden path="consteEncuestaGenerada" id="txhConsteEncuestaGenerada"/>
<form:hidden path="consteEncuestaPendiente" id="txhConsteEncuestaPendiente"/>
<form:hidden path="consteEncuestaAplicada" id="txhConsteEncuestaAplicada"/>
<form:hidden path="valTipoEncuesta" id="txhValTipoEncuesta"/>
<form:hidden path="bandera" id="txhBandera"/>
<form:hidden path="tamListaBandeja" id="txhTamListaBandeja"/>
<form:hidden path="dscMensaje" id="txhDscMensaje"/>

<form:hidden path="nomUsuario" id="txhNomUsuario"/>
<form:hidden path="perfilUsuario" id="txhPerfilUsuario"/>
<form:hidden path="codResponsable" id="txhCodResponsable"/>

	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>	
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Bandeja de Configuraci�n y Generaci�n de Encuestas					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- CABECERA -->	       
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2">	
		<tr>
			<td width="15%">&nbsp;&nbsp;Tipo Aplicaci�n:</td>
			<td width="22%">
			  <form:select path="codTipoAplicacion" id="codTipoAplicacion" cssClass="cajatexto" 
							cssStyle="width:160px" onchange="javascript:fc_TipoAplicacion();">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoAplicacion!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoAplicacion}" />
							</c:if>
			  </form:select>
			</td>
			<td width="10%">Tipo Encuesta:</td>
			<td>
			    <form:select path="codTipoEncuesta" id="codTipoEncuesta" cssClass="cajatexto" 
							cssStyle="width:160px">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoEncuesta!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoEncuesta}" />
							</c:if>
			  </form:select>
			</td>	
			<td id="td_Servicio" name="td_Servicio" style="display:none" width="10%">Tipo Servicio:</td>
			<td id="td_Servicio1" name="td_Servicio1" style="display:none">
			    <form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto"
							cssStyle="width:160px">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoServicio!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoServicio}" />
							</c:if>
			  </form:select>
			</td>		
		</tr>
		<tr>
			<td>&nbsp;&nbsp;Estado:</td>
			<td>
			   <form:select path="codTipoEstado" id="codTipoEstado" cssClass="cajatexto" 
							cssStyle="width:160px">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoEstado!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoEstado}" />
							</c:if>
			  </form:select>
			</td>
			<td colspan="4" align="right">
			    &nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
							 style="cursor: pointer; margin-right:" 
							 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>&nbsp;
			   		 <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
							 style="cursor: pointer;" onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
						
						
			</td>
		</tr>
	</table>
	<!-- BANDEJA -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td>
			<!-- div style="overflow: auto; height:350px;width:100%">
				<display:table name="sessionScope.listaBandejaConfiguracion" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.EncuestaDecorator"  requestURI="" 
				style="border: 1px solid #048BBA;width:98%;" id="tablaBandeja">					
					<display:column property="rbtSelEncuesta" title="Sel"  
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:4%"/>
					<display:column property="nroEncuesta" title="Nro. Encuesta" headerClass="grilla" 
					class="tablagrilla" style="text-align:center;width:15%"/>
					<display:column property="nombreEncuesta" title="Nombre" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:18%"/>
					<display:column property="nomTipoAplicacion" title="Tipo Aplicaci�n" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="nomTipoEncuesta" title="Tipo Encuesta" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:12%"/>
					<display:column property="nomEstado" title="Estado" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:12%"/>
					<display:column property="indEncuestaManual" title="Ind. Manual" headerClass="grilla" 
					class="tablagrilla" style="text-align:center;width:6%"/>
					<display:column property="visualizarEncuesta" title="Visualizar" headerClass="grilla" 
					class="tablagrilla" style="text-align:center;width:6%"/>
								
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='tablagrilla'><td colspan='8' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</div -->
			<iframe id="iFrameBandejaEncuesta" name="iFrameBandejaEncuesta" frameborder="0" height="360px" width="100%">
				</iframe>
		  </td>
		  <td width="30px" valign="middle" align="right">			
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" 
					id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
			</a>				
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
				<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar" style="cursor:pointer" 
					id="imgEliminarPerfil" onclick="javascript:fc_Eliminar();">
			</a>
		</td>
	    </tr>
	</table>
	<table border="0" width="100%" style="margin-top: 10px">
		<tr>
			<td align="center">					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/botones/modiformato2.jpg',1)">
				<img src="${ctx}/images/botones/modiformato1.jpg" onclick="javascript:fc_Modificar();" id="imgModificarPerfil" style="cursor:pointer;" alt="Modificar Formato"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgConfigPerfil','','${ctx}/images/botones/configperfil2.jpg',1)">
				<img src="${ctx}/images/botones/configperfil1.jpg" onclick="javascript:fc_Conf_Perfil();" id="imgConfigPerfil" style="cursor:pointer;" alt="Configurar Perfil"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCopiarFormato','','${ctx}/images/botones/copiarformato2.jpg',1)">
				<img src="${ctx}/images/botones/copiarformato1.jpg" onclick="javascript:fc_Copiar();" id="imgCopiarFormato" style="cursor:pointer;" alt="Copiar Formato"></a>					
			</td>
		</tr>
	</table>
		
</form:form>
</body>
