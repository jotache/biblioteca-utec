<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")	alert(mensaje);
	
	var mensajeGenerar = "<%=(( request.getAttribute("mensajeGenerar")==null)?"": request.getAttribute("mensajeGenerar"))%>";
			
	fc_ini();
	
	if( mensajeGenerar=="TRUE" )fc_Regresar();	
}

function fc_RegresarMenu(){
	document.location.href = "${ctx}/menuEncuesta.html"
}

function fc_generar(){
	codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
	
	
	if( codEncuesta != "" )
	{	if(confirm(mstrSeguroGrabar)){
			document.getElementById("operacion").value = "irGenerar";
			fc_oculta();
			document.getElementById("frmMain").submit();
		}	
		
	}else{
		alert(mstrNoSePuedeRealizarLaAccion);
	}

}

function fc_oculta(){
	
		document.getElementById("divajax").style.display = "";		
		document.getElementById("divtabla").style.display = "none";
		
}

function fc_Regresar(){ 
	window.location.href = "${ctx}/encuestas/bandeja_conf_gen_encuestas.html?txhCodUsuario="+
	document.getElementById("usuario").value +
	"&txhParCodTipoAplicacion=" + document.getElementById("txhParCodTipoAplicacion").value +
	"&txhParCodTipoEncuesta=" + document.getElementById("txhParCodTipoEncuesta").value +
	"&txhParCodTipoServicio=" + document.getElementById("txhParCodTipoServicio").value +
	"&txhParCodTipoEstado=" + document.getElementById("txhParCodTipoEstado").value +
	"&txhParBandera=" + document.getElementById("txhParBandera").value; 
}

function validaTexto(){

	if(document.getElementById("txaObservacion").value.length > 3000)
	{
		str = document.getElementById("txaObservacion").value.substring(0,3000);
		document.getElementById("txaObservacion").value=str;
		return false;
	}			
	
}

function fc_MixtaGruposPreguntas(){
	
	fc_ocultaTodos();
	codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
	usuario = document.getElementById("usuario").value;
	
	if( fc_Trim(document.getElementById("txhCodigo").value) != "" )
	{
	  fc_aparece("divGruposPreguntasMixta",'1');
	  document.getElementById("iframeGruposPreguntasMixta").src = "/SGA/encuestas/datosGeneralesMixtaVerGrupos.html"+		  
	  "?prmCodEncuesta="+codEncuesta+
	  "&prmUsuario="+usuario+
	  "&prmCodEstado="+document.getElementById("txhCodEstado").value;
	}
	else
		alert(mstrNoSePuedeRealizarLaAccion);
}

function fc_MixtaGruposAlternativas(){	
	
	fc_ocultaTodos();
	codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
	usuario = document.getElementById("usuario").value;
	
	if( fc_Trim(document.getElementById("txhCodigo").value) != "" )
	{
	  fc_aparece("divGruposAlternativasMixta",'1');
	  document.getElementById("iframeGruposAlternativasMixta").src = "${ctx}/encuestas/datosGeneralesMixtaGrupos.html"+
	  "?prmCodEncuesta="+codEncuesta+
	  "&prmUsuario="+usuario+
	  "&prmCodEstado="+document.getElementById("txhCodEstado").value;
	}
	else
		alert(mstrNoSePuedeRealizarLaAccion);

}

function Fc_encuesta()
{
	//fc_ocultaTodos();		
		
	if (document.getElementById("cboTipoEncuesta").value=="0002")
	{
		btnEti.style.display="none";
		btnGP.style.display="none";
		btnGru.style.display="";
		btnGE.style.display="";
		btnPreg.style.display="";
		btnGen.style.display="";
	}
	else
	{
		btnEti.style.display="";
		btnGP.style.display="";
		btnGru.style.display="";
		btnGE.style.display="none";
		btnPreg.style.display="none";
		btnGen.style.display="";
	}
}


function fc_ini(){

	if(fc_Trim(document.getElementById("cboTipoAplicacion").value=="0002")){			
			TipoServ.style.display="";
			TipoServ1.style.display="";		
	}
	
	Fc_encuesta();
	
	fc_InsertOrUpdate();
}

function fc_InsertOrUpdate(){
	
	if( fc_Trim(document.getElementById("txhCodigo").value) != "" )
	{	
		if( document.getElementById("txhCodEstado").value == "0001" ){
			fc_bloqueaPendiente();
		}else{
			fc_bloquea();
		}		
			
	}
}

function fc_bloqueaPendiente(){		
		document.getElementById("txtCodigo").readOnly = true;
		document.getElementById("txtCodigo").className = "cajatexto_1";
}

function fc_bloquea(){

		document.getElementById("cboTipoAplicacion").disabled = true;
		document.getElementById("cboTipoAplicacion").className = "cajatexto_1";		
		document.getElementById("txtCodigo").readOnly = true;
		document.getElementById("txtCodigo").className = "cajatexto_1";
		document.getElementById("cboTipoEncuesta").disabled = true;
		document.getElementById("cboTipoEncuesta").className = "cajatexto_1";
		
		if( Number(document.getElementById("txhNumPerfil").value)>0 ){
			document.getElementById("cboSede").disabled = true;
			document.getElementById("cboSede").className = "cajatexto_1";		
		}
		
}

function fc_desbloquea(){
	document.getElementById("cboTipoAplicacion").disabled = false;		
	document.getElementById("cboSede").disabled = false;	
	document.getElementById("txtCodigo").readOnly = false;
	document.getElementById("cboTipoEncuesta").disabled=false;
	
}


function fc_formato(){
	codEnc = document.getElementById("txhCodigo").value;
	codUsu = document.getElementById("usuario").value;
	
	if( fc_Trim(codEnc) != "" )
	{	
		URL = "${ctx}/encuestas/datosGenTipoFormato.html"+
		"?txhCodFormatoEncuesta="+codEnc+
		"&txhCodUsuario="+codUsu+
		"&prmCodEstado="+document.getElementById("txhCodEstado").value;	
		
		Fc_Popup(URL,500,350);
	}
	else
		alert(mstrNoSePuedeRealizarLaAccion);
		
	
}

function fc_Alternativas(){
	
	fc_ocultaTodos();
	
	codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
	usuario 	= document.getElementById("usuario").value;
	 
	if( codEncuesta != "" )
	{		
		fc_aparece("divAlternativas",1);
		
		document.getElementById("iframeAlternativas").src = "${ctx}/encuestas/datosGeneralesAlternativas.html"+
		"?prmCodEncuesta="+codEncuesta+
		"&prmUsuario="+usuario+
		"&prmCodEstado="+document.getElementById("txhCodEstado").value;
	}	
	else
	{	
		alert(mstrNoSePuedeRealizarLaAccion);
	}
}

function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}

function fc_ocultaTodos(){

	document.getElementById("divGruposEstandar").style.display 			= "none";
	document.getElementById("divAlternativas").style.display 			= "none";
	document.getElementById("divGruposAlternativasMixta").style.display = "none";	
	document.getElementById("divGruposPreguntasMixta").style.display 	= "none";
	
}

function fc_GruposStandar(){

	fc_ocultaTodos();
	
	codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
	usuario = document.getElementById("usuario").value;
	
	if( fc_Trim(document.getElementById("txhCodigo").value) != "" )
	{		
		  fc_aparece("divGruposEstandar",'1');		  
		  document.getElementById("iframeGruposEstandar").src = "${ctx}/encuestas/datosGeneralesGrupos.html"+
		  "?prmCodEncuesta="+codEncuesta+
		  "&prmUsuario="+usuario+
		  "&prmCodEstado="+document.getElementById("txhCodEstado").value;
	}
	else
		alert(mstrNoSePuedeRealizarLaAccion);	
}



function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){						
			fc_desbloquea();		
			document.getElementById("operacion").value="irRegistrar";
			fc_oculta();						
			document.getElementById("frmMain").submit();
		}
	}
}

function fc_validaGrabar(){

	if(fc_Trim(document.getElementById("cboTipoAplicacion").value)=="")
	{	alert(mstrSeleccioneTipoAplicacion);
		document.getElementById("cboTipoAplicacion").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboTipoEncuesta").value)==""){
		alert(mstrSeleccioneTipoEncuesta);
		document.getElementById("cboTipoEncuesta").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboSede").value)==""){
		alert(mstrSeleccioneSede);
		document.getElementById("cboSede").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtCodigo").value)==""){
		alert(mstrIngrCodigo);
		document.getElementById("txtCodigo").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtNombre").value)==""){
		alert(mstrIngrNombre);
		document.getElementById("txtNombre").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtDuracion").value)==""){
		alert(mstrIngrDuracion);
		document.getElementById("txtDuracion").focus();
		return 0;
	}	

	if(fc_Trim(document.getElementById("cboTipoAplicacion").value=="0002")){
		if(fc_Trim(document.getElementById("cboTipoServicio").value==""))
		{
			alert(mstrIngrTipoServicio);//mstrIngrCodigo;
			document.getElementById("cboTipoServicio").focus();
			return 0;
		}
	}
	return 1;
}	


function Fc_CambiaTipoApli(){

	
	if (document.getElementById("cboTipoAplicacion").value=="0002"){ //Mixta
		TipoServ.style.display="";
		TipoServ1.style.display="";	
	}else{
		TipoServ.style.display="none";
		TipoServ1.style.display="none";
	}
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function fc_MostrarPreguntasStandar(codGrupo){
	
		codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
		
		fc_aparece("divPreguntasEstandar",'1');
						
		document.getElementById("iframePreguntasEstandar").src = "${ctx}/encuestas/datosGeneralesPreguntas.html"+
		"?prmCodEncuesta="+codEncuesta+
		"&prmCodGrupo="+codGrupo+
		"&prmCodEstado="+document.getElementById("txhCodEstado").value;
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/datosGenerales.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<!--ESTADO DE LA ENCUESTA-->
	<form:hidden path="txhCodEstado" id="txhCodEstado" />
	<form:hidden path="txhNumPerfil" id="txhNumPerfil" />	
	
	<!-- parametros de la bandeja al regresar -->
	<form:hidden path="parCodTipoAplicacion" id="txhParCodTipoAplicacion" />
	<form:hidden path="parCodTipoEncuesta" id="txhParCodTipoEncuesta" />
	<form:hidden path="parCodTipoServicio" id="txhParCodTipoServicio" />
	<form:hidden path="parCodTipoEstado" id="txhParCodTipoEstado" />
	<form:hidden path="parBandera" id="txhParBandera" />
	
	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_RegresarMenu();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!-- Titulo de la Pagina -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Datos Generales de la encuesta					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	
	<table border="0" bordercolor="blue" cellpadding="0" cellspacing="0" width="98%">
		<tr>
			<td width="95%" valign="top">	
				<table class="tabla" style="width:98%;margin-left:9px;margin-top: 4px;" background="${ctx}/images/Evaluaciones/back.jpg" 
							cellspacing="2" cellpadding="3" border="0" bordercolor="red">
					<tr>
					<td>Tipo Aplicación :</td>
					<td>
						<form:select path="cboTipoAplicacion"
							id="cboTipoAplicacion" cssClass="cajatexto_o"
							cssStyle="width:100px" onchange="Fc_CambiaTipoApli()">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.lstTipoAplicacion!=null}">
								<form:options itemValue="codTipoTablaDetalle"
									itemLabel="descripcion" items="${control.lstTipoAplicacion}" />
							</c:if>
						</form:select>
					</td>
					<td>Tipo Encuesta :</td>
					<td>						
							<form:select path="cboTipoEncuesta" id="cboTipoEncuesta"
								cssClass="cajatexto_o" cssStyle="width:100px"
								onchange="Fc_encuesta()">
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.lstTipoEncuesta!=null}">
									<form:options itemValue="codTipoTablaDetalle"
										itemLabel="descripcion" items="${control.lstTipoEncuesta}" />
								</c:if>
							</form:select>						 
					</td>
					<td>Sede</td>
					<td><form:select path="cboSede" id="cboSede"
						cssClass="combo_o" cssStyle="width:100px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstSede!=null}">
							<form:options itemValue="dscValor1" itemLabel="descripcion"
								items="${control.lstSede}" />
						</c:if>
					</form:select></td>
					<td>Duración :</td>
					<td><form:input path="txtDuracion" id="txtDuracion"
						cssClass="cajatexto_o"
						onblur="fc_ValidaNumeroOnBlur('txtDuracion');"
						onkeypress="fc_PermiteNumeros();" maxlength="5" size="5" />
					&nbsp;min.</td>
				</tr>
				<tr>
					<td> Nro.Encuesta:</td>
					<td><form:input path="txtCodigo" id="txtCodigo"
						cssClass="cajatexto_o"						
						 maxlength="20" size="13" /></td>
					<td>Nombre :</td>
					<td colspan="5"><form:input path="txtNombre" id="txtNombre"
						cssClass="cajatexto_o" maxlength="255" size="80" /></td>
					
				</tr>
				<tr>
					<td id="TipoServ" style="display: none;">Tipo Servicio :</td>
					<td id="TipoServ1" style="display: none;" colspan="3"><form:select
						path="cboTipoServicio" id="cboTipoServicio" cssClass="cajatexto_o"
						cssStyle="width:190px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstTipoServicio!=null}">
							<form:options itemValue="codTipoTablaDetalle"
								itemLabel="descripcion" items="${control.lstTipoServicio}" />
						</c:if>
					</form:select></td>
				</tr>
				<tr>
					<td>Observación :</td>
					<td colspan="7"><form:textarea path="txaObservacion"
						id="txaObservacion" cssClass="cajatexto" rows="5" cols="140"
						onkeypress="validaTexto()" /></td>
				</tr>
			</table>
			</td>
			<td width="30%">
			<table>
				<tr>
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2g.jpg',1)">
						<img src="${ctx}/images/botones/grabar1g.jpg" align="absmiddle" alt="Regresar" id="imgGrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>							
				</tr>
				<tr id="btnEti">
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAlternativas','','${ctx}/images/botones/alternativas2g.jpg',1)">
						<img src="${ctx}/images/botones/alternativas1g.jpg" align="absmiddle" alt="Alternativas" id="imgAlternativas" style="cursor:pointer;" onclick="fc_Alternativas()"></a>
					</td>
				</tr>
				<tr id="btnGru">
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgformato','','${ctx}/images/botones/formatos2g.jpg',1)">
						<img src="${ctx}/images/botones/formatos1g.jpg" align="absmiddle" alt="Formato" id="imgformato" style="cursor:pointer;" onclick="javascript:fc_formato();"></a>
					</td>
				</tr>
				<tr id="btnGP">
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrupopreg','','${ctx}/images/botones/grupospreguntas2g.jpg',1)">
						<img src="${ctx}/images/botones/grupospreguntas1g.jpg" align="absmiddle" alt="Formato" id="imgGrupopreg" style="cursor:pointer;" onclick="fc_GruposStandar()"></a>
					</td>
				</tr>
				<tr id="btnGE" style="display: none;">
					<td>						
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGruposAlt','','${ctx}/images/botones/gruposalternativas2g.jpg',1)">
						<img src="${ctx}/images/botones/gruposalternativas1g.jpg" onclick="fc_MixtaGruposAlternativas()" alt="Grupos/Alternativas" id="imgGruposAlt" style="cursor:pointer;"></a>
					</td>
				</tr>
				<tr id="btnPreg" style="display: none;">
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGruposPreguntas','','${ctx}/images/botones/grupospreguntas2g.jpg',1)">
						<img src="${ctx}/images/botones/grupospreguntas1g.jpg" onclick="fc_MixtaGruposPreguntas()" alt="Grupos/Preguntas" id="imgGruposPreguntas" style="cursor:pointer;"></a>
					</td>
				</tr>
				<tr id="btnGen" style="display: ;">
					<td>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGenerar','','${ctx}/images/botones/generar2g.jpg',1)">
						<img src="${ctx}/images/botones/generar1g.jpg" align="absmiddle" alt="Generar" id="imgGenerar" style="cursor:pointer;" onclick="javascript:fc_generar();"></a>	
					</td>
				</tr>
				<tr>
					<td>					
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2g.jpg',1)">
						<img alt="Regresar" src="${ctx}/images/botones/regresar1g.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>
					</td>
						
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<DIV id="divajax" align="center" class="texto_bold" style="display:none" ><br><br><br><br><br><br><br><br>Espere...<br><img src="${ctx}/images/ajaxloader.gif"><br>Guardando Información<br></DIV>	
	<DIV id="divtabla"  align="left" >
	
	<div id="divAlternativas" style="display: none; width: 100%; height: 100%;"  >
		<table width="60%" border="0" bordercolor="RED" cellpadding="0" cellspacing="0">
			<tr>
				<td>			
					<iframe id="iframeAlternativas" name="iframeAlternativas"
						frameborder="0" height="300" style="width: 100%;">
					</iframe>
				</td>
			</tr>
		</table>
	</div>

	<div id="divGruposEstandar" style="display: none; width: 100%; height: 100%;">
		<table width="100%" border="0" bordercolor="RED" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<iframe id="iframeGruposEstandar" name="iframeGruposEstandar" frameborder="0" height="302" 
						style="width: 100%;">
					</iframe>
				</td>
			</tr>
		</table>
	</div>
	
	<table>
		<tr>
			<td>
			<div id="divPreguntasEstandar" style="display: none; width: 100%; height: 100%;">
				<iframe id="iframePreguntasEstandar" name="iframePreguntasEstandar" frameborder="0" height="300"
					style="width: 100%;">
				</iframe>
			</div>
			</td>
		</tr>
	</table>

	<div id="divGruposAlternativasMixta" style="display: none; width: 100%; height: 100%;">
	<table width="100%" border="0" bordercolor="red" cellpadding="0" cellspacing="0">
		<tr>
			<td valign="top">			
				<iframe id="iframeGruposAlternativasMixta" name="iframeGruposAlternativasMixta" frameborder="0" height="290"
					width="100%" style="width: 100%; margin-left: 0px;">
				</iframe>
			</td>
		</tr>
	</table>
	</div>

	<div id="divGruposPreguntasMixta" style="display: none; width: 100%; height: 100%;">
	<table width="100%" border="0" bordercolor="red" cellpadding="0" cellspacing="0">
		<tr>
			<td valign="top">
			<iframe id="iframeGruposPreguntasMixta" name="iframeGruposPreguntasMixta" frameborder="0"  height="290"
				style="width: 100%; margin-left: 0px;">
			</iframe>
			</td>
		</tr>
	</table>
	</div>
	</DIV>
	<!-- **************************************************************************************************** -->
</form:form>
</body>


