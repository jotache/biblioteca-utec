<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
	
	}
			
	function fc_SeleccionarRegistro(srtCodPersonal, srtUsuPersonal, srtNombrePersonal){
	
	parent.fc_SeleccionarRegistro(srtCodPersonal, srtUsuPersonal, srtNombrePersonal);
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/busqueda_empleados_iframe.html">

<form:hidden path="codTipoPersonal" id="txhCodTipoPersonal" />
<form:hidden path="apellidoPaterno" id="txhApellidoPaterno" />
<form:hidden path="apellidoMaterno" id="txhApellidoMaterno" />
<form:hidden path="nombre" id="txhNombre" />
			
	<table cellpadding="0" cellspacing="0" ID="Table1" width="97%" border="0"  class="" style="margin-left:9px ; margin-top: 4px;">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 280px;">
				 	<display:table name="sessionScope.listaBandejadatosPersonal" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.EncuestaDecorator"  pagesize="10" requestURI=""
						style="border: 1px solid #048BBA;width:100%;">
						
					    <display:column property="rbtSelEmpleado" title="Sel." 
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:4%"/>
						<display:column property="nombrePersonal" title="Nombre" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:48%"/>
						<display:column property="nombreTipoPersonal" title="Tipo Personal" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:48%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='tablagrilla'><td colspan='3' align='center' >No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
					  </display:table>
					</div>
					
				</td>
				
			</tr>
		</table>
		
</form:form>
</body>
</html>