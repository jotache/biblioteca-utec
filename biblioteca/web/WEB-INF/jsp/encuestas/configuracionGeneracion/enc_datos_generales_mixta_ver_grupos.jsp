<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
}

function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}


function fc_Sel(pos){

	document.getElementById("txhPosSel").value = pos;
	codGrupo = document.getElementById("hidCodigo"+pos).value;	
	codAlternativa = document.getElementById("hidAlternativa"+pos).value; 
	
	fc_MostrarPreguntasMixta(codGrupo,codAlternativa);
	//fc_aparece("divAlternativasMixta",'0');

}

function fc_MostrarPreguntasMixta(codGrupo,codAlternativa){
	
		codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
		usuario = document.getElementById("usuario").value;
		
		
		fc_aparece("divPreguntasMixta",'1');
		
		
						
		document.getElementById("iframePreguntasMixta").src 
		= "${ctx}/encuestas/datosGeneralesMixtaPreguntas.html"+
		"?prmCodEncuesta="+codEncuesta+
		"&prmCodGrupo="+codGrupo+
		"&prmUsuario="+usuario+
		"&prmCodAlternativa="+codAlternativa+
		"&prmCodEstado="+document.getElementById("txhCodEstado").value;		
				
}



function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/datosGeneralesMixtaVerGrupos.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />	
	<form:hidden path="txhCodEstado" id="txhCodEstado" />	
	
	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />	
	<!-- **************************************************************************************************** -->	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="blue"
		style="width:97%;margin-left:9px" >
		<tr height="80px;">
			<td valign="top">			
				<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
					<tr>
						<td class="grilla" width="5%">Sel.</td>
						<td class="grilla" width="55%">Grupo</td>
						<td class="grilla" width="20%">Formato</td>
						<td class="grilla" width="10%;">Alternativa</td>
						<td class="grilla" width="10%;">Peso</td>
					</tr>
				</table>
				<div style="overflow: auto; height: 60px;width:100%">
				<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
					<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
					<tr  class="tablagrilla">
						<td width="5%" align="center">
							<input type="hidden" id="hidCodigo<c:out value="${loop.index}" />"   value="<c:out value="${objCast.codGrupo}" />"		name="hidCodigo<c:out value="${loop.index}" />" />
							<input type="hidden" id="hidGrupo<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.nomGrupo}" />"		name="hidGrupo<c:out value="${loop.index}" />"  />
							<input type="hidden" id="hidFormato<c:out value="${loop.index}" />"  value="<c:out value="${objCast.codFormato}" />"		name="hidFormato<c:out value="${loop.index}" />" />
							<input type="hidden" id="hidAlternativa<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.indAlternativa}" />"		name="hidAlternativa<c:out value="${loop.index}" />" />									
							<input type="hidden" id="hidPeso<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.pesoGrupo}" />"		name="hidPeso<c:out value="${loop.index}" />" />
							<INPUT class="radio" type="radio"  name="radio" onclick="fc_Sel('<c:out value="${loop.index}" />')" >
						</td>
						<td width="55%"><c:out value="${objCast.nomGrupo}" /></td>
						<td width="20%" align="center"><c:out value="${objCast.nomFormato}" /></td>
						<td width="10%;" align="center"><c:out value="${objCast.desAlternativa}" /></td>
						<td width="10%;" align="center"><c:out value="${objCast.pesoGrupo}" /></td>
					</tr>							
					</c:forEach>					
				</table>
				</div>
			</td>
		</tr>
		<tr style="height: 4px">
			<td>
			</td>
		</tr>	
		<tr style="height: 190px">
			<td valign="top">
				<div id="divPreguntasMixta" style="display: none;width: 100%;height: 100%;" >
				
				<iframe id="iframePreguntasMixta" name="iframePreguntasMixta" frameborder="0"
					scrolling="no"  style="width:100%;margin-left:0px;height: 197px;" >
				</iframe>
					
				</div>
			</td>
		</tr>			
	</table>										
<!-- **************************************************************************************************** -->
</form:form>
</body>


