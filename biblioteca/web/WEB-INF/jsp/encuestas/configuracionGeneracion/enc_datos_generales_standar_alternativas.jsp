<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);
	
	document.getElementById("txtPeso").value = "1";		
}

function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtAlternativa").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("txtDescripcion").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	document.getElementById("txhCodigoSel").value = "";
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){

			document.getElementById("operacion").value="irRegistrar";						
			document.getElementById("frmMain").submit();
		}
	}
}

function fc_Actualizar(){
	var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_mostrarDatos(posSel){

	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtAlternativa").value =  document.getElementById("hidAlternativa"+posSel).value ;
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	document.getElementById("txtDescripcion").value = document.getElementById("hidDescripcion"+posSel).value;
	
}

function fc_validaGrabar(){
	if(fc_Trim(document.getElementById("txtAlternativa").value)=="")
	{	alert(mstrIngreseAlternativa);
		document.getElementById("txtAlternativa").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngresePeso);//mstrSelTitulo;
		document.getElementById("txtPeso").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtDescripcion").value)==""){
		alert(mstrIngreseDescripcion);//mstrSelSede;
		document.getElementById("txtDescripcion").focus();
		return 0;
	}
		
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function fc_Sel(pos){
	document.getElementById("txhPosSel").value = pos;
}

function fc_Eliminar(){
	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{	
			if(confirm(mstrSeguroEliminar1))
			{
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			}
		}
	else
		alert(mstrSeleccione);
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/datosGeneralesAlternativas.html">
	<!--OBJETOS PERSISTENTES-->
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario"   id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<form:hidden path="txhCodEstado" id="txhCodEstado" />
		
	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />
	
<!-- **************************************************************************************************** -->
<table cellpadding="0" cellspacing="0" border="0" bordercolor="blue"
		style="width:98%;margin-left:9px" >
	<tr>
		<td valign="top">			
			<table class="tabla2" width="100%" cellSpacing="0" cellPadding="0"
				border="0" bordercolor='gray' ID="Table2" style="margin-top: 0px;height: 18px;">
				<tr>
					<td align="left" style='cursor: hand;'>ALTERNATIVAS</td>
				</tr>
			</table>
			
			<table background="${ctx}/images/biblioteca/fondosup.jpg"
				style="width: 100%;height: 70px;margin-bottom: 4px;" border="0" cellspacing="1" cellpadding="1" class="tabla" bordercolor="red">
				<tr>
					<td class=""> Alternativa:</td>
					<td>					
						<form:input path="txtAlternativa" id="txtAlternativa" onkeypress="fc_ValidaTextoEspecialEncuestas()" onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtAlternativa')" maxlength="5" cssClass="cajatexto_o" size="25" />
					</td>
					<td class=""> Peso:</td>
					<td>						
						<form:input path="txtPeso" id="txtPeso" cssClass="cajatexto_o" cssStyle=""
						maxlength="3"
					 	onblur="fc_ValidaNumeroOnBlur('txtPeso');" 
							onkeypress="fc_PermiteNumeros();" 
						 size="3" />
					</td>
					<td>
					 	<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/iconos/grabar2.jpg',1)">
					 	<img onclick="fc_Grabar()" src="${ctx}/images/iconos/grabar1.jpg" alt="Grabar" style="cursor:pointer;" id="imgGrabar"></a>
					 	&nbsp;
					 	<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/cerrar2.jpg',1)">
					 	<img onclick="fc_Limpiar();" src="${ctx}/images/iconos/cerrar1.jpg" alt="Limpiar" style="cursor:pointer;" id="imgLimpiar"></a>
					</td>
				</tr>
				<tr>
					<td class=""> Descripción:</td>
					<td colspan="3">
					
					<form:input path="txtDescripcion" id="txtDescripcion" onkeypress="fc_ValidaTextoEspecialEncuestas()"  onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtDescripcion')"  cssClass="cajatexto_o" maxlength="255" size="68"/>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table style="width: 100%; margin-top: 0px" cellSpacing="0" cellPadding="0" border="0" bordercolor="blue" ID="Table12">			 
				<tr>
					<td style="width: 95%;">				
						<table cellpadding="0" cellspacing="1" style="width:97%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
							<tr>
								<td class="grilla" style="width: 5%;">Sel.</td>
								<td class="grilla" style="width: 20%;">Alternativa</td>
								<td class="grilla" style="width: 50%;">Descripción</td>
								<td class="grilla" style="width: 25%;">Peso</td>
							</tr>
						</table>
						<div style="overflow: auto; height: 175px;width:100%">
						<table cellpadding="0" cellspacing="1" style="width:97%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">								
						<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
							<tr  class="tablagrilla">
								<td style="width: 5%;">
									<input type="hidden" id="hidCodigo<c:out value="${loop.index}" />" 		name="hidCodigo<c:out value="${loop.index}" />" value="<c:out value="${objCast.codAlternativa}" />" />
									<input type="hidden" id="hidAlternativa<c:out value="${loop.index}" />" 	name="hidAlternativa<c:out value="${loop.index}" />"  value="<c:out value="${objCast.etiAlternativa}" />"   />
									<input type="hidden" id="hidDescripcion<c:out value="${loop.index}" />" 	name="hidDescripcion<c:out value="${loop.index}" />"  value="<c:out value="${objCast.desAlternativa}" />"  />
									<input type="hidden" id="hidPeso<c:out value="${loop.index}" />" 			name="hidPeso<c:out value="${loop.index}" />"  value="<c:out value="${objCast.peso}" />"  />
									
									<INPUT class="radio" type="radio"  name="radio" onclick="fc_Sel('<c:out value="${loop.index}" />')" >
								 </td>
								<td style="width: 20%;" align="center"  ><c:out value="${objCast.etiAlternativa}" /></td>
								<td style="width: 50%;"><c:out value="${objCast.desAlternativa}" /></td>
								<td style="width: 25%;" align="center"><c:out value="${objCast.peso}" /></td>
							</tr>							
						</c:forEach>
					</table>
					</div>
					</td>
					<td align="right" valign="middle">					
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificar1','','${ctx}/images/iconos/modificar2.jpg',1)">
							<img onclick="fc_Actualizar()" src= "${ctx}/images/iconos/modificar1.jpg" style="cursor:pointer" id="imgModificar1">
						</a>
						<br>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgQuitar','','${ctx}/images/iconos/quitar2.jpg',1)">
							<img onclick="fc_Eliminar()" src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer" id="imgQuitar">
						</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- **************************************************************************************************** -->
</form:form>
</body>


