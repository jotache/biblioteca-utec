<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
	function onLoad(){
		objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK_GRABAR" ){
			//PARA REFRESCAR EL TOTAL DE ENCUESTADOS DEL PADRE
			totalEncuestados=document.getElementById("txhTotalEncuestados").value;
			dscEncuestados=document.getElementById("txhDscEncuestados").value;
			parent.fc_ActualizarTotalEncuestado(totalEncuestados, dscEncuestados);
			//************************		
			alert(mstrGrabar);
			codTipoAplicacion=parent.document.getElementById("txhCodTipoAplicacion").value;
			if(codTipoAplicacion==document.getElementById("txhEncuestaPrograma").value){
				parent.document.getElementById("txhCodPerfilEncuesta").value=document.getElementById("txhPerfilPERSONAL").value;
				parent.fc_OcultarPerfiles(document.getElementById("txhPerfilPERSONAL").value);
			}
			else
			if(codTipoAplicacion==document.getElementById("txhEncuestaSeccion").value){
				parent.document.getElementById("txhCodPerfilEncuesta").value=document.getElementById("txhCodPerfilEncuesta").value;
			}			
		}		
		else
		if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);			
		}
		else
		if ( objMsg.value != 'null' &&  objMsg.value != ""){
			alert(document.getElementById("txhDscMensaje").value)
		}
		document.getElementById("txhMsg").value="";
		document.getElementById("txhOperacion").value="";
	}
	
	function fc_oculta(){
	
		document.getElementById("divajax").style.display = "";;		
		document.getElementById("divtabla").style.display = "none";
		
	}
	
	function fc_Grabar(){		
		fc_ObtenerDatosListas();
		if(confirm(mstrSeguroGrabar)){
			fc_oculta();
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
	function fc_ObtenerDatosListas(){
		fc_ListaPersonalSeleccionado();
		fc_ListaDocenteSeleccionado();
	}
	function fc_ListaPersonalSeleccionado(){
		cadenaPerSel=fc_ObtenerCadenaCombo("cboPersonalSeleccionados");
		//alert(">>"+cadenaPerSel+"<<");
		document.getElementById("txhCadPersonalSel").value=cadenaPerSel;
	}
	function fc_ListaDocenteSeleccionado(){
		cadenaDocSel=fc_ObtenerCadenaCombo("cboDocentesSeleccionados");
		//alert(">>"+cadenaDocSel+"<<");
		document.getElementById("txhCadDocenteSel").value=cadenaDocSel;
	}
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/perfilPersonalTecsup.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="dscMensaje" id="txhDscMensaje" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	
	<form:hidden path="codEncuesta" id="txhCodEncuesta" />
	<form:hidden path="codPerfilEncuesta" id="txhCodPerfilEncuesta" />
	<form:hidden path="codTipoEncuesta" id="txhCodTipoEncuesta" />
	
	<form:hidden path="cadPersonalSel" id="txhCadPersonalSel" />
	<form:hidden path="cadDocenteSel" id="txhCadDocenteSel" />
	<!-- CONSTANTES -->
	<form:hidden path="encuestaPrograma" id="txhEncuestaPrograma" />
	<form:hidden path="encuestaSeccion" id="txhEncuestaSeccion" />
	<form:hidden path="perfilPERSONAL" id="txhPerfilPERSONAL" />
	
	<form:hidden path="totalEncuestados" id="txhTotalEncuestados" />
	<form:hidden path="dscEncuestados" id="txhDscEncuestados" />	
	<!-- TABLA PERSONAL TECSUP -->
<DIV id="divajax" align="center" class="texto_bold" style="display:none" ><br><br><br><br><br><br><br><br>Espere...<br><img src="${ctx}/images/ajaxloader.gif"><br>Guardando Información<br></DIV>	
<DIV id="divtabla"  align="left" >	
	<table background="${ctx}/images/biblioteca/fondosup.jpg" class="tabla" style="width:97%;margin-left:9px;" 
		cellspacing="6" cellpadding="0" border="0" bordercolor="red" id="tablaPERSONAL">
		<tr>
			<td width="10%"></td>
			<td width="20%">Tipo Personal Disponible				
			</td>
			<td width="10%"></td>
			<td width="20%">Tipo Personal Seleccionados
			</td>
			<td width="40%" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar04','','${ctx}/images/iconos/grabar2.jpg',1)">
					<img src="${ctx}/images/iconos/grabar1.jpg" style="cursor:pointer;" id="imgGrabar04" onclick="fc_Grabar();" 
					alt="Grabar">
				</a>			
			</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td width="20%">
				<form:select  path="codPersonalDisponibles" id="cboPersonalDisponibles" cssClass="cajatexto" 
					cssStyle="width:100%;height: 105px;" multiple="true">
			        <c:if test="${control.listaPersonalDisponibles!=null}">
		        	<form:options itemValue="codigo" itemLabel="nombre" 
		        		items="${control.listaPersonalDisponibles}" />						        
		            </c:if>
		       </form:select>
			</td>
			<td align="center" valign="middle">			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgadelantePERSONAL01','','${ctx}/images/iconos/agregaruno2.jpg',1)">
				<img src="${ctx}/images/iconos/agregaruno1.jpg" alt="Agregar" onclick="javascript:fc_Mover('right','cboPersonalDisponibles','cboPersonalSeleccionados')" id="imgadelantePERSONAL01" style="cursor:hand"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgatrasPERSONAL01','','${ctx}/images/iconos/quitaruno2.jpg',1)">
				<img src="${ctx}/images/iconos/quitaruno1.jpg" alt="Quitar" onclick="javascript:fc_Mover('left','cboPersonalDisponibles','cboPersonalSeleccionados')" id="imgatrasPERSONAL01" style="cursor:hand"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagtodPERSONAL01','','${ctx}/images/iconos/agregartodos2.jpg',1)">
				<img src="${ctx}/images/iconos/agregartodos1.jpg" onclick="javascript:fc_MoverTodo('cboPersonalDisponibles','cboPersonalSeleccionados')"  alt="Agregar Todos" style="cursor:hand" id="imgagtodPERSONAL01"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitodPERSONAL01','','${ctx}/images/iconos/quitartodos2.jpg',1)">
				<img src="${ctx}/images/iconos/quitartodos1.jpg" onclick="javascript:fc_QuitarTodo('cboPersonalDisponibles','cboPersonalSeleccionados')" alt="Quitar Todos" style="cursor:hand" id="imgquitodPERSONAL01"></a>
			</td>
			<td width="20%" valign="top">
				<form:select  path="codPersonalSeleccionados" id="cboPersonalSeleccionados" cssClass="cajatexto_o" 
					cssStyle="width:100%;height: 105px;font-size: 11px;" multiple="true">
			        <c:if test="${control.listaPersonalSeleccionado!=null}">
		        	<form:options itemValue="codigo" itemLabel="nombre" 
		        		items="${control.listaPersonalSeleccionado}" />						        
		            </c:if>
		       </form:select>				
			</td>
			<td width="40%">
			</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td colspan="2">Tipo Personal Docente Disponible</td>			
			<td width="20%">Tipo Personal Seleccionados
			</td>
			<td width="40%">
			</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td width="20%">
				<form:select  path="codDocentesDisponibles" id="cboDocentesDisponibles" cssClass="cajatexto" 
					cssStyle="width:100%;height: 105px;" multiple="true">
			        <c:if test="${control.listaDocentesDisponibles!=null}">
		        	<form:options itemValue="codigo" itemLabel="nombre" 
		        		items="${control.listaDocentesDisponibles}" />						        
		            </c:if>
		       </form:select>				
			</td>
			<td align="center" valign="middle">			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgadelantePERSONAL02','','${ctx}/images/iconos/agregaruno2.jpg',1)">
				<img src="${ctx}/images/iconos/agregaruno1.jpg" alt="Agregar" onclick="javascript:fc_Mover('right','cboDocentesDisponibles','cboDocentesSeleccionados')" id="imgadelantePERSONAL02" style="cursor:hand"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgatrasPERSONAL02','','${ctx}/images/iconos/quitaruno2.jpg',1)">
				<img src="${ctx}/images/iconos/quitaruno1.jpg" alt="Quitar" onclick="javascript:fc_Mover('left','cboDocentesDisponibles','cboDocentesSeleccionados')" id="imgatrasPERSONAL02" style="cursor:hand"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagtodPERSONAL02','','${ctx}/images/iconos/agregartodos2.jpg',1)">
				<img src="${ctx}/images/iconos/agregartodos1.jpg" onclick="javascript:fc_MoverTodo('cboDocentesDisponibles','cboDocentesSeleccionados')"  alt="Agregar Todos" style="cursor:hand" id="imgagtodPERSONAL02"></a>
				<br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitodPERSONAL02','','${ctx}/images/iconos/quitartodos2.jpg',1)">
				<img src="${ctx}/images/iconos/quitartodos1.jpg" onclick="javascript:fc_QuitarTodo('cboDocentesDisponibles','cboDocentesSeleccionados')" alt="Quitar Todos" style="cursor:hand" id="imgquitodPERSONAL02"></a>
			</td>
			<td width="20%" valign="top" nowrap>
				<form:select  path="codDocentesSeleccionados" id="cboDocentesSeleccionados" cssClass="cajatexto_o" 
					cssStyle="width:100%;height: 105px;font-size: 11px;" multiple="true">
			        <c:if test="${control.listaDocentesSeleccionado!=null}">
		        	<form:options itemValue="codigo" itemLabel="nombre" 
		        		items="${control.listaDocentesSeleccionado}" />						        
		            </c:if>
		       </form:select>
			</td>
			<td width="40%">
			</td>
		</tr>
	</table>
</DIV>	
</form:form>
<script language="javascript">
	function fc_Mover(side, cboOrigen, cboDestino){
		var origen;//origen
		var destino;//destino
		if (side == "right")
		{ 
			origen = document.getElementById(cboOrigen);
			destino = document.getElementById(cboDestino);
		}
		else
		{  
			origen = document.getElementById(cboDestino);
			destino = document.getElementById(cboOrigen); 
		}
		var idSel=-1;
		for (var i = 0; i < origen.length; i++)
		{  
			if ( origen.options[i].selected )
			{
				idSel=	i;
			}
		}
		if(idSel==-1)
		{
			alert(mstrSeleccion);
			return;
		}
		var NroItemsDestino=destino.length;
		var oOption = document.createElement('OPTION');
		destino.options.add(oOption);
		oOption.value = origen.options[idSel].value;
		oOption.innerText = origen.options[idSel].text;
		for(i=idSel;i < origen.length-1; i++){
			origen.options[i].value=origen.options[i+1].value;
			origen.options[i].text=origen.options[i+1].text;
		}
		origen.length=origen.length-1;
		origen.focus()
	}
	function fc_MoverTodo( cboOrigen, cboDestino){
		fbox = document.getElementById(cboOrigen);
		tbox = document.getElementById(cboDestino);
		var arrFbox = new Array();
		var arrTbox = new Array();
		var arrLookup = new Array();
		var i;
		for (i = 0; i < tbox.options.length; i++){
			arrLookup[tbox.options[i].text] = tbox.options[i].value;
			arrTbox[i] = tbox.options[i].text;
		}
		var fLength = 0;
		var tLength = arrTbox.length;
		for(i = 0; i < fbox.options.length; i++){
			arrLookup[fbox.options[i].text] = fbox.options[i].value;				
			arrTbox[tLength] = fbox.options[i].text;
			tLength++;				
		}
		arrFbox.sort();
		arrTbox.sort();
		fbox.length = 0;
		tbox.length = 0;
		var c;
		for(c = 0; c < arrFbox.length; c++){
			var no = new Option();
			no.value = arrLookup[arrFbox[c]];
			no.text = arrFbox[c];
			fbox[c] = no;
		}
		for(c = 0; c < arrTbox.length; c++){
			var no = new Option();
			no.value = arrLookup[arrTbox[c]];
			no.text = arrTbox[c];
			tbox[c] = no;
		}
		document.getElementById(cboOrigen).value = fbox;
		document.getElementById(cboDestino).value = tbox;
	}
	function fc_QuitarTodo(cboOrigen, cboDestino){
		fbox = document.getElementById(cboDestino);
		tbox = document.getElementById(cboOrigen);
		var arrFbox = new Array();
		var arrTbox = new Array();
		var arrLookup = new Array();
		var i;
		for (i = 0; i < tbox.options.length; i++) 
		{
			arrLookup[tbox.options[i].text] = tbox.options[i].value;
			arrTbox[i] = tbox.options[i].text;
		}
		var fLength = 0;
		var tLength = arrTbox.length;
		for(i = 0; i < fbox.options.length; i++) 
		{
			arrLookup[fbox.options[i].text] = fbox.options[i].value;				
			arrTbox[tLength] = fbox.options[i].text;
			tLength++;				
		}
		arrFbox.sort();
		arrTbox.sort();
		fbox.length = 0;
		tbox.length = 0;
		var c;
		for(c = 0; c < arrFbox.length; c++) 
		{
			var no = new Option();
			no.value = arrLookup[arrFbox[c]];
			no.text = arrFbox[c];
			fbox[c] = no;
		}
		for(c = 0; c < arrTbox.length; c++) 
		{
			var no = new Option();
			no.value = arrLookup[arrTbox[c]];
			no.text = arrTbox[c];
			tbox[c] = no;
		}
		document.getElementById(cboDestino).value = fbox;
		document.getElementById(cboOrigen).value = tbox;
	}
	function fc_ObtenerCadenaCombo(combo){
		objCombo = document.getElementById(combo);
		cadena = "";							
		for(var i = 0; i < objCombo.options.length ; i ++){
			opcion = objCombo.options[i];
			if(i == "0"){
				cadena = opcion.value;
			}
			else{						
				cadena = cadena + "|" + opcion.value;
			}
		}
		return cadena;
	}
</script>
</body>