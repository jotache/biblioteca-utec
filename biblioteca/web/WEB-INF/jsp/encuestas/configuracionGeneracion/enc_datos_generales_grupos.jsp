<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
}

function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtGrupo").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("cboFormato").value = "";
	document.getElementById("txhCodigoUpdate").value = "";
	document.getElementById("txhCodigoSel").value = "";
	document.getElementById("cboFormato").disabled = false;
	document.getElementById("cboFormato").className = "combo_o";
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){
				
			document.getElementById("operacion").value="irRegistrar";
			document.getElementById("cboFormato").disabled = false;						
			document.getElementById("frmMain").submit();
		}	
	}
}

function fc_Actualizar(){

	var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_mostrarDatos(posSel){

	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtGrupo").value =  document.getElementById("hidGrupo"+posSel).value ;	
	document.getElementById("cboFormato").value = document.getElementById("hidFormato"+posSel).value;
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	document.getElementById("cboFormato").disabled = true;
	document.getElementById("cboFormato").className = "cajatexto_1";	
	
}

function fc_validaGrabar(){

	if(fc_Trim(document.getElementById("txtGrupo").value)=="")
	{	alert(mstrIngrGrupo);
		document.getElementById("txtGrupo").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboFormato").value)==""){
		alert(mstrSeleccioneFormato);
		document.getElementById("cboFormato").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngrPeso);
		document.getElementById("txtPeso").focus();
		return 0;
	}
		
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcción");
}

function fc_Sel(pos){

	document.getElementById("txhPosSel").value = pos;	 
	codGrupo = document.getElementById("hidCodigo"+pos).value;
	
	fc_MostrarPreguntasStandar(codGrupo);
}

function fc_SetNumPreg(numPreg){
	posSel = document.getElementById("txhPosSel").value;
	document.getElementById("hidPreguntasRel"+posSel).value = numPreg;
	 
}

function fc_Eliminar(){
	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{
			numPregRel = document.getElementById("hidPreguntasRel"+posSel).value;
			numAltRel = document.getElementById("hidAlternativasRel"+posSel).value;
			
			if( Number(numPregRel)>0 || Number(numAltRel)>0  ){
				alert(mstrTienePregOrAltRelacionadas);
			}else{
				if(confirm(mstrSeguroEliminar1))
				{
				 document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		 document.getElementById("operacion").value="irEliminar";
				 document.getElementById("frmMain").submit();
				}			
			}

		}
	else
		alert(mstrSeleccione);
}

function fc_MostrarPreguntasStandar(codGrupo){
	
		codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
		usuario = document.getElementById("usuario").value;
		fc_aparece("divPreguntasEstandar",'1');		
						
		document.getElementById("iframePreguntasEstandar").src 
		= "${ctx}/encuestas/datosGeneralesPreguntas.html"+"?prmCodEncuesta="+codEncuesta+"&prmCodGrupo="+codGrupo+"&prmUsuario="+usuario;
}

function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/encuestas/datosGeneralesGrupos.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />


	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />

	<!-- **************************************************************************************************** -->
	<table border="0" width="100%" bordercolor="blue" height="100%">
		<tr>
			<td width="25%" valign="top">
			
			<table class="tabla2" width="100%" cellSpacing="1" cellPadding="1"
			 border="0" bordercolor='gray' ID="Table2"  style="margin-left:3px; margin-top:3px">			  
				<tr>
					<td colspan='3' class="" style='cursor:hand;'>GRUPOS
					</td>					
				</tr>
			</table>				
			
			<table background="${ctx}/images/biblioteca/fondosup.jpg"
				style="width: 99%; margin-left: 3px" border="0" cellspacing="4"
				cellpadding="1" class="tabla" height="50px" bordercolor="red">
				<tr>
					<td class="">Grupo :</td>
					<td><form:input path="txtGrupo" id="txtGrupo" size="25"
						maxlength="50" cssClass="cajatexto_o" /></td>
				</tr>
				<tr>
					<td class="">Formato:</td>
					<td><form:select path="cboFormato" id="cboFormato"
						cssClass="combo_o" cssStyle="width:190px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstFormato!=null}">
							<form:options itemValue="idSeccion" itemLabel="nomFormato"
								items="${control.lstFormato}" />
						</c:if>
					</form:select></td>
					
				</tr>
				<tr>
					<td class="">Peso:</td>
					<td><form:input path="txtPeso" id="txtPeso" size="3"
						maxlength="3" onblur="fc_ValidaNumeroOnBlur('txtPeso');"
						onkeypress="fc_PermiteNumeros();" cssClass="cajatexto_o" /></td>
					<td><img onclick="fc_Grabar()"
						src="${ctx}/images/iconos/guardar_on.gif" alt="Grabar">&nbsp;
					<img onclick="fc_Limpiar();"
						src="${ctx}/images/iconos/cerrar_on.gif" alt="Cancelar"></td>
				</tr>
			</table>
			<table class="tabla" style="width: 100%; margin-top: 8px"
				cellSpacing="1" cellPadding="1" border="0" bordercolor='gray'
				ID="Table28">
				<tr>
					<td>
					<table cellpadding="0" cellspacing="0" align="center" border="1"
						bordercolor="white" width="100%" class="tabla">
						<tr>
							<td class="grilla" width="4%">Sel.</td>
							<td class="grilla">Grupo</td>
							<td class="grilla">Formato</td>
							<td class="grilla">Peso</td>
						</tr>



						<c:forEach var="objCast" items="${control.lstResultado}"
							varStatus="loop">
							<tr class="tablagrilla">
								<td width="4%"><input type="hidden"
									id="hidCodigo<c:out value="${loop.index}" />"
									value="<c:out value="${objCast.codGrupo}" />"
									name="hidCodigo<c:out value="${loop.index}" />" /> <input
									type="hidden" id="hidGrupo<c:out value="${loop.index}" />"
									value="<c:out value="${objCast.nomGrupo}" />"
									name="hidAlternativa<c:out value="${loop.index}" />" /> <input
									type="hidden" id="hidFormato<c:out value="${loop.index}" />"
									value="<c:out value="${objCast.codFormato}" />"
									name="hidDescripcion<c:out value="${loop.index}" />" /> 
									
									<input
									type="hidden" id="hidPeso<c:out value="${loop.index}" />"
									value="<c:out value="${objCast.pesoGrupo}" />"
									name="hidPeso<c:out value="${loop.index}" />" />
									<!-- PARA LA ELIMINACION -->
									<input
									type="hidden" id="hidPreguntasRel<c:out value="${loop.index}" />"
									value="<c:out value="${objCast.preguntasRel}" />"
									name="hidPreguntasRel<c:out value="${loop.index}" />" />
									<input
									type="hidden" id="hidAlternativasRel<c:out value="${loop.index}" />"
									value="<c:out value="${objCast.alternativasRel}" />"
									name="hidAlternativasRel<c:out value="${loop.index}" />" />									
									
									<INPUT
									class="radio" type="radio" name="radio"
									onclick="fc_Sel('<c:out value="${loop.index}" />')"></td>
								<td><c:out value="${objCast.nomGrupo}" /></td>
								<td><c:out value="${objCast.nomFormato}" /></td>
								<td align="center"><c:out value="${objCast.pesoGrupo}" /></td>
							</tr>
						</c:forEach>

					</table>
					</td>
					<td valign="top"><img onclick="fc_Actualizar()"
						src="${ctx}/images/iconos/modificar_on.gif" style="cursor: hand"><br>
					<img onclick="fc_Eliminar()"
						src="${ctx}/images/iconos/eliminar_on.gif" style="cursor: hand">
					</td>
					<TD></TD>
				</tr>
			</table>
			</td>


			<td width="75%" valign="top">
			<table width="100%" height="100%" border="0" bordercolor="black" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top"> 
					<div id="divPreguntasEstandar" style="display: none;">
					<iframe id="iframePreguntasEstandar" name="iframePreguntasEstandar"
						frameborder="0" height="275px" width="100%" >
					</iframe></div>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	<!-- **************************************************************************************************** -->
</form:form>
</body>


