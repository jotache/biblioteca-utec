<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
	function onLoad(){		
		fc_MarcarIndicador();
		fc_MostrarTipoServicio();		
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OK_GRABAR" ){		
			alert(mstrGrabar);
		}		
		else
		if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);
		}
		else
		if ( objMsg.value != 'null' &&  objMsg.value != ""){
			alert(document.getElementById("txhDscMensaje").value);
		}
		document.getElementById("txhMsg").value = "";
		document.getElementById("txhOperacion").value = "";	
	}
	function fc_MarcarIndicador(){		
		if(document.getElementById("txhIndicadorManual").value=="1"){
			document.getElementById("chkTipoEncuesta").checked=true;
			document.getElementById("chkTipoEncuesta").disabled=true;
		}
		else
		if(document.getElementById("txhIndicadorManual").value=="0"){
			document.getElementById("chkTipoEncuesta").checked=false;
			document.getElementById("chkTipoEncuesta").disabled=true;			
			fc_VerificarPerfil02();
		}
	}
	function fc_MostrarTipoServicio(){
		if(document.getElementById("txhCodTipoAplicacion").value=="0002"){
			tdTipoServicio01.style.display="";
			tdTipoServicio02.style.display="";
		}
	}
	function fc_VerificarPerfil02(){
		var perfilEncuesta=document.getElementById("txhCodPerfilEncuesta").value;
		var codTipoAplicacion=document.getElementById("txhCodTipoAplicacion").value;
		//CASO PROGRAMA
		if(codTipoAplicacion=="0001" && perfilEncuesta!=""){
			perfil=perfilEncuesta.split("|");			
			tablaTab.style.display="";
			//COMO TIENE COMO MINIMO UN PERFIL ASIGNADO 
			//SE MUESTRA ORIENTADO
			fc_CambiaBandeja(document.getElementById("txhConstOrientado").value);			
			fc_OcultarPerfiles(perfil[0]);			
		
		}
		else
		//CASO SERVICIOS
		if(codTipoAplicacion=="0002" && perfilEncuesta!=""){
			//COMO TIENE COMO MINIMO UN PERFIL ASIGNADO 
			//SE MUESTRA ORIENTADO
			tablaTab.style.display="";
			fc_CambiaBandeja(document.getElementById("txhConstOrientado").value);
		}		
	}

	function fc_VerificarPerfil_Anonimo_PCC(){
		
		var perfilEncuesta=document.getElementById("txhCodPerfilEncuesta").value;
		var codTipoAplicacion=document.getElementById("txhCodTipoAplicacion").value;
		var txhPerfil_anonimo = document.getElementById("txtTipoServicio").value;
		
		var txhPerfil_pcc_anonimo =  document.getElementById("txhConstPerfilAnonimo_PCC").value;
		// console.log('linea: PCC_ANONIMO: ' + txhPerfil_pcc_anonimo);
		// console.log('linea_68: '+perfilEncuesta + ' - ' + codTipoAplicacion + ' - ' +txhPerfil_anonimo);
		
		var conociendo_var = document.getElementById("txhConstPerfilPFC").value;
		// console.log('conociendo_variable: [dentro del else] ' + conociendo_var);
		
		if(txhPerfil_anonimo === 'An�nimo'){
			// console.log('Nuevo level - pedido txhConstPerfilAnonimo_PCC: '+txhPerfil_pcc_anonimo);
			tablaTab.style.display="";
			fc_OcultarPerfiles_anonimo(txhPerfil_pcc_anonimo);
			fc_CambiaBandeja(txhPerfil_pcc_anonimo);
		}
	}
	
	function fc_VerificarPerfil(){
		
		var perfilEncuesta=document.getElementById("txhCodPerfilEncuesta").value;
		var codTipoAplicacion=document.getElementById("txhCodTipoAplicacion").value;
		var txhPerfil_anonimo = document.getElementById("txtTipoServicio").value;
		var txhPerfil_pcc_anonimo =  document.getElementById("txhConstPerfilAnonimo_PCC").value;
		// console.log('linea: PCC_ANONIMO: ' + txhPerfil_pcc_anonimo);
		// console.log('linea_68: '+perfilEncuesta + ' - ' + codTipoAplicacion + ' - ' +txhPerfil_anonimo);
		var conociendo_var = document.getElementById("txhConstPerfilPFC").value;
		// console.log('conociendo_variable:' + conociendo_var);
		
			if(codTipoAplicacion=="0001" && perfilEncuesta!=""){
			perfil=perfilEncuesta.split("|");			
			tablaTab.style.display="";
			fc_CambiaBandeja(perfil[0]);						
			fc_OcultarPerfiles(perfil[0]);			
		
		}
		else{
			//COMO NO EXISTE UN PERFIL ASIGNADO A LA ENCUESTA 
			//SE MUESTRA EL PRIMER PERFIL PFR
			tablaTab.style.display="";
			fc_CambiaBandeja(document.getElementById("txhConstPerfilPFC").value);			
		}		
	}
	
	function fc_AgregarPerfiles(){
		var txhPerfil_anonimo = document.getElementById("txtTipoServicio").value;
		
		if(document.getElementById("txhIndicadorManual").value!=""){
			if(document.getElementById("chkTipoEncuesta").checked!=true){
				
				
				if(txhPerfil_anonimo === 'An�nimo'){
					// console.log('igualitos: ' + txhPerfil_anonimo);
					fc_VerificarPerfil_Anonimo_PCC();
				}else{
					// console.log('diferentes: ' + txhPerfil_anonimo);
					fc_VerificarPerfil();
				}
			}
			else{
				alert("No puede ingresar perfiles.");
			}
		}
		else{
			alert("Debe Grabar los datos de la encuesta.");
		}
	}	
	function fc_Grabar(){
		fc_ObtenerIndicador();
		if(document.getElementById("txtResponsable").value==""){
			alert("Debe Asignar un Responsable.");
			return;
		}
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value="GRABAR";		
			document.getElementById("frmMain").submit();
		}		
	}
	function fc_ObtenerIndicador(){
		if(document.getElementById("chkTipoEncuesta").checked==true){
			document.getElementById("txhIndicadorManual").value="1";
		}
		else{
			document.getElementById("txhIndicadorManual").value="0";
		}		
	}
	function fc_RegresarBandeja(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		/*alert("CodTipoAplicacion="+document.getElementById("txhParCodTipoAplicacion").value +
		"CodTipoEncuesta="  + document.getElementById("txhParCodTipoEncuesta").value +
		"CodTipoServicio="  + document.getElementById("txhParCodTipoServicio").value +
		"CodTipoEstado="    + document.getElementById("txhParCodTipoEstado").value +
		"ParBandera="       + document.getElementById("txhParBandera").value);*/
		window.location.href = "${ctx}/encuestas/bandeja_conf_gen_encuestas.html?txhCodUsuario="+codUsuario +
		"&txhParCodTipoAplicacion="+ document.getElementById("txhParCodTipoAplicacion").value +
		"&txhParCodTipoEncuesta="  + document.getElementById("txhParCodTipoEncuesta").value +
		"&txhParCodTipoServicio="  + document.getElementById("txhParCodTipoServicio").value +
		"&txhParCodTipoEstado="    + document.getElementById("txhParCodTipoEstado").value +
		"&txhParBandera="          + document.getElementById("txhParBandera").value ;
	}	
	function fc_BuscarEmpleado(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		Fc_Popup("${ctx}/encuestas/busqueda_empleados.html?txhCodUsuario="+codUsuario,660,460);
	}
	function fc_Aplicar(){
		/*if(codEstado==document.getElementById("txhEstadoGenerada").value){
			codUsuario=document.getElementById("txhCodUsuario").value;
			codEncuesta=document.getElementById("txhCodEncuesta").value;
			Fc_Popup("${ctx}/encuestas/aplicarProgramacionEncuesta.html?txhCodEncuesta="+codEncuesta+"&txhCodUsuario="+codUsuario,480,160);			
		}
		else{
			alert("Solo se pueden aplicar encuestas generadas.");
		}*/
		codEstado=document.getElementById("txhCodEstado").value;
		//alert(codEstado);
		if(codEstado==document.getElementById("txhEstadoEnAplicacion").value){
			alert("La Encuesta ya se encuesta aplicada y programada.");
			return;
		}
		codUsuario=document.getElementById("txhCodUsuario").value;
		codEncuesta=document.getElementById("txhCodEncuesta").value;
		Fc_Popup("${ctx}/encuestas/aplicarProgramacionEncuesta.html?txhCodEncuesta="+codEncuesta+"&txhCodUsuario="+codUsuario,500,260);
	}
	function fc_TipoEncuesta(){
		if(document.getElementById("chkTipoEncuesta").checked==true){
			divRegistrarPerfiles.style.display="none";
		}
		else{
			divRegistrarPerfiles.style.display="";			
		}		
	}
	function fc_Regresar(){
		document.location.href = "${ctx}/menuEncuesta.html"
	}
	function fc_ActualizarResponsable(codResponsable, nomResponsable){		
		document.getElementById("txhCodResponsable").value=codResponsable;
		document.getElementById("txtResponsable").value=nomResponsable;
	}
	function fc_ActualizarTotalEncuestado(totalEncuestados, dscEncuestados){
		document.getElementById("txtTotalEncuestado").value=totalEncuestados
		document.getElementById("tdEncuestados").innerText=dscEncuestados;
		
	}
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/perfilProgramacion.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="dscMensaje" id="txhDscMensaje" />	
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />	
	<!-- para la BD -->
	<form:hidden path="codEncuesta" id="txhCodEncuesta" />
	<form:hidden path="codPerfilEncuesta" id="txhCodPerfilEncuesta" />	
	<!-- para grabar la cabecera -->	
	<form:hidden path="codTipoAplicacion" id="txhCodTipoAplicacion" />
	<form:hidden path="codResponsable" id="txhCodResponsable" />
	<form:hidden path="indicadorManual" id="txhIndicadorManual" />
	<form:hidden path="dscEncuestados" id="txhDscEncuestados" />
	<form:hidden path="codEstado" id="txhCodEstado" />
	<!-- CONSTANTES -->
	<form:hidden path="estadoGenerada" id="txhEstadoGenerada" />
	<form:hidden path="estadoEnAplicacion" id="txhEstadoEnAplicacion" />
	
	<form:hidden path="constPerfilAnonimo_PCC" id="txhConstPerfilAnonimo_PCC" />
	<form:hidden path="constPerfilPFC" id="txhConstPerfilPFC" />
	<form:hidden path="constPerfilPFC" id="txhConstPerfilPFC" />
	<form:hidden path="constPerfilPCC" id="txhConstPerfilPCC" />
	<form:hidden path="constPerfilEGRESADO" id="txhConstPerfilEGRESADO" />
	<form:hidden path="constPerfilPERSONAL" id="txhConstPerfilPERSONAL" />
	<form:hidden path="constOrientado" id="txhConstOrientado" />
	<!-- parametros a devolver al regresar -->
	<form:hidden path="parCodTipoAplicacion" id="txhParCodTipoAplicacion" />
	<form:hidden path="parCodTipoEncuesta" id="txhParCodTipoEncuesta" />
	<form:hidden path="parCodTipoServicio" id="txhParCodTipoServicio" />
	<form:hidden path="parCodTipoEstado" id="txhParCodTipoEstado" />
	<form:hidden path="parBandera" id="txhParBandera" />
	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Configuraci�n de Perfil y Programaci�n de Aplicaci�n		 								 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<!-- cabecera -->
	<table cellpadding="0" cellspacing="0" style="margin-top: 4px; margin-left: 8px">
		<tr>
			<td><img src="${ctx}/images/Logistica\izq1.jpg" height="34px" id="tdProducto01"></td>			
			<td bgcolor=#048BBA class="opc_combo" height="34px" style="font-size: 12px;">
				DATOS ENCUESTA / PERFIL
			</td>
			<td><img src="${ctx}/images/Logistica\der1.jpg" height="34px" id="tdProducto03"></td>
		</tr>
	</table>
	<table border="0" bordercolor="blue" cellpadding="0" cellspacing="0" width="98%">
		<tr>
			<td width="95%">	
				<table class="tabla" style="width:98%;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
							cellspacing="6" cellpadding="0" border="0" bordercolor="red">
					<tr>
						<td width="15%">Tipo Aplicaci�n :</td>
						<td width="15%">			
							<form:input path="tipoAplicacion" id="txtTipoAplicacion"													 
								cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
								
						</td>
						<td width="15%">Tipo Encuesta :</td>
						<td width="25%">
						
							<form:input path="tipoEncuesta" id="txtTipoEncuesta"					
								cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
							
						</td>
						<td width="15%">Sede :</td>
						<td width="15%">			
							<form:input path="sede" id="txtSede"													 
								cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
								
						</td>
					</tr>
					<tr>
						<td width="15%">Nro. Encuesta :</td>
						<td width="15%">			
							<form:input path="codigo" id="txtCodigo"													 
								cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
								
						</td>
						<td width="15%">Nombre :</td>
						<td width="25%">
						
							<form:input path="nombre" id="txtNombre"					
								cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>
							
						</td>
						<td width="15%">Duraci�n :</td>
						<td width="15%">			
							<form:input path="duracion" id="txtDuracion"													 
								cssClass="cajatexto_1" cssStyle="width: 40px;text-align: right;" readonly="true"/>&nbsp;Min.
								
						</td>
					</tr>
					<tr>
						<td width="15%">Responsable :</td>
						<td colspan="3">			
							<form:input path="responsable" id="txtResponsable"													 
								cssClass="cajatexto_1" cssStyle="width: 91%" readonly="true"/>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_BuscarEmpleado();">
							</a>&nbsp;
						</td>
						<%-- <td id="tdTipoServicio01" style="display: none;">Tipo Servicio :</td>
						<td id="tdTipoServicio02" style="display: none;">			
							<form:input path="tipoServicio" id="txtTipoServicio"					
								cssClass="cajatexto_1" cssStyle="width: 96%" readonly="true"/>
						</td> --%>
						<td id="tdTipoServicio01">Tipo Servicio :</td>
						<td id="tdTipoServicio02">			
							<form:input path="tipoServicio" id="txtTipoServicio"	
								cssClass="cajatexto_1" cssStyle="width: 96%" readonly="true"/>
						</td>
						
						
					</tr>		
					<tr>
						<td colspan="2">
							<input type="checkbox" onclick="fc_TipoEncuesta();" id="chkTipoEncuesta" >
							Encuesta Manual
						</td>
						<c:choose>
							<c:when test="${control.codPerfilEncuesta == '0000|' }">
								<td></td>
								<td colspan="3"></td>							
							</c:when>
							<c:when test="${control.codPerfilEncuesta == '0000' }">
								<td></td>
								<td colspan="3"></td>							
							</c:when>
							<c:otherwise>
							
							<td>
								Total Encuestado :
							</td>
							<td colspan="3">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td nowrap="nowrap">
											<form:input path="totalEncuestado" id="txtTotalEncuestado" readonly="true"
											cssClass="cajatexto_1" cssStyle="width: 40px;text-align: right;"/>&nbsp;&nbsp;
										</td>
										<td id="tdEncuestados">
											${control.dscEncuestados}
										</td>
									</tr>
								</table>																	
							</td>
							
							</c:otherwise>
							
						</c:choose>
				
					</tr>					
				</table>
			</td>		
			<td>
				<table style="width: 100%" border="0" bordercolor="red" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonGrabar','','${ctx}/images/botones/grabar2g.jpg',1)">
							<img alt="Grabar" src="${ctx}/images/botones/grabar1g.jpg" id="botonGrabar" style="cursor:pointer" onclick="javascript:fc_Grabar();"></a>
						</td>
					</tr>
					<tr>
						<td>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgPerfiles','','${ctx}/images/botones/perfiles2g.jpg',1)">
							<img src="${ctx}/images/botones/perfiles1g.jpg" alt="Perfiles" id="imgPerfiles" onclick="fc_AgregarPerfiles();" style="cursor:pointer;"></a>
							
						</td>
					</tr>
					<tr>
						<td>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAplicar','','${ctx}/images/botones/aplicar2g.jpg',1)">
							<img src="${ctx}/images/botones/aplicar1g.jpg" onclick="fc_Aplicar();" style="cursor:pointer" id="imgAplicar"></a>							
						</td>
					</tr>
					<tr>
						<td>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonRegresar','','${ctx}/images/botones/regresar2g.jpg',1)">
							<img alt="Regresar" src="${ctx}/images/botones/regresar1g.jpg" id="botonRegresar" onclick="javascript:fc_RegresarBandeja();" style="cursor:pointer;"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<div id="divRegistrarPerfiles">
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" height="34px" 
		style="margin-left:8px;margin-top: 4px;display: none;" id="tablaTab">
		<tr>
			<!-- PESTA�A ANONIMO - PCC -->
			<td>
				<table cellpadding="0" cellspacing="0" id="pesta�aANONIMO_PCC">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq2.jpg" height="34px" id="tdANONIMO_PCC_01"></td>
						<td bgcolor=#00A2E4 class="opc_combo" onclick="javascript:fc_CambiaBandeja('0000');" style="cursor: pointer;font-size: 12px;" id="tdANONIMO_PCC_02">AN�NIMO</td>
						<td><img src="${ctx}/images/Logistica\der2.jpg" height="34px" id="tdANONIMO_PCC_03"></td>
					</tr>
				</table>
			</td>
			
			<!--  -->
			<td>
				<table cellpadding="0" cellspacing="0" id="pesta�aPFR">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq1.jpg" height="34px" id="tdPFR01"></td>
						<td bgcolor=#048BBA class="opc_combo" onclick="javascript:fc_CambiaBandeja('0001');" height="34px" style="cursor: pointer;font-size: 12px;" id="tdPFR02">ALUMNOS PFR</td>
						<td><img src="${ctx}/images/Logistica\der1.jpg" height="34px" id="tdPFR03"></td>
					</tr>
				</table>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" id="pesta�aPCC">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq2.jpg" height="34px" id="tdPCC01"></td>
						<td bgcolor=#00A2E4 class="opc_combo" onclick="javascript:fc_CambiaBandeja('0002');" style="cursor: pointer;font-size: 12px;" id="tdPCC02">ALUMNOS PCC</td>
						<td><img src="${ctx}/images/Logistica\der2.jpg" height="34px" id="tdPCC03"></td>
					</tr>
				</table>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" id="pesta�aEGRESADO">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq2.jpg" height="34px" id="tdEGRESADOS01"></td>
						<td bgcolor=#00A2E4 class="opc_combo" onclick="javascript:fc_CambiaBandeja('0003');" style="cursor: pointer;font-size: 12px;" id="tdEGRESADOS02">EGRESADOS</td>
						<td><img src="${ctx}/images/Logistica\der2.jpg" height="34px" id="tdEGRESADOS03"></td>
					</tr>
				</table>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" id="pesta�aPERSONAL">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq2.jpg" height="34px" id="tdPERSONAL01"></td>
						<td bgcolor=#00A2E4 class="opc_combo" onclick="javascript:fc_CambiaBandeja('0004');" style="cursor: pointer;font-size: 12px;" id="tdPERSONAL02">PERSONAL TECSUP</td>
						<td><img src="${ctx}/images/Logistica\der2.jpg" height="34px" id="tdPERSONAL03"></td>
					</tr>
				</table>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" id="pesta�aORIENTADO">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq2.jpg" height="34px" id="tdORIENTADO01"></td>
						<td bgcolor=#00A2E4 class="opc_combo" onclick="javascript:fc_CambiaBandeja('0005');" style="cursor: pointer;font-size: 12px;" id="tdORIENTADO02">ORIENTADO A:</td>
						<td><img src="${ctx}/images/Logistica\der2.jpg" height="34px" id="tdORIENTADO03"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>	
	<iframe id="iFrameRegistrarPerfiles" name="iFrameRegistrarPerfiles" frameborder="0" height="280px" width="100%" 
		style="margin-top: 4px;" scrolling="no">
	</iframe>
	</div>
</form:form>
<script language="javascript">

	function fc_CambiaBandeja(strParametro){	

		var txhPerfil_anonimo = document.getElementById("txtTipoServicio").value;
		if(txhPerfil_anonimo === 'An�nimo'){
			// console.log('igualitos: ' + txhPerfil_anonimo);
		}else{
			
			// console.log('diferentes: ' + txhPerfil_anonimo);
			pesta�aANONIMO_PCC.style.display="none";
				
		}
		
		var txhPerfil = document.getElementById("txhConstPerfilPFC").value;
		// console.log('valor_encontrado= '+txhPerfil);
		// console.log('STR_PARAMETRO_GET: '+ strParametro);

		switch (strParametro){
			
		case document.getElementById("txhConstPerfilAnonimo_PCC").value:
			
			//PERFIL_ANONIMO_PCC
			document.getElementById("tdANONIMO_PCC_01").src="${ctx}/images/Logistica/izq2.jpg";
			document.getElementById("tdANONIMO_PCC_02").bgColor="#00A2E4";
			document.getElementById("tdANONIMO_PCC_02").style.fontSize=12;				
			document.getElementById("tdANONIMO_PCC_03").src="${ctx}/images/Logistica/der2.jpg";
			
			//LLAMAMOS A LA PAGINA POR EL IFRAME
			codEncuesta = document.getElementById("txhCodEncuesta").value;
			codUsuario = document.getElementById("txhCodUsuario").value;
			codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
			codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
			codTipoEncuesta= document.getElementById("txtTipoEncuesta").value;
			var codigoTipoEncuesta="";
			
			if(codTipoEncuesta == 'Est�ndar'){
				codigoTipoEncuesta='0001';
			}else
				if (codTipoEncuesta == 'Mixta'){
				codigoTipoEncuesta='0002';
			}
			
			// console.log('codEncuesta= '+codEncuesta);
			// console.log('codUsuario= '+codUsuario);
			// console.log('codPerfilEncuesta= '+codPerfilEncuesta);
			// console.log('codTipoAplicacion= '+codTipoAplicacion);
			// console.log('txtTipoEncuesta= '+codTipoEncuesta);
			// console.log('txhCodTipoEncuesta= '+codigoTipoEncuesta);
			
			document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAnonimoPCC.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodTipoAplicacion="+codTipoAplicacion+
					"&txtTipoEncuesta="+codTipoEncuesta +
					"&txhCodTipoEncuesta="+codigoTipoEncuesta;
			break;
			
			/*document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAlumnoPCC.html" +
				"?txhCodEncuesta="+codEncuesta+
				"&txhCodUsuario="+codUsuario+
				"&txhCodPerfilEncuesta="+codPerfilEncuesta+
				"&txhCodTipoAplicacion="+codTipoAplicacion; */				
			
			case document.getElementById("txhConstPerfilPFC").value:
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdPFR02").bgColor="#00A2E4";
				document.getElementById("tdPFR02").style.fontSize=12;				
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der2.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPCC02").bgColor="#048BBA";
				document.getElementById("tdPCC02").style.fontSize=10;		
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
				document.getElementById("tdEGRESADOS02").style.fontSize=10;			
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#048BBA";
				document.getElementById("tdPERSONAL02").style.fontSize=10;		
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAlumnoPFR.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;				
				break;
			case document.getElementById("txhConstPerfilPCC").value:
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPFR02").bgColor="#048BBA";
				document.getElementById("tdPFR02").style.fontSize=10;
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdPCC02").bgColor="#00A2E4";
				document.getElementById("tdPCC02").style.fontSize=12;			
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der2.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
				document.getElementById("tdEGRESADOS02").style.fontSize=10;			
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#048BBA";
				document.getElementById("tdPERSONAL02").style.fontSize=10;		
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAlumnoPCC.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;
				break;
			case document.getElementById("txhConstPerfilEGRESADO").value:
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPFR02").bgColor="#048BBA";
				document.getElementById("tdPFR02").style.fontSize=10;
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPCC02").bgColor="#048BBA";
				document.getElementById("tdPCC02").style.fontSize=10;			
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#00A2E4";
				document.getElementById("tdEGRESADOS02").style.fontSize=12;				
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der2.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#048BBA";
				document.getElementById("tdPERSONAL02").style.fontSize=10;			
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;	
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilAlumnoEgresado.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;				
				break;
			case document.getElementById("txhConstPerfilPERSONAL").value:						
				//PFR
				document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPFR02").bgColor="#048BBA";
				document.getElementById("tdPFR02").style.fontSize=10;				
				document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
				//PCC		
				document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdPCC02").bgColor="#048BBA";
				document.getElementById("tdPCC02").style.fontSize=10;			
				document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
				//EGRESADOS
				document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
				document.getElementById("tdEGRESADOS02").style.fontSize=10;
				document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
				//PERSONAL
				document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq2.jpg";
				document.getElementById("tdPERSONAL02").bgColor="#00A2E4";
				document.getElementById("tdPERSONAL02").style.fontSize=12;			
				document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der2.jpg";
				//ORIENTADO
				document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq1.jpg";
				document.getElementById("tdORIENTADO02").bgColor="#048BBA";
				document.getElementById("tdORIENTADO02").style.fontSize=10;			
				document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der1.jpg";
				//LLAMAMOS A LA PAGINA POR EL IFRAME
				codEncuesta = document.getElementById("txhCodEncuesta").value;
				codUsuario = document.getElementById("txhCodUsuario").value;
				codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
				codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
				document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/perfilPersonalTecsup.html"+
					"?txhCodEncuesta="+codEncuesta+
					"&txhCodUsuario="+codUsuario+
					"&txhCodPerfilEncuesta="+codPerfilEncuesta+
					"&txhCodTipoAplicacion="+codTipoAplicacion;
				break;
			case document.getElementById("txhConstOrientado").value:
				if(document.getElementById("txhCodPerfilEncuesta").value!=""){
					//PFR
					document.getElementById("tdPFR01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdPFR02").bgColor="#048BBA";
					document.getElementById("tdPFR02").style.fontSize=10;
					document.getElementById("tdPFR03").src="${ctx}/images/Logistica/der1.jpg";
					//PCC		
					document.getElementById("tdPCC01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdPCC02").bgColor="#048BBA";
					document.getElementById("tdPCC02").style.fontSize=10;			
					document.getElementById("tdPCC03").src="${ctx}/images/Logistica/der1.jpg";
					//EGRESADOS
					document.getElementById("tdEGRESADOS01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdEGRESADOS02").bgColor="#048BBA";
					document.getElementById("tdEGRESADOS02").style.fontSize=10;			
					document.getElementById("tdEGRESADOS03").src="${ctx}/images/Logistica/der1.jpg";
					//PERSONAL
					document.getElementById("tdPERSONAL01").src="${ctx}/images/Logistica/izq1.jpg";
					document.getElementById("tdPERSONAL02").bgColor="#048BBA";
					document.getElementById("tdPERSONAL02").style.fontSize=10;
					document.getElementById("tdPERSONAL03").src="${ctx}/images/Logistica/der1.jpg";
					//ORIENTADO
					document.getElementById("tdORIENTADO01").src="${ctx}/images/Logistica/izq2.jpg";
					document.getElementById("tdORIENTADO02").bgColor="#00A2E4";
					document.getElementById("tdORIENTADO02").style.fontSize=12;								
					document.getElementById("tdORIENTADO03").src="${ctx}/images/Logistica/der2.jpg";
					//LLAMAMOS A LA PAGINA POR EL IFRAME
					fc_OrientacionPerfil();
				}
				else{
					alert("Debe registrar un perfil");
					return;
				}				
				break;			
		}				
	}
	function fc_OrientacionPerfil(){
		codEncuesta = document.getElementById("txhCodEncuesta").value;
		codUsuario = document.getElementById("txhCodUsuario").value;
		codPerfilEncuesta = document.getElementById("txhCodPerfilEncuesta").value;
		codTipoAplicacion = document.getElementById("txhCodTipoAplicacion").value;
		document.getElementById("iFrameRegistrarPerfiles").src="${ctx}/encuestas/orientacionPerfil.html"+
			"?txhCodEncuesta="+codEncuesta+
			"&txhCodUsuario="+codUsuario+
			"&txhCodPerfilEncuesta="+codPerfilEncuesta+
			"&txhCodTipoAplicacion="+codTipoAplicacion;
	}
	
	
	function fc_OcultarPerfiles_anonimo(opcion){
		// console.log('Ingresando al metodo perfil Anonimo PCC');

		switch(opcion){
		case document.getElementById("txhConstPerfilAnonimo_PCC").value:
			pesta�aPFR.style.display="none";
			pesta�aPCC.style.display="none";
			pesta�aEGRESADO.style.display="none";
			pesta�aPERSONAL.style.display="none";
			pesta�aORIENTADO.style.display="none";
			
			break;
		}
		
		// console.log('**************************************');
	}
	
	function fc_OcultarPerfiles(opcion){
		NombreObtenido = document.getElementById("txtTipoServicio").value;
		// console.log('RESPUESTA= ' + NombreObtenido);
		
			switch(opcion){
			case document.getElementById("txhConstPerfilPFC").value:
				pesta�aANONIMO_PCC.style.display="none";
				pesta�aPFR.style.display="";
				pesta�aPCC.style.display="none";
				pesta�aEGRESADO.style.display="none";
				pesta�aPERSONAL.style.display="none";
				break;
			case document.getElementById("txhConstPerfilPCC").value:
				pesta�aANONIMO_PCC.style.display="none";
				pesta�aPFR.style.display="none";
				pesta�aPCC.style.display="";
				pesta�aEGRESADO.style.display="none";
				pesta�aPERSONAL.style.display="none";
				break;
			case document.getElementById("txhConstPerfilEGRESADO").value:
				pesta�aANONIMO_PCC.style.display="none";
				pesta�aPFR.style.display="none";
				pesta�aPCC.style.display="none";
				pesta�aEGRESADO.style.display="";
				pesta�aPERSONAL.style.display="none";
				break;
			case document.getElementById("txhConstPerfilPERSONAL").value:
				pesta�aANONIMO_PCC.style.display="none";
				pesta�aPFR.style.display="none";
				pesta�aPCC.style.display="none";
				pesta�aEGRESADO.style.display="none";
				pesta�aPERSONAL.style.display="";
				break;
		}
	}	
</script>
</body>