<%@ include file="/taglibs.jsp"%>

<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<script language="javascript">
	function onLoad(){
		msg=document.getElementById("txhMsg").value;
		
		if(msg=="OK_GRABAR"){
			alert(mstrGrabar);
			window.opener.fc_RegresarBandeja();
			window.close();
		}
		else
		if(msg=="ERROR_GRABAR"){
			alert(mstrProblemaGrabar);
		}
		else
		if ( msg != 'null' &&  msg != ""){
			alert(document.getElementById("txhDscMensaje").value)
		}
		else{
			fc_AsignarFechaHoraActual();	
		}
		document.getElementById("txhOperacion").value="";
		document.getElementById("txhMsg").value="";
	}
	function fc_AsignarFechaHoraActual(){		
		document.getElementById("txtFechaVigenciaIni").value=document.getElementById("txhFechaActual").value;
		horaActual = document.getElementById("txhHoraActual").value;
		document.getElementById("txtHoraVigenciaIni").value=document.getElementById("txhHoraActual").value;		
	}
	function fc_Grabar(){
		if(fc_Valida()){
			if(confirm(mstrSeguroGrabar)){
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
	}	
	function fc_FechaMayor(opcion){
		fechaIni=document.getElementById("txtFechaVigenciaIni").value;
		fechaFin=document.getElementById("txtFechaVigenciaFin").value;
		if(fechaIni!="" && fechaFin!=""){
			if(fechaIni > fechaFin){
				alert("La fecha de vigencia inicial debe ser menor/no igual a la fecha de vigencia final.");
				if(opcion=='1'){
					document.getElementById("txtFechaVigenciaIni").value="";
					document.getElementById("txtFechaVigenciaIni").focus();
				}
				else{
					document.getElementById("txtFechaVigenciaFin").value="";
					document.getElementById("txtFechaVigenciaFin").focus();
				}
				return false;
			}
		}
	}
	function fc_ValidaFechaIngresada(idFecha,idHora){		
		objFecha = document.getElementById(idFecha);
		if (objFecha.value== ""){
			window.event.returnValue=0;
			alert("Antes de ingresar la hora, debe ingresar la fecha.");		
			document.getElementById(idHora).focus();
		}
	}
	function fc_Valida(){
		fecha1=document.getElementById("txtFechaVigenciaIni").value;
		fecha2=document.getElementById("txtFechaVigenciaFin").value;
		hora1=document.getElementById("txtHoraVigenciaIni").value;
		hora2=document.getElementById("txtHoraVigenciaFin").value;
		if(fecha1 < document.getElementById("txhFechaActual").value){
			alert("La Fecha inicial debe ser mayor o igual a la fecha Actual.");
			document.getElementById("txtFechaVigenciaIni").focus();
			return false;
		}
		horaActual=document.getElementById("txhHoraActual").value;		
		if(hora1 < horaActual){
			alert("La hora inicial debe ser mayor o igual a la hora Actual.");
			document.getElementById("txtHoraVigenciaIni").focus();
			return false;
		}
		if(fecha1==""){
			alert(mstrFechaInicio);
			document.getElementById("txtFechaVigenciaIni").focus();
			return false;
		}
		if(hora1==""){
			alert("Debe Seleccionar la hora inicial.");
			document.getElementById("txtHoraVigenciaIni").focus();
			return false;
		}
		if(fecha2==""){
			alert(mstrFechaFin);
			document.getElementById("txtFechaVigenciaFin").focus();
			return false;
		}
		if(hora2==""){
			alert("Debe Seleccionar la hora final.");
			document.getElementById("txtHoraVigenciaFin").focus();
			return false;
		}
		if(fc_ValidaFechaIniFechaFin2(fecha2,fecha1)!="1"){
			alert(mstrFechaReservaMayor);
			return false;
		}
		if(fecha1==fecha2){			
			if(fc_ValidaRangoHora("txtHoraVigenciaIni","txtHoraVigenciaFin","1")==false){							
				return false;
			}		
		}
		return true;
	}
</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/aplicarProgramacionEncuesta.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="dscMensaje" id="txhDscMensaje" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	
	<form:hidden path="codEncuesta" id="txhCodEncuesta" />
	<form:hidden path="fechaActual" id="txhFechaActual" />
	<form:hidden path="horaActual" id="txhHoraActual" />	
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="300px" class="opc_combo">
		 		<font style="">
		 			Programación de Encuesta					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<!-- cabecera -->			
	<table class="tabla" style="width:97%;margin-left:6px;margin-top: 4px;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="6" cellpadding="0" border="0" bordercolor="red">
		<tr>
			<td width="35%">Fecha/Hora Inicio Aplicación :</td>
			<td width="65%">
				<form:input path="fechaVigenciaIni" id="txtFechaVigenciaIni"
					cssClass="cajatexto_o" size="10" maxlength="10"				
					onkeypress="fc_ValidaFecha('txtFechaVigenciaIni');"
					onblur="fc_FechaOnblur('txtFechaVigenciaIni');fc_FechaMayor('1');"/>
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaVigenciaIni','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaVigenciaIni" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>
				<form:select path="horaVigenciaIni" id="txtHoraVigenciaIni" cssClass="cajatexto_o"
					cssStyle="width:60px">
					<form:option value="">-Hora-</form:option>
					<c:if test="${control.listaHoraInicio!=null}">
					<form:options itemValue="id" itemLabel="name"
						items="${control.listaHoraInicio}" />
					</c:if>
				</form:select>&nbsp;hrs.				
			</td>
		</tr>
		<tr>
			<td width="35%">Fecha/Hora Fin Aplicación :
			</td>
			<td width="65%">
				<form:input path="fechaVigenciaFin" id="txtFechaVigenciaFin"
					cssClass="cajatexto_o" size="10" maxlength="10"				
					onkeypress="fc_ValidaFecha('txtFechaVigenciaFin');"
					onblur="fc_FechaOnblur('txtFechaVigenciaFin');fc_FechaMayor('2');"/>
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaVigenciaFin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaVigenciaFin" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>
				<form:select path="horaVigenciaFin" id="txtHoraVigenciaFin" cssClass="cajatexto_o"
					cssStyle="width:60px">
					<form:option value="">-Hora-</form:option>
					<c:if test="${control.listaHoraFin!=null}">
					<form:options itemValue="id" itemLabel="name"
						items="${control.listaHoraFin}" />
					</c:if>
				</form:select>&nbsp;hrs.								
			</td>
		</tr>									
	</table>
	<br>
	<br>
	<br>	
	<br>
	<br>
	<table cellpadding="2" cellspacing="2" align="center" width="100%">
		<tr>
			<td align=center>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="botonGrabar" style="cursor:pointer" onclick="javascript:fc_Grabar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="botonCancelar" style="cursor:pointer" onclick="window.close();"></a>				
			</td>
		</tr>
	</table>
					
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFechaVigenciaIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaVigenciaIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFechaVigenciaFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaVigenciaFin",
		singleClick    :    true
	});		
</script>
</body>