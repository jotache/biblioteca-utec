<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.bean.DatosCabeceraBean'%>
<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
}
</script>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/visualizarEncuestaMixta.html">
		
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />
	<!-- CARACTERISTICAS DE LA ENCUESTA -->
	<form:hidden path="txhCodPerfil" id="txhCodPerfil" />
	<form:hidden path="txhCodEncuestado" id="txhCodEncuestado" />
	<form:hidden path="txhCodProfesor" id="txhCodProfesor" />
	
	<input type="hidden" name="tknCodPreguntaCerrada" id="tknCodPreguntaCerrada" />
	<input type="hidden" name="tknCodRespuestaCerrada" id="tknCodRespuestaCerrada" />
	<input type="hidden" name="tknNumPregCerrada" id="tknNumPregCerrada" />
	<input type="hidden" name="tknCodPreguntaAbierta" id="tknCodPreguntaAbierta" />
	<input type="hidden" name="tknCodRespuestaAbierta" id="tknCodRespuestaAbierta" />
	<input type="hidden" name="tknNumPregAbierta" id="tknNumPregAbierta" />	

	<%int varTamanio = 0; %>
	<%String tam = "0";%>
	<table width="98%" border="0" align="center" style="margin-top: 10px" cellpadding="0" cellspacing="0" bordercolor="red">
		<tr>
			<td align="right">				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','/SGA/images/iconos/cerrar2.jpg',1)">
				<img src="/SGA/images/iconos/cerrar1.jpg" onclick="javascript:window.close();" style="cursor:pointer;" id="imgCerrar" alt="Cerrar Ventana"></a>
			</td>
		</tr>
	</table>
	<%DatosCabeceraBean bean= (DatosCabeceraBean) request.getAttribute("OBJ_CABECERA"); %>
	<%if(bean!=null ){ %>
	<table cellpadding="0" cellspacing="2" align="center" 
			style="width:98%;margin-top: 8px; margin-bottom:5px;border: 1px solid #048BBA">
		<TR class="tablagrilla" style="height: 20px;">
			<TD style="width: 20%;"><b>&nbsp;Tipo Aplicación :</b></TD>
			<TD style="width: 40%;">&nbsp;<%=bean.getNomTipoAplicacion()%></TD>
			<TD style="width: 20%;"><b>&nbsp;Tipo Encuesta :</b></TD>
			<TD style="width: 20%;">&nbsp;<%=bean.getNomTipoEncuesta()%></TD>
		</TR>		
		<TR class="tablagrilla" style="height: 20px;">
			<TD style="width: 20%;"><b>&nbsp;Encuesta :</b></TD>
			<TD style="width: 40%;">&nbsp;<%=bean.getNomEncuesta()%></TD>
			<TD style="width: 20%;"><b>&nbsp;Duracion :</b></TD>
			<TD style="width: 20%;">&nbsp;<%=bean.getDuracion()%>&nbsp;Min</TD>
		</TR>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0002".equalsIgnoreCase(bean.getCodTipoAplicacion()) ){%>
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado a :</b></TD>
			<TD>&nbsp;<%=bean.getNomServicio()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
		<%if( bean!=null &&  bean.getCodTipoAplicacion()!=null && "0001".equalsIgnoreCase(bean.getCodTipoAplicacion()) && bean.getNomProfesor()!=null && !bean.getNomProfesor().equals("")){%>		
		<TR class="tablagrilla" style="height: 20px;">
			<TD><b>&nbsp;Aplicado al profesor :</b></TD>
			<TD>&nbsp;<%=bean.getNomProfesor()%></TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<%}%>
	</table>	
	<%}%>

<table cellpadding="0" cellspacing="2" align="center" 
	style="width:98%;margin-bottom:5px;border: 1px solid #048BBA">
<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{
		
		List consulta = (List) request.getAttribute("LST_RESULTADO");	
		
		varTamanio = consulta.size();
		
		String guia = "";
		String grupoVariable = "";
		String formatoVariable = "";
		VerEncuestaBean objFuturo =null;
		String grupoFutura = "";
		String formatoFutura = "";
		String preguntaVariable = "";
		String preguntaFutura = "";
		
		for (int i = 0; i < consulta.size(); i++)
		{		
			VerEncuestaBean obj = (VerEncuestaBean) consulta.get(i);			
			grupoVariable = obj.getIdGrupo();
			formatoVariable = obj.getIdSeccion();
			preguntaVariable = obj.getIdPregunta();
%>

		<!-- PINTAMOS EL INICIO FORMATO Y GRUPO -->
		
		<%
		if( i==0 ){%>
				<tr class="tablagrilla" style="height: 20px;">
					<td  colspan="1" style="width: 40%;" align="left">&nbsp;(*): Preguntas de caracter obligatorio.</td>
					<td  colspan="1" style="width: 60%;">&nbsp;</td>
				</tr>						
				<tr class="grilla" style="height: 20px;">
					<td  colspan="2"  align="left">&nbsp;<b><%=obj.getNomSeccion()%></b></td>
				</tr>			
				<tr class="tablagrilla" style="height: 20px;">
					<td  bgcolor="#C8E8F0" colspan="2"><b>&nbsp;&nbsp;&nbsp;<%=obj.getNomGrupo()%></b></td>
									
				</tr>
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td valign="top">
					<table width="100%" height="100%" bordercolor="red" border="0" cellpadding="0" cellspacing="0"><tr><td>
						<%for (int j = 0; j < obj.getLstAlternativas().size(); j++){
							VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
						%>	<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%>					
							<b><%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
							<%}%>
						<%}%>
					</td></tr></table>
					</td>
				</tr>
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->				
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1"  width="<%=tam%>%;">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="blue" border="0" cellpadding="0" cellspacing="0"><tr>					
						<%
						int ancho=0;
						int nroAlternativas=obj.getLstAlternativas().size();
						ancho=100/nroAlternativas;
						for (int j = 0; j < obj.getLstAlternativas().size(); j++){
							VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
						%>							
							<td align="center" width="<%=ancho%>%"><%=alt.getNomAlterntiva()%></td>
						<%}%>
					</tr></table>
					</td>
				</tr>
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->					
		<%}%>
		
		
		
		
		<!-- ************* PINTAMOS LOS REGISTROS INTERMEDIOS *************** -->
		<%if( i+1<consulta.size() )
		{	objFuturo = (VerEncuestaBean) consulta.get(i+1);
			grupoFutura = objFuturo.getIdGrupo();
			formatoFutura = objFuturo.getIdSeccion();
			preguntaFutura = objFuturo.getIdPregunta();		
			%>			
			<!--***************************** PINTA LAS PREGUNTA Y SUS PROPIEDASDES *****************************-->
			<tr class="tablagrilla">
				<!--***************** DATOS DE LA PREGUNTA ***************** -->
				<input type="hidden" id="hidFlgObligatorio<%=i%>"  value="<%=obj.getIndObligatorio()%>"  />
				<input type="hidden" id="hidFlgCombo<%=i%>"  		value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
				<input type="hidden" id="hidFlgMultiple<%=i%>"  	value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
				<input type="hidden" id="hidCodPregunta<%=i%>"  	value="<%=obj.getIdPregunta()%>"  />
				<input type="hidden" id="txhTamanioOpciones<%=i%>"  	value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  /> 
										
				<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>				
				
				<!-- ABIERTA O CERRRADA -->
				<td>
				<table width="100%" height="100%" bordercolor="black" border="0" cellpadding="0" cellspacing="0"><tr>				
				
				<%if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>			
					<td colspan="<%=obj.getLstAlternativas().size()%>"><textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>"    class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto"%>"   style="width: 98%;height:50px;"></textarea></td>
				<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>				
					<%for (int j = 0; j < obj.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);%>
				
						<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
							<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
							<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}%>
						
					<%}%>					
				<%}%>
				</tr></table>
				</td>
				<!-- FIN ABIERTA O CERRRADA -->
			</tr>
		<!--***************************** FIN PINTA LAS PREGUNTA *****************************-->			
			
				
				
				
			<%
			//PINTA LAS CABECERAS SIGUIENTES FORMATO  Y GRUPO
			if(!formatoVariable.equalsIgnoreCase(formatoFutura)){%>
				<!-- ****************PINTA FORMATO ****************-->
				<tr class="grilla" style="height: 20px;">				
					<td colspan="2" align="left">&nbsp;<b><%=objFuturo.getNomSeccion()%></b></td>
					
				</tr>				
				<!-- *****************PINTA GRUPO ****************-->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="2"  bgcolor="#C8E8F0"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>
					
				</tr>
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					
					<table width="100%" height="100%" bordercolor="blue" border="0" cellpadding="0" cellspacing="0">
					<tr><td>
					<%for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%> 
						<b><%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
						<%}%>
						
					<%}%>
					</td></tr>
					</table>
					</td>
				</tr>				
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
					<tr>					
					<%
					int ancho2=0;
					int nroAlternativas2=objFuturo.getLstAlternativas().size();
					ancho2=100/nroAlternativas2;
					for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<td  align="center" width="<%=ancho2%>%"><%=alt.getNomAlterntiva()%></td>
					<%}%>
					</tr></table>
					</td>
				</tr>
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->				
			<%
			}else if(!grupoVariable.equalsIgnoreCase(grupoFutura))
			{/**PINTA LAS CABECERAS SIGUIENTES GRUPO*/%>
				<!-- *****************PINTA GRUPO ****************-->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="2"  bgcolor="#C8E8F0"><b>&nbsp;&nbsp;&nbsp;<%=objFuturo.getNomGrupo()%></b></td>
					
				</tr>				
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
					<tr><td>
					<%for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%>
						<b><%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
						<%}%>
					<%}%>
					</td></tr></table>
					</td>
				</tr>				
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->				
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
					<tr>
					<%
					int ancho3=0;
					int nroAlternativas3=objFuturo.getLstAlternativas().size();
					ancho3=100/nroAlternativas3;
					for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%>
						<td align="center" width="<%=ancho3%>%"><%=alt.getNomAlterntiva()%></td>
					<%}%>
					</tr></table>
					</td>
				</tr>
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->				
			<%						
			}else if(grupoVariable.equalsIgnoreCase(grupoFutura)){
				if(objFuturo.getNivelAlternativa().equalsIgnoreCase("2")){%>
				
				<!-- *********************LEYENDA DE LAS ALTERNATIVAS********************* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
					<table width="100%" height="100%" bordercolor="orange" border="0" cellpadding="0" cellspacing="0">
					<tr><td>
					<%for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
					%><b>
						<%if(!"0".equalsIgnoreCase(alt.getIdAlternativa())){%> 
						<%=alt.getNomAlterntiva()%>:</b><%=alt.getDesAlternativa()%><br/>
						<%}%>
					<%}%>
					</td></tr></table>
					</td>
				</tr>				
				<!-- ********************* FIN LEYENDA DE LAS ALTERNATIVAS********************* -->				
				<!--********** PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
				<tr class="tablagrilla" style="height: 20px;">
					<td colspan="1">&nbsp;</td>
					<td>
						<table width="100%" height="100%" bordercolor="blue" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<%
						int ancho4=0;
						int nroAlternativas4=objFuturo.getLstAlternativas().size();
						ancho4=100/nroAlternativas4;
						for (int j = 0; j < objFuturo.getLstAlternativas().size(); j++){
							VerEncuestaBean alt = (VerEncuestaBean) objFuturo.getLstAlternativas().get(j);
						%>
							<td align="center" width="<%=ancho4%>%"><%=alt.getNomAlterntiva()%></td>
						<%}%>
						</tr>
						</table>
					</td>
				</tr>				
				<!--********** FIN PINTA LOS NOMBRES DE LAS ALTERNATIVAS********* -->
					
				<%}
			}
			// FIN PINTA LAS CABECERAS SIGUIENTES
			
		}
		
		
		
		
		
		///*************PINTAMOS EL ULTIMO REGISTRO*************** 
		if( i+1==consulta.size()){
		%>
		<!--***************************** PINTA LAS PREGUNTA *****************************-->
			<tr class="tablagrilla">
				<!--***************** DATOS DE LA PREGUNTA ***************** -->
				<input type="hidden" id="hidFlgObligatorio<%=i%>"  value="<%=obj.getIndObligatorio()%>"  />
				<input type="hidden" id="hidFlgCombo<%=i%>"  		value="<%="0001".equalsIgnoreCase(obj.getTipoPregunta())?"0":"1"%>"  />
				<input type="hidden" id="hidFlgMultiple<%=i%>"  	value="<%="1".equalsIgnoreCase(obj.getIndUnica())?"0":"1"%>"  /> 
				<input type="hidden" id="hidCodPregunta<%=i%>"  	value="<%=obj.getIdPregunta()%>"  />
				<input type="hidden" id="txhTamanioOpciones<%=i%>"  	value="<%=obj.getLstAlternativas()==null?"0":obj.getLstAlternativas().size()%>"  />
				
				<td colspan="1" style="padding-left: 20px;"><p align="justify"><%=i+1+"-"+obj.getNomPregunta()%><%="1".equalsIgnoreCase(obj.getIndObligatorio())?"(*)":""%></p></td>										
				
				<td>
				<table width="100%" height="100%" bordercolor="green" border="0" cellpadding="0" cellspacing="0">
				<tr>					

				<%				
				if("0001".equalsIgnoreCase(obj.getTipoPregunta())){%>				
				
					<td colspan="<%=obj.getLstAlternativas().size()%>">
					<textarea id="txhOpciones<%=i%>" name="txhOpciones<%=i%>"    class="<%="1".equalsIgnoreCase(obj.getIndObligatorio())?"cajatexto_o":"cajatexto" %>" style="width:98%;height:50px;"></textarea>
					</td>
					
				<%}else if("0002".equalsIgnoreCase(obj.getTipoPregunta())){%>
				
					<%/**FOR**/				
					for (int j = 0; j < obj.getLstAlternativas().size(); j++){
						VerEncuestaBean alt = (VerEncuestaBean) obj.getLstAlternativas().get(j);
					%>
				
						<%if( "1".equalsIgnoreCase(obj.getIndUnica()) ){%>
							<td align="center"><input type="radio" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}else if("0".equalsIgnoreCase(obj.getIndUnica())){%>
							<td align="center"><input type="checkbox" id="txhOpciones<%=i%><%=j%>" name="txhOpciones<%=i%>" value="<%=alt.getIdAlternativa()%>" /></td>
						<%}%>
					
					<%}
					/**FOR**/%>
				<%}%>
				</tr>
				</table>
				</td>
			</tr>
		<!--***************************** /PINTA LAS PREGUNTA *****************************-->	
		<%}
	}/*FOR*/		
  }/*IF*/
%>
</table>	
<!--CANTIDAD DE PREGUNTAS-->
<input type="hidden" name="txhTamanio" id="txhTamanio" value="<%=varTamanio%>" />
</form:form>
</body>


