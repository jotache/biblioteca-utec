<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);	
	document.getElementById("txtPeso").value = "1";		
}

function fc_SetNumPreg(numPreg){
	posSel = document.getElementById("txhPosSel").value;
	document.getElementById("hidPreguntasRel"+posSel).value = numPreg;	 
}

function fc_Alternativa(){
	var posSel = document.getElementById("txhPosSel").value;
	
	 
	
	if(fc_Trim(posSel)!="") 
		{
			if(document.getElementById("hidAlternativa"+posSel).value=="1")
			{	codGrupo = document.getElementById("hidCodigo"+posSel).value;
				fc_MostrarAlternativasMixta(codGrupo);
			}else{
				alert("No se puede realizar esta acci�n");
			}		
		}
	else
		alert(mstrSeleccione);
}

function fc_MostrarAlternativasMixta(codGrupo){
	
		codEncuesta = fc_Trim(document.getElementById("txhCodigo").value);
		usuario = document.getElementById("usuario").value;
		
		
		fc_aparece("divAlternativasMixta",'1');	
						
		document.getElementById("iframeAlternativasMixta").src 
		= "${ctx}/encuestas/datosGeneralesMixtaAlternativas.html"+
		"?prmCodEncuesta="+codEncuesta+
		"&prmCodGrupo="+codGrupo+
		"&prmUsuario="+usuario+
		"&prmCodEstado="+document.getElementById("txhCodEstado").value+
		"&prmTamanio=159";		
}

function fc_aparece(divId,flgMostrar){
	
	if( flgMostrar == '1' )
	{				
		document.getElementById(divId).style.display = "";	
	}
	 else
		document.getElementById(divId).style.display = "none";

}


function fc_Sel(pos){

	document.getElementById("txhPosSel").value = pos;
	codGrupo = document.getElementById("hidCodigo"+pos).value;	

	fc_aparece("divAlternativasMixta",'0');

}

function fc_Actualizar(){

	var posSel = document.getElementById("txhPosSel").value;
	if(fc_Trim(posSel)!="") 
		{
			fc_mostrarDatos(posSel);		
		}
	else
		alert(mstrSeleccione);	
}

function fc_mostrarDatos(posSel){

	document.getElementById("txhCodigoUpdate").value = document.getElementById("hidCodigo"+posSel).value;
	document.getElementById("txtGrupo").value =  document.getElementById("hidGrupo"+posSel).value ;	
	document.getElementById("cboFormato").value = document.getElementById("hidFormato"+posSel).value;	
	document.getElementById("cboAlternativa").value = document.getElementById("hidAlternativa"+posSel).value;	
	document.getElementById("txtPeso").value = document.getElementById("hidPeso"+posSel).value;
	document.getElementById("cboFormato").disabled = true;
	document.getElementById("cboAlternativa").disabled = true;
	document.getElementById("cboFormato").className = "cajatexto_1";
	document.getElementById("cboAlternativa").className = "cajatexto_1";
}

function fc_Eliminar(){
	posSel = document.getElementById("txhPosSel").value;
	if(posSel!="") 
		{	
			numPregRel = document.getElementById("hidPreguntasRel"+posSel).value;
			numAltRel = document.getElementById("hidAlternativasRel"+posSel).value;
			
			if( Number(numPregRel)>0 || Number(numAltRel)>0  ){
				alert(mstrTienePregOrAltRelacionadas);
			}else{
			  if(confirm(mstrSeguroEliminar1))
			  {
				document.getElementById("txhCodigoSel").value = document.getElementById("hidCodigo"+posSel).value;
		 		document.getElementById("operacion").value="irEliminar";
				document.getElementById("frmMain").submit();
			  }			
			}
			
			
		}
	else
		alert(mstrSeleccione);
}


function fc_Limpiar(){
	document.getElementById("operacion").value="irRegistrar";
	document.getElementById("txtGrupo").value = "";
	document.getElementById("txtPeso").value = "";
	document.getElementById("cboFormato").value = "";
	document.getElementById("cboAlternativa").value = "";
	document.getElementById("cboFormato").disabled = false;
	document.getElementById("cboFormato").className = "combo_o";	
	document.getElementById("cboAlternativa").disabled = false;
	document.getElementById("cboAlternativa").className = "combo_o";
	document.getElementById("txhCodigoUpdate").value ="";	
	//document.getElementById("txhCodigoSel").value = "";
}

function fc_Grabar(){
	if(fc_validaGrabar()){
		if(confirm(mstrSeguroGrabar)){		

			document.getElementById("operacion").value="irRegistrar";
			document.getElementById("cboFormato").disabled = false;
			document.getElementById("cboAlternativa").disabled = false;			
			document.getElementById("frmMain").submit();
		}	
	}
}

function fc_validaGrabar(){
	
	if(fc_Trim(document.getElementById("txtGrupo").value)=="")
	{	alert(mstrIngrGrupo);
		document.getElementById("txtGrupo").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboFormato").value)==""){
		alert(mstrSeleccioneFormato);
		document.getElementById("cboFormato").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPeso").value)==""){
		alert(mstrIngrPeso);
		document.getElementById("txtPeso").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboAlternativa").value)==""){
		alert(mstrSeleccioneAlternativa);
		document.getElementById("cboAlternativa").focus();
		return 0;
	}
		
	return 1;
}

function fc_enConstruccion(){
	alert("Funcionalidad en construcci�n");
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/encuestas/datosGeneralesMixtaGrupos.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodigo" id="txhCodigo" />	
	<form:hidden path="txhCodEstado" id="txhCodEstado" />
	
	<!-- ID ELIMINACION -->
	<input type="hidden" id="txhCodigoUpdate" name="txhCodigoUpdate" />
	<input type="hidden" id="txhCodigoSel" name="txhCodigoSel" />
	<input type="hidden" id="txhPosSel" name="txhPosSel" />	
	

<table cellpadding="0" cellspacing="0" border="0" bordercolor="blue"
		style="width:97%;margin-left:9px" >
	<tr>
	<td style="width:50%" valign="top">
		<table class="tabla2" width="97%" cellSpacing="0" cellPadding="0"
			border="0" bordercolor='gray' ID="Table2" style="margin-top: 0px;height: 18px;">
			<tr>
				<td align="left" style='cursor: hand;'>GRUPOS</td>
			</tr>
		</table>					
		<table background="${ctx}/images/biblioteca/fondosup.jpg"
				style="width: 100%;height: 70px;margin-bottom: 4px;" border="0" cellspacing="1" cellpadding="1" class="tabla" bordercolor="red">
			<tr>
				<td class=""> Grupo :</td>
				<td>					
					<form:input path="txtGrupo" 
					 onkeypress="fc_ValidaTextoEspecialEncuestas()"  onblur="fc_ValidaTextoEspecialEncuestasOnblur('txtGrupo')"  
					id="txtGrupo" size="29" maxlength="50" cssClass="cajatexto_o" />
				</td>
				
			</tr>
			<tr>
				<td>Formato :</td>
				<td colspan="1">
					
				<form:select path="cboFormato" id="cboFormato"
						cssClass="combo_o" cssStyle="width:140px"  >
						<form:option value="">--Seleccione--</form:option>				
						<c:if test="${control.lstFormato!=null}">
							<form:options itemValue="idSeccion" itemLabel="nomFormato"
								items="${control.lstFormato}" />
						</c:if>
				</form:select>	
				</td>						
				
			</tr>
			<tr>						
				<td id="etqPeso">Peso :</td>
				<td id="etqPeso1">					
				<form:input path="txtPeso" id="txtPeso" size="3" maxlength="3"
				 onblur="fc_ValidaNumeroOnBlur('txtPeso');" 
					onkeypress="fc_PermiteNumeros();"  
				cssClass="cajatexto_o" />
				</td>
				<td class=""> Alternativa:</td>
				<td>					
				<form:select path="cboAlternativa" id="cboAlternativa"
						cssClass="combo_o" cssStyle="width:100px"  >
						<form:option value="">--Seleccione--</form:option>
						<form:option value="1">Si</form:option>
						<form:option value="0">No</form:option>
				</form:select>						
				</td>
				<td width="71px" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/iconos/grabar2.jpg',1)">
				<img  onclick="fc_Grabar()" src="${ctx}/images/iconos/grabar1.jpg" alt="Grabar" id="imgGrabar" style="cursor:pointer"></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','${ctx}/images/iconos/cerrar2.jpg',1)">
				<img onclick="fc_Limpiar();" src="${ctx}/images/iconos/cerrar1.jpg" alt="Cancelar" id="imgCerrar" style="cursor:pointer"></a></td>
			</tr>
		</table>
		<table style="width: 100%; margin-top: 0px" cellSpacing="0" cellPadding="0" border="0" bordercolor="blue" ID="Table12">
			<tr>
				<td style="width: 95%;">				
				<table cellpadding="0" cellspacing="1" style="width:96%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">				
					<tr>
						<td class="grilla" style="width: 5%;">Sel.</td>
						<td class="grilla" style="width: 50%;">Grupo</td>
						<td class="grilla" style="width: 15%;">Formato</td>
						<td class="grilla" style="width: 15%;">Alternativa</td>
						<td class="grilla" style="width: 15%;">Peso</td>
					</tr>
				</table>
				<div style="overflow: auto; height: 160px;width:100%">
				<table cellpadding="0" cellspacing="1" style="width:96%;margin-top:0px;margin-bottom:0px;border: 1px solid #048BBA" ID="Table4">
					<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
					<tr class="tablagrilla">
						<td style="width: 5%;">
							<input type="hidden" id="hidCodigo<c:out value="${loop.index}" />"   value="<c:out value="${objCast.codGrupo}" />"		name="hidCodigo<c:out value="${loop.index}" />" />
							<input type="hidden" id="hidGrupo<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.nomGrupo}" />"		name="hidGrupo<c:out value="${loop.index}" />"  />
							<input type="hidden" id="hidFormato<c:out value="${loop.index}" />"  value="<c:out value="${objCast.codFormato}" />"		name="hidFormato<c:out value="${loop.index}" />" />
							<input type="hidden" id="hidAlternativa<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.indAlternativa}" />"		name="hidAlternativa<c:out value="${loop.index}" />" />									
							<input type="hidden" id="hidPeso<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.pesoGrupo}" />"		name="hidPeso<c:out value="${loop.index}" />" />
							
							<input type="hidden" id="hidPreguntasRel<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.preguntasRel}" />"		name="hidPreguntasRel<c:out value="${loop.index}" />" />
							<input type="hidden" id="hidAlternativasRel<c:out value="${loop.index}" />" 	 value="<c:out value="${objCast.alternativasRel}" />"		name="hidAlternativasRel<c:out value="${loop.index}" />" />
							
							<INPUT class="radio" type="radio"  name="radio" onclick="fc_Sel('<c:out value="${loop.index}" />')" >
						 </td>
						<td style="width: 50%;"><c:out value="${objCast.nomGrupo}" /></td>
						<td style="width: 15%;"><c:out value="${objCast.nomFormato}" /></td>
						<td align="center" style="width: 15%;"><c:out value="${objCast.desAlternativa}" /></td>
						<td align="center" style="width: 15%;"><c:out value="${objCast.pesoGrupo}" /></td>
					</tr>							
					</c:forEach>				
				</table>
				</div>
				</td>
				<td align="right" valign="middle">				
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmodificar','','${ctx}/images/iconos/modificar2.jpg',1)">
						<img  onclick="fc_Actualizar()"   src= "${ctx}/images/iconos/modificar1.jpg" style="cursor:pointer" id="imgmodificar">
					</a>
					<br>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgQuitar','','${ctx}/images/iconos/quitar2.jpg',1)">
						<img onclick="fc_Eliminar()"   src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer" id="imgQuitar">
					</a>
					<br>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgA','','${ctx}/images/iconos/a2.jpg',1)">
						<img onclick="fc_Alternativa()"  src="${ctx}/images/iconos/a.jpg" style="cursor:pointer" id="imgA">
					</a>
				</td>
			</tr>
		</table>
	</td>
	<td style="width:50%"  valign="top">	
		<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" bordercolor="orange">
			<tr>
				<td valign="top">
					<div id="divAlternativasMixta" style="display: none;width: 100%;height: 100%;" >
					
					<iframe id="iframeAlternativasMixta" name="iframeAlternativasMixta" frameborder="0"
						style="width:100%; height: 280px;" >
					</iframe>
						
					</div>
				</td>
			</tr>
		</table>
	</td>
	</tr>
</table>					
<!-- **************************************************************************************************** -->
</form:form>
</body>


