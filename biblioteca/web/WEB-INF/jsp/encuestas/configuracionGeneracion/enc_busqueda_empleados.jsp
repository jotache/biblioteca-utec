<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
		document.getElementById("txhCodPersonal").value="";		
		if(document.getElementById("txhFlag").value=="OK"){
			document.getElementById("txhFlag").value="";
			document.getElementById("iFrameBandejaEmpleado").src="${ctx}/encuestas/busqueda_empleados_iframe.html" ;
		}
		document.getElementById("txhFlag").value="";
	}
	
	function fc_Limpiar(){
		if(document.getElementById("txhTipoPersonal").value=="1"){
			document.getElementById("codTipoPersonal").value=document.getElementById("txhTipoDocente").value;
		}
		else{
			document.getElementById("codTipoPersonal").value="-1";
		}
		document.getElementById("nombre").value="";
		document.getElementById("apellidoPaterno").value="";
		document.getElementById("apellidoMaterno").value="";
	}
	
	function fc_Buscar(){
	 	document.getElementById("iFrameBandejaEmpleado").src="${ctx}/encuestas/busqueda_empleados_iframe.html?"+
		  	"txhCodTipoPersonal="+document.getElementById("codTipoPersonal").value +
			"&txhNombre=" + document.getElementById("nombre").value +
			"&txhApellidoPaterno=" + document.getElementById("apellidoPaterno").value +
			"&txhApellidoMaterno=" + document.getElementById("apellidoMaterno").value ;
	 
	}
	
	function fc_Aceptar(){		
	  	if(document.getElementById("txhCodPersonal").value!=""){
	  		codResponsable=document.getElementById("txhCodPersonal").value;
	  		usuResponsable=document.getElementById("txhUsuPersonal").value;
	  		nomResponsable=document.getElementById("txhNombrePersonal").value;	  		
	  		if(document.getElementById("txhTipoPersonal").value==""){	  			
	  			window.opener.fc_ActualizarResponsable(codResponsable,nomResponsable);
	  		}
	  		else{	  			
	  			window.opener.fc_ActualizarResponsable(codResponsable,usuResponsable,nomResponsable);
	  		}	  			    	
	    	window.close();
	  	}
	  	else{
	  		alert(mstrSeleccion);
	  	}
	}
	
	function fc_SeleccionarRegistro(srtCodPersonal, srtUsuPersonal, srtNombrePersonal){
		//alert(srtCodPersonal+">><<"+srtUsuPersonal+">><<"+srtNombrePersonal);
		document.getElementById("txhCodPersonal").value=srtCodPersonal;
		document.getElementById("txhUsuPersonal").value=srtUsuPersonal;
		document.getElementById("txhNombrePersonal").value=srtNombrePersonal;
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/busqueda_empleados.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codUsuario" id="txhCodUsuario" />
<form:hidden path="codPersonal" id="txhCodPersonal" />
<form:hidden path="codBDTipoPersonal" id="txhCodBDTipoPersonal" />
<form:hidden path="nombrePersonal" id="txhNombrePersonal" />
<form:hidden path="flag" id="txhFlag" />

<form:hidden path="tipoPersonal" id="txhTipoPersonal" />
<form:hidden path="tipoDocente" id="txhTipoDocente" />
<form:hidden path="usuPersonal" id="txhUsuPersonal" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top: 9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="455px" class="opc_combo">
		 		<font style="">
		 			Busqueda de Empleados					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	
	<table class="tabla" style="width:97%;margin-top:4px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red">
			<tr>
				<td width="20%">Tipo Personal :</td>
				<td>
					<form:select path="codTipoPersonal" id="codTipoPersonal" cssClass="cajatexto" 
								cssStyle="width:160px">
								<form:option  value="-1">--Todos--</form:option>
								<c:if test="${control.listaTipoPersonal!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listaTipoPersonal}" />
								</c:if>
				    </form:select>
				</td>
					<td></td>
					<td align=right>
					   <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;			
					   <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
					</td>
			</tr>
			<tr>
				<td>Nombres :</td>
				<td colspan="3"><form:input path="nombre" id="nombre" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Nombres');" 
					cssClass="cajatexto" size="60"/>
				</td>
			</tr>
			<tr>
				<td>Apellido Paterno:</td>
				<td>
					<form:input path="apellidoPaterno" id="apellidoPaterno" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Apellido Paterno');" 
					cssClass="cajatexto" size="40"/>
				</td>
				<td>Apellido Materno:</td>
				<td><form:input path="apellidoMaterno" id="apellidoMaterno" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Apellido Materno');" 
					cssClass="cajatexto" size="40"/>
				</td>
			</tr>
		</table>
	
	<iframe id="iFrameBandejaEmpleado" name="iFrameBandejaEmpleado" frameborder="0" height="290px" width="100%">
				</iframe>
		
		<table width="100%" style="display=''">
			<tr>
				<td align="center">
				    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/botones/aceptar2.jpg',1)">
						<img alt="Aceptar" src="${ctx}/images/botones/aceptar1.jpg" id="imgGrabar01" style="cursor:pointer;" onclick="javascript:fc_Aceptar();"></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar01','','${ctx}/images/botones/cancelar2.jpg',1)">
						<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar01" style="cursor:pointer;" onclick="window.close();"></a>
				</td>
			</tr>
		</table>
</form:form>
</body>
</html>