<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.bean.ConsultaByProfesorDetBean'%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<html>
<head>
<script language="javascript">
	function onLoad(){	
	}
</script>
<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.cabecera_grilla_2 {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	WIDTH: 300px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	WIDTH: 300px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
</style>
</head>
<body>
<div style="overflow: auto; height: 320px" >
<% if ( request.getAttribute("LST_RESULTADO")!= null ){
		System.out.println("primero entro if");
		List consulta = (List) request.getAttribute("LST_RESULTADO");
		int tamConsulta=consulta.size();
		int indice=0;
		int nroPreguntas=0;		
		
		if(tamConsulta > 0){
			//PARA OBTENER EL NRO DE PREGUNTAS
			nroPreguntas=1;
			ConsultaByProfesorDetBean reg1 = (ConsultaByProfesorDetBean) consulta.get(0);				
			for(int i = 1; i < tamConsulta; i++){
				ConsultaByProfesorDetBean reg2 = (ConsultaByProfesorDetBean) consulta.get(i);
				if(reg1.getCodPregunta().equalsIgnoreCase(reg2.getCodPregunta()) && reg1.getCodSeccion().equals(reg2.getCodSeccion()) &&
						reg1.getCodProducto().equals(reg2.getCodProducto()) && reg1.getCodPrograma().equals(reg2.getCodPrograma()) &&
						reg1.getCodDepartamento().equals(reg2.getCodDepartamento()) && reg1.getCodCurso().equals(reg2.getCodCurso()) &&
						reg1.getCodCiclo().equals(reg2.getCodCiclo()) && reg1.getCodSeccionAlumno().equals(reg2.getCodSeccionAlumno()) &&
						reg1.getCodGrupo().equals(reg2.getCodGrupo()) && reg1.getCodCiclo().equals(reg2.getCodCiclo())){				
					nroPreguntas++;
				}
				else{
					i = tamConsulta;
				}
			}
			System.out.println("nroPreguntas>>"+nroPreguntas);
			//********************************
		}
		else{
			nroPreguntas=3;
		}		
	%>
<table class="tabla" style="width:98%;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red">
	<tr>
		<td style="width: 10%">Sede :</td>
		<td style="width: 25%">
			<input type="text" value="${model.NOM_SEDE}" size="15" class="cajatexto_1" readonly="readonly">
		</td>
		<td style="width: 15%">Tipo Aplicación :</td>
		<td style="width: 20%">
			<input type="text" value="${model.NOM_TIPOAPLICACION}" size="30" class="cajatexto_1" readonly="readonly">
		</td>		
		<td style="width: 10%">Perfil :</td>
		<td style="width: 20%">
			<input type="text" value="${model.NOM_PERFIL}" size="30" class="cajatexto_1" readonly="readonly">
		</td>
	</tr>
	<tr>
		<td>Nro. Encuesta :</td>
		<td>
			<input type="text" value="${model.COD_ENCUESTA}" size="15" class="cajatexto_1" readonly="readonly">
		</td>
		<td>Nombre Encuesta :</td>
		<td>
			<input type="text" value="${model.NOM_ENCUESTA}" size="40" class="cajatexto_1" readonly="readonly">
		</td>				
	</tr>
	<tr>		
		<td>Programa :</td>
		<td>
			<input type="text" value="${model.NOM_PROGRAMA}" size="50" class="cajatexto_1" readonly="readonly">
		</td>
		<td>Nombre Profesor :</td>
		<td>
			<input type="text" value="${model.NOM_PROFESOR}" size="40" class="cajatexto_1" readonly="readonly">			
		</td>				
	</tr>
</table>
<%if(tamConsulta > 0){%>
<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
	<tr style="border: 0px;">
		<td colspan="7">
		</td>
		<td class="grilla" colspan="<%=nroPreguntas %>">
			Resultado			
		</td>
	</tr>	
	<tr>
		<td class="grilla">Formato</td>
		<td class="grilla">Departamento</td>
		<td class="grilla">Curso</td>
		<td class="grilla">Ciclo</td>
		<td class="grilla">Seccion</td>
		<td class="grilla">Grupo de Preguntas</td>
		<td class="grilla">Pregunta</td>
		<%
		for(int i=0; i< nroPreguntas; i++){
			ConsultaByProfesorDetBean reg = (ConsultaByProfesorDetBean) consulta.get(i);%>
			<td class="grilla">
				<%=reg.getNomAlternativa()%>
			</td>
			<%
		}
		%>
		<td class="grilla">Promedio</td>
	</tr>
					
	<%
	float totalPromedio=0;
	DecimalFormat df = new DecimalFormat("0.00");
	float alternativa=0;
	String variable="";
	while(indice!=tamConsulta){
		ConsultaByProfesorDetBean obj1 = (ConsultaByProfesorDetBean) consulta.get(indice);			
			%>
		<tr style="vertical-align: middle;">														
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj1.getNomSeccion()%></td>
			<td style="text-align:left;width: 15%;" class="tablagrilla"><%=obj1.getNomDepartamento()%></td>
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj1.getNomCurso()%></td>
			<td style="text-align:left;width: 5%;" class="tablagrilla"><%=obj1.getNomCiclo()%></td>
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj1.getNomSeccionAlumno()%></td>						
			<td style="text-align:left;width: 15%;" class="tablagrilla"><%=obj1.getNomGrupo()%></td>						
			<td style="text-align:left;width: 15%;" class="tablagrilla"><%=obj1.getNomPregunta()%></td>						
			<td style="text-align:right;width: 5%;" class="tablagrilla"><%=obj1.getTotalAlternativa()%></td>
			<%						
			variable=obj1.getTotalAlternativa().replace(",",".");
			alternativa=Float.parseFloat(variable);
			totalPromedio=alternativa;
			for(int i=1; i<nroPreguntas; i++ ){
				ConsultaByProfesorDetBean obj = (ConsultaByProfesorDetBean) consulta.get(indice+i);
				
				variable=obj.getTotalAlternativa().replace(",",".");
				
				alternativa=Float.parseFloat(variable);
				totalPromedio=totalPromedio+alternativa;
				%>
				<td style="text-align:right;width: 5%;" class="tablagrilla">
				<%=obj.getTotalAlternativa()%>
				</td>
				<%
			}
			totalPromedio=totalPromedio/nroPreguntas;
			%>
			<td style="text-align:right;width: 10%;" class="tablagrilla"><%=df.format(totalPromedio)%></td>
		</tr>
		<%
		indice=indice+nroPreguntas;
		}
		%>
</table>
<table cellpadding="0" cellspacing="0">	
	<tr>			
		<td class="tabla2">
			Leyenda
		</td>
	</tr>
	<tr>
		<td class="tabla2">
		<%		
		if ( request.getAttribute("LST_LEYENDA")!= null ){			
			List listaLeyenda = (List) request.getAttribute("LST_LEYENDA");			
			if(listaLeyenda.size() > 0){
				VerEncuestaBean obj = (VerEncuestaBean) listaLeyenda.get(0);
				%>
				<%=MetodosConstants.getLeyendaStandar(obj)%>
				<%				
			}
			request.removeAttribute("LST_LEYENDA");
		}	 
		%>
		</td>		
	</tr>
	<tr>
		<td></td>
	</tr>
</table>
<br>
	<%
		}
	}
	request.removeAttribute("LST_RESULTADO");
	
	if ( request.getAttribute("LST_RESULTADO_2")!= null ){
		List consulta_2 = (List) request.getAttribute("LST_RESULTADO_2");
		if(consulta_2.size() > 0){
	%>

<table border="0" bordercolor="red" cellpadding="0" cellspacing="0">
	<tr>		
		<td class="tabla2">Preguntas Abiertas
		</td>
	</tr>
	<!-- <tr>
		<td>&nbsp;
		</td>
	</tr> -->
</table>
<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
	<tr>
		
		<td class="grilla">Formato</td>
		<td class="grilla">Departamento</td>
		<td class="grilla">Curso</td>
		<td class="grilla">Ciclo</td>
		<td class="grilla">Seccion</td>
		<td class="grilla">Grupo de Preguntas</td>
		<td class="grilla">Pregunta</td>					
		<td class="grilla">Respuesta</td>
	</tr>
			<%
			for(int i = 0; i < consulta_2.size(); i++){
				ConsultaByProfesorDetBean obj = (ConsultaByProfesorDetBean) consulta_2.get(i);
			%>
			<tr style="vertical-align: middle;">														
				<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomSeccion()%></td>
				<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomDepartamento()%></td>
				<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomCurso()%></td>
				<td style="text-align:left;width: 5%;" class="tablagrilla"><%=obj.getNomCiclo()%></td>
				<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomSeccionAlumno()%></td>						
				<td style="text-align:left;width: 15%;" class="tablagrilla"><%=obj.getNomGrupo()%></td>						
				<td style="text-align:left;width: 20%;" class="tablagrilla"><%=obj.getNomPregunta()%></td>						
				<td style="text-align:left;width: 20%;" class="tablagrilla"><%=obj.getRespuesta()%></td>
			</tr>
			<%
			}
			%>
</table>	
<%
		}
	}
	request.removeAttribute("LST_RESULTADO_2");
%>
</body>
</html>