<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.bean.ConsultaEstandarByPerfilBean'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.cabecera_grilla_2 {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	WIDTH: 300px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	WIDTH: 300px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
</style>
<body>

<br>
<table>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold" align="center" rowspan="2" colspan="8">SGA - SISTEMA DE ENCUESTAS</td>
	</tr>
	<tr>
		<td>
			<img src="${ctx}/images/logoTecsup.jpg">
		</td>
		<td class="texto_bold">Fecha :</td>
		<td class="texto">${model.FECHA_ACTUAL}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold" align="center" colspan="8">
			Reporte de Encuesta Mixta - Detallado
		</td>
		<td class="texto_bold">Hora :</td>
		<td class="texto">${model.HORA_ACTUAL}</td>
	</tr>
</table>

<br>

<table>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold">Sede :</td>
		<td class="texto">${model.NOM_SEDE}</td>
		<td class="texto_bold" colspan="2">Tipo Aplicación :</td>
		<td class="texto">${model.NOM_TIPOAPLICACION}</td>		
<%-- 		<td class="texto_bold">Perfil :</td>
		<td class="texto" colspan="2">${model.NOM_PERFIL}</td> --%>
		<td></td><td></td>
		<td></td>				
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold">Nro. Encuesta :</td>
		<td class="texto" align="left">${model.COD_ENCUESTA}</td>
		<td class="texto_bold" colspan="2">Nombre Encuesta :</td>
		<td class="texto">${model.NOM_ENCUESTA}</td>
	</tr>	
</table>

<br>
<%
if ( request.getAttribute("LST_RESULTADO")!= null ){
	
	List consulta = (List) request.getAttribute("LST_RESULTADO");
	int tamConsulta=consulta.size();
	
	if(tamConsulta > 0){
%>
<table>	
	<tr>
		<td>			
		</td>
		<td>
			<table border="2">
				<tr>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Formato</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Tipo Perfil</td>
					<td class="cabecera_grilla" colspan="2" >Grupo Pregunta</td>										
					<td class="cabecera_grilla" colspan="2">Pregunta</td>
					<td>
						<table border="1">
							<tr>
								<td class="cabecera_grilla" width="10%" nowrap="nowrap">Respuesta</td>
								<td class="cabecera_grilla" width="10%" nowrap="nowrap">Leyenda</td>
							</tr>
						</table>
					</td>					
				</tr>
				<%
				//OBTENEMOS LOS DEMAS REGISTROS MENOS EL ULTIMO Y LO VAMOS PINTANDO
				int indice=0;
				String idPregunta="";
				String codPerfil="";
				String codGrupo="";
				
				int bandera=0;
				while(indice < tamConsulta-1){
					bandera=0;
					ConsultaEstandarByPerfilBean obj = (ConsultaEstandarByPerfilBean) consulta.get(indice);
					%>
				<tr style="vertical-align: middle;">														
					<td style="text-align:left;" class="texto_grilla"><%=obj.getNomSeccion()%></td>
					<td style="text-align:left;" class="texto_grilla"><%=obj.getNomPerfil()%></td>												
					<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj.getNomGrupo()%></td>						
					<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj.getNomPregunta()%><br><%=obj.getDscPreguntaUnica()%></td>						
					<td style="text-align:left;" class="texto_grilla">
						<table border="1">
						<tr>
							<td style="text-align:left;" class="texto_grilla">
								<%=obj.getCodAlternativa()%>&nbsp;=&nbsp;<%=obj.getTotalAlternativa()%>
							</td>
							<td style="text-align:left;" class="texto_grilla">
								<%=obj.getCodAlternativa()%>&nbsp;=&nbsp;<%=obj.getDscAlternativa()%>
							</td>
						</tr>						
						<%
						idPregunta=obj.getCodPregunta();
						codPerfil=obj.getCodPerfil();
						codGrupo=obj.getCodGrupo();
						
						System.out.println("codPregunta>>"+idPregunta+"<<");
						while(bandera == 0){
							//OBTENEMOS EL OBJETO TEMPORAL
							if(indice+1 < tamConsulta){
								ConsultaEstandarByPerfilBean objTemp = (ConsultaEstandarByPerfilBean) consulta.get(indice+1);
								if(idPregunta.equals(objTemp.getCodPregunta()) && codPerfil.equals(objTemp.getCodPerfil()) && codGrupo.equals(objTemp.getCodGrupo())){
									System.out.println("codPreguntaNuevo>>"+objTemp.getCodPregunta()+"<<");
									%>
									<tr>
										<td style="text-align:left;" class="texto_grilla">
											<%=objTemp.getCodAlternativa()%>&nbsp;=&nbsp;<%=objTemp.getTotalAlternativa()%>											
										</td>
										<td style="text-align:left;" class="texto_grilla">
											<%=objTemp.getCodAlternativa()%>&nbsp;=&nbsp;<%=objTemp.getDscAlternativa()%>
										</td>
									</tr>
								<%
									indice=indice+1;
								}
								else{
									bandera = 1;
								}
							}
							else{
								bandera = 1;
							}
						}
						%>
						</table>
					</td>					
				</tr>
					<%
					indice++;
					System.out.println("indice>>"+indice+"<<");
				}//FIN MIENTRAS
				%>
				
			</table>
		</td>
	</tr>
</table>
<br>
<br>
<%
	}//FIN CONDICION TAM CONSULTA
}
request.removeAttribute("LST_RESULTADO");

if ( request.getAttribute("LST_RESULTADO_2")!= null ){
	List consulta_2 = (List) request.getAttribute("LST_RESULTADO_2");
	if(consulta_2.size() > 0){
%>
<table>
	<tr>
		<td>			
		</td>
		<td class="texto_bold">Preguntas Abiertas
		</td>
	</tr>
	<tr>
		<td>			
		</td>
		<td>
			<table border="1">
				<tr>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Formato</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Tipo Perfil</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap" colspan="2">Grupo Pregunta</td>
					<td class="cabecera_grilla" colspan="2">Pregunta</td>					
					<td class="cabecera_grilla" colspan="2">Respuesta</td>
				</tr>
				<%
				for(int i = 0; i < consulta_2.size(); i++){
					ConsultaEstandarByPerfilBean obj = (ConsultaEstandarByPerfilBean) consulta_2.get(i);
				%>
				<tr style="vertical-align: middle;">														
					<td style="text-align:left;" class="texto_grilla"><%=obj.getNomSeccion()%></td>
					<td style="text-align:left;" class="texto_grilla"><%=obj.getNomPerfil()%></td>																				
					<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj.getNomGrupo()%></td>						
					<td style="text-align:left;width: 200px;" class="texto_grilla" colspan="2" ><%=obj.getNomPregunta()%></td>						
					<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj.getRespuesta()%></td>
				</tr>
				<%
				}
				%>			
			</table>
		</td>
	</tr>
</table>
<br>
<br>
<%
	}
}
%>		
</body>
</html>