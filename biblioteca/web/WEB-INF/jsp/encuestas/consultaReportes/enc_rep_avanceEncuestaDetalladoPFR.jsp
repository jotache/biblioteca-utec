<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.bean.ConsultaByProfesorDetBean'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<html>
<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.cabecera_grilla_2 {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	WIDTH: 300px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	WIDTH: 300px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
</style>

<body>

<br>
<%
if ( request.getAttribute("LST_RESULTADO")!= null ){	
	List consulta = (List) request.getAttribute("LST_RESULTADO");
	int tamConsulta=consulta.size();
	int indice=0;
	int nroPreguntas=0;		
	
	if(tamConsulta > 0){
		//PARA OBTENER EL NRO DE PREGUNTAS
		nroPreguntas=1;
		ConsultaByProfesorDetBean reg1 = (ConsultaByProfesorDetBean) consulta.get(0);				
		for(int i = 1; i < tamConsulta; i++){
			ConsultaByProfesorDetBean reg2 = (ConsultaByProfesorDetBean) consulta.get(i);
			if(reg1.getCodPregunta().equalsIgnoreCase(reg2.getCodPregunta()) && reg1.getCodSeccion().equals(reg2.getCodSeccion()) &&
					reg1.getCodProducto().equals(reg2.getCodProducto()) && reg1.getCodPrograma().equals(reg2.getCodPrograma()) &&
					reg1.getCodDepartamento().equals(reg2.getCodDepartamento()) && reg1.getCodCurso().equals(reg2.getCodCurso()) &&
					reg1.getCodCiclo().equals(reg2.getCodCiclo()) && reg1.getCodSeccionAlumno().equals(reg2.getCodSeccionAlumno()) &&
					reg1.getCodGrupo().equals(reg2.getCodGrupo()) && reg1.getCodCiclo().equals(reg2.getCodCiclo())){
				nroPreguntas++;
			}
			else{
				i = tamConsulta;
			}
		}
		System.out.println("nroPreguntas>>"+nroPreguntas);
		//********************************
	}
	else{
		nroPreguntas=3;
	}
%>
<table>
	<tr>
		<td>
			<img src="${ctx}/images/logoTecsup.jpg">
		</td>
		<td class="texto_bold" align="center" rowspan="2" colspan="<%=nroPreguntas+10 %>">SGA - SISTEMA DE ENCUESTAS</td>
	</tr>
	<tr>
		<td>			
		</td>
		<td class="texto_bold">Fecha :</td>
		<td class="texto">${model.FECHA_ACTUAL}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold" align="center" colspan="<%=nroPreguntas+10 %>">Consulta de Avance de Encuestas por Profesor - Detallado
		</td>
		<td class="texto_bold">Hora :</td>
		<td class="texto">${model.HORA_ACTUAL}</td>
	</tr>
</table>

<br>

<table>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold">Sede :</td>
		<td class="texto">${model.NOM_SEDE}</td>
		<td class="texto_bold">Tipo Aplicaci�n :</td>
		<td class="texto">${model.NOM_TIPOAPLICACION}</td>		
		<td class="texto_bold">Perfil :</td>
		<td class="texto" colspan="2">${model.NOM_PERFIL}</td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold">Nro. Encuesta :</td>
		<td class="texto" align="left">${model.COD_ENCUESTA}</td>		
		<td class="texto_bold">Nombre Encuesta :</td>
		<td class="texto">${model.NOM_ENCUESTA}</td>
		<td></td>				
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="texto_bold">Programa :</td>
		<td class="texto">
			${model.NOM_PROGRAMA}
		</td>
		<td class="texto_bold">Nombre Profesor :</td>
		<td class="texto">
			${model.NOM_PROFESOR}
		</td>				
	</tr>
</table>
<br>
<%
	if(tamConsulta > 0){
		System.out.println("Entra si el tama�o de la es mayor a 0");
%>
	
<table>	
	<tr>
		<td colspan="10">
		</td>		
		<td>
			<table border="1">
				<tr>					
					<td colspan="<%=nroPreguntas%>" class="cabecera_grilla" border="1" nowrap="nowrap">Resultado</td>					
				</tr>
			</table>
		</td>
		<td>
		</td>
	</tr>
</table>
<table>	
	<tr>
		<td>			
		</td>
		<td>
			<table border="1">
				<tr>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Formato</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Departamento</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Curso</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Ciclo</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Seccion</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap" colspan="2">Grupo de Preguntas</td>
					<td class="cabecera_grilla" colspan="2">Pregunta</td>
					<%
					for(int i=0; i< nroPreguntas; i++){
						ConsultaByProfesorDetBean reg = (ConsultaByProfesorDetBean) consulta.get(i);%>
						<td class="cabecera_grilla" colspan="1" width="100%">
							<%=reg.getNomAlternativa()%>
						</td>
						<%
					}
					%>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Promedio</td>
				</tr>
								
				<%
				float totalPromedio=0;
				DecimalFormat df = new DecimalFormat("0.00");
				float alternativa=0;
				String variable="";
				while(indice!=tamConsulta){
					ConsultaByProfesorDetBean obj1 = (ConsultaByProfesorDetBean) consulta.get(indice);			
						%>
					<tr style="vertical-align: middle;">														
						<td style="text-align:left;" class="texto_grilla"><%=obj1.getNomSeccion()%></td>
						<td style="text-align:left;" class="texto_grilla"><%=obj1.getNomDepartamento()%></td>
						<td style="text-align:left;" class="texto_grilla"><%=obj1.getNomCurso()%></td>
						<td style="text-align:left;" class="texto_grilla"><%=obj1.getNomCiclo()%></td>
						<td style="text-align:left;" class="texto_grilla"><%=obj1.getNomSeccionAlumno()%></td>						
						<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj1.getNomGrupo()%></td>						
						<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj1.getNomPregunta()%></td>						
						<td style="text-align:right;" class="texto_grilla" colspan="1" width="100%"><%=obj1.getTotalAlternativa()%></td>
						<%						
						variable=obj1.getTotalAlternativa().replace(",",".");
						alternativa=Float.parseFloat(variable);
						totalPromedio=alternativa;
						for(int i=1; i<nroPreguntas; i++ ){
							ConsultaByProfesorDetBean obj = (ConsultaByProfesorDetBean) consulta.get(indice+i);
							
							variable=obj.getTotalAlternativa().replace(",",".");
							
							alternativa=Float.parseFloat(variable);
							totalPromedio=totalPromedio+alternativa;
							%>
							<td style="text-align:right;" class="texto_grilla" colspan="1" width="100%">
							<%=obj.getTotalAlternativa()%>
							</td>
							<%
						}
						totalPromedio=totalPromedio/nroPreguntas;
						%>
						<td style="text-align:right;" class="texto_grilla"><%=df.format(totalPromedio)%></td>
					</tr>
				<%
				indice=indice+nroPreguntas;
				}
			%>		
			</table>
		</td>
	</tr>
</table>
<br>
<table>	
	<tr>
		<td>			
		</td>	
		<td class="texto_bold">
		Leyenda
		</td>
	</tr>
	<tr>
		<td>			
		</td>
		<td class="texto_bold" colspan="4">
		<%		
		if ( request.getAttribute("LST_LEYENDA")!= null ){			
			List listaLeyenda = (List) request.getAttribute("LST_LEYENDA");			
			if(listaLeyenda.size() > 0){
				VerEncuestaBean obj = (VerEncuestaBean) listaLeyenda.get(0);
				%>
				<%=MetodosConstants.getLeyendaStandar(obj)%>
				<%				
			}
			request.removeAttribute("LST_LEYENDA");
		}	 
		%>
		</td>
	</tr>
	<tr>
	</tr>
</table>
<%
	}
}
request.removeAttribute("LST_RESULTADO");

if ( request.getAttribute("LST_RESULTADO_2")!= null ){
	List consulta_2 = (List) request.getAttribute("LST_RESULTADO_2");
	if(consulta_2.size() > 0){
%>

<table>
	<tr>
		<td>			
		</td>
		<td class="texto_bold">Preguntas Abiertas
		</td>
	</tr>
	<tr>
		<td>			
		</td>
		<td>
			<table border="1">
				<tr>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Formato</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Departamento</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Curso</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Ciclo</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Seccion</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap" colspan="2">Grupo de Preguntas</td>
					<td class="cabecera_grilla" style="width: 100;" colspan="2">Pregunta</td>					
					<td class="cabecera_grilla" width="10%" nowrap="nowrap" colspan="6">Respuesta</td>
				</tr>
				<%				
					for(int i = 0; i < consulta_2.size(); i++){
						ConsultaByProfesorDetBean obj = (ConsultaByProfesorDetBean) consulta_2.get(i);				
						%>
						<tr style="vertical-align: middle;">														
							<td style="text-align:left;" class="texto_grilla"><%=obj.getNomSeccion()%></td>
							<td style="text-align:left;" class="texto_grilla"><%=obj.getNomDepartamento()%></td>
							<td style="text-align:left;" class="texto_grilla"><%=obj.getNomCurso()%></td>
							<td style="text-align:left;" class="texto_grilla"><%=obj.getNomCiclo()%></td>
							<td style="text-align:left;" class="texto_grilla"><%=obj.getNomSeccionAlumno()%></td>						
							<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj.getNomGrupo()%></td>						
							<td style="text-align:left;" class="texto_grilla" colspan="2"><%=obj.getNomPregunta()%></td>						
							<td style="text-align:left;" class="texto_grilla" colspan="6"><%=obj.getRespuesta()%></td>
						</tr>
					<%
					}
				%>		
			</table>
		</td>
	</tr>
</table>
<%
	}
}
request.removeAttribute("LST_RESULTADO_2");
%>
</body>
</html>