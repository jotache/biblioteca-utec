<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@page import='com.tecsup.SGA.bean.ConsultaByProfesorConBean'%>
<%@page import='java.math.BigDecimal'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator"
	prefix="html"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<%@page import="com.tecsup.SGA.common.MetodosConstants;"%>
<html>
<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

.detalleNro {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body>
<table class="tabla" width="896">
	<tr>
	<td></td>
		<td class="texto_bold" colspan="7"><span class="texto_bold">TECSUP<img src="${ctx}/images/logo.gif" width="115" height="30"></span>&nbsp;</td>
		<td  class="texto_bold"><div align="left">Fecha:</div></td>
		<td class="texto_bold">${model.FECHA_ACTUAL}</td>
	</tr>

	<tr>
	<td></td>
		<td class="texto_bold" colspan="7">&nbsp;</td>

		<td class="texto_bold"><div align="left">Hora:</div></td>
		<td class="texto_bold">${model.HORA_ACTUAL}</td>
	</tr>
	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>

 <table class="tabla" width="896">
	<tr>
		<td colspan="2">&nbsp;</td>
		<td class="texto_bold" colspan="6">
		<div align="center" class="texto_bold"><u>SGA - SISTEMA DE ENCUESTAS</u><BR>Reporte de Encuesta Estándar - Consolidado</div>
		</td>
	</tr>
	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>

<table class="tabla" width="896">
	<tr class="texto_grilla">
		<td></td>
		<td class="texto_bold">
		<div align="left">Sede</div>
		</td>
		<td>
		<div align="left">${model.NOM_SEDE}</div>

		</td>
		<td class="texto_bold">
		<div align="left">Tipo Aplicación</div>
		</td>
		<td>
		<div align="left">${model.NOM_TIPOAPLICACION}</div>
		</td>
		<td class="texto_bold">
		<div align="left">Perfil</div>
		</td>
		<td>
		<div align="left">${model.NOM_PERFIL}</div>  
		</td>
		<td></td>
	</tr>
	<tr class="texto_grilla">
		<td>&nbsp;</td>
		<td class="texto_bold">
		<div align="left">Nro. Encuesta</div>
		</td>
		<td>
		<div align="left">${model.COD_ENCUESTA}</div>
		</td>
		<td class="texto_bold">
		<div align="left">Nombre Encuesta</div>
		</td>
		<td>
		<div align="left">${model.NOM_ENCUESTA}</div>
		</td>
		<td class="texto_bold">
		<div align="left">&nbsp;</div>
		</td>
		<td>
		<div align="center" class="texto">&nbsp;
		<div align="left"></div>
		</div>
		</td>
		<td></td>
	</tr>
	<tr class="texto_grilla">
		<td></td>
		<td class="texto_bold">
		<div align="left">Producto</div>
		</td>
		<td>
		<div align="left">${model.NOM_PRODUCTO}</div>
		</td>
		<td class="texto_bold">
		<div align="left">Programa </div>
		</td>
		<td>
		<div align="left">${model.NOM_PROGRAMA}</div>
		</td>
		<td class="texto_bold">
		<div align="left">&nbsp;</div>
		</td>
		<td>
		<div align="left">&nbsp;</div>
		</td>
		<td></td>
	</tr>

	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>


<table class="tabla" width="896">
	
	<tr>
		<td class="texto_bold" colspan="6">&nbsp;</td>
	</tr>
</table>


<table>
<tr width="20px;">
<td></td>
<td>

<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{	
		List consulta = (List) request.getAttribute("LST_RESULTADO");		
		BigDecimal sumTotal = new BigDecimal("0");
		BigDecimal sumImporteTotal = new BigDecimal("0");		
		BigDecimal sumFamiliaTotal = new BigDecimal("0");		
		String guia = "";
		ConsultaByProfesorConBean objFuturo =null;
		
		String codFormatoVariable = "";
		String subFamiliasVariable = "";		
		
		String codFormatoFutura = "";
		String subFamiliaFutura = "";
		
		
	for (int i = 0; i < consulta.size(); i++) 
	{		
			ConsultaByProfesorConBean obj = (ConsultaByProfesorConBean) consulta.get(i);
			codFormatoVariable = obj.getCodSeccion();
	
	if(i==0){%>
			<table>
				<tr  style="height: 15px;">
					<td colspan="6">&nbsp;</td>
					<td  colspan="<%=obj.getListaGrupos().size()%>">
					<table border="1">
					<tr>
					<td  class="cabecera_grilla"  bgcolor="#CCCCCC"  align="CENTER" colspan="<%=obj.getListaGrupos().size()%>"><span class="Estilo5">&nbsp;Resultado</span></td>					
					</tr>
					</table>
					</td>

				</tr>
			</table>
			<table width="896" border="1" bordercolor="#000000" class="tabla">
				<tr bgcolor="#CCCCCC" class="cabecera_grilla" style="height: 30px;">
					<td><span class="Estilo5">Formato</span></td>
					<td><span class="Estilo5">Tipo Docente</span></td>
					<td><span class="Estilo5">Profesor</span></td>
					<td><span class="Estilo5">Departamento<br>Curso</span></td>
					<td><span class="Estilo5">Curso</span></td>
					<td><span class="Estilo5">Nro.<br>Encuestados</span></td>
								
						<%List listaTemp = obj.getListaGrupos();
						ConsultaByProfesorConBean objGrupo = null;
						for(int k=0;k<listaTemp.size();k++){
						objGrupo = (ConsultaByProfesorConBean) listaTemp.get(k);%>
					<td bgcolor="#FFCC66"><%=objGrupo.getNomGrupo()%></td>			
						<%}%>
						
					<td><span class="Estilo5">Promedio.</span></td>	
				</tr>
		
	<%}%>
	
	<!--*******************PINTA UNA FILA CON SUS DATOS RESPECTIVOS*******************-->		
			<tr class="texto_grilla">
					
				<td><%=obj.getNomSeccion()%></td>
				<td><%=obj.getNomTipoPersonal()%></td>
				<td><%=obj.getNomProfesor()%></td>
				<td><%=obj.getNomDpto()%></td>
				<td><%=obj.getNomCurso()%></td>				
				<td><%=obj.getNroEncuestadoxGrupo()%></td>	
					
					<%List listaTemp = obj.getListaGrupos();
					ConsultaByProfesorConBean objGrupo = null;
					sumTotal = new BigDecimal("0");				
					//PINTA LAS COLUMNAS DINAMICAMENTE
					if(listaTemp!=null && listaTemp.size()>0){
					for(int k=0;k<listaTemp.size();k++){
						objGrupo = (ConsultaByProfesorConBean) listaTemp.get(k);%>						
							<td><%=objGrupo.getSumTotalGrupo()%></td>					
						<%}
					}//FIN PINTA LAS COLUMNAS DINAMICAMENTE%>
									
				<td align="right"><%=MetodosConstants.getPromedioxFila(obj)%></td>							
			</tr>
	<!--*******************FINPINTA UNA FILA CON SUS DATOS RESPECTIVOS*******************-->	
		<%	
		
		if( i+1<consulta.size() )
		{	objFuturo = (ConsultaByProfesorConBean) consulta.get(i+1);
			//subFamiliaFutura = objFuturo.getCodSubFamilia();
			codFormatoFutura = objFuturo.getCodSeccion();
			if(!codFormatoVariable.equalsIgnoreCase(codFormatoFutura)){%>
				</table>
				<br><br>
				<table>
				<tr  style="height: 15px;">
					<td colspan="6">&nbsp;</td>
					<td  colspan="<%=objFuturo.getListaGrupos().size()%>">
					<table border="1"><tr>
					<td  class="cabecera_grilla"  bgcolor="#CCCCCC" align="CENTER"  colspan="<%=objFuturo.getListaGrupos().size()%>"><span class="Estilo5">&nbsp;Resultado</span></td>					
					</tr></table>
					</td>
					
				</tr>
				</table>			 	
				<table width="896" border="1" bordercolor="#000000" class="tabla">
				<tr bgcolor="#CCCCCC" class="cabecera_grilla" style="height: 30px;">
										<td><span class="Estilo5">Formato</span></td>
					<td><span class="Estilo5">Tipo Docente</span></td>
					<td><span class="Estilo5">Profesor</span></td>
					<td><span class="Estilo5">Departamento<br>Curso</span></td>
					<td><span class="Estilo5">Curso</span></td>
					<td><span class="Estilo5">Nro.<br>Encuestados</span></td>
					
						<%List listaTemp2 = objFuturo.getListaGrupos();
							ConsultaByProfesorConBean objGrupo2 = null;
							for(int k=0;k<listaTemp2.size();k++){
							objGrupo2 = (ConsultaByProfesorConBean) listaTemp2.get(k);%>
						<td bgcolor="#FFCC66"><%=objGrupo2.getNomGrupo()%></td>
						
						<%}%>
					<td><span class="detalleNro" class="Estilo5">Promedio</span></td>			
				</tr>
			<%}			
		}else if( (consulta.size()-(i+1)) == 0  ){%>
			</table>
		<%}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*FOR*/		
  }/*IF*/
%>

</td>
</tr>
</table>
</body>
</html>