<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.bean.ConsultaByProfesorConBean'%>
<%@page import='java.math.BigDecimal'%>
<%@ page language="java" errorPage="/error.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator"
	prefix="html"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 
<html>
<head>
<script language="javascript">
	function onLoad(){	
	}
</script>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
</style>
</head>
<body>
<div style="overflow: auto; height: 320px" >
<table class="tabla" style="width:98%;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red">
	<tr>		
		<td style="width: 10%">Sede:</td>
		<td style="width: 25%">
			<input type="text" value="${model.NOM_SEDE}" size="15" class="cajatexto_1" readonly="readonly">
		</td>
		<td style="width: 15%">Tipo Aplicaci�n:</td>
		<td style="width: 20%">
			<input type="text" value="${model.NOM_TIPOAPLICACION}" size="30" class="cajatexto_1" readonly="readonly">
		</td>
		<td style="width: 10%">Perfil:</td>
		<td style="width: 20%">
			<input type="text" value="${model.NOM_PERFIL}" size="30" class="cajatexto_1" readonly="readonly">
		</td>
	</tr>
	<tr>
		<td>Nro. Encuesta:</td>
		<td>
			<input type="text" value="${model.COD_ENCUESTA}" size="15" class="cajatexto_1" readonly="readonly">
		</td>
		<td>Nombre Encuesta:</td>
		<td>
			<input type="text" value="${model.NOM_ENCUESTA}" size="40" class="cajatexto_1" readonly="readonly">
		</td>
		<td></td>
		<td></td>		
	</tr>
	<tr>		
		<td>Programa:</td>
		<td>
			<input type="text" value="${model.NOM_PROGRAMA}" size="50" class="cajatexto_1" readonly="readonly">
		</td>
		<td>Nombre Profesor:</td>
		<td>
			<input type="text" value="${model.NOM_PROFESOR}" size="40" class="cajatexto_1" readonly="readonly">
		</td>
		<td>
		</td>		
	</tr>
</table>
<table width="98%" border="0" bordercolor="red" cellpadding="0" cellspacing="0">
<tr>
<td>

<%
	if ( request.getAttribute("LST_RESULTADO")!= null ) 
	{	
		List consulta = (List) request.getAttribute("LST_RESULTADO");		
		BigDecimal sumTotal = new BigDecimal("0");
		BigDecimal sumImporteTotal = new BigDecimal("0");		
		BigDecimal sumFamiliaTotal = new BigDecimal("0");		
		String guia = "";
		ConsultaByProfesorConBean objFuturo =null;
		
		String codFormatoVariable = "";
		String subFamiliasVariable = "";		
		
		String codFormatoFutura = "";
		String subFamiliaFutura = "";
		
		
	for (int i = 0; i < consulta.size(); i++) 
	{		
			ConsultaByProfesorConBean obj = (ConsultaByProfesorConBean) consulta.get(i);			
			//subFamiliasVariable = obj.getCodSubFamilia();
			codFormatoVariable = obj.getCodSeccion();
	
	if(i==0){%>
	<table cellpadding="0" cellspacing="1"  style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
		<tr>
			<td colspan="5" style="width:50%">&nbsp;</td>
			<td class="grilla" colspan="<%=obj.getListaGrupos().size()%>" style="width:40%">Resultado</td>
			<td style="width:10%">&nbsp;</td>
		</tr>
		<tr>
			<td class="grilla" style="width:10%" >Formato</td>
			<td class="grilla" style="width:10%" >Departamento<br>curso</td>
			<td class="grilla" style="width:10%" >Curso</td>
			<td class="grilla" style="width:10%" >Ciclo</td>
			<td class="grilla" style="width:10%" >Seccion</td>			
			<%
			List listaTemp = obj.getListaGrupos();
			ConsultaByProfesorConBean objGrupo = null;
			for(int k=0;k<listaTemp.size();k++){
				objGrupo = (ConsultaByProfesorConBean) listaTemp.get(k);%>
				<td class="grilla"><%=objGrupo.getNomGrupo()%></td>			
			<%}%>
			<td class="grilla" style="width:10%">Promedio</td>	
		</tr>
		
		<%}%>
		
		<!--PINTA UNA FILA CON SUS DATOS RESPECTIVOS-->
		<tr style="vertical-align: middle;">		
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomSeccion()%></td>
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomDpto()%></td>
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomCurso()%></td>
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomCiclo()%></td>
			<td style="text-align:left;width: 10%;" class="tablagrilla"><%=obj.getNomSeccionAlumno()%></td>		
			<%List listaTemp = obj.getListaGrupos();
			ConsultaByProfesorConBean objGrupo = null;
			sumTotal = new BigDecimal("0");
			float tama�o=40;
			float ancho=0;
			DecimalFormat df = new DecimalFormat("0.00");
			if(listaTemp!=null && listaTemp.size()>0){
				ancho=tama�o/listaTemp.size();
			for(int k=0;k<listaTemp.size();k++){
				objGrupo = (ConsultaByProfesorConBean) listaTemp.get(k);
				objGrupo.setTotalGrupo(objGrupo.getTotalGrupo().replace(",","."));
				%>
				<td style="text-align:left;width: <%=df.format(ancho).replace(",",".")%>%;" class="tablagrilla"><%=objGrupo.getSumTotalGrupo()%></td>
				<%System.out.println("objGrupo.getTotalGrupo()>>"+objGrupo.getTotalGrupo()+">>");%>
				<%sumTotal = sumTotal.add(  new BigDecimal( "".equalsIgnoreCase(objGrupo.getTotalGrupo())?"0":objGrupo.getTotalGrupo() ) );
				}
			}%>
			<td style="text-align: right;width: 10%;" class="tablagrilla"><%=(listaTemp==null || listaTemp.size()==0 )?"0":sumTotal.divide(new BigDecimal(""+listaTemp.size()),2,BigDecimal.ROUND_UP)%></td>							
		</tr>
		<%	
		System.out.println("i>>"+i+">>");
		if( i+1<consulta.size() )
		{	objFuturo = (ConsultaByProfesorConBean) consulta.get(i+1);
			//subFamiliaFutura = objFuturo.getCodSubFamilia();
			codFormatoFutura = objFuturo.getCodSeccion();
			System.out.println(">>"+codFormatoVariable+">>");
			System.out.println(">>"+codFormatoFutura+">>");
			if(!codFormatoVariable.equalsIgnoreCase(codFormatoFutura)){%>
	</table>	
	<br>
	<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
		<tr>
			<td colspan="5">&nbsp;</td>
			<td class="grilla" colspan="<%=objFuturo.getListaGrupos().size()%>"><span class="Estilo5">&nbsp;Resultado</span></td>
		</tr>
		<tr>
			<td class="grilla" style="width:10%" >Formato</td>
			<td class="grilla" style="width:10%" >Departamento<br>curso</td>
			<td class="grilla" style="width:10%" >Curso</td>
			<td class="grilla" style="width:10%" >Ciclo</td>
			<td class="grilla" style="width:10%" >Seccion</td>			
			<%List listaTemp2 = objFuturo.getListaGrupos();
				ConsultaByProfesorConBean objGrupo2 = null;
				for(int k=0;k<listaTemp2.size();k++){
				objGrupo2 = (ConsultaByProfesorConBean) listaTemp2.get(k);%>
			<td class="grilla"><%=objGrupo2.getNomGrupo()%></td>
			
			<%}%>
			<td class="grilla">Promedio</td>			
		</tr>
		<%}			
	}else if( (consulta.size()-(i+1)) == 0  ){
		/**PARA EL ULTIMO REGISTRO**/
		
	%>			
	</table>	
	<%		
	}
	}/*FOR*/		
}/*IF*/
%>

</td>
</tr>
</table>
</div>
</body>
</html>