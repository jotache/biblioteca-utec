<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%@page import='com.tecsup.SGA.modelo.TipoTablaDetalle'%>

<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
		document.getElementById("tblIframe").style.display='none';
		fc_AsignarDatosPerfilUsuario();
	}
	
	function fc_BuscarEncuesta(){
			Fc_Popup("${ctx}/encuestas/busquedaEncuesta.html?prmTipo=1&prmInicio=true",900,460);		
	}
	function fc_BuscarEncuesta2(){
			Fc_Popup("${ctx}/encuestas/busquedaEncuesta.html?prmTipo=2&prmInicio=true",900,460);		
	}
	function fc_BuscarEncuesta3(){
			Fc_Popup("${ctx}/encuestas/busquedaEncuesta.html?prmTipo=3&prmInicio=true",900,460);		
	}		
	
	function fc_AsignarDatosPerfilUsuario(){		
		if(document.getElementById("txhPerfilUsuario").value == "2"){
			document.getElementById("txhCodResponsable").value=document.getElementById("txhCodUsuario").value;
			document.getElementById("txtUsuResponsable").value=document.getElementById("txhCodUsuario").value;			
			document.getElementById("txtResponsable").value=document.getElementById("txhNomUsuario").value;
			botonBuscarResponsable.style.display="none";
		}		
	}
	function fc_Regresar(){
		location.href =  "${ctx}/menuEncuesta.html";
	}
	function fc_ActualizarResponsable(codResponsable, usuResponsable, nomResponsable){
		document.getElementById("txhCodResponsable").value=codResponsable;
		document.getElementById("txtUsuResponsable").value=usuResponsable;
		document.getElementById("txtResponsable").value=nomResponsable;
	}
	function fc_BuscarEmpleado(){
		flag="0";
		if(document.getElementById("txhPerfilUsuario").value=="0"){
			flag="1";
		}
		codUsuario=document.getElementById("txhCodUsuario").value;
 		Fc_Popup("${ctx}/encuestas/busqueda_empleados.html?txhCodUsuario="+codUsuario+"&txhTipoPersonal="+flag,660,460);
	}	
	function fc_CambiaReporte(){	
		
		opcion=document.getElementById("cboReportes").value;
		fc_LimpiarTodo();//limpia todas las cabeceras cuando se selecciona otra opcion
		switch(opcion){
			case "0001":
				tablaProfesores.style.display="";
				tablaBandejaProfesores.style.display="";
				tablaEstandar.style.display="none";
				tablaMixta.style.display="none";
				tablaBoton.style.display="none";
				break;
			case "0002":
				tablaProfesores.style.display="none";
				tablaBandejaProfesores.style.display="none";
				tablaEstandar.style.display="";
				tablaMixta.style.display="none";
				tablaBoton.style.display="";
				break;
			case "0003":
				tablaProfesores.style.display="none";
				tablaBandejaProfesores.style.display="none";
				tablaEstandar.style.display="none";
				tablaMixta.style.display="";
				tablaBoton.style.display="";
				break;
			case "":
				tablaProfesores.style.display="none";
				tablaBandejaProfesores.style.display="none";
				tablaEstandar.style.display="none";
				tablaMixta.style.display="none";
				tablaBoton.style.display="none";
				break;
		}
	}
	function fc_LimpiarTodo(){
		fc_Limpiar();
		document.getElementById("txtCodEncuestaEstandar").value="";
		document.getElementById("txtCodEncuestaMixta").value="";
	}
	
	function fc_Generar(){
		
		url="";
		opcion=document.getElementById("cboReportes").value;
		codUsuario=document.getElementById("txhCodUsuario").value;
		switch(opcion){
			case "0001":
				codTipoReporte = opcion;
				codEncuesta = document.getElementById("txtCodEncuestaProfesor").value;
				if(fc_Trim(codEncuesta) == ""){
					alert("Debe ingresar un n�mero de encuesta.");
					document.getElementById("txtCodEncuestaProfesor").value="";
					document.getElementById("txtCodEncuestaProfesor").focus();
					return;
				}
				codTipoResultado = fc_CodTipoResEncProfesor();
				codResponsable = document.getElementById("txhCodResponsable").value;
				if(codResponsable==""){
					alert("Debe seleccionar un Profesor.");
					return;
				}
				break;
			case "0002":
				codTipoReporte = opcion;
				codEncuesta = document.getElementById("txtCodEncuestaEstandar").value;
				if(fc_Trim(codEncuesta) == ""){
					alert("Debe ingresar un n�mero de encuesta.");
					document.getElementById("txtCodEncuestaEstandar").value="";
					document.getElementById("txtCodEncuestaEstandar").focus();
					return;
				}
				codTipoResultado = fc_CodTipoResEncEstandar();
				codResponsable = ""
				break;
			case "0003":
				codTipoReporte = opcion;
				codEncuesta = document.getElementById("txtCodEncuestaMixta").value;
				if(fc_Trim(codEncuesta) == ""){
					alert("Debe ingresar un n�mero de encuesta.");
					document.getElementById("txtCodEncuestaMixta").value="";
					document.getElementById("txtCodEncuestaMixta").focus();
					return;
				}
				codTipoResultado = "";
				codResponsable = "";
				break;			
		}		
	
		url="?txhCodTipoReporte="+codTipoReporte+"&txhCodEncuesta="+codEncuesta+
			"&txhCodTipoResultado="+codTipoResultado+"&txhCodResponsable="+
			codResponsable+"&txhCodPerfil=0004&txhCodUsuario="+codUsuario;
				 			
		window.open("${ctx}/encuestas/reporteEncuestas.html"+url,"","resizable=yes, menubar=yes");
	}
	function fc_CodTipoResEncProfesor(){
		var codTipoResultado = "0";		
		if(document.getElementById("tipoResultadoProfesor01").checked==true){
			codTipoResultado="0";//consolidado
		}
		else{
			codTipoResultado="1";//detallado
		}
		return codTipoResultado;
	}
	function fc_CodTipoResEncEstandar(){
		var codTipoResultado = "0";		
		if(document.getElementById("tipoResultadoEstandar01").checked==true){
			codTipoResultado="0";//consolidado
		}
		else{
			codTipoResultado="1";//detallado
		}
		return codTipoResultado;
	}	
	function fc_Limpiar(){
		if(document.getElementById("txhPerfilUsuario").value != "2"){
			document.getElementById("txhCodResponsable").value="";
			document.getElementById("txtUsuResponsable").value="";
			document.getElementById("txtResponsable").value="";	
		}
		document.getElementById("txtCodEncuestaProfesor").value="";		
		document.getElementById("iFrameBandejaEncuesta").src = "";	
		document.getElementById("divResultado").style.display = "none";
		
	}
	
	function fc_Buscar(){
		codTipoReporte = "0001";
		codEncuesta = document.getElementById("txtCodEncuestaProfesor").value;
		codUsuario = document.getElementById("txhCodUsuario").value;
		if(codEncuesta==""){
			alert("Debe ingresar un n�mero de encuesta.");
			document.getElementById("txtCodEncuestaProfesor").focus();
			return;
		}
		codTipoResultado = fc_CodTipoResEncProfesor();
		codResponsable = document.getElementById("txhCodResponsable").value;
		if(codResponsable==""){
			alert("Debe seleccionar un Profesor.");
			return;
		}
		url="?txhCodTipoReporte="+codTipoReporte+"&txhCodEncuesta="+codEncuesta+
			"&txhCodTipoResultado="+codTipoResultado+"&txhCodResponsable="+codResponsable+"&txhPagina=true&txhCodUsuario="+codUsuario;
			
		document.getElementById("iFrameBandejaEncuesta").src = "${ctx}/encuestas/reporteEncuestas.html"+url;
		
		document.getElementById("divResultado").style.display = "";
	}
	
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/consultaReportes.html">
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	
	<form:hidden path="nomUsuario" id="txhNomUsuario" />
	<form:hidden path="perfilUsuario" id="txhPerfilUsuario" />
	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="240px" class="opc_combo">
		 		<font style="">
		 			Consultas y Reportes					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" class="tabla" style="width:97%;margin-left:9px;margin-top: 4px;height: 60px;" 
		cellspacing="6" cellpadding="0" border="0" bordercolor="red">
		<tr>	
			<td nowrap="nowrap">&nbsp;Reportes :&nbsp;&nbsp;&nbsp;				
				
				<form:select path="codReportes" id="cboReportes" cssClass="cajatexto_o" 
								cssStyle="width:350px;" onchange="javascript:fc_CambiaReporte();">
				<form:option value="">--Seleccione--</form:option>
				<% if( request.getAttribute("listaReportesTotal")!=null ){%>					
					
					<%List opciones = (List) request.getAttribute("listaReportesTotal");
						TipoTablaDetalle obj = null;
						for( int i=0;i<opciones.size();i++ ){	obj = (TipoTablaDetalle)opciones.get(i);%>
								
								<!--SI ES EL REPORTE 0001 Y EL USUARIO TIENE PERMISO-->
								<%if( "0001".equalsIgnoreCase(obj.getCodTipoTablaDetalle()) && 
										CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_CONREP_3") == 1		
								){%>									
									<form:option value="<%=obj.getCodTipoTablaDetalle()%>"><%=obj.getDescripcion()%></form:option>	
								<%}%>
								
								
								<!--SI ES EL REPORTE 0002 Y EL USUARIO TIENE PERMISO-->
								<%if( "0002".equalsIgnoreCase(obj.getCodTipoTablaDetalle()) && 
										CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_CONREP_4") == 1		
								){%>
									<form:option value="<%=obj.getCodTipoTablaDetalle()%>"><%=obj.getDescripcion()%></form:option>	
								<%}%>
								
								
								<!--SI ES EL REPORTE 0003 Y EL USUARIO TIENE PERMISO-->
								<%if( "0003".equalsIgnoreCase(obj.getCodTipoTablaDetalle()) && 
										CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_CONREP_5") == 1		
								){%>
									<form:option value="<%=obj.getCodTipoTablaDetalle()%>"><%=obj.getDescripcion()%></form:option>	
								<%}%>
													
					<%}%>
					
				<%}%>
				</form:select>
			</td>
		</tr>
	</table>
	<table>	
	<!-- CONSULTAR AVANCE DE ENCUESTAS A PROFESORES -->
	<table id="tablaProfesores" style="display: none; margin-left:9px; width:97%;margin-top: 4px;" border="0" cellspacing="2" cellpadding="2" 
		height="60px" class="tabla" bordercolor="red" background="${ctx}/images/biblioteca/fondoinf.jpg">
		<tr>
			<td width="15%">Nro. Encuesta :
			</td>
			<td width="40%">
				<input id="txtCodEncuestaProfesor" class="cajatexto_o" size="20">&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscarResponsable','','${ctx}/images/iconos/buscar2.jpg',1)" id="botonBuscarResponsable">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar Responsable" id="imgBuscarResponsable" style="CURSOR: pointer" onclick="javascript:fc_BuscarEncuesta();">
				</a>
			</td>			
			<td width="45%">Tipo de Resultado:&nbsp;
				<input type="radio" id="tipoResultadoProfesor01" checked="checked" name="tipoResultadoProfesor">&nbsp;Consolidado
				<input type="radio" id="tipoResultadoProfesor02" name="tipoResultadoProfesor">&nbsp;Detallado
			</td>
		</tr>
		<tr>
			<td>Profesor :</td>
			<td nowrap="nowrap">
				<input type="hidden" id="txhCodResponsable">
				<input id="txtUsuResponsable" class="cajatexto_o" size="8" readonly="readonly">
				<form:input path="responsable" id="txtResponsable"													 
					cssClass="cajatexto_o" cssStyle="width: 70%" readonly="true"/>				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscarResponsable','','${ctx}/images/iconos/buscar2.jpg',1)" id="botonBuscarResponsable">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar Responsable" id="imgBuscarResponsable" style="CURSOR: pointer" onclick="javascript:fc_BuscarEmpleado();">
				</a>				
				&nbsp;
			</td>
			<td align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: hand" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="cursor:pointer;" onclick="javascript:fc_Buscar();"></a>
				
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
				<img src="${ctx}/images/iconos/exportarex1.jpg" align="absmiddle" alt="Excel" id="icoExcel" style="cursor:hand" onclick="javascript:fc_Generar();"></a>
				&nbsp;
			</td>
		</tr>
	</table>
	<div id="divResultado">
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="width:97%;margin-top:4px;margin-left:9px;display: none;" id="tablaBandejaProfesores">
		<tr>
			<td>		
				<iframe id="iFrameBandejaEncuesta" name="iFrameBandejaEncuesta" frameborder="0" height="320px" width="100%">
				</iframe>
			</td>
		</tr>
	</table>
	</div>
	<!-- REPORTE DE ENCUESTAS ESTANDAR -->
	<table id="tablaEstandar" style="display: none; margin-left:9px; width:97%;margin-top: 4px;" border="0" cellspacing="2" cellpadding="2" 
		height="60px" class="tabla" bordercolor="red" background="${ctx}/images/biblioteca/fondoinf.jpg">
		<tr>
			<td width="15%">Nro. Encuesta :
			</td>
			<td width="40%">
				<input id="txtCodEncuestaEstandar" class="cajatexto_o" size="20">
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscarResponsable','','${ctx}/images/iconos/buscar2.jpg',1)" id="botonBuscarResponsable">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar Responsable" id="imgBuscarResponsable" style="CURSOR: pointer" onclick="javascript:fc_BuscarEncuesta2();">
				</a>
			</td>			
			<td width="45%">Tipo de Resultado:&nbsp;
				<input type="radio" id="tipoResultadoEstandar01" checked="checked" name="tipoResultadoEstandar">&nbsp;Consolidado
				<input type="radio" id="tipoResultadoEstandar02" name="tipoResultadoEstandar">&nbsp;Detallado
			</td>
		</tr>
	</table>
	<!-- REPORTE DE ENCUESTAS MIXTAS -->
	<table id="tablaMixta" style="display: none; margin-left:9px; width:97%;margin-top: 4px;" border="0" cellspacing="2" cellpadding="2" 
		height="60px" class="tabla" bordercolor="red" background="${ctx}/images/biblioteca/fondoinf.jpg">
		<tr>
			<td width="15%">Nro. Encuesta :
			</td>
			<td width="40%">
				<input id="txtCodEncuestaMixta" class="cajatexto_o" size="20">
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscarResponsable','','${ctx}/images/iconos/buscar2.jpg',1)" id="botonBuscarResponsable">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar Responsable" id="imgBuscarResponsable" style="CURSOR: pointer" onclick="javascript:fc_BuscarEncuesta3();">
				</a>
			</td>
			<td width="45%">
			</td>
		</tr>
	</table>	
	<br>
	<table id="tablaBoton" style="display: none" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td>
			<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
				<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
			</td>
		</tr>
	</table>
	<table><tr><td height="5px"></td></tr></table>
	
	<table cellpadding="0" cellspacing="0" id="tblIframe" class="tabla" border="0" bordercolor="red" 
		style="display: none;margin-left:11px; width:90%">
		<tr><td><iframe id="iFrame" name="iFrame" frameborder="0" height="250px" width="100%"></iframe>
		</td></tr>
	</table>
	
</form:form>
	<script type="text/javascript">		
	</script>
</body>