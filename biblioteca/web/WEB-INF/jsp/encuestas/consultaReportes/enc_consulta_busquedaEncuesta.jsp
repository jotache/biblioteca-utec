<%@ include file="/taglibs.jsp"%>
<style>
.tablagrilla02
{
    /*BACKGROUND-COLOR: #CEE3F6;*/
    BACKGROUND-COLOR: #C8E8F0;
	FONT-SIZE: 8pt;
    /*TEXT-TRANSFORM: capitalize;*/
    WIDTH: 100%;
    FONT-FAMILY: Arial;
    color: #606060;
    
}
</style>
<head>
<script language=javascript>
	function onLoad(){
		fc_verificaTipoBusqueda();
		fc_TipoAplicacion();
		fc_IntercalarColorColumnas();
		
		
	}
	
	function fc_verificaTipoBusqueda(){
		
		tipoBusqueda = document.getElementById("txhTipoBusqueda").value;	
		
		if(tipoBusqueda=="1"){
		
			document.getElementById("codTipoEncuesta").value = "0001";
			document.getElementById("codTipoEncuesta").disabled = true;

			
		}else if(tipoBusqueda=="2"){
			document.getElementById("codTipoEncuesta").value = "0001";
			document.getElementById("codTipoEncuesta").disabled = true;
			document.getElementById("codTipoEstado").value = "0004";
			document.getElementById("codTipoEstado").disabled = true;				
			
		}else if(tipoBusqueda=="3"){
			document.getElementById("codTipoEstado").value = "0004";
			document.getElementById("codTipoEstado").disabled = true;		
		}
		
		
	}
	
	function fc_habilita(){
		tipoBusqueda = document.getElementById("txhTipoBusqueda").value;		
		
		if(tipoBusqueda=="1"){
		
			document.getElementById("codTipoEncuesta").value = "0001";
			document.getElementById("codTipoEncuesta").disabled = false;

			
		}else if(tipoBusqueda=="2"){
			document.getElementById("codTipoEncuesta").value = "0001";
			document.getElementById("codTipoEncuesta").disabled = false;
			document.getElementById("codTipoEstado").value = "0004";
			document.getElementById("codTipoEstado").disabled = false;				
			
		}else if(tipoBusqueda=="3"){
			document.getElementById("codTipoEstado").value = "0004";
			document.getElementById("codTipoEstado").disabled = false;		
		}
	}
	
	function fc_deshabilita(){
		fc_verificaTipoBusqueda();
	}	
	
	function fc_IntercalarColorColumnas(){
		//tamTabla=document.getElementById("txhTamListaBandeja").value;
		tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');		
		numeroHijoTabla = hijosTr.length;
		//alert(numeroHijoTabla)
		
		if(numeroHijoTabla > 2){
			for(var i=1;i<numeroHijoTabla;i++){			
				trHijo=document.getElementById("tablaBandeja").rows[i];
				if(i%2){				
					for(var j=0;j<6;j++){
						trHijo.cells[j].className="tablagrilla02";					
					}					
				}
			}
		}		
	}
	
	function fc_Aceptar(){
		nroEnc = document.getElementById("txhValTipoEncuesta").value;
		if(nroEnc!="")
		{
			tipo = document.getElementById("txhTipo").value
			if(tipo=="1")
			window.opener.document.getElementById("txtCodEncuestaProfesor").value =nroEnc;
			else if(tipo=="2")
			window.opener.document.getElementById("txtCodEncuestaEstandar").value =nroEnc;
			else if(tipo=="3")
			window.opener.document.getElementById("txtCodEncuestaMixta").value =nroEnc;
			
			window.close();
		}
		else
			alert("Seleccióne registro");	 
	
	}
		

	function fc_Buscar(){
		/*document.getElementById("txhOperacion").value = "BUSCAR";
	  	document.getElementById("frmMain").submit();
	  	document.getElementById("txhBandera").value="1";*/
	  	
	  	fc_habilita();
	  	document.getElementById("iFrameBandejaEncuesta").src="${ctx}/encuestas/busquedaEncuestaIframe.html?"+
	  	"txhCodUsuario="+document.getElementById("txhCodUsuario").value +
		"&txhParCodTipoAplicacion=" + document.getElementById("codTipoAplicacion").value +
		"&txhParCodTipoEncuesta=" + document.getElementById("codTipoEncuesta").value +
		"&txhParCodTipoServicio=" + document.getElementById("codTipoServicio").value +
		"&txhParCodTipoEstado=" + document.getElementById("codTipoEstado").value +
		"&txhParBandera=" + document.getElementById("txhBandera").value +
		"&txhTxtBusqueda=" + document.getElementById("txtNombre").value;
		
		fc_deshabilita();
	}
	
	function fc_Limpiar(){
		fc_habilita();
		document.getElementById("codTipoAplicacion").value="-1";
		document.getElementById("codTipoEncuesta").value="-1";
		document.getElementById("codTipoServicio").value="-1";
		document.getElementById("codTipoEstado").value="-1";
		document.getElementById("txtNombre").value="";
		fc_deshabilita();
		fc_TipoAplicacion();
	}	

	function fc_TipoAplicacion(){
	  if(document.getElementById("codTipoAplicacion").value!=document.getElementById("txhConsteAplicacionServicio").value)
	  {       td_Servicio.style.display = 'none';
	  		  td_Servicio1.style.display = 'none';
	  		  document.getElementById("codTipoServicio").value="-1"; 
	  }
	  else{ if(document.getElementById("codTipoAplicacion").value==document.getElementById("txhConsteAplicacionServicio").value)
	  		{ td_Servicio.style.display = 'inline-block';
	  		  td_Servicio1.style.display = 'inline-block';
	 		}
	  } 
	}
	
	function fc_SeleccionarRegistro(srtInd, codEncuesta, codEstado){
	   //alert(srtInd+">><<"+codEncuesta+">><<"+codEstado);	  
	  document.getElementById("txhCodSelec").value = codEncuesta;		  
	   document.getElementById("txhValTipoEncuesta").value = codEstado;
	  
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/busquedaEncuesta.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="consteTipoPrograma" id="txhConsteTipoPrograma"/>
<form:hidden path="consteTipoServicios" id="txhConsteTipoServicios"/>
<form:hidden path="consteEstadoPendiente" id="txhConsteEstadoPendiente"/>
<form:hidden path="consteEstadoAplicacion" id="txhConsteEstadoAplicacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="consteAplicacionPrograma" id="txhConsteAplicacionPrograma"/>
<form:hidden path="consteAplicacionServicio" id="txhConsteAplicacionServicio"/>
<form:hidden path="consteEncuestaGenerada" id="txhConsteEncuestaGenerada"/>
<form:hidden path="consteEncuestaPendiente" id="txhConsteEncuestaPendiente"/>
<form:hidden path="consteEncuestaAplicada" id="txhConsteEncuestaAplicada"/>
<form:hidden path="valTipoEncuesta" id="txhValTipoEncuesta"/>
<form:hidden path="bandera" id="txhBandera"/>
<form:hidden path="tamListaBandeja" id="txhTamListaBandeja"/>
<form:hidden path="dscMensaje" id="txhDscMensaje"/>

<form:hidden path="nomUsuario" id="txhNomUsuario"/>
<form:hidden path="perfilUsuario" id="txhPerfilUsuario"/>
<form:hidden path="codResponsable" id="txhCodResponsable"/>
<form:hidden path="txhTipo" id="txhTipo"/>
<form:hidden path="txhTipoBusqueda" id="txhTipoBusqueda"/>




	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Busqueda de Encuestas					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- CABECERA -->	       
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="1" cellpadding="1" border="0" bordercolor="red">	
		<tr>
			<td width="15%">&nbsp;&nbsp;Tipo Aplicación:</td>
			<td width="22%">
			  <form:select path="codTipoAplicacion" id="codTipoAplicacion" cssClass="cajatexto" 
							cssStyle="width:160px" onchange="javascript:fc_TipoAplicacion();">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoAplicacion!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoAplicacion}" />
							</c:if>
			  </form:select>
			</td>
			<td width="10%">Tipo Encuesta:</td>
			<td>
			    <form:select path="codTipoEncuesta" id="codTipoEncuesta" cssClass="cajatexto" 
							cssStyle="width:160px">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoEncuesta!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoEncuesta}" />
							</c:if>
			  </form:select>
			</td>	
			<td colspan="2" id="td_Servicio" name="td_Servicio" style="display:none" width="10%">Tipo Servicio:</td>
			<td id="td_Servicio1" name="td_Servicio1" style="display:none">
			    <form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto"
							cssStyle="width:160px">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoServicio!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoServicio}" />
							</c:if>
			  </form:select>
			</td>		
		</tr>
		<tr>
			<td>&nbsp;&nbsp;Estado:</td>			
			<td>
			   <form:select path="codTipoEstado" id="codTipoEstado" cssClass="cajatexto" 
							cssStyle="width:160px">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.listaTipoEstado!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoEstado}" />
							</c:if>
			  </form:select>
			</td>
			<td>&nbsp;</td>
			<td colspan="4" align="right">
			    &nbsp;
						
						
			</td>
		</tr>
		<TR>
		<td>&nbsp;&nbsp;Nombre :</td>
		<td colspan="3"><form:input path="txtNombre" id="txtNombre" cssClass="cajatexto" cssStyle="width:300px" maxlength="50" /></td>
		
		<TD>&nbsp;</TD>
		<TD>&nbsp;</TD>
		<td colspan="1" align="right">
			    &nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
							 style="cursor: pointer; margin-right:" 
							 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>&nbsp;
			   		 <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
							 style="cursor: pointer;" onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
						
						
			</td>
		</TR>
	</table>
	<!-- BANDEJA -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td>
			<!-- div style="overflow: auto; height:270px;width:100%">
				<display:table name="sessionScope.listaBandejaConfiguracion2" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.EncuestaDecorator"  requestURI="" 
				style="border: 1px solid #048BBA;width:98%;" id="tablaBandeja">					
					<display:column property="rbtSelEncuestaBusqueda" title="Sel"  
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:4%"/>
					<display:column property="nroEncuesta" title="Nro. Encuesta" headerClass="grilla" 
					class="tablagrilla" style="text-align:center;width:15%"/>
					<display:column property="nombreEncuesta" title="Nombre" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:18%"/>
					<display:column property="nomTipoAplicacion" title="Tipo Aplicación" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="nomTipoEncuesta" title="Tipo Encuesta" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:12%"/>
					<display:column property="nomEstado" title="Estado" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:12%"/>
								
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='tablagrilla'><td colspan='8' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</div-->
			<iframe id="iFrameBandejaEncuesta" name="iFrameBandejaEncuesta" frameborder="0" height="300px" width="100%">
			</iframe>
		  </td>		
	    </tr>
	</table>
	<table border="0" width="100%" style="margin-top: 10px">
		<tr>
			<td align="center">					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/botones/aceptar2.jpg',1)">
				<img src="${ctx}/images/botones/aceptar1.jpg" onclick="javascript:fc_Aceptar();" id="imgModificarPerfil" style="cursor:pointer;" alt="Modificar Formato"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgConfigPerfil','','${ctx}/images/botones/cancelar1.jpg',1)">
				<img src="${ctx}/images/botones/cancelar1.jpg" onclick="javascript:window.close();" id="imgConfigPerfil" style="cursor:pointer;" alt="Configurar Perfil"></a>		 			
			</td>
		</tr>
	</table>
		
</form:form>
</body>
