<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.common.MetodosConstants'%>
<%@page import='com.tecsup.SGA.bean.VerEncuestaBean'%>
<%@page import='com.tecsup.SGA.bean.ConsultaByProfesorDetBean'%>
<%@ include file="/taglibs.jsp"%> 
<html>
<head>
<script language="javascript">
	function onLoad(){	
	}
</script>
<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.cabecera_grilla_2 {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	WIDTH: 300px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px; */
	COLOR: #636563; */
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	WIDTH: 300px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
</style>
</head>
<body>
<div style="overflow: auto; height: 320px" >
<% if ( request.getAttribute("LST_RESULTADO")!= null ){
		System.out.println("primero entro if");
		List consulta = (List) request.getAttribute("LST_RESULTADO");
		int tamConsulta=consulta.size();
		int indice=0;
		int nroPreguntas=0;		
		
		if(tamConsulta > 0){
			//PARA OBTENER EL NRO DE PREGUNTAS
			nroPreguntas=1;
			ConsultaByProfesorDetBean reg1 = (ConsultaByProfesorDetBean) consulta.get(0);				
			for(int i = 1; i < tamConsulta; i++){
				ConsultaByProfesorDetBean reg2 = (ConsultaByProfesorDetBean) consulta.get(i);
				
				if(reg1.getCodPregunta().equalsIgnoreCase(reg2.getCodPregunta()) && reg1.getCodSeccion().equalsIgnoreCase(reg2.getCodSeccion()) &&
						reg1.getCodGrupo().equalsIgnoreCase(reg2.getCodGrupo())){
					nroPreguntas++;
				}
				else{
					i = tamConsulta;
				}
			}
			System.out.println("nroPreguntas>>"+nroPreguntas);
		}
		else{
			nroPreguntas=3;
		}
		//********************************	
	%>
<table class="tabla" style="width:98%;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red">
	<tr>
		<td style="width: 10%">Sede :</td>
		<td style="width: 20%">
			<input type="text" value="${model.NOM_SEDE}" size="15" class="cajatexto_1" readonly="readonly">
		</td>
		<td style="width: 12%">Tipo Aplicación :</td>
		<td style="width: 20%">
			<input type="text" value="${model.NOM_TIPOAPLICACION}" size="30" class="cajatexto_1" readonly="readonly">
		</td>		
		<td style="width: 12%">Perfil :</td>
		<td style="width: 20%">
			<input type="text" value="${model.NOM_PERFIL}" size="30" class="cajatexto_1" readonly="readonly">
		</td>						
	</tr>
	<tr>		
		<td>Nro. Encuesta :</td>
		<td>
			<input type="text" value="${model.COD_ENCUESTA}" size="15" class="cajatexto_1" readonly="readonly">
		</td>
		<td>Nombre Encuesta :</td>
		<td>
			<input type="text" value="${model.NOM_ENCUESTA}" size="40" class="cajatexto_1" readonly="readonly">
		</td>
	</tr>
	<tr>		
		<td>Producto :</td>
		<td>
			<input type="text" value="${model.NOM_PRODUCTO}" size="40" class="cajatexto_1" readonly="readonly">
		</td>
		<td>Programa :</td>
		<td>
			<input type="text" value="${model.NOM_PROGRAMA}" size="40" class="cajatexto_1" readonly="readonly">
		</td>
		<td>Nombre Profesor :</td>
		<td>
			<input type="text" value="${model.NOM_PROFESOR}" size="40" class="cajatexto_1" readonly="readonly">
		</td>				
	</tr>
</table>
<%if(tamConsulta > 0){%>
<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
	<tr style="border: 0px;">
		<td colspan="5">
		</td>
		<td class="grilla" colspan="<%=nroPreguntas %>">
			Resultado			
		</td>
	</tr>	
	<tr>			
		<td class="grilla" style="width: 15%;" >Formato</td>
		<td class="grilla" style="width: 15%;" >Departamento</td>
		<td class="grilla" style="width: 15%;" >Curso</td>					
		<td class="grilla" style="width: 15%;" >Grupo de Preguntas</td>
		<td class="grilla" style="width: 15%;" >Pregunta</td>
		<%
		for(int i=0; i< nroPreguntas; i++){
			ConsultaByProfesorDetBean reg = (ConsultaByProfesorDetBean) consulta.get(i);%>
			<td class="grilla" colspan="1" style="width: 5%;">
				<%=reg.getNomAlternativa()%>
			</td>
			<%
		}
		%>
		<td class="grilla" style="width: 10%;">Promedio</td>
	</tr>
								
		<%
		float totalPromedio=0;
		DecimalFormat df = new DecimalFormat("0.00");
		float alternativa=0;
		String variable="";
		while(indice!=tamConsulta){
			ConsultaByProfesorDetBean obj1 = (ConsultaByProfesorDetBean) consulta.get(indice);			
				%>
			<tr style="vertical-align: middle;">		
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj1.getNomSeccion()%></td>
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj1.getNomDepartamento()%></td>
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj1.getNomCurso()%></td>												
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj1.getNomGrupo()%></td>						
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj1.getNomPregunta()%></td>						
				<td style="text-align:right;" class="tablagrilla"  style="width: 5%;"><%=obj1.getTotalAlternativa()%></td>
				<%						
				variable=obj1.getTotalAlternativa().replace(",",".");
				alternativa=Float.parseFloat(variable);
				totalPromedio=alternativa;
				for(int i=1; i<nroPreguntas; i++ ){
					ConsultaByProfesorDetBean obj = (ConsultaByProfesorDetBean) consulta.get(indice+i);
					
					variable=obj.getTotalAlternativa().replace(",",".");
					
					alternativa=Float.parseFloat(variable);
					totalPromedio=totalPromedio+alternativa;
					%>
					<td style="text-align:right;" class="tablagrilla" style="width: 5%;">
					<%=obj.getTotalAlternativa()%>
					</td>
					<%
				}
				totalPromedio=totalPromedio/nroPreguntas;
				%>
				<td style="text-align:right;" class="tablagrilla" style="width: 10%;"><%=df.format(totalPromedio)%></td>
			</tr>
		<%
		indice=indice+nroPreguntas;
		}
	%>	
</table>
<table cellpadding="0" cellspacing="0">	
	<tr>			
		<td class="tabla2">
			Leyenda
		</td>
	</tr>
	<tr>
		<td class="tabla2">
		<%		
		if ( request.getAttribute("LST_LEYENDA")!= null ){			
			List listaLeyenda = (List) request.getAttribute("LST_LEYENDA");			
			if(listaLeyenda.size() > 0){
				VerEncuestaBean obj = (VerEncuestaBean) listaLeyenda.get(0);
				%>
				<%=MetodosConstants.getLeyendaStandar(obj)%>
				<%
			}
			request.removeAttribute("LST_LEYENDA");
		}	 
		%>
		</td>		
	</tr>
	<tr>
		<td></td>
	</tr>
</table>
<br>
	<%
		}
	}
	request.removeAttribute("LST_RESULTADO");
	
	if ( request.getAttribute("LST_RESULTADO_2")!= null ){
		List consulta_2 = (List) request.getAttribute("LST_RESULTADO_2");
		if(consulta_2.size() > 0){
	%>

<table cellpadding="0" cellspacing="0">
	<tr>		
		<td class="tabla2">Preguntas Abiertas
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
	<tr>
		<td class="grilla" style="width: 15%;">Formato</td>
		<td class="grilla" style="width: 15%;">Departamento</td>
		<td class="grilla" style="width: 15%;">Curso</td>					
		<td class="grilla" style="width: 15%;">Grupo de Preguntas</td>
		<td class="grilla" style="width: 20%;">Pregunta</td>					
		<td class="grilla" style="width: 18%;">Respuesta</td>
	</tr>
			<%
			for(int i = 0; i < consulta_2.size(); i++){
				ConsultaByProfesorDetBean obj = (ConsultaByProfesorDetBean) consulta_2.get(i);
			%><tr style="vertical-align: middle;">														
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj.getNomSeccion()%></td>
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj.getNomDepartamento()%></td>
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj.getNomCurso()%></td>													
				<td style="text-align:left;" class="tablagrilla" style="width: 15%;"><%=obj.getNomGrupo()%></td>						
				<td style="text-align:left;" class="tablagrilla" style="width: 20%;"><%=obj.getNomPregunta()%></td>						
				<td style="text-align:left;" class="tablagrilla" style="width: 18%;"><%=obj.getRespuesta()%></td>
			</tr>
			<%
			}
			%>
</table>
	<%
		}
	}
	request.removeAttribute("LST_RESULTADO_2");
	%>
</div>	
</body>
</html>