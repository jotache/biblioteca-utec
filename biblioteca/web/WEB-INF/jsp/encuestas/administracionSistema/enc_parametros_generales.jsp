<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){	
		objMsj = document.getElementById("txhMsg");
		if(objMsj.value == "OK_GRABAR"){
			alert(mstrGrabar);
		}
		else
		if(objMsj.value == "ERROR_GRABAR"){
			alert(mstrProblemaGrabar);
		}
		document.getElementById("txhOperacion").value="";
		document.getElementById("txhMsg").value="";
	}
	
	function fc_Grabar(){
		nroDias = document.getElementById("txtNroDias").value;
		if(nroDias == "" && fc_Trim(nroDias)==""){
			alert("Debe Ingresar el Nro. de Dias.");
			return;
		}
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
				
	}
	
	function fc_Agregar(){
		
	}		
</script>
</head>
<body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/parametrosGenerales.html">

	<form:hidden path="operacion" id="txhOperacion" />	
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="523px" class="opc_combo"><font style="">Par�metros Generales</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
			
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" 
		style="width:92%; margin-left:4px; margin-top: 10px" class="tabla" height="50px" bordercolor="red">
		<tr>
			<td width="30%">Nro. Dias Antes del Env�o de Correo:</td>
			<td width="70%">
				<form:input path="nroDias" id="txtNroDias" maxlength="2"
					onkeypress="fc_ValidaNumero();"
					onblur="fc_ValidaNumeroFinal(this,'Nro. de Dias');fc_ValidaNumeroFinalMayorCero(this);"
					cssClass="cajatexto" size="5"/>
			</td>			
		</tr>
	</table>
	<table border="0" width="92%" style="margin-top: 10px">
		<tr>
			<td align="center">					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar2.jpg" onclick="javascript:fc_Grabar();" id="imgGrabar" style="cursor:pointer;" alt="Grabar"></a>
			</td>
		</tr>
	</table>
</form:form>
</body>
</html>