<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script language=javascript>
    function onLoad()
    {   objMsg = document.getElementById("txhMsg");
	   	if ( objMsg.value == "OK" )
		{
			window.opener.fc_Buscar();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else{ if ( objMsg.value == "ERROR" )
		       {
			         alert(mstrProblemaGrabar);			
		       }
		      else{ if ( objMsg.value == "DUPLICADO" )
		            alert(mstrExistenRegistros);}
		}
	document.getElementById("txhMsg").value="";	
	}
	function fc_Valida(){
	
	  return true;
	}
	function fc_Grabar(){
		objDescripcion = document.getElementById("tipoFormato").value;
		if (  fc_Trim(objDescripcion) == "")
		{   
			alert(mstrIngreseDescripcion);
			return;
		}
		else{
		
		document.getElementById("txhOperacion").value = "GUARDAR";
		document.getElementById("frmMain").submit();
		}
	
	}
    /*onkeypress="fc_ValidaNombreAutorOnkeyPress();"
	  onblur="fc_ValidaNombreAutorOnblur(this.id,'Tipo de Formato');" */
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/tipo_formato_agregar.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codSeleccion" id="txhCodSeleccion"/>
<form:hidden path="codOpcion" id="txhCodOpcion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="tipoBandera" id="txhTipoBandera"/>

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo"><font style="">Mantenimiento de Tipo Formato</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			 </tr>
		</table>
		<table><tr height="1px"><td></td></tr></table>
		<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="2" border="0" bordercolor='gray'>  
			
			<tr>
				<td nowrap="nowrap" align="left">Tipo de Formato:</td>
				<td>&nbsp;			
					<form:input path="tipoFormato" id="tipoFormato" 
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this,'Tipo de Formato');"
					cssClass="cajatexto" size="50" maxlength="50"/>
				</td>
			</tr>		
		</table>
		<table><tr height="5px"><td></td></tr></table>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
			</tr>
		</table>

</form:form>
</body>
</html>