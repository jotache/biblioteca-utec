<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
	
	document.getElementById("txhCodSelec").value="";
	if ( document.getElementById("txhMsg").value=="OK")
		{   document.getElementById("txhMsg").value="";
			fc_Buscar();
			alert(mstrElimino);
				
		}		
		else if ( document.getElementById("txhMsg").value== "ERROR" )
		{
			   alert(mstrNoElimino);			
		}
		document.getElementById("txhMsg").value="";
	}
	
	function fc_Buscar()
	{ document.getElementById("txhOperacion").value = "BUSCAR";
	  document.getElementById("frmMain").submit();
	}
	
	function fc_Limpiar(){
	 document.getElementById("dscTipoFormato").value="";
	
	}
	
	function fc_Agregar()
	{srtcodigo=document.getElementById("txhCodSelec").value;
	 srtTipoBandera="1";
	 Fc_Popup("${ctx}/encuestas/tipo_formato_agregar.html?txhCodUsuario="+
	 document.getElementById("txhCodUsuario").value+ "&txhTipoBandera=" + srtTipoBandera +
	 "&txhCodSelec=" + srtcodigo,450,160); 
		
	}
	function fc_Eliminar()
	{if(document.getElementById("txhCodSelec").value != "" ){
						if(confirm(mstrSeguroEliminar1)){  
						    document.getElementById("txhOperacion").value = "ELIMINAR";
							document.getElementById("frmMain").submit();
						
						}
		}
	    else alert(mstrSeleccione); 
	}
	
	function fc_Modificar(){
	if(document.getElementById("txhCodSelec").value!=""){
		srtcodigo=document.getElementById("txhCodSelec").value;
		srtdescripcion=document.getElementById("txhDescripSelec").value;
		srtTipoBandera="2";
		//alert(srtdescripcion);		
		Fc_Popup("${ctx}/encuestas/tipo_formato_agregar.html?txhCodUsuario="+
		document.getElementById("txhCodUsuario").value+ "&txhCodSelec=" + srtcodigo
		+ "&txhDescripSelec=" + srtdescripcion+ "&txhTipoBandera=" + srtTipoBandera,450,160); 	
	
	 }
	  else alert(mstrSeleccione); 
	}
	
	function fc_SeleccionarRegistro(srtId, srtCodigo){
	//alert(document.getElementById("text"+srtId).value);
	document.getElementById("txhCodSelec").value=srtCodigo;	
	document.getElementById("txhDescripSelec").value=document.getElementById("text"+srtId).value;
	
	}
	/*onkeypress="fc_ValidaNombreAutorOnkeyPress();"
	onblur="fc_ValidaNombreAutorOnblur(this.id,'Tipo de Formato');" */
	//window.opener.document.getElementById("txtNroIngreso").value=document.getElementById("txhCodMaterial").value;	
</script>
</head>
<body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/tipoFormato.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codUsuario" id="txhCodUsuario" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="523px" class="opc_combo"><font style="">Consulta Tipo de Formato</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
			
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" 
		style="width:92%; margin-left:4px; margin-top: 10px" class="tabla" height="50px" bordercolor="red">
			<tr>
				<td class="" width="20%">Tipo de Formato:</td>
				<td width="20%"><form:input path="dscTipoFormato" id="dscTipoFormato" maxlength="50"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this,'Tipo de Formato');"
					cssClass="cajatexto" size="50"/></td>
				<td align="right">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" 
						   style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;			
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
				</td>	
				
			</tr>
			
		</table>
		
	<table cellpadding="0" cellspacing="0" ID="Table1" width="98%" border="0"  class="" 
	        style="margin-left:3px ;margin-top: 10px">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 280px;">
				 	<display:table name="sessionScope.listaBandejaFormato" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.EncuestaDecorator"  pagesize="10" requestURI=""
						style="border: 1px solid #048BBA;width:98%;">
					    <display:column property="rbtSelTipoFormato" title="Sel."  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:2%"/>
						<display:column property="descripcion" title="Tipo de Formato" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:95%"/>
													
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
					  </display:table>
					</div>
				</td>
				<td width="30px" valign="top" align="right">
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
						<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
							id="imgModificarPerfil" onclick="javascript:fc_Modificar();">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
						<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar" style="cursor:pointer" 
							id="imgEliminarPerfil" onclick="javascript:fc_Eliminar();">
					</a>
				</td>
			</tr>
		</table>
		
</form:form>

</body>
</html>