<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script type="text/javascript">
	function onLoad(){		
	}
	
	function fc_Aparecer(param){				
		td_mantenimiento.className = "Tab_vert";		
		//td_configuraciones.className = "Tab_vert";		
		tr_mant.style.display = 'none';		
		//tr_conf.style.display = 'none';		
		switch(param){
		case '1':
				td_mantenimiento.style.display = '';
				tr_mant.style.display = '';
				td_mantenimiento.className = "Tab_vert_resaltado";	
				//td_configuraciones.className = "Tab_vert";
				break;
			/*case '2':	
				td_configuraciones.style.display = '';
				tr_conf.style.display = '';
				td_configuraciones.className = "Tab_vert_resaltado";
				td_mantenimiento.className = "Tab_vert";
				break;	*/
		}		
	}
	function fc_muestra(param){
	
		pag_tipo.className = "Tab_sub";
		pag_desc.className = "Tab_sub";
		pag_par_gen.className = "Tab_sub";
				
		switch(param){
			case '1':
				pag_tipo.className = "Tab_sub_res";
				destino = "${ctx}/encuestas/tipoServicio.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;								
				break;
			case '2':	
				pag_desc.className = "Tab_sub_res";
				destino = "${ctx}/encuestas/tipoFormato.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;								
				break;
			case '3':	
				pag_par_gen.className = "Tab_sub_res";
				destino = "${ctx}/encuestas/parametrosGenerales.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;								
				break;
			
		}
		document.getElementById("iFrame").src = destino;
	}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuEncuesta.html"
		}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuesta/mantenimiento_configuracion_enlaces.html">		
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!--Page Title -->
	<table cellSpacing="0" cellPadding="2" border="0"
		style="height: 18px; width: 98%; margin-top: 3px;margin-left:10px" bordercolor="green">	
		<tr>
			<td valign="top" width="20%">					
				<table style="HEIGHT: 20px" cellspacing="0" cellpadding="0" border="1" bordercolor="white">					
					<tr>
						<td id="td_mantenimiento"  onclick="javascript:fc_Aparecer('1');" width="12%" class="Tab_vert">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmantenimiento','','${ctx}/images/biblioteca/Mant_Conf/mantenimientos2.jpg',1)">
							<img id="imgmantenimiento" src= "${ctx}/images/biblioteca/Mant_Conf/mantenimientos1.jpg" style="cursor:pointer"></a>
						</td>
					</tr>
					<tr style="DISPLAY:none"id="tr_mant" name="tr_mant" border="0">
						<td>
							<table cellspacing="0" cellpadding="0" border="0" bordercolor="green">
								
								<tr>
										<td id="pag_tipo" width="10%" onclick="javascript:fc_muestra('1');" class="tab_sub">
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgtiposerv','','${ctx}/images/Logistica/Menu/tiposervicio1.jpg',1)">
										<img id="imgtiposerv" border="0" src= "${ctx}/images/Logistica/Menu/tiposervicio2.jpg" style="cursor:pointer"></a>
										</td>
									</tr>
								
								<tr>
									<td id="pag_desc" width="10%" onclick="javascript:fc_muestra('2');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgtipoform','','${ctx}/images/Logistica/Menu/tipoformato1.jpg',1)">
									<img id="imgtipoform" src= "${ctx}/images/Logistica/Menu/tipoformato2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
								
								<tr>
									<td id="pag_par_gen" width="10%" onclick="javascript:fc_muestra('3');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgtipoParGen','','${ctx}/images/Evaluaciones/Menu/parametrosg1.jpg',1)">									
									<img id="imgtipoParGen" src= "${ctx}/images/Evaluaciones/Menu/parametrosg2.jpg" style="cursor:pointer"></a>
									</td>
								</tr>
																	
							</table>
						</td>
					</tr>
					
				</table>
			</td>
			<td width="80%" valign="top">
				<table style="width:99%;background-color:white;margin-left: 5px"
					cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<iframe id="iFrame" name="iFrame" width="100%" height="500px" frameborder="0"
								src="${ctx}/eva/mto_menu.html">
							</iframe>
						</td>		
					</tr>
				</table>
			</td>	
		</tr>
	</table>
</form:form>
</body>
</html>