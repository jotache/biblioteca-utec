<%@ include file="/taglibs.jsp"%>

<head>
	<script language=javascript>
	function onLoad(){
	
	if ( document.getElementById("txhMsg").value=="OK")
		{   document.getElementById("txhMsg").value="";
			alert(mstrSeGraboConExito);
		}		
		else if ( document.getElementById("txhMsg").value== "ERROR" )
		{
			   alert(mstrProblemaGrabar);			
		}
		document.getElementById("txhMsg").value="";
	}
	
	function fc_Grabar(){
	var bandera=fc_Validar();
	
	if(bandera==1)
		{  	document.getElementById("txhOperacion").value = "GUARDAR";
	  		document.getElementById("frmMain").submit();
		}
	}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuEncuesta.html"
		}
		
	function fc_Buscar(){
	Fc_Popup("${ctx}/encuestas/act_consulta_empresa.html?txhCodUsuario="+
	document.getElementById("txhCodUsuario").value,550,350);
	}
	
	function fc_Validar(){
	var band=1;
	
	if( fc_Trim(document.getElementById("primerNombre").value)!="")
	{  if(fc_Trim(document.getElementById("apellidoPaterno").value)!="")
		 {  if(fc_Trim(document.getElementById("apellidoMaterno").value)!="")
			{  if(fc_Trim(document.getElementById("codTipoDocumento").value)!="-1")
			    { if(fc_Trim(document.getElementById("nroDocumento").value)!="")
					 {  if(fc_Trim(document.getElementById("email").value)!="")
							 {  if(fc_Trim(document.getElementById("domicilio").value)!="")
							     {  //if(fc_Trim(document.getElementById("empresa").value)!="")
							           band=1;
							       /* else{ alert("Debe ingresar una empresa.");  
							               band=0;
							           }*/
							     }
							   else{ alert("Debe ingresar la direcci�n de su domicilio.");  
							         band=0;
							   }
						   }
						   else{ alert("Debe ingresar su email.");  
						         band=0;
						   }
				     }
				   else{ alert("Debe ingresar su Nro de Documento.");  
				         band=0;
				       } 
				 }
				 else{ alert("Debe Seleccionar un Tipo de Documento.");  
		                band=0;
				 }
		     }
		    else{ alert("Debe ingresar su apellido materno.");  
		         band=0;
		   }
     	}
   		else{ alert("Debe ingresar su apellido paterno.");  
         band=0;
  		}
	}    
	else{ alert("Debe ingresar su primer nombre.");  
	      band=0;
	     }
	  return band;   
	}
	
	function fc_EnviarEmail(){
		document.getElementById("txhOperacion").value = "ENVIAR_EMAIL";
	  	document.getElementById("frmMain").submit();
	}
	/*
	<td><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviar01','','${ctx}/images/botones/enviar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/enviar1.jpg" id="imgEnviar01" style="cursor:pointer;" onclick="javascript:fc_EnviarEmail();"></a>
	</td>
	*/
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/actualizacion_datos.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codEmpresa" id="txhCodEmpresa"/>

	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Actualizaci�n de Datos		 								 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- CABECERA -->		 
	<table cellpadding="2" cellspacing="2" style="margin-left:9px;width:97%;margin-top:6px;" bgcolor=#048BBA>
		<tr>
			<td style="text-align:left;font-family: Arial;font-size: 12px;color: white;">DATOS PERSONALES</td>			
		</tr>
	</table>
	<table class="tabla" style="width:97%;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red">
			<tr>
				<td width="20%" class="">Primer Nombre :</td>
				<td width="30%"><form:input path="primerNombre" id="primerNombre" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Primer Nombre');" 
					cssClass="cajatexto_o" size="50"/>
				</td> 
				<td width="20%">Segundo Nombre :
				<td width="30%"><form:input path="segundoNombre" id="segundoNombre" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Segundo Nombre');" 
					cssClass="cajatexto" size="50"/>
				</td>
				<!--td width="20%" rowspan="5"><img src="../images/jav.JPG" width="100" height="120px"></td-->
			</tr>
			<tr>
				<td width="20%" class="">Apellido Paterno :</td>
				<td width="30%"><form:input path="apellidoPaterno" id="apellidoPaterno" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Apellido Paterno');" 
					cssClass="cajatexto_o" size="30"/>
				</td>
				<td width="20%" class="">Apellido Materno :</td>
				<td width="30%"><form:input path="apellidoMaterno" id="apellidoMaterno" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Apellido Materno');" 
					cssClass="cajatexto_o" size="30"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">Tipo de Documento :</td>
				<td width="30%">
				    <form:select path="codTipoDocumento" id="codTipoDocumento" cssClass="combo_o" 
								cssStyle="width:220px">
								<form:option value="-1">--Seleccione--</form:option>
								<c:if test="${control.listaTipoDocumento!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listaTipoDocumento}" />
								</c:if>
				   </form:select>
				</td>
				<td width="20%" class="">Nro. Documento :</td>
				<td width="30%"><form:input path="nroDocumento" id="nroDocumento" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Email');" 
					cssClass="cajatexto_o" size="30"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">Email :</td>
				<td width="30%" colspan="3"><form:input path="email" id="email" maxlength="50"
					
					onblur="fc_ValidaTextoGeneralEmail(this.id,'Email');" 
					cssClass="cajatexto_o" size="55"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">Domicilio :</td>
				<td width="30%" colspan="4"><form:input path="domicilio" id="domicilio" maxlength="150"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Domicilio');" 
					cssClass="cajatexto_o" size="75"/>&nbsp;(Avenida, Calle, N�mero)</td>
			</tr>
			<tr>
				<td width="20%" class="">Tel�fono Domicilio :</td>
				<td width="30%"><form:input path="telefonoDomicilio" id="telefonoDomicilio" maxlength="10"
					onkeypress="fc_ValidaNumeroGuion();"
					onblur="fc_ValidaNumeroGuionOnblur(telefonoDomicilio,'Tel�fono Domicilio');" 
					cssClass="cajatexto" size="10"/>
				</td>
				<td width="20%" class="">Tel�fono Movil :</td>
				<td width=30%"><form:input path="telefonoMovil" id="telefonoMovil" maxlength="10"
					onkeypress="fc_ValidaNumeroGuion();"
					onblur="fc_ValidaNumeroGuionOnblur(telefonoMovil,'Tel�fono Movil');"
					cssClass="cajatexto" size="10"/>
				</td>
			</tr>
		</table>		
		<table cellpadding="2" cellspacing="2" style="margin-left:9px;width:97%;margin-top:6px;" bgcolor=#048BBA>
			<tr>
				<td style="text-align:left;font-family: Arial;font-size: 12px;color: white;">DATOS LABORALES</td>			
			</tr>
		</table>
		<table class="tabla" style="width:97%;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
					cellspacing="2" cellpadding="2" border="0" bordercolor="red">
			<tr>
				<td width="20%">Empresa :</td>
				<td width="30%"><form:input path="empresa" id="empresa" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();" readonly="true"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Empresa');" 
					cssClass="cajatexto" size="50"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					   <img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
						 style="cursor: pointer;" onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
				</td>
				<td width="20%" class="">�rea :</td>
				<td width="30%"><form:input path="area" id="area" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Area');" 
					cssClass="cajatexto" size="25"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">Cargo :</td>
				<td width="30%"><form:input path="cargo" id="cargo" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Cargo');" 
					cssClass="cajatexto" size="25"/>
				</td>
				<td width="20%" class="">Tel�fono Contacto :</td>
				<td width="30%"><form:input path="telefonoContacto" id="telefonoContacto" maxlength="10"
					onkeypress="fc_ValidaNumeroGuion();"
					onblur="fc_ValidaNumeroGuionOnblur(telefonoContacto,'Contacto');"
					cssClass="cajatexto" size="10"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">Email :</td>
				<td width="20%" colspan="3"><form:input path="emailLaboral" id="emailLaboral" maxlength="50"
					
					onblur="fc_ValidaTextoGeneralEmail(this.id,'Email');"
					cssClass="cajatexto" size="50"/>
				</td>
			</tr>
		</table>
		<br>
		<table align=center>
			<tr>
				<td><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imgGrabar01" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
				</td>
			</tr>
		</table>
</form:form>
</body>
