<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){	
	document.getElementById("txhCodSelec").value="";
	fc_Buscar();
	}
	
	function fc_Regresar()
		{
			document.location.href = "${ctx}/menuEncuesta.html"
		}
		
	function fc_SeleccionarRegistro(srtId, srtCodigo){
	
	document.getElementById("txhDescripSelec").value=srtId; //document.getElementById("text"+srtId).value;
	document.getElementById("txhCodSelec").value=srtCodigo;
	
	}
	
	function fc_Aceptar(){
	if(document.getElementById("txhCodSelec").value!="")
	  {	window.opener.document.getElementById("empresa").value=document.getElementById("txhDescripSelec").value;
		window.opener.document.getElementById("txhCodEmpresa").value=document.getElementById("txhCodSelec").value;
	    window.close();
	  }
	 else alert(mstrSeleccion);
	}
	
	function fc_Buscar(){
	     /*document.getElementById("txhOperacion").value = "BUSCAR";
	  	 document.getElementById("frmMain").submit();*/
	  	 document.getElementById("iFrameBandejaAplicacion").src="${ctx}/encuestas/act_consulta_empresa_iframe.html?"+
	  	 "txhCodUsuario="+document.getElementById("txhCodUsuario").value+ 
	  	 "&txhNombre="+document.getElementById("nombre").value;
	}
	//window.opener.document.getElementById("txtNroIngreso").value=document.getElementById("txhCodMaterial").value;
	
	function fc_Limpiar(){
	  document.getElementById("nombre").value="";
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/act_consulta_empresa.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codUsuario" id="txhCodUsuario" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top: 9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="345px" class="opc_combo">
		 		<font style="">
		 			Consulta de Empresa					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>	
	
	<table class="tabla" style="width:97%;margin-top:4px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="2" border="0" bordercolor="red">	
		<tr>
			<td class="" width="10%">Nombre :</td>
			<td width="20%">
				<form:input path="nombre" id="nombre" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Nombre');" 
					cssClass="cajatexto" size="50"/>
			</td>
			<td align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
			</td>
		</tr>
	</table>
	
	<!-- table cellpadding="0" cellspacing="0" ID="Table1" width="97%" border="0"  class="" 
	       style="margin-left:9px ;margin-top: 4px">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 220px;">
				 	<display:table name="sessionScope.listaBandejaEmpresa" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.EncuestaDecorator"  pagesize="7" requestURI=""
						style="border: 1px solid #048BBA;width:100%;">
						
					    <display:column property="rbtSelEncuestaEmpresa" title="Sel."  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:7%"/>
						<display:column property="nombreEmpresa" title="Nombre" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:93%; margin-left: 5px;"/>
						
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
					  </display:table>
					</div>
					
				</td>
				
			</tr>
		</table-->
		<iframe id="iFrameBandejaAplicacion" name="iFrameBandejaAplicacion" frameborder="0" height="220px" width="97%">
		</iframe>
			
		<table width="100%">
			<tr>
				<td align="center">
				    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar01','','${ctx}/images/botones/aceptar2.jpg',1)">
						<img alt="Aceptar" src="${ctx}/images/botones/aceptar1.jpg" id="imgGrabar01" style="cursor:pointer;" onclick="javascript:fc_Aceptar();"></a>
								
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar01','','${ctx}/images/botones/cancelar2.jpg',1)">
						<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar01" style="cursor:pointer;" onclick="window.close();"></a>
					
					
				</td>
			</tr>
		</table>
</form:form>
</body>
</html>