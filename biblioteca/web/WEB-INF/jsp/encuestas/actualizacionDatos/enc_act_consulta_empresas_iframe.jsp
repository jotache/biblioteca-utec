<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){	
	
	}		
	
	function fc_SeleccionarRegistro(srtId, srtCodigo){
	
	parent.fc_SeleccionarRegistro(document.getElementById("text"+srtId).value, srtCodigo);
	}	
	
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/act_consulta_empresa_iframe.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codUsuario" id="txhCodUsuario" />
		
	<table cellpadding="0" cellspacing="0" ID="Table1" width="95%" border="0"  class="" 
	       style="margin-left:9px ;margin-top: 4px">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 200px;">
				 	<display:table name="sessionScope.listaBandejaEmpresa" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.EncuestaDecorator"  pagesize="7" requestURI=""
						style="border: 1px solid #048BBA;width:100%;">
						
					    <display:column property="rbtSelEncuestaEmpresa" title="Sel."  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:7%"/>
						<display:column property="nombreEmpresa" title="Nombre" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:93%; margin-left: 5px;"/>
						
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
					  </display:table>
					</div>
				</td>
				
			</tr>
		</table>		
		
</form:form>
</body>
</html>