<%@ include file="/taglibs.jsp"%>
<head>
<script language=javascript>
	function onLoad(){			
		if(document.getElementById("txhTamListaEncPublicadas").value=="0"){
			fc_Regresar();
		}
	}
	function fc_Regresar(){
		document.location.href = "${ctx}/menuEncuesta.html"
	}
	function fc_refrescar(){
		document.getElementById("frmMain").submit();
	}	
	function fc_SeleccionarEncuesta(codEncuesta, codPerfil, codEncuestado, codTipoEncuesta, codProfesor){		
		codUsuario=document.getElementById("txhCodUsuario").value;
		if(codTipoEncuesta==document.getElementById("txhCodTipoEstandar").value){
			document.getElementById("iFrameEncuesta").src="${ctx}/encuestas/verEncuestas.html"+
			"?txhCodEncuesta="+codEncuesta+
			"&txhUsuario="+codUsuario+
			"&txhCodPerfil="+codPerfil+
			"&txhCodEncuestado="+codEncuestado+
			"&txhCodProfesor="+codProfesor;
		}
		else
		if(codTipoEncuesta==document.getElementById("txhCodTipoMixta").value){
			document.getElementById("iFrameEncuesta").src="${ctx}/encuestas/verEncuestasMixtas.html"+
			"?txhCodEncuesta="+codEncuesta+
			"&txhUsuario="+codUsuario+
			"&txhCodPerfil="+codPerfil+
			"&txhCodEncuestado="+codEncuestado+
			"&txhCodProfesor="+codProfesor;
		}
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/encuestas/completarEncuestaPublicada.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="msg" id="txhMsg"/>

<form:hidden path="codTipoEstandar" id="txhCodTipoEstandar"/>
<form:hidden path="codTipoMixta" id="txhCodTipoMixta"/>

<form:hidden path="tamListaEncPublicadas" id="txhTamListaEncPublicadas"/>

	<!-- ICONO DE REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>	
	<!-- TITULO -->
	<!-- <table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="775px" class="opc_combo">
		 		<font style="">
		 			Bandeja de Configuracion y Generacion de Encuestas					 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>-->
	<!-- CABECERA -->	       
	<!-- <table class="tabla" style="width:97%;margin-top:6px;margin-left:9px;"  
				cellspacing="0" cellpadding="0" border="1" bordercolor="blue">
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table> -->
	<table class="tabla" style="width:99%;margin-top:0px;margin-left:9px"  
				cellspacing="0" cellpadding="0" border="0" bordercolor="red">	
		<tr>
			<td width="20%" valign="top">
				<div style="overflow: auto; height: 200px" >
				<table cellpadding="0" cellspacing="1" id="tablaBandeja"
					style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
					<tr>
						<td class="grilla" style="text-align: left;">&nbsp;Seleccione una Encuesta</td>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaEncPublicadas}"  >
					<tr style="cursor: hand;" onclick="fc_SeleccionarEncuesta('${lista.codEncuesta}','${lista.codPerfil}','${lista.codEncuestado}','${lista.codTipoEncuesta}','${lista.codProfesor}')">						
						<td align="left" class="tablagrilla">
							&nbsp;&nbsp;&nbsp;${lista.nombreEncuesta}
						</td>	
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaEncPublicadas == "0"}'>
					<tr>
						<td align="center" class="tablagrilla">						
							No se encontraron Registros		
						</td>			
					</tr>
					</c:if>									
				</table>
				</div>
			</td>
			<td width="80%" rowspan="2">
				<iframe id="iFrameEncuesta" name="iFrameEncuesta" frameborder="0" height="505px" width="100%" scrolling="no">
				</iframe>
			</td>
		</tr>
		<tr height="10px">
			<td align="center" valign="bottom">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>
			</td>			
		</tr>
	</table>	
</form:form>
</body>
