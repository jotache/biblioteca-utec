<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.ReporteReclutamiento'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td> 
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="2" align="center"><u>SGA - SISTEMA DE RECLUTAMIENTO</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="2" align="center">REPORTE DE PROCESOS POR POSTULANTE</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Nombre :</b></td>
	<td colspan="4" align="left">${model.nombre}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Apellido Paterno :</b></td>
	<td colspan="4" align="left">${model.apePaterno}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Apellido Materno :</b></td>
	<td colspan="4" align="left">${model.apeMaterno}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="5" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">C�d Postulante</td>
				<td class="cabecera_grilla" width="5%">Nombre de Postulante</td>
				<td class="cabecera_grilla" width="5%">Estado CV</td>
				<td class="cabecera_grilla" width="5%">Nombre de Proceso</td>
				<td class="cabecera_grilla" width="5%">Tipo de Proceso</td>
				<td class="cabecera_grilla" width="5%">Estado Proceso</td>								
				<td class="cabecera_grilla" width="5%">Fecha</td>
			</tr>
			
			<%  
				if(request.getSession().getAttribute("lista_cabecera")!=null){
				System.out.println("Reporte xD");
				List consulta = (List) request.getSession().getAttribute("lista_cabecera"); 
				DecimalFormat df = new DecimalFormat("0.00");
				for(int i=0;i<consulta.size();i++){
					
					ReporteReclutamiento obj  = (ReporteReclutamiento) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:center"><%=obj.getCodPostulante()==null?"":obj.getCodPostulante()%></td>
						<td style="text-align:left"><%=obj.getDscPostulante()==null?"":obj.getDscPostulante()%></td>
						<td style="text-align:left"><%=obj.getDscEstadoCV()==null?"":obj.getDscEstadoCV()%></td>
						<td style="text-align:left"><%=obj.getDscProceso()==null?"":obj.getDscProceso()%></td>
						<td style="text-align:left"><%=obj.getDscTipoProceso()==null?"":obj.getDscTipoProceso()%></td>
						<td style="text-align:center"><%=obj.getDscEstado()==null?"":obj.getDscEstado()%></td>													
						<td style="text-align:center"><%=obj.getDscEstado()==null?"":obj.getFecProceso()%></td>
					</tr>
					<%}
				}
				request.removeAttribute("lista_cabecera"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>