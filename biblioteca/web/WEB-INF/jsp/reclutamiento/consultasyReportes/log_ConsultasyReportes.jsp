<%@ include file="/taglibs.jsp"%>
<html>
	<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-fechas.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script language=javascript>
	function onLoad()
	{
	}
	function fc_Regresar(){
	parent.location.href = "${ctx}/Reclutamiento.html";
	}
	function fc_Muestra(){
	
		if(document.getElementById("tipoReporte").value=='0003'){
			tablaGen.style.display='';	
			fechaIni.style.display='';
			nom1.style.display='none';
			genera.style.display='none';
			genera1.style.display='none';
			genera2.style.display='';
		}
		else if(document.getElementById("tipoReporte").value=='0002'){
			tablaGen.style.display='';
			fechaIni.style.display='';
			nom1.style.display='';
			genera.style.display='none';
			genera1.style.display='';
			genera2.style.display='none';
		}
		else if(document.getElementById("tipoReporte").value=='0001'){
			tablaGen.style.display='';
			fechaIni.style.display='';
			nom1.style.display='none';
			genera.style.display='';
			genera1.style.display='none';
			genera2.style.display='none';
		}		
	}
	
	function fc_Generar(){
		srtFechaInicio=document.getElementById("txhFechaIni").value;
		srtFechaFin=document.getElementById("txhFechaFin").value;
		var num=0;
		var srtFecha="0";
		if( srtFechaInicio!="" && srtFechaFin!="")
		   {   
	          num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
	          //num=1: si el primer parametro es mayor al segundo parametro
	          //num=0: si el segundo parametro es mayor o igual al primer parametro
	          //alert(num);
	          if(num==1) 	srtFecha="0";
	          else srtFecha="1";	    
		    }
		     
		 if(srtFecha=="0")   
		    {   u = "1";
				url="?txhOperacion=" + u + 			
				"&txhCodTipoPro=" + document.getElementById("tipoProceso").value +  
				"&txhCodIniFun=" + document.getElementById("unidadFuncional").value + 
				"&txhDscProceso=" + document.getElementById("nomProceso").value +
				"&txhFecIni=" + document.getElementById("txhFechaIni").value + 
				"&txhFecFin=" + document.getElementById("txhFechaFin").value ;
			//alert(url);
			window.open("/SGA/reclutamiento/reportesReclutamiento.html"+url,"ReporteReclutamiento","resizable=yes, menubar=yes");
		 }
			else alert('La fecha de inicio debe ser menor a la fecha final.');
		
	}
	
	function fc_Generar1(){
	u = "2";
		url="?txhOperacion=" + u + 			
			"&txhCodTipoPro=" + document.getElementById("tipoProceso").value +  
			"&txhCodIniFun=" + document.getElementById("unidadFuncional").value + 
			"&txhDscProceso=" + document.getElementById("nomProceso").value +
			"&txhApePaterno=" + document.getElementById("apePaterno").value +
			"&txhNombre=" + document.getElementById("nom").value +					
			"&txhApeMaterno=" + document.getElementById("apeMaterno").value + 
			"&txhFecIni=" + document.getElementById("txhFechaIni").value + 
			"&txhFecFin=" + document.getElementById("txhFechaFin").value;
	
		window.open("/SGA/reclutamiento/reportesReclutamiento.html"+url,"ReporteReclutamiento","resizable=yes, menubar=yes");
	
	}
	
	function fc_Generar2(){
		srtFechaInicio=document.getElementById("txhFechaIni").value;
		srtFechaFin=document.getElementById("txhFechaFin").value;
	    var num=0;
	     var srtFecha="0";
	     if( srtFechaInicio!="" && srtFechaFin!="")
	       {   
	           num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
	           //num=1: si el primer parametro es mayor al segundo parametro
	           //num=0: si el segundo parametro es mayor o igual al primer parametro
	           //alert(num);
	           if(num==1) 	srtFecha="0";
	           else srtFecha="1";	    
	       }
	     
	    if(srtFecha=="0")   
	    {   u = "3";
			url="?txhOperacion=" + u + 			
			"&txhCodTipoPro=" + document.getElementById("tipoProceso").value +  
			"&txhCodIniFun=" + document.getElementById("unidadFuncional").value + 
			"&txhDscProceso=" + document.getElementById("nomProceso").value +
			"&txhFecIni=" + document.getElementById("txhFechaIni").value + 
			"&txhFecFin=" + document.getElementById("txhFechaFin").value ;
			//alert(url);
			window.open("/SGA/reclutamiento/reportesReclutamiento.html"+url,"ReporteReclutamiento","resizable=yes, menubar=yes");
	    }
		else alert('La fecha de inicio debe ser menor a la fecha final.');		
	}	
</script>
	</head>

   	<form:form id="frmMain" commandName="control" action="${ctx}/reclutamiento/log_ConsultasyReportes.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="usuario" id="txhUsuario"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand" alt="Regresar"></td>
			</tr>
		</table>
	<table cellspacing="0" cellpadding="0" border="0" style="margin-left:10px">
			<tr>
				<td align="left"><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="517px" height="27px" class="opc_combo">Consultas y Reportes</td>
				<td align="right"><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
	</table>
	<table cellpadding="1" cellspacing="2" class="tablagrilla" 
			style="margin-left:10px; margin-top: 5px; width: 920px;"> 
	 <tr>
         <td width="15%">Tipo Reporte:</td>
         <td width="85%"><form:select  path="tipoReporte" id="tipoReporte" cssClass="cajatexto" 
				cssStyle="width:280px" onclick="javascript:fc_Muestra();">
			        <form:option value="">--Seleccione--</form:option>
		      	    <c:if test="${control.listReportes!=null}">
		        	<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
		        		items="${control.listReportes}" />						        
		            </c:if>
		            </form:select></td>
	  </tr>
	</table>
	
	
	<table cellpadding="1" cellspacing="2" class="tablagrilla" bordercolor="red" border="0"
			style="margin-left:10px; margin-top: 5px; width: 920px; display: none;" id="tablaGen"> 						
			        
		<tr>
			<td width="15%">Tipo Proceso:</td>
			<td>
				<form:select  path="tipoProceso" id="tipoProceso" cssClass="cajatexto" cssStyle="width:180px">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.listProcesos!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" items="${control.listProcesos}" />						        
				</c:if>
				</form:select>
			</td>
			
		</tr>
		<tr>
			<td width="15%">Unidad Funcional:</td>
			<td>
				<form:select  path="unidadFuncional" id="unidadFuncional" cssClass="cajatexto" cssStyle="width:180px">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.listUniFuncional!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" items="${control.listUniFuncional}" />						        
				</c:if>
				</form:select>
			</td>
		
		</tr>
		<tr id="nomProc" style="display: none;">
			 <td width="15%">Nombre Proceso:</td>
			 <td> <form:input path="nomProceso" id="nomProceso" 
				 onkeypress="fc_ValidaTextoNumeroEspecial();" maxlength="30" cssClass="cajatexto" size="40" />
			</td>
		</tr>
		<tr id="fechaIni" style="display: none;">
			<td width="15%">Fecha Inicio:</td>
			<td><form:input path="fechaInicio" id="txhFechaIni"
				onkeypress="fc_ValidaFecha('txhFechaIni');"	onblur="fc_ValidaFechaOnblur('txhFechaIni');" cssClass="cajatexto" size="10" />
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" style='cursor:hand;' align="absmiddle"></a>
				&nbsp;&nbsp;Fecha Fin:&nbsp;
				<form:input path="fechaFin" id="txhFechaFin"
				onkeypress="fc_ValidaFecha('txhFechaFin');"	onblur="fc_ValidaFechaOnblur('txhFechaFin');"	cssClass="cajatexto" size="10" />
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="/SGA/images/iconos/calendario1.jpg" id="imgFecFin" alt="Calendario" style='cursor:hand;' align="absmiddle"></a>
			</td>
			<td></td>
		    <td>
			</td>
		</tr>       
		<tr id="nom1" style="display: none;">
			<td width="15%">Nombre:</td>
			<td> <form:input path="nom" id="nom" onkeypress="fc_ValidaTextoNumeroEspecial();" 
			    maxlength="30" cssClass="cajatexto" size="20" />&nbsp;
				Ape. Paterno:&nbsp;
			<form:input path="apePaterno" id="apePaterno" onkeypress="fc_ValidaTextoNumeroEspecial();" 
			    maxlength="30" cssClass="cajatexto" size="20" />
				&nbsp;Ape. Materno:&nbsp;
			<form:input path="apeMaterno" id="apeMaterno" onkeypress="fc_ValidaTextoNumeroEspecial();" 
			    maxlength="30" cssClass="cajatexto" size="20" />				
			 </td>
		</tr>
		<tr id="apePaterno1" style="display: none;">
			<td width="15%">Ape Paterno:</td>
			<td> 
				<form:input path="apePaterno" id="apePaterno" onkeypress="fc_ValidaTextoNumeroEspecial();"
				   maxlength="30" cssClass="cajatexto" size="40" />
			</td>
			<td>Ape Materno:</td>
			<td> 
				<form:input path="apeMaterno" id="apeMaterno" onkeypress="fc_ValidaTextoNumeroEspecial();" 
				  maxlength="30" cssClass="cajatexto" size="40" />
			</td>
		</tr>
	</table>
	<table align="center">
		<tr>
		<td id="genera" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
		</td>
		<tr>
		<td id="genera1" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar1','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar1();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar1"></a>
		</td>
		<tr>
		<td id="genera2" style="display: none" align="center">
		<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar2','','${ctx}/images/botones/generar2.jpg',1)">
			<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar2();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar2"></a>
		</td>
		</table>
	</form:form>
	<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txhFechaIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	
	Calendar.setup({
		inputField     :    "txhFechaFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
	</script>