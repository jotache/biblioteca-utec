<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.ReporteReclutamiento'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalleNro {
               FONT-SIZE: 10px;
               /*COLOR: #636563;*/
               COLOR: #000000;
               FONT-FAMILY: Verdana;
               HEIGHT: 18px;
               CURSOR: hand;
               mso-style-parent:style0;
               mso-number-format:Standard;
       }
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td> 
	<td colspan="2"><img src="${ctx}/images/logoTecsup.jpg"></td>
	<td colspan="6" align="center"><u>SGA - SISTEMA DE RECLUTAMIENTO</u></td>
	<td align="right" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="2"></td>
	<td colspan="6" align="center">REPORTE DE PROCESOS POR PERIODO</td>
	<td align="right" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Fecha Inicio :</b></td>
	<td colspan="6" align="left">${model.fechaIni}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Fecha Fin :</b></td>
	<td colspan="6" align="left">${model.fechaFin}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="5%">Codigo</td>
				<td class="cabecera_grilla" width="5%">Tipo de Proceso</td>
				<td class="cabecera_grilla" width="5%">Nombre de Proceso</td>
				<td class="cabecera_grilla" width="5%">Unid. Funcional</td>
				<td class="cabecera_grilla" width="5%">Estado</td>
				<td class="cabecera_grilla" width="5%">Fecha Inicio</td>
				<td class="cabecera_grilla" width="5%">Fecha Fin</td>
				
				<td class="cabecera_grilla" width="10%"># Postulantes</td>
				<td class="cabecera_grilla" width="5%"># Seleccionados</td>
				<td class="cabecera_grilla" width="5%">�Tiene Oferta?</td>							
			</tr>
			
			<%  
				if(request.getSession().getAttribute("lista_cabecera")!=null){
				System.out.println("Reporte xD");
				List consulta = (List) request.getSession().getAttribute("lista_cabecera"); 
				DecimalFormat df = new DecimalFormat("0.00");
				for(int i=0;i<consulta.size();i++){
					
					ReporteReclutamiento obj  = (ReporteReclutamiento) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:center"><%=obj.getCodProceso()==null?"":obj.getCodProceso()%></td>
						<td style="text-align:left"><%=obj.getDscTipoProceso()==null?"":obj.getDscTipoProceso()%></td>
						<td style="text-align:left"><%=obj.getDscProceso()==null?"":obj.getDscProceso()%></td>
						<td style="text-align:left"><%=obj.getDscUniFuncional()==null?"":obj.getDscUniFuncional()%></td>
						<td style="text-align:left"><%=obj.getDscEstado()==null?"":obj.getDscEstado()%></td>
						<td style="text-align:center"><%=obj.getFecInicio()==null?"":obj.getFecInicio()%></td>
						<td style="text-align:center"><%=obj.getFecFin()==null?"":obj.getFecFin()%></td>
						
						<td style="text-align:right"><%=obj.getNumPostulantes()==null?"":obj.getNumPostulantes()%></td>
						<td style="text-align:right"><%=obj.getNumSeleccionados()==null?"":obj.getNumSeleccionados()%></td>
						<td style="text-align:right"><%=obj.getIndTieneEncuesta()==null?"":obj.getIndTieneEncuesta()%></td>
																
					</tr>
					<%}
				}
				request.removeAttribute("lista_cabecera"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>