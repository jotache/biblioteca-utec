<%@ include file="/taglibs.jsp"%>
<html>
	<head>	
	
	<script language=javascript>
	function onLoad()
		{var objMsg = $("txhMsg");
			if ( objMsg.value == "OK" )
			{
				window.opener.document.getElementById("frmMain").submit();
				alert(mstrSeGraboConExito);
				window.close();
			}
			else if ( objMsg.value == "ERROR" )
			{
				alert(mstrProblemaGrabar);			
			}
				else if(objMsg.value == "DOBLE"){
		alert('La demoninación ingresada ya existe');
		}
		}

	function fc_Grabar(){
		if ( fc_Trim($("txtDscProceso").value) == "")
		{
			alert('Debe ingresar la denominación.');
			return;
		}
		if(confirm(mstrSeguroGrabar)){
		$("txhOperacion").value = "GRABAR";
		$("frmMain").submit();
		}
	}
</script>	
	<body topmargin="5" leftmargin="5" rightmargin="5">
	<form:form action="${ctx}/reclutamiento/rec_agregar_areas_interes_1.html" commandName="control" id="frmMain" name="frmMain">
		<form:hidden path="codDetalle" id="txhCodDetalle"/>
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="valor1" id="txhValor1"/>
		<form:hidden path="codEval" id="txhCodEval"/>
		<form:hidden path="msg" id="txhMsg"/>	
		
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px; margin-top: 10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">Agregar Areas de Interes - 2º Nivel</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table class="tabla2" style="width:95%;margin-left:10px;margin-top:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td valign='top'>
					<table class="tabla"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="30px" width="100%"> 
						<tr>
							<td nowrap>Denominación:&nbsp;			
								<form:input path="descripcion" id="txtDscProceso"
								onkeypress="fc_ValidaTextoEspecial();"
								onblur="fc_ValidaTextoEspOnBlur('txtDscProceso','denominacion');" 
								cssClass="cajatexto" size="40" maxlength="100"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();" ></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
	</form:form>
	</body>
</html>
