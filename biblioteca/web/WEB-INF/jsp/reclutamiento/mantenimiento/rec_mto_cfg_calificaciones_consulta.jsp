<%@ include file="/taglibs.jsp"%>
<html>
	<head>
	
	<script language=javascript>
		function onLoad(){
			document.getElementById("txhCan").value = 0;
			document.getElementById("txhDescripcion").value = "";
			document.getElementById("txhCodDetalle").value = "";
			objMsg = document.getElementById("txhMsg");
			if ( objMsg.value == "OK" ){
				document.getElementById("txhCodDetalle").value = "";
				objMsg.value = "";
				alert('Se elimin� exitosamente.');
				window.fc_Valor();
			}else if ( objMsg.value == "ERROR" )
				alert('Problemas al eliminar. Consulte con el administrador.');
			else if(objMsg.value == "ERROR")
				alert('No se puede eliminar ya que el registro est� siendo utilizado');
		}
		function fc_Valor(){
			document.getElementById("txhOperacion").value = "DETALLE";
			document.getElementById("frmMain").submit();
		}
		function Fc_Agregar(){
			srtSeltipo = document.getElementById("cboSelTipo").value;
			Fc_Popup("${ctx}/reclutamiento/rec_agregar_calificaciones.html?txhSelTipo=" + srtSeltipo + "&txhCodEva=" + document.getElementById("txhCodEval").value + "&txhTexto=" + document.getElementById("txhTexto").value
			    ,500,140,"");
		}		
		function fc_seleccionarRegistro(strCodNIvel1, descripcion){
			strDes = document.getElementById("txhDescripcion").value;
			strCodSel = document.getElementById("txhCodDetalle").value;
			flag=false;
			if (strCodSel!=''){
				ArrDes = strDes.split("|");
				ArrCodSel = strCodSel.split("|");
				strCodSel = "";
				strDes = "";
				for (i=0;i<=ArrCodSel.length-2;i++){
					if (ArrCodSel[i] == strCodNIvel1)
						flag = true; 					
					else{
						strDes = strDes + ArrDes[i]+'|';
						strCodSel = strCodSel + ArrCodSel[i]+'|';
					}
				}
			}
			if (!flag){
				strDes = strDes + descripcion + '|';
				strCodSel = strCodSel + strCodNIvel1 + '|';
			}
		    document.getElementById("txhDescripcion").value = strDes;
			document.getElementById("txhCodDetalle").value = strCodSel;
			fc_GetNumeroSeleccionados();	
		}
		function fc_GetNumeroSeleccionados(){
			strCodSel = document.getElementById("txhCodDetalle").value;
			if ( strCodSel != '' ){
				ArrCodSel = strCodSel.split("|");
				document.getElementById("txhCan").value = ArrCodSel.length - 1;
			}else 
				document.getElementById("txhCan").value = 0;	
		}
		function Fc_Modificar(){
			var  intcant = document.getElementById("txhCan").value;
			if(intcant == 1){
				if (fc_Trim(document.getElementById("txhCodDetalle").value) == ""){
					alert('Debe seleccionar un registro');
					return false;
				}
				strDescripcion = document.getElementById("txhDescripcion").value;
				strCodDetalle = document.getElementById("txhCodDetalle").value;
				pes = strCodDetalle;
				pes = pes.replace("|","");
				strCodDetalle = fc_Trim(pes);
				des = strDescripcion;
				des = des.replace("|","");
				strDescripcion = fc_Trim(des);	
				strSelTipo = document.getElementById("cboSelTipo").value;
				Fc_Popup("${ctx}/reclutamiento/rec_modificar_calificacion.html?txhDescripcion=" + strDescripcion +"&txhCodDetalle=" + strCodDetalle + "&txhSelTipo=" + strSelTipo + "&txhCodEva=" + document.getElementById("txhCodEval").value + "&txhTexto=" + document.getElementById("txhTexto").value  
				,500,130,"");
			}else				
				alert('Debe seleccionar un registro');
			
		}
		function Fc_Eliminar(){
			if (fc_Trim(document.getElementById("txhCodDetalle").value) == ""){				
				alert('Debe seleccionar un registro');
				return false;
			}
			if(confirm('�Est� seguro de eliminar?')){
			        document.getElementById("txhOperacion").value = "ELIMINAR";
			        document.getElementById("frmMain").submit();
			        document.getElementById("txhCodDetalle").value = "";
			}
		}
		function fc_Regresar(){
			parent.location.href = "${ctx}/menuReclutamiento.html";
		}	
	</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/reclutamiento/rec_mto_cfg_calificaciones_consulta.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="seltipo" id="txhSelTipo"/>
		<form:hidden path="codDetalle" id="txhCodDetalle"/>
		<form:hidden path="descripcion" id="txhDescripcion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codEval" id="txhCodEval"/>
		<form:hidden path="can" id="txhCan"/>
		<form:hidden path="texto" id="txhTexto"/>
		<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand" alt="Regresar"></td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" border="0" style="margin-left:18px">
			<tr>
				<td align="left"><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="516px" height="27px" class="opc_combo">Tabla Varios</td>
				<td align="right"><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" border="0" style="margin-left:17px;margin-top:10px">
			<tr>
				<td background="${ctx}/images/reclutamiento/Mant_Conf/back-rec1.jpg" style="width:707px;height:40px">
					<table cellSpacing="0" cellPadding="0" border="0" bordercolor="cyan" width="350px"> 						
						<tr>
							<td width="25%" class="texto_bupper">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tabla:</td>
							<td id="td_Seleccion" name="td_Seleccion">
								<form:select  path="codSelTipo" id="cboSelTipo"  cssClass="cajatexto" 
									cssStyle="width:180px" onchange="javascript:fc_Valor();">
							        <form:option  value="0000">--Seleccione--</form:option>
						      	    <c:if test="${control.listTipos!=null}">
						        	<form:options itemValue="codTipoTipo" itemLabel="denominacionTipo" 
						        		items="${control.listTipos}" />						        
						            </c:if>
	 				            </form:select>
							</td>
						</tr>
		       		</table>
				</td>
			</tr>
		</table>			
		<br>		
		<table style="display:" style="margin-left:18px" id="tbl_calificaciones" name="tbl_calificaciones" cellpadding="0" cellspacing="0" ID="Table1" width="97%">
			<tr>
				<td>
				<div style="overflow: auto; height: 350px;width:98%">
                        <display:table name="sessionScope.listaCalificaciones" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
						 requestURI="" style="border: 1px solid #6b6d6b ;width:98%">
						
						<display:column property="chkSelDetalle" title="Sel." headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center" />
						<display:column property="descripcion" title="Denominaci�n" style="text-align:left; width:100%"	headerClass="grilla" class="tablagrilla" />
						
					  	<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"/>
					    </display:table>	
					<% request.getSession().removeAttribute("listaCalificaciones"); %>
				</div>
				</td>
				<td width="27px" valign="top" align="center"><BR>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img id="imgagregar" src= "${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer" onclick="javascript:Fc_Agregar();"  alt="Agregar">
					</a>					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmodificar','','${ctx}/images/iconos/actualizar2.jpg',1)">
					<img id="imgmodificar" src= "${ctx}/images/iconos/actualizar1.jpg" style="cursor:pointer"  onclick="javascript:Fc_Modificar();"  alt="Modificar">
					</a>					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img id="imgquitar" src= "${ctx}/images/iconos/kitar1.jpg" style="cursor:pointer"  onclick="javascript:Fc_Eliminar();"  alt="Eliminar">
					</a>
				<td>
			</tr>
		</table>
	</form:form>
