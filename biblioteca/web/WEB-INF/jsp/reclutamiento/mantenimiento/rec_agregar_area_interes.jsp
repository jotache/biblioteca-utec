<%@ include file="/taglibs.jsp"%>

<html>
	<head>

	<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>
		
	<script language=javascript>
		function onLoad(){
			objMsg = document.getElementById("txhMsg");
			if (objMsg.value == "OK"){				
				window.opener.document.getElementById("frmMain").submit();
				alert('Se grab� exitosamente');
				window.close();
			}else if ( objMsg.value == "ERROR" ){		
				alert('Problemas al Grabar. Consulte con el administrador.');
			}else if(objMsg.value == "DOBLE"){			
				alert('La demoninaci�n ingresada ya existe');
			}
		}	
		function fc_Grabar(){
			if ( fc_Trim(document.getElementById("txtDscProceso").value) == ""){			
				alert('Debe ingresar la denominaci�n.');
				return;
			}
			if(confirm('�Est� seguro de grabar?')){
				document.getElementById("txhOperacion").value = "GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
</script>	
	<body topmargin="5" leftmargin="5" rightmargin="5">
	<form:form action="${ctx}/reclutamiento/rec_agregar_areas_interes.html" commandName="control" id="frmMain" name="frmMain">
		<form:hidden path="codDetalle" id="txhCodDetalle"/>
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codEval" id="txhCodEval"/>
		<form:hidden path="msg" id="txhMsg"/>	
		<table cellspacing="0" cellpadding="0" border="0" style="width:97%; margin-left: 10px; margin-top: 10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">Agregar Areas de Interes - 1� Nivel</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table class="tabla2" style="width:93%;margin-left:10px;margin-top:10px" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td valign='top'>
					<table class="tabla"  cellSpacing="1" cellPadding="1" border="0" bordercolor='red' height="30px" width="100%"> 
						<tr>
							<td nowrap>Denominaci�n:&nbsp;			
								<form:input path="descripcion" id="txtDscProceso" 
								onkeypress="fc_ValidaTextoNumeroEspecial();" maxlength="100"
								cssClass="cajatexto" size="40" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
	</form:form>
	
	</body>
</html>

