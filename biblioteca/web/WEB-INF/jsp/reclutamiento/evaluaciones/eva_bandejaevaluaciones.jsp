<%@ include file="/taglibs.jsp"%>
<html>
<head>

<script type="text/javascript">
	function onLoad()
	{
		indEtapa = document.getElementById("txhIndEtapa").value;

		if ( indEtapa == 0)
		{
			document.getElementById("rbtSeleccion").checked = false;
			document.getElementById("rbtRevision").checked = true;
			document.getElementById("tblSeleccion").style.display = "none";
			document.getElementById("tblRevision").style.display = "inline-block";
		}
		else
		{
			document.getElementById("rbtRevision").checked = false;
			document.getElementById("rbtSeleccion").checked = true;
			document.getElementById("tblRevision").style.display = "none";
			document.getElementById("tblSeleccion").style.display = "inline-block";
		}
		
		objMsg = document.getElementById("txhMsg");
		if ( fc_Trim(objMsg.value) != "" )
		{
			if ( fc_Trim(objMsg.value) == "ERROR" ) alert('Problemas al Grabar. Consulte con el administrador.');
			
		}
		objMsg.value = "";
	}
	function fc_Regresar()
	{
		document.location.href = "${ctx}/menuReclutamiento.html";
	}
	function fc_Evaluar()
	{
		strCodPostulantes = document.getElementById("txhCodPostulantes").value;
		strNomPostulantes = document.getElementById("txhNomPostulantes").value;
		strCodTipoEvals = document.getElementById("txhCodTipoEvals").value;
		strDscTipoEvals = document.getElementById("txhDscTipoEvals").value;
		strCodProcesos = document.getElementById("txhCodProcesos").value;
		strCodEvaEnProcs = document.getElementById("txhCodEvaEnProcs").value;
		strCodPostulanteProceso = document.getElementById("txhCodPostulanteProceso").value;
		strCodUsuario = document.getElementById("txhCodUsuario").value;
		
		arrCodPostulantes = strCodPostulantes.split("|");
		
		if ( arrCodPostulantes.length - 1 == 0 )
		{
			alert('Debe seleccionar un registro');
			return false;
		}
		else if ( arrCodPostulantes.length - 1 > 1 )
		{
			alert('Debe seleccionar sólo un registro');
			return false;
		}
		
		var codEtapaProceso;
		var dscEtapaProceso;
		if ( document.getElementById("txhIndEtapa").value == "" ||
			 document.getElementById("txhIndEtapa").value == "0" )
		{
			codEtapaProceso = "0001";
			dscEtapaProceso = "Revisión";
		}
		else
		{
			codEtapaProceso = "0002";
			dscEtapaProceso = "Selección";
		}
		
		strUrl = "${ctx}/reclutamiento/evalucion_postulante_rev.html";
		strUrl = strUrl + "?txhPostulante=" + strCodPostulantes; 
		strUrl = strUrl + "&txhNomPostulante=" + strNomPostulantes;
		strUrl = strUrl + "&txhTipoEvaluacion=" + strCodTipoEvals;
		strUrl = strUrl + "&txhDscEvaluacion=" + strDscTipoEvals;
		strUrl = strUrl + "&txhProceso=" + strCodProcesos;
		strUrl = strUrl + "&txhEvaluacionAct=" + strCodEvaEnProcs;
		strUrl = strUrl + "&txhPostulanteProceso=" + strCodPostulanteProceso;
		strUrl = strUrl + "&txhCodUsuario=" + strCodUsuario;
		strUrl = strUrl + "&txhCodEtapaProceso=" + codEtapaProceso;
		strUrl = strUrl + "&txhDscEtapaProceso=" + dscEtapaProceso;
		 
		Fc_Popup(strUrl,500,240,"");
		
	}

	function fc_EnviarRHH(){
		$('txhCodPostulantes').value = '' + $('txhDato1').value;
		$('txhNomPostulantes').value = '' + $('txhDato2').value+'|';
		$('txhCodTipoEvals').value = '' + $('txhDato3').value+'|';
		$('txhDscTipoEvals').value = '' + $('txhDato4').value+'|';
		$('txhCodProcesos').value = '' + $('txhDato5').value;		
		$('txhCodEvaEnProcs').value = '' + $('txhDato6').value+'|';
		$('txhCodPostulanteProceso').value = '' + $('txhDato7').value+'|';
		fc_Enviar();		 
	}
	
	function fc_Enviar()
	{
		//Solo se envia si todos los registros seleccionados tienen una evaluacion Actual.
		strCodEvaEnProcs = document.getElementById("txhCodEvaEnProcs").value;		
		arrCodEvaEnProcs = strCodEvaEnProcs.split("|");		
		if ( arrCodEvaEnProcs.length - 1 == 0 )
		{
			alert('Debe seleccionar un registro');
			return false;
		}		
		for ( var  i = 0 ; i < arrCodEvaEnProcs.length - 1 ; i++ )
		{
			if ( fc_Trim(arrCodEvaEnProcs[i]) == "" )
			{
				alert('Todos los postulantes seleccionados deben haber sido evaluados.');
				return false;
			}
		}
		
		//if ( ! confirm(mstrSeguroEnviarRRHH) ) return false;
		document.getElementById("txhAccion").value = "ENVIAR_RRHH";
		document.getElementById("frmMain").submit();		
		
	}
	
	function fc_seleccionarRegistro(codPostulante, nomPostulante, codTipoEval, dscTipoEval
						, codProceso, codEvaEnProc, codPostProceso )
	{
		strCodPostulantes = document.getElementById("txhCodPostulantes").value;
		strNomPostulantes = document.getElementById("txhNomPostulantes").value;
		strCodTipoEvals = document.getElementById("txhCodTipoEvals").value;
		strDscTipoEvals = document.getElementById("txhDscTipoEvals").value;
		strCodProcesos = document.getElementById("txhCodProcesos").value;
		strCodEvaEnProcs = document.getElementById("txhCodEvaEnProcs").value;
		strCodPostulanteProceso = document.getElementById("txhCodPostulanteProceso").value;
		
		flag=false;
		
		if ( strCodProcesos != '' )
		{
			arrCodPostulantes = strCodPostulantes.split("|");
			arrNomPostulantes = strNomPostulantes.split("|");
			arrCodTipoEvals = strCodTipoEvals.split("|");
			arrDscTipoEvals = strDscTipoEvals.split("|");
			arrCodProcesos = strCodProcesos.split("|");
			arrCodEvaEnProcs = strCodEvaEnProcs.split("|");
			arrCodPostulanteProceso = strCodPostulanteProceso.split("|");
			
			strCodPostulantes = "";
			strNomPostulantes = "";
			strCodTipoEvals = "";
			strDscTipoEvals = "";
			strCodProcesos = "";
			strCodEvaEnProcs = "";
			strCodPostulanteProceso = "";
					
			for (i=0;i<=arrCodProcesos.length-2;i++)
			{
				if ( arrCodTipoEvals[i] == codTipoEval &&  
					arrCodProcesos[i] == codProceso &&
					arrCodPostulantes[i] == codPostulante )
				{ 
					flag = true 
				}
				else 
				{
					strCodPostulantes = strCodPostulantes + arrCodPostulantes[i]+'|';
					strNomPostulantes = strNomPostulantes + arrNomPostulantes[i]+'|';
					strCodTipoEvals = strCodTipoEvals + arrCodTipoEvals[i]+'|';
					strDscTipoEvals = strDscTipoEvals + arrDscTipoEvals[i]+'|';
					strCodProcesos = strCodProcesos + arrCodProcesos[i]+'|';
					strCodEvaEnProcs = strCodEvaEnProcs + arrCodEvaEnProcs[i]+'|';
					strCodPostulanteProceso =  strCodPostulanteProceso + arrCodPostulanteProceso[i]+'|';
				}
			}
		}
		if (!flag)
		{
			strCodPostulantes = strCodPostulantes + codPostulante + '|';
			strNomPostulantes = strNomPostulantes + nomPostulante + '|';
			strCodTipoEvals = strCodTipoEvals + codTipoEval + '|';
			strDscTipoEvals = strDscTipoEvals + dscTipoEval + '|';
			strCodProcesos = strCodProcesos + codProceso + '|';
			strCodEvaEnProcs = strCodEvaEnProcs + codEvaEnProc + '|';
			strCodPostulanteProceso = strCodPostulanteProceso + codPostProceso + '|';			
		}
		
		document.getElementById("txhCodPostulantes").value = strCodPostulantes;
		document.getElementById("txhNomPostulantes").value = strNomPostulantes;
		document.getElementById("txhCodTipoEvals").value = strCodTipoEvals;
		document.getElementById("txhDscTipoEvals").value = strDscTipoEvals;
		document.getElementById("txhCodProcesos").value = strCodProcesos;		
		document.getElementById("txhCodEvaEnProcs").value = strCodEvaEnProcs;
		document.getElementById("txhCodPostulanteProceso").value = strCodPostulanteProceso;

		/*alert('txhCodPostulantes:'+strCodPostulantes+'\n'+
				'txhNomPostulantes:'+strNomPostulantes+'\n'+
				'txhCodTipoEvals:'+strCodTipoEvals+'\n'+
				'txhDscTipoEvals:'+strDscTipoEvals+'\n'+
				'txhCodProcesos:'+strCodProcesos+'\n'+
				'txhCodEvaEnProcs:'+strCodEvaEnProcs+'\n'+
				'txhCodPostulanteProceso:'+strCodPostulanteProceso);*/
	}
	
	function fc_CambiaEtapa(indEtapa)
	{	
		if ( indEtapa == 0)
		{
			document.getElementById("txhAccion").value = "BUSCAR_REV";
			document.getElementById("rbtSeleccion").checked = false;
			document.getElementById("tblRevision").style.display = "none";
			document.getElementById("tblSeleccion").style.display = "inline-block";
		}
		else
		{
			document.getElementById("txhAccion").value = "BUSCAR_SEL";			
			document.getElementById("rbtRevision").checked = false;
			document.getElementById("tblSeleccion").style.display = "none";
			document.getElementById("tblRevision").style.display = "inline-block";
		}		
		document.getElementById("txhIndEtapa").value = indEtapa;
		document.getElementById("frmMain").submit();
	}

	function fc_Seleccionar(){				
		var indEtapa;
		if (document.getElementById("rbtSeleccion").checked == true){
			document.getElementById("txhAccion").value = "BUSCAR_SEL";
			indEtapa=1;
		}else{
			document.getElementById("txhAccion").value = "BUSCAR_REV";
			indEtapa=0;
		}	
		document.getElementById("txhIndEtapa").value = indEtapa;	 
		document.getElementById("frmMain").submit();
	}
	
	function fc_VerCV(strCV)
	{
		strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>";
		if ( strRuta != "" )
		{
			if ( fc_Trim(strCV) != "") window.open(strRuta + strCV);
			else alert('El postulante no cuenta con CV.');
		}
	}

	function fc_HojaVida(strCodPostulante)
	{
		Fc_PopupScroll("${ctx}/reclutamiento/hoja_vida.html?txhCodPostulante=" + strCodPostulante,865,500);
	}
</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/bandeja_evaluaciones_evaluador.html" >
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	
	<form:hidden path="codPostulantes" id="txhCodPostulantes"/>
	<form:hidden path="nomPostulantes" id="txhNomPostulantes"/>
	<form:hidden path="codTipoEvals" id="txhCodTipoEvals"/>
	<form:hidden path="dscTipoEvals" id="txhDscTipoEvals"/>
	<form:hidden path="codProcesos" id="txhCodProcesos"/>	
	<form:hidden path="codEvaEnProcs" id="txhCodEvaEnProcs"/>
	<form:hidden path="codPostulanteProceso" id="txhCodPostulanteProceso"/>
	<form:hidden path="indEtapa" id="txhIndEtapa"/>
	<!-- Icono Regresar -->
	<input type="hidden" id="txhDato1" value=""/>
	<input type="hidden" id="txhDato2" value=""/>
	<input type="hidden" id="txhDato3" value=""/>
	<input type="hidden" id="txhDato4" value=""/>
	<input type="hidden" id="txhDato5" value=""/>
	<input type="hidden" id="txhDato6" value=""/>
	<input type="hidden" id="txhDato7" value=""/>

	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif"
			onclick="javascript:fc_Regresar();" style="cursor: hand" alt="Regresar"></td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 10px">
		<tr>
			<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
			<td background="${ctx}/images/reclutamiento/centro.jpg" width="757px" height="27px" 
				class="opc_combo">Evaluación de Postulantes</td>
			<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
		</tr>
	</table>
	<table cellpadding="1" cellspacing="2" class="tabla" style="margin-left:10px; margin-top: 5px; width: 97%;">
		<tr>
			<td width="10%">Etapa :</td>
			<td>
				<input type="radio" id="rbtRevision" name="etapa" checked onclick="javascript:fc_CambiaEtapa('0');">Revisión&nbsp;
				<input type="radio" id="rbtSeleccion" name="etapa" onclick="javascript:fc_CambiaEtapa('1');">Selección
			</td>
		</tr>
		<tr>
			<td width="10%">Proceso :</td>
			<td>
				
				<form:select path="codProceso" id="codProceso" cssClass="combo_o" 
					cssStyle="width:300px" onchange="javascript:fc_Seleccionar();">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listaProcesos!=null}">
					<form:options itemValue="codProceso" itemLabel="dscProceso" 					
						items="${control.listaProcesos}" />
					</c:if>
				</form:select>

			</td>
		</tr>
	</table>
	
	<br/>
	<table cellpadding="0" cellspacing="0" class="tabla" id="tblRevision" 
			style="margin-left:10px; width: 97%">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="1" width="100%" style="border-left:1px solid white;border-right: 1px solid white; border-top: 1px solid white">
					<tr>
						<td class="grilla" rowspan="2" style="width: 2%"></td>
						<td class="grilla" rowspan="2" style="width: 22%">Postulante</td>
						<!-- td class="grilla" rowspan="2" style="width: 5%">CV</td-->
						<td class="grilla" rowspan="2" style="width: 34%">Proceso</td>
						<!--td class="grilla">Ambito</td-->
						<td class="grilla" rowspan="2" style="width: 19%" >Tipo Evaluación</td>
						<td class="grilla" colspan="2" >RRHH</td>
<!--						<td class="grilla" rowspan="2" style="width: 15%">Calificación Jefe Dpto.</td>-->
					</tr>
					<tr>
						<td class="grilla" style="width: 7%" >Calificación</td>
						<td class="grilla" style="width: 16%" >Comentario</td>						
					</tr>
					
				</table>

				<display:table name="sessionScope.listRevision" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.EvaluacionesByEvaluadorDecorator" 
					style="border: 1px;width:100%">
					<display:column property="chkSelPostulante" class="tablagrilla2" style="text-align: center; width:2%;" />
					<display:column property="nomPostulante" class="tablagrilla2" style="text-align: left; width:22%" />
					<!-- display:column property="verCV" class="tablagrilla2" style="text-align: center; width:5%" /-->
					<display:column property="dscProceso" class="tablagrilla2" style="text-align: left; width:34%"/>
					<display:column property="dscTipoEvaluacion" class="tablagrilla2" style="text-align: center; width:19%"/>
					<display:column property="dscCalificacion" class="tablagrilla2" style="text-align: left;width:7%"/>
					<display:column property="textAreaComentario" class="tablagrilla2" style="text-align: center;width:16%"/>					
<!--					<display:column property="dscCalificacionJefeDpto_Revision" class="tablagrilla2" style="text-align: left; width:15%"/>										-->

					<display:setProperty name="basic.show.header" value="false"/>
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='6' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</td>
		</tr>
		<tr>
			<td height="7px"></td>
		</tr>
		<tr>
			<td class="grilla"><strong>Lista de Postulantes Revisados</strong>
			</td>
		</tr>
		<tr>
			<td>

				<table cellpadding="0" cellspacing="1" width="100%" style="border-left:1px solid white;border-right: 1px solid white; border-top: 1px solid white">
					<tr>						
						<td class="grilla" rowspan="2" style="width: 30%">Postulante</td>						
						<td class="grilla" rowspan="2" style="width: 20%">Proceso</td>						
						<td class="grilla" rowspan="2" style="width: 13%" >Tipo Evaluación</td>
						<td class="grilla" colspan="2" >RRHH</td>
						<td class="grilla" rowspan="2" >Calificación Jefe Dpto.</td>
					</tr>
					<tr>
						<td class="grilla" style="width: 7%">Calificación</td>
						<td class="grilla" style="width: 15%">Comentario</td>						
					</tr>
				
					<c:if test="${control.listPostRevisados!=null}">
						<c:forEach var="lista" items="${control.listPostRevisados}" varStatus="loop" >
							<tr class="tablagrilla2">
								<td valign="middle">
									<a href="javascript:fc_HojaVida('<c:out value="${lista.codPostulante}"/>');" ><c:out value="${lista.nomPostulante}"/></a>
									&nbsp;<img src="/SGA/images/reclutamiento/cv.gif" width="16" height="16" style="cursor:pointer" alt="Ver curriculum" align="middle" onclick="fc_VerCV('<c:out value="${lista.cv}"/>');"/>
								</td>
								<td ><c:out value="${lista.dscProceso}"/></td>
								<td ><c:out value="${lista.dscTipoEvaluacion}"/></td>
								
								<td ><c:out value="${lista.dscCalificacion2}"/></td>
								<td ><c:out value="${lista.comentario2}"/></td>
								<td ><c:out value="${lista.dscCalificacion}"/></td>
							</tr>
						</c:forEach>
					</c:if>

				</table>



			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="1"  width="97%" class="tabla" id="tblSeleccion" style="margin-left:10px">
		<tr>
			<td>
				<display:table name="sessionScope.listSeleccion" cellpadding="0" cellspacing="1" decorator="com.tecsup.SGA.bean.EvaluacionesByEvaluadorDecorator" style="border: 1px solid white;width:100%">
					<display:column property="chkSelPostulante" title="Sel." class="tablagrilla2" headerClass="grilla" 
						style="text-align: center; width:5%;" />
					<display:column property="nomPostulante" title="Postulante" class="tablagrilla2" headerClass="grilla"  
						style="text-align: left; width:30%" />

					<display:column property="dscProceso" title="Proceso" class="tablagrilla2" headerClass="grilla"  
						style="text-align: left; width:30%"/>
					<display:column property="dscTipoEvaluacion" title="Tipo Evaluación" class="tablagrilla2" headerClass="grilla"  
						style="text-align: left; width:20%"/>
					<display:column property="dscCalificacionJefeDpto_Revision" title="Calificación Jefe Dpto." class="tablagrilla2" headerClass="grilla"  
						style="text-align: left; width:15%"/>					

					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</td>
		</tr>
	</table>
	<br>
	<table align="center">
		<tr id="BtRevision">
			<td align=center>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnEvaluar','','${ctx}/images/botones/evaluar2.jpg',1)">
				<img alt="Evaluar" src="${ctx}/images/botones/evaluar1.jpg" id="btnEvaluar" style="cursor:pointer;" onclick="javascript:fc_Evaluar();"></a>&nbsp;

				<%-- 
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnEnviar1','','${ctx}/images/botones/enviarrrhh2.jpg',1)">
					<img alt="Enviar a RRHH" src="${ctx}/images/botones/enviarrrhh1.jpg" id="btnEnviar1" style="cursor:pointer;" onclick="javascript:fc_Enviar();">
				</a>
				&nbsp;
				--%>

				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnregresar','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="btnregresar" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>&nbsp;
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
</script>
</html>