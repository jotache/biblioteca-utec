<%@ include file="/taglibs.jsp"%>
<html>
<head>

<script type="text/javascript">
	function onLoad(){
		objMsg = document.getElementById("txhMsg");
		if (objMsg.value == "OK" ){					
			objDato1 = document.getElementById("datosRRHH1");
			objDato2 = document.getElementById("datosRRHH2");
			objDato3 = document.getElementById("datosRRHH3");
			objDato4 = document.getElementById("datosRRHH4");
			objDato5 = document.getElementById("datosRRHH5");
			objDato6 = document.getElementById("datosRRHH6");
			objDato7 = document.getElementById("datosRRHH7");
			window.opener.document.getElementById("txhCodEvaEnProcs").value=objDato6.value;			
			window.opener.document.getElementById("txhDato1").value=objDato1.value;
			window.opener.document.getElementById("txhDato2").value=objDato2.value;
			window.opener.document.getElementById("txhDato3").value=objDato3.value;
			window.opener.document.getElementById("txhDato4").value=objDato4.value;
			window.opener.document.getElementById("txhDato5").value=objDato5.value;
			window.opener.document.getElementById("txhDato6").value=objDato6.value;
			window.opener.document.getElementById("txhDato7").value=objDato7.value;			
			window.opener.fc_EnviarRHH();		
		}
	}
	
	function fc_Grabar()
	{
		if ( confirm('�Est� seguro de Grabar y Enviar a RRHH?') )
		{
			document.getElementById("txhAccion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
</script>
</head>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/reclutamiento/evalucion_postulante_rev.html">
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="msgAux" id="txhMsgAux"/>
	<form:hidden path="codPostSel" id="txhCodPostulanteSel"/>
	<form:hidden path="nomPostSel" id="txhNomPostulanteSel"/>	
	<form:hidden path="codProceso" id="txhCodProcesoRelacionado"/>
	<form:hidden path="codEtapa" id="txhCodEtapaSel"/>
	<form:hidden path="codEvalSel" id="txhCodEvaluacionRelacionada"/>	
	<form:hidden path="codPostulanteProceso" id="txhCodPostulanteProceso"/>
	<form:hidden path="codEvaluacionActual" id="txhCodEvaluacionActual"/>

	<form:hidden path="datosRRHH1" id="datosRRHH1"/>
	<form:hidden path="datosRRHH2" id="datosRRHH2"/>
	<form:hidden path="datosRRHH3" id="datosRRHH3"/>
	<form:hidden path="datosRRHH4" id="datosRRHH4"/>
	<form:hidden path="datosRRHH5" id="datosRRHH5"/>
	<form:hidden path="datosRRHH6" id="datosRRHH6"/>
	<form:hidden path="datosRRHH7" id="datosRRHH7"/>

	<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 10px; margin-top: 5px">
		<tr>
			<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
			<td background="${ctx}/images/reclutamiento/centro.jpg" width="94%" height="27px" 
				class="opc_combo">Evaluar al Postulante</td>
			<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
		</tr>
	</table>
	<table cellpadding="1" cellspacing="2" class="tabla"
		style="margin-left:10px; margin-top: 5px; margin-right: 10px; width: 95%">
		<tr>
			<td>Postulante:</td>
			<td>
				<form:label path="nomPostSel" id="lblNomPostulante">${control.nomPostSel}</form:label>
			</td>
		</tr>
		<tr>
			<td>Tipo Evaluaci�n:</td>
			<td><form:label path="dscEvalSel" id="lblDscEtapa">${control.dscEvalSel}</form:label></td>
		</tr>
		<tr>
			<td>Calificaci�n:</td>
			<td>
				<form:select path="codCalSel" id="cboCalificacion" cssClass="Combo" cssStyle="width:200px">
					<form:option value="" >--Seleccione--</form:option>
					<c:if test="${control.listCalificaciones!=null}">
					<form:options itemValue="codCalificacionId" itemLabel="dscCalificacionNormal" 
						items="${control.listCalificaciones}" />
					</c:if>
				</form:select>
			</td>
		</tr>
		<tr>
			<td valign="top">Comentario:</td>
			<td>
				<form:textarea path="comentario" id="txaComentario" cols="50" cssStyle="height:80px"  
					cssClass="cajatexto"/>
			</td>
		</tr>
	</table>
	<br>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btngrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="btngrabar" style="cursor:pointer;" onclick="fc_Grabar();"></a>&nbsp;
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btncancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="btncancelar" style="cursor:pointer;" onclick="window.close();"></a>&nbsp;
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	objMsg = document.getElementById("txhMsg");
	if ( fc_Trim(objMsg.value) != "")
	{
		if ( fc_Trim(objMsg.value) == "ERROR" ) 
		{
			alert('Problemas al Grabar. Consulte con el administrador.');
		}
		else if ( fc_Trim(objMsg.value) == "OK" ) 
		{
			if ( document.getElementById("txhCodEtapaSel").value == "0001" ) window.opener.fc_CambiaEtapa("0");
			else window.opener.fc_CambiaEtapa("1");
			alert('Se Grab� y Envi� a RRHH satisfactoriamente');
			window.close();
		} 
		else if (fc_Trim(objMsg.value) == "EVAL_ANT_NO_CALIF"){
			alert('Postulante A�n no ha sido Calificado en la Evaluaci�n Previa');
			window.close();			
		}
	}
</script>
</html>