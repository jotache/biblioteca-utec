<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script type="text/javascript" type="text/javascript">
		function onLoad(){			
		}	
		
		function fc_EliAreaInteres(obj) {
			var tr = document.getElementById('trArea'+obj);
			tr.style.display = 'none';
		}		
		function copyParametros(){
		 	parent.document.getElementById("cboDepartamento").value = document.getElementById("cboDepartamento").value;
		 	parent.document.getElementById("cboProvincia").value = document.getElementById("cboProvincia").value;
		 	parent.document.getElementById("cboDistrito").value = document.getElementById("cboDistrito").value;
		}		
		function Fc_AgregarAreaInteres(obj) {		
			
		   if (fc_Trim(document.getElementById("txhCadenaAI").value) == ""){
				document.getElementById("txhCadenaAI").value = 	document.getElementById("txhCadenaAI").value + 
																parent.document.getElementById("cboAreaInteres").value;
			}else{
				document.getElementById("txhCadenaAI").value = 	document.getElementById("txhCadenaAI").value + ", " + 
																parent.document.getElementById("cboAreaInteres").value;
			}
			document.getElementById("txhOperacion").value="AGREGAR_INTERES";
			document.getElementById("frmMain").submit();		
		}
		
		function fc_getAreaInteres(){
			var objTabla = document.getElementById('tblAreaInteres');			
			var contador = 0;
			document.getElementById("txhCodArea").value="";
	
			for(i=1 ; i < objTabla.rows.length ; i++){				
				str1 = document.getElementById('txhArea' + i).value;				
				obj = document.getElementById('trArea' + i);			
				if( document.getElementById("chkSeleccion"+i).checked==true ){
					
					document.getElementById("txhCodArea").value = document.getElementById("txhCodArea").value + 
																  str1 + "|";
					contador = contador + 1;
				}
			}
			//alert('V:'+document.getElementById("txhCodArea").value);
			document.getElementById("txhNroRegAreaInt").value = String(contador);			
		}
		
	</script>
</head>
<body bgcolor="#cccc00">
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/datos_personales_areas_interes.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="nroRegAreaInt" id="txhNroRegAreaInt"/>
		<form:hidden path="idRec" id="txhIdRec"/>
		<form:hidden path="codArea" id="txhCodArea"/>
		<form:hidden path="cadenaAI" id="txhCadenaAI"/>
		
		<form:hidden path="sexo" id="txhIndSexo"/>
		<form:hidden path="nacionalidad" id="txhIndNacionalidad"/>
		<form:hidden path="postuladoAntes" id="txhIndPostulado"/>
		<form:hidden path="trabajadoAntes" id="txhIndTrabajado"/>
		<form:hidden path="familiaTecsup" id="txhIndFamiliares"/>
		<form:hidden path="expDocente" id="txhIndExperiencia"/>
		<form:hidden path="dispoViaje" id="txhIndDispoViaje"/>
	
		<table id="tblAreaInteres0" cellpadding="0" cellspacing="0" style="width:100%;"  border="0" >
			<tr class="grilla" height="5px" valign="middle">
				<td width="10%" align="center" nowrap="nowrap">Sel.</td>				
				<td align="center">&Aacute;reas de Inter&eacute;s</td>						
			</tr>	
		</table>
	
		<div style="overflow: auto; height: 280px; width:99.9%; vertical-align: top; border: 0px solid blue" class="tabla_1A">
		<table id="tblAreaInteres" cellpadding="0" cellspacing="0" 
			style="width:93%; height: 150; vertical-align: top" class="tabla_1A" border="1" bordercolor="white">
							
				<c:forEach varStatus="loop" var="lista" items="${control.listaAreaInteresByPost}" >				
					<input type="hidden" id="txhArea<c:out value="${loop.index}"/>" value="<c:out value="${lista.area}" />" />
					<c:if test="${lista.termino=='D'}">				
						<tr class=<c:if test="${lista.termino=='C'}">
									<c:out value="grilla2"/>
								  </c:if>
								  <c:if test="${lista.termino=='D'}">
									<c:out value="grilladetalle"/>
								  </c:if>
							style="display: " id='trArea<c:out value="${loop.index}"/>'  name='trArea<c:out value="${loop.index}"/>'>
							<td align="center">
							
								<c:if test="${lista.termino=='C'}">
									<input id="chkSeleccion<c:out value="${loop.index}"/>" value="" type="hidden"/>
								</c:if>					
								<c:if test="${lista.termino=='D'}">
									<input type="checkbox" name="caja" id="chkSeleccion<c:out value="${loop.index}"/>"
									<c:if test='${lista.codTTDInteres!=null}'>
										<c:out value="checked" />
									</c:if>/>
								</c:if>
							</td>					
							<td align="left">&nbsp;
								<c:out value="${lista.descInteres}" />
							</td>
						</tr>
					</c:if>
				</c:forEach>				
		</table>		
		</div>
	</form:form>	
</body>