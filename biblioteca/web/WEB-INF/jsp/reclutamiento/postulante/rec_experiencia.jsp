<%@ include file="/taglibs.jsp"%>

<head>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-fechas.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script type="text/javascript">
		function onLoad(){}		
		function fc_onChange(obj, strObjName){
			if (document.getElementById(obj.id).value != ""){
				document.getElementById(strObjName).disabled = true;
			}
			else{
				document.getElementById(strObjName).disabled = false;
			}
		}
		function fc_ValidaLetrasNumerosPunto() { 
			var valido = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.() ";                     
		      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){
		            var intEncontrado = 0;
		            window.event.keyCode = 209;
		      }else{          
		            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
		            var intEncontrado = valido.indexOf(ch_Caracter);            
		            if (intEncontrado == -1)
		                 window.event.keyCode = 0;          
		            else
		                 window.event.keyCode = ch_Caracter.charCodeAt();
		      }
		}		
		function Fc_Valida(){
			return true;
		}
		
		function fc_GrabarExpLab(){
			if (fc_GetCadenaExperiencia()){
				if (confirm('�Est� seguro de grabar?')){
					document.getElementById("txhOperacion").value = "GRABAR";
					$('frmMain').submit();
				}
			}
		}
		
		function fc_GetCadenaExperiencia(){
			var NumFilas = 3;
			document.getElementById('txhCadenaExp').value = "";
			for (i=1;i<=NumFilas;i++){
				if(fc_Trim(document.getElementById('txtOrganizacion'+i).value) != ""){
					if(	document.getElementById('cboPuesto'+i).value == "" &&
						fc_Trim(document.getElementById('txtOtroPuesto'+i).value) == ""){
						alert('Debe ingresar el valor correspondiente en el campo ' + 'Puesto y/o especificarlo si no lo encuentra\nen la lista desplegable.');
						document.getElementById('cboPuesto'+i).focus();
						return false;
					}
					
					if(fc_Trim(document.getElementById('txtFechaInicio'+i).value) == ""){
						alert('Debe ingresar el valor correspondiente en el campo ' + 'Fecha inicio de la experiencia laboral ' + i);
						document.getElementById('txtFechaInicio'+i).focus();
						return false;
					}
					
					document.getElementById('txhCadenaExp').value =
						document.getElementById('txhCadenaExp').value +
						document.getElementById('txtOrganizacion'+i).value + "$" +
						document.getElementById('cboPuesto'+i).value + "$" +
						document.getElementById('txtOtroPuesto'+i).value + "$" +
						document.getElementById('txtFechaInicio'+i).value + "$" +
						document.getElementById('txtFechaFin'+i).value + "$" +
						document.getElementById('txtPersonaRef'+i).value + "$" +
						document.getElementById('txtTelefonoRef'+i).value + "$" +
						document.getElementById('txtDesPuesto'+i).value + "$" +
						document.getElementById('txhCodExpLab'+i).value + "$+_";
				}
			}
			///alert(document.getElementById('txhCadenaExp').value);
			return true;
		}
		
		/*Contador de Caracteres*/
		function textKey(name, num) {
			n = document.getElementById(name).value.length;
		  	t = 800;
		  	if (n > t) {
		    	document.getElementById(name).value = document.getElementById(name).value.substring(0, t);
		  	}
		  	else {
		    	document.getElementById('msgCont'+num).value = t-n;
		  	}
		}
		
	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/experiencia.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="idRec" id="txhIdRec"/>
		
		<form:hidden path="cadenaExp" id="txhCadenaExp"/>
		<form:hidden path="codExpLab1" id="txhCodExpLab1"/>
		<form:hidden path="codExpLab2" id="txhCodExpLab2"/>
		<form:hidden path="codExpLab3" id="txhCodExpLab3"/>
		<form:hidden path="codUsuario" id="codUsuario"/>

		<div style="height: 450px; width: 100%; border: 0px; border-color: red;align:center;">

		<table cellpadding="0" cellspacing="0" width="950px" height="36px" style="margin-left:6px" border="0">
			<tr>
				<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo.jpg" style="width:780px;height:36px" class="opc_combo">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Experiencia Laboral (por favor, ingrese los 3 �ltimos desde el m�s reciente al m�s antiguo)
				</td>
			</tr>
		</table>
<!--		<div style="overflow: auto; height: 420px; width:971px">-->
		<table style="width:943px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr>
				<td class="texto_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Experiencia Laboral 1<br><br>
				</td>
			</tr>
			<tr>
				<td width="37%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Organizaci&oacute;n/Empresa :</td>
				<td width="60%" colspan="3">
					<form:input path="organizacion1" id="txtOrganizacion1" cssClass="cajatexto_o" size="60" maxlength="100" onkeypress="javascript:fc_ValidaLetrasNumerosPunto();"/>
				</td>
				<td></td>				
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Puesto :</td>
				<td width="20%" colspan="4">
					<form:select path="puesto1" id="cboPuesto1" cssClass="cajatexto" cssStyle="width:315px" onchange="javascript:fc_onChange(this, 'txtOtroPuesto1');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaPuesto1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaPuesto1}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Otro Puesto :</td>
				<td width="20%" colspan="4" class="texto_norm"><form:input path="otroPuesto1" id="txtOtroPuesto1" cssClass="cajatexto_1" size="60" maxlength="100" onkeypress="javascript:fc_ValidaTextoEspecial();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				(Llenar si no encontr&oacute; en la lista anterior)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Inicio :</td>
				<td width="20%" colspan="4" class="texto_norm">
					<form:input path="fechaInicio1" id="txtFechaInicio1" cssClass="cajatexto_o" size="12" onblur="javascript:fc_ValidaFechaOnblur(this.id);" onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" align="middle" id="imgCalendar1" name="imgCalendar1" alt="Calendario" style="CURSOR: pointer">
					</a>
					&nbsp; (Fecha Inicio de Labores en el Puesto)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Fin :</td>
				<td width="60%" colspan="4" class="texto_norm"><form:input path="fechaFin1" id="txtFechaFin1" cssClass="cajatexto_1" size="12" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaInicio1','txtFechaFin1','Inicio','T�rmino',2);" onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1a','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" align="middle" id="imgCalendar1a" name="imgCalendar1a" alt="Calendario" style="CURSOR: pointer">
				</a>
					&nbsp; (Fecha T&eacute;rmino de Labores en el Puesto)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Persona de Referencia :</td>
				<td width="34%"><form:input path="personaRef1" id="txtPersonaRef1" cssClass="cajatexto_1" size="40" maxlength="100" onkeypress="javascript:fc_ValidaTextoEspecial();"/></td>
				<td width="25%">Tel&eacute;fono Contacto :</td>
				<td><form:input path="telefonoRef1" id="txtTelefonoRef1" cssClass="cajatexto_1" size="20" maxlength="20" onkeypress="javascript:fc_PermiteNumeros();"/></td>
			</tr>
			<tr>
				<td width="20%" valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Descripci&oacute;n de las Actividades<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;del Puesto :</td>
				<td width="80%" colspan="4"><form:textarea path="desPuesto1" id="txtDesPuesto1" cssClass="cajatexto_1" 
											cols="100" rows="6" onkeyup="textKey('txtDesPuesto1','1');"/></td>
			</tr>
			<tr>
				<td width="20%">&nbsp;</td>
				<td align="right" colspan="3">
					<label class="">M&aacute;ximo 800 caracteres</label>&nbsp;
					<input type="text" value="800" size="3" style="text-align:center;" id="msgCont1" name="msgCont1" disabled class="cajatexto">
				</td>
			</tr>
		</table>
		<br>
		<table style="width:943px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr>
				<td class="texto_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Experiencia Laboral 2<br><br>
				</td>
			</tr>
			<tr>
				<td width="37%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Organizaci&oacute;n/Empresa :</td>
				<td width="60%" colspan="3"><form:input path="organizacion2" id="txtOrganizacion2" cssClass="cajatexto_o" size="60" maxlength="100" onkeypress="javascript:fc_ValidaLetrasNumerosPunto();"/></td>
				<td></td>				
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Puesto :</td>
				<td width="20%" colspan="4">
					<form:select path="puesto2" id="cboPuesto2" cssClass="cajatexto" cssStyle="width:315px" onchange="javascript:fc_onChange(this, 'txtOtroPuesto2');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaPuesto2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaPuesto2}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Otro Puesto :</td>
				<td width="20%" colspan="4" class="texto_norm"><form:input path="otroPuesto2" id="txtOtroPuesto2" cssClass="cajatexto_1" size="60" maxlength="100" onkeypress="javascript:fc_ValidaTextoEspecial();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				(Llenar si no encontr&oacute; en la lista anterior)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Inicio :</td>
				<td width="20%" colspan="4" class="texto_norm"><form:input path="fechaInicio2" id="txtFechaInicio2" cssClass="cajatexto_o" size="12" onblur="javascript:fc_ValidaFechaOnblur(this.id);" onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar2','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" align="middle" id="imgCalendar2" name="imgCalendar2" alt="Calendario" style="CURSOR: pointer">
				</a>
					&nbsp; (Fecha Inicio de Labores en el Puesto)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Fin :</td>
				<td width="60%" colspan="4" class="texto_norm"><form:input path="fechaFin2" id="txtFechaFin2" cssClass="cajatexto_1" size="12" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaInicio2','txtFechaFin2','Inicio','T�rmino',2);" onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar2a','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" align="middle" id="imgCalendar2a" name="imgCalendar2a" alt="Calendario" style="CURSOR: pointer">
				</a>
				&nbsp; (Fecha T&eacute;rmino de Labores en el Puesto)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Persona de Referencia :</td>
				<td width="34%"><form:input path="personaRef2" id="txtPersonaRef2" maxlength="100" cssClass="cajatexto_1" size="40" onkeypress="javascript:fc_ValidaTextoEspecial();"/></td>
				<td width="25%">Tel&eacute;fono Contacto :</td>
				<td><form:input path="telefonoRef2" id="txtTelefonoRef2" cssClass="cajatexto_1" size="20" maxlength="20" onkeypress="javascript:fc_PermiteNumeros();"/></td>
			</tr>
			<tr>
				<td width="20%" valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Descripci&oacute;n de las Actividades<br>&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;del Puesto :</td>
				<td width="80%" colspan="4"><form:textarea path="desPuesto2" id="txtDesPuesto2" cssClass="cajatexto_1" 
											cols="100" rows="6" onkeyup="textKey('txtDesPuesto2','2');"/></td>
			</tr>
			<tr>
				<td width="20%">&nbsp;</td>
				<td align="right" colspan="3">
					<label class="">M&aacute;ximo 800 caracteres</label>&nbsp;
					<input type="text" value="800" size="3" style="text-align:center;" name="msgCont2" id="msgCont2" disabled class="cajatexto">
				</td>
			</tr>
		</table>
		<br>
		<table style="width:943px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr>
				<td class="texto_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Experiencia Laboral 3<br><br>
				</td>
			</tr>
			<tr>
				<td width="37%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Organizaci&oacute;n/Empresa :</td>
				<td width="60%" colspan="3"><form:input path="organizacion3" id="txtOrganizacion3" cssClass="cajatexto_o" size="60" maxlength="100" onkeypress="javascript:fc_ValidaLetrasNumerosPunto();"/></td>
				<td></td>				
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Puesto :</td>
				<td width="20%" colspan="4">
					<form:select path="puesto3" id="cboPuesto3" cssClass="cajatexto" cssStyle="width:315px" onchange="javascript:fc_onChange(this, 'txtOtroPuesto3');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaPuesto3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaPuesto3}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Otro Puesto :</td>
				<td width="20%" colspan="4" class="texto_norm"><form:input path="otroPuesto3" id="txtOtroPuesto3" cssClass="cajatexto_1" size="60" onkeypress="javascript:fc_ValidaTextoEspecial();" maxlength="100"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				(Llenar si no encontr&oacute; en la lista anterior)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Inicio :</td>
				<td width="20%" colspan="4" class="texto_norm"><form:input path="fechaInicio3" id="txtFechaInicio3" cssClass="cajatexto_o" size="12" onblur="javascript:fc_ValidaFechaOnblur(this.id);" onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar3','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" align="middle" id="imgCalendar3" name="imgCalendar3" alt="Calendario" style="CURSOR: pointer">
				</a>
				&nbsp; (Fecha Inicio de Labores en el Puesto)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Fin :</td>
				<td width="60%" colspan="4" class="texto_norm"><form:input path="fechaFin3" id="txtFechaFin3" cssClass="cajatexto_1" size="12" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaInicio3','txtFechaFin3','Inicio','T�rmino',2);" onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar3a','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" align="middle" id="imgCalendar3a" name="imgCalendar3a" alt="Calendario" style="CURSOR: pointer">
				</a>&nbsp; (Fecha T&eacute;rmino de Labores en el Puesto)</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Persona de Referencia :</td>
				<td width="34%"><form:input path="personaRef3" id="txtPersonaRef3" cssClass="cajatexto_1" size="40" maxlength="100" onkeypress="javascript:fc_ValidaTextoEspecial();"/></td>
				<td width="25%">Tel&eacute;fono Contacto :</td>
				<td><form:input path="telefonoRef3" id="txtTelefonoRef3" cssClass="cajatexto_1" size="20" maxlength="20" onkeypress="javascript:fc_PermiteNumeros();"/></td>
			</tr>
			<tr>
				<td width="20%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Descripci&oacute;n de las Actividades<br>&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;del Puesto :</td>
				<td width="80%" colspan="4"><form:textarea path="desPuesto3" id="txtDesPuesto3" cssClass="cajatexto_1" 
											cols="100" rows="6" onkeyup="textKey('txtDesPuesto3','3');"/></td>
			</tr>
			<tr>
				<td width="20%">&nbsp;</td>
				<td align="right" colspan="3">
					<label class="">M&aacute;ximo 800 caracteres</label>&nbsp;
					<input type="text" value="800" size="3" style="text-align:center;" name="msgCont3" id="msgCont3" disabled class="cajatexto">
				</td>
			</tr>
		</table>
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img id="imggrabar" src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_GrabarExpLab();" style="cursor:pointer" alt="Grabar">
					</a>				
					<!--input type="button" class="Boton" value="Cancelar" ID="Button1" NAME="Button1"--></td>
			</tr>
		</table>
		</div>
	</form:form>
	
	<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txtFechaInicio1",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1",
			singleClick    :    true
		
		});
		Calendar.setup({
			inputField     :    "txtFechaInicio2",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar2",
			singleClick    :    true
		
		});
		Calendar.setup({
			inputField     :    "txtFechaInicio3",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar3",
			singleClick    :    true
		
		});
		Calendar.setup({
			inputField     :    "txtFechaFin1",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1a",
			singleClick    :    true
		
		});
		Calendar.setup({
			inputField     :    "txtFechaFin2",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar2a",
			singleClick    :    true
		
		});
		Calendar.setup({
			inputField     :    "txtFechaFin3",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar3a",
			singleClick    :    true
		
		});
	</script>
	
	<script type="text/javascript" language="javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case 'ERROR':
					fc_Message(strMsg);
					break;
				case 'OK':
					fc_Message(strMsg);
					break;
			}
		}
		
		function fc_Message(strMsg){
			if (strMsg != ""){
				alert(strMsg);
			}
		}
		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
	</script>
	
</body>
