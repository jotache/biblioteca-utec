<%@ include file="/taglibs.jsp"%>
<head>

        <link href="/SGA/styles/estilo_reclutam.css" rel="stylesheet" type="text/css" />
        <script src="/SGA/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>

        <script language=javascript>
        <!--

                /*
                Event.observe(window, 'load', function() {
                        //$('txtUsuario').focus();
                });
                */

                document.observe("dom:loaded", function() {
                        $('txtUsuario').focus();
                });

                function onLoad(){}

                function fc_Validar(){
                        if (fc_Trim(document.getElementById("txtUsuario").value) == ""){
                                alert('Debe ingresar su correo');
                                document.getElementById("txtUsuario").focus();
                                return false;
                        }

                        if(fc_Trim(document.getElementById("txtClave").value) == ""){
                                alert('Debe ingresar su clave');
                                document.getElementById("txtClave").focus();
                                return false;
                        }
                        return true;
                }

                function fc_Enter(e, param){
                        if (e.keyCode == 13){fc_Ingresar(param)}
                }

                function fc_Upload(param) {
                        switch(param){
                                case '1':
                                        Fc_Popup("/SGA/reclutamiento/cambio_clave.html",350,195,"SGA")
                                        break;
                                case '2':
                                        Fc_Popup("/SGA/reclutamiento/olvido_clave.html",375,125,"SGA")
                                        break;
                        }
                }

                function fc_Ingresar(param) {
                                switch(param){
                                        case '1':
                                                if (fc_Validar()){
                                                        document.frmMain.txhOperacion.value='ACCESO';
                                                        document.frmMain.submit();
                                                        //document.getElementById('txhOperacion').value = "ACCESO";
                                                        //document.getElementById("frmMain").submit();
                                                }
                                                break;
                                        case '2':
                                                document.getElementById('txhOperacion').value = "REGISTRO";
                                                location.href="/SGA/ReclutamientoPostulante.html?txhIdRec=&txhOperacion=" + document.getElementById('txhOperacion').value;
                                                break;
                                }
                }
        //-->
        </script>

<style type="text/css">

        table.login {
            background-image:url(/SGA/images/login/rec_logueo.jpg);
            background-repeat:no-repeat;
            height:522px;
            width:937px;

            border-style: solid;
            border-width: 0px;

            border-spacing: 0;
            border-collapse: collapse;

            padding: 0px;

            margin-top:6%;

            margin-left:auto;
            margin-right:auto;
        }

        td {
            color: white;
        }

        div.message {
        background: #ffc;
        border: 1px solid green;
        color: black;
        font-weight: normal;
        font: 8pt/14pt Verdana, sans-serif;
        margin: 10px auto;
        padding: 2px;
        padding-left: 4px;
        text-align: center;
        vertical-align: bottom;
        }
</style>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/SGA/images/login/ingresar2.jpg','/SGA/images/login/olvido2.jpg')">

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16798972-1']);  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

        <form:form name="frmMain" id="frmMain" commandName="control" action="/SGA/logueoPostulante.html" method="post">
                <form:hidden path="operacion" id="txhOperacion"/>
                <form:hidden path="message" id="txhMessage"/>
                <form:hidden path="typeMessage" id="txhTypeMessage"/>
                <form:hidden path="idRec" id="txhIdRec"/>

                <table class="login">
                    <tr height="115px">
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr height="20px">
                        <td width="600px">&nbsp;</td>
                        <td width="70px">&nbsp;</td>
                        <td width="20px">&nbsp;</td>
                        <td width="110px" class="superiorlink" bgcolor="#FFFFFF"><a href="#" class="superiorlink" onclick="javascript:fc_Ingresar('2');">Registrarse</a></td>
                        <td width="110px" class="superiorlink" bgcolor="#FFFFFF"><a href="#" class="superiorlink" onclick="javascript:fc_Upload('1');">Cambiar Clave</a></td>
                        <td width="27px" >&nbsp;</td>
                    </tr>
                    <tr height="130px">
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr height="26px">
                        <td colspan="2">&nbsp;</td>
                        <td colspan="4">
                            <label>
                                <form:input path="usuario" id="txtUsuario" cssClass="relleno" onblur="javascript:fc_ValidaTextoGeneralEmail(this.id,'');"/>
                            </label>
                        </td>
                    </tr>
                    <tr height="13px">
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr height="26px">
                        <td colspan="2">&nbsp;</td>
                        <td colspan="4">
                            <form:password path="clave" id="txtClave" onkeypress="javascript:fc_Enter(event,'1')" cssClass="relleno"/>
                        </td>
                    </tr>
                    <tr height="20px">
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr height="40px">
                        <td>&nbsp;</td>
                        <td colspan="4" align="center">
                            <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image55','','/SGA/images/login/ingresar2.jpg',1)">
                                <img src="/SGA/images/login/ingresar1.jpg" name="Image55" border="0" onclick="javascript:fc_Ingresar('1');">
                            </a>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr height="20px">
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr height="40px">
                        <td>&nbsp;</td>
                        <td colspan="4" align="center">
                            <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image56','','/SGA/images/login/olvido2.jpg',1)">
                                <img src="/SGA/images/login/olvido1.jpg" name="Image56"  border="0" onclick="javascript:fc_Upload('2');">
                            </a>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr height="72px">
                        <td colspan="6">&nbsp;</td>
                    </tr>
                </table>
            <!--<table width="817" style="margin-top:6%" border="0" bordercolor="green" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                                <td><img src="/SGA/images/login/spacer.gif" width="458" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="86" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="21" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="7" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="4" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="60" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="45" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="8" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="53" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="11" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="41" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="23" height="1" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="1" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td colspan="12"><img name="login_r1_c1" src="/SGA/images/login/login_r1_c1.jpg" width="100%" height="105" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="105" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="2" colspan="5"><img name="login_r2_c1" src="/SGA/images/login/login_r2_c1.jpg" width="576" height="132" border="0" alt=""></td>
                                <td colspan="2" bgcolor="#FFFFFF" class="superiorlink"><a href="#" class="superiorlink" onclick="javascript:fc_Ingresar('2');">Registrarse</a></td>
                                <td rowspan="3" align="left"><img name="login_r2_c8" src="/SGA/images/login/login_r2_c8.jpg" width="8" height="134" border="0" alt=""></td>
                                <td colspan="3" bgcolor="#FFFFFF" class="superiorlink"><a href="#" class="superiorlink" onclick="javascript:fc_Upload('1');">Cambiar Clave</a> </td>
                                <td rowspan="14" valign="top"><img name="login_r2_c12" src="/SGA/images/login/login_r2_c12.jpg" width="23" height="361" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="20" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="2" colspan="2"><img name="login_r3_c6" src="/SGA/images/login/login_r3_c6.jpg" width="105" height="114" border="0" alt=""></td>
                                <td rowspan="2" colspan="3"><img name="login_r3_c9" src="/SGA/images/login/login_r3_c9.jpg" width="108" height="114" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="112" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="12" valign="top"><img name="login_r4_c1" src="/SGA/images/login/login_r4_c1.jpg" width="458" height="229" border="0" alt=""></td>
                                <td rowspan="3" bgcolor="#B9434F" class="datosizq">E-mail</td>
                                <td colspan="3"><img name="login_r4_c3" src="/SGA/images/login/login_r4_c3.jpg" width="32" height="2" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="2" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="11" valign="top"><img name="login_r5_c3" src="/SGA/images/login/login_r5_c3.jpg" width="21" height="227" border="0" alt=""></td>
                                <td colspan="6"><label>
                                <form:input path="usuario" id="txtUsuario" cssClass="relleno" onblur="javascript:fc_ValidaTextoGeneralEmail(this.id,'');"/>
                                </label></td>
                                <td rowspan="7" colspan="2" valign="top"><img name="login_r5_c10" src="/SGA/images/login/login_r5_c10.jpg" width="100%" height="81" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="20" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="3" colspan="6" valign="top"><img name="login_r6_c4" src="/SGA/images/login/login_r6_c4.jpg" width="100%" height="15" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="1" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td><img name="login_r7_c2" src="/SGA/images/login/login_r7_c2.jpg" width="86" height="13" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="13" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="3" background="/SGA/images/login/login_r8_c2.jpg" class="datosizq">Clave</td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="1" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td colspan="6"><form:password path="clave" id="txtClave" onkeypress="javascript:fc_Enter(event,'1')" cssClass="relleno"/></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="20" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="2" colspan="6"><img name="login_r10_c4" src="/SGA/images/login/login_r10_c4.jpg" width="100%" height="26" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="2" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="5"  valign="top"><img name="login_r11_c2" src="/SGA/images/login/login_r11_c2.jpg" width="86" height="170" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="24" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="2" colspan="3" align="center"><img name="login_r12_c4" src="/SGA/images/login/login_r12_c4.jpg" width="100%" height="89" border="0" alt=""></td>
                                <td colspan="4" align="center"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image55','','/SGA/images/login/ingresar2.jpg',1)"><img src="/SGA/images/login/ingresar1.jpg" name="Image55" width="100%" height="45" border="0" onclick="javascript:fc_Ingresar('1');"></a></td>
                                <td rowspan="4"><img name="login_r12_c11" src="/SGA/images/login/login_r12_c11.jpg" width="100%" height="146" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="0" height="45" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td colspan="4"><img name="login_r13_c7" src="/SGA/images/login/login_r13_c7.jpg" width="100%" height="44" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="0" height="44" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td rowspan="2"><img name="login_r14_c4" src="/SGA/images/login/login_r14_c4.jpg" width="7" height="57" border="0" alt=""></td>
                                <td colspan="6" align="center"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image56','','/SGA/images/login/olvido2.jpg',1)">
                                <img src="/SGA/images/login/olvido1.jpg" name="Image56" width="100%" height="30" border="0" onclick="javascript:fc_Upload('2');"></a></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="30" border="0" alt=""></td>
                        </tr>
                        <tr>
                                <td colspan="6"><img name="login_r15_c5" src="/SGA/images/login/login_r15_c5.jpg" width="100%" height="27" border="0" alt=""></td>
                                <td><img src="/SGA/images/login/spacer.gif" width="1" height="27" border="0" alt=""></td>
                        </tr>
                </table>  -->

        </form:form>

        <script type="text/javascript" language="javascript">
                if (document.getElementById("txhTypeMessage").value != ""){
                strMsg = document.getElementById("txhMessage").value;
                        switch(document.getElementById("txhTypeMessage").value){
                                case 'ERROR':
                                        if ( strMsg != ""){
                                                alert(strMsg);
                                        }
                                        break;
                                case 'PASS':
                                        if ( strMsg != ""){
                                                alert(strMsg);
                                        }
                                        break;
                                case 'OK':
                                        location.href="/SGA/ReclutamientoPostulante.html";
                                        break;
                        }
                }
                document.getElementById("txhTypeMessage").value = "";
                document.getElementById("txhMessage").value = "";
        </script>
</body>