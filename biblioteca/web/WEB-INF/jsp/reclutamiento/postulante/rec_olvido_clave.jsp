<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script type="text/javascript">
		function onLoad(){}		
		function fc_Enviar(){
			if(trim(document.frmMain.txtUsuario.value)==''){
				alert('Ingrese su Email');
				document.frmMain.txtUsuario.focus();
				return false;
			}			
			document.getElementById("txhOperacion").value="ENVIAR";
			document.getElementById("frmMain").submit();
		}
	</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" >
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/olvido_clave.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<table cellspacing="0" cellpadding="0" border="0" style="width:99%; margin-left: 10px; margin-top: 10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">�Olvid&oacute; su Clave?</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<TABLE style="width:94%;margin-top:5px;margin-left:10px" class="tabla" cellspacing="2" cellpadding="2" border="0" bordercolor="blue">		
			<TR>
				<TD class="texto_bold" width="30%">Ingresar E-mail:</TD>
				<td width="70%" align="left">
					<form:input path="usuario" id="txtUsuario" cssClass="cajatexto_1" cssStyle="width:98%"  
					onblur="javascript:fc_ValidaTextoGeneralEmail(this.id,'');"/>
				</td>
			</TR>
		</table>
		<br>
		<table align="center">
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/enviar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/enviar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Enviar();"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="javascript:window.close();" ID="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
	</form:form>
	
	<script type="text/javascript" language="javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
			strMsg = document.getElementById("txhMessage").value;		
			switch(document.getElementById("txhTypeMessage").value){
				case 'OK':
					fc_Message(strMsg);
					window.close();
					break;
				case 'ERROR':
					fc_Message(strMsg);
					fc_Limpia();
					break;
			}
		}
		
		function fc_Message(strMsg){
			if (strMsg != ''){
				alert(strMsg);
			}
		}
		
		function fc_Limpia(){
			document.getElementById("txtUsuario").value = "";
		}
		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
	</script>
</body>