<%@ include file="/taglibs.jsp"%>
<head>
<style type="text/css">
	.tablaDatPer{
	BORDER-RIGHT: #6b6d6b 1px solid;
    BORDER-TOP: #6b6d6b 1px solid;
	BORDER-BOTTOM: #6b6d6b 1px solid;
    BORDER-LEFT: #6b6d6b 1px solid;
    FONT-FAMILY: Arial;
    BORDER-COLOR:#AD222F;
    /*COLOR: #606060;*/
    COLOR:black;    
    BACKGROUND-COLOR: #f2f2f2;
    /*BACKGROUND-COLOR: white;*/
	}
</style>
<link href="${ctx}/scripts/jscalendar/calendar-blue.css"
	rel="stylesheet" type="text/css" media="all" title="winter" />
<script type="text/javascript"
	src="${ctx}/scripts/jscalendar/calendar.js"></script>
<script type="text/javascript"
	src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript"
	src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"
	type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_rec-textos.js"
	language="JavaScript;" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_rec-numeros.js"
	language="JavaScript;" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_rec-fechas.js"
	language="JavaScript;" type="text/JavaScript"></script>
<script type="text/javascript" type="text/javascript">



	
		document.observe("dom:loaded", function() {		
			document.getElementById("td_c1").style.display = "none";
			document.getElementById("td_c2").style.display = "none";
			//document.getElementById("td_c3").style.display = "none";
			//document.getElementById("td_c4").style.display = "none";
			if(document.getElementById("txhIdRec").value == ""){
				document.getElementById("td_c1").style.display = "inline-block";
				document.getElementById("td_c2").style.display = "inline-block";
				//document.getElementById("td_c3").style.display = "inline-block";
				//document.getElementById("td_c4").style.display = "inline-block";				
			}
			else{
				if (document.getElementById("txhIndSexo").value == "0002")
					document.getElementById("rbtSexoM").checked = true;
				else
					document.getElementById("rbtSexoF").checked = true;
				
				if (document.getElementById("txhIndPuestoPostula").value == "2")
					document.getElementById("rbtPuesPostAmb").checked = true;
				else {
					if (document.getElementById("txhIndPuestoPostula").value == "1")
						document.getElementById("rbtPuesPostDoc").checked = true;
					  else
						document.getElementById("rbtPuesPostAdm").checked = true;
				}
				if (document.getElementById("txhIndPostulado").value == "1")
					document.getElementById("rbtPostSi").checked = true;
				else
					document.getElementById("rbtPostNo").checked = true;
					
				if (document.getElementById("txhIndTrabajado").value == "1")
					document.getElementById("rbtTrabSi").checked = true;
				else
					document.getElementById("rbtTrabNo").checked = true;
					
				if (document.getElementById("txhIndFamiliares").value == "1"){
					document.getElementById("rbtFamSi").checked = true;
					trFamilia.style.display = "";
				}
				else{
					document.getElementById("rbtFamNo").checked = true;
					trFamilia.style.display = "none";
				}
					
				if (document.getElementById("txhIndExperiencia").value == "1"){
					document.getElementById("rbtExpSi").checked = true;
					tr_Experiencia.style.display = "";
				}
				else{
					document.getElementById("rbtExpNo").checked = true;
					tr_Experiencia.style.display = "none";
				}
				if (document.getElementById("txhIndPago").value == "1"){
					document.getElementById("rbtPagoMes").checked = true;					
				}
				else{
					document.getElementById("rbtPagoHoras").checked = true;
				}
				fc_AsignarDisponibilidad();				
			}
		});
		
		function onLoad(){}
		
		function fc_AsignarDisponibilidad(){		
			cadena = document.getElementById("txhIndDispoViaje").value; 					
			codDisponibilidad = cadena.split("|");
			if(codDisponibilidad[0]=="1")
				document.getElementById("rbtDentroPais").checked = true;			
			if(codDisponibilidad[1]=="1")
				document.getElementById("rbtFueraPais").checked = true;			
		}		
		function fc_GetNumeroSeleccionados(){
			strCodSel = document.getElementById("txhCodArea").value;
			if ( strCodSel != '' ){
				ArrCodSel = strCodSel.split("|");
				return ArrCodSel.length - 1;
			}
			else return 0;
		}				
		function Fc_AgregarAreaInteres(obj){	
			document.getElementById("iFrame2").contentWindow.Fc_AgregarAreaInteres(obj);						
		}
		
		function Fc_AgregarAreaInteres2(obj){
			document.getElementById("iFrame2").src = "";
		}			
		function fc_getAreaInteres(){		
			objHijo = document.getElementById("iFrame2").contentWindow;
			objHijo.fc_getAreaInteres();			
			document.getElementById("txhCodArea").value = objHijo.document.getElementById("txhCodArea").value;
			document.getElementById("txhNroRegAreaInt").value = objHijo.document.getElementById("txhNroRegAreaInt").value;
			//alert(document.getElementById("txhCodArea").value);
		}	
		function fc_EliAreaInteres(obj) {
			var tr = document.getElementById('trArea'+obj);
			tr.style.display = 'none';
		}		
		function fc_Trabajado(strTipo) {
			objTrabSi = document.getElementById("rbtTrabSi");
			objTrabNo = document.getElementById("rbtTrabNo");
			objTrabSi.checked = false;
			objTrabSi.checked = false;
			if (strTipo == 1)
				objTrabSi.checked = true;			
			else
				objTrabNo.checked = true;			
			document.getElementById("txhIndTrabajado").value = strTipo;
		}
		function fc_PuestoPostulado(strTipo){
			objPuesPostDoc = document.getElementById("rbtPuesPostDoc");
			objPuesPostAdm = document.getElementById("rbtPuesPostAdm");
			objPuesPostAmb = document.getElementById("rbtPuesPostAmb");
			objPuesPostDoc.checked = false;
			objPuesPostAdm.checked = false;
			objPuesPostAmb.checked = false;
			if (strTipo == 2)		
				objPuesPostAmb.checked = true;			
			else if (strTipo == 1)
				objPuesPostDoc.checked = true;
			else if (strTipo==0)
				objPuesPostAdm.checked = true;				
			document.getElementById("txhIndPuestoPostula").value = strTipo;
		}		
		function fc_Postulado(strTipo) {
			objPostSi = document.getElementById("rbtPostSi");
			objPostNo = document.getElementById("rbtPostNo");
			objPostSi.checked = false;
			objPostNo.checked = false;
			if (strTipo == 1)
				objPostSi.checked = true;			
			else
				objPostNo.checked = true;			
			document.getElementById("txhIndPostulado").value = strTipo;
		}
		function fc_Experiencia(strTipo) {
			objExpSi = document.getElementById("rbtExpSi");
			objExpNo = document.getElementById("rbtExpNo");
			objExpSi.checked = false;
			objExpNo.checked = false;
			if (strTipo == 1)
				objExpSi.checked = true;			
			else
				objExpNo.checked = true;			
			document.getElementById("txhIndExperiencia").value = strTipo;		
			tr_Experiencia.style.display = "none";			
			if(objExpSi.checked == true)
				tr_Experiencia.style.display = "";			
			else if(objExpNo.checked == true)
				tr_Experiencia.style.display = "none";			
		}		
		function fc_Pago(strTipo) {
			objPagoMes = document.getElementById("rbtPagoMes");
			objPagoHoras = document.getElementById("rbtPagoHoras");			
			objPagoMes.checked = false;
			objPagoHoras.checked = false;			
			if (strTipo == 1)
				objPagoMes.checked = true;
			else
				objPagoHoras.checked = true;
			document.getElementById("txhIndPago").value = strTipo;
		}		
		function fc_Familia(strTipo) {
			objFamSi = document.getElementById("rbtFamSi");
			objFamNo = document.getElementById("rbtFamNo");
			objFamSi.checked = false;
			objFamNo.checked = false;
			if (strTipo == 1)
				objFamSi.checked = true;
			else
				objFamNo.checked = true;
			document.getElementById("txhIndFamiliares").value = strTipo;
			trFamilia.style.display = "none";
			if (objFamSi.checked == true)
				trFamilia.style.display = "";
			else if(objFamNo.checked == true)
				trFamilia.style.display = "none";
		}
		function fc_Nacionalidad(strTipo){
		}
		
		function fc_Sexo(strTipo){
			objSexoF = document.getElementById("rbtSexoF");
			objSexoM = document.getElementById("rbtSexoM");			
			objSexoF.checked = false;
			objSexoM.checked = false;
			if (strTipo == '0002')
				objSexoM.checked = true;			
			else
				objSexoF.checked = true;
			document.getElementById("txhIndSexo").value = strTipo;
		}
		function fc_DispoViaje(strTipo){				
		}		
		function Fc_Valida(){
			if(fc_Trim(document.getElementById("txtNombre").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Nombre');
				document.getElementById("txtNombre").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtApePat").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Apellido Paterno');
				document.getElementById("txtApePat").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtFecNac").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Fecha de Nacimiento');
				document.getElementById("txtFecNac").focus();
				return false;
			}
			if(document.getElementById("txhIndSexo").value == ""){
				alert('Seleccione el Sexo');
				document.getElementById("rbtSexoF").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("cboEstadoCivil").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Estado Civil');
				document.getElementById("cboEstadoCivil").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtDni").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'DNI');
				document.getElementById("txtDni").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtEmail").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Email');
				document.getElementById("txtEmail").focus();
				return false;
			}
			if(document.getElementById("txhIdRec").value == ""){
				if(document.getElementById("txtClave").value == ""){
					alert('Debe ingresar el valor correspondiente en el campo ' + 'Debe ingresar la clave de su cuenta.');
					document.getElementById("txtClave").focus();
					return false;
				}
				if(document.getElementById("txtClave1").value == ""){
					alert('Debe ingresar el valor correspondiente en el campo ' + 'Confirmaci�n de Clave');
					document.getElementById("txtClave1").focus();
					return false;
				}
				if (document.getElementById("txtClave").value!=document.getElementById("txtClave1").value){
					alert('La confirmaci�n de la contrase�a no coincide.');
					document.getElementById("txtClave").value = "";
					document.getElementById("txtClave1").value = "";
					document.getElementById("txtClave").focus();
					return false;
				}
			}
			if(fc_Trim(document.getElementById("txtDireccion").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Direcci�n');
				document.getElementById("txtDireccion").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtTelef").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Tel�fono Domicilio');
				document.getElementById("txtTelef").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtPais").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Pa�s');
				document.getElementById("txtPais").focus();
				return false;
			}
			if(document.getElementById("cboMoneda").value==""){
		
			}
			if(fc_Trim(document.getElementById("txtPretensionEconomica").value) == "" ){
		
			}
			if(document.getElementById("txhIndPago").value==""){
				alert('Debe seleccionar la frecuencia de pago.');
				return false;
			}
			
			if (document.getElementById("txhIndFamiliares").value == "0"){
				document.getElementById("txtFamiliaNombres").value = "";
			}
			if(fc_Trim(document.getElementById("cboDispoTrabajar").value) == "" ){
				alert('Debe ingresar el valor correspondiente en el campo ' + 'Disponibilidad');
				document.getElementById("cboDispoTrabajar").focus();
				return false;
			}			
			if(document.getElementById("txhIndPuestoPostula").value == ""){
				alert("Seleccione el tipo de puesto al que Postula (Docente o Administrativo)");
				return false;
			}				
			return true;
		}
		
		function fc_GrabarDatos(){							
			document.getElementById("iFrame1").contentWindow.copyParametros();			
			fc_DisponibilidadViajar();				
			if (Fc_Valida()){				
				fc_getAreaInteres();				
				if (confirm('�Est� seguro de grabar?')){		 						
					document.getElementById("txhOperacion").value="GRABAR";
					$('frmMain').submit();
				}
			}
		}
		function fc_DisponibilidadViajar(){
			objDentroPais = document.getElementById("rbtDentroPais");
			objFueraPais = document.getElementById("rbtFueraPais");				
			var dentroPais = "0";
			var fueraPais = "0";
			if(objDentroPais.checked == true)
				dentroPais = "1";
			if(objFueraPais.checked == true)
				fueraPais = "1";
			var disponibilidad = dentroPais+"|"+fueraPais;
			document.getElementById("txhIndDispoViaje").value = disponibilidad;
		}
		
		function fc_onChange(obj, strValor){
			if (document.getElementById(obj.id).value != ""){
				document.getElementById("txhOperacion").value = strValor;
				$('frmMain').submit();
			}
		}
		
		function textKey(name) {
			n = document.getElementById(name).value.length;
		  	t = 1000;
		  	if(n > t)
		    	document.getElementById(name).value = document.getElementById(name).value.substring(0, t);		  	
		  	else{
		  		//document.getElementById('msgCont').value = t-n;
		  		var contenido = $('msgCont');
		  		contenido.value = t-n;
		  	}
		    			  	
		}
		function fc_CantCaracter2(strNameObj, cant, campo){	
			var strCadena = strNameObj.value;
			if(strCadena!=""){
				switch(campo){				
					case 'Dni':
						if (strCadena.length>cant){
							alert("El DNI no debe tener mas 12 caracteres");
							strNameObj.focus();
						}
						if (strCadena.length<8){
							alert("El DNI no debe tener menos de 8 caracteres");
							strNameObj.focus();
						}
					break;				
				}
			}
		}		
	</script>

</head>
<body>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/datos_personales.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="typeMessage" id="txhTypeMessage" />
	<form:hidden path="message" id="txhMessage" />
	<form:hidden path="nroRegAreaInt" id="txhNroRegAreaInt" />
	<form:hidden path="idRec" id="txhIdRec" />
	<form:hidden path="codArea" id="txhCodArea" />
	<form:hidden path="cadenaAI" id="txhCadenaAI" />

	<form:hidden path="sexo" id="txhIndSexo" />
	<form:hidden path="postuladoAntes" id="txhIndPostulado" />
	<form:hidden path="puestoPostula" id="txhIndPuestoPostula" />
	<form:hidden path="trabajadoAntes" id="txhIndTrabajado" />
	<form:hidden path="familiaTecsup" id="txhIndFamiliares" />
	<form:hidden path="expDocente" id="txhIndExperiencia" />
	<form:hidden path="indPago" id="txhIndPago" />
	<form:hidden path="dispoViaje" id="txhIndDispoViaje" />

	<!-- MODIFICACION -->
	<form:hidden path="departamento" id="cboDepartamento" />
	<form:hidden path="provincia" id="cboProvincia" />
	<form:hidden path="distrito" id="cboDistrito" />
	<form:hidden path="codUsuario" id="codUsuario" />

	<div style="height: 450px; width: 100%; border: 0px; border-color: red;align:center;">
	<!--	Datos Personales "overflow: auto;  -->
	<table cellpadding="0" cellspacing="0" border="0" width="940px" height="36px" style="margin-left: 8px" border="0" align="center">
		<tr>
			<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo2.jpg"
				style="height: 36px" class="opc_combo">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Datos
			Personales</td>
		</tr>
	</table>
    
	<table style="width: 935px; margin-left: 9px"  border="0" class="tablaDatPer" cellspacing="3" cellpadding="0" align="center">
		<tr>
			<td height="14px" colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td width="200px" style="font-size:12px;">NOMBRES :</td>
			<td width="200px" style="font-size:12px;">	
				<form:input path="nombres" id="txtNombre" cssClass="cajatexto_o" onkeypress="javascript:fc_ValidaTextoEspecial();" />
			</td>
			<td width="200px" class="" style="font-size:12px;">FECHA DE REGISTRO :</td>
			<td width="150px" style="font-size:12px;"><c:out value="${control.fecreg}"></c:out></td>
			<td width="155px" rowspan="5">&nbsp;<c:if test="${control.foto!=null}">
					<img src="${control.foto}" id="imgFoto" width="100" height="120px" border="1">
				</c:if>
			</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td style="font-size:12px;">APELLIDO PATERNO :</td>
			<td ><form:input path="apepat" id="txtApePat"
				cssClass="cajatexto_o"
				onkeypress="javascript:fc_ValidaTextoEspecial();" /></td>
			<td  style="font-size:12px;">APELLIDO MATERNO:</td>
			<td ><form:input path="apemat" id="txtApeMat"
				cssClass="cajatexto_1"
				onkeypress="javascript:fc_ValidaTextoEspecial();" /></td>
			
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td style="font-size:12px;">FECHA NACIMIENTO :</td>
			<td ><form:input path="fecnac" id="txtFecNac"
				cssClass="cajatexto_o"
				onblur="javascript:fc_ValidaFechaOnblur(this.id);"
				onkeypress="javascript:fc_ValidaFecha(this.id);" maxlength="10" />&nbsp;
			<a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imgCalendar1','','${ctx}/images/iconos/calendario2.jpg',1)">
			<img src="${ctx}/images/iconos/calendario1.jpg" align="middle"
				id="imgCalendar1" name="imgCalendar1" alt="Calendario"
				style="CURSOR: pointer"> </a></td>
			<td width="20%" style="font-size:12px;">SEXO :</td>
			<td width="20%" class="texto_cap"><input type="radio"
				ID="rbtSexoF" onclick="javascript:fc_Sexo('0001');" NAME="rbtSexo">
			  Femenino
			    <input type="radio" ID="rbtSexoM"
				onclick="javascript:fc_Sexo('0002');" NAME="rbtSexo">Masculino
			</td>
			
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td style="font-size:12px;">ESTADO CIVIL :</td>
			<td ><form:select path="estadoCivil"
				id="cboEstadoCivil" cssClass="cajatexto" cssStyle="width:115px">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.listaEstadoCivil!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion"
						items="${control.listaEstadoCivil}" />
				</c:if>
			</form:select></td>
			<td style="font-size:12px;">NACIONALIDAD :</td>
			<td class="texto_cap"><!-- //--RNAPA -- 23/04/2008 -->
			<form:input path="nacionalidad" id="txtNacionalidad"
				cssClass="cajatexto_1"
				onkeypress="javascript:fc_ValidaTextoEspecial();" /> <!-- <input type="radio" ID="rbtNacPeru" NAME="rbtNacion" onclick="javascript:fc_Nacionalidad('0');">Peruano 
					<input type="radio" ID="rbtNacOtro" NAME="rbtNacion" onclick="javascript:fc_Nacionalidad('1');">Otro
					 --></td>
			
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td style="font-size:12px;">DNI :</td>
			<td ><form:input path="dni" id="txtDni"
				maxlength="12" cssClass="cajatexto_o"
				onkeypress="javascript:fc_PermiteNumerosPunto();"
				onblur="javascript:javascript:fc_CantCaracter2(this,12,'Dni');" /></td>
			<td style="font-size:12px;">Nro. RUC :</td>
			<td ><form:input path="ruc" id="txtRuc"
				maxlength="11" cssClass="cajatexto_1"
				onkeypress="javascript:fc_PermiteNumerosPunto();"
				onblur="javascript:javascript:ValidarRuc_Retorno('txtRuc');" /></td>
			
		</tr>				
		<tr>
			<td >&nbsp;</td>
			<td style="font-size:12px;">EXPERIENCIA LABORAL :</td>
			<td colspan="4" class="texto_cap"><form:input
				path="anioExpLaboral" id="txtAnioExpLaboral" cssClass="cajatexto_1"
				maxlength="3"
				onblur="javascript:fc_ValidaNumeroOnBlur('txtAnioExpLaboral');"
				onkeypress="javascript:fc_PermiteNumeros();" />&nbsp;&nbsp;(en
			a�os)</td>
		</tr>
		<tr>
		  <td colspan="5" height="10PX"></td>
		 
	  </tr>
		<tr>
			<td >&nbsp;</td>
			<td colspan="2" style="font-size:12px;"><strong>DATOS PARA SU CUENTA DE USUARIO:</strong></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
		  <td colspan="5" height="7px"></td>
	  </tr>
		<tr>
			<td >&nbsp;</td>
			<td style="font-size:12px;">EMAIL:</td>
			<td><form:input path="email" id="txtEmail"
				cssClass="cajatexto_o" maxlength="100"
				onblur="javascript:fc_ValidaTextoGeneralEmail(this.id,'');"
				size="30" /></td>
			<td colspan="2" style="font-size:11px;">
					<em>(usuario para ingresar al sistema)</em></td>
			<td>&nbsp;</td>
		</tr>
		<tr id="td_c1" name="td_c1" style="display: none;">
			<td >&nbsp;</td>
			<td  style="font-size:12px;">CLAVE:</td>
			<td style="font-size:12px;">
				<form:password path="clave" id="txtClave" cssClass="cajatexto_o" maxlength="20"
				onblur="javascript:fc_CantCaracter(this,6,'Clave');"></form:password>
			</td>
		  <td colspan="3" style="font-size:11px;" ><em>(Clave para ingresar al sistema, no es la contrase&ntilde;a de su correo electr&oacute;nico)<BR>
		      <strong>M�ximo 20 caracteres</strong>.</em></td>
		</tr>
		<tr id="td_c2" name="td_c2" style="display: none">
			<td >&nbsp;</td>
			<td style="font-size:12px;">CONFIRMACI�N DE CLAVE:</td>
			<td >
				<form:password path="clave1"
				id="txtClave1" cssClass="cajatexto_o" maxlength="20"
				onblur="javascript:fc_CantCaracter(this,6,'Clave');" />
			</td>
			<td colspan="2">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td style="font-size:12px;">EMAIL ALTERNO:</td>
			<td>
				<form:input path="email1" id="txtEmailAlterno"
				cssClass="cajatexto" maxlength="100"
				onblur="javascript:fc_ValidaTextoGeneralEmail(this.id,'');"
				size="30" />
			</td>
			<td colspan="2">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="940px" height="36px" style="margin-left: 9px" border="0" align="center">
		<tr>
			<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo2.jpg" style="height: 36px" class="opc_combo">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Datos
			de domicilio</td>
		</tr>
	</table>

	<table style="width: 935px; margin-left: 9px" border="0" cellspacing="3" cellpadding="0" class="tablaDatPer" id="Table2" align="center">
		<tr>
			<td width="30px">&nbsp;</td>
			<td heigth="14px" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td width="150px" style="font-size:12px;">DOMICILIO :</td>
			<td colspan="3" class="texto_cap" align="left">
				<form:input path="direccion" maxlength="200" id="txtDireccion" cssClass="cajatexto_o" size="91" />&nbsp;(Av.,
			Calle, Numero, Pje.)</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td rowspan="3" colspan="2" valign="top" class="texto_cap" >
				
				<%-- --%>
				<iframe src="${ctx}/reclutamiento/datos_personales_domicilio.html?departamento=${control.departamento}&provincia=${control.provincia}&distrito=${control.distrito}"
					id="iFrame1" height="73px" width="350px" frameborder="0" scrolling="no"></iframe>
				
			</td>
			<td width="100px" style="font-size:12px;">TEL�FONO DOMICILIO :</td>
			<td width="200"><form:input path="telef" id="txtTelef" cssClass="cajatexto_o"
				onkeypress="javascript:fc_PermiteNumerosPunto();" maxlength="20" />
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">TEL�FONO ADICIONAL :</td>
			<td colspan="1"><form:input path="telefAdicional"
				id="txtTelefAdicional" cssClass="cajatexto_1"
				onkeypress="javascript:fc_PermiteNumerosPunto();" maxlength="20" />
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">TEL�FONO M�VIL :</td>
			<td colspan="1"><form:input path="telefMovil"
				id="txtTelefMovil" cssClass="cajatexto_1"
				onkeypress="javascript:fc_PermiteNumerosPunto();" maxlength="20" />
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;" width="150">PA�S DE RESIDENCIA :</td>
			<td ><form:input path="pais" id="txtPais"
				cssClass="cajatexto_o" maxlength="30" /></td>
			<td style="font-size:12px;" >C�DIGO POSTAL :</td>
			<td ><form:input path="codPostal"
				id="txtCodPostal" cssClass="cajatexto_1" maxlength="3" /></td>
		</tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="940px" height="36px" style="margin-left: 9px" border="0" align="center">
		<tr>
			<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo2.jpg"
				style="width: 500px; height: 36px" class="opc_combo">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informaci&oacute;n
			Laboral</td>
		</tr>
	</table>

	<table style="width: 935px; margin-left: 9px" border="0" cellspacing="3" cellpadding="0" class="tablaDatPer" id="Table3" align="center">
		<tr>			
			<td heigth="14px" colspan="4">&nbsp;</td>
		</tr>
		<!-- 			<tr>
				<td width="37%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&Aacute;rea de Inter&eacute;s :</td>
				<td width="20%">
					<form:select path="areaInteres" id="cboAreaInteres" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaAreaInteres!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaAreaInteres}" />
						</c:if>
					</form:select>&nbsp;&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/avanzar2.jpg',1)">
					<img src="${ctx}\images\iconos\avanzar.jpg" style="cursor:hand" id="imgagregar"
					onclick="javascript:Fc_AgregarAreaInteres(cboAreaInteres);" align="center">
				</td>
			</tr> -->
		<tr>
			<td width="30px">&nbsp;</td>
			<td width="220px" style="font-size:12px;">�HA POSTULADO ANTES A TECSUP?</td>
			<td class="texto_cap" width="320px"><input type="radio"
				ID="rbtPostSi" onclick="javascript:fc_Postulado('1');"
				NAME="rbtPostulado">Si <input type="radio" ID="rbtPostNo"
				onclick="javascript:fc_Postulado('0');" NAME="rbtPostulado">No
			</td>

			<td valign="baseline" style="font-size:12px;">PUESTO AL QUE POSTULA:&nbsp;<br>
				<input type="radio" ID="rbtPuesPostDoc" onclick="javascript:fc_PuestoPostulado('1');" NAME="rbtPuesPostDoc">Docente&nbsp;
				<input type="radio" ID="rbtPuesPostAdm" onclick="javascript:fc_PuestoPostulado('0');" NAME="rbtPuesPostAdm">Administrativo&nbsp;
				<input type="radio" ID="rbtPuesPostAmb" onclick="javascript:fc_PuestoPostulado('2');" NAME="rbtPuesPostAmb">Ambos
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">�HA TRABAJADO ANTES EN TECSUP?</td>
			<td class="texto_cap">
					<input type="radio" ID="rbtTrabSi"
					onclick="javascript:fc_Trabajado('1');" NAME="rbtTrabajado">Si
				<input type="radio" ID="rbtTrabNo"
					onclick="javascript:fc_Trabajado('0');" NAME="rbtTrabajado">No
			</td>
			<td rowspan="9" valign="top">
				<table id="tblAreaInteres" cellpadding="0" cellspacing="0" class="tabla_1A" style="width: 100%; height: 310px" border="0">					
					<tr class="tabla_1A">
						<!--td valign="top" nowrap="nowrap" width="120px">&Aacute;rea de Inter&eacute;s :</td-->
						<td valign="top" class="tabla_1A">
							<iframe
								src="${ctx}/reclutamiento/datos_personales_areas_interes.html?txhIdRec=${control.idRec}"
								id="iFrame2" height="310px" width="100%" frameborder="0"
								class="tabla_1A">
							</iframe>
						</td>
				</table>
			
<!--				<table class="tabla_1A">-->
<!--					<tr height="2px">-->
<!--						<td></td>-->
<!--					</tr>-->
<!--				</table>-->
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">�TIENE FAMILIARES TRABAJANDO EN TECSUP?</td>
			<td class="texto_cap" nowrap="nowrap" align="left">
				<table width="100%">
					<tr align="left">
						<td align="left"><input type="radio" ID="rbtFamSi"
							NAME="rbtFamilia" onclick="javascript:fc_Familia('1');">Si
						<input type="radio" ID="rbtFamNo" NAME="rbtFamilia"
							onclick="javascript:fc_Familia('0');">No</td>
						<td id="trFamilia" style="display: none"><form:textarea
							path="familiaNombres" id="txtFamiliaNombres" cssClass="cajatexto"
							cols="28" rows="2" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">�TIENE EXPERIENCIA COMO DOCENTE?</td>
			<td class="texto_cap" nowrap="nowrap" align="left">
				<table width="100%">
					<tr align="left">
						<td align="left" width="30%"><input type="radio" ID="rbtExpSi"
							NAME="rbtExperiencia" onclick="javascript:fc_Experiencia('1');">Si
						<input type="radio" ID="rbtExpNo" NAME="rbtExperiencia"
							onclick="javascript:fc_Experiencia('0');">No</td>
						<td id="tr_Experiencia" style="display: none; width: 70%">
						A�os : &nbsp;&nbsp;<form:input path="expDocenteAnios"
							id="expDocenteAnios" cssClass="cajatexto"
							onkeypress="javascript:fc_PermiteNumerosPunto();" maxlength="2"
							cssStyle="width:20px" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">DISPONIBILIDAD DE VIAJAR</td>
			<td class="texto_cap" nowrap="nowrap"><!-- //--RNAPA -- 23/04/2008 -->
				<input type="checkbox" ID="rbtDentroPais"
					onclick="javascript:fc_DispoViaje('0');" NAME="rbtDispoViaje">Dentro
				del Pa&iacute;s <input type="checkbox" ID="rbtFueraPais"
					onclick="javascript:fc_DispoViaje('1');" NAME="rbtDispoViaje">Fuera
				del Pa&iacute;s <!--<input type="radio" ID="rbtDentroPais" onclick="javascript:fc_DispoViaje('0');" NAME="rbtDispoViaje">Dentro del Pa&iacute;s 
						<input type="radio" ID="rbtFueraPais" onclick="javascript:fc_DispoViaje('1');" NAME="rbtDispoViaje">Fuera del Pa&iacute;s
					 -->
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">SEDE PREFERENTE PARA TRABAJAR</td>
			<td><form:select path="sedePrefTrabajo" id="cboSedePrefTrabajo"
				cssClass="cajatexto" cssStyle="width:115px">
				<form:option value="">--Seleccione--</form:option>
				<form:option value="0000">(TODAS)</form:option>
				<c:if test="${control.listaSedePrefTrabajo!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="descripcion" items="${control.listaSedePrefTrabajo}" />
				</c:if>
			</form:select></td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">INTERESADO EN:</td>
			<td><form:select path="interesEn" id="cboInteresEn"
				cssClass="cajatexto" cssStyle="width:115px">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.listaInteresEn!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="descripcion" items="${control.listaInteresEn}" />
				</c:if>
			</form:select></td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">PRETENSI�N ECON�MICA :</td>
			<td class="texto_cap"><form:select path="moneda"
				id="cboMoneda" cssClass="cajatexto_o" cssStyle="width:50px">
				<form:option value="">-Moneda-</form:option>
				<c:if test="${control.listaMoneda!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="dscValor3"
						items="${control.listaMoneda}" />
				</c:if>
			</form:select> <form:input path="pretensionEconomica" id="txtPretensionEconomica"
				cssClass="cajatexto_o" size="15"
				onblur="javascript:fc_ValidaNumeroOnBlur('txtPretensionEconomica');"
				onkeypress="javascript:fc_PermiteNumerosPunto();" /> <input
				type="radio" ID="rbtPagoMes" NAME="pago"
				onclick="javascript:fc_Pago('1');">Mes <input type="radio"
				ID="rbtPagoHoras" NAME="pago" onclick="javascript:fc_Pago('0');">Horas
			</td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">DEDICACI�N :</td>
			<td><form:select path="dedicacion" id="cboDedicacion"
				cssClass="cajatexto" cssStyle="width:145px">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.listaDedicacion!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="descripcion" items="${control.listaDedicacion}" />
				</c:if>
			</form:select></td>
		</tr>
		<tr>
			<td width="30px">&nbsp;</td>
			<td style="font-size:12px;">DISPONIBILIDAD :</td>
			<td ><form:select path="dispoTrabajar"
				id="cboDispoTrabajar" cssClass="cajatexto" cssStyle="width:115px">
				<form:option value="">--Seleccione--</form:option>
				<c:if test="${control.listaDispoTrabajar!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="descripcion" items="${control.listaDispoTrabajar}" />
				</c:if>
			</form:select></td>
			
		</tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="940px" height="36px" style="margin-left: 9px" border="0" align="center">
		<tr>
			<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo2.jpg"
				style="width: 500px; height: 36px" class="opc_combo">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Perfil
			Profesional</td>
		</tr>
	</table>


	<table style="width: 935px; margin-left: 9px" border="0" cellspacing="3" cellpadding="0" class="tablaDatPer" ID="Table4" align="center">
		<tr>
			<td heigth="14px">&nbsp;</td>
		</tr>
		<tr>
			<td style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INGRESE UN BREVE RESUMEN DE SU PERFIL:</td>
		</tr>
		<tr>
			<td width="20%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<form:textarea path="perfil" id="txtPerfil" cssClass="cajatexto"
				cols="150" rows="7" onkeyup="textKey('txtPerfil');" /></td>
		</tr>
		<tr>
			<td align="right" style="font-size:11px;"> <strong>M�ximo 1000 caracteres</strong>
			  &nbsp; <input type="text" value="1000" size="4"
				style="text-align: center;" name="msgCont" id="msgCont" disabled
				class="cajatexto">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	</table>
	<br>
	<table align="center" border="0" bordercolor="cyan" >
		<tr>
			<td><a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img id="imggrabar" src="${ctx}/images/botones/grabar1.jpg"
				onclick="javascript:fc_GrabarDatos();" style="cursor: pointer"
				alt="Grabar"> </a></td>
		</tr>
	</table>
	</div>
</form:form>

<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txtFecNac",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1",
			singleClick    :    true
		
		});
	</script>

<script type="text/javascript" language="javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case '-1':
					fc_Message(strMsg);
					fc_Limpia();
					//window.parent.location.href = "${ctx}/reclutamiento/logueo_reclutamiento.html";
					window.parent.location.href = "${ctx}/logueoPostulante.html";
					break;
				case '-2':
					fc_Message(strMsg);
					document.getElementById("txtEmail").value = "";
					break;
				case '-3':
					fc_Message(strMsg);
					document.getElementById("txtDni").value = "";
					document.getElementById("txtFecNac").value = "";
					break;
				case '-4':
					fc_Message(strMsg);
					document.getElementById("txtDni").value = "";
					break;
				case 'OK':					
					var strIndUsuNuevo = '<%=request.getAttribute("usuNuevo") == null
					? "0"
					: (String) request.getAttribute("usuNuevo")%>';
			if (strIndUsuNuevo == "1") {
				alert(strMsg
						+ " Es necesario ingresar la informaci�n de Estudios.");

				window.parent.location.href = "${ctx}/reclutamiento/cuerpo_registro.html?txhIdRec="
						+ document.getElementById('txhIdRec').value
						+ "&txhOperacion="
						+ document.getElementById('txhOperacion').value
						+ "&txhIndMenu=2&txhCodUsuario="
						+ document.getElementById('codUsuario').value;
			} else {
				fc_Message(strMsg);
				window.parent.location.href = "${ctx}/reclutamiento/cuerpo_registro.html?txhIdRec="
						+ document.getElementById('txhIdRec').value
						+ "&txhOperacion="
						+ document.getElementById('txhOperacion').value;
			}
			break;
		}
	}

	function fc_Message(strMsg) {
		if (strMsg != "") {
			alert(strMsg);
		}
	}

	function fc_Limpia() {
		/*document.getElementById("txhClave").value = "";
		document.getElementById("txtNuevaClave").value = "";
		document.getElementById("txtConfirmNuevaClave").value = "";*/
	}
	document.getElementById("txhTypeMessage").value = "";
	document.getElementById("txhMessage").value = "";
</script>
</body>