<%@page import="com.tecsup.SGA.common.CommonConstants"%>
<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/Func_Comunes.js" type="text/JavaScript"></script>
	<script type="text/javascript">
		var strExtensionJPG = "<%=CommonConstants.GSTR_EXTENSION_JPG%>";
		var strExtensionGIF = "<%=CommonConstants.GSTR_EXTENSION_GIF%>";
		var strExtensionDOC = "<%=CommonConstants.GSTR_EXTENSION_DOC%>";
		var strExtensionPDF = "<%=CommonConstants.GSTR_EXTENSION_PDF%>";
		var strExtensionDOCX = "<%=CommonConstants.GSTR_EXTENSION_DOCX%>";
		function onLoad(){		
			if ( document.getElementById("lblInfFinal") != null)
				document.getElementById("lblInfFinal").innerHTML = document.getElementById("txhInfFinal").value;
		}
		
		function fc_Valida(){
			var strExtension;
			var lstrRutaArchivo;

			if(	fc_Trim(document.getElementById("txtFoto").value) == "" && fc_Trim(document.getElementById("txtCV").value) == "" &&
				fc_Trim(document.getElementById("tdFoto").innerText) == "" && fc_Trim(document.getElementById("tdCV").innerText) == ""){
					alert('Debe seleccionar su CV y/o imagen a subir.');
					return false;
			}
			else if(fc_Trim(document.getElementById("txtCV").value) == "" && fc_Trim(document.getElementById("tdCV").innerText) != "" &&
					fc_Trim(document.getElementById("txtFoto").value) == "" && fc_Trim(document.getElementById("tdFoto").innerText) != ""){
					alert('Debe seleccionar su CV y/o imagen a subir.');
					return false;
			}
			else{
				if(fc_Trim(document.getElementById("txtFoto").value) != ""){					
					lstrRutaArchivo = document.getElementById("txtFoto").value;
					strExtension = "";
					strExtension = extension(lstrRutaArchivo); // lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);
					
					if (strExtension != strExtensionJPG && strExtension != strExtensionGIF){
						alert('El archivo imagen debe tener las siguientes\nextensiones : .jpg o .gif ');
						return false;
					}
					else{
						document.getElementById("txhExtFoto").value = strExtension;
					}
				}
				
				if(fc_Trim(document.getElementById("txtCV").value) != ""){
					lstrRutaArchivo = document.getElementById("txtCV").value;
					strExtension = "";
					strExtension = extension(lstrRutaArchivo); //lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);
					
					if (strExtension != strExtensionDOC && strExtension != strExtensionPDF && strExtension != strExtensionDOCX){
						alert('El archivo imagen debe tener las siguientes\nextensiones : .doc .docx .pdf ');
						return false;
					}
					else{
						document.getElementById("txhExtCv").value = strExtension;
					}
				}
			}
			return true;
		}
		
		function fc_GrabarArchivos(){
			if (fc_Valida()){
				if (confirm('�Est� seguro de grabar?')){
					document.getElementById("txhOperacion").value="GRABAR";
					$('frmMain').submit();
				}
			}
		}
		
		function fc_VerCV(strCV){
			if(fc_Trim(strCV) != ""){
				document.getElementById("txhOperacion").value="OPEN_CV";
				$('frmMain').submit();			
			}
		}
		
		function fc_VerFoto(strCV){
			if(fc_Trim(strCV) != ""){
				document.getElementById("txhOperacion").value="OPEN_FOTO";
				$('frmMain').submit();			
			}
		}
		
		function fc_VerInfFinal()
		{
			strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%><c:out value="${objCast3.archivo}" />"
			if ( fc_Trim(strRuta) != "" ){
				window.open(strRuta + "INFORME_FINAL/" + document.getElementById("txhInfFinalGen").value);
			}
		} 
		
		function extension(filename){
			return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
		}
	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/adjuntarCV.html"  enctype="multipart/form-data" method="post">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="idRec" id="txhIdRec"/>
		<form:hidden path="extFoto" id="txhExtFoto"/>
		<form:hidden path="extCv" id="txhExtCv"/>
		<form:hidden path="infFinalGen" id="txhInfFinalGen"/>
		<form:hidden path="infFinal" id="txhInfFinal"/>
		<form:hidden path="codUsuario" id="codUsuario"/>	
<form:hidden path="foto" id="foto_nombre"/>
<form:hidden path="cv" id="cv_nombre"/>

		<table cellpadding="0" cellspacing="0" width="950px" height="36px" style="margin-left:5px" border="0">
			<tr>
				<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo.jpg" style="width:200px;height:36px" class="opc_combo">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adjuntar CV
					
				</td>
			</tr>
		</table>
		
		<table style="width:944px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr><td heigth="20px">&nbsp;</td></tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Imagen (Foto)</td>
				<td width="60%" colspan="3">:
					<input type="file" name="txtFoto" id="txtFoto" class="cajatexto_1" size="50"></td>
				<td class="texto_cap">(Medida: 100 x 50 px)</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td id="tdfoto" class="texto_cap">
					<c:if test="${control.foto!=null}">
						<a href="#" onclick="fc_VerFoto('<c:out value="${control.foto}" />');" >
						*Archivo: ${control.fotoOriginal}
						</a>
					</c:if>
					&nbsp;&nbsp;
					<form:errors path="*" cssStyle="color : red;" />${errors}
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C.V.</td>
				<td width="60%" colspan="3">:
					<input type="file" name="txtCV" id="txtCV" class="cajatexto_1" size="50">&nbsp;&nbsp;
<%-- 					<c:if test="${control.cv!=null}"> --%>
<%-- 						<img src="${ctx}/images/iconos/buscar1.jpg" style="cursor:pointer" onclick="fc_VerCV('<c:out value="${control.cv}" />');"/> --%>
<%-- 					</c:if>	 --%>
				</td>
				<td>&nbsp;</td>				
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td id="tdcv" class="texto_cap">
				&nbsp;
					<c:if test="${control.cv!=null}">
						<a href="#" onclick="fc_VerCV('<c:out value="${control.cv}" />');" >
						*Archivo: ${control.cvOriginal}
						</a>						
					</c:if>
				</td>
			</tr>
			<% if ( request.getSession().getAttribute("indRRHH") != null ) {%>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Informe Final </td>
				<td width="60%" colspan="3">:<span id="lblInfFinal" onclick="fc_VerInfFinal();" style="cursor: pointer;"></span></td>
				<td>&nbsp;</td>
			</tr>
			<%} %>
		</table>
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img id="imggrabar" src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_GrabarArchivos();" style="cursor:pointer" alt="Grabar">
					</a>		
				</td>					
			</tr>
		</table>
	</form:form>
	
	<script type="text/javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case 'OK':
					fc_Message(strMsg);
					break;
				case 'ERROR':
					fc_Message(strMsg);
					break;
			}
		}
		
		function fc_Message(strMsg){
			if (strMsg != ""){
				alert(strMsg);
			}
		}
		
		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
		
		document.getElementById("txhExtFoto").value = "";
		document.getElementById("txhExtCv").value = "";
	</script>
</body>