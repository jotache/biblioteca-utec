<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<script type="text/javascript">
		function onLoad(){}
		function fc_onChange(obj, strObjName){
			if (document.getElementById(obj.id).value != ""){
				document.getElementById(strObjName).disabled = true;
			}
			else{
				document.getElementById(strObjName).disabled = false;
			}
		}	
		function fc_ValidaFechaEstSup(strNameObj){								
			var Obj = document.all[strNameObj];
			var intEncontrado = "1234567890".indexOf(String.fromCharCode(window.event.keyCode));		
			if (intEncontrado == -1) 
				window.event.keyCode = 0;		
			
			if (document.all[strNameObj].value.length == 2)
				document.all[strNameObj].value = document.all[strNameObj].value + "/";
		}	
		function fc_limpia(obj){
			document.getElementById(obj).value='';
		}
		function fc_ValidaAnioMayor(strNameObj1, strNameObj2, valor){					
			var Obj1 = document.all[strNameObj1];
			var Obj2 = document.all[strNameObj2];			
			var strCadena1 = Obj1.value;
			var strCadena2 = Obj2.value;			
			if(fc_ValidaAnio(strNameObj1) && fc_ValidaAnio(strNameObj2)){
				if(fc_Trim(strCadena1) != "" && fc_Trim(strCadena2) != "") {
					if (fc_Trim(strCadena1) > fc_Trim(strCadena2)){
						if(valor==1){
							alert("El a�o de inicio debe ser menor o igual al a�o fin ");
							Obj1.value = "";
							Obj1.focus();
							return false;
						}
						if(valor==2){
							alert("El a�o fin debe ser mayor o igual al a�o de inicio ");
							Obj2.value = "";
							Obj2.focus();
							return false;
						}
					}
					else{
						if (fc_Trim(strCadena1) == ""){
							//var mstrValidaCampo = "Debe ingresar el valor correspondiente en el campo ";
							alert('Debe ingresar el valor correspondiente en el campo ' + "A�o Inicio");
							Obj2.value();
							Obj1.focus();
							return false;
						}
					}
				}
			}
			else{
				alert('El a�o no es v�lido');
				if(valor==1){
					Obj1.value = "";
					Obj1.focus();
					return false;
				}
				if(valor==2){
					Obj2.value = "";
					Obj2.focus();
					return false;
				}
			}
			return true;
		}
		function fc_ValidaAnio(strNameObj){
			var Obj = document.all[strNameObj];
			var strCadena = Obj.value;
			if (fc_Trim(strCadena)!=""){
				if (strCadena < 1900){
					return false;
				}
			}
			return true;
		}
		function fc_AnioAntes(obj1, obj2, valorMsg){
			var Obj1 = document.all[obj1];
			var Obj2 = document.all[obj2];

			if (Obj1.value != ""){
				if (valorMsg == 1){
					if(fc_ValidaAnio(obj1)){
						if(Obj1.value <= Obj2.value){
							alert("El a�o de inicio del Colegio 2 debe ser mayor que el a�o fin del Colegio 1");
							Obj1.value = "";
							Obj1.focus();
						}
					}
				}
				else if (valorMsg == 2){
					strAnho = Obj1.value.split("/")[1];
					
					if (Obj2.value!="") {
						if(strAnho <= Obj2.value){
							alert("El a�o de inicio ingresado debe ser mayor que el a�o fin del Colegio");									
							Obj1.focus();
						}						
					}
					else{
						var Obj3 = document.all['txtAnioFin1'];
						if(strAnho <= Obj3.value){
							alert("El a�o de inicio debe ser mayor que el a�o fin del Colegio");									
							Obj1.focus();
						}
					}
				}
				else if (valorMsg == 3){
					if(fc_Trim(Obj2.value) != ""){
						strAnho1 = Obj1.value.split("/")[1];
						strAnho2 = Obj2.value.split("/")[1];								
						if(strAnho1 < strAnho2) {
							alert("El a�o de inicio ingresado debe ser mayor que el a�o de t�rmino del Estudio 1");									
							Obj1.focus();
						}
						else if(strAnho1 == strAnho2) {
							strMes1 = Obj1.value.split("/")[0];
							strMes2 = Obj2.value.split("/")[0];
							if(strMes1 <= strMes2) {
								alert("El mes de inicio ingresado debe ser mayor que el mes de t�rmino del Estudio 1");										
								Obj1.focus();
							}
						}
					}
					else{
						alert("Para ingresar las fechas del Estudio 2, debe completar los datos del Estudio anterior");
						Obj1.value = "";
						Obj2.focus();
					}
				}
				else if (valorMsg == 4){
					if(fc_Trim(Obj2.value) != ""){							
						strAnho1 = Obj1.value.split("/")[1];
						strAnho2 = Obj2.value.split("/")[1];
						if(strAnho1 < strAnho2) {
							alert("El a�o de inicio ingresado debe ser mayor que el a�o de t�rmino del Estudio 2");									
							Obj1.focus();
						}
						else if(strAnho1 == strAnho2) {
							strMes1 = Obj1.value.split("/")[0];
							strMes2 = Obj2.value.split("/")[0];
							if(strMes1 <= strMes2) {
								alert("El mes de inicio ingresado debe ser mayor que el mes de t�rmino del Estudio 2");										
								Obj1.focus();
							}
						}
					}
					else{
						alert("Para ingresar las fechas del Estudio 3, debe completar los datos del Estudio anterior");
						Obj1.value = "";
						Obj2.focus();
					}
				}
				else if (valorMsg == 5){
					if(fc_Trim(Obj2.value) != ""){
						strAnho1 = Obj1.value.split("/")[1];
						strAnho2 = Obj2.value.split("/")[1];
						
						if(strAnho1 < strAnho2) {
							alert("El a�o de t�rmino ingresado debe ser mayor que el a�o de inicio");
							//Obj1.value = "";
							Obj1.focus();
						}
						else if(strAnho1 == strAnho2) {
							strMes1 = Obj1.value.split("/")[0];
							strMes2 = Obj2.value.split("/")[0];
							if(strMes1 < strMes2) {
								alert("El mes de t�rmino ingresado debe ser mayor que el mes de inicio");
								Obj1.focus();
							}
						}								
					}
					else{
						alert("Ingrese la fecha de inicio");
						Obj1.value = "";
						Obj2.focus();
					}
				}
			}
		}
		//Descripcion : Valida campo fecha "mm/aaaa" verificando si el mes y a�o son v�lidos		
		function fc_ValidaFechaEstSupOnBlur(strNameObj){
			var Obj = document.all[strNameObj];
			var error = false;
			if(Obj.value != ""){
				strAnho = Obj.value.split("/")[1];
				strMes = Obj.value.split("/")[0];
				if (strAnho<'1900' || strMes > 12 || strMes <= 0) error=true;				
				if (error){
					alert('Debe ingresar una fecha valida.');
					Obj.focus();                      
					return false;
				}
			}
			return true;
		}
		function fc_CambiaIdioma(obj,n){
			if (!document.getElementById(obj.id).value==""){
				var strIdioma1 = "";
				var strIdioma2 = "";
				var strIdioma3 = "";
			
				strIdioma1 = document.getElementById('cboIdioma1').value;
				strIdioma2 = document.getElementById('cboIdioma2').value;
				strIdioma3 = document.getElementById('cboIdioma3').value;
				
				switch(n){
					case '1':
						if (strIdioma1 == strIdioma2 || strIdioma1 == strIdioma3){
							fc_RegresaCombo(obj);		
						}
						break;	
					case '2':
						if (strIdioma1 == strIdioma2 || strIdioma2 == strIdioma3){
							fc_RegresaCombo(obj);				
						}
						break;
					case '3':
						if (strIdioma1 == strIdioma3 || strIdioma2 == strIdioma3){
							fc_RegresaCombo(obj);					
						}
						break;		
				}
			}
		}
		
		function fc_RegresaCombo(obj){
			alert('El idioma seleccionado ya ha sido elegido.');
			document.getElementById(obj.id).value="";
			document.getElementById(obj.id).focus();
			return;
		}
		
		function Fc_Valida(){
			if(	fc_Trim(document.getElementById('txtColegio1').value) == "" || 
				fc_Trim(document.getElementById('txtAnioInicio1').value) == "" || 
				fc_Trim(document.getElementById('txtAnioFin1').value) == ""){
					alert('Debe ingresar el valor correspondiente en el campo ' + 'Colegio 1, A�o Inicio 1 y A�o Fin 1');
					document.getElementById('txtColegio1').focus();
					return false;
			}
			if(	fc_Trim(document.getElementById('txtColegio2').value) != ""){
				if(	fc_Trim(document.getElementById('txtAnioInicio2').value) == "" || 
					fc_Trim(document.getElementById('txtAnioFin2').value) == ""){
					alert('Debe ingresar el valor correspondiente en el campo '+ 'A�o Inicio 2 y/o A�o Fin 2');
					document.getElementById('txtAnioInicio2').focus();
					return false;
				}
			}
			else{
				alert('Existen datos en la secci�n estudios secundarios que no se guardar�n.');
				document.getElementById('txtAnioInicio2').value = "";
				document.getElementById('txtAnioFin2').value = "";
			}		
			return true;
		}
		
		function fc_GrabarEstudios(){
			if (Fc_Valida() && fc_GetCadenaIdiomas() && fc_GetCadenaEstSup()){
				if (confirm('�Est� seguro de grabar?')){
					document.getElementById('txhOperacion').value = 'GRABAR';
					$('frmMain').submit();
				}
			}
		}
		
		function fc_GetCadenaIdiomas(){
			var NumFilas = 3;
			var cont = 0;
			document.getElementById('txhCadenaIdioma').value = "";
			for (i=1;i<=NumFilas;i++){
				if(document.getElementById('cboIdioma'+i).value != ""){
					if(document.getElementById('cboNivel'+i).value == ""){
						alert('Debe ingresar el valor correspondiente en el campo ' + 'Nivel del Idioma ' + i);
						return false;
					}
					if(document.getElementById('cboGrado'+i).value == ""){
						alert('Debe ingresar el valor correspondiente en el campo ' + 'Grado del Idioma ' + i);
						return false;
					}
					
					document.getElementById('txhCadenaIdioma').value =
						document.getElementById('txhCadenaIdioma').value +
						document.getElementById('cboIdioma'+i).value + "$" +
						document.getElementById('cboNivel'+i).value + "$" +
						document.getElementById('cboGrado'+i).value + "$" +
						document.getElementById('txhCodEstudioIdioma'+i).value + "|";
					cont++;
				}
			}
			document.getElementById('txhNnroRegIdioma').value = cont;
			return true;
		}
		
		function fc_GetCadenaEstSup(){
			var NumFilas = 3;
			var cont = 0;
			var fecIni = "";
			var fecFin = "";
			var contador = 0;
			
			document.getElementById('txhCadenaEstSup').value = "";
			for (i=1;i<=NumFilas;i++){
				if(document.getElementById('cboGradoAcad'+i).value != ""){
					if(	document.getElementById('cboAreaEstudio'+i).value == ""){
						alert('Debe ingresar el valor correspondiente en el campo ' + '\n�rea de estudio del estudio superior ' + i);
						document.getElementById('cboAreaEstudio'+i).focus();
						return false;
					}
					if(	document.getElementById('cboInstitucion'+i).value == "" &&
						fc_Trim(document.getElementById('txtOtraInstitucion'+i).value) == ""){
						alert('Debe ingresar el valor correspondiente en el campo ' + '\nInstituci�n del estudio superior ' + i);
						document.getElementById('cboInstitucion'+i).focus();
						return false;
					}
					if(i==1){
						if(document.getElementById('txtCiclo'+i).value == ""){
							alert('Debe ingresar el valor correspondiente en el campo ' + 'Ciclo del estudio superior ' + i);
							document.getElementById('txtCiclo'+i).focus();
							return false;
						}
						if(document.getElementById('cboMerito'+i).value == ""){
							alert('Debe ingresar el valor correspondiente en el campo ' + 'M�rito del estudio superior ' + i);
							document.getElementById('cboMerito'+i).focus();
							return false;
						}
					}
					if(document.getElementById('txtInicio'+i).value == ""){
						alert('Debe ingresar el valor correspondiente en el campo ' + 'Inicio del estudio superior ' + i);
						document.getElementById('txtInicio'+i).focus();
						return false;
					}									
					
					fecIni = fc_Trim(document.getElementById('txtInicio'+i).value);
					fecFin = fc_Trim(document.getElementById('txtTermino'+i).value);
					
					fecIni = fecIni.replace("/","");
					fecFin = fecFin.replace("/","");
					
					document.getElementById('txhCadenaEstSup').value = 
						document.getElementById('txhCadenaEstSup').value +
						document.getElementById('cboGradoAcad'+i).value + "$" +
						document.getElementById('cboAreaEstudio'+i).value + "$" +
						document.getElementById('cboInstitucion'+i).value + "$" +
						fc_Trim(document.getElementById('txtOtraInstitucion'+i).value) + "$" +
						fc_Trim(document.getElementById('txtCiclo'+i).value) + "$" +
						document.getElementById('cboMerito'+i).value + "$" +
						fecIni + "$" +
						fecFin + "$" +
						document.getElementById('txhCodEstudioSup'+i).value + "|";
					cont++;
				}
				else{
					if(i==1){
						alert('Debe ingresar el valor correspondiente en el campo ' + 'Grado acad�mico del estudio superior ' + i);
						document.getElementById('cboGradoAcad'+i).focus();
						return false;
					}
					else {
						if(contador == 0){
							if( document.getElementById('cboInstitucion'+i).value != "" ||
								fc_Trim(document.getElementById('txtOtraInstitucion'+i).value) != "" || 
								document.getElementById('txtCiclo'+i).value != "" ||
								document.getElementById('cboMerito'+i).value != "" ||
								document.getElementById('txtInicio'+i).value != "" ||
								document.getElementById('txtTermino'+i).value != ""){
								contador ++;
								if (!confirm('Algunos datos no ser�n tomados en cuenta en la grabaci�n \npues los campos del Estudio 2 y/o Estudio 3 no est�n completos.\nDesea continuar?')){
									return false;
								}
							}
						}
					}
					
				}
			}
			document.getElementById('txhNroRegEstSup').value = cont;
			return true;
		}
		
		function fc_ValidarFechas(obj, v1, v2, v3){
			//if (fc_ValidaFechaEstSupOnBlur(obj)){
				//fc_AnioAntes(v1, v2, v3);
			//}
			//else return;
			if (fc_ValidaFechaEstSupOnBlur(obj)){
				return true;
			}
			return false;
		}
	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/estudios.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="idRec" id="txhIdRec"/>
		
		<form:hidden path="cadenaIdioma" id="txhCadenaIdioma"/>
		<form:hidden path="nroRegIdioma" id="txhNnroRegIdioma"/>
		<form:hidden path="codEstudioIdioma1" id="txhCodEstudioIdioma1"/>
		<form:hidden path="codEstudioIdioma2" id="txhCodEstudioIdioma2"/>
		<form:hidden path="codEstudioIdioma3" id="txhCodEstudioIdioma3"/>		
		
		<form:hidden path="cadenaEstSup" id="txhCadenaEstSup"/>
		<form:hidden path="nroRegEstSup" id="txhNroRegEstSup"/>
		<form:hidden path="codEstudioSup1" id="txhCodEstudioSup1"/>
		<form:hidden path="codEstudioSup2" id="txhCodEstudioSup2"/>
		<form:hidden path="codEstudioSup3" id="txhCodEstudioSup3"/>
		<form:hidden path="indMenu" id="txhIndMenu"/>
		<form:hidden path="codUsuario" id="codUsuario"/>
		
<!--		<div style="overflow: auto; height: 455px; width:975px">-->
		<div style="height: 450px; width: 100%; border: 0px; border-color: red;align:center;">
		
		<table cellpadding="0" cellspacing="0" width="950px" height="36px" style="margin-left:7px" border="0">
			<tr>
				<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo.jpg" style="width:455px;height:36px" class="opc_combo">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estudios Secundarios
				</td>
			</tr>
		</table>
		<table style="width:945px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr><td height="7px"></td></tr>
			<tr>
				<td width="20%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Colegio 1 :</td>
				<td width="20%"><form:input path="colegio1" id="txtColegio1" cssClass="cajatexto_o" size="40" onkeypress="javascript:fc_ValidaTextoEspecialAndNumeroTodo();"/></td>
				<td>&nbsp;</td>
				<td width="20%" class="">A�o Inicio : 
				     <form:input path="anioInicio1" id="txtAnioInicio1" cssClass="cajatexto_o" size="10" 
				       onkeypress="javascript:fc_PermiteNumeros();" maxlength="4" 
				       onblur="javascript:fc_ValidaAnioMayor('txtAnioInicio1','txtAnioFin1',1);"/>
				</td>
				<td width="20%" class="">A�o Fin : 
				<form:input path="anioFin1" id="txtAnioFin1" cssClass="cajatexto_o" size="10" 
				       onkeypress="javascript:fc_PermiteNumeros();" maxlength="4" 
				       onblur="javascript:fc_ValidaAnioMayor('txtAnioInicio1','txtAnioFin1',2);"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Colegio 2 :</td>
				<td width="20%"><form:input path="colegio2" id="txtColegio2" cssClass="cajatexto" size="40" onkeypress="javascript:fc_ValidaTextoEspecialAndNumeroTodo();"/></td>
				<td>&nbsp;</td>
				<td width="20%" class="">A�o Inicio : <form:input path="anioInicio2" id="txtAnioInicio2" cssClass="cajatexto" size="10" onkeypress="javascript:fc_PermiteNumeros();" maxlength="4" onblur="javascript:fc_AnioAntes('txtAnioInicio2','txtAnioFin1',1);fc_ValidaAnioMayor('txtAnioInicio2','txtAnioFin2',1);"/></td>
				<td width="20%" class="">A�o Fin : <form:input path="anioFin2" id="txtAnioFin2" cssClass="cajatexto" size="10" onkeypress="javascript:fc_PermiteNumeros();" maxlength="4" onblur="javascript:fc_ValidaAnioMayor('txtAnioInicio2','txtAnioFin2',2);"/></td>
			</tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="950px" height="36px" style="margin-left:7px" border="0">
			<tr>
				<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo.jpg" style="width:500px;height:36px" class="opc_combo">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Idiomas
				</td>
			</tr>
		</table>
		<table style="width:945px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr><td height="7px"></td></tr>
			<tr>
				<td class="" width="15%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Idioma 1 : </td> 
				<td  width="15%">
					<form:select path="idioma1" id="cboIdioma1" cssClass="cajatexto" cssStyle="width:115px" onchange="javascript:fc_CambiaIdioma(this,'1');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaIdioma1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaIdioma1}" />
						</c:if>
					</form:select>
				</td>
				<td width="15%" class="">Nivel :  </td>
				<td  width="15%">
					<form:select path="nivel1" id="cboNivel1" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaNivel1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaNivel1}" />
						</c:if>
					</form:select>
				</td>
				<td width="15%" class="" align="center">Habilidad :  </td>
				<td  width="15%">
					<form:select path="grado1" id="cboGrado1" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaGrado1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaGrado1}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td class="" width="15%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Idioma 2 : </td> 
				<td  width="15%">
					<form:select path="idioma2" id="cboIdioma2" cssClass="cajatexto" cssStyle="width:115px" onchange="javascript:fc_CambiaIdioma(this,'2');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaIdioma2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaIdioma2}" />
						</c:if>
					</form:select>
				</td>
				<td width="15%" class="">Nivel :  </td>
				<td  width="15%">
					<form:select path="nivel2" id="cboNivel2" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaNivel2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaNivel2}" />
						</c:if>
					</form:select>
				</td>
				<td width="15%" class="" align="center">Habilidad :  </td>
				<td  width="15%">
					<form:select path="grado2" id="cboGrado2" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaGrado2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaGrado2}" />
						</c:if>
					</form:select>	
			</td>
			</tr>
			<tr>
				<td class="" width="15%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Idioma 3 : </td> 
				<td  width="15%">
					<form:select path="idioma3" id="cboIdioma3" cssClass="cajatexto" cssStyle="width:115px" onchange="javascript:fc_CambiaIdioma(this,'3');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaIdioma3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaIdioma3}" />
						</c:if>
					</form:select>
				</td>
				<td width="15%" class="">Nivel :  </td>
				<td  width="15%">
					<form:select path="nivel3" id="cboNivel3" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaNivel3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaNivel3}" />
						</c:if>
					</form:select>
				</td>
				<td width="15%" class="" align="center">Habilidad :  </td>
				<td  width="15%">
					<form:select path="grado3" id="cboGrado3" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaGrado3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaGrado3}" />
						</c:if>
					</form:select>
				</td>
			</tr>
		</table>
		<br>
		
		<table cellpadding="0" cellspacing="0" width="950px" height="36px" style="margin-left:7px" border="0">
			<tr>
				<td background="${ctx}/images/reclutamiento/Reg_Dat/titulo.jpg" style="width:700px;height:36px" class="opc_combo">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estudios Superiores (Ingrese los datos desde el m�s reciente al m�s antiguo)
				</td>
			</tr>
		</table>
		<table style="width:945px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr><td height="7px"></td></tr>
			<tr>
				<td class="texto_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estudio 1<br><br>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nivel de Estudio :</td>
				<td width="52" colspan="4">
					<form:select path="gradoAcad1" id="cboGradoAcad1" cssClass="cajatexto_o" cssStyle="width:475px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaGradoAcad1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaGradoAcad1}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&Aacute;rea de Estudio :</td>
				<td width="52%" colspan="4">
					<form:select path="areaEstudio1" id="cboAreaEstudio1" cssClass="cajatexto_o" cssStyle="width:475px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaAreaEstudio1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaAreaEstudio1}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instituci&oacute;n :</td>
				<td width="52%" colspan="4">
					<form:select path="institucion1" id="cboInstitucion1" cssClass="cajatexto_o" cssStyle="width:475px" 
					       onchange="javascript:fc_onChange(this, 'txtOtraInstitucion1');fc_limpia('txtOtraInstitucion1')">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaInstitucion1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaInstitucion1}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Otra Instituci&oacute;n :</td>
				<td width="52%" colspan="4"><form:input path="otraInstitucion1" id="txtOtraInstitucion1" cssClass="cajatexto" size="89" onkeypress="javascript:fc_ValidaTextoNumeroEspecial();"/></td>
			</tr>
			<tr>
			  <td class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ciclos Estudiados :</td>
			  <td width="9%"><form:input path="ciclo1" id="txtCiclo1" cssClass="cajatexto_o" size="5" onkeypress="javascript:fc_PermiteNumeros();" maxlength="2"/></td>
			  <td width="8%">M&eacute;rito :</td>
			  <td colspan="1" width="15%"><form:select path="merito1" id="cboMerito1" cssClass="cajatexto_o" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaMerito1!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaMerito1}" />
						</c:if>
					</form:select></td>
			  <td width="20%">Inicio (mm/aaaa) : <form:input path="inicio1" id="txtInicio1" cssClass="cajatexto_o" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtInicio1','txtAnioFin2',2);"/></td>
			  <td width="23%">T&eacute;rmino (mm/aaaa) : <form:input path="termino1" id="txtTermino1" cssClass="cajatexto_o" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtTermino1','txtInicio1',5);"/></td>
		  </tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td width="9%"></td>
				<td class="" width="8%">&nbsp;</td>
				<td colspan="1" width="15%">
				</td>
				<td class="" width="20%"></td>
				<td class=""><label style="font-size:8px; ">(Si es actual dejar en blanco)</label></td>
			</tr>
		</table>
		<br>
		<table style="width:945px;margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr><td height="7px"></td></tr>
			<tr>
				<td class="texto_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estudio 2<br><br>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nivel de Estudio :</td>
				<td width="52" colspan="4">
					<form:select path="gradoAcad2" id="cboGradoAcad2" cssClass="cajatexto" cssStyle="width:475px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaGradoAcad2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaGradoAcad2}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&Aacute;rea de Estudio :</td>
				<td width="52%" colspan="4">
					<form:select path="areaEstudio2" id="cboAreaEstudio2" cssClass="cajatexto" cssStyle="width:475px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaAreaEstudio2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaAreaEstudio2}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instituci&oacute;n :</td>
				<td width="52%" colspan="4">
					<form:select path="institucion2" id="cboInstitucion2" cssClass="cajatexto" cssStyle="width:475px" onchange="javascript:fc_onChange(this, 'txtOtraInstitucion2');fc_limpia('txtOtraInstitucion2');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaInstitucion2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaInstitucion2}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Otra Instituci&oacute;n :</td>
				<td width="52%" colspan="4"><form:input path="otraInstitucion2" id="txtOtraInstitucion2" cssClass="cajatexto" size="89" onkeypress="javascript:fc_ValidaTextoNumeroEspecial();"/></td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ciclos Estudiados :</td>
				<td width="9%"><form:input path="ciclo2" id="txtCiclo2" cssClass="cajatexto" size="5" onkeypress="javascript:fc_PermiteNumeros();" maxlength="2"/></td>
				<td class="" width="8%">M&eacute;rito :</td>
				<td colspan="1" width="15%">
					<form:select path="merito2" id="cboMerito2" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaMerito2!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaMerito2}" />
						</c:if>
					</form:select>
				</td>
			<!-- JCMU 28/10/2008	<td class="" width="20%">Inicio (mm/aaaa) : <form:input path="inicio2" id="txtInicio2" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtInicio2','txtTermino1',3);"/></td>
				<td class="" width="23%">T&eacute;rmino (mm/aaaa) : <form:input path="termino2" id="txtTermino2" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtTermino2','txtInicio2',5);"/></td>   -->
				<td class="" width="20%">Inicio (mm/aaaa) : <form:input path="inicio2" id="txtInicio2" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtInicio2','txtTermino1',3);"/></td>
				<td class="" width="23%">T&eacute;rmino (mm/aaaa) : <form:input path="termino2" id="txtTermino2" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtTermino2','txtInicio2',5);"/></td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td width="9%"></td>
				<td class="" width="8%">&nbsp;</td>
				<td colspan="1" width="15%">
				</td>
				<td class="" width="20%"></td>
				<td class=""><label style="font-size:8px; ">(Si es actual dejar en blanco)</label></td>
			</tr>
		</table>
		<br>
		<table style="width:945px; margin-left:9px" border="0" cellspacing="0" cellpadding="5" class="tabla_1" bordercolor="red">
			<tr><td height="7px"></td></tr>
			<tr>
				<td class="texto_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estudio 3<br><br>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nivel de Estudio :</td>
				<td width="52" colspan="4">
					<form:select path="gradoAcad3" id="cboGradoAcad3" cssClass="cajatexto" cssStyle="width:475px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaGradoAcad3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaGradoAcad1}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&Aacute;rea de Estudio :</td>
				<td width="52%" colspan="4">
					<form:select path="areaEstudio3" id="cboAreaEstudio3" cssClass="cajatexto" cssStyle="width:475px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaAreaEstudio3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaAreaEstudio3}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instituci&oacute;n :</td>
				<td width="52%" colspan="4">
					<form:select path="institucion3" id="cboInstitucion3" cssClass="cajatexto" cssStyle="width:475px" onchange="javascript:fc_onChange(this, 'txtOtraInstitucion3');fc_limpia('txtOtraInstitucion3');">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaInstitucion3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaInstitucion3}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Otra Instituci&oacute;n :</td>
				<td width="52%" colspan="4"><form:input path="otraInstitucion3" id="txtOtraInstitucion3" cssClass="cajatexto" size="89" onkeypress="javascript:fc_ValidaTextoNumeroEspecial();"/></td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ciclos Estudiados :</td>
				<td width="9%"><form:input path="ciclo3" id="txtCiclo3" cssClass="cajatexto" size="5" onkeypress="javascript:fc_PermiteNumeros();" maxlength="2"/></td>
				<td class="" width="8%">M&eacute;rito :</td>
				<td colspan="1" width="15%">
					<form:select path="merito3" id="cboMerito3" cssClass="cajatexto" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaMerito3!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaMerito3}" />
						</c:if>
					</form:select>
				</td>
			<!-- JCMU 28/10/2008	<td class="" width="20%">Inicio (mm/aaaa) : <form:input path="inicio3" id="txtInicio3" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtInicio3','txtTermino2',4);"/></td>
				<td class="" width="23%">T&eacute;rmino (mm/aaaa) : <form:input path="termino3" id="txtTermino3" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" onblur="javascript:fc_ValidarFechas(this.id, 'txtTermino3','txtInicio3',5);"/></td>   -->
				<td class="" width="20%">Inicio (mm/aaaa) : <form:input path="inicio3" id="txtInicio3" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7"  /></td>
				<td class="" width="23%">T&eacute;rmino (mm/aaaa) : <form:input path="termino3" id="txtTermino3" cssClass="cajatexto" size="8" onkeypress="javascript:fc_ValidaFechaEstSup(this.id);" maxlength="7" /></td>
			</tr>
			<tr>
				<td width="25%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td width="9%"></td>
				<td class="" width="8%">&nbsp;</td>
				<td colspan="1" width="15%">
				</td>
				<td class="" width="20%"></td>
				<td class=""><label style="font-size:8px; ">(Si es actual dejar en blanco)</label></td>
			</tr>
		</table>
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img id="imggrabar" src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_GrabarEstudios();" style="cursor:pointer" alt="Grabar">
					</a>
			</tr>
		</table>
		</div>
	</form:form>
	
	<script type="text/javascript" language="javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case 'ERROR':
					fc_Message(strMsg);
					break;
				case 'OK':
					/*Si el usuario es nuevo.*/
					if ( document.getElementById("txhIndMenu").value != "" )
					{
						alert(strMsg + " Es necesario ingresar la informaci�n de Experiencia Laboral.");
						window.parent.location.href = "${ctx}/reclutamiento/cuerpo_registro.html?txhIdRec=" + document.getElementById('txhIdRec').value + 
													  "&txhOperacion=ACCESO&txhIndMenu=3&txhCodUsuario=" + document.getElementById('codUsuario').value;
						  //
					}
					else
					{
						fc_Message(strMsg);
					}
					break;
			}
		}
		
		function fc_Message(strMsg){
			if (strMsg != ""){
				alert(strMsg);
			}
		}
		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
	</script>
</body>