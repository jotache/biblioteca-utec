<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script type="text/javascript">
		function onLoad(){					
		}

		document.observe("dom:loaded", function() {		
			if($('txhOpMenu').value!='')			
				fc_Cambia($('txhOpMenu').value);			

			$('txhOpMenu').value = '';
		});
			
		function fc_CerrarSesion(){
			if ( confirm('�Est� seguro de cerrar sesi�nn?') ){			
				<% 
					request.getSession().removeAttribute("postUsuario");
					request.getSession().removeAttribute("postUsuario2");			
				%>				
				top.location.href = "${ctx}/logueoPostulante.html";
			}
		}		
		function fc_Cambia(obj){
			var valor = "<%=(((String)request.getAttribute("idRec")==null)?"":request.getAttribute("idRec"))%>";
			if(fc_Trim(valor) != ""){
				switch(obj){
					case '1':
						parent.Body.location.href = "${ctx}/reclutamiento/datos_personales.html<%=(String)request.getAttribute("lstrURL")%>";
						break;
					case '2':
						parent.Body.location.href = "${ctx}/reclutamiento/estudios.html<%=(String)request.getAttribute("lstrURL")%>";
						break;
					case '3':
						parent.Body.location.href = "${ctx}/reclutamiento/experiencia.html<%=(String)request.getAttribute("lstrURL")%>";
						break;
					case '4':
						parent.Body.location.href = "${ctx}/reclutamiento/adjuntarCV.html<%=(String)request.getAttribute("lstrURL")%>";
						break;
					case '5':
						parent.Body.location.href = "${ctx}/reclutamiento/lista_oferta.html<%=(String)request.getAttribute("lstrURL")%>";
						break;
				}
			}
			else{
				if (obj != '1'){
					alert('Debe registrar sus datos personales, para acceder a las dem�s secciones.');
					return false;
				}
			}
		}
	</script>
</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/menu_registro.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="opeMenu" id="txhOpMenu"/>		
		<table border="0" bordercolor="white" align="center" cellspacing="0" cellpadding="0" style="width:945px;left:20px;top:74px">
			<tr>	
				<td align="right" style="">
					<%-- 
					<c:if test="${control.codUsuario==''}">
					--%>
						<img alt="Cerrar Sesi�n" src="${ctx}/images/iconos/limpiar1.jpg" onclick="fc_CerrarSesion();"
							style="cursor: pointer;"/>
						<label onclick="fc_CerrarSesion();" style="cursor:pointer; color: #048BBB; vertical-align: top;font-family: verdana;font-size:12px">Cerrar Sesi&oacute;n</label>
					<%-- 
					</c:if>
					--%> 
				</td>
			</tr>	
		</table>
		<%-- 
		<table class="grilla" style="width:940px;margin-left:8px" cellSpacing="0" cellPadding="0" border="0" bordercolor="cyan" ID="Table2" align="center"> 
			<tr>
				<td valign='top' width="80%">
					<table class="grilla"  cellSpacing="2" cellPadding="1" border="0" bordercolor='black' height="23px" align=center width="100%" ID="Table1"> 
						<tr>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
							<td width="10%" style="cursor:hand" onclick="javascript:fc_Cambia('5');"><b>Listado de Ofertas</b></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		--%>
		<table style="width:940px;margin-left:8px;background-color:#7B0F1E; " cellSpacing="0" cellPadding="0" border="0" bordercolor='gray' align="center"> 
			<tr>
				<td valign='top' colspan="2">
					<table class=""  cellSpacing="2" cellPadding="1" border="0" bordercolor='red' height="23px" align=center width="100%"> 
						<tr>
							<td width="20%" style="cursor:hand" onclick="javascript:fc_Cambia('1');" ><b><span style="font-family: Tahoma,Arial; color: white; font-size: 12px">Datos Personales</span></b></td>
							<td width="20%" style="cursor:hand" onclick="javascript:fc_Cambia('2');" ><b><span style="font-family: Tahoma,Arial; color: white; font-size: 12px">Estudios</span></b></td>
							<td width="20%" style="cursor:hand" onclick="javascript:fc_Cambia('3');" ><b><span style="font-family: Tahoma,Arial; color: white; font-size: 12px">Experiencia Laboral</span></b></td>
							<td width="20%" style="cursor:hand" onclick="javascript:fc_Cambia('4');" ><b><span style="font-family: Tahoma,Arial; color: white; font-size: 12px">Archivos  Adjuntos</span></b></td>
							<td width="20%" style="cursor:hand" onclick="javascript:fc_Cambia('5');" ><b><span style="font-family: Tahoma,Arial; color: yellow; font-size: 12px;">Ofertas de Empleo</span></b></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form:form>
</body>