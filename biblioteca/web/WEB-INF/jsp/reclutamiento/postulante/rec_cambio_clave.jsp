<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>	
	<script type="text/javascript">
		function onLoad(){}				
		function fc_CambiaClave(){
			if(trim(document.frmMain.txtUsuario.value)==''){
				alert('Ingrese su usuario');
				document.frmMain.txtUsuario.focus();
				return false;
			}
			if (document.getElementById("txtNuevaClave").value == document.getElementById("txtConfirmNuevaClave").value){
				document.getElementById('txhOperacion').value = "CHANGE";
				document.getElementById("frmMain").submit();
			}
			else {
				alert('La confirmación de la contraseña no coincide.');
				document.getElementById("txtNuevaClave").value = "";
				document.getElementById("txtConfirmNuevaClave").value = "";
				document.getElementById("txtNuevaClave").focus();
				return;
			}
		}
		function fc_ValidaTextoNum() {		    
		     if((window.event.keyCode != 209) && (window.event.keyCode != 241)){            
		       	var ch_Caracter = String.fromCharCode(window.event.keyCode);
				var intEncontrado = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(ch_Caracter);            
				if (intEncontrado == -1)
					window.event.keyCode = 0;
				else
					window.event.keyCode = ch_Caracter.charCodeAt(); 
			}
		}
	</script>
</head>
<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/cambio_clave.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<table cellspacing="0" cellpadding="0" border="0" style="width:99%; margin-left: 10px; margin-top: 10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">Cambio de Clave</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<TABLE style="width:93%;margin-top:5px" class="tabla" cellspacing="2" cellpadding="2" border="0" align="center" bordercolor="blue">
			<TR>
				<TD class="texto_bold">Correo:</TD>
				<TD><form:input path="usuario" id="txtUsuario" cssClass="cajatexto" size="25" onblur="javascript:fc_ValidaTextoGeneralEmail(this.id,'');"/></TD>
			</TR>		
			<TR>
				<TD class="texto_bold">Clave Actual:</TD>
				<td><form:password path="clave" id="txtClaveActual" cssClass="cajatexto" size="25" maxlength="" onkeypress="javascript:fc_ValidaTextoNum();"/></td>
			</TR>
			<TR>
				<TD class="texto_bold" valign="top">Nueva Clave :</TD>
				<TD><form:password path="claveNueva" id="txtNuevaClave" cssClass="cajatexto" size="25" onkeypress="javascript:fc_ValidaTextoNum();" onblur="javascript:fc_CantCaracter(this,6,'Clave');"/></TD>
			</tr>
			<TR>
				<TD class="texto_bold" valign="top">Confirmar Clave :</TD>
				<TD><form:password path="claveNueva1" id="txtConfirmNuevaClave" cssClass="cajatexto" size="25" onkeypress="javascript:fc_ValidaTextoNum();" onblur="javascript:fc_CantCaracter(this,6,'Clave');"/></TD>
			</tr>
		</table>
		<br>
		<table align="center">
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_CambiaClave();"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
				</td>	
			</tr>
		</table>
	</form:form>
	
	<script type="text/javascript" language="javascript">
		if (document.getElementById("txhTypeMessage").value != ""){
		strMsg = document.getElementById("txhMessage").value;
			switch(document.getElementById("txhTypeMessage").value){
				case '1':
					fc_Message(strMsg);
					fc_Limpia();
					break;
				case '2':
					fc_Message(strMsg);
					document.getElementById("txtUsuario").value = "";
					fc_Limpia();
					break;
				case '0':
					fc_Message(strMsg);
					window.close();
					break;
				case 'ERROR':
					fc_Message(strMsg);
					window.close();
					break;
			}
		}
		
		function fc_Message(strMsg){
			if (strMsg != ""){
				alert(strMsg);
			}
		}
		
		function fc_Limpia(){
			document.getElementById("txtNuevaClave").value = "";
			document.getElementById("txtConfirmNuevaClave").value = "";
		}
		document.getElementById("txhTypeMessage").value = "";
		document.getElementById("txhMessage").value = "";
	</script>
	
</body>