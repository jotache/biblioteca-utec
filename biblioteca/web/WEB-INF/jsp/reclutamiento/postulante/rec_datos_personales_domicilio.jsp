<%@ include file="/taglibs.jsp"%>
<head>
	<script type="text/javascript" type="text/javascript">
		function onLoad(){			
		}			
		function fc_onChange(obj, strValor){
			var elemento = $(obj.id);
			if (elemento.value != ""){
				$('txhOperacion').value = strValor;				
				$('frmMain').submit();
			}
		}
		function copyParametros(){
		 	parent.document.getElementById("cboDepartamento").value = $F('cboDepartamento'); //document.getElementById("cboDepartamento").value;
		 	parent.document.getElementById("cboProvincia").value = $F('cboProvincia'); //document.getElementById("cboProvincia").value;
		 	parent.document.getElementById("cboDistrito").value = $F('cboDistrito'); //document.getElementById("cboDistrito").value;
		}		
	</script>
</head>
<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/datos_personales_domicilio.html" >
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="typeMessage" id="txhTypeMessage"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="nroRegAreaInt" id="txhNroRegAreaInt"/>
		<form:hidden path="idRec" id="txhIdRec"/>
		<form:hidden path="codArea" id="txhCodArea"/>
		<form:hidden path="cadenaAI" id="txhCadenaAI"/>
		
		<form:hidden path="sexo" id="txhIndSexo"/>
		<form:hidden path="nacionalidad" id="txhIndNacionalidad"/>
		<form:hidden path="postuladoAntes" id="txhIndPostulado"/>
		<form:hidden path="trabajadoAntes" id="txhIndTrabajado"/>
		<form:hidden path="familiaTecsup" id="txhIndFamiliares"/>
		<form:hidden path="expDocente" id="txhIndExperiencia"/>
		<form:hidden path="dispoViaje" id="txhIndDispoViaje"/>
		
		<table style="width:100%;margin-left:0px;font-family: Arial;" border="0" cellspacing="3" cellpadding="0" bgcolor="#f2f2f2">
				<!-- 
				<tr><td height="20px">&nbsp;</td></tr>
				<tr>
					<td width="20%" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Domicilio :</td>					
				</tr>
				-->
				<tr height="20px" valign="top">
					<td width="150px" style="font-size:12px;" height="20px">DEPARTAMENTO :</td>
					<td width="200px">
						<form:select path="departamento" id="cboDepartamento" cssClass="cajatexto" 
							cssStyle="width:150px" onchange="javascript:fc_onChange(this, 'PROV');">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listaDepartamento!=null}">
							<form:options itemValue="codigo" itemLabel="dscValor1" 
								items="${control.listaDepartamento}" />
							</c:if>
						</form:select>
					</td>					
				</tr>
				<tr height="20px">
					<td style="font-size:12px;" height="20px">PROVINCIA :</td>
					<td >
						<form:select path="provincia" id="cboProvincia" cssClass="cajatexto" cssStyle="width:150px" 
						onchange="javascript:fc_onChange(this, 'DIST');">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listaProvincia!=null}">
							<form:options itemValue="codigo" itemLabel="dscValor1" 
								items="${control.listaProvincia}" />
							</c:if>
						</form:select>
					</td>					
				</tr>
				<tr height="20px">
					<td style="font-size:12px;" height="20px">DISTRITO :</td>
					<td >
						<form:select path="distrito" id="cboDistrito" cssClass="cajatexto" 
							cssStyle="width:150px">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listaDistrito!=null}">
							<form:options itemValue="codigo" itemLabel="dscValor1" 
								items="${control.listaDistrito}" />
							</c:if>
						</form:select>
					</td>					
				</tr>				
			</table>
	</form:form>
</body>