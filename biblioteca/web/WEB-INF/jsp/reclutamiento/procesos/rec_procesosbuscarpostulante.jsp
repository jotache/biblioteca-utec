<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	function fc_AddAreaInteres(){	
		Fc_Popup("${ctx}/reclutamiento/buscar_areainteres.html",400,320,"");		
	} 
	
	function fc_AddDetalleAreaInteres(codNivel2, dscNivel2, dscNivel1){	
		objCboAreas = document.getElementById("cboAreasInteres");
		objCodigos = document.getElementById("txhCodAreasInteres");
		if ( objCodigos.value.indexOf(codNivel2) > -1 ){				
			return false;
		}
		var oOption = document.createElement("OPTION");
		objCboAreas.options.add(oOption);
		oOption.value = codNivel2;
		oOption.innerText = dscNivel2;
		objCodigos.value = objCodigos.value + codNivel2 + '|';
		return true;
	}
	
	function fc_Buscar(){
		document.getElementById("txhAccion").value = "BUSCAR";
		document.getElementById("frmMain").submit();
	}
	
	function onLoad(){}
	
	function fc_seleccionarRegistro(codPostulante){	
		strCodSel = document.getElementById("txhCodPostSels").value;
		flag=false;
		if ( strCodSel != '' ){
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";
			for (i=0; i <= ArrCodSel.length-2 ; i++){
				if (ArrCodSel[i] == codPostulante){ flag = true }
				else strCodSel = strCodSel + ArrCodSel[i]+'|';
			}
		}
		if (!flag){
			strCodSel = strCodSel + codPostulante + '|';
		}
		document.getElementById("txhCodPostSels").value = strCodSel;
	}
	
	function fc_Grabar(){
		if ( document.getElementById("txhCodPostSels").value == "" ){
			alert('Debe seleccionar un registro');
			return ;
		}		
		if (!confirm('�Est� seguro de grabar?')) return false;
		document.getElementById("txhAccion").value = "GRABAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_VerHistorial(srtCodPostulante){		
		document.getElementById("txhAccion").value = "VER_DATOS";
		document.getElementById("txhMessage").value = srtCodPostulante;
		document.getElementById("frmMain").submit();
	}
	
	function fc_HojaVida(strCodPostulante){
		Fc_PopupScroll("${ctx}/reclutamiento/hoja_vida.html?txhCodPostulante=" + strCodPostulante,865,500);
	}
</script>
</head>
<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/buscar_postulantes.html" >
		<form:hidden path="codUsuario" id="txhCodUsuario"/>
		<form:hidden path="operacion" id="txhAccion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="codAreasInteres" id="txhCodAreasInteres"/>
		<form:hidden path="codPostulantesSeleccioandos" id="txhCodPostSels"/>
		<form:hidden path="codProceso" id="txhCodProceso"/>
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px; margin-top: 5px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">B�squeda de Postulantes</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="1" class="tabla" 
			style="margin-left: 10px; margin-right: 5px; width: 96%; margin-top: 10px">			
			<tr>
				<td>Nombres:</td>
				<td><form:input id="txtNombres" path="nombres" cssClass="cajatexto_1" cssStyle="width:170px"/></td>
				<td>Apellidos:</td>
				<td><form:input id="txtApellidos" path="apellidos"  cssClass="cajatexto_1" cssStyle="width:170px"/></td>
			</tr>
			<tr>
				<td >Area de Interes:</td>
				<td>
					<form:select path="strAreaInteres" id="cboAreasInteres" cssClass="cajatexto" 
						cssStyle="width:173px" multiple="true">
						<c:if test="${control.listAreasInteres!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listAreasInteres}" />
						</c:if>
					</form:select>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar1','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img alt="Buscar" src="${ctx}/images/iconos/buscar1.jpg" id="imgbuscar1" 
					style="cursor:pointer;" onclick="javascript:fc_AddAreaInteres();" align="center"></a>					
				</td>
				<td >Profesi�n:</td>
				<td>
					<form:select path="codProfesion" id="cboProfesion" cssClass="cajatexto" cssStyle="width:170px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listProfesion!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listProfesion}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="15%">Egresado de:</td>
				<td>
					<form:select path="codEgresado" id="cboEgresado" cssClass="cajatexto" cssStyle="width:170px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listEgresado!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listEgresado}" />
						</c:if>
					</form:select>
				</td>
				<td width="15%">Grado Acad�mico:</td>
				<td>
					<form:select path="codGrado" id="cboGrado" cssClass="cajatexto" cssStyle="width:170px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listGrado!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listGrado}" />
						</c:if>
					</form:select>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img alt="Buscar" src="${ctx}/images/iconos/buscar1.jpg" id="imgbuscar" 
					style="cursor:pointer;" onclick="javascript:fc_Buscar();" align="center"></a>
				</td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" width="100%" 
			style="margin-left: 10px; margin-right: 5px; width: 98%; margin-top: 10px">
			<tr style="width: 100%">
				<td>
					<div style="overflow: auto; width: 100%; height: 150px">
						<display:table name="sessionScope.listaBusqPostulantes" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.PostulantesDecorator"
							style="border: 1px solid #6b6d6b ;width:98%">
	
							<display:column property="chkSelPostulante" title="Sel" headerClass="grilla" class="tablagrilla" style="width:2%" />
							<display:column property="nombreApellido" title="Apellidos y Nombres" headerClass="grilla" class="tablagrilla" style="text-align: left;width:38%"/>
							<display:column property="apepat" title="Instituci�n" headerClass="grilla" class="tablagrilla" style="text-align: left;width:20%"/>
							<display:column property="profesion" title="Profesi�n" headerClass="grilla" class="tablagrilla" style="text-align: left;;width:13%"/>
							<display:column property="area_interes" title="�rea Interes" headerClass="grilla" class="tablagrilla" style="text-align: left;width:13%"/>
							<display:column property="fechaModificacion" title="Fecha Modificaci�n" headerClass="grilla" class="tablagrilla" style="text-align: left;width:10%"/>
							<display:column property="linkImagen" title="Editar Datos" headerClass="grilla" class="tablagrilla" style="text-align:center ;width:4%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
						</display:table>
					</div>
					<%request.getSession().removeAttribute("listaBusqPostulantes"); %>
				</td>
			</tr>
			<tr><td class="texto_bold" align="right">&nbsp;</td></tr>
			<tr>
				<td align=center>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgaceptar','','${ctx}/images/botones/aceptar2.jpg',1)">
					<img alt="Aceptar" src="${ctx}/images/botones/aceptar1.jpg" id="imgaceptar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" style="cursor:pointer;" onclick="javascript:window.close();"></a>
				</td>
			</tr>
		</table>
	</form:form>
    <form id="frmPostulante" name="frmPostulante" >
    	<input type="hidden" id="txhCodPostulante" />
   	</form>
	<script type="text/javascript">
		objMessage = document.getElementById("txhMessage");
		if (objMessage.value == "ERROR" ) alert('Problemas al Grabar. Consulte con el administrador.');
		else if (objMessage.value == "OK" ){
			alert('Se grab� exitosamente');
			window.opener.fc_Buscar();
			window.close();
		}
		else if (objMessage.value == "EXISTE" ){
			alert('El o los Postulantes ya esta registrados en este Proceso');
		}
		else if (objMessage.value == "OKPARCIAL" ){
			alert('Ingreso satisfactorio pero algunos postulantes ya estaban asociados al Proceso');
			window.opener.fc_Buscar();
			window.close();
		}		
		else if (objMessage.value == "VER_DATOS" ){
			window.open("${ctx}/logueoPostulante.html");
		}
		objMessage.value = "";
	</script>
</body>
</html>