<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script language="javascript" type="text/javascript">
	function onLoad(){}
	
	function fc_CambiaEvaluador(objCombo, codDetalle)
	{
		strEvaluacionesSel = document.getElementById("txhCodEvaluaciones").value;
		strEvaluadoresSel = document.getElementById("txhCodEvaluadores").value;
		
		flag=false;
		
		if ( strEvaluadoresSel != '')
		{
			arrEvaluacionesSel = strEvaluacionesSel.split("|");
			arrEvaluadoresSel = strEvaluadoresSel.split("|");
			
			strEvaluacionesSel = "";
			strEvaluadoresSel = "";
		
			for (i = 0 ; i <= arrEvaluadoresSel.length-2 ; i++)
			{

				if ( arrEvaluacionesSel[i] == codDetalle ){ flag = true }
				else{ 
					strEvaluadoresSel = strEvaluadoresSel + arrEvaluadoresSel[i] + '|';
					strEvaluacionesSel = strEvaluacionesSel + arrEvaluacionesSel[i] + '|';
				}
			}
		}
		
		if ( objCombo.value != "" )
		{
			strEvaluadoresSel = strEvaluadoresSel + objCombo.value + '|';
			strEvaluacionesSel = strEvaluacionesSel + codDetalle + '|';
		}

		
		document.getElementById("txhCodEvaluaciones").value = strEvaluacionesSel;
		document.getElementById("txhCodEvaluadores").value = strEvaluadoresSel;

	}
	
	function fc_Grabar(){
		nroEvaluaciones = document.getElementById("txhNroEvaluaciones").value;
		objEvaluadores = document.getElementById("txhCodEvaluadores");
		nroEvaluadores = objEvaluadores.value.split("|").length - 1;
		if (nroEvaluadores < nroEvaluaciones){
			alert('Debe seleccionar un evaluador por cada evaluaci�n.');
			return false;
		}
		if ( !confirm('�Est� seguro de grabar?') ) return false;		
		document.getElementById("txhAccion").value = "GRABAR";
		document.getElementById("frmMain").submit();
	}
</script>
</head>
<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/evaluador_revision.html" >
		<form:hidden path="codUsuario" id="txhCodUsuario"/>
		<form:hidden path="operacion" id="txhAccion"/>
		<form:hidden path="message" id="txhMsg"/>
		<form:hidden path="codEvaluaciones" id="txhCodEvaluaciones"/>
		<form:hidden path="codEvaluadores" id="txhCodEvaluadores"/>
		<form:hidden path="nroEvaluaciones" id="txhNroEvaluaciones"/>
		<form:hidden path="codProceso" id="txhCodProceso"/>
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px; margin-top: 5px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">Definici�n de Evaluadores</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="2" class="tabla"
			style="width:95%; margin-left: 10px; margin-top: 5px">			
			<tr>
				<td width="20%">Proceso:</td>
				<td width="80%">
					<form:label path="dscProceso" id="txtProceso">${control.dscProceso}</form:label>
				</td>
			</tr>
			<tr>
				<td width="20%">Etapa:</td>
				<td width="80%">
					<form:label path="dscEtapa" id="txtEtapa">${control.dscEtapa}</form:label>
				</td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="2" style="width:96%; margin-left: 7px; margin-top: 10px">
			<tr>
				<td>
					<display:table name="sessionScope.listaTipoEvaluaciones" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.ProcesosDecorator" requestURI="" 
						id="tblTipoEvaluaciones" style="border: 1px solid #6b6d6b ;width:100%">
						<display:column property="descripcion" title="Tipo de Evaluaci�n" headerClass="grilla"						 
							class="tablagrilla" style="width:50%; text-align:left" />
						<display:column property="cboEvaluadores" title="Evaluador (Jefe Dpto.)" headerClass="grilla" 
							class="tablagrilla" style="width:50%" />						

						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFound' class='texto'>No se encontraron registros</span>" />
						<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
					</display:table>
				</td>			
			</tr>			
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td align=center>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="window.fc_Grabar();"></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" style="cursor:pointer;" onclick="window.close();"></a>
				</td>
			</tr>
		</table>
	</form:form>
	<script type="text/javascript">
		if ( document.getElementById("txhNroEvaluaciones").value == 0 ){
			alert('No existen evaluaciones configuradas para este tipo de proceso.');
			window.close();
		}
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value != "" ){
			if ( objMsg.value == "OK" ) alert('Se grab� exitosamente');
			else if ( objMsg.value == "ERROR" ) alert('Problemas al Grabar. Consulte con el administrador.');
			window.close();
		}
	</script>
</body>
</html>