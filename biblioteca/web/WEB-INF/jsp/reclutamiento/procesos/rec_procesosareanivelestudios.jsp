<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	function onLoad()
	{ 
	}
	function fc_seleccionarRegistro(codDetalle, dscDetalle)
	{   		
		document.getElementById("txhCodAreaEstNiv2").value = codDetalle;		
		document.getElementById("txhDscAreaEstNiv2").value = dscDetalle;
	}
	
	function fc_Grabar() {			
		flagRpt = true;
		if ( document.getElementById("txhCodAreaEstNiv2").value != "" ){		   
		   var Long=document.getElementById("txhLongitud").value;
		   var b;
		   for( a=0; a<Long; a++){			  			  
			   if( document.getElementById("text"+a).checked){			     
			      if ( !window.opener.fc_AddDetalleAreaNivelEstudios(document.getElementById("text"+a).value
					, document.getElementById("hid"+a).value
					, ""  ) )
					{
						flagRpt = false;
					}
				}
			}
			
			if ( !flagRpt )		
				alert("Una o mas niveles de estudio ya se encuentran seleccionadas.");
				
			window.close();
		}
		else alert('Debe seleccionar un nivel de estudio.');		
	}
</script>
</head>
<body>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/buscar_areanivelestudios.html" >
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="codAreaEstNiv2" id="txhCodAreaEstNiv2"/>
	<form:hidden path="dscAreaEstNiv2" id="txhDscAreaEstNiv2"/>
	<form:hidden path="longitud" id="txhLongitud"/>
	<table cellspacing="0" cellpadding="0" border="0" style="width:99%; margin-left: 10px; margin-top: 10px">
		<tr style="width: 100%">
			<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
			<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
				class="opc_combo">Niveles de Estudio</td>
			<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
		</tr>
	</table>
	<table style="width:94%;margin-top:10px;margin-left:10px" class="" cellspacing="2" cellpadding="2" border="0" bordercolor="blue">
		<tr>
			<td>
				<div style="overflow: auto; height: 220px">
				<table class="" style="width:95%;margin-left:11px;border: 1px solid #048BBA;" cellspacing="0" cellpadding="0" border="0" align="left">
					<tr>
						<TD class="grilla" width="5%" align="center">&nbsp;Sel.</TD>
						<TD class="grilla" width="10%" align="center">&nbsp;Niveles de Estudio</TD>
					</tr>
					<c:forEach var="lista" items="${control.listaAreasNivelEstudios}" varStatus="loop">										
						<tr class="tablagrilla">
							<td align="center">
								<input type="checkbox" value="${lista.codTipoTablaDetalle}" id="text<c:out value="${loop.index}"/>" onclick="javascript:fc_seleccionarRegistro(this.value,'<c:out value="${lista.descripcion}"/>')" />
								<input type="hidden" value="${lista.descripcion}" id="hid<c:out value="${loop.index}"/>" name="hid<c:out value="${loop.index}"/>"/>
							</td>
							<td align="left">
								<c:out value="${lista.descripcion}" />
							</td>
						</tr>	
					</c:forEach>				
				</table>				
				</div>				 				
			</td>
		</tr>
	</table>
	<table align="center" style="margin-top: 10px">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>
</form:form>
</body>
</html>