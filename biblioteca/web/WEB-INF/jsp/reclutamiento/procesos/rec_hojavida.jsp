<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script type="text/javascript">
	function onLoad(){
		document.getElementById("txtPerfil").readOnly=true;
		//document.getElementById("txtDesPuesto1").readOnly=true;
		
		if (document.getElementById("txhIndPago").value == "1"){
			document.getElementById("rbtPagoMes").checked = true;
			document.getElementById("rbtPagoHoras").disabled = true;					
		}
		else{
			document.getElementById("rbtPagoMes").disabled = true;
			document.getElementById("rbtPagoHoras").checked = true;
		}
		if (document.getElementById("txhIndSexo").value == "0002"){
			document.getElementById("rbtSexoM").checked = true;
			document.getElementById("rbtSexoF").disabled = true;
		}
		else{
			document.getElementById("rbtSexoM").disabled = true;
			document.getElementById("rbtSexoF").checked = true;
		}
	}

	function fc_VerHistorial(){				
		document.getElementById("txhAccion").value = "VER_DATOS";
		document.getElementById("txhMessage").value = document.getElementById("txhCodPostulante").value;
		document.getElementById("frmMain").submit();
	}
	
</script>
</head>
<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/hoja_vida.html" >
		<form:hidden path="idRec" id="txhCodPostulante"/>
		<form:hidden path="indPago" id="txhIndPago"/>
		<form:hidden path="sexo" id="txhIndSexo"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="codUsuario" id="codUsuario"/>
		<form:hidden path="operacion" id="txhAccion"/>
		<form:hidden path="procedencia" id="txhProcedencia"/>
		
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px;margin-top:10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">DATOS PERSONALES</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellspacing="0" class="tabla" bordercolor="red" style="margin-left: 10px; margin-right: 10px;margin-top:5px; width:96%">
			<tr>
				<td width="20%" class="">&nbsp;Nombres :</td>
				<td width="20%"><form:input path="nombres" cssClass="cajatexto_1_black"  readonly="true"/></td>
				<td width="20%" class="">Fecha de Registro :</td>
				<td width="20%"><form:label path="fecreg">${control.fecreg}</form:label></td>
				<td width="20%" rowspan="5">
					<c:if test="${control.foto!=null}">
						<img src="${control.foto}" id="imgFoto" width="100" height="120px" border="1">
					</c:if>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Apellido Paterno :</td>
				<td width="20%"><form:input path="apepat" cssClass="cajatexto_1_black" readonly="true"/></td>
				<td width="20%" class="">Apellido Materno :</td>
				<td width="20%"><form:input path="apemat" cssClass="cajatexto_1_black" readonly="true"/></td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Fecha Nacimiento :</td>
				<td width="20%"><form:input path="fecnac" cssClass="cajatexto_1_black" readonly="true" cssStyle="width:70px"/></td>
				<td width="20%" class="">Sexo :</td>
				<td width="20%">
					<input type="radio" ID="rbtSexoF" name="rbtSexo">Femenino 
					<input type="radio" ID="rbtSexoM" name="rbtSexo">Masculino
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Estado Civil :</td>
				<td width="20%">
					<form:select path="estadoCivil" id="cboEstadoCivil" cssClass="cajatexto_1_black" disabled="false">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaEstadoCivil!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaEstadoCivil}" />
						</c:if>
					</form:select>
				</td>
				<td width="20%" class="">Nacionalidad :</td>
				<td width="20%">
					<form:input path="nacionalidad" id="txtNacionalidad" cssClass="cajatexto_1_black" 
						readonly="true"/>
					<!-- <input type="radio" id="rbtNacPeru" name="rbtNacion" readonly="true">Peruano
					<input type="radio" id="rbtNacOtro" name="rbtNacion" readonly="true">Otro -->
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;DNI :</td>
				<td width="20%">
					<form:input path="dni" cssClass="cajatexto_1_black" readonly="true"/>
				</td>
				<td width="20%" class="">Nro. Ruc :</td>
				<td width="20%">
					<form:input path="ruc" id="txtRuc" maxlength="11" cssClass="cajatexto_1_black" readonly="true"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Email :</td>
				<td width="20%" colspan="3">
					<form:input path="email" id="txtEmail" cssClass="cajatexto_1_black" size="70" readonly="true"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Email Alterno :</td>
				<td width="20%" colspan="3">
					<form:input path="email1" id="txtEmailAlterno" size="70" cssClass="cajatexto_1_black" />
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Experiencia Laboral :</td>
				<td width="20%" colspan="3">
					<form:input path="anioExpLaboral" id="txtAnioExpLaboral" cssClass="cajatexto_1_black" size="5" />(en a�os)
				</td>
			</tr>
		</table>
		<br>
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px;margin-top:10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">PERFIL PROFESIONAL</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>			
		<table cellspacing="5" class="tabla" style="margin-left: 10px; margin-top: 5px; width:96%">
			<tr>
				<td width="20%" align="center">
					<form:textarea path="perfil" id="txtPerfil" cssClass="cajatexto_1_black" cols="150" rows="4" onkeypress="return false;"/>
				</td>
			</tr>
		</table>
		<br>
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px;margin-top:10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">DATOS DE DOMICILIO</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>			
		<table cellspacing="0" class="tabla" style="margin-left: 10px; margin-top: 5px; width:96%" border="0" bordercolor="red">
			<tr>
				<td width="20%" class="">&nbsp;Domicilio :</td>
				<td width="20%" colspan="4">
					<form:input path="direccion" id="txtDireccion" cssClass="cajatexto_1_black" size="50" readonly="true"/>
					&nbsp;(Avenida, Calle, Numero)
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Pais Domicilio :</td>
				<td width="30%">
					<form:input path="pais" id="txtPais" cssClass="cajatexto_1_black" size="30" readonly="true"/>
				</td>
				<td width="30%" class="">Departamento Domicilio :</td>
				<td width="20%">
					<form:select path="departamento" id="cboDepartamento"  disabled="false" 
						cssClass="cajatexto_1_black" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaDepartamento!=null}">
						<form:options itemValue="codigo" itemLabel="dscValor1" 
							items="${control.listaDepartamento}" />
						</c:if>
					</form:select>
				</td>
				<td></td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Provincia :</td>
				<td width="20%">
					<form:select path="provincia" id="cboProvincia" cssClass="cajatexto_1_black" disabled="false" 
						cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaProvincia!=null}">
						<form:options itemValue="codigo" itemLabel="dscValor1" 
							items="${control.listaProvincia}" />
						</c:if>
					</form:select>
				</td>
				<td width="20%" class="">Distrito :</td>
				<td width="20%">
					<form:select path="distrito" id="cboDistrito" cssClass="cajatexto_1_black" disabled="false" 
						cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaDistrito!=null}">
						<form:options itemValue="codigo" itemLabel="dscValor1" 
							items="${control.listaDistrito}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Tel�fono Domicilio :</td>
				<td width="20%">
					<form:input path="telef" id="txtTelef" cssClass="cajatexto_1_black" readonly="true"/>
				</td>
				<td width="20%" class="">Tel�fono Adicional :</td>
				<td width="20%">
					<form:input path="telefAdicional" id="txtTelefAdicional" cssClass="cajatexto_1_black" readonly="true"/>
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Tel�fono M�vil :</td>
				<td width="20%" colspan="3">
					<form:input path="telefMovil" id="txtTelefMovil" cssClass="cajatexto_1_black" readonly="true"/>
				</td>
			</tr>
		</table>
		<br>
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px;margin-top:10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">INFORMACION LABORAL</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>		
		<table cellspacing="2" cellpadding="0" class="tabla" style="margin-left: 10px; margin-top: 5px; width:96%" border="0" bordercolor="red">
			<tr>
				<td width="20%" class="">&nbsp;Pretensi�n Econ�mica S/. :</td>
				<td width="30%">
					<form:select path="moneda" id="cboMoneda" cssClass="cajatexto" disabled="true" 
						cssStyle="width:40px">
						<form:option value="">-S-</form:option>
						<c:if test="${control.listaMoneda!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="dscValor3" 
							items="${control.listaMoneda}" />
						</c:if>
					</form:select>
					<form:input path="pretensionEconomica" cssClass="cajatexto_1_black" 
						readonly="true" cssStyle="text-align:right" size="12"/>
					<input type="radio" ID="rbtPagoMes" NAME="pago" onclick="javascript:fc_Pago('1');">Mes
					<input type="radio" ID="rbtPagoHoras" NAME="pago" onclick="javascript:fc_Pago('0');">Horas
				</td>
				<td width="50%" rowspan="4">
					<div style="overflow: auto; height: 100px; width:90%; vertical-align: top; border: 0px solid blue" class="tabla_1A">
					<table id="tblAreaInteres" cellpadding="0" cellspacing="0" 
						style="width:95%; height: 150; vertical-align: top" class="tabla_1A" border="1" bordercolor="white" >
							<tr class="grilla" height="5px" valign="middle">								
								<td align="center">&Aacute;reas de Inter&eacute;s</td>
							</tr>
							<c:forEach varStatus="loop" var="lista" items="${control.listaAreaInteresByPost}" >							
							<tr class="grillaDetalle" style="display: " id='trArea<c:out value="${loop.index}"/>'  name='trArea<c:out value="${loop.index}"/>'>								
								<td align="left">&nbsp;
									<c:out value="${lista.descInteres}" />
								</td>
							</tr>
							</c:forEach>				
					</table>		
					</div>					
				</td>
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Dedicaci�n :</td>
				<td width="30%">
					<form:select path="dedicacion" id="cboDedicacion" disabled="false" 
						cssClass="cajatexto_1_black" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaDedicacion!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaDedicacion}" />
						</c:if>
					</form:select>
				</td>				
			</tr>
			<tr>
				<td width="20%" class="">&nbsp;Disponibilidad :</td>
				<td width="30%">
					<form:select path="dispoTrabajar" id="cboDispoTrabajar" disabled="false"
						cssClass="cajatexto_1_black" cssStyle="width:115px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaDispoTrabajar!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaDispoTrabajar}" />
						</c:if>
					</form:select>
				</td>								
			</tr>
			<tr><td>&nbsp;</td>
			</tr>			
		</table>
		<br>

		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px;margin-top:10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">ESTUDIOS SUPERIORES</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>	
			<display:table 
				name="control.listaEstudiosSuperiores" cellpadding="0"
				cellspacing="0"
				decorator="com.tecsup.SGA.bean.HojaVidaDatosDecorator"
				pagesize="10" style="width:96%; border: 0px solid #048BBA; margin-left: 10px;margin-top:0px; " requestURI="">

				<display:column property="estudiosSuperiores" headerClass="grilla"
					class="" style="text-align:left; width:100%" />

					<display:setProperty name="basic.show.header" value="false"/>
					<display:setProperty name="basic.empty.showtable" value="true"/>
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
				
			</display:table>	
		<br>
		<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px;margin-top:10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">EXPERIENCIA LABORAL</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<display:table name="control.listaExperienciasLaborales" cellpadding="0" 				
				cellspacing="0" decorator="com.tecsup.SGA.bean.HojaVidaDatosDecorator"
				pagesize="10" style="width:96%; border: 0px solid #048BBA; margin-left: 10px;margin-top:0px; " requestURI="">
				<display:column property="experienciasLaborales" headerClass="grilla" class="" style="text-align:left; width:100%" />					
				<display:setProperty name="basic.show.header" value="false"/>
				<display:setProperty name="basic.empty.showtable" value="true"/>
				<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
				<display:setProperty name="paging.banner.placement" value="bottom"/>
				<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
				<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
				<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
				<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
				<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
				<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
				<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
				<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
				<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
				<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />				
	</display:table>
	<br>
		<table cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
					<c:if test="${control.procedencia=='proceso'}">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgeditar','','${ctx}/images/botones/editardatospost2.png',1)">
						<img alt="Editar datos" src="${ctx}/images/botones/editardatospost1.png" id="imgeditar" style="cursor:pointer;" onclick="fc_VerHistorial();" ></a>
					</c:if>
				</td>
				<td width="2px">&nbsp;</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcerrar','','${ctx}/images/botones/cerrar2.jpg',1)">
					<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="imgcerrar" style="cursor:pointer;" onclick="window.close();" ></a>
				</td>
			</tr>
		</table>
		<br>
	</form:form>

	<script type="text/javascript">
		objMessage = document.getElementById("txhMessage");
		if (objMessage.value == "VER_DATOS")
		{
			window.open("${ctx}/logueoPostulante.html");
			window.close();
		}
		objMessage.value = "";
	</script>

</body>
</html>