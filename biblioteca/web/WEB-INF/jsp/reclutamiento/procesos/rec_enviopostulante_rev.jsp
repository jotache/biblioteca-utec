<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	function onLoad(){}
		
	function fc_CambiaCalificacion(objCombo, strCodTipoEvaluacion, indiceFila){	
		strCaliSel = document.getElementById("txhCodCalificaciones").value;
		strEvalSel = document.getElementById("txhCodEvalSel").value;		
		flag=false;
				
		if ( strEvalSel != '') {		
			arrEvalSel = strEvalSel.split("|");
			arrCaliSel = strCaliSel.split("|");			
			strEvalSel = "";
			strCaliSel = "";
					
			for (i = 0 ; i <= arrEvalSel.length-2 ; i++){	
				if ( arrEvalSel[i] == strCodTipoEvaluacion){
					flag = false 
				} else{
					strEvalSel = strEvalSel + arrEvalSel[i] + '|';
					strCaliSel = strCaliSel + arrCaliSel[i] + '|';
				}
			}
		}
		if (!flag){		
			if ( objCombo.value != "" ){			
				strEvalSel = strEvalSel + strCodTipoEvaluacion + '|';
				strCaliSel = strCaliSel + objCombo.value + '|';
			}
		}
		document.getElementById("txhCodEvalSel").value = strEvalSel;
		document.getElementById("txhCodCalificaciones").value = strCaliSel;
	}
	
	function fc_Grabar(){	
		objNumEval = document.getElementById("txhNumEvaluaciones");
		objComentario = document.getElementById("txhComentario");		
		arrCalificaciones = document.getElementById("txhCodCalificaciones").value.split("|");		
		if ( objNumEval.value != (arrCalificaciones.length - 1) ){		
			alert('Debe seleccionar una calificaci�n por cada tipo de evaluaci�n.');
			return false;
		}
		
		var z = 0;
		objComentario.value = "";		
		objTxa = document.getElementById("txaObservacion" + z);
		if ( objTxa != null ){	
			if (objTxa.value==''){
				objComentario.value = objComentario.value + objTxa.value + " |";
			}else{
				objComentario.value = objComentario.value + objTxa.value + "|";
			}
		} 
					
		if (!confirm('�Est� seguro de grabar?')) return false;

		document.getElementById("txhAccion").value = "GRABAR";
		document.getElementById("frmMain").submit();
	}
</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/eviar_proceso_revision.html" >
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="numEvaluaciones" id="txhNumEvaluaciones"/>
	<form:hidden path="codCalificaciones" id="txhCodCalificaciones"/>
	<form:hidden path="codEvalSel" id="txhCodEvalSel"/>
	
	<form:hidden path="codPostSel" id="txhCodPostSel"/>
	<form:hidden path="nomPostSel" id="txhNomPostSel"/>
	<form:hidden path="codProceso" id="txhCodProceso"/>
	<form:hidden path="comentario" id="txhComentario"/>
	<table cellspacing="0" cellpadding="0" border="0" style="width:95%; margin-left: 10px; margin-top: 5px">
		<tr style="width: 100%">
			<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
			<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
				class="opc_combo">Postulantes a Enviar</td>
			<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
		</tr>
	</table>
	<table cellpadding="1" cellspacing="2" class="tabla" style="width:95%; margin-left: 10px; margin-top: 5px" >
		<tr>
			<td width="15%" valign=top>Postulantes :</td>
			<td >
				<form:select path="codPostulantes" id="cboPostulantes" cssClass="cajatexto_1" 
					cssStyle="width:180px" multiple="true">
					<c:if test="${control.listPostulantes!=null}">
					<form:options itemValue="codPostulante" itemLabel="nombres" 
						items="${control.listPostulantes}" />
					</c:if>
				</form:select>
			</td>
			<td width="15%" valign="top">Etapa :</td>
			<td valign="top">
				<form:label path="dscEtapa" id="lblDscEtapa">Revisi�n</form:label>
			</td>
		</tr>
		<tr>
			<td colspan="4">Tipo Evaluaci�n :</td>
		</tr>
		<tr>
			<td colspan="4" align="center">
				<display:table name="sessionScope.listaEvaluaciones" cellpadding="3" cellspacing="0"
					decorator="com.tecsup.SGA.bean.ProcesosDecorator" requestURI="" 
					style="width:100%; vertical-align: middle">
					
					<display:column property="descripcion" title="" style="width:25%; text-align: left;"/>
					<display:column property="cboCalificacion" title="" style="width:25%; text-align: center;"/>
					<display:column property="observacion" title="" style="width:50%; text-align: center;"/>
										
					<display:setProperty name="basic.show.header" value="false"/>
					<display:setProperty name="basic.empty.showtable" value="true"/>
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
				</display:table>
			</td>
		</tr>
	</table>
	<table align=center>
		<tr>
			<td><input type=button class="boton" value="Grabar" onclick="fc_Grabar();">&nbsp;<input type=button class="boton" value="Cancelar" ID="Button1" NAME="Button1"  onclick="window.close();"></td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	objMsg = document.getElementById("txhMsg");
	if ( fc_Trim(objMsg.value) == "OK" )
	{
		alert('Se grab� exitosamente');
		window.opener.fc_Buscar();
		window.close();
	}
	else if ( fc_Trim(objMsg.value) == "ERROR" ) alert('Problemas al Grabar. Consulte con el administrador.');
	objMsg.value = "";
</script>
</html>