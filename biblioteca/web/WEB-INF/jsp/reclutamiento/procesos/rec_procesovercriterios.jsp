<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	function onLoad()	
	{}
	function fc_DispViajar( strTipo )
	{
		objDentroPais = document.getElementById("rbtDentroPais");
		objFueraPais = document.getElementById("rbtFueraPais");
		//objPostDoc	= document.getElementById("rbtDoc");
		
		objDentroPais.checked = false;
		objFueraPais.checked = false;
		
		if ( strTipo == 1)
		{
			objDentroPais.checked = true;
		}
		else
		{
			objFueraPais.checked = true;
		}
		
		document.getElementById("txhIndViaje").value = strTipo;
	}
</script>
</head>
<body style="margin-top:0; margin-left: 10px; margin-right: 5px;">
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/ver_criterios.html" >
	<form:hidden path="codProceso" id="txhCodProceso"/>
	<form:hidden path="flagDisponibilidadViajar" id="txhIndViaje"/>
	<form:hidden path="flagDisponibilidadViajar1" id="txhIndViaje1"/>

	<form:hidden path="puestoPostulaDoc" id="txhIndPostDoc"/>
	<form:hidden path="puestoPostulaAdm" id="txhIndPostAdm"/>


		<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 10px; margin-top: 5px">
			<tr>
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="292px" height="27px" 
					class="opc_combo">Criterios del Proceso</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="3" cellspacing="3" class="tabla" bordercolor="red" border="0"
		      style="width:95%; margin-left: 10px; margin-top: 5px">
			<tr>
				<td width="50%">
					<div style="overflow: auto;width: 100%; height: 100px">
						<display:table name="control.listAreasInteres" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator"  
							style="border: 1px solid white;width:93%" id="tblAreasInteres">
							<display:column property="dscValor1" title="Areas de Inter�s" headerClass="grilla2" class="grillaDetalle" style="text-align: left;"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFound' class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
						</display:table>
					</div>
					<%--request.getSession().removeAttribute("listaAreasInteres"); --%>
				</td>
				<td width="50%">
				
					<div style="overflow: auto; width: 100%; height: 100px">
						<display:table name="control.listAreaEstudio" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
							style="border: 1px solid white;width:93%" id="tblAreasEstudio">
							
							<display:column property="descripcion" title="Areas de Estudio" headerClass="grilla2" class="grillaDetalle"
								style="text-align: left;"/>	
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center' id='pagNoItemFoundEst' >No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFoundEst' class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
						</display:table>
					</div>
				
				</td>
			</tr>
			<tr class="texto">
			  <td colspan="1">			  			 									  
				<div style="overflow: auto; width: 100%; height: 100px">
					<display:table name="control.listAreasNivelEstudios" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
						style="border: 1px solid white;width:93%" id="tblAreasNivEstudio">
						
						<display:column property="descripcion" title="Niveles de Estudio" headerClass="grilla2" class="grillaDetalle"
							style="text-align: left;"/>	
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center' id='pagNoItemFoundNivEst' >No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFoundNivEst' class='texto'>No se encontraron registros</span>" />
						<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
					</display:table>
				</div>
			  
			  </td>
			  <td>
					<div style="overflow: auto; width: 100%; height: 100px">
						<display:table name="control.listInstEdu" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
							style="border: 1px solid white;width:93%" id="tblInstituciones">
							
							<display:column property="descripcion" title="Instituciones educativas" headerClass="grilla2" class="grillaDetalle"
								style="text-align: left;"/>	
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center' id='pagNoItemFoundInst' >No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFoundInst' class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
						</display:table>
				  </div>										
			  
			  </td>
		  </tr>
		  <%-- 
			<tr class="texto">
			  <td colspan="1">Sexo :</td>
			  <td>
			  <form:select path="codSexo" id="cboSexo" cssClass="cajatexto" 
									cssStyle="width:100px" disabled="true">
									<form:option value="">--Ambos--</form:option>
									<c:if test="${control.listSexo!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listSexo}" />
									</c:if>
								</form:select>
			  </td>
		  </tr>
		
			<tr class="texto">
			  <td colspan="1">Grado Acad&eacute;mico</td>
			  <td>
			  <form:select path="codGradoAcedemico" id="cboGradoAcedemico" cssClass="cajatexto" 
									cssStyle="width:250px" disabled="true">
									<form:option value="">---Seleccione---</form:option>
									<c:if test="${control.listGradoAcedemico!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listGradoAcedemico}" />
									</c:if>
				</form:select>
			  </td>
		  </tr>
		  
			<tr class="texto">
							<td colspan="1">Area de Estudios :
								
							</td>
							<td>
							<form:select path="codAreaEstudio" id="cboAreaEstudio" cssClass="cajatexto" 
									cssStyle="width:300px" disabled="true">
									<form:option value="">---Seleccione---</form:option>
									<c:if test="${control.listAreaEstudio!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listAreaEstudio}" />
									</c:if>
								</form:select>
							</td>
						</tr>
						
			<tr class="texto">
							<td colspan="1">Instituci�n Acad�mica:
								
							</td>
							<td>
							<form:select path="codInstitucionAcademica" id="cboInstitucionAcedemica" cssClass="cajatexto" 
									cssStyle="width:300px" disabled="true">
									<form:option value="">---Seleccione---</form:option>
									<c:if test="${control.listInstitucionAcademica!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listInstitucionAcademica}" />
									</c:if>
								</form:select>
							</td>
			</tr>
				--%>		
			<tr>
				<td colspan="2">
					<table bordercolor="red" border="0">

						<tr class="texto">
							<td width="20%">Sexo : </td>
							<td width="30%">
								<form:select path="codSexo" id="cboSexo" cssClass="cajatexto" 
									cssStyle="width:100px" disabled="true">
									<form:option value="">--Ambos--</form:option>
									<c:if test="${control.listSexo!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listSexo}" />
									</c:if>
								</form:select>								
							</td>
							<td colspan="2" width="50%">&nbsp; 							
							</td>
						</tr>

						<tr class="texto">
							<td width="20%">Expectativa<br>Salarial : </td>
							<td width="30%">
								<form:select path="codTipoMoneda" id="codTipoMoneda" cssClass="cajatexto" 
											cssStyle="width:45px" disabled="true">
											<form:option value="">SEL</form:option>
											<c:if test="${control.listaTipoMoneda!=null}">
											<form:options itemValue="codTipoTablaDetalle" itemLabel="dscValor3" 
												items="${control.listaTipoMoneda}" />
											</c:if>
										</form:select>&nbsp;&nbsp;
								<form:input path="salarioIni" id="txtSalarioIni" cssClass="cajatexto" size="6" maxlength="8" disabled="true"/>&nbsp;y&nbsp;
								<form:input path="salarioFin" id="txtSalarioFin" cssClass="cajatexto" size="6" maxlength="8" disabled="true"/>
							</td>
							<td colspan="2" width="50%">Edad :&nbsp; 
								<form:select path="codTipoEdad" id="cboTipoEdad" cssClass="cajatexto" 
									cssStyle="width:40px" disabled="true">
									<form:option value="">SEL</form:option>
									<c:if test="${control.listTipoEdad!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listTipoEdad}" />
									</c:if>
								</form:select>&nbsp;
								<form:input path="edad" id="txtEdad" cssClass="cajatexto" cssStyle="width:20px;" disabled="true"/>
								&nbsp;Y&nbsp;
								<form:select path="codTipoEdad1" id="cboTipoEdad1" cssClass="cajatexto" 
									cssStyle="width:40px" disabled="true">
									<form:option value="">SEL</form:option>
									<c:if test="${control.listTipoEdad!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listTipoEdad}" />
									</c:if>
								</form:select>&nbsp;
								<form:input path="edad1" id="txtEdad1" cssClass="cajatexto" cssStyle="width:20px; text-align: right"
									 		maxlength="2" disabled="true" />
								</td>
							
						</tr>
						<tr class="texto">
							<td>Dedicaci�n : </td>
							<td>
								<form:select path="codDedicacion" id="cboDedicacion" cssClass="cajatexto_1" 
									cssStyle="width:120px" disabled="true">
									<form:option value="">--Seleccione--</form:option>
									<c:if test="${control.listDedicacion!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listDedicacion}" />
									</c:if>
								</form:select>
							</td>
							<td colspan="2">Postula a : <br><!-- JCMU 27/11/2008 - Deshabilitado -->
								<input type="checkbox" id="rbtDoc" disabled="disabled"/>Docente
								<input type="checkbox" id="rbtAdm" disabled="disabled"/>Administrativo
							</td>
						</tr>
						<tr class="texto">
							<td>Interesado en : </td>
							<td>
								<form:select path="codInteresado" id="cboInteresado" cssClass="cajatexto_1" 
									cssStyle="width:120px" disabled="true">
									<form:option value="">--Seleccione--</form:option>
									<c:if test="${control.listInteresado!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
										items="${control.listInteresado}" />
									</c:if>
								</form:select>
							</td>
							<td>Disponibilidad<br>de viajar : </td>
							<td>
								<input type="checkbox" id="rbtDentroPais" disabled="disabled"/>Dentro del Pa�s
								<input type="checkbox" id="rbtFueraPais" disabled="disabled"/>Fuera del Pa�s
							</td>
						</tr>
						<tr class="texto">
							<td>A�os de Experiencia : </td>
							<td>
								<form:input path="anhosExperiencia" id="txtAnhosExperiencia" size="5" maxlength="3"
									cssClass="cajatexto" disabled="true"/>
							</td>
							<td>A�os Experiencia<br>Docencia : </td>
							<td>
								<form:input path="anhosExperienciaDocencia" id="txtAnhosExperienciaDocencia" size="5"
									maxlength="3"  cssClass="cajatexto" disabled="true"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcerrar','','${ctx}/images/botones/cerrar2.jpg',1)">
					<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="imgcerrar" style="cursor:pointer;" onclick="window.close();"></a>
				</td>
			</tr>
		</table>
	</form:form>
	<script type="text/javascript" language="javascript">
	
	//alert('**'+document.getElementById("txhIndViaje").value+'**');
	
	if ( document.getElementById("txhIndViaje").value == "1" ) 
		 document.getElementById("rbtDentroPais").checked = true;
	if ( document.getElementById("txhIndViaje1").value == "1" )
		 document.getElementById("rbtFueraPais").checked = true;

	//Puesto al que postula: 	
	if ( document.getElementById("txhIndPostDoc").value == "1" ) 
		 document.getElementById("rbtDoc").checked = true;
	if ( document.getElementById("txhIndPostAdm").value == "1" )
		 document.getElementById("rbtAdm").checked = true;

	
	</script>
</body>
</html>