<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	function onLoad(){}		
	function fc_GetNumeroSeleccionados(){	
		strCodSel = document.getElementById("txhCodProcesosSeleccionados").value;
		if ( strCodSel != '' ){		
			ArrCodSel = strCodSel.split("|");
			return ArrCodSel.length - 1;
		}
			else return 0;
	}
	function fc_SoloUnRegistro(){	
		intNumeroSeleccionados = fc_GetNumeroSeleccionados();		
		if ( intNumeroSeleccionados == 0){		
			alert('Debe seleccionar un registro');
			return "";
		}		
		if ( intNumeroSeleccionados > 1){		
			alert('Debe seleccionar s�lo un registro');
			return "";
		}		 
		strCodSel = document.getElementById("txhCodProcesosSeleccionados").value;
		ArrCodSel = strCodSel.split("|");
		return ArrCodSel[0]; 	
	}
	function fc_seleccionarRegistro(strCodProceso){	
		strCodSel = document.getElementById("txhCodProcesosSeleccionados").value;
		flag=false;		
		if (strCodSel!=''){		
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";		
			for (i=0;i<=ArrCodSel.length-2;i++){			
				if (ArrCodSel[i] == strCodProceso) 
					flag = true 
				else 
					strCodSel = strCodSel + ArrCodSel[i]+'|';
			}
		}
		if (!flag)		
			strCodSel = strCodSel + strCodProceso + '|';
			
		document.getElementById("txhCodProcesosSeleccionados").value = strCodSel;
	}
	
	function fc_AbreOferta(){	
		codSel = fc_SoloUnRegistro();		
		if ( codSel != "" ){		
			var url = "";
			url = "${ctx}/reclutamiento/procesos_oferta.html";
			url = url + "?txhCodProceso=" + codSel;
			url = url + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
			Fc_Popup(url,500,270,"");
		}
	}
	
	function fc_NuevaProceso(){	
		var url = "";
		codTipoEtapa=document.getElementById("txhTipoEtapa").value;
		url = "${ctx}/reclutamiento/procesos_configuracion.html";
		url = url + "?txhCodUsuario=" + document.getElementById("txhCodUsuario").value +
		"&txhCodTipoEtapa="+codTipoEtapa;		
		Fc_Popup(url,700,650,"");
	}
	
	function fc_EditaProceso(){	
		codSel = fc_SoloUnRegistro();
		if ( codSel != "" ){		
			var url = "";
			url = "${ctx}/reclutamiento/procesos_configuracion.html";
			url = url + "?txhCodProceso=" + codSel;
			url = url + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
			Fc_Popup(url,700,650,"");
		}
	}
	function fc_EliminarProceso(){			
		codSel = fc_SoloUnRegistro();
		if ( codSel != "" ){		
			if ( !confirm('�Est� seguro de eliminar?') ) return false;
			document.getElementById("txhAccion").value = "ELIMINAR";
			document.getElementById("frmMain").submit();
		}
	}
	
	function fc_CambiaEtapa(strTipo){	
		objRevision = document.getElementById("rbtRevision");
		objSeleccion = document.getElementById("rbtSeleccion");
		objOferta = document.getElementById("btnOferta");		
		objRevision.checked = false;
		objSeleccion.checked = false;
		objOferta.style.display = "none";		 
		if ( strTipo == 1){		
			objRevision.checked = true;
			objOferta.style.display = "inline-block";
		}
		else objSeleccion.checked = true;
		
		document.getElementById("txhTipoEtapa").value = strTipo;		
		document.getElementById("txhAccion").value = "BUSCAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_Postulantes(){	
		codSel = fc_SoloUnRegistro();
		if ( codSel != "" ){		
			var url = "";
			url = "${ctx}/reclutamiento/procesos_postulantes.html";
			url = url + "?txhCodProceso=" + codSel;
			url = url + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
			url = url + "&txhCodPeriodo=" + document.getElementById("txhCodPeriodo").value;
			//agregado napa
			url = url + "&txhTipoEtapa=" + document.getElementById("txhTipoEtapa").value;
			url = url + "&txhCodUniFun=" + document.getElementById("cboUnidadFuncional").value;
			url = url + "&txhFecIni=" + document.getElementById("txtFecIni").value;
			url = url + "&txhFecFin=" + document.getElementById("txtFecFin").value;
			location.href = url;
		}
	}
	
	function fc_Evaluadores(){	
		codSel = fc_SoloUnRegistro();
		var url = "";
		if (codSel != ""){		
			if ( document.getElementById("txhTipoEtapa").value == "2")
				url = "${ctx}/reclutamiento/evaluador_seleccion.html";
			else
				url = "${ctx}/reclutamiento/evaluador_revision.html";
				
			url = url + "?txhCodProceso=" + codSel;
			url = url + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
			url = url + "&txhCodPeriodo=" + document.getElementById("txhCodPeriodo").value;
			
			if ( document.getElementById("txhTipoEtapa").value == "2")
				Fc_Popup(url,500,320,"");
			else Fc_Popup(url,500,235,"");			
		}
	}
	
	function fc_Buscar(){	
		objFecIni = document.getElementById("txtFecIni");
		objFecFin = document.getElementById("txtFecFin");		
		if ( objFecIni.value == "" || objFecFin.value == "" ){		
			alert('Debe ingresar fecha de inicio y fin de registro.');
			return false;
		}		
		if (Fc_RestaFechas(objFecIni.value,"dd/MM/yyyy",objFecFin.value,"dd/MM/yyyy") < 0 ){		
			alert("Fecha de inicio debe ser mayor a fecha fin.");
			objFecIni.focus();
			return false;
		}		
		document.getElementById("txhAccion").value = "BUSCAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_VerOferta(codProceso){	
		var url = "";
		url = "${ctx}/reclutamiento/procesos_oferta.html";
		url = url + "?txhIndEdicicion=1&txhCodProceso=" + codProceso;
		url = url + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
		url = url + "&txhCodPeriodo=" + document.getElementById("txhCodPeriodo").value;
		Fc_Popup(url,500,270,"");
	}
	
	function fc_Regresar(){	
		document.location.href = "${ctx}/menuReclutamiento.html";
	}
</script>
<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
<script src="${ctx}/scripts/js/funciones_rec-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
</head>
<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/procesos_bandeja.html" >
		<form:hidden path="operacion" id="txhAccion"/>
		<form:hidden path="codUsuario" id="txhCodUsuario"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codProcesosSeleccionados" id="txhCodProcesosSeleccionados"/>
		<form:hidden path="flagEtapa" id="txhTipoEtapa"/>
		<form:hidden path="msg" id="txhMsg"/>	
		<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" width="99%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand" alt="Regresar"></td>
			</tr>
		</table>	
		<!-- Titulo de la Pagina -->
		<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 10px; margin-top: 5px">
			<tr>
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="728px" height="27px" class="opc_combo">Lista de Procesos</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="2" class="tabla" 
			style="margin-left:10px; margin-top: 5px; width: 920px;">
			<tr>
				<td>Etapa :</td>
				<td width="20%">
					<input type="radio" id="rbtRevision" checked onclick="fc_CambiaEtapa('1');">Revisi�n&nbsp;
					<input type="radio" id="rbtSeleccion" onclick="fc_CambiaEtapa('2');">Selecci�n
				</td>
			</tr>
			<tr>
				<td class="">Unidad Funcional :</td>
				<td>
					<form:select path="unidadFuncional" id="cboUnidadFuncional" cssClass="cajatexto" cssStyle="width:180px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaUnidadFuncional!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaUnidadFuncional}" />
						</c:if>
					</form:select>
				</td>
			</tr>
			<tr>
				<td class="">Fec. Inicio Registro :</td>
				<td><form:input maxlength="10" path="fecIni" id="txtFecIni"
					onkeypress="javascript:fc_ValidaFecha('txtFecIni');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecIni');"  
					cssClass="cajatexto_1" cssStyle="width:70px"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgfecini','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgfecini" alt="Seleccionar Fecha Inicial" 
						style="cursor:pointer; vertical-align: middle"></a>
				</td>
				<td width="15%">Fec. Fin Registro :</td>
				<td><form:input maxlength="10" path="fecFin" id="txtFecFin"
					onkeypress="javascript:fc_ValidaFecha('txtFecFin');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecFin');" 
					cssClass="cajatexto_1" cssStyle="width:70px"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgfecfin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" alt="Seleccionar Fecha Final" id="imgfecfin" 
						style="cursor:pointer; vertical-align: middle"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" style="cursor:pointer; vertical-align: middle" 
						alt="Buscar" id="imgbuscar" onclick="javascript:fc_Buscar();"></a>
				</td>
			</tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0"  width="98%" style="margin-left:10px">
			<tr>
				<td width="96%">
					<table class="" width="100%" cellspacing="0" cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td><div style="overflow: auto; height: 260px;width:100%" class="">
								<display:table name="sessionScope.listaProcesos" cellpadding="0" cellspacing="1"
									decorator="com.tecsup.SGA.bean.ProcesosDecorator" style="border: 1px solid #AD222F;width:98%">
									<display:column property="checkSelBandeja" title="Sel." headerClass="grilla" class="tablagrilla"
										style="width:5%; text-align: center" />
									<display:column property="codProceso" title="C�digo" headerClass="grilla" class="tablagrilla" style="width:11%;text-align:center"/>
									<display:column property="dscProceso" title="Proceso" headerClass="grilla" class="tablagrilla"
										style="text-align: left;width:27%"/>
									<display:column property="dscUnidadFuncional" title="Unidad Funcional" headerClass="grilla" class="tablagrilla"
										style="text-align: left;width:27%"/>
									<display:column property="fecRegistro" title="Fecha Registro" headerClass="grilla" class="tablagrilla" style="width:15%;text-align: center"/>
									<display:column property="linkOferta" title="Oferta Asociada" headerClass="grilla" class="tablagrilla" style="width:15%;text-align: center"/>
									
									<display:setProperty name="basic.empty.showtable" value="true"  />
									<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto_libre' style='text-align:right'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
									<display:setProperty name="paging.banner.placement" value="bottom"/>
									<display:setProperty name="paging.banner.item_name" value="<span  style='text-align:right'>Registro</span>" />
									<display:setProperty name="paging.banner.items_name" value="<span  style='text-align:right'>Registros</span>" />
									<display:setProperty name="paging.banner.no_items_found" value="<span id='1' class='texto_libre'>No se encontraron registros</span>" />
									<display:setProperty name="paging.banner.one_item_found" value="<span id='2' class='texto_libre'>Un registro encontrado</span>" />
									<display:setProperty name="paging.banner.all_items_found" value="<span id='3' class='texto_libre' style='text-align: right; width: 100%'></span>" />
								</display:table></div>
							</td>
						</tr>
					</table>
				</td>	
				<td width="2%" valign="top"><br><br>
					<table cellspacing="0" cellpadding="0" border="0" bordercolor="red">
						<tr>
							<td valign="top">
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
								&nbsp;<img src="${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer;" id="imgagregar" onclick="fc_NuevaProceso();" alt="Agregar Proceso"></a><br>
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmodificar','','${ctx}/images/iconos/modificar2.jpg',1)">
								&nbsp;<img src="${ctx}/images/iconos/modificar1.jpg" style="cursor:pointer;" id="imgmodificar" onclick="fc_EditaProceso();" alt="Modificar Proceso"></a><br>
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
								&nbsp;<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_EliminarProceso();" alt="Eliminar Proceso"></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
		</table>
		<table align="center">
			<tr id="BtRevision">
				<td align=center>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgpostulantes','','${ctx}/images/botones/postulantes2.jpg',1)">
					<img alt="Postulantes" style="cursor:pointer"  onclick="fc_Postulantes();" id="imgpostulantes" src="${ctx}/images/botones/postulantes1.jpg"></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnOferta','','${ctx}/images/botones/oferta2.jpg',1)">
					<img alt="Oferta" style="cursor:pointer" onclick="fc_AbreOferta();" id="btnOferta" src="${ctx}/images/botones/oferta1.jpg"></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEvaluadores','','${ctx}/images/botones/evaluadores2.jpg',1)">
					<img alt="Evaluadores" style="cursor:pointer" onclick="fc_Evaluadores();" id="imgEvaluadores" src="${ctx}/images/botones/evaluadores1.jpg"></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" style="cursor:pointer" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar" onclick="fc_Regresar();"></a>
				</td>
			</tr>
		</table>
	</form:form>
	<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txtFecIni",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgfecini",
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "txtFecFin",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgfecfin",
			singleClick    :    true
		});
	</script>
	<script type="text/javascript" language="javascript">
		if ( document.getElementById("txhTipoEtapa").value != "")
		{
			objRevision = document.getElementById("rbtRevision");
			objSeleccion = document.getElementById("rbtSeleccion");
			objOferta = document.getElementById("btnOferta");
			
			objRevision.checked = false;
			objSeleccion.checked = false;
			objOferta.style.display = "none";
			 
			if ( document.getElementById("txhTipoEtapa").value == 1)
			{
				objRevision.checked = true;
				objOferta.style.display = "inline-block";
			}
			else objSeleccion.checked = true;
		}
		
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value != "" )
		{
			if ( objMsg.value == "ELI" ) alert('Se elimin� exitosamente.');
			else if ( objMsg.value == "NOELI" ) alert('Problemas al eliminar. Consulte con el administrador.');
			else if ( objMsg.value == "EN_PROC" ) alert('Uno o m�s postulantes est�n en proceso de revisi�n. El proceso no puede ser eliminado.');
		}
		objMsg.value = "";
	</script>	
</body>
</html>