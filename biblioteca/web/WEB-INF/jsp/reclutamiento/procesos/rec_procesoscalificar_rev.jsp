<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	function onLoad()
	{
		if (document.getElementById("txhNombreDoc").value != "")
		{
			document.getElementById("lblInforme").innerText = document.getElementById("txhNombreDoc").value;
		}
	}
	
	function fc_Regresar(){					
		codSel = document.getElementById("txtCodProceso").value;
		location.href = "${ctx}/reclutamiento/procesos_postulantes.html"
						+"?txhCodProceso=" + codSel
						+"&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
	}
	
	function fc_Grabar(){
		if ( fc_Trim( document.getElementById("cboCalificacion").value ) == "")
		{
			alert("Debe seleccionar una calificaci�n.");
			return false;
		}
		if ( document.getElementById("txhCodEtapa").value == "0002" )
		{
			if ( fc_Trim(document.getElementById("txaComentario").value) == "")
			{
				document.getElementById("txaComentario").value='';				
			}
		}
		if ( !confirm('�Est� seguro de grabar? Luego de grabar no se podr� modificar.') ) return false;
		
		document.getElementById("txhAccion").value = "GRABAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_Adjuntar()
	{
		Fc_Popup("${ctx}/reclutamiento/adjuntarArchivoInfFinal.html"
				 + "?txhCodPostulante=" + document.getElementById("txhCodPostulante").value
				 + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value
				,450,120,"");
	}
	
	function verArchivo()
	{
		strRuta = "<%=(( request.getAttribute("rutaServidor")==null)?"": request.getAttribute("rutaServidor"))%>";
		if ( strRuta != "")
			window.open(strRuta + document.getElementById("txhNombreAbreDoc").value );
	}
</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/calificacion_rev.html" >
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="message" id="txhMsg"/>
	<form:hidden path="codPostulantesSel" id="txhCodPostulante"/>
	<form:hidden path="estPostulantesSel" id="txhCodEstado"/>
	<form:hidden path="codEstRevJefe" id="txhEstadoRevJefe"/>
	<form:hidden path="codEtapa" id="txhCodEtapa"/>
	
	<form:hidden path="infFinal" id="txhNombreDoc"/>
	<form:hidden path="infFinalGen" id="txhNombreAbreDoc"/>
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif"
			onclick="javascript:fc_Regresar();" style="cursor: hand" alt="Regresar"></td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 9px">
		<tr style="width: 100%">
			<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
			<td background="${ctx}/images/reclutamiento/centro.jpg" width="755px" height="27px" 
				class="opc_combo">Calificaci�n del Postulante</td>
			<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
		</tr>
	</table>
	<table cellpadding="2" cellspacing="2" class="tabla" 
		style="margin-left: 10px; width: 96%; margin-top: 5px">			
		<tr>
			<td width="10%">C�digo:</td>
			<td>
				<form:input cssClass="cajatexto_1" readonly="true" cssStyle="width:70px"
				 path="codProceso" id="txtCodProceso"/>
			</td>
			<td>Proceso:</td>
			<td>
				<form:input cssClass="cajatexto_1" readonly="true" path="dscProceso" cssStyle="width:300px"/>
			</td>
		</tr>
		<tr>
			<td>Postulante:</td>
			<td width="30%">
				<form:input cssClass="cajatexto_1" readonly="true" cssStyle="width:200px" path="nomPostulantesSel"/>
			</td>
			<td>Etapa:</td>
			<td>
				<form:label path="dscEtapa">${control.dscEtapa}</form:label>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="100%" style="margin-left: 10px; width: 98%; margin-top: 5px">
		<tr>
			<td colspan="3">&nbsp;
				<display:table name="sessionScope.listEvalPostRev" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.EvaluacionesByEvaluadorDecorator" 
					style="border: 1px solid #6b6d6b ;width:98%">
					
					<display:column property="nomPostulante"  title="Evaluador" headerClass="grilla" 
						class="tablagrilla" style="text-align: left; width:30%" />
					<display:column property="dscTipoEvaluacion" title="Tipo Evaluaci�n" headerClass="grilla" 
						class="tablagrilla" style="text-align: left; width:30%" />
					<display:column property="dscCalificacion" title="Calificacion" headerClass="grilla" 
						class="tablagrilla" style="text-align: left; width:20%" />
					<display:column property="textAreaComentario" title="Comentario" headerClass="grilla" 
						class="tablagrilla" style="text-align: center; width:20%" />
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</td>
		</tr>
		<tr height="30px">
			<td class="texto_bold" id="tdInfFinal" >Informe final: </td>
			<td class="texto_bold" style="width: 40%">
				<span id="lblInforme" onclick="verArchivo();" style="cursor: pointer;vertical-align: middle"></span>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAdjuntar','','${ctx}/images/iconos/adjuntar2.jpg',1)">
				<img alt="Adjuntar Informe Final" src="${ctx}/images/iconos/adjuntar1.jpg" id="btnAdjuntar" 
				style="cursor:pointer; vertical-align: middle" onclick="javascript:fc_Adjuntar();"></a>
			</td>
			<td class="texto_bold" style="width: 50%" align="right">Calificaci�n:
				<form:select path="codCalificacion" id="cboCalificacion" cssClass="cajatexto_o" 
					cssStyle="width:150px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listCalificaciones!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.listCalificaciones}" />
					</c:if>
				</form:select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr height="30px" id="trTxa" style="vertical-align: top">
			<td class="texto_bold" align="left" style="vertical-align: top; width: 10%">Comentario: &nbsp;&nbsp;</td>
			<td class="texto_bold" align="left" style="vertical-align: top; " colspan="2">
				<form:textarea path="comentario" id="txaComentario" cssClass="cajatexto" rows="3" cols="35"/>
			</td>
		</tr>
		<tr height="30px">
			<td align=center colspan="3">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="btnGrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgcancelar" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>
			</td>
		</tr>
	</table>
	<script type="text/javascript" language="javascript">
		if ( document.getElementById("txhCodEtapa").value != "0002" )
		{
			document.getElementById("trTxa").style.display = "none";
			document.getElementById("tdInfFinal").style.display = "none";
			document.getElementById("lblInforme").style.display = "none";
			document.getElementById("btnAdjuntar").style.display = "none";
		}
		
		if ( fc_Trim(document.getElementById("txhCodEstado").value) !=
				fc_Trim(document.getElementById("txhEstadoRevJefe").value) )
		{
			document.getElementById("txaComentario").disabled = true;
			document.getElementById("cboCalificacion").disabled = true;
			document.getElementById("btnGrabar").style.display = "none";
			//document.getElementById("btnAdjuntar").disabled = true;
		}
		
		objMsg = document.getElementById("txhMsg");
		
		if ( objMsg.value != "" )
		{
			if ( objMsg.value == "ERROR" ) alert('Problemas al Grabar. Consulte con el administrador.');
			if ( objMsg.value == "OK" )
			{
				alert('Se grab� exitosamente');
				fc_Regresar();
			}
		}
		objMsg.value = "";
	</script>	
</form:form>
</html>