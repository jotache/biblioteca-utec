<%@ include file="/taglibs.jsp"%>
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_rec-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
<script type="text/javascript">
	function onLoad(){}
		
	function fc_Grabar(){	
		if ( document.getElementById("txtDscOferta").value == "" ){		
			alert('Debe ingresar la descripci�n de la oferta.');
			return false;
		}		
		if ( document.getElementById("txtNroVacantes").value == "" ){				
			alert('Debe ingresar el n�mero de vacantes.');
			return false;
		}		
		if ( document.getElementById("txaTextoPublicar").value == "" )
		{
			alert('Debe ingresar el texto a publicar.');
			return false;
		}
		
		if ( document.getElementById("txtFecIni").value == "" )
		{
			alert('Debe ingresar la fecha inicio de publicaci�n.');
			return false;
		}
		
		if ( document.getElementById("txtFecFin").value == "" )
		{
			alert('Debe ingresar la fecha fin de publicaci�n.');
			return false;
		}
		
		if (Fc_RestaFechas(document.getElementById("txtFecIni").value,"dd/MM/yyyy"
				,document.getElementById("txtFecFin").value,"dd/MM/yyyy") < 0 )
		{
			alert("Fecha de inicio debe ser mayor a fecha fin de publicaci�n.");
			document.getElementById("txtFecIni").focus();
			return false;
		}
			
		if (!confirm('�Est� seguro de grabar?')) return false;
		
		document.getElementById("txhAccion").value = "GRABAR";
		document.getElementById("frmMain").submit();
	}
</script>
</head>
<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/procesos_oferta.html" >
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="codProceso" id="txhCodProceso"/>
	<form:hidden path="codOferta" id="txhCodOferta"/>
	<form:hidden path="message" id="txhMessage"/>
	<form:hidden path="indEdicicion" id="txhIndEdicicion"/>
		<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 10px; margin-top: 5px">
			<tr>
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">Creaci�n de Oferta</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="1" cellspacing="2" class="tabla"
			style="width:95%; margin-left: 10px; margin-top: 5px" >
			<tr>
				<td>Proceso:</td>
				<td><form:input id="txtDscProceso" path="dscProceso" cssClass="cajatexto_1" size="50" readonly="true"/></td>
			</tr>
			<tr>
				<td>Unidad funcional:</td>
				<td><form:input id="txtDscUnidadFuncional" path="dscUnidadFuncional" cssClass="cajatexto_1" size="50" readonly="true"/></td>
			</tr>
			<tr>
				<td>Oferta:</td>
				<td><form:input id="txtDscOferta" path="dscOferta" cssClass="cajatexto" size="50"
					onkeypress="javascript:fc_ValidaTextoEspecialAndNumeroTodo();" 
					onblur="javascript:fc_ValidaNumerosAndLetrasFinalTodo(this.id,'Oferta');fc_maxlength(this, 150);"
					maxlength="150"/>&nbsp;<b>(*)</b>
				</td>
			</tr>
			<tr>
				<td>Nro. Vacantes:</td>
				<td><form:input id="txtNroVacantes" path="nroVacantes" cssStyle="text-align:right;"
						onkeypress="javascript:fc_PermiteNumeros();"
						onblur="javascript:fc_ValidaNumeroOnBlur('txtNroVacantes');fc_maxlength(this, 3);"
						maxlength="3" cssClass="cajatexto" size="10"/>&nbsp;<b>(*)</b></td>
			</tr>
			<tr>
				<td>Texto a Publicar:</td>
				<td><form:textarea path="textoPublicar" id="txaTextoPublicar" cssClass="cajatexto" cols="50"
						onkeypress="javascript:fc_ValidaTextoEspecialAndNumeroTodo();" 
						onblur="javascript:fc_ValidaNumerosAndLetrasFinalTodo(this.id,'texto a publicar.');fc_maxlength(this, 800);"
						rows="3"/>&nbsp;<b>(*)</b>
				</td>
			</tr>
			<tr>
				<td>Fecha Inicio Publicaci�n:</td>
				<td><form:input maxlength="10" path="fecIniPublicacion" id="txtFecIni"
					onkeypress="javascript:fc_ValidaFecha('txtFecIni');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecIni');"  
					cssClass="cajatexto" cssStyle="width:70px"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" alt="Seleccionar Fecha Final" 
						id="imgFecIni" style="cursor:pointer; vertical-align: middle"></a>&nbsp;<b>(*)</b>
				</td>
			</tr>
			<tr>
				<td>Fecha Fin Publicaci�n:</td>
				<td><form:input maxlength="10" path="fecFinPublicacion" id="txtFecFin"
					onkeypress="javascript:fc_ValidaFecha('txtFecFin');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecFin');" 
					cssClass="cajatexto" cssStyle="width:70px"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" alt="Seleccionar Fecha Final" 
						id="imgFecFin" style="cursor:pointer; vertical-align: middle"></a>&nbsp;<b>(*)</b>
				</td>
			</tr>
		</table>
		<table align=center id="tblBotonesUno" style="margin-top: 5px">
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="fc_Grabar();" ></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" style="cursor:pointer;" onclick="window.close();"></a>
					&nbsp;
				</td>
			</tr>
		</table>
		<table align=center id="tblBotonesDos" style="display:none;margin-top: 9px">
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCerrar','','${ctx}/images/botones/cerrar2.jpg',1)">
					<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" onclick="window.close();" ID="btnCerrar" NAME="btnCerrar" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
	</form:form>
	<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txtFecIni",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFecIni",
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "txtFecFin",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFecFin",
			singleClick    :    true
		});
	</script>
	<script type="text/javascript" language="javascript">

		strMsg = document.getElementById("txhMessage").value;
		if ( strMsg != "")
		{
			alert(strMsg);
			if ( document.getElementById("txhAccion").value == "OK" )
			{
				window.opener.document.getElementById("txhAccion").value = "BUSCAR";
				window.opener.document.getElementById("frmMain").submit();
				window.close();
			}
		}
		
		if ( document.getElementById("txhIndEdicicion").value == "1" )
		{
			document.getElementById("txtDscOferta").disabled = true;
			document.getElementById("txtNroVacantes").disabled = true;
			document.getElementById("txaTextoPublicar").disabled = true;
			document.getElementById("txtFecIni").disabled = true;
			document.getElementById("txtFecFin").disabled = true;
			
			document.getElementById("imgFecFin").disabled = true;
			document.getElementById("imgFecIni").disabled = true;
			
			document.getElementById("tblBotonesUno").style.display = "none"; 
			document.getElementById("tblBotonesDos").style.display = "inline-block";
		}
		document.getElementById("txhAccion").value = "";
	</script>
</body>
</html>