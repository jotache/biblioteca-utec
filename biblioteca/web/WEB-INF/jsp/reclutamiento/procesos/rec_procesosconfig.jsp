<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_rec-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_rec-textos.js" language="JavaScript;" type="text/JavaScript"></script>
<script language="javascript" type="text/javascript">
<!--
	/*VARIABLES GLOBALES*/
	var objRowAreaInt = null; //Fila Seleccionada de area de interes.
	var codSelAreaInt = ""; //Codigo del �rea de interes seleccioanda.
	
	var objRowAreaEst = null; //Fila Seleccionada de area de interes.	
	var codSelAreaEst = ""; //Codigo del �rea de estudios seleccioanda.

	var objRowAreaNivEst = null;
	var codSelAreaNivEst = "";
	
	var objRowInstEdu = null;
	var codSelInstEdu = "";
	
	function onLoad(){}
	
	function fc_CambiaTipo(strTipoEtapa)
	{
		var objRevision = document.getElementById("rbtRevision");
		var objSeleccion = document.getElementById("rbtSeleccion");
		
		var objSeccionRevision = document.getElementById("trSeccionRevision");
		var objSeccionSeleccion = document.getElementById("trSeccionSeleccion");
		
		objRevision.checked = false;
		objSeleccion.checked = false;
		objSeccionRevision.style.display = "none";
		objSeccionSeleccion.style.display = "none";
		
		if ( strTipoEtapa == 1)
		{
			objRevision.checked = true;
			objSeccionRevision.style.display = "inline-block";			
		}
		else
		{
			objSeleccion.checked = true;
			objSeccionSeleccion.style.display = "inline-block";
		}
		document.getElementById("txhTipoEtapa").value = strTipoEtapa;
	}
	
	function fc_Grabar()
	{
		var objUnidad = document.getElementById("cboUnidadFuncional");
		var objDescripcion = document.getElementById("txtDscProceso");		
		
		if (objDescripcion.value == "")
		{
			alert('Debe ingresar la descripci�n del proceso.');
			return;
		}
		
		if (objUnidad.value == "")
		{
			alert('Debe seleccionar una unidad funcional.');
			return;
		}
		
		if ( document.getElementById("txhTipoEtapa").value == "" ||
			 document.getElementById("txhTipoEtapa").value == "1" )
		{
			/* JCMU - Las �reas de inter�s ya no son obligatorias
			if ( document.getElementById("txhCodAreasIntSeleccionadas").value == "" )
			{
				alert(mstrDebeSelAreaInteres);
				return;
			}*/
			var objSalIni = document.getElementById("txtSalarioIni");
			var objSalFin = document.getElementById("txtSalarioFin");
			
			if ( objSalIni.value != "" && objSalFin.value != "")
				if ( parseFloat(objSalFin.value) < parseFloat(objSalIni.value) )
				{
					alert('La expectativa salarial inicial debe ser menor que la final.');
					return false;
				} 
			
			/*if(document.getElementById("codTipoMoneda").value=="-1" )
			  { alert("Debe seleccionar un tipo de moneda.");
			    return false;
			  }*/
				
			var objCboTipoEdad = document.getElementById("cboTipoEdad");
			var objCboTipoEdad1 = document.getElementById("cboTipoEdad1");
			var objTxtEdad = document.getElementById("txtEdad");
			var objTxtEdad1 = document.getElementById("txtEdad1");
			
			if ( ( objCboTipoEdad.value != "" && objTxtEdad.value == "") ||
				 ( objCboTipoEdad1.value != "" && objTxtEdad1.value == "") )
			{
				alert("Debe ingresar un edad por cada simbolo de edad seleccionado.");
				return false;
			}
		}
		else
		{
			if ( document.getElementById("cboProcRevAsociado").value == "" )
			{
				alert('Debe seleccionar un proceso de revisi�n.');
				return;
			}
		}
		
		//asigando los flag de disponibilidad de viaje.
		var objFlag = document.getElementById("rbtDentroPais");
		var objFlag1 = document.getElementById("rbtFueraPais");

		document.getElementById("txhIndViaje").value = "0";
		document.getElementById("txhIndViaje1").value = "0";
		if ( objFlag.checked ) document.getElementById("txhIndViaje").value = "1";
		if ( objFlag1.checked ) document.getElementById("txhIndViaje1").value = "1";

		//Asignado los flag para Puesto al que postula (Doc o Adm).
		var objFlag = document.getElementById("rbtDoc");
		var objFlag1 = document.getElementById("rbtAdm");
		document.getElementById("txhIndPostDoc").value = "0";
		document.getElementById("txhIndPostAdm").value = "0";
		if ( objFlag.checked ) document.getElementById("txhIndPostDoc").value = "1";
		if ( objFlag1.checked ) document.getElementById("txhIndPostAdm").value = "1";

		
		if ( confirm('�Est� seguro de grabar?') )
		{
			document.getElementById("txhOperacion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
	
	function fc_AddAreaInteres()
	{   
		Fc_Popup("${ctx}/reclutamiento/buscar_areainteres.html",400,320,"");
	}

	function fc_AddAreaEstudio(){
		Fc_Popup("${ctx}/reclutamiento/buscar_areaestudios.html",400,320,"");
	}

	function fc_AddAreaNivelEstudios(){
		Fc_Popup("${ctx}/reclutamiento/buscar_areanivelestudios.html",400,320,"");
	}
	
	function fc_AddInstitucionEducativa(){
		Fc_Popup("${ctx}/reclutamiento/buscar_institucioneducativa.html",400,320,"");
	}

	function fc_DelInstitucionEducativa()	{	
		var objCodigos = document.getElementById("txhCodInstEduSeleccionadas");
		if ( objRowInstEdu == null )
		{
			alert('Debe seleccionar un Instituci�n Educativa.');
			return;
		}
		objRowInstEdu.style.display = "none";		
		codSelInstEdu = codSelInstEdu + "|";
		objCodigos.value = objCodigos.value.replace(codSelInstEdu,"");		
		objRowInstEdu = null;
		codSelInstEdu = "";			
	}
	
	function fc_DelAreaEstudios(){		
		var objCodigos = document.getElementById("txhCodAreasEstSeleccionadas");
		if ( objRowAreaEst == null )
		{
			alert('Debe seleccionar un �rea de estudio.');
			return;
		}
		objRowAreaEst.style.display = "none";		
		codSelAreaEst = codSelAreaEst + "|";
		objCodigos.value = objCodigos.value.replace(codSelAreaEst,"");		
		objRowAreaEst = null;
		codSelAreaEst = "";		
	}

	function fc_DelAreaNivelEstudios(){
		var objCodigos = document.getElementById("txhCodAreasNivEstSeleccionadas");
		if ( objRowAreaNivEst == null )
		{
			alert('Debe seleccionar un nivel de estudio.');
			return;
		}
		objRowAreaNivEst.style.display = "none";
		codSelAreaNivEst = codSelAreaNivEst + "|";
		objCodigos.value = objCodigos.value.replace(codSelAreaNivEst,"");		
		objRowAreaNivEst = null;
		codSelAreaNivEst = "";	
	}
	
	function fc_DelAreaInteres(){
		var objCodigos = document.getElementById("txhCodAreasIntSeleccionadas");
		if ( objRowAreaInt == null )
		{
			alert('Debe seleccionar un �rea de interes.');
			return;
		}
		objRowAreaInt.style.display = "none";
		//Saco el codigo seleccionado de la lista de codigos.
		codSelAreaInt = codSelAreaInt + "|";
		objCodigos.value = objCodigos.value.replace(codSelAreaInt,"");
		//Reinicio las variables globales;
		objRowAreaInt = null;
		codSelAreaInt = "";		
	}
	function fc_seleccionarObject(objRbt, codNivel)
	{
		if ( codSelAreaInt == codNivel )
		{
			codSelAreaInt = "";
			objRowAreaInt = null;
			return false;
		}
		objTd = objRbt.parentNode;
		//Asigno valor a las Variables Globales.
		codSelAreaInt = codNivel;
		objRowAreaInt = objTd.parentNode;
	}

	function fc_seleccionarEstObject(objRbt, codNivel) {
		if ( codSelAreaEst == codNivel )
		{
			codSelAreaEst = "";
			objRowAreaEst = null;
			return false;
		}
		objTd = objRbt.parentNode;
		codSelAreaEst = codNivel;
		objRowAreaEst = objTd.parentNode;
	}
	
	function fc_seleccionarNivEstObject(objRbt, codNivel) {
		/*objRowAreaNivEst = null;
		codSelAreaNivEst = "";*/
		if ( codSelAreaNivEst == codNivel ){		
			codSelAreaNivEst = "";
			objRowAreaNivEst = null;
			return false;
		}
		objTd = objRbt.parentNode;
		codSelAreaNivEst = codNivel;
		objRowAreaNivEst = objTd.parentNode;
	}
	
	function fc_seleccionarInstEduObject(objRbt, codNivel) {			
		if ( codSelInstEdu == codNivel ){		
			codSelInstEdu = "";
			objRowInstEdu = null;
			return false;
		}
		objTd = objRbt.parentNode;
		codSelInstEdu = codNivel;
		objRowInstEdu = objTd.parentNode;
	}	
	
	function fc_AddDetalleAreaEstudios(codNivel2, dscNivel2){
		var objCodigos = document.getElementById("txhCodAreasEstSeleccionadas");
		if ( objCodigos.value.indexOf(codNivel2) > -1 ){		
			alert('El �rea de estudio seleccionada ya se encuentra en lista.');
			return false;
		}		
		var objTabla = document.getElementById("tblAreasEstudio");
		var tbody = objTabla.getElementsByTagName("TBODY")[0];
		var objSpan = document.getElementById("pagNoItemFoundEst");
		if ( objSpan != null )
			if ( objSpan.innerHTML != "" ){			
				objSpan.innerHTML = "";
				tbody.deleteRow(0);
			}
		//objFil = document.createElement("<tr id='NFIL' ></tr>");
		var objFil;
		objFil = document.createElement("tr");
		objFil.id="NFIL";
		
		//objCol0 = document.createElement("<TD  class='grillaDetalle' align='center'  ></td>");				
		var objCol0 = document.createElement("td");
				
		//objCol0.style.class='grillaDetalle';
		objCol0.style.align="center";
		objCol0.innerHTML = "<input type='radio' name='codDetalle' onclick=javascript:fc_seleccionarEstObject(this,'" + codNivel2 + "'); >";
	
		//objCol2 = document.createElement("<TD  class='grillaDetalle' align='left' style='text-align:left'></td>");
		var objCol2 = document.createElement("td");
		//objCol2.style.class="grillaDetalle";
		objCol2.style.align="left";
		
		
		objCol2.innerHTML = dscNivel2;
		objFil.appendChild(objCol0);
		objFil.appendChild(objCol2);
		tbody.appendChild(objFil);		
				
		objCodigos.value = objCodigos.value + codNivel2 + "|";		
		return true;		
	}

	function fc_AddDetalleAreaNivelEstudios(codNivel2, dscNivel2){
		var objCodigos = document.getElementById("txhCodAreasNivEstSeleccionadas");
		if ( objCodigos.value.indexOf(codNivel2) > -1 ){		
			alert('El nivel de estudio seleccionada ya se encuentra en lista.');
			return false;
		}		
		var objTabla = document.getElementById("tblAreasNivEstudio");
		var tbody = objTabla.getElementsByTagName("TBODY")[0];
		var objSpan = document.getElementById("pagNoItemFoundNivEst");
		if ( objSpan != null )
			if ( objSpan.innerHTML != "" ){			
				objSpan.innerHTML = "";
				tbody.deleteRow(0);
			}
		//objFil = document.createElement("<tr id='NFIL' ></tr>");
		
		var objFil = document.createElement("tr");
		objFil.id="NFIL";
				
		//objCol0 = document.createElement("<TD  class='grillaDetalle' align='center'  ></td>");
		var objCol0 = document.createElement("td");
		//objCol0.style.class="grillaDetalle";
		objCol0.style.align="center";
		
		objCol0.innerHTML = "<input type='radio' name='codDetalle' onclick=javascript:fc_seleccionarNivEstObject(this,'" + codNivel2 + "'); >";		
		//objCol2 = document.createElement("<TD  class='grillaDetalle' align='left' style='text-align:left'></td>");
		var objCol2 = document.createElement("td");
		//objCol2.style.class="grillaDetalle";
		objCol2.style.align="left";		
		objCol2.innerHTML = dscNivel2;
		
		objFil.appendChild(objCol0);
		objFil.appendChild(objCol2);
		tbody.appendChild(objFil);		
		objCodigos.value = objCodigos.value + codNivel2 + "|";
		return true;		
	}
	
	function fc_AddDetalleInstitucionEducativa(codNivel2, dscNivel2){
		var objCodigos = document.getElementById("txhCodInstEduSeleccionadas");
		if ( objCodigos.value.indexOf(codNivel2) > -1 ){		
			alert('La instituci�n seleccionada ya se encuentra en lista.');
			return false;
		}		
		var objTabla = document.getElementById("tblInstituciones");
		var tbody = objTabla.getElementsByTagName("TBODY")[0];
		var objSpan = document.getElementById("pagNoItemFoundInst");
		if ( objSpan != null )
			if ( objSpan.innerHTML != "" ){			
				objSpan.innerHTML = "";
				tbody.deleteRow(0);
			}
		//objFil = document.createElement("<tr id='NFIL' ></tr>");
		var objFil = document.createElement("tr");
		objFil.id="NFIL";
		
		//objCol0 = document.createElement("<TD  class='grillaDetalle' align='center'  ></td>");
		var objCol0 = document.createElement("td");
		//objCol0.style.class="grillaDetalle";
		objCol0.style.align="center";
		
		objCol0.innerHTML = "<input type='radio' name='codDetalle' onclick=javascript:fc_seleccionarInstEduObject(this,'" + codNivel2 + "'); >";		
		//objCol2 = document.createElement("<TD  class='grillaDetalle' align='left' style='text-align:left'></td>");
		var objCol2 = document.createElement("td");
		//objCol2.style.class="grillaDetalle";
		objCol2.style.align="left";		
		objCol2.innerHTML = dscNivel2;
		
		objFil.appendChild(objCol0);
		objFil.appendChild(objCol2);
		tbody.appendChild(objFil);		
		objCodigos.value = objCodigos.value + codNivel2 + "|";
		return true;		
	}	
	
	
	function fc_AddDetalleAreaInteres(codNivel2, dscNivel2, dscNivel1)
	{		
		var objCodigos = document.getElementById("txhCodAreasIntSeleccionadas");		
		if ( objCodigos.value.indexOf(codNivel2) > -1 )
		{
			alert('El �rea de interes seleccionada ya se encuentra en lista.');
			return false;
		}
		var objTabla = document.getElementById("tblAreasInteres");
		var tbody = objTabla.getElementsByTagName("TBODY")[0];		
		var objSpan = document.getElementById("pagNoItemFound");

		if ( objSpan != null )
			if ( objSpan.innerHTML != "" ){			
				objSpan.innerHTML = "";
				tbody.deleteRow(0);
			}
		
		//objFil = document.createElement("<tr id=NFIL ></tr>");
		 var objFil= document.createElement("tr");
		 objFil.id="NFIL";
		
		//objCol0 = document.createElement("<TD  class='grillaDetalle' align='center'  ></td>");		
		var objCol0 = document.createElement("td");
		if(objCol0 != null){
			//objCol0.style.class="grillaDetalle";
			objCol0.style.align="center";		
			objCol0.innerHTML = "<input type='radio' name='codDetalle' onclick=javascript:fc_seleccionarObject(this,'" + codNivel2 + "'); >";
			
			var objCol2 = document.createElement("td");
			//objCol2.style.class="grillaDetalle";
			objCol2.style.align="left",				
			objCol2.innerHTML = dscNivel2;

			objFil.appendChild(objCol0);
			objFil.appendChild(objCol2);
			tbody.appendChild(objFil);
		}
		
		/*objCol1 = document.createElement("<TD  class='grillaDetalle' align='left' style='text-align:left'></td>");
		objCol1.innerHTML = dscNivel1;*/
		
		//objCol2 = document.createElement("<TD  class='grillaDetalle' align='left' style='text-align:left'></td>");
				
		objCodigos.value = objCodigos.value + codNivel2 + "|";		
		return true;
	}
	
	function fc_CambiaProcRevAsoc()
	{
		var objCboProc = document.getElementById("cboProcRevAsociado");
		var objTxtDsc = document.getElementById("txtDscProceso");
		var objCboUni = document.getElementById("cboUnidadFuncional");
		
		if ( objCboProc.value != "")
		{
			var arrDscs = objCboProc.value.split("|");
			objCboUni.value = arrDscs[1];
			objTxtDsc.value = objCboProc.options[objCboProc.selectedIndex].innerText;
		} 
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
//-->
</script>
</head>
<body>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/procesos_configuracion.html" >
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="codProceso" id="txhCodProceso"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="flagEtapa" id="txhTipoEtapa"/>
	<form:hidden path="flagDisponibilidadViajar" id="txhIndViaje"/>
	<form:hidden path="flagDisponibilidadViajar1" id="txhIndViaje1"/>

	<form:hidden path="puestoPostulaDoc" id="txhIndPostDoc"/>
	<form:hidden path="puestoPostulaAdm" id="txhIndPostAdm"/>

	<form:hidden path="message" id="txhMessage"/>
	<form:hidden path="codAreasIntSeleccionadas" id="txhCodAreasIntSeleccionadas"/>
	<form:hidden path="codAreasEstSeleccionadas" id="txhCodAreasEstSeleccionadas"/>
	<form:hidden path="codAreasNivEstSeleccionadas" id="txhCodAreasNivEstSeleccionadas"/>	
	<form:hidden path="codInstEduSeleccionadas" id="txhCodInstEduSeleccionadas"/>	
	
	
	<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 15px; margin-top: 5px">
		<tr>
			<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
			<td background="${ctx}/images/reclutamiento/centro.jpg" width="473px" height="27px" 
				class="opc_combo">Configuraci�n del Proceso</td>
			<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
		</tr>
	</table>
	<table class="tabla" style="width:95%; margin-left: 15px; margin-top: 5px"  
		cellspacing="2" cellpadding="2" border="0" bordercolor="red">		
		<tr>
			<td colspan="3" class="texto_bold">&nbsp;
				Proceso :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<form:input path="dscProceso" id="txtDscProceso" cssClass="cajatexto" maxlength="200"
				 	onblur="fc_maxlength(this, 200);"
					cssStyle="width:310px"/>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="texto_bold" valign="top">&nbsp;
				Unidad Funcional Solicitante :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<form:select path="codUnidadFuncional" id="cboUnidadFuncional" cssClass="combo_o" 
					cssStyle="width:337px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listUnidadFuncional!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.listUnidadFuncional}" />
					</c:if>
				</form:select>
			</td>
		</tr>		
		<tr>
			<td colspan="3" class="texto_bold" valign="top">&nbsp;
				Etapa :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" id="rbtRevision" <c:if test="${control.flagEtapa!='1'}">
				<c:out value=" checked=checked " /></c:if> onclick="fc_CambiaTipo('1');">Revisi�n&nbsp;
				<input type="radio" id="rbtSeleccion" <c:if test="${control.flagEtapa=='1'}">
				<c:out value=" checked=checked " /></c:if> onclick="fc_CambiaTipo('2');">Selecci�n
			</td>
		</tr>
		<tr id="trSeccionRevision">
			<td colspan="2" >
				<table width="100%" border="0" bordercolor="blue">
					<TR>
						<td class="texto_bold" width="48%">&nbsp;Criterios:</td>
						<td width="2%">&nbsp;</td>
						<td width="30%">&nbsp;</td>
					</tr>
					
					<tr>	
						<td colspan="3" >
							<table  height="58px" border="0">
								
								<tr class="texto">
								  <td width="180px">Sexo : </td>
								  <td width="400px"><form:select path="codSexo" id="cboSexo" cssClass="cajatexto" 
											cssStyle="width:130px">
											<%--<form:option value="">--Ambos--</form:option>--%>
											<c:if test="${control.listSexo!=null}">
											<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
												items="${control.listSexo}" />
											</c:if>
										</form:select></td>
								  <td bordercolor="blue">&nbsp;</td>
							  </tr>
								<tr class="texto">
								  <td valign="top">Nivel de Estudio : </td>
								  <td>
								  	<%--
								  	<form:select path="codGradoAcedemico" id="cboGradoAcedemico" cssClass="cajatexto" 
											cssStyle="width:130px">
											<form:option value="">---Seleccione---</form:option>
											<c:if test="${control.listGradoAcedemico!=null}">
											<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
												items="${control.listGradoAcedemico}" />
											</c:if>
										</form:select>
										--%>
										<div style="overflow: auto; width: 100%; height: 70px">
											<display:table name="control.listAreasNivelEstudios" cellpadding="0" cellspacing="1"
												decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
												style="border: 1px solid white;width:95%" id="tblAreasNivEstudio">
												<display:column property="rbtSelNivEstObject" title="Sel" headerClass="grilla2" class="grillaDetalle"
													style="width:30px" />
												<display:column property="descripcion" title="Niveles de Estudio" headerClass="grilla2" class="grillaDetalle"
													style="text-align: left;"/>	
												
												<display:setProperty name="basic.empty.showtable" value="true"  />
												<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center' id='pagNoItemFoundNivEst' >No se encontraron registros</td></tr>"  />
												<display:setProperty name="paging.banner.placement" value="bottom"/>
												<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
												<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
												<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFoundNivEst' class='texto'>No se encontraron registros</span>" />
												<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
												<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
												<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
												<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
											</display:table>
										</div>										
										
								  </td>
								  <td valign="top" bordercolor="blue"> <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar_ne','','${ctx}/images/iconos/agregar2.jpg',1)"> <img src="${ctx}/images/iconos/agregar1.jpg" align=top style="cursor:hand;" id="imgagregar_ne" onclick="javascript:fc_AddAreaNivelEstudios();" /></a><br>
                                  <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgeliminar_ne','','${ctx}/images/iconos/quitar2.jpg',1)"> <img src="${ctx}/images/iconos/quitar1.jpg" align=top style="cursor:hand;" id="imgeliminar_ne" onclick="javascript:fc_DelAreaNivelEstudios();" /> </a></td>
							  </tr>
								<tr class="texto">
								  <td valign="top">&Aacute;rea de Inter&eacute;s : </td>
								  <td align="left"><div style="overflow: auto; width: 100%; height: 70px">
								<display:table name="control.listAreasInteres" cellpadding="0" cellspacing="1"
									decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
									style="border: 1px solid white;width:95%" id="tblAreasInteres"><display:column property="rbtSelObject" title="Sel" headerClass="grilla2" class="grillaDetalle"
										style="width:30px" />
									<display:column property="dscValor1" title="Areas de Inter�s" headerClass="grilla2" class="grillaDetalle"
										style="text-align: left;"/>									
									
									<display:setProperty name="basic.empty.showtable" value="true"  />
									<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center' id='pagNoItemFound' >No se encontraron registros</td></tr>"  />
									<display:setProperty name="paging.banner.placement" value="bottom"/>
									<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
									<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
									<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFound' class='texto'>No se encontraron registros</span>" />
									<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
									<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
									<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
									<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
									<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
									<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
									<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
								</display:table>
							</div>
							
							    <%-- request.getSession().removeAttribute("listaAreasInteres"); --%>
								</td>
								  <td bordercolor="blue" valign="top"> <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)"> <img src="${ctx}/images/iconos/agregar1.jpg" align=top style="cursor:hand;" id="imgagregar" onclick="javascript:fc_AddAreaInteres();" /></a><br>
                                  <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgeliminar','','${ctx}/images/iconos/quitar2.jpg',1)"> <img src="${ctx}/images/iconos/quitar1.jpg" align=top style="cursor:hand;" id="imgeliminar" onclick="javascript:fc_DelAreaInteres();" /> </a></td>
							  </tr>
								<tr class="texto">
									<td valign="top">�rea de Estudio :</td>
									<td align="left">
										<div style="overflow: auto; width: 100%; height: 70px">

											<display:table name="control.listAreaEstudio" cellpadding="0" cellspacing="1"
												decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
												style="border: 1px solid white;width:95%" id="tblAreasEstudio">
												<display:column property="rbtSelEstObject" title="Sel" headerClass="grilla2" class="grillaDetalle"
													style="width:30px" />
												<display:column property="descripcion" title="Areas de Estudio" headerClass="grilla2" class="grillaDetalle"
													style="text-align: left;"/>	
												
												<display:setProperty name="basic.empty.showtable" value="true"  />
												<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center' id='pagNoItemFoundEst' >No se encontraron registros</td></tr>"  />
												<display:setProperty name="paging.banner.placement" value="bottom"/>
												<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
												<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
												<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFoundEst' class='texto'>No se encontraron registros</span>" />
												<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
												<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
												<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
												<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
											</display:table>
										</div>
										<%-- request.getSession().removeAttribute("listaAreasEstudios"); --%>
								  </td>
									<td align="center" valign="top">
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar_est','','${ctx}/images/iconos/agregar2.jpg',1)">
											<img src="${ctx}/images/iconos/agregar1.jpg" align=top style="cursor:hand;" id="imgagregar_est" onclick="javascript:fc_AddAreaEstudio();" /></a><br>
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgeliminar_est','','${ctx}/images/iconos/quitar2.jpg',1)">
											<img src="${ctx}/images/iconos/quitar1.jpg" align=top style="cursor:hand;" id="imgeliminar_est" onclick="javascript:fc_DelAreaEstudios();" />
									</td>
								</tr>
								
								<tr class="texto" valign="top">
									<td>Instituci�n Acad�mica :</td>
									<td><%--<form:select path="codInstitucionAcademica" id="cboInstitucionAcedemica" cssClass="cajatexto" 
											cssStyle="width:393px">
											<form:option value="">---Seleccione---</form:option>
											<c:if test="${control.listInstitucionAcademica!=null}">
											<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
												items="${control.listInstitucionAcademica}" />
											</c:if>
										</form:select>--%>

									<div style="overflow: auto; width: 100%; height: 70px">
											<display:table name="control.listInstEdu" cellpadding="0" cellspacing="1"
												decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator" 
												style="border: 1px solid white;width:95%" id="tblInstituciones">
												<display:column property="rbtSelInstEduObject" title="Sel" headerClass="grilla2" class="grillaDetalle"
													style="width:30px" />
												<display:column property="descripcion" title="Instituciones educativas" headerClass="grilla2" class="grillaDetalle"
													style="text-align: left;"/>	
												
												<display:setProperty name="basic.empty.showtable" value="true"  />
												<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center' id='pagNoItemFoundInst' >No se encontraron registros</td></tr>"  />
												<display:setProperty name="paging.banner.placement" value="bottom"/>
												<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
												<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
												<display:setProperty name="paging.banner.no_items_found" value="<span id='pagNoItemFoundInst' class='texto'>No se encontraron registros</span>" />
												<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'></span>" />
												<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
												<display:setProperty name="paging.banner.first" value="<span class='texto'>[&lt;&lt;/&lt;] {0} [<a href={3}>&gt;</a>/<a href={4}>&gt;&gt;</a>]</span>" />
												<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>&lt;&lt;</a>/<a href={2}>&lt;</a>] {0} [&gt;/&gt;&gt;]</span>" />
												<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
											</display:table>
									  </div>										
										
								  </td>
								  <td align="center"> <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar_instedu','','${ctx}/images/iconos/agregar2.jpg',1)"> <img src="${ctx}/images/iconos/agregar1.jpg" align=top style="cursor:hand;" id="imgagregar_instedu" onclick="javascript:fc_AddInstitucionEducativa();" /></a><br>
                                      <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgeliminar_instedu','','${ctx}/images/iconos/quitar2.jpg',1)"> <img src="${ctx}/images/iconos/quitar1.jpg" align=top style="cursor:hand;" id="imgeliminar_instedu" onclick="javascript:fc_DelInstitucionEducativa();" /> </a></td>
								</tr>
						  </table>
						</td>
						
					</tr>
					<tr>
						<td>
							<table width="100%" bordercolor="red" border="0">
								<tr class="texto">
									<td width="33%">Expectativa Salarial :</td>
									<td width="67%">
									    <form:select path="codTipoMoneda" id="codTipoMoneda" cssClass="cajatexto" 
											cssStyle="width:45px">
											<form:option value="-1">SEL</form:option>
											<c:if test="${control.listaTipoMoneda!=null}">
											<form:options itemValue="codTipoTablaDetalle" itemLabel="dscValor3" 
												items="${control.listaTipoMoneda}" />
											</c:if>
										</form:select>&nbsp;&nbsp;
										<form:input path="salarioIni" id="txtSalarioIni" cssClass="cajatexto" size="8" maxlength="8" 
											onkeypress="javascript:fc_ValidaNumeroDecimal();"
											onblur="javascript:fc_ValidaDecimalOnBlur('txtSalarioIni',6,2);fc_maxlength(this, 8);"/>&nbsp;y&nbsp;
										<form:input path="salarioFin" id="txtSalarioFin" cssClass="cajatexto" size="8" maxlength="8" 
											onkeypress="javascript:fc_ValidaNumeroDecimal();"
											onblur="javascript:fc_ValidaDecimalOnBlur('txtSalarioFin',6,2);fc_maxlength(this, 8);"/>
									</td>
								</tr>
							</table>
						</td>
						<td colspan="2">
							Edad :&nbsp;&nbsp;
							<form:select path="codTipoEdad" id="cboTipoEdad" cssClass="cajatexto" 
								cssStyle="width:45px">
								<form:option value="">SEL</form:option>
								<c:if test="${control.listTipoEdad!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listTipoEdad}" />
								</c:if>
							</form:select>&nbsp;
							<form:input path="edad" id="txtEdad" cssClass="cajatexto" cssStyle="width:20px; text-align: right"
							 	onkeypress="javascript:fc_PermiteNumeros();" maxlength="2"
								onblur="javascript:fc_ValidaNumeroOnBlur('txtEdad');fc_maxlength(this, 2);" />
							&nbsp;Y&nbsp;
							<form:select path="codTipoEdad1" id="cboTipoEdad1" cssClass="cajatexto" 
								cssStyle="width:45px">
								<form:option value="">SEL</form:option>
								<c:if test="${control.listTipoEdad!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listTipoEdad}" />
								</c:if>
							</form:select>&nbsp;
							<form:input path="edad1" id="txtEdad1" cssClass="cajatexto" cssStyle="width:20px; text-align: right"
							 	onkeypress="javascript:fc_PermiteNumeros();" maxlength="2"
								onblur="javascript:fc_ValidaNumeroOnBlur('txtEdad1');fc_maxlength(this, 2);" />
						</td>
					</tr>
					<tr class="texto">
						<td>
							Dedicaci�n :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<form:select path="codDedicacion" id="cboDedicacion" cssClass="cajatexto_1" 
								cssStyle="width:130px" >
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.listDedicacion!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listDedicacion}" />
								</c:if>
							</form:select>
						</td>
						<td colspan="2">Postula a : <br>
							<input type="checkbox" id="rbtDoc" checked="checked"/>Docente
							<input type="checkbox" id="rbtAdm" checked="checked"/>Administrativo
						</td>
					</tr>
					<tr class="texto">
						<td>Interesado en :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<form:select path="codInteresado" id="cboInteresado" cssClass="cajatexto_1" 
								cssStyle="width:130px">
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.listInteresado!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
									items="${control.listInteresado}" />
								</c:if>
							</form:select>
						</td>
						<td colspan="2" valign="absmiddle">Disponibilidad de viajar :&nbsp;&nbsp;&nbsp;
							<br>
							<input type="checkbox" id="rbtDentroPais" />Dentro del Pa�s
							<input type="checkbox" id="rbtFueraPais" />Fuera del Pa�s
						</td>
					</tr>
					<tr class="texto">
						<td>
							A�os de Experiencia :&nbsp;&nbsp;&nbsp;
							<form:input path="anhosExperiencia" id="txtAnhosExperiencia" size="5" maxlength="3"
							 	onkeypress="javascript:fc_PermiteNumeros();"
								onblur="javascript:fc_ValidaNumeroOnBlur('txtAnhosExperiencia');fc_maxlength(this, 3);"
								cssClass="cajatexto"/>
						</td>
						<td colspan="2">A�os Experiencia Docencia :
							<form:input path="anhosExperienciaDocencia" id="txtAnhosExperienciaDocencia" size="5"
							 	onkeypress="javascript:fc_PermiteNumeros();"
								onblur="javascript:fc_ValidaNumeroOnBlur('txtAnhosExperienciaDocencia');fc_maxlength(this, 3);"
								maxlength="3"  cssClass="cajatexto"/>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="trSeccionSeleccion">
			<td class="texto_bold" valign="top" colspan="3">Proceso Revisi�n Asociado :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<form:select path="codProcRevAsociado" id="cboProcRevAsociado" cssClass="combo_o" 
					cssStyle="width:200px" onchange="javascript:fc_CambiaProcRevAsoc();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listProcRevAsociado!=null}">
					<form:options itemValue="codProceso" itemLabel="dscProceso" 
						items="${control.listProcRevAsociado}" />
					</c:if>
				</form:select>
			</td>
		</tr>
	</table>
	<br>	
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="fc_Grabar();"></a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>	
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript" language="javascript">

	if ( document.getElementById("txhTipoEtapa").value != "2"  )
		fc_CambiaTipo("1");
	else fc_CambiaTipo(document.getElementById("txhTipoEtapa").value); 
	
	if ( document.getElementById("txhIndViaje").value == "1" ) 
		 document.getElementById("rbtDentroPais").checked = true;
	if ( document.getElementById("txhIndViaje1").value == "1" )
		 document.getElementById("rbtFueraPais").checked = true;
	
	//Puesto al que postula: 
	if ( document.getElementById("txhIndPostDoc").value == "1" ) 
		 document.getElementById("rbtDoc").checked = true;
	if ( document.getElementById("txhIndPostAdm").value == "1" )
		 document.getElementById("rbtAdm").checked = true;

	
	strMsg = document.getElementById("txhMessage").value;
	if ( strMsg != "")
	{
		alert(strMsg);
		if ( document.getElementById("txhOperacion").value == "OK" )
		{
			window.opener.document.getElementById("txhAccion").value = "BUSCAR";
			window.opener.document.getElementById("frmMain").submit();
			window.close();
		}
	}
	
	if ( document.getElementById("txhCodProceso").value != "" )
	{
		document.getElementById("rbtRevision").disabled = true;
		document.getElementById("rbtSeleccion").disabled = true;
		//document.getElementById("cboProcRevAsociado").disabled = true;
	}
</script>
</body>
</html>