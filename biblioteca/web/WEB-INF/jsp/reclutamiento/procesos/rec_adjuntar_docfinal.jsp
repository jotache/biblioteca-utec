<%@ include file="/taglibs.jsp"%>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	var strExtensionDOC = "<%=(String)request.getAttribute("strExtensionDOC")%>";
	var strExtensionDOCX = "<%=(String)request.getAttribute("strExtensionDOCX")%>";
	var strExtensionPDF = "<%=(String)request.getAttribute("strExtensionPDF")%>";
	var strExtensionXLS = "<%=(String)request.getAttribute("strExtensionXLS")%>";
	var strExtensionXLSX = "<%=(String)request.getAttribute("strExtensionXLSX")%>";
		
	function onLoad()
	{
		strMsg = fc_Trim(document.getElementById("txhMessage").value);
		
		if ( strMsg != "" )
		{
			if ( strMsg == "OK" )
			{ 
				window.opener.document.getElementById("frmMain").submit();
				alert('Se grab� exitosamente');
				window.close();
			}
			else if ( strMsg == "ERROR" ) alert('Problemas al Grabar. Consulte con el administrador.');
			else if ( strMsg == "NO_EXI" ) alert('La direcci�n ingresada no es v�lida.');
		}
		document.getElementById("txhMessage").value = "";
	}
	
	function fc_Valida(){
	
		if ( fc_Trim(document.getElementById("txtCV").value) == "" )
		{
			alert('Debe seleccionar el archivo relacionado');
			return false;
		}
		
		lstrRutaArchivo = document.getElementById("txtCV").value;
		strExtension = "";
		strExtension = lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);		
		arrRuta = document.getElementById("txtCV").value.split("\\");
		var strNombreArchivo = arrRuta[arrRuta.length - 1]; 
		
		if (strExtension != strExtensionDOC.toUpperCase() && 
			strExtension != strExtensionDOCX.toUpperCase() &&
			strExtension != strExtensionPDF.toUpperCase() &&
			strExtension != strExtensionXLSX.toUpperCase() &&
			strExtension != strExtensionXLS.toUpperCase()){
			alert('El archivo debe tener la siguiente : XLS, XLSX, DOC, DOCX o PDF.');
			return false;
		}
		else{ 
			document.getElementById("txhNombreOriginal").value = strNombreArchivo;
			document.getElementById("txhExtCv").value = strExtension.toLowerCase();
			return true; 
		}		
	}
	
	function fc_GrabarArchivos()
	{
		if (fc_Valida())
		{
			if (!confirm('�Est� seguro de grabar?')) return false;
			document.getElementById("txhAccion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
</script>
</head>
<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/adjuntarArchivoInfFinal.html" 
	enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhAccion"/>
	<form:hidden path="message" id="txhMessage"/>
	<form:hidden path="idRec" id="txhIdRec"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="extCv" id="txhExtCv"/>
	<form:hidden path="extFoto" id="txhNombreOriginal"/>
	<table cellspacing="0" cellpadding="0" border="0" style="width:98%; margin-left: 10px; margin-top: 5px">
		<tr style="width: 100%">
			<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
			<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
				class="opc_combo">Adjuntar Archivo</td>
			<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
		</tr>
	</table>
	<table cellpadding="2" cellspacing="2" class="tabla" 
		style="margin-left: 10px; width: 96%; margin-top: 5px">
		<tr>
			<td width="50%"><b>Archivo:</b></td>
			<td>
				<input type="file" name="txtCV" class="cajatexto_1" size="50">
			</td>
		</tr>
	</table>
	<table cellpadding="1" cellspacing="2" style="width:96%; margin-left: 7px; margin-top: 10px">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img id="imggrabar" src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_GrabarArchivos();" style="cursor:pointer" alt="Grabar">
				</a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgcancelar" style="cursor:pointer;" onclick="javascript:window.close();"></a>
			</td>
		</tr>
	</table>
</form:form>
