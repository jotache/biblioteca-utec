<%@ include file="/taglibs.jsp"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<script type="text/javascript">
	function fc_ValidaEstado(cadEstados, estado ,estadoaux){	
		arrEstados = cadEstados.split("|");
		for (var i = 0; i < arrEstados.length - 1 ; i++)
			if (estadoaux!=''){
				if ( arrEstados[i] != estado && arrEstados[i] != estadoaux ) return false;
			}else{
				if ( arrEstados[i] != estado ) return false;
			}
		return true;
	}

	function onLoad(){}	
	function cambiaEstadoCtrls(tipo){
		
	}
	function fc_VerCriterios(){	
		var codSel = "";
		codSel = document.getElementById("txhCodProcesoRel").value;
		if ( fc_Trim(codSel) == "")
			codSel = document.getElementById("txhCodProceso").value;
		
		Fc_Popup("${ctx}/reclutamiento/ver_criterios.html?txhCodProceso=" + codSel,560,460,"");
	}
	
	function fc_Regresar(){	
		location.href = "${ctx}/reclutamiento/procesos_bandeja.html"
						+ "?txhCodUsuario=" + document.getElementById("txhCodUsuario").value
						+ "&txhOpcion=0"
						+ "&txhTipoEtapa=" + document.getElementById("txhTipoEtapa").value
						+ "&txhCodUniFun=" + document.getElementById("txhCodUniFun").value
						+ "&txhFecIni=" + document.getElementById("txhFecIni").value
						+ "&txhFecFin=" + document.getElementById("txhFecFin").value;
	}
	function fc_BuscarPostulante()
	{
		Fc_Popup("${ctx}/reclutamiento/buscar_postulantes.html"
				 + "?txhCodProceso=" + document.getElementById("txhCodProceso").value
				 + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value
				,850,700,"");
	}
	function fc_Buscar(orden)
	{
		document.getElementById("txhAccion").value = "BUSCAR";
		document.getElementById("ordenConsulta").value = orden;
		document.getElementById("frmMain").submit();
	}
	
	function fc_VerCV(strCV)
	{
		strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>";
		if ( strRuta != "" )
		{
			if ( fc_Trim(strCV) != "") window.open(strRuta + strCV);
			else alert('El postulante no cuenta con CV.');
		}
	}
	
	function fc_HojaVida(strCodPostulante)
	{
		var strCodUsuario = document.getElementById("txhCodUsuario").value;
		var strProcedencia = "proceso";
		Fc_PopupScroll("${ctx}/reclutamiento/hoja_vida.html?txhCodPostulante=" + strCodPostulante+ "&txhCodUsuario=" + strCodUsuario + "&txhProcedencia=" + strProcedencia,865,500);
	}
	
	function fc_seleccionarRegistro(strCodPostulante, strNombre, codEstado)
	{
		strCodSel = document.getElementById("txhCodPostSels").value;
		strNomSel = document.getElementById("txhNomPostSels").value;
		strEstSel = document.getElementById("txhEstPostSels").value;
		
		flag=false;
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			arrNomSel = strNomSel.split("|");
			arrEstSel = strEstSel.split("|");

			strCodSel = "";
			strNomSel = "";
			strEstSel = "";

			for (i=0; i <= ArrCodSel.length-2 ; i++)
			{
				if (ArrCodSel[i] == strCodPostulante){ flag = true }
				else
				{
					strCodSel = strCodSel + ArrCodSel[i] + '|';
					strNomSel = strNomSel + arrNomSel[i] + '|';
					strEstSel = strEstSel + arrEstSel[i] + '|';
				}
			}
		}
		if (!flag)
		{
			strCodSel = strCodSel + strCodPostulante + '|';
			strNomSel = strNomSel + strNombre + '|';
			strEstSel = strEstSel + codEstado + '|';
		}

		document.getElementById("txhNomPostSels").value = strNomSel; 
		document.getElementById("txhCodPostSels").value = strCodSel;
		document.getElementById("txhEstPostSels").value = strEstSel;
	}
	
	function fc_EnviarJefe(){
	
		strCodSel = document.getElementById("txhCodPostSels").value;
		arrCodSel = strCodSel.split("|");
		if ( arrCodSel.length - 1 == 0 )
		{
			alert('Debe seleccionar al menos un postulante.');
			return false;
		}

		//Validar que el proceso tenga un evaluador asignado
		var nroEvaluadores = document.getElementById("txhNroEvaluadores").value;
		if (nroEvaluadores==0){
			alert('Debe asignar evaluador(es) a este proceso');
			return false;
		}
		
		//solo los que tienen el estado 'REVISADO NO ENVIADO'.
		if ( !fc_ValidaEstado(document.getElementById("txhEstPostSels").value,
				document.getElementById("txhCodEstRevisadoNoEnviado").value,
				'')
				 ){				
			alert('Los postulantes que se deseen enviar a Jefe de Dpto. deben estar en estado REVISADOS NO ENVIADO.');
			return false;
		}
		if (confirm('� Seguro de enviar a Jefe Dpto.?')){
		
			document.getElementById("txhAccion").value = "ENVIARJEFE";
			document.getElementById("frmMain").submit();
		}
	}

	function fc_Revisar(){
		
		strCodSel = document.getElementById("txhCodPostSels").value;
		arrCodSel = strCodSel.split("|");
		if ( arrCodSel.length - 1 == 0 )
		{
			alert('Debe seleccionar al menos un postulante.');
			return false;
		}
				
		if ( !fc_ValidaEstado(document.getElementById("txhEstPostSels").value,
						document.getElementById("txhCodEstSinRev").value,
						document.getElementById("txhCodEstRevisadoNoEnviado").value)
						 )				 
		{		
			
			alert('Los postulantes que se deseen revisar deben estar en estado SIN REVISI�N � REVISADO NO ENVIADO');
			return false;
		}

		/*
		//Validar que el proceso tenga un evaluador asignado
		var nroEvaluadores = document.getElementById("txhNroEvaluadores").value;
		if (nroEvaluadores==0){
			alert('Debe asignar evaluador(es) a este proceso');
			return false;
		}*/
		
		Fc_PopupScroll("${ctx}/reclutamiento/eviar_proceso_revision.html?txhCodProceso=" + document.getElementById("txhCodProceso").value
					+ "&txhCodPostSele=" + document.getElementById("txhCodPostSels").value
					+ "&txhNomPostSele=" + document.getElementById("txhNomPostSels").value
					+ "&txhDscEtapa=${control.dscEtapa}"
					+ "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value
					,450,250,"");		
	}
	
	function fc_Calificar()
	{
		strCodSel = document.getElementById("txhCodPostSels").value;
		strEstSel = document.getElementById("txhEstPostSels").value;
		
		arrCodSel = strCodSel.split("|");
		arrEstSel = strEstSel.split("|");
		
		if ( arrCodSel.length - 1 == 0 )
		{
			alert('Debe seleccionar al menos un postulante.');
			return false;
		}
		else if ( arrCodSel.length - 1 > 1 )
		{
			alert('Debe seleccionar s�lo un postulante.');
			return false;
		}
		
		location.href = "${ctx}/reclutamiento/calificacion_rev.html"
						+ "?txhCodProceso=" + document.getElementById("txhCodProceso").value
						+ "&txhCodPostulante=" + arrCodSel[0] 
						+ "&txhCodEstado=" + arrEstSel[0]
						+ "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value
						+ "&txhCodEtapa=" + document.getElementById("txhCodEtapa").value;
	}
	
	function fc_Eliminar()
	{
		//document.getElementById("txhEstPostSels").value;
		strCodSel = document.getElementById("txhCodPostSels").value;
		
		arrCodSel = strCodSel.split("|");
		
		if ( arrCodSel.length - 1 == 0 )
		{
			alert('Debe seleccionar al menos un postulante.');
			return false;
		}
		
		if ( !confirm('�Est� seguro de eliminar los registros seleccionados?') ) return false;
		
		document.getElementById("txhAccion").value = "ELIMINAR";
		document.getElementById("frmMain").submit();
	}
	
	/*FUNCTION ETAPA DE SELECCION*/
	function fc_Evaluar()
	{
		strCodSel = document.getElementById("txhCodPostSels").value;
		arrCodSel = strCodSel.split("|");
		
		if ( arrCodSel.length - 1 == 0 ){		
			alert('Debe seleccionar al menos un postulante.');
			return false;
		}

		//Validar que el proceso tenga un evaluador asignado
		var nroEvaluadores = document.getElementById("txhNroEvaluadores").value;
		if (nroEvaluadores==0){
			alert('Debe asignar evaluador(es) a este proceso');
			return false;
		}
				
		if ( !fc_ValidaEstado(document.getElementById("txhEstPostSels").value,
						document.getElementById("txhCodEstSelSinEvaluar").value,
						'' )){		
			alert('Los postulantes que se deseen enviar a evaluar deben estar en estado SIN EVALUAR.');
			return false;
		}
				
		if (!confirm('Seguro de enviar el(los) registro(s) seleccionado(s) a evaluar.')) return false;
		document.getElementById("txhAccion").value = "EVALUAR";
		document.getElementById("frmMain").submit();
		
	}
</script>

<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
<script src="${ctx}/scripts/js/funciones_rec-fechas.js" language="JavaScript;" type="text/JavaScript"></script>

</head>

<body>
	<form:form name="frmMain" id="frmMain" commandName="control" action="${ctx}/reclutamiento/procesos_postulantes.html" >	
		<form:hidden path="codUsuario" id="txhCodUsuario"/>
		<form:hidden path="codProceso" id="txhCodProceso"/>
		<form:hidden path="operacion" id="txhAccion"/>
		<form:hidden path="message" id="txhMessage"/>
		<form:hidden path="codPostulantesSel" id="txhCodPostSels"/>
		<form:hidden path="nomPostulantesSel" id="txhNomPostSels"/>
		<form:hidden path="estPostulantesSel" id="txhEstPostSels"/>
		<form:hidden path="codPostulanteCV" id="txhCodCV"/>
		<form:hidden path="codEstSinRevision" id="txhCodEstSinRev"/>
		<form:hidden path="codEstRevisadoNoEnviado" id="txhCodEstRevisadoNoEnviado"/>
		<form:hidden path="codProcesoRel" id="txhCodProcesoRel"/>
		<form:hidden path="codEstSelSinEvaluar" id="txhCodEstSelSinEvaluar"/>
		<form:hidden path="codProcSelEnviadoAEvaluadores" id="txhCodProcSelEnviadoAEvaluadores"/>
		<form:hidden path="codEtapa" id="txhCodEtapa"/>
		<form:hidden path="dscProceso" id="txhDscProceso"/>
		<form:hidden path="dscEtapa" id="txhDscEtapa"/>
		<form:hidden path="codOperta" id="txhCodOperta"/>
		<form:hidden path="dscOferta" id="txhDscOperta"/>
		<!-- AGREGADO NAPA -->
		<form:hidden path="tipoEtapa" id="txhTipoEtapa"/>
		<form:hidden path="codUniFun" id="txhCodUniFun"/>
		<form:hidden path="fecIni" id="txhFecIni"/>
		<form:hidden path="fecFin" id="txhFecFin"/>

		<form:hidden path="tipoVistaFecha1" id="txhTipoVistaFecha1"/>
		<form:hidden path="tipoVistaFecha2" id="txhTipoVistaFecha2"/>
		
		<form:hidden path="nroEvaluadores" id="txhNroEvaluadores"/>
		<input type="hidden" name="ordenConsulta" id="ordenConsulta" value=""/>

		<!--/AGREGADO NAPA -->		
		<!-- Icono Regresar -->
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="97%">
			<tr>
				<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
				<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand" alt="Regresar"></td>
			</tr>
		</table>
		<!-- -->
		<table cellspacing="0" cellpadding="0" border="0" style="margin-left: 10px">
			<tr>
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="700px" height="27px" 
					class="opc_combo">Detalle del Proceso</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table> 
		<table cellpadding="1" cellspacing="2" class="tabla" style="margin-left: 10px; width: 892px; margin-top: 5px">
			
			<tr>
				<td width="15%">Codigo:</td>
				<td>
					<label id="lblCodProceso"></label>
				</td>
				<td>Etapa:</td>
				<td>
					<label id="lblEtapa"></label>
				</td>
			</tr>
			<tr>
				<td>Proceso:</td>
				<td>
					<label id="lblDscProceso" ></label>
				</td>
				<td></td>
				<td><a href="javascript:fc_VerCriterios();" class="Enlace">Ver Criterios</a></td>
			</tr>
			<tr>
				<td>Oferta:</td>
				<td>
					<label id="lblCodOferta"></label>
				</td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" border="0" style="width:45%; margin-left: 10px; margin-top: 10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">B�squeda</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="1" border="1" cellspacing="2" class="tabla" style="margin-left: 10px; width: 893px; margin-top: 5px">
			<tr>
				<td width="60px">Nombres :</td>
				<td width="100px"><form:input path="nombres" id="txtNombres" cssClass="cajatexto_1" cssStyle="width:100px" /></td>
				<td width="90px" align="right">Ape. Paterno :</td>
				<td width="140px"><form:input path="apellidos" id="txtApellidos" cssClass="cajatexto_1"/></td>
				<td width="70px">																	
					<input type="radio" name="optFecReg" id="opt1" onclick="cambiaEstadoCtrls('0');" <c:if test="${control.tipoVistaFecha1=='1'}">checked="checked"</c:if> />Todos					
				</td>
				<td width="90px">
					<input type="radio" name="optFecReg" id="opt2" onclick="cambiaEstadoCtrls('1');" <c:if test="${control.tipoVistaFecha2=='1'}">checked="checked"</c:if> />Por fechas
				</td>
				
				<td width="313px">
					<form:input maxlength="10" path="fecIniPost" id="txtFecIniPost"
					onkeypress="javascript:fc_ValidaFecha('txtFecIniPost');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecIniPost');"  
					cssClass="cajatexto_1" cssStyle="width:70px"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgfecini','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgfecini" alt="Seleccionar Fecha Inicial" 
						style="cursor:pointer; vertical-align: middle"></a>
					&nbsp;-&nbsp;
					<form:input maxlength="10" path="fecFinPost" id="txtFecFinPost"
					onkeypress="javascript:fc_ValidaFecha('txtFecFinPost');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecFinPost');" 
					cssClass="cajatexto_1" cssStyle="width:70px"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgfecfin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" alt="Seleccionar Fecha Final" id="imgfecfin" 
						style="cursor:pointer; vertical-align: middle"></a>						
				</td>
				<td width="30px">					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" style="cursor:hand;" alt="Buscar" onclick="fc_Buscar('NOM');" id="imgbuscar" align="center"/></a>
				</td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" border="0" style="width:45%; margin-left: 10px; margin-top: 10px">
			<tr style="width: 100%">
				<td><img alt="" src="${ctx}/images/reclutamiento/izquierda.jpg"></td>
				<td background="${ctx}/images/reclutamiento/centro.jpg" width="100%" height="27px" 
					class="opc_combo">Postulantes Asociados</td>
				<td><img alt="" src="${ctx}/images/reclutamiento/fin.jpg"></td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left: 10px; width: 98%; margin-top: 5px">
			<tr style="width: 100%">
				<td style="width: 95%">
					<div style="overflow: auto; height: 250px; border: 0px">
					<%-- --%>
					<table id="tblPostulantes" style="width:890px;margin-left:0px;border: 1px solid #048BBA;" cellspacing="1" cellpadding="0" border="0" bordercolor="#6b6d6b">
						<tr height="17px">
							<td class="grilla" width="20px" align="center">&nbsp;</td>
							<td class="grilla" width="260px" align="left">&nbsp;<a href="#" onclick="fc_Buscar('NOM');" style="color: white;">Postulante</a></td>
<!--							<td class="grilla" width="20px" align="center">CV</td>-->
							<td class="grilla" width="150px" align="center">Profesi�n</td>
							<td class="grilla" width="150px" align="center" title="Grado Acad�mico">Grado Acad.</td>
							<td class="grilla" width="50px" align="center" title="Expectativa Salarial">Monto</td>
							<c:if test="${control.codEtapa=='0001'}">
								<td class="grilla" width="30px" align="center" title="Numero de postulaciones">
									<!--  N.P. -->
									Fec. Reg.
								</td>
							</c:if>
							<td class="grilla" width="150px" align="center"><a href="#" onclick="fc_Buscar('EST');" style="color: white;">Estado</a></td>
							<c:if test="${control.codEtapa=='0002'}">
								<td class="grilla" width="150px" align="center">Evaluaciones</td>
							</c:if>	
							<c:if test="${control.codEtapa=='0001'}">
								<td class="grilla" width="50px" align="center" title="Calificaci�n">Calif.</td>
							</c:if>
						</tr>

						<!-- PINTADO DE EL CUERPO-->
						<c:if test="${control.listPostulantes!=null}">
							<c:forEach var="lista" items="${control.listPostulantes}" varStatus="loop" >
								<tr class="tablagrilla2">
									<td valign="middle">										
										<input type="checkbox" name="codPostulante<c:out value="${loop.index}" />" value="<c:out value="${lista.codPostulante}"/>"  
													onclick="javascript:fc_seleccionarRegistro(this.value,'<c:out value="${lista.nombres}"/>','<c:out value="${lista.codEstado}"/>');" />
									</td>
									<td valign="middle">
										<a href="javascript:fc_HojaVida('<c:out value="${lista.codPostulante}"/>');" ><c:out value="${lista.nombres}"/></a>
										&nbsp;<img src="/SGA/images/reclutamiento/cv.gif" width="16" height="16" style="cursor:pointer" alt="Ver curriculum" align="middle" onclick="fc_VerCV('<c:out value="${lista.perfil}"/>');"/>
									</td>

<!--									<td align="center"></td>-->

									<td>
										<c:out value="${lista.profesion}"/>
									</td>
									<td>
										<c:out value="${lista.gradoInstruccion}"/>
									</td>
									<td align="center">
										<c:out value="${lista.pretension_economica}"/>
									</td>
									<c:if test="${control.codEtapa=='0001'}">
										<td align="center">
											<c:out value="${lista.fecreg}"/>
										</td>
									</c:if>
									<td align="center">
										<c:out value="${lista.dscEstado}"/>
									</td>
									<td 
										<c:if test="${control.codEtapa=='0001'}">
											align="center"
										</c:if> 
									>
										<c:if test="${control.codEtapa=='0002'}">
											<c:forEach var="traza" items="${lista.evaluaciones}" varStatus="loop2" >
												<c:out value="${traza.etapa}"/><c:out value=" : "/><c:out value="${traza.calificacion}"/><br>
											</c:forEach>
										</c:if>
										<c:if test="${control.codEtapa=='0001'}">
											<c:out value="${lista.ultEstadoPostulRevision}"/>
										</c:if>										
									</td>
								</tr>
							</c:forEach>
						</c:if>

					</table>
						
					</div>
					<% request.getSession().removeAttribute("listaPostulantes"); %>
				</td>
				<td id="tdBotonesRevision" style="width: 5%" valign="top"><br><br>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src="${ctx}/images/iconos/agregar1.jpg" alt="Agregar Postulante" onclick="javascript:fc_BuscarPostulante();" id="imgagregar"  style="cursor:hand;" /></a><br/>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar Postulante" id="imgquitar" onclick="javascript:fc_Eliminar();" style="cursor:hand;" />
				</td>	
			</tr>	
			<tr height="7px"><td></td></tr>
			<tr id="trBotonesRevision">
				<td align=center valign="top">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgrevisar','','${ctx}/images/botones/revisar2.png',1)">
						<img alt="Revisar" src="${ctx}/images/botones/revisar1.png" id="imgrevisar" style="cursor:pointer;" onclick="javascript:fc_Revisar();">
					</a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgenviar','','${ctx}/images/botones/enviarjefe2.jpg',1)">
						<img alt="Enviar a jefe de departamento" src="${ctx}/images/botones/enviarjefe1.jpg" id="imgenviar" style="cursor:pointer;" onclick="javascript:fc_EnviarJefe();">
					</a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgcalificar','','${ctx}/images/botones/calificar2.jpg',1)">
						<img alt="Calificar" src="${ctx}/images/botones/calificar1.jpg" id="imgcalificar" style="cursor:pointer;" onclick="javascript:fc_Calificar();" >
					</a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
						<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar" style="cursor:pointer;" onclick="fc_Regresar();" >
					</a>	
				</td>
			</tr>
			<tr id="trBotonesSeleccion" style="display: none;">
				<td align=center>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgrEnvEval','','${ctx}/images/botones/enviar_evaluar.gif',1)">
					<img alt="Enviar a Evaluar" src="${ctx}/images/botones/enviar_evaluar.gif" id="imgrEnvEval" style="cursor:pointer;" onclick="fc_Evaluar();" ></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgrCalFinal','','${ctx}/images/botones/calficacion_final.gif',1)">
					<img alt="Calificaci�n Final" src="${ctx}/images/botones/calficacion_final.gif" id="imgrCalFinal" style="cursor:pointer;" onclick="fc_Calificar();" ></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar1','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar1" style="cursor:pointer;" onclick="fc_Regresar();" ></a>
				</td>
			</tr>
		</table>
	</form:form>
	
	<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txtFecIniPost",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgfecini",
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "txtFecFinPost",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgfecfin",
			singleClick    :    true
		});
	</script>	
	
	<script type="text/javascript">
		/*Cargando Lbls*/
		document.getElementById("lblCodProceso").innerText = document.getElementById("txhCodProceso").value; 
		document.getElementById("lblDscProceso").innerText = document.getElementById("txhDscProceso").value;
		document.getElementById("lblEtapa").innerText = document.getElementById("txhDscEtapa").value;
		document.getElementById("lblCodOferta").innerText = document.getElementById("txhDscOperta").value;  
		objMsg = document.getElementById("txhMessage");
		if ( fc_Trim(objMsg.value) != "" ){
			if ( fc_Trim(objMsg.value) == "ERROR" )  alert('Problemas al eliminar. Consulte con el administrador.');
			else if ( fc_Trim(objMsg.value) == "OK" ) alert('Se elimin� exitosamente.');
			else if ( fc_Trim(objMsg.value) == "EVA_ERROR" ) alert('Problemas al Grabar. Consulte con el administrador.');
			else if ( fc_Trim(objMsg.value) == "EVA_OK" ) alert('Se grab� exitosamente');
		}
		if ( fc_Trim(document.getElementById("txhCodProcesoRel").value) != "" )
		{
			document.getElementById("tdBotonesRevision").style.display = "none";
			document.getElementById("trBotonesRevision").style.display = "none";
			document.getElementById("trBotonesSeleccion").style.display = "inline-block";
		}
		else
		{
			document.getElementById("tdBotonesRevision").style.display = "inline-block";
			document.getElementById("trBotonesRevision").style.display = "inline-block";
			document.getElementById("trBotonesSeleccion").style.display = "none";
		}
		objMsg.value = "";
	</script>
</body>
</html>