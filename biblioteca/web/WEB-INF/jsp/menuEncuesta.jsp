<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%	if ( request.getSession().getAttribute("usuarioSeguridad") == null ) { %>
<script type="text/javascript">	

	alert("Su sesión ha terminado, favor de volver a ingresar al sistema");
	
	//top.parent.location.href = "/SGA/logeoEncuesta.html";
	top.location.href = "/SGA/logeoEncuesta.html";
	
</script>
<%} %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 transitional//EN" "http://www.w3.org/tr/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
		function onLoad(){
		}
				
		function fc_Perfiles(obj){
		  srtCod="A";
			switch(obj){
				case '1':
					window.location.href = "${ctx}/encuestas/bandeja_conf_gen_encuestas.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;																				
					break;
				case '2':
					window.location.href = "${ctx}/encuestas/bandejaAplicacionSeguimiento.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;					
					break;
				case '3':
					window.location.href = "${ctx}/encuestas/consultaReportes.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
					break;					
				case '4':
					window.location.href = "${ctx}/encuestas/mantenimiento_configuracion_enlaces.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
					break;
				case '5': 
					window.location.href = "${ctx}/encuestas/actualizacion_datos.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value ;
					break;
				case '6':
					window.location.href = "${ctx}/encuestas/completarEncuestaPublicada.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value ;
					break;
				default:
					break;
			}
		}
		
	</script>
</head>
<body>
<form:form name="frmMain" commandName="control" action="${ctx}/menuEncuesta.html">	
	<!-- ULTIMO MODIFICADO TECSUP-->
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<!-- Icono Regresar -->
	<table cellPadding="0" cellSpacing="0" border="0" width="100%">
		<tr>
			<td class="opc_combo" height="29px">&nbsp;&nbsp;&nbsp;</td>			
		</tr>
	</table>
	<!--Page Title -->
	<table border="0" align="center" width="95%;" cellpadding="0" cellspacing="0"> 
	<tr>
	<td>
	<!--  -->
	
		<table cellSpacing="0" cellPadding="0" border="0"
		style="height: 500px; width: 100%; margin-top: 0px;margin-left:10px" bordercolor="green">
		
		<tr height="2px" bgcolor="#00A0E4">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<%-- <td rowspan="8" align="right" style="height:310px;" width="463px"><img
				src="${ctx}/images/Encuestas/opciones-encuestas.jpg" height="500px"></td> --%>
		</tr>
		<%		
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"ENC_CONGEN") == 1) {
		%>

		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_Perfiles('1');" class="enlace"
				style="cursor: hand">&nbsp;Configuración y Generación </a></td>
		</tr>
		<%
		}
		%>

		<%
		if (
		CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_APLSEG") == 1 ) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_Perfiles('2');" class="enlace"
				style="cursor: hand">&nbsp;Aplicación y Seguimiento</a></td>
		</tr>
		<%
		}
		%>

		<%
		if ( 
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_CONREP_3") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_CONREP_4") == 1 ||
			CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_CONREP_5") == 1
				
		) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_Perfiles('3');" class="enlace"
				style="cursor: hand">&nbsp;Consultas y Reportes</a></td>
		</tr>
		<%
		}
		%>

		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_ADMSIS") == 1 ) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_Perfiles('4');" class="enlace"
				style="cursor: hand">&nbsp;Administración del Sistema</a></td>
		</tr>
		<%
		}
		%>

		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"), "ENC_ACTDAT") == 1
				&& request.getSession().getAttribute("verificarAlumno").equals("1")) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_Perfiles('5');" class="enlace"
				style="cursor: hand">&nbsp;Actualización de Datos</a></td>
		</tr>
		<%
		}
		%>
		
		<%
		String tieneEncuesta=(String)request.getSession().getAttribute("verificarTieneEncuesta");
		String nroEncuestasCompletar=(String)request.getSession().getAttribute("nroEncuestasCompletar");
		int valor = Integer.valueOf(tieneEncuesta);
		int nroEncuestas = Integer.valueOf(nroEncuestasCompletar);
		//System.out.println(">>"+valor+"<<");
		if ( valor > 0 && nroEncuestas > 0) {
		%>
		<tr bgcolor="#00A0E4">
			<td width="6%" align="right"><img
				src="${ctx}/images/iconos/ico_link.gif" style="cursor: hand"></td>
			<td><a href="javascript:fc_Perfiles('6');" class="enlace"
				style="cursor: hand">&nbsp;Completar Encuestas Publicadas</a></td>
		</tr>
		<%
		}
		%>
		<tr bgcolor="#00A0E4" height="5px">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	
	<!--  -->
	</td>
	
	<!--  -->
	<td rowspan="8" align="left" style="height:310px;" width="463px">
		<img src="${ctx}/images/Encuestas/opciones-encuestas.jpg" height="500px">
	</td>
	<!--  -->
	</tr>
	
	</table>
</form:form>
</body>