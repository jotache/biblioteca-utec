<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/taglibs.jsp"%>    
<html>
<head>
<link href="${ctx}/styles/estilo_biblio_admin2.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Confirmaci�n</title>
<%String result = (String)request.getAttribute("rptaDatosPer"); %>

<script type="text/javascript">
	$(function () {
		$("input:submit, button, input:button").button();
				
	});
	
			
	function onLoad(){
		
	}
	
		
	function fc_deacuerdo(){
		//if(confirm('�Confirma su aceptaci�n?')){				
			frmMain.operacion.value = "ACEPTA";
			frmMain.txhRespuesta.value = "S";
			frmMain.submit();
			
		//}
	}
		
	
</script>		

</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/confirmar_datos_personales.html">
<form:hidden path="operacion" id="operacion" />
<form:hidden path="respuesta" id="txhRespuesta" />


<div class="DivMarco" style="width:98%;">
    	<table border="0" align="center" width="100%" cellpadding="4" cellspacing="0">
            <tr>
                <td align="center">
                    <div class="Caption">&nbsp;<b>LEY DE DATOS PERSONALES 29733</b></div>
                </td>                					
            </tr> 
            
            <tr>
                <td align="left" >
                    
                    <input type="checkbox" value="1" name="chkAcepta" checked="checked" disabled="disabled"/>
                    <div>
Autorizo a TECSUP N� 1 y a la Asociaci�n TECSUP N� 2, afiliadas, asociadas, personas naturales o jur�dicas con las que haya suscrito contratos o convenios a realizar el tratamiento* de mis datos personales por un plazo indeterminado con la finalidad** de promoci�n, de todo tipo de publicidad, anuncio, en cualquier medio de comunicaci�n sea f�sico, virtual, televisivo u otro; as� mismo para �l env�o de informaci�n promocional, educativa, administrativa.<br/> <br/>

El titular de los datos personales puede ejercer sus derechos ARCO*** escribi�ndonos al correo datospersonales@tecsup.edu.pe Sus datos personales ser�n almacenados en las bases de datos de TECSUP N� 1 y de la Asociaci�n TECSUP N� 2, respectivamente. 
<br/><br/>
Ley de Protecci�n de Datos Personales concordado con su Reglamento<br/><br/> 
*Ley de Protecci�n de datos Personales <br/><br/>

Articulo 2.- Numeral 17 Tratamiento de Datos personales .- Cualquier procedimiento t�cnico automatizado o no, que permite la recopilaci�n registro, organizaci�n, almacenamiento, conservaci�n, elaboraci�n, modificaci�n extracci�n, consulta, utilizaci�n, bloqueo, supresi�n, comunicaci�n por transferencia o por difusi�n o cualquier otra forma de procesamiento que facilite el acceso, correlaci�n o interconexi�n de los datos personales.<br/> 
<br/>
** Art�culo 6.- Principio de finalidad. En atenci�n al principio de finalidad se considera que una finalidad est� determinada cuando haya sido expresada con claridad, sin lugar a confusi�n y cuando de manera objetiva se especifica el objeto que tendr� el tratamiento de los datos personales. 
<br/><br/>
***El titular de los datos personales tiene derecho al acceso, rectificaci�n, cancelaci�n, oposici�n de sus datos personales.
					</div> 
                   <br>
                   
                    
                </td>                					
            </tr>  
                   
                   
			<tr>
				<td align="center">
				
		<%if(result!=null && result.equals("1")) {%>
		<div id="dialog-message-alert" title="Sistema de Evaluaciones">
			<div class="ui-widget" style="font-size: 11px;">
				<div class="ui-state-highlight ui-corner-all">
					<p style="margin-top: 0.7em; margin-bottom: 0.7em; margin-left: 0em;">
						<span class="ui-icon ui-icon-alert"
							style="float: left; margin-left: 1.5em"></span>&nbsp;&nbsp;Se actualiz� su respuesta. Gracias.
					</p>
				</div>
			</div>
		</div>
		<%}else{ %>				
				
				<input type="button" value="Aceptar" id="imgGrabar" onClick="javascript:fc_deacuerdo();">
					&nbsp;&nbsp;&nbsp;<!--
					<input type="button" value="No acepto" id="imgRegresar" onClick="javascript:fc_rechazar();">
                    -->
					
	    <%}%>
				
									
				</td>				
			</tr>	

                       
        </table>                   
	
	<br />
	
	
        	
	
</div>
	


	
	
	
</form:form>	
</body>
</html>