<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%	
if(request.getSession().getAttribute("usuarioSeguridad")==null) {%>
<script type="text/javascript">	
	<% if (request.getSession().getAttribute("reingreso")!=null) {%>
		alert("Por favor reingrese al sistema con su nueva clave.");
	<% }else{ %>
		alert("Su sesi�n ha terminado, favor de volver a ingresar al sistema.");
	<% } %>		
	location.href = "/biblioteca/logeoBiblioteca.html";	
</script>
<%}%>

<head>
<title>CEDITEC - Centro de Documentaci�n e Informaci�n de Tecsup</title>
<link href="${ctx}/styles/cab.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" type="text/javascript">
		function onLoad(){
			
		}
		function fc_CerrarSesion()
		{
			if ( confirm("�Est� seguro de cerrar sesi�n?") )
			{				
				form = document.forms[0];
				form.txhAccion.value="CERRAR_SESION";
				form.submit();
			}
		}
		function fc_CambiarClave()
		{			 			
			window.location.href = "${ctx}/biblioteca/cambiaClaveBiblioteca.html";			
		}		
	</script>	
</head>

<body>
	<form:form name="frmMain" commandName="control" action="${ctx}/Biblioteca.html"> 
		<form:hidden path="accion" id="txhAccion"/>
		       
		      
		
		        
        <table cellpadding="0" cellspacing="0" width="989px" border="0" align="center" background="${ctx}/images/cabecera/cabecera_celeste.jpg">
        	<tr>
            	<td height="65px">&nbsp;
                	
                </td>                
            </tr>
            <tr>
            	<td>
                	<span class="textonormal">&nbsp;&nbsp;Bienvenido(a): &nbsp; <form:label path="codEval" cssClass="Opciones"><b>${control.codEval}</b></form:label></span>
					<label onClick="fc_CerrarSesion();" style="cursor: pointer;">
                        <span class="textomenu" onMouseOver="this.className='textolink'" onMouseOut="this.className='textomenu'">[Cerrar Sesi&oacute;n]</span>
					</label>&nbsp;&nbsp;												
					<label onClick="fc_CambiarClave();" style="cursor: pointer;">
						<span class="textomenu" onMouseOver="this.className='textolink'" onMouseOut="this.className='textomenu'" >[Cambiar Contrase&ntilde;a]		</span>
					</label>
                </td></tr>
        </table>
        
         <table cellpadding="0" cellspacing="0" width="989px" border="0" align="center">
         <tr><td height="10px">&nbsp;</td></tr>
         <tr><td align="center">
         <iframe id="Body" name="Body" align="middle" src="${ctx}/menuBiblioteca.html" scrolling="no" frameborder="0" height="532px" width="989px"></iframe>
         </td></tr>
         </table>
				
	</form:form>	
</body>