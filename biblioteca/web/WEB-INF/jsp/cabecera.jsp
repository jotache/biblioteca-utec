<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script type="text/javascript" type="text/javascript">
		function onLoad(){
			
		}
		
		function fc_Cambiar(obj) {
			var strCadena = obj.value;
			switch(strCadena){
				case '00' :					
					parent.Body.location.href = "${ctx}/menu.html";
					break;
				case '01' :
					parent.Body.location.href  = "${ctx}/eva/mto_menu.html?construccion=construccion";
					break;					
				case '02' :
					parent.Body.location.href  = "${ctx}/eva/mto_menu.html?construccion=construccion";
					break;
				case '03' :
					parent.Body.location.href  = "${ctx}/eva/mto_menu.html?construccion=construccion";
					break;
				case '04' :
					parent.Body.location.href = "${ctx}/menu.html" + 
												"?txhCodEval=jmoran&txhType=" + strCadena;
					break;
				case '05' :
					{parent.Body.location.href = "${ctx}/menu.html" + 
												"?txhCodPeriodo=120&txhCodEval=jmoran&txhType=" + strCadena;} 
					break;
			}				
		}
	</script>
</head>

<body>
	<form:form name="frmMain" commandName="control" action="${ctx}/inicio.html">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td><img src="${ctx}\images\cabecera\cabecera.jpg"></td>
			</tr>
		</table>
		<table border="0" bordercolor="white" cellspacing="0" cellpadding="0" style="position:absolute;left:30px;top:30px">
			<tr>	
				<td>
					<SELECT id="cboTipo" name="cboTipo" class="cajatexto" style="WIDTH:200px" onchange="javascript:fc_Cambiar(this);">
						<OPTION value="00" selected>--Elegir un m&oacute;dulo--</OPTION>
						<OPTION value="01">Sistemas Biblioteca</OPTION>
						<OPTION value="02">Sistemas Encuestas</OPTION>
						<OPTION value="03">Sistemas Log&iacute;stica</OPTION>
						<OPTION value="04">Sistemas Reclutamiento</OPTION>
						<OPTION value="05">Sistemas Evaluaciones</OPTION>
					</SELECT>
				</td>
			</tr>	
		</table>
		<iframe id="Body" name="Body" src="${ctx}/menu.html" scrolling="auto" frameborder="0" height="530px" width="98%" style="align:center;"></iframe>
	</form:form>	
</body>