<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script language=javascript>
    function onLoad()
	{
		objMsg = document.getElementById("txhMsg");
		
		if ( objMsg.value == "OK" )
		{
			//window.opener.document.getElementById("frmMain").submit();
			window.opener.fc_Buscar();
			alert(mstrActualizo);
			window.close();
		}
		else{ if ( objMsg.value == "ERROR" )
		       {
			         alert(mstrProblemaGrabar);			
		       }
		      else{ if ( objMsg.value == "DUPLICADO" )
		            alert(mstrExistenRegistros);}
		}
	}
    function fc_Categorias(){
    
    }
    
	function fc_Guardar(){
	     
		objDescripcion = document.getElementById("descripcionDewey").value;
		objCodigoDewey = document.getElementById("codigoDewey").value;
		
		if(document.getElementById("codCategoria").value=="-1"){ alert(mstrSelecCategoria); document.getElementById("codCategoria").focus(); return false;}
		
		if (fc_Trim(objDescripcion) == "")
		{
			alert('Debe ingresar una descripci�n.');
			document.getElementById("descripcionDewey").focus();
			return;
		}
		else if (fc_Trim(objCodigoDewey) == "")
		{
			alert('Debe ingresar el c�digo.'); 
			document.getElementById("codigoDewey").focus(); 
			return;
		}
		else{
		   
		   document.getElementById("txhOperacion").value = "GUARDAR";
		   document.getElementById("frmMain").submit();
		}
		
	   }
	
	function fc_cancelar(){
		//window.opener.document.getElementById("frmMain").submit();
	}

	function fc_ValidaNumeroGrr(){
		fc_ValidaNumerosAndLetrasFinalTodo('codigoDewey','Codigo');
		
		/*
		srtTexto=document.getElementById("codCategoria").value;
		srtMaD=srtTexto.substring(3,4)-1;
		
		if(fc_Trim(document.getElementById("codigoDewey").value)!= ""){
	
			srtMoon=document.getElementById("codigoDewey").value.substring(0,1);
			if(srtMaD==srtMoon)
			 	fc_ValidaNumerosAndLetrasFinalTodo('codigoDewey','Codigo'); 
			else
			 {	 document.getElementById("codigoDewey").focus();	 
			 	 alert('El c�digo ingresado no es correcto');			 	 
			 }
		}
		*/
	}
	// onblur="fc_ValidaNumeroOnBlur('codigoDewey');" 
</script>	
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form action="${ctx}/biblioteca/bib_dewey_modificar.html" commandName="control" id="frmMain">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="codValor" id="txhCodValor" />
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>
			
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="503px" class="opc_combo"><font style="">Mantenimiento de Clasificaci&oacute;n Dewey Modificar</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			 </tr>
		</table>
		<table><tr height="1px"><td></td></tr></table>
		<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td width="20%" align="left">&nbsp;Categor&iacute;a :</td>
				<td align="left">&nbsp;
				    <form:select path="codCategoria"
							id="codCategoria" cssClass="cajatexto_o"
							cssStyle="width:180px"  onchange="javascript:fc_Categorias();">
							<form:option value="-1">--Seleccione--</form:option>
							<c:if test="${control.codListaCategoria!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
									items="${control.codListaCategoria}" /> 
							</c:if>
						</form:select>
				</td>						
		    </tr>
			<tr>
				<td nowrap="nowrap" align="left">&nbsp;C&oacute;digo :</td>
				<td align="left">&nbsp;
					<form:input path="codigoDewey" id="codigoDewey" maxlength="10"
						onblur="javascript:fc_ValidaNumeroGrr();" 
		                onkeypress="fc_ValidaTextoEspecialAndNumeroTodo();"
						cssClass="cajatexto" size="10" />
				</td>
			</tr>
			<tr align="center">
				<td nowrap="nowrap" align="left">&nbsp;Descripci&oacute;n :</td>
				<td align="left">&nbsp;			
						<form:input path="descripcionDewey" id="descripcionDewey" 
						onkeypress="fc_ValidaNombreAutorOnkeyPress();" maxlength="100"
						onblur="fc_ValidaNombreAutorOnblur(this.id,'Descripcion');" 
						 cssClass="cajatexto" size="80" />
				</td>
			</tr>
		</table>
		<table><tr height="5px"><td></td></tr></table>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Guardar();" style="cursor:pointer;"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">					
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();fc_cancelar();" ID="Button	1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
		<br>
	</form:form>
	</body>
</html>