<%@ include file="/taglibs.jsp"%>
<head>
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" type="text/JavaScript"></script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/JavaScript">
	
	function onLoad(){ 
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )		
			alert(mstrSeGraboConExito);				
		else if ( objMsg.value == "ERROR" )		
			alert(mstrProblemaGrabar);		
	}
	function Fc_Graba(){
		
		if(confirm('�Seguro de Guardar la Configuraci�n?')){
			form = document.forms[0];
			var max = document.getElementById("tamanioLista").value;
// 			console.log('max:'+max);
			var i;
			var objCod;
			var SA1;
			var SA2;
			var SA3;
			var IN1;
			var IN2;
			var IN3;
			var BI;
			var VI;
			var PO1;
			var PO2;
			var datos='';
			for(i=0;i<max;i++) {
				objCod = document.getElementById("oCodParamUsu_"+i).value;
// 				console.log('objCod:'+objCod);
				SA1 = document.getElementById("SA1_"+i).value;
				SA2 = document.getElementById("SA2_"+i).value;
				SA3 = document.getElementById("SA3_"+i).value;
				IN1 = document.getElementById("IN1_"+i).value;
				IN2 = document.getElementById("IN2_"+i).value;
				IN3 = document.getElementById("IN3_"+i).value;
				BI = document.getElementById("BI_"+i).value;
				VI = document.getElementById("VI_"+i).value;
				PO1 = document.getElementById("PO1_"+i).value;
				PO2 = document.getElementById("PO2_"+i).value;
				datos += objCod + '$' + SA1 + '$' + SA2 + '$' + SA3 + '$' + IN1 + '$' + IN2 + '$' + IN3 + '$' + BI + '$' + VI + '$' + PO1 + '$' + PO2 + '|'; 
			}
// 			alert("Datos:" + datos);
			form.cadenaDatos.value = datos;
			form.txhOperacion.value = "GRABAR";
			form.submit();
		}
		
	}
	
	function Fc_Cancelar(){
		
	}
	</script>
    
    <style>
		.celda_sup {	
			border-right: 1px solid #048BBA;
		}
		
		.celda_det {	
			border-right: 1px solid #048BBA;	
			border-top: 1px solid #048BBA;		
		}
	</style>
    
	</head>
	
	<form:form action="${ctx}/biblioteca/bib_Parametros_Generales.html" commandName="control" id="frmMain">	
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codEval" id="txhCodEval"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="tamanioLista" id="tamanioLista"/>
	<form:hidden path="cadenaDatos" id="cadenaDatos"/>
	
	<!--T�tulo de la P�gina  -->		
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Par�metros Generales</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
    
    
    <table border="0" bgcolor="#eae9e9" class="" style="border: 1px solid #048BBA;margin-left:3px; width:98%;" cellspacing="0" cellpadding="2" height="50px">
       <tr>
    	  <td class="celda_sup">&nbsp;</td>
    	  <td colspan="3" align="center" class="celda_sup"><b>POR RESERVA DE SALAS DE  ESTUDIO</b></td>
    	  <td colspan="3" align="center" class="celda_sup"><b>POR RESERVA DE SALAS DE  INTERNET</b></td>
    	  <td align="center" class="celda_sup"><b>POR RESERVA DE MATERIAL  BIBLIOGR&Aacute;FICO EN GENERAL</b></td>
    	  <td align="center" class="celda_sup"><b>NRO REG. A VISUALIZAR EN  SECCION NOVEDADES</b></td>
    	  <td colspan="2" align="center" ><b>PARA RESERVAS DE POLIDEPORTIVO</b></td>
   	  </tr>
      <tr>
  	  	<td class="celda_det" width="20%"><b>Tipo de Usuario</b></td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. M�x.  horas<br />para reserva al d�a</td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. De Minutos De Tolerancia</td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. De D�as  Penalidad </td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. M&aacute;x. De Horas Para  Reservar Al D&iacute;a</td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. De Minutos   Tolerancia&nbsp;</td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. De D&iacute;as  Penalidad</td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. De D&iacute;as<br/>Penalidad</td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. M&aacute;x. A <br/>Visualizar</td>
        <td class="celda_det" width="8%" style="background-color: #fff;">Nro. M&aacute;x. De Horas Para  Reservar Al D&iacute;a</td>
        <td style="background-color: #fff; border-top: 1px solid #048BBA" width="8%">Nro. De Minutos   Tolerancia</td>
     </tr>
         
     <c:forEach var="lista" items="${listaParametrosUsuario}" varStatus="loop">
     	<tr>
     		<td class="celda_det" style="background-color: #fff;"><c:out value="${lista.nomTipoUsuario}" />
     			<input type="hidden" id="oCodParamUsu_<c:out value="${loop.index}" />" value="${lista.codParametroUsuario}">
     		</td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.esMaDi}" id="SA1_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.esMiTo}" id="SA2_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.esDiPe}" id="SA3_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.inMaDi}" id="IN1_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.inMiTo}" id="IN2_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.inDiPe}" id="IN3_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.biNroDiPe}" id="BI_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.cantMostrarWeb}" id="VI_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" class="celda_det"><input type="text" value="${lista.maxHrResPoli}" id="PO1_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     		<td align="center" style="border-top: 1px solid #048BBA;"><input type="text" value="${lista.minTolePoli}" id="PO2_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();"></td>
     	</tr>
     </c:forEach>
     
    </table><br />
    
<!-- 	<table border="0" bgcolor="#eae9e9" class="tablaMant" style="border: 1px solid #048BBA;margin-left:3px; width:98%;" cellspacing="4" height="50px"> -->
<!-- 		<tr> -->
<!-- 			<td colspan='6'>&nbsp;POR RESERVA DE SALAS DE ESTUDIO</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td width='2%'>&nbsp;</td> -->
<!-- 			<td width='20%' nowrap="nowrap">&nbsp;Nro. M&aacute;ximo de Horas para Reservar al d&iacute;a :</td> -->
<%-- 			<td width='5%' colspan="4">&nbsp;<form:input path="esmadi" id="txtEsmadi"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtEsmadi');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td width='2%'>&nbsp;</td> -->
<!-- 			<td>&nbsp;Nro. de Minutos de Tolerancia :</td> -->
<%-- 			<td>&nbsp;<form:input path="esmito" id="txtEsmito"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtEsmito');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 			<td width="2%">&nbsp;</td> -->
<!-- 			<td width="10%">&nbsp;Nro. de D&iacute;as de Penalidad :</td> -->
<%-- 			<td width="5%">&nbsp;<form:input path="esdipe" id="txtEsdipe"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtEsdipe');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 		</tr>				 -->
<!-- 		<tr height="20px"> -->
<%-- 			<td colspan="6">&nbsp;<img src="${ctx}/images/separador_n.gif" width="100%" height="1"></td> --%>
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td colspan='6'>&nbsp;POR RESERVA DE SALAS DE INTERNET</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td width='2%'>&nbsp;</td> -->
<!-- 			<td width='20%'>&nbsp;Nro. M&aacute;ximo de Horas para Reservar al d&iacute;a :</td> -->
<%-- 			<td width='5%' colspan="4">&nbsp;<form:input path="inmadi" id="txtInmadi"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtInmadi');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td width='2%'>&nbsp;</td> -->
<!-- 			<td>&nbsp;Nro. de Minutos de Tolerancia :</td> -->
<%-- 			<td>&nbsp;<form:input path="inmito" id="txtInmito"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtInmito');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 			<td width="2%">&nbsp;</td> -->
<!-- 			<td nowrap="nowrap">&nbsp;Nro. de D&iacute;as de Penalidad :</td> -->
<%-- 			<td width="5%">&nbsp;<form:input path="indipe" id="txtIndipe"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtIndipe');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 		</tr> -->
<!-- 		<tr height="20px"> -->
<%-- 			<td colspan="6">&nbsp;<img src="${ctx}/images/separador_n.gif" width="100%" height="1"></td> --%>
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td colspan='6'>&nbsp;POR RESERVA DE MATERIAL BIBLIOGR&Aacute;FICO EN GENERAL</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td width='2%'>&nbsp;</td> -->
<!-- 			<td width='20%'>&nbsp;Nro. de D&iacute;as de Penalidad :</td> -->
<%-- 			<td width='5%'>&nbsp;<form:input path="binrodipe" id="txtBinrodipe"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtInmito');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 			<td width="2%">&nbsp;</td> -->
<!-- 			<td>&nbsp;</td> -->
<!-- 			<td>&nbsp;</td> -->
<!-- 		</tr>		 -->
<!-- 			<tr height="20px"> -->
<%-- 			<td colspan="6">&nbsp;<img src="${ctx}/images/separador_n.gif" width="100%" height="1"></td> --%>
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td colspan='6'>&nbsp;NRO REG. A VISUALIZAR EN SECCION NOVEDADES.</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td width='2%'>&nbsp;</td> -->
<!-- 			<td width='20%'>&nbsp;Nro. m�ximo a visualizar :</td> -->
<%-- 			<td width='5%'>&nbsp;<form:input path="visualizar" id="txtVisualizar"  --%>
<%-- 							onkeypress="fc_PermiteNumeros();" --%>
<%-- 							onblur="fc_ValidaNumeroOnBlur('txtVisualizar');" --%>
<%-- 							cssClass="cajatexto_o" size="4" /></td> --%>
<!-- 			<td width="2%">&nbsp;</td> -->
<!-- 			<td>&nbsp;</td> -->
<!-- 			<td>&nbsp;</td> -->
<!-- 		</tr>		 -->
<!-- 	</table> -->
	
	  
	<table border="0" width="98%" align="center" style="margin-left:3px;">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:Fc_Graba();" style="cursor:pointer"></a>		
			</td>
		</tr>
	</table>
	
</form:form>