<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
    function onLoad()
	{   objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK" ){
			window.opener.document.getElementById("txhSeleccion").value="";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "IGUAL" ){
			alert(mstrExistenRegistros);
			document.getElementById("txtDscProcedencia").focus();						
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
	}	
	function fc_Grabar(){		
		a=document.getElementById("txtDscProcedencia").value;		
		if(a==""||fc_Trim(a)==""){
			alert('Debe ingresar el Tipo de Procedencia.');
			document.getElementById("txtDscProcedencia").value="";
			document.getElementById("txtDscProcedencia").focus();
			return;
		}		
		if(confirm(mstrSeguroGrabar)){
			if(document.getElementById("txhOperacion").value=="MODIFICAR"){			
				document.getElementById("frmMain").submit();
			}
			if(document.getElementById("txhOperacion").value=="AGREGAR"){						
				document.getElementById("frmMain").submit();
			}
		}
	}	
</script>
</head>
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_procedencia_agregar.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />	
	<form:hidden path="msg" id="txhMsg"/>
	
    <form:hidden path="codigoSec" id="txhCodigoSec"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo">
		 		<font style="">
		 			<c:if test="${control.operacion=='MODIFICAR'}"> <!-- operacion -->
				 		Mantenimiento de Tipo de Procedencia Modificar
				 	</c:if>
				 	<c:if test="${control.operacion=='AGREGAR'}"> <!-- operacion -->
				 		Mantenimiento de Tipo de Procedencia Agregar
				 	</c:if>
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
	<table><tr height="1px"><td></td></tr></table>
	
	<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="2" border="0" bordercolor='gray'>
		<tr class="texto">
			<td nowrap>C�digo:</td>			
			<td>
				<form:input path="codigo" id="txtCodigo"
					maxlength="5"
					onkeypress="fc_PermiteNumeros();"								 
					cssClass="cajatexto_1" size="8" readonly="true"/>
			</td>
		</tr>
		<tr class="texto">
			<td nowrap>Tipo de Procedencia:</td>			
			<td>
				<form:input path="dscProcedencia" id="txtDscProcedencia"
					maxlength="50"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this.id,'procedencia');" 
					cssClass="cajatexto" size="50" />
			</td>
		</tr>					 
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
	</table>
	
	<br>	
	
</form:form>	
</body>
</html>