<%@ include file="/taglibs.jsp"%>
<head>
<script src="${ctx}/scripts/js/funciones_bib-numeros.js" type="text/JavaScript"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/JavaScript">
	
	function onLoad(){ 
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )		
			alert(mstrSeGraboConExito);
		else if ( objMsg.value == "ERROR" )		
			alert(mstrProblemaGrabar);		
	}
	
	function Fc_Graba(){
		if(confirm('�Seguro de Guardar la Configuraci�n?')){
			form = document.forms[0];
			var max = document.getElementById("tamanioLista").value;
			var datos = '';
			var dias = '';
			var sLu, sMa, sMi , sJu, sVi, sSa, sDo;
			for(i=0;i<max;i++) {
				codSala = document.getElementById("oCodSala_"+i).value;
				hi = document.getElementById("selHoraIni_"+i).value; //valor hora de inicio
				hf = document.getElementById("selHoraFin_"+i).value; //valor hora de fin
				dLU = document.getElementById("chk_"+i+"_LU");
				dMA = document.getElementById("chk_"+i+"_MA");
				dMI = document.getElementById("chk_"+i+"_MI");
				dJU = document.getElementById("chk_"+i+"_JU");
				dVI = document.getElementById("chk_"+i+"_VI");
				dSA = document.getElementById("chk_"+i+"_SA");
				dDO = document.getElementById("chk_"+i+"_DO");
												
				if(dLU.checked){ sLu = '1';} else {sLu = '0';}
				if(dMA.checked){ sMa = '1';} else {sMa = '0';}
				if(dMI.checked){ sMi = '1';} else {sMi = '0';}
				if(dJU.checked){ sJu = '1';} else {sJu = '0';}
				if(dVI.checked){ sVi = '1';} else {sVi = '0';}
				if(dSA.checked){ sSa = '1';} else {sSa = '0';}
				if(dDO.checked){ sDo = '1';} else {sDo = '0';}
				
				dias = sLu + ':' + sMa + ':' + sMi + ':' + sJu + ':' + sVi + ':' + sSa + ':' + sDo + ':';	
				datos += codSala + '$' + hi + '$' + hf + '$' + dias + '|';
				
				
			}
			
			form.cadenaDatos.value = datos;			
			form.txhOperacion.value = "GRABAR";
			form.submit();
		}
	}
	
	</script>
	
	<style>
		.celda_sup {	
			border-right: 1px solid #048BBA;
		}
		
		.celda_det {	
			border-right: 1px solid #048BBA;	
			border-top: 1px solid #048BBA;		
		}
	</style>
	
	<form:form action="${ctx}/biblioteca/bib_horario_salas.html" commandName="control" id="frmMain">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="tamanioLista" id="tamanioLista"/>
		<form:hidden path="cadenaDatos" id="cadenaDatos"/>
		<form:hidden path="codUsuario" id="txhCodUsuario"/>
		<form:hidden path="sede" id="txhSede"/>
		
		<!--T�tulo de la P�gina  -->		
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Horarios de Sala</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
			 </tr>
		</table>
		<table><tr height="5px"><td></td></tr></table>
	
		<table border="0" bgcolor="#eae9e9" class="" style="border: 1px solid #048BBA;margin-left:3px; width:98%;" cellspacing="0" cellpadding="2" height="50px">
			<tr>
		  	  	<td class="celda_det" width="30%"><b>Tipo de Sala</b></td>
		        <td class="celda_det" width="15%" align="center"><b>Hora Inicio</b></td>
		        <td class="celda_det" width="15%" align="center"><b>Hora Fin</b></td>
		        <td class="celda_det" width="40%" align="center"><b>D�as</b></td>
	     	</tr>
	     	
		     <c:forEach var="lista" items="${listaTipoSalas}" varStatus="loop">
		     	<tr>
		     		<td class="celda_det" style="background-color: #fff;"><c:out value="${lista.nombreSala}" />
		     			<input type="hidden" id="oCodSala_<c:out value="${loop.index}" />" value="${lista.codSala}">
		     		</td>
		     		<td align="center" class="celda_det" style="background-color: #fff;">
		     			<select id="selHoraIni_<c:out value="${loop.index}"/>" >
		     				<option value="0">&nbsp;-&nbsp;</option>
			     			<c:forEach var="horasI" items="${listaHoras}" varStatus="linicio">
			     				<option value="<c:out value="${horasI.codDetalle}" />"
			     					<c:if test="${horasI.codDetalle==lista.idHoraIni}"><c:out value="selected=selected"/>"</c:if>			     						     			
			     					><c:out value="${horasI.dscValor1}" /></option>  
			     			</c:forEach>
		     			</select>
		     		</td>
		     		<td align="center" class="celda_det" style="background-color: #fff;">
<%-- 		     			<c:out value="${lista.horaFin}" /> --%>
						<select id="selHoraFin_<c:out value="${loop.index}"/>" >
		     				<option value="0">&nbsp;-&nbsp;</option>
			     			<c:forEach var="horasF" items="${listaHoras}" varStatus="lfin">
			     				<option value="<c:out value="${horasF.codDetalle}" />"
			     					<c:if test="${horasF.codDetalle==lista.idHoraFin}"><c:out value="selected=selected"/>"</c:if>
			     				><c:out value="${horasF.dscValor2}" /></option>
			     			</c:forEach>
		     			</select>		     			
		     		</td>
		     		<td align="center" class="celda_det" style="background-color: #fff;">
		     		
		     			<c:set var="diasSemana" value="${lista.dias}"/>
		     			<c:set var="dias" value="${fn:split(diasSemana, ':')}" />
		     			<input type="checkbox" value="${dias[0]}" id="chk_<c:out value="${loop.index}" />_LU"
		     				<c:if test="${dias[0]=='1'}">
		     					<c:out value="checked='checked'" />
		     				</c:if> 
		     			 />Lu
		     			<input type="checkbox" value="${dias[1]}" id="chk_<c:out value="${loop.index}" />_MA"  
		     				<c:if test="${dias[1]=='1'}">
		     					<c:out value="checked='checked'" />
		     				</c:if>
		     			/>Ma
		     			<input type="checkbox" value="${dias[2]}" id="chk_<c:out value="${loop.index}" />_MI" 
		     				<c:if test="${dias[2]=='1'}">
		     					<c:out value="checked='checked'" />
		     				</c:if>		     			
		     			/>Mi
		     			<input type="checkbox" value="${dias[3]}" id="chk_<c:out value="${loop.index}" />_JU" 
		     				<c:if test="${dias[3]=='1'}">
		     					<c:out value="checked='checked'" />
		     				</c:if>
		     			/>Ju
		     			<input type="checkbox" value="${dias[4]}" id="chk_<c:out value="${loop.index}" />_VI" 
		     				<c:if test="${dias[4]=='1'}">
		     					<c:out value="checked='checked'" />
		     				</c:if>
		     			/>Vi
		     			<input type="checkbox" value="${dias[5]}" id="chk_<c:out value="${loop.index}" />_SA" 
		     				<c:if test="${dias[5]=='1'}">
		     					<c:out value="checked='checked'" />
		     				</c:if>
		     			/><span style="color: green;">S&aacute;</span>
		     			<input type="checkbox" value="${dias[6]}" id="chk_<c:out value="${loop.index}" />_DO" 
		     				<c:if test="${dias[6]=='1'}">
		     					<c:out value="checked='checked'" />
		     				</c:if>
		     			/><span style="color: red;">Do</span>
		     		</td>		     		
		     	</tr>
		     </c:forEach>	     	
	     				
		</table>
	
		<table border="0" width="98%" align="center" style="margin-left:3px;">
			<tr>
				<td align="center">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:Fc_Graba();" style="cursor:pointer"></a>		
				</td>
			</tr>
		</table>
	
	</form:form>