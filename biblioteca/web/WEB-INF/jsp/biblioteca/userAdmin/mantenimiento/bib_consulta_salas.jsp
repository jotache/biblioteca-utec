<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){			
	}
	
	function fc_Buscar(){		
		seleccion=document.getElementById("cboTipoSala").value;
		if(seleccion!=""){			
			document.getElementById("txhOperacion").value="BUSCAR";
			document.getElementById("frmMain").submit();
		}
		else{
			alert('Debe seleccionar una Sala.');
			return;
		}
	}
	
	function fc_Limpiar(){
		document.getElementById("cboTipoSala").value="";
		document.getElementById("txtDscSala").value="";
	}
</script>
</head>
<body topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_salas.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="sedeSel" id="txhSedeSelec"/>
	<form:hidden path="sedeUsuario" id="txhSedeUsuario"/>
	<form:hidden path="msg" id="txhMsg" />

	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left:4px; margin-top:3px; width:688px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo">
				Consulta de Salas				
				<c:choose>
					<c:when test="${control.sedeSel=='L'}"> (Lima)</c:when>
					<c:when test="${control.sedeSel=='A'}"> (Arequipa)</c:when>
					<c:when test="${control.sedeSel=='T'}"> (Trujillo)</c:when>					
				</c:choose>
			</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
    
	<div style="width:688px;">    
	<table background="${ctx}/images/biblioteca/fondomarco.png" border="0" cellspacing="2" cellpadding="1" style="margin-left:4px" class="tabla" height="50px">
		<tr>
			<td width="80">&nbsp;Tipo Sala:</td>
			<td  width="220">
				<form:select path="tipoSala" id="cboTipoSala" cssClass="cajatexto_o" cssStyle="width:200px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaTipoSala!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaTipoSala}" />
					</c:if>
				</form:select>					
			</td >
			<td  width="40">&nbsp;Sala:</td>
			<td  width="160">
				<form:input path="dscSala" id="txtDscSala"
					maxlength="50"
					onkeypress="fc_ValidaTextoNumeroEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'sala');" 
					cssClass="cajatexto" cssStyle="width:150px;"  />
			</td>
			<td align="right" width="60">
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="middle" title="Limpiar" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onClick="javascript:fc_Limpiar();" ></a>&nbsp;
					
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" title="Buscar"  align="middle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onClick="javascript:fc_Buscar();"></a>&nbsp;
			</td>
            <td width="98">&nbsp;
            </td>
		</tr>		
	</table>
    </div>
	
<table><tr height="5px"><td></td></tr></table>
	
	<iframe id="iFrameGrilla" name="iFrameGrilla" frameborder="0" height="350px" width="688" src="${ctx}/biblioteca/consulta_salas_paginado.html?txhCodUsuario=<c:out value='${control.codUsuario}'/>&sede=<c:out value='${control.sedeSel}'/>">
	</iframe>
		
</form:form>	
</body>
</html>