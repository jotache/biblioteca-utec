<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script language=javascript>
    function onLoad()
	{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrActualizo);
			window.close();
		}
		else{ if ( objMsg.value == "ERROR" )
		       {
			         alert(mstrProblemaGrabar);			
		       }
		      else{ if ( objMsg.value == "DUPLICADO" )
		            alert(mstrExistenRegistros);}
		}
	}

	function fc_Modificar(){
		objDescripcion = document.getElementById("descripcionDescriptor").value;
		
		if (fc_Trim(objDescripcion) == "")
		{	alert('Debe ingresar una descripción.');
			return;
		}
		document.getElementById("txhOperacion").value = "MODIFICAR";
		document.getElementById("frmMain").submit();
		
	}
	
	function fc_cancelar(){
	//window.opener.document.getElementById("frmMain").submit();
	}

</script>	
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form action="${ctx}/biblioteca/bib_descriptores_modificar.html" commandName="control" id="frmMain">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="codValor" id="txhCodValor" />
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>
			
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo"><font style="">Mantenimiento de Descriptores Modificar</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			 </tr>
		</table>
		<table><tr height="1px"><td></td></tr></table>
		<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="2" border="0" bordercolor='gray'> 
			<tr>
				<td nowrap="nowrap" width="20%" align="left">C&oacute;digo :</td>
				<td align="left">&nbsp;
					<form:input path="codigoDescriptor" id="codigoDescriptor" 
					onblur="fc_ValidaNumeroOnBlur('codigoDescriptor');"  
					onkeypress="fc_PermiteNumeros();" maxlength="3"
					cssClass="cajatexto_1" size="8" readonly="true" />				
				</td>
            </tr>
			<tr>
				<td nowrap="nowrap" align="left">Descriptor :</td>
				<td align="left">&nbsp;
					<form:input path="descripcionDescriptor" id="descripcionDescriptor" 
					onkeypress="fc_ValidaNombreAutorOnkeyPress();" maxlength="50"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Descriptor');" 
					 cssClass="cajatexto" size="50" />
				</td>
			</tr>
		</table>	
		<table><tr height="5px"><td></td></tr></table>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Modificar();" style="cursor:pointer;"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">					
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();fc_cancelar();" ID="Button	1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
	</form:form>
	</body>
</html>