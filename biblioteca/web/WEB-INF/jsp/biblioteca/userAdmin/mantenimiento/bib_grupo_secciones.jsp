<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	var contador=0;
	var operacion;
    function onLoad()
	{   document.getElementById("txhFlgOpe").value = "1";
		objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK" ){
			window.opener.fc_ActualizarGrupo();					
			alert(mstrSeguroGrabar);
		}		
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
	}	
	function fc_Agregar(){
		if(operacion!="MODIFICAR"){
			var lista=fc_CrearLista();		
			dscGrupo=document.getElementById("txtGrupo").value;
			
			if(dscGrupo==""||fc_Trim(dscGrupo)==""){
				alert("Debe ingresar una descripci�n.");
				document.getElementById("txtGrupo").value="";
				document.getElementById("txtGrupo").focus();
				return false;
			}
			
			if(fc_VerificarDescripcion(dscGrupo,lista)==true){
				alert("Debe ingresar otra descripci�n.");			
				document.getElementById("txtGrupo").focus();
				return false;
			}
			
			if(fc_ObtenerNroRegistros()>=100){
				alert('No puede ingresar m�s de 5 grupos.');
				document.getElementById("txtGrupo").values="";
				return false;
			}
				
			var tr = document.createElement("<tr class='tablagrilla' id='trGrupo"+contador+"'>");
			
			var td01 = document.createElement("<td align='center'>");					
			var td02 = document.createElement("<td align='left' id='tdGrupo"+contador+"'>");
			
			var hidden="<input type='hidden' id='hidGrupo"+contador+"' value=''>";						
			var radio="<input type='radio' id='rbtGrupo"+contador+"' name='radio' value='"+dscGrupo+"' onclick=\"fc_SeleccionarRegistro(this.value,'"+contador+"','');\">";		
			
			td01.appendChild(document.createElement(hidden));
			td01.appendChild(document.createElement(radio));		
			td02.appendChild(document.createTextNode(dscGrupo));
			
			tr.appendChild(td01);
			tr.appendChild(td02);
			
			document.getElementById("tablaGrupo").lastChild.appendChild(tr);		
			document.getElementById("txtGrupo").value="";
		
			contador=contador+1;
			
			return true;
		}
		else{
			dscGrupo=document.getElementById("txtGrupo").value;
			
			if(dscGrupo==""||fc_Trim(dscGrupo)==""){
				alert("Debe ingresar una descripci�n.");
				document.getElementById("txtGrupo").value="";
				document.getElementById("txtGrupo").focus();
				return false;
			}
						
			seleccion=document.getElementById("txhSeleccion").value;
			codigo=document.getElementById("txhCodigo").value;
			document.getElementById("tdGrupo"+seleccion).innerText=document.getElementById("txtGrupo").value;
			document.getElementById("rbtGrupo"+seleccion).value=document.getElementById("txtGrupo").value;
			
			operacion="";
			document.getElementById("txhSeleccion").value="";
			document.getElementById("txhCodigo").value="";
			document.getElementById("txtGrupo").value="";
			document.getElementById("rbtGrupo"+seleccion).checked=false;
			
			return true;			
		}
	}	
	function fc_Quitar(obj){
		if(document.getElementById("txhSeleccion").value==""){
			alert("Debe seleccionar un registro.");
			return;
		}				
		seleccion=document.getElementById("txhSeleccion").value;
		var tr=document.getElementById("trGrupo"+seleccion);		
		var padre = tr.parentNode;
		// Eliminamos el hijo (el) del elemento padre		
		padre.removeChild(tr);		
		document.getElementById("txhSeleccion").value="";
		
		document.getElementById("txhFlgOpe").value = "2"; //Eliminar
		
		fc_Grabar();
	}	
	function fc_SeleccionarRegistro(descripcion,seleccion,codGrupo){
		document.getElementById("txhSeleccion").value=seleccion;
		document.getElementById("txhDescripcion").value=descripcion;		
		document.getElementById("txhCodigo").value=descripcion;
		//alert("codigo="+codGrupo+"\nseleccion="+seleccion+"\ndescripcion="+descripcion);						
	}
	function fc_Modificar(){
		document.getElementById("txhFlgOpe").value = "3"; //Modificar
		if(document.getElementById("txhSeleccion").value==""){
			alert("Debe seleccionar un registro.");
			return;
		}
		document.getElementById("txtGrupo").value=document.getElementById("txhDescripcion").value;
		operacion="MODIFICAR";
	}
	
	function fc_GrabarOpe(obj){
		document.getElementById("txhFlgOpe").value = obj; //Grabar nuevo Grupo
		fc_Grabar();
	}
	
	function fc_Grabar(){
		if(fc_Trim(document.getElementById("txhFlgOpe").value) == "1" ||
		   fc_Trim(document.getElementById("txhFlgOpe").value) == "3"){
			if(!fc_Agregar())return false;
		}
		
		cel = document.getElementById('tablaGrupo');
		var st="";
		var numeroGrupos=0;		
     	for (i=0; ele=cel.getElementsByTagName('input')[i]; i++){
     		if(i==0){
     			st=st+ele.value;
     		}
     		else{
     			if(i%2==0){			     				     		
     				st=st+"|"+ele.value;
     			}
     			else{     				
     				st=st+"$"+ele.value;
     				numeroGrupos++;
     			}
     		}	     	
     	}
     	
		document.getElementById("txhCadena").value=st;
		document.getElementById("txhNroRegistros").value=numeroGrupos;
		document.getElementById("txhOperacion").value="GRABAR";		
		
		if(fc_Trim(document.getElementById("txhFlgOpe").value) == "1" ||
		   fc_Trim(document.getElementById("txhFlgOpe").value) == "3" ){
				if(confirm(mstrSeguroGrabar))
					document.getElementById("frmMain").submit();
		}
		else if(fc_Trim(document.getElementById("txhFlgOpe").value) == "2"){
			if(confirm(mstrSeguroEliminar1)){			
				document.getElementById("frmMain").submit();
			}
			else{
				document.getElementById("txhOperacion").value="LOAD";
				document.getElementById("frmMain").submit();
			}
			
		}
	}
	function fc_CrearLista(){     	
     	var listaDscGrupo = new Array();		
		
     	tab = document.getElementById("tablaGrupo");
		hijoTabla=tab.getElementsByTagName('tr');
				
		var contadorLista=0;
		numeroHijoTabla=hijoTabla.length-1;
		
		for (var i=1; i<=numeroHijoTabla; i++){						
			var tr = tab.getElementsByTagName('tr')[i]; 
			var td = tr.getElementsByTagName('td')[0];
						
			var input = td.getElementsByTagName('input')[1];						
			
			listaDscGrupo[contadorLista]=input.value;
			contadorLista++;				
		}				
		return listaDscGrupo;
	}
	function fc_VerificarDescripcion(descripcion, listaDscGrupo){	
		
		var band=false;
		tama�oLista=listaDscGrupo.length;		
		for(var i=0;i<tama�oLista;i++){
			if(descripcion==listaDscGrupo[i]){				
				band=true;
			}
		}		
		return band;
	}
	function fc_ObtenerNroRegistros(){
		tab=document.getElementById("tablaGrupo");
		hijoTabla=tab.getElementsByTagName('tr');
		nroRegistros=hijoTabla.length-1;
		return nroRegistros;
	}
	
	function fc_Limpiar(){
		document.getElementById("txhFlgOpe").value = "";
		document.getElementById("txtGrupo").value="";
		document.getElementById("txhDescripcion").value="";
		document.getElementById("txhCadena").value="";
		document.getElementById("txhSeleccion").value="";
		
		nro = fc_ObtenerNroRegistros();
		for (i=0;i<nro;i++){
			if(document.getElementById("rbtGrupo"+i)!=null){
				document.getElementById("rbtGrupo"+i).checked=false;
			}
		}
	}
	
</script>
</head>
<body topmargin="10" leftmargin="10" rightmargin="10">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_grupo_secciones.html">
	<form:hidden path="operacion" id="txhOperacion" />	
	<form:hidden path="msg" id="txhMsg"/>		
	<form:hidden path="cadena" id="txhCadena"/>
	<form:hidden path="nroRegistros" id="txhNroRegistros"/>
	<form:hidden path="seleccion" id="txhSeleccion"/>
	<form:hidden path="descripcion" id="txhDescripcion"/>
	<form:hidden path="codigo" id="txhCodigo"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	
	<input type="hidden" id="txhFlgOpe" name="txhFlgOpe" value="" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:5px; margin-right:3px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="350px" class="opc_combo">
		 		<font style="">		 	
				 	Mantenimiento de Grupos				 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
			
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" style="width:98%; margin-left:3px;" class="tabla" height="30px" bordercolor="red">
		<tr>
			<td>Grupo:&nbsp;
				<form:input path="grupo" id="txtGrupo" maxlength="30"					
					onkeypress="fc_ValidaTextoNumeroEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this.id,'grupo');" 
					cssClass="cajatexto" size="30" />
			</td>
			<td>
				&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabarfec','','${ctx}/images/iconos/grabar2.jpg',1)">
						<img hspace="6" align="absmiddle" src= "${ctx}/images/iconos/grabar1.jpg" id="imggrabarfec" onclick="javascript:fc_GrabarOpe('1');" style="cursor:pointer;" alt="Grabar"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;						
			</td>
			
		</tr>		
	</table>
			
	<table cellpadding="0" cellspacing="0" id="Table1" width="98%" border="0" class="" style="margin-left:3px" bordercolor="red">
		<tr><td height="7px"></td></tr>
		<tr>
			<td valign="top">
<!--			style="height: 140px;"-->
			
				<div style="overflow: auto; height: 160px; width: 100%;border: 0px">
				<table cellpadding="0" cellspacing="1" align="center" style="border: 1px solid #048BBA;width:95%;" id="tablaGrupo">
					<tr class="grilla" height="30px">
						<td width="8%">Sel.</td>
						<td width="92%">Grupo</td>						
					</tr>
																	
					<c:forEach varStatus="loop" var="lista" items="${control.listaGrupos}" >												
					<tr class="tablagrilla" id='trGrupo<c:out value="${loop.index}"/>'> 						
						<td align="center">
							<input type="hidden" id='hidGrupo<c:out value="${loop.index}"/>' value='<c:out value="${lista.codTipoTablaDetalle}" />'>
							
							<input type="radio"	name="radio" value='<c:out value="${lista.descripcion}" />' id='rbtGrupo<c:out value="${loop.index}"/>' onclick="fc_SeleccionarRegistro(this.value,'<c:out value="${loop.index}" />','<c:out value="${lista.codTipoTablaDetalle}" />');">
							<script type="text/javascript">
								contador=contador+1;													
							</script>
						</td>
						<td align="left" id='tdGrupo<c:out value="${loop.index}"/>'>
							<c:out value="${lista.descripcion}" />							
						</td>						
					</tr>
					</c:forEach>					
				</table>
				</div>
			</td>
			<td width="31px" valign="top" align="right">
				<!-- 
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
				
				</a>
				-->
				<table><tr height="20px"><td></td></tr></table>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
					<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
						id="imgModificarPerfil" onclick="javascript:fc_Modificar();">
				</a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliminarPerfil" onclick="javascript:fc_Quitar();">
				</a>
			</td>			
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td style="display:none">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_GrabarOpe('1');"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		
		</tr>
	</table>	
	<br>
</form:form>
</body>
</html>