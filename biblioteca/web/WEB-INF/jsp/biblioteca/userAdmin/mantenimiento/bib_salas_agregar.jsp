<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>

<%-- <script src="${ctx}/scripts/jquery/jquery-1.9.1.js" type="text/JavaScript"></script> --%>
<%-- <script src="${ctx}/scripts/jquery/jquery-ui-1.10.1.custom.js" type="text/JavaScript"></script> --%>
<%-- <script src="${ctx}/scripts/jquery/ui/jquery.ui.datepicker.js" type="text/javascript"></script> --%>
<%-- <script src="${ctx}/scripts/jquery/ui/i18n/jquery.ui.datepicker-es.js" type="text/javascript"></script> --%>


<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#txtFechaInicio, #txtFechaFin').datepicker({
	        changeMonth: false,	        
	        dateFormat: "dd/mm/yy",
	        firstDay: 1,
		    changeFirstDay: false
	    });
		
		$("#txtFechaInicio").datepicker($.datepicker.regional["es"]);
		$("#txtFechaFin").datepicker($.datepicker.regional["es"]);
		
	});
</script>

<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
    function onLoad(){
	   objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK" ){
			window.opener.fc_Actualizar();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "IGUAL" ){
			alert('La denominaci�n ya existe para otro registro.');			
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}	
					
	}	
	function clearDates(objValue){
		var fecini = $("txtFechaInicio"); 
		var fecfin = $("txtFechaFin");
		var imgini = $("imgFecIni");
		var imgfin = $("imgFecFin");			
	}
	
	//Grabar
	function fc_Grabar(){		
		a=document.getElementById("txtDscSala").value;
		b=document.getElementById("cboTipoSala").value;
		c=document.getElementById("txtCapacidad").value;
		
		if(b==""){
			alert('Debe seleccionar una Sala.');
			document.getElementById("cboTipoSala").focus();
			return;
		}		
		if(a==""||fc_Trim(a)==""){
			alert('Debe ingresar la Sala.');
			document.getElementById("txtDscSala").value="";
			document.getElementById("txtDscSala").focus();
			return;
		}
		if(c==""||fc_Trim(c)==""){
			alert('Debe ingresar la Capacidad.');
			document.getElementById("txtCapacidad").value="";
			document.getElementById("txtCapacidad").focus();
			return;
		}	

		if($("txhOperacion").value=="MODIFICAR"){
			//si ingresa fecha de inicio de inactividad entonces debe colocar fecha de fin...
			fecini = $("txtFechaInicio").value;
			fecfin = $("txtFechaFin").value;
			if(fecini!='' && fc_Trim(fecfin)==''){
				alert('Complete las fechas de inicio y fin de inactividad.');
				return;
			}
			if (fecfin!='' && fc_Trim(fecini)==''){
				alert('Complete las fechas de inicio y fin de inactividad.');
				return;
			}

			if(fc_Trim(fecini)!='' && fc_Trim(fecfin)!=''){
				if(fc_ValidaFechaIniFechaFin2(fecfin,fecini)!="1"){
					alert('La fecha final debe ser mayor o igual a la inicial');
					return false;
				}
			}
			
			//se debe seleccionar las horas de inicio y fin para ambas fechas...
			hi1 = $("codHoraInicio1").value;					
			hi2 = $("codHoraInicio2").value;
			
			if(hi1!='' && hi2!=''){
				if(fc_Trim(fecini)=='' || fc_Trim(fecfin)==''){
					alert('Complete las fechas de inicio y fin de inactividad')
					return false;
				}
			}else{
				//&& (fecini!='' || fecfin!='')
// 				if((hi1=='' || hi2=='')) {
// 					alert('Deber�a completar las horas de inicio y fin.');
// 					return false;
// 				}
			}
										
			
		}
		
		
		if(confirm(mstrSeguroGrabar)){
			if(document.getElementById("txhOperacion").value=="MODIFICAR"){			
				document.getElementById("frmMain").submit();
			}
			if(document.getElementById("txhOperacion").value=="AGREGAR"){						
				document.getElementById("frmMain").submit();
			}
		}
		
		
	}	
	
	
</script>
</head>
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_salas_agregar.html">

	<form:hidden path="codUsuario" id="txhCodUsuario"  />
	<form:hidden path="operacion" id="txhOperacion"  />	
	<form:hidden path="msg" id="txhMsg"  />
	
    <form:hidden path="codigoSec" id="txhCodigoSec"  />
    <form:hidden path="codigo" id="txhCodigo"  />
	<form:hidden path="sede" id="sede"  />
	
	<table cellpadding="0" cellspacing="0" border="0" style="margin-center:2px;margin-top:2px; margin-right:2px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo">
		 		<font style="">
		 			<c:if test="${control.operacion=='MODIFICAR'}"> <!-- operacion -->
				 		Mantenimiento de Salas Modificar
				 	</c:if>
				 	<c:if test="${control.operacion=='AGREGAR'}"> <!-- operacion -->
				 		Mantenimiento de Salas Agregar
				 	</c:if>
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
	
	<table><tr height="1px"><td></td></tr></table>
	
	<table class="tablaflotante" style="width:456px;margin-top:2px; margin-center:2px; margin-left:2px;" cellspacing="3" cellpadding="2" border="0" > 
		<tr class="texto">
			<td width="120">Tipo Sala:</td>
			<td width="336">
				<form:select path="tipoSala" id="cboTipoSala" cssClass="cajatexto_o" cssStyle="width:200px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaTipoSala!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaTipoSala}" />
					</c:if>
				</form:select>					
			</td>						
		</tr>
		<tr class="texto">
			<td>Sala:</td>
			<td>
				<form:input path="dscSala" id="txtDscSala"
					maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNumerosAndLetrasFinalTodo(this,'sala');"								 
					cssClass="cajatexto" cssStyle="width:200px" />
			</td>
		</tr>
		<tr class="texto">
			<td>Capacidad:</td>			
			<td>							
				<form:input path="capacidad" id="txtCapacidad" 
					maxlength="3"
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroFinal(this,'capacidad');fc_ValidaNumeroFinalMayorCero(this);" 
					cssClass="cajatexto" size="5" />
			</td>
		</tr>	

		<c:if test="${control.operacion=='MODIFICAR'}">
			<tr class="texto">
				<td>Estado:</td>			
				<td>
					<%--  --%>
					<form:select path="codEstado" id="estadoSala" cssClass="cajatexto_o" cssStyle="width:100px" onchange="clearDates(this.value);">
						<form:option value="A">Activo</form:option>
						<form:option value="I">Inactivo</form:option>
					</form:select>
					
				</td>
			</tr>		
			
			  <td colspan="2">
              	
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                	<tr>
                	  <td colspan="3" bgcolor="#FFFFFF" align="center">&nbsp<b>Inhabilitar Sala</b></td>
               	  </tr>
                	<tr>
                    	<td width="12%">&nbsp;Desde:</td>                        
                        <td width="22%"><form:input path="fechaInicio" id="txtFechaInicio" onkeypress="fc_ValidaFecha('txtFechaInicio');" 
			  			onblur="fc_ValidaFechaOnblur('txtFechaInicio');"   
			  			cssClass="cajatexto_o" cssStyle="width:60px;" maxlength="10"/>

			  		<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgFecIni','','/biblioteca/images/iconos/calendario2.jpg',1)">
			  			<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"   
			  				alt="Calendario" style='cursor:hand;' align="middle">
					</a></td>
                        <td width="54%" rowspan="2" align="left" valign="middle">
                        <table width="150" border="0" cellpadding="0" cellspacing="0">                          
                          <tr>
                            <td align="center" width="75">Empieza</td>
                            <td align="center" width="75">Termina</td>
                          </tr>
                          <tr>
                            <td align="center">
                            	<form:select path="codHoraInicio1" id="codHoraInicio1" cssClass="cajatexto" cssStyle="width:55px">
                                    <form:option value="">-Hora-</form:option>
                                    <c:if test="${control.listaHoras!=null}">
                                        <form:options itemValue="idHoraIni" itemLabel="horaIni" items="${control.listaHoras}" />
                                    </c:if>
                                </form:select>&nbsp;
                        	</td>
                            <td align="center">
                            	<form:select path="codHoraInicio2" id="codHoraInicio2" cssClass="cajatexto"	cssStyle="width:55px" >
                                    <form:option value="">-Hora-</form:option>
                                    <c:if test="${control.listaHoras!=null}">
                                        <form:options itemValue="idHoraFin" itemLabel="horaFin" items="${control.listaHoras}" />
                                    </c:if>
                                </form:select>&nbsp;
                            </td>
                          </tr>
                        </table>
                          
                          
                      </td>
                    </tr>
                                                            
                    <tr>
                    	<td>&nbsp;Hasta:
                        </td>
                        <td><form:input path="fechaFin" id="txtFechaFin" onblur="fc_ValidaFechaOnblur('txtFechaFin');"
			   			onkeypress="fc_ValidaFecha('txtFechaFin');"
			   			cssClass="cajatexto_o" cssStyle="width:60px;" maxlength="10" />

			  		<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgFecFin','','/biblioteca/images/iconos/calendario2.jpg',1)">
			  			<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin" 
			  			alt="Calendario" style='cursor:hand;' align="middle">
					</a></td>
                    </tr>
                </table>
                
              </td>
		  </tr>
		</c:if>		
				 
	</table>
<table><tr height="5px"><td></td></tr></table>
	<table class="tablaflotante" style="width:456px;margin-top:2px;" cellspacing="3" cellpadding="2" border="0" > 
		<tr>
        	<td width="80">&nbsp;</td>
			<td align="center" width="148">
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onClick="javascript:fc_Grabar();"></a></td>
			<td align="center" width="148">
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onClick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
			<td width="80">&nbsp;</td>
		</tr>
	</table>
	
	<br>	
	
</form:form>	
<script type="text/javascript">
	var operacion = $("txhOperacion").value;	
	if (operacion!='AGREGAR' && !(operacion==null || operacion==false)){
		Calendar.setup({
			inputField     :    "txtFechaInicio",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFecIni",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaFin",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFecFin",
			singleClick    :    true
		});	
		
		var stateValue = $F("estadoSala");
		clearDates(stateValue);
	}
</script>
</body>
</html>