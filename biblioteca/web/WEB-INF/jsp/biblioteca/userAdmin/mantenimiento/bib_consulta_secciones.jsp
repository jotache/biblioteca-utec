<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	
	function onLoad(){
		if(document.getElementById("txhOpcion").value=='1'){		
			document.getElementById("radio01").checked=true;
		}
		if(document.getElementById("txhOpcion").value=='0'){
			document.getElementById("radio02").checked=true;
		}
		fc_TipoSeccion();
		fc_MostrarPaginado();		
	}	
	
	function fc_Actualizar(){
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_Buscar(){
		seleccion=document.getElementById("cboSeccion").value;
		if(seleccion==""){
			alert('Debe seleccionar una Secci�n.');
			return;
		}
		fc_EstablecerOpcion();		
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	function fc_MostrarPaginado(){
		if(document.getElementById("cboSeccion").value==document.getElementById("txhCodNovedades").value){
			tablaNovedades.style.display="";
			tablaEnlacesInteres.style.display="none";
		}
		else{
			tablaNovedades.style.display="none";
			tablaEnlacesInteres.style.display="";			
		}
	}
	
	function fc_EstablecerOpcion(){
		if(document.getElementById("radio01").checked==true){
			document.getElementById("txhOpcion").value=1;
		}
		else{
			document.getElementById("txhOpcion").value=0;
		}
	}
	
	function fc_Limpiar(){
		document.getElementById("cboSeccion").value="";
		document.getElementById("cboGrupo").value="";
		fc_TipoSeccion();
	}
	
	function fc_TipoSeccion(){
		if(document.getElementById("cboSeccion").value==document.getElementById("txhCodNovedades").value){
			trGrupo.style.display="";
			iFrameGrilla01.btnGrupo.style.display="";
			iFrameGrilla02.btnGrupo.style.display="";			
		}
		else{
			trGrupo.style.display="none";
			document.getElementById("cboGrupo").value="";
			iFrameGrilla01.btnGrupo.style.display="none";
			iFrameGrilla02.btnGrupo.style.display="none";		
						
		}		
	}
</script>
</head>
<body topmargin="0" leftmargin="10" rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_secciones.html">	
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />	
	
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="codUnico" id="txhCodUnico" />

	<form:hidden path="opcion" id="txhOpcion" />	
	<form:hidden path="codNovedades" id="txhCodNovedades" />

	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>	
	<form:hidden path="codUsuario" id="txhCodUsuario"/>

	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Consulta de Secciones</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" style="width:92%; margin-left:4px" class="tabla" height="55px" bordercolor="red">
		<tr>
			<td width="20%">Tipo Secci&oacute;n:</td>
			<td width="20%">
			    <form:select path="codSeccion"	id="cboSeccion" cssClass="cajatexto_o" cssStyle="width:180px" onchange="javascript:fc_TipoSeccion();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaSeccion!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
							items="${control.listaSeccion}" />
					</c:if>
				</form:select>
			</td>
			<td width="50%">			
				<input type="radio" checked id="radio01" name="radioCabecera">Vigentes&nbsp;				
				<input type="radio" id="radio02" name="radioCabecera">Todos				
			</td>			
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
			</td>
		</tr>
		<tr id="trGrupo" style="display: none;">
			<td width="20%">Grupos :</td>
			<td colspan="3">
				<form:select path="codGrupo" id="cboGrupo" cssClass="cajatexto_o" cssStyle="width:180px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listaGrupo!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
							items="${control.listaGrupo}" />
					</c:if>
				</form:select>
			</td>						
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
		
	<div id="tablaNovedades" style="display: none;">
		<iframe id="iFrameGrilla01" name="iFrameGrilla01" frameborder="0" height="350px" width="100%" src="${ctx}/biblioteca/bib_consulta_secciones_paginado.html">
		</iframe>
	</div>
	
	<div id="tablaEnlacesInteres" style="display: none;">
		<iframe id="iFrameGrilla02" name="iFrameGrilla02" frameborder="0" height="350px" width="100%" src="${ctx}/biblioteca/bib_consulta_secciones_paginado_enlaces_interes.html">
		</iframe>
	</div>
			
</form:form>	
</body>
</html>