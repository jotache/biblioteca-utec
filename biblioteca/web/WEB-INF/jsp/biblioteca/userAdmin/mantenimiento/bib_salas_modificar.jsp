<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	
	<script src="${ctx}/scripts/jquery/jquery-1.9.1.js" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/jquery/jquery-ui-1.10.1.custom.js" type="text/JavaScript"></script>
	
	<script language=javascript>
    function onLoad()
	{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrActualizo);
			window.close();
		}
		else if ( objMsg.value == "ERROR" )		
			alert(mstrProblemaGrabar);			
		
	}
    
   function fc_Sala(){
   }
   
    function fc_Modificar(){
		objDescripcion = document.getElementById("txtDscProceso");
		if (fc_Trim(objDescripcion.value) == "")
		{
			alert("Debe ingresar la descripcion del Perfil");
			return;
		}
		document.getElementById("txhOperacion").value = "MODIFICAR";
		document.getElementById("frmMain").submit();
		
	}
	
	function fc_cancelar(){
		window.opener.document.getElementById("frmMain").submit();
	}
</script>	
<body topmargin="3" leftmargin="3" rightmargin="3">
	<form:form action="${ctx}/biblioteca/bib_salas_modificar.html" commandName="control" id="frmMain">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="codDetalle" id="txhCodDetalle"/>
	<form:hidden path="msg" id="txhMsg"/>
						
		<table cellpadding="0" cellspacing="0" border="0" style="margin-left:3px;margin-top:5px">
             <tr>
                <td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
                <td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width="400px" class="opc_combo"><font style="">Modificar</font></td>
                <td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
             </tr>
		</table>	
		<table style="width:581;margin-top:3px" cellSpacing="0" cellPadding="0" border="0" align="center"> 
			<tr>
				<td valign='top' colspan="2">
					<table class="tablaflotante" cellSpacing="1" cellPadding="2" border="0" height="30px"> 
						<tr><td nowrap class="" width="30%">Tipo de Sala :
						<form:select path="tipoSala" id="tipoSala"
							cssClass="cajatexto" cssStyle="width:200px"
							onchange="javascript:fc_Sala();">
							<form:option value="-1">--Seleccione--</form:option>
							<c:if test="${control.codListaSala!=null}">
							<form:options itemValue="codProducto" itemLabel="descripcion"
							items="${control.codListaSala}" />
						    </c:if>
			                </form:select></td>
			           </tr>
						<tr>
							<td nowrap>Sala :			
								<form:input path="descripcionSala" id="descripcionSala" 
								onkeypress="fc_ValidaTextoEspecial1();"
								onblur="fc_ValidaSoloLetrasFinal1(this,'descripcion');" 
								 cssClass="cajatexto" cssStyle="width:200px" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
		<br>
		<table align=center>
			<tr>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onClick="javascript:fc_Modificar();" style="cursor:pointer;"></a>
				</td>
				<td>
					<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">					
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onClick="window.close();fc_cancelar();" ID="Button	1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
		<br>
	</form:form>
	</body>
</html>