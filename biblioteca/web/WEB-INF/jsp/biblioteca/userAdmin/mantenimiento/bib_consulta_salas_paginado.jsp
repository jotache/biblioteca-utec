<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
		if(document.getElementById("txhMsg").value=="OK"){
			fc_Actualizar();
		}
	}
	function fc_Actualizar(){		
		parent.document.getElementById("txhOperacion").value="BUSCAR";
		parent.document.getElementById("frmMain").submit();
	}
	function fc_Agregar(){		
		operacion="AGREGAR";
		document.getElementById("txhOperacion").value="BUSCAR";
		codUsuario=document.getElementById("txhCodUsuario").value;
		Fc_Popup("${ctx}/biblioteca/bib_salas_agregar.html?txhOperacion="+operacion+"&txhCodUsuario="+codUsuario+"&sede="+$('sedeSel').value,450,190);		
	}	
	function fc_Modificar(){
		operacion="MODIFICAR";
		codigoSec=document.getElementById("txhCodigoSec").value;
		codigoSala=document.getElementById("txhCodigoSala").value;
		descripcion=document.getElementById("txhDescripcion").value;
		tipoSala=document.getElementById("txhSeleccionTipoSala").value;
		capacidad=document.getElementById("txhCapacidad").value;
		seleccion=document.getElementById("txhSeleccion").value;
		codUsuario=document.getElementById("txhCodUsuario").value;
		if(seleccion==""){
			alert(mstrSeleccione);
			return;
		}		
		Fc_Popup("${ctx}/biblioteca/bib_salas_agregar.html?txhOperacion="+operacion+
			"&txhCodigoSec="+codigoSec+
			"&txhCodigoSala="+codigoSala+
			"&txhDescripcion="+descripcion+
			"&txhTipoSala="+tipoSala+
			"&txhCapacidad="+capacidad+
			"&txhCodUsuario="+codUsuario+"&sede="+$('#sedeSel').val()
			,470,270);
	}
	function fc_Quitar(){
		if (document.getElementById("txhSeleccion").value != ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhSeleccion").value="";
				document.getElementById("txhOperacion").value = "QUITAR";
				document.getElementById("frmMain").submit();
			}			
		}
		else{
			alert(mstrSeleccione);
			return false;
		}	
	}
	function fc_SeleccionarRegistro(codigoSec,codigoTipoSala,codigoSala,descripcion,capacidad){
		document.getElementById("txhSeleccion").value=codigoSec;
		document.getElementById("txhCodigoSec").value=codigoSec;		
		document.getElementById("txhCodigoSala").value=codigoSala;
		document.getElementById("txhDescripcion").value=descripcion;		
		document.getElementById("txhSeleccionTipoSala").value=codigoTipoSala;
		document.getElementById("txhCapacidad").value=capacidad;
		
		a=document.getElementById("txhCodigoSec").value;		
		b=document.getElementById("txhCodigoSala").value;
		c=document.getElementById("txhDescripcion").value;
		d=document.getElementById("txhSeleccion").value;
		e=document.getElementById("txhSeleccionTipoSala").value;
		f=document.getElementById("txhCapacidad").value;		
		
			
	}	
</script>
</head>
<body topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_salas_paginado.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	
	<form:hidden path="codigoSec" id="txhCodigoSec" />
	<form:hidden path="codigoSala" id="txhCodigoSala" />
	<form:hidden path="descripcion" id="txhDescripcion" />
	<form:hidden path="seleccionTipoSala" id="txhSeleccionTipoSala" />
	<form:hidden path="capacidad" id="txhCapacidad" />
	<form:hidden path="sedeSel" id="sedeSel"/>
	<form:hidden path="seleccion" id="txhSeleccion" />
	
	<table cellpadding="0" cellspacing="0" id="Table1" width="670" border="0" class="" style="margin-left:3px">
		<tr>
			<td>
				<div style="overflow: auto; height: 280px;">
				
                <display:table name="sessionScope.listaSala" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator" pagesize="10" requestURI="" 
					style="border: 1px solid #048BBA;width:98%;">
					
						<display:column property="rbtSelBibManSala" title="&nbsp;&nbsp;" headerClass="grilla" class="tablagrilla" style="text-align: center;width:6%"/>
						<display:column property="descripTipoSala" title="Tipo de Sala" headerClass="grilla" class="tablagrilla" style="text-align: left;width:20%"/>
						<display:column property="descripcion" title="Sala" headerClass="grilla" class="tablagrilla" style="text-align: left;width:25%"/>
						<display:column property="dscValor3" title="Cap." headerClass="grilla" class="tablagrilla" style="text-align: center;width:5%"/>
						<display:column property="estReg" title="Estado" headerClass="grilla" class="tablagrilla" style="text-align: center;width:10%"></display:column>						
						<display:column property="fechaHoraInicio" title="Desde" headerClass="grilla" class="tablagrilla" style="text-align: center;width:17%"></display:column>						
						<display:column property="fechaHoraFin" title="Hasta" headerClass="grilla" class="tablagrilla" style="text-align: center;width:17%"></display:column>

						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" 
                        	value="<tr class='cajatexto-login'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span>Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
                        <display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />                        
						<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
						<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
						<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
						<display:setProperty name="paging.banner.full" 
                        	value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
						<display:setProperty name="paging.banner.first" 
                        	value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
						<display:setProperty name="paging.banner.last" 
                        	value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
						<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
						
				</display:table>
				</div>				
			</td>
			<td width="25px" valign="top" align="right">
				<br/>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src= "${ctx}/images/iconos/agregar1.jpg" onClick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar" title="Nueva Sala">
				</a>
				<br/>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
					<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" title="Modificar Sala" 
						id="imgModificarPerfil" onClick="javascript:fc_Modificar();">
				</a>
				<br/>
				<%-- 
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliminarPerfil" onclick="javascript:fc_Quitar();">
				</a>
				--%>
			</td>
		</tr>		
	</table>	
</form:form>	
</body>
</html>