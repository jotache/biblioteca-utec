<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<head>
    <script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script language=javascript>

    function onLoad(){
        objMsg = document.getElementById("txhMsg");
        if ( objMsg.value == "OK" ){
            $('txhNumIP').setValue('');
            alert(mstrSeGraboConExito);
        }
        else if ( objMsg.value == "ERROR" )
            alert(mstrProblemaGrabar);
    }

    function fc_ValidarIP(ip) {
        if(ip != ''){
            partes=ip.split('.');
            if (partes.length!=4) {
                alert('ip no valida');
                $("txhNumIP").focus();
                return false;
            }
            for (i=0;i<4;i++) {
                num=partes[i];
                if (num>255 || num<0 || num.length==0 || isNaN(num)){
                alert('ip no valida');
                $("txhNumIP").focus();
                return false;
                }
            }
        }
        return true;
    }

    function IsNumber(evt){
        // Backspace = 8, Enter = 13, '0' = 48, '9' = 57, '.' = 46
        var key = window.Event ? evt.which : evt.keyCode;
        if(!key) key = evt.keyCode;
        return (key < 13 || (key >= 48 && key <= 57) || key == 46); //key < 13 (No responde al ENTER)
    }

    function fc_GrabarPermiso(){
        //cadena de texto
        form = document.forms[0];
        var max = form.txhTamanoLista.value;
        var nro = 0;
        var i;
        var datos='';
        var seleccion='';
        var codopcion='';
        for(i=1;i<=max;i++) {
            var nomChk = 'chkOpcion_' + i;
            codopcion = document.getElementById('otxtcodapoyo_' + i).value;
            if(document.getElementById(''+nomChk).checked==true)
                seleccion='S';
            else
                seleccion='N';

            datos += codopcion + '$' + seleccion + '$' + '|';
            nro = nro +1;
        }

        if(confirm('�Confirma que desea guardar los permisos?')){
            form.txhCadenaDatos.value = datos;
            form.txhTamanoLista.value = nro;
            form.operacion.value = "GUARDAR_PERMISOS";
            form.submit();
        }
    }

    function fc_Grabar(){
        if(fc_Trim(document.getElementById("txhNumIP").value) !== ""){
            document.getElementById("txhOperacion").value = "GRABAR";
            document.getElementById("frmMain").submit();
        }
        else{
            if(fc_Trim(document.getElementById("txhNumIP").value) == ""){
                alert('Debe ingresar una direcci�n IP v�lida');
                document.getElementById("txhNumIP").focus();
                return false;
            }
        }
    }

    function fc_Eliminar(param){
        if(confirm(mstrSeguroEliminar1)){
            document.getElementById("txhOperacion").value = "ELIMINAR";
            document.getElementById("txhNumIP").value = param;
            document.getElementById("frmMain").submit();
        }
    }

    </script>

    <script>
    $(function() {
        $( "#tabs" ).tabs();
    });
    </script>

</head>
<body topmargin="5" rightmargin="5" style="margin-left: 5px;">
    <form:form action="${ctx}/biblioteca/bib_AccesosLista.html" commandName="control" id="frmMain">
        <form:hidden path="operacion" id="txhOperacion"/>
        <form:hidden path="msg" id="txhMsg"/>
        <form:hidden path="codUsuario" id="txhCodUsuario"/>
        <form:hidden path="codSujeto"  id="txhCodSujeto"/>
        <form:hidden path="nomUsuario" id="txhNomUsuario"/>
        <form:hidden path="tamanoLstOpciones" id="txhTamanoLista"/>
        <form:hidden path="cadenaDatos" id="txhCadenaDatos"/>

    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Accesos por Direcci&oacute;n IP</a></li>
            <li><a href="#tabs-2">Permisos sobre Materiales</a></li>
        </ul>


    <div id="tabs-1">

        <table class="tablaflotante" style="width:400px; margin-top:0px; margin-left:0px; margin-right:0px;" cellSpacing="2" cellPadding="2" border="0" bordercolor='gray'>
            <tr>
                <td class="grilla" style="padding: 0px 6px; text-align: left; font-weight: bold;"><c:out value="${control.nomUsuario}"></c:out></td>
            </tr>
            <tr>
                <td valign='top'>

                        <table cellSpacing="0" cellPadding="5" border="0" width="100%" bordercolor="red">
                            <tr class="texto">
                                <td width="20">IP: </td>
                                <td valign="middle"><form:input path="numIP" id="txhNumIP" cssClass="cajatexto_o" size="30"
                                        onkeypress="return IsNumber(event);" onblur="fc_ValidarIP(this.value);"
                                        maxlength="15"/></td>
                                <td width="70">
                                    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
                                    <img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a>
                                </td>
                                <td width="85">
                                    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
                                    <img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
                                </td>
                            </tr>
                        </table>

                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="400" bgcolor="#eae9e9" style="border: 1px solid #048BBA;margin-top:5px; margin-left:0px; margin-right:0px;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="1" bordercolor="white" width="100%">
                        <tr>
                            <td class="grilla" width="20"><b>&nbsp;</b></td>
                            <td class="grilla" width="220"><b>IP</b></td>
                            <td class="grilla" width="140"><b>Fecha de Asignaci&oacute;n</b></td>
                            <td class="grilla" width="20"><b>&nbsp;</b></td>
                        </tr>
                        <c:forEach varStatus="loop" var="acceso" items="${control.usuario.listaAcceso}"  >
                        <tr class="texto">
                            <td class="" align="center"><c:out value="${loop.count}"></c:out> </td>
                            <td class="" style="padding-left: 10px;"><c:out value="${acceso.numIP}"></c:out> </td>
                            <td class="" align="center"><c:out value="${acceso.feCreacion}"></c:out> </td>
                            <td class="" align="center"><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('img_conf_${loop.count}','','${ctx}/images/iconos/kitar2.jpg',1)"
                                href="javascript:void(0);" onclick="fc_Eliminar('<c:out value="${acceso.numIP}"></c:out>');">
                                <img alt="Eliminar" src="${ctx}/images/iconos/kitar1.jpg" id="img_conf_${loop.count}" border="0"></a></td>
                        </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
        </table>

    </div>

    <div id="tabs-2">
        <table class="tablaflotante" style="width:400px; margin-top:0px; margin-left:0px; margin-right:0px;" cellSpacing="2" cellPadding="2" border="0" bordercolor='gray'>
            <tr>
                <td class="grilla" style="padding: 0px 6px; text-align: left; font-weight: bold;"><c:out value="${control.nomUsuario}"></c:out></td>
            </tr>
        </table>
        <table  cellpadding="0" cellspacing="0" width="400px" bgcolor="#eae9e9" style="border: 1px solid #048BBA;margin-left:0px;">
            <tr >
                <td colspan="2">

                    <table cellSpacing="0" cellPadding="1" bordercolor="white" width="100%"  border="0">
                        <tr>
                            <td class="grilla" width="10%">&nbsp;Nro.</td>
                            <td class="grilla" width="70%" align="left">Permiso</td>
                            <td class="grilla" width="20%">&nbsp;Sel.</td>
                        </tr>

                        <c:forEach varStatus="loop" var="opcion" items="${control.usuario.listaOpciones}"  >
                        <tr class="texto">
                            <td class="texto" align="center"><c:out value="${loop.count}"></c:out> </td>
                            <td class="texto" align="left">
                                <input type="hidden" name="otxtcodapoyo_<c:out value="${loop.count}"></c:out>" id="otxtcodapoyo_<c:out value="${loop.count}"></c:out>"
                                    value="<c:out value="${opcion.codOpcion}"></c:out>" />
                                <c:out value="${opcion.nomOpcion}"></c:out>
                            </td>
                            <td class="texto" style="padding-left: 10px;" align="center">
                                <input type="checkbox" id="chkOpcion_<c:out value="${loop.count}"></c:out>"
                                <c:if test="${opcion.seleccionado=='S'}">
                                    checked="checked"
                                </c:if>
                                />
                            </td>
                        </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
        </table>
        <table class="tablaflotante" style="width:400px; margin-top:0px; margin-left:0px; margin-right:0px;" cellSpacing="2" cellPadding="2" border="0" bordercolor='gray'>
            <tr class="">
                <td width="50%" align="center">
                    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar2','','${ctx}/images/botones/grabar2.jpg',1)">
                    <img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar2" style="cursor:pointer;" onclick="javascript:fc_GrabarPermiso();"></a>
                </td>
                <td width="50%" align="center">
                    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelarP2','','${ctx}/images/botones/cancelar2.jpg',1)">
                    <img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="btnCancelarP2" NAME="btnCancelarP2" style="cursor:pointer;"></a>
                </td>
            </tr>
        </table>

    </div>

    </div>

</form:form>
</body>