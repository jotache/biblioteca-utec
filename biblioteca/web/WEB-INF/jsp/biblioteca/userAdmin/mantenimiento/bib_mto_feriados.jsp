<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	
	function onLoad(){		
		
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			alert(mstrElimino);
		}		
		else if ( objMsg.value == "ERROR" )
		{
			alert(mstrProbEliminar);			
		}
		if(fc_Trim(document.getElementById("txhMeseses").value) !== ""){
			document.getElementById("cbomes").value = document.getElementById("txhMeseses").value;
		
		}
		document.getElementById("txhOperacion").value = "";
	}
	
	function aniadeOpcion(elSelect, texto, valor) {  
	    var laOpcion=document.createElement("OPTION");  
	    laOpcion.appendChild( document.createTextNode(texto) );  
	    laOpcion.setAttribute("value",valor);  
	    elSelect.appendChild(laOpcion);  
	}
	
	function Fc_Buscar(){		
		document.getElementById("txhMeseses").value = document.getElementById("cbomes").value;
		document.getElementById("txhAnosos").value = document.getElementById("cboAnios").value;
		document.getElementById("txhOperacion").value = "BUSCAR";
		document.getElementById("frmMain").submit();
	}	
	function Fc_Agregar(){
		flag = "2";
		Fc_Popup("${ctx}/biblioteca/bib_mto_agregar_feriado.html?txhcodEva=" + document.getElementById("txhCodEval").value + "&txhFlag=" + flag + "&txhSede=" + document.getElementById("txhSede").value
		,450,180);		
	}
		
	function fc_SeleccionarRegistro(strCod, strAnio, strDescripcion, strDia, strMes){
	document.getElementById("txhCod").value = strCod;
	document.getElementById("txhAnio").value = strAnio;
	document.getElementById("txhDes").value = strDescripcion;
	document.getElementById("txhDia").value = strDia;
	document.getElementById("txhmess").value = strMes;  
	}	
		
	function Fc_Modifica(){
		flag = "0";
		
		if (fc_Trim(document.getElementById("txhCod").value) != ""){
			Fc_Popup("${ctx}/biblioteca/bib_mto_agregar_feriado.html?txhcodEva=" + document.getElementById("txhCodEval").value + "&txhCod=" + document.getElementById("txhCod").value + "&txhAnio=" + document.getElementById("txhAnio").value
			+ "&txhDes=" + document.getElementById("txhDes").value + "&txhDia=" + document.getElementById("txhDia").value + "&txhmess=" + document.getElementById("txhmess").value + "&txhFlag=" + flag + "&txhSede=" + document.getElementById("txhSede").value
				,400,180);	
		}
		else{
			alert(mstrSeleccione);
				return false;	
		}
	}
		
	function Fc_Eliminar(){
	if ( fc_Trim(document.getElementById("txhCod").value) == "")
				{
				alert(mstrSeleccione);
				return false;
				}
	if( confirm(mstrEliminar) ){
		document.getElementById("txhOperacion").value = "ELIMINAR";
		document.getElementById("frmMain").submit();
		}	
	}	
	</script>	
</head>
<body topmargin="0" leftmargin="0" rightmargin="10px">
<form:form action="${ctx}/biblioteca/bib_mto_feriados.html" commandName="control" id="frmMain">	
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="cod" id="txhCod"/>
	<form:hidden path="descripcion" id="txhDes"/>
	<form:hidden path="dia" id="txhDia"/>
	<form:hidden path="meseses" id="txhMeseses"/>
	<form:hidden path="aniosos" id="txhAnosos"/>
	<form:hidden path="mess" id="txhmess"/>
	<form:hidden path="anio" id="txhAnio"/>
	<form:hidden path="codEval" id="txhCodEval"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="sede" id="txhSede"/>
	
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Consulta de Feriados</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" style="width:92%; margin-left:4px" class="tabla" height="50px">
		<tr>
			<td width="10%">Mes :</td>
			<td>
			<form:select path="mes"
						id="cbomes" cssClass="cajatexto"
						cssStyle="width:120px"  onchange="javascript:Fc_Buscar();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.codListaMeses!=null}">
							<form:options itemValue="id" itemLabel="name"
								items="${control.codListaMeses}" />
						</c:if>
				</form:select>
			&nbsp;&nbsp;&nbsp;
				<form:select path="ano" 
						id="cboAnios" cssClass="cajatexto"
						cssStyle="width:80px"  onchange="javascript:Fc_Buscar();">
						<c:if test="${control.codListaAnios!=null}">
							<form:options itemValue="id" itemLabel="name"
								items="${control.codListaAnios}" />
						</c:if>
					</form:select></td>
		</tr>	
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" id="Table1" border="0" class="" style="margin-left:3px" width="98%">
		<tr>
			<td>
				<!-- div style="overflow:hidden; height: 230px;width:98%"-->
					<display:table name="sessionScope.Fecha" cellpadding="0" cellspacing="1"
						pagesize="10" requestURI="" style="border: 1px solid #048BBA;width:98%;" 
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator" >
						<display:column property="rbtSelferiado" title="Sel." headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center" />
						<display:column property="mes" title="Mes" headerClass="grilla" class="tablagrilla" style="width:10%;text-align:center" />
						<display:column property="dia" title="D&iacute;a" headerClass="grilla" class="tablagrilla" style="text-align:center;width:10%"/>
						<display:column property="anio" title="A�o" headerClass="grilla" class="tablagrilla" style="text-align:center;width:10%"/>
						<display:column property="descripcion" title="Descripci&oacute;n" headerClass="grilla" class="tablagrilla" style="text-align:left;width:72%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='5' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span>Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
						<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado. </span>" />
						<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
						<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
						<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
						<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
						<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
						<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
						</display:table>
						<% request.getSession().removeAttribute("Fecha"); %>
					<!-- /div -->
			</td>
			<td width="30px" valign="top" align="right">
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:Fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
				</a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
					<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
						id="imgModificarPerfil" onclick="javascript:Fc_Modifica();">
				</a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliminarPerfil" onclick="javascript:Fc_Eliminar();">
				</a>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</form:form>	
</body>
</html>