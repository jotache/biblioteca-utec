<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
<script language=javascript>
    function onLoad()
	{   objMsg = document.getElementById("txhMsg");
	    
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else{ if ( objMsg.value == "ERROR" )
		       {
			         alert(mstrProblemaGrabar);			
		       }
		      else{ if ( objMsg.value == "DUPLICADO" )
		            alert(mstrExistenRegistros);}
		}
	}
	function fc_Valida(){
	
	  return true;
	}
	function fc_Grabar(){
		objDescripcion = document.getElementById("descriptor").value;
		
		if (  fc_Trim(objDescripcion) == "")
		{   
		    alert('Debe ingresar una descripción.');
			return;
		}
		else{
			document.getElementById("txhOperacion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}
	
	}
	function fc_cancelar(){
	
	}
</script>
</head>
<body leftmargin="5px" rightmargin="5px">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_descriptores_agregar.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codDetalle" id="txhCodDetalle"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>

		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo"><font style="">Mantenimiento de Descriptores Agregar</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			 </tr>
		</table>
		<table><tr height="1px"><td></td></tr></table>
		<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="2" border="0" bordercolor='red'> 
			<tr>
				<td nowrap="nowrap" width="20%" align="left">&nbsp;&nbsp;C&oacute;digo :</td>
				<td align="left">&nbsp;	
					<form:input path="codigo" id="codigo" maxlength="3"
					onblur="fc_ValidaNumeroOnBlur('codigo');"  
					onkeypress="fc_PermiteNumeros();" 
					cssClass="cajatexto_1" size="8" readonly="true" />				
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" align="left">&nbsp;&nbsp;Descriptor :</td>
				<td align="left">&nbsp;
					<form:input path="descriptor" id="descriptor" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'descriptor');" 
					cssClass="cajatexto" size="50" />
				</td>
			</tr>			
		</table>
		<table><tr height="5px"><td></td></tr></table>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();fc_cancelar();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
			</tr>
		</table>
</form:form>
</body>
</html>