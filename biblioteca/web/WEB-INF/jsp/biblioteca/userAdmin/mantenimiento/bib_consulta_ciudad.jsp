<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){		
	}		
	function fc_Buscar(){		
		seleccion=document.getElementById("cboPais").value;
		if(seleccion!=""){			
			document.getElementById("txhOperacion").value="BUSCAR";
			document.getElementById("frmMain").submit();
		}
		else{
			alert('Debe seleccionar un Pa�s.');
			return;
		}
	}	
	function fc_Limpiar(){
		document.getElementById("cboPais").value="";
		document.getElementById("txtDscCiudad").value="";
	}
</script>
</head>
<script src="${ctx}/scripts/js/funciones-textos.js" language="JavaScript;" type="text/JavaScript"></script>
<body topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_ciudad.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Consulta de Ciudades</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" style="width:92%; margin-left:4px" class="tabla" height="50px" bordercolor="red">	
		<tr>
			<td width="10%">Pa&iacute;s :</td>
			<td width="20%">
				<form:select path="pais" id="cboPais" cssClass="cajatexto_o" cssStyle="width:100px">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaPais!=null}">
						<form:options itemValue="paisId" itemLabel="paisDescripcion" 
							items="${control.listaPais}" />
					</c:if>
				</form:select>					
			</td>
			<td width="10%">Ciudad :</td>
			<td width="50%">
				<form:input path="dscCiudad" id="txtDscCiudad"
					maxlength="50"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this.id,'ciudad');" 
					cssClass="cajatexto" size="50" />
			</td>
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
			</td>
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	
	<iframe id="iFrameGrilla" name="iFrameGrilla" frameborder="0" height="350px" width="100%" src="${ctx}/biblioteca/consulta_ciudad_paginado.html?txhCodUsuario=<c:out value='${control.codUsuario}'/>">
	</iframe>
	
</form:form>	
</body>
</html>