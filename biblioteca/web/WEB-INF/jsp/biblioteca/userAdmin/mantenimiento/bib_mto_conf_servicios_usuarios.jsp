<%@page import="com.tecsup.SGA.modelo.TipoTablaDetalle"%>
<%@ include file="/taglibs.jsp"%>
<%@page import="java.util.List"%>
<script src="${ctx}/scripts/js/funciones_bib-numeros.js" type="text/JavaScript"></script>
<html>
<head>

<%List<TipoTablaDetalle> lista = (List<TipoTablaDetalle>)request.getAttribute("listaDiasPrestamo");%>

	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
<!-- 	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> -->
	<script language=javascript>	
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
			{
			    objMsg.value = "";
			    document.getElementById("txhMsg").value = "";
				alert(mstrSeGraboConExito);
			
			}
			else if ( objMsg.value == "ERROR" )
			{ 
				objMsg.value = "";
				document.getElementById("txhMsg").value = "";
				alert(mstrProblemaGrabar);			
			}
		}
		function fc_Busca(){
		
			document.getElementById("txhOperacion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_FlagSala(){
		if(document.getElementById("txhpresSala").value =="" || document.getElementById("txhpresSala").value =="0"){
		document.getElementById("txhpresSala").value = "1";
		}	
		else{
		document.getElementById("txhpresSala").value = "";
		}
		}
		
		function fc_FlagDomi(){
		if(document.getElementById("txhpresDomi").value =="" || document.getElementById("txhpresDomi").value =="0"){
		document.getElementById("txhpresDomi").value = "1";
		}	
		else{
		document.getElementById("txhpresDomi").value = "";
		}
		}  
		   
		function fc_SancionSI(){
		document.getElementById("txhapSanciones").value = "1";
		}
		
		function fc_SancionNO(){
		document.getElementById("txhapSanciones").value = "0";
		}
		    
		function fc_ReaReserSI(){
		document.getElementById("txhreaRe").value = "1";
		}
		
		function fc_ReaReserNo(){
		document.getElementById("txhreaRe").value = "0";
		}
		
		function fc_Graba(){
			if(document.getElementById('cboSelAluno').value==''){
				alert('Seleccione Tipo de Usuario');
				return false;
			}		
			
			//cadena de datos para configuracion maximo de dias de prestamos segun tipo de usuario y tipo de material....
			var i;
			var tipoMat;
			var nroMax;
			var max = document.getElementById("datosTamano").value;
			var datos ='';
			for(i=0;i<max;i++) {
				tipoMat = document.getElementById("txt_codtipomat_"+i).value;
				nroMax = document.getElementById("txtmaxdias_"+i).value;
				datos = datos + tipoMat + '$' + nroMax + '|';
			}
			
			$("#datosCadena").val(datos);			
			$("#txhOperacion").val('GRABAR');
 			document.getElementById("frmMain").submit();
		}		
	</script>	
</head>
<form:form action="${ctx}/biblioteca/bib_mto_conf_servicios_usuarios.html" commandName="control" id="frmMain">	
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codEval" id="txhcodEval"/>
	<form:hidden path="presSala" id="txhpresSala"/>	
	<form:hidden path="presDomi" id="txhpresDomi"/>
	<form:hidden path="apSanciones" id="txhapSanciones"/>
	<form:hidden path="reaRe" id="txhreaRe"/>
	
	<form:hidden path="datosCadena" id="datosCadena"/>
	<form:hidden path="datosTamano" id="datosTamano"/>
	
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Configuraci&oacute;n de Servicios por tipos de usuario</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
<!-- 	<table><tr height="5px"><td></td></tr></table>	 -->
	<table cellspacing="4" border="0" height="50px" bgcolor="#eae9e9" style="margin-left:3px; width: 92%;border: 1px solid #048BBA;">
		<tr>
			<td width="35%">&nbsp;Tipo de usuario : </td>
			<td colspan="3">&nbsp;<form:select  path="codAlumno" id="cboSelAluno" cssClass="cajatexto" 
					cssStyle="width:150px" onchange="javascript:fc_Busca();">
				<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.tipoAlumnos!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.tipoAlumnos}" />					        
					</c:if>
				</form:select>
		</td>
		</tr>
		<tr>
			<td>&nbsp;Tipo Pr&eacute;stamo :</td>
			<td nowrap="nowrap">&nbsp;<input type="checkbox"  id="presSalas" name="presSalas" <%if("1".equalsIgnoreCase((String)request.getAttribute("presSalas"))){ %> checked<%} %> onclick="javascript:fc_FlagSala();"/>Sala
			&nbsp;&nbsp;<input type="checkbox"  id="presDomicilio" name="presDomicilio" <%if("1".equalsIgnoreCase((String)request.getAttribute("presDomicilio"))){ %> checked<%} %> onclick="javascript:fc_FlagDomi();"/>Domicilio</td>
			<td colspan="2">&nbsp;</td>
		</tr>	
		<tr>
			<td nowrap="nowrap">&nbsp;Nro. M&aacute;ximo de Pr&eacute;stamos Pendientes (Domicilio) :</td>
			<td colspan="3">&nbsp;<form:input path="prestPen" id="txtPrestPen" 
								onkeypress="fc_PermiteNumeros();"
								onblur="fc_ValidaNumeroOnBlur('txtPrestPen');" 
								cssStyle="text-align:center"
								cssClass="cajatexto_o" size="5" maxlength="4"/>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="height: 4px;"></td>
		</tr>
		<tr>
			<td valign="top">&nbsp;Nro. M&aacute;ximo de D&iacute;as de Pr&eacute;stamo (Domicilio) :</td>
			<td colspan="3">
<%-- 					 <form:input path="diasPrest" id="txtDiasPrest"  --%>
<%-- 								onkeypress="fc_PermiteNumeros();" --%>
<%-- 								onblur="fc_ValidaNumeroOnBlur('txtDiasPrest');"  --%>
<%-- 								cssStyle="text-align:center" --%>
<%-- 								cssClass="cajatexto_o" size="5" maxlength="4"/> --%>
				
				<table border="0" bgcolor="#eae9e9" class="" style="border: 1px solid #048BBA;margin-left:3px; width:98%;" cellspacing="0" cellpadding="2">
					<tr>
						<td class="celda_sup" width="70%" style="background-color: #fff;"><b>Tipo de Material</b></td>
						<td class="celda_sup" width="30%" align="center" style="background-color: #fff;"><b>Max. d�as Prestamo</b></td>
					</tr>
					
					<%if(lista!=null && !lista.isEmpty()){												
					%>
					<c:forEach var="lista1" items="${listaDiasPrestamo}" varStatus="loop">																		
						<tr>
							<td class="celda_det" style="border-top-style:solid;border-top-width: 1px;border-top-color: #048BBA;">
								<input type="hidden" value="<c:out value="${lista1.codTipoMat}" />" name="txt_codtipomat_<c:out value="${loop.index}" />" id="txt_codtipomat_<c:out value="${loop.index}" />">							
								<c:out value="${lista1.descripcion}" />
							</td>
							<td class="celda_det" align="center" style="border-top-style:solid;border-top-width: 1px;border-top-color: #048BBA;">
								<input type="text" value="${lista1.dscValor1}" id="txtmaxdias_<c:out value="${loop.index}" />" style="width: 20px; text-align: center;" maxlength="3" class="cajatexto_o" onkeypress="fc_PermiteNumeros();">
							</td>
								
						</tr>
					</c:forEach>
					<%
					}else{%>
						<tr>	
							<td colspan="2" align="center" class="celda_det">No se encontraron registros</td>
						</tr>
					<%} %>
				</table>

			</td>
		</tr>	
		<tr>
			<td>&nbsp;Aplica Sanciones</td>
			<td>&nbsp;<input type=radio ID="Radio1" NAME="Checkbox1" <%if("1".equalsIgnoreCase((String)request.getAttribute("apliSancion"))){ %> checked<%} %> onclick="javascript:fc_SancionSI();">S&iacute; 
			&nbsp;&nbsp;<input type=radio ID="Radio2" NAME="Checkbox1" <%if("0".equalsIgnoreCase((String)request.getAttribute("apliSancion"))){ %> checked<%} %> onclick="javascript:fc_SancionNO();">No</td> 
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;Realiza Reservas</td>
			<td>&nbsp;<input type=radio ID="Radio3" NAME="Checkbox2" VALUE="Radio1" <%if("1".equalsIgnoreCase((String)request.getAttribute("resaReser"))){ %> checked<%} %> onclick="javascript:fc_ReaReserSI();">S&iacute; 
			&nbsp;&nbsp;<input type=radio ID="Radio4" NAME="Checkbox2" VALUE="Radio2" <%if("0".equalsIgnoreCase((String)request.getAttribute("resaReser"))){ %> checked<%} %> onclick="javascript:fc_ReaReserNo();">No</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;Nro. M&aacute;ximo de Pr&oacute;rrogas :</td>
			<td colspan="3">&nbsp;<form:input path="nroProrrogas" id="txtNroProrrogas" 
								onkeypress="fc_PermiteNumeros();"
								onblur="fc_ValidaNumeroOnBlur('txtDiasPrest');" 
								cssStyle="text-align:center"
								cssClass="cajatexto_o" size="5" maxlength="4"/>
			</td>
		</tr>	
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table width="10%" style="margin-right:50px" align="center" border="0">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Graba();" style="cursor:pointer"></a>			
			</td>
		</tr>
	</table>
</form:form>
