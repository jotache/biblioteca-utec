<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
<script language=javascript>
    
    function onLoad()
	{   objMsg = document.getElementById("txhMsg");
	   
		if ( objMsg.value == "OK" )
		{
			//window.opener.document.getElementById("frmMain").submit();
			//fc_Buscar
			alert(mstrSeGraboConExito);
			window.opener.document.getElementById("codCategoria").value = document.getElementById("codCategoria").value;
			window.opener.fc_Buscar();
			window.close();
		}
		else{ if ( objMsg.value == "ERROR" )
		       {
			         alert(mstrProblemaGrabar);			
		       }
		      else{ if ( objMsg.value == "DUPLICADO" )
		            alert(mstrExistenRegistros);}
		}
	}
	function fc_Valida(){
	
	  return true;
	}
	
	function fc_Grabar(){
		objDescripcion = document.getElementById("descripcionDewey").value;
		objCodigoDewey = document.getElementById("codigoDewey").value;
		if ((  fc_Trim(objDescripcion)!= "")&&(document.getElementById("codCategoria").value!="-1") && (fc_Trim(objCodigoDewey)!= ""))
		{   
		    document.getElementById("txhOperacion").value = "GRABAR";
		    document.getElementById("frmMain").submit();
		   
		}
		else{ if(document.getElementById("codCategoria").value=="-1"){ alert('Seleccione una categor�a'); document.getElementById("codCategoria").focus(); return false;}
			  if(fc_Trim(objCodigoDewey)== ""){ alert('Debe ingresar el c�digo.'); document.getElementById("codigoDewey").focus(); return false;}
			  if(fc_Trim(objDescripcion)== ""){ alert('Debe ingresar una descripci�n.'); document.getElementById("descripcionDewey").focus(); return false;}
		}
		
	}
	
	function fc_Categorias(){
	
	}
	function fc_cancelar(){
	//window.opener.document.getElementById("frmMain").submit();
	}
	
	function fc_ValidaNumeroGrr(){

		fc_ValidaNumerosAndLetrasFinalTodo('codigoDewey','Codigo');

		/*
		srtTexto=document.getElementById("codCategoria").value;
		
		var numTexto = parseFloat(srtTexto);
		
		if (numTexto>=10) {
			srtMaD = srtTexto.substring(2,4)-1;
		}else
			srtMaD = srtTexto.substring(3,4)-1;
				
		if(fc_Trim(document.getElementById("codigoDewey").value)!= ""){			
			srtMoon=document.getElementById("codigoDewey").value.substring(0,1);
									
			if(srtMaD==srtMoon)
			 	fc_ValidaNumerosAndLetrasFinalTodo('codigoDewey','Codigo');
			else{
			 	 document.getElementById("codigoDewey").focus();	 
			 	 alert('El c�digo ingresado no es correcto');
			}
		}
		*/
	}
	
</script>
</head>
	<body leftmargin="5px" rightmargin="5px">
	<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_dewey_agregar.html">
		<form:hidden path="operacion" id="txhOperacion" />
		<form:hidden path="codDetalle" id="txhCodDetalle"/>
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>

		<table cellpadding="0" cellspacing="0" border="0" style="margin-left:5px;margin-top:5px; margin-right:5px;">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="503px" class="opc_combo"><font style="">Mantenimiento de Clasificaci&oacute;n Dewey Agregar</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			 </tr>
		</table>
		<table><tr height="1px"><td></td></tr></table>
		<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="0" border="0" bordercolor='gray'> 
			<tr>
				<td width="20%" align="left">&nbsp;Categor&iacute;a :</td>
				<td align="left">&nbsp;
				    <form:select path="codCategoria" id="codCategoria" cssClass="cajatexto_o"	cssStyle="width:180px"  onchange="javascript:fc_Categorias();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCategoria!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.codListaCategoria}" /> 
						</c:if>
					</form:select>
				</td>							
		    </tr>
			<tr>
				<td align="left">&nbsp;C&oacute;digo :</td>
				<td align="left">&nbsp;
				<form:input path="codigoDewey" id="codigoDewey" 
					maxlength="10" 
					onblur="javascript:fc_ValidaNumeroGrr();" 
					onkeypress="fc_ValidaTextoEspecialAndNumeroTodo();"
					cssClass="cajatexto" size="10" /></td>
			</tr>
			<tr>
				<td align="left">&nbsp;Descripci&oacute;n :</td>
				<td align="left">&nbsp;
					<form:input path="descripcionDewey" id="descripcionDewey" 
					onkeypress="fc_ValidaNombreAutorOnkeyPress();" maxlength="100"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Descripcion');" 
					cssClass="cajatexto" size="80" />
				</td>
			</tr>		
		</table>
		<table><tr height="5px"><td></td></tr></table>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();fc_cancelar();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
			</tr>
		</table>
</form:form>
</body>
</html>