<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<head>
  <script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <script language=javascript>

    function onLoad(){
       objMsg = document.getElementById("txhMsg");
       if (objMsg.value == "OK" )
          alert(mstrSeGraboConExito);
       else if ( objMsg.value == "ERROR" )
          alert(mstrProblemaGrabar);
       else if ( objMsg.value == "OK_ELIMINAR" )
          alert(mstrElimino);
       else if ( objMsg.value == "ERROR_ELIMINAR" )
          alert(mstrProbEliminar);
    }

    function fc_Eliminar(codUsuario, codSujeto, nomUsuario){
        if (confirm('�Confirma que desea eliminar al usuario:\n ' + nomUsuario + '?')){
            document.getElementById("txhOperacion").value = 'ELIMINAR';
            document.getElementById("txhCodUsuario").value = codUsuario;
            document.getElementById("txhCodSujeto").value = codSujeto;
            document.getElementById("frmMain").submit();
        }
    }
  </script>
  </head>

  <!--T�tulo de la P�gina  -->
  <table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
     <tr>
       <td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
       <td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Pol&iacute;ticas de Acceso</font></td>
       <td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
     </tr>
  </table>
  <table><tr height="5px"><td></td></tr></table>
  <table cellpadding="0" cellspacing="0" width="92%" bgcolor="#eae9e9" style="border: 1px solid #048BBA;margin-left:3px;">
    <tr>
      <td>
        <table cellpadding="0" cellspacing="0" border="1" bordercolor="white" width="100%">
          <tr>
            <td class="grilla" width="20"><b>&nbsp;</b></td>
            <td class="grilla" width="80"><b>C&oacute;digo</b></td>
            <td class="grilla"><b>Apellidos y Nombres</b></td>
            <td class="grilla" width="20"><b>&nbsp;</b></td>
            <td class="grilla" width="20"><b>&nbsp;</b></td>
          </tr>
          <c:forEach varStatus="loop" var="usuario" items="${control.usuarios}"  >
          <tr class="texto">
            <td class="" align="center"><c:out value="${loop.count}"></c:out> </td>
            <td class="" align="center"><c:out value="${usuario.codSujeto}"></c:out> </td>
            <td class="" style="padding-left: 10px;"><c:out value="${usuario.nomUsuario}"></c:out> </td>
            <td class="" align="center"><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('img_conf_${loop.count}','','${ctx}/images/iconos/modificar2.jpg',1)"
              href="javascript:void(0);"
              onclick="Fc_PopupScroll('${ctx}/biblioteca/bib_AccesosLista.html?txhCodUsuario=<c:out value="${usuario.codUsuario}"></c:out>&txhCodSujeto=<c:out value="${usuario.codSujeto}"></c:out>&txhNomUsuario=<c:out value="${usuario.nomUsuario}"></c:out>',460,420);">
              <img alt="Ver Lista" src="${ctx}/images/iconos/modificar1.jpg" id="img_conf_${loop.count}" border="0" title="Modificar"></a></td>
            <td class="" align="center"><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('img_elim_${loop.count}','','${ctx}/images/iconos/kitar2.jpg',1)"
              href="javascript:void(0);"
              onclick="fc_Eliminar('${usuario.codUsuario}','${usuario.codSujeto}','${usuario.nomUsuario}');">
              <img alt="Eliminar" src="${ctx}/images/iconos/kitar1.jpg" id="img_elim_${loop.count}" border="0" title="Eliminar"></a></td>
          </tr>
          </c:forEach>
        </table>
      </td>
    </tr>
  </table>
  <br>
  <table border="0" width="10%" align="right" style="margin-right:45px">
    <tr>
      <td>
        <form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/biblioteca/bib_Accesos.html">
            <form:hidden path="msg"        id="txhMsg"/>
            <form:hidden path="operacion"  id="txhOperacion"/>
            <form:hidden path="codUsuario" id="txhCodUsuario"/>
            <form:hidden path="codSujeto"  id="txhCodSujeto"/>
        </form:form>
      </td>
    </tr>
  </table>