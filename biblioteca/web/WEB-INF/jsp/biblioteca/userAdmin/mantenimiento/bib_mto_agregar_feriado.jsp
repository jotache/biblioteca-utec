<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	
	<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	
	function onLoad(){
		dia=document.getElementById("cboDia");
		
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
			{
				objMsg.value = "";
				window.opener.document.getElementById("frmMain").submit();
				alert(mstrSeGraboConExito);
				window.close();
			}
			else if ( objMsg.value == "ERROR" )
			{
				objMsg.value = "";
				alert(mstrProblemaGrabar);			
			}
		}
	function esBisiesto(year) {  
	    return (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) ? true : false;  
		}  
	
	function limpiaSelect(elSelect) {  
	    while( elSelect.hasChildNodes() ) 
	        elSelect.removeChild(elSelect.firstChild );  
		}  
	
	function aniadeOpcion(elSelect, texto, valor) {  
	    var laOpcion=document.createElement("OPTION");  
	    laOpcion.appendChild( document.createTextNode(texto) );  
	    laOpcion.setAttribute("value",valor);  
	    elSelect.appendChild(laOpcion);  
	} 
	
	function rellenar_dia(elSelect, anio, mes ) {	
	    var ultimo_dia; 
	    // febrero 
	    if(mes==2) 
	        ultimo_dia=esBisiesto(anio)?29:28; 
	    // acaban en 31 
	    else if( mes==1 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12) 
	        ultimo_dia=31; 
	    else 
	        ultimo_dia=30; 
	    for(var a=1; a<=ultimo_dia; a++) { 
	        aniadeOpcion(elSelect, a, a); 
	    } 
	    
	    if(fc_Trim(document.getElementById("txhDia").value) !== ""){
			document.getElementById("cboDia").value = document.getElementById("txhDia").value;
		}
	    
	}
	function actualizar() {
	    if(document.getElementById("cboAnio").value!=""){
	    	limpiaSelect(dia);
	    	rellenar_dia(dia, document.getElementById("cboAnio").value ,document.getElementById("cboMes").value);
	    }
	}
	function fc_Grabar(){
		if(fc_Trim(document.getElementById("txtDscProceso").value) !== "" && 
		   fc_Trim(document.getElementById("txtFecha").value) != ""){
			//document.getElementById("txhDia").value = document.getElementById("cboDia").value;
			document.getElementById("txhOperacion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}
		else{
			if(fc_Trim(document.getElementById("txtFecha").value) == ""){
				alert('Debe ingresar el valor correspondiente en el campo '+" Fecha");
				document.getElementById("txtFecha").focus();
				return false;
			}
			if(fc_Trim(document.getElementById("txtDscProceso").value) == ""){
				alert('Debe ingresar el valor correspondiente en el campo '+" Descripcion.");
				document.getElementById("txtDscProceso").focus();
				return false;
			}
		}
	}
	</script>	
</head>
	
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form action="${ctx}/biblioteca/bib_mto_agregar_feriado.html" commandName="control" id="frmMain">	
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="dia" id="txhDia"/>
	<form:hidden path="codEval" id="txhCodEval"/>
	<form:hidden path="flag" id="txhFlag"/>
	<form:hidden path="cod" id="txhCod"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="sede" id="txhSede"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo">
		 		<font style="">	
				 	<c:if test="${control.flag=='0'}"> <!-- operacion -->
				 		Mantenimiento de Feriado Modificar
				 	</c:if>
				 	<c:if test="${control.flag=='2'}"> <!-- operacion -->
				 		Mantenimiento de Feriado Agregar
				 	</c:if>				 	
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
	
	<table><tr height="1px"><td></td></tr></table>
	
	<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="2" border="0" bordercolor='gray'>
		<tr>	
			<td valign='top' colspan="2">
				<table cellSpacing="4" cellPadding="5" border="0" width="100%" bordercolor="red"> 
					<tr class="texto" style="display:none;">
						<td>Mes / A�o :</td>			
						<td>&nbsp;
							<form:select path="mes"	id="cboMes" cssClass="cajatexto_o" cssStyle="width:120px"  onchange="javascript:actualizar();">
								<form:option value="0">--Seleccione--</form:option>
								<c:if test="${control.codListaMeses!=null}">
									<form:options itemValue="id" itemLabel="name"
										items="${control.codListaMeses}" />
								</c:if>
							</form:select>&nbsp;&nbsp;&nbsp;						
							<form:select path="ano" id="cboAnio" cssClass="cajatexto_o" cssStyle="width:80px"  onchange="javascript:actualizar();">
								<c:if test="${control.codListaAnios!=null}">
									<form:options itemValue="id" itemLabel="name"
										items="${control.codListaAnios}" />
								</c:if>
							</form:select>
						</td>
					</tr>
					<tr class="texto" style="display:none;">
						<td>D�a :</td>			
						<td>&nbsp;
							<select id="cboDia" class="cajatexto_o">
								<option value="" selected>&nbsp;&nbsp;&nbsp;&nbsp;</option>
							</select>							
						</td>
					</tr>
					<tr>
						<td>&nbsp;Fecha : </td>
						<td>&nbsp;
							<form:input path="txtFecha" id="txtFecha" cssClass="cajatexto_o" size="12" 
								onkeypress="fc_ValidaFecha('txtFecha');" onblur="fc_ValidaFechaOnblur('txtFecha');" 
								maxlength="10"/>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar','','${ctx}/images/iconos/calendario2.jpg',1)">
								<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar" 
									name="imgCalendar" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
						</td>
					</tr>
					<tr class="texto">
						<td nowrap>Descripci&oacute;n :</td>
						<td>&nbsp;			
							<form:input path="descripcion" id="txtDscProceso"
								maxlength="50%"
								onkeypress="fc_ValidaTextoEspecial();"								
								onblur="fc_ValidaNombreAutorOnblur('txtDscProceso','denominacion');" 
							 	cssClass="cajatexto" size="50" />
						</td>
					</tr>
				</table>
			</td>
		</tr>				
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
	</table>	
	<br>
</form:form>

<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecha",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgCalendar",
		singleClick    :    true
	});
</script>
</body>
</html>