<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
		fc_HabilitarCamposSeleccion();
		
		objMsg = document.getElementById("txhMsg");
			
		if ( objMsg.value == "OK" ){
			alert(mstrSeGraboConExito);
			document.getElementById("txhMsg").value="";
		}		
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			document.getElementById("txhMsg").value="";
		}		
	}	
	function fc_HabilitarCampos(obj,fila,columna){
		var estadoFila=true;		
		var estadoColumna=true;
		if(fila!=""){			
			for(var i=1;i<fila;i++){
				if(document.getElementById("rbt"+i+"0").checked==false){
					document.getElementById(obj).checked=false;
					estadoFila=false;
				}			
			}
			if(fila!=5){
				indice=parseInt(fila);
				indice=indice+1;			
				if(document.getElementById("rbt"+indice+"0").checked==true){
					document.getElementById(obj).checked=true;
					estadoFila=false;	
				}
			}
			if(estadoFila==true){				
				fc_BorrarFila(fila);
			}
			fc_HabilitarCamposSeleccion();
		}
		else{			
			for(var i=1;i<columna;i++){
				if(document.getElementById("rbt0"+i).checked==false){					
					document.getElementById(obj).checked=false;
					estadoColumna=false;					
				}			
			}
			if(columna!=5){
				indice=parseInt(columna);
				indice=indice+1;			
				if(document.getElementById("rbt0"+indice).checked==true){
					document.getElementById(obj).checked=true;
					estadoColumna=false;				
				}
			}
			if(estadoColumna==true){				
				if(fc_RangosVacios(columna)==true){					
					alert("El rango anterior se encuentra vacio");
					document.getElementById("rbt0"+columna).checked=false;
					return;					
				}					
				columnaAnterior=columna-1;				
				fc_BorrarColumna(columna);
				fc_RangoSiguiente(columnaAnterior);
			}			
			fc_HabilitarCamposSeleccion();
		}
	}
	function fc_HabilitarCamposSeleccion(){		
		var fila=fc_ObtenerFilasCheck();
		var columna=fc_ObtenerColumnasCheck();
		
		fc_Habilitar(fila,columna);
	}
	function fc_ObtenerFilasCheck(){
		var fila;
		for(var i=5;i>0;i--){
			if(document.getElementById("rbt"+i+"0").checked==true){
				fila=i;
				break;
			}
		}
		return fila;
	}
	function fc_ObtenerColumnasCheck(){
		var columna;
		for(var j=5;j>0;j--){
			if(document.getElementById("rbt0"+j).checked==true){
				columna=j;
				break;				
			}
		}
		return columna;
	}
	//*****
	function fc_marcar(){
		for(var i=1;i<=5;i++){
			document.getElementById("txtDia"+i+"1").className="cajatexto_o";						
			document.getElementById("txtDia"+i+"2").className="cajatexto_o";
			for(var j=1;j<=5;j++){				
				document.getElementById("txtOcurrencia"+i+j).className="cajatexto_o";								
			}
		}
	}
	//*****
	function fc_Habilitar(fila,columna){				
		fc_marcar();		
		fc_DesabilitarCampos();		
		fc_HabilitarDias(columna);
		for(var i=1;i<=fila;i++){
			for(var j=1;j<=columna;j++){
				document.getElementById("txtOcurrencia"+i+j).readOnly=false;
				document.getElementById("txtOcurrencia"+i+j).className="cajatexto";												
			}
		}
	}	
	function fc_HabilitarDias(columna){		
		for(var i=1;i<=columna;i++){
			//document.getElementById("txtDia"+i+"1").readOnly=false;						
			document.getElementById("txtDia"+i+"2").readOnly=false;						
			document.getElementById("txtDia"+i+"1").className="cajatexto";
			document.getElementById("txtDia"+i+"2").className="cajatexto";
			document.getElementById("txtDia"+columna+"2").focus();			
		}
		if(columna=='1'){
			if(document.getElementById("rbt01").checked==true){
				document.getElementById("txtDia11").value="1";
			}
			document.getElementById("txtDia"+columna+"2").focus();			
		}
	}
	function fc_DesabilitarCampos(){		
		for(var i=1;i<=5;i++){
			document.getElementById("txtDia"+i+"1").readOnly=true;						
			document.getElementById("txtDia"+i+"2").readOnly=true;			
			for(var j=1;j<=5;j++){
				document.getElementById("txtOcurrencia"+i+j).readOnly=true;				
			}
		}		
	}
	function fc_BorrarFila(fila){
		for(var i=1;i<=5;i++){
			document.getElementById("txtOcurrencia"+fila+i).value="";
		}		
	}
	function fc_BorrarColumna(columna){		
		document.getElementById("txtDia"+columna+"1").value="";						
		document.getElementById("txtDia"+columna+"2").value="";
		//document.getElementById("txtDia"+columna+"2").focus();
		for(var i=1;i<=5;i++){
			document.getElementById("txtOcurrencia"+i+columna).value="";			
		}		
	}
	function fc_RangosVacios(columna){
		band=false;
		if(document.getElementById("rbt0"+columna).checked==true){			
			if(columna!=1){
				columna=columna-1;
				//rango1=document.getElementById("txtDia"+columna+"1").value;
				rango2=document.getElementById("txtDia"+columna+"2").value;
				if(rango2=="" || fc_Trim(rango2)==""){
					document.getElementById("txtDia"+columna+"2").focus();					
					band=true;	
				}
			}
		}
		return band;
	}
	function fc_Grabar(){
		if(fc_ValidarCamposVacios()==true){
			alert("Debe llenar todos los campos");
			return;
		}		
		var fila=fc_ObtenerFilasCheck();
		var columna=fc_ObtenerColumnasCheck();
		var contador=0;
		var cadena="";
		for(var i=1;i<=5;i++){
			codFila=document.getElementById("hidCodFila"+i+"0").value;
			if(document.getElementById("rbt"+i+"0").checked==false){
				valorFila="0";
			}
			else{
				valorFila="1";
			}			
			for(var j=1;j<=5;j++){
				codColumna=document.getElementById("hidCodColumna0"+j).value;
				if(document.getElementById("rbt0"+j).checked==false){
					valorColumna="0";
				}
				else{
					valorColumna="1";
				}				
				valorSancion=document.getElementById("txtOcurrencia"+i+j).value;
				valorRango1=document.getElementById("txtDia"+j+"1").value;
				valorRango2=document.getElementById("txtDia"+j+"2").value;
				cadena=cadena+codFila;
				cadena=cadena+"$"+valorFila;
				cadena=cadena+"$"+codColumna;
				cadena=cadena+"$"+valorColumna;
				cadena=cadena+"$"+valorRango1;
				cadena=cadena+"$"+valorRango2;
				cadena=cadena+"$"+valorSancion;
				if(j!=5){
					cadena=cadena+"|";
				}
				contador++;
			}			
			if(i!=5){
				cadena=cadena+"|";
			}			
		}
		//alert(cadena+"\n"+contador);
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txhCadena").value=cadena;
			document.getElementById("txhNumeroRegistros").value=contador;
			document.getElementById("txtNumeroOcurrencias").value=parseInt(document.getElementById("txtNumeroOcurrencias").value,10);
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}  
	}
	function fc_ValidarCamposVacios(){				
		var band=false;
		var fila=fc_ObtenerFilasCheck();
		var columna=fc_ObtenerColumnasCheck();		
		for(var i=1;i<=fila;i++){
			for(var j=1;j<=columna;j++){				
				var rango1=document.getElementById("txtDia"+j+"1").value;
				var rango2=document.getElementById("txtDia"+j+"2").value;
				var ocurrencia=document.getElementById("txtOcurrencia"+i+j).value;
				if(rango1==""||fc_Trim(rango1)==""){
					document.getElementById("txtDia"+j+"1").value="";
					document.getElementById("txtDia"+j+"1").focus();
					band=true;
					return band;
				}
				if(rango2==""||fc_Trim(rango2)==""){
					document.getElementById("txtDia"+j+"2").value="";
					document.getElementById("txtDia"+j+"2").focus();
					band=true;
					return band;
				}				
				if(ocurrencia==""||fc_Trim(ocurrencia)==""){				
					document.getElementById("txtOcurrencia"+i+j).value="";
					document.getElementById("txtOcurrencia"+i+j).focus();
					band=true;
					return band;					
				}								
			}
		}
		var nroOcurrencias=document.getElementById("txtNumeroOcurrencias").value;
		if(nroOcurrencias==""||fc_Trim(nroOcurrencias)==""){
			band=true;			
		}
		return band;
	}
	function fc_Cancelar(){
		document.getElementById("txhOperacion").value="CANCELAR";
		document.getElementById("frmMain").submit();		
	}
	function fc_RangoSiguiente(columna){		
		if(columna!='0'){		
			rango=document.getElementById("txtDia"+columna+"2").value;
			if(rango!=""){			
				if(fc_ValidarMayorAnterior(rango,columna)==true){
					//fc_LimpiarRangos(columna);
					columna=parseInt(columna,10)+parseInt(1,10);					
					if(columna<6){				
						if(document.getElementById("rbt0"+columna).checked==true){					
							document.getElementById("txtDia"+columna+"1").value=parseInt(rango,10)+1;					
							//document.getElementById("txtDia"+columna+"1").readOnly="true";			
						}
					}
				}
			}
		}		
	}
	function fc_ValidarMayorAnterior(rango,columna){
		rangoAnterior=document.getElementById("txtDia"+columna+"1").value;
		if(parseInt(rango,10)<parseInt(rangoAnterior,10)){
			alert("Debe ingresar un rango mayor al anterior.");
			document.getElementById("txtDia"+columna+"2").focus();
			return false;
		}		
		return true;
	}
	function fc_LimpiarRangos(columna){		
		columna=parseInt(columna,10)+parseInt(1,10);	
		for(var i=columna;i<=5;i++){
			document.getElementById("txtDia"+i+"1").value="";
			document.getElementById("txtDia"+i+"2").value="";			
		}		
	}
	</script>	
</head>
<body topmargin="0" leftmargin="5" rightmargin="0">
<form:form action="${ctx}/biblioteca/bib_mto_conf_sanciones.html" commandName="control" id="frmMain">	
	
	<form:hidden path="codUsuario" id="txhCodUsuario"/>
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	
	<form:hidden path="cadena" id="txhCadena"/>
	<form:hidden path="numeroRegistros" id="txhNumeroRegistros"/>	
	
	
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Configuraci&oacute;n de Sanciones</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" width="92%" bgcolor="#eae9e9" style="border: 1px solid #048BBA;margin-left:3px;">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="1" bordercolor="white" width="100%">
					<tr>
						<td class="grilla" rowspan="2" width="8%" colspan="2"><b>Rango de D&iacute;as</b></td>
						<c:forEach varStatus="loop" var="lista" items="${control.listaRangoDias}"  >
							<td class="grilla" width="5%" align=center>
								<input type="hidden"
								id='hidCodColumna0<c:out value="${loop.index+1}"/>'
								value='<c:out value="${lista.codTipoTablaDetalle}"/>'/>
								<input type="hidden"
								id='hidValorColumna0<c:out value="${loop.index+1}"/>'
								value='<c:out value="${lista.dscValor5}"/>'/>
								<input type="checkbox" 
								id="rbt0<c:out value="${loop.index+1}"/>"
								<c:if test="${lista.dscValor5=='1'}">									
								<c:out value="checked"/>	
								</c:if>								
								onclick="fc_HabilitarCampos(this.id,'','<c:out value='${loop.index+1}'/>');">
							</td>
						</c:forEach>
					</tr>
					<tr class="texto">
						<c:forEach varStatus="loop" var="lista" items="${control.listaRangoDias}"  >						
							<td class="grilla" width="5%">								
								<input
								id='txtDia<c:out value="${loop.index+1}"/>1'
								value='<c:out value="${lista.dscValor3}"/>'								
								maxlength="2"
								readonly="readonly"
								onkeypress='fc_PermiteNumeros();'
								onblur="fc_ValidaNumeroFinal(this,'dia');" 
								Class="cajatexto" style="text-align:center; font-weight:bold" size="3" />
								-								
								<input
								id='txtDia<c:out value="${loop.index+1}"/>2' 
								value='<c:out value="${lista.dscValor4}"/>'
								maxlength="2"
								onkeypress="fc_PermiteNumeros();fc_LimpiarRangos('<c:out value='${loop.index+1}'/>');"
								onblur="fc_ValidaNumeroFinal(this,'dia');fc_RangoSiguiente('<c:out value='${loop.index+1}'/>');" 
								Class="cajatexto" style="text-align:center; font-weight:bold" size="3" />
							</td>
						</c:forEach>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaSanciones}"  >
					<tr height="35px" class="texto">
						<c:if test="${loop.index=='0'}">
						<td valign="center" align="center" rowspan="5" width="1%">#<br><br>O<br>c<br>u<br>r<br>r<br>e<br>n<br>c<br>i<br>a<br>s</td>	
						</c:if>						
						<td class="" width="4%" align="center">
							<input type="hidden"
							id='hidCodFila<c:out value="${loop.index+1}"/>0'
							value='<c:out value="${lista.codFila}"/>'/>
							<input type="hidden"
							id='hidValorFila<c:out value="${loop.index+1}"/>0'
							value='<c:out value="${lista.valorFila}"/>'/>
							<input type=checkbox 
							id='rbt<c:out value="${loop.index+1}"/>0' 
							<c:if test="${lista.valorFila=='1'}">
							<c:out value="checked"/>	
							</c:if> 
							onclick="fc_HabilitarCampos(this.id,'<c:out value="${loop.index+1}"/>','');">
							&nbsp;&nbsp;&nbsp;<c:out value="${lista.dscFila}"/>&nbsp;&nbsp;
						<td class="" width="5%" align="center">
							<input id='txtOcurrencia<c:out value="${loop.index+1}"/>1'
							value="<c:out value='${lista.valorSancion1}'/>"  
							maxlength="2" style="text-align:center;"
							onkeypress="fc_PermiteNumeros();"
							onblur="fc_ValidaNumeroFinal(this,'ocurrencias');" 
							class="cajatexto" size="2" />
						</td>
						<td class="" width="5%" align="center">
							<input id='txtOcurrencia<c:out value="${loop.index+1}"/>2'
							value="<c:out value='${lista.valorSancion2}'/>"
							maxlength="2" style="text-align:center;"
							onkeypress="fc_PermiteNumeros();"
							onblur="fc_ValidaNumeroFinal(this,'ocurrencias');" 
							class="cajatexto" size="2" />
						</td>
						<td class="" width="5%" align="center">
							<input id='txtOcurrencia<c:out value="${loop.index+1}"/>3'
							value="<c:out value='${lista.valorSancion3}'/>"
							maxlength="2" style="text-align:center;"
							onkeypress="fc_PermiteNumeros();"
							onblur="fc_ValidaNumeroFinal(this,'ocurrencias');" 
							class="cajatexto" size="2" />
						</td>
						<td class="" width="5%" align="center">
							<input id='txtOcurrencia<c:out value="${loop.index+1}"/>4'
							value="<c:out value='${lista.valorSancion4}'/>"
							maxlength="2" style="text-align:center;"
							onkeypress="fc_PermiteNumeros();"
							onblur="fc_ValidaNumeroFinal(this,'ocurrencias');" 
							class="cajatexto" size="2" />
						</td>
						<td class="" width="5%" align="center">
							<input id='txtOcurrencia<c:out value="${loop.index+1}"/>5'
							value="<c:out value='${lista.valorSancion5}'/>"
							maxlength="2" style="text-align:center;"
							onkeypress="fc_PermiteNumeros();"
							onblur="fc_ValidaNumeroFinal(this,'ocurrencias');" 
							class="cajatexto" size="2" />
						</td>
					</tr>
					</c:forEach>					
				</table>
			</td>				
		</tr>		
	</table>
	<table>
		<tr>
			<td class="cajatexto-login">Intervalo del Nro. de Ocurrencias:</td>
			<td class="cajatexto-login">&nbsp;
				<form:input path="numeroOcurrencias" id="txtNumeroOcurrencias"  
				maxlength="3" cssStyle="text-align:center"
				onkeypress="fc_PermiteNumeros();"
				onblur="fc_ValidaNumeroFinal(this,'Nro. de ocurrencias');fc_ValidaNumeroFinalMayorCero(this);" 
				cssClass="cajatexto_o" size="3"/>&nbsp;d�as
			</td>
		</tr>		
	</table>
	<table align="right" style="margin-right:50px">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Grabar();" style="cursor:pointer"></a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">					
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="javascript:fc_Cancelar();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a>
			</td>
		</tr>
	</table>
</form:form>
</body>
</html>