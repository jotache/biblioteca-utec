<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){		
	}
	
	function fc_Buscar(){
		codigo=document.getElementById("txtCodigo").value;
		document.getElementById("txtCodigo").value=fc_Trim(codigo);
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	function fc_Agregar(){		
		operacion="AGREGAR";
		codUsuario=document.getElementById("txhCodUsuario").value;
		Fc_Popup("${ctx}/biblioteca/bib_idiomas_agregar.html?txhOperacion="+operacion+"&txhCodUsuario="+codUsuario,450,160);		
	}
	function fc_Quitar(){
		if (document.getElementById("txhSeleccion").value != ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhSeleccion").value="";
				document.getElementById("txhOperacion").value = "QUITAR";
				document.getElementById("frmMain").submit();
			}			
		}
		else{
			alert(mstrSeleccion);
			return false;
		}	
	}
	function fc_Modificar(){		
		operacion="MODIFICAR";
		codigoSec=document.getElementById("txhCodigoSec").value;
		codigoIdioma=document.getElementById("txhCodigoIdioma").value;
		descripcion=document.getElementById("txhDescripcion").value;
		seleccion=document.getElementById("txhSeleccion").value;
		codUsuario=document.getElementById("txhCodUsuario").value;				
		if(seleccion==""){
			alert(mstrSeleccion);
			return;
		}		
		Fc_Popup("${ctx}/biblioteca/bib_idiomas_agregar.html?txhOperacion="+operacion+
			"&txhCodigoSec="+codigoSec+
			"&txhCodigoIdioma="+codigoIdioma+
			"&txhDescripcion="+descripcion+
			"&txhCodUsuario="+codUsuario
			,450,160);
	}
	function fc_SeleccionarRegistro(codigoSec,codigoIdioma,descripcion ){		
		document.getElementById("txhCodigoSec").value=codigoSec;		
		document.getElementById("txhCodigoIdioma").value=codigoIdioma;
		document.getElementById("txhDescripcion").value=descripcion;
		document.getElementById("txhSeleccion").value=codigoSec;
		/*alert("codigoSec="+codigoSec+
			"codigoIdioma="+codigoIdioma+
			"descripcion="+descripcion);*/
	}
	function fc_Limpiar(){
		document.getElementById("txtCodigo").value="";
		document.getElementById("txtDscIdioma").value="";
	}
</script>
</head>
<body topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_idioma.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	
	<form:hidden path="codigoSec" id="txhCodigoSec" />
	<form:hidden path="codigoIdioma" id="txhCodigoIdioma" />
	<form:hidden path="descripcion" id="txhDescripcion" />
	
	<form:hidden path="seleccion" id="txhSeleccion" />	

	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Consulta de Idioma</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" style="width:92%; margin-left:4px" class="tabla" height="50px" bordercolor="red">
		<tr>
			<td width="10%">C&oacute;digo:</td>
			<td width="20%">
				<form:input path="codigo" id="txtCodigo"
					maxlength="5"
					onkeypress="fc_PermiteNumeros();"
					onblur="fc_ValidaNumeroFinal(this,'codigo');" 
					cssClass="cajatexto" size="8" />				
			</td>
			<td width="10%">Idioma:</td>
			<td width="50%">
				<form:input path="dscIdioma" id="txtDscIdioma"
					maxlength="50"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaNumeroLetrasGuionOnblur(this.id,'Idioma');" 
					cssClass="cajatexto" size="50" />
			</td>
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
			</td>
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" id="Table1" width="98%" border="0" class="" style="margin-left:3px">
		<tr>
			<td>	
				<display:table name="sessionScope.listaIdioma" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator" pagesize="10" requestURI="" 
					style="border: 1px solid #048BBA;width:98%;">
					
						<display:column property="rbtSelBibMan" title="Sel." headerClass="grilla" class="tablagrilla" style="text-align: center;width:5%"/>
						<display:column property="dscValor2" title="C�digo" headerClass="grilla" class="tablagrilla" style="text-align: center;width:8%"/>
						<display:column property="descripcion" title="Idioma" headerClass="grilla" class="tablagrilla" style="text-align: left;width:87%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
						<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
						<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
						<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
						<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
						<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
						<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
						<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
						
				</display:table>
			</td>
			<td width="30px" valign="top" align="right">
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
				</a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
					<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
						id="imgModificarPerfil" onclick="javascript:fc_Modificar();">
				</a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliminarPerfil" onclick="javascript:fc_Quitar();">
				</a>
			</td>
		</tr>		
	</table>	
</form:form>	
</body>
</html>