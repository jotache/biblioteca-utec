<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script language=javascript>	
	function onLoad(){
	document.getElementById("txhCodSelec").value="";
	if ( document.getElementById("txhMsg").value=="OK")
		{   document.getElementById("txhMsg").value="";
		    fc_Buscar();
			alert(mstrElimino);
				
		}		
		else if ( document.getElementById("txhMsg").value== "ERROR" )
		{
			alert(mstrNoElimino);			
		}
	
	}
	function fc_Buscar()
	{
	  if(document.getElementById("codCategoria").value!="-1")
	  { 
	     document.getElementById("txhOperacion").value = "BUSCAR";
	     document.getElementById("frmMain").submit();}
	  else {
	    alert('Seleccione una categor�a');
	  }
	}
	
      function fc_Limpiar(){
	  document.getElementById("codCategoria").value="-1";
	  document.getElementById("descripcionDewey").value="";
	  document.getElementById("txhOperacion").value = "LIMPIAR";
	  document.getElementById("frmMain").submit();
	}
	
	function fc_Categorias(){
	//alert(document.getElementById("codCategoria").value); 
	 if(document.getElementById("codCategoria").value!="-1")
	  {document.getElementById("txhOperacion").value = "CATEGORIAS";
	   document.getElementById("frmMain").submit();
	  }
	 else{document.getElementById("txhOperacion").value = "LIMPIAR";
	      document.getElementById("frmMain").submit();} 
	}
	
	
</script>
</head>
	<body>
	<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_dewey.html">
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codSelec" id="txhCodSelec"/>
		<form:hidden path="descripSelec" id="txhDescripSelec"/>
		<form:hidden path="codValor" id="txhCodValor" />
		<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="msg" id="txhMsg"/>
	
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Consulta de Clasificaci&oacute;n Dewey</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" class="tabla" height="50px" bordercolor="red" style="width:92%; margin-left:4px">
		<tr>
			<td width="10%">Categor&iacute;a :</td>
			<td colspan="1" name="tablaNiveles1" id="tablaNiveles1">
			    <form:select path="codCategoria"
						id="codCategoria" cssClass="cajatexto_o"
						cssStyle="width:130px" onchange="javascript:fc_Categorias();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaCategoria!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.codListaCategoria}" />
						</c:if>
					</form:select>
			</td>							
		</tr>
		<tr>
			<td class="">Descripci&oacute;n :</td>
			<td><form:input path="descripcionDewey" id="descripcionDewey" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Descripcion');" 
					cssClass="cajatexto" size="40" /></td>
			<td align="right" colspan="2">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>
				&nbsp;
			</td>				
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	
	<iframe id="iFrameGrilla" name="iFrameGrilla" frameborder="0" height="350px" width="100%" 
	src="${ctx}/biblioteca/consulta_dewey_paginado.html?txhCodUsuario=<c:out value='${control.codEvaluador}'/>
	&txhCodCategoria=<c:out value='${control.codCategoria}'/>">
	</iframe>
	<!--table border="0" width="100%">
		<tr>
			<td align="center">					
				<input type=button class="boton" value="Regresar" onclick="history.back();">
			</td>
		</tr>
	</table-->
</form:form>	
</body>
</html>