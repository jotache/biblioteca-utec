<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<script language=javascript>
	//VALORES PARA EL NOMBRE DEL ARCHIVO A SUBIR
	var fecha=new Date();
	var miFecha=""+fecha.getFullYear()+fecha.getMonth()+fecha.getDate()+fecha.getHours()+fecha.getMinutes()+fecha.getSeconds();
	//PARAMETROS DE EXTENSIONES
	var strExtensionDOC = "<%=(String)request.getAttribute("strExtensionDOC")%>";
	var strExtensionDOCX = "<%=(String)request.getAttribute("strExtensionDOCX")%>";
	var strExtensionPDF = "<%=(String)request.getAttribute("strExtensionPDF")%>";
	var strExtensionXLS = "<%=(String)request.getAttribute("strExtensionXLS")%>";
	var strExtensionXLSX = "<%=(String)request.getAttribute("strExtensionXLSX")%>";
	var strExtensionGIF = "<%=(String)request.getAttribute("strExtensionGIF")%>";
	var strExtensionJPG = "<%=(String)request.getAttribute("strExtensionJPG")%>";
	//*******************************
	var codigoSeccion = "<%=(String)request.getAttribute("codSeccion")%>";
	//*******************************
	var codNOV = "<%=(String)request.getAttribute("codNOV")%>";
	var codENLINT = "<%=(String)request.getAttribute("codENLINT")%>";
	var codBIB = "<%=(String)request.getAttribute("codBIB")%>";
	var codFUEINF = "<%=(String)request.getAttribute("codFUEINF")%>";
	var codREG = "<%=(String)request.getAttribute("codREG")%>";
	var codHOR = "<%=(String)request.getAttribute("codHOR")%>";	
   	//*******************************
   	
    function onLoad(){
    	if(document.getElementById("txhOperacion").value=="AGREGAR"){
    		document.getElementById("cboSeccion").value=codigoSeccion;	
    	}
    	    	
    	objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK" ){			
			/*window.opener.document.getElementById("cboSeccion").value=document.getElementById("txhCodSeccion").value;
			window.opener.document.getElementById("cboGrupo").value=document.getElementById("txhCodGrupo").value;
			window.opener.document.getElementById("txhOpcion").value="1";*/			
			fc_Muestra(document.getElementById("txhCodSeccion").value);
			window.opener.document.getElementById("txhCodSeccion").value=document.getElementById("txhCodSeccion").value;
			window.opener.document.getElementById("txhCodGrupo").value=document.getElementById("txhCodGrupo").value;
			window.opener.document.getElementById("txhCodOpcion").value="1";
			window.opener.fc_Actualizar();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "IGUAL" ){
			alert('La denominaci�n ya existe para otro registro.');			
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
		fc_Cambia();				
    	//se carga cuando la operacion sea modificar
		if(document.getElementById("txhOperacion").value=="MODIFICAR"){			
			document.getElementById("cboSeccion").disabled=true;
			fc_Muestra(document.getElementById("txhCodSeccion").value);
			if(document.getElementById("txhFlag").value=='1'){				
				document.getElementById("chkNovedades").checked=true;
				document.getElementById("chkEnlacesInteres").checked=true;
				//document.getElementById("chkReglamento").checked=true;
				//document.getElementById("chkHorarioAtencion").checked=true;
			}	
		}				
	}
	function ismaxlength(obj,tamanio){		
		var mlength=parseInt(tamanio);
		if (obj.value.length>mlength){
			obj.value=obj.value.substring(0,mlength)
		}
	}
	function fc_Muestra(seccion){								
		switch(seccion){
			case codNOV:
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="";				
				tablaFecha.style.display="";
				break;
			case codENLINT:				
				tablaEnlacesInteres.style.display="";
				tablaNovedades.style.display="none";												
				trAreaInteres.style.display="";
				trAdjuntarArchivo.style.display="none";
				tablaFecha.style.display="";			
				break;
			case codBIB:
				tablaEnlacesInteres.style.display="";
				tablaNovedades.style.display="none";				
				trAreaInteres.style.display="none";
				trAdjuntarArchivo.style.display="none";
				tablaFecha.style.display="";				
				break;
			case codFUEINF:
				tablaEnlacesInteres.style.display="";
				tablaNovedades.style.display="none";				
				trAreaInteres.style.display="none";
				trAdjuntarArchivo.style.display="";
				tablaFecha.style.display="";				
				break;
			case codREG:
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="none";				
				tablaFecha.style.display="none";
				break;
			case codHOR:
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="none";				
				tablaFecha.style.display="none";
				break;
			default:				
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="";				
				tablaFecha.style.display="";				
				document.getElementById("cboSeccion").value=codNOV;
				break;	
		}		
	}	
	function fc_Cambia(){		
		a=document.getElementById("cboSeccion").value;												
		switch(a){
			case codNOV:
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="";				
				tablaFecha.style.display="";
				break;
			case codENLINT:				
				tablaEnlacesInteres.style.display="";
				tablaNovedades.style.display="none";												
				trAreaInteres.style.display="";
				trAdjuntarArchivo.style.display="none";
				tablaFecha.style.display="";			
				break;
			case codBIB:
				tablaEnlacesInteres.style.display="";
				tablaNovedades.style.display="none";				
				trAreaInteres.style.display="none";
				trAdjuntarArchivo.style.display="none";
				tablaFecha.style.display="";				
				break;				
			case codFUEINF:
				tablaEnlacesInteres.style.display="";
				tablaNovedades.style.display="none";				
				trAreaInteres.style.display="none";
				trAdjuntarArchivo.style.display="";
				tablaFecha.style.display="";				
				break;
			case codREG:
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="none";				
				tablaFecha.style.display="none";
				break;
			case codHOR:
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="none";				
				tablaFecha.style.display="none";
				break;
			default:				
				tablaEnlacesInteres.style.display="none";
				tablaNovedades.style.display="";				
				tablaFecha.style.display="";				
				document.getElementById("cboSeccion").value=codNOV;
				break;	
		}		
	}
	
	function fc_Grabar(){
		if(document.getElementById("txhOperacion").value=="AGREGAR"){
			document.getElementById("txhCodUnico").value="";
			document.getElementById("txhCodSeccion").value=document.getElementById("cboSeccion").value;
			seccion=document.getElementById("cboSeccion").value;
		}
		if(document.getElementById("txhOperacion").value=="MODIFICAR"){
			seccion=document.getElementById("txhCodSeccion").value;
		}
		if(seccion==""){
			alert('Debe seleccionar una Secci�n.');
			return;
		}
		if(seccion==codHOR || seccion==codREG)return false;
		//ASIGNA LOS VALORES POR SECCION PARA GRABARLOS EN LA BD
		
		if(fc_AsignarValores(seccion)){
				
			if(fc_ValidarArchivo(seccion)){
				
				if(confirm(mstrSeguroGrabar)){
					document.getElementById("codArchivo").value=miFecha;
					document.getElementById("frmMain").submit();
				}		
			}
		}
	}
	
	//*********************************************************************
	function fc_AsignarValores(seccion){
		
		switch(seccion){
			case codNOV://NOVEDADES
				document.getElementById("txhCodTipoMaterial").value="";
				if(document.getElementById("cboGrupo").value==""){
					alert('Debe seleccionar un Grupo.');
					document.getElementById("cboGrupo").focus();
					return false;
				}
				
				document.getElementById("txhCodGrupo").value=document.getElementById("cboGrupo").value;
				if(document.getElementById("txtTema").value=="" || fc_Trim(document.getElementById("txtTema").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtTema").focus();
					return false;
				}
				
				document.getElementById("txhDscTema").value=document.getElementById("txtTema").value;
				if(document.getElementById("txtDscCorta").value=="" || fc_Trim(document.getElementById("txtDscCorta").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtDscCorta").focus();
					return false;
				}
				
				document.getElementById("txhDscCorta").value=document.getElementById("txtDscCorta").value;
				if(document.getElementById("txtDscLarga").value=="" || fc_Trim(document.getElementById("txtDscLarga").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtDscLarga").focus();
					return false;
				}
				
				document.getElementById("txhDscLarga").value=document.getElementById("txtDscLarga").value;
				document.getElementById("txhDscUrl").value=document.getElementById("txtUrlNov").value;
				if(document.getElementById("txhOperacion").value=="AGREGAR"){
					if(document.getElementById("archivoImagen").value=="" || fc_Trim(document.getElementById("archivoImagen").value)==""){
						alert('Debe seleccionar la imagen relacionada.');
						document.getElementById("archivoImagen").focus();
						return false;
					}
				}
				document.getElementById("txhDscArchivo").value=document.getElementById("archivoImagen").value;
				document.getElementById("txhDscDocumNov").value=document.getElementById("archivoDocumentoNov").value;
				
				if(document.getElementById("chkNovedades").checked==true){
					document.getElementById("txhFlag").value="1";
				}
				else{
					document.getElementById("txhFlag").value="0";
				}
				break;
				
			case codENLINT://AREAS DE INTERES
				document.getElementById("txhCodGrupo").value="";
				if(document.getElementById("txtNombre").value=="" || fc_Trim(document.getElementById("txtNombre").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtNombre").focus();
					return false;
				}								
				document.getElementById("txhDscTema").value=document.getElementById("txtNombre").value;								
				document.getElementById("txhCodTipoMaterial").value=document.getElementById("cboTipoMaterial").value;
				document.getElementById("txhDscCorta").value="";
				document.getElementById("txhDscLarga").value="";
				if(document.getElementById("txtUrl").value=="" || fc_Trim(document.getElementById("txtUrl").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtUrl").focus();
					return false;
				}				
				document.getElementById("txhDscUrl").value=document.getElementById("txtUrl").value;
				/*if(document.getElementById("txhOperacion").value=="AGREGAR"){
					if(document.getElementById("archivoDocumento").value=="" || fc_Trim(document.getElementById("archivoDocumento").value)==""){
						alert(mstrNoArchivo);
						document.getElementById("archivoDocumento").focus();
						return false;
					}
				}*/					
				//document.getElementById("txhDscArchivo").value=document.getElementById("archivoDocumento").value;
				document.getElementById("txhDscArchivo").value="";	
				document.getElementById("txhDscDocumNov").value="";			
				if(document.getElementById("chkEnlacesInteres").checked==true){
					document.getElementById("txhFlag").value="1";
				}
				else{
					document.getElementById("txhFlag").value="0";
				}				
				break;
				
			case codBIB://BIBLIOTECA
				document.getElementById("txhCodTipoMaterial").value="";
				document.getElementById("txhCodGrupo").value="";
				if(document.getElementById("txtNombre").value=="" || fc_Trim(document.getElementById("txtNombre").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtNombre").focus();
					return false;
				}
				document.getElementById("txhDscTema").value=document.getElementById("txtNombre").value;
				document.getElementById("txhDscCorta").value="";
				document.getElementById("txhDscLarga").value="";
				if(document.getElementById("txtUrl").value=="" || fc_Trim(document.getElementById("txtUrl").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtUrl").focus();
					return false;
				}				
				document.getElementById("txhDscUrl").value=document.getElementById("txtUrl").value;				
				document.getElementById("txhDscArchivo").value="";			
				document.getElementById("txhDscDocumNov").value="";					
				if(document.getElementById("chkEnlacesInteres").checked==true){
					document.getElementById("txhFlag").value="1";
				}
				else{
					document.getElementById("txhFlag").value="0";
				}				
				break;
			case codFUEINF://OTRAS FUENTES DE INFORMACION
				document.getElementById("txhCodTipoMaterial").value="";
				document.getElementById("txhCodGrupo").value="";
				if(document.getElementById("txtNombre").value=="" || fc_Trim(document.getElementById("txtNombre").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtNombre").focus();
					return false;
				}
				document.getElementById("txhDscTema").value=document.getElementById("txtNombre").value;
				document.getElementById("txhDscCorta").value="";
				document.getElementById("txhDscLarga").value="";
				if(document.getElementById("txtUrl").value=="" || fc_Trim(document.getElementById("txtUrl").value)==""){
					alert('Debe completar todos los campos.');
					document.getElementById("txtUrl").focus();
					return false;
				}				
				document.getElementById("txhDscUrl").value=document.getElementById("txtUrl").value;
				/*if(document.getElementById("txhOperacion").value=="AGREGAR"){
					if(document.getElementById("archivoDocumento").value=="" || fc_Trim(document.getElementById("archivoDocumento").value)==""){
						alert(mstrNoArchivo);
						document.getElementById("archivoDocumento").focus();
						return false;
					}
				}*/
				document.getElementById("txhDscArchivo").value=document.getElementById("archivoDocumento").value;
				document.getElementById("txhDscDocumNov").value="";								
				if(document.getElementById("chkEnlacesInteres").checked==true){
					document.getElementById("txhFlag").value="1";
				}
				else{
					document.getElementById("txhFlag").value="0";
				}				
				break;
		}

		
		
		//ASIGNA LAS FECHAS ES EL MISMO JUEGO PARA TODAS LAS SECCIONES
		objFecIni = document.getElementById("txhFechaIni");
		objFecFin = document.getElementById("txhFechaFin");
		if(objFecIni.value=="" || fc_Trim(objFecIni.value)==""){
			alert('Debe ingresar una fecha de Publicaci�n.');
			document.getElementById("txhFechaIni").focus();
			return false;
		}
		
		document.getElementById("txhFechaPublicacion").value=objFecIni.value;
		if(objFecFin.value=="" || fc_Trim(objFecFin.value)==""){
			alert('Debe ingresar una fecha de Vigencia.');
			document.getElementById("txhFechaFin").focus();
			return false;
		}				
		document.getElementById("txhFechaVigencia").value=objFecFin.value;		
		
		if (Fc_RestaFechas(objFecIni.value,"dd/MM/yyyy",objFecFin.value,"dd/MM/yyyy") < 0 ){
			alert("Fecha de vigencia debe ser mayor a fecha publicaci�n.");
			objFecIni.focus();
			return false;
		}
		fechaActual=document.getElementById("txhFechaActual").value;
		
		if (Fc_RestaFechas(fechaActual,"dd/MM/yyyy",objFecFin.value,"dd/MM/yyyy") < 0 ){
			alert("Fecha de vigencia debe ser mayor � igual a la fecha actual.");
			objFecFin.focus();
			return false;
		}
		return true;				
	}	
	//*********************************************************************	
	function fc_ValidarArchivo(seccion){		
		var band=true;
		if(document.getElementById("txhDscArchivo").value!="" ){
			switch(seccion){
				case codNOV:
					band=fc_ValidaImagen();
					if(document.getElementById("txhDscDocumNov").value!="" &&
					   band){
						band=fc_ValidaDocumentoNov();
					}
					break;
				//case codENLINT:
				case codFUEINF:
					band=fc_ValidaDocumento();
					break;
			}
		}
		else if(document.getElementById("txhDscDocumNov").value!=""){
			band=fc_ValidaDocumentoNov();
		}
		
		return band;
	}
	function fc_ValidaImagen(){
		var nombreImagen;
		var strExtension;
		var lstrRutaArchivo;
		var band=true;
		lstrRutaArchivo = document.getElementById("archivoImagen").value;
		strExtension = "";
		strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
		//strExtension = lstrRutaArchivo.substring(lstrRutaArchivo.length - 3);
		nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();		
		if (strExtension != strExtensionGIF.toLowerCase()  && strExtension != strExtensionJPG.toLowerCase() ){
			alert('El archivo imagen debe tener las siguientes\nextensiones : JPG o GIF ');
			band=false;
		}
		else{
			document.getElementById("extArchivo").value = strExtension;			
		}
		return band;
	}
	function fc_ValidaDocumento(){
		var strExtension;
		var lstrRutaArchivo;
		var band=true;
		lstrRutaArchivo = document.getElementById("archivoDocumento").value;
		strExtension = "";
		//strExtension = lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);
		strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
		nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();		
		if (strExtension != strExtensionDOC.toLowerCase() && strExtension != strExtensionDOCX.toLowerCase()  && strExtension != strExtensionPDF.toLowerCase() && strExtension != strExtensionXLS.toLowerCase() && strExtension != strExtensionXLSX.toLowerCase()){
			alert('El archivo documento debe tener las siguientes\nextensiones : DOC, DOCX, XLS, XLSX o PDF.');
			band=false;
		}
		else{
			document.getElementById("extArchivo").value = strExtension;			
		}		
		return band;
	}
	
	function fc_ValidaDocumentoNov(){
		var strExtension;
		var lstrRutaArchivo;
		var band=true;
		lstrRutaArchivo = document.getElementById("archivoDocumentoNov").value;
		strExtension = "";
		strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
		//strExtension = lstrRutaArchivo.substring(lstrRutaArchivo.length - 3);
		nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();		
		if (strExtension != strExtensionDOC.toLowerCase() && strExtension != strExtensionDOCX.toLowerCase()  && strExtension != strExtensionPDF.toLowerCase() && strExtension != strExtensionXLS.toLowerCase() && strExtension != strExtensionXLSX.toLowerCase()){
			alert('El archivo documento debe tener las siguientes\nextensiones : DOC, DOCX, XLS, XLSX o PDF.');
			band=false;
		}
		else{
			document.getElementById("extArchivoNov").value = strExtension;			
		}		
		return band;
	}
</script>
</head>
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_secciones_agregar.html" enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhOperacion" />	
	<form:hidden path="msg" id="txhMsg"/>
	
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	
	<form:hidden path="codUnico" id="txhCodUnico"/>
	<form:hidden path="codSeccion" id="txhCodSeccion"/>
	<form:hidden path="codTipoMaterial" id="txhCodTipoMaterial"/>
	<form:hidden path="codGrupo" id="txhCodGrupo"/>	
	<form:hidden path="dscTema" id="txhDscTema"/>	
	<form:hidden path="dscCorta" id="txhDscCorta"/>
	<form:hidden path="dscLarga" id="txhDscLarga"/>
	<form:hidden path="dscUrl" id="txhDscUrl"/>
	<form:hidden path="dscArchivo" id="txhDscArchivo"/>	
	<form:hidden path="fechaPublicacion" id="txhFechaPublicacion"/>
	<form:hidden path="fechaVigencia" id="txhFechaVigencia" />
	<form:hidden path="fechaActual" id="txhFechaActual" />
	<form:hidden path="flag" id="txhFlag"/>	
	
	<form:hidden path="txtArchivo" id="txtArchivo"/>
	<form:hidden path="extArchivo" id="extArchivo"/>
	<form:hidden path="codArchivo" id="codArchivo"/>
	
	<form:hidden path="dscDocumNov" id="txhDscDocumNov"/>
	<form:hidden path="extArchivoNov" id="extArchivoNov"/>
	
    
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="450px" class="opc_combo"><font style="">
		 		<c:if test="${control.codUnico!=null}"> <!-- codigo seleccionado -->
				 		Mantenimiento de Secciones Modificar
				 	</c:if>
				 	<c:if test="${control.codUnico==null}"> <!-- codigo seleccionado -->
				 		Mantenimiento de Secciones Agregar
				 	</c:if>
		 	</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
	<table><tr height="1px"><td></td></tr></table>
	
	<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="0" cellPadding="0" border="0" bordercolor='gray'>
	<tr>
	<td>  
		<table cellSpacing="3" cellPadding="2" border="0" height="100%" bordercolor="black" width="100%" align="center">			 
			<tr>					
				<td width="25%">Tipo de Secci&oacute;n :</td>			
				<td align="left"><form:select path="tipoSeccion" id="cboSeccion" cssClass="cajatexto_o" cssStyle="width:200px" onchange="javascript:fc_Cambia();">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaSeccion!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.listaSeccion}" />
						</c:if>
					</form:select>
				</td>
			</tr>
		</table>
		
		<!--TABLA ENLACES DE INTERES  && BIBLIOTECAS && OTRAS FUENTES DE INFORMACION-->
		<table id="tablaEnlacesInteres" style="display:none;" cellSpacing="3" cellPadding="2" border="0" height="100%" bordercolor="black" width="100%" align="center">
			<tr>
				<td width="25%">Mostrar en Portal :
				</td>
				<td><input type="checkbox" id="chkEnlacesInteres" align="left">
				</td>
			</tr>	
			<tr >
				<td width="25%">Tema :</td>			
				<td><form:input path="nombre" id="txtNombre"
						maxlength="255"					 
						cssClass="cajatexto_o" size="60"/>				
				</td>
			</tr>
			<tr id="trAreaInteres" style="display: none;">
				<td>Tipo de Material :</td>			
				<td>
					<form:select path="tipoMaterial" id="cboTipoMaterial" cssClass="cajatexto_o" cssStyle="width:200px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaTipoMaterial!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.listaTipoMaterial}" />
						</c:if>
					</form:select>											
				</td>
			</tr>
			<tr>
				<td>URL :</td>			
				<td>
					<form:input path="url" id="txtUrl"
						maxlength="255"
						onkeypress="fc_ValidaUrl();"					 
						cssClass="cajatexto_o" size="60"/>											
				</td>
			</tr>
			<tr id="trAdjuntarArchivo">
				<td>Archivo :</td>			
				<td>
					<input type="file" class="cajatexto_o" size="40" name="archivoDocumento" maxlength="100">				
				</td>
			</tr>
			<c:if test="${control.dscArchivo!=null}">
				<tr class="texto_bold">
					<td></td>			
					<td>
						${control.dscArchivo}				
					</td>		
				</tr>			
			</c:if>
		</table>
		
		<!-- TABLA NOVEDADES -->
		<table id="tablaNovedades" style="display:none;" cellSpacing="3" cellPadding="2" border="0" height="100%" bordercolor="red" width="100%" align="center">
			<tr>
				<td width="25%">
					Mostrar en Portal :
				</td>
				<td>
					<input type="checkbox" id="chkNovedades" align="left">
				</td>
				
				<c:if test="${control.dscArchivo!=null}">
				<td rowspan="4" align="left">
					<img src="<c:out value="${control.pathImagen}" />" border="1" width="100px" height="110px">
				</td>
				</c:if>
			</tr>					
			<tr>
				<td width="25%">Grupo :</td>			
				<td>
					<form:select path="tipoGrupo" id="cboGrupo" cssClass="cajatexto_o" cssStyle="width:200px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaGrupo!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.listaGrupo}" />
						</c:if>
					</form:select>				
				</td>
			</tr>
			<tr>
				<td>Tema :</td>			
				<td>
					<form:input path="tema" id="txtTema"
						maxlength="255"					 
						cssClass="cajatexto_o" size="53" />				
				</td>
			</tr>
			<tr>
				<td>Descripci&oacute;n Corta :</td>			
				<td>
					<form:textarea path="dscCortaNov" id="txtDscCorta"										 
						cssClass="cajatexto_o" cols="55" rows="3" 
						onkeyup="return ismaxlength(this,'400')"/>										
				</td>
			</tr>
			<tr>
				<td>Descripci&oacute;n Larga :</td>		
				<td colspan="2">
					<form:textarea path="dscLargaNov" id="txtDscLarga"										 
						cssClass="cajatexto_o" cols="82" rows="3"
						onkeyup="return ismaxlength(this,'4000')"/>								
				</td>
			</tr>					
			<tr>
				<td>Imagen :</td>			
				<td colspan="2">
					<input type="file" class="cajatexto_o" size="50" name="archivoImagen" maxlength="100" id="archivoImagen">
					<c:if test="${control.dscArchivo!=null}">
						<font class="texto_bold">${control.dscArchivo}</font>		
					</c:if>	
				</td>
			</tr>
			<tr>
				<td>URL :</td>			
				<td colspan="2">
					<form:input path="urlNov" id="txtUrlNov" maxlength="255"
						onkeypress="fc_ValidaUrl();" cssClass="cajatexto" size="56"/>											
				</td>
			</tr>
			<tr id="trAdjuntarArchivoNov">
				<td>Archivo :</td>			
				<td colspan="2">
					<input type="file" class="cajatexto" size="40" name="archivoDocumentoNov" maxlength="100" id="archivoDocumentoNov">
					<c:if test="${control.dscDocumNov!=null}">
						<font class="texto_bold">${control.dscDocumNov}</font>			
					</c:if>
				</td>
			</tr>
		</table>
		
		<!--TABLA REGLAMENTO-->
		<!--<table id="tablaReglamento" style="display:none;" cellSpacing="3" cellPadding="2" border="0" bordercolor="red" width="100%" align="center">
			<tr>
				<td width="25%">
					Mostrar en Portal :
				</td>
				<td>
					<input type="checkbox" name="chkReglamento" align="left">
				</td>			
			</tr>
			<tr class="texto">			
				<td>Reglamento :</td>			
				<td>
					<form:textarea path="reglamento" cssClass="cajatexto_o" cols="80" rows="4" id="txtReglamento"/>
				</td>
			</tr>
		</table>-->
		
		<!--TABLA HORARIO DE ATENCION-->
		<!--<table id="tablaHorarioAtencion" style="display:none;" cellSpacing="3" cellPadding="2" border="0" bordercolor="red" width="100%" align="center">
			<tr>
				<td width="25%">
					Mostrar en Portal :
				</td>
				<td>
					<input type="checkbox" name="chkHorarioAtencion" align="left">
				</td>			
			</tr>
			<tr>			
				<td>Horario de Atenci&oacute;n :</td>			
				<td>
					<form:textarea path="horarioAtencion" cssClass="cajatexto_o" cols="80" rows="2" id="txtHorarioAtencion"/>
				</td>
			</tr>
		</table>-->
		
		<!--TABLA FECHAS EL MISMO PARA LAS DEMAS TABLAS-->
		<table id="tablaFecha" style="display:none;" cellSpacing="3" cellPadding="2" border="0" bordercolor="red" width="100%" align="center">
			<tr>
				<td width="25%">Fecha Publicaci&oacute;n :</td>
				<td>
					<form:input path="fechaInicio" id="txhFechaIni"
						maxlength="10"
						onkeypress="fc_ValidaFecha('txhFechaIni');"
						onblur="fc_ValidaFechaOnblur('txhFechaIni');" 
						cssClass="cajatexto_o" size="10" />
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" style='cursor:hand;' align="absmiddle"></a> - 
					<form:input path="fechaFin" id="txhFechaFin"
						maxlength="10"
						onkeypress="fc_ValidaFecha('txhFechaFin');"
						onblur="fc_ValidaFechaOnblur('txhFechaFin');" 
						cssClass="cajatexto_o" size="10" />
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin" alt="Calendario" style='cursor:hand;' align="absmiddle"></a> -
						&nbsp;&nbsp;(DD-MM-AAAA)				
				</td>
			</tr>
		</table>
	</td>
	</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" style="cursor:pointer;"></a></td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txhFechaIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	
	Calendar.setup({
		inputField     :    "txhFechaFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});	
</script>	
</body>
</html>