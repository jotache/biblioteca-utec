<%@ include file="/taglibs.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script type="text/javascript">
	function onLoad(){		
	}
	function fc_Regresar(){
		location.href =  "${ctx}/menuBiblioteca.html";
	}
	function fc_Aparece(param){			
		DatosVeh.style.display = "none";
		OT.style.display = "none";
		
		if (param == '1'){DatosVeh.style.display="";}	
		if (param == '2'){OT.style.display="";}	
	}		
	function fc_Aparecer(param){
		var td_mantenimiento = document.getElementById("td_mantenimiento"); // $('td_mantenimiento');
		var td_configuraciones = document.getElementById("td_configuraciones"); // $('td_configuraciones');
		var tr_mant = document.getElementById("tr_mant"); //$('tr_mant');
		var tr_conf = document.getElementById("tr_conf"); //$('tr_conf');		
		td_mantenimiento.className = "Tab_vert";		
		td_configuraciones.className = "Tab_vert";		
		tr_mant.style.display = 'none';		
		tr_conf.style.display = 'none';		
		switch(param){
		case '1':
				td_mantenimiento.style.display = '';
				tr_mant.style.display = '';
				td_mantenimiento.className = "Tab_vert_resaltado";	
				td_configuraciones.className = "Tab_vert";
				break;
			case '2':	
				td_configuraciones.style.display = '';
				tr_conf.style.display = '';
				td_configuraciones.className = "Tab_vert_resaltado";
				td_mantenimiento.className = "Tab_vert";
				break;	
		}		
	}
	function fc_muestra(param){
		var pag_tipo = document.getElementById("pag_tipo");// $('pag_tipo');
		var pag_desc = document.getElementById("pag_desc");// $('pag_desc');
		var pag_clas = document.getElementById("pag_clas");// $('pag_clas');
		var pag_auto = document.getElementById("pag_auto");// $('pag_auto');
		var pag_edit = document.getElementById("pag_edit");// $('pag_edit');
		var pag_pais = document.getElementById("pag_pais");// $('pag_pais');
		var pag_ciud = document.getElementById("pag_ciud");// $('pag_ciud');
		var pag_idio = document.getElementById("pag_idio");// $('pag_idio');
		var pag_serv = document.getElementById("pag_serv");// $('pag_serv');
		var pag_sala = document.getElementById("pag_sala");// $('pag_sala');
		var pag_sanc = document.getElementById("pag_sanc");// $('pag_sanc');
		var pag_proc = document.getElementById("pag_proc");// $('pag_proc');
		var pag_vide = document.getElementById("pag_vide");// $('pag_vide');
		var pag_secbib = document.getElementById("pag_secbib");//$('pag_secbib');
		var pag_feri = document.getElementById("pag_feri");//$('pag_feri');
		var pag_para = document.getElementById("pag_para");//$('pag_para');
		pag_tipo.className = "Tab_sub";
		pag_desc.className = "Tab_sub";
		pag_clas.className = "Tab_sub";
		pag_auto.className = "Tab_sub";
		pag_edit.className = "Tab_sub";
		pag_pais.className = "Tab_sub";
		pag_ciud.className = "Tab_sub";
		pag_idio.className = "Tab_sub";
		pag_serv.className = "Tab_sub";		
		pag_sala.className = "Tab_sub";
		pag_sanc.className = "Tab_sub";
		pag_proc.className = "Tab_sub";
		pag_vide.className = "Tab_sub";
		pag_secbib.className = "Tab_sub";
		pag_feri.className = "Tab_sub";		
		pag_para.className = "Tab_sub";	
		
		switch(param){
			case '1':
				pag_tipo.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_material.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;								
				break;
			case '2':	
				pag_desc.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_descriptores.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;								
				break;
			case '3':	
				pag_clas.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_dewey.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;								
				break;
			case '4':
				pag_auto.className = "Tab_sub_res";	
				destino = "${ctx}/biblioteca/consulta_autor.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '5':	
				pag_edit.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_editorial.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '6':	
				pag_pais.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_pais.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '7':	
				pag_ciud.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_ciudad.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '8':
				pag_idio.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_idioma.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '9':
				pag_secbib.className = "Tab_sub_res";					
				destino = "${ctx}/biblioteca/consulta_secciones.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '10'://salas
				pag_sala.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_salas.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value
							+"&txhSedeSelec="+document.getElementById("txhSedeSelec").value
							+"&txhSedeUsuario="+document.getElementById("txhSedeUsuario").value
				break;
			case '11':
				pag_vide.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_tipo_video.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '12':
				pag_proc.className = "Tab_sub_res";
				destino = "${ctx}/biblioteca/consulta_tipo_procedencia.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;		
			case '13':
				pag_sanc.className = "Tab_sub_res";	
				destino = "${ctx}/biblioteca/bib_mto_conf_sanciones.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;				
			case '14':
				pag_serv.className = "Tab_sub_res";	
				destino = "${ctx}/biblioteca/bib_mto_conf_servicios_usuarios.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '15':
				pag_feri.className = "Tab_sub_res";	
				destino = "${ctx}/biblioteca/bib_mto_feriados.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value
								+"&txhSedeSelec="+document.getElementById("txhSedeSelec").value;
				break;		
			case '16':
				pag_para.className = "Tab_sub_res";	
				destino = "${ctx}/biblioteca/bib_Parametros_Generales.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '17':
				pag_para.className = "Tab_sub_res";	
				destino = "${ctx}/biblioteca/bib_Accesos.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value;
				break;
			case '18':
				pag_para.className = "Tab_sub_res";	
				destino = "${ctx}/biblioteca/bib_horario_salas.html?txhCodUsuario="+document.getElementById("txhCodUsuario").value
						+"&txhSedeSelec="+document.getElementById("txhSedeSelec").value;
				break;
		}
		document.getElementById("iFrame").src = destino;
	}
</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/mantenimiento_configuracion_enlaces.html">		
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="sedeSel" id="txhSedeSelec"/>
	<form:hidden path="sedeUsuario" id="txhSedeUsuario"/>	

	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!--Page Title -->
	<table cellSpacing="0" cellPadding="2" border="0"
		style="height: 18px; width: 98%; margin-top: 3px;margin-left:10px" bordercolor="green">	
		<tr>
			<td valign="top" width="20%">					
				<table style="HEIGHT: 20px" cellspacing="0" cellpadding="0" border="0" bordercolor="white">					
					<tr>
						<td id="td_mantenimiento"  onclick="javascript:fc_Aparecer('1');" width="12%" class="Tab_vert">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmantenimiento','','${ctx}/images/biblioteca/Mant_Conf/mantenimientos2.jpg',1)">
							<img id="imgmantenimiento" src= "${ctx}/images/biblioteca/Mant_Conf/mantenimientos1.jpg" style="cursor:pointer"></a>
						</td>
					</tr>
					<tr style="DISPLAY:none"id="tr_mant" name="tr_mant" border="0">
						<td>
							<table cellspacing="0" cellpadding="0" border="0" bordercolor="green">
								<tr><td id="pag_tipo" width="10%" onclick="javascript:fc_muestra('1');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgtipomat','','${ctx}/images/biblioteca/Mant_Conf/tipomaterial1.jpg',1)">
									<img id="imgtipomat" src= "${ctx}/images/biblioteca/Mant_Conf/tipomaterial2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_desc" width="10%" onclick="javascript:fc_muestra('2');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgdescrip','','${ctx}/images/biblioteca/Mant_Conf/descriptor1.jpg',1)">
									<img id="imgdescrip" src= "${ctx}/images/biblioteca/Mant_Conf/descriptor2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_clas" width="10%" onclick="javascript:fc_muestra('3');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgdewey','','${ctx}/images/biblioteca/Mant_Conf/dewey1.jpg',1)">
									<img id="imgdewey" src= "${ctx}/images/biblioteca/Mant_Conf/dewey2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_auto" width="10%" onclick="javascript:fc_muestra('4');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgautor','','${ctx}/images/biblioteca/Mant_Conf/autor1.jpg',1)">
									<img id="imgautor" src= "${ctx}/images/biblioteca/Mant_Conf/autor2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_edit" width="10%" onclick="javascript:fc_muestra('5');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgeditorial','','${ctx}/images/biblioteca/Mant_Conf/editorial1.jpg',1)">
									<img id="imgeditorial" src= "${ctx}/images/biblioteca/Mant_Conf/editorial2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_pais" width="10%" onclick="javascript:fc_muestra('6');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgpais','','${ctx}/images/biblioteca/Mant_Conf/pais1.jpg',1)">
									<img id="imgpais" src= "${ctx}/images/biblioteca/Mant_Conf/pais2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_ciud" width="10%" onclick="javascript:fc_muestra('7');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgciudad','','${ctx}/images/biblioteca/Mant_Conf/ciudad1.jpg',1)">
									<img id="imgciudad" src= "${ctx}/images/biblioteca/Mant_Conf/ciudad2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_idio" width="10%" onclick="javascript:fc_muestra('8');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgidioma','','${ctx}/images/biblioteca/Mant_Conf/idioma1.jpg',1)">
									<img id="imgidioma" src= "${ctx}/images/biblioteca/Mant_Conf/idioma2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_secbib" width="10%" onclick="javascript:fc_muestra('9');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgsecc','','${ctx}/images/biblioteca/Mant_Conf/secbib1.jpg',1)">
									<img id="imgsecc" src= "${ctx}/images/biblioteca/Mant_Conf/secbib2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_sala" width="10%" onclick="javascript:fc_muestra('10');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgsalas','','${ctx}/images/biblioteca/Mant_Conf/salas1.jpg',1)">
									<img id="imgsalas" src= "${ctx}/images/biblioteca/Mant_Conf/salas2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_vide" width="10%" onclick="javascript:fc_muestra('11');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgtipovideo','','${ctx}/images/biblioteca/Mant_Conf/tipovideo1.jpg',1)">
									<img id="imgtipovideo" src= "${ctx}/images/biblioteca/Mant_Conf/tipovideo2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_proc" width="10%" onclick="javascript:fc_muestra('12');" class="Tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgtipoproc','','${ctx}/images/biblioteca/Mant_Conf/tipoprocedencia1.jpg',1)">
									<img id="imgtipoproc" src= "${ctx}/images/biblioteca/Mant_Conf/tipoprocedencia2.jpg" style="cursor:pointer"></a>	
								</td></tr>									
							</table>
						</td>
					</tr>
					<tr>
						<td id="td_configuraciones" width="12%" onclick="javascript:fc_Aparecer('2');" class="tab_vert_resaltado">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgconfiguraciones','','${ctx}/images/biblioteca/Mant_Conf/config2.jpg',1)">
							<img id="imgconfiguraciones" src= "${ctx}/images/biblioteca/Mant_Conf/config1.jpg" style="cursor:pointer"></a>
						</td></tr>
					<tr style="DISPLAY:none"id="tr_conf" name="tr_conf" border="0">
						<td>
							<table cellspacing="0" cellpadding="0" border="0" bordercolor="green">
								<tr><td id="pag_sanc" width="10%" onclick="javascript:fc_muestra('13');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgsancion','','${ctx}/images/biblioteca/Mant_Conf/sanciones1.jpg',1)">
									<img id="imgsancion" src= "${ctx}/images/biblioteca/Mant_Conf/sanciones2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_serv" width="10%" onclick="javascript:fc_muestra('14');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgserv','','${ctx}/images/biblioteca/Mant_Conf/servusuarios1.jpg',1)">
									<img id="imgserv" src= "${ctx}/images/biblioteca/Mant_Conf/servusuarios2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_feri" width="10%" onclick="javascript:fc_muestra('15');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgferiados','','${ctx}/images/biblioteca/Mant_Conf/feriados1.jpg',1)">
									<img id="imgferiados" src= "${ctx}/images/biblioteca/Mant_Conf/feriados2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_para" width="10%" onclick="javascript:fc_muestra('16');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgparareservas','','${ctx}/images/Evaluaciones/Menu/parametrosg1.jpg',1)">
									<img id="imgparareservas" src= "${ctx}/images/Evaluaciones/Menu/parametrosg2.jpg" style="cursor:pointer"></a>
								</td></tr>
								<tr><td id="pag_access" width="10%" onclick="javascript:fc_muestra('17');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgaccesos','','${ctx}/images/biblioteca/Mant_Conf/accesos1.jpg',1)">
									<img id="imgaccesos" src= "${ctx}/images/biblioteca/Mant_Conf/accesos2.jpg" style="cursor:pointer"></a>
								</td></tr>
								
								<tr><td id="pag_access" width="10%" onclick="javascript:fc_muestra('18');" class="tab_sub">
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imghorariosalas','','${ctx}/images/biblioteca/Mant_Conf/horariosalas1.jpg',1)">
									<img id="imghorariosalas" src= "${ctx}/images/biblioteca/Mant_Conf/horariosalas2.jpg" style="cursor:pointer"></a>
								</td></tr>
								
<!-- 								<tr> -->
<!-- 									<td id="pag_usus" width="10%" onclick="javascript:fc_muestra('18');" class="tab_sub"> -->
<%-- 										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgusus','','${ctx}/images/biblioteca/Mant_Conf/imgusus1.jpg',1)"> --%>
<%-- 											<img id="imgusus" src= "${ctx}/images/biblioteca/Mant_Conf/imgusus2.jpg" style="cursor:pointer" /> --%>
<!-- 										</a> -->
<!-- 									</td> -->
<!-- 								</tr>												 -->
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="80%" valign="top">
				<table style="width:99%;background-color:white;margin-left: 5px"
					cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<!-- ${ctx}/eva/mto_menu.html -->
							<iframe id="iFrame" name="iFrame" width="100%" height="500px" frameborder="0"
								src="about:blank">
							</iframe>
						</td>		
					</tr>
				</table>
			</td>	
		</tr>
	</table>	
</form:form>					

