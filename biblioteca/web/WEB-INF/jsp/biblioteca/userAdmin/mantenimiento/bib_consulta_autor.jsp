<%@ include file="/taglibs.jsp"%>

<head>
	
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<script language=javascript>
	function onLoad(){
	document.getElementById("txhCodSelec").value="";
	
	if ( document.getElementById("txhMsg").value=="OK")
		{   document.getElementById("txhMsg").value="";
			alert(mstrElimino);
				
		}		
		else if ( document.getElementById("txhMsg").value== "ERROR" )
		{
			alert(mstrNoElimino);			
		}
	
	}
	function fc_Buscar()
	{
	 document.getElementById("txhOperacion").value = "BUSCAR";
	 document.getElementById("frmMain").submit();
	}
	function Fc_Cambia()
	{
	if(cboTipoApl.value=='02'){location.href="Configuracion_Bandeja_TS.htm";}
	}
	function fc_Limpiar(){
	  document.getElementById("codigoAutor").value="";
	  document.getElementById("descripcionAutor").value="";
	  document.getElementById("cboTipo").value="";
	}
	function fc_Agregar()
	{srtCodPeriodo= document.getElementById("txhCodPeriodo").value;
	 srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	 srtcodValor=document.getElementById("txhCodValor").value;
	    document.getElementById("codigoAutor").value="";
	 	document.getElementById("descripcionAutor").value="";
	 	
		Fc_Popup("${ctx}/biblioteca/bib_autores_agregar.html?txhCodigo="+document.getElementById("txhCodSelec").value + "&txhCodValor=" + srtcodValor 
		+ "&txhCodPeriodo=" + srtCodPeriodo + "&txhtxhCodUsuario=" + srtCodEvaluador,450,200);
	 	
	}
	function fc_Eliminar()
	{   if(document.getElementById("txhCodSelec").value != "" ){
			if(confirm(mstrSeguroEliminar1)){  
			    document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
				document.getElementById("codigoAutor").value="";
	            document.getElementById("descripcionAutor").value="";
			}
			else document.getElementById("frmMain").submit();
		}
	    else alert(mstrSeleccione); 
	  
	}	
	function fc_Modificar(){
		if(document.getElementById("txhCodSelec").value!=""){
			srtcodigo=document.getElementById("txhCodSelec").value;
			srtdescripcion=document.getElementById("txhDescripSelec").value;
			srtcodValor=document.getElementById("txhCodValor").value;
			srtCodPeriodo= document.getElementById("txhCodPeriodo").value;
			srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
			stsCodTipo=document.getElementById("txhCodTipoAutor").value;
			
			document.getElementById("codigoAutor").value="";
		    document.getElementById("descripcionAutor").value="";
		    
			Fc_Popup("${ctx}/biblioteca/bib_autores_modificar.html?txhDescripcion=" + srtdescripcion + "&txhCodigo=" + srtcodigo + "&txhCodValor=" + srtcodValor 
				 + "&txhCodPeriodo=" + srtCodPeriodo + "&txhCodUsuario=" + srtCodEvaluador +"&txhCodTipo="+stsCodTipo,450,200);
		}
	 	else alert(mstrSeleccione);     
	}
	
	function fc_seleccionarRegistro(srtCodSecuencial, srtCodigoValor, srtIdTexto, srtTipoAutor){
		document.getElementById("txhCodSelec").value=srtCodSecuencial;	
		document.getElementById("txhDescripSelec").value=document.getElementById(srtIdTexto).value;
		document.getElementById("txhCodValor").value=srtCodigoValor;
		document.getElementById("txhCodTipoAutor").value=srtTipoAutor;
		//alert(srtTipoAutor);
	}
	function selTipo(valor){		
		document.getElementById("codSelTipo").value=valor;
	}
</script>	
	
</head>

<body topmargin="0" leftmargin="10px" rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_autor.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="codValor" id="txhCodValor" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>

<form:hidden path="codPersona" id="txhCodPersona"/>
<form:hidden path="codEmpresa" id="txhCodEmpresa"/>
<form:hidden path="codTipoAutor" id="txhCodTipoAutor"/>
<input type="hidden" name="codSelTipo" id="codSelTipo" value=""/>  


	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Consulta de Autor</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:92%; margin-left:3px" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red">
		<tr>
			<td width="10%">C&oacute;digo:</td>
			<td width="10%">
				<form:input path="codigoAutor" 
					id="codigoAutor" maxlength="5"
					onblur="fc_ValidaNumeroOnBlur('codigoAutor');" 
					onkeypress="fc_PermiteNumeros();" 
					cssClass="cajatexto" size="8" /></td>
			<td width="10%">Autor:</td>
			<td width="35%">
				<form:input path="descripcionAutor" 
					id="descripcionAutor" maxlength="50"
					onkeypress="fc_ValidaNombreAutorOnkeyPress();"
					onblur="fc_ValidaNombreAutorOnblur(this.id,'Autor');" 
					cssClass="cajatexto" size="40" /></td>
			<td width="10%">Tipo:
			</td>
			<td width="15%">
				<form:select path="codTipo" id="cboTipo" cssClass="cajatexto" cssStyle="width:80%" onchange="selTipo(this.value);">
					<form:option value="">--Todos--</form:option>
					<form:option value="${control.codPersona}">Persona</form:option>
					<form:option value="${control.codEmpresa}">Empresa</form:option>
				</form:select>
			</td>
			<td width="10%" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_Limpiar();" ></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;
			</td>
		</tr>	
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1" width="98%" border="0"  class="" style="margin-left:3px">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 280px;">
				 	<display:table name="sessionScope.listaAutores" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  pagesize="10" requestURI=""
						style="border: 1px solid #048BBA;width:98%;">
					    <display:column property="rbtSelDetalle" title="Sel."  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
						<display:column property="dscValor2" title="C&oacute;digo" headerClass="grilla" 
							class="tablagrilla" style="text-align:center;width:8%"/>						
						<display:column property="descripcion" title="Autor" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:77%"/>
						<display:column property="tipo" title="Tipo"  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:10%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
					  </display:table>
					</div>
				</td>
				<td width="30px" valign="top" align="right">
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
						<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
							id="imgModificarPerfil" onclick="javascript:fc_Modificar();">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
						<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar" style="cursor:pointer" 
							id="imgEliminarPerfil" onclick="javascript:fc_Eliminar();">
					</a>
				</td>
			</tr>
		</table>
	<!--table border="0" width="100%">
		<tr>
			<td align="center">					
				<input type=button class="boton" value="Regresar" onclick="history.back();">
			</td>
		</tr>
	</table>-->
</form:form>	
</body>

