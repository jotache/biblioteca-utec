<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	<!--
	function Fc_Buscar()
	{
	location.href="../TCS_SB/Biblioteca_Tecsup.htm";
	}

	function Fc_Perfiles(strParametro)
	{	
		switch (strParametro)
		{
		case "1":	
			location.href="Registrar_Material_Bibliográfico.htm";
			break;
		case "2":
			location.href="Consulta_Adm_Buzon.htm";
			break;
		case "3":
			location.href="Cuerpo_Reportes.htm";
			break;
		case "4":
			location.href="mantenimiento_configuracion.html";
			break;
		case "5":
			parent(1).location.href='reserva_salas.htm';
			break;
		}				
	}
	//-->
	</script>
</head>
<body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="red">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/opciones_administrador.html">
	<div style="overflow:scroll">
	<TABLE cellSpacing="2"  align=center cellPadding="2" border="0" style="HEIGHT: 18px; WIDTH: 100%; margin-top:50px" bordercolor="green" ID="Table1">
		<TR>
			<td width="30%" align="right"><img src="${ctx}\images\iconos/ico_link.gif" style="cursor:hand"></td>
			<td><a href="javascript:Fc_Perfiles('1');" class="Enlace" style="cursor:hand">Gestión Material Bibliográfico</a></td>
		</TR>
		<TR>
			<td width="30%" align="right"><img src="${ctx}\images\iconos/ico_link.gif" style="cursor:hand"></td>
			<td><a href="javascript:Fc_Perfiles('2');" class="Enlace" style="cursor:hand">Administración del Buzón de Sugerencias</a></td>
		</TR>			
		<TR>
			<td width="30%" align="right"><img src="${ctx}\images\iconos/ico_link.gif" style="cursor:hand"></td>
			<td><a href="javascript:Fc_Perfiles('5');" class="Enlace" style="cursor:hand">Gestión de Reserva de Salas</a></td>
		</TR>			
		<TR>
			<td width="30%" align="right"><img src="${ctx}\images\iconos/ico_link.gif" style="cursor:hand"></td>
			<td><a href="javascript:Fc_Perfiles('3');" class="Enlace" style="cursor:hand">Consultas y Reportes</a></td>
		</TR>
		<TR>
			<td width="30%" align="right"><img src="${ctx}\images\iconos/ico_link.gif" style="cursor:hand"></td>
			<td><a href="javascript:Fc_Perfiles('4');" class="Enlace" style="cursor:hand">Mantenimiento y Configuraciones</a></td>
		</TR>
	</TABLE>
	</div>
</form:form>	
</body>
</html>