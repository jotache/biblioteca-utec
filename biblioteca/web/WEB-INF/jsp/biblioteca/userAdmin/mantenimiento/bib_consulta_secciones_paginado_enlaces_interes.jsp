<%@ include file="/taglibs.jsp"%>
<head>

<script language="javascript">
	function onLoad(){		
		if(parent.document.getElementById("cboSeccion").value=='0001'){
			document.getElementById("btnGrupo").style.display="";				
		}	
	}
	function fc_Actualizar(){
		parent.document.getElementById("cboSeccion").value=document.getElementById("txhCodSeccion").value;
		parent.document.getElementById("cboGrupo").value=document.getElementById("txhCodGrupo").value;
		parent.document.getElementById("txhOpcion").value=document.getElementById("txhCodOpcion").value;
		parent.document.getElementById("txhOperacion").value="BUSCAR";
		parent.document.getElementById("frmMain").submit();
	}
	function fc_ActualizarGrupo(){		
		parent.document.getElementById("cboSeccion").value='0001';
		parent.document.getElementById("frmMain").submit();
	}
	function fc_Agregar(){
		operacion="AGREGAR";
		//alert(operacion);		
		codSeccion=parent.document.getElementById("cboSeccion").value;
		//alert(codSeccion);		
		Fc_Popup("${ctx}/biblioteca/bib_secciones_agregar.html?txhOperacion="+operacion+"&txhCodSeccion="+codSeccion,500,420);	
		
	}	
	function fc_Modificar(){
		operacion="MODIFICAR";		
		seleccion=document.getElementById("txhCodUnico").value;
		if(seleccion==""){
			alert(mstrSeleccione);
			return;
		}		
		Fc_Popup("${ctx}/biblioteca/bib_secciones_agregar.html?txhCodUnico="+seleccion+"&txhOperacion="+operacion,500,420);		
	}
	function fc_Quitar(){
		if (document.getElementById("txhCodUnico").value==""){
			alert(mstrSeleccione);
			return;						
		}
		if(confirm(mstrEliminar)){
			parent.document.getElementById("txhCodUnico").value=document.getElementById("txhCodUnico").value;
			parent.document.getElementById("txhOperacion").value="QUITAR";
			parent.document.getElementById("frmMain").submit();
		}		
	}
	function fc_Grupo(){				
		Fc_Popup("${ctx}/biblioteca/bib_grupo_secciones.html?codusuario="+document.getElementById("txhCodUsuario").value,400,280);		
	}
	function fc_SeleccionarRegistro(codUnico){		
		document.getElementById("txhCodUnico").value=codUnico;		
	}
</script>
</head>
<body>

<input type="hidden" id="txhCodUnico">
<input type="hidden" id="txhCodSeccion">
<input type="hidden" id="txhCodGrupo">
<input type="hidden" id="txhCodOpcion">
<input type="hidden" id="txhCodUsuario" value="<%=request.getAttribute("codusuario_consultasecciones")%>">

<table cellpadding="0" cellspacing="0" id="Table1" width="98%" border="0" bordercolor="blue" class="" style="margin-left:3px">
	<tr>
		<td valign="top">
			<display:table name="sessionScope.listaBandejaSeccion" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator" pagesize="10" requestURI="" 
				style="border: 1px solid #048BBA;width:98%;">
				
				<display:column property="rbtSelBibManSeccion" title="Sel." headerClass="grilla" class="tablagrilla" style="text-align: center;width:5%"/>				
				<display:column property="tema" title="Titulo" headerClass="grilla" class="tablagrilla" style="text-align: left;width:42%"/>
				<display:column property="fechaPublicacion" title="Fec. Publicaci&oacute;n" headerClass="grilla" class="tablagrilla" style="text-align: center;width:15%"/>
				<display:column property="fechaVigencia" title="Fec. Vigencia" headerClass="grilla" class="tablagrilla" style="text-align: center;width:15%"/>
					
				<display:setProperty name="basic.empty.showtable" value="true"  />
				<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='5' align='center'>No se encontraron registros</td></tr>"  />
				<display:setProperty name="paging.banner.placement" value="bottom"/>
				<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
				<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
				<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
				<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado. </span>" />
				<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
				<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
				<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
				<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
					
			</display:table>										
		</td>
		<td width="30px" valign="top" align="right">
			<br/>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
			</a>
			<br/>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
				<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
					id="imgModificarPerfil" onclick="javascript:fc_Modificar();">
			</a>
			<br/>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
				<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
					id="imgEliminarPerfil" onclick="javascript:fc_Quitar();">
			</a>
			<br/>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnGrupo','','${ctx}/images/biblioteca/Mant_Conf/g2.jpg',1)">
				<img  src="${ctx}/images/biblioteca/Mant_Conf/g1.jpg" alt="Grupos" style="cursor:pointer; display:none;" 
					id="btnGrupo" name="btnGrupo" onclick="javascript:fc_Grupo();"></a>
			<!-- input id="btnGrupo" style="display:none;" type="button" value="G" alt="Grupos" onclick='javascript:fc_Grupo();'-->
		</td>
	</tr>		
</table>
</body>