<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">		
	<script type="text/javascript">
	function onLoad(){
	document.getElementById("txhCodSelec").value="";
	if ( document.getElementById("txhMsg").value=="OK")
		{   document.getElementById("txhMsg").value="";
			alert(mstrElimino);
				
		}		
		else if ( document.getElementById("txhMsg").value== "ERROR" )
		{
			alert(mstrNoElimino);			
		}
	
	}
	
	function fc_Agregar(){	
	srtCodPeriodo= document.getElementById("txhCodPeriodo").value;
	srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	Fc_Popup("${ctx}/biblioteca/bib_tipo_material_agregar.html?txhCodigo="+document.getElementById("txhCodSelec").value
	+ "&txhCodPeriodo=" + srtCodPeriodo + "&txhCodUsuario=" + srtCodEvaluador,450,150);
	}
		
	function Fc_Cambiar(param){
		switch(param){
			case '0':
				Fc_Popup("${ctx}/biblioteca/bib_tipo_material_modificar.html",400,150);
				break;
		}
	}
	
	function fc_Eliminar(param){
		//se muestra un mensaje de advertencia al administrador
		if(document.getElementById("txhCodSelec").value != "" ){
						if(confirm(mstrSeguroEliminar1)){  
						    document.getElementById("txhOperacion").value = "ELIMINAR_MATERIAL";
							document.getElementById("frmMain").submit();
							
						}
						else {document.getElementById("frmMain").submit();}
		}
	    else alert(mstrSeleccione);
	}
	function fc_Modificar(){
	  if(document.getElementById("txhCodSelec").value!=""){
		srtCodPeriodo= document.getElementById("txhCodPeriodo").value;
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
		srtcodigo=document.getElementById("txhCodSelec").value;
		srtdescripcion=document.getElementById("txhDescripSelec").value;
		 Fc_Popup("${ctx}/biblioteca/bib_tipo_material_modificar.html?txhDescripcion=" + srtdescripcion + "&txhCodigo=" + srtcodigo
		 + "&txhCodPeriodo=" + srtCodPeriodo + "&txhCodUsuario=" + srtCodEvaluador,450,150);
	     }
	    else alert(mstrSeleccione);
	}
	/*
	function fc_seleccion(xCodigo, xDescripcion){
	   document.getElementById("txhCodSelec").value=xCodigo;
	   document.getElementById("txhDescripSelec").value=xDescripcion;
	}
	*/
	function fc_seleccionarRegistro(srtCodSecuencial, srtCodigoValor, srtIdTexto){
	//alert(srtCodSecuencial+">><<"+srtCodigoValor+">><<"+srtIdTexto);
	//alert(document.getElementById(srtIdTexto).value);
	//alert(srtCodSecuencial+">><<"+srtCodigoValor+">><<"+document.getElementById("srtIdTexto").value);
	document.getElementById("txhCodSelec").value=srtCodSecuencial;
	document.getElementById("txhDescripSelec").value=document.getElementById(srtIdTexto).value;
	}
	
	function fc_seleccion(a){
		alert(document.getElementById("txhCodigo_"+a).value);
		document.getElementById("txhCodSelec").value=document.getElementById("txhCodigo_"+a).value
		alert(document.getElementById("txhDesc_"+a).value);
		document.getElementById("txhDescripSelec").value=document.getElementById("txhDesc_"+a).value;
	}
</script>
</head>
<body topmargin="0" leftmargin="10 rightmargin="0">
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/consulta_material.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codSelec" id="txhCodSelec" />
<form:hidden path="descripSelec" id="txhDescripSelec" />
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="msg" id="txhMsg"/>
	
	<!--T�tulo de la P�gina  -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="503px" class="opc_combo"><font style="">Consulta de Tipos de Material</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" id="Table1" width="98%" border="0"  class="" style="margin-left:3px">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height:280px;">
				 	<display:table name="sessionScope.listaMateriales" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  pagesize="10" requestURI=""
						style="border: 1px solid #048BBA;width:98%;">
					    <display:column property="rbtSelDetalleMaterial" title="Sel."  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
						<display:column property="descripcion" title="Tipo Material" headerClass="grilla" 
							class="tablagrilla" style="text-align:left;width:95%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto_libre'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='texto_libre'>{0}</span>" />
							
					  </display:table>
					</div>
				</td>
				<td width="30px" valign="top" align="right">
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_Agregar();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
						<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
							id="imgModificarPerfil" onclick="javascript:fc_Modificar();">
					</a>
					<br/>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
						<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Quitar" style="cursor:pointer" 
							id="imgEliminarPerfil" onclick="javascript:fc_Eliminar();">
					</a>
				</td>
			</tr>
		</table>
		
	<br>
</form:form>	
</body>
</html>