<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;" type="text/JavaScript"></script>
	<script language=javascript>
    function onLoad(){	
		if(document.getElementById("txhCodTipo").value=="0001"){
			document.getElementById("radioPersona").checked=true;
		}
		if(document.getElementById("txhCodTipo").value=="0002"){
			document.getElementById("radioEmpresa").checked=true;
		}
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrActualizo);
			window.close();
		}
		else{ if ( objMsg.value == "ERROR" )
		       {
			         alert(mstrProblemaGrabar);			
		       }
		      else{ if ( objMsg.value == "DUPLICADO" )
		            alert(mstrExistenRegistros);}
		}
	}

	function fc_Modificar(){
		objDescripcion = document.getElementById("descripcion");
		
		if (fc_Trim(objDescripcion.value) == "" || objDescripcion.value == ""){
			alert('Debe ingresar una descripción.');
			return;
		}
		if (document.getElementById("radioPersona").checked==true){
			document.getElementById("txhCodTipo").value=document.getElementById("txhCodPersona").value;
		}
		else{
			document.getElementById("txhCodTipo").value=document.getElementById("txhCodEmpresa").value;
		}
		document.getElementById("txhOperacion").value = "MODIFICAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_cancelar(){
	//window.opener.document.getElementById("frmMain").submit();
	}

</script>	
<body topmargin="5" leftmargin="5" rightmargin="5">
<form:form action="${ctx}/biblioteca/bib_autores_modificar.html" commandName="control" id="frmMain">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="codDetalle" id="txhCodDetalle"/>
<form:hidden path="descripDetalle" id="txhDescripDetalle"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>

<form:hidden path="codPersona" id="txhCodPersona"/>
<form:hidden path="codEmpresa" id="txhCodEmpresa"/>
<form:hidden path="codTipo" id="txhCodTipo"/>
			
		<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:5px;margin-top:5px; margin-right:5px;">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo"><font style="">Mantenimiento de Autores Modificar</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			 </tr>
		</table>
		<table><tr height="1px"><td></td></tr></table>
		<table class="tablaflotante" style="width:97%;margin-top:5px; margin-left:6px;" cellSpacing="4" cellPadding="2" border="0" bordercolor='red'>  
			<tr>
				<td nowrap class="" width="20%" align="left">&nbsp;C&oacute;digo :</td>
				<td>&nbsp;
					<form:input path="codigo" id="codigo"  maxlength="3"
						onblur="fc_ValidaNumeroOnBlur('codigo');"  
						onkeypress="fc_PermiteNumeros();" 
						cssClass="cajatexto_1" size="8" readonly="true" />
				
				</td>
            </tr>
			<tr>
				<td nowrap="nowrap" align="left">&nbsp;Autor :&nbsp;</td>
				<td>&nbsp;	
					<form:input path="descripcion" id="descripcion" 
						onkeypress="fc_ValidaNombreAutorOnkeyPress();" maxlength="150"
						onblur="fc_ValidaNombreAutorOnblur(this.id,'Autor');" 
					 	cssClass="cajatexto" size="70" />
				</td>
			</tr>
			<tr>
				<td>&nbsp;Tipo :
				</td>
				<td>
					<input type="radio" value="${control.codPersona}" name="radio" id="radioPersona">Persona
					<input type="radio" value="${control.codEmpresa}" name="radio" id="radioEmpresa">Empresa
				</td>
			</tr>
		</table>
		<table><tr height="5px"><td></td></tr></table>
		<table align=center>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" onclick="javascript:fc_Modificar();" style="cursor:pointer;"></a>
				</td>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">					
					<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();fc_cancelar();" ID="Button	1" NAME="Button1" style="cursor:pointer;"></a>
				</td>
			</tr>
		</table>
		<br>
	</form:form>
	</body>
</html>