<%@ include file="/taglibs.jsp"%>
<%	if ( request.getSession().getAttribute("usuarioSeguridad") == null ) { %>
	<script language="javascript">	
		alert("Su session ha expirado debe volver a logearse");		
		window.close();			
	</script>
<%}%>
<html>
<head>	
	<script src="${ctx}/scripts/js/funciones_bib-fechas.js" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/jquery/jquery-1.9.1.js" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/jquery/jquery-ui-1.10.1.custom.js" type="text/JavaScript"></script>
	
	<title>Atención</title>
	<style>
      .cell {
        background-repeat: no-repeat;
        background-position: center center;        
      }
    </style>

	<script type="text/javascript">		
		function onLoad(){
			//mostrarReloj();			
		}
		
		function fc_TipoSalas(){
			document.frmAtencion.txhAccion.value='listarTipoSala';	
			document.frmAtencion.submit();
		}
		
		function setZeroString(s_valor){
			return (s_valor<10)?'0'+s_valor:s_valor;
		}
		function mostrarReloj(){			
			local=new Date();
			tiempo.setSeconds(tiempo.getSeconds()+1);			
			document.getElementById('txtHoraIni').value=setZeroString(tiempo.getHours())+":"+setZeroString(tiempo.getMinutes())+":"+setZeroString(tiempo.getSeconds());			
			setTimeout('mostrarReloj()',990);
		}
		
		function AbrirHistorialRecurso(Url,NombreVentana,width,height,extras) {
			var largo = width;
			var altura = height;
			var adicionales= extras;
			var top = (screen.height-altura)/2;
			var izquierda = (screen.width-largo)/2; 
			nuevaVentana=window.open(''+ Url + '',''+ NombreVentana + '','width=' + largo + ',height=' + altura + ',top=' + top + ',left=' + izquierda + ',features=' + adicionales + ''); 
			nuevaVentana.focus();
		}
		function historial(codRecurso,nombrePC){			
			var vsede = document.getElementById('txhSede').value;
			var fecha = document.getElementById('txtFecha').value;			
			var sRuta = '${ctx}/biblioteca/bib_atencion_recurso.html?sede='+vsede+'&codrecurso='+codRecurso+'&fecha='+fecha+'&pc='+nombrePC;
			
			AbrirHistorialRecurso(sRuta,'Atenciones','510','280','');
		}	
		function detalleBloque(idTd){					
			/*if(document.getElementById(idTd).style.backgroundImage!=''){
				alert('MostrarResumen Bloque');
			}*/
		}
		function reserva(usuario,hrini,hrfin){
			var msg='Usuario: ' + usuario + '\n' +
					'Hr.Inicio: ' + hrini + ':00' +
					' Hr. Fin: ' + hrfin + ':00' + '\n';
			alert(msg);
		}
		function consultar(){
			document.frmAtencion.txhAccion.value = 'consultar';	 	
		 	document.frmAtencion.submit();
		}
		function verificaUsuario(){
			if(document.frmAtencion.txtUsuario.value!=''){							
				document.frmAtencion.txhAccion.value='buscarUsuario';	
				document.frmAtencion.submit();
			}else{
				document.frmAtencion.txtUsuarioNombre.value='';	
			}
			document.frmAtencion.txtHoraIni.focus();
		}

		function grabar(){
			var usuario = document.getElementById('txtUsuario');
			var nombre = document.getElementById('txtUsuarioNombre');
			if (trim(usuario.value)=='' || trim(nombre.value)==''){
				alert('Ingrese el usuario.');
				usuario.focus();
				return false;
			}

			var recurso = document.getElementById('codPc');
			if(recurso.value==''){
				alert('Seleccione un Recurso');
				recurso.focus();
				return false;
			}
			
			var horaIni = document.getElementById('txtHoraIni');
			var horaIni2;			
			
			horaIni2=horaIni.value.substr(0,5)
						
			//validar que hora de fin no sea menor que la de inicio
			var horaFin = document.getElementById("txtHoraFin");			
			if (trim(horaFin.value)==''){				
				alert('Ingrese la hora de fin');
				horaFin.focus();
				return false;
			}
			
			var nHoraIni=0;
			var nHoraFin=0;
			nHoraIni = parseFloat(horaIni2.replace(':',''));
			nHoraFin = parseFloat(horaFin.value.replace(':',''));
			
			if(nHoraIni>=nHoraFin){
				alert('Hora de Finalización Incorrecta');
				horaFin.focus();
				return false;
			}

			var mensaje = 'Confirma que desea grabar:\n';
			mensaje += '  Usuario: ' +  nombre.value + '\n';
			mensaje += '  Recurso: ' +  recurso.options[recurso.selectedIndex].text + '\n';
			mensaje += '  Hr. Inicio: ' +  horaIni2 + '\n';
			mensaje += '  Hr. Termino: ' +  horaFin.value;		 
			
			if(confirm(mensaje)){
				document.frmAtencion.txhAccion.value = 'grabar';	 	
			 	document.frmAtencion.submit();
			}
		}
	</script>
	
</head>

<body onLoad="onLoad();">
<form:form id="frmAtencion" name="frmAtencion" commandName="control" action="${ctx}/biblioteca/bib_atencion.html">
<form:hidden path="accion" id="txhAccion" />
<form:hidden path="codUsuario" id="txhCodUsuario" />
<form:hidden path="sede" id="txhSede" />
<form:hidden path="rangos" id="txhRangos" />
<form:hidden path="tipoMensaje" id="txhTipoMensaje" />
<form:hidden path="mensaje" id="txhMensaje" />
<form:hidden path="minutoActual" id="minutoActual" />
<form:hidden path="horaActual" id="horaActual" />
<form:hidden path="segundoActual" id="segundoActual" />
<%-- 
<%@ include file="../../comun/reloj.jsp"%> --%>

<table width="100%" border="0" align="center">
  <tr>
    <td>
		<table width="100%" border="0" align="left" style="BORDER-RIGHT: #048BBA 2px solid;BORDER-TOP: #048BBA 2px solid;BORDER-BOTTOM: #048BBA 2px solid;BORDER-LEFT: #048BBA 2px solid;">
			<tr>
            	<td width="5%" class="Detalle">&nbsp;Tipo Sala:</td>
                <td width="10%">
                	<form:select path="codTipoSala" id="codTipoSala" cssClass="cajatexto"
							cssStyle="width:120px;"  onchange="javascript:fc_TipoSalas();">
                         	
                            <form:option value="0">--Todas--</form:option>
                            <form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
 								items="${control.codListaTipoSala}"  />   
                                
                    </form:select>
                            
                </td>
				<td width="5%" class="Detalle" >Fecha:&nbsp;</td>
				<td width="10%"> 					
					<form:input path="fechaAtencion" id="txtFecha" maxlength="10" cssStyle="width:60px" disabled="true" cssClass="cajatexto_o"/>
					 <a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnConsultar','','${ctx}/images/cambia.gif',1)">					
						<img src="${ctx}/images/cambia.gif" onClick="javascript:consultar();" style="cursor:hand;" align="middle" id="btnConsultar" width="16" height="16" alt="Refrescar">
					 </a>
				</td>				
				<td width="70%">
					<table width="730" border="0">
                         <tr>
	                         <td class="Detalle" width="40px">Usuario:</td>
	                         <td width="300px">
	                           	<form:input path="usuario" id="txtUsuario" maxlength="20" cssStyle="width:80px" cssClass="cajatexto" onblur="verificaUsuario();"/>
							
								<!-- input type="text" id="nomUsuario" name="nomUsuario" readonly="readonly" width="170px" class="cajatexto_1"-->
								<form:input path="nomUsuario" id="txtUsuarioNombre" cssStyle="width:190px"  readonly="readonly"  cssClass="cajatexto_1"  />
							 </td >
							<td width="70px">
								<form:select path="codPc" id="codPc" cssClass="cajatexto" cssStyle="width:100px;">
									<form:option value="">&nbsp;</form:option>
									<c:if test="${control.listaComputadoras!=null}">
										<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" items="${control.listaComputadoras}" />									
									</c:if>
								</form:select>
							</td>
	                         <td class="Detalle" width="200px" >Hr.Ini:																		
									<form:input path="horaIni" id="txtHoraIni" cssClass="cajatexto_1" readonly="true" cssStyle="width:45px;"/>										
									&nbsp;
									Hr.Fin:
									<form:input maxlength="5" path="horaFin" id="txtHoraFin" cssClass="cajatexto" 
										cssStyle="width:40px" onkeypress="fc_ValidaHora('txtHoraFin');"
										onblur="fc_ValidaHoraOnBlur('frmAtencion','txtHoraFin','hora final');"/>									
							 </td>                        	
                        	<td width="80px" >
								<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">					
									<img src="${ctx}/images/botones/grabar1.jpg" onClick="javascript:grabar();" style="cursor:hand;" align="middle" id="btnGrabar" width="65" height="22" >
					 			</a>
							</td>
                         </tr>
                   </table>
				</td>
			</tr>
	  </table>
	</td>
  </tr>
  <tr>
    <td height="1px"></td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="0" align="left" cellspacing="0" cellpadding="1" bordercolor="#999999" 
			style="BORDER-RIGHT: #048BBA 1px solid;BORDER-TOP: #048BBA 1px solid;BORDER-BOTTOM: #048BBA 1px solid;BORDER-LEFT: #048BBA 1px solid;" >
			<tr class="fondoceldablanco">
				<td width="40px">&nbsp;</td>
				<c:forEach var="itemHoras" items="${requestScope.listaHoras}" varStatus="nItem">				
					<td id="tdHr<c:out value='${itemHoras.idHoraIni}'/>" align="center" style="BORDER-LEFT: #048BBA 1px solid;">
						<c:out value="${itemHoras.horaIni}"/>-<c:out value="${itemHoras.horaFin}"/> 
					</td>
		  		</c:forEach>		  
			</tr>
			<!-- requestScope -->
			<c:forEach var="itemRec" items="${control.listaRecursos}">
				<tr class="fondoceldablanco" >
					<td width="40px" style="cursor: pointer;" onClick="javascript:historial('<c:out value="${itemRec.codSala}"/>','<c:out value="${itemRec.nombreSala}"/>');" align="center" style="border-top: #048BBA 1px solid;">						
						<c:out value="${itemRec.nombreSala}"/>
					</td>					

					<c:forEach var="itemHrs" items="${requestScope.listaHoras}" varStatus="nItem">
											<!-- 0001:Reservado 0002:Ejecutandose -->
						<c:if test="${itemRec.codEstado=='0001' || itemRec.codEstado=='0002'}">
							<c:choose>
								<c:when test="${itemHrs.idHoraFin > itemRec.idHoraIni && itemHrs.idHoraFin<=itemRec.idHoraFin}">
									<td id="tdRec<c:out value="${itemRec.codSala}"/><c:out value="${itemHrs.idHoraIni}"/><c:out value="${itemHrs.idHoraFin}"/>" class="cell" align="center" style="BORDER-TOP: #048BBA 1px solid;BORDER-LEFT: #048BBA 1px solid;cursor:pointer;"  
										bgcolor="white" 
										onclick="reserva('<c:out value="${itemRec.nombreUsuario}"/>','<c:out value="${itemRec.idHoraIni}"/>','<c:out value="${itemRec.idHoraFin}"/>');" name="xxx">
									 	<c:if test="${itemRec.codEstado=='0001'}">
											<%-- 
											<c:out value="${itemRec.idHoraIni}"/>-<c:out value="${itemRec.idHoraFin}"/>
											--%>
											
											<strong><span class="">Reservado</span></strong>
											
											<%--
											<c:out value="${itemHrs.idHoraIni}"/>-<c:out value="${itemHrs.idHoraFin}"/>
											--%>
											
											
										</c:if>
									 	<c:if test="${itemRec.codEstado=='0002'}">
											<!--<strong>Ejecutandose</strong> -->
											<img src="${ctx}/images/biblioteca/status_ejecucion.png" border="0">&nbsp;En ejecución
										</c:if>
									</td>
								</c:when>
								<c:otherwise>
									<td onClick="javascript:detalleBloque('tdRec<c:out value="${itemRec.codSala}"/><c:out value="${itemHrs.idHoraIni}"/><c:out value="${itemHrs.idHoraFin}"/>');" 
											id="tdRec<c:out value="${itemRec.codSala}"/><c:out value="${itemHrs.idHoraIni}"/><c:out value="${itemHrs.idHoraFin}"/>" align="center" class="cell" style="BORDER-TOP: #048BBA 1px solid;BORDER-LEFT: #048BBA 1px solid;" >
										&nbsp;
									</td>
								</c:otherwise>
							</c:choose>	

						</c:if>		
					
						<c:if test="${itemRec.codEstado!='0001' && itemRec.codEstado!='0002'}">
							<td onClick="javascript:detalleBloque('tdRec<c:out value="${itemRec.codSala}"/><c:out value="${itemHrs.idHoraIni}"/><c:out value="${itemHrs.idHoraFin}"/>');" 
									id="tdRec<c:out value="${itemRec.codSala}"/><c:out value="${itemHrs.idHoraIni}"/><c:out value="${itemHrs.idHoraFin}"/>" align="center" class="cell" style="BORDER-TOP: #048BBA 1px solid;BORDER-LEFT: #048BBA 1px solid;" >
								&nbsp;
							 	  
							</td>
						</c:if>

		  			</c:forEach>
				</tr>
			</c:forEach>
  </table>
	</td>
  </tr>
</table>
</form:form>

<!-- Rangos -->
<script type="text/javascript">
	var elementos = document.getElementById('txhRangos').value;	
	if(elementos!=null && elementos!=''){
		var vRangos = elementos.split(',');
		for (var i = 0; i < vRangos.length-1 ; i++){				
			//alert(vRangos[i]);			
			var vElemento = vRangos[i].split('|'); //dividir elemento en cod.recurso, rango_inicio y rango_fin			
			var codrecurso = vElemento[0];
			var rango_ini = parseFloat(vElemento[1]);
			var rango_fin ;			
			if (vElemento[2]=='XX'){
				rango_fin = parseFloat('21'); //libre no indica termino				
			}else{
				rango_fin = parseFloat(vElemento[2]);
			}			
			var estado =  vElemento[3];
			//obteniendo id de TD's
			var ini;
			var fin;
			for (var j = 0; j < (rango_fin-rango_ini) ; j++){
				//id="tdRec00322021"
				//var finparcial = (rango_fin-rango_ini);				
				ini = rango_ini + j;
				fin = ini + 1;
				
				ini = '0'+ini; //formato "08" ,"09", etc.
				ini = ini.substr(ini.length-2,2);
				fin = '0'+fin;
				fin = fin.substr(fin.length-2,2);
				
				var idTd = 'tdRec'+codrecurso+ini+fin;
				//alert('tdRec'+codrecurso+ini+fin);
				//var celda = document.getElementById(idTd);
				
				//newImage = "url(/biblioteca/images/useraten_on.gif)";
				//imgOld = "url(/biblioteca/images/useraten_off.gif)";

				newImage = "url(/biblioteca/images/biblioteca/status_ejecucion.png)";
				imgOld = "url(/biblioteca/images/biblioteca/status_ejecutado.png)";
				
				//document.getElementById(idTd).style.cursor= "hand";
				if(estado=='X'){
// 		        	document.getElementById(idTd).style.backgroundImage = newImage;
					document.getElementById(idTd).bgColor = '#FFF65B';
					document.getElementById(idTd).innerHTML='<img src="/biblioteca/images/biblioteca/status_ejecucion.png" title=""/>&nbsp;En ejecuci&oacute;n';
				}else if(estado=='T'){
					document.getElementById(idTd).style.backgroundImage = imgOld;
					//document.getElementById(idTd).bgColor = '#00ff00';
				}
			}		
		}
	}
</script>

<!-- Manejo de Mensajes -->
<script type="text/javascript" language="javascript">
	if (document.getElementById('txhTipoMensaje').value != ''){
		strMsg = document.getElementById('txhMensaje').value;
		cancel = 'Operación Cancelada:\n';
		switch(document.getElementById('txhTipoMensaje').value){
			case 'ERROR':
				if ( strMsg != ""){
					alert(cancel + strMsg);
				}
				break;
			case 'OK':
				if ( strMsg != ''){
					alert(strMsg);
					document.frmAtencion.txtUsuario.value='';
					document.frmAtencion.txtHoraFin.value='';
					document.frmAtencion.txtUsuarioNombre.value='';
				}
				break;
			/*case 'REG':
					alert("---");
				break;*/
		}
	}
	document.frmAtencion.txhTipoMensaje.value = '';
	document.frmAtencion.txhMensaje.value = '';
	document.frmAtencion.txhAccion.value = '';
</script>
<script type="text/javascript">	
	var tiempo_hora=document.getElementById('horaActual').value;
	var tiempo_minuto=document.getElementById('minutoActual').value;
	var tiempo_segundo=document.getElementById('segundoActual').value;
	var tiempo=new Date(2009,6,15,tiempo_hora,tiempo_minuto,tiempo_segundo);
	mostrarReloj();
</script>
</body>


</html>