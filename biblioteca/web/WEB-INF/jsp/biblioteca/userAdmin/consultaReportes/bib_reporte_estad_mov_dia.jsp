<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="center">Reporte Material Bibliogr�fico Faltante</td>
	<td align="left">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
		
		reporte.setCodigoMaterial(rs.getString("COD_MATERIAL") == null ? "" : rs.getString("COD_MATERIAL") );
	        	reporte.setCodigo(rs.getString("CODIGO") == null ? "" : rs.getString("CODIGO") );
	        	reporte.setTipoMaterial(rs.getString("TIPO_MATERIAL") == null ? "" : rs.getString("TIPO_MATERIAL") );
	        	reporte.setTituloMaterial(rs.getString("TITULO") == null ? "" : rs.getString("TITULO") );
	        	reporte.setNroIngresos(rs.getString("NRO_INGRESO") == null ? "" : rs.getString("NRO_INGRESO") );
	        	reporte.setUsuario(rs.getString("USUARIO") == null ? "" : rs.getString("USUARIO") );
	        	reporte.setTipoUsuario(rs.getString("TIPOUSUARIO") == null ? "" : rs.getString("TIPOUSUARIO") );
	        	reporte.setFecPrestamo(rs.getString("FECHA_PRESTAMO") == null ? "" : rs.getString("FECHA_PRESTAMO") );
		
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Nro.</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Codigo</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Tipo Material</td>
				<td class="cabecera_grilla" width="35%" nowrap="nowrap">T�tulo</td>
				<td class="cabecera_grilla" width="35%" nowrap="nowrap">Nro. Ingresos</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Usuario</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Tipo de Usuario</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Fecha de Pr�stamo</td>
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					Integer numero = 0;
					Integer cont = 0;
					String variable = "";
					String variable2 = "";
					
					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						
						variable = obj.getCodigoMaterial();
						
						for(int j=0;j<consulta.size();j++){
							Reporte obj2  = (Reporte) consulta.get(j);
							
							if(obj2.getCodigoMaterial().equals(variable)){
								if (cont == 0){
									nroIngresos = obj2.getNroIngresos();
								}
								else{
									nroIngresos = nroIngresos + obj2.getNroIngresos() + ", ";
								}
								cont = cont + 1;
							}
							
						}
						if (!variable.equals(variable2)) {
							numero = numero + 1;
						%>
							<tr class="texto_grilla" style="vertical-align: middle;">
								<td style="text-align:center;"><%=numero%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getCodigo()==null?"":obj.getCodigo()%></td>
								<td style="text-align:left;"><%=obj.getTipoMaterial()==null?"":obj.getTipoMaterial()%></td>
								<td style="text-align:left;"><%=obj.getTituloMaterial()==null?"":obj.getTituloMaterial()%></td>
								<td style="text-align:left;"><%=nroIngresos%></td>
								<td style="text-align:left;"><%=obj.getUsuario()==null?"":obj.getUsuario()%></td>
								<td style="text-align:left;"><%=obj.getTipoUsuario()==null?"":obj.getTipoUsuario()%></td>
								<td style="text-align:left;"><%=obj.getFecPrestamo()==null?"":obj.getFecPrestamo()%></td>
							</tr>
						<%	
							variable2 = obj.getCodigoMaterial();
						}
						nroIngresos = "";
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>