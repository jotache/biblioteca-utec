<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td colspan="7" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td colspan="7" align="center">Reporte Por Movimiento de Pr�stamos y Devoluciones por D�a y Hora</td>
	<td align="left">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Periodo :</b></td>
	<td colspan="7" align="left">${model.fecIni} -  ${model.fecFin}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Sede :</b></td>
	<td colspan="7" align="left">${model.dscSede}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr rowspan="2">
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Fecha</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">D�a</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Hora</td>
				<td class="cabecera_grilla" width="35%" nowrap="nowrap">Pr�stamos<br>Sala</td>
				<td class="cabecera_grilla" width="35%" nowrap="nowrap">Pr�stamos<br>Domicilio</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Total<br>Pr�stamos</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Devoluciones</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Total<br>Movimientos</td>
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						%>
							<tr class="texto_grilla" style="vertical-align: middle;">
								<td style="text-align:center;"><%=obj.getFechaPresDev()==null?"":obj.getFechaPresDev()%></td>
								<td style="text-align:center;"><%=obj.getHora()==null?"":obj.getHora()%></td>
								<td style="text-align:left;"><%=obj.getDiaSem()==null?"":obj.getDiaSem()%></td>
								<td style="text-align:right;"><%=obj.getNroPrestSala()==null?"":obj.getNroPrestSala()%></td>
								<td style="text-align:right;"><%=obj.getNroPrestCasa()==null?"":obj.getNroPrestCasa()%></td>
								<td style="text-align:right;"><%=obj.getTotalPrestamos()==null?"":obj.getTotalPrestamos()%></td>
								<td style="text-align:right;"><%=obj.getDevoluciones()==null?"":obj.getDevoluciones()%></td>
								<td style="text-align:right;"><%=obj.getTotalMovimientos()==null?"":obj.getTotalMovimientos()%></td>
							</tr>
						<%
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>