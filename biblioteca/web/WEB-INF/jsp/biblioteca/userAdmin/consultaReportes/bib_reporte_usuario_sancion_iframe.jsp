<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
function onLoad(){}
</script>
</head>
<body>


<form:form  id="frmMain" commandName="control" action="${ctx}/bib_reporte_usuario_sancion_iframe.html" method="post">

<table width="100%" border="0">
	<tr>
		<td>
			<div style="OVERFLOW:auto; WIDTH: 100%; HEIGHT: 250px;" id="tblUsuarios" >
				<table border="0" style="OVERFLOW:auto; WIDTH: 99%; border: 1px solid #048BBA; table-layout: auto;">
				<tr>
					<td class="grilla" nowrap="nowrap">N�</td>
					<td class="grilla" nowrap="nowrap">C�digo</td>
					<td class="grilla" nowrap="nowrap">Usuario</td>
					<td class="grilla" nowrap="nowrap">Nombre Usuario</td>
					<td class="grilla" nowrap="nowrap">Cic.</td>
					<td class="grilla" nowrap="nowrap">Tipo Usuario</td>
					<td class="grilla" nowrap="nowrap">Esp.</td>
										
					<td class="grilla"  nowrap="nowrap">Titulo..</td>
					<td class="grilla" nowrap="nowrap">Nro. Ing.</td>
					<td class="grilla" nowrap="nowrap">Clasificaci�n</td>
					<td class="grilla" nowrap="nowrap">Fec. Prestamo</td>
					<td class="grilla" nowrap="nowrap">Fec. Devol. Prog</td>
					<td class="grilla" nowrap="nowrap">Fec. Devoluci�n</td>
					<td class="grilla" nowrap="nowrap">Demora</td>					
					<td class="grilla" nowrap="nowrap">Resultado</td>
				</tr>
				<%  
					if(request.getSession().getAttribute("consulta")!=null){
						String nroIngresos = "";
						
						List consulta = (List) request.getSession().getAttribute("consulta");
	
						if (consulta.size() > 0) {
							for(int i=0;i<consulta.size();i++){
								
								
								Reporte obj  = (Reporte) consulta.get(i);
								%>
									<tr class="tablagrilla" style="vertical-align: middle;">
										<td style="text-align:center;"><%=i+1%></td>										
										<td style="text-align:left;">&nbsp;<%=obj.getCodUsuario()==null?"":obj.getCodUsuario()%></td>
										<td style="text-align:left;">&nbsp;<%=obj.getUsuario()==null?"":obj.getUsuario()%></td>										
										<td style="text-align:left;">&nbsp;<%=obj.getNomUsuario()==null?"":obj.getNomUsuario()%></td>
										<td style="text-align:center;">&nbsp;<%=obj.getCiclo()==null?"":obj.getCiclo()%></td>
										<td style="text-align:left;"><%=obj.getTipoUsuario()==null?"":obj.getTipoUsuario()%></td>
										<td style="text-align:left;"><%=obj.getEspecialidad()==null?"":obj.getEspecialidad()%></td>
										<td style="text-align:left;"><%=obj.getTituloMaterial()%></td>
										<td style="text-align:left;"><%=obj.getNroIngresos()%></td>
										<td style="text-align:left;"><%=obj.getTipoMaterial() %></td> <!-- clasificacion -->
										<td style="text-align:left;"><%=obj.getFecPrestamo() %></td>
										<td style="text-align:left;"><%=obj.getFechaPresDev() %></td>
										<td style="text-align:left;"><%=obj.getFechaRetiro() %></td>
										<td style="text-align:left;"><%=obj.getDetalle()%></td> <!-- dias de demora -->
										
										
										<td style="text-align:left;"><%=obj.getTipoResultSancion()==null?"":obj.getTipoResultSancion()%></td>
									</tr>
								<%
							}
						}
						else{
							%>
								<tr class="tablagrilla" style="vertical-align: middle;">
									<td style="text-align:center;" colspan="15">No se encontraron registros</td>
								</tr>
							<%
						}
					}
					request.removeAttribute("consulta");
				%>
				</table>
			</div>
		</td>
	</tr>
	
	
	
	
	
</table>
</form:form>	
</body>