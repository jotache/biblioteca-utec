<%@page import='java.util.List'%>
<%@page import='java.text.*'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}
.cabecera_grilla02 {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="center">Reporte Resumen de Movimientos por D�a y Hora</td>
	<td align="left" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Periodo :</b></td>
	<td colspan="7" align="left">${model.fecIni} -  ${model.fecFin}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Sede :</b></td>
	<td colspan="7" align="left">${model.dscSede}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Hora</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Lunes</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Martes</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Miercoles</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Jueves</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Viernes</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Sabado</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Domingo</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Promedio</td>
			</tr>
			<%  int totalLunes=0;
				int totalMartes=0;
				int totalMiercoles=0;
				int totalJueves=0;
				int totalViernes=0;
				int totalSabado=0;
				int totalDomingo=0;
				
				float totalPromedio=0;
				
				DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
				simbolo.setDecimalSeparator('.');
				DecimalFormat df = new DecimalFormat("#.##",simbolo);
				
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						int diaLunes = Integer.parseInt(obj.getDiaLunes());
						int diaMartes = Integer.parseInt(obj.getDiaMartes());
						int diaMiercoles = Integer.parseInt(obj.getDiaMiercoles());
						int diaJueves = Integer.parseInt(obj.getDiaJueves());
						int diaViernes = Integer.parseInt(obj.getDiaViernes());
						int diaSabado = Integer.parseInt(obj.getDiaSabado());
						int diaDomingo = Integer.parseInt(obj.getDiaDomingo());
						float promedio = diaLunes+diaMartes+diaMiercoles+diaJueves+diaViernes+diaSabado+diaDomingo;
						promedio=promedio/7;
						//calculando totales
						totalLunes = totalLunes + Integer.parseInt(obj.getDiaLunes());
						totalMartes = totalMartes + Integer.parseInt(obj.getDiaMartes());
						totalMiercoles = totalMiercoles + Integer.parseInt(obj.getDiaMiercoles());
						totalJueves = totalJueves + Integer.parseInt(obj.getDiaJueves());
						totalViernes = totalViernes + Integer.parseInt(obj.getDiaViernes());
						totalSabado = totalSabado + Integer.parseInt(obj.getDiaSabado());
						totalDomingo = totalDomingo + Integer.parseInt(obj.getDiaDomingo());
						totalPromedio = totalPromedio + promedio;
						%>
							<tr class="texto_grilla" style="vertical-align: middle;">
								<td style="text-align:right;"><%=obj.getHora()==null?"":obj.getHora()%></td>
								<td style="text-align:right;"><%=obj.getDiaLunes()==null?"":obj.getDiaLunes()%></td>
								<td style="text-align:right;"><%=obj.getDiaMartes()==null?"":obj.getDiaMartes()%></td>
								<td style="text-align:right;"><%=obj.getDiaMiercoles()==null?"":obj.getDiaMiercoles()%></td>
								<td style="text-align:right;"><%=obj.getDiaJueves()==null?"":obj.getDiaJueves()%></td>
								<td style="text-align:right;"><%=obj.getDiaViernes()==null?"":obj.getDiaViernes()%></td>
								<td style="text-align:right;"><%=obj.getDiaSabado()==null?"":obj.getDiaSabado()%></td>
								<td style="text-align:right;"><%=obj.getDiaDomingo()==null?"":obj.getDiaDomingo()%></td>
								<td style="text-align:right;"><%=df.format(promedio)%></td>
							</tr>
						<%
					}
			%>
			</table>
		</td>
		<td></td>
	</tr>
	<tr>
		<td>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<table border="1">
				<tr>
					<td class="cabecera_grilla02" width="10%" nowrap="nowrap">Totales</td>
					<td class="texto_grilla" style="text-align:right;" width="10%" nowrap="nowrap"><%=totalLunes%></td>
					<td class="texto_grilla" style="text-align:right;" width="10%" nowrap="nowrap"><%=totalMartes%></td>
					<td class="texto_grilla" style="text-align:right;" width="10%" nowrap="nowrap"><%=totalMiercoles%></td>
					<td class="texto_grilla" style="text-align:right;" width="10%" nowrap="nowrap"><%=totalJueves%></td>
					<td class="texto_grilla" style="text-align:right;" width="10%" nowrap="nowrap"><%=totalViernes%></td>
					<td class="texto_grilla" style="text-align:right;" width="10%" nowrap="nowrap"><%=totalSabado%></td>
					<td class="texto_grilla" style="text-align:right;" width="30%" nowrap="nowrap"><%=totalDomingo%></td>
					<td class="texto_grilla" style="text-align:right;" width="30%" nowrap="nowrap"><%=df.format(totalPromedio)%></td>
				</tr>
			</table>
		</td>
		<%
		}
		request.removeAttribute("consulta");
	%>
	</tr>
</table>

</body>
</html>