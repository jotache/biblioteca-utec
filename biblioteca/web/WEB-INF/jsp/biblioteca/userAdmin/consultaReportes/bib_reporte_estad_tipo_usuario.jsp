<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td colspan="8" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td></td>
	<td colspan="8" align="center">Reporte por Tipo de Usuario</td>
	<td align="left" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="10" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Periodo :</b></td>
	<td colspan="9" align="left">${model.fecIni} -  ${model.fecFin}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Sede :</b></td>
	<td colspan="9" align="left">${model.dscSede}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="10" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr rowspan="2">
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Nro.</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Tipo<br>Usuario</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Dewey</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Idioma</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">C�digo</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Tipo Material</td>
				<td class="cabecera_grilla" width="35%" nowrap="nowrap">T�tulo</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Nro<br>Usuarios</td>
				<td class="cabecera_grilla" width="35%" nowrap="nowrap">Pr�stamos<br>Sala</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Pr�stamos<br>Domicilio</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Total<br>Pr�stamos</td>
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";

					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						%>
							<tr class="texto_grilla" style="vertical-align: middle;">
								<td style="text-align:center;"><%=i+1%></td>																
								<td style="text-align:left;"><%=obj.getUsuario()==null?"":obj.getUsuario()%></td>
								<td style="text-align:left;"><%=obj.getDewey()==null?"":obj.getDewey()%></td>
								<td style="text-align:left;"><%=obj.getIdioma()==null?"":obj.getIdioma()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getCodigo()==null?"":obj.getCodigo()%></td>
								<td style="text-align:left;"><%=obj.getTituloMaterial()==null?"":obj.getTituloMaterial()%></td>
								<td style="text-align:left; vertical-align: text-top;" valign="top" class="texto_grilla" style="height: 25px;"><%=obj.getDscMaterial()==null?"":obj.getDscMaterial()%></td>
								<td style="text-align:right;"><%=obj.getNroUsuarios()==null?"":obj.getNroUsuarios()%></td>
								<td style="text-align:right;"><%=obj.getNroPrestSala()==null?"":obj.getNroPrestSala()%></td>
								<td style="text-align:right;"><%=obj.getNroPrestCasa()==null?"":obj.getNroPrestCasa()%></td>
								<td style="text-align:right;"><%=obj.getTotalPrestamos()==null?"":obj.getTotalPrestamos()%></td>
							</tr>
						<%
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>