<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>	
	<td colspan="4" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td colspan="2" align="left">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>	
	<td colspan="4" align="center">Reporte de Usuarios Deudores y Sancionados</td>
	<td colspan="2" align="left">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="4" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
  <td></td>
  <td align="left"><strong>Sede : </strong></td>
  <td colspan="2" align="left">${model.dscSede}</td>
  <td></td>
  <td></td>
</tr>
<tr class="texto">
	<td></td>
	<td align="left"><b>Periodo :</b></td>
	<td colspan="2" align="left">${model.dscPeriodo}</td>
	<td></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td align="left" rowspan="2"><b>Tipo<br>Resultado :</b></td>
	<td colspan="3" align="left" style="vertical-align: middle;" rowspan="2">${model.dscSubOpcion}</td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
					<td class="cabecera_grilla" width="5%" nowrap="nowrap">Nro.</td>
					<td class="cabecera_grilla" width="4%" nowrap="nowrap">Código</td>
					<td class="cabecera_grilla" width="4%" nowrap="nowrap">Usuario</td>
					<td class="cabecera_grilla" width="10%" nowrap="nowrap">Nombre Usuario</td>
					<td class="cabecera_grilla" width="2%" nowrap="nowrap">Cic.</td>
					<td class="cabecera_grilla" width="30%" nowrap="nowrap">Tipo Usuario</td>
					<td class="cabecera_grilla" width="2%" nowrap="nowrap">ESP</td>					
					<td class="cabecera_grilla" width="30%" nowrap="nowrap">Titulo</td>
					<td class="cabecera_grilla" width="3%" nowrap="nowrap">Nro. Ing.</td>
					<td class="cabecera_grilla" width="5%" nowrap="nowrap">Clasificación</td>
					<td class="cabecera_grilla" width="5%" nowrap="nowrap">Fec. Prestamo</td>
					<td class="cabecera_grilla" width="5%" nowrap="nowrap">Fec. Devol. Prog</td>
					<td class="cabecera_grilla" width="5%" nowrap="nowrap">Fec. Devolución</td>
					<td class="cabecera_grilla" width="3%" nowrap="nowrap">Demora</td>					
					<td class="cabecera_grilla" width="5%" nowrap="nowrap">Resultado</td>
					
			</tr>
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					
					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						
						%>
							<tr class="texto_grilla" style="vertical-align: middle;">
								<td style="text-align:center;"><%=i+1%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getCodUsuario()==null?"":obj.getCodUsuario()%></td>
										<td style="text-align:left;">&nbsp;<%=obj.getUsuario()==null?"":obj.getUsuario()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getNomUsuario()==null?"":obj.getNomUsuario()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getCiclo()==null?"":obj.getCiclo()%></td>
								<td style="text-align:left;"><%=obj.getTipoUsuario()==null?"":obj.getTipoUsuario()%></td>
								<td style="text-align:left;"><%=obj.getEspecialidad()==null?"":obj.getEspecialidad()%></td>
								
								<td style="text-align:left;"><%=obj.getTituloMaterial()%></td>
										<td style="text-align:left;"><%=obj.getNroIngresos()%></td>
										<td style="text-align:left;"><%=obj.getTipoMaterial() %></td> <!-- clasificacion -->
										<td style="text-align:left;"><%=obj.getFecPrestamo() %></td>
										<td style="text-align:left;"><%=obj.getFechaPresDev() %></td>
										<td style="text-align:left;"><%=obj.getFechaRetiro() %></td>
										<td style="text-align:left;"><%=obj.getDetalle()%></td> <!-- dias de demora -->
								
								<td style="text-align:left;"><%=obj.getTipoResultSancion()==null?"":obj.getTipoResultSancion()%></td>
							</tr>
						<%
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>