<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td colspan="10" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="10" align="center">Reporte de Reserva de Salas por Ciclo y Especialidad</td>
	<td align="left" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Periodo :</b></td>
	<td colspan="2" align="left">${model.fecIni} -  ${model.fecFin}</td>
	<td colspan="5" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Sede :</b></td>
	<td colspan="2" align="left">${model.dscSede}</td>
	<td colspan="5" align="left"></td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="8" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" style="width:5%;" nowrap="nowrap">Nro.</td>
				<td class="cabecera_grilla" style="width:15%;" nowrap="nowrap">Especialidad</td>
				<td class="cabecera_grilla" style="width:5%;" nowrap="nowrap">Ciclo</td>
				<td class="cabecera_grilla" style="width:10%;" nowrap="nowrap">Sala</td>
				<td class="cabecera_grilla" style="width:20%;" nowrap="nowrap">Usuario</td>
				<td class="cabecera_grilla" style="width:15%;" nowrap="nowrap">Fecha Reserva</td>
				<td class="cabecera_grilla" style="width:10%;" nowrap="nowrap">Hr. Inicio</td>
				<td class="cabecera_grilla" style="width:10%;" nowrap="nowrap">Hr. Fin</td>
				<td class="cabecera_grilla" style="width:10%;" nowrap="nowrap">Estado</td>
			</tr>
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					List consulta = (List) request.getSession().getAttribute("consulta");
					String codEstado = "";
					String horaIni = "";
					String horaFin = "";
					for(int i=0;i<consulta.size();i++){						
						Reporte obj  = (Reporte) consulta.get(i);
						codEstado = obj.getCodEstadoReserva()==null?"":obj.getCodEstadoReserva();
						
						if (codEstado.equals("0001")){
							horaIni= obj.getHoraIniProg()==null?"":obj.getHoraIniProg();
							horaFin= obj.getHoraFinProg()==null?"":obj.getHoraFinProg();
						}else if (codEstado.equals("0002") || codEstado.equals("0004") || codEstado.equals("X") || codEstado.equals("T")){
							horaIni= obj.getHoraIniEjec()==null?"":obj.getHoraIniEjec();
							horaFin= obj.getHoraFinEjec()==null?"":obj.getHoraFinEjec();
						}else{
							horaIni="";
							horaFin="";
						}												
						%>						
						<tr class="texto_grilla" style="vertical-align: middle;">
								<td style="text-align:center;"><%=i+1%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getEspecialidad()==null?"":obj.getEspecialidad()%></td>
								<td style="text-align:left;"><%=obj.getCiclo()==null?"":obj.getCiclo()%></td>
								<td style="text-align:left;"><%=obj.getSala()==null?"":obj.getSala()%></td>
								<td style="text-align:left;"><%=obj.getNomUsuario()==null?"":obj.getNomUsuario()%></td>
								<td style="text-align:left;"><%=obj.getFechaReserva()==null?"":obj.getFechaReserva()%></td>
								<td style="text-align:left;"><%=horaIni%></td>								
								<td style="text-align:right;"><%=horaFin%></td>								
								<td style="text-align:right;"><%=obj.getEstadoReservaSala()==null?"":obj.getEstadoReservaSala()%></td>																							
							</tr>
						<%
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>