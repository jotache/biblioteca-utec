<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}
.detalle_numero {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
	mso-style-parent:style0;
	mso-number-format:Standard;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td colspan="10" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left">Fecha : </td>
	<td align="left">${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="10" align="center">Reporte Material Bibliogr�fico Adquiridos</td>
	<td align="left">Hora : </td>
	<td align="left">${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="10" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Periodo :</b></td>
	<td colspan="2" align="left">${model.fecIni} -  ${model.fecFin}</td>
	<td colspan="6"></td>
	<td></td>
</tr>
<tr class="texto">
  <td></td>
  <td><strong>Sede : </strong></td>
  <td align="left">${model.dscSede}</td>
  <td colspan="7" align="left"></td>
  <td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>T.C. :</b></td>
	<td align="left">${model.tipoCambio}</td>
	<td colspan="7" align="left"></td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="9" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr rowspan="2">
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Nro.</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">C�digo</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Tipo Material</td>
				<td class="cabecera_grilla" width="35%" nowrap="nowrap">T�tulo<br>Nro. Ingresos</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Autor</td>
				<td class="cabecera_grilla" width="15%" nowrap="nowrap">Nro.<br>Ejemplares</td>
				<td class="cabecera_grilla" width="30%" nowrap="nowrap">Procedencia</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Moneda</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Precio<br>Ult. Compra</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Total</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Idioma</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Dewey</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Nro. Ingresos</td>				
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					Integer numero = 0;
					Integer cont = 0;
					String variable = "";
					String variable2 = "";
					
					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						
						variable = obj.getCodigoMaterial();
						
						for(int j=0;j<consulta.size();j++){
							Reporte obj2  = (Reporte) consulta.get(j);
							
							if(obj2.getCodigoMaterial().equals(variable)){
								if (cont == 0){
									nroIngresos = obj2.getNroIngresos();
								}
								else{
									nroIngresos = nroIngresos + "( "+obj.getFechaReserva()+ " ), " + obj2.getNroIngresos() + "( "+obj.getFechaReserva()+ " ), ";
								}
								cont = cont + 1;
							}
							
						}
						if (!variable.equals(variable2)) {
							numero = numero + 1;
						%>
							<tr rowspan="2" style="vertical-align: middle;">
								<td style="text-align:center;" class="texto_grilla"><%=numero%></td>
								<td style="text-align:left;" class="texto_grilla">&nbsp;<%=obj.getCodigo()==null?"":obj.getCodigo()%></td>
								<td style="text-align:left;" class="texto_grilla"><%=obj.getTipoMaterial()==null?"":obj.getTipoMaterial()%></td>
<%-- 								<td style="text-align:left; vertical-align: text-top;" valign="top" class="texto_grilla" style="height: 50px;"><%=obj.getTituloMaterial()==null?"":obj.getTituloMaterial()%><br><%=nroIngresos%></td> --%>
								<td style="text-align:left; vertical-align: text-top;" valign="top" class="texto_grilla" style="height: 50px;"><%=obj.getTituloMaterial()==null?"":obj.getTituloMaterial()%><br><%=nroIngresos%></td>
								<td style="text-align:left;" class="texto_grilla"><%=obj.getAutor()==null?"":obj.getAutor()%></td>
								<td style="text-align:right;" class="texto_grilla"><%=obj.getNroVolumenes()==null?"":obj.getNroVolumenes()%></td>
								<td style="text-align:left;" class="texto_grilla"><%=obj.getProcedencia()==null?"":obj.getProcedencia()%></td>
								<td style="text-align:center;" class="texto_grilla"><%=obj.getMoneda()==null?"":obj.getMoneda()%></td>
								<td style="text-align:right;" class="detalle_numero"><%=obj.getPrecio()==null?"":obj.getPrecio()%></td>
								<td style="text-align:right;" class="detalle_numero"><%=obj.getTotal()==null?"":obj.getTotal()%></td>
								<td style="text-align:left;" class="detalle_numero"><%=obj.getIdioma()==null?"":obj.getIdioma()%></td>
								<td style="text-align:left;" class="detalle_numero"><%=obj.getNomDewey()==null?"":obj.getNomDewey()%></td>
							</tr>
						<%	
							variable2 = obj.getCodigoMaterial();
						}
						nroIngresos = "";
						cont = 0;
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>