<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td colspan="7" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="7" align="center">Reporte Estad�stico del Buz�n de Sugerencias</td>
	<td align="left">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="7" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Periodo :</b></td>
	<td colspan="6" align="left">${model.periodo}</td>
	<td></td>
</tr>
<tr class="texto">
	<td></td>
	<td><b>Material :</b></td>
	<td colspan="6" align="left">${model.descMaterial}</td>
	<td></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="7" align="left"></td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="20%">Fec. Reg.</td>
				<td class="cabecera_grilla" width="25%">Tipo Sugerencia</td>
				<td class="cabecera_grilla" width="30%">Sugerencia</td>
				<td class="cabecera_grilla" width="30%">Detalle</td>
				<td class="cabecera_grilla" width="25%">Calificaci�n</td>
				<td class="cabecera_grilla" width="25%">Orientado a</td>
				<td class="cabecera_grilla" width="30%">Usuario</td>
				<td class="cabecera_grilla" width="20%">Estado</td>
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
				List consulta = (List) request.getSession().getAttribute("consulta"); 
					for(int i=0;i<consulta.size();i++){
					
					Reporte obj  = (Reporte) consulta.get(i);%>
					<tr class="texto_grilla">
						<td style="text-align:center"><%=obj.getFechaReg()==null?"":obj.getFechaReg()%></td>
						<td style="text-align:left"><%=obj.getTipoSugerencia()==null?"":obj.getTipoSugerencia()%></td>
						
						<td style="text-align:left"><%=obj.getSugerencia()==null?"":obj.getSugerencia()%></td>
						
						<td style="text-align:left vertical-align: text-top;" valign="top" class="texto_grilla" style="height: 25px;"><%=obj.getDetalle()==null?"":obj.getDetalle()%></td>
						<td style="text-align:left"><%=obj.getCalificacion()==null?"":obj.getCalificacion()%></td>
						<td style="text-align:left"><%=obj.getOrientadoA()==null?"":obj.getOrientadoA()%></td>
						<td style="text-align:left"><%=obj.getUsuario()==null?"":obj.getUsuario()%></td>
						<td style="text-align:center"><%=obj.getEstado()==null?"":obj.getEstado()%></td>
					</tr>
					<%}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>