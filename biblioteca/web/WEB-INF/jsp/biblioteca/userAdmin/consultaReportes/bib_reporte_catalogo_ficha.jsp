<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

.detalle_numero {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
	mso-style-parent:style0;
	mso-number-format:Standard;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>
	<td colspan="5" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="5" align="center">Reporte Catálogo de Fichas</td>
	<td align="left" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto">
	<td></td>
	<td colspan="5" align="left"><strong>Sede:&nbsp;${model.dscSede}</strong></td>
	<td align="left" colspan="2"></td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="5" align="left"></td>
	<td></td>
</tr>
</table>
<br>

<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr rowspan="2">
				<td class="cabecera_grilla" width="5%" nowrap="nowrap">Nro.</td>
				<td class="cabecera_grilla" width="5%" nowrap="nowrap">Código</td>				
				<td class="cabecera_grilla" width="5%" nowrap="nowrap">Cod Dewey</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Tipo Material</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Título<br>Nro. Ingresos</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Total Ingresos</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Descriptores</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Autor</td>
				<td class="cabecera_grilla" width="5%" nowrap="nowrap">Dewey</td>
					
				<td class="cabecera_grilla" width="5%" nowrap="nowrap">A&ntilde;o</td>
				<td class="cabecera_grilla" width="20%" nowrap="nowrap">Editorial</td>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">Tipo Vid.</td>
			</tr>
			
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					Integer numero = 0;
					Integer nroIng = 0; 
					Integer cont = 0;
					String variable = "";
					String variable2 = "";
					
					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						
						variable = obj.getCodigoMaterial();
						nroIng = 0;
						for(int j=0;j<consulta.size();j++){
							Reporte obj2  = (Reporte) consulta.get(j);
							
							if(obj2.getCodigoMaterial().equals(variable)){
								if (cont == 0){
									nroIngresos = obj2.getNroIngresos();
								}
								else{
									nroIngresos = nroIngresos + ", " + obj2.getNroIngresos();
								}
								nroIng = nroIng + 1;
								cont = cont + 1;
							}
						}
						
						if (!variable.equals(variable2)) {
							numero = numero + 1;
						%>
							<tr rowspan="2" style="vertical-align: middle;">
								<td style="text-align:center;" nowrap="nowrap" class="texto_grilla"><%=numero%></td>
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla">&nbsp;<%=obj.getCodigo()==null?"":obj.getCodigo()%></td>
								
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla">&nbsp;<%=obj.getCodDewey()==null?"":obj.getCodDewey()%></td>
								
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla"><%=obj.getTipoMaterial()==null?"":obj.getTipoMaterial()%></td>
								<td style="text-align:left; vertical-align: text-top;" valign="top" style="height: 50px;" class="texto_grilla" rowspan="1"><%=obj.getTituloMaterial()==null?"":obj.getTituloMaterial()%><br><%=nroIngresos%></td>
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla"><%=nroIng%></td>
								<td style="text-align:left; vertical-align: text-top;" valign="top" class="texto_grilla"><%=obj.getDescriptores()==null?"":obj.getDescriptores()%></td>
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla"><%=obj.getAutor()==null?"":obj.getAutor()%></td>
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla"><%=obj.getNomDewey()==null?"":obj.getNomDewey()%></td>								
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla"><%=obj.getAñoPublicacion()==null?"":obj.getAñoPublicacion()%></td>
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla"><%=obj.getNombreEditorial()==null?"":obj.getNombreEditorial()%></td>
								<td style="text-align:left;" nowrap="nowrap" class="texto_grilla"><%=obj.getTipoVideo()==null?"":obj.getTipoVideo()%></td>
							</tr>
						<%	
							variable2 = obj.getCodigoMaterial();
						}
						nroIngresos = "";
						cont = 0;
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>

</body>
</html>