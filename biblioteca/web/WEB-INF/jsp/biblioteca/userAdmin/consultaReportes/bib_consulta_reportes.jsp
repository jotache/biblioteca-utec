<%@ include file="/taglibs.jsp"%>

<head>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />	
	<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$("#nroIngresoIni").prop('disabled', true);
			$("#nroIngresoFin").prop('disabled', true);
				
			$("#chkTipoEtiqueta").click(function() {  
		        if($("#chkTipoEtiqueta").is(':checked')) {  
		        	$("#nroIngresoIni").prop('disabled', false);
		        	$("#nroIngresoFin").prop('disabled', false);
		        	$("#nroIngresoFin").removeClass("cajatexto_1");
		        	$("#nroIngresoFin").addClass("cajatexto_o");
		        	$("#nroIngresoIni").removeClass("cajatexto_1");
		        	$("#nroIngresoIni").addClass("cajatexto_o");
		        	
		        	$('#txtFechaIni').val('');
		        	$('#txtFechaFin').val('');
		        } else {  
		        	$("#nroIngresoIni").val('');
		        	$("#nroIngresoFin").val('');
		        	$("#nroIngresoIni").prop('disabled', true);
		        	$("#nroIngresoFin").prop('disabled', true);
		        	$("#nroIngresoFin").removeClass("cajatexto_o");
		        	$("#nroIngresoFin").addClass("cajatexto_1");
		        	$("#nroIngresoIni").removeClass("cajatexto_o");
		        	$("#nroIngresoIni").addClass("cajatexto_1");
		        }  
		    });
		});	  	
	</script>
	
	<script type="text/javascript">
	function onLoad(){
		document.getElementById("tblIframe").style.display='none';
	}
	function fc_ValidaDecimalSiNo(strNameObj, NumEntero, NumDecimal) 
	{
		var Obj = document.all[strNameObj];
		var fieldValue = Obj.value;

		if (fieldValue !=""){
			if (!fc_ValidaDecimal(fieldValue, NumEntero, NumDecimal)) {
				Obj.value="";
				alert('Debe ingresar un valor correcto.');
				Obj.focus();				
			}
			else{
				if (fieldValue.indexOf('.') != -1){
					var num = Number(fieldValue)
					Obj.value = num.toFixed(NumDecimal)
				}
			}
		}
	}	
	function buscarAlumno(){		
		Fc_Popup("${ctx}/biblioteca/biblio_buscar_usuario.html?tipoRetorno=C&ctrlUSE=&ctrlCOD=txtCodUsuario&ctrlNOM=",600,400);
	}	
	
	function fc_Regresar(){
		location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;
	}
	
	function fc_CambiaReporte(){
// 		$('#tablaBoton').style.display = "none";
		document.getElementById("tablaBoton").style.display = "none";
// 		$('#tblIframe').style.display = "none";
		document.getElementById("tblIframe").style.display = "none";
		var tablaBoton = document.getElementById("tablaBoton"); //$('#tablaBoton');
		//var opcion = $F('cboReportes');
		var opcion = $('#cboReportes').val();
// 		var tablaDeuSan = $('#tablaDeuSan');
		var tablaDeuSan = document.getElementById("tablaDeuSan");
// 		var tablaMatbib = $('#tablaMatbib');
		var tablaMatbib = document.getElementById("tablaMatbib");
// 		var tablaCicEsp = $('#tablaCicEsp');
		var tablaCicEsp = document.getElementById("tablaCicEsp");
// 		var tablaMovDia = $('#tablaMovDia');
		var tablaMovDia = document.getElementById("tablaMovDia");
// 		var tablaBuzSug = $('#tablaBuzSug');
		var tablaBuzSug = document.getElementById("tablaBuzSug");
// 		var tablaUsuInd = $('#tablaUsuInd');
		var tablaUsuInd = document.getElementById("tablaUsuInd");		
// 		var tablaCicEspReservas = $('#tablaCicEspReservas');
		var tablaCicEspReservas = document.getElementById("tablaCicEspReservas");		
// 		var tablaMovDiaReserv = $('#tablaMovDiaReserv');
		var tablaMovDiaReserv = document.getElementById("tablaMovDiaReserv");
		
		switch(opcion){
			case "":
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="none";
				break;
			case document.getElementById("txhReporte1").value:
				tablaDeuSan.style.display="inline-block";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="none";
				break;
			case document.getElementById("txhReporte2").value:
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="inline-block";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="none";
				tablaBoton.style.display = "inline-block";				
				break;
			case document.getElementById("txhReporte3").value:
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="inline-block";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="none";
				tablaBoton.style.display = "inline-block";
				break;
			case document.getElementById("txhReporte4").value:
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="inline-block";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="none";
				tablaBoton.style.display = "inline-block";
				break;
			case document.getElementById("txhReporte5").value:
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="inline-block";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="none";
				tablaBoton.style.display = "inline-block";
				break;
			case document.getElementById("txhReporte6").value:
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="inline-block";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="none";
				tablaBoton.style.display = "inline-block";				
				break;
			case document.getElementById("txhReporte7").value:
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="inline-block";
				tablaMovDiaReserv.style.display="none";
				tablaBoton.style.display = "inline-block";				
				break;
			case document.getElementById("txhReporte8").value:
				tablaDeuSan.style.display="none";
				tablaMatbib.style.display="none";
				tablaCicEsp.style.display="none";
				tablaMovDia.style.display="none";
				tablaBuzSug.style.display="none";
				tablaUsuInd.style.display="none";
				tablaCicEspReservas.style.display="none";
				tablaMovDiaReserv.style.display="inline-block";
				tablaBoton.style.display = "inline-block";				
				break;				
		}
	}
	
	function fc_CambiaTipoRepMatBib(){
		var trFecMatBib = document.getElementById("trFecMatBib"); // $('#trFecMatBib');
		trFecMatBib.style.display = "none";
		
// 		$("#tblIframe").style.display = "none";
		document.getElementById("tblIframe").style.display = "none";
		
// 		$("#trTipoCambio").style.display = "none";
		document.getElementById("trTipoCambio").style.display = "none";
		//var opcion = $F("cbotipoRepMatBib");
		var opcion = $("#cbotipoRepMatBib").val();
		var trNroRegistros = document.getElementById("trNroRegistros"); //$('#trNroRegistros');
		var tdLimpiarMatBib = document.getElementById("tdLimpiarMatBib"); //$('#tdLimpiarMatBib');
		var divProcedLabel = document.getElementById("divProcedLabel"); //$('#divProcedLabel');
		var divProcedDato = document.getElementById("divProcedDato"); //$('#divProcedDato');
		
		var divRangoIngLabel = document.getElementById("divRangoIngLabel");
		var divRangoDato = document.getElementById("divRangoDato");
		
		
		
		switch(opcion){
			
		case "":
				trFecMatBib.style.display="none";
				document.getElementById("trTipoCambio").style.display = "none"; //; $("#trTipoCambio").style.display = "none";
				trNroRegistros.style.display="none";
				tdLimpiarMatBib.style.display="none";		
				divProcedLabel.style.display="none";
				divProcedDato.style.display="none";
				divRangoIngLabel.style.display="none";
				divRangoDato.style.display="none";
				break;
			case document.getElementById("txhRep2A").value:				
				trFecMatBib.style.display="none";
				trNroRegistros.style.display="none";
				tdLimpiarMatBib.style.display="none";				
				divProcedLabel.style.display="none";
				divProcedDato.style.display="none";
				divRangoDato.style.display="none";
				break;
			case document.getElementById("txhRep2B").value:				
				trFecMatBib.style.display="inline-block";
				trNroRegistros.style.display="none";
				tdLimpiarMatBib.style.display="";				
				divProcedLabel.style.display="none";
				divProcedDato.style.display="none";
				divRangoIngLabel.style.display="none";
				divRangoDato.style.display="none";
				break;
			case document.getElementById("txhRep2C").value:
				trFecMatBib.style.display="inline-block";
				document.getElementById("trTipoCambio").style.display = "inline-block";
				trNroRegistros.style.display="none";
				tdLimpiarMatBib.style.display="";
				divProcedLabel.style.display="inline-block";
				divProcedDato.style.display="inline-block";
				divRangoIngLabel.style.display="none";
				divRangoDato.style.display="none";
				break;
			case document.getElementById("txhRep2D").value:
				trFecMatBib.style.display="inline-block";
				trNroRegistros.style.display="none";
				tdLimpiarMatBib.style.display="";				
				divProcedLabel.style.display="none";
				divProcedDato.style.display="none";
				divRangoIngLabel.style.display="none";
				divRangoDato.style.display="none";
				break;
			case document.getElementById("txhRep2E").value:
				trFecMatBib.style.display="inline-block";
				trNroRegistros.style.display="";
				tdLimpiarMatBib.style.display="";				
				divProcedLabel.style.display="none";
				divProcedDato.style.display="none";
				divRangoIngLabel.style.display="none";
				divRangoDato.style.display="none";
				break;
				
			//Erick
			case document.getElementById("txhRep2G").value:
				trFecMatBib.style.display="inline-block";
				trNroRegistros.style.display="none";
				tdLimpiarMatBib.style.display="";				
				divProcedLabel.style.display="none";
				divProcedDato.style.display="none";
				divRangoIngLabel.style.display="inline-block";
				divRangoDato.style.display="inline-block";
				break;
		}
	}
	
	function fc_Generar(){
		url="";
		//var opcion=$F("cboReportes");
		var opcion=$("#cboReportes").val();
		switch(opcion){
			case document.getElementById("txhReporte1").value:
				//alert('txtFechaIniUsuDeu:'+document.getElementById("txtFechaIniUsuDeu").value+'\ntxtFechaFinUsuDeu:'+document.getElementById("txtFechaFinUsuDeu").value);			
				if(document.getElementById("txtFechaIniUsuDeu").value=='' || document.getElementById("txtFechaFinUsuDeu").value==''){ //(document.getElementById("cboPeriodoSancion").value == ""){
					alert('Seleccione un per�odo');
					//document.getElementById("cboPeriodoSancion").focus();
					document.getElementById("txtFechaIniUsuDeu").focus();
					return false;
				}
				else{
					url="?opcion=" + document.getElementById("cboReportes").value +
						"&subOpcion=" + document.getElementById("codTipoResultado").value + 
						"&tipousuario=" + document.getElementById("cboTipoUsuarios").value + 
						"&sede=" + document.getElementById("cboSedeRep").value +
						"&periodo1=" + document.getElementById("txtFechaIniUsuDeu").value +
						"&periodo2=" + document.getElementById("txtFechaFinUsuDeu").value +
						//"&periodo=" + document.getElementById("cboPeriodoSancion").value + "/" +  
							//		  document.getElementById("cboAnioSancion").value + 
						//"&dscPeriodo=" + document.getElementById('cboPeriodoSancion')[document.getElementById('cboPeriodoSancion').selectedIndex].text + " " +  
							//		  document.getElementById("cboAnioSancion").value + 
						"&dscSubOpcion=" + document.getElementById('cboTipoUsuarios')[document.getElementById('cboTipoUsuarios').selectedIndex].text ;
				}	
				break;
			case document.getElementById("txhReporte2").value:				
				//REPORTE MATERIAL BIBLIOGRAFICO
				if(document.getElementById("cbotipoRepMatBib").value == ""){
					alert('Seleccione el Tipo de Reporte a generar.');
					document.getElementById("cbotipoRepMatBib").focus();
					return false;
				}
				else{
					if (document.getElementById("cbotipoRepMatBib").value == document.getElementById("txhRep2C").value){
						if (fc_Trim(document.getElementById("txtTipoCambio").value) == ""){
							alert('Debe ingresar el Tipo de Cambio');
							document.getElementById("txtTipoCambio").focus();
							return false;
						}
					}
					if (document.getElementById("cbotipoRepMatBib").value == document.getElementById("txhRep2E").value){
						if (fc_Trim(document.getElementById("txtNroRegistros").value) == ""){
							alert('Debe ingresar el Nro. de Registros a Mostrar.');
							document.getElementById("txtNroRegistros").focus();
							return false;
						}
					}
				
					if (document.getElementById("cbotipoRepMatBib").value == document.getElementById("txhRep2G").value){
					
						if(document.getElementById('chkTipoEtiqueta').checked == true){
							var n1 = fc_Trim($('#nroIngresoIni').val());
							var n2 = fc_Trim($('#nroIngresoFin').val());
							if(n1=='' || n2==''){
								alert('Debe ingresar un rango de Nros. de Ingreso');
								return false;
							}
						}else{
							var f1 = fc_Trim($('#txtFechaIni').val());
							var f2 = fc_Trim($('#txtFechaFin').val());
							if(f1=='' || f2==''){
								alert('Debe seleccionar un rango de fechas');
								return false;
							}
						}
					
// 						if (fc_Trim(document.getElementById("txtNroRegistros").value) == ""){
// 							alert('Debe ingresar el Nro. de Registros a Mostrar.');
// 							document.getElementById("txtNroRegistros").focus();
// 							return false;
// 						}
					}
					
					url="?opcion=" + document.getElementById("cboReportes").value +
						"&subOpcion=" + document.getElementById("cbotipoRepMatBib").value +
						"&sede=" + document.getElementById("cboSedeRep1").value +
						"&fecIni=" + document.getElementById("txtFechaIni").value +
						"&fecFin=" + document.getElementById("txtFechaFin").value +
						"&nroIng1=" + document.getElementById("nroIngresoIni").value +
						"&nroIng2=" + document.getElementById("nroIngresoFin").value +
						"&tipoCambio=" + document.getElementById("txtTipoCambio").value +
						"&nroMax=" + document.getElementById("txtNroRegistros").value +						
						"&procedencia=" + document.getElementById('cboProcedencia')[document.getElementById('cboProcedencia').selectedIndex].text
						"&dscSubOpcion=" + document.getElementById('cbotipoRepMatBib')[document.getElementById('cbotipoRepMatBib').selectedIndex].text ;
				} 
				break;
			//REPORTE ESTADISTICO POR CICLO Y ESPECIALIDAD
			case document.getElementById("txhReporte3").value:		
				if (document.getElementById("txtFechaIniEst").value==""){
					alert('Ingrese una fecha de inicio');
					document.getElementById("txtFechaIniEst").focus();
					return false;
				}
				if (document.getElementById("txtFechaFinEst").value==""){
					alert('Ingrese una fecha de fin');
					document.getElementById("txtFechaFinEst").focus();
					return false;
				}		
				if(document.getElementById("cboTipoRepCicloEsp").value == ""){
					alert('Seleccione el Tipo de Reporte a generar.');
					document.getElementById("cboTipoRepCicloEsp").focus();
					return false;
				}
				else{
						url="?opcion=" + document.getElementById("cboReportes").value +
							"&subOpcion=" + document.getElementById("cboTipoRepCicloEsp").value +
							"&sede=" + document.getElementById("cboSedeRep2").value + 
							"&fecIni=" + document.getElementById("txtFechaIniEst").value +
							"&fecFin=" + document.getElementById("txtFechaFinEst").value;
				} 
				break;
			//MOVIMIENTOS DEL DIA
			case document.getElementById("txhReporte4").value:
				if(document.getElementById("txtFechaIniMov").value==""){
					alert("Debe ingresar la fecha de Reserva inicial.");
					document.getElementById("txtFechaIniMov").focus();
					return false;
				}
				if(document.getElementById("txtFechaFinMov").value==""){
					alert("Debe ingresar la fecha de Reserva final.");
					document.getElementById("txtFechaFinMov").focus();
					return false;
				}
				if(document.getElementById("cboTipoRepMovPresDev").value == ""){
					alert('Seleccione el Tipo de Reporte a generar.');
					document.getElementById("cboTipoRepMovPresDev").focus();
					return false;
				}
				else{
				//cuando este sacar esto					
					if (document.getElementById("cboTipoRepMovPresDev").value == '0001'){						
						url="?opcion=" + document.getElementById("cboReportes").value +
							"&subOpcion=" + document.getElementById("cboTipoRepMovPresDev").value +
							"&sede=" + document.getElementById("cboSedeRep3").value + 
							"&fecIni=" + document.getElementById("txtFechaIniMov").value +
							"&fecFin=" + document.getElementById("txtFechaFinMov").value;
					}
					else
					if (document.getElementById("cboTipoRepMovPresDev").value == '0002'){
						url="?opcion=" + document.getElementById("cboReportes").value +
							"&subOpcion=" + document.getElementById("cboTipoRepMovPresDev").value +
							"&sede=" + document.getElementById("cboSedeRep3").value + 
							"&fecIni=" + document.getElementById("txtFechaIniMov").value +
							"&fecFin=" + document.getElementById("txtFechaFinMov").value;
					}
					else return false;
				} 
				
				break;
			case document.getElementById("txhReporte5").value:								
				if(document.getElementById("cboPeriodoBuzon").value==""){
					alert('Seleccione un per�odo');
					document.getElementById("cboPeriodoBuzon").focus();
					return false;
				}				
				url="?opcion=" + document.getElementById("cboReportes").value +
					"&periodo=" + document.getElementById("cboPeriodoBuzon").value + "/" +  
								  document.getElementById("cboAnioBuzon").value + 
					"&periodoDesc=" + document.getElementById('cboPeriodoBuzon')[document.getElementById('cboPeriodoBuzon').selectedIndex].text + " " +  
								  document.getElementById("cboAnioBuzon").value +
					"&codTipoMaterial=" + document.getElementById("cboMaterial").value + 
					"&descMaterial=" + document.getElementById('cboMaterial')[document.getElementById('cboMaterial').selectedIndex].text; 
				break;
			case document.getElementById("txhReporte6").value:
				//alert('Reporte Usuario Individual');
				//REPORTE USUARIO INDIVIDUAL:
				var tipo ="";
				if (fc_Trim(document.getElementById("txtCodUsuario").value)=='' && fc_Trim(document.getElementById("txtUsuario").value)=='' ) {
					alert("Ingrese el C�digo o Usuario de la Persona");
					document.getElementById("txtCodUsuario").focus();
					return false;
				}
				
				if (document.getElementById("txtCodUsuario")!=""){
					tipo = "CODIGO";
				} else {
					tipo = "USUARIO";
				}
				
				url="?opcion=" + document.getElementById("cboReportes").value +
					"&tipobusqueda=" + tipo + 
					"&codusuario=" + document.getElementById("txtCodUsuario").value +
					"&sede=" + document.getElementById("cboSedeRep4").value +
					"&username=" + document.getElementById("txtUsuario").value; 
				break;
			case document.getElementById("txhReporte7").value:				
				//REPORTE DE RESERVAS SALAS POR CICLO ESPECIALIDAD 
				if (document.getElementById("txtFechaIniEstRes").value==""){
					alert('Ingrese una fecha de inicio');
					document.getElementById("txtFechaIniEstRes").focus();
					return false;
				}
				if (document.getElementById("txtFechaFinEstRes").value==""){
					alert('Ingrese una fecha de fin');
					document.getElementById("txtFechaFinEstRes").focus()
					return false;
				}	

				url="?opcion=" + document.getElementById("cboReportes").value +
					"&subOpcion=" + 
					"&fecIni=" + document.getElementById("txtFechaIniEstRes").value +
					"&sede=" + document.getElementById("cboSedeRep5").value +
					"&fecFin=" + document.getElementById("txtFechaFinEstRes").value;
				
				break;
				
			case document.getElementById("txhReporte8").value:
				//MOVIMIENTOS DE RESERVAS DEL DIA
				if(document.getElementById("txtFechaIniMovRes").value==""){
					alert("Debe ingresar la fecha de Reserva inicial.");
					document.getElementById("txtFechaIniMovRes").focus();
					return false;
				}
				if(document.getElementById("txtFechaFinMovRes").value==""){
					alert("Debe ingresar la fecha de Reserva final.");
					document.getElementById("txtFechaFinMovRes").focus();
					return false;
				}
				
				url="?opcion=" + document.getElementById("cboReportes").value +
					"&subOpcion=" + 
					"&fecIni=" + document.getElementById("txtFechaIniMovRes").value +
					"&sede=" + document.getElementById("cboSedeRep6").value +
					"&fecFin=" + document.getElementById("txtFechaFinMovRes").value;
															
				break;				
		}
		window.open("${ctx}/biblioteca/bib_reporte.html"+url,"Reportes","resizable=yes, menubar=yes");
		//Fc_Popup("${ctx}/biblioteca/bib_reporte.html"+url,450,160);
	}
	
	function fc_Limpiar(obj){
		switch(obj){
			case '1':
				document.getElementById("tblIframe").style.display = "none";
				document.getElementById("cboPeriodoSancion").value = "";
				document.getElementById("cboAnioSancion").value = document.getElementById("cboAnioSancion")[0].value;
				document.getElementById("cboTipoUsuarios").value = "";
				break;
				
			case '2':
				document.getElementById("txtFechaIniMov").value = "";
				document.getElementById("txtFechaFinMov").value = "";
				document.getElementById("cboTipoRepMovPresDev").value = "";
				break;
			
			//modificacion 24/04/2008 RNapa
			case '3':				
				document.getElementById("cboTipoRepCicloEsp").value = "";
				document.getElementById("txtFechaIniEst").value = "";
				document.getElementById("txtFechaFinEst").value = "";				
				break;
			
			case '4':				
				document.getElementById("cboMaterial").value = "";
				document.getElementById("cboPeriodoBuzon").value = "";				
				document.getElementById("cboAnioBuzon").value = document.getElementById("cboAnioBuzon")[0].value;				
				break;
				
			case '5':								
				//document.getElementById("cbotipoRepMatBib").value = "";
				document.getElementById("txtFechaIni").value = "";
				document.getElementById("txtFechaFin").value = "";
				document.getElementById("txtTipoCambio").value = "";
				document.getElementById("txtNroRegistros").value = "";				
				break;
			case '6':
				document.getElementById("txtCodUsuario").value = "";
				document.getElementById("txtUsuario").value = "";							
				break;
			case '7':				
				document.getElementById("txtFechaIniEstRes").value = "";
				document.getElementById("txtFechaFinEstRes").value = "";				
				break;
			case '8':
				document.getElementById("txtFechaIniMovRes").value = "";
				document.getElementById("txtFechaFinMovRes").value = "";
				//document.getElementById("cboTipoRepMovReserDev").value = "";
				break;				
		}
	}
	
	function fc_Buscar(){
		if (document.getElementById("txtFechaIniUsuDeu").value=="" || document.getElementById("txtFechaFinUsuDeu").value==""){ //(document.getElementById("cboPeriodoSancion").value == ""){
			alert('Seleccione un per�odo');
			//document.getElementById("cboPeriodoSancion").focus();
			document.getElementById("txtFechaIniUsuDeu").focus();
			return false;
		}
		else{			
			 document.getElementById("iFrame").src = 
				//window.location.href=
					"${ctx}/biblioteca/bib_reporte_usuario_sancion_iframe.html?"+
					"subOpcion=" + document.getElementById("codTipoResultado").value +
					"&tipousuario=" + document.getElementById("cboTipoUsuarios").value +
					"&sede=" + document.getElementById("cboSedeRep").value + 
					"&periodo1=" + document.getElementById("txtFechaIniUsuDeu").value +
					"&periodo2=" + document.getElementById("txtFechaFinUsuDeu").value;
					//"&periodo=" + document.getElementById("cboPeriodoSancion").value + "/" +  
						//		  document.getElementById("cboAnioSancion").value;
			document.getElementById("tblIframe").style.display='inline-block';
		}
	}
	
	
</script>
</head>
<body> 
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_consulta_reportes.html">

	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	
	<form:hidden path="txhReporte1" id="txhReporte1" />
	<form:hidden path="txhReporte2" id="txhReporte2" />
	<form:hidden path="txhReporte3" id="txhReporte3" />
	<form:hidden path="txhReporte4" id="txhReporte4" />
	<form:hidden path="txhReporte5" id="txhReporte5" />
	<form:hidden path="txhReporte6" id="txhReporte6" />
	<form:hidden path="txhReporte7" id="txhReporte7" />
	<form:hidden path="txhReporte8" id="txhReporte8" />
	
	<form:hidden path="txhRep2A" id="txhRep2A" />
	<form:hidden path="txhRep2B" id="txhRep2B" />
	<form:hidden path="txhRep2C" id="txhRep2C" />
	<form:hidden path="txhRep2D" id="txhRep2D" />
	<form:hidden path="txhRep2E" id="txhRep2E" />
	<form:hidden path="txhRep2F" id="txhRep2F" />
	<form:hidden path="txhRep2G" id="txhRep2G" />
	<form:hidden path="sedeSel" id="txhSedeSel"/>
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align="right">&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo"><font style="">Consultas y Reportes</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<!-- table><tr height="3px"><td></td></tr></table-->
	<table border="0" cellpadding="2" cellspacing="2" height="60px" background="${ctx}/images/biblioteca/fondosup.jpg" 
		style="margin-left:11px;" class="tabla" style="width:90%">	 
		<tr>
			<td nowrap="nowrap">&nbsp;Reportes :&nbsp;&nbsp;&nbsp;
				<form:select path="cboReportes" id="cboReportes" cssClass="cajatexto_o" 
								cssStyle="width:350px;" onchange="javascript:fc_CambiaReporte();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaReportes!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.listaReportes}" />
					</c:if>
				</form:select>
			</td>
		</tr>
	</table>
	<table><tr><td height="5px"></td></tr></table>
	<!-- TABLA CONSULTA DE USUARIOS DEUDORES Y SANCIONADOS -->
	<table id="tablaDeuSan" style="display: none; margin-left:11px; width:100%" border="0" cellspacing="2" cellpadding="2" height="60px" class="tabla"
		background="${ctx}/images/biblioteca/fondoinf.jpg">	 
		<tr>
			<td width="5%">&nbsp;Per�odo :</td>
			<td width="25%">
				<!-- &nbsp;<form:select path="cboPeriodoSancion" id="cboPeriodoSancion" cssClass="cajatexto_o" 
			  			cssStyle="width:150px">
						<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.listaMeses!=null}">
								<form:options itemValue="id" itemLabel="name" 
									items="${control.listaMeses}" />
							</c:if>
					</form:select>&nbsp;&nbsp;
					<form:select path="cboAnioSancion" id="cboAnioSancion" cssClass="cajatexto_o" 
						cssStyle="width:55px">	
						<c:if test="${control.listaAnios!=null}">
							<form:options itemValue="id" itemLabel="name" 
								items="${control.listaAnios}" />
						</c:if>
					</form:select>
				-->

				&nbsp;<input type="text" id="txtFechaIniUsuDeu" name="txtFechaIniUsuDeu" class="cajatexto" 
					style="width:70px" onkeypress="javascript:fc_ValidaFecha(this.id);" 
					onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendarUsuDeu','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendarUsuDeu" 
						name="imgCalendarUsuDeu" alt="Calendario" style="CURSOR:" align="middle"></a>
				&nbsp;-&nbsp;
				<input type="text" id="txtFechaFinUsuDeu" name="txtFechaFinUsuDeu" class="cajatexto" style="width:70px" 
					onkeypress="javascript:fc_ValidaFecha(this.id);" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaIniUsuDeu','txtFechaFinUsuDeu','Inicio','T�rmino',2);" 
					maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1UsuDeu','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar1UsuDeu" 
						name="imgCalendar1UsuDeu" alt="Calendario" style="CURSOR:" align="middle"></a>


			</td>
			<td width="21%">Tipo Usuario:
				&nbsp;<form:select path="cboTipoUsuarios" id="cboTipoUsuarios" cssClass="cajatexto"	cssStyle="width:100px;">
									<!-- cssStyle="width:200px;" onchange="javascript:fc_CambiaTipoRepMatBib();"> -->
						<form:option value="">--Todos--</form:option>
						<c:if test="${control.listaTipoUsuarios!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaTipoUsuarios}" />
						</c:if>
					</form:select>
			</td>
			<td width="24%">&nbsp;Tipo Resultado : 
					&nbsp;<form:select path="codTipoResultado" id="codTipoResultado" cssClass="cajatexto"	cssStyle="width:100px;">
								<!-- cssStyle="width:200px;" onchange="javascript:fc_CambiaTipoRepMatBib();"> -->
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.lstTipoResultado!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.lstTipoResultado}" />
					</c:if>
				</form:select>
			</td>
			
			<td width="16%">Sede :&nbsp;
				<form:select path="cboSedeRep" id="cboSedeRep" cssClass="combo_o" cssStyle="width:100px">
					<c:if test="${control.listaSedeRep!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion" 
						items="${control.listaSedeRep}" />
					</c:if>
				</form:select>
			</td>
						
			<td align="right" width="9%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: hand" id="imglimpiar" onclick="javascript:fc_Limpiar('1');" ></a>
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="cursor:pointer;" onclick="javascript:fc_Buscar();"></a>
				
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
				<img src="${ctx}/images/iconos/exportarex1.jpg" align="absmiddle" alt="Excel" id="icoExcel" style="cursor:hand" onclick="javascript:fc_Generar();"></a>
				&nbsp;
			</td>
		</tr>
	</table>
	<!-- REPORTE DE MATERIAL BIBLIOGRAFICO -->
	<table id="tablaMatbib" style="display: none; margin-left:11px; width:90%" border="1"  cellspacing="2" cellpadding="2" height="60px" class="tabla"
		background="${ctx}/images/biblioteca/fondoinf.jpg">	 
		<tr>
			<td width="10%">&nbsp;Tipo Resultado :</td>
			<td width="35%">
				&nbsp;<form:select path="cbotipoRepMatBib" id="cbotipoRepMatBib" cssClass="cajatexto_o" 
								cssStyle="width:200px;" onchange="javascript:fc_CambiaTipoRepMatBib();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaTipoRepMatBib!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
						items="${control.listaTipoRepMatBib}" />
					</c:if>
				</form:select>
			</td>

			<td width="10%" align="left">&nbsp; Sede :</td>
			<td width="20%" align="left">
				<form:select path="cboSedeRep" id="cboSedeRep1" cssClass="combo_o" cssStyle="width:100px">
					<c:if test="${control.listaSedeRep!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion" 
						items="${control.listaSedeRep}" />
					</c:if>
				</form:select>
			</td>

			<td align="right" width="10%" id="tdLimpiarMatBib" style="display: none;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar5','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" id="imgLimpiar5" 
						style="cursor:pointer;" onclick="javascript:fc_Limpiar('5');"></a>
			</td>
		</tr>
		<tr id="trFecMatBib" style="display:none;">
			<td width="20%">&nbsp;Per&iacute;odo :</td>
			<td width="40%">
				&nbsp;<input type="text" id="txtFechaIni" name="txtFechaIni" class="cajatexto" style="width:70px" 
							onkeypress="javascript:fc_ValidaFecha(this.id);" 
							onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar" 
						name="imgCalendar" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
				&nbsp;-&nbsp;
				<input type="text" id="txtFechaFin" name="txtFechaFin" class="cajatexto" style="width:70px" 
					onkeypress="javascript:fc_ValidaFecha(this.id);" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaIni','txtFechaFin','Inicio','T�rmino',2);" 
					maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar1" 
						name="imgCalendar1" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
			</td>
			<td>&nbsp;	
				<div style="display: none;" id="divProcedLabel">
				&nbsp;Procedencia :			
				</div>	
				
				<div style="display: none;" id="divRangoIngLabel">
				&nbsp;<input type="checkbox" name="chkTipoEtiqueta" id="chkTipoEtiqueta" >
				Nros. :			
				</div>
				
			</td>
			<td>&nbsp;
				<div style="display: none;" id="divProcedDato">				
					<form:select path="cboProcedencia" id="cboProcedencia" cssClass="combo_o" cssStyle="width:145px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstProcedencia!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.lstProcedencia}" />
						</c:if>
					</form:select>
				</div>
				
				<div style="display: none;" id="divRangoDato">
					De:&nbsp;<form:input path="nroIngresoIni" cssClass="cajatexto_1" id="nroIngresoIni" maxlength="10" size="8" />
					a:&nbsp;<form:input path="nroIngresoFin" cssClass="cajatexto_1" id="nroIngresoFin" maxlength="10" size="8" />
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr id="trTipoCambio" style="display:none;">
			<td>&nbsp;Tipo de Cambio
			</td>
			<td colspan="4">
				&nbsp;<input type="text" id="txtTipoCambio" name="txtTipoCambio" class="cajatexto_o" style="width:70px" 
						onkeypress="javascript:fc_PermiteNumerosPunto();"
						onblur="javascript:fc_ValidaDecimalSiNo(this.id,5,5);" maxlength="10">
			</td>
		</tr>
		<tr id="trNroRegistros" style="display:none;">
			<td>&nbsp;Nro. Registros a Mostrar:
			</td>
			<td colspan="4">
				&nbsp;<input type="text" id="txtNroRegistros" name="txtNroRegistros" class="cajatexto_o" style="width:70px" 
						onkeypress="javascript:fc_ValidaNumero();"
						onblur="javascript:fc_ValidaNumeroFinal(this,'Nro. Registros');fc_ValidaNumeroFinalMayorCero(this);" 
						maxlength="3">
			</td>
		</tr>
	</table>
	<!-- REPORTE ESTADISTICO POR CICLO Y ESPECIALIDAD -->
	<table id="tablaCicEsp" style="display: none;margin-left:11px; width:90%" border="0"  cellspacing="2" cellpadding="2" height="60px" class="tabla"
		background="${ctx}/images/biblioteca/fondoinf.jpg" bordercolor="red">	 
		<tr>
			<td width="10%">&nbsp;Periodo :</td>
			<td width="50%">
				&nbsp;<input type="text" id="txtFechaIniEst" name="txtFechaIniEst" class="cajatexto" 
					style="width:70px" onkeypress="javascript:fc_ValidaFecha(this.id);" 
					onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendarEst','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendarEst" 
						name="imgCalendarEst" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
				&nbsp;-&nbsp;
				<input type="text" id="txtFechaFinEst" name="txtFechaFinEst" class="cajatexto" style="width:70px" 
					onkeypress="javascript:fc_ValidaFecha(this.id);" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaIniEst','txtFechaFinEst','Inicio','T�rmino',2);" 
					maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1Est','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar1Est" 
						name="imgCalendar1Est" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
			</td>
			<td width="10%" align="left">&nbsp; Sede :</td>
			<td width="20%" align="left">
				<form:select path="cboSedeRep" id="cboSedeRep2" cssClass="combo_o" cssStyle="width:100px">
					<c:if test="${control.listaSedeRep!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion" 
						items="${control.listaSedeRep}" />
					</c:if>
				</form:select>
			</td>
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar3','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" id="imgLimpiar3" 
						style="cursor:pointer;" onclick="javascript:fc_Limpiar('3');"></a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;Tipo Resultado :</td>
			<td colspan="4">
				&nbsp;<form:select path="cboTipoRepCicloEsp" id="cboTipoRepCicloEsp" cssClass="cajatexto_o" 
								cssStyle="width:200px;">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaTipoRepCicloEsp!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoRepCicloEsp}" />
						</c:if>
				</form:select>
			</td>
		</tr>
	</table>

	<!-- REPORTE ESTADISTICO DE MOVIMIENTOS DEL DIA -->
	<table id="tablaMovDia" style="display: none;margin-left:11px; width:90%" border="0"  cellspacing="2" 
		cellpadding="2" height="60px" class="tabla" background="${ctx}/images/biblioteca/fondoinf.jpg">	 
		<tr>
			<td width="10%">&nbsp;Fecha Reserva :</td>
			<td width="50%">
				&nbsp;<input type="text" id="txtFechaIniMov" name="txtFechaIniMov" class="cajatexto_o" 
					style="width:70px" onkeypress="javascript:fc_ValidaFecha(this.id);" 
					onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendarMov','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendarMov" 
						name="imgCalendarMov" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
				&nbsp;-&nbsp;
				<input type="text" id="txtFechaFinMov" name="txtFechaFinMov" class="cajatexto_o" style="width:70px" 
					onkeypress="javascript:fc_ValidaFecha(this.id);" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaIniMov','txtFechaFinMov','Inicio','T�rmino',2);" 
					maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1Mov','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar1Mov" 
						name="imgCalendar1Mov" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
			</td>	

			<td width="10%" align="left">&nbsp; Sede :</td>
			<td width="20%" align="left">
				<form:select path="cboSedeRep" id="cboSedeRep3" cssClass="combo_o" cssStyle="width:100px">
					<c:if test="${control.listaSedeRep!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion" 
						items="${control.listaSedeRep}" />
					</c:if>
				</form:select>
			</td>
							
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar1','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" id="imgLimpiar1" 
						style="cursor:pointer;" onclick="javascript:fc_Limpiar('2');"></a>
			</td>			
		</tr>
		<tr>
			<td>&nbsp;Tipo Resultado :</td>
			<td colspan="4">
				&nbsp;<form:select path="cboTipoRepMovPresDev" id="cboTipoRepMovPresDev" cssClass="cajatexto_o" 
								cssStyle="width:320px;">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaTipoRepMovPresDev!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoRepMovPresDev}" />
						</c:if>
				</form:select>
			</td>
		</tr>
	</table>
	<!-- REPORTE ESTADISTICO DEL BUZON DE SUGERENCIAS -->
	<table id="tablaBuzSug" style="display: none;margin-left:11px; width:90%" border="0"  cellspacing="2" cellpadding="2" height="60px" class="tabla"
		background="${ctx}/images/biblioteca/fondoinf.jpg" bordercolor="blue">	 
		<tr>
			<td width="20%">&nbsp;Periodo :</td>
			<td width="80%">
				&nbsp;<form:select path="cboPeriodoBuzon" id="cboPeriodoBuzon" cssClass="cajatexto_o" 
		  			cssStyle="width:150px">
					<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaMeses!=null}">
							<form:options itemValue="id" itemLabel="name" 
								items="${control.listaMeses}" />
						</c:if>
				</form:select>&nbsp;&nbsp;
				<form:select path="cboAnioBuzon" id="cboAnioBuzon" cssClass="cajatexto_o" 
					cssStyle="width:55px">	
					<c:if test="${control.listaAnios!=null}">
						<form:options itemValue="id" itemLabel="name" 
							items="${control.listaAnios}" />
					</c:if>
				</form:select>
			</td>
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar4','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" id="imgLimpiar4" 
						style="cursor:pointer;" onclick="javascript:fc_Limpiar('4');"></a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;Tipo de Material :</td>
			<td colspan="2">
				&nbsp;<form:select path="cboMaterial" id="cboMaterial" cssClass="cajatexto" 
						cssStyle="width:350px;" onchange="javascript:fc_CambiaReporte();">
						<form:option value="">--Todos--</form:option>
						<c:if test="${control.listaMaterial!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaMaterial}" />
						</c:if>
					</form:select>
			</td>
		</tr>
	</table>
	
	<!-- REPORTE DE USUARIOS INDIVIDUALES -->
	<table id="tablaUsuInd" style="display: none;margin-left:11px; width:90%" border="0"  cellspacing="2" 
		cellpadding="2" height="40px" class="tabla" background="${ctx}/images/biblioteca/fondoinf.jpg">			
		<tr>			
			<td width="10%">&nbsp;C&oacute;digo :</td>
			<td width="15%">
				&nbsp;<input type="text" id="txtCodUsuario" name="txtCodUsuario" class="cajatexto_o" 
					style="width:70px" onkeypress="fc_PermiteNumeros();" 
					onblur="fc_ValidaNumeroOnBlur('txtCodUsuario');" maxlength="10">				
				&nbsp;	
				
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerarX','','${ctx}/images/iconos/buscara2.jpg',1)">
						<img src="${ctx}/images/iconos/buscara1.jpg" onclick="javascript:buscarAlumno();" alt="Buscar Alumno" style="cursor:hand;" align="absmiddle" id="btnGenerarX" width="20" height="17" >
					</a>
							
			</td>	
			<td width="10%">&nbsp;Usuario :</td>							
			<td align="left" width="35%">
				&nbsp;<input type="text" id="txtUsuario" name="txtUsuario" class="cajatexto_o" 
					style="width:140px"  maxlength="20">&nbsp;&nbsp;&nbsp;	<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar6','','${ctx}/images/iconos/limpiar2.jpg',1)"><img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" id="imgLimpiar6" 
						style="cursor:pointer;" onclick="javascript:fc_Limpiar('6');"></a>			  
			</td>
			<td width="10%" align="left">&nbsp; Sede :</td>
			<td width="20%" align="left">
				<form:select path="cboSedeRep" id="cboSedeRep4" cssClass="combo_o" cssStyle="width:100px">
					<c:if test="${control.listaSedeRep!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion" 
						items="${control.listaSedeRep}" />
					</c:if>
				</form:select>
			</td>			
		</tr>		
	</table>	

	<!-- REPORTE DE RESERVAS POR CICLO Y ESPECIALIDAD -->
	<table id="tablaCicEspReservas" style="display: none;margin-left:11px; width:90%" border="0"  cellspacing="2" cellpadding="2" height="60px" class="tabla"
		background="${ctx}/images/biblioteca/fondoinf.jpg" >	 
		<tr>
			<td width="10%">&nbsp;Periodo :</td>
			<td width="50%">
				&nbsp;<input type="text" id="txtFechaIniEstRes" name="txtFechaIniEstRes" class="cajatexto" 
					style="width:70px" onkeypress="javascript:fc_ValidaFecha(this.id);" 
					onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendarEst','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendarEstRes" 
						name="imgCalendarEst" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
				&nbsp;-&nbsp;
				<input type="text" id="txtFechaFinEstRes" name="txtFechaFinEstRes" class="cajatexto" style="width:70px" 
					onkeypress="javascript:fc_ValidaFecha(this.id);" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaIniEstRes','txtFechaFinEstRes','Inicio','T�rmino',2);" 
					maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1Est','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar1EstRes" 
						name="imgCalendar1EstRes" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
			</td>
			<td width="10%" align="left">&nbsp; Sede :</td>
			<td width="20%" align="left">
				<form:select path="cboSedeRep" id="cboSedeRep5" cssClass="combo_o" cssStyle="width:100px">
					<c:if test="${control.listaSedeRep!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion" 
						items="${control.listaSedeRep}" />
					</c:if>
				</form:select>
			</td>
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar6','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" id="imgLimpiar7" 
						style="cursor:pointer;" onclick="javascript:fc_Limpiar('7');"></a>
			</td>
		</tr>		
	</table>
	
<!-- REPORTE ESTADISTICO DE MOVIMIENTOS DE RESERVAS DE DIA -->
	<table id="tablaMovDiaReserv" style="display: none;margin-left:11px; width:90%" border="0"  cellspacing="2" 
		cellpadding="2" height="60px" class="tabla" background="${ctx}/images/biblioteca/fondoinf.jpg">	 
		<tr>
			<td width="15%">&nbsp;Fecha Reserva :</td>
			<td width="45%">
				&nbsp;<input type="text" id="txtFechaIniMovRes" name="txtFechaIniMovRes" class="cajatexto_o" 
					style="width:70px" onkeypress="javascript:fc_ValidaFecha(this.id);" 
					onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendarMovRes','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendarMovRes" 
						name="imgCalendarMovRes" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
				&nbsp;-&nbsp;
				<input type="text" id="txtFechaFinMovRes" name="txtFechaFinMovRes" class="cajatexto_o" style="width:70px" 
					onkeypress="javascript:fc_ValidaFecha(this.id);" onblur="javascript:fc_ValidaFechaOnblur(this.id);fc_ValidaRangoFechas1('txtFechaIniMovRes','txtFechaFinMovRes','Inicio','T�rmino',2);" 
					maxlength="10">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar1MovRes','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" style="cursor:pointer;" id="imgCalendar1MovRes" 
						name="imgCalendar1MovRes" alt="Calendario" style="CURSOR: pointer; vertical-align:bottom"></a>
			</td>	
			<td width="10%" align="left">&nbsp; Sede :</td>
			<td width="20%" align="left">
				<form:select path="cboSedeRep" id="cboSedeRep6" cssClass="combo_o" cssStyle="width:100px">
					<c:if test="${control.listaSedeRep!=null}">
					<form:options itemValue="dscValor1" itemLabel="descripcion" 
						items="${control.listaSedeRep}" />
					</c:if>
				</form:select>
			</td>	
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar1Res','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" id="imgLimpiar1Res" 
						style="cursor:pointer;" onclick="javascript:fc_Limpiar('8');"></a>
			</td>			
		</tr>
		<!-- tr>
			<td>&nbsp;Tipo Resultado :</td>
			<td colspan="2">
				&nbsp;<form:select path="cboTipoRepMovReserDev" id="cboTipoRepMovReserDev" cssClass="cajatexto_o" 
								cssStyle="width:320px;">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listaTipoRepMovReserDev!=null}">											  
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaTipoRepMovReserDev}" />
						</c:if>
				</form:select>
			</td>
		</tr-->
	</table>


	<br>
	
	<table id="tablaBoton" style="display: none; margin-left:11px; width:90%" cellpadding="0" cellspacing="0" align="center">
		<tr><td height="10px"></td></tr>
		<tr>
			<td align="center">
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnGenerar','','${ctx}/images/botones/generar2.jpg',1)">
					<img src="${ctx}/images/botones/generar1.jpg" onclick="javascript:fc_Generar();" alt="Generar Excel" style="cursor:hand;" id="btnGenerar"></a>
			</td>
		</tr>
	</table>
	
	<table><tr><td height="5px"></td></tr></table>
	<table cellpadding="0" cellspacing="0" id="tblIframe" class="tabla" border="0" style="display: none;margin-left:11px; width:100%">
		<tr><td><iframe id="iFrame" name="iFrame" frameborder="0" height="300px" width="100%"></iframe>
		</td></tr>
	</table>
</form:form>

	<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txtFechaIni",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaFin",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaIniMov",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendarMov",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaFinMov",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1Mov",
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "txtFechaIniMovRes",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendarMovRes",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaFinMovRes",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1MovRes",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaIniEst",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendarEst",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaFinEst",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1Est",
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "txtFechaIniEstRes",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendarEstRes",
			singleClick    :    true
		});
		
		Calendar.setup({
			inputField     :    "txtFechaFinEstRes",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1EstRes",
			singleClick    :    true
		});

		Calendar.setup({
			inputField     :    "txtFechaIniUsuDeu",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendarUsuDeu",
			singleClick    :    true
		});	

		Calendar.setup({
			inputField     :    "txtFechaFinUsuDeu",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgCalendar1UsuDeu",
			singleClick    :    true
		});	
	</script>
	


</body>