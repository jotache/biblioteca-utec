<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	FONT-WEIGHT: bold;
}

.texto {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: hand;
}

.texto_grilla {
	FONT-SIZE: 9px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: left;
}

.cabecera_grilla {
	FONT-SIZE: 10px;
	/*COLOR: #636563;*/
	COLOR: #000000;
	BACKGROUND-COLOR: #cccccc;
	FONT-FAMILY: Verdana;
	HEIGHT: 30px;
	CURSOR: arrow;
	FONT-WEIGHT: bold;
	TEXT-ALIGN: center;
	vertical-align: middle;
}

.texto_grilla_c {
	FONT-SIZE: 10px;
	*/COLOR: #636563;*/
	COLOR: #000000;
	FONT-FAMILY: Verdana;
	HEIGHT: 18px;
	CURSOR: arrow;
	TEXT-ALIGN: center;
}

.sancion {
	color: #FF0000;
	font-weight: bold;
}
.suspension {
	color: #FF8800;
	font-weight: bold;
}
</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
	<td></td>	
	<td colspan="4" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
	<td align="left">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>	
	<td colspan="4" align="center">Reporte de Usuarios Individuales</td>
	<td align="left">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
	<td></td>
	<td colspan="4" align="left"></td>
	<td></td>
</tr>
<tr class="texto">
  <td></td>
  <td align="left"><b>C&oacute;digo :</b></td>
  <td colspan="2" align="left">${model.codusuariorep}</td>
  <td></td>
  <td><strong>Sede :</strong> &nbsp;${model.dscSede}</td>
</tr>
<tr class="texto">
  <td></td>
  <td align="left"><b>Nombre :</b></td>
  <td colspan="2" align="left">${model.nombreusuariorep}</td>
  <td></td>
  <td></td>
</tr>

<tr class="texto">
	<td></td>
	<td align="left"><strong>Situaci&oacute;n actual: </strong></td>
	<td colspan="2" align="left">
		<c:if test='${(model.fechafinsancion==null || model.fechafinsancion=="" || model.fechafinsuspension==null || model.fechafinsuspension=="")}'>
			<b>No est&aacute; sancionado ni suspendido</b>
		</c:if>			
	</td>
	<td></td>
	<td></td>
</tr>

<c:if test='${model.fechafinsancion!=""}'>
<tr class="texto">
	<td></td>
	<td colspan="3" align="left">		
		<span class="sancion">Sancionado hasta el <c:out value="${model.fechafinsancion}"/></span>
	</td>
	<td></td>
	<td></td>
</tr>
</c:if>

<c:if test='${model.fechafinsuspension!=""}'>
<tr class="texto">
	<td></td>
	<td colspan="3" align="left">		
		<span class="suspension">Suspendido hasta el <c:out value="${model.fechafinsuspension}"/></span>
	</td>
	<td></td>
	<td></td>
</tr>
</c:if>

<tr class="texto">
	<td></td>	
	<td align="left" rowspan="2">&nbsp;</td>
	<td colspan="3" align="left" style="vertical-align: middle;" rowspan="2">		

		
	</td>
	<td></td>
</tr>
</table>
<br>
<table>
	<tr>
		<td></td>
		<td>
			<table border="1">
			<tr>
				<td class="cabecera_grilla" width="10%" nowrap="nowrap">C&oacute;digo</td>
				<td class="cabecera_grilla" width="40%" nowrap="nowrap">T&iacute;tulo</td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Nro. Ejemplar </td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Autor</td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Pie</td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Fec. Pr&eacute;stamo</td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Fec. Dev. Prog.</td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Fec. Dev. Real </td>
				<!--td class="cabecera_grilla" width="25%" nowrap="nowrap">Fin de Sanci&oacute;n</td-->
				
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Sanci&oacute;n</td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Id Sanci&oacute;n</td>
				<td class="cabecera_grilla" width="25%" nowrap="nowrap">Estado</td>
			</tr>
			<%  
				if(request.getSession().getAttribute("consulta")!=null){
					String nroIngresos = "";
					
					List consulta = (List) request.getSession().getAttribute("consulta");

					for(int i=0;i<consulta.size();i++){
						
						Reporte obj  = (Reporte) consulta.get(i);
						
						%>
							<tr class="texto_grilla" style="vertical-align: middle;">
								<td style="text-align:center;">&nbsp;<%=obj.getCodigo()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getTituloMaterial()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getTotalEjemplares()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getAutor()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getDetalle()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getFecPrestamo()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getFechaPresDev()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getFechaDevReal()%></td>
								<!--td style="text-align:left;">&nbsp;<%=obj.getFechaFinSancion()%></td--> <!-- jhpr 2008-08-27 -->
								
								<td style="text-align:left;">&nbsp;<%=obj.getSancion()%></td>
								<td style="text-align:left;">&nbsp;<%=obj.getIdSancion()%></td>
								
								<td style="text-align:left;">&nbsp;<%=obj.getEstado()%></td>
							</tr>
						<%
					}
				}
				request.removeAttribute("consulta"); 
			%>
			</table>
		</td>
		<td></td>
	</tr>
</table>
</body>
</html>