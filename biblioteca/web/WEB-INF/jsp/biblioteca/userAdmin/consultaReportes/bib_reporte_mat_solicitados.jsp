<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.modelo.Reporte'%>
<%@page contentType="application/vnd.ms-excel"%>
<%@ page language="java" errorPage="/error.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springmodules.org/tags/commons-validator" prefix="html" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>

<style>
.texto_bold {
        FONT-SIZE: 10px;
        /*COLOR: #636563;*/
        COLOR: #000000;
        FONT-FAMILY: Verdana;
        FONT-WEIGHT: bold;
}

.texto {
        FONT-SIZE: 10px;
        /*COLOR: #636563;*/
        COLOR: #000000;
        FONT-FAMILY: Verdana;
        HEIGHT: 18px;
        CURSOR: hand;
}

.texto_grilla {
        FONT-SIZE: 9px;
        /*COLOR: #636563;*/
        COLOR: #000000;
        FONT-FAMILY: Verdana;
        HEIGHT: 18px;
        CURSOR: arrow;
        TEXT-ALIGN: left;
}

.cabecera_grilla {
        FONT-SIZE: 10px;
        /*COLOR: #636563;*/
        COLOR: #000000;
        BACKGROUND-COLOR: #cccccc;
        FONT-FAMILY: Verdana;
        HEIGHT: 30px;
        CURSOR: arrow;
        FONT-WEIGHT: bold;
        TEXT-ALIGN: center;
        vertical-align: middle;
}

.texto_grilla_c {
        FONT-SIZE: 10px;
        */COLOR: #636563;*/
        COLOR: #000000;
        FONT-FAMILY: Verdana;
        HEIGHT: 18px;
        CURSOR: arrow;
        TEXT-ALIGN: center;
}

.detalle_numero {
        FONT-SIZE: 9px;
        /*COLOR: #636563;*/
        COLOR: #000000;
        FONT-FAMILY: Verdana;
        HEIGHT: 18px;
        CURSOR: arrow;
        TEXT-ALIGN: left;
        mso-style-parent:style0;
        mso-number-format:Standard;
}

</style>

<body topmargin="0" leftmargin="10" rightmargin="0" >
<table>
<tr class="texto_bold">
        <td></td>
        <td></td>
        <td colspan="6" align="center"><u>SGA - SISTEMA CEDITEC</u></td>
        <td align="left" colspan="2">Fecha : ${model.fechaRep}</td>
</tr>
<tr class="texto_bold">
        <td></td>
        <td></td>
        <td colspan="6" align="center">Reporte Material Bibliográfico Solicitado</td>
        <td align="left" colspan="2">Hora : ${model.horaRep}</td>
</tr>
<tr class="texto_bold">
        <td></td>
        <td colspan="6" align="left"></td>
        <td></td>
</tr>
<tr class="texto">
        <td></td>
        <td><b>Periodo :</b></td>
        <td colspan="6" align="left">${model.fecIni} -  ${model.fecFin}</td>
        <td></td>
</tr>
<tr class="texto">
        <td></td>
        <td><b>Sede :</b></td>
        <td colspan="6" align="left">${model.dscSede}</td>
        <td></td>
</tr>
<tr class="texto_bold">
        <td></td>
        <td colspan="6" align="left"></td>
        <td></td>
</tr>
</table>
<br>
<table>
        <tr>
                <td></td>
                <td>
                        <table border="1">
                        <tr rowspan="3">
                                <td class="cabecera_grilla" width="10%" nowrap="nowrap">Nro.</td>
                                <td class="cabecera_grilla" width="15%" nowrap="nowrap">Código</td>
                                <td class="cabecera_grilla" width="30%" nowrap="nowrap">Tipo Material</td>
                                <td class="cabecera_grilla" width="35%" nowrap="nowrap">Título</td>
                                <td class="cabecera_grilla" width="15%" nowrap="nowrap">Nro.<br>Ingresos</td>
                                <td class="cabecera_grilla" width="15%" nowrap="nowrap">Nro.<br>Ejemplares</td>
                                <td class="cabecera_grilla" width="15%" nowrap="nowrap">Nro.<br>Pr&eacute;stamos<br>Tecsup</td>
                                <td class="cabecera_grilla" width="15%" nowrap="nowrap">Nro.<br>Pr&eacute;stamos<br>Utec</td>
                                <td class="cabecera_grilla" width="15%" nowrap="nowrap">Nro.<br>Pr&eacute;stamos<br>Total</td>
                                <td class="cabecera_grilla" width="25%" nowrap="nowrap">Idioma</td>
                                <td class="cabecera_grilla" width="25%" nowrap="nowrap">Dewey</td>
                                <td class="cabecera_grilla" width="25%" nowrap="nowrap">Autor</td>
                                <td class="cabecera_grilla" width="25%" nowrap="nowrap">Año Publicación</td>
                                <td class="cabecera_grilla" width="25%" nowrap="nowrap">Editorial</td>
                            <!--<td class="cabecera_grilla" width="25%" nowrap="nowrap">Cod</td>-->
                        </tr>

                        <%
                                if(request.getSession().getAttribute("consulta")!=null){

                                        List consulta = (List) request.getSession().getAttribute("consulta");

                                        for(int i=0;i<consulta.size();i++){

                                                Reporte obj  = (Reporte) consulta.get(i);

                                                %>
                                                        <tr class="texto_grilla" style="vertical-align: middle;">
                                                                <td style="text-align:center;"><%=i+1%></td>
                                                                <td style="text-align:left;">&nbsp;<%=obj.getCodigo()==null?"":obj.getCodigo()%></td>
                                                                <td style="text-align:left;"><%=obj.getTipoMaterial()==null?"":obj.getTipoMaterial()%></td>
                                                                <td style="text-align:left; vertical-align: text-top;" valign="top" class="texto_grilla" style="height: 25px;"><%=obj.getTituloMaterial()==null?"":obj.getTituloMaterial()%></td>
                                                                <td style="text-align:left; vertical-align: text-top;" valign="top" class="texto_grilla"><%=obj.getNroIngresos()==null?"":obj.getNroIngresos()%></td>
                                                                <td style="text-align:right;"><%=obj.getTotalEjemplares()==null?"":obj.getTotalEjemplares()%></td>
                                                                <td style="text-align:right;"><%=obj.getNroPrestamosTecsup()==null?"":obj.getNroPrestamosTecsup()%></td>
                                                                <td style="text-align:right;"><%=obj.getNroPrestamosUtec()==null?"":obj.getNroPrestamosUtec()%></td>
                                                                <td style="text-align:right;"><%=obj.getNroPrestamosTotal()==null?"":obj.getNroPrestamosTotal()%></td>
                                                                <td style="text-align:left;"><%=obj.getIdioma()==null?"":obj.getIdioma()%></td>
                                                                <td style="text-align:left;"><%=obj.getNomDewey()==null?"":obj.getNomDewey()%></td>
                                                                <td style="text-align:left;"><%=obj.getAutor()==null?"":obj.getAutor()%></td>
                                                                <td style="text-align:left;"><%=obj.getAñoPublicacion()==null?"":obj.getAñoPublicacion()%></td>
                                                                <td style="text-align:left;"><%=obj.getNombreEditorial()==null?"":obj.getNombreEditorial()%></td>
                                                            <!--<td style="text-align:left;"><%=obj.getCodigoMaterial()==null?"":obj.getCodigoMaterial()%></td>-->
                                                        </tr>
                                                <%
                                        }
                                }
                                request.removeAttribute("consulta");
                        %>
                        </table>
                </td>
                <td></td>
        </tr>
</table>

</body>
</html>