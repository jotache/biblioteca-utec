<%@page import="com.tecsup.SGA.bean.UsuarioSeguridad"%>
<%@ include file="/taglibs.jsp"%>
<head>
<script src="${ctx}/scripts/js/funciones_bib-numeros.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_bib-textos.js" type="text/JavaScript"></script>
<%-- <script src="${ctx}/scripts/js/prototype-1.6.0.3.js" type="text/JavaScript"></script> --%>

<%
UsuarioSeguridad userLogin = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
// out.println(userLogin);

String opciones = "";
String tipoUser = "";
if(userLogin!=null){
    opciones = userLogin.getOpcionesApoyo();
    tipoUser = userLogin.getTipoUsuario();
}
// out.println("<br/>opciones:"+opciones.indexOf(""));
// out.println("<br/>tipoUser:"+tipoUser);
%>

<script type="text/javascript">
function onLoad(){
    var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
    if(mensaje!=""){
        alert(mensaje);
        document.getElementById("txhIndIsbn").value='';
    }
    fc_iniciaParametros();

    Fc_VerificaMaterial();

    //verificar mensaje...
    var mens_isbn="<%=(( request.getAttribute("misbn")==null)?"": request.getAttribute("misbn"))%>";
    if (mens_isbn!="") {
        mens_isbn = mens_isbn + '\n Desea Continuar con la grabaci�n...';
        if (confirm(mens_isbn)){
            document.getElementById("txhIndIsbn").value='1';
            fc_guardar('0');
        }else{
            document.getElementById("txhIndIsbn").value='';
        }
    }

}

function Fc_VerificaMaterial(){
    if("00002"==trim(document.getElementById("cboTipoMaterial").value)){
        titulo.innerText=" ISSN:";
        //document.getElementById("trFrecuencia").style.display = "";
    }else{
        titulo.innerText=" ISBN:";
        //document.getElementById("txtFrecuencia").value="";
        //document.getElementById("trFrecuencia").style.display = "none";
    }
}

function fc_concatena(){
    strFinal = '';
//     str1 = $('txtPrefijo').value;
    str1 = $('#txtPrefijo').val();
//     str2 = $('txhCodGeneradoDewey').value;
    str2 = $('#txhCodGeneradoDewey').val();
//     str3 = $('txtSufijo').value;
    str3 = $('#txtSufijo').val();

    if(fc_Trim(str1)!="" )
        strFinal = strFinal+str1+"/";
    if(fc_Trim(str2)!="" )
        strFinal = strFinal+str2+"/";
    if(fc_Trim(str3)!="" )
        strFinal = strFinal+str3+"/";

//     $("txtIdDewey").value = ''+strFinal.substring(0, (strFinal.length-1) );
    $("#txtIdDewey").val(''+strFinal.substring(0, (strFinal.length-1)));

//     if($('txtSufijo').value!=''){
//         $('operacion').value='verificaCategoria';
//         $('frmMain').submit();
//     }

    if($('#txtSufijo').val()!=''){
        $('#operacion').val('verificaCategoria');
        $('#frmMain').submit();
    }
}

function fc_Regresar(){
    strParametrosBusqueda = "&prmTxtCodigo="+document.getElementById("prmTxtCodigo").value+
    "&prmTxtNroIngreso="+document.getElementById("prmTxtNroIngreso").value+
    "&prmCboTipoMaterial="+document.getElementById("prmCboTipoMaterial").value+
    "&prmCboBuscarPor="+document.getElementById("prmCboBuscarPor").value+
    "&prmTxtTitulo="+document.getElementById("prmTxtTitulo").value+
    "&prmCboIdioma="+document.getElementById("prmCboIdioma").value+
    "&prmCboAnioIni="+document.getElementById("prmCboAnioIni").value+
    "&prmCboAnioFin="+document.getElementById("prmCboAnioFin").value+
    "&prmTxtFechaReservaIni="+document.getElementById("prmTxtFechaReservaIni").value+
    "&prmTxtFechaReservaFin="+document.getElementById("prmTxtFechaReservaFin").value;

    location.href="${ctx}/biblioteca/biblio_admin_gestion_material.html?"+
    "irRegresar=true&txhCodUsuario="+document.getElementById("usuario").value+
    strParametrosBusqueda+"&txhCodSede="+document.getElementById("txhSedeSel").value+
    "&txhSedeUsuario="+document.getElementById("txhSedeUsuario").value;

}

function fc_menu(){
    location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;
}

function validaTexto(id,tam){
    if(document.getElementById(""+id).value.length > Number(tam) ){
        str = document.getElementById(""+id).value.substring(0,tam);
        document.getElementById(""+id).value=str;
        return false;
    }
}

function fc_validaMaterialRegistrado(){
    if(fc_Trim(document.getElementById("txhCodUnico").value)!="")
    return 1;
    else
    return 0;
}

function fc_CambiarOpcion(param){
 if(fc_validaMaterial()){
    if(fc_validaMaterialRegistrado()){
        switch(param){
            case '1':
                TablaA.style.display="none";
                TablaB.style.display="";
                TablaC.style.display="none";
                TablaD.style.display="none";
                break;
            case '2':
                TablaA.style.display="none";
                TablaB.style.display="none";
                TablaC.style.display="";
                TablaD.style.display="none";
                break;

            case '3':
                TablaA.style.display="none";
                TablaB.style.display="none";
                TablaC.style.display="none";
                TablaD.style.display="";
                break;
    }
    }
    else{
        alert('No se puede realizar la acci�n.');
    }
}
}
function fc_RegresarBandeja(){
    if(TablaA.style.display==""){
        fc_Regresar();
    }
    else{
        TablaA.style.display="";
        TablaB.style.display="none";
        TablaC.style.display="none";
        TablaD.style.display="none";
    }
}

function fc_iniciaParametros(){
    document.getElementById("operacion").value="";
}

function fc_openModificarIngreso(){

    url = "${ctx}/biblioteca/biblio_admin_registrar_material_registrar_ingreso.html?"+
    "flgModificarIngreso=true"+
    "&txhCodUnico="+document.getElementById("txhCodUnico").value;

    Fc_Popup(url,950,250);

}

function fc_seleccionaDescriptor(codUnico){

}

function Fc_Upload(){
        url = "${ctx}/biblioteca/biblio_admin_registrar_material_adjuntar_documentos.html?"+
        "txhCodUnico="+document.getElementById("txhCodUnico").value;
        Fc_Popup(url,430,180);
}

function fc_openDewey(){
    usuario = document.getElementById("usuario").value;
    Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_material_dewey.html?txhCodUsuario="+usuario,600,400);
}

function fc_openAutor(){
    usuario = document.getElementById("usuario").value;
    Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_material_autor.html?txhCodUsuario="+usuario,600,400);
}

function Fc_AdjuntarArchivo(){
    if(fc_validaMaterialRegistrado()){
        url = "${ctx}/biblioteca/biblio_adjuntar_archivo_material.html?"+
        "txhCodUnico="+document.getElementById("txhCodUnico").value+
        "&txhCodUsuario="+document.getElementById("usuario").value;
        Fc_Popup(url,450,160);
    }else{
        alert('No se puede realizar la acci�n.');
    }
}

function Fc_EliminarArchivo(){
    if(!window.confirm('�Realmente desea eliminar el archivo?')) return false;
    if(fc_validaMaterialRegistrado()){

//         $('imgEliminar').hide();
//         $('img_material').hide();
//         $('nomImagen').setValue('0');

        $('#imgEliminar').hide();
        $('#img_material').hide();
        $('#nomImagen').val('0');

    }else{
        alert('No se puede realizar la acci�n.');
    }
}
function Fc_ActualizarPadre(){
    fc_enableDropDowns();
    document.getElementById("operacion").value="irActualizaImg";
    document.getElementById("frmMain").submit();
}

function fc_enableDropDowns(){
    $('#cboTipoMaterial').prop('disabled', false);
    $('#cboIdioma').prop('disabled',false);
    $('#txtPrefijo').prop('disabled',false);
    $('#txtSufijo').prop('disabled',false);
    $('#cboPais').prop('disabled',false);
    $('#cboCiudad').prop('disabled',false);
    $('#cboEditorial').prop('disabled',false);
    $('#cboTipoVideo').prop('disabled',false);
    $('#chkTipoPrestamoDomicilio').prop('disabled',false);
    $('#chkTipoPrestamoSala').prop('disabled',false);
    $('#Si').prop('disabled',false);
    $('#No').prop('disabled',false);
}

function fc_guardar(preguntar){
    form = document.forms[0];
    if(fc_validaMaterial())
    {
        if (preguntar=='1'){
            if(confirm(mstrSeguroGrabar)){
                form.imgGrabar.disabled=true;
                if(fc_Trim(document.getElementById("txhCodUnico").value)!="")
                    document.getElementById("operacion").value="irActualizar";
                else
                    document.getElementById("operacion").value="irRegistrar";

                fc_enableDropDowns();
                document.getElementById("frmMain").submit();
            }else{
                form.imgGrabar.disabled=false;
            }
        }else{
            document.getElementById("txhIndIsbn").value=='1'; //para que en el controller evitar preguntar por el ISBN duplicado
            if(fc_Trim(document.getElementById("txhCodUnico").value)!="")
                document.getElementById("operacion").value="irActualizar";
            else
                document.getElementById("operacion").value="irRegistrar";

            fc_enableDropDowns();
            document.getElementById("frmMain").submit();
        }

    }
}

function fc_validaMaterial(){
    if(document.getElementById("cboTipoMaterial").value=="")
    {    alert('Seleccione un tipo de Material.');
        document.getElementById("cboTipoMaterial").focus();
        return 0;
    }else if(fc_Trim(document.getElementById("txtTitulo").value)==""){
        alert('Debe ingresar un t�tulo.');
        document.getElementById("txtTitulo").focus();
        return 0;
    }else if(fc_Trim(document.getElementById("txhCodigoAutor").value)==""){
        alert('Seleccione autor.');
        return 0;
    }else if(fc_Trim(document.getElementById("txhCodigoDewey").value)==""){
        alert('Seleccione Dewey.');
        return 0;
    }else if(fc_Trim(document.getElementById("txtSufijo").value)==""){
        alert('Debe ingresar el valor correspondiente en el campo ' + "Sufijo");
        document.getElementById("txtSufijo").focus();
        return 0;
    }else if(fc_Trim(document.getElementById("cboEditorial").value)==""){
        alert('Debe ingresar el valor correspondiente en el campo '+"Editorial");
        document.getElementById("cboEditorial").focus();
        return 0;
    }else if(fc_Trim(document.getElementById("cboPais").value)==""){
        alert('Debe ingresar el valor correspondiente en el campo ' + "Pais");
        document.getElementById("cboPais").focus();
        return 0;
    }else if(fc_Trim(document.getElementById("cboCiudad").value)==""){
        alert('Debe ingresar el valor correspondiente en el campo ' + "Ciudad");
        document.getElementById("cboCiudad").focus();
        return 0;
    }
    else if(fc_Trim(document.getElementById("txtFecPublicacion").value)==""){
        alert('Debe ingresar un a�o de publicaci�n.');
        document.getElementById("txtFecPublicacion").focus();
        return 0;
    }else if(fc_Trim(document.getElementById("cboIdioma").value)==""){
        alert('Seleccione un Idioma.');
        document.getElementById("cboIdioma").focus();
        return 0;
    }else if(fc_Trim(document.getElementById("cboSede").value)==""){
        alert('Debe seleccionar una Sede.');
        document.getElementById("cboSede").focus();
        return 0;
    }

    //LIBROS Y REVISTAS ES OBLIGATORIO EL ISBN

    if( fc_Trim(document.getElementById("cboTipoMaterial").value)=="0003"
        || fc_Trim(document.getElementById("cboTipoMaterial").value)=="0026" ){

    }

    if(fc_Trim(document.getElementById("txhCodUnico").value)=="")
    {
        if(fc_Trim(document.getElementById("cboProcedencia").value)==""){
        alert('Seleccione procedencia.');
        document.getElementById("cboProcedencia").focus();
        return 0;
        }else if(fc_Trim(document.getElementById("cboPrecio").value)==""){
        alert('Seleccione moneda.');
        document.getElementById("cboPrecio").focus();
        return 0;
        }else if(fc_Trim(document.getElementById("txtPrecio").value)==""){
        alert('Debe ingresar cantidad.');
        document.getElementById("txtPrecio").focus();
        return 0;
        }
    }

    if(document.getElementById("chkTipoPrestamoSala").checked==false && document.getElementById("chkTipoPrestamoDomicilio").checked==false ){
        alert('Seleccione tipo de pr�stamo.');
        document.getElementById("chkTipoPrestamoSala").focus();
        return 0;
    }else if(document.getElementById("Si").checked==false && document.getElementById("No").checked==false){
        alert('Seleccione aplica reserva.');
        document.getElementById("Si").focus();
        return 0;
    }

    return 1;

}

function fc_CambiaPais(){
        document.getElementById("operacion").value="irCambiaPais";
        document.getElementById("frmMain").submit();
}

function Fc_Cambia(){
    if(cboTipoApl.value=='02'){location.href="Configuracion_Bandeja_TS.htm";}
}


function Fc_CambiaTipoMat()
{
    if(cboTipoMat.value=='01')//Libro
    {
        dewey.style.display="";
    }
    else
    {
        dewey.style.display="none";
    }
}
function Fc_CambiaPagina()
{
    TablaA.style.display="none";
    TablaB.style.display="";
}

function Fc_CambiaDescriptores()
{
    TablaA.style.display="none";
    TablaB.style.display="none";
    TablaC.style.display="";
}

function Fc_CambiaDocumentos()
{
    TablaA.style.display="none";
    TablaB.style.display="none";
    TablaC.style.display="none";
    TablaD.style.display="";
}

 function fc_PermiteNumerosPunto() {

    var valido = "0123456789.() ";
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){
            var intEncontrado = 0;
            //convierte la � en �
            window.event.keyCode = 209;
      }
      else{
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);
            if (intEncontrado == -1)
            {
                 window.event.keyCode = 0;
            }
            else
            {
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }
}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_registrar_material.html">
    <form:hidden path="operacion" id="operacion" />
    <form:hidden path="nomImagen" id="nomImagen" />
    <form:hidden path="txhCodigoAutor" id="txhCodigoAutor" />
    <form:hidden path="txhCodigoDewey" id="txhCodigoDewey" />
    <form:hidden path="accion" id="accion" />
    <form:hidden path="txhCodUnico" id="txhCodUnico" />
    <form:hidden path="pathImagen" id="pathImagen" />
    <form:hidden path="usuario" id="usuario" />
    <form:hidden path="txhCodGeneradoDewey" id="txhCodGeneradoDewey" />
    <form:hidden path="sedeSel" id="txhSedeSel" />
    <form:hidden path="sedeUsuario" id="txhSedeUsuario" />
    <form:hidden path="indISBN" id="txhIndIsbn" />

        <!-- Icono Regresar -->
    <table cellpadding="0" cellspacing="0" border="0" width="97%">
        <tr>
            <td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
            <td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_menu();" style="cursor: hand" alt="Regresar">
            </td>
        </tr>
    </table>
    <!--Page Title -->

    <table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
        <tr>
            <td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
            <td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo">
            <font style="">Registrar Material Did&aacute;ctico</font></td>
            <td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
        </tr>
    </table>


    <!-- table><tr height="3px"><td></td></tr></table-->
    <div style="overflow: auto; height: 415px; width:100%;">
    <table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="1" class="tabla" style="width:97%;margin-left:11px;"
        height="125px">

        <tr>
        <td>
            <table width="93%" align="center" border="0" bordercolor="blue" cellspacing="4" cellpadding="1">

            <tr>
            <td width="10%">&nbsp;C&oacute;digo :</td>
                    <td>&nbsp;<form:input readonly="readonly" path="txtCodigo"
                        id="txtCodigo" cssClass="cajatexto_1" cssStyle="width:175px" size="30"  />
                    </td>
                    <td width="15%">&nbsp;Tipo Material :</td>
                    <td colspan="2">&nbsp;
                        <form:select path="cboTipoMaterial"
                            id="cboTipoMaterial" cssClass="combo_o" cssStyle="width:200px" onchange="Fc_VerificaMaterial()"
                            >
                            <form:option value="">--Seleccione--</form:option>
                            <c:if test="${control.lstTipoMaterial!=null}">
                                <form:options itemValue="codTipoTablaDetalle"
                                    itemLabel="descripcion" items="${control.lstTipoMaterial}" />
                            </c:if>
                        </form:select>

                    </td>
                    <td width="15%">&nbsp;Total Ejemplares :</td>
                    <td>&nbsp;<form:input path="txtTotalVolumen" id="txtTotalVolumen"
                        onblur="fc_ValidaNumeroOnBlur('txtTotalVolumen');"
                        onkeypress="fc_PermiteNumeros();" maxlength="4" readonly="readonly" cssClass="cajatexto_1"
                        cssStyle="text-align:center;" size="4"/></td>

            </tr>
                <tr>
                    <td  id="titulo" >&nbsp;ISBN :</td>
                    <td>&nbsp;<form:input path="txtIsbn" id="txtIsbn"  maxlength="50"
                    onblur="fc_ValidaNumeroOnBlur('txtIsbn');"
                        onkeypress="fc_PermiteNumeros();"   cssClass="cajatexto"
                        size="30" /></td>

                    <td>&nbsp;Idioma :</td>
                    <td>&nbsp;<form:select path="cboIdioma" id="cboIdioma"
                        cssClass="combo_o" cssStyle="width:200px">
                        <form:option value="">--Seleccione--</form:option>
                        <c:if test="${control.lstIdioma!=null}">
                            <form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
                                items="${control.lstIdioma}" />
                        </c:if>
                        </form:select>
                    </td>
                <td width="200px;">&nbsp;</td>
                <td >&nbsp;</td>
                    <c:if test="${control.txhCodUnico != null || control.txhCodUnico == '' }">
                    <td rowspan="5" align="center">&nbsp;
                        <c:if test="${control.nomImagen!=null}">
                            <img src="<c:out value="${control.pathImagen}" />" id='img_material'
                            border="1" width="100px" height="110px">
                        </c:if>
                        &nbsp;

                        <%if(opciones.indexOf("UPD_IMG|")>=0 || tipoUser.equals("A")){ %>

                            <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAdjuntar','','${ctx}/images/iconos/adjuntar2.jpg',1)">
                            <img src="${ctx}/images/iconos/adjuntar1.jpg" onclick="Fc_AdjuntarArchivo()" id="imgAdjuntar" alt="Upload" style="cursor: hand">
                            </a>

                            <c:if test="${control.nomImagen!=null}">
                            &nbsp;
                            <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/iconos/quitar2.jpg',1)">
                            <img src="${ctx}/images/iconos/quitar1.jpg" onclick="Fc_EliminarArchivo()" id="imgEliminar" alt="Remove" style="cursor: hand"></a></td>
                            </c:if>

                        <%} %>

                    </c:if>
                </tr>

                <tr>
                    <td>&nbsp;Clasificaci�n :&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td colspan="5">

                    &nbsp;<form:input path="txtPrefijo" id="txtPrefijo"
                        cssClass="cajatexto"  size="10" maxlength="10" onblur="fc_concatena()" />

                    &nbsp;<form:input path="txtDewey" id="txtDewey"
                        cssClass="cajatexto_1" readonly="readonly" size="60" /> &nbsp;&nbsp;
                    <form:input path="txtSufijo" id="txtSufijo"  onblur="fc_concatena()"
                        cssClass="cajatexto_o"  maxlength="20"  size="35"  />
                    &nbsp;
                        <%if(tipoUser.equals("A")){ %>
                        <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar1','','${ctx}/images/iconos/buscar2.jpg',1)">
                            <img src="${ctx}/images/iconos/buscar1.jpg" align="middle" alt="Buscar" id="imgBuscar1" style="CURSOR: pointer" onclick="javascript:fc_openDewey();">
                        </a>
                        <%}%>
                    </td>
                </tr>
                <tr>
                <td>&nbsp;</td>
                    <td colspan="4">
                    &nbsp;<form:input path="txtIdDewey" id="txtIdDewey"
                        cssClass="cajatexto_1" readonly="readonly"  size="60" cssStyle="width:175px"  />
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;Autor :</td>
                    <td colspan="3">&nbsp;<form:input path="txtAutor" id="txtAutor"
                        cssClass="cajatexto_1" size="60" readonly="readonly" /> &nbsp;&nbsp;

                        <%if(tipoUser.equals("A")){ %>
                        <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
                            <img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_openAutor();">
                        </a>
                        <%} %>

                    </td>
                    <td>&nbsp;<!-- Nro. P&aacute;gs/&nbsp;Duraci&oacute;n :--></td>
                    <td>&nbsp;<!--cambiar input x hidden  onblur="fc_ValidaNumeroOnBlur('txtNroPag');" cssStyle="text-align:center;"
                        onkeypress="fc_PermiteNumeros();" maxlength="4" cssClass="combo_o" size="4"-->
                        <form:hidden path="txtNroPag" id="txtNroPag" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;T&iacute;tulo :</td>
                    <td colspan="5">&nbsp;
                    <%--
                    <form:input path="txtTitulo" id="txtTitulo"
                        cssClass="cajatexto_o" cssStyle="height:40px; width:560px;" maxlength="500"  />--%>

                        <form:textarea path="txtTitulo" id="txtTitulo"
                                        cssClass="cajatexto"   onkeypress="validaTexto('txtTitulo','500')"
                                        cols="122" rows="4"  onblur="validaTexto('txtTitulo','500')"  />


                    </td>

                </tr>
            </table>
        </td>
        </tr>
    </table>

    <table><tr><td height="2px"></td></tr></table>

    <div id="TablaA">
    <table background="${ctx}/images/biblioteca/fondoinf.jpg" style="width:97%;margin-left:11px" border="0" bordercolor="red" cellspacing="4" cellpadding="1"
        height="80px" class="tabla">
    <tr>
        <td>
            <table width="93%" align="center" border="0" bordercolor="black" cellspacing="4" cellpadding="1">
                <tr>
                    <td valign="middle">&nbsp;Ciudad:</td>
                    <td rowspan="2" valign="top">
                        <table border="0" bordercolor="red" cellspacing="0" cellpadding="2">
                            <tr>
                                <td>
                                    <form:select path="cboPais" id="cboPais"
                                        cssClass="combo_o" cssStyle="width:110px" onchange="fc_CambiaPais()"  >
                                        <form:option value="" >--Seleccione--</form:option>
                                        <c:if test="${control.lstPais!=null}">
                                            <form:options itemValue="paisId" itemLabel="paisDescripcion"
                                                items="${control.lstPais}" />
                                        </c:if>
                                        </form:select>

                                    <form:select path="cboCiudad" id="cboCiudad"
                                        cssClass="combo_o" cssStyle="width:110px">
                                        <form:option value="">--Seleccione--</form:option>
                                        <c:if test="${control.lstCiudad!=null}">
                                            <form:options itemValue="ciudadId" itemLabel="ciudadDescripcion"
                                                items="${control.lstCiudad}" />
                                        </c:if>
                                    </form:select>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%">&nbsp;Editorial :</td>
                    <td width="17%">&nbsp;<form:select path="cboEditorial" id="cboEditorial"
                            cssClass="combo_o" cssStyle="width:235px">
                            <form:option value="">--Seleccione--</form:option>
                            <c:if test="${control.lstEditorial!=null}">
                                <form:options itemValue="codTipoTablaDetalle"
                                    itemLabel="descripcion" items="${control.lstEditorial}" />
                            </c:if>
                        </form:select>
                    </td>

                    <td width="40%">&nbsp;A�o Publicaci&oacute;n :</td>
                    <td>&nbsp;

                    <form:input path="txtFecPublicacion" id="txtFecPublicacion"
                        cssClass="cajatexto_o" size="20" maxlength="10"   />
                    &nbsp;
                        &nbsp;
                    </td>
                </tr>

                <tr>
                <td>&nbsp;</td>
                    <td width="13%">&nbsp;Nro.&nbsp;Edici&oacute;n :</td>
                    <td>&nbsp;<form:input path="txtNroEdicion" id="txtNroEdicion"
                        cssClass="cajatexto" size="21" maxlength="20"

                        /></td>
                    <td>&nbsp;URL.Arch. Digital :</td>
                    <td colspan="3">&nbsp;<form:input path="txtUrlArchivoDig"
                        id="txtUrlArchivoDig" cssClass="cajatexto" size="35" maxlength="70"
                        /></td>
                </tr>

                <tr>
                    <td>&nbsp;Descripci�n &nbsp;F&iacute;sica :</td>
                    <td colspan="6">&nbsp;<form:textarea path="txtUbicacionFisica" id="txtUbicacionFisica" cssClass="cajatexto"
                            cols="122" rows="4"  onkeypress="validaTexto('txtUbicacionFisica','100')" onblur="validaTexto('txtUbicacionFisica','100')" />
                    </td>
                </tr>
                <tr id="trFrecuencia" >
                    <td>&nbsp;Frec. act.<br>&nbsp;descripci�n :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td colspan="7">&nbsp;<form:input path="txtFrecuencia" id="txtFrecuencia" cssClass="cajatexto"
                                maxlength="50" size="45" cssStyle="width:150px;" onkeypress="validaTexto('txtFrecuencia','50')" onblur="validaTexto('txtFrecuencia','50')" />
                    </td>
                </tr>

                <tr>
                <td>&nbsp;Serie:</td>
                    <td colspan="6">&nbsp;<form:textarea path="mencionSerie" id="mencionSerie"
                                        cssClass="cajatexto"   onkeypress="validaTexto('mencionSerie','100')"
                                        cols="122" rows="2"  onblur="validaTexto('mencionSerie','200')"  />
                    </td>
                </tr>

                <tr>
                    <c:if test="${control.txhCodUnico == null  || control.txhCodUnico == ''}">
                        <td>&nbsp;Observaciones :</td>
                        <td colspan="7">&nbsp;<form:textarea path="txtObservaciones"
                            id="txtObservaciones" cssClass="cajatexto" cols="122" rows="4"  onkeypress="validaTexto('txtObservaciones','400')" onblur="validaTexto('txtObservaciones','400')" /></td>
                    </c:if>
                </tr>

                <tr>
                    <td>&nbsp;Contenido :</td>
                    <td colspan="7">&nbsp;<form:textarea path="txtContenido"
                        id="txtContenido" cssClass="cajatexto" cols="122" rows="4"  onkeypress="validaTexto('txtContenido','4000')" onblur="validaTexto('txtContenido','4000')" /></td>
                </tr>

                <tr>
                    <td>&nbsp;<!-- Formato :--></td>
                    <td>&nbsp;<!--hidden x input  cssClass="cajatexto" maxlength="50" size="16"-->
                    <form:hidden path="txtFormato" id="txtFormato"
                        /></td>
                </tr>
                <tr>
                    <td>&nbsp;Tipo Video :</td>
                    <td>&nbsp;<form:select path="cboTipoVideo" id="cboTipoVideo"
                            cssClass="cajatexto" cssStyle="width:145px">
                            <form:option value="">--Seleccione--</form:option>
                            <c:if test="${control.lstTipoVideo!=null}">
                                <form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
                                    items="${control.lstTipoVideo}" />
                            </c:if>
                        </form:select>
                    </td>

                    <td>&nbsp;Sede:&nbsp;</td>
                    <td><form:select path="cboSede" id="cboSede"
                        cssClass="combo_o" cssStyle="width:105px">
                        <form:option value="">--Seleccione--</form:option>
                        <c:if test="${control.lstSede!=null}">
                            <form:options itemValue="dscValor1" itemLabel="descripcion"
                                items="${control.lstSede}" />
                        </c:if>
                    </form:select></td>
                </tr>
                <c:if test="${control.txhCodUnico == null || control.txhCodUnico == '' }">

                        <td>&nbsp;Procedencia :</td>
                        <td>&nbsp;<form:select path="cboProcedencia" id="cboProcedencia"
                                cssClass="combo_o" cssStyle="width:145px">
                                <form:option value="">--Seleccione--</form:option>
                                <c:if test="${control.lstProcedencia!=null}">
                                    <form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
                                        items="${control.lstProcedencia}" />
                                </c:if>
                            </form:select>
                        </td>
                    </c:if>
                    <c:if test="${control.txhCodUnico == null  || control.txhCodUnico == ''}">

                        <td>&nbsp;Precio :</td>
                        <td>
                        <form:select path="cboPrecio" id="cboPrecio"
                            cssClass="combo_o" cssStyle="width:45px">
                            <form:option value="">SEL.</form:option>
                            <c:if test="${control.lstPrecio!=null}">
                                <form:options itemValue="codTipoTablaDetalle" itemLabel="dscValor3"
                                    items="${control.lstPrecio}" />
                            </c:if>
                        </form:select>&nbsp;<form:input path="txtPrecio" id="txtPrecio"
                            onkeypress="fc_PermiteNumerosPunto()"
                            onblur="fc_ValidaDecimalOnBlur('txtPrecio','7','2')"
                            maxlength="10" cssClass="cajatexto_o" size="7" />

                        </td>
                    </c:if>
                <tr>

                </tr>
                <tr>
                    <td colspan="3">&nbsp;Tipo Pr&eacute;stamo : &nbsp;&nbsp;&nbsp;
                    <form:checkbox path="chkTipoPrestamoSala" id="chkTipoPrestamoSala" value="1"/>Sala&nbsp;
                    <form:checkbox path="chkTipoPrestamoDomicilio" id="chkTipoPrestamoDomicilio" value="1" />
                    &nbsp;Domicilio</td>
                    <td>&nbsp;Aplica Reserva :</td>
                    <td><form:radiobutton path="rdoAplicaReserva" id="Si" value="1"/>S&iacute;&nbsp;
                    <form:radiobutton path="rdoAplicaReserva" id="No" value="0"/>No</td>
                    <td colspan="1">&nbsp;</td>
                </tr>
            </table>
        </td>
        </tr>
    </table>
    </div>

    <div id="TablaB" style="display: none;">

        <iframe id="iFrameRegistrarIngreso" name="iFrameRegistrarIngreso" frameborder="0"
        height="200px" width="100%"  style="width:94%;margin-left:11px;"
        src="${ctx}/biblioteca/biblio_admin_registrar_material_iframe_ingreso.html?txhCodigoUnico=${control.txhCodUnico}" >
        </iframe>

    </div>
    <div id="TablaC" style="display: none;">

        <iframe id="iFrameDescriptores" name="iFrameDescriptores" frameborder="0"
        height="200px" width="100%"  style="width:94%;margin-left:11px;"
        src="${ctx}/biblioteca/biblio_admin_registrar_material_iframe_descriptores.html?txhCodigoUnico=${control.txhCodUnico}" >
        </iframe>

    </div>

    <div id="TablaD" style="display: none;">

    <iframe id="iFrameDocumentosRelacionados" name="iFrameDocumentosRelacionados" frameborder="0"
        height="200px" width="100%"  style="width:94%;margin-left:11px;"
        src="${ctx}/biblioteca/biblio_admin_registrar_material_iframe_documentos_relacionados.html?txhCodigoUnico=${control.txhCodUnico}" >
    </iframe>


    </div>
    <table><tr><td></td></tr></table>
    </div>
    <div>
        <table><tr><td height="5px"></td></tr></table>
    </div>
    <div>
    <table border="0" width="97%" align="center" cellspacing="2" cellpadding="2">
        <tr>
            <td align="right">
                <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
                        <img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" align="middle" alt="Grabar" id="imgGrabar" onclick="javascript:fc_guardar('1');" style="cursor:pointer;"></a>

                <%if(opciones.indexOf("_ING|")>=0 || tipoUser.equals("A")){ %>
                <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegIngreso','','${ctx}/images/biblioteca/Gest_Mat/regingresos2.jpg',1)">
                        <img src="${ctx}/images/biblioteca/Gest_Mat/regingresos1.jpg" align="middle" alt="Registro de Ingresos" id="imgRegIngreso" style="CURSOR: pointer" onclick="javascript:fc_CambiarOpcion('1');"></a>

                <%} %>

                <%if(opciones.indexOf("_DES|")>=0 || tipoUser.equals("A")){ %>
                <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDescriptor','','${ctx}/images/biblioteca/Gest_Mat/descriptores2.jpg',1)">
                        <img src="${ctx}/images/biblioteca/Gest_Mat/descriptores1.jpg" align="middle" alt="Descriptores" id="imgDescriptor" style="CURSOR: pointer" onclick="javascript:fc_CambiarOpcion('2');"></a>
                <%} %>

                <%if(opciones.indexOf("_DOC|")>=0 || tipoUser.equals("A")){ %>
                <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDocRel','','${ctx}/images/biblioteca/Gest_Mat/docrelacionados2.jpg',1)">
                        <img src="${ctx}/images/biblioteca/Gest_Mat/docrelacionados1.jpg" align="middle" alt="Documentos Relacionados" id="imgDocRel" style="CURSOR: pointer" onclick="javascript:fc_CambiarOpcion('3');"></a>
                <%} %>

                <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
                        <img src="${ctx}/images/botones/regresar1.jpg" align="middle" alt="Regresar" id="imgRegresar" style="cursor:pointer;" onclick="javascript:fc_RegresarBandeja();"></a>

            </td>
        </tr>
    </table>
    </div>

    <!-- PARAMETROS DE BUSQUEDA DE LA PAGIN ANTERIOR -->
    <form:hidden path="prmTxtCodigo" id="prmTxtCodigo" />
    <form:hidden path="prmTxtNroIngreso" id="prmTxtNroIngreso" />
    <form:hidden path="prmCboTipoMaterial" id="prmCboTipoMaterial" />
    <form:hidden path="prmCboBuscarPor" id="prmCboBuscarPor" />
    <form:hidden path="prmTxtTitulo" id="prmTxtTitulo" />
    <form:hidden path="prmCboIdioma" id="prmCboIdioma" />
    <form:hidden path="prmCboAnioIni" id="prmCboAnioIni" />
    <form:hidden path="prmCboAnioFin" id="prmCboAnioFin" />
    <form:hidden path="prmTxtFechaReservaIni" id="prmTxtFechaReservaIni" />
    <form:hidden path="prmTxtFechaReservaFin" id="prmTxtFechaReservaFin" />

    </form:form>
    <script type="text/javascript">
        //TODO: Esto se debe descomentar, y verificar que cuando este desactivado no se envie el mensaje de ''seleccionar sede'
        if (document.getElementById("txhSedeSel").value!=''){
            document.getElementById("cboSede").value=document.getElementById("txhSedeSel").value;
            document.getElementById("cboSede").disabled=true;
        }


        <%if(!tipoUser.equals("A")){%>
            //Inhabilitando controles para cuando el usuario es Apoyo.
            $('#cboTipoMaterial').prop('disabled', true);

            $('#txtIsbn').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtIsbn').attr('readonly', true);

            $('#cboIdioma').prop('disabled',true);

            $('#txtPrefijo').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtPrefijo').prop('disabled', true);

            $('#txtSufijo').removeClass('cajatexto_o').addClass('cajatexto_1');
            $('#txtSufijo').prop('disabled', true);

            $('#txtTitulo').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtTitulo').attr('readonly', true);

            $('#cboPais').prop('disabled',true);
            $('#cboCiudad').prop('disabled',true);
            $('#cboEditorial').prop('disabled',true);

            $('#txtFecPublicacion').removeClass('cajatexto_o').addClass('cajatexto_1');
            $('#txtFecPublicacion').attr('readonly', true);

            $('#txtNroEdicion').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtNroEdicion').attr('readonly', true);

            $('#txtUrlArchivoDig').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtUrlArchivoDig').attr('readonly', true);

            $('#txtUbicacionFisica').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtUbicacionFisica').attr('readonly', true);

            $('#txtFrecuencia').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtFrecuencia').attr('readonly', true);

            $('#mencionSerie').removeClass('cajatexto').addClass('cajatexto_1');
            $('#mencionSerie').attr('readonly', true);

//             $('#txtObservaciones').removeClass('cajatexto').addClass('cajatexto_1');
//             $('#txtObservaciones').attr('readonly', true);

            $('#txtContenido').removeClass('cajatexto').addClass('cajatexto_1');
            $('#txtContenido').attr('readonly', true);

            $('#cboTipoVideo').prop('disabled',true);
//             $('#cboProcedencia').prop('disabled',true);

            $('#chkTipoPrestamoDomicilio').prop('disabled',true);
            $('#chkTipoPrestamoSala').prop('disabled',true);

            $('#Si').prop('disabled',true);
            $('#No').prop('disabled',true);

        <%}%>

    </script>
</body>


