<%@page import="com.tecsup.SGA.bean.UsuarioSeguridad"%>
<%String contString = "";%>
<%@ include file="/taglibs.jsp"%>
<head>	

	<%
		UsuarioSeguridad userLogin = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
		String opciones = "";
		String tipoUser = "";
		if(userLogin!=null){
			opciones = userLogin.getOpcionesApoyo();
			tipoUser = userLogin.getTipoUsuario();
		}
	
	%>
		
	<script type="text/javascript">
	function onLoad(){
			
	}	
	
	function fc_openAgregarIngreso(){
			url = "${ctx}/biblioteca/biblio_admin_registrar_material_registrar_ingreso.html?"+
			"txhCodUnico="+document.getElementById("txhCodigoUnico").value+
			"&txhCodUsuario="+parent.document.getElementById("usuario").value;			
			
			Fc_Popup(url,650,220);		
	}
	
	function fc_openModificarIngreso(){
		if(fc_Trim(document.getElementById("txhCodigoIngreso").value)!="")
		{		
		
		var url = "${ctx}/biblioteca/biblio_admin_registrar_material_registrar_ingreso.html?"+
		"flgModificarIngreso=true"+
		"&txhCodigoIngreso="+document.getElementById("txhCodigoIngreso").value+
		"&txhCodUnico="+document.getElementById("txhCodigoUnico").value+
		"&txhCodUsuario="+parent.document.getElementById("usuario").value;
		
		Fc_Popup(url,650,220);			
		
		}else{
			alert(mstrSeleccione);
		}
	}
	

	function fc_Eliminar(){
	
	if(fc_Trim(document.getElementById("txhCodigoIngreso").value)!=""){
		
		if(confirm(mstrSeguroEliminar))
		{
			document.getElementById("operacion").value="irEliminar";
			document.getElementById("usuario").value=parent.document.getElementById("usuario").value;
			document.getElementById("frmMain").submit();
		}
		
		}else
			alert(mstrSeleccione);
	}	
	
	function Fc_Upload(){
		url = "${ctx}/biblioteca/biblio_admin_registrar_material_adjuntar_documentos.html?"+
		"txhCodUnico="+document.getElementById("txhCodigoUnico").value;		
		Fc_Popup(url,430,180);
	}
	
	function fc_selecciona(codigoUnico){
		document.getElementById("txhCodigoDescriptor").value = codigoUnico;  
	}
	
	function fc_submit(){
		document.getElementById("frmMain").submit();
	}
	function fc_SeleccionaRegistro(codUnico){
	
		document.getElementById("txhCodigoIngreso").value=codUnico ;
	}
	
	</script>

</head>

<body topmargin="5" leftmargin="10" rightmargin="10">
<form:form  id="frmMain" action="${ctx}/biblioteca/biblio_admin_registrar_material_iframe_ingreso.html"  method="post">

<input type="hidden" name="txhCodigoUnico" id="txhCodigoUnico" value="${model.txhCodigoUnico}" />
<input type="hidden" name="operacion" id="operacion" value="irConsultar" />
<input type="hidden" id="usuario" name="usuario"/>

<!-- VARIABLE QUE ALMACENA EL ITEM SELECCIONADO -->
<input type="hidden" name="txhCodigoIngreso" id="txhCodigoIngreso" />
<%int contInt = 0;%>	
	<table cellpadding="0" cellspacing="0" id="Table1" width="100%" border="0" class="">
		<tr>			
			<td valign="top">
				<div style="overflow: auto; height: 200px; width:100%;">
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:98%;">
					<tr class="grilla">				
						<td class="grilla" width="4%">Sel</td>
						<td class="grilla" width="10%">Fec. Ing.</td>
						<td class="grilla" width="8%">Num. Ing.</td>						
						<td class="grilla" width="50%">Procedencia</td>
						<td class="grilla" width="4%">Mon.</td>
						<td class="grilla" width="8%">Precio</td>
						<td class="grilla" width="8%">Estado</td>
					</tr>
				<!-- BIBLIOTECA -->
					<c:forEach var="objCast" items="${model.lstRegistroIngreso}" varStatus="loop" >						
						<tr class="tablagrilla">
							<%contInt++; %>
							<td align="center">
								<input type=radio name="Radio1" id="Radio1" onclick="fc_SeleccionaRegistro('<c:out value="${objCast.codUnico}" />')" >							
							</td>
							<td align="center"><c:out value="${objCast.fecIngreso}" /></td>
							<td align="center"><c:out value="${objCast.nroIngreso}" /></td>							
							<td align="left"><c:out value="${objCast.desPrecedencia}" /></td>
							<td align="right"><c:out value="${objCast.descMoneda}" /></td>
							<td align="right"><c:out value="${objCast.nroPrecio}" /></td>
							<td align="center"><c:out value="${objCast.descEstado}" /></td>
						</tr>
					</c:forEach>
				<!-- /BIBLIOTECA -->
				</table>
				</div>
			</td>
			<td width="25px" valign="top" align="right">
				<br/>
				
				<%if(opciones.indexOf("INS_ING|")>=0 || tipoUser.equals("A")){ %>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_openAgregarIngreso();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar Requerimiento">
					</a>
				<%} %>
				
				<br/>
				
				<%if(opciones.indexOf("UPD_ING|")>=0 || tipoUser.equals("A")){ %>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
						<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" 
							id="imgModificarPerfil" onclick="javascript:fc_openModificarIngreso();">
					</a>
				<%} %>
				
				<br/>
				
				<%if(tipoUser.equals("A")){ %>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliminarPerfil" onclick="javascript:fc_Eliminar();">
				</a>
				<%} %>
				
			</td>			
		</tr>
	</table>
<%contString = ""+contInt;%>
</form:form>
<script language="javascript" >
	parent.document.getElementById("txtTotalVolumen").value = "<%=contString%>";
</script>	
</body>