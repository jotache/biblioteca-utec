<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var strExtensionDOC = "<%=(String)request.getAttribute("strExtensionDOC")%>";
	var strExtensionPDF = "<%=(String)request.getAttribute("strExtensionPDF")%>";	
	
	function onLoad(){
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" ){		
			//window.opener.document.getElementById("nomImagen").value=document.getElementById("nomArchivo").value;
			window.opener.Fc_ActualizarPadre();			
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if ( objMsg.value == "ERROR" )
		{
			alert(mstrProblemaGrabar);
		}else if( objMsg.value == "ERROR_TAMANIO" )
		{
			alert('El tama�o del archivo debe ser menor a 5MB');
		}
	}
	function fc_Valida(){
		var strExtension;
		var lstrRutaArchivo;
		if(fc_Trim(document.getElementById("txtArchivo").value)==""){
			alert('Debe seleccionar su documento a subir.');
			return false;
		}
		else{
			if(fc_Trim(document.getElementById("txtArchivo").value) != ""){
				lstrRutaArchivo = document.getElementById("txtArchivo").value;
				strExtension = "";
				//strExtension = lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);
				strExtension = lstrRutaArchivo.toUpperCase().substring(lstrRutaArchivo.length - 3);
				
				//**************************
				//obtiene el nombre del archivo
				nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();
								 
				document.getElementById("txhNomArchivo").value=nombre.substring("1");
				
				//**************************
			
				//if (strExtension != strExtensionGIF && strExtension != strExtensionXLS){
				if(strExtension != strExtensionDOC.toUpperCase() && strExtension != strExtensionPDF.toUpperCase() ){
					alert('El documento debe tener la \nextensi�n : DOC � PDF');
					return false;
				}
				else{
					document.getElementById("txhExtArchivo").value = strExtension;
				}
			}
		
		}
		return true;
	}
		
	function fc_Grabar(){
	if(fc_Trim(document.getElementById("txtTitulo").value)!=""){
		if (fc_Valida()){
			if (confirm(mstrSeguroGrabar)){				
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
			}
		}	
	}else{
		alert('Seleccione un t�tulo');
		document.getElementById("txtTitulo").focus();
	}

	}

</script>
</head>
<body topmargin="5" leftmargin="10" rightmargin="10" >
<form:form  id="frmMain" action="${ctx}/biblioteca/biblio_admin_registrar_material_adjuntar_documentos.html" commandName="control" enctype="multipart/form-data" method="post">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="extArchivo" id="txhExtArchivo"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codProducto" id="txhCodProducto"/>
	<form:hidden path="codCiclo" id="txhCodCiclo"/>
	<form:hidden path="nomNuevoArchivo" id="txhNomNuevoArchivo"/>
	<form:hidden path="nomArchivo" id="txhNomArchivo"/>
	
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="txhCodigoUnico" id="txhCodigoUnico"/>
	<form:hidden path="usuario" id="usuario"/>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:2px; margin-top:2px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="160px" class="opc_combo"><font style="">Adjuntar Archivo</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="3px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:99%; margin-left:2px; margin-right: 3px;" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red">
		<tr>			
			<td width="10%">
				Titulo:
			</td>
			<td>								
				<form:input path="txtTitulo" cssClass="cajatexto" id="txtTitulo" maxlength="200" size="70"  />
			</td>
		</tr>
		<tr>		
			<td>
				Archivo:
			</td>
			<td>				
				<input type=file name="txtArchivo" class="cajatexto" style="width:100%">
			</td>
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=right>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_Grabar();" style="cursor:hand" id="imgAgregar" alt="Grabar">&nbsp;</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img ID="Button1" NAME="Button1" src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();"></a>
			</td>		
		</tr>
	</table>		
</form:form>	
</body>
</html>



