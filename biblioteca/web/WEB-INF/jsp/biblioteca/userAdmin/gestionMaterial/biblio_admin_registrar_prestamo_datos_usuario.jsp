<%@ include file="/taglibs.jsp"%>

<head>
<script language=javascript>
function onLoad(){
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
		
	if(mensaje!=""){
		alert(mensaje);					
	}
	
	fc_mostrar();
}

function fc_mostrar(){
	var flgAlumno = document.getElementById("txhIndTipoUsuario").value;
	
	document.getElementById("tr_TipoAlumno").style.display = "none";
	document.getElementById("tr_TipoPersonal").style.display = "none";
	
	if(flgAlumno=="1")
		document.getElementById("tr_TipoAlumno").style.display = "";
	else if(flgAlumno=="0")
		document.getElementById("tr_TipoPersonal").style.display = "";
}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control" 
action="${ctx}/biblioteca/biblio_admin_registrar_prestamo_datos_usuario.html">

<form:hidden path="operacion" id="operacion" />
<form:hidden path="usuario" id="usuario" />
<form:hidden path="txhCodUsuario" id="txhCodUsuario" />
<form:hidden path="txhIndTipoUsuario" id="txhIndTipoUsuario" />
<form:hidden path="txhCodTipoUsuario" id="txhCodTipoUsuario" />


	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="220px" class="opc_combo"><font style="">Datos del Usuario</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>		
				
		<table background="${ctx}\images\biblioteca\fondosup.jpg" border="0" cellspacing="4" cellpadding="5" class="tabla"
		bordercolor="red" style="width:94%;height:70px;margin-left: 3px;">					
			<tr>
				<td ID="Select3" name="Select3" width="17%">Nombres:</td>
				<td colspan="3">				
				<form:input path="txtNombres" id="txtNombres" cssClass="cajatexto_1" cssStyle="width:350px"/>
				</td>
			</tr>
			<tr>
				<td  ID="Select3" name="Select3">Tipo Usuario:</td>
				<td colspan="3">				
				<form:input path="txtTipoUsuario" id="txtTipoUsuario" cssClass="cajatexto_1" cssStyle="width:200px"/>
				</td>
			</tr>
			
			<tr id="tr_TipoAlumno" name="tr_TipoAlumno">
				<td  ID="Select3" name="Select3">Especialidad:</td>
				<td>				
				<form:input path="txtEspecialidad" id="txtEspecialidad" cssClass="cajatexto_1" cssStyle="width:200px"/>
				</td>
				<td  ID="Select3" name="Select3">Ciclo:</td>
				<td>				
				<form:input path="txtCiclo" id="txtCiclo" cssClass="cajatexto_1" cssStyle="width:100px"/>
				</td>
			</tr>
			<tr id="tr_TipoPersonal" name="tr_TipoPersonal"  >  
				<td  ID="Select3" name="Select3">Cargo:</td>
				<td>				
				<form:input path="txtCargo" id="txtCargo" cssClass="cajatexto_1" cssStyle="width:200px"/>
				</td>				
			</tr>						
		</table>	
		
		<table align=center>
		<tr>		
			<td><br>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
		</table>	

		
</form:form>		
</body>
