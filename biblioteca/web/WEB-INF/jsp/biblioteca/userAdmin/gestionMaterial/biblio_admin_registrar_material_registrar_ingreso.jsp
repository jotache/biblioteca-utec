<%@ include file="/taglibs.jsp"%>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;"	type="text/JavaScript"></script>	
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter"/>
<script language=javascript>
function onLoad(){
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
	var refresh = "<%=(( request.getAttribute("refresh")==null)?"": request.getAttribute("refresh"))%>";
	if(mensaje!="")
	{
		alert(mensaje);
		if(refresh!="" && refresh=="1")
		{	window.close();
			window.opener.fc_submit();
		
		}
	}
}

function fc_valida(){

	if(document.getElementById("txtFecIngreso").value==""){
		alert('Debe ingresar la fecha de ingreso.');
		document.getElementById("txtFecIngreso").focus();		
		return 0;
	}/*else if(document.getElementById("txtFecIngreso").value!=""){
		alert("1");
		fc_ValidaFechaActualRet('txtFecIngreso','3');
		return 0;
	}*/
	else if(fc_Trim(document.getElementById("cboProcedencia").value)==""){
		alert('Seleccione procedencia.');		
		document.getElementById("cboProcedencia").focus();		
		return 0;
	}else if(fc_Trim(document.getElementById("cboMoneda").value)==""){
		alert('Seleccione moneda.');
		document.getElementById("cboMoneda").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtPrecio").value)==""){
		alert('Debe ingresar cantidad.');
		document.getElementById("txtPrecio").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboEstado").value)==""){
		alert('Seleccione estado.');
		document.getElementById("cboEstado").focus();
		return 0;
	}
	
	/*else if(fc_Trim(document.getElementById("nroVolumen").value)==""){
		alert(mstrIngreseNroVolumen);
		document.getElementById("nroVolumen").focus();
		return 0;
	}*/
	
	return 1;
}

function validaTexto(id){

	if(document.getElementById(""+id).value.length > 400)
	{
		str = document.getElementById(""+id).value.substring(0,400);
		document.getElementById(""+id).value=str;
		return false;
	}	
}


function fc_guardar(){
	
	if(fc_valida()){
		if(confirm(mstrSeguroGrabar)){
			
			if(document.getElementById("flgUpdate").value=="true")
				document.getElementById("operacion").value="irActualizar";
			else
				document.getElementById("operacion").value="irRegistrar";
							
			document.getElementById("frmMain").submit();	
		}
		else
			return false;
	}

}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_material_registrar_ingreso.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="txhCodigoUnico" id="txhCodigoUnico" />
	<form:hidden path="txhCodigoMaterial" id="txhCodigoMaterial" />
	<form:hidden path="flgUpdate" id="flgUpdate" />
	<form:hidden path="usuario" id="usuario" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="590px" class="opc_combo"><font style="">Registro de Ingresos</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>

	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:99%; margin-left:3px" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red">
		<tr>
			<td width="15%">Nro. Ingreso :</td>
			<td width="20%">				
				<form:input path="txtIngreso" id="txtIngreso" readonly="readonly" cssClass="cajatexto_1" maxlength="10" 
				onblur="fc_ValidaNumeroOnBlur('txtIngreso');" onkeypress="fc_PermiteNumeros();"  />
			</td>
			<td width="15%">Fec. Ingreso :</td>
			<td width="20%">				
				<form:input path="txtFecIngreso" id="txtFecIngreso" maxlength="10" cssClass="cajatexto_o" 
				onkeypress="fc_ValidaFecha('txtFecIngreso')" onblur="fc_ValidaFechaOnblur('txtFecIngreso');fc_ValidaFechaActualRet('txtFecIngreso','3');" 
				size="12" />
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgCalendar" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>
			</td>
			<!-- td width="15%">Nro. P�gs/Duraci�n :</td-->
			<td width="15%">
			<form:hidden path="nroVolumen" id="nroVolumen"/>&nbsp;
			</td>
		</tr>
		<tr>
			<td>Procedencia :</td>
			<td>					
				<form:select path="cboProcedencia" id="cboProcedencia" cssClass="cajatexto_o" cssStyle="width:100%">
					<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstProcedencia!=null}">
							<form:options itemValue="codTipoTablaDetalle"
								itemLabel="descripcion" items="${control.lstProcedencia}" />
						</c:if>
				</form:select>						
				</td>
			<td>Precio :</td>
			<td>
				<form:select path="cboMoneda" id="cboMoneda" cssClass="cajatexto_o" cssStyle="width:40px">
					<form:option value="">-Sel-</form:option>
					<c:if test="${control.lstMoneda!=null}">
						<form:options itemValue="codTipoTablaDetalle"
							itemLabel="dscValor3" items="${control.lstMoneda}" />
					</c:if>
				</form:select>	
				<form:input path="txtPrecio" id="txtPrecio"						
					onkeypress="fc_PermiteNumerosPunto()"  
			  		onblur="fc_ValidaDecimalOnBlur('txtPrecio','7','2')"
					maxlength="10" cssClass="cajatexto_o" size="12"/>
			</td>			
			<td>Estado :</td>
			<td>
				<form:select path="cboEstado" id="cboEstado" cssClass="combo_o" cssStyle="width:80px">
					<form:option value="">--Seleccione--</form:option>
					<form:option value="0001">Activo</form:option>
					<form:option value="0002">Inactivo</form:option>
					<c:if test="${control.lstEstado!=null}">
						<form:options itemValue="codTipoTablaDetalle"
							itemLabel="descripcion" items="${control.lstEstado}" />
					</c:if>
				</form:select>
			</td>
		<tr>
			<td>Observaci�n :</td>
			<td colspan="4">
				<form:textarea path="txtObservacion" id="txtObservacion" cols="40" rows="3" cssClass="cajatexto" 
				 onkeypress="validaTexto('txtObservacion')"  />
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_guardar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
	</table>	
</form:form>
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "txtFecIngreso",
	ifFormat       :    "%d/%m/%Y",
	daFormat       :    "%d/%m/%Y",
	button         :    "imgCalendar",
	singleClick    :    true

	});
                          
</script>
</body>