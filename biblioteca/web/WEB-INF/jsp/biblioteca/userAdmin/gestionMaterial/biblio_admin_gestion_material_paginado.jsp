<%@ include file="/taglibs.jsp"%>   
<head>
<script type="text/javascript">
function onLoad(){
	
}

function fc_Prestamo(){
	parent.fc_Prestamo();
}
  
function fc_Devolucion(){
	parent.fc_Devolucion();
}

function fc_Sanciones(){
	parent.fc_Sanciones();
}

function fc_Modificar(){
	var sedeSel = document.getElementById("txhSedeSel").value;
	var sedeUsu = document.getElementById("txhSedeUsuario").value;
	//if (sedeSel!=sedeUsu)
		//alert('No puede modificar materiales para otra sede.');
	//else {
						
		if(fc_Trim(document.getElementById("txhCodigoUnico").value) == "")
			alert(mstrSeleccione);
		else{
			parent.location.href="${ctx}/biblioteca/biblio_admin_registrar_material.html?"+
				"parModifica=true&txhCodigoUnico="+fc_Trim(document.getElementById("txhCodigoUnico").value)+
				"&txhCodigoGenerado="+fc_Trim(document.getElementById("txhCodigoGenerado").value)+
				"&txhCodUsuario="+fc_Trim(parent.document.getElementById("usuario").value)+
				"&"+parent.fc_getParametrosBusqueda();	
		}			
	//}	
}

function fc_Construccion(){
	alert("Funcionalidad en construcci�n");
}

function fc_Eliminar(){
	var sedeSel = document.getElementById("txhSedeSel").value;
	var sedeUsu = document.getElementById("txhSedeUsuario").value;
	//if (sedeSel!=sedeUsu)
		//alert('No puede eliminar materiales para otra sede.');
	//else {
		if(fc_Trim(document.getElementById("txhCodigoUnico").value) == "")
			alert(mstrSeleccione);
		else{
			if(confirm(mstrSeguroEliminar)){
			parent.document.getElementById("idEliminacion").value = document.getElementById("txhCodigoUnico").value;
			parent.document.getElementById("operacion").value = "irEliminar";
			parent.document.getElementById("frmMain").submit();		
			}
		}	
	//}
	//parent.location.href="${ctx}/biblioteca/biblio_admin_registrar_material.html?txhCodigoUnico="+fc_Trim(document.getElementById("txhCodigoUnico").value);
}

function fc_seleccion(codigoUnico,codigoGeneredo){			
	$('#txhCodigoUnico').val(''+codigoUnico);
	$('#txhCodigoGenerado').val(''+codigoGeneredo);	
}

function fc_VerHistorial(codigoUnico,codigoGeneredo,pos){	
	var url ="${ctx}/biblioteca/biblio_admin_gestion_material_historial.html"+
	"?codigoUnico="+fc_Trim(codigoUnico)+
	"&codigoGeneredo="+fc_Trim(codigoGeneredo)+
	"&codigo="+codigoGeneredo+
	"&tipoMaterial="+document.getElementById("hidTipoMat"+pos).value+
	"&titulo="+document.getElementById("hidTituloMat"+pos).value;	
	Fc_Popup(url,600,420);
}

function fc_detalleMaterial(codUnico){
	var url ="${ctx}/biblioteca/detalle_material.html"+
		"?txhCodigoUnico="+fc_Trim(codUnico);
	Fc_Popup(url,700,520);
}

function fc_Agregar(){	
	var sedeSel = document.getElementById("txhSedeSel").value;
	var sedeUsu = document.getElementById("txhSedeUsuario").value;
	//if (sedeSel!=sedeUsu)
		//alert('No puede ingresar materiales para otra sede.');
	//else
		parent.fc_Agregar(); //location.href="${ctx}/biblioteca/biblio_admin_registrar_material.html";
}

function Fc_VerDetalle(){
	Fc_Popup("Historial_Prestamos.htm",600,250);
}

function fc_VerReservas(codUnico){
	var url ="${ctx}/biblioteca/bib_admin_consultar_reservas_material.html" +
	"?codUsuario=" + fc_Trim(parent.document.getElementById("usuario").value) +
	"&codMaterial=" + fc_Trim(codUnico);
	Fc_Popup(url,600,300);
}

</script>
</head>
<body>

<form:form  id="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_gestion_material_paginado.html"
  method="post">
  
<input type="hidden" id="txhCodigoUnico" >
<input type="hidden" id="txhCodigoGenerado" >
<input type="hidden" id="operacion" name="operacion">
<form:hidden path="tipoUsuario" id="tipoUsuario"/>
<form:hidden path="sedeSel" id="txhSedeSel"/>
<form:hidden path="sedeUsuario" id="txhSedeUsuario"/>

	<table cellpadding="0" cellspacing="0" ID="Table1" border="0" style="margin-left:11px;width:98.5%" class="" >
		<tr>		
			<td style="width: 97%;">
			<!--PAGINADO-->
				<div style="overflow: auto; height: 330px;width:98.8%;border: 1px">
				<display:table name="sessionScope.SS_RESULTADO_MATERIAL_FILTRO" 
				cellpadding="0" cellspacing="1" decorator="com.tecsup.SGA.bean.BibliotecaAgregarMaterialDecorator"				 
				pagesize="9" style="border: 1px solid #048BBA;width:100%;" requestURI="" >	
	 		
				<display:column property="radioHistorialBandeja" 	title="Sel." headerClass="grilla" class="tablagrilla"  style="width:5%" />				
				<display:column property="codGenerado" 		title="Codigo" headerClass="grilla"  class="tablagrilla"  style="text-align:left;width:10%"/>
				<display:column property="textAreaTitulo" 			title="Titulo" headerClass="grilla"  class="tablagrilla" style="width:30%;text-align: left"/>
				<display:column property="descAutor" 		title="Autor" headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align: left"/>
				<display:column property="fechaPublicacion" title="A�o" headerClass="grilla"  class="tablagrilla"  style="width:6%;text-align: center"/>
				<display:column property="descTipoMat" 		title="Tipo Material" headerClass="grilla"  class="tablagrilla"  style="text-align: left;width:15%"/>
				<display:column property="volumenes" 		title="Total.<br>Ejemp." headerClass="grilla"  class="tablagrilla"  style="width:5%;text-align: center"/>
				<display:column property="nomSede" 			title="Sede" headerClass="grilla"  class="tablagrilla"  style="text-align:center;width:8%"/>
				<!-- JCMU 16/06/08 display:column property="dewey" 			title="Dewey" headerClass="grilla"  class="tablagrilla"  style="text-align:left;width:20%"/> -->
			
				<display:column property="prestSala" title="Prest.<br>Sala." headerClass="grilla"  class="tablagrilla"  style="width:5%;text-align: center"/>
				<display:column property="prestDomicilio" title="Prest.<br>Domic." headerClass="grilla"  class="tablagrilla"   style="width:15%;text-align: center"/>
				<display:column property="flgReservaMaterial" title="Reserv." headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align: center"/>
				<display:column property="verHistorialBandeja" title="Hist." headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align: center"/>
				<display:column property="detalleMaterial" title="Det." headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align: center"/>
				
				
				<display:setProperty name="basic.empty.showtable" value="true"  />
				<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='14' align='center'>No se encontraron registros</td></tr>"  />
				<display:setProperty name="paging.banner.placement" value="bottom"/>
				<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
				<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
				<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
				<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
				<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
				<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
				<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
				<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
			</display:table>
			<!--/PAGINADO-->
			</div>
			</td>
			<td align="center" style="width:3%;">&nbsp;
				<c:if test="${control.tipoUsuario=='A'}">
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
							<img alt="Agregar" src="${ctx}/images/iconos/agregar1.jpg" id="imgAgregar" style="cursor:pointer;" onclick="javascript:fc_Agregar();"></a>
					&nbsp;
				</c:if>	
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgModificarPerfil','','${ctx}/images/iconos/actualizar2.jpg',1)">
							<img style="cursor:pointer" src="${ctx}/images/iconos/actualizar1.jpg" alt="Modificar" id="imgModificarPerfil" onclick="javascript:fc_Modificar();"></a>
					&nbsp;
				<c:if test="${control.tipoUsuario=='A'}">	
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
							<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" id="imgEliminarPerfil" onclick="javascript:fc_Eliminar();"></a>
				</c:if>
			</td>
		</tr>
	</table>

	<table cellspacing="0" cellpadding="0" border="0" align="right" style="margin-top: 5px;margin-right:43px">
		<tr>
			<td align="center">
				<c:if test="${control.tipoUsuario=='A'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgSancion','','${ctx}/images/botones/sanciones2.png',1)">
						<img src="${ctx}/images/botones/sanciones1.png" align="absmiddle" alt="Sanciones" id="imgSancion" style="CURSOR: pointer" onclick="javascript:fc_Sanciones();"></a>
				</c:if>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgPrest','','${ctx}/images/biblioteca/Gest_Mat/prestamos2.jpg',1)">
						<img src="${ctx}/images/biblioteca/Gest_Mat/prestamos1.jpg" align="absmiddle" alt="Pr&eacute;stamos" id="imgPrest" style="CURSOR: pointer" onclick="javascript:fc_Prestamo();"></a>
						
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDevol','','${ctx}/images/biblioteca/Gest_Mat/devoluciones2.jpg',1)">
						<img src="${ctx}/images/biblioteca/Gest_Mat/devoluciones1.jpg" align="absmiddle" alt="Devoluciones" id="imgDevol" style="CURSOR: pointer" onclick="javascript:fc_Devolucion();"></a>		
			</td>
		</tr>
	</table>
	
</form:form>	

</body>
				