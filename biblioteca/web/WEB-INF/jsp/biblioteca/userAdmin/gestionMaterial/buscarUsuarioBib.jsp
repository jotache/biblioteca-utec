<%@ include file="/taglibs.jsp"%>
<html>
<head>

<!-- 
	U: Recupera el username del alumno (indicar campo ctrlUSE)
	UYN : Recupera el username y nombre (indicar campos ctrlUSE y ctrlNOM)
	C : Recupera el c�digo (indicar campo ctrlCOD)
	CYN : Recupera el c�digo y el nombre (indicar campos ctrlCOD y ctrlNOM)
	CYU: Recupera el c�digo y el username (indicar campos ctrlCOD y ctrlUSE)
	ALL : Recupera c�digo,username y nombre (indicar los 3 campos)
 -->
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<script language=javascript>
function seleccionar_alumno(codigo,carnet,nombre){
	document.getElementById("codigo_s").value = codigo;
	document.getElementById("carnet_s").value = carnet;
	document.getElementById("nombre_s").value = nombre;
}
function onLoad(){
	document.getElementById("txtCodCarnet").focus();
}
function buscarAlumno(){	
	if(validar()){
		document.getElementById("operacion").value="BUSCAR";		
		document.getElementById("frmBusqueda").submit();
	}	
}
function validar(){
	//if(fc_Trim(document.getElementById("txtCodCarnet").value)=="" && fc_Trim(document.getElementById("txtNombre").value)=="" && fc_Trim(document.getElementById("apellPaterno").value)=="" && fc_Trim(document.getElementById("apellMaterno").value)==""){
	if(document.getElementById("txtCodCarnet").value=="" && document.getElementById("txtNombre").value=="" && document.getElementById("txtApellPaterno").value=="" && document.getElementById("txtApellMaterno").value==""){
		alert('Por favor ingrese un criterio de b�squeda');
		return false;
	}
	return true;			
}     
function fc_aceptar(){

	if(fc_Trim(document.getElementById("codigo_s").value)!=""
		&& fc_Trim(document.getElementById("carnet_s").value)!=""	
			&& fc_Trim(document.getElementById("nombre_s").value)!="" )
	{

		var ctrlUSE = document.getElementById("ctrlUSE").value;
		var ctrlCOD = document.getElementById("ctrlCOD").value;
		var ctrlNOM = document.getElementById("ctrlNOM").value;		
		
		if (document.getElementById("tipoRetorno").value=="U"){			
			window.opener.document.getElementById(ctrlUSE).value = fc_Trim(document.getElementById("carnet_s").value);
		}else if (document.getElementById("tipoRetorno").value=="UYN"){			
			window.opener.document.getElementById(ctrlUSE).value = fc_Trim(document.getElementById("carnet_s").value);
			window.opener.document.getElementById(ctrlNOM).value = fc_Trim(document.getElementById("nombre_s").value);
		}else if (document.getElementById("tipoRetorno").value=="C"){
			window.opener.document.getElementById(ctrlCOD).value = fc_Trim(document.getElementById("codigo_s").value);
		}else if (document.getElementById("tipoRetorno").value=="CYN"){
			window.opener.document.getElementById(ctrlCOD).value = fc_Trim(document.getElementById("codigo_s").value);	
			window.opener.document.getElementById(ctrlNOM).value = fc_Trim(document.getElementById("nombre_s").value);
		}else if (document.getElementById("tipoRetorno").value=="CYU"){
			window.opener.document.getElementById(ctrlCOD).value = fc_Trim(document.getElementById("codigo_s").value);
			window.opener.document.getElementById(ctrlUSE).value = fc_Trim(document.getElementById("carnet_s").value);
		}else if (document.getElementById("tipoRetorno").value=="ALL"){
			window.opener.document.getElementById(ctrlCOD).value = fc_Trim(document.getElementById("codigo_s").value);
			window.opener.document.getElementById(ctrlUSE).value = fc_Trim(document.getElementById("carnet_s").value);
			window.opener.document.getElementById(ctrlNOM).value = fc_Trim(document.getElementById("nombre_s").value);			
		}

		
		if (document.getElementById("ventanaOrigen").value=="eva_con_bandeja"){			
			window.opener.actualizarCabecera();			
		}
			
		window.close();
	}else
		alert("No ha seleccionado un alumno");	
}
</script>
<title>B�squeda de Alumno</title>
</head>
<body>
<form:form id="frmBusqueda" commandName="control" action="${ctx}/biblioteca/biblio_buscar_usuario.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="tipoRetorno" id="tipoRetorno"/>
	<form:hidden path="ctrlUSE" id="ctrlUSE"/>
	<form:hidden path="ctrlCOD" id="ctrlCOD"/>
	<form:hidden path="ctrlNOM" id="ctrlNOM"/>	
	<form:hidden path="ventanaOrigen" id="ventanaOrigen"/>

	<!-- VARIABLES DE SELECCION -->
	<input type="hidden" name="codigo_s" id="codigo_s" />
	<input type="hidden" name="carnet_s" id="carnet_s" />
	<input type="hidden" name="nombre_s" id="nombre_s" />

<table cellpadding="0" cellspacing="0" style="margin-top: 4px; margin-left: 8px" border="0" >
		<tr>		
			<td><img src="${ctx}/images/Logistica\izq1.jpg" height="34px" id="tdProducto01"></td>			
			<td bgcolor=#048BBA class="opc_combo" height="34px" style="font-size: 12px;">
				DATOS DEL USUARIO
			</td>
			<td><img src="${ctx}/images/Logistica\der1.jpg" height="34px" id="tdProducto02"></td>
		</tr>
</table>
	<table border="0" bordercolor="blue" cellpadding="0" cellspacing="0" width="98%">
		<tr>
			<td width="95%">	
				<table class="tabla" style="width:98%;margin-left:9px" background="${ctx}/images/biblioteca/fondoinf.jpg" cellspacing="6" cellpadding="0" border="0" >
					<tr>
						<td width="15%">Usuario :</td>
						<td width="15%">			
							<form:input path="codCarnet" id="txtCodCarnet" cssClass="cajatexto" cssStyle="width: 80%" />								
						</td>
						<td width="15%">Nombre :</td>
						<td width="25%">						
							<form:input path="nombre" id="txtNombre" cssClass="cajatexto" cssStyle="width: 80%" />							
						</td>
					</tr>
					<tr>
						<td width="15%">Apellido Paterno :</td>
						<td width="15%">			
							<form:input path="apellPaterno" id="txtApellPaterno" cssClass="cajatexto" cssStyle="width: 80%" />								
						</td>
						<td width="15%">Apellido Materno:</td>
						<td width="25%">										
							<form:input path="apellMaterno" id="txtApellMaterno" cssClass="cajatexto" cssStyle="width: 80%" />							
						</td>
					</tr>
					<tr>
						<td width="15%">&nbsp;</td>
						<td colspan="3">							
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/botones/buscar2.gif',1)">
								<img src="${ctx}/images/botones/buscar1.gif" alt="Buscar" width="138" height="22" align="absmiddle" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:buscarAlumno();">
							</a>&nbsp;
						</td>
					</tr>		
			  </table>
			</td>		
		</tr>
		<tr>
			<td width="95%">	
				<table cellpadding="0" cellspacing="0" style="margin-top: 4px; margin-left: 8px">
					<tr>					
						<td><img src="${ctx}/images/Logistica\izq1.jpg" height="34px" id="tdProducto03"></td>			
						<td bgcolor=#048BBA class="opc_combo" height="34px" style="font-size: 12px;">
							RESULTADO DE LA B&Uacute;SQUEDA
						</td>
						<td><img src="${ctx}/images/Logistica\der1.jpg" height="34px" id="tdProducto04"></td>
					</tr>
				</table>
				<!-- width:98%;margin-left:9px -->
				<div style="overflow: auto; height: 200px">
				<table cellpadding="6" cellspacing="0"  style="WIDTH: 95%;margin-top:0px;margin-bottom:5px;margin-left:9px;border: 1px solid #048BBA" border="0">
					<tr height="25px">
						<td class="grilla" width="5%" >Sel.</td>
						<td class="grilla" width="10%" align="center">C�digo</td>
						<td class="grilla" width="10%" align="center">Usuario</td>			
						<td class="grilla" width="55%">Nombre</td>
						<td class="grilla" width="20%">Tipo Usuario</td>
					</tr>														
					<c:forEach var="objAlumno" items="${control.listaAlumnos}" varStatus="loop">
							<c:choose>
								<c:when test="${loop.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
								<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
							</c:choose>							
							<td align="center">
								<!-- seleccion -->
								<input type="hidden" name="hid<c:out value="${loop.index}" />" id="hid<c:out value="${loop.index}" />" value="<c:out value="${objAlumno.nombreAlumno}" />" />
								<input type="radio" ID="Radio3" NAME="Radio" onclick="seleccionar_alumno('<c:out value="${objAlumno.codAlumno}" />','<c:out value="${objAlumno.codCarnet}" />','<c:out value="${objAlumno.nombreAlumno}" />')" >
							</td>
							<td align="center">
								<!-- c�digo -->
								&nbsp;<c:out value="${objAlumno.codAlumno}" />
							</td>
							<td align="left">
								<!-- c�digo de carnet -->
								<strong>
									<c:out value="${objAlumno.codCarnet}" />
								</strong>
							</td>
							<td align="left">
								<!-- nombre -->
								<c:out value="${objAlumno.nombreAlumno}" />
							</td>
							<td align="left">
								<!-- nombre -->
								<c:out value="${objAlumno.tipo}" />
							</td>
							
						</tr>						
					</c:forEach>	
				</table>
				</div>	
				<table align=right  border="0" >
					<tr>
						<td>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/aceptar2.jpg',1)">
							<img alt="Grabar" src="${ctx}/images/botones/aceptar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_aceptar();"></a></td>
						<td>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
								<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;">
							</a></td>
					</tr>
				</table>
		
			</td>
		</tr>
	</table>
	
</form:form>
</body>
</html>