<%@ include file="/taglibs.jsp"%>
<head>
<script language="javascript">
function onLoad(){
}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_gestion_material.html">
	<form:hidden path="txhCodigoUnico" id="txhCodigoUnico" />
			
	<table style="width:99%; margin-left:3px" border="0" cellspacing="0" cellpadding="1" class="borde_tabla" height="50px" >
		<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%" colspan="4">
				Historial de Pr�stamos
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td>C�digo :</td>
			<td>				
				<form:input path="txtCodigo" id="txtCodigo" size="20" cssClass="cajatexto_1" readonly="readonly" />
			</td>
			<td>Tipo Material :</td>
			<td>				
				<form:input path="txtTipoMaterial" id="txtTipoMaterial" size="30" cssClass="cajatexto_1" readonly="readonly" />
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td>Titulo :</td>
			<td colspan=4>				
				<form:input path="txtTitulo" id="txtTitulo" size="70" cssClass="cajatexto_1" readonly="readonly" />
			</td>			
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1" width="99%" border="0"  class="" style="margin-left:3px">
		<tr>
			 <td align="left">
			 
				<iframe id="iFrame" name="iFrame" frameborder="0" align="left"
					height="300px" style="width:100%;margin-left:0px;"
					src="/biblioteca/historial.html?txhCodigoUnico=${control.txhCodigoUnico}" >
				</iframe>
	
				<!-- IFRAME -->
			</td>
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/cerrar2.jpg',1)">
				<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:window.close();"></a></td>
		</tr>
	</table>
</form:form>		
</body>