<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
	var strCod = "";			
		function onLoad(){
			document.getElementById("matTitulo").readOnly=true;
		}
		
		function fc_SeleccionarRegistro(srtCodUnico, srtNroIngreso, srtId, srtFechaReserva, srtDewey)
		{ //alert(srtCodUnico+">><<"+srtNroIngreso+">><<"+srtId+">><<"+srtFechaReserva);
		  strCod = srtCodUnico;
		  
		  document.getElementById("txhCodMaterial").value=srtNroIngreso;
		  document.getElementById("txhFechaReserva").value=srtFechaReserva;
		  strCodSel =document.getElementById(srtId).value;
		  ArrCodSel = strCodSel.split("|");
		  document.getElementById("txhTipoMaterial").value=ArrCodSel[0];
		  document.getElementById("txhTitulo").value=ArrCodSel[1];
		  document.getElementById("txhDewey").value=srtDewey;
		 		  
		}
		function fc_Aceptar(){
			srtFechaFin=document.getElementById("txhFechaBD").value;
			srtFechaInicio=document.getElementById("txhFechaReserva").value;
		 	num=fc_ValidaFechaIniFechaFin(srtFechaInicio,srtFechaFin); //0
		 
			if(document.getElementById("txhCodMaterial").value!=""){
				if(num==0){
					window.opener.document.getElementById("txtNroIngreso").value=document.getElementById("txhCodMaterial").value;
			      	window.opener.document.getElementById("txtTipomaterial").value=document.getElementById("txhTipoMaterial").value;
			      	window.opener.document.getElementById("txtTitulo").value=document.getElementById("txhTitulo").value;
			      	window.opener.document.getElementById("txhFlgReserva").value = strCod;
			      	window.opener.document.getElementById("txtCodUsuarioReserva").value=document.getElementById("codUsuario").value;
			      	window.opener.document.getElementById("txhCodDewey").value=document.getElementById("txhDewey").value;
			      	window.close();
			    }
			    else  alert(mstrProblemaEntreFechas);
			}
			else alert(mstrSeleccione);
		}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_prestamo_flotante.html">

	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="codMaterial" id="txhCodMaterial" />
	<form:hidden path="tipoMaterial" id="txhTipoMaterial" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="fechaBD" id="txhFechaBD" />
	<form:hidden path="codUnico" id="txhCodUnico" />
	<form:hidden path="fechaReserva" id="txhFechaReserva" />
	<form:hidden path="titulo" id="txhTitulo" />
	<form:hidden path="codUsuario" id="codUsuario" />
	<form:hidden path="dewey" id="txhDewey" />

	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="margin-left: 11px; margin-top: 10px">
		<tr>
			<td align="left"><img
				src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
			<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg"
				width="240px" class="opc_combo"><font style="">Consultar
			Reservas</font></td>
			<td align="right"><img
				src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		</tr>
	</table>

	<table class="tabla" background="${ctx}\images\biblioteca\fondosup.jpg"
		border="0" bordercolor="red"
		style="width: 97%; margin-left: 11px; margin-top: 0px; height: 65px"
		cellspacing="2" cellpadding="2">

		<tr>
			<td width="10%">&nbsp;Nro.Ingreso :</td>
			<td><form:input path="matNroIngreso" id="matNroIngreso"
				cssStyle="text-align:left;" readonly="true" cssClass="cajatexto_1"
				size="15" /></td>
		</tr>
		<tr>
			<td>&nbsp;T�tulo :</td>
			<td><form:textarea path="matTitulo" id="matTitulo" cssClass="cajatexto_1" cols="110" rows="2"/></td>
		</tr>
		<tr>
			<td>&nbsp;Usuario :</td>
			<td><form:input path="usuarioLogin" id="usuarioLogin"
				cssStyle="text-align:left;" readonly="true" cssClass="cajatexto_1"
				size="15" />&nbsp;&nbsp; <form:input path="dscUsuario"
				id="dscUsuario" readonly="true" cssClass="cajatexto_1" size="84" />
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" id="Table1" width="97%"
		border="0" style="margin-left: 11px; margin-top: 3px;" bordercolor="blue">
		<tr>
			<td align="left">
			<div
				style="overflow: auto; height: 270px; width: 99.3%; border: 0px solid #048BBA;">
			<display:table name="sessionScope.listaBandeja" cellpadding="0"
				cellspacing="1"
				decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"
				pagesize="10" requestURI=""
				style="border: 1px solid #048BBA;width:98%;">
				<display:column property="rbtSelTipoMaterial" title="Sel."
					headerClass="grilla" class="tablagrilla"
					style="text-align:center; width:5%" />
				<display:column property="isbn" title="Nro.<br>Ingreso"
					headerClass="grilla" class="tablagrilla"
					style="text-align:center;width:10%" />
				<display:column property="tipoMaterial" title="Tipo Material"
					headerClass="grilla" class="tablagrilla"
					style="text-align:left;width:10%" />
				<display:column property="titulo" title="T&iacute;tulo"
					headerClass="grilla" class="tablagrilla"
					style="text-align:left;width:25%" />
				<display:column property="fechaBD" title="Fecha<br>Reserva"
					headerClass="grilla" class="tablagrilla"
					style="text-align:center;width:10%" />
				<display:column property="usuarioReserva" title="Usuario"
					headerClass="grilla" class="tablagrilla"
					style="text-align:center;width:10%" />
				<display:column property="nomUsuario" title="Nombre"
					headerClass="grilla" class="tablagrilla"
					style="text-align:center;width:20%" />

				<display:setProperty name="basic.empty.showtable" value="true" />
				<display:setProperty name="basic.msg.empty_list_row"
					value="<tr class='tablagrilla'><td colspan='7' align='center'>No se encontraron registros</td></tr>" />
				<display:setProperty name="paging.banner.placement" value="bottom" />
				<display:setProperty name="paging.banner.item_name"
					value="<span >Registro</span>" />
				<display:setProperty name="paging.banner.items_name"
					value="<span>Registros</span>" />
				<display:setProperty name="paging.banner.no_items_found"
					value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
				<display:setProperty name="paging.banner.one_item_found"
					value="<span class='cajatexto-login'>Un registro encontrado </span>" />
				<display:setProperty name="paging.banner.all_items_found"
					value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
				<display:setProperty name="paging.banner.some_items_found"
					value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
				<display:setProperty name="paging.banner.full"
					value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.first"
					value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.last"
					value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />
				<display:setProperty name="paging.banner.onepage"
					value="<span class='cajatexto-login'>{0}</span>" />

			</display:table></div>
			</td>
		</tr>
	</table>
	<table align="center" style="margin-top: 5px;">
		<tr>
			<td><a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/aceptar2.jpg',1)">
			<img alt="Aceptar" src="${ctx}/images/botones/aceptar1.jpg"
				id="imggrabar" onclick="javascript:fc_Aceptar();"
				style="cursor: pointer;"></a></td>
			<td><a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cerrar2.jpg',1)">
			<img alt="Cancelar" src="${ctx}/images/botones/cerrar1.jpg"
				onclick="window.close();" ID="Button	1" NAME="Button1"
				style="cursor: pointer;"></a></td>
		</tr>
	</table>
</form:form>
</body>
</html>