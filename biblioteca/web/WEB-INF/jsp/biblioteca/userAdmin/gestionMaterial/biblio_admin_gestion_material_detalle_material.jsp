<%@page import="java.util.List"%> 
<%@page import="com.tecsup.SGA.bean.DetalleMaterialFichaBibliograficaBean;"%>
<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
function onLoad(){
	if(document.getElementById("txhFlg").value=="1"){
		
	}
}

function fc_Cerrar(){
	if(document.getElementById("txhFlg").value=="1"){ //debe regresar
		location.href="${ctx}/biblioteca/bib_busqueda_simple.html?"+"<%=(( request.getAttribute("strUrlNuevo")==null)?"": request.getAttribute("strUrlNuevo"))%>"+
						"&strUrl="+"<%=(( request.getAttribute("strUrl")==null)?"": request.getAttribute("strUrl"))%>";
	}
	else if(document.getElementById("txhFlg").value=="2"){ //debe regresar
		location.href="${ctx}/biblioteca/bib_busqueda_avanzada_flotante.html?"+"<%=(( request.getAttribute("strUrlNuevo")==null)?"": request.getAttribute("strUrlNuevo"))%>"+
						"&strUrl="+"<%=(( request.getAttribute("strUrl")==null)?"": request.getAttribute("strUrl"))%>";
	}
	else{
		window.close();
	}
}

</script>
</head>
<body>

<form:form action="/biblioteca/biblioteca/detalle_material.html" method="post">
<input id="txhFlg" name="txhFlg" type="hidden" value="<%=(( request.getAttribute("flg")==null)?"": request.getAttribute("flg"))%>"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" 
		style="margin-left:5px; margin-top:3px; display:;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo"><font style="">Detalle Material Did�ctico</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
	<%
	List lst = (List) request.getAttribute("OBJResultado"); 
	
	if( lst!=null && lst.size()>0 )
	{
		DetalleMaterialFichaBibliograficaBean obj = (DetalleMaterialFichaBibliograficaBean) lst.get(0);
		
		%>

  
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:98%; margin-left:5px; margin-right:5px" border="0" 
		cellspacing="4" cellpadding="2" class=grilla height="50px" bordercolor="red" align="center">		
		<tr>
		    <td width="16%" align="left">&nbsp;&nbsp;&nbsp;C�digo :</td>
		    <td>
		    	<input type="text" class=ficha value="<%=obj.getCodigoGenerado()==null?"":obj.getCodigoGenerado()%>" readonly="readonly" style="width: 98%;">
		    </td>
		    <td width="5%">&nbsp;</td>
		    <td align="left" width="16%">&nbsp;Tipo Material :</td>
		    <td align="left">		    
		    	<input type="text" class="ficha" value="<%=obj.getNomTipoMaterial()==null?"":obj.getNomTipoMaterial()%>" readonly="readonly" style="width: 90%;">		    	
		    </td>
		</tr>				
		<tr>
		    <td align="left">&nbsp;&nbsp;&nbsp;Autor :</td>
		    <td colspan="4" align="left">
		    	<textarea rows="2" style="width: 95%" class="ficha" readonly><%=obj.getNomAutor()==null?"":obj.getNomAutor()%></textarea>
		    </td>
		</tr>
		<tr>
		    <td align="left">&nbsp;&nbsp;&nbsp;Clasificaci�n :</td>
		    <td colspan="4" align="left">		    
		    	<input type="text" class="ficha" value="<%=obj.getNomDewey()==null?"":obj.getNomDewey()%>" readonly="readonly" style="width: 95%;">		    	
		    </td>
		</tr>
		<tr>			
		    <td align="left">&nbsp;&nbsp;&nbsp;T�tulo :</td>
		    <td colspan="4" align="left">
		    	<textarea rows="2" style="width: 95%" class="ficha" readonly><%=obj.getNomMaterial()==null?"":obj.getNomMaterial()%></textarea>
		    </td>		    			
		</tr>
		<tr style="display: none;">
		    <td align="left">&nbsp;&nbsp;&nbsp;Total Ejemplares :</td>
		    <td>
		    	<input type="text" class="ficha" value="<%=obj.getNroEjemplares()==null?"":obj.getNroEjemplares()%>" readonly="readonly" style="width: 40%;">		    
		    </td>
		    <td>&nbsp;</td>
		    <td>
		    	&nbsp;			
			</td>		    
		</tr>
		<tr>
			<td align="left">&nbsp;&nbsp;&nbsp;Pie de Imprenta :</td>
		    <td colspan="4" align="left">
		    	<input type="text" class="ficha" value="<%=obj.getDscCiudad()==null?"":obj.getDscCiudad()%> / <%=obj.getNomEditorial()==null?"":obj.getNomEditorial()%> / <%=obj.getFecPublicacion()==null?"":obj.getFecPublicacion()%>" readonly="readonly" style="width: 95%;">
		    </td>
		</tr>
		<tr>
			<td align="left">&nbsp;&nbsp;&nbsp;Contenido :</td>
			<td colspan="4" align="left">
				<textarea rows="2" style="width: 95%" class="ficha" readonly><%=obj.getContenido()==null?"":obj.getContenido()%></textarea>
			</td>
		</tr>
		<tr>
		    <td style="display: none;" align="left">&nbsp;Nro Edici�n :</td>
		    <td style="display: none;">
		    	<input type="text" class="ficha" value="<%=obj.getNroEdicion()==null?"":obj.getNroEdicion()%>" readonly="readonly" style="width: 40%;">
		    </td>
		</tr>
		<tr>
			<td align="left">&nbsp;&nbsp;&nbsp;Descripci�n &nbsp;&nbsp;&nbsp;F�sica :</td>
			<td colspan="4" align="left">
				<textarea rows="2" style="width: 95%" class="ficha" readonly><%=obj.getUbicacionMaterial()==null?"":obj.getUbicacionMaterial()%></textarea>
			</td>
		</tr>		
		<tr>
		    <td align="left">&nbsp;&nbsp;&nbsp;Serie :</td>
		    <td align="left">
		    	<input type="text" class="ficha" value="<%=obj.getMensionSeries()==null?"":obj.getMensionSeries()%>" readonly="readonly" style="width: 95%;">
		    </td>
		    <td>&nbsp;</td>
		    <td align="left">&nbsp;
		    <% if (obj.getCodTipoMaterial().trim().equals("0002")) {%>
		    ISSN :
		    <% } else {%>
		    ISBN :
		    <% } %>
		    </td>
		    <td align="left"><input type="text" class="ficha" value="<%=obj.getNroIsbn()==null?"":obj.getNroIsbn()%>" readonly="readonly" style="width: 90%;">
		    </td>
		</tr>
		
		<tr><td align="left">&nbsp;&nbsp;&nbsp;Descriptores :</td>
			<td colspan="4" align="left">
				<textarea rows="2" style="width: 95%" class="ficha"  readonly><%=obj.getDscDescriptores()==null?"":obj.getDscDescriptores()%></textarea>
			</td>
		</tr>
		
		<tr><td align="left">&nbsp;&nbsp;&nbsp;Nro. Ingresos :</td>
			<td colspan="4" align="left">
				<textarea rows="2" style="width: 95%" class="ficha" readonly><%=obj.getNroIngresos()==null?"":obj.getNroIngresos()%></textarea>
			</td>
		</tr>
		
		<tr>
			<td align="left">&nbsp;&nbsp;&nbsp;Idioma :</td>
		    <td align="left">
		    	<input type="text" class="ficha" value="<%=obj.getNomIdioma()==null?"":obj.getNomIdioma()%>" readonly="readonly" style="width: 95%;">
		    </td>
		    <td>&nbsp;</td>
		    <td align="left">&nbsp;Nro. Ejemplares :</td>
		    <td align="left"><input type="text" class="ficha" value="<%=obj.getNroEjemplares()==null?"":obj.getNroEjemplares()%>" readonly="readonly" style="width: 40%;">
		    </td>
		</tr>
		<tr>
		    <td align="left">&nbsp;&nbsp;&nbsp;Pr�stamo :</td>
		    <td align="left">
		    	<input type="checkbox" disabled="disabled" name="prestamo" <%=obj.getIndSala()==null?"":("1".equalsIgnoreCase(obj.getIndSala())?" checked=checked ":"")%> id="sala"  >Sala
		    	<input type="checkbox" disabled="disabled" name="domicilio" id="domicilio" "<%=obj.getIndDomicilio()==null?"":("1".equalsIgnoreCase(obj.getIndDomicilio())?" checked=checked ":" ")%>" >Domicilio
		    </td>
		</tr>
	</table>
	<%	}
	%>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar" onclick="javascript:fc_Cerrar();" style="cursor:pointer;"></a>
				
			</td>
		</tr>
	</table>
</form:form>
</body>		