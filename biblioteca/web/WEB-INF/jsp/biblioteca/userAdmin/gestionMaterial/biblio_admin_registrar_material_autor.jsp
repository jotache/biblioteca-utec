<%@ include file="/taglibs.jsp"%>

<head>

<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
<script language=javascript>
function onLoad(){
}

function fc_limpiar(){
	document.getElementById("txtCodigo").value  ="";
	document.getElementById("txtAutores").value ="";
	document.getElementById("cboTipo").value ="";	
}

function fc_buscar(){
if(fc_Trim(document.getElementById("txtCodigo").value)!=""
	|| fc_Trim(document.getElementById("txtAutores").value)!=""	)
	{
		document.getElementById("operacion").value="irConsultar";
		document.getElementById("frmMain").submit();
	}else
		alert("Seleccione criterio de busqueda");
}

function fc_aceptar(){

if(fc_Trim(document.getElementById("nombre").value)!=""
	&& fc_Trim(document.getElementById("codigo").value)!=""	)
	{
		//window.opener.document.getElementById("txtAutor").value = document.getElementById("codigo").value+"-"+document.getElementById("nombre").value;
		window.opener.document.getElementById("txtAutor").value = document.getElementById("nombre").value;
		window.opener.document.getElementById("txhCodigoAutor").value = document.getElementById("codigo").value;
		window.close();
	}else
		alert("Seleccione Autor");	
}

function fc_selecciona(cod,nom){
	document.getElementById("codigo").value = cod;
	document.getElementById("nombre").value = document.getElementById("hid"+nom).value;
}

function fc_AgregarAutor(){
	var winl = (screen.width - parseInt("450")) / 2;	
	var wint = (screen.height - parseInt("160")) / 2;
	srtCodUsuario = document.getElementById("txhCodUsuario").value;
	window.open("${ctx}/biblioteca/bib_autores_agregar.html?txhCodigo=&txhCodValor=" +
		"&txhCodPeriodo=&txhCodUsuario=" + srtCodUsuario,"","left=" + winl + ",top=" + wint + ",width=450,height=185");
}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_material_autor.html">
	<form:hidden path="operacion" id="operacion" />
	
	<form:hidden path="txhCodUsuario" id="txhCodUsuario" />
	
	<!-- VARIABLES DE SELECCION -->
	<input type="hidden" name="codigo" id="codigo" />
	<input type="hidden" name="nombre" id="nombre" />
	
	<form:hidden path="codPersona" id="txhCodPersona"/>
	<form:hidden path="codEmpresa" id="txhCodEmpresa"/>	
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="180px" class="opc_combo"><font style="">Busqueda de Autor</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>	
	<table><tr height="3px"><td></td></tr></table>	
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:99%; margin-left:3px" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red">
		<tr>
			<td width="10%">C�digo:</td>
			<td width="10%">				
				<form:input path="txtCodigo" id="txtCodigo" 
					onblur="fc_ValidaNumeroOnBlur('txtCodigo');"
					cssClass="cajatexto" 
				 	onkeypress="fc_PermiteNumeros();"  size="6" maxlength="5" />
			</td>
			<td width="10%">Autor:</td>
			<td width="30%">				
				<form:input path="txtAutores" id="txtAutores"
					cssClass="cajatexto"
			   		 maxlength="40" cssStyle="width: 95%" />
			</td>
			<td width="10%">Tipo:
			</td>
			<td width="15%">
				<form:select path="codTipo" id="cboTipo" cssClass="cajatexto" cssStyle="width:95%">
					<form:option value="">--Todos--</form:option>
					<form:option value="${control.codPersona}">Persona</form:option>
					<form:option value="${control.codEmpresa}">Empresa</form:option>
				</form:select>
			</td>
			<td width="15%" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_limpiar();" ></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_buscar();"></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" align="absmiddle" onclick="javascript:fc_AgregarAutor();" id="imgAgregar" style="cursor:pointer" alt="Agregar">
					</a>				
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1" width="99%" border="0"  class="" style="margin-left:3px">
		<tr>
			 <td align="left">
			 	<div style="overflow: auto; height: 250px;width:100%">
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:97%;">
					<tr>
						<td class="grilla" width="5%">Sel.</td>
						<td class="grilla" width="8%">C�digo</td>
						<td class="grilla" width="77%">Autor</td>
						<td class="grilla" width="10%">Autor</td>
					</tr>
				<c:set var="contItems" value="0"/>						
				<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
					<tr class="tablagrilla">
						<c:set var="contItems" value="${contItems+1}" />
						<input type="hidden" name="hid<c:out value="${loop.index}" />" id="hid<c:out value="${loop.index}" />" value="<c:out value="${objCast.descripcion}" />" />
						<td align="center">
							<input type="radio" ID="Radio3" NAME="Radio" onclick="fc_selecciona('<c:out value="${objCast.codTipoTablaDetalle}" />','<c:out value="${loop.index}" />')" >
						</td>
						<td align="center">
							<c:out value="${objCast.dscValor2}" />
						</td>
						<td align="left">
							<c:out value="${objCast.descripcion}" />
						</td>
						<td align="center">
							<c:if test='${objCast.dscValor3=="0001"}'>
								Persona
							</c:if>
							<c:if test='${objCast.dscValor3=="0002"}'>
								Empresa
							</c:if>		
						</td>					
					</tr>
				</c:forEach>	
				<c:if test="${control.lstResultado!=null && contItems==0}">
					<tr class="tablagrilla">
						<td colspan="4" align="center">No se encontraron registros</td>
					</tr>
				</c:if>
											
				</table>
				</div>
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=right>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/aceptar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/aceptar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_aceptar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
	</table>		
	
</form:form>
</body>
