<%@ include file="/taglibs.jsp"%>   

<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;"	type="text/JavaScript"></script>
	<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;"	type="text/JavaScript"></script>
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter"/>
	<script language=javascript>
	function onLoad(){
		var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
		if(mensaje!="")
			alert(mensaje);
			
		fc_FecRes();
		Fc_BusquedaPor();
	}
  

	function fc_getParametrosBusqueda(){
	
		strParametrosBusqueda = "prmTxtCodigo="+document.getElementById("txtCodigo").value+
		"&prmTxtNroIngreso="+document.getElementById("txtNroIngreso").value+
		"&prmCboTipoMaterial="+document.getElementById("cboTipoMaterial").value+
		"&prmCboBuscarPor="+document.getElementById("cboBuscarPor").value+
		"&prmTxtTitulo="+document.getElementById("txtTitulo").value+
		"&prmCboIdioma="+document.getElementById("cboIdioma").value+
		"&prmCboAnioIni="+document.getElementById("cboAnioIni").value+
		"&prmCboAnioFin="+document.getElementById("cboAnioFin").value+
		"&prmTxtFechaReservaIni="+document.getElementById("txtFechaReservaIni").value+
		"&txhSedeSel="+document.getElementById("txhSedeSel").value+
		"&txhSedeUsuario="+document.getElementById("txhSedeUsuario").value+
		"&prmTxtFechaReservaFin="+document.getElementById("txtFechaReservaFin").value;
		
		return strParametrosBusqueda; 
	}

	function fc_Agregar()
	{		
		location.href="${ctx}/biblioteca/biblio_admin_registrar_material.html?"+
		"txhCodUsuario="+document.getElementById("usuario").value+	
		"&"+fc_getParametrosBusqueda();
		
	}
	
	function fc_Prestamo(){
		window.location.href="${ctx}/biblioteca/biblio_admin_registrar_prestamo.html?"+
		"txhCodUsuario="+document.getElementById("usuario").value+"&"+fc_getParametrosBusqueda();
	}

	function fc_Devolucion(){
		window.location.href="${ctx}/biblioteca/biblio_admin_registrar_devolucion.html?"+
		"txhCodUsuario="+document.getElementById("usuario").value+"&"+fc_getParametrosBusqueda()+"&txhTipoUsuario="+document.getElementById("tipoUsuario").value;
	}
	
	function fc_Sanciones(){
		window.location.href="${ctx}/biblioteca/biblio_admin_sanciones.html?"+
		"txhCodUsuario="+document.getElementById("usuario").value+"&"+fc_getParametrosBusqueda();		
	}
	
	function fc_Regresar(){
		location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;
	}
	
	function Fc_BusquedaPor(){
		nameObj = "cboBuscarPor";
		titulo.innerText=" "+document.getElementById(""+nameObj).options[document.getElementById(""+nameObj).selectedIndex].text;	
	}

	function fc_limpiar(){
		document.getElementById("txtCodigo").value="";
		document.getElementById("txtNroIngreso").value="";
		document.getElementById("cboTipoMaterial").value="";
		document.getElementById("cboBuscarPor").value="0001";
		document.getElementById("txtTitulo").value="";
		document.getElementById("cboIdioma").value="";
		document.getElementById("cboAnioIni").value="";
		//document.getElementById("cboAnioFin").value="";
		document.getElementById("txtFechaReservaIni").value="";
		document.getElementById("txtFechaReservaFin").value="";
		
		document.getElementById("chkConsultaReserva").checked = false;
		divFechaReserva.style.display = "none";	
	
	}

	function validaTexto(){
		
		//fc_ValidaTextoEspecial();
		if(document.getElementById("txtTitulo").value.length > 244)
		{
			str = document.getElementById("txtTitulo").value.substring(0,244);
			document.getElementById("txtTitulo").value=str;
			return false;
		}			
		
	}

	function fc_Calendario(op){
		if(op == '1'){
			Calendar.setup({
			inputField     :    "txtFechaReservaIni",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFechaIni",
			singleClick    :    true
	
			});
		}else if(op == '2'){
			Calendar.setup({
			inputField     :    "txtFechaReservaFin",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFechaFin",
			singleClick    :    true
	
			});
		}	
	}
	function fc_Consultar(){	
		if(fc_valida()){			
			$('#operacion').val('irConsultar')
			$('#frmMain').submit();			
		}
	}
	function fc_Exportar(){	
		if(fc_valida()){
			$('#operacion').val('irExportar');	
			$('#frmMain').submit();
			
		}
	}
	function fc_valida(){
		
		if(document.getElementById("cboBuscarPor").value=="")
		{	alert(mstrSeleccioneCriterio);
			document.getElementById("cboBuscarPor").focus();
			return 0;
		}
		objFecIni = document.getElementById("txtFechaReservaIni");
		objFecFin = document.getElementById("txtFechaReservaFin");
			
		if ( objFecIni.value != "" || objFecFin.value != "" )
		{
			if ( objFecIni.value == "" || objFecFin.value == "" )
			{	alert(mstrFechasObligatoria);
				return false;
			}
				
			if (Fc_RestaFechas(objFecIni.value,"dd/MM/yyyy",objFecFin.value,"dd/MM/yyyy") < 0 )
			{
				alert("Fecha de inicio debe ser mayor a fecha fin.");
				objFecIni.focus();
				return 0;
			}else
				return 1;
		}			
		return 1;
	}

	function Fc_Cambia()
	{
	if(cboTipoApl.value=='02'){location.href="Configuracion_Bandeja_TS.htm";}
	}
	
	function Fc_VerReserva()
	{
		Fc_Popup("Consultar_Reservas.htm",600,250);
	}

	function fc_FecRes(){
		if(document.getElementById("chkConsultaReserva").checked == true){
			divFechaReserva.style.display = "inline-block";	
		}
		else {divFechaReserva.style.display = "none";}
	}

</script>
</head>
<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_gestion_material.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="idEliminacion" id="idEliminacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="tipoUsuario" id="tipoUsuario"/>
	<form:hidden path="sedeSel" id="txhSedeSel"/>
	<form:hidden path="sedeUsuario" id="txhSedeUsuario"/>


	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>	
		
	<table border="0" cellspacing="0" cellpadding="0" class="borde_tabla" style="width:94%;height:70px;margin-left: 11px;">

			<tr class="fondo_cabecera_azul">		 
			 	<td class="titulo_cabecera" width="100%" colspan="7">
					Gesti�n de Material Did�ctico
				</td>
			</tr>
		
			<tr class="fondo_dato_celeste">
				<td id="Select3" name="Select3">&nbsp;C&oacute;digo :</td>
				<td>&nbsp;<form:input path="txtCodigo" id="txtCodigo"
					cssClass="cajatexto" 
					 maxlength="30" size="28"/>
				</td>
				<td width="2%">&nbsp;</td>
				<td ID="Select3" name="Select3">&nbsp;Nro. Ingreso :</td>
				<td>&nbsp;<form:input path="txtNroIngreso" id="txtNroIngreso" maxlength="10"
					cssClass="cajatexto" onblur="fc_ValidaNumeroOnBlur('txtNroIngreso');" 
					 onkeypress="fc_PermiteNumeros();" /></td>
				<td nowrap="nowrap">&nbsp;Tipo Material :</td>
				<td>&nbsp;<form:select path="cboTipoMaterial" id="cboTipoMaterial"
					cssClass="cajatexto" cssStyle="width:180px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.lstTipoMaterial!=null}">
						<form:options itemValue="codTipoTablaDetalle"
							itemLabel="descripcion" items="${control.lstTipoMaterial}" />
					</c:if>
				</form:select></td>
			</tr>
			<tr class="fondo_dato_celeste">
				<td nowrap="nowrap">&nbsp;Buscar por :</td>
				<td>&nbsp;<form:select path="cboBuscarPor" id="cboBuscarPor"
					cssClass="combo_o" cssStyle="width:167px" onchange="Fc_BusquedaPor('cboBuscarPor')" >				
					<c:if test="${control.lstBuscarPor!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
							items="${control.lstBuscarPor}" />
					</c:if>
					</form:select>
				</td>
				<td rowspan="2">&nbsp;</td>
				<td rowspan="2" id="titulo" name="Select3" valign="top">&nbsp;Autor :</td>
				<td colspan="4" rowspan="2">&nbsp;<form:textarea path="txtTitulo"
					id="txtTitulo" cssClass="cajatexto"
					cssStyle="width:467px;height:50px;" onkeypress="validaTexto()"/></td>
			</tr>
			<tr class="fondo_dato_celeste">
				<td>&nbsp;Idioma :</td>
				<td>&nbsp;<form:select path="cboIdioma" id="cboIdioma"
					cssClass="cajatexto" cssStyle="width:167px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.lstIdioma!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
							items="${control.lstIdioma}" />
					</c:if>
				</form:select></td>
			</tr>
			<tr class="fondo_dato_celeste">
				<td>&nbsp;A�o :</td>
				<td>&nbsp;<form:input path="cboAnioIni" id="cboAnioIni" maxlength="10"
						cssClass="cajatexto"  />
					<form:hidden path="cboAnioFin" id="cboAnioFin"	/>
				</td>
				<td>&nbsp;</td>
				<td colspan="4" nowrap="nowrap">
					<table width="100%" border="0" bordercolor="blue" cellspacing="0" cellpadding="0">
						<tr>
							<td width="30%">
								&nbsp;Consultar Reservas: <form:checkbox id="chkConsultaReserva" path="chkConsultaReserva" value="1" onclick="fc_FecRes();"/>
							</td>
							<td width="55%"><div id="divFechaReserva" style="display:none;">
								&nbsp;Fecha Reserva :&nbsp;
								<form:input path="txtFechaReservaIni"
									id="txtFechaReservaIni" cssStyle="width:60px" cssClass="cajatexto"  maxlength="10"  
									onkeypress="fc_ValidaFecha('txtFechaReservaIni')" onblur="fc_ValidaFechaOnblur('txtFechaReservaIni')"  />
									<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaIni','','${ctx}/images/iconos/calendario2.jpg',1)">
										<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaIni"
											align="absmiddle" alt="Calendario" style="cursor:pointer;" onclick="fc_Calendario('1');"></a>
								&nbsp;-&nbsp; 
								<form:input path="txtFechaReservaFin"
									id="txtFechaReservaFin" cssStyle="width:60px" cssClass="cajatexto" maxlength="10"  
										onkeypress="fc_ValidaFecha('txtFechaReservaFin')" onblur="fc_ValidaFechaOnblur('txtFechaReservaFin')"  />
									
										<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaFin','','${ctx}/images/iconos/calendario2.jpg',1)">
												<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaFin"
													align="absmiddle" alt="Calendario" style="cursor:pointer;" onclick="fc_Calendario('2')"></a>&nbsp;
					</div>
					<td width="15%" align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
								<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_limpiar();" ></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
										<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_Consultar();"></a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExportar','','${ctx}/images/iconos/exportarex2.jpg',1)">
										<img src="${ctx}/images/iconos/exportarex1.jpg" align="absmiddle" alt="Exportar" id="imgExportar" style="CURSOR: pointer" onclick="javascript:fc_Exportar();"></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
					
	<table><tr><td height="5px"></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1"
		style="width: 1000px" class="tabla" border="0" >
		<tr>
			<td>				
				<iframe id="iFrameGrilla" name="iFrameGrilla" frameborder="0" height="380px" width="99%" 
					src="${ctx}/biblioteca/biblio_admin_gestion_material_paginado.html?txhCodUsuario=${control.usuario}&txhSedeSel=${control.sedeSel}&txhSedeUsuario=${control.sedeUsuario}" >			
				</iframe>				
			</td>
		</tr>
	</table>	
	
</form:form>
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "txtFechaReservaIni",
	ifFormat       :    "%d/%m/%Y",
	daFormat       :    "%d/%m/%Y",
	button         :    "imgFechaIni",
	singleClick    :    true

	});
                          
</script>
</body>
