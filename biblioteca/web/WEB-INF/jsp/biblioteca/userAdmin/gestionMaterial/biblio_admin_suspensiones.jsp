<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/js/funciones_bib-fechas.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad(){
		var objMsg = "<%=((request.getAttribute("Msg")==null)?"":(String)request.getAttribute("Msg"))%>";
		if ( objMsg == "OK" ){		   						
			window.opener.fc_buscarSanciones();			
			alert('Servicios inhabilitados satisfactoriamente');
			window.close();			
		}		
		else if ( objMsg== "ERROR" ){		
			alert('Ocurri un problema al inhabilitar servicios. Comun�quese con el Administrador');			
		}      
	}

	function validar(){
		
		if(fc_Trim(document.getElementById("txtFecIni").value)==""){
			alert('Seleccione una fecha de Inicio');
			return false;
		}
		if(fc_Trim(document.getElementById("txtFecFin").value)==""){
			alert('Seleccione una fecha de Fin');
			return false;
		}		
		
		var fechaIni = document.getElementById("txtFecIni").value;
		var fechaFin = document.getElementById("txtFecFin").value;
	    var num=fc_ValidaFechaIniFechaFin(fechaIni,fechaFin);
	    var srtFecha='';
	    if(num==1) 	srtFecha='0';
	    else srtFecha='1';	    
	     
	    if(srtFecha=='0')  		    	
		{
			alert('La fecha de inicio debe ser menor a la fecha final.');
			return false;
		}
		
		return true;			
	}
				
	function fc_Grabar(){
	    nroSelec= fc_GetNumeroSeleccionados();	   
		if(nroSelec != "" ){
			if(validar()){
				if(confirm('�Seguro de inhabilitar los servicios seleccionados?')){		
					document.getElementById("txhNroSelec").value = nroSelec;	
				   	document.getElementById("txhOperacion").value = "SUSPENDER";
					document.getElementById("frmMain").submit();		    
				}
			}
		}
		else alert(mstrSeleccione);			
	}

	function fc_seleccionarRegistro(strCodNIvel1){			
		strCodSel = document.getElementById("txhCodSancionesSeleccionados").value;
		flag=false;
		if(strCodSel!='')
		{
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";
		
			for (i=0;i<=ArrCodSel.length-2;i++)
			{
				if (ArrCodSel[i] == strCodNIvel1){ flag = true }
				else strCodSel = strCodSel + ArrCodSel[i]+'|';
			}
		}
		if(!flag) strCodSel = strCodSel + strCodNIvel1 + '|';		
		document.getElementById("txhCodSancionesSeleccionados").value = strCodSel;        
	}
		
	function fc_GetNumeroSeleccionados(){
		strCodSel = document.getElementById("txhCodSancionesSeleccionados").value;		
		if ( strCodSel != '' ) {		
			ArrCodSel = strCodSel.split("|");
			return ArrCodSel.length - 1;
		}
		else return 0;
	}
	
	function fc_TextAreaLimited(txtArea,limit){
		if(txtArea.value.length > limit){
			//alert("Lleg� al l�mite de n�mero de caracteres.");
			txtArea.value = txtArea.value.substring(0,limit);
		}
		//$F('txtUsuariologin') = limit-$F(txtArea).length;
	}
		
		

		
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_suspensiones.html">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codSancionesSeleccionados" id="txhCodSancionesSeleccionados"/>
<form:hidden path="codUsuario" id="codUsuario" />
<form:hidden path="nroSelec" id="txhNroSelec"/>
<form:hidden path="usuario" id="usuario" />

		<!--T�tulo de la P�gina  -->
		<table cellpadding="0" cellspacing="0" border="0" style="margin-left:5px; margin-top:0px">
			 <tr>
			 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
			 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo"><font style="">Suspensi�n de Servicio</font></td>
			 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
			 </tr>
		 </table>		
			

		<table background="${ctx}\images\biblioteca\fondosup.jpg" style="width: 90%; margin-left: 5px;" cellspacing="4" cellpadding="5" class="tabla" border="0" >
			<tr>
				<td width="10%">Usuario :</td>
				<td width="90%">	
					<form:input id="txtUsuariologin" path="txtUsuariologin" cssClass="cajatexto_o" 
						cssStyle="width:70px" readonly="true"/>
					&nbsp;
					<form:input id="txtUsuarioNombre" path="txtUsuarioNombre" cssClass="cajatexto_o" cssStyle="width:250px" readonly="true"/>			
				</td>																					
			</tr>
	    </table>


		<table cellpadding="0" cellspacing="0" ID="Table1" width="90%" border="0"  class="" style="margin-left:5px">
			<tr>
			 <td align="left">
				<div style="overflow: auto; height: 130px;width:100%">
				 	<display:table name="requestScope.listaSuspenciones" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleDecorator"  
						style="border: 1px solid #048BBA;width:100%">
					    <display:column property="chkSelSancion" title="Sel."  
								style="text-align:center; width:10%"/>
						<display:column property="descripcion" title="Tipo de Inhabilitaci�n" style="text-align:left;width:90%"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
							
					  </display:table>
					</div>
				</td>				
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" ID="tblFechas" width="90%" border="0" style="margin-left:5px">
			<tr height="24">
				<td colspan="2" class="cabecera">Rango de fechas de inhabilitaci&oacute;n</td>	
				<td colspan="2" class="cabecera">Motivo <small>(250 m�ximo)</small></td>									
			</tr>
			<tr>
				<td width="30%" class="cabecera">Fecha de inicio:&nbsp;</td>				
				<td width="30%">
					<form:input path="fecIniInhabi" id="txtFecIni" cssClass="cajatexto_1" cssStyle="width:70px" readonly="readonly" />
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaIni','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaIni"
								align="absmiddle" alt="Calendario" style="cursor:pointer;" onclick="fc_Calendario('1');">
							</a>
					
				</td>
				<td colspan="2" rowspan="2" width="20%"><form:textarea path="obsInhabi" cssClass="cajatexto_1" cols="50" rows="3" onkeydown="fc_TextAreaLimited(this,250)" onkeyup="fc_TextAreaLimited(this,250)"></form:textarea></td>
			</tr>
			<tr>
				<td  class="cabecera">Fecha de t&eacute;rmino:&nbsp;</td>					
				<td >
					<form:input path="fecFinInhabi" id="txtFecFin" cssClass="cajatexto_1" cssStyle="width:70px" readonly="readonly" />
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaFin','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaFin"
								align="absmiddle" alt="Calendario" style="cursor:pointer;" onclick="fc_Calendario('1');">
							</a>
				</td>
			</tr>
		</table>

		<table align=center>
			<tr height="10px"><td></td></tr>
			<tr>
				<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg"  id="imgGrabar" onclick="javascript:fc_Grabar();" style="cursor:pointer;">
					</a>
					&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','${ctx}/images/botones/cerrar2.jpg',1)">
						<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg"  id="imgCerrar" onclick="javascript:window.close();" style="cursor:pointer;">
					</a>
				</td>
			</tr>
		</table>
		
</form:form>

<script type="text/javascript">	
	Calendar.setup({
		inputField     :    "txtFecIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaIni",
		singleClick    :    true
		});

	Calendar.setup({
		inputField     :    "txtFecFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaFin",
		singleClick    :    true
		});
</script>

</body>
</html>