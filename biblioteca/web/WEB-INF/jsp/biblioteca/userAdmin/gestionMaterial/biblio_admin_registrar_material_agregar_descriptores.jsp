<%@ include file="/taglibs.jsp"%>

<head>

<script language=javascript>
function onLoad(){
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
	var refresh = "<%=(( request.getAttribute("refresh")==null)?"": request.getAttribute("refresh"))%>";
	if(mensaje!="")
	{
		alert(mensaje);
		if(refresh!="" && refresh=="1")
		{	window.close();
			window.opener.fc_submit();
		
		}
	}
}

function fc_AgregarDescriptor() {
	//JHPR	
	srtCodPeriodo= "";
	 srtCodEvaluador=document.getElementById("usuario").value;
	 srtcodValor="";
	var winl = (screen.width - parseInt("450")) / 2;	
	var wint = (screen.height - parseInt("160")) / 2;
	 
	//	Fc_Popup("${ctx}/biblioteca/bib_descriptores_agregar.html?txhCodigo="+document.getElementById("txhCodSelec").value + "&txhCodValor=" + srtcodValor 
		//+ "&txhCodPeriodo=" + srtCodPeriodo + "&txhCodUsuario=" + srtCodEvaluador,450,160);
		
	window.open("${ctx}/biblioteca/bib_descriptores_agregar.html?txhCodigo="+ "" + "&txhCodValor=" + srtcodValor 
		+ "&txhCodPeriodo=" + srtCodPeriodo + "&txhCodUsuario=" + srtCodEvaluador,"","left=" + winl + ",top=" + wint + ",width=450,height=185");

				
		//document.getElementById("codigoDescriptor").value="";
		//document.getElementById("descripcionDescriptor").value="";	
}

function fc_limpiar(){
	document.getElementById("txtDescriptores").value="";
}

function fc_buscar(){
if(fc_Trim(document.getElementById("txtDescriptores").value)!="")
	{
		document.getElementById("operacion").value="irConsultar";
		document.getElementById("frmMain").submit();
	}else
		alert("Seleccione criterio de busqueda");
}

function fc_grabar(){
	
	var max = document.getElementById("txhtamanio").value;
	var st	= "";
	var srdo= "";
	var cont= 0;
	
	if(max>0){
		if( confirm(mstrSeguroGrabar) ){
	
			for(i=0;i<max;i++){		
				srdo = "";					
				rdo  = document.getElementById("checkbox"+i);						
				if(rdo.checked){
					srdo = rdo.value;
					st=st+srdo+"|";
					cont = cont+1;
				}
						
			}
		
		document.getElementById("operacion").value="irRegistrar";
		document.getElementById("txhToken").value = st;		
		document.getElementById("txhTamanioSeleccionado").value = cont;
		
		document.getElementById("frmMain").submit();
		}
	}else
		alert("no se puede realizar esta accion");
		
}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_material_agregar_descriptores.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="txhtamanio" id="txhtamanio" />
	<form:hidden path="txhToken" id="txhToken" />
	<form:hidden path="txhCodMaterial" id="txhCodMaterial" />
	<form:hidden path="txhTamanioSeleccionado" id="txhTamanioSeleccionado"  />
	<form:hidden path="usuario" id="usuario"  />
	
	 
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="180px" class="opc_combo"><font style="">Agregar Descriptores</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>	
	<table><tr height="3px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:99%; margin-left:3px" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red">
		<tr>			
			<td nowrap width="10%">Descriptor :</td>			
			<td width="70%">								
				<form:input path="txtDescriptores" id="txtDescriptores"
				 maxlength="50" size="60" cssClass="cajatexto" />
			</td>
			<td width="20%" align="right">
								
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_limpiar();" ></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_buscar();"></a>&nbsp;
				<!-- JHPR: 2008-05-30 Para registrar un nuevo descriptor desde esta ventana de busqueda sin necesidad de ir ha la secci�n de mantenimientos. -->
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src= "${ctx}/images/iconos/agregar1.jpg" align="absmiddle" onclick="javascript:fc_AgregarDescriptor();" id="imgAgregar" style="cursor:pointer" alt="Agregar">
				</a>									
			</td>
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1" width="99%" border="0"  class="" style="margin-left:3px">
		<tr>
			 <td align="left">
			 	<div style="overflow: auto; height: 150px;width:100%">					
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:97%;">
					<tr>
						<td class="grilla" width='5%'>Sel</td>
						<td class="grilla">Descriptor</td>						
					</tr>
				<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
					<tr class="tablagrilla" >
						<td align="center"><input type="checkbox" value="<c:out value="${objCast.codTipoTablaDetalle}" />" ID="checkbox<c:out value="${loop.index}" />" NAME="checkbox<c:out value="${loop.index}" />"   ></td>
						<td align="left"><c:out value="${objCast.descripcion}" /></td>
					</tr>
				</c:forEach>				
				</table>
				</div>
			</td>			
		</tr>		
	</table>	
	<table><tr height="5px"><td></td></tr></table>
	<table align="right">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_grabar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
	</table>
		
</form:form>
</body>
