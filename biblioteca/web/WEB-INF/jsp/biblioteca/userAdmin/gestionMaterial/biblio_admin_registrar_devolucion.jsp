<%@page import="com.tecsup.SGA.bean.MaterialxIngresoBean"%>
<%@page import="java.util.List"%>
<%@ include file="/taglibs.jsp"%>
<head>

<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />

<script src="${ctx}/scripts/js/funciones_bib-numeros.js" language="JavaScript;" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;" type="text/JavaScript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" language="JavaScript;" type="text/javascript" ></script>
<link href="${ctx}/scripts/jquery.colorbox/colorbox.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/scripts/jquery.colorbox/jquery.colorbox.js" language="JavaScript;"	type="text/JavaScript"></script>


<%List<MaterialxIngresoBean> bloquePrestamo = (List<MaterialxIngresoBean>)request.getAttribute("bloquePrestamo"); %>
<style type="text/css">
/* 	.lista-cursos{ */
/* 		background-color: #1ABAE0; */
/* 		margin: 0px; */
/* 		padding: 5px 10px; */
/* 		height: 100px; */
/* 	} */
/* 	.lista-cursos p a{ */
/* 		display: block; */
/* 		text-decoration: none; */
/* 		color: white; */
/* 		font-weight: bold; */
/* 	} */
/* 	.lista-cursos p a:hover{ */
/* 		color: #2C3333; */
/* 	} */
</style>

<script type="text/javascript">

	jQuery.noConflict();

	<% if(bloquePrestamo!=null && !bloquePrestamo.isEmpty()) {%>
	
	(function($) {
		$(function(){
		<%
		int devolver = 0;
		String codBloque = "";
		if(bloquePrestamo.size()>0){
			for(MaterialxIngresoBean mi : bloquePrestamo)
				if(mi.getFecDevolucion().equals("")){
					codBloque=mi.getCodPrestamoMultiple();	
					devolver += 1;			
				}
		}
				
 		if(devolver>0){
		%>
			var url = "${ctx}/biblioteca/biblio_admin_registrar_devolucion_bloque.html";
			url += "?idBloque=<%=codBloque%>"; 
			url += "&txhCodUsuario=" + $('#usuario').val();
			url += "&txhTipoUsuario=" + $('#txhTipoUsuario').val();
			url += "&txhSedeSel=" + $('#txhSedeSel').val();
			url += "&txhSedeUsuario=" + $('#txhSedeUsuario').val();
			url += "&txhCodUsuarioLogin=" + $('#txhCodUsuarioLogin').val();
			
			$.colorbox({href:url, iframe:true, transition:'elastic', speed:500, width:'100%', height:'75%'});
		
		<%
 		}
		%>
		
		})
		
	})(jQuery);
	
	<%}%>

	function closeModal(){
		(function($) {
			$.colorbox.close();
		})(jQuery);
	}
	
	
	

function onLoad(){
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
	if(mensaje!="")
		alert(mensaje);

	var tieneSancion = document.getElementById("txhFlgSancion").value;
	if (tieneSancion=="")
		document.getElementById("txtNroIngreso").focus();
}

function fc_ValidaFechaPrestamoOnblur(strNameObj) {  
       var Obj = document.getElementById(strNameObj);
       var objFecPrestado = document.getElementById("txtFecPrestamo");
       var error=false;
       if (Obj.value !="") {       
			if(Obj.value.length == 8){
				var _anio = Obj.value.substring(6,8)
				Obj.value = Obj.value.substring(0,6)+'20'+_anio
			}			
			if (!isFecha(Obj.value,"dd/MM/yyyy")) error=true;
			else{
				strAnho=Obj.value.split("/")[2];

				if (strAnho<'1900') error=true;                
			}
			if (error){	
				alert('Debe ingresar una fecha válida.');
				Obj.focus();
			}else{
				
			    if (objFecPrestado.value!=""){
			    	if(retornaFecha(Obj.value) < retornaFecha(objFecPrestado.value)){			    	
				        alert('Fecha de devolución no debe ser menor a la fecha de Prestamo');
				        Obj.focus();
			    	}
			    }
			}
       }            
}

function fc_usuario(){
	var usu = document.getElementById("txhCodUsuarioLogin").value
	if(usu!="")	
		Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_prestamo_datos_usuario.html?txhCodUsuario="+usu,700,250);
	else
		alert("Realize una busqueda primero");	
}

function fc_grabar(){
	if(fc_valida()){			
		if(document.getElementById("txhFlgSancion")=="")
			document.getElementById("txhFlgSancion").value  = "0";
		
		var form = document.forms[0];
		form.chkIndPrestamo.disabled = false; //document.getElementById("chkIndPrestamo").disabled=false;		
		form.operacion.value="irRegistrar"; //document.getElementById("operacion").value="irRegistrar";
		form.imgGrabar.disabled=true;
		form.txhTipo.value="1"; //document.getElementById("txhTipo").value="1";		
		form.submit();//document.getElementById("frmMain").submit();
		
	}
}

function fc_capturaKey(e,txt){
	if(e.keyCode == 13){
		fc_verificaNroIngreso(txt)
		// o haz un focus a otra cosa si hay problems
	}
}

function fc_valida(){

	var sedeMat = fc_Trim(document.getElementById("txhSedeMat").value);
	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
	
	if (sedeMat!=''){
		if (sedeMat!=sedeUsu){
			alert('El libro pertenece a una Sede diferente.');
			return 0;
		}
	}
		
	if(fc_Trim(document.getElementById("txhCodUnico").value)==""){
		alert(mstrNoSePuedeRealizarLaAccion);		
		return 0;
	}else if(fc_Trim(document.getElementById("txhFlgPrestado").value)=="0"){
		alert(mstrLibroNoPretado);	
		return 0;
	}else if(fc_Trim(document.getElementById("txtNroIngreso").value)==""){
		alert(mstrIngreseNroIngreso);
		document.getElementById("txtNroIngreso").focus();	
		return 0;
	}else if(fc_Trim(document.getElementById("txhCodMaterial").value)==""){
		alert(mstrMaterialInvalido);
		document.getElementById("txhCodMaterial").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtUsuariologin").value)==""){
	alert(mstrIngreseUsuario);
		document.getElementById("txtUsuariologin").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtUsuarioNombre").value)==""){
		alert(mstrIngreseUsuarioValido);
		document.getElementById("txtUsuarioNombre").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtFecDevolucionProrroga").value)==""){
		alert("Ingrese una fecha de devolución");		
		return 0;
	}else if(fc_Trim(document.getElementById("txtFecDevolucionProrroga").value)!=""){
		srtFechaInicio = document.getElementById("txtFecPrestamo").value
		srtFechaFin	   = document.getElementById("txtFecDevolucion").value;
		srtFechaHoy	   = document.getElementById("txhFecHoy").value;	
		num = fc_ValidaFechaIniFechaFin(srtFechaInicio,srtFechaFin);
		num2 = fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaHoy);
		if(num!=0){
			alert("Ingrese una fecha de Devolución mayor a la de prestamo");
			return 0;
		}	
		if(num2!=0){
			alert("Ingrese una fecha de Devolución menor a la fecha actual");
			return 0;
		}	
	} 
	return 1;
}

function fc_verificaNroIngreso(objName){
	if(document.getElementById("txtNroIngreso").value!=""){
		document.getElementById("operacion").value="irGetMaterial";
		document.getElementById("frmMain").submit();
	}else{
		document.getElementById("txtTipomaterial").value="";
		document.getElementById("txtTitulo").value="";
		document.getElementById("txhCodMaterial").value="";	
	}
}

function fc_verificaCodigoAlumno(){
	if(document.getElementById("txtUsuariologin").value!=""){
		document.getElementById("operacion").value="irGetAlumno";	
		document.getElementById("frmMain").submit();
	}else{
		document.getElementById("txtUsuarioNombre").value="";	
	}
}
function fc_Menu(){
	window.location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;	
}

function Fc_Sancionado()
{	
	Sancionado.style.display="";
}

function fc_Regresar(){
	strParametrosBusqueda = "&prmTxtCodigo="+document.getElementById("prmTxtCodigo").value+
	"&prmTxtNroIngreso="+document.getElementById("prmTxtNroIngreso").value+
	"&prmCboTipoMaterial="+document.getElementById("prmCboTipoMaterial").value+
	"&prmCboBuscarPor="+document.getElementById("prmCboBuscarPor").value+
	"&prmTxtTitulo="+document.getElementById("prmTxtTitulo").value+
	"&prmCboIdioma="+document.getElementById("prmCboIdioma").value+
	"&prmCboAnioIni="+document.getElementById("prmCboAnioIni").value+
	"&prmCboAnioFin="+document.getElementById("prmCboAnioFin").value+
	"&prmTxtFechaReservaIni="+document.getElementById("prmTxtFechaReservaIni").value+
	"&txhCodSede="+document.getElementById("txhSedeSel").value+
	"&txhSedeUsuario="+document.getElementById("txhSedeUsuario").value+		
	"&prmTxtFechaReservaFin="+document.getElementById("prmTxtFechaReservaFin").value;
	
	location.href="${ctx}/biblioteca/biblio_admin_gestion_material.html?"+
	"txhCodUsuario="+document.getElementById("usuario").value+
	"&irRegresar=true"+strParametrosBusqueda;	
}

function fc_VerHistorialProrrogas(nunIngreso,codUsuario,nomUsuario){	
	var usu = document.getElementById("txhCodUsuarioLogin").value;
	var sede = document.getElementById("txhSedeSel").value;
	if(usu!=""){	
		var url ="${ctx}/biblioteca/biblio_admin_gestion_prorroga_historial.html"+
		"?txhCodUsuario="+codUsuario+
		"&txhNroIngreso="+nunIngreso+
		"&txhNomUsuario="+nomUsuario+
		"&txhSede="+sede; 
		Fc_Popup(url,600,320);
	}else
		alert("Realize una busqueda primero");	
}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_registrar_devolucion.html">

<form:hidden path="operacion" id="operacion" />
<form:hidden path="txhCodMaterial" id="txhCodMaterial" />
<form:hidden path="txhTipo" id="txhTipo" />
<!-- txhTipo INDICA 1 TIPO DEVOLUCION -->
<form:hidden path="usuario" id="usuario" />

<form:hidden path="txhCodUnico" id="txhCodUnico" />
<form:hidden path="txhFlgSancion" id="txhFlgSancion" />

<form:hidden path="txhFlgPrestado" id="txhFlgPrestado" />
<form:hidden path="txhFlgReserva" id="txhFlgReserva" />
<form:hidden path="txhFecHoy" id="txhFecHoy" />
<form:hidden path="txhCodUsuarioLogin" id="txhCodUsuarioLogin" />
<form:hidden path="tipoUsuario" id="txhTipoUsuario" />
<!-- SEDE -->
<form:hidden path="sedeSel" id="txhSedeSel" />
<form:hidden path="sedeUsuario" id="txhSedeUsuario" />
<form:hidden path="txhSedeMat" id="txhSedeMat" />

	<!-- Icono Regresar -->	
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Menu();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo"><font style="">Registrar Devoluci&oacute;n</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	 </table>
	 <!-- table><tr height="3px"><td></td></tr></table-->
	 <table bordercolor="red" border="0" cellspacing="0" cellpadding="0"
		style="width: 94%; margin-top: 0px; margin-left: 11px;">
		<tr>
			<td>
			<table background="${ctx}/images/biblioteca/fondosup.jpg"
				style="width: 100%; height: 70px; margin-left: 0px;" cellspacing="4"
				cellpadding="5" class="tabla" border="0" bordercolor="red">
					<tr>
						<td>Nro.Ingreso :</td>
						<td>						
						<form:input path="txtNroIngreso" id="txtNroIngreso" 
						onkeydown="fc_capturaKey(event,'txtNroEdicion');"
						  onblur="fc_verificaNroIngreso('txtNroEdicion')"  cssClass="cajatexto_o" 					   
				 			onkeypress="fc_PermiteNumeros();" maxlength="10" 
						   />						
						</td>
						<td>Tipo Material :&nbsp;
							<form:input path="txtTipomaterial" id="txtTipomaterial" cssClass="cajatexto_1" readonly="readonly" cssStyle="width:200px"/>
						</td>						
						<td align="right">Dewey :
						</td>
						<td><form:input path="txtCodDewey" id="txtCodDewey" 
							  	cssClass="cajatexto_1" size="25"/>			
						</td>
					</tr>
					<tr>				
						<td>Título :</td>
						<td colspan="2">						
						<form:input path="txtTitulo" id="txtTitulo" cssClass="cajatexto_1" cssStyle="width:510px; height:50px;" readonly="readonly" />						
						</td>
						<td colspan="2">
							<form:checkbox path="chkIndPrestamo"  id="chkIndPrestamo" value="0" disabled="true" />&nbsp;Préstamo en sala
						</td>
					</tr>
					<tr>
						<td colspan="5">
<!-- 							<img src="../images/separador_n.gif" width="100%"> -->
						</td>				
					</tr>
					<tr>		
						<td>Fec.Préstamo :</td>
						<td>						
						<form:input path="txtFecPrestamo" id="txtFecPrestamo" cssClass="cajatexto_1" cssStyle="width:70px" readonly="readonly" />						
						</td>
						<td>Fec.Devolución Prog. :
							&nbsp;					
						<form:input path="txtFecDevolucionProrroga" id="txtFecDevolucionProrroga" cssClass="cajatexto_1" cssStyle="width:70px" readonly="readonly" />			
		
						</td>								
						<td colspan="2">	
							<c:choose>		
								<c:when test="${control.flagProrroga == '1'}">
									<form:checkbox path="chkIndProrroga" value="1" />
										Indica Renovaci&oacute;n
								</c:when>
								<c:otherwise>
									<input type="checkbox" disabled="disabled"/>
										L&iacute;mite de renovaciones excedido (<c:out value="${control.totalProrroga}"/> )
								</c:otherwise>		
							
							</c:choose>		
						</td>			
					</tr>
					<tr>	
						<td>Usuario :</td>
						<td colspan="2">							
							<form:input path="txtUsuariologin" 
							   id="txtUsuariologin" cssClass="cajatexto_1" readonly="readonly" />
							&nbsp;							
							<form:input path="txtUsuarioNombre" id="txtUsuarioNombre" cssClass="cajatexto_1" cssStyle="width:380px" readonly="readonly" />
							
							&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Ver Datos Usuario"
								 id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_usuario();">
							</a>
						</td>	
						<td colspan="2">			
						<!-- if(la cantidad de proroga es mayor a 0) . Poner otro mensaje "Limite excedido" si se sepero la prorroga pro rol -->		
							<c:if test="${control.totalProrroga != '0'}">
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar2','','${ctx}/images/iconos/buscar2.jpg',1)">
									<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Ver Datos Usuario"
									 id="imgBuscar2" style="CURSOR: pointer" onclick="javascript:fc_VerHistorialProrrogas(<c:out value="${control.txtNroIngreso}" default="0"/>,<c:out value="${control.txhCodUsuarioLogin}" default="0"/>,'<c:out value="${control.txtUsuarioNombre}"/>');">
								</a>
								Historial Pr&oacute;rrogas (<c:out value="${control.totalProrroga}"/> )
							</c:if>
						</td>				
					</tr>
					<tr>
						<td colspan="5"></td>				
					</tr>
					<tr>		
						<td>Fec.Devolución :</td>
						<td>						
						<form:input path="txtFecDevolucion" id="txtFecDevolucion" cssClass="cajatexto_o" cssStyle="width:70px" maxlength="10" 
							onkeypress="fc_ValidaFecha('txtFecDevolucion')" 							
							onblur="fc_ValidaFechaPrestamoOnblur('txtFecDevolucion')"/>

							<c:if test="${control.tipoUsuario=='A'}">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaIni','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaIni"
								align="absmiddle" alt="Calendario" style="cursor:pointer;" onclick="fc_Calendario('1');">
							</a>
							</c:if>

						</td>							
					</tr>					
				</table>	
			</td>
		</table>
		<table class="tabla" cellpadding="2" border="0" cellspacing="2" ID="Sancionado" style="display:none;width:94%" align=center>
			<tr>
				<td>Sancionado :</td>
				<td colspan="2">				
				<form:input path="txtSancionado" id="txtSancionado" cssClass="cajatexto_1" cssStyle="width:550px; height:50px" readonly="readonly" />
				
				</td>
			</tr>
		</table>
		<table bordercolor="red" align="right" border="0" cellspacing="2" cellpadding="2" style="width:17%;margin-top:10px;margin-right:43px">
			<tr>
				<td align="right">					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" align="absmiddle" alt="Grabar" id="imgGrabar" onclick="javascript:fc_grabar();" style="cursor:pointer;"></a>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
						<img src="${ctx}/images/botones/regresar1.jpg" align="absmiddle" alt="Regresar" id="imgRegresar" style="cursor:pointer;" onclick="fc_Regresar();"></a>
				</td>				
			</tr>	
		</table>
		
	<!-- PARAMETROS DE BUSQUEDA DE LA PAGIN ANTERIOR -->
	<form:hidden path="prmTxtCodigo" id="prmTxtCodigo" />
	<form:hidden path="prmTxtNroIngreso" id="prmTxtNroIngreso" />
	<form:hidden path="prmCboTipoMaterial" id="prmCboTipoMaterial" />
	<form:hidden path="prmCboBuscarPor" id="prmCboBuscarPor" />
	<form:hidden path="prmTxtTitulo" id="prmTxtTitulo" />
	<form:hidden path="prmCboIdioma" id="prmCboIdioma" />
	<form:hidden path="prmCboAnioIni" id="prmCboAnioIni" />
	<form:hidden path="prmCboAnioFin" id="prmCboAnioFin" />
	<form:hidden path="prmTxtFechaReservaIni" id="prmTxtFechaReservaIni" />
	<form:hidden path="prmTxtFechaReservaFin" id="prmTxtFechaReservaFin" />	
		
</form:form>
<script type="text/javascript">

	if (document.getElementById("txhTipoUsuario").value=='A'){
		Calendar.setup({
			inputField     :    "txtFecDevolucion",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgFechaIni",
			singleClick    :    true

			});
	}else{
		//document.getElementById("txtFecDevolucion").disabled=true;
		document.getElementById("txtFecDevolucion").readOnly = true;
		
	}


                          
</script>
		
</body>
