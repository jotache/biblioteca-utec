<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">
function onLoad(){}
</script>
</head>
<body>

<form:form  id="frmMain" action="/biblioteca/historial.html"
  method="post">



<table cellpadding="0" cellspacing="0" ID="Table1"
		style="margin-left:0px;width:98%" class="" >
		<tr>		
			<td>
			<!--PAGINADO-->
	<display:table name="sessionScope.SS_HISTORIAL" 
	cellpadding="0" cellspacing="1"  
	 pagesize="17" style="border: 1px solid #048BBA;width:100%" requestURI="" >	
	 										
				<display:column property="nroIngreso" 			title="Nro.Ingreso" headerClass="grilla"  class="tablagrilla"  style="text-align:center;width:10%"/>
				<display:column property="nomUsuario" 			title="Nombre" headerClass="grilla"  class="tablagrilla"  style="text-align: left;width:30%"/>
				<display:column property="usuario" 			title="Usuario" headerClass="grilla"  class="tablagrilla"  style="text-align: left;width:12%"/>
				<display:column property="fecPrestamo" 			title="Fec. Préstamo" headerClass="grilla"  class="tablagrilla" style="width:15%;text-align:center"/>
				<display:column property="fecPrestamoProg" 			title="Fec. Devol. Prog." headerClass="grilla"  class="tablagrilla"  style="width:16%;text-align:center"/>
				<display:column property="fecDevolucion" 			title="Fec. Devolución" headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align:center"/>

				
				
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
			</display:table>
			<!--/PAGINADO-->
			
			</td>
		</tr>
	</table>

	
</form:form>	
</body>
				