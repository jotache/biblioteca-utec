<%@ include file="/taglibs.jsp"%>
<head>
<script type="text/javascript">
function onLoad(){
}
</script>
</head>
<body>
	
	<table style="width:99%; margin-left:3px;" border="0" cellspacing="0" cellpadding="1" class="borde_tabla" height="50px" >
		<tr class="fondo_cabecera_azul">		 
		 	<td class="titulo_cabecera" width="100%" colspan="4">
				Historial de Pr&oacute;rrogas
			</td>
		</tr>
		<tr class="fondo_dato_celeste">
			<td style="width: 20%">Nro. Ingreso :</td>
			<td style="width: 20%">				
				<c:out value="${model.txhNroIngreso}"/>
			</td>
			<td style="width: 20%">Usuario :</td>
			<td style="width: 40%">				
				<c:out value="${model.txhNomUsuario}"/>
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1" width="99%" border="0"  class="" style="margin-left:3px">
		<tr>
			 <td align="left">
			 
			 
				<table cellpadding="0" cellspacing="0" ID="Table1" style="margin-left:0px;width:100%" class="" >
					<tr>		
						<td>
						<!--PAGINADO-->
				<display:table name="model.lista" id="bean" cellpadding="0" cellspacing="1"  
				 pagesize="20" style="border: 1px solid #048BBA;width:100%" requestURI="" >	
				 										
							<display:column title="" headerClass="grilla"  class="tablagrilla" style="width:5%;text-align:center"><c:out value="${bean_rowNum}"/></display:column>
							<display:column property="fecPrestamo" title="Fec. Préstamo" headerClass="grilla"  class="tablagrilla" style="width:15%;text-align:center"/>
							<display:column property="fecDevolucionProg" title="Fec. Devol. Prog." headerClass="grilla"  class="tablagrilla"  style="width:16%;text-align:center"/>
							<display:column property="fecDevolucion" title="Fec. Devolución" headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align:center"/>
							<display:column property="nomAsistente" title="Asistente" headerClass="grilla"  class="tablagrilla"  style="text-align: left;width:40%"/>
							<!-- c:out value="${bean.fecPrestamo}"/-->
							
     
										<display:setProperty name="basic.empty.showtable" value="true"  />
										<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='5' align='center'>No se encontraron registros</td></tr>"  />
										<display:setProperty name="paging.banner.placement" value="bottom"/>
										<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
										<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
										<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
										<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
										<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
										<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
										<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
										<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
										<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
										<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
						</display:table>
						<!--/PAGINADO-->
						
						</td>
					</tr>
				</table>

			</td>
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/cerrar2.jpg',1)">
				<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:window.close();"></a></td>
		</tr>
	</table>
	
</body>