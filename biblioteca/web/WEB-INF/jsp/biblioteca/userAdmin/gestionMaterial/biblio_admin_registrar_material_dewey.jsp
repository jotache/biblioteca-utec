<%@ include file="/taglibs.jsp"%>

<head>

<script language=javascript>
function onLoad(){
}

function fc_limpiar(){
	document.getElementById("codCategoria").value  ="";
	document.getElementById("txtDescripcion").value ="";	
}

function fc_buscar(){
if(fc_Trim(document.getElementById("codCategoria").value)!="")
	{
		document.getElementById("operacion").value="irConsultar";
		document.getElementById("frmMain").submit();
	}else
		alert("Seleccione Categoria");
}

function fc_aceptar(){

if(fc_Trim(document.getElementById("nombre").value)!=""
	&& fc_Trim(document.getElementById("codigo").value)!=""	)
	{
		window.opener.document.getElementById("txtDewey").value = document.getElementById("codigo2").value+"-"+document.getElementById("nombre").value;
		//window.opener.document.getElementById("txtDewey").value = document.getElementById("nombre").value;
		window.opener.document.getElementById("txhCodigoDewey").value = document.getElementById("codigo").value;
		window.opener.document.getElementById("txhCodGeneradoDewey").value = document.getElementById("codigo2").value;
		window.opener.fc_concatena();		
		window.close();
	}else
		alert("Seleccione Clasificación");	
}

function fc_selecciona(cod,nom,cod2){
	document.getElementById("codigo").value = cod;
	document.getElementById("nombre").value = document.getElementById("hid"+nom).value;
	document.getElementById("codigo2").value = cod2;
}

function fc_AgregarDewey(){
	var winl = (screen.width - parseInt("500")) / 2;	
	var wint = (screen.height - parseInt("170")) / 2;
	srtCodUsuario = document.getElementById("txhCodUsuario").value;
	window.open("${ctx}/biblioteca/bib_dewey_agregar.html?txhCodigo=&txhCodValor=" +
		"&txhCodPeriodo=&txhCodCategoria=&txhCodUsuario=" + srtCodUsuario,"","left=" + winl + ",top=" + wint + ",width=500,height=170");
}

function fc_Buscar(){
	fc_buscar();
}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_material_dewey.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="txhCodUsuario" id="txhCodUsuario" />
	
	<!-- VARIABLES DE SELECCION -->
	<input type="hidden" name="codigo" id="codigo" />
	<input type="hidden" name="codigo2" id="codigo2" />
	<input type="hidden" name="nombre" id="nombre" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="220px" class="opc_combo"><font style="">Busqueda de Clasificación</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="3px"><td></td></tr></table>
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:99%; margin-left:3px" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red">
		<tr>
			<td width="10%">Categoría :&nbsp;</td>
			<td width="20%">
				<form:select path="cboCategoria" id="codCategoria" cssClass="cajatexto_o" cssStyle="width:120px">
					<form:option value="">--Seleccione--</form:option>					
						<c:if test="${control.lstCategoria!=null}">
							<form:options itemValue="codTipoTablaDetalle"
							itemLabel="descripcion" items="${control.lstCategoria}" />
						</c:if>
				</form:select>				
			</td>			
			<td width="10%">Descripción :&nbsp;</td>
			<td width="50%">				
				<form:input path="txtDescripcion" id="txtDescripcion" cssClass="cajatexto"
				  maxlength="40" size="40"  />
			</td>
			<td align="right" width="10%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:fc_limpiar();" ></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_buscar();"></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
						<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_AgregarDewey();" id="imgAgregar" style="cursor:pointer" alt="Agregar"></a>
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1" width="99%" border="0"  class="" style="margin-left:3px">
		<tr>
			 <td align="left">
			 	<div style="overflow: auto; height: 250px;width:100%">
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:97%;">
					<tr height="30px">
						<td class="grilla" width="5%"  >Sel.</td>
						<td class="grilla" width="15%">Código</td>
						<td class="grilla">Descripción</td>
					</tr>
				<c:set var="contItems" value="0"/>
				<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">					
					<tr class="tablagrilla">
						<c:set var="contItems" value="${contItems+1}" />
						<td align="center">
						<input type="hidden" name="hid<c:out value="${loop.index}" />" id="hid<c:out value="${loop.index}" />" value="<c:out value="${objCast.descripcion}" />" />
						<input type="radio" ID="Radio3" NAME="Radio" onclick="fc_selecciona('<c:out value="${objCast.codTipoTablaDetalle}" />','<c:out value="${loop.index}" />','<c:out value="${objCast.dscValor2}" />')" >
						</td>
						<td align="center">&nbsp;<c:out value="${objCast.dscValor2}" /></td>
						<td><c:out value="${objCast.descripcion}" /></td>
					</tr>
				</c:forEach>	
				<c:if test="${control.lstResultado!=null && contItems==0}">
					<tr class="tablagrilla">
						<td colspan="3" align="center">No se encontraron registros</td>
					</tr>
				</c:if>											
				</table>
				</div>
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=right  border="0" bordercolor="red">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/aceptar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/aceptar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_aceptar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
	</table>		
	
</form:form>
</body>
