<%@ include file="/taglibs.jsp"%>
<head>
<link href="${ctx}/styles/estilo_biblio_admin2.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/scripts/js/funciones_bib-textos.js" language="JavaScript;"	type="text/JavaScript"></script>
<script language=javascript>
<!--
function onLoad(){
	document.getElementById('txhIdSancion').value = "";
	var objMsg = "<%=((request.getAttribute("mensaje")==null)?"":(String)request.getAttribute("mensaje"))%>";	  
	if(objMsg!="")
		alert(objMsg);
}

function fc_seleccionarRegistro(codUsuario,codFila){	
	document.getElementById('codUsuario').value = fc_Trim(codUsuario);
	document.getElementById('txhIdSancion').value = fc_Trim(codFila)
}

function fc_buscarUsuario(){
	//Fc_Popup("${ctx}/inicio/buscar_alumno.html?tipoRetorno=C&ctrlUSE=&ctrlCOD=txtCodUsuario&ctrlNOM=",600,400);
	Fc_Popup("${ctx}/biblioteca/biblio_buscar_usuario.html?tipoRetorno=ALL&ctrlUSE=txtUsuariologin&ctrlCOD=codUsuario&ctrlNOM=txtUsuarioNombre",600,400);
}

function fc_Menu(){
	window.location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;
}

function Fc_AdjuntarArchivo(){
	if(fc_validaMaterialRegistrado()){
				
	}else{
		alert('No se puede realizar la acci�n.');
	}	
}

function fc_buscarSanciones(){
	if(validar()){
		//document.getElementById('txhCodUsuario').value = document.getElementById('otxtCodUsuario').value;		
		document.getElementById("operacion").value="BUSCARSANCIONES";		
		document.getElementById("frmMain").submit();					
	}
}

function fc_levantarSancion(){
	if(document.getElementById('txhIdSancion').value == ""){
		alert(mstrSeleccione);
		return;
	}
	if (confirm('�Seguro de levantar la sanci�n?')){
		document.getElementById("operacion").value="LEVANTARSANCION";		
		document.getElementById("frmMain").submit();
	}
}

function fc_suspenderServicios(){
	if(validar()){
		url = "${ctx}/biblioteca/biblio_admin_suspensiones.html?"+
			"txhNomUsuario="+document.getElementById("txtUsuarioNombre").value+
			"&txhIdUsuario="+document.getElementById("txtUsuariologin").value+
			"&txhCodUsuarioAcc="+document.getElementById("usuario").value+
			"&txhCodUsuario="+document.getElementById("codUsuario").value;
		Fc_Popup(url,500,360);
	}
}

function validar(){
	if(fc_Trim(document.getElementById("codUsuario").value)==""){
		alert('Por favor seleccione un Usuario');
		return false;
	}
	return true;			
}

function fc_buscarUsuarioKey(){	
	document.getElementById("operacion").value="BUSCARUSUARIO";	
	document.getElementById("frmMain").submit();	
}

function fc_capturaKey(e){
	if(e.keyCode == 13){
		fc_buscarUsuarioKey();
	}
}

function fc_Regresar(){
	strParametrosBusqueda = "&prmTxtCodigo="+document.getElementById("prmTxtCodigo").value+
	"&prmTxtNroIngreso="+document.getElementById("prmTxtNroIngreso").value+
	"&prmCboTipoMaterial="+document.getElementById("prmCboTipoMaterial").value+
	"&prmCboBuscarPor="+document.getElementById("prmCboBuscarPor").value+
	"&prmTxtTitulo="+document.getElementById("prmTxtTitulo").value+
	"&prmCboIdioma="+document.getElementById("prmCboIdioma").value+
	"&prmCboAnioIni="+document.getElementById("prmCboAnioIni").value+
	"&prmCboAnioFin="+document.getElementById("prmCboAnioFin").value+
	"&prmTxtFechaReservaIni="+document.getElementById("prmTxtFechaReservaIni").value+
	"&txhCodSede="+document.getElementById("txhSedeSel").value+
	"&txhSedeUsuario="+document.getElementById("txhSedeUsuario").value+		
	"&prmTxtFechaReservaFin="+document.getElementById("prmTxtFechaReservaFin").value;
	
	location.href="${ctx}/biblioteca/biblio_admin_gestion_material.html?"+
	"txhCodUsuario="+document.getElementById("usuario").value+
	"&irRegresar=true"+strParametrosBusqueda;	
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>
</head>
<body>														  
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_sanciones.html">

<form:hidden path="operacion" id="operacion" />
<form:hidden path="sedeSel" id="txhSedeSel" />
<form:hidden path="sedeUsuario" id="txhSedeUsuario" />
<form:hidden path="idSancion" id="txhIdSancion"/>
<form:hidden path="usuario" id="usuario" />
<form:hidden path="codUsuario" id="codUsuario" />

	<!-- Icono Regresar -->	
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Menu();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<table>
		<tr height="10px">
			<td height="10px"></td>
		</tr>
	</table>
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo"><font style="">Sanciones de Usuario</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	 </table>
	
	 <table border="0" cellspacing="0" cellpadding="0"
		style="width: 94%; margin-top: 0px; margin-left: 11px;">
		<tr>
			<td>
			<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width: 90%; margin-left: 0px;" cellspacing="4"
				cellpadding="5" class="tabla" border="0" >
					<tr>
						<td width="10%">Usuario :</td>
						<td width="90%">	
							<form:input id="txtUsuariologin" path="txtUsuariologin" cssClass="cajatexto_o" 
								cssStyle="width:70px" maxlength="20" 
								onkeydown="fc_capturaKey(event);"								
								onkeypress="fc_ValidaTextoNumeroEspecial();" 															
								/>

							&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)"><img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar Usuario"
									 id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_buscarUsuario();">
							</a>
							&nbsp;
											 
							<form:input id="txtUsuarioNombre" path="txtUsuarioNombre" cssClass="cajatexto_o" cssStyle="width:250px" readonly="true"/>							
							&nbsp;&nbsp;

							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscarSanciones','','${ctx}/images/botones/buscar_sanciones2.png',1)"><img src="${ctx}/images/botones/buscar_sanciones1.png" align="absmiddle" alt="Buscar Sanciones"
									 id="imgBuscarSanciones" style="CURSOR: pointer" onclick="javascript:fc_buscarSanciones();">
							</a>										
						</td>
					</tr>
				</table>	
			</td>
		</table>
		<table class="" style="width:87%;margin-top:6px; margin-left: 11px;" cellspacing="2" cellpadding="0" border="0">
			<tr>
				<td>					
					<div style="OVERFLOW:auto; WIDTH: 99%; HEIGHT: 230px; display: ;" id="tblSanciones">
						<display:table name="requestScope.listaSanciones" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.SancionesUsuarioDecorator" 
							requestURI="" style="border: 1px solid #048BBA;width:98%">
							<display:column property="rbtSelSancion" title="Sel." style="text-align:center; width:4%" />
							<display:column property="valorSancion1" title="C�digo" style="text-align:center;width:9%" />
							<display:column property="codFila" title="Id" style="text-align:center;width:4%" />
							<display:column property="valorSancion2" title="Tipo de Sanci�n" style="text-align:left;width:34%" />
							<display:column property="valorSancion3" title="Fecha Inicio" style="text-align:center;width:12%"/>
							<display:column property="valorSancion4" title="Fecha Termino" style="text-align:center;width:12%"/>
							<display:column property="valorSancion5" title="Estado" style="text-align:center;width:8%"/>
							<display:column property="observacion" title="Motivo" style="text-align:left;"/>

							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'></span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
							<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
							<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
						</display:table>						
					</div>
				</td>
			</tr>
		</table>

		<table border="0" cellspacing="2" cellpadding="2" style="width:80%;margin-top:10px;margin-left: 11px;">
			<tr>
				<td align="center">

					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgSuspende','','${ctx}/images/botones/suspender_servicios2.png',1)">
						<img alt="Suspender servicios" src="${ctx}\images\botones\suspender_servicios1.png" align="absmiddle" id="imgSuspende" onclick="javascript:fc_suspenderServicios();" style="cursor:pointer;"></a>
				&nbsp;
					
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLevanta','','${ctx}/images/botones/levantar_sancion2.png',1)">
						<img alt="Levantar Sanci�n" src="${ctx}\images\botones\levantar_sancion1.png" align="absmiddle" id="imgLevanta" onclick="javascript:fc_levantarSancion();" style="cursor:pointer;"></a>
				&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
						<img src="${ctx}/images/botones/regresar1.jpg" align="absmiddle" alt="Regresar" id="imgRegresar" style="cursor:pointer;" onclick="fc_Regresar();"></a>
				</td>				
			</tr>	
		</table>
		
	<!-- PARAMETROS DE BUSQUEDA DE LA PAGIN ANTERIOR -->
	<form:hidden path="prmTxtCodigo" id="prmTxtCodigo" />
	<form:hidden path="prmTxtNroIngreso" id="prmTxtNroIngreso" />
	<form:hidden path="prmCboTipoMaterial" id="prmCboTipoMaterial" />
	<form:hidden path="prmCboBuscarPor" id="prmCboBuscarPor" />
	<form:hidden path="prmTxtTitulo" id="prmTxtTitulo" />
	<form:hidden path="prmCboIdioma" id="prmCboIdioma" />
	<form:hidden path="prmCboAnioIni" id="prmCboAnioIni" />
	<form:hidden path="prmCboAnioFin" id="prmCboAnioFin" />
	<form:hidden path="prmTxtFechaReservaIni" id="prmTxtFechaReservaIni" />
	<form:hidden path="prmTxtFechaReservaFin" id="prmTxtFechaReservaFin" />	
		
</form:form>
<script type="text/javascript">

                     
</script>
		
</body>

