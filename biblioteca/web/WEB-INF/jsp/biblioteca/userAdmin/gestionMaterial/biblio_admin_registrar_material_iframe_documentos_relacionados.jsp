<%@page import="com.tecsup.SGA.bean.UsuarioSeguridad"%>
<%@ include file="/taglibs.jsp"%>
<head>	
	
	<%
		UsuarioSeguridad userLogin = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
		String opciones = "";
		String tipoUser = "";
		if(userLogin!=null){
			opciones = userLogin.getOpcionesApoyo();
			tipoUser = userLogin.getTipoUsuario();
		}
	
	%>
		
	<script type="text/javascript">
	function onLoad(){
			
	}
	
	function fc_Eliminar(){
	
	if(fc_Trim(document.getElementById("txhCodigoUnicoDocumento").value)!=""){
		
		if(confirm(mstrSeguroEliminar))
		{
			document.getElementById("operacion").value="irEliminar";
			document.getElementById("usuario").value=parent.document.getElementById("usuario").value;
			document.getElementById("frmMain").submit();
		}
		
		}else
			alert(mstrSeleccione);
	}	
	
	function Fc_Upload(){
		url = "${ctx}/biblioteca/biblio_admin_registrar_material_adjuntar_documentos.html?"+
		"txhCodUnico="+document.getElementById("txhCodigoUnico").value+
		"&txhCodUsuario="+parent.document.getElementById("usuario").value;		
		Fc_Popup(url,450,160);
	}
	
	function fc_selecciona(codigoUnico){
		document.getElementById("txhCodigoUnicoDocumento").value = codigoUnico;  
	}
	
	function Fc_ActualizarPadre(){
		document.getElementById("frmMain").submit();
	}
	
	</script>

</head>

<body topmargin="5" leftmargin="10" rightmargin="10" bgcolor="red">
<form:form  id="frmMain" action="${ctx}/biblioteca/biblio_admin_registrar_material_iframe_documentos_relacionados.html"  method="post">

<input type="hidden" name="txhCodigoUnico" id="txhCodigoUnico" value="${model.txhCodigoUnico}" />
<input type="hidden" name="operacion" id="operacion" value="irConsultar" />
<input type="hidden" name="usuario" id="usuario" />

<!-- VARIABLE QUE ALMACENA EL ITEM SELECCIONADO -->
<input type="hidden" name="txhCodigoUnicoDocumento" id="txhCodigoUnicoDocumento" />
	<table cellpadding="0" cellspacing="0" id="Table1" width="100%" border="0" class="">
		<tr>			
			<td valign="top">
				<div style="overflow: auto; height: 199px; width:100%;">
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:98%;">
					<tr class="grilla">					
						<td width="4%">Sel.</td>
						<td width="92%">Titulo</td>					
						<td width="4%">Ver</td>
					</tr>
				  	<!-- BIBLIOTECA -->
					<c:forEach var="objCast3" items="${model.lstDocumentosRelacionados}" varStatus="loop3" >						
						<tr class="tablagrilla">
							<td width="4%" align="center">
								<input type=radio  NAME="RadioDescriptor" 
								onclick="fc_selecciona('<c:out value="${objCast3.codUnico}" />')" >
							</td>												
							<td><c:out value="${objCast3.titulo}" /></td>
							<td align="center">								
								<a href="<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%><c:out value="${objCast3.archivo}" />" target="_blank">								
								<img  src="${ctx}/images/iconos/buscar1.jpg" border="0" style="cursor:pointer"	id="imgEliminarPerfil">
								</a>
							</td>
						</tr>
					</c:forEach>
					<!-- /BIBLIOTECA -->
				</table>
				</div>
			</td>
			<td width="25px" valign="top" align="right">
				<br/>
				
				<%if(opciones.indexOf("INS_DOC|")>=0 || tipoUser.equals("A")){ %>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/adjuntar2.jpg',1)">
					<img src="${ctx}/images/iconos/adjuntar1.jpg" onclick="javascript:Fc_Upload();" id="imgAgregarPerfil" style="cursor:pointer" alt="Adjuntar Archivo">
				</a>	
				<%} %>
				
				<br/>
				
				<%if(opciones.indexOf("DEL_DOC|")>=0 || tipoUser.equals("A")){ %>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil1','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" id="imgEliminarPerfil1" onclick="javascript:fc_Eliminar();">
				</a>
				<%} %>
				
			</td>
		</tr>		
	</table>
</form:form>	
</body>




