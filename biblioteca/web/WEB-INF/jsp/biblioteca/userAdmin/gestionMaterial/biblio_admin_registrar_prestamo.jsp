<%@page import="java.util.ArrayList"%>
<%@page import="com.tecsup.SGA.bean.MaterialxIngresoBean"%>
<%@page import="java.util.List"%>
<%@page import="com.tecsup.SGA.common.CommonMessage"%>
<%@page import="com.tecsup.SGA.common.CommonConstants"%>
<jsp:useBean id="item_numero" class="java.lang.String" scope="request"></jsp:useBean>
<%@ include file="/taglibs.jsp"%>
<%
	List<MaterialxIngresoBean> listaAdeuda = (List<MaterialxIngresoBean>)request.getAttribute("listaLibrosAdeuda");
%>
<head>
<script src="${ctx}/scripts/js/jquery-1.7.2.min.js" type="text/JavaScript"></script>
<script type="text/javascript">
	jQuery.noConflict();

	$(document).ready(function(){
			
		$("#dialog-error").dialog({
			autoOpen : false,
			resizable : false,
			height : 240,
			width : 400,
			modal : true,
			buttons : {					
				"Aceptar" : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#dialog-error-adeuda").dialog({
			autoOpen : false,
			resizable : false,
			height : 240,
			width : 520,
			modal : true,
			buttons : {					
				"Aceptar" : function() {
					$(this).dialog("close");
				}
			}
		});
		
	
		$("#dialog-error-adeuda-pasa").dialog({
			autoOpen : false,
			resizable : false,
			height : 240,
			width : 520,
			modal : true,
			buttons : {					
				"Continuar" : function() {
					$(this).dialog("close");
				},
				"Cancelar" : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#dialog-confirmacion").dialog({
			autoOpen : false,
			resizable : false,
			height : 200,
			width : 360,
			modal : true,
			buttons : {					
				"Aceptar" : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$('#txtNroIngreso1').keypress(function(e) {
			if(e.which == 13){
				fc_verificaNroIngreso_Multiple('1');
			}
		});
		
		$('#txtNroIngreso2').keypress(function(e) {
			if(e.which == 13){
				fc_verificaNroIngreso_Multiple('2');
			}
		});
		
		$('#txtNroIngreso3').keypress(function(e) {
			if(e.which == 13){
				fc_verificaNroIngreso_Multiple('3');
			}
		});
		
		$('#txtNroIngreso4').keypress(function(e) {
			if(e.which == 13){
				fc_verificaNroIngreso_Multiple('4');
			}
		});
		
		$('#txtNroIngreso5').keypress(function(e) {
			if(e.which == 13){
				fc_verificaNroIngreso_Multiple('5');
			}
		});
		
		if(document.getElementById("operacion").value=='irGetMateriales'){
			document.getElementById("prestamoMultiple").checked=1;
			$("#panelDefault").css("display", "none");				      
			
			 $("#prestamoMultiple").click(function() { 
				 if($("#prestamoMultiple").is(':checked')) {
			          $('#panelDefault').slideUp('fast');	
			          $('#panelMultiple').slideDown('fast');
			          			         
			     } else {
			          $('#panelDefault').slideDown('fast');
			          $('#panelMultiple').slideUp('fast');	
			          //clean
			          limpiaCasilla('1');
			          limpiaCasilla('2');
			          limpiaCasilla('3');
			          limpiaCasilla('4');
			          limpiaCasilla('5');
			          
			     }	
			 });
			 
			 			
		}else{
										
		$("#panelMultiple").css("display", "none");			
		
		 $("#prestamoMultiple").click(function() { 
			 if($("#prestamoMultiple").is(':checked')) {
		          $('#panelDefault').slideUp('fast');	
		          $('#panelMultiple').slideDown('fast');
		     } else {
		          $('#panelDefault').slideDown('fast');
		          $('#panelMultiple').slideUp('fast');	
		     }
		 });
		
		 
			
		 
		}
				
	});
</script>

<script type="text/javascript">

function fc_VerPrestados(codUsuario){	
	
	var url ="${ctx}/biblioteca/biblio_admin_gestion_prestamos_usuario.html"+
	"?codUsuario="+codUsuario;	
	Fc_Popup(url,700,300);
}

function casilla() {
	document.getElementById("txtUsuarioNombre").value = "";
	document.getElementById("txtUsuariologin").focus();
}

function verFecha(objCheck){
	if(objCheck.checked == true)		
		document.getElementById("txtFecDevolucionProrroga").value = document.getElementById("txtFecPrestamo").value;	
	else
		document.getElementById("txtFecDevolucionProrroga").value = document.getElementById("auxtxtFecDevolucionProrroga").value;
}
	
function onLoad(){
	//document.getElementById("txtUsuariologin").focus();
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
	var flgU 	= "<%=(( request.getAttribute("flgUsuario")==null)?"": request.getAttribute("flgUsuario"))%>";
	var flgM 	= "<%=(( request.getAttribute("flgMaterial")==null)?"": request.getAttribute("flgMaterial"))%>";
	var busca_codigo = "<%=(( request.getAttribute("busca_codigo")==null)?"": request.getAttribute("busca_codigo"))%>";
	var busca_tipo = "<%=(( request.getAttribute("busca_tipo")==null)?"": request.getAttribute("busca_tipo"))%>";
	var nNumero = "<%=(( request.getAttribute("nNumero")==null)?"": request.getAttribute("nNumero"))%>";
		
	var tipoMensOk = '<%=CommonMessage.GRABAR_EXITO_PRESTAMO%>';
	if(mensaje!=""){
		if(nNumero=='0' || nNumero==''){
			
			var new_mens = mensaje;
			if(new_mens.indexOf('�')!=-1){
				//Prestamo m�ltiple...
				var parte=new_mens.split('�');
				var i;
				var mens_tot = '<ul>';
				
				for(i=0;i<parte.length-1;i++){					
					mens_tot += '<li>' + parte[i] + '</li><br>';
				}
				mens_tot += '</ul>';				
				
				var tipoMens = '<%=CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO%>';
				var n  = mens_tot.indexOf(tipoMens);
				if(n>0){
					
					var textomsg = 'El Usuario adeuda los siguientes libros:<br/>';
					var libros = '<table border=1 align=center width=100%><tr>';
					libros += '<th>T�tulo</th><th>Fec. Pr�stamo</th><th>Ingreso</th><th>Dewey</th></tr>';
					<%
					if(listaAdeuda!=null && listaAdeuda.size()>0){
						for(MaterialxIngresoBean o:listaAdeuda){
							%>
							libros += '<tr><td>'+'<%=o.getTitulo()%>' + '</td><td>' + '<%=o.getFecPrestamo()%>' + '</td>';
							libros += '<td>' + '<%=o.getCodUnico()%>' + '</td><td>' + '<%=o.getCodDewey()%>' + '</td></tr>';
							<%					
						}
					}
					%>
					libros += '</table>';
					
					document.getElementById('msgError-adeuda').innerHTML = textomsg + libros;
					$("#dialog-error-adeuda").dialog("open");
					
				}else{
					//alert(mens_tot);
					if(new_mens.indexOf(tipoMensOk)>0){
						$("#dialog-confirmacion").dialog("open");
					}else{
						document.getElementById('msgError').innerHTML = mens_tot;
						$("#dialog-error").dialog("open");	
					}
					
				}
				
			}else{			
				//prestamo de un unico libro				
				//alert(new_mens);				
				var tipoMens = '<%=CommonMessage.LIBRO_PRESTADO_HACE_TIEMPO%>';
				if(tipoMens==mensaje){					
					var libros = '<table border=1 align=center width=100%><tr>';
					libros += '<th>T�tulo</th><th>Fec. Pr�stamo</th><th>Ingreso</th><th>Dewey</th></tr>';
					<%
					if(listaAdeuda!=null && listaAdeuda.size()>0){
						for(MaterialxIngresoBean o:listaAdeuda){
							%>
							libros += '<tr><td>'+'<%=o.getTitulo()%>' + '</td><td>' + '<%=o.getFecPrestamo()%>' + '</td>';
							libros += '<td>' + '<%=o.getCodUnico()%>' + '</td><td>' + '<%=o.getCodDewey()%>' + '</td></tr>';						
							<%					
						}
					}
					%>
					libros += '</table>';
					document.getElementById('msgError-adeuda').innerHTML = 'El Usuario adeuda los siguientes libros:<br/>' + libros;
					$("#dialog-error-adeuda").dialog("open");
					
				}else{
					
					if(new_mens.indexOf(tipoMensOk)>0 || tipoMensOk==new_mens){
						//mensaje de exito
						$("#dialog-confirmacion").dialog("open");
					}else{
						//otro mensaje de error
						document.getElementById('msgError').innerHTML = new_mens;
						$("#dialog-error").dialog("open");
					}
					
// 					alert(new_mens);
				}
								
			}
			if(flgU!=""){ document.getElementById("txtUsuariologin").focus(); }
			if(flgM!=""){ document.getElementById("txtNroIngreso").focus(); }
			
			limpiaDetPrestamo();
			
		}else{
						
			alert("Libro " + nNumero+ ": "+ mensaje);						
			limpiaCasilla(nNumero);
			
			if(flgU!=""){ document.getElementById("txtUsuariologin").focus(); }
			if(flgM!=""){ document.getElementById("txtNroIngreso").focus(); }

		}
		//comentando return (libros para prestamos en sala podian registrarse como prestamo a domicilio) jhpr 2009-09-25
		//return;//erick_pm 31-08-2009
	}
			
	fc_Check();
	document.getElementById("txtTitulo").readOnly=true;
	
	var tieneSancion = document.getElementById("txhFlgSancion").value;
	//alert(tieneSancion);	
	if (tieneSancion=="1"){
		var fechaFin = document.getElementById("txhFechaFinSancion").value;
		var nomUsuario = document.getElementById("txtUsuarioNombre").value;			
		alert("El Usuario "+ nomUsuario +" tiene una sanci�n vigente hasta el d�a " + fechaFin);		
	}else if (tieneSancion=="") {	//erick_pm 31-08-2009
		//document.getElementById("txtUsuariologin").focus();
		document.getElementById("txtNroIngreso").focus();
	}else if (tieneSancion=="0") {
			document.getElementById("txtNroIngreso").focus();
	}

	var tieneInhabilitacion = document.getElementById("txhFlgInhabilitado").value
	
	if (tieneInhabilitacion=="1"){
		var fechaFin2 = document.getElementById("txhFechaFinInhabilitado").value;
		var nomUsuario2 = document.getElementById("txtUsuarioNombre").value;			
		alert("El Usuario "+ nomUsuario2 +" esta inhabilitado hasta el d�a " + fechaFin2);		
	}else if (tieneInhabilitacion==""){//erick_pm 31-08-2009
		//document.getElementById("txtUsuariologin").focus();
		document.getElementById("txtNroIngreso").focus();
	}else if (tieneInhabilitacion=="0"){
		document.getElementById("txtNroIngreso").focus();
	}

	if(busca_codigo!=""){//erick_pm 31-08-2009
		fc_openUsuarios(busca_codigo,busca_tipo); //101555
	}
}


	function limpiaCasilla(nroCasilla){
		document.getElementById('txtNroIngreso'+nroCasilla+'').value='';
		document.getElementById('txtTipomaterial'+nroCasilla+'').value='';
		document.getElementById('txtTitulo'+nroCasilla+'').value='';
		document.getElementById('txhCodDewey'+nroCasilla+'').value='';
		document.getElementById('txhCodMaterial'+nroCasilla+'').value='';	
		document.getElementById('txhFlgMatIndCasa'+nroCasilla+'').value='';		
		document.getElementById('txhFlgSancion'+nroCasilla+'').value='';
		document.getElementById('txhFlgPrestado'+nroCasilla+'').value='';
		document.getElementById('txhFlgLibroRetirado'+nroCasilla+'').value='';
		document.getElementById('txhSedeMat'+nroCasilla+'').value='';
		document.getElementById('txhNroIngresoReal'+nroCasilla+'').value='';		
	}

	function limpiaDetPrestamo(){
		var i=0;
		for(i=1;i<=5;i++){
			limpiaCasilla(i);
		}
	}

function fc_capturaKey(e){
	if(e.keyCode == 13){
		document.getElementById("txtNroIngreso").focus();
	}
}

function fc_capturaKeyMat(e){
	if(e.keyCode == 13){
		document.getElementById("ASi").focus();
	}
}

function fc_VerificarCodOraAlumno(){	
	document.getElementById("operacion").value="irGetAlumno2";
	document.getElementById("frmMain").submit();		
}

function fc_openUsuarios(codigo,tipo){
	Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_prestamo_busca_usuario.html?txhCodUsuario="+codigo+"&txhTipo="+tipo,600,400);
}


function fc_Check(){
	if(document.getElementById("chkIndPrestamo") != null){//erick_pm 31-08-2009
		document.getElementById("chkIndPrestamo").disabled = false;
		document.getElementById("txtUsuariologin").className = "cajatexto_o";
		
		var permisoLibro =  fc_permisoMaterial();
		
		if(fc_Trim(document.getElementById("txhFlgMatIndCasa").value) == "0" &&
			fc_Trim(document.getElementById("txhFlgMatIndSala").value) == "0"){
			document.getElementById("txtUsuariologin").disabled = true;
			document.getElementById("txtUsuariologin").className = "cajatexto_1";
		}else if(fc_Trim(document.getElementById("txhFlgMatIndSala").value) == "0"){
			//document.getElementById("chkIndPrestamo").disabled = true;
		}else {
			//document.getElementById("chkIndPrestamo").disabled = false;
		}
	
		if(permisoLibro==0){
			document.getElementById("chkIndPrestamo").disabled = true;			
		}else if(permisoLibro==1){
			document.getElementById("chkIndPrestamo").disabled = false;
			//document.getElementById("txtUsuariologin").className = "cajatexto_1";
		}else if(permisoLibro==2){
			document.getElementById("chkIndPrestamo").checked = true;
			document.getElementById("chkIndPrestamo").disabled = true;
		}else if(permisoLibro==3){
			document.getElementById("chkIndPrestamo").disabled = true;
		}	
	}
}

function fc_usuario(){
	var usu = document.getElementById("txhCodUsuario").value

	if(usu!="")	
		Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_prestamo_datos_usuario.html?txhCodUsuario="+usu,700,250);
	else
		alert("realize una b�squeda primero");	
}

function Fc_Sancionado(){
	Sancionado.style.display="";	
}

function fc_grabar(){
	if(document.getElementById('prestamoMultiple').checked==1){
		//Prestamo de varios libros
		if(fc_ValidaMultiple()){
			if(confirm(mstrSeguroGrabar)){	
				form = document.forms[0];
				form.operacion.value="irRegistrarMultiple";
				form.chkIndPrestamo.disabled = false;			
				form.imgGrabar.disabled=true;			
				form.txhTipo.value="0";				
				form.submit();	
				
			}else{
				form.imgGrabar.disabled=false;
			}
		}
	}else{
		
		//Prestamo de 1 libro
		if(fc_valida()){	
			if(confirm(mstrSeguroGrabar)){	
				form = document.forms[0];
				form.operacion.value="irRegistrar";
				form.chkIndPrestamo.disabled = false;			
				form.imgGrabar.disabled=true;			
				form.txhTipo.value="0";				
				form.submit();					
			}else{
				form.imgGrabar.disabled=false;
			}
		}
	}
		
	
}

	function fc_ValidaMultiple(){
		var tieneSancion = document.getElementById("txhFlgSancion").value;
		if (tieneSancion==1){
			var fechaFin = document.getElementById("txhFechaFinSancion").value;
			alert("El Usuario est� sancionado hasta el d�a " + fechaFin);
			return false;
		}
		var txtTitulo1 = document.getElementById("txtTitulo1").value;
		var txtTitulo2 = document.getElementById("txtTitulo2").value;
		var txtTitulo3 = document.getElementById("txtTitulo3").value;
		var txtTitulo4 = document.getElementById("txtTitulo4").value;
		var txtTitulo5 = document.getElementById("txtTitulo5").value;
				
		if(txtTitulo1!='')
			if(!fc_SubValidacion('1'))
				return false;		
		if(txtTitulo2!='')
			if(!fc_SubValidacion('2'))
				return false;		
		if(txtTitulo3!='')
			if(!fc_SubValidacion('3'))
				return false;
		if(txtTitulo4!='')
			if(!fc_SubValidacion('4'))
				return false;
		if(txtTitulo5!='')
			if(!fc_SubValidacion('5'))
				return false;
		
		
		//validamos cantidad de libros que se prestar�...
		//contador libros que se le van a prestar...
		var cont = 0;
		if ((document.getElementById('txtNroIngreso1').value!='')) cont++;
		if ((document.getElementById('txtNroIngreso2').value!='')) cont++;
		if ((document.getElementById('txtNroIngreso3').value!='')) cont++;
		if ((document.getElementById('txtNroIngreso4').value!='')) cont++;
		if ((document.getElementById('txtNroIngreso5').value!='')) cont++;		
		
		var prestamos = parseInt(document.getElementById('txhNroPrestamos').value);
		var limite = parseInt(document.getElementById('txhLimitePrestamos').value);
					
		if(parseInt(prestamos + cont) >  parseInt(limite)){
			alert('Limite de prestamos excedido. Debe quitar ' + parseInt(parseInt(prestamos + cont) - parseInt(limite)) + ' ejemplares');
			return false;
		}
		
		return true;
	}

	function fc_SubValidacion(Nro){
		var sedeMat = fc_Trim(document.getElementById("txhSedeMat"+Nro).value);
		var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
		if (sedeMat!='')
			if (sedeMat!=sedeUsu){
				alert('El libro ' + Nro + ' pertenece a una Sede diferente.');
				return false;
			}
		
		if(fc_Trim(document.getElementById("txhFlgLibroRetirado"+Nro).value)=="1"){
			alert('El c�digo de libro ' + Nro + ' est� retirado de la biblioteca.');
			return false;
		}else if(fc_Trim(document.getElementById("txhFlgPrestado"+Nro).value)=="1"){
			alert('El libro ' + Nro + ' ha sido prestado no se puede registrar');		
			return false;
		}else if(fc_Trim(document.getElementById("txtNroIngreso"+Nro).value)==""){
			alert('Debe Ingresar Nro de Ingreso en ' + Nro);			
			return false;
		}else if(fc_Trim(document.getElementById("txhCodMaterial"+Nro).value)==""){
			alert('El material ' + Nro + ' No es v�lido o ha sido prestado.');	
			return false;
		}else if(fc_Trim(document.getElementById("txtUsuariologin").value)==""){
			alert('Debe ingresar un c�digo de usuario.\no Nro. de ingreso');
			document.getElementById("txtUsuariologin").focus();
			return false;
		}else if(fc_Trim(document.getElementById("txtUsuarioNombre").value)==""){
			alert('Debe ingresar usuario valido.');
			document.getElementById("txtUsuarioNombre").focus();
			return false;
		}else if(fc_Trim(document.getElementById("txhSedeMat"+Nro).value) != fc_Trim(document.getElementById("txhSedeUsu").value)){
			alert('El Usuario y libro ' + Nro + ' pertenecen a sedes distintas.\nNo se puede realizar el pr�stamo.');
			return false;
		}
		
		return true;
	}
	
function fc_valida(){
	var tieneSancion = document.getElementById("txhFlgSancion").value;
	if (tieneSancion==1){
		var fechaFin = document.getElementById("txhFechaFinSancion").value;
		alert("El Usuario est� sancionado hasta el d�a " + fechaFin);
		return 0;
	}

	var sedeMat = fc_Trim(document.getElementById("txhSedeMat").value);
	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
	
	if (sedeMat!=''){
		if (sedeMat!=sedeUsu){
			alert('El libro pertenece a una Sede diferente.');
			return 0;
		}
	}
	
	if (document.getElementById("txhSedeMat").value){
		if(fc_Trim(document.getElementById("txhFlgLibroRetirado").value)=="1"){
			alert('El c�digo de libro seleccionado est� retirado de la biblioteca.');
			return 0;
		}
		else if(fc_Trim(document.getElementById("txhFlgPrestado").value)=="1"){
			alert("El libro ha sido prestado no se puede registrar");		
			return 0;
		}else if(fc_Trim(document.getElementById("txtNroIngreso").value)==""){
			alert('Debe Ingresar un Nro de Ingreso.');
			document.getElementById("txtNroIngreso").focus();	
			return 0;
		}else if(fc_Trim(document.getElementById("txhCodMaterial").value)==""){
			alert('El material No es v�lido o ha sido prestado.');	
			return 0;
		}else if(fc_Trim(document.getElementById("txtUsuariologin").value)==""){
			alert('Debe ingresar un c�digo de usuario.\no Nro. de ingreso');
			document.getElementById("txtUsuariologin").focus();
			return 0;
		}else if(fc_Trim(document.getElementById("txtUsuarioNombre").value)==""){
			alert('Debe ingresar usuario valido.');
			document.getElementById("txtUsuarioNombre").focus();
			return 0;
		}else if(fc_Trim(document.getElementById("txhSedeMat").value) != fc_Trim(document.getElementById("txhSedeUsu").value)){
			alert('El Usuario y libro seleccionado pertenecen a sedes distintas.\nNo se puede realizar el pr�stamo.');
			return 0;
		}
	}
	
	if(!valida()){
		return 0;
	}

	return 1;
}

function valida(){
 var permisoUsuario = fc_permisoUsuario();
 var permisoLibro =  fc_permisoMaterial();
 var chek = document.getElementById("chkIndPrestamo").checked;
 //var chk = true;
 
 if(permisoLibro==0){
 	alert("El libro no tiene permisos para ser prestado");
 	return 0;
 }else if(permisoLibro==1){
 	//LIBRO SALA Y DOMICILIO
 	if(permisoUsuario==0){
 		alert("El usuario no tiene permiso para realizar prestamos");
 		return 0; 
 	}
 	
 	if( permisoUsuario==2 && chek==false ){
 		alert("El usuario solo tiene premiso para realizar prestamos a sala");
 		return 0;  	
 	}
 	
 	if( permisoUsuario==3 && chek==true ){
 		alert("El usuario solo tiene premiso para realizar prestamos a domicilio");
 		return 0;  	
 	}
 	
 }else if(permisoLibro==2){
 	//USU SOLO SALA
 	if(permisoUsuario==0 || permisoUsuario==3  ){
 		//LIBRO DOMI
 		alert("El usuario no tiene permisos para prestar en sala");
 		return 0; 
 	}
 }else if(permisoLibro==3){
 	//USU DOMICILIO
 	if(permisoUsuario==0 || permisoUsuario==2  ){
 		//LIBRO SALA
 		alert("El usuario no tiene permisos para prestar a domicilio");
 		return 0; 
 	}
 }
 
 return 1;
 
}


function fc_permisoUsuario(){
	var flgDomi = document.getElementById("txhFlgPermitido").value
	var flgSala = document.getElementById("txhFlgPermitidoSala").value
	
	if( fc_Trim(flgDomi)=="1" && fc_Trim(flgSala)=="1" )
	return 1;
	else if( fc_Trim(flgSala)=="1" )
	return 2;
	else if( fc_Trim(flgDomi)=="1" )		
	return 3;
	else
		return 0;	
}

function fc_permisoMaterial(){
	var flgDomi = document.getElementById("txhFlgMatIndCasa").value
	var flgSala = document.getElementById("txhFlgMatIndSala").value
	
	if( fc_Trim(flgDomi)=="1" && fc_Trim(flgSala)=="1" )
	return 1;
	else if( fc_Trim(flgSala)=="1" )
	return 2;
	else if( fc_Trim(flgDomi)=="1" )		
	return 3;
	else
		return 0; 	
}


function fc_verificaNroIngreso(){
	if(document.getElementById("txtNroIngreso").value!=""){			
		document.getElementById("operacion").value="irGetMaterial";	
		document.getElementById("frmMain").submit();
	}else{
		document.getElementById("txtTipomaterial").value="";
		document.getElementById("txtTitulo").value="";
		document.getElementById("txhCodDewey").value="";
		document.getElementById("txhCodMaterial").value="";	
	}	
}

function fc_verificaNroIngreso_Multiple(Numero){
	if(document.getElementById("txtNroIngreso"+Numero).value!=""){			
		document.getElementById("operacion").value="irGetMateriales";	
		document.getElementById("txtNroMat").value=Numero;
		document.getElementById("frmMain").submit();
	}else{
		limpiaCasilla(Numero);		
	}	
}

function fc_verificaCodigoAlumno(){
	if(document.getElementById("txtUsuariologin").value!="")
	{	
		//jhpr 2008-08-15	
		document.getElementById("operacion").value="irGetAlumno";	
		document.getElementById("frmMain").submit();
	}else{
		document.getElementById("txtUsuarioNombre").value="";	
	}
	document.getElementById("txtNroIngreso").focus();
}


function Fc_Cambia(){
	if(cboTipoApl.value=='02'){location.href="Configuracion_Bandeja_TS.htm";}
}


function fc_Menu(){
	window.location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;
	//location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;
}

function fc_Regresar(){

	strParametrosBusqueda = "&prmTxtCodigo="+document.getElementById("prmTxtCodigo").value+
	"&prmTxtNroIngreso="+document.getElementById("prmTxtNroIngreso").value+
	"&prmCboTipoMaterial="+document.getElementById("prmCboTipoMaterial").value+
	"&prmCboBuscarPor="+document.getElementById("prmCboBuscarPor").value+
	"&prmTxtTitulo="+document.getElementById("prmTxtTitulo").value+
	"&prmCboIdioma="+document.getElementById("prmCboIdioma").value+
	"&prmCboAnioIni="+document.getElementById("prmCboAnioIni").value+
	"&prmCboAnioFin="+document.getElementById("prmCboAnioFin").value+
	"&txhCodSede="+document.getElementById("txhSedeSel").value+
	"&txhSedeUsuario="+document.getElementById("txhSedeUsuario").value+	
	"&prmTxtFechaReservaIni="+document.getElementById("prmTxtFechaReservaIni").value+
	"&prmTxtFechaReservaFin="+document.getElementById("prmTxtFechaReservaFin").value;

 	location.href="${ctx}/biblioteca/biblio_admin_gestion_material.html?"+
 	"txhCodUsuario="+document.getElementById("usuario").value+
 	"&irRegresar=true"+strParametrosBusqueda;
}

function fc_Enviar()
{	
	if(document.getElementById("txtUsuariologin").value=="")
		srtCodUsuario="";	
	else
		srtCodUsuario=document.getElementById("txhCodUsuario").value;
	
	srtUsuarioLogin=document.getElementById("txtUsuariologin").value;	
	srtDscUsuario=document.getElementById("txtUsuarioNombre").value;
	srtNroIngreso=document.getElementById("txtNroIngreso").value;
	srtTitulo=document.getElementById("txtTitulo").value;
	
	if(document.getElementById("txtUsuarioNombre").value!="" || document.getElementById("txtNroIngreso").value!=""  )
	 {		 	
	 	Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_prestamo_flotante.html"+
	 	"?txhCodUsuario="+srtCodUsuario+
	 	"&txhDscUsuario="+ srtDscUsuario+
	    "&txhNroIngreso="+ srtNroIngreso+
	    "&txhTitulo="+ srtTitulo+
	    "&txhUsuarioLogin="+srtUsuarioLogin	    
	    ,800,470);
	 }
	 else {
	 	alert('Debe ingresar un c�digo de usuario.\no Nro. de ingreso');
	 	document.getElementById("txtUsuariologin").focus();
	 	return false;
	 }
}
</script>



</head>
<body >
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_registrar_prestamo.html">

<form:hidden path="operacion" id="operacion" />
<form:hidden path="txtNroMat" id="txtNroMat" />
<form:hidden path="txhCodMaterial" id="txhCodMaterial" />

<form:hidden path="txhCodMaterial1" id="txhCodMaterial1" />
<form:hidden path="txhCodMaterial2" id="txhCodMaterial2" />
<form:hidden path="txhCodMaterial3" id="txhCodMaterial3" />
<form:hidden path="txhCodMaterial4" id="txhCodMaterial4" />
<form:hidden path="txhCodMaterial5" id="txhCodMaterial5" />

<form:hidden path="txhTipo" id="txhTipo" />
<form:hidden path="usuario" id="usuario" />

<form:hidden path="txhCodUnico" id="txhCodUnico" />

<form:hidden path="txhNroIngreso" id="txhNroIngreso" />

<!--********* ACCESOS Y PERMISOS DE DEL USUARIO**********************-->
<form:hidden path="txhFlgPermitido" id="txhFlgPermitido" />
<form:hidden path="txhFlgPermitidoSala" id="txhFlgPermitidoSala" />
<!--*****************************************************************-->
<!--**************ACCESOS Y PERMISOS DE DEL MATERIAL*****************-->
<form:hidden path="txhFlgMatIndCasa" id="txhFlgMatIndCasa" />
<form:hidden path="txhFlgMatIndCasa1" id="txhFlgMatIndCasa1" />
<form:hidden path="txhFlgMatIndCasa2" id="txhFlgMatIndCasa2" />
<form:hidden path="txhFlgMatIndCasa3" id="txhFlgMatIndCasa3" />
<form:hidden path="txhFlgMatIndCasa4" id="txhFlgMatIndCasa4" />
<form:hidden path="txhFlgMatIndCasa5" id="txhFlgMatIndCasa5" />

<form:hidden path="txhFlgMatIndSala" id="txhFlgMatIndSala" />
<form:hidden path="txhFlgMatIndSala1" id="txhFlgMatIndSala1" />
<form:hidden path="txhFlgMatIndSala2" id="txhFlgMatIndSala2" />
<form:hidden path="txhFlgMatIndSala3" id="txhFlgMatIndSala3" />
<form:hidden path="txhFlgMatIndSala4" id="txhFlgMatIndSala4" />
<form:hidden path="txhFlgMatIndSala5" id="txhFlgMatIndSala5" />

<form:hidden path="txhFlgSancion" id="txhFlgSancion"/>
<form:hidden path="txhFlgSancion1" id="txhFlgSancion1"/>
<form:hidden path="txhFlgSancion2" id="txhFlgSancion2"/>
<form:hidden path="txhFlgSancion3" id="txhFlgSancion3"/>
<form:hidden path="txhFlgSancion4" id="txhFlgSancion4"/>
<form:hidden path="txhFlgSancion5" id="txhFlgSancion5"/>

<form:hidden path="fechaFinSancion" id="txhFechaFinSancion"/>
<!--*****************************************************************-->

<!-- INHABILITACION -->
<form:hidden path="txhFlgInhabilitado" id="txhFlgInhabilitado"/>
<form:hidden path="fechaFinInhabilitado" id="txhFechaFinInhabilitado"/>

<form:hidden path="txhFlgPrestado" id="txhFlgPrestado" />
<form:hidden path="txhFlgPrestado1" id="txhFlgPrestado1" />
<form:hidden path="txhFlgPrestado2" id="txhFlgPrestado2" />
<form:hidden path="txhFlgPrestado3" id="txhFlgPrestado3" />
<form:hidden path="txhFlgPrestado4" id="txhFlgPrestado4" />
<form:hidden path="txhFlgPrestado5" id="txhFlgPrestado5" />

<!-- Reserva -->
<form:hidden path="txhFlgReserva" id="txhFlgReserva" />
<form:hidden path="txtCodUsuarioReserva" id="txtCodUsuarioReserva" />
<form:hidden path="txhFlgLibroRetirado" id="txhFlgLibroRetirado" />
<form:hidden path="txhFlgLibroRetirado1" id="txhFlgLibroRetirado1" />
<form:hidden path="txhFlgLibroRetirado2" id="txhFlgLibroRetirado2" />
<form:hidden path="txhFlgLibroRetirado3" id="txhFlgLibroRetirado3" />
<form:hidden path="txhFlgLibroRetirado4" id="txhFlgLibroRetirado4" />
<form:hidden path="txhFlgLibroRetirado5" id="txhFlgLibroRetirado5" />

<form:hidden path="txhSedeMat" id="txhSedeMat" />
<form:hidden path="txhSedeMat1" id="txhSedeMat1" />
<form:hidden path="txhSedeMat2" id="txhSedeMat2" />
<form:hidden path="txhSedeMat3" id="txhSedeMat3" />
<form:hidden path="txhSedeMat4" id="txhSedeMat4" />
<form:hidden path="txhSedeMat5" id="txhSedeMat5" />

<form:hidden path="txhSedeUsu" id="txhSedeUsu" />

<!-- USUARIO DEL LOGIN -->
<form:hidden path="txhCodUsuario" id="txhCodUsuario" />
<form:hidden path="flgBusqXCodOracle" id="txhflgBusqXCodOracle"/>

<!-- SEDE -->
<form:hidden path="sedeSel" id="txhSedeSel" />
<form:hidden path="sedeUsuario" id="txhSedeUsuario" />

<form:hidden path="nroIngresoReal" id="txhNroIngresoReal" />
<form:hidden path="nroIngresoReal1" id="txhNroIngresoReal1" />
<form:hidden path="nroIngresoReal2" id="txhNroIngresoReal2" />
<form:hidden path="nroIngresoReal3" id="txhNroIngresoReal3" />
<form:hidden path="nroIngresoReal4" id="txhNroIngresoReal4" />
<form:hidden path="nroIngresoReal5" id="txhNroIngresoReal5" />

<form:hidden path="txtNroPrestamos" id="txhNroPrestamos"/>
<form:hidden path="txtLimitePrestamos" id="txhLimitePrestamos"/>

	

	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>
				&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Menu();" style="cursor: hand" alt="Regresar">
			</td>
		</tr>
	</table>
	
	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" style="margin-left:11px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo"><font style="">Registrar Pr&eacute;stamo</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	 </table>
	 <!-- table><tr height="3px"><td></td></tr></table-->
	 
	 
	<table border="0" cellspacing="0" cellpadding="0" style="width:94%;margin-top:0px;margin-left:11px;">
		<tr>
			<td>
				<table background="${ctx}/images/biblioteca/fondoinf.jpg" style="width:100%;margin-left:0px" border="0" cellspacing="4" cellpadding="5"
					height="80px" class="tabla">
					

					  <td>Tipo de Usuario </td>
					  <td colspan="3">
					  	
					  	<form:radiobutton path="tipousuario" id="ASi" value="0"/>Alumno PFR / Participante&nbsp;&nbsp;&nbsp;
						<form:radiobutton path="tipousuario" id="ANo" value="1"/>Trabajador
					  						  
					    </td>
					  <td align="left">&nbsp;</td>
				  </tr>
					<tr>	
						<td>&nbsp;Usuario :</td>
						<td colspan="2">&nbsp;	
				
							<form:input path="txtUsuariologin" id="txtUsuariologin" cssClass="cajatexto_o"
								onkeydown="fc_capturaKey(event);" 
								onblur="fc_verificaCodigoAlumno();" 
								onchange="casilla();" 
								onkeypress="casilla();" 
								onkeyup="casilla();"  />
							&nbsp;					
							<form:input path="txtUsuarioNombre" id="txtUsuarioNombre" cssStyle="width:380px"  readonly="readonly"  cssClass="cajatexto"  />
							&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
										<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Ver Datos Usuario" id="imgBuscar" style="CURSOR: pointer" onclick="fc_usuario();"></a>
							
						</td>
						<td align="left">&nbsp;
							&nbsp;Nro. Pr�st.:&nbsp;
							
							<c:if test="${control.txtNroPrestamos==0}">
								<a href="#" style="color: #fff;">${control.txtNroPrestamos}</a>
							</c:if>
							<c:if test="${control.txtNroPrestamos>0}">
								<a href="#" onclick="fc_VerPrestados('${control.txhCodUsuario}');" style="color: #fff;">${control.txtNroPrestamos}</a>
							</c:if>
															
						</td>	
						<td align="left">&nbsp;
							
						</td>					
					</tr>
					<tr>		
						<td ID="Select3" name="Select3" width="14%">&nbsp;Fec. Pr�stamo :</td>
						<td>&nbsp;
							<form:input path="txtFecPrestamo" id="txtFecPrestamo" cssClass="cajatexto_1" readonly="readonly"  cssStyle="width:70px" />
						</td>
						<td ID="Select3" name="Select3">&nbsp;Fec. Devoluci�n Prog. :</td>
						<td>&nbsp;
							<form:input path="txtFecDevolucionProrroga" id="txtFecDevolucionProrroga" readonly="readonly"  cssClass="cajatexto_1" cssStyle="width:70px" maxlength="10" size="10"/>
							<form:hidden path="txtFecDevolucionProrroga2" id="auxtxtFecDevolucionProrroga" />
						</td>				
						<td colspan="2">&nbsp;
							 
						</td>
					</tr>
					<tr>					
				</table>
			</td>
		</tr>
	</table>	
	<table border="0" cellspacing="0" cellpadding="0" style="width:94%;margin-top:0px;margin-left:17px;">
		<tr>
			<td>
				<table class="tabla" cellspacing="4" cellpadding="5" border="0" id="Sancionado" style="display:none;width:98%" align="center">
					<tr>
						<td>&nbsp;Sancionado :</td>
						<td colspan="2">&nbsp;
							<form:input path="txtSancionado" id="txtSancionado" cssClass="cajatexto_1" cssStyle="width:550px; height:50px" readonly="readonly" />			
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>	 
	 
	 <table border="0" cellspacing="0" cellpadding="0" style="width:94%;margin-top:0px;margin-left:11px;">
		<tr>
			<td bgcolor="#07A9D8">
				<div id="panelDefault">
				<table bgcolor="#07A9D8" style="width:100%;height:70px;margin-left: 0px;" cellspacing="2" cellpadding="2" border="0" bordercolor="red" class="tabla">	
					<tr>
						<td id="Select3" name="Select3" width="14%">&nbsp;Nro.Ingreso :</td>
						<td>&nbsp;							
							<form:input path="txtNroIngreso" id="txtNroIngreso" 
							 onblur="fc_verificaNroIngreso()"
							 onkeydown="fc_capturaKeyMat(event);"  cssClass="cajatexto_o"/>
						</td>
						<td ID="Select3" name="Select3">&nbsp;Tipo Material :</td>
						<td>&nbsp;
							<form:input path="txtTipomaterial" id="txtTipomaterial" readonly="readonly" cssClass="cajatexto_1" cssStyle="width:210px" />
						</td>
						<td ID="Select3" name="Select3">&nbsp;Dewey :</td>
						<td>&nbsp;
							<form:input path="txhCodDewey" id="txhCodDewey" readonly="readonly" cssClass="cajatexto_1" size="25" />
						</td>
					</tr>
					<tr>				
						<td id="Select3" name="Select3" valign="top">&nbsp;T�tulo :</td>
						<td colspan="3">&nbsp;
							<form:textarea path="txtTitulo" id="txtTitulo" rows="3" cssStyle="width:495px;" cssClass="cajatexto_1"/>
						</td>
						<td colspan="2">
							<form:checkbox id="chkIndPrestamo" path="chkIndPrestamo" value="0" onclick="javascript:verFecha(this);" />&nbsp;Pr�stamo en sala
						</td>
					</tr>
					
				</table>
				</div>
				
				<table style="width:100%;margin-left: 0px;" bgcolor="#07A9D8" class="tabla" align="center">
					<tr>
						<td>
							<input type="Checkbox" name="prestamoMultiple" id="prestamoMultiple" value="0"> Pr�stamo m�ltiple&nbsp;
						</td>
					</tr>
				</table>
				<div id="panelMultiple">
					<table style="width:890px;margin-left: 0px;" bgcolor="#07A9D8" border="0" class="tabla" cellpadding="3" cellspacing="1">
						<tr>
							<td width="5px">&nbsp;</td>
							<td width="15px">&nbsp;</td>
							<td width="80px">Nro. Ingreso</td>	
							<td width="120px">Tipo Material</td>
							<td width="400">T�tulo</td>
							<td width="80px">Dewey</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>1.</td>											
							<td width="80px"><form:input path="txtNroIngreso1" onblur="fc_verificaNroIngreso_Multiple('1')" onkeypress="" id="txtNroIngreso1" cssClass="cajatexto_o" cssStyle="width:80px;"/></td>
							<td width="120px"><form:input path="txtTipomaterial1" id="txtTipomaterial1" cssClass="cajatexto_1" cssStyle="width: 120px;"/></td>						
							<td width="350px"><form:input path="txtTitulo1" id="txtTitulo1" cssClass="cajatexto_1" cssStyle="width: 400PX;"/></td>						
							<td width="80px"><form:input path="txhCodDewey1" id="txhCodDewey1" cssClass="cajatexto_1" cssStyle="width: 80px;"/></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>2.</td>
							<td width="80px"><form:input path="txtNroIngreso2" id="txtNroIngreso2" cssClass="cajatexto_o" cssStyle="width:80px;" onblur="fc_verificaNroIngreso_Multiple('2')"/></td>
							<td width="120px"><form:input path="txtTipomaterial2" id="txtTipomaterial2" cssClass="cajatexto_1" cssStyle="width: 120px;"/></td>						
							<td width="350px"><form:input path="txtTitulo2" id="txtTitulo2" cssClass="cajatexto_1" cssStyle="width: 400PX;"/></td>						
							<td width="80px"><form:input path="txhCodDewey2" id="txhCodDewey2" cssClass="cajatexto_1" cssStyle="width: 80px;"/></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>3.</td>
							<td width="80px"><form:input path="txtNroIngreso3" id="txtNroIngreso3" cssClass="cajatexto_o" cssStyle="width:80px;" onblur="fc_verificaNroIngreso_Multiple('3')"/></td>
							<td width="120px"><form:input path="txtTipomaterial3" id="txtTipomaterial3" cssClass="cajatexto_1" cssStyle="width: 120px;"/></td>						
							<td width="350px"><form:input path="txtTitulo3" id="txtTitulo3" cssClass="cajatexto_1" cssStyle="width: 400PX;"/></td>						
							<td width="80px"><form:input path="txhCodDewey3" id="txhCodDewey3" cssClass="cajatexto_1" cssStyle="width: 80px;"/></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>4.</td>
							<td width="80px"><form:input path="txtNroIngreso4" id="txtNroIngreso4" cssClass="cajatexto_o" cssStyle="width:80px;" onblur="fc_verificaNroIngreso_Multiple('4')"/></td>
							<td width="120px"><form:input path="txtTipomaterial4" id="txtTipomaterial4" cssClass="cajatexto_1" cssStyle="width: 120px;"/></td>						
							<td width="350px"><form:input path="txtTitulo4" id="txtTitulo4" cssClass="cajatexto_1" cssStyle="width: 400PX;"/></td>						
							<td width="80px"><form:input path="txhCodDewey4" id="txhCodDewey4" cssClass="cajatexto_1" cssStyle="width: 80px;"/></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>5.</td>
							<td width="80px"><form:input path="txtNroIngreso5" id="txtNroIngreso5" cssClass="cajatexto_o" cssStyle="width:80px;" onblur="fc_verificaNroIngreso_Multiple('5')"/></td>
							<td width="120px"><form:input path="txtTipomaterial5" id="txtTipomaterial5" cssClass="cajatexto_1" cssStyle="width: 120px;"/></td>						
							<td width="350px"><form:input path="txtTitulo5" id="txtTitulo5" cssClass="cajatexto_1" cssStyle="width: 400PX;"/></td>						
							<td width="80px"><form:input path="txhCodDewey5" id="txhCodDewey5" cssClass="cajatexto_1" cssStyle="width: 80px;"/></td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
			</td>
		<tr>
			<td colspan="5"><img src="../images/separador_n.gif" width="100%"></td>				
		</tr>
	</table>

	<table align="right" border="0" cellspacing="2" cellpadding="2" style="width:35%;margin-top:10px;margin-right:45px">
		<tr>
			<td align="right" width="">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" align="absmiddle" alt="Grabar" id="imgGrabar" name="imgGrabar" onclick="javascript:fc_grabar();" style="cursor:pointer;"></a>		
			</td>
			<td align="right" width="">
			&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviar','','${ctx}/images/biblioteca/Gest_Mat/consultarReserva2.jpg',1)">
				<img src="${ctx}/images/biblioteca/Gest_Mat/consultarReserva1.jpg" align="absmiddle" alt="Consultar Reservas" id="imgEnviar" style="cursor:pointer;" onclick="fc_Enviar();"></a>
			</td>
            <td align="right" width="">
			&nbsp;<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
				<img src="${ctx}/images/botones/regresar1.jpg" align="absmiddle" alt="Regresar" id="imgRegresar" style="cursor:pointer;" onclick="fc_Regresar();"></a>
			</td>

		</tr>	
	</table>
		
		<script type="text/javascript">
		if(document.getElementById("operacion").value=='irGetMateriales')
			document.getElementById("operacion").value=='';
		</script>
		
	<!-- PARAMETROS DE B�SQUEDA DE LA P�GINA ANTERIOR -->
	<form:hidden path="prmTxtCodigo" id="prmTxtCodigo" />
	<form:hidden path="prmTxtNroIngreso" id="prmTxtNroIngreso" />
	<form:hidden path="prmCboTipoMaterial" id="prmCboTipoMaterial" />
	<form:hidden path="prmCboBuscarPor" id="prmCboBuscarPor" />
	<form:hidden path="prmTxtTitulo" id="prmTxtTitulo" />
	
	<form:hidden path="prmCboIdioma" id="prmCboIdioma" />
	<form:hidden path="prmCboAnioIni" id="prmCboAnioIni" />
	<form:hidden path="prmCboAnioFin" id="prmCboAnioFin" />
	<form:hidden path="prmTxtFechaReservaIni" id="prmTxtFechaReservaIni" />
	<form:hidden path="prmTxtFechaReservaFin" id="prmTxtFechaReservaFin" />		

	<script type="text/javascript">
		<%if(item_numero != null && !item_numero.equals("")){
			if(Integer.valueOf(item_numero).intValue()<5){
		%>
			var item = <%=item_numero%>;
			item = item + 1;
			var nom = 'txtNroIngreso' + item;			
			document.getElementById(nom).focus();
		<%}
		}%>
		
							
	</script>
		
		<div id="dialog-error" title="Biblioteca">
			<div class="ui-widget" style="font-size: 11px;">
				<div class="ui-state-error ui-corner-all">
					<p style="margin-top: 0.7em; margin-bottom: 0.7em; margin-left: 0em;">
						<span class="ui-icon ui-icon-alert" style="float: left; margin-left: 0.5em;">
						</span>
						<span id="msgError">							
						</span>
					</p>
				</div>
			</div>
		</div>
		
		<div id="dialog-error-adeuda" title="Biblioteca">
			<div class="ui-widget" style="font-size: 11px;">
				<div class="ui-state-error ui-corner-all">
					<p style="margin-top: 0.7em; margin-bottom: 0.7em; margin-left: 0em;">
						<span class="ui-icon ui-icon-alert" style="float: left; margin-left: 0.5em;">
						</span>
						<span id="msgError-adeuda">							
						</span>
					</p>
				</div>
			</div>
		</div>
		
		<div id="dialog-confirmacion" title="Biblioteca">
			<div class="ui-widget" style="font-size: 11px;">
				<div class="ui-state-highlight ui-corner-all">
					<p style="margin-top: 0.7em; margin-bottom: 0.7em; margin-left: 0em;">
						<span class="ui-icon ui-icon-circle-check"
							style="float: left; margin-left: 0.5em"></span>&nbsp;&nbsp;El pr�stamo se registro satisfactoriamente.
					</p>
				</div>
			</div>
		</div>
		
		
	</form:form>		
</body>

