<%@page import="java.sql.Date"%>
<%@page import="com.tecsup.SGA.bean.MaterialxIngresoBean"%>
<%@page import="java.util.List"%>
<jsp:useBean id="fecActual" class="java.lang.String" scope="request"></jsp:useBean>
<jsp:useBean id="fecMinDate" class="java.lang.String" scope="request"></jsp:useBean>
<jsp:useBean id="result" class="java.lang.String" scope="request"></jsp:useBean>
<jsp:useBean id="sancion" class="com.tecsup.SGA.modelo.Sancion" scope="request"></jsp:useBean>
<%@ include file="/taglibs.jsp"%>

<head>
<link href="${ctx}/styles/estilo_biblio_admin2.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/scripts/js/funciones_bib-numeros.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/funciones_bib-fechas.js" type="text/JavaScript"></script>

<%List<MaterialxIngresoBean> bloquePrestamo = (List<MaterialxIngresoBean>)request.getAttribute("bloquePrestamo"); %>

<script type="text/javascript">
	$(function () {
		$("input:submit, button, input:button").button();
				
		$( "#txtfecdevolucion" ).val('<%=fecActual%>');
		
				
		$('#txtfecdevolucion').datepicker({	        
	        regional:"es",
	        dateFormat: "dd/mm/yy",
	        minDate:'<%=fecMinDate%>',
	        firstDay: 1
	    });
				
		
	});
	
	function fc_cerrar(){
		if(parent.closeModal){
			parent.closeModal();
		}
	}
		
	function devolver(item){		
		var oDevol = document.getElementById('chkDevol_'+item);		
		var oPro = document.getElementById('chkPro_'+item);
		if(oDevol.checked==true){
			oPro.checked=false;			
		}
	}
	
	function prorrogar(item){		
		var oDevol = document.getElementById('chkDevol_'+item);		
		var oPro = document.getElementById('chkPro_'+item);
		if(oPro.checked==true){
			oDevol.checked=false;			
		}
	}	
</script>

<script type="text/javascript">
	
function onLoad(){
	
}

function fc_ValidaFechaPrestamoOnblur(strNameObj) { 
       var Obj = document.getElementById(strNameObj);
       var objFecPrestado = document.getElementById("txtFecPrestamo");
       var error=false;
       if (Obj.value !="") {       
			if(Obj.value.length == 8){
				var _anio = Obj.value.substring(6,8)
				Obj.value = Obj.value.substring(0,6)+'20'+_anio
			}			
			if (!isFecha(Obj.value,"dd/MM/yyyy")) error=true;
			else{
				strAnho=Obj.value.split("/")[2];

				if (strAnho<'1900') error=true;                
			}
			if (error){	
				alert('Debe ingresar una fecha v�lida.');
				Obj.focus();
			}else{
				
			    if (objFecPrestado.value!=""){
			    	if(retornaFecha(Obj.value) < retornaFecha(objFecPrestado.value)){			    	
				        alert('Fecha de Proceso no debe ser menor a la fecha de Prestamo');
				        Obj.focus();
			    	}
			    }
			}
       }            
}



function fc_grabar(){
	
	if(!fc_valida()){
		alert('No ha seleccioando materiales para procesar');
		return false;
	}		

	if($('#txtfecdevolucion').val()==''){
		alert('Debe seleccionar una fecha de Proceso');
		return false;
	}
	
	form = document.forms[0];
	var max = <%=bloquePrestamo.size()%>;	
	var nro = 0;
	var i;
	var codIngreso;
	var fechaDevol = $('#txtfecdevolucion').val();
	var sedeMat;
	var datos='';
	var prorroga;
	var codBloque = $('#codPrestamoMultiple').val();
	var codprestamo;
	for(i=0;i<max;i++) {
		var nomChk = 'chkDevol_' + i;
		var nomPro = 'chkPro_' + i;
		
		var fecDevuelto = document.getElementById('otxtfecdevol_' + i).value;
		codprestamo = document.getElementById('otxtcodprest_' + i).value;
		//solo los que el usuario ha seleccionado y aun no han sido devueltos			
		if(fecDevuelto=='' && (document.getElementById(''+nomChk).checked==true || document.getElementById(''+nomPro).checked==true)){
			codIngreso = document.getElementById('otxtcoding_'+i).value;
			sedeMat = document.getElementById('otxtsedemat_'+i).value;
			if(document.getElementById(''+nomPro).checked)
				prorroga='1';
			else
				prorroga='0';
			datos += codIngreso + '$' + fechaDevol + '$' + sedeMat + '$' + prorroga + '$' + codBloque + '$' + codprestamo + '|';
			nro = nro +1;
		}
	}
	
	if(confirm('�Confirma que desea registrar la devoluci�n de material?')){
		form.txtCadenaDatos.value = datos;
		form.txtTamanioLista.value = nro;
		form.operacion.value = "DEVOLUCION_BLOQUE";		
		form.submit();
		
	}

}

function fc_valida(){
	
	//seleccion
	form = document.forms[0];
	var max = <%=bloquePrestamo.size()%>;
	//alert(max);
	var seleccion = false;
	var i;
	for(i=0;i<max;i++) {
		var nomChk = 'chkDevol_' + i;
		var nomPro = 'chkPro_' + i;
		var fecDevol = document.getElementById('otxtfecdevol_' + i).value; 		 
		if(fecDevol==''){
			if(document.getElementById(''+nomChk).checked==true || document.getElementById(''+nomPro).checked==true){
				seleccion = true;			
			}	
		}
		
	}
	
	return seleccion;
}



</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/biblio_admin_registrar_devolucion_bloque.html">

<form:hidden path="operacion" id="operacion" />
<form:hidden path="usuario" id="usuario" />
<form:hidden path="tipoUsuario" id="txhTipoUsuario" />
<!-- SEDE -->
<form:hidden path="sedeSel" id="txhSedeSel" />
<form:hidden path="sedeUsuario" id="txhSedeUsuario" />
<form:hidden path="txtCadenaDatos" id="txtCadenaDatos" />
<form:hidden path="txtTamanioLista" id="txtTamanioLista"/>
<form:hidden path="txhCodUsuarioLogin" id="txhCodUsuarioLogin" />
<form:hidden path="codPrestamoMultiple" id="codPrestamoMultiple" />

	<div class="DivMarco" style="width:98%;">
    	<table border="0" align="center" width="100%" cellpadding="4" cellspacing="0">
            <tr>
                <td align="left">
                    <div class="Caption">&nbsp;Registro de Devoluci&oacute;n de Material Bibliogr&aacute;fico</div>
                </td>                					
            </tr>            
        </table>                   
	</div>		
		<br />
	<div class="DivMarcoTabla" style="width:98%;">
	 <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; margin-top: 0px; margin-left: 11px;">
		 <tr>
			<td>
				<table style="width: 100%;" cellspacing="4" cellpadding="5" border="0">						
					<tr>
						<td width="6%"><b>Nombre:</b></td>
						<td width="22%">
							<div class="InputDatoDesactivo"><%=bloquePrestamo.get(0).getNombreUsuario()%>&nbsp;</div>
							<input type="hidden" name="fecprestamo" id="fecprestamo" value="<%=bloquePrestamo.get(0).getFecPrestamo()%>">
						</td>
						<td width="10%"><b>Tipo Usuario:</b></td>
						<td width="12%">
							<div class="InputDatoDesactivo">&nbsp;<%=bloquePrestamo.get(0).getTipoDeUsuario()%></div>
						</td>
						<td width="6%"><b>Usuario:</b></td>
						<td width="14%">
							<div class="InputDatoDesactivo">&nbsp;<%=bloquePrestamo.get(0).getUsuarioReserva()%></div>
						</td>
						<td width="15%">&nbsp;<b>Fecha de Proceso:</b></td>
						<td width="15%">&nbsp;
							<input type="text" value=""  class="InputDato" style="width: 70px; background-color: #FFF65B" id="txtfecdevolucion" name="txtfecdevolucion"                        
                        			 onblur="javascript:fc_ValidaFechaOnblur(this.id);" maxlength="10"
                        			 
                        			 <c:if test="${control.tipoUsuario!='A'}">
										disabled="disabled"
									</c:if>
                        			 
                        			 />							
						</td>											
					</tr>					
					<%
					//
					if(sancion!=null && sancion.getCodFila()!=null) {%>
					<tr>
						<td colspan="8">
							<table style="width: 100%;" cellspacing="1" cellpadding="1" border="0">
								<tr>
									<td width="6%"><b>Sanci&oacute;n:</b></td>
									<td width="35%"><span style="color: #ff0000;"><%=sancion.getValorSancion2() %></span></td>
									<td width="6%"><b>Desde:</b></td>
									<td width="7%"><%=sancion.getValorSancion3() %></td>
									<td width="6%"><b>Hasta:</b></td>
									<td align="left" width="36%"><%=sancion.getValorSancion4() %></td>
								</tr>
								<tr>
									<td><b>Observaci&oacute;n:</b></td>
									<td colspan="5"><%=sancion.getObservacion() %></td>
								</tr>
							</table>						
						</td>
					</tr>
					<%} %>
					
				</table>										
			</td>
		 </tr>
	 </table>
	 <br />
	 <table style="width: 100%;" cellspacing="0" cellpadding="4" border="0">	
		<tr class="TRCabeceraTabla">
			<td width="30%">T�tulo</td>
			<td width="3%">Lug</td>
			<td width="6%">Tipo</td>
			<td width="7%">Nro. Ing.</td>			
			<td width="8%">Dewey</td>
			<td width="10%">Fec. Prestamo</td>
			<td width="10%">Fec. Devol. Prog.</td>
			<td width="10%">Fec. Devoluci&oacute;n</td>
			<td width="8%">Devoluci�n</td>
			<td width="7%">Renovar</td>			
		</tr>
	</table>
	
	<div style="OVERFLOW:auto; WIDTH: 100%; " >
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
        	<tbody>
        		
			<%if(bloquePrestamo!=null && !bloquePrestamo.isEmpty()) {
				int item = 0;
				String fondofila="";
				for(MaterialxIngresoBean mat : bloquePrestamo){
					if((item%2)==0)
          				fondofila = "TRRow";
          			else
          				fondofila = "TRRowAlterno";
					
					
			%>
			
				<tr class="<%=fondofila%>">
				
					<td class="TDRow" align="left" width="30%">
						<input type="hidden" name="otxtcoding_<%=item%>" id="otxtcoding_<%=item%>" value="<%=mat.getNroIngreso()%>">
						<input type="hidden" name="otxtcodprest_<%=item%>" id="otxtcodprest_<%=item%>" value="<%=mat.getCodPrestamo()%>">
						<input type="hidden" name="otxtfecdevol_<%=item%>" id="otxtfecdevol_<%=item%>" value="<%=mat.getFecDevolucion()%>">
						<input type="hidden" name="otxtsedemat_<%=item%>" id="otxtsedemat_<%=item%>" value="<%=mat.getSede()%>">
                          <%=mat.getTitulo() %>                          	
                    </td>
					<td class="TDRow" align="left" width="3%">
                          <%=mat.getSede().equals("L")?"Lim":(mat.getSede().equals("A")?"Aqp":(mat.getSede().equals("T")?"Tru":(mat.getSede().equals("U")?"Utec":"")))%>                          	
                    </td>
					<td class="TDRow" align="left" width="6%">
                          <%=mat.getDesTipoMaterial() %>                          	
                    </td>
                    
                    <td class="TDRow" align="left" width="7%">
                          <%=mat.getNroIngreso() %>                          	
                    </td>
                    <td class="TDRow" align="left" width="8%">
                          <%=mat.getCodDewey() %>                          	
                    </td>
                    <td class="TDRow" align="left" width="10%">
                          <%=mat.getFecPrestamo() %>                          	
                    </td>
                    <td class="TDRow" align="left" width="10%">
                          <%=mat.getFecDevolucionProg() %>                          	
                    </td>
                    <td class="TDRow" align="left" width="10%">
                          <%=mat.getFecDevolucion() %>                          	
                    </td>
                    <td class="TDRow" width="8%">&nbsp;
                    	<input type="checkbox" name="chkDevol_<%=item%>" id="chkDevol_<%=item%>" onclick="devolver('<%=item%>');" 
                    	<%if(!mat.getFecDevolucion().equals("")) {%>
                    		disabled="disabled"
                    	<%} %>
                    	/>
                    </td>
                    
                    <td class="TDRow" width="7%">&nbsp;
                    	<input type="checkbox" name="chkPro_<%=item%>" id="chkPro_<%=item%>" onclick="prorrogar('<%=item%>')" 
                    	<%
                    	if((Integer.valueOf(mat.getNroProrrogas()).intValue()>=Integer.valueOf(mat.getLimiteProrrogas()).intValue()) 
                    			|| !mat.getFecDevolucion().equals("") ){
                    	%>
                    	disabled="disabled"
                    	<%
                    	}
                    	%>
                    	/>
                    	&nbsp;                    	
                    	<%=mat.getNroProrrogas()%>/<%=mat.getLimiteProrrogas()%>
                    </td>
				</tr>
				
			<%
				item+=1;
				}
			}else{ %>
				
				<tr class="">
					<td colspan="8" align="center">No se encontraron registros</td>
				</tr>
			
			<%} %>
	
			</tbody>
	    </table>
	    
	    
	    
	 </div>
	 
	 	
	 
	</div>
	<br />
	<div class="DivMarcoTabla" style="width:98%;">
		<table border="0" cellspacing="2" cellpadding="2" style="width:98%;">
		<tr>
			<td align="center">
			
				<input type="button" value="Guardar" id="imgGrabar" onclick="javascript:fc_grabar();">
				&nbsp;&nbsp;&nbsp;
				<input type="button" value="Cerrar" id="imgRegresar" onclick="javascript:fc_cerrar();">				
			</td>				
		</tr>	
		</table>
	</div>	 
	
	<br />
	<div class="" style="width:60%; margin-left:20%; margin-right:20%;">
		
		<%if(result!=null && result.equals("1")) {%>
		<div id="dialog-message" title="Biblioteca Tecsup">
			<div class="ui-widget" style="font-size: 11px;">
				<div class="ui-state-highlight ui-corner-all">
					<p style="margin-top: 0.7em; margin-bottom: 0.7em; margin-left: 0em;">
						<span class="ui-icon ui-icon-circle-check"
							style="float: left; margin-left: 1.5em"></span>&nbsp;&nbsp;La operaci�n se realiz� satisfactoriamente.
					</p>
				</div>
			</div>
		</div>			
		<%} %>
		
		<%if(result!=null && result.equals("2")) {%>
		<div id="dialog-message-alert" title="Biblioteca Tecsup">
			<div class="ui-widget" style="font-size: 11px;">
				<div class="ui-state-highlight ui-corner-all">
					<p style="margin-top: 0.7em; margin-bottom: 0.7em; margin-left: 0em;">
						<span class="ui-icon ui-icon-alert"
							style="float: left; margin-left: 1.5em"></span>&nbsp;&nbsp;La operaci�n se realiz� satisfactoriamente. Se actualiz� una sanci�n al usuario.
					</p>
				</div>
			</div>
		</div>
		<%} %>
		
		<%if(result!=null && result.equals("-1")) {%>
		<div id="dialog-alert-error" title="Biblioteca Tecsup">
			<div class="ui-widget" style="font-size: 11px;">
				<div class="ui-state-error ui-corner-all">
					<p style="margin-top: 0.7em; margin-bottom: 0.7em; margin-left: 0em;">
						<span class="ui-icon ui-icon-alert"
							style="float: left; margin-left: 1.5em"></span>&nbsp;&nbsp; Ocurri� un problema al procesar la solicitud.
					</p>
				</div>
			</div>
		</div>		
		<%} %>
		
	</div>
		
		
</form:form>

		
</body>
