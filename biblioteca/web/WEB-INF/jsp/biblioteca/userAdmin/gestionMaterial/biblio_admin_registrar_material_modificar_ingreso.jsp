<%@ include file="/taglibs.jsp"%>

<head>

	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<script language=javascript>
function onLoad(){}

function fc_guardar(){
		document.getElementById("operacion").value="irRegistrar";
		document.getElementById("frmMain").submit();
}
function validaTexto(id){	
	fc_ValidaTextoEspecial();
	if(document.getElementById(""+id).value.length > 400)
	{
		str = document.getElementById(""+id).value.substring(0,400);
		document.getElementById(""+id).value=str;
		return false;
	}	
}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_material_modificar_ingreso.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="txhCodigoUnico" id="txhCodigoUnico" />
	

<table cellpadding="0" cellspacing="0" ID="Table2">
			<tr class="texto" >
				<td class=>Registro de Ingresos</td>
			</tr>
		</table>
<table bgcolor="" width="1020px" border="0" cellspacing="0" class="" height="70px" bordercolor="red" ID="Table1">
			<tr>
				<td class="">Nro. Ingreso:</td>
				<td>				
				<form:input path="txtIngreso" id="txtIngreso" cssClass="cajatexto_1" />
				</td>
				<td class="">Fec. Ingreso :</td>
				<td colspan="2">				
				<form:input path="txtFecIngreso" id="txtFecIngreso" cssClass="cajatexto_o" size="12" />				
				&nbsp;<img src="${ctx}\images\iconos\calendario_on.gif" id="imgCalendar" align=absmiddle style="cursor:hand"></td>
				<td class="">Fec. Baja :</td>
				<td colspan="2">				
				<form:input path="txtFecBaja" id="txtFecBaja" cssClass="cajatexto_o" size="12" />				
				&nbsp;<img src="${ctx}\images\iconos\calendario_on.gif" id="imgCalendar" align=absmiddle style="cursor:hand"></td>
			</tr>
			<tr>
				<td class="">Procedencia :</td>
				<td>					
				<form:select path="cboProcedencia"
				id="cboProcedencia" cssClass="cajatexto_o" cssStyle="width:120px">
				<form:option value="-1">--Seleccione--</form:option>
				<c:if test="${control.lstProcedencia!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="descripcion" items="${control.lstProcedencia}" />
				</c:if>
			</form:select>						
					</td>
				<td class="">Precio :
				</td>
				<td>
				<form:select path="cboMoneda"
				id="cboMoneda" cssClass="cajatexto_o" cssStyle="width:50px">
				<form:option value="-1">--Seleccione--</form:option>
				<c:if test="${control.lstMoneda!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="dscValor3" items="${control.lstMoneda}" />
				</c:if>
			</form:select>												
					&nbsp;					
						<form:input path="txtPrecio" id="txtPrecio" cssClass="cajatexto_o" />
				</td>
				<td class="">Estado :</td>
				<td class="">
				<form:select path="cboEstado"
				id="cboEstado" cssClass="combo_o" cssStyle="width:80px">
				<form:option value="-1">--Seleccione--</form:option>
				<form:option value="0001">Activo</form:option>
				<form:option value="0002">Inactivo</form:option>
				<c:if test="${control.lstEstado!=null}">
					<form:options itemValue="codTipoTablaDetalle"
						itemLabel="descripcion" items="${control.lstEstado}" />
				</c:if>
			</form:select>					
					</td>
				<tr>
				<td class="">Observación :</td>
				<td colspan="4">				
				<form:textarea path="txtObservacion" id="txtObservacion" cols="40" rows="3" onkeypress="validaTexto('txtObservacion')"  />
				</td>
			</tr>
		</table>
		<br>
		<table align=center>
			<tr>
				<td><input type=button value="Grabar" class="boton" ID="Button1" NAME="Button1" onclick="fc_guardar();">&nbsp;
				<input type=button value="Cancelar" class="boton" ID="Button2" NAME="Button2" onclick="window.close();"></td>
			</tr>
		</table>	
	
</form:form>
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "txtFecIngreso",
	ifFormat       :    "%d/%m/%Y",
	daFormat       :    "%d/%m/%Y",
	button         :    "imgCalendar",
	singleClick    :    true

	});
                          
</script>
</body>
