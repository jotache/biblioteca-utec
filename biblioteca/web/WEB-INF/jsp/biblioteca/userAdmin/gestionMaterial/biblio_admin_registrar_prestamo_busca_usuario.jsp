<%@ include file="/taglibs.jsp"%>
<head>
<script language=javascript>
function onLoad(){
}

function fc_aceptar(){

	if(fc_Trim(document.getElementById("nombre").value)!=""
		&& fc_Trim(document.getElementById("codigo").value)!=""	)
	{			
		window.opener.document.getElementById("txtUsuariologin").value = document.getElementById("codigo").value;
		window.opener.document.getElementById("txtUsuarioNombre").value = document.getElementById("nombre").value;		
		window.opener.document.getElementById("txhflgBusqXCodOracle").value = "1"; 
		window.opener.fc_VerificarCodOraAlumno();		
		window.close();
	}else
		alert("Seleccione Usuario");	
}

function fc_selecciona(cod,nom){	
	document.getElementById("codigo").value = cod;
	document.getElementById("nombre").value = nom;
	//jhpr 2008-08-15	
}

function cierrame(){
	window.opener.document.getElementById("txtUsuariologin").focus();
	window.close();
}

</script>
</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_prestamo_busca_usuario.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="txhCodUsuario" id="txhCodUsuario" />
	<form:hidden path="txhTipo" id="txhTipo" />
	
	<!-- VARIABLES DE SELECCION -->
	<input type="hidden" name="codigo" id="codigo" />	
	<input type="hidden" name="nombre" id="nombre" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="220px" class="opc_combo"><font style="">Seleccione el Usuario</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	<table><tr height="3px"><td></td></tr></table>	
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" ID="Table1" width="99%" border="0"  class="" style="margin-left:3px">
		<tr>
			 <td align="left">
			 	<div style="overflow: auto; height: 250px;width:100%">
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:97%;">
					<tr height="30px">
						<td class="grilla" width="5%"  >Sel.</td>
						<td class="grilla" width="15%">C�digo</td>
						<td class="grilla">Nombre</td>
					</tr>
					
				<c:forEach var="objCast" items="${control.lstResultado}" varStatus="loop">
					<tr class="tablagrilla">
						<td align="center">
						<input type="hidden" name="hid<c:out value="${loop.index}" />" id="hid<c:out value="${loop.index}" />" value="<c:out value="${objCast.nomUsuario}" />" />
						<input type="radio" ID="Radio3" NAME="Radio" onclick="fc_selecciona('<c:out value="${objCast.codTipoUsuario}" />','<c:out value="${objCast.nomUsuario}" />')" ></td>
						<td align="center">&nbsp;<c:out value="${objCast.codTipoUsuario}" /></td>
						<td><c:out value="${objCast.nomUsuario}" /></td>
					</tr>
				</c:forEach>		
														
				</table>
				</div>
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=right  border="0" bordercolor="red">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/aceptar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/aceptar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_aceptar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="cierrame();" ID="Button1" NAME="Button1" style="cursor:pointer;"></a></td>
		</tr>
	</table>		
	
</form:form>
</body>
