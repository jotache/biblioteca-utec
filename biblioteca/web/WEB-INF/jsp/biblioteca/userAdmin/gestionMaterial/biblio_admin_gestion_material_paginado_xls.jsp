<%
	response.setContentType("application/vnd.ms-excel;charset=ISO-8859-1"); //Para no tener problemas con �s y tildes
	response.setHeader( "Content-Disposition", "inline;filename=\"Reporte_Materiales.xls\"" );
	response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server
	
%>
<%@ include file="/taglibs.jsp"%>   
<body>

	
			<!--PAGINADO-->
				
			<display:table name="sessionScope.SS_RESULTADO_MATERIAL_FILTRO" 
				cellpadding="0" cellspacing="1" 
				style="border: 1px solid #048BBA;width:100%;" requestURI="" >	
	 					
				<display:column property="codGenerado" 		title="Codigo" headerClass="grilla"  class="tablagrilla"  style="text-align:left;width:10%"/>
				<display:column property="titulo" 			title="Titulo" headerClass="grilla"  class="tablagrilla" style="width:30%;text-align: left"/>
				<display:column property="descAutor" 		title="Autor" headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align: left"/>
				<display:column property="nomEditorial"		title="Editorial" headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align: left"/>
				<display:column property="fechaPublicacion" title="A�o" headerClass="grilla"  class="tablagrilla"  style="width:6%;text-align: center"/>
				<display:column property="descTipoMat" 		title="Tipo Material" headerClass="grilla"  class="tablagrilla"  style="text-align: left;width:15%"/>
				<display:column property="volumenes" 		title="Total. Ejemp." headerClass="grilla"  class="tablagrilla"  style="width:5%;text-align: center"/>
				<display:column property="nomSede" 			title="Sede" headerClass="grilla"  class="tablagrilla"  style="text-align:center;width:8%"/>
				
				<display:column property="prestSala" title="Prest. Sala." headerClass="grilla"  class="tablagrilla"  style="width:5%;text-align: center"/>
				<display:column property="prestDomicilio" title="Prest. Domic." headerClass="grilla"  class="tablagrilla"   style="width:15%;text-align: center"/>
				<display:column property="reserva" title="Reserv." headerClass="grilla"  class="tablagrilla"  style="width:15%;text-align: center"/>
				
				
				<display:setProperty name="basic.empty.showtable" value="true"  />
				<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='14' align='center'>No se encontraron registros</td></tr>"  />
				<display:setProperty name="paging.banner.placement" value="bottom"/>
				<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
				<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
				<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
				<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
				<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
				<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
				<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
				<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
			</display:table>
			<!--/PAGINADO-->
			
</body>
				