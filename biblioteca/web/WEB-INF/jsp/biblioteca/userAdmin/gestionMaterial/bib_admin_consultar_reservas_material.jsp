<%@page import='java.util.List'%>
<%@page import='com.tecsup.SGA.bean.ReservasByMaterial'%>
<%@ include file="/taglibs.jsp"%>

<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<script type="text/javascript">		
		function onLoad(){
			document.getElementById("txtTitulo").readOnly=true;
			var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
			if(mensaje!="")
				alert(mensaje);
		}
		
		function fc_Eliminar(){
		
		var tam = document.getElementById("txhTamanio").value;
		
		 if(Number(tam)>0){
			fc_Seleccion();
			
			if(document.getElementById('txhCadena').value == ''){
				alert(mstrSeleccione);
				return false;
			}
			else{
				document.getElementById("txhOperacion").value = "ELIMINAR";
				document.getElementById("frmMain").submit();
			}
		 }else{
			alert(mstrNoSePuedeRealizarLaAccion);
		 }	
		}
		
		function fc_Seleccion(){
			var tbl = document.getElementById('tblUsuarios');
			
			for(i=0 ; i < tbl.rows.length - 1 ; i++){
				objSel = document.getElementById('chk_'+i);
				if(objSel.checked){
					document.getElementById('txhCadena').value = 
							document.getElementById('txhCadena').value + 
							document.getElementById('txhCodReserva'+i).value + "|";
				}
			}
		}
</script>
</head>
<body>

<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_admin_consultar_reservas_material.html" 
	method="post">
	
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="cadena" id="txhCadena" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px; margin-top:3px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="180px" class="opc_combo"><font style="">Consultar Reservas</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>	
	<table><tr height="3px"><td></td></tr></table>	
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:94%; margin-left:3px" border="0" cellspacing="4" 
		cellpadding="1" class="tabla" height="50px" bordercolor="red">
		<tr>
			<td>&nbsp;C&oacute;digo :</td>
			<td>&nbsp;<form:input path="codigo" id="txtCodigo" 
				 readonly="true" cssClass="cajatexto_1" size="15" />
			</td>
			<td>&nbsp;Tipo Material :</td>
			<td>&nbsp;<form:input path="tipoMaterial" id="txtTipoMaterial" 
				 readonly="true" cssClass="cajatexto_1" size="35" />
			</td>			
		</tr>
		<tr>
			<td>&nbsp;T&iacute;tulo :</td>
			<td colspan="3">&nbsp;<form:textarea path="titulo" id="txtTitulo" cssClass="cajatexto_1" rows="2" cols="100"/></td>
		</tr>
	</table>
	<c:set var="tam" value="0" />
	
	<table cellpadding="0" cellspacing="0" id="Table1" width="99%" border="0" bordercolor="red" 
		style="margin-left:3px;margin-top:3px">
		<tr>
			 <td align="left" width="95%">
				<div style="WIDTH: 100%; HEIGHT: 130px; display: ;border: 0px solid #048BBA;">
					<table border="0" bordercolor="blue" style="WIDTH: 96.5%; border: 1px solid #048BBA;" id="tblUsuarios">
						<tr rowspan="2">
							<td class="grilla" width="5%" nowrap="nowrap">Sel.</td>
							<td class="grilla" width="49%" nowrap="nowrap">Usuario</td>
							<td class="grilla" width="22%" nowrap="nowrap">Tipo Usuario</td>
							<td class="grilla" width="12%" nowrap="nowrap">Fecha<br>Reserva</td>
							<td class="grilla" width="11%" nowrap="nowrap" style="display:none;">Hora<br>Reserva</td>
						</tr>
					<%  
					if(request.getSession().getAttribute("listaBandeja")!=null){
						
						List consulta = (List) request.getSession().getAttribute("listaBandeja");
	
						if (consulta.size() > 0) {
							for(int i=0;i<consulta.size();i++){
								
								ReservasByMaterial obj  = (ReservasByMaterial) consulta.get(i);
								%>
								<c:set var="tam" value="${tam+1}" />
								<tr class="tablagrilla" style="vertical-align: middle;">
									<input type="hidden" id="txhCodReserva<%=i%>" name="txhCodReserva<%=i%>" value="<%=obj.getCodReserva()%>" />
									<td style="text-align:center;"><%if (obj.getFlgEliminar().equals("1")){%><input type="checkbox" id="chk_<%=i%>" name="chk_<%=i%>" /><%}else{%><input type="checkbox" id="chk_<%=i%>" name="chk_<%=i%>" disabled="disabled"/><%}%></td>
									<td style="text-align:left;"><%=obj.getUsuario()==null?"":obj.getUsuario()%></td>
									<td style="text-align:left;"><%=obj.getTipoUsuario()==null?"":obj.getTipoUsuario()%></td>
									<td style="text-align:center;"><%=obj.getFechaReserva()==null?"":obj.getFechaReserva()%></td>
									<td style="text-align:center; display:none;"><%=obj.getHoraReserva()==null?"":obj.getHoraReserva()%></td>
								</tr>
								<%
							}
						}
						else{
							%>
								<tr class="tablagrilla" style="vertical-align: middle;">
									<td style="text-align:center;" colspan="5">No se encontraron registros</td>
								</tr>
							<%
						}
					}
					request.removeAttribute("listaBandeja");
					%>
					</table>
				</div>
			</td>
			<td align="center" valign="middle">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminar','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" id="imgEliminar" name="imgEliminar" onclick="javascript:fc_Eliminar();"></a>
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align=right style="width:10%;margin-right:20px">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/cerrar2.jpg',1)">
				<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:window.close();"></a></td>
		</tr>
	</table>
	
<input type="hidden" name="txhTamanio" id="txhTamanio" value="<c:out value="${tam}" />" />	
</form:form>
</body>