<%@page import="com.tecsup.SGA.bean.UsuarioSeguridad"%>
<%@ include file="/taglibs.jsp"%>
<head>	

	<%
		UsuarioSeguridad userLogin = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
		String opciones = "";
		String tipoUser = "";
		if(userLogin!=null){
			opciones = userLogin.getOpcionesApoyo();
			tipoUser = userLogin.getTipoUsuario();
		}
	
	%>
		
	<script type="text/javascript">
	function onLoad(){
			
	}	
	function fc_openAgregarDescriptor(){
		
		url = "${ctx}/biblioteca/biblio_admin_registrar_material_agregar_descriptores.html?"+
		"txhCodUnico="+document.getElementById("txhCodigoUnico").value+
		"&txhCodUsuario="+parent.document.getElementById("usuario").value;
			
		Fc_Popup(url,600,300);	
	}
	
	function fc_Eliminar(){
	
	if(fc_Trim(document.getElementById("txhCodigoDescriptor").value)!=""){
		
		if(confirm(mstrSeguroEliminar))
		{
			document.getElementById("operacion").value="irEliminar";
			document.getElementById("usuario").value=parent.document.getElementById("usuario").value;			
			document.getElementById("frmMain").submit();
		}
		
		}else
			alert(mstrSeleccione);
	}	
	
	function Fc_Upload(){
		url = "${ctx}/biblioteca/biblio_admin_registrar_material_adjuntar_documentos.html?"+
		"txhCodUnico="+document.getElementById("txhCodigoUnico").value;		
		Fc_Popup(url,430,180);
	}
	
	function fc_selecciona(codigoUnico){
		document.getElementById("txhCodigoDescriptor").value = codigoUnico;  
	}
	
	function fc_submit(){
		document.getElementById("frmMain").submit();
	}
	
	</script>

</head>

<body topmargin="5" leftmargin="10" rightmargin="10" bgcolor="red">
<form:form  id="frmMain" action="${ctx}/biblioteca/biblio_admin_registrar_material_iframe_descriptores.html"  method="post">

<input type="hidden" name="txhCodigoUnico" id="txhCodigoUnico" value="${model.txhCodigoUnico}" />
<input type="hidden" name="operacion" id="operacion" value="irConsultar" />
<input type="hidden" name="usuario" id="usuario" />

<!-- VARIABLE QUE ALMACENA EL ITEM SELECCIONADO -->
<input type="hidden" name="txhCodigoDescriptor" id="txhCodigoDescriptor" />

	<table cellpadding="0" cellspacing="0" id="Table1" width="100%" border="0" class="">
		<tr>			
			<td valign="top">
				<div style="overflow: auto; height: 200px; width:100%;">
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:98%;">
					<tr class="grilla">
						<td width="4%">Sel.</td>
						<td width="10%">Fec. Reg.</td>
						<td width="61%">Descriptor</td>
						<td width="25%">Usuario</td>
					</tr>
				<!-- BIBLIOTECA -->
					<c:forEach var="objCast2" items="${model.lstDescriptores}" varStatus="loop2" >						
						<tr class="tablagrilla">
							<td align="center">
								<input type=radio  NAME="RadioDescriptor" onclick="fc_selecciona('<c:out value="${objCast2.codUnico}" />')" >
							</td>
							<td align="center"><c:out value="${objCast2.fecRegistro}" /></td>						
							<td align="left"><c:out value="${objCast2.desDescriptor}" /></td>
							<td align="left"><c:out value="${objCast2.usuario}" /></td>							
						</tr>
					</c:forEach>
				<!-- /BIBLIOTECA -->			
				</table>
				</div>
			</td>
			<td width="25px" valign="top" align="right">
				<br/>
				
				<%if(opciones.indexOf("INS_DES|")>=0 || tipoUser.equals("A")){ %>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregarPerfil','','${ctx}/images/iconos/agregar2.jpg',1)">
					<img src= "${ctx}/images/iconos/agregar1.jpg" onclick="javascript:fc_openAgregarDescriptor();" id="imgAgregarPerfil" style="cursor:pointer" alt="Agregar">
				</a>				
				<%} %>
				
				<br/>
				
				<%if(opciones.indexOf("DEL_DES|")>=0 || tipoUser.equals("A")){ %>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEliminarPerfil','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img  src="${ctx}/images/iconos/quitar1.jpg" alt="Eliminar" style="cursor:pointer" 
						id="imgEliminarPerfil" onclick="javascript:fc_Eliminar();">
				</a>
				<%} %>
				
			</td>
		</tr>		
	</table>	
</form:form>	
</body>




