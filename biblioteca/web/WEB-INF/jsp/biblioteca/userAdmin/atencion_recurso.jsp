<%@ include file="/taglibs.jsp"%>
<%	if ( request.getSession().getAttribute("usuarioSeguridad") == null ) { %>
	<script language="javascript">	
		alert("Su session ha expirado debe volver a logearse");		
		window.close();			
	</script>
<%}%>
<html>
<head>
<title>Atenci�n Recurso</title>
<script type="text/javascript">
	function onLoad(){
		//cambia('1');
	}
	function finalizar(id){
		if(confirm('�Confirma que desea finalizar el uso de este Recurso?')){			
			//alert('Finalizar:'+id);
			document.frmAtencionRecurso.txhIdAtencion.value=id;
			document.frmAtencionRecurso.txhAccion.value='finalizar';
			document.frmAtencionRecurso.submit();
		}
	}
	function cambia(pestania){
		
		actual_activo = "url(/biblioteca/images/biblioteca/atenciones.actuales.png)";
		actual_opaco = "url(/biblioteca/images/biblioteca/atenciones.actuales.opaco.png)";
		historico_activo = "url(/biblioteca/images/biblioteca/atenciones.historicas.png)";
		historico_opaco =  "url(/biblioteca/images/biblioteca/atenciones.historicas.opaco.png)";
		if (pestania=='1'){
			document.getElementById('tdactual').style.backgroundImage = actual_activo;
			document.getElementById('tdhistorico').style.backgroundImage = historico_opaco;
			divactual.style.display="inline-block";
			divhistorico.style.display="none";
		}else{
			document.getElementById('tdactual').style.backgroundImage = actual_opaco;
			document.getElementById('tdhistorico').style.backgroundImage = historico_activo;
			divactual.style.display="none";
			divhistorico.style.display="inline-block";
			if(document.frmAtencionRecurso.txhPestania.value=='1'){
				document.frmAtencionRecurso.txhAccion.value='historico';
				document.frmAtencionRecurso.submit();
			}
		}
	}
</script>		
</head>
<body >
<form:form id="frmAtencionRecurso" name="frmAtencionRecurso" commandName="control" action="${ctx}/biblioteca/bib_atencion_recurso.html">
<form:hidden path="accion" id="txhAccion" />
<form:hidden path="sede" id="txhSede" />
<form:hidden path="nombrePC" id="nombrePC" />
<form:hidden path="codPc" id="codPc" />
<form:hidden path="fechaAtencion" id="fechaAtencion" />
<form:hidden path="idAtencion" id="txhIdAtencion" />
<form:hidden path="tipoMensaje" id="txhTipoMensaje" />
<form:hidden path="mensaje" id="txhMensaje" />
<form:hidden path="pestania" id="txhPestania" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="BORDER-RIGHT: #007ACA 1px solid;BORDER-TOP: #007ACA 1px solid;BORDER-BOTTOM: #007ACA 1px solid;BORDER-LEFT: #007ACA 1px solid;left: 1">
  <tr height="20px">	
    <td colspan="3" class=fondoceleste>
		<p valign=middle class=textoblanco><b>&nbsp;Atenciones registradas hasta el momento</b></p>
	</td>
  </tr>
  <tr>
    <td class="Detalle" width="25%" height="30px" valign="middle">
		&nbsp;Fecha:&nbsp;
		<form:input path="fechaAtencion" id="txtFecha" maxlength="10" cssStyle="width:70px" disabled="true" cssClass="cajatexto_o"/>
	</td>
    <td class="Detalle" width="25%">Recurso:
		&nbsp;<form:input path="nombrePC" id="txtPC" cssStyle="width:40px" disabled="true" cssClass="cajatexto_o"/>
	</td>
    <td width="50%">&nbsp;  </td>
  </tr>

</table>

<table cellpadding="0" cellspacing="0" style="margin-top: 4px; margin-left: 8px" border="0">
	<tr height="25px">
		<td id="tdactual" style="cursor: hand" width="142px" height="25px" onclick="cambia('1');" >
		</td>
		<td id="tdhistorico" style="cursor: hand" width="142px" height="25px" onclick="cambia('2');" >
		</td>
	</tr>
</table>

<div style="overflow: auto; height:280px;width: 510" id="divactual">
<table border="0" width="480px" align="left" cellspacing="0" style="margin-top:0px; margin-left: 9px">	
	<tr height="20px">
		<td class="headtabla2" width="220">&nbsp;Usuario</td>
		<td class="headtabla2" width="70" align="center">&nbsp;Hr. Ini</td>
		<td class="headtabla2" width="70" align="center">&nbsp;Hr. Fin</td>
		<td class="headtabla2" width="120">&nbsp;Estado</td>
	</tr>
	<c:forEach varStatus="loop" var="lista" items="${control.listaAtencionesPorPc}"  >
		<c:choose>
			<c:when test="${loop.count % 2 == 0}">
				<tr class="fondoceldablanco">
			</c:when>
			<c:otherwise>
				<tr class="fondocelesteclaro">
			</c:otherwise>
		</c:choose>	
			<td align="left">&nbsp;<c:out value="${lista.nombreUsuario}" />
			</td>
			<td align="center"><c:out value="${lista.horaIni}" />
			</td>
			<td align="center"><c:out value="${lista.horaFin}" />
			</td>
			<td align="left">
				&nbsp; 
				<c:choose>
					<c:when test="${lista.codEstado == 'X'}"> <img src="${ctx}/images/status_progress.gif" width="15" height="15" border="0" onclick="javascript:finalizar('<c:out value="${lista.codReserva}" />');" style="cursor: hand" alt="En Uso" align="middle">	</c:when>
					<c:when test="${lista.codEstado == 'C'}"><img src="${ctx}/images/deleted.gif" width="12" height="12" border="0" alt="Cancelado" align="middle"></c:when>
					<c:when test="${lista.codEstado == 'T'}"><img src="${ctx}/images/end-time-dot.gif" width="16" height="16" border="0" alt="Finalizado" align="middle"></c:when>
				</c:choose>
				&nbsp;<c:out value="${lista.estado}" />
			</td>		
			
		</tr>
	</c:forEach>
</table>
</div>

<div style="overflow: auto; height:280px;width: 510;" id="divhistorico">
	<table border="0" width="480px" align="left" cellspacing="0" style="margin-top:0px; margin-left: 9px">		
		<tr height="20px">
			<td class="headtabla2" width="60">&nbsp;Fecha</td>
			<td class="headtabla2" width="220">&nbsp;Usuario</td>
			<td class="headtabla2" width="70" align="center">&nbsp;Hr. Ini</td>
			<td class="headtabla2" width="70" align="center">&nbsp;Hr. Fin</td>
			<td class="headtabla2" width="60">&nbsp;Estado</td>
		</tr>

		<c:forEach varStatus="loop2" var="lista2" items="${control.listaAtencionesPorPcHistorico}"  >
			<c:choose>
				<c:when test="${loop2.count % 2 == 0}">
					<tr class="fondoceldablanco">
				</c:when>
				<c:otherwise>
					<tr class="fondocelesteclaro">
				</c:otherwise>
			</c:choose>	
			<td align="left">&nbsp;<c:out value="${lista2.fechaReserva}" />
			</td>
			<td align="left">				
				&nbsp;<c:out value="${lista2.nombreUsuario}" />
			</td>
			<td align="center"><c:out value="${lista2.horaIni}" />
			</td>
			<td align="center"><c:out value="${lista2.horaFin}" />
			</td>
			<td align="left"><c:out value="${lista2.estado}" />
			</td>	
			
		</tr>
		</c:forEach>

	</table>
</div>

</form:form>
<script type="text/javascript">
	if (document.getElementById('txhTipoMensaje').value != ''){
		strMsg = document.getElementById('txhMensaje').value;
		cancel = 'Operaci�n Cancelada:\n';
		switch(document.getElementById('txhTipoMensaje').value){
			case 'ERROR':
				if ( strMsg != ""){
					alert(cancel + strMsg);
				}
				break;
			case 'OK':
				if ( strMsg != ''){
					alert(strMsg);
					window.opener.consultar();		
				}
				break;			
		}
	}
	document.frmAtencionRecurso.txhAccion.value='';
	if(document.frmAtencionRecurso.txhPestania.value=='1')
		cambia('1');
	else
		cambia('2');
	
</script>
</body>
</html>