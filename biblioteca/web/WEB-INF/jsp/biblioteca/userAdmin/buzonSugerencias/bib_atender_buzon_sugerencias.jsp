<%@ include file="/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	function onLoad(){
	document.getElementById("txhOperacion").value="";
	if(document.getElementById("txhMsg").value=="OK")
	 {  
	    document.getElementById("txhMsg").value="";
	    window.opener.document.getElementById("frmMain").submit();
		window.close();
		
	 }		
	 	document.getElementById("txhMsg").value="";	
	}  
	
	function imposeMaxLength(Object, MaxLen){
		return (Object.value.length <= MaxLen);
	}
	
	function fc_CalificacionRespuesta(){
	
	}
	
	function fc_TipoMaterial(){
	
	}
	function fc_Enviar(){
	
		if(document.getElementById("calificacionRespuesta").value=="-1" ){
			document.getElementById("calificacionRespuesta").focus();
	 		alert('Seleccione una Calificaci�n.');
			return false;
		}
		
		if(document.getElementById("textoRespuesta").value==""){
			document.getElementById("textoRespuesta").focus();
	  	  	alert('Debe ingresar una respuesta.');
			return false;
		}
			
		if(confirm('�Seguro de Atender este registro?')){
			document.getElementById("txhOperacion").value = "ENVIAR";
	  		document.getElementById("frmMain").submit();	  		
		}
	  	
	  	/*
	    var bandera="0";
	    if(      (document.getElementById("txhCodSelTipoSugerencia").value==document.getElementById("txhConsteBuzonConsulta").value)
	          || (document.getElementById("txhCodSelTipoSugerencia").value==document.getElementById("txhConsteBuzonReclamo").value)) 
	  	   { 
	  	     bandera="1"; //Es tipo "Consulta" o "Reclamo".
	  	   }
	  
	  	 if(bandera=="0"){
	  	    
	     	document.getElementById("txhOperacion").value = "ENVIAR";
		  	//document.getElementById("frmMain").submit();
		  	alert("ENVIAR 1");
		  
		  }else{	 
			 document.getElementById("txhOperacion").value = "ENVIAR";
		  	 //document.getElementById("frmMain").submit();
			 alert("ENVIAR 2");
		  }
	  	 */
	  	 	 	 	
	}	
	
</script>	
</head>	
<body topmargin="10" leftmargin="10" rightmargin="10">
<form:form action="${ctx}/biblioteca/bib_atender_buzon_sugerencias.html" commandName="control" id="frmMain">	
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codSelec" id="txhCodSelec"/>
<form:hidden path="comentario" id="txhComentario"/>
<form:hidden path="consteBuzonConsulta" id="txhConsteBuzonConsulta"/>
<form:hidden path="consteBuzonComentario" id="txhConsteBuzonComentario"/>
<form:hidden path="consteBuzonSugerencia" id="txhConsteBuzonSugerencia"/>
<form:hidden path="consteBuzonReclamo" id="txhConsteBuzonReclamo"/>
<form:hidden path="codSelTipoSugerencia" id="txhCodSelTipoSugerencia"/>
<!-- 
<form:hidden path="email" id="txhEmail"/>
 -->

	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:8px; margin-top:5px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo"><font style="">Atender Buz�n de Sugerencias</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>	
	<table border="0" cellspacing="2" cellpadding="3" class="tabla"
		bordercolor="red" style="width:99%;height:35px;">
		<tr>
			<td>
				<table background="${ctx}\images\biblioteca\fondosup.jpg" style="width:100%;height:35px;margin-left:3px;" align="center" border="0" bordercolor="red" cellspacing="4" cellpadding="1">
					<tr>	
						<td width="20%">&nbsp;Fec. Reg. :</td>
						<td width="20%">&nbsp;
								<form:input path="fechaRegistro" id="fechaRegistro" maxlength="50"
										readonly="true"
										cssClass="cajatexto_1" size="12" />
						</td>
						<td width="30%" align="right">&nbsp;Tipo de Sugerencia :</td>
						<td width="30%" align="right">&nbsp;
								<form:input path="tipoSugerencia" id="tipoSugerencia" maxlength="50"
										readonly="true"
										cssClass="cajatexto_1" size="22" />
						</td>
					</tr>
					<tr> 
						<td colspan="4">
						  <textarea class="cajatexto_1" readonly="readonly" style="width: 99%;" rows="3"><c:out value="${control.comentario}"/></textarea>
						</td>
					<tr>
						<td>&nbsp;Usuario :</td>
						<td >&nbsp;
								<form:input path="usuario" id="usuario" maxlength="0"
										readonly="true"
										cssClass="cajatexto_1" size="50"/>
						</td>
						<!--JHPR 2008-7-31-->
						<td align="right">&nbsp;Correo :</td>
						<td>
						&nbsp;<form:input path="email" id="email" maxlength="0"
										readonly="true"
										cssClass="cajatexto_1" size="45"/>						
						</td>
					</tr>
				</table>
				<table style="width:100%;">
					<tr>
						<td colspan="4">
							<table><tr><td height="2px"></td></tr></table>
							<img src="${ctx}\images\separador_n.gif" width="102%">
							<table><tr><td height="1px"></td></tr></table>
						</td>
					</tr>
				</table>
				<table background="${ctx}\images\biblioteca\fondoinf.jpg" style="width:100%;height:35px;margin-left:3px;margin-top:1px;" align="center" border="0" bordercolor="red" cellspacing="4" cellpadding="1">
					<tr>
						<td>&nbsp;Calificaci�n :</td>
						<td>&nbsp;
								<form:select path="calificacionRespuesta" id="calificacionRespuesta" cssClass="cajatexto_o"	cssStyle="width:140px"  onchange="javascript:fc_CalificacionRespuesta();">
										<form:option value="-1">--Seleccione--</form:option>
											<c:if test="${control.codListaCalificacion!=null}">
												<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
													items="${control.codListaCalificacion}" />
										</c:if>
								</form:select>
						</td>
						<td align="right">&nbsp;Orientado a :</td>
						<td align="right">&nbsp;
								<form:select path="tipoMaterial" id="tipoMaterial" cssClass="cajatexto" cssStyle="width:140px"  onchange="javascript:fc_TipoMaterial();">
									<form:option value="-1">--Seleccione--</form:option>
										<c:if test="${control.codListaTipoMaterial!=null}">
											<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
												items="${control.codListaTipoMaterial}" />
										</c:if>
								</form:select>
						</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;Respuesta :</td>
					</tr>
					<tr>
				 		<td colspan="5">
				 			<form:textarea path="textoRespuesta" id="textoRespuesta" 
								 cssClass="cajatexto_o" cssStyle="width: 99%;" rows="4" onkeypress="return imposeMaxLength(this, 1000);"/>
							<table><tr><td height="2px"></td></tr></table>
						</td>
					</tr>
					<tr>
						<td colspan="5" valign="top">
							(M�ximo 1000 caracteres)
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<table cellspacing="0" cellpadding="0" border=0 align="right" style="margin-top: 15px;margin-right:5px">
		<tr>
			<td align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgPrest','','${ctx}/images/biblioteca/enviar2.jpg',1)">
						<img src="${ctx}/images/biblioteca/enviar1.jpg" align="absmiddle" alt="Enviar" id="imgPrest" style="CURSOR: pointer" onclick="javascript:fc_Enviar();"></a>
						
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDevol','','${ctx}/images/botones/cancelar2.jpg',1)">
						<img src="${ctx}/images/botones/cancelar1.jpg" align="absmiddle" alt="Cancelar" id="imgDevol" style="CURSOR: pointer" onclick="window.close();"></a>		
		</td>
		</tr>
	</table>	
</form:form>	
</body>
</html>