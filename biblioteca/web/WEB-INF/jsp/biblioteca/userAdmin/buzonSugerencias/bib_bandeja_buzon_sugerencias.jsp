<%@ include file="/taglibs.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var fecha=new Date();
	
	var miFecha=""+fecha.getFullYear()+fecha.getMonth()+fecha.getDate()+fecha.getHours()+fecha.getMinutes()+fecha.getSeconds();
	var miMes=fecha.getMonth();
	var miAnio=fecha.getFullYear();
	var Mes=parseInt(miMes)+1;
	
	function onLoad(){	    
	 	$('#txhCodSelec').val('');
		if($('#txhOperacion').val('ATENDER')){
			$('#codEstado').val('-1');
			//Fc_Buscar();
		}
		if($('#txhOperacion').val('')){
			//$('#codMeses').val(''+Mes);
		 	//$('#codAnios').val(''+miAnio);
		}		
	}
	function fc_Regresar(){
		location.href =  "${ctx}/menuBiblioteca.html";
	}
	//*****************************************************************
	function Fc_Actualizar(){		
		$('#codMeses').val('-1');
		$('#codAnios').val(''+$('#txhPeriodoActual').val());
	}
	function Fc_Limpiar(){		
		$("#codMeses").val("-1");
		$("#codAnios").val(''+$("txhPeriodoActual").val());
		$("#codTipoSugerencia").val("-1");
		$("#codEstado").val("-1");
		$("#txhCodSelec").val("");
		$("#txhOperacion").val("LIMPIAR");
	    $('#frmMain').submit();		
	}
	function Fc_Buscar(){
		var mes=document.getElementById("codMeses");
		var anio=document.getElementById("codAnios");
		var tipoSugerencia=document.getElementById("codTipoSugerencia");
		var estado=document.getElementById("codEstado");
		$("#txhMes").val(''+$("#codMeses").val());
		$("#txhAnio").val(''+$("#codAnios").val());
		
		if( (anio.value != "-1") && (mes.value != "-1")){
		   $('#txhOperacion').val('BUSCAR');
	       $('#frmMain').submit();
	    }else{ 
		    if(mes.value=="-1") alert('Seleccione un Mes.');
			  else if(anio.value == "-1") alert('Seleccione un A�o.');	
		}
	}
	//*****************************************************************	
	function Fc_Atender(){	
	   if(document.getElementById("txhCodSelec").value!=""){
	      srtCodSugerencia=document.getElementById("txhCodSelec").value;	
		  srtFechaCrea=document.getElementById("txhFechaRegistro").value;
		  srtDescripTipoSugerencia=document.getElementById("txhTipoSugerencia").value;
		  srtDescripcion=document.getElementById("txhComentario").value;
		  srtDescripCalificacion=document.getElementById("txhCalRespuesta").value;
		  srtDescripCodMaterial=document.getElementById("txhTipoMaterial").value;
		  srtDescripcionUsuario=document.getElementById("txhUsuario").value;
		  		  
	      document.getElementById("txhOperacion").value = "ATENDER";
	      Fc_Popup("${ctx}/biblioteca/bib_atender_buzon_sugerencias.html?txhCodSelec="+srtCodSugerencia+
	      "&txhFechaRegistro="+srtFechaCrea+ "&txhTipoSugerencia="+srtDescripTipoSugerencia+
	      "&txhComentario="+srtDescripcion+"&txhCalRespuesta="+srtDescripCalificacion+
	      "&txhTipoMaterial="+srtDescripCodMaterial+ "&txhUsuario="+srtDescripcionUsuario +
	      "&txhEmail="+document.getElementById("txhEmail").value ,700,360);
	   }
	   else alert(mstrSeleccione);		   
	}	
	function fc_FechaxD(){ alert(document.getElementById("cboAnio").value);
	 document.getElementById("txhAnio").value=document.getElementById("cboAnio").value;}
	 
	 function fc_Estado(){
	 
	 }	
	 function fc_TipoSugerencia(){
	 
	 }
				
	 function fc_SeleccionarRegistro(srtCodSugerencia, srtFechaCrea, srtDescripTipoSugerencia, srtDescripcion,
	  srtDescripCalificacion, srtDescripCodMaterial, srtDescripcionUsuario, srtEmail)
	  {		  document.getElementById("txhCodSelec").value=srtCodSugerencia;	
			  document.getElementById("txhFechaRegistro").value=srtFechaCrea;
			  document.getElementById("txhTipoSugerencia").value=srtDescripTipoSugerencia;
			  document.getElementById("txhComentario").value=srtDescripcion;
			  document.getElementById("txhCalRespuesta").value=srtDescripCalificacion;
			  document.getElementById("txhTipoMaterial").value=srtDescripCodMaterial;
			  document.getElementById("txhUsuario").value=srtDescripcionUsuario;
	 		  document.getElementById("txhEmail").value=srtEmail;
	 }
</script>	
</head>	
<body topmargin="10" leftmargin="10" rightmargin="10">
<form:form action="${ctx}/biblioteca/bib_bandeja_buzon_sugerencias.html" commandName="control" id="frmMain">	
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codSelec" id="txhCodSelec"/>
<form:hidden path="mes" id="txhMes"/>
<form:hidden path="anio" id="txhAnio"/>
<form:hidden path="estado" id="txhEstado"/>
<form:hidden path="fechaRegistro" id="txhFechaRegistro"/>
<form:hidden path="tipoSugerencia" id="txhTipoSugerencia"/>
<form:hidden path="comentario" id="txhComentario"/>
<form:hidden path="usuario" id="txhUsuario"/>
<form:hidden path="calRespuesta" id="txhCalRespuesta"/>
<form:hidden path="tipoMaterial" id="txhTipoMaterial"/>
<form:hidden path="periodoActual" id="txhPeriodoActual"/>
<form:hidden path="email" id="txhEmail"/>

<!-- form:hidden path="banderaActualizar" id="txhBanderaActualizar"/-->
	
	<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>

	<!--Page Title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="340px" class="opc_combo"><font style="">Administraci�n del Buz�n de Sugerencias</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>	
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="5" class="tabla"
		bordercolor="red" style="width:94%;height:50px;margin-left: 11px;">
		<tr>
			<td>
				<table width="99%" align="center" border="0" bordercolor="red" cellspacing="3" cellpadding="1">
					<tr>
						<td>&nbsp;Periodo :</td>
						<td>&nbsp;
							<form:select path="codMeses" id="codMeses" cssClass="combo_o" cssStyle="width:180px" onchange="javascript:fc_TipoSugerencia();">
								
									<c:if test="${control.codListaMeses!=null}">
										<form:options itemValue="id" itemLabel="name" 
											items="${control.codListaMeses}" />
									</c:if>
							</form:select>&nbsp;&nbsp;&nbsp;							
							<form:select path="codAnios" id="codAnios" cssClass="combo_o" cssStyle="width:80px"  onchange="javascript:fc_TipoSugerencia();">
								
									<c:if test="${control.codListaAnios!=null}">
										<form:options itemValue="id" itemLabel="name" 
											items="${control.codListaAnios}" />
									</c:if>
								</form:select>
						</td>
						<td align="right">&nbsp;Tipo Sugerencia :</td>
						<td align="right">&nbsp;
						    <form:select path="codTipoSugerencia" id="codTipoSugerencia" cssClass="cajatexto" cssStyle="width:150px"  onchange="javascript:fc_TipoSugerencia();">
								<form:option value="-1">--Todos--</form:option>
									<c:if test="${control.codListaTipoSugerencia!=null}">
										<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
											items="${control.codListaTipoSugerencia}" />
									</c:if>
								</form:select>&nbsp;
						</td>	
					</tr>
					<tr>
						<td>&nbsp;Estado :</td>
						<td>&nbsp;
						    <form:select path="codEstado" id="codEstado" cssClass="cajatexto" cssStyle="width:120px"  onchange="javascript:fc_Estado();">
								<form:option value="-1">--Todos--</form:option>
									<c:if test="${control.codListaEstado!=null}">
										<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
											items="${control.codListaEstado}" />
									</c:if>
								</form:select>
						</td>
						<td></td>
						<td align="right">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
								<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:Fc_Limpiar();" ></a>
							&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:Fc_Buscar();"></a>
							&nbsp;
						</td>								
					</tr>
				</table>
			</td>
		</tr>	
	</table>
	
	<table><tr><td height="5px"></td></tr></table>
	
	<div style="overflow: auto; height: 335px;width:97%">
	<table cellpadding="0" cellspacing="0" ID="Table1" style="margin-left:11px;width:97%" border="0" bordercolor="red">
		<tr>		
			<td>				
				<display:table name="sessionScope.listaSugerencias" cellpadding="0" cellspacing="1"
				   decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator" id="tblIncidencias" 
				   pagesize="6" requestURI="" style="border: 1px solid #048BBA;width:100%">
						<display:column property="rbtSeleccion" title="Sel" headerClass="grilla" 
							class="tablagrilla" style="width:5%;text-align:center"/>
						<display:column property="fechaCrea" title="Fec. Reg." headerClass="grilla" 
							class="tablagrilla" style="width:8%; text-align:center"/>
						<display:column property="descripTipoSugerencia" title="Tipo" headerClass="grilla" 
							class="tablagrilla" style="width:8%" />
						<display:column property="mostrarComentario" title="Comentario." headerClass="grilla" 
							class="tablagrilla" style="width:25%;text-align:center"/>

						<display:column property="respuesta" title="Respuesta" headerClass="grilla" 
							class="tablagrilla" style="width:20%;text-align:left"/>
						
						<display:column property="descripCalificacion" title="Calificaci&oacute;n" headerClass="grilla" 
							class="tablagrilla" style="width:8%;text-align:center"/>
						<display:column property="descripCodMaterial" title="Orientado A" headerClass="grilla" 
							class="tablagrilla" style="width:8%;text-align:left"/>
						<display:column property="descripcionUsuario" title="Usuario" headerClass="grilla" 
							class="tablagrilla" style="width:20%;text-align:left"/>
						<display:column property="email" title="Correo" headerClass="grilla" 
							class="tablagrilla" style="width:8%;text-align:left"/> <!-- JHPR 2008-7-31 -->		
													
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
					</display:table>					
			</td>			
		</tr>		
	</table>
	</div>
	
	<table cellspacing="0" cellpadding="0" border="0" bordercolor="red" align="right" style="margin-top: 8px;margin-right:43px">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgPrest','','${ctx}/images/biblioteca/atender2.jpg',1)">
						<img src="${ctx}/images/biblioteca/atender1.jpg" align="absmiddle" alt="Atender" id="imgPrest" style="CURSOR: pointer" onclick="javascript:Fc_Atender();"></a>
		</td>
		</tr>
	</table>
</form:form>	
</body>
</html>