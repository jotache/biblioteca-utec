<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}	
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Horario de Atenci&oacute;n
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 455px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" bordercolor="blue" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<p align="justify"><br>
					       <span class="TituloBiblio">TECSUP CENTRO - Campus Lima</span><br>
					Lunes a Viernes: 8:00 a 21:30<br>
					S&aacute;bados: 8:00 a 17:00 <br>
					<br/>
					<span class="TituloBiblio">TECSUP SUR - Campus Arequipa</span><br>
					Lunes a Viernes: 9:00 a 18:00<br>
					S&aacute;bados: 8:00 a 11:00<br>
					<br/>
					<span class="TituloBiblio">TECSUP NORTE - Campus Trujillo</span><br>
					Lunes a Viernes: 08:00 a 13:00 - 14:00 a 18:30<br/> 
					<br/>
					<span class="TituloBiblio">UTEC - BARRANCO</span><br>
					Lunes a Viernes: 08:00 a 20:00<br/>
					S&aacute;bados: 08:00 a 13:00
					</p>
				</td>
			</tr>
	    </table>
		</div>		

</body>
</html>