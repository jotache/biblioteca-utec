<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
	function onLoad()
	{
	}	
</script>
</head>
<body>
	
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	 Recursos de Acceso Abierto
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 550px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" bordercolor="blue" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	
 				<div style="text-align:center; margin-top:10px">
	                <table style="width:660px; margin-left:auto; margin-right:auto; border-color: #A8A8A8;" cellpadding=5 cellspacing=0 border="1">
	                	<tr>
	                		<td class="TituloBiblio">T&iacute;tulo</td>
	                		<td class="TituloBiblio">Autor</td>
	                		<td class="TituloBiblio">Pais</td>	                		
	                		<td class="TituloBiblio">A�o Pub.</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UMUhRRlJ1aTdHTDg&authuser=0" target="_blank">
	                			Momentos en la historiografia de la Paideia Griega y lecturas de ella en los tiempos de la Posmodernidad</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                	
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UT3pGSVY2RTJhczg&authuser=0" target="_blank">
	                			La buena pregunta y el libro"�Tu qu� sabes?". Elementos a considerar: Lecturas desde el mundo Globalizado y la Concrecci�n del "Paradigma Ecologico".
	                			</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                		                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208US3pSTXc4VGhVWnc&authuser=0" target="_blank">
	                			El libro y el Bios: Algunos momentos en su historiograf�a. Lectura desde EL Paradigma Ecol�gico./ Volumen 1.</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>
	                		
	                		<td>2010</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UZjFoMVQ0d05odVE&authuser=0" target="_blank">
	                			El libro y el Bios: Algunos momentos en su historiograf�a. Lectura desde EL Paradigma Ecol�gico./ Volumen 2.</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UYXJ3QkRHSWhBR00&authuser=0" target="_blank">
	                			La antigua Ret�rica Grecoromana y la educaci�n : En la perspectiva de la Te�ria de la Argumentaci�n y la Posmodernidad.</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UazhoSERLZ3AtMTA&authuser=0" target="_blank">
	                			Apuntes sobre la Cibercultura y la Alfabetizaci�n Digital Posmoderna.</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>	                	
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UMC1IUkh5djIzck0&authuser=0" target="_blank">
	                			Morin y el "M�todo" Consolidaci�n de su Corpus Te�rico</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UY19BNWpVOU9USlU&authuser=0" target="_blank">
	                			Las conexiones ocultas de Fritjof Capra</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UdG1UeFlsZUFKQW8&authuser=0" target="_blank">
	                			El pr�ximo escenario global de Knichi Ohmae: Momento cumbre de su tejido te�rico y la Socializaci�n del Paradigma de la Econom�a Global.</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>	
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208Ub1BaTTBfNHlRMVU&authuser=0" target="_blank">
	                			La confianza elemento dinamizador del �xito organizacional y empresarial en la perspectiva teor�tica del paradigma ecol�gico</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208USnZQc2JjaHJWRW8&authuser=0" target="_blank">
	                			Sobre las sociedades de la informaci�n y la del conocimiento: criticas a las llamadas ciudades del conocimento latinomericanas desde el paradigma ecol�gico</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UN3VtWXFoSU95cHM&authuser=0" target="_blank">
	                			La pedagog�a critica en el IESALC/UNESCO y la universidad colombiana en el marco de la revoluci�n educativa entre el 2005-2010</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2011</td>
	                	</tr>
	                	
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UaVNjQmF0ZGgydjA&authuser=0" target="_blank">
	                			Apuntes sobre la pedagog�a cr�tica: su emergencia, desarrollo y rol en la posmodernidad
	                			</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2010</td>
	                	</tr>	
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208UNTBkMGZTcmpLZmM&authuser=0" target="_blank">
	                			Apuntes sobre la pedagog�a cr�tica: su emergencia, desarrollo y rol en la posmodernidad vol2.
	                			</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2011</td>
	                	</tr>	                	
	                		
	                	<tr>
	                		<td><a href="https://drive.google.com/open?id=0B2yxzIKs208Ub2s5TDNWLVlJbXM&authuser=0" target="_blank">
	                			Elementos de la historiograf�a de la universidad y la departamentalizaci�n en la Santiago De Cali en estos los tiempos del paradigma de la econom�a global
	                			</a></td>
	                		<td>Lopez Nore�a, German</td>
	                		<td>Colombia</td>	                		
	                		<td>2013</td>
	                	</tr>
	                	
	                </table>
                </div>        			
        			
				</td>
			</tr>
	    </table>
		</div>		

</body>
</html>