<%@ include file="/taglibs.jsp"%>

<html>
<head>
	
<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />	
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
<script src="${ctx}/scripts/js/funciones_bib-fechas.js" type="text/JavaScript"></script>

<script type="text/javascript">
	function ventanaAtencion(){		
		var vsede = document.getElementById('txhSedeSel').value;
		var sRuta = '${ctx}/biblioteca/bib_atencion.html?sede='+vsede;
		var sNameWindow = 'Atencion';
		var featuresWin	  = 'width=' +  (screen.availWidth - 10) +
							', height=' + (screen.availHeight - 10) + 
							', top=0'+
							', left=0'+
							', scrollbars=1'+
							', resize=0,'+
							', menubar=0';
		var winLogin = window.open(sRuta, sNameWindow,featuresWin);
		winLogin.focus();	
	}
	
	function onLoad(){
		fc_OcultarDisponibilidad();
	  	
		if(document.getElementById("txhDisponible").value=="")
			document.getElementById("txhDisponible").value="0";
			
		document.getElementById("txhCodComponentesSeleccionados").value="";
				
		if(document.getElementById("txhMsg").value=="OK_ELIMINAR"){
			alert(mstrElimino);
		}else if(document.getElementById("txhMsg").value=="ERROR_ELIMINAR"){
			alert(mstrNoElimino);
		}else if(document.getElementById("txhMsg").value=="FECHA_HORA_INVALIDA"){
			alert('La fecha u hora de reserva no son v�lidas.');
		}else if(document.getElementById("txhMsg").value=="RECURSO_YA_NO_ESTA_RESERVADO"){
			alert("El recurso ya no se encuentra Reservado, verifique nuevamente");						
		}else if(document.getElementById("txhMsg").value=="OK_APLICAR"){
			alert("El recurso se aplic� satisfactoriamente.");
		}else if(document.getElementById("txhMsg").value=="NO_CONINCIDE_FECHA"){		
			alert("La fecha actual no es la \nmisma que la fecha de reserva");
		}else if(document.getElementById("txhMsg").value=="ERROR_APLICAR"){
			alert("Problemas al aplicar recurso consulte con el Administrador.");
		}else if(document.getElementById("txhMsg").value=="USU_NO_CONFIG"){
			alert("El usuario no esta configurado\npara aplicar sanciones");
		}else if(document.getElementById("txhMsg").value=="APL_SANCION"){			
			var msg2 = "<%=(( request.getAttribute("msg2")==null)?"": request.getAttribute("msg2"))%>";
			var msg3 = "<%=(( request.getAttribute("msg3")==null)?"": request.getAttribute("msg3"))%>";
			var msg4 = "<%=(( request.getAttribute("msg4")==null)?"": request.getAttribute("msg4"))%>";			
			alert("Al usuario se le aplico una sanci�n con " + msg3 + " dias de penalidad \nsu fecha fin de sanci�n es " + msg4);
		}
		document.getElementById("txhMsg").value="";
		document.getElementById("txhOperacion").value="";
	}
	
	function fc_Regresar(){
		location.href =  "${ctx}/menuBiblioteca.html?txhSedeSel="+document.getElementById("txhSedeSel").value;		
	}
	function fc_Buscar(){			
		if(fc_ValidaRangoHora("codHoraInicio","codHoraFin","1")==false){			
			return;
		}		
		fecha1=document.getElementById("txtFechaInicio").value;
		fecha2=document.getElementById("txtFechaFin").value;
		
		if(fecha1==""){
			alert('Debe ingresar una fecha inicial');
			document.getElementById("txtFechaInicio").focus();
			return false;
		}
		//verifica si la disponibilidad esta con check
		if(document.getElementById("Checkbox1").checked==false){
			if(fecha2==""){
				alert('Debe ingresar una fecha de f�n');
				document.getElementById("txtFechaFin").focus();
				return false;
			}
			if(fc_ValidaFechaIniFechaFin2(fecha2,fecha1)!="1"){
				alert('La fecha final debe ser mayor o igual a la inicial');
				return false;
			}
		}
		if(document.getElementById("txhDisponible").value==""){
	 		document.getElementById("txhDisponible").value="0";
	 	}
	 	document.getElementById("txhOperacion").value = "BUSCAR";	 	
	 	document.getElementById("frmMain").submit();
 	}
 	
	function fc_Actualizar(){
		document.getElementById("txhOperacion").value = "BUSCAR";	 	
	 	document.getElementById("frmMain").submit();
	}
	function fc_Limpiar(){
		document.getElementById("codTipoSala").value="-1";
	    document.getElementById("codNombreSalas").value="-1" 
		document.getElementById("codHoraInicio").value="";
		document.getElementById("codHoraFin").value=""; 
		document.getElementById("txtFechaInicio").value="";
		document.getElementById("txtFechaFin").value="";
		document.getElementById("txtCapacidadMaxima").value="";
		document.getElementById("txhDisponible").value="0";
		document.getElementById("txtCodigo").value="";		
		fc_LimpiarCombo();
	}
	function fc_LimpiarCombo(){
		combo = document.getElementById("codNombreSalas");
		tam = combo.options.length;
		for(var i=tam-1; i > 0 ; i--){						
			var aBorrar = document.getElementById("codNombreSalas").options[i];			
			aBorrar.parentNode.removeChild(aBorrar);
		}		
	}
		
	function fc_NombreSalas(){//codHoraFin
		if(document.getElementById("codNombreSalas").value!="-1"){
			document.getElementById("txhOperacion").value = "NOMBRE_SALA";
			document.getElementById("frmMain").submit();
		}
		else document.getElementById("txtCapacidadMaxima").value="";
		
	}
	
	function fc_TipoSalas(){		
	    if(document.getElementById("codTipoSala").value!="-1"){
	    	document.getElementById("txhOperacion").value = "TIPO_SALA";
			document.getElementById("frmMain").submit();
		}
		else{
			document.getElementById("txtCapacidadMaxima").value="";
		    document.getElementById("txhOperacion").value = "LIMPIAR_NOMBRE_SALA";
			document.getElementById("frmMain").submit();
		} 
	}
	
    function fc_seleccionarRegistro(srtCodReserva, srtUsuarioReserva){    	
		strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
		
		flag=false;
		
		if(strCodSel!=''){	
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";
			for (i=0;i<=ArrCodSel.length-2;i++)
			{	if (ArrCodSel[i] == srtCodReserva) flag = true; 
				else strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
				
		}
		if(!flag){
			strCodSel = strCodSel + srtCodReserva + '|';
			
		}
		
		document.getElementById("txhCodComponentesSeleccionados").value = strCodSel;		
	}
	
	function fc_GetNumeroSeleccionados(){
		strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
		
		if ( strCodSel != '' ){
			ArrCodSel = strCodSel.split("|");			
			return ArrCodSel.length - 1;			
		}		
		else return 0;
	}
    
    function fc_Eliminar(){

    	var sedeSel = fc_Trim(document.getElementById("txhSedeSel").value);
    	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
    	
    	if (sedeSel!=''){
    		if (sedeSel!=sedeUsu){
    			alert('No puede Eliminar reservas en esta Sede.');
    			return 0;
    		}
    	}
    	    	
	    nroSelec = fc_GetNumeroSeleccionados();
	    document.getElementById("txhNroComponentes").value=nroSelec;
	   	if(document.getElementById("txhDisponible").value=="0"){
	   		if( nroSelec != "0"){
	   			if( confirm(mstrEliminar) ){
					document.getElementById("txhOperacion").value = "ELIMINAR";
					document.getElementById("frmMain").submit();
				}
			}
		  	else alert(mstrSeleccione);
		}			
    }
    
    function fc_Agregar(){

    	var sedeSel = fc_Trim(document.getElementById("txhSedeSel").value);
    	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
    	
    	if (sedeSel!=''){
    		if (sedeSel!=sedeUsu){
    			alert('No puede Agregar reservas en esta Sede.');
    			return 0;
    		}
    	}
    	        
	    srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	    srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	    srtCodSalas=document.getElementById("codNombreSalas").value;
	    srtCapMaxima=document.getElementById("txtCapacidadMaxima").value;
	    srtFechaBD=document.getElementById("txhFechaBD").value;
	    srtVacantes=document.getElementById("txhTxtVacantes").value;
	    srtEstado=document.getElementById("txhEstado").value;
	    srtCodTipoSala = document.getElementById("codTipoSala").value; 
	    
	    Fc_Popup_Resizable("${ctx}/biblioteca/bib_consulta_reserva_salas_agregar.html?txhCodUsuario="+srtCodEvaluador+
				"&txhCodPeriodo="+srtCodPeriodo+
				"&txhCodigoSala="+srtCodSalas+
				"&txhCapMaxima="+srtCapMaxima+
				"&txhFechaBD="+srtFechaBD+
				"&txhVacantes="+srtVacantes+
				"&txhEstado="+srtEstado+
				"&txhSedeSel="+sedeSel+
				"&txhCodTipoSala="+srtCodTipoSala
				,450,200);	
    }

	function reservar(codRecurso,capacidad,horaInicio,horaFin){

    	var sedeSel = fc_Trim(document.getElementById("txhSedeSel").value);
    	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
    	
    	if (sedeSel!=''){
    		if (sedeSel!=sedeUsu){
    			alert('No puede Aplicar reservas en esta Sede.');
    			return 0;
    		}
    	}
		
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	    srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	    srtCodSalas=document.getElementById("codNombreSalas").value;
	    srtCapMaxima=document.getElementById("txtCapacidadMaxima").value;
	    srtFechaBD=document.getElementById("txhFechaBD").value;
	    srtVacantes=document.getElementById("txhTxtVacantes").value;
	    srtEstado=document.getElementById("txhEstado").value;
	    srtCodTipoSala = document.getElementById("codTipoSala").value; 
	    strFechaReserva = document.getElementById("txtFechaInicio").value; 
	    Fc_Popup("${ctx}/biblioteca/bib_consulta_reserva_salas_agregar.html?txhCodUsuario="+srtCodEvaluador+
				"&txhCodPeriodo="+srtCodPeriodo+
				"&txhCodigoSala="+srtCodSalas+
				"&txhCapMaxima="+srtCapMaxima+
				"&txhFechaBD="+srtFechaBD+
				"&txhVacantes="+srtVacantes+
				"&txhEstado="+srtEstado+
				"&txhCodTipoSala="+srtCodTipoSala+
				"&txhAccion=RESERVAR"+
				"&txhCodRecurso="+fc_Trim(codRecurso)+
				"&txhCodSeqRecurso="+
				"&txhHoraIni="+horaInicio+
				"&txhHoraFin="+horaFin+
				"&txhCapacidad="+capacidad+
				"&txhFechaRec="+strFechaReserva
				,450,200); 
	}

	function accionReservar(codRecurso,codSecuencia){

    	var sedeSel = fc_Trim(document.getElementById("txhSedeSel").value);
    	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
    	
    	if (sedeSel!=''){
    		if (sedeSel!=sedeUsu){
    			alert('No puede ejecutar o cancelar reservas en esta Sede.');
    			return 0;
    		}
    	}
		
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	    srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	    srtCodSalas=document.getElementById("codNombreSalas").value;
	    srtCapMaxima=document.getElementById("txtCapacidadMaxima").value;
	    srtFechaBD=document.getElementById("txhFechaBD").value;
	    srtVacantes=document.getElementById("txhTxtVacantes").value;
	    srtEstado=document.getElementById("txhEstado").value;
	    srtCodTipoSala = document.getElementById("codTipoSala").value; 
	    
	    Fc_Popup("${ctx}/biblioteca/bib_consulta_reserva_salas_agregar.html?txhCodUsuario="+srtCodEvaluador+
				"&txhCodPeriodo="+srtCodPeriodo+
				"&txhCodigoSala="+srtCodSalas+
				"&txhCapMaxima="+srtCapMaxima+
				"&txhFechaBD="+srtFechaBD+
				"&txhVacantes="+srtVacantes+
				"&txhEstado="+srtEstado+
				"&txhCodTipoSala="+srtCodTipoSala+
				"&txhAccion=CANCELAR_EJECUTAR"+
				"&txhCodRecurso="+codRecurso+
				"&txhCodSeqRecurso="+codSecuencia
				,450,200); 
	}	
    
    function terminarEjecucion(codRecurso,codSecuencia){

    	var sedeSel = fc_Trim(document.getElementById("txhSedeSel").value);
    	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
    	
    	if (sedeSel!=''){
    		if (sedeSel!=sedeUsu){
    			alert('No puede terminar reservas en esta Sede.');
    			return 0;
    		}
    	}
    	        
	    srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	    srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	    srtCodSalas=document.getElementById("codNombreSalas").value;
	    srtCapMaxima=document.getElementById("txtCapacidadMaxima").value;
	    srtFechaBD=document.getElementById("txhFechaBD").value;
	    srtVacantes=document.getElementById("txhTxtVacantes").value;
	    srtEstado=document.getElementById("txhEstado").value;
	    srtCodTipoSala = document.getElementById("codTipoSala").value; 
	    
	    Fc_Popup("${ctx}/biblioteca/bib_consulta_reserva_salas_agregar.html?txhCodUsuario="+srtCodEvaluador+
				"&txhCodPeriodo="+srtCodPeriodo+
				"&txhCodigoSala="+srtCodSalas+
				"&txhCapMaxima="+srtCapMaxima+
				"&txhFechaBD="+srtFechaBD+
				"&txhVacantes="+srtVacantes+
				"&txhEstado="+srtEstado+
				"&txhCodTipoSala="+srtCodTipoSala+
				"&txhAccion=TERMINAR"+
				"&txhCodRecurso="+codRecurso+
				"&txhCodSeqRecurso="+codSecuencia
				,450,200);        
    }
    
    function fc_Ver(param){
    	fc_OcultarDisponibilidad();    	
    	if(document.getElementById("txhDisponible").value==param)    		
    		document.getElementById("txhDisponible").value="0";    	
    	else    		
    		document.getElementById("txhDisponible").value=param;    		
    	
    }

	function fc_CheckEstado(tipo,param){
			
		if (tipo=='P')
			if(document.getElementById("txhEstadoPendiente").value==param)    		
	    		document.getElementById("txhEstadoPendiente").value="0";    	
	    	else    		
	    		document.getElementById("txhEstadoPendiente").value=param;
		else if (tipo=='R')
			if(document.getElementById("txhEstadoReservado").value==param)    		
	    		document.getElementById("txhEstadoReservado").value="0";    	
	    	else    		
	    		document.getElementById("txhEstadoReservado").value=param;
		else if (tipo=='E')
			if(document.getElementById("txhEstadoEjecutado").value==param)    		
	    		document.getElementById("txhEstadoEjecutado").value="0";    	
	    	else    		
	    		document.getElementById("txhEstadoEjecutado").value=param;
	}
	
		    
    function fc_OcultarDisponibilidad(){
    	if(document.getElementById("Checkbox1").checked==true){
    		bandeja01.style.display="none";
    		bandeja02.style.display="";
    		document.getElementById("botonA").style.display="none";
    		document.getElementById("imgquitar").style.display="none";
    		document.getElementById("txtFechaFin").style.display="none";
    		document.getElementById("imgFecFin").style.display="none";
    		document.getElementById("trCodigo").style.display="none";
    		document.getElementById("trCodigo2").style.display="none";    		
    		document.getElementById("chkEstados").style.display="";
    	}
    	else{
    		bandeja01.style.display="";
    		bandeja02.style.display="none";
    		document.getElementById("botonA").style.display="";
    		document.getElementById("imgquitar").style.display="";
    		document.getElementById("txtFechaFin").style.display="";
    		document.getElementById("imgFecFin").style.display="";
    		document.getElementById("trCodigo").style.display="";
    		document.getElementById("trCodigo2").style.display="";
    		document.getElementById("chkEstados").style.display="none";    		    		
    	}
    	if(document.getElementById("txhEstado").value!="A"){
    		document.getElementById("botonA").style.display="none";
    		document.getElementById("tablaRegresar").style.display="none";    		
    	}
    }
    function fc_Aplicar(){

    	var sedeSel = fc_Trim(document.getElementById("txhSedeSel").value);
    	var sedeUsu = fc_Trim(document.getElementById("txhSedeUsuario").value);
    	
    	if (sedeSel!=''){
    		if (sedeSel!=sedeUsu){
    			alert('No puede Aplicar reservas en esta Sede.');
    			return 0;
    		}
    	}
    	    	
    	if(fc_GetNumeroSeleccionados()=="0"){
    		alert(mstrSeleccione);
    		return false;    			
   		}
    	if(fc_GetNumeroSeleccionados() > 1){
    		alert(mstrSeleccioneUno);
    		return false;
    	}
    	cadena = document.getElementById("txhCodComponentesSeleccionados").value;
    	var codReserva = cadena.split("|");
    	        	
    	document.getElementById("txhCodReserva").value=codReserva[0];
    	document.getElementById("txhCodSala").value = document.getElementById("txhCodSala"+codReserva[0]).value;
    	document.getElementById("txhUsuarioReserva").value = document.getElementById("txhUsuReserva"+codReserva[0]).value;
    	
    	document.getElementById("txhOperacion").value = "APLICAR";
    	document.getElementById("frmMain").submit();
    }
    
    function fc_Horas(){
    }
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_consulta_reserva_salas.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
	<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
	<form:hidden path="codComponentesSeleccionados" id="txhCodComponentesSeleccionados"/>
	<form:hidden path="nroComponentes" id="txhNroComponentes"/>
	<form:hidden path="fechaBD" id="txhFechaBD"/>
	<form:hidden path="txtVacantes" id="txhTxtVacantes"/>
	<form:hidden path="usuarioReserva" id="txhUsuarioReserva"/>
	<form:hidden path="disponible" id="txhDisponible"/>
	<form:hidden path="estado" id="txhEstado"/>
	<form:hidden path="flgUsuAdmin" id="flgUsuAdmin"/>	
	<form:hidden path="estadoPendiente" id="txhEstadoPendiente"/>
	<form:hidden path="estadoReservado" id="txhEstadoReservado"/>
	<form:hidden path="estadoEjecutado" id="txhEstadoEjecutado"/>
	<form:hidden path="sedeSel" id="txhSedeSel"/>
	<form:hidden path="sedeUsuario" id="txhSedeUsuario"/>	

	<!-- AGREGADO NAPA -->
	<form:hidden path="tamListaReservaSala01" id="txhTamListaReservaSala01"/>
	<form:hidden path="tamListaReservaSala02" id="txhTamListaReservaSala02"/>
	<form:hidden path="codSala" id="txhCodSala"/>
	<form:hidden path="codReserva" id="txhCodReserva"/>

	<!-- Icono Regresar -->
	
	<table cellpadding="0" cellspacing="0" border="0" width="97%" id="tablaRegresar">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_Regresar();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	
			
	<!--T�tulo de la P�gina  -->
	<!-- TITULO ADMINISTRADOR -->
	<c:if test="${control.estado=='A'}">
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="340px" class="opc_combo"><font style="">Consulta de Reserva de Salas </font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	</table>
	</c:if>
	<!-- TITULO WEB -->
	<c:if test="${control.estado!='A'}">
	<table cellpadding="0" cellspacing="0" border="0" width="98%" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
			<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
			<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
			 	<font style="">
				 	Consulta de Reserva de Salas
			 	</font>
			</td>				
			<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		</tr>
	</table>
	<!-- table><tr height="5px"><td></td></tr></table-->
	</c:if>
	<!-- table><tr height="3px"><td></td></tr></table-->	
	<table background="${ctx}/images/biblioteca/fondosup.jpg" border="0" cellspacing="4" cellpadding="0" class="tabla"
			style="width:97%;height:50px;margin-left: 11px;" >	
		<tr>				
			<td  nowrap="nowrap">Tipo de Sala :</td>
			<td >
			
<%-- 				<form:select path="codTipoSala" id="codTipoSala" cssClass="cajatexto" --%>
<%--  							cssStyle="width:120px;"  onchange="javascript:fc_TipoSalas();">  --%>
<%--  					<form:option value="-1">--Todas--</form:option>  --%>
			
<%--  					<c:if test="${control.codListaTipoSala!=null}">  --%>
				
<%--  				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"  --%>
<%--   					items="${control.codListaTipoSala}"  />   --%>
				
					<%
					//TODO: Actualizar (No se permite que los alumnos seleccionen Polideportivo)	
					//Se maneja por flag en tablas
					%>
													
<%-- 						<form:option value="0001" label="Sala de Estudio"/>					 --%>
<%-- 						<form:option value="0002" label="Sala de Internet"/>														 --%>
<%-- 						<c:if test="${control.flgUsuAdmin=='1'}" > --%>
<%-- 							<form:option value="0005" label="Salas Eventos Especiales"/> --%>
<%-- 							<form:option value="0004" label="Polideportivo"/> --%>
<%-- 							<form:option value="0006" label="Lab. Qu�mica y Metalurgia"/> --%>
<%-- 						</c:if> --%>
<%-- 						<c:if test="${control.flgLabQuimica=='1'}" > --%>
<%-- 	            			<form:option value="0006" label="Lab. Qu�mica y Metalurgia"/>            		            	 --%>
<%-- 	          			</c:if> --%>
<%--           				<form:option value="0003" label="Sala de Internet - UTEC"/> --%>
<%--             			<form:option value="0007" label="Sala de Estudio - UTEC"/> --%>
<%--             			<form:option value="0008" label="Lab. 603 - UTEC"/> --%>
<%-- 					</c:if>																	 --%>
<%-- 				</form:select> --%>
				
				
				<c:if test="${control.codListaTipoSala!=null}">
					<select id="codTipoSala" name="codTipoSala" class="cajatexto" style="width:120px;" onchange="javascript:fc_TipoSalas();">
					<option value="-1">--Todas--</option>
					<c:forEach items="${control.codListaTipoSala}" var="salas">
						<!-- usuarios no admin  -->
						<c:if test="${control.flgUsuAdmin=='0'}">
							<c:if test="${salas.dscValor6=='0'}">
								<option value="${salas.dscValor1}" 								
									<c:if test="${salas.dscValor1==control.codTipoSala}">
										<c:out value="selected='selected'"/> 
									</c:if>								
								><c:out value="${salas.descripcion}"></c:out></option>
							</c:if>
						</c:if>
							
						<!-- usuarios admin -->
						<c:if test="${control.flgUsuAdmin=='1'}">							
							<option value="${salas.dscValor1}"
								<c:if test="${salas.dscValor1==control.codTipoSala}">
									<c:out value="selected='selected'"/> 
								</c:if>
							><c:out value="${salas.descripcion}"></c:out></option>							
						</c:if>
														
					</c:forEach>
					</select>
				</c:if>
				
			</td>
			
			<td align="left" colspan="4">Nombre Sala :&nbsp;
				<form:select path="codNombreSalas" id="codNombreSalas" cssClass="cajatexto"
							cssStyle="width:120px;"  onchange="javascript:fc_NombreSalas();">
					<form:option value="-1">--Todas--</form:option>
					<c:if test="${control.codListaNombreSala!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
							items="${control.codListaNombreSala}" />
					</c:if>
				</form:select>&nbsp;&nbsp;
					Capac. Max.:&nbsp;<form:input path="txtCapacidadMaxima" id="txtCapacidadMaxima" readonly="true"
					cssClass="cajatexto_1" size="3" cssStyle="text-align:center;"/>
			</td>				
			<td>
				<%--  
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('btnAtencion','','${ctx}/images/iconos/buscaraaaa.jpg',1)">
						<img src="${ctx}/images/iconos/buscaraaaaa.jpg" onclick="javascript:ventanaAtencion();" style="cursor:hand;" align="absmiddle" id="btnAtencion" width="20" height="17" >
				</a>
				--%>

				<%-- --%> 
				<c:if test="${control.flgUsuAdmin=='1'}" >				
					<a href="#" onclick="javascript:ventanaAtencion();">
						<img src="${ctx}/images/biblioteca/reservas-biblioteca.png" width="100" height="20" border="0" />						
					</a>
				</c:if>
				
				
			</td>
		</tr>
		<tr>
			
			<td class="">Fecha :</td>
			<td colspan="2" nowrap="nowrap">
				<form:input path="txtFechaInicio" id="txtFechaInicio" onkeypress="fc_ValidaFecha('txtFechaInicio');" 
			  		onblur="fc_ValidaFechaOnblur('txtFechaInicio');" 
			  		cssClass="cajatexto_o" cssStyle="width:60px;" maxlength="10"/>
			  	<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','/biblioteca/images/iconos/calendario2.jpg',1)">
			  	<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni" 
			  		alt="Calendario" style='cursor:hand;' align="absmiddle"></a>
			  	<form:input path="txtFechaFin" id="txtFechaFin" onblur="fc_ValidaFechaOnblur('txtFechaFin');"
			   		onkeypress="fc_ValidaFecha('txtFechaFin');"
			   		cssClass="cajatexto_o" cssStyle="width:60px;" maxlength="10"/>
			  	<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','/biblioteca/images/iconos/calendario2.jpg',1)">
			  	<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin" 
			  		alt="Calendario" style='cursor:hand;' align="absmiddle"></a>
			</td>
			
			<td align="left" colspan="2">Horas:&nbsp;
				<form:select path="codHoraInicio" id="codHoraInicio" cssClass="cajatexto"
							cssStyle="width:55px"  onchange="javascript:fc_Horas();">
						<form:option value="">-Hora-</form:option>
						<c:if test="${control.listaHoras!=null}">
						<form:options itemValue="idHoraIni" itemLabel="horaIni"
							items="${control.listaHoras}" />
						</c:if>
			</form:select>&nbsp;
			 
			<form:select path="codHoraFin"	id="codHoraFin" cssClass="cajatexto"
							cssStyle="width:55px"  onchange="javascript:fc_Horas();">
						<form:option value="">-Hora-</form:option>
						<c:if test="${control.listaHoras!=null}">
						<form:options itemValue="idHoraFin" itemLabel="horaFin"
							items="${control.listaHoras}" />
						</c:if>
				</form:select>
			</td>

			<td>
				&nbsp;&nbsp;
				<input type="checkbox" id="Checkbox1" name="Checkbox1" 
				<c:if test="${control.disponible==control.consteDisponible}">
					<c:out value=" checked=checked " />
				</c:if>
				onclick="javascript:fc_Ver('1');">Consultar disponibilidad 
			</td>

			<td align="left" id="tdBuscar" >
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','/biblioteca/images/iconos/limpiar2.jpg',1)">				
				<img src="${ctx}/images/iconos/limpiar1.jpg"  align="absmiddle" id="imglimpiar" style="cursor:pointer" alt="Limpiar" onclick="javascript:fc_Limpiar();"></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','/biblioteca/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="absmiddle" id="imgBuscar" style="cursor:pointer" onclick="javascript:fc_Buscar();"></a>&nbsp;				
			</td>				
		</tr>
		<tr >
			<td valign="middle"><div id="trCodigo2" >Usuario:</div>&nbsp;
			</td>
			<td>
				<div id="trCodigo"><form:input id="txtCodigo" path="codigo" 
					cssClass="cajatexto" cssStyle="width:90px"					 
					maxlength="30"/></div>&nbsp;
			</td>
			<td colspan="2" >
			
			</td>
			<td align="left"  colspan="3" id="chkEstados">
				Estado:
				<input type="checkbox" id="chkEstPendiente" name="chkEstPendiente" 
					<c:if test="${control.estadoPendiente==control.consteDisponible}">
						<c:out value=" checked=checked " />
					</c:if>
					onclick="javascript:fc_CheckEstado('P','1');">Pendientes 
				&nbsp;	
				<input type="checkbox" id="chkEstReservado" name="chkEstReservado" 
					<c:if test="${control.estadoReservado==control.consteDisponible}">
						<c:out value=" checked=checked " />
					</c:if>
					onclick="javascript:fc_CheckEstado('R','1');">Reservados
				&nbsp;	
				<input type="checkbox" id="chkEstEjecutado" name="chkEstEjecutado" 
					<c:if test="${control.estadoEjecutado==control.consteDisponible}">
						<c:out value=" checked=checked " />
					</c:if>
					onclick="javascript:fc_CheckEstado('E','1');">Ejecutandose
				
			</td>
			<%-- 		
			<td align="left" id="tdBuscarWeb">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiarWeb','','/biblioteca/images/iconos/limpiar2.jpg',1)">				
				<img src="${ctx}\images\iconos\limpiar1.jpg"  align="absmiddle" id="imglimpiarWeb" style="cursor:pointer" alt="Limpiar" onclick="javascript:fc_Limpiar();"></a>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscarWeb','','/biblioteca/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}\images\iconos\buscar1.jpg" alt="Buscar" align="absmiddle" id="imgBuscarWeb" style="cursor:pointer" onclick="javascript:fc_Buscar();"></a>				
				
				
			</td>
			--%>						
		</tr>		
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table cellpadding="0" cellspacing="0" id="" width="97%" 
		border="0" class="" style="margin-left:11px">				
			<tr>
			 	<td>
			 	<div style="overflow: auto; height:285px;" id="bandeja01">
			 	<table cellpadding="0" cellspacing="1" id="tablaBandeja01"
					style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
					<tr>						
						<td class="headtabla" width="5%" align="center">Sel.</td>
						<td class="headtabla" width="25%">Sala</td>
						<td class="headtabla" width="10%" align="center">Fecha</td>
						<td class="headtabla" width="10%" align="center">Hora<br>Inicio</td>
						<td class="headtabla" width="10%" align="center">Hora<br>Fin</td>
						<td class="headtabla" width="15%" align="center">Usuario</td>
						<td class="headtabla" width="25%">Nombre Usuario</td>													
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaConsultaReservaSala}"  >
					
					<c:choose>
						<c:when test="${loop.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
						<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>																								
							
						<td align="center" style="width: 5%">
						
						
						<c:if test="${control.flgUsuAdmin=='1'}" >
							<input type="checkbox" name="codProceso"
								value='<c:out value="${lista.codReserva}" />'
								id='chk<c:out value="${lista.codReserva}"/>'								
								onclick="fc_seleccionarRegistro(${lista.codReserva},${lista.usuReserva});">						
						</c:if> 
						<c:if test="${control.flgUsuAdmin!='1'}" >
							<c:if test="${lista.usuReserva==control.codEvaluador}">
							<input type="checkbox" name="codProceso"
								value='<c:out value="${lista.codReserva}" />'
								id='chk<c:out value="${lista.codReserva}"/>'								
								onclick="fc_seleccionarRegistro(${lista.codReserva},${lista.usuReserva});">
							</c:if>
							<c:if test="${lista.usuReserva!=control.codEvaluador}">
								<input type="checkbox" class="combo_1" readonly="readonly" disabled="disabled" />
							</c:if>
						</c:if>
						
								
							<input type="hidden" 
								id='txhCodSala<c:out value="${lista.codReserva}"/>'
								value='<c:out value="${lista.codTipoSala}" />'>							
							<input type="hidden" 
								id='txhUsuReserva<c:out value="${lista.codReserva}"/>'
								value='<c:out value="${lista.usuReserva}" />'>
						</td>
						<td align="left" style="width: 25%">
							<c:out value="${lista.nombreSala}" />
						</td>
						<td align="center" style="width: 10%">
							<c:out value="${lista.fechaReserva}" />
						</td>
						<td align="center" style="width: 10%">
							<c:out value="${lista.horaIni}" />
						</td>
						<td align="center" style="width: 10%">
							<c:out value="${lista.horaFin}" />														
						</td>
						<td align="center" style="width: 15%">
							<c:out value="${lista.loginUsuario}" />
						</td>
						<td align="left" style="width: 25%">						
							<c:out value="${lista.usuario}" />							
						</td>	
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaReservaSala01 == 0}'>
					<tr class="texto">
						<td colspan="7" align="center">
							No se encontraron registros
						</td>
					</tr>
					</c:if>
				</table>
				<c:if test='${control.tamListaReservaSala01 > 12}'>
					<script type="text/javascript">
						document.getElementById("tablaBandeja01").style.width = "98%";						
					</script>		 		
				</c:if>				
				</div>
				
				<div style="overflow: auto; height:285px;display: none;" id="bandeja02">
				<table cellpadding="0" cellspacing="1" id="tablaBandeja02"
					style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
					<tr>
						<td class="headtabla" width="20%" align="left">Sala</td>
						<td class="headtabla" width="10%" align="center">Hr. Inicio</td>
						<td class="headtabla" width="10%" align="center">Hr. Fin</td>
						<td class="headtabla" width="10%" align="center">Cap.</td>
						<td class="headtabla" width="15%" align="center">Estado</td>
						<td class="headtabla" width="25%" align="center">Usuario</td>																			
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaConsultaReservaSala02}"  >
					
					<c:if test='${(lista.codTipSala != "0006") || (lista.codTipSala=="0006" && lista.horaIni < "13:00")}'>
					<c:choose>
						<c:when test="${loop.count % 2 == 0}"><tr class="fondoceldablanco"></c:when>
						<c:otherwise><tr class="fondocelesteclaro"></c:otherwise>
					</c:choose>	
					
						<td align="left"  style="width: 20%">
							<input type="hidden" value="<c:out value="${lista.codTipoSala}" />" id="txhCodRec<c:out value="${loop.index}" />" />
							<input type="hidden" value="<c:out value="${lista.codReserva}" />" id="txhCodSeq<c:out value="${loop.index}" />" />

							<c:out value="${lista.nombreSala}" />
						</td>						
						<td align="center"  style="width: 10%">
							<c:out value="${lista.horaIni}" />
						</td>
						<td align="center"  style="width: 10%">
							<c:out value="${lista.horaFin}" />														
						</td>
						<td align="center"  style="width: 10%">
							<c:out value="${lista.capacidadMax}" />
						</td>						
						<td align="center"  style="width: 20%">
							<c:choose>
								<c:when test="${lista.reservas == 'R'}">
									<img id='imgReserv<c:out value="${loop.count}"/>' width="16" height="16" 
										src="${ctx}/images/biblioteca/status_reservado.png" alt="Reservado"	title="Reservado"
										<c:if test="${control.flgUsuAdmin=='1'}"> 
										style="cursor: hand;" onclick="javascript:accionReservar('<c:out value="${lista.codTipoSala}" />','<c:out value="${lista.codReserva}" />');"
										</c:if>								
										>&nbsp;&nbsp;Reservado
								</c:when>
								<c:when test="${lista.reservas == 'E'}">
									<img id='imgEjec<c:out value="${loop.count}"/>' width="16" height="16" 
										src="${ctx}/images/biblioteca/status_ejecucion.png" alt="Ejecutandose" title="En ejecuci�n"
										<c:if test="${control.flgUsuAdmin=='1'}"> 
										style="cursor: hand;" onclick="javascript:terminarEjecucion('<c:out value="${lista.codTipoSala}" />','<c:out value="${lista.codReserva}" />');"
										</c:if>
										>&nbsp;&nbsp;En ejecuci&oacute;n
								
								</c:when>
								<c:when test="${lista.reservas == 'X'}">
									<img id='imgEjec<c:out value="${loop.count}"/>' width="16" height="16" 
										src="${ctx}/images/biblioteca/status_inhabil.png" alt="No disponible" title="No disponible"										
										>&nbsp;&nbsp;<i>No Disponible</i>
								
								</c:when>
								<c:when test="${lista.reservas == ''}">
									<img id='imgLibre<c:out value="${loop.count}"/>' width="16" height="16" 
										src="${ctx}/images/biblioteca/status_libre.png" alt="Libre" title="Libre" 
										<c:if test="${control.flgUsuAdmin=='1'}"> 
										style="cursor: hand;" onclick="javascript:reservar('<c:out value="${lista.codSala}" />','<c:out value="${lista.capacidadMax}" />','<c:out value="${lista.horaIni}" />','<c:out value="${lista.horaFin}" />');"
										</c:if>
										>&nbsp;&nbsp;Libre							
								</c:when>
								<c:otherwise><c:out value="?" /></c:otherwise>
							</c:choose>
						</td>	
						<td align="center"  style="width: 25%">
							<c:out value="${lista.usuario}" />
						</td>
					</tr>
					</c:if>
					</c:forEach>
					<c:if test='${control.tamListaReservaSala02 == 0}'>
					<tr class="texto">
						<td colspan="6" align="center">
							No se encontraron registros
						</td>
					</tr>
					</c:if>	
				</table>
				<c:if test='${control.tamListaReservaSala02 > 12}'>
					<script type="text/javascript">
						document.getElementById("tablaBandeja02").style.width = "98%";
					</script>
				</c:if>				
				</div>
			</td>
			<td width="30px" valign="middle" align="right">			
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src="${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer;" id="imgagregar" onclick="fc_Agregar();" 
				alt="Agregar"></a>
				<br/>					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
				<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_Eliminar();"
				alt="Eliminar"></a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonA','','${ctx}/images/iconos/a2.jpg',1)">
				<img src="${ctx}/images/iconos/a.jpg" id="botonA" style="display: none;" style="cursor:pointer;" onclick="fc_Aplicar();"></a> 
			</td>			
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFechaInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	
	Calendar.setup({
		inputField     :    "txtFechaFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>