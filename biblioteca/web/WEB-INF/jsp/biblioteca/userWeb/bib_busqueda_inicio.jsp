<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%@page import="com.tecsup.SGA.modelo.UsuarioSeguriBib;"%>

<html>
<head>

<script type="text/javascript" src="${ctx}/scripts/jquery/jquery-1.10.2.min.js"></script>

<script src="${ctx}/scripts/js/Func_Comunes.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/stmenu.js" type="text/JavaScript"></script>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript" src="${ctx}/scripts/jquery/preloaders/jquery.preloaders.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jquery/filebox/jquery.filebox.js"></script>

<link href="${ctx}/scripts/jquery/switchery/dist/switchery.min.css" rel="stylesheet" />
<script src="${ctx}/scripts/jquery/switchery/dist/switchery.min.js"></script>



<script type="text/javascript">
	function go(url){
		window.location.href=url;
// 		window.location.reload(false);
	}

	function onLoad(){
		
	}
	
	function fc_Enter(e){
		if (e.keyCode == 13){fc_Ingresar()}
	}
	
	function fc_IngresarSistema(){
		document.getElementById("txtUsuario").focus();
	}

	//EBC-03-09-09
	function abrir_popup(url,width,height){
		var name = 'CambiarClave';
		//var width = 750;
		//var height = 550;
		var x = (screen.width-width)/2;
		var y = (screen.height-height)/2;
		
		campusNoticia = window.open(url, name, "width=" + width + ",height=" + height + ",toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,top=" + y + ",left=" + x + "");
		campusNoticia.focus();
	}
	
	function fc_Ingresar() {		
		if (document.getElementById("txtUsuario").value != "" && document.getElementById("txtClave").value != ""){
			
			strMsg = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
			
			document.getElementById('txhOperacion').value="INGRESAR";
			document.getElementById('frmMain').submit();
			
		} else {
			if (document.getElementById("txtUsuario").value == "") {
				alert('Debe ingresar el valor correspondiente en el campo ' + "Usuario");
				document.getElementById("txtUsuario").focus();
			}
			else if(document.getElementById("txtClave").value == "") {
				alert('Debe ingresar el valor correspondiente en el campo ' + "Clave");
				document.getElementById("txtClave").focus();
			}
			return false;
		}
	}
	
	
	function fc_BusquedaSimple()
	{ 
	  srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	  srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	  srtCodTipoMaterial=document.getElementById("codTipoMaterial").value;
	  srtTexto=document.getElementById("txtTexto").value;
	  srtCodBuscarBy=document.getElementById("codBuscarBy").value;
	  srtCodOrdenarBy=document.getElementById("codOrdenarBy").value;
	  srtSelSala=document.getElementById("txhSelSala").value;
	  srtSelDomicilio=document.getElementById("txhSelDomicilio").value;
	  
	  Fc_Popup("${ctx}/biblioteca/bib_busqueda_simple.html?txhCodUsuario="
	  	+srtCodEvaluador+ "&txhCodPeriodo="+srtCodPeriodo+ "&txhCodTipoMaterial="+srtCodTipoMaterial+
	  	"&txhTexto="+srtTexto+ "&txhCodBuscarBy="+srtCodBuscarBy+ "&txhCodOrdenarBy="+srtCodOrdenarBy+
	  	"&txhSelSala="+srtSelSala+ "&txhSelDomicilio="+srtSelDomicilio,950,600);
	 
	}
	
	function fc_BusquedaAvanzada()
	{ 	
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	 	srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
		parent.location.href="${ctx}/biblioteca/bib_busqueda_avanzada.html?txhCodUsuario="
	 	+srtCodEvaluador+ "&txhCodPeriodo="+srtCodPeriodo; //+ "&txhCodigo="+srtCodigo;
	}
	
	function fc_Limpiar()
	{
	}
	
	function fc_VerDomicilio(param){
		if(document.getElementById("txhSelDomicilio").value==param)
	   		 document.getElementById("txhSelDomicilio").value="0";
	    else
	  		 document.getElementById("txhSelDomicilio").value=param;
	}
	
	function fc_VerSala(param){
	  
	  if(document.getElementById("txhSelSala").value==param)
	    document.getElementById("txhSelSala").value="0";
	  else
	   document.getElementById("txhSelSala").value=param;
	   
	}
	//para el manejo de sugerencias
	function fc_TipoSugerencia(){
		opcion=document.getElementById("codSugerencia").value;		
		switch(opcion){			
			case document.getElementById("txhCodTipoComentario").value:
			case document.getElementById("txhCodTipoSugerencia").value:				
				trMail.style.display="none";
				break;
			case document.getElementById("txhCodTipoConsulta").value:
			case document.getElementById("txhCodTipoReclamo").value:
				trMail.style.display="";
				break;
		}
		//siempre que cambie el tipo se limpian los campos
		//document.getElementById("txtSugerencia").value="";				
		//document.getElementById("txtMail").value="";
	}
	function fc_EnviarSugerencia(){
		codSugerencia=document.getElementById("codSugerencia").value;
		if(codSugerencia==""){
			alert(mstrSeleccioneTipoSugerencia);
			document.getElementById("codSugerencia").focus();
			return false;
		}
		texto=document.getElementById("txtSugerencia").value;
		if(texto=="" || fc_Trim(texto)==""){
			alert(mstrIngreseSugerencia);
			document.getElementById("txtSugerencia").focus();
			return false;
		}
		switch(codSugerencia){
			case document.getElementById("txhCodTipoConsulta").value:
			case document.getElementById("txhCodTipoReclamo").value:
				mail=document.getElementById("txtMail").value;
				if(mail=="" || fc_Trim(mail)==""){
					alert(mstrValidaCampo + mstrEmail);
					document.getElementById("txtMail").focus();
					return false;
				}
				break;
		}
		if(confirm(mstrEnviarDatos)){
			document.getElementById("txhOperacion").value="ENVIAR";
			document.getElementById("frmMain").submit();
		}
	}
	
	
	function fc_Reservas(){
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	 	srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	 	estado="web";
	 		 
	 	<%	 	
		String getFlgVerifica = "" ;
		if (request.getSession().getAttribute("verificarUsuario")!=null)
			getFlgVerifica = (String)request.getSession().getAttribute("verificarUsuario");		
			
	 	if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>	 		 	
	 	<%	 	
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_RES") == 1) {
			UsuarioSeguridad usuario = (UsuarioSeguridad) (request.getSession().getAttribute("usuarioSeguridad"));
			String sedeUsuario = usuario.getSede();
			if (sedeUsuario==null) sedeUsuario="";
		 	if(getFlgVerifica.equals("1")) {
		%>		

			var sedeUsuario = "<%=sedeUsuario%>";
		
			parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_consulta_reserva_salas.html?txhCodUsuario="
	 		+srtCodEvaluador+ 
	 		"&txhCodPeriodo="+srtCodPeriodo+
	 		"&txhEstado="+estado+
	 		"&txhCodSede="+sedeUsuario+
	 		"&txhSedeUsuario="+sedeUsuario+
	 		"&flgUsuAdmin=0";
	 		
		<%
		 	//}
		} else {
		%> 
		   	alert("Ud. No tiene permiso para realizar reservas de salas");		   
		<% } 
		//}  
		} else {
		%>
			alert("La opci�n requiere el acceso al Sistema Biblioteca");			

	    <%
	    }
	 	} else {
	    %>
	    	alert("Necesita iniciar sesi�n para acceder a esta opci�n");
	    
	    <%}%>		
	}

	function fc_AccedeBaseDatos2() {
		parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_base_datos.html?";
	}
	
	function fc_ManualesAyuda(){
		parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_manuales_ayuda.html?";
	}
	
	function fc_AccedeBaseDatos() {
		<%	 	
		String getFlgVerifica1 = "" ;
		if (request.getSession().getAttribute("verificarUsuario")!=null)
			getFlgVerifica1 = (String)request.getSession().getAttribute("verificarUsuario");
							 	
	 	if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>	 		 	
	 	<%	 	
			if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_WEB") == 1) {
		 		if(getFlgVerifica1.equals("1")) {			
		%>							
					parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_base_datos.html?";	 	
		<%
				} else {
		%> 
		   			alert("Ud. No tiene permiso para visualizar esta opci�n");		   
		<% 		}  
			} else {
		%>
				alert("La opci�n requiere el acceso al Sistema Biblioteca");
	    <%
	    	}
	 	} else {
	    %>
	    	alert("Necesita iniciar sesi�n para acceder a esta opci�n");	    
	    <%}%>		
	}

	
	function fc_BibliotecaVirtual() {	
				
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;			
	 	srtCodPeriodo=document.getElementById("txhCodPeriodo").value;	
	 	
	 	<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>
	 	 	
	 	<%				
	 	if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_WEB") == 1) {
	 		
	 		UsuarioSeguridad userSeg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
	 		if (userSeg.getUsuarioSeguriBib()!=null){
	 			UsuarioSeguriBib userseguri = userSeg.getUsuarioSeguriBib();
	 			userseguri.getUsuario();
	 			userseguri.getClave();
			%>			
				var codigousuario = "<%=( (userseguri.getUsuario()==null)?"": userseguri.getUsuario())%>";
				var claveusuario = "<%=( (userseguri.getClave()==null)?"": userseguri.getClave())%>";								
			  	Fc_Popup("${ctx}/biblioteca.jsp?usuario="+codigousuario+ "&claveacceso="+claveusuario,950,600);
			  	

			<%} else {%> 
				alert("Ud. requiere Privilegios de Egresado para acceder");		   
			<%}%>  
	
		<%} else {%> 
			alert("Ud. No tiene permiso para Ingresar a la Biblioteca Virtual");		   
		<%}
		} else {%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<% }%>	
	}
		
	function fc_Novedades(){
		parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_novedades_inicio.html?a=0";
		//parent.location.href="${ctx}/biblioteca/index.html";
	}
	
	function fc_RevistaIi(){
		parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_revistaIi.html";
		//parent.iFrameDescriptores2.location.href="bib_revistaIi.htm";
	}
	
	function fc_Blog(){		
		window.open('http://ceditec-info.blogspot.com/');
	}
	
	function fc_Inicio(tipo){
		var vtipo = tipo;
		if (vtipo=="1") {			
			parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_acercade.html";
		} else if (vtipo=="2") {
			parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_mision.html";
		} else if (vtipo=="3") {
			parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_servicios.html";
		} else if (vtipo=="4") {
			parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_colecciones.html";
		} else if (vtipo=="5") {
			parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_horarioatencion.html";
		//} else if (vtipo=="6") {
			//parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_base_datos.html";											
		}		
	}
	
	
	
	function fc_LasBibliotecas(){
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	 	srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	 	
	 	parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_bibliotecas.html"
													+ "?txhCodEvaluador=" + srtCodEvaluador
													+ "&txhCodPeriodo=" + srtCodPeriodo;
	}
	
	function fc_RecursosAbiertos(){
		parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_recursos_abiertos.html";
	}
	
	function fc_EnlacesInteres(){
		srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
	 	srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	 	parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_enlaces_de_interes.html";
	 }	
	
	function fc_Sugerencias(){
		parent.iFrameDescriptores2.location.href="${ctx}/biblioteca/bib_portal_sugerencias.html";		
	}
		
</script>

<!-- 	<script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script> -->

<!-- 	<script src="https://apis.google.com/js/client:platform.js?onload=initGAPI" async defer></script> -->
<!-- 	<script src="https://apis.google.com/js/platform.js" async defer></script> -->
	 
	<script src="https://apis.google.com/js/client:platform.js?onload=initGAPI" async defer></script>
	 
	<script type="text/javascript">
			function initGAPI() {
				gapi.load('auth2', function(){
					gapi.auth2.init({						
						client_id: '903982786098-6p1dmpfhhas2a5frj49r7r3u23tdc2nu.apps.googleusercontent.com',									
					}).then(function(){
						
						var auth2 = gapi.auth2.getAuthInstance();	// Recuperamos la instancia iniciada con gapi.auth2.init()
						
						$('#g-signin-custom').click(function() {
							
							auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then(function(authResult) {
								
								//console.log(authResult);
								
								if (authResult['code']) {
									
									$.preloader.start({modal:true});
									
									//google_connect.html
									//'${ctx}/biblioteca/bib_busqueda_inicio.html'
									//'${ctx}/GoogleConnect'
									$.post('${ctx}/GoogleConnect', {code: authResult['code'], operacion:'GOOGLE_CONNECT'}, function(data){
										
// 										console.log(data);
										if(data.isValid){
// 											alert('Valid:');
// 											go('${ctx}/biblioteca/bib_busqueda_inicio.html?txhCodUsuario=' + data.codusuario);
											top.location.href="${ctx}/biblioteca/index.html";
										}else{
// 											alert('Invalid:'+ data.operacion);
// 											alert('invalid');
// 											showError(data.errors.join('<br/>'));
											alert(data.errors);
											$.preloader.stop();
											//auth2.signOut();
										}										
										
									}, 'json').fail(function(data) {
										
		    							$.preloader.stop();
		    						}).always(function(){
		    							$.preloader.stop();
		    							
		    							//console.log('Do it Always');
		    						});
									
								}
								
							});
							
						});
												
						auth2.signOut();
						
					});
					
				});
			}
		</script>	
		

</head>
<body>
	<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/biblioteca/bib_busqueda_inicio.html">
		<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="selSala" id="txhSelSala"/>
		<form:hidden path="selDomicilio" id="txhSelDomicilio"/>		
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="log" id="log"/>
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codTipoConsulta" id="txhCodTipoConsulta"/>
		<form:hidden path="codTipoComentario" id="txhCodTipoComentario"/>
		<form:hidden path="codTipoSugerencia" id="txhCodTipoSugerencia"/>
		<form:hidden path="codTipoReclamo" id="txhCodTipoReclamo"/>

		<table class="" border="0" width="70%">
			<tr>
				<td>
				
			<script type="text/javascript">
			<!--
			stm_bm(["menu3b81",850,"img","${ctx}/images/biblioteca/portal/blank.gif",0,"","",0,0,250,200,1000,1,0,0,"","",0,0,1,2,"hand","hand",""],this);
			
			stm_bp("p0",[1,4,0,0,0,2,10,7,100,"",-2,"",-2,50,0,0,"#999999","opaque","${ctx}/images/biblioteca/portal/line.gif",3,0,0,"#000000","${ctx}/images/biblioteca/portal/up.gif",23,210,0,"opaque","",3,"",-1,-1,0,"opaque","${ctx}/images/biblioteca/portal/rightline.gif",3,"${ctx}/images/biblioteca/portal/down.gif",22,210,0,"opaque","",3,"",-1,-1,0,"opaque","${ctx}/images/biblioteca/portal/leftline.gif",3,"${ctx}/images/biblioteca/portal/leftup.gif","${ctx}/images/biblioteca/portal/rightup.gif","${ctx}/images/biblioteca/portal/rightdown.gif","${ctx}/images/biblioteca/portal/leftdown.gif",0,0,0,0,0,0,0,0]);
			stm_ai("p0i0",[0,"Acerca de Ceditec","","",-1,-1,0,"javascript:fc_Inicio('1');","","","","","",0,0,0,"","",0,0,0,1,1,"#3399CC",0,"#0099CC",0,"${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg","${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg",3,3,2,3,"#3399ff #3399ff #3399ff #3399ff","#66ccff #66ccff #66ccff #66ccff","#000000","#000000","bold 10pt Verdana","bold 10pt Verdana",0,0],206,0);
			stm_ai("p0i1",[6,1,"#999999","",-1,-1,0]);
			stm_aix("p0i2","p0i0",[0,"Misi�n, Visi�n y Funciones","","",-1,-1,0,"javascript:fc_Inicio('2');","","","","${ctx}/images/biblioteca/portal/dian.gif","${ctx}/images/biblioteca/portal/dian.gif",10,7,0,"","",0,0,0,0,1,"#FFFFF7",1,"#C1E7F4",0,"","",3,3,0,0,"#C8E5EE","#000000","#000000","#000000","10pt Verdana","10pt Verdana"],206,0);
			stm_aix("p0i3","p0i2",[0,"Servicios","","",-1,-1,0,"javascript:fc_Inicio('3');"],206,0);
			stm_aix("p0i4","p0i2",[0,"Colecciones","","",-1,-1,0,"javascript:fc_Inicio('4');"],206,0);
			stm_aix("p0i5","p0i2",[0,"Horario de Atenci�n","","",-1,-1,0,"javascript:fc_Inicio('5');"],206,0);
			stm_aix("p0i6","p0i1",[]);
			stm_ai("p0i7",[0,"Reservas","","",-1,-1,0,"javascript:fc_Reservas();","","","","","",0,0,0,"","",0,0,0,1,1,"#3399CC",0,"#3399CC",0,"${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg","${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg",3,3,2,3,"#3399ff #3399ff #3399ff #3399ff","#66ccff #66ccff #66ccff #66ccff","#000000","#000000","bold 10pt Verdana","bold 10pt Verdana",0,1],206,0);
			stm_aix("p0i8","p0i1",[]);
			stm_aix("p0i9","p0i7",[0,"Recursos de Ayuda","","",-1,-1,0,"javascript:fc_ManualesAyuda();"],206,0);
			stm_aix("p0i10","p0i1",[]);
			stm_aix("p0i11","p0i7",[0,"Revista I+i","","",-1,-1,0,"javascript:fc_RevistaIi();"],206,0);
			stm_aix("p0i12","p0i1",[]);
			//stm_aix("p0i13","p0i0",[0,"Biblioteca Virtual","","",-1,-1,0,"javascript:fc_BibliotecaVirtual();","","","","","",0,0,0,"","",0,0,0,1,1,"#3399CC",0,"#3399CC"],206,0);
			stm_aix("p0i13","p0i0",[0,"Biblioteca Virtual","","",-1,-1,0,"javascript:fc_AccedeBaseDatos2();","","","","","",0,0,0,"","",0,0,0,1,1,"#3399CC",0,"#3399CC"],206,0);
			stm_aix("p0i14","p0i1",[]);
			stm_aix("p0i15","p0i2",[0,"Otras Bibliotecas","","",-1,-1,0,"javascript:fc_LasBibliotecas();"],206,0);
			stm_aix("p0i15","p0i2",[0,"Recursos de Acceso Abierto","","",-1,-1,0,"javascript:fc_RecursosAbiertos();"],206,0);
			//stm_aix("p0i26","p0i2",[0,"Bases de Datos","","",-1,-1,0,"javascript:fc_AccedeBaseDatos();"],206,0);			
			
			stm_aix("p0i20","p0i1",[]);			
			stm_aix("p0i21","p0i7",[0,"Enlaces de Inter�s","","",-1,-1,0,"javascript:fc_EnlacesInteres();"],206,0);
			stm_aix("p0i22","p0i1",[]);
			stm_aix("p0i23","p0i7",[0,"Novedades","","",-1,-1,0,"javascript:fc_Novedades();"],206,0);
			stm_aix("p0i24","p0i1",[]);
			//stm_aix("p0i24","p0i7",[0,"Blog","","",-1,-1,0,"javascript:fc_Blog();"],206,0);
			//stm_aix("p0i25","p0i1",[]);
			stm_aix("p0i26","p026",[0,"Sugerencias","","",-1,-1,0,"javascript:fc_Sugerencias();","","","","","",0,0,0,"","",0,0,0,1,1,"#3399CC",0,"#3399CC",0,"${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg","${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg",3,3,2,3,"#3399ff #3399ff #3399ff #3399ff","#66ccff #66ccff #66ccff #66ccff","#000000","#000000","bold 10pt Verdana","bold 10pt Verdana",0,1],206,0);
			stm_ep();
			stm_em();
			//-->
			</script>
			
			<script type="text/javascript">
			<!--
			stm_bm(["menu3b81",850,"img","${ctx}/images/biblioteca/portal/blank.gif",0,"","",0,0,250,200,1000,1,0,0,"","",0,0,1,2,"hand","hand",""],this);
			stm_bp("p0",[1,4,0,0,0,2,10,0,100,"",-2,"",-2,50,0,0,"#999999","opaque","${ctx}/images/biblioteca/portal/line.gif",3,0,0,"#000000","${ctx}/images/biblioteca/portal/up.gif",23,210,0,"transparent","",3,"",-1,-1,0,"transparent","${ctx}/images/biblioteca/portal/rightline.gif",3,"${ctx}/images/biblioteca/portal/down.gif",22,210,0,"transparent","",3,"",-1,-1,0,"transparent","${ctx}/images/biblioteca/portal/leftline.gif",3,"${ctx}/images/biblioteca/portal/leftup.gif","${ctx}/images/biblioteca/portal/rightup.gif","${ctx}/images/biblioteca/portal/rightdown.gif","${ctx}/images/biblioteca/portal/leftdown.gif",19,23,18,23,18,22,19,22]);
			stm_ai("p0i0",[0,"Login","","",-1,-1,0,"","","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg","${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg",3,3,2,3,"#3399ff #3399ff #3399ff #3399ff","#66ccff #66ccff #66ccff #66ccff","#000000","#000000","bold 10pt Verdana","bold 10pt Verdana",0,0],206,0);
			stm_ai("p0i1",[6,1,"#999999","",-1,-1,0]);
			stm_ep();
			stm_em();
			//-->			
			</script>	
			
				<table border="0" align="center">
					<tr class="texto">
						<td>Usuario :</td>
						<td>
							<form:input id="txtUsuario" path="usuario" cssClass="cajatexto" size="15" maxlength="25"/>
						</td>
					</tr>
					<tr class="texto">
						<td>Contrase�a :</td>
						<td>
							<form:password id="txtClave" path="clave" cssClass="cajatexto" onkeypress="javascript:fc_Enter(event);" size="15" maxlength="25"/>
						</td>
					</tr>
					<tr >
						<td align="center" colspan="2">
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgIngresar','','${ctx}/images/biblioteca/portal/ingresarbiblio.jpg',1)">
						<br>
						<img src="${ctx}/images/biblioteca/portal/ingresarbiblio.jpg" alt="Ingresar" onclick="javascript:fc_Ingresar();" id="imgIngresar" style="cursor:pointer"></a>
						
						<br/>
						
												
						<div class="SocialSignIn" style="margin-top: 16px; text-align: center;">
							<div id="g-signin-custom" class="social-signin">
							    <span class="icon"></span>
							    <span class="buttonText">Ingresar con Google+</span>
							</div>
						</div>
						
						
						</td>
					</tr>
					<tr><td height="10px"></td></tr>
					<tr>
						<td align="center" colspan="2">
							&nbsp;
						</td>
					</tr>								
				</table>
				</td>			
			</tr>
																							
				</table>
	</form:form>
	
<script type="text/javascript">
	
	strMsg = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	if(strMsg!="" && strMsg=="OK") top.location.href="${ctx}/biblioteca/index.html";
	else if ( strMsg == "0" || strMsg == "-5"){alert('No tiene permisos para acceder.');}
	else if ( strMsg == "-1" || strMsg == "-5"){alert('Usuario o clave incorrecta');}
	else if ( strMsg == "-2"){alert('Usuario no est� activo.');}
	else if ( strMsg == "-3"){alert('Usuario est� dentro del periodo de bloqueo.');}
	else if ( strMsg == "-4"){alert('Usuario alcanz� el n�mero m�ximo de intentos fallidos permitidos.');}				
	else if ( strMsg == "-6"){		
		window.parent.parent.parent.location.href="${ctx}/biblioteca/cambiaClaveBibliotecaPortal.html?msg=1";
	}	
	else if ( strMsg == "-7"){alert('Error al verificar usuario.');}
	else if ( strMsg == "-8"){alert('Usuario no registrado en Ceditec.');}
	else if ( strMsg == "-9"){alert('Su cuenta de correo no pertenece a la instituci�n');}
	else if ( strMsg == "NA"){alert('Lo sentimos pero no tiene opciones disponibles en este sistema');}
	
</script>	

</body>

</html>

