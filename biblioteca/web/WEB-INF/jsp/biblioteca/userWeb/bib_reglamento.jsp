<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}
	function fc_TipoMaterial(){
	
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_reglamento.html">
<table class="tablaborde" style="width:100%" cellSpacing="2" cellPadding="2" border="0" bordercolor='gray'> 
			<tr>
				<td colspan='2' class="Tab_vert" style='cursor:hand;'>REGLAMENTO</td>					
			</tr>
			
</table>
	<BR>
<table class="tabla2" style="width:100%" cellSpacing="2" cellPadding="2" border="0" bordercolor='gray'> 			
			
			<table cellpadding="0" cellspacing="0" ID="Table1" width="70%" border="1px"  class="" style="margin-left:3px">
			<tr>
			 	<td align="left">			
				 	<display:table name="sessionScope.listaReglamento" cellpadding="0" cellspacing="0"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  
						style="border: 1px solid #048BBA;width:98%;">
					    <display:column property="textReglamento"   
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:100%"/>
													
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
							
					</display:table>
				</td>
			</tr>
		   </table>
		</table>
</form:form>
</body>
</html>