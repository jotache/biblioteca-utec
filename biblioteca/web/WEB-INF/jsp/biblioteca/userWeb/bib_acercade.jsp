<%@ include file="/taglibs.jsp"%>
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}	
</script>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Acerca De Ceditec
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 500px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">
			 		<br>			
				 	<p align="justify">	
				 		<span class="TituloBiblio">			 	
						CEDITEC es el Centro de Documentación e Información.
						</span>
						<br><br>
						El fondo bibliográfico se inició con la donación de la GTZ, 
						hasta alcanzar en la actualidad aproximadamente 17 000 ejemplares de libros, además de revistas, periódicos, manuales, 
						catálogos, material audiovisual y proyectos, todo este material se encuentra distribuido en sus tres sedes: Lima, Arequipa y Trujillo				      
				    </p>
				    
				    <p align="justify">
				    	Nuestro Centro de Documentación se encuentra al servicio de la comunidad de alumnos de los distintos programas académicos, 
				    	quienes acceden a la información bajo un reglamento establecido por el Centro. Recibe decenas de estudiantes que hacen uso de 
				    	los servicios y recursos que ofrecemos.
				    </p>  
				      
				      				      
				      <p align="right"><br>
				          <span class="TituloBiblio">Reglamento</span><br>				          				         				          
				          <a href="/file/sga/documentos/ReglamentoBiblioteca.pdf" target="_blank">Descargar Archivo PDF</a></p>
				</td>
			</tr>
	    </table>
		</div>
		

</body>
</html>