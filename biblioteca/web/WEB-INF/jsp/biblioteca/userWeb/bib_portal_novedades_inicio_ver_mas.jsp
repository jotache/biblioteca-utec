<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}
	function fc_Regresar(){		
		location.href="${ctx}/biblioteca/bib_portal_novedades_inicio.html"
	}
		
</script>
</head>
<body>
	<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_portal_novedades_inicio_ver_mas.html">
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEvaluador" id="txhCodEvaluador"/>

		<table cellpadding="0" cellspacing="0" border="0" width="98%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<font style="">
					 	Novedades
				 	</font>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<table class="tablagrilla" cellSpacing="2" cellPadding="1" style="margin-left:9px;margin-top:5px; margin-right:5px;border:1px solid #048BBA;width: 97%"> 			
			<tr class="tabla2">
				<td width="2%" align="center"></td>
				<td colspan="2"><c:out value="${control.descripcionGrupo}"/></td>
			</tr>
			<tr>
				<td width="1%" align="center"><li></li></td>
				<td><c:out value="${control.tema}"/></td>				
				<td class="texto_vermas" align="center">
					<a onclick="fc_Regresar();">
						Regresar
					</a>					
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><textarea style="width: 99%" rows="20" class="cajatexto_1" readonly><c:out value="${control.descripcionLarga}"/>
					</textarea>
				</td>
				<td rowspan="3" width="20%" valign="top" align="center">
					<c:if test="${control.imagen!=null}">
					     <img src="<c:out value="${control.archivo}"/>" style="cursor: pointer;" id="imgFoto" width="100px" height="110px" border="1"  onclick="javascript:Fc_Popup_AutoResizable2('<c:out value="${control.archivo}"/>');" >
					</c:if>
				</td>
			</tr>
			<c:if test="${control.url!=null}">
				<tr height="4px">
					<td>&nbsp;</td>
					<td colspan="2">
						Url : <a class="enlace2" href="<c:out value="${control.url}"/>" target="_blank"><c:out value="${control.url}"/></a>
					</td>
				</tr>
			</c:if>
			<c:if test="${control.nombreFile!=null}">
			<tr height="4px">
				<td>&nbsp;</td>
				<td colspan="2">
					Archivo : <a class="enlacelogin" href="<c:out value="${control.rutaArchivos}"/><c:out value="${control.nombreFile}"/>" target="_blank">
									<img src="${ctx}/images/<c:out value="${control.extDocNov}"/>" style="cursor:pointer;" alt="<c:out value="${control.nombreOriginal}"/>" border="0" width="21px" height="22px">&nbsp;
								  	<span style="letter-spacing: 0pt">
								  		<font face="verdana" size="1" COLOR="#2B4963"><b><c:out value="${control.nombreOriginal}"/></b>
								  		</font>
								  	</span>
							  </a>
					
				</td>
			</tr>
			</c:if>
		</table>
</form:form>
</body>
</html>