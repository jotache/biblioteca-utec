<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<script language=javascript>
	function onLoad(){
	} 
	
	function fc_Cambia(param){
	}
	function fc_TipoMaterial(){
	
	}
	function fc_BusquedaBy(){
	
	}
	
	function fc_detalleMaterial(codUnico){
		var url ="${ctx}/biblioteca/detalle_material.html?txhCodigoUnico="+fc_Trim(codUnico)+"&flgView=1"+
				 "&strUrl="+"<%=(( request.getAttribute("strUrl")==null)?"": request.getAttribute("strUrl"))%>";
		Fc_Popup(url,600,250,"renzo");
	}
	
	function fc_detalleAdjunto(codUnico){
		var url ="${ctx}/biblioteca/bib_material_adjuntos.html?txhCodigoUnico="+fc_Trim(codUnico);
		Fc_Popup(url,600,250,"renzo");
	}
	

	function fc_Reservar(srtCodigoUnico, srtFechaBD, index){
		
		srtCodPeriodo= document.getElementById("txhCodPeriodo").value;
		srtCodUsuario=document.getElementById("txhCodEvaluador").value;
		
		if (document.getElementById("txtDisponible"+index)!=null){
			if (document.getElementById("txtDisponible"+index).value == "0"){
				alert('El material seleccionado no tiene ejemplares disponibles.');
				return false;
			}
		}
		
		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_RES") == 1) 
		{
		%>
			 Fc_Popup("${ctx}/biblioteca/bib_reserva_material_bibliografico.html?txhCodigoUnico="+
			 	srtCodigoUnico + "&txhFechaBD=" + srtFechaBD + "&txhCodPeriodo=" + srtCodPeriodo + 
			 	"&txhCodUsuario=" + srtCodUsuario,440,240,"angela");
			 
		<%}else{%>
				alert('La opci�n requiere el acceso al Sistema Biblioteca');
				window.opener.fc_IngresarSistema();
				window.close();
		<%}%>
	}
	
	function fc_seleccion(strCodNIvel1){
		strCodSel = document.getElementById("txhCodComponentesSeleccionados").value;
		flag=false;
		if (strCodSel!=''){	
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";
			for (i=0;i<=ArrCodSel.length-2;i++)
			{	if ([i] == strCodNIvel1)flag = true 
				else strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
		}
		if (!flag){
			strCodSel = strCodSel + strCodNIvel1 + '|';
		}
		document.getElementById("txhCodComponentesSeleccionados").value = strCodSel;
	}
	
	function fc_Buscar(){
	  document.getElementById("txhOperacion").value = "BUSCAR";
	  document.getElementById("frmMain").submit();
	}
	
	function fc_Limpiar(){
		document.getElementById("codTipoMaterial").value="-1";
		document.getElementById("codBusquedaBy").value="-1";
		document.getElementById("codOrdenarBy").value="-1";
		document.getElementById("txtTexto").value="";
		document.getElementById("txhSelSala").value="0";
		document.getElementById("txhSelDomicilio").value="0";
		document.getElementById("Checkbox2").checked=false;
		document.getElementById("Checkbox1").checked=false;
	}
	
	function fc_VerSala(param){
		if(document.getElementById("txhSelSala").value==param)
			document.getElementById("txhSelSala").value="0";
		else
			document.getElementById("txhSelSala").value=param;
	}
	
	function fc_VerDomicilio(param){
		if(document.getElementById("txhSelDomicilio").value==param)
		  	document.getElementById("txhSelDomicilio").value="0";
		else
		 	document.getElementById("txhSelDomicilio").value=param;
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_busqueda_simple.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="codComponentesSeleccionados" id="txhCodComponentesSeleccionados" />
	<form:hidden path="codEvaluador" id="txhCodEvaluador" />
	<form:hidden path="codPeriodo" id="txhCodPeriodo" />	
	
	<form:hidden path="codTipoMaterial" id="txhCodTipoMaterial" />
	<form:hidden path="txtTexto" id="txhTxtTexto" />
	<form:hidden path="codBusquedaBy" id="txhCodBusquedaBy" />
	<form:hidden path="codOrdenarBy" id="txhCodOrdenarBy" />
	<form:hidden path="selSala" id="txhSelSala" />
	<form:hidden path="selDomicilio" id="txhSelDomicilio" />	

	<table cellpadding="0" cellspacing="0" border="0" width="99%"
		bordercolor="blue" style="margin-left: 3px; margin-top: 4px;">
		<tr>
			<td align="left">
				<img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
			<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg"
				width="100%" class="opc_combo"><font style=""> RESULTADO
			DE LA B�SQUEDA </font></td>
			<td align="right"><img
				src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="99%" border="0"
		bordercolor="red"
		style="margin-left: 4px; margin-top: 5px; margin-right: 5px;">
		<tr>
			<td align="left">
			<div style="overflow: auto; height: 530px; width: 100%"><display:table
				name="sessionScope.listaBusquedaSimple" cellpadding="0"
				cellspacing="0"
				decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"
				pagesize="3" style="width:97.5%;" requestURI="">

				<display:column property="textBusquedaSimple" headerClass="grilla"
					class="tablagrilla" style="text-align:center; width:97%" />

				<display:setProperty name="basic.empty.showtable" value="true" />
				<display:setProperty name="basic.show.header" value="false" />
				<display:setProperty name="basic.msg.empty_list_row"
					value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>" />
				<display:setProperty name="paging.banner.placement" value="bottom" />
				<display:setProperty name="paging.banner.item_name"
					value="<span >Registro</span>" />
				<display:setProperty name="paging.banner.items_name"
					value="<span>Registros</span>" />
				<display:setProperty name="paging.banner.no_items_found"
					value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
				<display:setProperty name="paging.banner.one_item_found"
					value="<span class='cajatexto-login'>Un registro encontrado </span>" />
				<display:setProperty name="paging.banner.all_items_found"
					value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
				<display:setProperty name="paging.banner.some_items_found"
					value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
				<display:setProperty name="paging.banner.full"
					value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.first"
					value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
				<display:setProperty name="paging.banner.last"
					value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />
				<display:setProperty name="paging.banner.onepage"
					value="<span class='cajatexto-login'>{0}</span>" />

			</display:table></div>
			</td>
		</tr>

	</table>
	<table cellspacing="0" cellpadding="0" border=0 align="center"
		style="margin-top: 15px;">
		<tr>
			<td align="center"><a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('imgPrest','','${ctx}/images/botones/cerrar2.jpg',1)">
			<img src="${ctx}/images/botones/cerrar1.jpg" align="absmiddle"
				alt="Cerrar" id="imgPrest" style="CURSOR: pointer"
				onclick="window.close();"></a></td>
		</tr>
	</table>
</form:form>
</body>
</html>