<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}	
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Revista I+i: Investigaci&oacute;n aplicada e innovaci&oacute;n
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 455px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">      <table width="710px">
                  <tr>
                    <td width="100" rowspan="2" valign="top"><img src="${ctx}/images/biblioteca/portal/No3.jpg" width="100" height="143"></td>
                    <td width="10">&nbsp;</td>
                    <td align="right" valign="top">&nbsp;</td>
                    <td width="541" valign="top"><table width="500" border="0">
                      <tr>
                        <td align="center" class="TituloBiblio">2013</td>
                        <td align="center" class="TituloBiblio">2014</td>
                        <td align="center" class="TituloBiblio">2015</td>
                      </tr>
                                            
                      <tr>
                        <td align="center" >
                        <a href="http://issuu.com/ceditec/docs/revista_investigaci__n_aplicada_e_i" 
                        target="_blank">Volumen 7</a></td>
                        <td align="center" >
                        	<a href="http://issuu.com/ceditec/docs/revista_i_i_2014_compendio" target="_blank">
                        	Volumen 8</a></td>
                        <td align="center">
                        	<a href="http://www.tecsup.edu.pe/consultoria-y-asistencia-tecnica/uploads/Revista/revista-i-i-2015-vol-9.pdf" target="_blank">
                        	Volumen 9</a></td>
                      </tr>
                      <tr>
                        <td align="center" colspan="3" height="28px">                        
                        </td>                        
                      </tr>
                      
                      <tr>
                        <td align="center" class="TituloBiblio">2010</td>
                        <td align="center" class="TituloBiblio">2011</td>
                        <td align="center" class="TituloBiblio">2012</td>
                      </tr>
                                            
                      <tr>
                        <td align="center" >
                        <a href="http://issuu.com/ceditec/docs/compendio_2010_i?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" 
                        target="_blank">Volumen 4 No 1</a></td>
                        <td align="center" >
                        <a href="http://issuu.com/ceditec/docs/compendio_2011a_completo_final?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true"                         
                        target="_blank">Volumen 5 No 1</a>
                        </td>
                        <td align="center">
                        <a href="http://issuu.com/ceditec/docs/investgacion_aplicada_tarea_web?e=1230778/2616675" target="_blank">Volumen 6</a></td>
                      </tr>
                      <tr>
                        <td align="center" ><a href="http://issuu.com/ceditec/docs/archivo_tecsup_final_16-01-11?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" target="_blank">Volumen 4 No 2</a></td>
                        <td align="center" >
                        	                        
                        	<a href="http://issuu.com/ceditec/docs/compendio_tecsup_invest._aplicada_5.2?mode=window&backgroundColor=%23222222"                         
                        		target="_blank">Volumen 5 No 2</a>
                            
                        </td>
                        <td align="center" class="TituloBiblio">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center" colspan="3" height="28px"></td>                        
                      </tr>
                      <tr>
                        <td align="center" class="TituloBiblio">2007</td>
                        <td align="center" class="TituloBiblio">2008</td>
                        <td align="center" class="TituloBiblio">2009</td>
                      </tr>
                      <tr >
                        <td align="center"><a href="http://issuu.com/ceditec/docs/compendiotecsup?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" target="_blank">Volumen 1 No 1 </a></td>
                        <td align="center"><a href="http://issuu.com/ceditec/docs/compendio_v2n1?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" target="_blank">Volumen 2 No 1</a></td>
                        <td align="center"><a href="http://issuu.com/ceditec/docs/compendiot3_sem1_2009_interio_tecsup?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" target="_blank">Volumen 3 No 1</a></td>
                      </tr>
                      <tr>
                        <td align="center"><a href="http://issuu.com/ceditec/docs/volumen_2?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" target="_blank">Volumen 1 No 2 </a></td>
                        <td align="center"><a href="http://issuu.com/ceditec/docs/compendiot2_nov_2008_completo?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" target="_blank">Volumen 2 No 2</a></td>
                        <td align="center"><a href="http://issuu.com/ceditec/docs/investigacion_aplicada?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Flight%2Flayout.xml&showFlipBtn=true" target="_blank">Volumen 3 No 2</a></td>
                      </tr>
                    </table></td>
                    <td width="37">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td align="right" valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
                
                <div style="text-align:center; margin-top:100px">
                <table style="width:510px; margin-left:auto; margin-right:auto; border-color: #A8A8A8;" cellpadding=6 cellspacing=0 border="1">
                	<tr>
                		<td colspan="2" class="TituloBiblio">Convocatoria abierta para publicar en la Revista I+i</td>
                	</tr>
                	<tr>
                		<td><a href="http://www.tecsup.edu.pe/home/wp-content/uploads/2015/02/instrucciones-autores.pdf" target="_blank">
                			Instrucciones autores</a></td>
                		<td><a href="http://www.tecsup.edu.pe/home/wp-content/uploads/2015/02/plantilla-articulos.pdf" target="_blank">                		
                		Formato para la presentaci&oacute;n de art&iacute;culos</a></td>
                	</tr>
                </table>
                </div>
                
                </td>
			</tr>
	    </table>
		</div>
		

</body>
</html>