<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}	
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Colecciones
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 455px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" bordercolor="blue" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<p align="justify">
				 	<br>			        
			        <span class="TituloBiblio">Libros: </span>
			        	Contamos con libros de todas las especialidades (Cursos generales, Electrotecnia, Electr&oacute;nica, Mec&aacute;nica, Inform&aacute;tica , Qu&iacute;mica y Metalurgia).<br>
			        <strong class="TituloBiblio"><br>
			        	Manuales y Cat&aacute;logos: 
			        </strong>
			        Amplia colecci&oacute;n de todas las especialidades.<br>
			        <strong><br>
			        	<span class="TituloBiblio">Proyectos:</span>
			        </strong>Realizadas por alumnos, disponible en nuestra Biblioteca Virtual.<br>
			        <strong class="TituloBiblio"><br>
			        	<span class="TituloBiblio">Hemeroteca:</span>
			        </strong>Ofrecemos una variedad de revistas especializadas que abarcan diferentes &aacute;reas de estudio y peri&oacute;dicos para lectura en sala.<br>
			        <strong><br>
			        </strong><span class="TituloBiblio"><strong>Multimedia:</strong></span> Contamos con una colecci&oacute;n de software y textos en discos compactos.<br>
			        <strong><br>
			        <span class="TituloBiblio">Mediateca:</span> </strong>Colecci&oacute;n variada de documentales, videos de clases, etc.<br>
			      </p>
				</td>
			</tr>
	    </table>
		</div>		

</body>
</html>