<%@ include file="/taglibs.jsp"%>

<head>
<script language="JavaScript">
	function onLoad(){
	}
</script>
</head>

<body>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" 
		style="margin-left:5px; margin-top:3px; display:;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="763px" class="opc_combo"><font style="">Documentos Adjuntos</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>

	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:98%; margin-left:5px; margin-right:5px" border="0" 
		cellspacing="4" cellpadding="2" class=grilla height="50px" bordercolor="red" align="center">
		<tr>			
			<td valign="top">
				<div style="overflow: auto; height: 140px; width:100%;">
				<table cellpadding="0" cellspacing="1" style="border: 1px solid #048BBA;width:100%; table-layout: fixed;">
					<tr class="grilla">				
						<td>Titulo</td>					
						<td width="30">Ver</td>
					</tr>
				  	<!-- BIBLIOTECA -->
					<c:forEach var="objCast3" items="${model.lstDocumentosRelacionados}" varStatus="loop3" >						
						<tr class="tablagrilla">											
							<td style="text-align: left; text-indent: 10px;"><c:out value="${loop3.count}"></c:out>. <c:out value="${objCast3.titulo}" /></td>
							<td align="center">								
								<a href="<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%><c:out value="${objCast3.archivo}" />" target="_blank">								
								<img  src="${ctx}/images/iconos/buscar1.jpg" border="0" style="cursor:pointer"	id="imgEliminarPerfil">
								</a>
							</td>
						</tr>
					</c:forEach>
					
					<c:if test="${fn:length(model.lstDocumentosRelacionados)==0}">
						<tr class="tablagrilla">											
							<td align="center" colspan="2" height="16">
								<b>No existe adjuntos.</b>
							</td>
						</tr>
					</c:if>
					
					<!-- /BIBLIOTECA -->
				</table>
				</div>
			</td>
		</tr>		
	</table>	
	
	
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgregresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgregresar" onclick="javascript:history.back();" style="cursor:pointer;"></a>
				
			</td>
		</tr>
	</table>
</body>