<%@ include file="/taglibs.jsp"%>
<head>
<script src="${ctx}/scripts/js/funciones_bib-textos.js" type="text/JavaScript"></script>
<script type="text/javascript">
	function onLoad(){
	} 

	function fc_VerDomicilio(param){
		if(document.getElementById("txhSelDomicilio").value==param)
	   		 document.getElementById("txhSelDomicilio").value="0";
	    else
	  		 document.getElementById("txhSelDomicilio").value=param;
	}
	
	function fc_BusquedaSimple(){		
		srtTexto1=document.getElementById("txtTexto").value;
		
		if(fc_Trim(srtTexto1)==""){
			alert('Ingrese Criterio de b�squeda v�lido');
			document.getElementById("txtTexto").focus();
			return 0;
		}		
		else 	
		if( fc_Trim(srtTexto1).length<3 ){
			alert('Ingrese al menos 3 caracteres');
			document.getElementById("txtTexto").focus();
			return 0;
		}
		srtCodUsuario=document.getElementById("txhCodUsuario").value;
		srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
		srtCodTipoMaterial=document.getElementById("codTipoMaterial").value;
		srtTexto=document.getElementById("txtTexto").value;
		srtCodBuscarBy=document.getElementById("codBuscarBy").value;
		srtCodOrdenarBy=document.getElementById("codOrdenarBy").value;
		srtSelSala=document.getElementById("txhSelSala").value;
		srtSelDomicilio=document.getElementById("txhSelDomicilio").value;	
	    strSelSede=document.getElementById("codSede").value;
		if(srtCodTipoMaterial=="-1")
		   srtCodTipoMaterial="";
		if(srtCodBuscarBy=="-1")
		   srtCodBuscarBy="";		
		strUrl = "txhCodUsuario=" +srtCodUsuario+ 
				   "|txhCodPeriodo="+srtCodPeriodo+ 
				   "|txhCodTipoMaterial="+srtCodTipoMaterial+
				   "|txhTexto="+srtTexto+ 
				   "|txhCodBuscarBy="+srtCodBuscarBy+ 
				   "|txhCodOrdenarBy="+srtCodOrdenarBy+
				   "|txhSelSala="+srtSelSala+ 
				   "|txhSelDomicilio="+srtSelDomicilio+
				   "|txhSelSede="+strSelSede;		
		Fc_Popup("${ctx}/biblioteca/bib_busqueda_simple.html?txhCodUsuario="+srtCodUsuario+
			"&txhCodPeriodo="+srtCodPeriodo+
			"&txhCodTipoMaterial="+srtCodTipoMaterial+
			"&txhTexto="+srtTexto+
			"&txhCodBuscarBy="+srtCodBuscarBy+
			"&txhCodOrdenarBy="+srtCodOrdenarBy+
			"&txhSelSala="+srtSelSala+
			"&txhSelDomicilio="+srtSelDomicilio+
			"&txhSelSede="+strSelSede+
			"&strUrl="+strUrl,950,620,"renzo");
	}
	function fc_TipoMaterial(param){}
	
	function fc_Limpiar(){
	
	  document.getElementById("codTipoMaterial").value="-1";
	  document.getElementById("codBuscarBy").value="-1";
	  document.getElementById("codOrdenarBy").value="0001";
	  document.getElementById("txtTexto").value="";
	  document.getElementById("checkDomicilio").checked=false;
	  
	  document.getElementById("checkSala").checked=false; 
	}
	
	function fc_VerSala(param){
	  
	  if(document.getElementById("txhSelSala").value==param)
	    document.getElementById("txhSelSala").value="0";
	  else
	   document.getElementById("txhSelSala").value=param;
	   
	}
	function fc_Cambia(param){
	
	}
	
	function fc_BusquedaAvanzada(){
	
	  srtCodEvaluador=document.getElementById("txhCodUsuario").value;
	  srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
	  	
	  var ruta = "${ctx}/biblioteca/bib_busqueda_avanzada.html?txhCodUsuario="
			  +srtCodEvaluador+ "&txhCodPeriodo="+srtCodPeriodo+ "&txhSelCodUsuario="+srtCodEvaluador;
			  
	  parent.iFrameDescriptores.iFrameDescriptores2.location.href=ruta;	  	    
	}
	
	function fc_IngresarSistema(){
		parent.fc_IngresarSistema();
	}
	
</script>

</head>

<body>

<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/userWeb/bandejaBusquedaSimple.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="selSala" id="txhSelSala" />
	<form:hidden path="selDomicilio" id="txhSelDomicilio" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codPeriodo" id="txhCodPeriodo" />

	<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" >
		<tr>
			<td valign="top" style="width:100%;" align="left">
				<table cellpadding="0" cellspacing="0" style="width:100%; height: 151px;"
					background="${ctx}/images/biblioteca/portal/fondoBusqSimple.jpg"
					bordercolor="green" border="0">
					<tr>
						<td class="TextoSimple">Tipo Material</td>
						<td colspan="2"><form:select path="codTipoMaterial" id="codTipoMaterial"
							cssClass="cajatexto" cssStyle="width:180px"
							onchange="javascript:fc_TipoMaterial();">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.codListaTipoMaterial!=null}">
								<form:options itemValue="codTipoTablaDetalle"
									itemLabel="descripcion" items="${control.codListaTipoMaterial}" />
							</c:if>
						</form:select></td>
					</tr>
					<tr>
						<td class="TextoSimple">Texto</td>
						<td colspan="2"><form:input path="txtTexto" id="txtTexto"
							onkeypress="fc_ValidaNombreAutorOnkeyPress();"
							onblur="fc_ValidaNumerosAndLetrasFinalTodo(this,'Texto');"
							cssClass="cajatexto_o" size="30" maxlength="75" /></td>
					</tr>
					<tr>
						<td class="TextoSimple">Buscar por</td>
						<td colspan="2"><form:select path="codBuscarBy" id="codBuscarBy"
							cssClass="cajatexto" cssStyle="width:180px"
							onchange="javascript:fc_TipoMaterial();">
							<form:option value="-1">--Todos--</form:option>
							<c:if test="${control.codListaBuscarBy!=null}">
								<form:options itemValue="codTipoTablaDetalle"
									itemLabel="descripcion" items="${control.codListaBuscarBy}" />
							</c:if>
						</form:select></td>
					</tr>
					<tr>
						<td class="TextoSimple">Ordenar por</td>
						<td colspan="2"><form:select path="codOrdenarBy" id="codOrdenarBy"
							cssClass="cajatexto_o" cssStyle="width:180px"
							onchange="javascript:fc_TipoMaterial();">
							<c:if test="${control.codListaOrdenarBy!=null}">
								<form:options itemValue="codTipoTablaDetalle"
									itemLabel="descripcion" items="${control.codListaOrdenarBy}" />
							</c:if>
						</form:select></td>
					</tr>
					<tr>
					  <td class="TextoSimple">Disponibilidad</td>
					  <td class="TextoSimple2" valign="top" colspan="2"><input type="checkbox"
							name="checkSala" id="checkSala"
							onclick="javascript:fc_VerSala('1');">Sala&nbsp; <input
							type="checkbox" name="checkDomicilio" id="checkDomicilio"
							onclick="javascript:fc_VerDomicilio('1');">Domicilio&nbsp;</td>					  
				  </tr>
					<tr>
						<td class="TextoSimple">Sede</td>
						<td class="" valign="top">
							<form:select path="codSede" id="codSede"
								cssClass="cajatexto_o" cssStyle="width:180px">
								<form:option value="-1">--Todos--</form:option>
								<c:if test="${control.codListaSede!=null}">
									<form:options itemValue="dscValor1"
										itemLabel="descripcion" items="${control.codListaSede}" />
								</c:if>
							</form:select>
						</td>
						<td>
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
								<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar"
									onclick="javascript:fc_Limpiar();"></a>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
								<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" id="imgBuscar" style="CURSOR: pointer"
									onclick="javascript:fc_BusquedaSimple();">
							</a><br>
						</td>
					</tr>
					<tr>
						<td colspan="3" >
							<table cellspacing="0" cellpadding="0" border="0" width="100%" bordercolor="black">
								<tr>
									<td width="221px" height="15px" background="${ctx}/images/biblioteca/portal/biblioteca_r11_c11.jpg" ></td>
									<td width="244px" height="15px" class="txtlink" style='cursor: hand;'
										background="${ctx}/images/biblioteca/portal/biblioteca_r11_c13.jpg">
										<a href="#" class="txtlink" onclick="javascript:fc_BusquedaAvanzada();" style="cursor: pointer;">
											B�squeda Avanzada
										</a>&nbsp;&nbsp;/&nbsp;&nbsp;
										<a href="#" class="txtlink" onclick="" style="cursor: pointer;">
											Ayuda&nbsp;&nbsp;
										</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<!-- <td style="width: 15px;"></td> -->
		</tr>
	</table>

</form:form>
</body>