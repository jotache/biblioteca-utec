<%@ include file="/taglibs.jsp"%>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<script language=javascript>
	function onLoad()
	{
	}
	function fc_TipoMaterial(){
	
	}
	function fc_Linkear(srtUrl){
	 //alert(srtUrl);
	 //parent.location.href=srtUrl;
	 window.open(srtUrl,"Biblioteca",'resizable=1,width=940,height=780,menubar=1,scrollbars=1,toolbar=1,top=1,left=1');
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_bibliotecas.html">

 
	<table cellpadding="0" cellspacing="0" border="0" width="98%" bordercolor="blue" style="margin-left:5px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<font style="">
					 	Las Bibliotecas
				 	</font>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
	</table>	
	
	<table width="100%" bgcolor="white"><tr height="2px"><td></td></tr></table>	
			
	<table cellpadding="0" cellspacing="0" ID="Table1" width="98%" border="0px"  class="" align="center">
			<tr>
			 	<td align="left">			
				 	<display:table name="sessionScope.listaBibliotecas" cellpadding="0" cellspacing="0"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  
						style="border: 0px solid #FBF9F9;width:98%;">
					    <display:column property="textBibliotecas" title="WilDATilA"  
								headerClass="grilla" class="tablagrillaenlacebib" style="text-align:center; width:100%"/>
													
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
							
					</display:table>
				</td>
			</tr>
		   </table>
		
</form:form>		
</body>