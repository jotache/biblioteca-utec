<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%@page import="com.tecsup.SGA.modelo.UsuarioSeguriBib;"%>
<html>
<head>
<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<script language=javascript>
	function onLoad(){}		
	
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="94%" class="opc_combo">
				 	<span class="">
					 	Bases de datos en prueba</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 500px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94%" border="0" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<p align="justify"><br>						
			        Estimado Usuario CEDITEC:</p>
				 	<p align="justify"> A continuaci&oacute;n presentamos las bases de datos en per&iacute;odo de prueba. Por favor,&nbsp; le pedimos revisarlas y enviarnos sus comentarios o sugerencias al correo: 

 						<a href="mailto:biblioteca@tecsup.edu.pe">biblioteca@tecsup.edu.pe</a> para su evaluaci&oacute;n y posible suscripci&oacute;n para el a&ntilde;o 2010. <br/>
				      <br/>
	
					</p>
				 	<p></p>			      
				</td>
			</tr>
			<tr>
			<td>

			<table width="700" border="0" align="center">
	        <tr>
	          <td width="250" valign="middle">
				<div align="center">
					<a href="http://proquest.umi.com/login/refurl" style="cursor: hand" target="_blank">
						<img id="img01" src="${ctx}/images/biblioteca/portal/proquest_logo.jpg" width="174px" height="66px" border="0"
							alt="" align="absmiddle">
					</a><br>
	              	<strong><br>
	              	</strong>
				</div>
			  </td>
	          <td width="450">	
				<div align="left">							
					</a>
		            <p>Proquest es un servicio de informaci�n en l�nea que proporciona acceso a miles de art�culos en resumen y texto completo de publicaciones peri�dicas cient�ficas de primera l�nea, los cuales son actualizados diariamente.<br>
		              <strong><br>
		              </strong>
					</p>
            	</div>
			  </td>
	        </tr>

			<tr>
	          <td width="250">
				<div align="center">
					<a href="http://proquest.safaribooksonline.com/?uicode=tecsup" style="cursor: hand" target="_blank">
						<img id="img01" src="${ctx}/images/biblioteca/portal/safari.jpg" width="174px" height="66px" border="0"
							alt="">
					</a><br>
	              	<strong><br>
	              	</strong>
				</div>
			  </td>
	          <td width="450">	
				<div align="left">							
		            <p>Permite el acceso inmediato a m�s de 640 libros electr�nicos a texto completo, especializados en inform�tica, nuevas tecnolog�as, comercio electr�nico, y direcci�n y administraci�n de empresas.<br>
		              <strong><br>
		              </strong>
					</p>
            	</div>
			  </td>
	        </tr>

			<tr>
	          <td width="250">
				<div align="center">
					<a href="http://site.ebrary.com/lib/tecsup" style="cursor: hand" target="_blank">
						<img id="img01" src="${ctx}/images/biblioteca/portal/ebrary.jpg" width="174px" height="71px" border="0"
							alt="">
					</a><br>
	              	<strong><br>
	              	</strong>
				</div>
			  </td>
	          <td width="450">	
				<div align="left">							
		            <p>Ofrece acceso a libros electr�nicos en ingl�s.<br>
		              <strong><br>
		              </strong>
					</p>
            	</div>
			  </td>
	        </tr>
			<!--  -->
	        <tr>
	          <td colspan="2"><div align="center"><br>
					<%--
	                  <a href="http://search.ebscohost.com/login.aspx?authtype=uid&user=ns003565&password=trial&profile=autorefctr" target="_blank">
					<img src="${ctx}/images/biblioteca/portal/logoSERRC.jpg" width="200" height="37" border="0"></a><br>
					--%>
					 
	          </div></td>
	        </tr>
	        
	      </table>

			</td>
			</tr>
	    </table>
		</div>
		

</body>
</html>