<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}
	function fc_TipoMaterial(){
	
	}
	function fc_Reservar(){}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_los_mas_solicitados.html">
<table class="tabla2" style="width:100%" cellSpacing="2" cellPadding="2" border="0" > 
		<tr>
				<td colspan='2' class="Tab_vert" style='cursor:hand;' onclick="javascript:fc_Cambia('0');">LOS MAS SOLICITADOS</td>					
		</tr>
		<tr>
			<td width="10%">Tipo MAterial :</td>
			<td colspan="1" name="tablaNiveles1" id="tablaNiveles1">
			    <form:select path="codTipoMaterial"
						id="codTipoMaterial" cssClass="cajatexto"
						cssStyle="width:200px"  onchange="javascript:fc_TipoMaterial();">
						<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaTipoMaterial!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.codListaTipoMaterial}" />
						</c:if>
					</form:select>
			</td>
											
		</tr>
		<tr>
				<td colspan='2' width="100%"><img src="../images/separador_n.gif" width="100%"></td>
		</tr>	
</table>
	
<table cellpadding="0" cellspacing="0" ID="Table1" width="100%" border="1px"  class="" style="margin-left:3px">
			
			<tr>
			 	<td align="left">			
				 	<display:table name="sessionScope.listaTipoMaterial" cellpadding="0" cellspacing="0"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  
						style="border: 1px solid #048BBA;width:100%;">
					    <display:column property="textValor" title="WilDATilA"  
								headerClass="grilla" class="tablagrilla" style="text-align:center; width:100%"/>
													
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
							
					</display:table>
				</td>
			</tr>
		</table>
		
</form:form>		
</body>
</html>