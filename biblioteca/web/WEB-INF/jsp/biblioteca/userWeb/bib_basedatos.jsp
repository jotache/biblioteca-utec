<%@page import="com.tecsup.SGA.common.MetodosConstants"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.lang.*"%>
<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%@page import="com.tecsup.SGA.modelo.UsuarioSeguriBib;"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="${ctx}/scripts/js/pearson/jquery-2.1.1.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/basesdatos.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/basepearson.js" type="text/JavaScript"></script>
<script src="${ctx}/scripts/js/baselibrisite.js" type="text/JavaScript"></script>

<style type="text/css">
.textolink { 
	font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; font-weight: none; font-variant: normal; text-transform: none; color: blue; text-decoration: underline; 
	}
.textomenu { 
	font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; font-weight: none; text-transform: none; color: #00335A; text-decoration: none; }
</style>

    <script type="text/javascript">
    var un; //Nombre del usuario codificado.
    var ul1; //Apellido paterno del usuario codificado.
    var ul2; //Apellido materno del usuario codificado.
    var ue; //Correo electrónico del usuario codificado.
    var ui; //Dirección IP del usuario codificada.
    var i; //ID de la institución en la base de datos de Pearson (entero)
    var ic; //ID del campus de la institución en la base de datos de Pearson (entero)
    var ik; //Institución Key en la base de datos de Pearson (entero)
    var res;
    var urlInitial="";
    var attemptCount = 0;
    
    function GetUrlBV() {
    	<%
    	out.println("1");
    	
    	if (request.getSession().getAttribute("usuarioSeguridad")!=null){
    		UsuarioSeguridad userSeg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
    		String sKey = "lstecl21303sup";

    		out.println("2");
    		
    		String nombre = userSeg.getNomUsuario().toLowerCase();						
    		String apellido = userSeg.getApeUsuario().toLowerCase();
    		String especialidad = userSeg.getEspecialidad();
    		String tipoUsuario = userSeg.getTipoUsuario();
    		
    		if(nombre==null) nombre="";
    		if(apellido==null) apellido="";
    		
    		String correo = "";
    		if(userSeg.getCorreo()!=null)
    			correo = userSeg.getCorreo().toLowerCase();
    		else
    			correo ="";			
    		String username = userSeg.getCodUsuario().toLowerCase();
    		String datoCorreo = "";
    		
    		//si el correo contiene los caracteres "," , ";" , "/" � tiene mas de una "@" entonces se considera que tiene varios correos
    		if("".equals(correo))
    			datoCorreo=username;	
    		else
    			if(correo.indexOf(",")>0 || correo.indexOf(";")>0 || correo.indexOf("/")>0 || correo.indexOf("@",correo.indexOf("@")+1)>0 )
    				datoCorreo=username;
    			else
    				datoCorreo=correo;
    								    			        
    	%>
    	    	
    	un = '<%=nombre%>';
        ul1 = '<%=apellido%>';
        ul2 = "";
        ue = '<%=datoCorreo%>';
        ui = "";
        i = "33";
        ic = "";
        ik = "t3cs1p2105";
        res = "";
        
        GetURLPage();
        
    	<%}else{%>
    		alert("Necesita iniciar sesi�n para acceder a esta opci�n");
    	<%}%>
    };


    function GetURLPage() {
        
        var urlInitial = "http://www.biblionline.pearson.com/Services/GenerateURLAccess.svc/GetUrl?firstname=" + un + "&lastname1=" + ul1 + "&lastname2=" + ul2 + "&email=" + ue + "&ip=" + ui + "&idInstitution=" + i + "&idCampus=" + ic + "&institutionKey=" + ik + "&$callback=successCall&$format=json";
        var url;
        $.ajax({
            dataType: "jsonp",
            contentType: "application/json; charset=utf-8",
            url: urlInitial,
            jsonpCallback: "successCall",
            error: function () {
            	if(attemptCount == 0){
            		attemptCount++;
            		GetURLPage();
            	}
                else{
                	alert("Error");
                }
            },
            success: successCall
        });

        function parseJSON(jsonData) {
            return jsonData.Message;
        }

        function successCall(result) {
            res = parseJSON(result.GetUrlAccessResult);
//          top.parent.location.replace(res); //Redirecciona a la página de la BV.
            
            var sRuta = res;
     		var sNameWindow = 'PEARSON';
     		var featuresWin	  = 'width=' +  950 +
    							', height=' + 600 + ', top=0, left=0, scrollbars=1, resize=0, resizable=1, menubar=0';
    			 			 		
     		var winLogin = window.open(sRuta, sNameWindow,featuresWin);
     		winLogin.focus();
     		
        }
    };    

    </script>

<script type="text/javascript">
	function onLoad(){}		
	
	function GotoPearson2(){		
 		GetUrlBV();
	}
	
		
	function GotoGale(){		
	 	<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>
	 		ValidAccessGale();		
		<%}else{%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<%}%>
	}
	
	function GotoCengageEBooks(){		
	 	<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>
	 		ValidAccessCengage();		
		<%}else{%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<%}%>
	}	
	
	function GotoLibrisite(){
		<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){
								
			UsuarioSeguridad userSeg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
			String sKey = "lstecl21303sup";
			String matricula = userSeg.getIdUsuario().toString();
			String nombre = userSeg.getNomUsuario().toLowerCase();						
			String apellido = userSeg.getApeUsuario().toLowerCase();
			String especialidad = userSeg.getEspecialidad();
			String tipoUsuario = userSeg.getTipoUsuario();
			
			if(nombre==null) nombre="";
			if(apellido==null) apellido="";
			if(tipoUsuario==null) tipoUsuario="";
			if(especialidad==null) especialidad="";
									
			String username = userSeg.getCodUsuario().toLowerCase();
											
			//calculo de segundos desde 1 Enero 1970
			Date d0 = new GregorianCalendar(1969,12,31,23,59).getTime();
			Date dHoy = new Date();
			long diff = (dHoy.getTime() - d0.getTime()) / 1000;
			String stControl = String.valueOf(diff);
			
			nombre = nombre.replaceAll("�", "a");
			nombre = nombre.replaceAll("�", "e");
			nombre = nombre.replaceAll("�", "i");
			nombre = nombre.replaceAll("�", "o");
			nombre = nombre.replaceAll("�", "u");
			nombre = nombre.replaceAll("�", "n");
			nombre = nombre.replaceAll("�", "u");
			
			apellido = apellido.replaceAll("�", "a");
			apellido = apellido.replaceAll("�", "e");
			apellido = apellido.replaceAll("�", "i");
			apellido = apellido.replaceAll("�", "o");
			apellido = apellido.replaceAll("�", "u");
			apellido = apellido.replaceAll("�", "n");
			apellido = apellido.replaceAll("�", "u");
			
			String nombreUsuario = nombre + " " + apellido;
			String sToConvert = matricula + "|" + sKey + "|" + stControl;
			
			String hashKey = MetodosConstants.hexDigest(sToConvert);
									
		%>	
		
		ValidAccessLibrisite('<%=matricula%>','<%=stControl%>','<%=especialidad%>','<%=tipoUsuario%>','<%=nombreUsuario%>','<%=hashKey%>');
	 								
		<%}else{%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<%}%>			
	}
	
	function BibliotecaVirtual() {
	 	<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>
	 	 	
	 	<%				
	 	if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_WEB") == 1) {
	 		
	 		UsuarioSeguridad userSeg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
	 		if (userSeg.getUsuarioSeguriBib()!=null){
	 			UsuarioSeguriBib userseguri = userSeg.getUsuarioSeguriBib();
	 			userseguri.getUsuario();
	 			userseguri.getClave();
			%>			
				var codigousuario = "<%=( (userseguri.getUsuario()==null)?"": userseguri.getUsuario())%>";
				var claveusuario = "<%=( (userseguri.getClave()==null)?"": userseguri.getClave())%>";								
			  	Fc_Popup("${ctx}/biblioteca.jsp?usuario="+codigousuario+ "&claveacceso="+claveusuario,950,600);
			  	
			<%} else {%> 
				alert("Ud. requiere Privilegios de Egresado para acceder");		   
			<%}%>  
	
		<%} else {%> 
			alert("Ud. No tiene permiso para Ingresar a la Biblioteca Virtual");		   
		<%}
		} else {%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<% }%>	
	}	
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Biblioteca Virtual
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 500px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94%" border="0" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<p align="justify"><br>						
				      La Biblioteca Virtual del CEDITEC contiene informaci&oacute;n actualizada a texto completo (libros y revistas en formato digital) de alta calidad acad&eacute;mica. Son elaborados por expertos de todo el mundo y tratan sobre las principales &aacute;reas de tecnolog&iacute;a y negocios.<br/><br/> Adem&aacute;s ofrece una base de datos con los textos elaborados por Tecsup para los cursos virtuales en formato flash paper.<br/><br/>Para acceder a las base de datos, es necesario ingresar al Sistema de Biblioteca con su usuario y clave de acceso.<br>
				      <br>
        					
					</p>
				</td>
			</tr>
			<tr>
			<td>

			<table width="600" border="0" align="center">
	        <tr>
	          <td width="300">
				<div align="center">
					<a onClick="GotoPearson2();" style="cursor: pointer;">
                	<img src="${ctx}/images/biblioteca/portal/logo_pearson.gif" width="146" height="43" border="0" 
                		title="Pearson"/>
                    </a>
                    
				</div>
			  </td>
	          <td width="300">	
				<div align="center">		
					<a onClick="GotoGale();" style="cursor: pointer;">
						<img id="img02" src="${ctx}/images/biblioteca/portal/cengale_logo.jpg" width="171px" height="64px" border="0"
							alt="Ofrece acceso remoto a bases de datos de revistas y libros a texto completo en formato digital."
							title="Gale">
					</a>
		           
            	</div>
			  </td>
	        </tr>
			
			<tr>
				<td colspan="2" height="14">
				</td>
			</tr>
			
			<%----%>
	       <tr>
	       	<td	colspan="1">
	       		<div align="center">		
					<a onClick="GotoLibrisite();" style="cursor: pointer;">
						<img id="img03" src="${ctx}/images/biblioteca/portal/librisite_logo.png" width="200" height="51" border="0"
							title="Librisite">
					</a>
		           
            	</div>
	       	</td>
	       	
	       	<td	colspan="1">
	       		<div align="center">		
					<a onClick="BibliotecaVirtual();" style="cursor: pointer;">
						<img id="img04" src="${ctx}/images/logo200x58.png" width="200" height="58" border="0"
							title="Biblioteca Virtual Tecsup">
					</a>
		           
            	</div>
	       	</td>
	       	
	       </tr>
	         
			 <tr>
				<td colspan="2" height="14">
				</td>
			</tr>
			 
	       <!-- inicio -->
	       <tr>
	       	<td	colspan="1">
	       		<div align="center">		
					<a href="http://metabuscador.istec.org" style="cursor: pointer;" target="_blank">
						<img id="img05" src="${ctx}/images/biblioteca/portal/istec.gif" height="54" border="0" alt="ISTEC">
					</a>
		           
            	</div>
	       	</td>	

			<td	colspan="1">
	       		<div align="center">							
					<a href="javascript:void(0);" onClick="GotoCengageEBooks();" style="cursor: pointer;">
						<img id="img05" src="${ctx}/images/biblioteca/portal/logo-cengage-ebooks24.png" border="0" alt="CENGAGE">
					</a>
		           
            	</div>
	       	</td>	
			
	       </tr>
	       <!-- fin -->
	         
	         
	      </table>

			</td>
			</tr>
	    </table>
		</div>
		

</body>
</html>