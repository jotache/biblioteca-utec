<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<html>
<head>

<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />

<script language=javascript>
	function onLoad(){
	}
	
	function fc_detalleMaterial(codUnico){
		var url ="${ctx}/biblioteca/detalle_material.html?txhCodigoUnico="+fc_Trim(codUnico)+"&flgView=2"+
				"&strUrl="+"<%=(( request.getAttribute("strUrl")==null)?"": request.getAttribute("strUrl"))%>";
		Fc_Popup(url,600,250,"renzo");
	}
	
	function fc_detalleAdjunto(codUnico){
		var url ="${ctx}/biblioteca/bib_material_adjuntos.html?txhCodigoUnico="+fc_Trim(codUnico);
		Fc_Popup(url,600,250,"renzo");
	}
	
	function fc_Reservar(srtCodigoUnico, srtFechaBD, index){
		srtCodPeriodo= document.getElementById("txhCodPeriodo").value;
	 	srtCodUsuario=document.getElementById("txhCodUsuario").value;
	 	
		if (document.getElementById("txtDisponible"+index)!=null){
			if (document.getElementById("txtDisponible"+index).value == "0"){
				alert('El material seleccionado no tiene ejemplares disponibles.');
				return false;
			}
		}
	 
		<%
		if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_RES") == 1) 
		{
		%>
			 Fc_Popup("${ctx}/biblioteca/bib_reserva_material_bibliografico.html?txhCodigoUnico="+
			 srtCodigoUnico + "&txhFechaBD=" + srtFechaBD + "&txhCodPeriodo=" + srtCodPeriodo + 
		 	"&txhCodUsuario=" + srtCodUsuario,440,240,"angela");
	 	<%}else{%>alert('La opci�n requiere el acceso al Sistema Biblioteca');
				window.opener.fc_IngresarSistema();
				window.close();
		<%}%>
	}
	function fc_Buscar(){
		document.getElementById("txhOperacion").value = "BUSCAR";
	  	document.getElementById("frmMain").submit();
	}
	
</script>
</head>

<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_busqueda_avanzada_flotante.html">
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>

<form:hidden path="operacion" id="txhOperacion"/>

	<form:hidden path="codTipoMaterial" id="txhCodTipoMaterial"/>
	<form:hidden path="codSeleccion1" id="txhCodSeleccion1"/>
	<form:hidden path="codSeleccion2" id="txhCodSeleccion2"/>
	<form:hidden path="codSeleccion3" id="txhCodSeleccion3"/>
	<form:hidden path="codSeleccion4" id="txhCodSeleccion4"/>
	<form:hidden path="txtTexto1" id="txhTxtTexto1"/>
	<form:hidden path="txtTexto2" id="txhTxtTexto2"/>
	<form:hidden path="txtTexto3" id="txhTxtTexto3"/>
	<form:hidden path="codCondicion1" id="txhCodCondicion1"/>
	<form:hidden path="codCondicion2" id="txhCodCondicion2"/>
	<form:hidden path="codIdioma" id="txhCodIdioma"/>
	<form:hidden path="codFechaAnioInicio" id="txhCodFechaAnioInicio"/>
	<form:hidden path="codFechaAnioFin" id="txhCodFechaAnioFin"/>
	<form:hidden path="ordenarBy" id="txhOrdenarBy"/>
	<form:hidden path="selSala" id="txhSelSala"/>
	<form:hidden path="selDomicilio" id="txhSelDomicilio"/>

	<table cellpadding="0" cellspacing="0" border="0" width="99%" bordercolor="blue" style="margin-left:3px;margin-top:4px;">
		<tr>
			<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
			<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
			 	<font style="">
				 	Resultado de la B&uacute;squeda</font>
			</td>				
			<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		</tr>
	</table>
		
		<table cellpadding="0" cellspacing="0" width="99%" border="0" bordercolor="blue" style="margin-left:4px;margin-top:5px; margin-right:5px;">		
			<tr>
			 	<td align="left">	
	
			 	 <div style="overflow: auto; height: 500px;width:100%">		
				 	<display:table name="sessionScope.listaBusquedaAvanzada" cellpadding="0" cellspacing="0"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  pagesize="3" 
						style="width:97.5%;" requestURI="">
						
						<display:column property="textBusquedaSimple" headerClass="grilla" class="tablagrilla" style="text-align:center; width:100%"/>					    
													
						<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.show.header" value="false"/>
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='cajatexto-login'><td colspan='3' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='cajatexto-login'>No se encontraron registros. </span>" />
							<display:setProperty name="paging.banner.one_item_found" value="<span class='cajatexto-login'>Un registro encontrado </span>" />
							<display:setProperty name="paging.banner.all_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando todo {2}. </span>" />
							<display:setProperty name="paging.banner.some_items_found" value="<span class='cajatexto-login'>{0} {1} encontrados, mostrando {2} a {3}. </span>" />
							<display:setProperty name="paging.banner.full" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.first" value="<span class='cajatexto-login'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>&Uacute;ltimo</a>]</span>" />
							<display:setProperty name="paging.banner.last" value="<span class='cajatexto-login'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/&Uacute;ltimo]</span>" />							
							<display:setProperty name="paging.banner.onepage" value="<span class='cajatexto-login'>{0}</span>" />
							
					</display:table>	
				  </div>			  
				</td>
			</tr>
			
	    </table>
	    <table cellspacing="0" cellpadding="0" border=0 align="center" style="margin-top: 15px;">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgPrest','','${ctx}/images/botones/cerrar2.jpg',1)">
						<img src="${ctx}/images/botones/cerrar1.jpg" align="absmiddle" alt="Cerrar" id="imgPrest" style="CURSOR: pointer" onclick="window.close();"></a>
		</td>
		</tr>
	</table>
</form:form>
</body>
</html>