<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{ 	
		objMsg = document.getElementById("txhMsg");
		
		if ( objMsg.value == "1" )
		{
			window.opener.fc_Buscar();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else{
				msg1 = "<%=(( request.getAttribute("msg1")==null)?"": request.getAttribute("msg1"))%>";
			  	msg2 = "<%=(( request.getAttribute("msg2")==null)?"": request.getAttribute("msg2"))%>";
			  	msg3 = "<%=(( request.getAttribute("msg3")==null)?"": request.getAttribute("msg3"))%>";
			  	 
			  if ( objMsg.value == "2" ){			  
			  	 alert("No se puede realizar la reserva \nla fecha de reserva debe ser mayor a "+msg2);
			  	 window.close();		       	 
		      }else if ( objMsg.value == "3" ){
		      	 	alert(mstrResNoCoincideSedeMatUsu);
		      }else if ( objMsg.value == "4" ){
		      		alert("no existe material disponible");
		      }else if ( objMsg.value == "5" ){		      	 
		         alert("No se puede realizar la reserva existen \nreservas o prestamos pendientes para el mismo material");
		      }else if ( objMsg.value == "6" ){
		      	 alert("No de puede realizar la reserva\n el usuario tiene sanciones vigentes");
		         window.close();
		      }else if ( objMsg.value == "7" ){
		      	 alert("No de puede realizar la reserva\n el usuario exedio el nro de reservas/prestamos permitidos");
		         window.close();
		      }else if ( objMsg.value == "-" ){
		      	 alert(mstrProblemaGrabar);		             
		      }
		           
		}
	}
	
	function fc_Guardar()
	{ 	
	 	fecha2=document.getElementById("txhTxtFechaBD").value;
	    fecha1=document.getElementById("txtFecha").value;
	    var num=fc_ValidaFechaIniFechaFin(fecha2,fecha1); //fc_ValidaFechaIniFechaFinMayorOIgual(fecha1,fecha2);
	    if(document.getElementById("txhCodEvaluador").value!="")
		{    if(document.getElementById("txtFecha").value!="")
			 {  if(num==0)
			    { 
			      document.getElementById("txhOperacion").value="GUARDAR";
			      document.getElementById("frmMain").submit();
			    }
			    else{ 
			          alert(mstrFechaReservaMayorIgual);
			        }
			  }
			else alert(mstrFechaReserva);
		}
		else {
			alert("Debe registrarse en el sistema");
			window.close();
			window.opener.close();
		}
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_reserva_material_bibliografico.html">
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="txtFechaBD" id="txhTxtFechaBD"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codTipoMaterial" id="txhCodTipoMaterial"/>
<form:hidden path="operacion" id="txhOperacion" />

	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:3px; margin-right:3px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="400px" class="opc_combo">
			 	<font style="">			
				 		Reserva de Material Bibliográfico				 	
			 	</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
	
	<table><tr height="1px"><td></td></tr></table>
		
	<table class="tablaflotante" style="width:98%;margin-top:5px; margin-left:4px;" cellSpacing="4" cellPadding="2" border="0" bordercolor='gray'>  
		<tr>
			<td nowrap="nowrap" align="left" width="20%">&nbsp;Fecha Reserva:</td>
			<td>
				<form:input path="txtFecha" id="txtFecha" cssClass="cajatexto_o" 
					onkeypress="fc_ValidaFecha('txtFecha');"
					onblur="fc_ValidaFechaOnblur('txtFecha');" maxlength="10" />
					&nbsp;<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" 
					style='cursor:hand;' align="absmiddle">
			</td>
		</tr>
	</table>
	
	<table><tr height="5px"><td></td></tr></table>
	
	<table align=center>
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Guardar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" onclick="window.close();" style="cursor:pointer;"></a></td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecha",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
</script>
</body>
</html>