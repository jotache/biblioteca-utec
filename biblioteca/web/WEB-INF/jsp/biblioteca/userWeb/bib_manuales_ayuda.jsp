<%@page import="com.tecsup.SGA.common.MetodosConstants"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.lang.*"%>
<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%@page import="com.tecsup.SGA.modelo.UsuarioSeguriBib;"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script src="${ctx}/scripts/js/Func_Comunes.js" type="text/JavaScript"></script>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />

<style type="text/css">
.textolink { 
	font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; font-weight: none; font-variant: normal; text-transform: none; color: blue; text-decoration: underline; 
	}
.textomenu { 
	font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; font-weight: none; text-transform: none; color: #00335A; text-decoration: none; }
</style>

<script type="text/javascript">
	function onLoad(){}		
	
		
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Recursos de Ayuda
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 500px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94%" border="0" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<p align="justify" class="TituloBiblio">
                    <br>
					 	<a href="/file/sga/documentos/BASE_DE_DATOS_galecengage.pdf" target="_blank">
					 	�C�mo buscar en Gale Cengage Learning?
					 	</a>
				 	</p>                    			     
                    <br>
                    
                    <p align="justify" class="TituloBiblio">
					<a href="/file/sga/documentos/BASE_DE_DATOS_pearson.pdf" target="_blank">�C�mo buscar en Pearson?</a>                       
                    </p>                   		     
                    <br>                    
                    
                    
                    <p align="justify" class="TituloBiblio">
					<a href="/file/sga/documentos/REFERENCIA_FUENTES.pdf" target="_blank">
						Referencia las fuentes de tu trabajo 
					</a>                       
                    </p>                    			     
                    <br> 
                    
                    <p align="justify" class="TituloBiblio">
					<a href="/file/sga/documentos/GUIA_DEL_USUARIO.pdf" target="_blank">
						Gu&iacute;a del usuario 
					</a>                       
                    </p>
                    <br>
                    <p align="justify" class="TituloBiblio">
	                    <a href="http://www.tecsup.edu.pe/home/wp-content/uploads/2015/03/Gale_Digital_Library_manual_de_ayuda.pdf" target="_blank">
	                    	�C&oacute;mo buscar en Cengage Digital Library?
	                    </a> 
                    </p>                   			     
                    <br>
                    <p align="justify" class="TituloBiblio">
	                    <a href="/file/sga/documentos/Normas_APA_Tecsup_2015.pdf" target="_blank">
	                    	Adaptaci&oacute;n Normas APA- 6� edici&oacute;n
	                    </a> 
                    </p>
                    <br>
                    <p align="justify" class="TituloBiblio">
	                    <a href="https://docs.google.com/a/tecsup.edu.pe/file/d/0B2PqHeBEKJdlejR4Y1RCaU1FQk0/edit" target="_blank">
	                    	Video Tutorial: Uso de Biblioteca Virtual
	                    </a> 
                    </p>
                    <br>
                    <p align="justify" class="TituloBiblio">
	                    <a href="https://www.youtube.com/watch?v=XV02YX3yUJg&feature=youtu.be" target="_blank">
	                    	Video Tutorial: Nueva Plataforma Pearson
	                    </a> 
                    </p>
                         
                         
                    <br>
                    <p align="justify" class="TituloBiblio">
	                    <a href="https://www.youtube.com/watch?v=XV02YX3yUJg&feature=youtu.be" target="_blank">
	                    	Video Tutorial: Nueva Plataforma Pearson
	                    </a> 
                    </p>
				</td>
			</tr>
			
	    </table>
		</div>
		

</body>
</html>