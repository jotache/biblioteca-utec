<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}
	function fc_TipoMaterial(){
	
	}
	function fc_VerMas(srtCodigo){
		 
		 srtCodEvaluador=document.getElementById("txhCodEvaluador").value;
		 srtCodPeriodo=document.getElementById("txhCodPeriodo").value;
		 
		 location.href="${ctx}/biblioteca/bib_portal_novedades_inicio_ver_mas.html?txhCodUsuario=" + srtCodEvaluador + "&txhCodPeriodo=" + srtCodPeriodo + "&txhCodigo=" + srtCodigo; 
		 
	}
	
</script>
</head>
<body>
	
	<%-- --%>
	<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_portal_novedades_inicio.html">
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="codEvaluador" id="txhCodEvaluador"/>

		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">				 	
					 	Novedades				 	
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 550px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" bordercolor="blue" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">
				 	<display:table name="sessionScope.listaNovedades" cellpadding="0" cellspacing="0"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  
						style="width:100%;">
					    
					    <display:column property="textNovedadesInicio" headerClass="grilla" class="tablagrilla" style="text-align:center; width:97%"/>
													
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />	
					</display:table>
				</td>
			</tr>
	    </table>
		</div>
	</form:form>
	
</body>
</html>