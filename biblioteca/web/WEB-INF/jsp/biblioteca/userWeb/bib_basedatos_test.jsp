<%@page import="com.tecsup.SGA.common.MetodosConstants"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.lang.*"%>
<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>
<%@page import='com.tecsup.SGA.common.CommonSeguridad'%>
<%@page import="com.tecsup.SGA.modelo.UsuarioSeguriBib;"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script src="${ctx}/scripts/js/Func_Comunes.js" language="JavaScript;"	type="text/JavaScript"></script>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />

<style type="text/css">
.textolink { 
	font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; font-weight: none; font-variant: normal; text-transform: none; color: blue; text-decoration: underline; 
	}
.textomenu { 
	font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; font-weight: none; text-transform: none; color: #00335A; text-decoration: none; }
</style>

<script language=javascript>
	function onLoad(){}		
	
	function GotoTest(){
		<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>						
			var sRuta = 'bib_base_datos_prueba.html';
	 		var sNameWindow = 'TEST';
	 		var featuresWin	  = 'width=' +  950 +
			', height=' + 600 + ', top=0, left=0, scrollbars=1, resize=1, menubar=0, resizable=0 ';				 	
	 		var winLogin = window.open(sRuta, sNameWindow,featuresWin);
	 		winLogin.focus(); 
		<%}else{%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<%}%>
	}
	
	function GotoGale(){		
	 	<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>
	 		var sRuta = 'http://infotrac.galegroup.com/itweb/tecsup?id=tecsup0519';
	 		var sNameWindow = 'GALE';
	 		var featuresWin	  = 'width=' +  950 +
			', height=' + 600 + ', top=0, left=0, scrollbars=1, resize=0, resizable=1, menubar=0';	 		
	 		var winLogin = window.open(sRuta, sNameWindow,featuresWin);
	 		winLogin.focus();			
		<%}else{%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<%}%>
	}
	
	function GotoPearson(){
		<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){
					//HTMLEncode htmlEnco = new HTMLEncode();				
			UsuarioSeguridad userSeg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
			String sKey = "t3cs1p1120";
			String nombre = userSeg.getNomUsuario().toLowerCase();						
			String apellido = userSeg.getApeUsuario().toLowerCase();
			
			if(nombre==null) nombre="";
			if(apellido==null) apellido="";
			
			String correo = "";
			if(userSeg.getCorreo()!=null)
				correo = userSeg.getCorreo().toLowerCase();
			else
				correo ="";			
			String username = userSeg.getCodUsuario().toLowerCase();
			String datoCorreo = "";
			
			//si el correo contiene los caracteres "," , ";" , "/" � tiene mas de una "@" entonces se considera que tiene varios correos
			if("".equals(correo))
				datoCorreo=username;	
			else
				if(correo.indexOf(",")>0 || correo.indexOf(";")>0 || correo.indexOf("/")>0 || correo.indexOf("@",correo.indexOf("@")+1)>0 )
					datoCorreo=username;
				else
					datoCorreo=correo;
						
			//calculo de segundos desde 1 Enero 1970
			Date d0 = new GregorianCalendar(1969,12,31,23,59).getTime();
			Date dHoy = new Date();
			long diff = (dHoy.getTime() - d0.getTime()) / 1000;
			String stControl = String.valueOf(diff);
			
			nombre = nombre.replaceAll("�", "a");
			nombre = nombre.replaceAll("�", "e");
			nombre = nombre.replaceAll("�", "i");
			nombre = nombre.replaceAll("�", "o");
			nombre = nombre.replaceAll("�", "u");
			nombre = nombre.replaceAll("�", "n");
			nombre = nombre.replaceAll("�", "u");
			
			apellido = apellido.replaceAll("�", "a");
			apellido = apellido.replaceAll("�", "e");
			apellido = apellido.replaceAll("�", "i");
			apellido = apellido.replaceAll("�", "o");
			apellido = apellido.replaceAll("�", "u");
			apellido = apellido.replaceAll("�", "n");
			apellido = apellido.replaceAll("�", "u");
			
			String sToConvert = nombre + "|" + apellido + "|" + datoCorreo + "|" + sKey + "|" + stControl;
			
			String hashKey = MetodosConstants.hexDigest(sToConvert);
			
			/*
			System.out.println("TO: " + sToConvert);			
			System.out.println("Nombre: " + nombre);
			System.out.println("Apellido: " + apellido);			
			System.out.println("Username: " + username);
			System.out.println("Correo: " + datoCorreo);
			System.out.println("HASH: " + hashKey);
			
			String url="http://www.pearsonbv.com/sso/tecsup.asp?stHash=" + hashKey + "&stName=" + nombre + "&stLName=" + apellido + "&stMail=" + datoCorreo + "&stControl=" + stControl;
			System.out.println(""+url);
			*/
		%>	
			
			var sRuta = 'http://www.pearsonbv.com/sso/tecsup.asp?stHash=<%=hashKey%>&stName=<%=nombre%>&stLName=<%=apellido%>&stMail=<%=datoCorreo%>&stControl=<%=stControl%>';
	 		var sNameWindow = 'PEARSON';
	 		var featuresWin	  = 'width=' +  950 +
								', height=' + 600 + ', top=0, left=0, scrollbars=1, resize=0, resizable=1, menubar=0';
				 			 		
	 		var winLogin = window.open(sRuta, sNameWindow,featuresWin);
	 		winLogin.focus();
	 								
		<%}else{%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<%}%>			
	}
	
	function GotoLibrisite(){
		<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){
					//HTMLEncode htmlEnco = new HTMLEncode();				
			UsuarioSeguridad userSeg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
			String sKey = "lstecl21303sup";
			String matricula = userSeg.getIdUsuario().toString();
			String nombre = userSeg.getNomUsuario().toLowerCase();						
			String apellido = userSeg.getApeUsuario().toLowerCase();
			String especialidad = userSeg.getEspecialidad();
			String tipoUsuario = userSeg.getTipoUsuario();
			
			if(nombre==null) nombre="";
			if(apellido==null) apellido="";
			if(tipoUsuario==null) tipoUsuario="";
			if(especialidad==null) especialidad="";
									
			String username = userSeg.getCodUsuario().toLowerCase();
											
			//calculo de segundos desde 1 Enero 1970
			Date d0 = new GregorianCalendar(1969,12,31,23,59).getTime();
			Date dHoy = new Date();
			long diff = (dHoy.getTime() - d0.getTime()) / 1000;
			String stControl = String.valueOf(diff);
			
			nombre = nombre.replaceAll("�", "a");
			nombre = nombre.replaceAll("�", "e");
			nombre = nombre.replaceAll("�", "i");
			nombre = nombre.replaceAll("�", "o");
			nombre = nombre.replaceAll("�", "u");
			nombre = nombre.replaceAll("�", "n");
			nombre = nombre.replaceAll("�", "u");
			
			apellido = apellido.replaceAll("�", "a");
			apellido = apellido.replaceAll("�", "e");
			apellido = apellido.replaceAll("�", "i");
			apellido = apellido.replaceAll("�", "o");
			apellido = apellido.replaceAll("�", "u");
			apellido = apellido.replaceAll("�", "n");
			apellido = apellido.replaceAll("�", "u");
			
			String nombreUsuario = nombre + " " + apellido;
			String sToConvert = matricula + "|" + sKey + "|" + stControl;
			
			String hashKey = MetodosConstants.hexDigest(sToConvert);
						
			System.out.println("TO: " + sToConvert);			
			System.out.println("Nombre: " + nombre);
			System.out.println("Apellido: " + apellido);			
			System.out.println("Username: " + username);			
			System.out.println("HASH: " + hashKey);
			
			//String url="http://tecsup.libri.mx/usuario_valida.php?matricula=" + matricula + "&fecha=" + stControl + "&carrera=" + especialidad + "&tipodeusuario=" + tipoUsuario + "&nombreusuario=" + nombreUsuario + "&libroId=&hash=" + hashKey;
			//System.out.println(""+url);
			
		%>	
			
			var sRuta = 'http://tecsup.libri.mx/usuario_valida.php?matricula=<%=matricula%>&fecha=<%=stControl%>&carrera=<%=especialidad%>&tipodeusuario=<%=tipoUsuario%>&nombreusuario=<%=nombreUsuario%>&libroId=&hash=<%=hashKey%>';
	 		var sNameWindow = 'LIBRISITE';
	 		var featuresWin	  = 'width=' +  950 +
								', height=' + 600 + ', top=0, left=0, scrollbars=1, resize=0, resizable=1, menubar=0';
				 			 		
	 		var winLogin = window.open(sRuta, sNameWindow,featuresWin);
	 		winLogin.focus();
	 								
		<%}else{%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<%}%>			
	}
	
	function BibliotecaVirtual() {
	 	<%if (request.getSession().getAttribute("usuarioSeguridad")!=null){%>
	 	 	
	 	<%				
	 	if (CommonSeguridad.getValidaOpcion(request.getSession().getAttribute("usuarioSeguridad"),"BIB_WEB") == 1) {
	 		
	 		UsuarioSeguridad userSeg = (UsuarioSeguridad) request.getSession().getAttribute("usuarioSeguridad");
	 		if (userSeg.getUsuarioSeguriBib()!=null){
	 			UsuarioSeguriBib userseguri = userSeg.getUsuarioSeguriBib();
	 			userseguri.getUsuario();
	 			userseguri.getClave();
			%>			
				var codigousuario = "<%=( (userseguri.getUsuario()==null)?"": userseguri.getUsuario())%>";
				var claveusuario = "<%=( (userseguri.getClave()==null)?"": userseguri.getClave())%>";								
			  	Fc_Popup("${ctx}/biblioteca.jsp?usuario="+codigousuario+ "&claveacceso="+claveusuario,950,600);
			  	
			<%} else {%> 
				alert("Ud. requiere Privilegios de Egresado para acceder");		   
			<%}%>  
	
		<%} else {%> 
			alert("Ud. No tiene permiso para Ingresar a la Biblioteca Virtual");		   
		<%}
		} else {%>
			alert("Necesita iniciar sesi�n para acceder a esta opci�n");
		<% }%>	
	}	
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Biblioteca Virtual
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 500px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94%" border="0" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<p align="justify"><br>						
				      La Biblioteca Virtual del CEDITEC contiene informaci&oacute;n actualizada a texto completo (libros y revistas en formato digital) de alta calidad acad&eacute;mica. Son elaborados por expertos de todo el mundo y tratan sobre las principales &aacute;reas de tecnolog&iacute;a y negocios.<br/><br/> Adem&aacute;s ofrece una base de datos con los textos elaborados por Tecsup para los cursos virtuales en formato flash paper.<br/><br/>Para acceder a las base de datos, es necesario ingresar al Sistema de Biblioteca con su usuario y clave de acceso.<br>
				      <br>
        				<br>	
					</p>
				      	<p></p>			      
				</td>
			</tr>
			<tr>
			<td>

			<table width="600" border="0" align="center">
	        <tr>
	          <td width="300">
				<div align="center">
					<a onClick="GotoPearson();" style="cursor: hand">
                	<img src="${ctx}/images/biblioteca/portal/logo_pearson.gif" width="146" height="43" border="0"/>
                    </a>
                    
				</div>
			  </td>
	          <td width="300">	
				<div align="center">		
					<a onClick="GotoGale();" style="cursor: hand">
						<img id="img02" src="${ctx}/images/biblioteca/portal/cengale_logo.jpg" width="171px" height="64px" border="0"
							alt="Ofrece acceso remoto a bases de datos de revistas y libros a texto completo en formato digital.">
					</a>
		           
            	</div>
			  </td>
	        </tr>
			
			<tr>
				<td colspan="2" height="14">
				</td>
			</tr>
			
			
	       <tr>
	       	<td	colspan="2">
	       		<div align="center">		
					<a onClick="GotoLibrisite();" style="cursor: hand">
						<img id="img03" src="${ctx}/images/biblioteca/portal/librisite_logo.png" width="200" height="51" border="0"
							title="LIBRISITE">
					</a>
		           
            	</div>
	       	</td>
	       </tr>
	         
	         
	      </table>

			</td>
			</tr>
	    </table>
		</div>
		

</body>
</html>