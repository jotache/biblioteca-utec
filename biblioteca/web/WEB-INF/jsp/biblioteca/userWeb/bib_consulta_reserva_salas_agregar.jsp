<%@ include file="/taglibs.jsp"%>
<%	if ( request.getSession().getAttribute("usuarioSeguridad") == null ) { %>
	<script language="javascript">	
		alert("Su session ha expirado debe volver a logearse");
		window.opener.top.parent.location.href = "<%=(( request.getAttribute("pagina")==null)?"": request.getAttribute("pagina"))%>";
		window.close();			
	</script>
<%}%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<link href="${ctx}/scripts/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css" media="all" title="winter" />
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>
	<script src="${ctx}/scripts/js/funciones_bib-fechas.js" language="JavaScript;" type="text/JavaScript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{	objMsg = document.getElementById("txhMsg");

		//alert(document.getElementById("txhAccion").value);
    
		if ( objMsg.value == "OK" ){
		    window.opener.fc_Actualizar();			
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if(objMsg.value == "ERROR" ) 		  	alert(mstrProblemaGrabar);			
		else if(objMsg.value == "NOT_ALLOW" ) 		alert('El Usuario no est� permitido para realizar reservas.');//El Usuario no est� permitido para realizar reservas.
        else if(objMsg.value == "FECHA_HORA" )		alert('La fecha u hora de reserva no son v�lidas.');//La fecha u hora de reserva no son v�lidas.
        else if(objMsg.value == "YA_EXISTE" ) 		alert('El usuario ya registra una reserva en la fecha y hora seleccionada.');//El usuario ya registra una reserva en la fecha y hora seleccionada.
        else if(objMsg.value == "SANCION" ) 		alert('El usuario registra sanciones vigentes.');//El usuario registra sanciones vigentes.
        else if(objMsg.value == "NO_DISPONIBLE" ) 	alert('La sala ya no cuenta con vacantes disponibles.');//La sala ya no cuenta con vacantes disponibles.
        else if(objMsg.value == "EXCESO" ) 			alert('El usuario excedi� el n�mero de reservas (hrs) en el d�a.');//El usuario excedi� el n�mero de reservas (hrs) en la semana.
        else if(objMsg.value == "ERROR_USUARIO" ) 	alert('El usuario ingresado es invalido');//El usuario ingresado es invalido
        else if(objMsg.value == "USUARIO_INHABILITADO" ) 	alert('El usuario se encuentra inhabilitado para este servicio');//
        else if(objMsg.value == "CRUCE_ATENCION") 	alert('Existe un cruce de horas con una Atenci�n ya registrada.');
        else if(objMsg.value == "RESERVA_NO_PERMITIDA") alert('El recurso no pertenece al Centro de Estudios del Usuario');
        else if(objMsg.value == "SALA_INHABILITADA") alert('La sala est� inhabilitada en la fecha indicada');
        else if(objMsg.value == "SALA_INHABILITADA_HORAS") alert('La sala est� inhabilitada en la fecha y hora indicada');
		

        if (document.getElementById("txhAccion").value=="TERMINAR") {
        	if(document.getElementById("txhMsg").value=="OK_TERMINAR"){
        		alert("La ejecuci�n culmin� satisfactoriamente.");
        		window.opener.fc_Actualizar();
        		window.close();        		
        	}else if (document.getElementById("txhMsg").value=="ERROR_TERMINAR")
        		alert("Error al culminar ejecuci�n de recurso.");
        	else if (document.getElementById("txhMsg").value=="RECURSO_YA_NO_ESTA_EJECUTANDOSE"){
        		alert("El recurso ya no se encuentra Ejecutandose, verifique nuevamente.");
        		window.opener.fc_Actualizar();
        		window.close();
        	}
        }
        

        if (document.getElementById("txhAccion").value=="CANCELAR_EJECUTAR") {
            //Aplicar reserva
			if(document.getElementById("txhMsg").value=="OK_APLICAR"){
				alert("La reserva se inici� satisfactoriamente.");		
			}else if(document.getElementById("txhMsg").value=="ERROR_APLICAR"){
				alert("Problemas al aplicar recurso consulte con el Administrador.");
			}else if(document.getElementById("txhMsg").value=="USU_NO_CONFIG"){
				alert("El usuario no est� configurado\npara aplicar sanciones");
			}else if(document.getElementById("txhMsg").value=="RECURSO_YA_NO_ESTA_RESERVADO"){
				alert("El recurso ya no se encuentra Reservado, verifique nuevamente");
			}else if(document.getElementById("txhMsg").value=="FECHA_HORA_INVALIDA"){
				alert('La fecha u hora de reserva no son v�lidas.');
			}else if(document.getElementById("txhMsg").value=="APL_SANCION"){			
				var msg2 = "<%=(( request.getAttribute("msg2")==null)?"": request.getAttribute("msg2"))%>";
				var msg3 = "<%=(( request.getAttribute("msg3")==null)?"": request.getAttribute("msg3"))%>";
				var msg4 = "<%=(( request.getAttribute("msg4")==null)?"": request.getAttribute("msg4"))%>";			
				alert("Al usuario se le aplic� una sanci�n con "+msg3+" d�as de penalidad \nsu fecha de fin de sanci�n es "+msg4);
			}

			//Cancelar
			if(document.getElementById("txhMsg").value=="OK_CANCELAR"){
				alert("La reserva de cancel� satisfactoriamente.");		
			}else if(document.getElementById("txhMsg").value=="ERROR_CANCELAR"){
				alert("Problemas al cancelar reserva consulte con el Administrador.");
			}else if(document.getElementById("txhMsg").value=="RECURSO_YA_NO_ESTA_RESERVADO_2"){
				alert("El recurso ya no se encuentra Reservado, verifique nuevamente");				
			}else if(document.getElementById("txhMsg").value=="USU_NO_CONFIG_CANCELAR"){
				alert("El usuario no esta configurado\npara aplicar sanciones");
			}else if(document.getElementById("txhMsg").value=="APL_SANCION_CANCELAR"){			
				var msg2 = "<%=(( request.getAttribute("msg2")==null)?"": request.getAttribute("msg2"))%>";
				var msg3 = "<%=(( request.getAttribute("msg3")==null)?"": request.getAttribute("msg3"))%>";
				var msg4 = "<%=(( request.getAttribute("msg4")==null)?"": request.getAttribute("msg4"))%>";			
				alert("Al usuario se le aplico una sanci�n con "+msg3+" dias de penalidad \nsu fecha fin de sanci�n es "+msg4);
			}
			
			if(document.getElementById("txhMsg").value!=""){
				window.opener.fc_Buscar();
				window.close();	
			}
        }
        
		if (document.getElementById("txhAccion").value=='RESERVAR' && (objMsg.value=='ERROR' || objMsg.value == "NOT_ALLOW" || objMsg.value == "FECHA_HORA" || objMsg.value == "YA_EXISTE" || objMsg.value == "SANCION" || objMsg.value == "NO_DISPONIBLE" || objMsg.value == "EXCESO" || objMsg.value == "ERROR_USUARIO" || objMsg.value=="USUARIO_INHABILITADO" )){
			window.close();
		}
        
        document.getElementById("txhOperacion").value="";
        document.getElementById("txhMsg").value="";
		
        
        if (document.getElementById("txhEstado").value != "A"){
        	document.getElementById("txtUserReserva").readOnly = true;
			document.getElementById("txtUserReserva").className = "cajatexto_1";
		}
		else{
			if (document.getElementById("txhAccion").value!='TERMINAR'  && document.getElementById("txhAccion").value!='CANCELAR_EJECUTAR' && document.getElementById("txhAccion").value!='CANCELAR'){				
				document.getElementById("txtUserReserva").value = "";
				document.getElementById("txtUserReserva").disabled = false;
				document.getElementById("txtUserReserva").className = "cajatexto_o";
			}									
		}
        
      var obj = "<%=((request.getAttribute("txtNombre")==null)?"":(String)request.getAttribute("txtNombre"))%>";  
	  if(obj!="")
	    document.getElementById("txtUserReserva").value = obj;
	}
	
	function fc_Horas(){
	
	}
	
	function fc_Salas(){
		if(document.getElementById("codSalas").value!="-1"){
	  		document.getElementById("txhOperacion").value = "TIPOSALA";
	  		
		 	document.getElementById("frmMain").submit();
		}	
	}

	function fc_Terminar(){
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value = "TERMINAR";
			document.getElementById("frmMain").submit();
		}
	}

	function fc_CancelarReserva(){
		if(confirm('�Est� seguro de Cancelar la Reserva?')){
			document.getElementById("txhOperacion").value = "CANCELARESERVA";
			document.getElementById("frmMain").submit();			
		}
	}

	function fc_AplicarReserva(){
		if(confirm('�Est� seguro de Aplicar la Reserva?')){
			document.getElementById("txhOperacion").value = "APLICARESERVA";
			document.getElementById("frmMain").submit();			
		}		
	}
	
	function fc_Grabar(){
		fecha2=document.getElementById("txtFechaInicio").value;
		fecha1=document.getElementById("txhFechaBD").value; 
	    
	    var num=fc_ValidaFechaIniFechaFin(fecha1,fecha2);
	    	  		  	
	    if(document.getElementById("codSalas").value!="-1"){
	    	if(document.getElementById("txtUserReserva").value == ""){
				alert('Debe ingresar un usuario.');
			}
			else{	    
		    	if(num==0){			    				    	
		    		var num1=parseFloat(document.getElementById("codHoraInicio").value);
		    		var num2=parseFloat(document.getElementById("codHoraFin").value);
		    		if(num2!="-1" && num1!="-1"){		    			
		    			if(num1>=num2){			    			
		    				alert('Debe seleccionar una hora de inicio y de fin correctamente.');
				 		}
				 		else{
				 			if(confirm(mstrSeguroGrabar)){
				 				document.getElementById("txhOperacion").value = "GUARDAR";
								document.getElementById("frmMain").submit();
				 			}
						}
					}
					else{					
						if(document.getElementById("codHoraInicio").value=="-1"){
							alert('Debe seleccionar una hora de inicio.');
							document.getElementById("codHoraInicio").focus();
						}
				        else 
				        	if(document.getElementById("codHoraFin").value=="-1"){
				        		alert('Debe seleccionar una hora de fin.');
				        		document.getElementById("codHoraFin").focus();
				        	}
				    }
			    }
			    else{
			    	if(num==1) alert('Debe seleccionar una fecha superior o igual a la de hoy.');
			        else{
			        	alert('Debe ingresar una fecha.');
		        		document.getElementById("txtFechaInicio").focus();
			        }
				}
			}
		}
		else{
			alert('Debe seleccionar una Sala.');
			document.getElementById("codSalas").focus();
		}
		
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_consulta_reserva_salas_agregar.html">
<form:hidden path="operacion" id="txhOperacion" />
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
<form:hidden path="fechaBD" id="txhFechaBD"/>
<form:hidden path="txtVacantes" id="txhTxtVacantes"/>
<form:hidden path="estado" id="txhEstado"/>

<form:hidden path="accion" id="txhAccion"/>
<form:hidden path="codSecuencia" id="txhCodSecuencia"/>
<form:hidden path="codRecurso" id="txhCodRecurso"/>

<form:hidden path="fechaReservSel" id="txhFechaReservSel"/>
<form:hidden path="horaIniSel" id="txhHoraIniSel"/>
<form:hidden path="horaFinSel" id="txhHoraFinSel"/>
<form:hidden path="capacidadSel" id="txhCapacidadSel"/>
<form:hidden path="txtCodUserReserva" id="txhCodUserReserva"/>
<form:hidden path="sedeSel" id="txhSedeSel"/>

	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:3px;margin-top:3px; margin-right:3px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
			 				 	

					<c:choose>
						<c:when test="${control.accion == 'TERMINAR'}">
							<c:out value="Terminar ejecuci�n de recurso" />
						</c:when>
						<c:when test="${control.accion == 'RESERVAR'}">
							<c:out value="Reservar recurso" />						
						</c:when>
						<c:when test="${control.accion == 'CANCELAR_EJECUTAR'}">
							<c:out value="Aplicar o Cancelar reserva de recurso" />							
						</c:when>
						<c:otherwise><c:out value="Agregar Registro de Salas" /></c:otherwise>
					</c:choose>

				 				 	
			 	
		 	</td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
		 </tr>
	</table>
	
	<table><tr height="1px"><td></td></tr></table>
	
	<table class="tablaflotante" style="width:430px;margin-top:5px; margin-left:4px;" cellSpacing="2" cellPadding="2" border="0">  
		<tr>
			<td width="60PX">&nbsp;Usuario :</td>
			<td width="120PX"><form:input path="txtUserReserva" id="txtUserReserva" 
							cssClass="cajatexto" size="21" maxlength="30"/>
			</td>
			<td width="70PX">&nbsp;Cap. Max :</td>	
			<td width="150PX">
				<form:input path="txtCapacidadMaxima" id="txtCapacidadMaxima" 
				readonly="true"
				cssClass="cajatexto_1" size="5" maxlength="5"/>
			</td>
		</tr>
		<tr>			
			<td nowrap="nowrap" align="left" >&nbsp;Nombre :</td>
			<td colspan="3">
				<form:select path="codSalas" id="codSalas" cssClass="cajatexto_o" cssStyle="width:150px" onchange="javascript:fc_Salas();">
					<form:option value="-1">--Seleccione--</form:option>
						<c:if test="${control.codListaSalas!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.codListaSalas}" />
						</c:if>
				</form:select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;Fecha :</td>
			<td>
				<form:input path="txtFechaInicio" id="txtFechaInicio" onkeypress="fc_ValidaFecha('txtFechaInicio');"
					  onblur="fc_ValidaFechaOnblur('txtFechaInicio');"  
					  cssClass="cajatexto_o" size="8" maxlength="10"/>&nbsp;
			  	<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
			  	<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni" alt="Calendario" style='cursor:hand;' align="absmiddle">&nbsp;&nbsp;&nbsp;&nbsp;
			  	</a>
			</td>
			
			<td>&nbsp;Horas :</td>
			<td>
				<form:select path="codHoraInicio"	id="codHoraInicio" cssClass="cajatexto_o"
								cssStyle="width:60px"  onchange="javascript:fc_Horas();">
							<form:option value="-1">--Hora--</form:option>
							<c:if test="${control.listaHoras!=null}">
							<form:options itemValue="idHoraIni" itemLabel="horaIni"
								items="${control.listaHoras}" />
							</c:if>
				</form:select>&nbsp;&nbsp;&nbsp;&nbsp;
				<form:select path="codHoraFin"	id="codHoraFin" cssClass="cajatexto_o"
								cssStyle="width:60px"  onchange="javascript:fc_Horas();">
							<form:option value="-1">--Hora--</form:option>
							<c:if test="${control.listaHoras!=null}">
							<form:options itemValue="idHoraFin" itemLabel="horaFin"
								items="${control.listaHoras}" />
							</c:if>
				</form:select>
			</td>
		</tr>	
	</table>
	
	<table><tr height="5px"><td></td></tr></table>
	<table align=center>
		<tr>
			<td>
	
				<c:if test="${control.accion=='CANCELAR_EJECUTAR'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAplicar','','${ctx}/images/botones/aplicar2.jpg',1)">
					<img alt="Aplicar" align="middle" src="${ctx}/images/botones/aplicar1.jpg" id="imgAplicar" style="cursor:pointer;" onclick="javascript:fc_AplicarReserva();">
				</a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
					<img alt="Cancelar" align="middle" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar" style="cursor:pointer;" onclick="javascript:fc_CancelarReserva();">
				</a>
				&nbsp;
				</c:if>

				<c:if test="${control.accion=='TERMINAR'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgTerminar','','${ctx}/images/botones/terminar2.jpg',1)">
					<img alt="Terminar" align="middle" src="${ctx}/images/botones/terminar1.jpg" id="imgTerminar" style="cursor:pointer;" onclick="javascript:fc_Terminar();">
				</a>
				</c:if>
				
				<c:if test="${control.accion=='RESERVAR' || control.accion==null || control.accion==''}">				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" align="middle" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_Grabar();">
				</a>
				</c:if>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCerrar','','${ctx}/images/botones/cerrar2.jpg',1)">
				<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="imgCerrar" onclick="window.close();" style="cursor:pointer;"></a></td>
		</tr>
	</table>	
</form:form>
<script type="text/javascript">

	if (document.getElementById("txhAccion").value != ""){	
		document.getElementById("codSalas").disabled=true;
		document.getElementById("codHoraInicio").disabled=true;
		document.getElementById("codHoraFin").disabled=true;
		document.getElementById("txtFechaInicio").disabled=true;
		document.getElementById("txtCapacidadMaxima").disabled=true;
		document.getElementById("txtUserReserva").disabled=true;
		document.getElementById("imgFecIni").disabled=true;
		
	}

	Calendar.setup({
		inputField     :    "txtFechaInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
		
</script>
</body>
</html>