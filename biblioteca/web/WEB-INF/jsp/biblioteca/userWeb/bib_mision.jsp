<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad(){
	}	
</script>
</head>
<body>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Misi&oacute;n, Visi&oacute;n y Funciones
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 455px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" bordercolor="blue" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<p align="justify"><span class="TituloBiblio"><br>
				        Misi&oacute;n:</span><br>
				  El Centro de Documentaci&oacute;n e Informaci&oacute;n de TECSUP tiene como misi&oacute;n proporcionar recursos y servicios de informaci&oacute;n de calidad necesarios para el fortalecimiento acad&eacute;mico, que cumplan con las necesidades y expectativas de la comunidad tecnol&oacute;gica y que permitan el desarrollo integral de personas y empresas.<br>
				        <br>
				        <span class="TituloBiblio">Visi&oacute;n:</span><br>
				        Ser un Centro de Documentaci&oacute;n especializado l&iacute;der e innovador en la comunidad tecnol&oacute;gica, caracterizado por brindar servicios de calidad, satisfaciendo y anticipando las necesidades de informaci&oacute;n de sus usuarios.<br>
				        <br>
				        <span class="TituloBiblio">Funciones:</span><br>
				        Seleccionar, adquirir, organizar, preservar y difundir los materiales bibliogr&aacute;ficos para satisfacer la demanda de informaci&oacute;n de sus usuarios. <br>
				      </p>
				</td>
			</tr>
	    </table>
		</div>		

</body>
</html>