<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
	function onLoad()	
	{
		objMsg = document.getElementById("txhMsg");	
		if ( objMsg.value == "OK" ){			
			alert("Se envi� su sugerencia satisfactoriamente.");
			document.getElementById("txtSugerencia").value="";
			document.getElementById("txtMail").value="";
			document.getElementById("codSugerencia").value="";
		}		
		else if ( objMsg.value == "ERROR" ){
			alert('Problemas al Grabar. Consulte con el administrador.');		
		}	
	}

	function fc_ValidaTextoGeneralEmail( strNameObj,strMensaje ){	
		var Obj = document.getElementById(strNameObj);
		var strCadena = new String(Obj.value);
		var s = strCadena;	
		var filter=/^[A-Za-z][A-Za-z0-9_.]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
		if (s.length == 0 ) return true;
		if (filter.test(s))	return true;
		else{
			alert("Ingrese una direccion de correo valida");
			Obj.value = fc_Trim(s);
			Obj.focus();
		}	
		return false;
	}
	
	function imposeMaxLength(Object, MaxLen){
		return (Object.value.length <= MaxLen);
	}
	
	function fc_TipoSugerencia(){
		opcion=document.getElementById("codSugerencia").value;		
		switch(opcion){			
			case document.getElementById("txhCodTipoComentario").value:
			case document.getElementById("txhCodTipoSugerencia").value:				
// 				trMail.style.display="none";
// 				break;
			case document.getElementById("txhCodTipoConsulta").value:
			case document.getElementById("txhCodTipoReclamo").value:
// 				trMail.style.display="";
// 				break;
		}
	}
	
	function fc_Trim(psString){
		return String(psString).replace(/[\s]/g,"");
	}
	
	function fc_EnviarSugerencia(){
		codSugerencia=document.getElementById("codSugerencia").value;
		if(codSugerencia==""){
			alert('Seleccione un Tipo Sugerencia');
			document.getElementById("codSugerencia").focus();
			return false;
		}
		texto=document.getElementById("txtSugerencia").value;
		if(texto=="" || fc_Trim(texto)==""){
			alert('Debe ingresar una sugerencia');
			document.getElementById("txtSugerencia").focus();
			return false;
		}
// 		switch(codSugerencia){			
// 			case document.getElementById("txhCodTipoConsulta").value:
// 			case document.getElementById("txhCodTipoReclamo").value:
				mail=document.getElementById("txtMail").value;
				if(mail=="" || fc_Trim(mail)==""){
					alert('Debe ingresar el valor correspondiente en el campo E-mail');
					document.getElementById("txtMail").focus();
					return false;
				}
// 				break;
// 		}

		if(confirm('�Est� seguro de enviar los datos?')){
			document.getElementById("txhOperacion").value="ENVIAR";
			document.getElementById("frmMain").submit();
		}
	}		
</script>

<style type="text/css">
.cboSeleccion
{	
	BORDER-TOP: #048BBA 1px solid;
	BORDER-left: #048BBA 1px solid;
	BORDER-right: #048BBA 1px solid;
	BORDER-bottom: #048BBA 1px solid;
    FONT-SIZE: 11px;   
	COLOR:black;
    FONT-FAMILY: Tahoma,Arial;
}
</style>

</head>
<body>

	<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_busqueda_inicio.html">
		<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
		<form:hidden path="codPeriodo" id="txhCodPeriodo"/>
		<form:hidden path="selSala" id="txhSelSala"/>
		<form:hidden path="selDomicilio" id="txhSelDomicilio"/>		
		<form:hidden path="msg" id="txhMsg"/>
		<form:hidden path="operacion" id="txhOperacion"/>
		<form:hidden path="codTipoConsulta" id="txhCodTipoConsulta"/>
		<form:hidden path="codTipoComentario" id="txhCodTipoComentario"/>
		<form:hidden path="codTipoSugerencia" id="txhCodTipoSugerencia"/>
		<form:hidden path="codTipoReclamo" id="txhCodTipoReclamo"/>
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Sugerencias
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 355px; width:100%;">
		<table align="center" cellpadding="0" cellspacing="0" width="94%" border="0" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
				<td>
					<table width="500" border="0" cellspacing="1" cellpadding="0">
						<tr>
							<td width="70px">Tipo</td>
							<td width="430px">
                            	<form:select path="codSugerencia" id="codSugerencia" cssClass="cboSeleccion"
                                    cssStyle="width:354px" onchange="javascript:fc_TipoSugerencia();">
                                    <form:option value="">--Seleccione--</form:option>
                                    <c:if test="${control.codListaSugerencia!=null}">
                                        <form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
                                            items="${control.codListaSugerencia}" />
                                    </c:if>
                                </form:select>
                            </td>		
						</tr>
                         <tr>
                        	<td colspan="2" height="4PX">
                            </td>
                        </tr>
                        <tr>
                        	<td valign="top" align="left">
                            	
                            </td>
                            <td>                            	
                                <form:textarea cssClass="texto" cssStyle="width:350px;height:120px;" path="txtSugerencia" 
                                id="txtSugerencia" onkeypress="return imposeMaxLength(this, 1000);"/>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="7PX">
                            </td>
                        </tr>

                        <tr id="trMail" class="texto">
                        	<td>E-mail&nbsp;</td>
                        	<td>                            	
                                <form:input path="txtMail" id="txtMail" onblur="fc_ValidaTextoGeneralEmail(this.id,'');" cssClass="cajatexto" cssStyle="width:210px"/>
                            </td>
                        </tr>                                                                          																
                        <tr>
                        	<td colspan="2" height="7PX"></td>
                        </tr>
                        
                        <tr valign="top">
                        	<td></td>
                            <td >                               
<!--                                 <a onClick="javascript:fc_EnviarSugerencia();"  -->
<!--                                 style='cursor:hand;font-weight: bolder;font-size: 8pt;color: #B50000;font-family: Verdana;text-align: left' >						 -->
<!--                                     Enviar						 -->
<!--                                 </a>								 -->
<%--                                 <img src="${ctx}/images/iconos/email_on.gif" style='cursor:hand;' alt="Correo" onClick="javascript:fc_EnviarSugerencia();"> --%>


				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviar','','${ctx}/images/botones/enviar2.jpg',1)">
			  		<img src="${ctx}/images/botones/enviar1.jpg" id="imgEnviar" alt="Enviar" style='cursor:hand;' onclick="javascript:fc_EnviarSugerencia();">&nbsp;&nbsp;&nbsp;&nbsp;
			  	</a>
			  	
                            </td>				
                        </tr>
                        
					</table>
				</td>
			</tr>
												
			
				
	    </table>
		</div>
		
	</form:form>
	
</body>
</html>