<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}
	function fc_TipoMaterial(){
	
	}
	
	function fc_Linkear(srtUrl){
	 //alert(srtUrl);
	 parent.location.href=srtUrl;
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_otras_fuentes_de_informacion.html">
<table class="tablaborde" style="width:98%" cellSpacing="2" cellPadding="2" border="0" bordercolor='gray'> 
			<tr>
				<td colspan='2' class="Tab_vert" style='cursor:hand;' onclick="javascript:fc_Cambia('0');">OTRAS FUENTES DE INFORMACION</td>					
			</tr>
			
</table>
<table class="tablaborde" style="width:100%" cellSpacing="2" cellPadding="2" border="0" bordercolor=''> 			
			
			<table cellpadding="0" cellspacing="0" ID="Table1" width="99%" border="0px"  class="" style="margin-left:3px">
			<tr>
			 	<td align="left">			
				 	<display:table name="sessionScope.listaOtrasFuentesInformacion" cellpadding="0" cellspacing="0"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  
						style="border: 0px solid #FBF9F9;width:98%;">
					    <display:column property="textOtrasFuentesInformacion" title="WilDATilA"  
								headerClass="grilla" class="tablagrillaenlacebib" style="text-align:center; width:100%"/>
													
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
							
					</display:table>
				</td>
			</tr>
		   </table>
		</table>
</form:form>		
</body>
</html>