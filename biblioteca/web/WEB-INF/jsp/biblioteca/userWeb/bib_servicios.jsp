<%@ include file="/taglibs.jsp"%>
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}	
</script>
</head>
<body>
	
	
		<table cellpadding="0" cellspacing="0" border="0" width="94%" bordercolor="blue" style="margin-left:9px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<span class="">
					 	Servicios
				 	</span>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
		</table>
		<div style="overflow: auto; height: 550px; width:100%;">
		<table cellpadding="0" cellspacing="0" width="94.5%" border="0" bordercolor="blue" style="margin-left:9px;margin-top:5px; margin-right:7px;">
			<tr>
			 	<td width="100%">			
				 	<br>
				 	<p align="justify">
				 		<span class="TituloBiblio">Estante abierto y lectura en sala:</span> Material bibliogr�fico al alcance de los usuarios para su consulta en el ambiente de 
				 		Biblioteca (Revistas, Peri�dicos, Diccionarios, etc.)				 		
				 	</p>
				 	
				 	<p align="justify">
				 		<span class="TituloBiblio">Pr�stamo de libros a domicilio:</span> Servicio realizado mediante lectoras de c�digos de barras por un plazo determinado 
						de acuerdo al tipo de usuario (Alumnos, Personal Docente y Personal Administrativo).
				 	</p>
        
        			<p align="justify">
        			<span class="TituloBiblio">Orientaci�n Bibliogr�fica:</span> Es un servicio pregunta-respuesta que proporciona informaci�n espec�fica inmediata a la demanda.<br>					        		
        			</p>
        			
        			<p align="justify">
        			<span class="TituloBiblio">
        			Pr�stamo interbibliotecario:</span> Permite acceder a otras bibliotecas de acuerdo a un reglamento establecido. Para la lectura en sala de usuarios de otras instituciones se les pide una carta de presentaci�n de su Instituci�n.
        			</p>
        			
        			<p align="justify">
        			<span class="TituloBiblio">Salas de Trabajo en grupo:</span> Salas acondicionadas para el trabajo en equipo como m�ximo de 9 personas.
        			</p>
        			
        			<p align="justify">
        			<span class="TituloBiblio">Difusi�n de informaci�n:</span> A trav�s de boletines y alertas v�a correo electr�nico.
        			</p>
        			
        			<p align="justify">
        			<span class="TituloBiblio">Sala de computadoras:</span> Permite a los usuarios el intercambio de informaci�n de car�cter acad�mico, cient�fico y tecnol�gico. 
        			Los alumnos deber�n registrarse con anticipaci�n.
        			</p>
        			
        			<p align="justify">
        			<span class="TituloBiblio">Acceso a Internet con conexi�n inal�mbrica:</span> Permite a los usuarios el acceso a Internet a trav�s de la conexi�n inal�mbrica en el CEDITEC. 
        			</p>
        			
        			<p align="left"><br>
        			<span style="font-weight: bold;">Servicios en l�nea:</span><br>
        			</p>
        			
        			<p align="justify">
        			<span class="TituloBiblio">Cat�logo en L�nea:</span> Posibilidad de b�squeda de libros, revistas, discos compactos, proyectos, cat�logos y manuales existentes en nuestra biblioteca.
        			</p>
        			
        			
        			<p align="justify">
        			<span class="TituloBiblio">Reservas: </span>Servicio que permitir� a los usuarios la reserva de libros y salas (de estudio y de computadoras).
        			</p>
        			
        			<p align="justify">
        				<span class="TituloBiblio">Biblioteca online:</span> Atenci�n de consultas v�a Internet o correo electr�nico.
        			</p>
        			
        			<p align="justify">
        				<span class="TituloBiblio">Enlaces de Inter�s:</span> Lista de sitios webs de inter�s seg�n la especialidad.
        			</p>
        			
        			<p align="justify">
        			<span class="TituloBiblio">Biblioteca Virtual: </span>Informaci�n actualizada a texto completo (libros y revistas en formato digital) de alta calidad acad�mica.
        			</p>
        			        			
        			<p align="justify">
        			<span class="TituloBiblio">Novedades:</span> Espacio que permitir� dar a conocer las nuevas adquisiciones de biblioteca, noticias sobre cursos, seminarios, etc.
        			</p>
        			
        			
				</td>
			</tr>
	    </table>
		</div>		

</body>
</html>