<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${ctx}/styles/estilo_biblio.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language=javascript>
	function onLoad()
	{
	}
	
	function fc_TipoMaterial(){
	  /*if(document.getElementById("codTipoMaterial").value!="-1")
		{ */ document.getElementById("txhOperacion").value = "TIPOMATERIAL";
	 	     document.getElementById("frmMain").submit();
	 	//}
	  /*else{	document.getElementById("txhOperacion").value = "LIMPIAR";
	 	  	document.getElementById("frmMain").submit();
	  }*/
	  
	}
	function fc_Linkear(srtUrl){
	 //alert("http://"+srtUrl);
	 //parent.location.href=srtUrl;
	 var newUrl="http://"+srtUrl;
	 window.open(newUrl,"Biblioteca",'resizable=1,width=940,height=780,menubar=1,scrollbars=1,toolbar=1,top=1,left=1');
	}
	
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/biblioteca/bib_enlaces_de_interes.html">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codEvaluador" id="txhCodEvaluador"/>
<form:hidden path="codPeriodo" id="txhCodPeriodo"/>

	<table cellpadding="0" cellspacing="0" border="0" width="98%" style="margin-left:5px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq2.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="100%" class="opc_combo">
				 	<font style="">
					 	Enlaces de Inter&eacute;s
				 	</font>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
	</table>
	
	<table width="100%" bgcolor="white"><tr height="2px"><td></td></tr></table>	
	<!-- JCMU 20/01/2009 - El filtro por tipo de material, no tiene sentido para enlaces de inter�s 
	<table background="${ctx}/images/biblioteca/fondosup.jpg" style="width:98%;margin-left:4px" border="0" cellspacing="4" cellpadding="1" class="tabla" height="50px">		
			<tr>
			<td width="10%"></td>
			<td colspan="1" name="tablaNiveles1" id="tablaNiveles1">
			    <form:select path="codTipoMaterial"
						id="codTipoMaterial" cssClass="cajatexto"
						cssStyle="width:200px"  onchange="javascript:fc_TipoMaterial();">
						<form:option value="-1">--Todos--</form:option>
						<c:if test="${control.codListaTipoMaterial!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.codListaTipoMaterial}" />
						</c:if>
					</form:select>
			</td>
											
		</tr>
	</table>
	<table width="100%" bgcolor="white"><tr height="2px"><td></td></tr></table>
		
	<table cellpadding="0" cellspacing="0" border="0" width="98%" style="margin-left:5px;margin-top:4px; margin-right:5px;">
			<tr>
				<td align="left"><img src="${ctx}/images/biblioteca/Flotante/titizq.jpg"></td>
				<td background="${ctx}/images/biblioteca/Flotante/titrepeticion.jpg" width="94%" class="opc_combo">
				 	<font style="">
					 	Resultado de la B&uacute;squeda.
				 	</font>
				</td>				
				<td align="right"><img src="${ctx}/images/biblioteca/Flotante/titder.jpg"></td>
			</tr>
	</table>
	-->
	
	<table class="tabla2" style="width:100%" cellSpacing="2" cellPadding="2" border="0" > 			
		<tr>
		<td>			
			<table cellpadding="0" cellspacing="0" ID="Table1" width="100%" border="0px"  class="" style="margin-left:3px">
			<tr>

			 	<td align="left">			
				 	<display:table name="sessionScope.listaSecciones" cellpadding="0" cellspacing="0"
						decorator="com.tecsup.SGA.bean.TipoTablaDetalleBibliotecaDecorator"  
						style="border: 0px solid #FBF9F9;width:98%;">
					    <display:column property="textEnlacesInteres" title=""  
								headerClass="grilla" class="tablagrillaenlacebib" style="text-align:center; width:100%"/>
													
						<display:setProperty name="basic.show.header" value="false"/>
						<display:setProperty name="basic.empty.showtable" value="true"/>
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						
						<display:setProperty name="paging.banner.full" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.first" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.last" value="<span class='texto'></span>" />
						<display:setProperty name="paging.banner.onepage" value="<span class='texto'></span>" />
							
					</display:table>
					<%request.getSession().removeAttribute("listaSecciones"); %>
				</td>
			</tr>
		   </table>
		</td>
		</tr>
		</table>
</form:form>		
</body>
</html>