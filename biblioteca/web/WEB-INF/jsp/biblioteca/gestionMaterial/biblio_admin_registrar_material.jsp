<%@ include file="/taglibs.jsp"%> 
  
<head>
<script language=javascript>
function onLoad(){
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";
	if(mensaje!="")
		alert(mensaje);
	fc_iniciaParametros();	
}

function fc_Regresar(){

	strParametrosBusqueda = "&prmTxtCodigo="+document.getElementById("prmTxtCodigo").value+
	"&prmTxtNroIngreso="+document.getElementById("prmTxtNroIngreso").value+
	"&prmCboTipoMaterial="+document.getElementById("prmCboTipoMaterial").value+
	"&prmCboBuscarPor="+document.getElementById("prmCboBuscarPor").value+
	"&prmTxtTitulo="+document.getElementById("prmTxtTitulo").value+
	"&prmCboIdioma="+document.getElementById("prmCboIdioma").value+
	"&prmCboAnioIni="+document.getElementById("prmCboAnioIni").value+
	"&prmCboAnioFin="+document.getElementById("prmCboAnioFin").value+
	"&prmTxtFechaReservaIni="+document.getElementById("prmTxtFechaReservaIni").value+
	"&prmTxtFechaReservaFin="+document.getElementById("prmTxtFechaReservaFin").value;

	location.href="${ctx}/biblioteca/biblio_admin_gestion_material.html?"+
	"irRegresar=true&txhCodUsuario="+document.getElementById("usuario").value+
	strParametrosBusqueda;
		
}


	function fc_menu(){
		location.href =  "${ctx}/menuBiblioteca.html";
	}

function validaTexto(id){	

	if(document.getElementById(""+id).value.length > 400)
	{
		str = document.getElementById(""+id).value.substring(0,400);
		document.getElementById(""+id).value=str;
		return false;
	}			
	
}


function fc_validaMaterialRegistrado(){
	if(fc_Trim(document.getElementById("txhCodUnico").value)!="")
	return 1;
	else 
	return 0;
}

function fc_CambiarOpcion(param){

	if(fc_validaMaterialRegistrado()){
		 switch(param){
			case '1':	
			    TablaA.style.display="none";
				TablaB.style.display="";
				TablaC.style.display="none";
				TablaD.style.display="none";
			    break;
			case '2':
			    TablaA.style.display="none";
				TablaB.style.display="none";
				TablaC.style.display="";
				TablaD.style.display="none";
			   	break;	
			   	
		    case '3':
			    TablaA.style.display="none";
				TablaB.style.display="none";
				TablaC.style.display="none";
				TablaD.style.display="";
			   	break;
			   	
			  }
	}else{
		alert(mstrNoSePuedeRealizarLaAccion);
	}

}

function fc_iniciaParametros(){
 	document.getElementById("operacion").value="";
}

function fc_openModificarIngreso(){
	
	url = "${ctx}/biblioteca/biblio_admin_registrar_material_registrar_ingreso.html?"+
	"flgModificarIngreso=true"+
	"&txhCodUnico="+document.getElementById("txhCodUnico").value;
	
	Fc_Popup(url,950,250);
	
}

function fc_seleccionaDescriptor(codUnico){

}

function Fc_Upload(){
		url = "${ctx}/biblioteca/biblio_admin_registrar_material_adjuntar_documentos.html?"+
		"txhCodUnico="+document.getElementById("txhCodUnico").value;		
		Fc_Popup(url,430,180);
}

/*
function fc_openAgregarIngreso(){
	
	url = "${ctx}/biblioteca/biblio_admin_registrar_material_registrar_ingreso.html?"+
	"txhCodUnico="+document.getElementById("txhCodUnico").value+
	"&txhCodUsuario="+document.getElementById("usuario").value;	
	
	Fc_Popup(url,950,250);
}*/

/*
function fc_openAgregarDescriptor(){
	url = "${ctx}/biblioteca/biblio_admin_registrar_material_agregar_descriptores.html?"+
	"txhCodUnico="+document.getElementById("txhCodUnico").value;
	
	Fc_Popup(url,600,300);
}*/

function fc_openDewey(){
	Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_material_dewey.html",600,400);
}

function fc_openAutor(){
	Fc_Popup("${ctx}/biblioteca/biblio_admin_registrar_material_autor.html",600,400);
} 

function Fc_AdjuntarArchivo(){
		if(fc_validaMaterialRegistrado()){
			url = "${ctx}/biblioteca/biblio_adjuntar_archivo_material.html?"+
			"txhCodUnico="+document.getElementById("txhCodUnico").value+
			"&txhCodUsuario="+document.getElementById("usuario").value;
			Fc_Popup(url,450,160);		
		}else{
			alert(mstrNoSePuedeRealizarLaAccion);
		}
		
}

function Fc_ActualizarPadre(){
	
	document.getElementById("operacion").value="irActualizaImg";
	document.getElementById("frmMain").submit();
}

function fc_guardar(){

	if(fc_validaMaterial())
	{
		if(confirm(mstrSeguroGrabar)){
		
			if(fc_Trim(document.getElementById("txhCodUnico").value)!="")
				document.getElementById("operacion").value="irActualizar";
			else 
				document.getElementById("operacion").value="irRegistrar";
						
			document.getElementById("frmMain").submit();
		}	
		
	}
}

function fc_validaMaterial(){
	if(document.getElementById("cboTipoMaterial").value=="")
	{	alert(mstrSeleccioneMaterial);
		document.getElementById("cboTipoMaterial").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtTitulo").value)==""){
		alert(mstrIngreseTitulo);
		document.getElementById("txtTitulo").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txhCodigoAutor").value)==""){
		alert(mstrSeleccioneAutor);		
		return 0;
	}else if(fc_Trim(document.getElementById("txhCodigoDewey").value)==""){
		alert(mstrSeleccioneDewey);		
		return 0;
	}else if(fc_Trim(document.getElementById("txtNroPag").value)==""){
		alert(mstrIngreseNroPagDuracion);
		document.getElementById("txtNroPag").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtFecPublicacion").value)==""){
		alert(mstrIngreseFecPublicacion);
		document.getElementById("txtFecPublicacion").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboIdioma").value)==""){
		alert(mstrSeleccioneIdioma);
		document.getElementById("cboIdioma").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("txtIsbn").value)==""){
		alert(mstrIngreseIsbn);
		document.getElementById("txtIsbn").focus();
		return 0;
	}else if(fc_Trim(document.getElementById("cboSede").value)==""){
		alert(mstrSeleccioneSede);
		document.getElementById("cboSede").focus();
		return 0;
	}
	
	if(fc_Trim(document.getElementById("txhCodUnico").value)=="")
	{
		if(fc_Trim(document.getElementById("cboProcedencia").value)==""){
		alert(mstrSeleccioneProcedencia);
		document.getElementById("cboProcedencia").focus();
		return 0;
		}else if(fc_Trim(document.getElementById("cboPrecio").value)==""){
		alert(mstrSeleccioneMoneda);
		document.getElementById("cboPrecio").focus();
		return 0;
		}else if(fc_Trim(document.getElementById("txtPrecio").value)==""){
		alert(mstrIngreseCantidad);
		document.getElementById("txtPrecio").focus();
		return 0;
		}
	}
	
	if(document.getElementById("chkTipoPrestamoSala").checked==false && document.getElementById("chkTipoPrestamoDomicilio").checked==false ){
		alert(mstrSeleccioneTipoPrestamo);
		document.getElementById("chkTipoPrestamoSala").focus();
		return 0;
	}else if(document.getElementById("Si").checked==false && document.getElementById("No").checked==false){
		alert(mstrSeleccioneAplicaReserva);
		document.getElementById("Si").focus();
		return 0;
	}
	
	return 1;
	
} 

function fc_CambiaPais(){
		document.getElementById("operacion").value="irCambiaPais";
		document.getElementById("frmMain").submit();
}

function Fc_Buscar(){
	location.href="../TCS_SB/Biblioteca_Tecsup.htm";
}

function Fc_Cambia(){
	if(cboTipoApl.value=='02'){location.href="Configuracion_Bandeja_TS.htm";}
}

function Fc_Agregar()
{
	//Fc_Popup("Agregar_Encuesta.htm",700,300);
	location.href="Agregar_Encuesta.htm";
}

function Fc_CambiaTipoMat()
{
	if(cboTipoMat.value=='01')//Libro
	{
		dewey.style.display="";
	}
	else
	{
		dewey.style.display="none";
	}
}
function Fc_CambiaPagina()
{   
	TablaA.style.display="none";
	TablaB.style.display="";
}

function Fc_CambiaDescriptores()
{
	TablaA.style.display="none";
	TablaB.style.display="none";
	TablaC.style.display="";
}

function Fc_CambiaDocumentos()
{
	TablaA.style.display="none";
	TablaB.style.display="none";
	TablaC.style.display="none";
	TablaD.style.display="";
}

 function fc_PermiteNumerosPunto() {
	
	var valido = "0123456789.() ";                     
      if((window.event.keyCode == 209) || (window.event.keyCode == 241)){            
            var intEncontrado = 0;
            //convierte la � en �
            window.event.keyCode = 209;
      }
      else{          
            var ch_Caracter = String.fromCharCode(window.event.keyCode).toUpperCase();
            var intEncontrado = valido.indexOf(ch_Caracter);            
            if (intEncontrado == -1)
            {           
                 window.event.keyCode = 0;          
            }
            else
            {
                 window.event.keyCode = ch_Caracter.charCodeAt();
            }
      }
}

</script>

<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

</head>
<body>
<form:form id="frmMain" commandName="control"
	action="${ctx}/biblioteca/biblio_admin_registrar_material.html">
	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="nomImagen" id="nomImagen" />		
	<form:hidden path="txhCodigoAutor" id="txhCodigoAutor" />
	<form:hidden path="txhCodigoDewey" id="txhCodigoDewey" />
	<form:hidden path="accion" id="accion" />	
	<form:hidden path="txhCodUnico" id="txhCodUnico" />
	<form:hidden path="pathImagen" id="pathImagen" />
	<form:hidden path="usuario" id="usuario" />
	
		<!-- Icono Regresar -->
	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img
				src="${ctx}/images/iconos/Regresar.gif"
				onclick="javascript:fc_menu();" style="cursor: hand"
				alt="Regresar"></td>
		</tr>
	</table>
	<!--Page Title -->
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:11px; margin-top:10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/biblioteca/iconotitulos.jpg"></td>
		 	<td background="${ctx}/images/biblioteca/repeticiontitulos.jpg" width="240px" class="opc_combo"><font style="">Registrar Material Did&aacute;ctico</font></td>
		 	<td align="right"><img src="${ctx}/images/biblioteca/curvatit.jpg"></td>
		 </tr>
	 </table>

	<table background="${ctx}\images\biblioteca\fondosup.jpg" border="0" cellspacing="4" cellpadding="1" class="tabla" style="width:94%;margin-left:11px;"
		height="125px">
		<tr>
		<td>
			<table width="93%" align="center" border="0" bordercolor="red" cellspacing="4" cellpadding="1">
				<tr>
					<td >&nbsp;C&oacute;digo :</td>
					<td>&nbsp;<form:input readonly="readonly" path="txtCodigo"
						id="txtCodigo" cssClass="cajatexto_1" /></td>
					<td>&nbsp;Tipo Material :</td>
					<td colspan="3">&nbsp;<form:select path="cboTipoMaterial"
						id="cboTipoMaterial" cssClass="combo_o" cssStyle="width:150px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstTipoMaterial!=null}">
							<form:options itemValue="codTipoTablaDetalle"
								itemLabel="descripcion" items="${control.lstTipoMaterial}" />
						</c:if>
					</form:select></td>
					<c:if test="${control.txhCodUnico != null || control.txhCodUnico == '' }">
					<td rowspan="4" align=center>&nbsp;
						<c:if test="${control.nomImagen!=null}">
							<img src="<c:out value="${control.pathImagen}" />"
							border="1" width="100px" height="110px">
						</c:if>
						&nbsp;<img
						src="${ctx}\images\iconos\adjuntar.gif" onclick="Fc_AdjuntarArchivo()" alt="Upload"
						style="cursor: hand"></td>
					</c:if>	
		
				</tr>
				<tr>
					<td>&nbsp;T&iacute;tulo :</td>
					<td colspan="5">&nbsp;<form:input path="txtTitulo" id="txtTitulo"
						cssClass="cajatexto_o" cssStyle="height:40px; width:560px;" maxlength="244"  /></td>
				</tr>
				<tr>
					<td>&nbsp;Autor :</td>
					<td colspan="3">&nbsp;<form:input path="txtAutor" id="txtAutor"
						cssClass="cajatexto_1" size="60" readonly="readonly" /> &nbsp;&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
									<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:fc_openAutor();"></a>
					</td>
					<td>&nbsp;Nro. P&aacute;gs/
					&nbsp;Duraci&oacute;n :</td>
					<td><form:input path="txtNroPag" id="txtNroPag"
						onblur="fc_ValidaNumeroOnBlur('txtNroPag');" cssStyle="text-align:center;" 
						 onkeypress="fc_PermiteNumeros();" maxlength="4" cssClass="combo_o" size="4"/></td>
				</tr>
				<tr>
					<td>&nbsp;Dewey :</td>
					<td colspan="3">&nbsp;<form:input path="txtDewey" id="txtDewey"
						cssClass="cajatexto_1" readonly="readonly" size="60" /> &nbsp;&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar1','','${ctx}/images/iconos/buscar2.jpg',1)">
									<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar1" style="CURSOR: pointer" onclick="javascript:fc_openDewey();"></a>
					</td>				
					<td>&nbsp;Total. Vol&uacute;menes:</td>
					<td><form:input path="txtTotalVolumen" id="txtTotalVolumen"
						onblur="fc_ValidaNumeroOnBlur('txtTotalVolumen');" 
						onkeypress="fc_PermiteNumeros();" maxlength="4" readonly="readonly" cssClass="cajatexto_1" 
						cssStyle="text-align:center;" size="4"/></td>
				</tr>
			</table>
		</td>
		</tr>
	</table>
	
	<table><tr><td height="2px"></td></tr></table>
	
	<div id="TablaA">
	<table background="${ctx}\images\biblioteca\fondoinf.jpg" style="width:94%;margin-left:11px" border="0" cellspacing="4" cellpadding="1"
		height="80px" class="tabla">
	<tr>
		<td>
			<table width="93%" align="center" border="0" bordercolor="red" cellspacing="4" cellpadding="1">
				<tr>
					<td width="14%">&nbsp;Editorial :</td>
					<td width="17%">&nbsp;<form:select path="cboEditorial" id="cboEditorial"
							cssClass="cajatexto" cssStyle="width:135px">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.lstEditorial!=null}">
								<form:options itemValue="codTipoTablaDetalle"
									itemLabel="descripcion" items="${control.lstEditorial}" />
							</c:if>
						</form:select>
					</td>
					<td valign="middle" width="1%">&nbsp;</td>
					<td width="13%">&nbsp;Nro. Edici&oacute;n :</td>
					<td>&nbsp;<form:input path="txtNroEdicion" id="txtNroEdicion"
						cssClass="cajatexto" size="24" maxlength="20" 
						  
						   /></td>
					<td valign="middle">&nbsp;</td>
					<td valign="middle">&nbsp;Ciudad:</td>
					<td rowspan="2" valign="top">
						<table border="0" bordercolor="red" cellspacing="0" cellpadding="2">
							<tr>
								<td>
									<form:select path="cboPais" id="cboPais"
										cssClass="cajatexto" cssStyle="width:110px" onchange="fc_CambiaPais()"  >
										<form:option value="" >--Seleccione--</form:option>
										<c:if test="${control.lstPais!=null}">
											<form:options itemValue="paisId" itemLabel="paisDescripcion"
												items="${control.lstPais}" />
										</c:if>
										</form:select>
								</td>
							</tr>
							<tr>
								<td>
									<form:select path="cboCiudad" id="cboCiudad"
										cssClass="cajatexto" cssStyle="width:110px">
										<form:option value="">--Seleccione--</form:option>
										<c:if test="${control.lstCiudad!=null}">
											<form:options itemValue="ciudadId" itemLabel="ciudadDescripcion"
												items="${control.lstCiudad}" />
										</c:if>
									</form:select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;Fec. Publicaci&oacute;n :</td>
					<td>&nbsp;<form:input path="txtFecPublicacion" id="txtFecPublicacion"
						cssClass="combo_o" size="10"  maxlength="10" onkeypress="fc_ValidaFecha('txtFecPublicacion')" onblur="fc_ValidaFechaOnblur('txtFecPublicacion')"   /> &nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCalendar','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgCalendar"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;
					</td>
					<td valign="middle" width="1%">&nbsp;</td>
					<td>&nbsp;URL.Arch. Digital :</td>
					<td colspan="3">&nbsp;<form:input path="txtUrlArchivoDig"
						id="txtUrlArchivoDig" cssClass="cajatexto" size="45" maxlength="70" 
						/></td>
				</tr>
				<tr>
					<td>&nbsp;Idioma :</td>
					<td>&nbsp;<form:select path="cboIdioma" id="cboIdioma"
						cssClass="cajatexto_o" cssStyle="width:135px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstIdioma!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
								items="${control.lstIdioma}" />
						</c:if>
						</form:select>
					</td>
					<td valign="middle" width="1%">&nbsp;</td>
					<td>&nbsp;ISBN :</td>
					<td>&nbsp;<form:input path="txtIsbn" id="txtIsbn"  maxlength="50" 
					 onblur="fc_ValidaNumeroOnBlur('txtIsbn');" 
						 onkeypress="fc_PermiteNumeros();"   cssClass="combo_o"
						size="30" /></td>
					<td valign="middle">&nbsp;</td>
					<td>&nbsp;Formato :</td>
					<td>&nbsp;<form:input path="txtFormato" id="txtFormato"
						cssClass="cajatexto" maxlength="50" size="16" /></td>
				</tr>
				<tr>
					<td>&nbsp;Tipo Video :</td>
					<td>&nbsp;<form:select path="cboTipoVideo" id="cboTipoVideo"
							cssClass="cajatexto" cssStyle="width:135px">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.lstTipoVideo!=null}">
								<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
									items="${control.lstTipoVideo}" />
							</c:if>
						</form:select>
					</td>
					<c:if test="${control.txhCodUnico == null || control.txhCodUnico == '' }">
						<td valign="middle" width="1%">&nbsp;</td>
						<td>&nbsp;Procedencia :</td>
						<td>&nbsp;<form:select path="cboProcedencia" id="cboProcedencia"
								cssClass="combo_o" cssStyle="width:145px">
								<form:option value="">--Seleccione--</form:option>
								<c:if test="${control.lstProcedencia!=null}">
									<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
										items="${control.lstProcedencia}" />
								</c:if>
							</form:select>			
						</td>
					</c:if>
					<c:if test="${control.txhCodUnico == null  || control.txhCodUnico == ''}">
					<td valign="middle" width="8%">&nbsp;</td>
					<td>&nbsp;Precio :</td>
					<td>&nbsp;
					<form:select path="cboPrecio" id="cboPrecio"
						cssClass="combo_o" cssStyle="width:45px">
						<form:option value="">SEL.</form:option>
						<c:if test="${control.lstPrecio!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="dscValor3"
								items="${control.lstPrecio}" />
						</c:if>
					</form:select>&nbsp;<form:input path="txtPrecio" id="txtPrecio"			
						  onkeypress="fc_PermiteNumerosPunto()"  
						  onblur="fc_ValidaDecimalOnBlur('txtPrecio','7','2')"  
						   maxlength="10" cssClass="cajatexto_o" size="7" />
					
					</td>
					</c:if>	
				</tr>
				<tr>
					<td>&nbsp;Tipo Pr&eacute;stamo :</td>
					<td> <form:checkbox path="chkTipoPrestamoSala" id="chkTipoPrestamoSala" value="1"/>Sala&nbsp;
					<form:checkbox path="chkTipoPrestamoDomicilio" id="chkTipoPrestamoDomicilio" value="1" />
					&nbsp;Domicilio</td>
					<td valign="middle" width="1%">&nbsp;</td>
					<td>&nbsp;Aplica Reserva :</td>
					<td><form:radiobutton path="rdoAplicaReserva" id="Si" value="1"/>S&iacute;&nbsp;
					<form:radiobutton path="rdoAplicaReserva" id="No" value="0"/>No</td>
					<td colspan="1">&nbsp;</td>
					
					<td>&nbsp;Sede :</td>
					<td><form:select path="cboSede" id="cboSede"
						cssClass="combo_o" cssStyle="width:90px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.lstSede!=null}">
							<form:options itemValue="dscValor1" itemLabel="descripcion"
								items="${control.lstSede}" />
						</c:if>
					</form:select></td>
					
					
				</tr>
				<tr>
					<td>&nbsp;Ubicaci&oacute;n F&iacute;sica :</td>
					<td colspan="7">&nbsp;<form:input path="txtUbicacionFisica" id="txtUbicacionFisica" cssClass="cajatexto"
							size="120" maxlength="100"  /></td>
				</tr>
				<tr>
					<c:if test="${control.txhCodUnico == null  || control.txhCodUnico == ''}">
						<td>&nbsp;Observaciones :</td>
						<td colspan="7">&nbsp;<form:textarea path="txtObservaciones"
							id="txtObservaciones" cssClass="cajatexto" cols="122" rows="4"  onkeypress="validaTexto('txtObservaciones')" /></td>
					</c:if>		
				</tr>
			</table>
		</td>
		</tr>
	</table>
	</div>
	
	<div id="TablaB" style="display: none;">
	
	<iframe id="iFrameRegistrarIngreso" name="iFrameRegistrarIngreso" frameborder="0"
	height="200px" width="100%"  style="width:94%;margin-left:11px;"
	src="/biblioteca/biblioteca/biblio_admin_registrar_material_iframe_ingreso.html?txhCodigoUnico=${control.txhCodUnico}" >
	</iframe>
	
	
	</div>
	<div id="TablaC" style="display: none;">
	<!-- sssssssss -->
	
		<!-- IFRAME -->
	<iframe id="iFrameDescriptores" name="iFrameDescriptores" frameborder="0"
	height="200px" width="100%"  style="width:94%;margin-left:11px;"
	src="/biblioteca/biblioteca/biblio_admin_registrar_material_iframe_descriptores.html?txhCodigoUnico=${control.txhCodUnico}" >
	</iframe>
	<!-- IFRAME -->
	
	<!-- ssssssssssssssss -->
	</div>
	
	<div id="TablaD" style="display: none;">
	<!-- IFRAME -->
	<iframe id="iFrameDocumentosRelacionados" name="iFrameDocumentosRelacionados" frameborder="0"
	height="200px" width="100%"  style="width:94%;margin-left:11px;"
	src="/biblioteca/biblioteca/biblio_admin_registrar_material_iframe_documentos_relacionados.html?txhCodigoUnico=${control.txhCodUnico}" >
	</iframe>
	<!-- IFRAME -->

	</div>
	<table><tr><td></td></tr></table>
	<table border="0" width="90%" align="center" cellspacing="2" cellpadding="2">
		<tr>
			<td align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}\images\botones\grabar1.jpg" align="absmiddle" alt="Grabar" id="imgGrabar" onclick="javascript:fc_guardar();" style="cursor:pointer;"></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegIngreso','','${ctx}/images/biblioteca/Gest_Mat/regingresos2.jpg',1)">
						<img src="${ctx}/images/biblioteca/Gest_Mat/regingresos1.jpg" align="absmiddle" alt="Registro de Ingresos" id="imgRegIngreso" style="CURSOR: pointer" onclick="javascript:fc_CambiarOpcion('1');"></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDescriptor','','${ctx}/images/biblioteca/Gest_Mat/descriptores2.jpg',1)">
						<img src="${ctx}/images/biblioteca/Gest_Mat/descriptores1.jpg" align="absmiddle" alt="Descriptores" id="imgDescriptor" style="CURSOR: pointer" onclick="javascript:fc_CambiarOpcion('2');"></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDocRel','','${ctx}/images/biblioteca/Gest_Mat/docrelacionados2.jpg',1)">
						<img src="${ctx}/images/biblioteca/Gest_Mat/docrelacionados1.jpg" align="absmiddle" alt="Documentos Relacionados" id="imgDocRel" style="CURSOR: pointer" onclick="javascript:fc_CambiarOpcion('3');"></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
						<img src="${ctx}/images/botones/regresar1.jpg" align="absmiddle" alt="Regresar" id="imgRegresar" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a>				
						
			</td>
		</tr>
	</table>
	
<!-- PARAMETROS DE BUSQUEDA DE LA PAGIN ANTERIOR -->
<form:hidden path="prmTxtCodigo" id="prmTxtCodigo" />
<form:hidden path="prmTxtNroIngreso" id="prmTxtNroIngreso" />
<form:hidden path="prmCboTipoMaterial" id="prmCboTipoMaterial" />
<form:hidden path="prmCboBuscarPor" id="prmCboBuscarPor" />
<form:hidden path="prmTxtTitulo" id="prmTxtTitulo" />
<form:hidden path="prmCboIdioma" id="prmCboIdioma" />
<form:hidden path="prmCboAnioIni" id="prmCboAnioIni" />
<form:hidden path="prmCboAnioFin" id="prmCboAnioFin" />
<form:hidden path="prmTxtFechaReservaIni" id="prmTxtFechaReservaIni" />
<form:hidden path="prmTxtFechaReservaFin" id="prmTxtFechaReservaFin" />

</form:form>
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "txtFecPublicacion",
	ifFormat       :    "%d/%m/%Y",
	daFormat       :    "%d/%m/%Y",
	button         :    "imgCalendar",
	singleClick    :    true

	});
                          
</script>
</body>

