<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

function onLoad(){
	
	var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
	
	if(mensaje!="")
		alert(mensaje);			
	
}

</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain" action="${ctx}/logistica/detalleProveedor.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		<tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="400px" class="opc_combo">
		 		<font style="">Detalle Proveedor
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	<table class="tabla" style="width:98%;margin-top:6px;margin-left:6px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
					cellspacing="2" cellpadding="0" border="0" bordercolor="red">	
		<tr>
			<td style="width: 80px">Proveedor:</td>
			<td width="12%">
				<form:input path="txtRuc" id="txtRuc"
					cssClass="cajatexto_1" readonly="readonly" />
			</td>
			<td><form:input path="txtDesProveedor" id="txtDesProveedor"
				cssClass="cajatexto_1" size="90" readonly="readonly" /></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" class="tabla2" style="margin-left:9px;margin-top:7px">
		<tr height="25">
			<td>Condiciones de Compra</td>
		</tr>
	</table>	

	<div style="overflow: auto; height: 250px; width: 100%">
	<table cellpadding="0" cellspacing="1" style="width:98%;margin-top:6px;margin-bottom:5px;margin-left:6px;border: 1px solid #048BBA" id="tablaBandeja"
		border="0" bordercolor="black">
		<tr>
			<td class="grilla">Condici&oacute;n</td>
			<td class="grilla">Descripci&oacute;n</td>
			<td class="grilla">Descripci&oacute;n Proveedor</td>
		</tr>
		<c:forEach var="objCurrent" items="${control.lstResultado}"
			varStatus="loop">
			<tr class="tablagrilla">
				<td><c:out value="${objCurrent.descripcion}" /></td>
				<td align="Center"><textarea class="cajatexto_1" readonly
					style="width: 200px; height: 30px;"><c:out
					value="${objCurrent.detalleCotizacion}" /></textarea></td>
				<td align="Center"><textarea class="cajatexto_1" readonly
					style="width: 200px; height: 30px;"><c:out
					value="${objCurrent.detalleProveedor}" /></textarea></td>
			</tr>
		</c:forEach>
	</table>			
	</div>
	
	<table cellpadding="0" cellspacing="0" ID="Table1" width="100%">
		<tr>
			<td align="center"><a onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('iCerrarCot','','${ctx}/images/botones/cerrar2.jpg',1)">
			<img alt="Cerrar Cotización" src="${ctx}/images/botones/cerrar1.jpg"
				id="iCerrarCot" style="cursor: pointer;" onclick="window.close();"></a>

			</td>
		</tr>
	</table>

</form:form>
</body>


