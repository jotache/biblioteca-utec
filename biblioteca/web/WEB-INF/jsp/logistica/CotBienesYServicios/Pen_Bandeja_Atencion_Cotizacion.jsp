<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">

	function fc_TipoBien(){
		
		document.getElementById("chkIndInversion").style.display="";
		document.getElementById("lblIndInversion").style.display="";
	}
	
	function fc_TipoActivo(){
		//document.getElementById("chkIndInversion").style.display="none";
		//document.getElementById("lblIndInversion").style.display="none";
	}

	function onLoad(){
		document.getElementById("activo").style.display='none';
		mad.style.display = 'none';
		var objLong = "<%=((request.getAttribute("TIPO")==null)?"":(String)request.getAttribute("TIPO"))%>";
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		  {  TablaFamlilia.style.display = 'inline-block';
		     TablaServicios.style.display = 'none';
		     document.getElementById("radio1").style.display = 'inline-block';
		     if(document.getElementById("txhConsteBienConsumible").value==document.getElementById("txhConsteRadio").value)
		     {
			     fc_TablaBienesConsumibles(objLong);
			     fc_TipoBien();
		     }
		     else{ if(document.getElementById("txhConsteBienActivo").value==document.getElementById("txhConsteRadio").value)
		            { fc_TablaBienesBienesActivo(objLong);
		            fc_TipoActivo();
		            }
		      }
		  }
		else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
			  {  fc_TablaBandejaServicio(objLong);
			     radio1.style.display = 'none';
			  }
		}
		var objLong = "<%=((request.getAttribute("ESTADO")==null)?"":(String)request.getAttribute("ESTADO"))%>";
		if(objLong=="OK")
		{	objMsg = document.getElementById("txhMsg");
		    
			if ( objMsg.value == "OK" )
			{
				//ALQD,12/02/09.DESHABILITANDO EL MENSAJE DE EXITO
				//alert(mstrSeGraboConExito);
				fc_Buscar();
				window.close();
			}
			else{ if ( objMsg.value == "ERROR" )
			       {
				         alert(mstrProblemaGrabar);
			       }
			      else{ if ( objMsg.value == "DUPLICADO" )
			      		{
			            alert(mstrExistenRegistros);
			            }
			            else{ if ( objMsg.value == "DIF_TIPO" )
			            	alert("No puede combinar productos de Folleter�a con otros prductos consumibles.");
			            }
			       }
			}
		 }else if(objLong=="OK1"){
			 objMsg = document.getElementById("txhMsg");
			 if ( objMsg.value == "OK" ){
				 alert(mstrSeGraboConExito);
			 }else if( objMsg.value == "ERROR" ){
				 alert(mstrProblemaGrabar); 
			 }
		 }
		 document.getElementById("txhIndice").value="";
		 document.getElementById("txhCadCodigos").value="";

		if(document.getElementById("txhIndInversion").value=="1")
			document.getElementById("chkIndInversion").checked=true;
	}
	
	function fc_TablaBienesConsumibles(objLong){
    	TablaBandejaBienesActivo.style.display = 'none';
    	TablaBandejaServicio.style.display = 'none';
    	TablaBotonesBienConsumible.style.display = 'inline-block';
    	TablaBandejaBienesConsumibles.style.display = 'inline-block';
    	TablaBotonesBienActivo.style.display = 'none';
    	TablaBotonesServicio.style.display = 'none';
	}
	
	function fc_TablaBienesBienesActivo(objLong){
		TablaBandejaBienesConsumibles.style.display = 'none';
    	TablaBandejaServicio.style.display = 'none';
    	TablaBotonesBienConsumible.style.display = 'none';
    	TablaBotonesBienActivo.style.display = 'inline-block';
    	TablaBandejaBienesActivo.style.display = 'inline-block';
    	TablaBotonesServicio.style.display = 'none';
	}
	
	function fc_TablaBandejaServicio(objLong){
		
		TablaBandejaBienesConsumibles.style.display = 'none';
	    TablaBandejaBienesActivo.style.display = 'none';
	    TablaFamlilia.style.display = 'none';
	    /* ------- Botones -------- */
	    TablaBotonesBienConsumible.style.display = 'none';
	    TablaBotonesBienActivo.style.display = 'none';
	    TablaBotonesServicio.style.display = 'inline-block';
	    TablaBandejaServicio.style.display = 'inline-block';
	    
	}
	
	function fc_Buscar(){
		
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		  {
		    TablaFamlilia.style.display = 'inline-block';
		     TablaServicios.style.display = 'none';
		    
		     if(document.getElementById("txhConsteBienConsumible").value==document.getElementById("txhConsteRadio").value)
		     {
		         fc_TablaBienesConsumibles();
		     }
		     else{ if(document.getElementById("txhConsteBienActivo").value==document.getElementById("txhConsteRadio").value)
		            {
		              fc_TablaBienesBienesActivo();
		             
		            }
		      }
		    
		     document.getElementById("txhOperacion").value = "BUSCAR_BIENES";
			 document.getElementById("frmMain").submit();
			}
		else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
			  {  var num=fc_ValidarCombos();
		    	
		   		 if(num==1)
		   		 {
			     TablaFamlilia.style.display = 'none';
			     TablaServicios.style.display = 'inline-block';
			     radio1.style.display = 'none';
			   	 fc_TablaBandejaServicio();
			   	 
			   	 document.getElementById("txhOperacion").value = "BUSCAR_SERVICIO";
			     document.getElementById("frmMain").submit();
			   }
			}
		  }
	}
	
	function fc_Limpiar(){
		document.getElementById("radio1").style.display = 'none';
		document.getElementById("txtNroRequerimiento").value="";
		document.getElementById("txtUsuSolicitante").value="";
		document.getElementById("codFamilia").value="-1";
		
		document.getElementById("codSubFamilia").value="-1";
		document.getElementById("codGrupoServicio").value="-1";
		document.getElementById("codTipoServicio").value="-1";
		document.getElementById("codCentroCosto").value="-1";
		document.getElementById("txhOperacion").value = "LIMPIAR";
	    document.getElementById("frmMain").submit();
	}

	function fc_Etapa2(param){
		var tipo=document.getElementById("codTipoRequerimiento").value;		
		//0001: Bien ; 0002:Servicio
		if (tipo=='0001'){
		  	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value ;
		    TablaBotonesBienConsumible.style.display = 'inline-block';
		    TablaBotonesBienActivo.style.display = 'none';
		    TablaBandejaBienesConsumibles.style.display = 'inline-block';
	        TablaBandejaBienesActivo.style.display = 'none';
	        
		    document.getElementById("txhOperacion").value = "FAMILY";		    	    	
		}	    
		if(document.getElementById("chkIndInversion").checked==true)
			document.getElementById("txhIndInversion").value="1";
		else
			document.getElementById("txhIndInversion").value="0";
		//document.getElementById("frmMain").submit();
		fc_Buscar();
	}
	
	function fc_Etapa(param){
				
		switch(param){
			case '1':
			  	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value ;
			    TablaBotonesBienConsumible.style.display = 'inline-block';
			    TablaBotonesBienActivo.style.display = 'none';
			    TablaBandejaBienesConsumibles.style.display = 'inline-block';
		        TablaBandejaBienesActivo.style.display = 'none';
		        
			    document.getElementById("txhOperacion").value = "FAMILY";
			    
				if(document.getElementById("chkIndInversion").checked==true){
					document.getElementById("txhIndInversion").value="1";
				}else{
					document.getElementById("txhIndInversion").value="0";
				}
			    
	   			//document.getElementById("frmMain").submit();
	   			
			    break;
			case '2':
			    document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienActivo").value ;
			  	TablaBotonesBienConsumible.style.display = 'none';
			    TablaBotonesBienActivo.style.display = 'inline-block';
			    TablaBandejaBienesConsumibles.style.display = 'none';
		        TablaBandejaBienesActivo.style.display = 'inline-block';
			    document.getElementById("txhOperacion").value = "FAMILY";
	   			//document.getElementById("frmMain").submit();
	   			
			   	break;
		}
	}
	//ALQD,13/08/09.OCULTANDO LOS NUEVOS CONTROLES
	function fc_TipoReq(){
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		  { TablaFamlilia.style.display = 'inline-block';
			TablaServicios.style.display = 'none';
			radio1.style.display = 'inline-block';
			/* ------------------- */
			TablaBandejaBienesConsumibles.style.display = 'inline-block';
		    TablaBandejaBienesActivo.style.display = 'none';
		    TablaBandejaServicio.style.display = 'none';
		    document.getElementById("txt1").value="Cons.Normal: "+document.getElementById("txhPendNormal").value;
		    document.getElementById("txt2a").value="Cons.Inversion: "+document.getElementById("txhPendInversion").value;
		    document.getElementById("txt2b").value="";
		    document.getElementById("txt3").value="Activo: "+document.getElementById("txhPendActivo").value;;
		    fc_Buscar();
		  }
		else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
			{
		 	   TablaFamlilia.style.display = 'none';
			   TablaServicios.style.display = 'inline-block';
			   radio1.style.display = 'none';
			   /* ------TablaBotonesBienActivo--------TablaBotonesServicio-----TablaBotonesBienConsumible----- */
			   TablaBotonesBienActivo.style.display = 'none';
			   TablaBotonesBienConsumible.style.display = 'none';
			   TablaBotonesServicio.style.display = 'inline-block';
			   TablaBandejaBienesConsumibles.style.display = 'none';
		       TablaBandejaBienesActivo.style.display = 'none';
		       TablaBandejaServicio.style.display = 'inline-block';
		       document.getElementById("codGrupoServicio").value="-1";
			   document.getElementById("txt1").value="Serv.Gral.: "+document.getElementById("txhPendGeneral").value;
			   document.getElementById("txt2a").value="";
			   document.getElementById("txt2b").value="Serv.Mantto.: "+document.getElementById("txhPendMantto").value;
			   document.getElementById("txt3").value="Serv.Otro: "+document.getElementById("txhPendOtro").value;
		  	 }
		  }
	}
	
	function fc_Familia(){
		document.getElementById("txhOperacion").value = "FAMILIA";
        document.getElementById("frmMain").submit();
	}
	function fc_GrupoServicio(){
	document.getElementById("txhOperacion").value = "GRUPOSERVICIO";
        document.getElementById("frmMain").submit();
	}
	
	function fc_Cotizacion(){
	}
	function fc_Regresar()
	{
		document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
	}
	function fc_CotizacionExistente(){
	}
	
	function fc_AgregarCotizacion(){
		codSel = fc_GetNumeroSeleccionados();
		if(codSel != "" )
	    {
		   if(confirm(mstrSeguroAgregarCotizacion)){
	
			if(document.getElementById("chkIndInversion").checked==true){
				document.getElementById("txhIndInversion").value="1";
			}else{
				document.getElementById("txhIndInversion").value="0";
			}
			document.getElementById("txhOperacion").value = "AGREGAR_A_COTIZACION";
			document.getElementById("txhIndice").value="";
		    document.getElementById("frmMain").submit();
		   }
	    }
	    else alert(mstrSeleccion);
	}
	
	function fc_EliminarDeBandeja(){
		form = document.forms[0];
		codSel = fc_GetNumeroSeleccionados();
		if(codSel != "" )
	    {
		   if(confirm('�Confirma que desea quitar los elementos seleccionados?')){	
			    form.imgElimBandeja1.disabled=true;
			    if(document.getElementById("chkIndInversion").checked==true){
					document.getElementById("txhIndInversion").value="1";
				}else{
					document.getElementById("txhIndInversion").value="0";
				}
				document.getElementById("txhOperacion").value = "ELIMINAR_DE_BANDEJA";
				document.getElementById("txhIndice").value="";
			    document.getElementById("frmMain").submit();
		   }else{
			   form.imgElimBandeja1.disabled=false;
		   }
	    }
	    else alert(mstrSeleccion);
	}
	
	//ALQD,13/08/09.ENVIANDO NUEVO PARAMETRO
	function fc_VerCotizacionActual(param){
	
	var srtDescripcion="";
	var srtSubTipoReq="";
	var srtIndInversion="";
	if(param=="1")
	 { srtDescripcion="CONSUMIBLE";
	   srtSubTipoReq=document.getElementById("txhConsteRadio").value
	   }
	else{ if(param=="2")
		  { srtDescripcion="ACTIVO";
		    srtSubTipoReq=document.getElementById("txhConsteRadio").value;
		   }
	      else{
	          if(param=="3")
	          { srtDescripcion="SERVICIO";
	            if(document.getElementById("codTipoServicio").value!="-1")
	               srtSubTipoReq=document.getElementById("codTipoServicio").value;
	          }
	      }
	    }
	srtCodTipoReq=document.getElementById("codTipoRequerimiento").value;
	srtCodSede=document.getElementById("txhCodSede").value;
	if(document.getElementById("txhIndInversion").value!=null &&
		document.getElementById("txhIndInversion").value!=""){
		srtIndInversion=document.getElementById("txhIndInversion").value;
	}
	else
	{
		srtIndInversion="0";
	}
	if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
	{  if(document.getElementById("codGrupoServicio").value!="-1"){
	       {   if(document.getElementById("codTipoServicio").value!="-1"){
		         url="${ctx}/logistica/CotizacionActualServicios.html?"
		         +"txhCodUsuario="+document.getElementById("txhCodUsuario").value
		         +"&txhCodSede="+srtCodSede+"&txhCodTipoCotizacion="+srtCodTipoReq
		         +"&txhCodSubTipoCotizacion="+srtSubTipoReq
		    	 +"&txhIndInversion=0";
		    	 window.location.href=url;
		    	 }
		    	 else alert(mstrSelTipoServicio);
	       }
	    }
	    else{ alert(mstrSelGrupoServicio);
	    }
	}
	else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		     { url="${ctx}/logistica/CotizacionActualBienes.html?"
		     	+"txhCodUsuario="+document.getElementById("txhCodUsuario").value
		     	+"&txhCodSede="+srtCodSede+"&txhCodTipoCotizacion="+srtCodTipoReq
		     	+"&txhCodSubTipoCotizacion="+srtSubTipoReq
		     	+"&txhIndInversion="+srtIndInversion;
			  window.location.href=url;
			 }
		}
    }
	//ALQD,07/08/09.PERMITIENDO MULTIPLE SELECCION Y FILTRANDO COTIZACIONES POR TIPO
	function fc_AgregarCotizacionExistente(param){
		codSel = fc_SoloUnRegistro(); //DEVOLVERA LA CADENA REEMPLAZANDO EL & POR *
		if(codSel != "" ){
			srtCodUsuario=document.getElementById("txhCodUsuario").value;
			srtCodSede=document.getElementById("txhCodSede").value;
			srtCodTipoReq=document.getElementById("codTipoRequerimiento").value;
			var srtSubTipoReq="";
			var srtDescripcion="";
			var srtBandera="";
			var srtIndInversion="";
			if(param=="1")
			 {  srtDescripcion="Bienes";
				srtSubTipoReq=document.getElementById("txhConsteRadio").value;
				if(document.getElementById("chkIndInversion").checked==true){
					srtIndInversion="1";
				}else{
					srtIndInversion="0";
				}
			   }
			else{ if(param=="2")
				  { srtDescripcion="Bienes";
				    srtSubTipoReq=document.getElementById("txhConsteRadio").value;
			   		srtIndInversion="";
				   }
			      else{
			          if(param=="3")
			          { srtDescripcion="Servicios";
			            srtSubTipoReq="";
			            srtIndInversion="";
		                if(document.getElementById("codTipoServicio").value!="-1")
		                   srtSubTipoReq=document.getElementById("codTipoServicio").value;
			          }
			      }
			    }
			if(confirm(mstrSeguroAgregarCotExistente)){
				Fc_Popup("${ctx}/logistica/bandejaPenAgregarCotizacionExistente.html"+
					"?txhCodUsuario="+srtCodUsuario+"&txhCodSede="+srtCodSede+
					"&txhCodSubTipoReq="+srtSubTipoReq+"&txhCodTipoReq="+srtCodTipoReq+
					"&txhDscTipoReq="+srtDescripcion+"&txhBandera="+param+
					"&txhCodigos="+codSel+"&txhIndInversion="+srtIndInversion
					,750,400);
			}
		}
		else alert(mstrSeleccion);
	}
	
	function fc_MostrarDetalleRegistroBA(param1){
	 srtCadena=document.getElementById('valHidBa'+param1).value;
	 ArrCodSel = srtCadena.split("|");
	 document.getElementById("txhCodRequerimiento").value=ArrCodSel[0];
	 document.getElementById("txhCodDetalle").value=ArrCodSel[1];
	 document.getElementById("txhCodDetalle").value=document.getElementById("txhCodDetalle").value.replace("$","");
	 
	 mad.style.display = 'inline-block';
	 document.getElementById("txtDescripcion").value=document.getElementById("valHidDscBa"+param1).value;
	 document.getElementById("activo").style.display='inline-block';
	 codRequerimintoDetalle=document.getElementById("txhCodDetalle").value;
	 codRequerimiento=document.getElementById("txhCodRequerimiento").value;
	 document.getElementById("iFrameGrilla").src="${ctx}/logistica/SreqDocumentosDetalle.html?txhCodRequerimiento="+codRequerimiento+
				"&txhCodDetRequerimiento="+codRequerimintoDetalle;
	}
	
	function fc_MostrarDetalleRegistroSE(param1){
		srtCadena=document.getElementById('valHidSe'+param1).value;
		ArrCodSel = srtCadena.split("|");
		document.getElementById("txhCodRequerimiento").value=ArrCodSel[0];
		document.getElementById("txhCodDetalle").value=ArrCodSel[1];
		document.getElementById("txhCodDetalle").value=document.getElementById("txhCodDetalle").value.replace("$","");
		
		mad.style.display = 'inline-block';
		document.getElementById("txtDescripcion").value=document.getElementById("valHidDscSe"+param1).value;
		document.getElementById("activo").style.display='inline-block';
		codRequerimintoDetalle=document.getElementById("txhCodDetalle").value;
		codRequerimiento=document.getElementById("txhCodRequerimiento").value;
		document.getElementById("iFrameGrilla").src="${ctx}/logistica/SreqDocumentosDetalle.html?txhCodRequerimiento="+codRequerimiento+
			"&txhCodDetRequerimiento="+codRequerimintoDetalle;
	}
		
	function fc_AsignarDescripcion(){
	}
	
	function fc_SeleccionarRegistro(strCodNIvel1,obj){
		strCodSel = document.getElementById("txhCadCodigos").value;
		flag=false;
		
		if (strCodSel!='')
		{	ArrCodSel = strCodSel.split("&");
		    ArrMad = strCodSel.split("$");
			strCodSel = "";
			for (i=0;i<=ArrCodSel.length-2;i++)
			{	if (ArrCodSel[i] == strCodNIvel1) flag = true;
				else strCodSel = strCodSel + ArrCodSel[i]+'&';
			}
			
		}else document.getElementById("txhNroCodigos").value="0";
		if (!flag)
		{
			strCodSel = strCodSel + strCodNIvel1 + '&';
		}
		
		document.getElementById("txhCadCodigos").value = strCodSel;
	}

	function fc_GetNumeroSeleccionados()
	{
		strCodSel = document.getElementById("txhCadCodigos").value;
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("&");
			return ArrCodSel.length - 1;
		}
		else return 0;
	}
	//ALQD,06/08/09.DEVUELVE EL PRIMER SELECCIONADO. NO
	//SE MODIFICARA PARA QUE DEVUELVA LA CADENA SIN EL &, CON EL *
	function fc_SoloUnRegistro()
	{
		intNumeroSeleccionados = fc_GetNumeroSeleccionados();
		
		if ( intNumeroSeleccionados == 0)	return "";
		//if ( intNumeroSeleccionados > 1)    return "";
		
		strCodSel = document.getElementById("txhCadCodigos").value;
		//ArrCodSel = strCodSel.split("&");
		var strCadena = strCodSel.split("&").join("*");
		//for(var i=0;i<ArrCodSel.length;i++) {
		//	strCadena = strCadena+ArrCodSel[i]+'*';
		//}
		return strCadena;
	}
		
	function fc_ValidarCombos(){
	    if(document.getElementById("codGrupoServicio").value!="-1"){
	       { return 1;
	       }
	    }
	    else{ alert(mstrSelGrupoServicio);
	          return 0;
	    }
	}
	
	function fc_SoloCotizacion(){
	 if(document.getElementById("checkbox1").checked)
	  {  if(document.getElementById("codGrupoServicio").value!="-1"){
	     document.getElementById("txhValSelec").value="1";
	     document.getElementById("txhOperacion").value = "SOLO_COTIZACION";
		 document.getElementById("frmMain").submit();
		 }
		 else alert(mstrSelGrupoServicio);
	  }
	  else{  document.getElementById("txhOperacion").value = "LLENAR_COMBO";
		     document.getElementById("frmMain").submit();
		     }
	}
</script>
</head>
<!-- ALQD,12/08/09.COLOCANDO RESUMEN SOBRE PEDIDOS PENDIENTES -->
<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/bandejaPenAtencionCotizacion.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="valConsumible" id="txhValConsumible"/>
<form:hidden path="valActivo" id="txhValActivo"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="consteBien" id="txhConsteBien"/>
<form:hidden path="consteServicio" id="txhConsteServicio"/>
<form:hidden path="consteBienConsumible" id="txhConsteBienConsumible"/>
<form:hidden path="consteBienActivo" id="txhConsteBienActivo"/>
<form:hidden path="valRadio" id="txhValRadio"/>
<form:hidden path="codDetalle" id="txhCodDetalle"/>
<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
<form:hidden path="txtComentario" id="txhTxtComentario"/>
<form:hidden path="cadCodigos" id="txhCadCodigos"/>
<form:hidden path="nroCodigos" id="txhNroCodigos"/>
<form:hidden path="indice" id="txhIndice"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="banListaBienesConsumibles" id="txhBanListaBienesConsumibles"/>
<form:hidden path="banListaBienesActivos" id="txhBanListaBienesActivos"/>
<form:hidden path="banListaServicios" id="txhBanListaServicios"/>
<form:hidden path="bandera" id="txhBandera"/>
<form:hidden path="valSelec" id="txhValSelec"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="indInversion" id="txhIndInversion"/><!-- jhpr 2008-09-30 -->
<form:hidden path="cantPendConsNormal" id="txhPendNormal"/>
<form:hidden path="cantPendConsInversion" id="txhPendInversion"/>
<form:hidden path="cantPendActivo" id="txhPendActivo"/>
<form:hidden path="cantPendServGeneral" id="txhPendGeneral"/>
<form:hidden path="cantPendServMantto" id="txhPendMantto"/>
<form:hidden path="cantPendServOtro" id="txhPendOtro"/>

	<table cellpadding="0" cellspacing="0" border="0" width="97%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="750px" class="opc_combo"><font style="">Bandeja de Pendientes de Atenci�n para Cotizaci�n</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
  
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg"
			cellspacing="2" cellpadding="2" border="0" bordercolor="RED">
		
		<tr>
			<td>
				<input type="text" id="txt1"
					readonly value=
					<c:if test="${control.codTipoRequerimiento==null}">""</c:if>
					<c:if test="${control.codTipoRequerimiento=='0001'}">"Cons.Normal: ${control.cantPendConsNormal}"</c:if>
					<c:if test="${control.codTipoRequerimiento=='0002'}">"Serv.Gral.: ${control.cantPendServGeneral}"</c:if>
				/>
			</td>
			<td>Tipo Req. :</td>
			<td align="left">
				<form:select path="codTipoRequerimiento" id="codTipoRequerimiento" cssClass="combo_o"
					cssStyle="width:100px" onchange="javascript:fc_TipoReq();">
					<c:if test="${control.listCodTipoRequerimiento!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
						items="${control.listCodTipoRequerimiento}" />
					</c:if>
				</form:select>
			</td>
			<td>
				<table id="radio1" bordercolor="blue" border="0" width="100%" cellpadding="0" cellspacing="0">
				 <tr>
					<td align="left" width="4%">
						<input Type="radio" checked name="rdoEtapa"
						    <c:if test="${control.consteRadio==control.consteBienConsumible}"><c:out value=" checked=checked " /></c:if>
							id="gbradio1" onclick="fc_Etapa('1');fc_TipoBien();">&nbsp;Consumible&nbsp;
						<input Type="radio" name="rdoEtapa"
							<c:if test="${control.consteRadio==control.consteBienActivo}"><c:out value=" checked=checked " /></c:if>
							id="gbradio2" onclick="fc_Etapa('2');fc_TipoActivo();">&nbsp;Activo&nbsp;
					</td>
				</tr>
				<tr>
					<td>						
					</td>
				</tr>
				</table>
				
				<input type="checkbox" id="chkIndInversion" onclick="fc_Etapa2('1');"/>&nbsp;<label id="lblIndInversion">Inversi�n</label>
				
			</td>
			<td>Nro. Req.:</td>
			<td colspan="2">
				<form:input path="txtNroRequerimiento" id="txtNroRequerimiento" cssClass="cajatexto"
                    onkeypress="fc_ValidaNumerosGuion();"
			    	onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Req.');"
					cssStyle="width:90px" maxlength="15" />
			</td>
		</tr>
		<tr id="TablaFamlilia" name="TablaFamlilia">
			<td>
				<input type="text" id="txt2a"
					readonly value=
					<c:if test="${control.codTipoRequerimiento==null}">""</c:if>
					<c:if test="${control.codTipoRequerimiento=='0001'}">"Cons.Inversi�n: ${control.cantPendConsInversion}"</c:if>
					<c:if test="${control.codTipoRequerimiento=='0002'}">"Serv.Mantto.: ${control.cantPendServMantto}"</c:if>
				/>
			</td>
			<td>Familia</td>
			<td colspan="2">
				<form:select path="codFamilia" id="codFamilia" cssClass="cajatexto"
					cssStyle="width:250px" onchange="javascript:fc_Familia();">
					<form:option  value="-1">--Todos--</form:option>
					<c:if test="${control.listaCodFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion" 
						items="${control.listaCodFamilia}" />
					</c:if>
				</form:select>
			</td>
			<td>Sub Familia</td>
			<td colspan="2">
				<form:select path="codSubFamilia" id="codSubFamilia" cssClass="cajatexto"
					cssStyle="width:230px">
					<form:option  value="-1">--Todos--</form:option>
					<c:if test="${control.listaCodSubFamilia!=null}">
					<form:options itemValue="codUnico" itemLabel="desSubFam"
						items="${control.listaCodSubFamilia}" />
					</c:if>
				</form:select>
			</td>
		</tr>
		<tr id="TablaServicios" name="TablaServicios">
			<td>
				<input type="text" id="txt2b"
					readonly value=
					<c:if test="${control.codTipoRequerimiento==null}">""</c:if>
					<c:if test="${control.codTipoRequerimiento=='0001'}">"Cons.Inversi�n: ${control.cantPendConsInversion}"</c:if>
					<c:if test="${control.codTipoRequerimiento=='0002'}">"Serv.Mantto.: ${control.cantPendServMantto}"</c:if>
				/>
			</td>
			<td>Grupo Servicio:</td>
			<td colspan="2">
				<form:select path="codGrupoServicio" id="codGrupoServicio" cssClass="combo_o"
					cssStyle="width:180px" onchange="javascript:fc_GrupoServicio();">
					<form:option  value="-1">--Seleccione--</form:option>
					<c:if test="${control.listaCodGrupoServicio!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
						items="${control.listaCodGrupoServicio}" />
					</c:if>
				</form:select>
			</td>
			<td>Tipo Servicio</td>
			<td colspan="2">
				<form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto"
					cssStyle="width:250px">
					<form:option  value="-1">--Seleccione--</form:option>
					<c:if test="${control.listaCodTipoServicio!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
						items="${control.listaCodTipoServicio}" />
					</c:if>
				</form:select>
				<br>
				<input Type="checkbox" name="checkbox1"
					 <c:if test="${control.valSelec=='1'}">
					 <c:out value=" checked=checked " /></c:if>
					id="checkbox1" onclick="javascript:fc_SoloCotizacion();">Ver Cotizaci�n&nbsp;
			</td>
		</tr>
		<tr>
			<td width="13%">
				<input type="text" id="txt3"
					readonly value=
					<c:if test="${control.codTipoRequerimiento==null}">""</c:if>
					<c:if test="${control.codTipoRequerimiento=='0001'}">"Activo: ${control.cantPendActivo}"</c:if>
					<c:if test="${control.codTipoRequerimiento=='0002'}">"Serv.Otro: ${control.cantPendServOtro}"</c:if>
				/>
			</td>
			<td width="9%">Centro Costos:</td>
			<td width="40%" colspan="2">
				<form:select path="codCentroCosto" id="codCentroCosto" cssClass="cajatexto"
					cssStyle="width:380px">
					<form:option  value="-1">--Todos--</form:option>
					<c:if test="${control.listaCodCentroCosto!=null}">
					<form:options itemValue="codTecsup" itemLabel="descripcion"
						items="${control.listaCodCentroCosto}" />
					</c:if>
				</form:select>
			</td>
			<td width="9%">U. Solicitante:</td>
			<td width="22%">
				<form:input path="txtUsuSolicitante" id="txtUsuSolicitante" cssClass="cajatexto"
                    onkeypress="fc_ValidaTextoEspecial();"
	                onblur="fc_ValidaSoloLetrasFinal1(this,'U. Solicitante');"
					cssStyle="width:200px" maxlength="50" />
			</td>
			<td align="right" width="6%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
				<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle"
					 style="cursor: pointer; 60px"
					 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle"
					 style="cursor: pointer; margin-right: "
					 onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
			</td>
		</tr>
	</table>
	
<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:97%;margin-left:9px">
	<tr>
		<td colspan="3" id="TablaBandejaBienesConsumibles" width="100%" style="display:none ;">
			<div style="overflow: auto; height:250px;width:100%">
<!-- 			 <table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0"
					 style="border: 1px solid #048BBA;width:98%;margin-left:0px ;margin-top: 6px"> -->
			<table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0" width="1100px"
				style="border: 1px solid #048BBA;margin-left:0px ;margin-top: 6px">
				<tr>
					<td width="5%" class="grilla">Sel.</td>
					<td width="7%" class="grilla">C�digo</td>
					<td width="27%" class="grilla">Producto</td>
					<td width="7%" class="grilla">Unidad<br>Medida</td>
					<td width="7%" class="grilla">Cant.</td>
					<td width="7%" class="grilla">Fec. Entrega<br>Deseada</td>
					<td width="7%" class="grilla">Nro.<br>Prov.</td>
					<td width="12%" class="grilla">U.Solicitante</td>
					<td width="8%" class="grilla">C.Costo</td>
					<td width="12%" class="grilla">Comentarios</td>
				</tr>
				<c:forEach varStatus="loop" var="lista" items="${control.listaBienesConsumibles}">
					<tr class="cajatexto">
						<td align="center" class="tablagrilla" style="width: 5%">
							<input type="checkbox" name="producto"
								id='valBc<c:out value="${loop.index}"/>'
							    onclick="fc_SeleccionarRegistro('<c:out value="${lista.codInvolucrados}" />','<c:out value="${loop.index}"/>');">
						    <input type="hidden" 
								id='indiceBc<c:out value="${loop.index}"/>'
								value='<c:out value="${loop.index}"/>'>
						</td>
						<td align="center" class="tablagrilla" style="width: 7%"> <!-- 11% -->
							<c:out value="${lista.codBien}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 30%"> <!-- 40% -->
							<c:out value="${lista.valor1}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 7%"> <!-- 11% -->
							<c:out value="${lista.valor2}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 7%"> <!-- 11% -->
							<c:out value="${lista.valor3}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 7%"> <!-- 11% -->
							<c:out value="${lista.fechaEntrega}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 7%"> <!-- 11% -->
							<c:out value="${lista.nroProveedores}" />
						</td>
<!-- ALQD,12/11/08.AGREGANDO NUEVAS COLUMNAS -->
						<td align="left" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.desCeco}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscDetalle}" />
						</td>
					</tr>
					</c:forEach>
					
					<c:if test='${control.banListaBienesConsumibles=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="10" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
				</table>
			</div>
		</td>
		
		<td colspan="3" id="TablaBandejaBienesActivo" width="94%" style="display:none ;">
			<div style="overflow: auto; height:250px;width:100%">
			 <table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0" width="1100px"
					 style="border: 1px solid #048BBA;margin-left:0px ;margin-top: 6px">
					<tr>
						<td width="4%" class="grilla">Sel.</td>
						<td width="7%" class="grilla">Nro. Req.</td>
						<td width="17%" class="grilla">U.Solicitante</td>
						
						<td width="5%" class="grilla">C�digo</td>
						<td width="20%" class="grilla">Producto</td>
						<td width="7%" class="grilla">Unidad<br>Medida</td>
						<td width="6%" class="grilla">Cant.</td>
						<td width="7%" class="grilla">Fec. Entrega<br>Deseada</td>
						<td width="7%" class="grilla">Nro.<br>Prov.</td>
						<td width="20%" class="grilla">Comentarios</td>
						
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaBienesActivos}" >
					<tr class="cajatexto" style="cursor: pointer;" ondblclick="fc_MostrarDetalleRegistroBA('<c:out value="${loop.index}"/>');">
						<td align="center" class="tablagrilla" style="width: 4%">
							<input type="checkbox" name="producto"
								id='valBa<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('<c:out value="${lista.codInvolucrados}" />','<c:out value="${loop.index}"/>');">
							<input type="hidden"
								id='valHidBa<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codInvolucrados}" />'>
							<input type="hidden"
								id='valHidDscBa<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.dscDetalle}" />'>
							<input type="hidden"
								id='indiceBa<c:out value="${loop.index}"/>'
								value='<c:out value="${loop.index}"/>'>
						</td>
						<td align="center" class="tablagrilla" style="width: 7%">
							<c:out value="${lista.nroReq}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 17%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 5%">
							<c:out value="${lista.codBien}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.valor1}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 7%">
							<c:out value="${lista.valor2}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 6%">
							<c:out value="${lista.valor3}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 7%">
							<c:out value="${lista.fechaEntrega}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 7%">
							<c:out value="${lista.nroProveedores}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.dscDetalle}" />
						</td>
						
					</tr>
					</c:forEach>
					<c:if test='${control.banListaBienesActivos=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="9" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
				</table>
			 </div>
			</td>
		 
			<td colspan="3" id="TablaBandejaServicio" width="94%" style="display:none ;">
			 <div style="overflow: auto; height:250px;width:100%">
			 <table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0" width="1100px"
					 style="border: 1px solid #048BBA;margin-left:0px ;margin-top: 6px">
					<tr>
						<td width="4%" class="grilla">Sel.</td>
						<td width="7%" class="grilla">Nro. Req.</td>
						<td width="12%" class="grilla">U.Solicitante</td>						
						<td width="10%" class="grilla">Grupo<br>Servicio</td>
						<td width="15%" class="grilla">Tipo Servicio</td>
						<td width="20%" class="grilla">Nombre Servicio</td>
						<td width="7%" class="grilla">Fec. Entrega<br>Deseada</td>
						<td width="5%" class="grilla">Nro.<br>Prov.</td>
						<td width="20%" class="grilla">Comentarios</td>
						
					</tr>
					
					<c:forEach varStatus="loop" var="lista" items="${control.listaServicios}" >
					<tr class="cajatexto" style="cursor: pointer;" ondblclick="fc_MostrarDetalleRegistroSE('<c:out value="${loop.index}"/>');">
						<td align="center" class="tablagrilla" style="width: 4%">
							<input type="checkbox" name="producto"
								id='valSe<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('<c:out value="${lista.codInvolucrados}" />','<c:out value="${loop.index}"/>');">
							<input type="hidden"
								id='valHidSe<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codInvolucrados}" />'>
							<input type="hidden"
								id='valHidDscSe<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.dscDetalle}" />'>
							<input type="hidden"
								id='indiceSe<c:out value="${loop.index}"/>'
								value='<c:out value="${loop.index}"/>'>
						</td>
						<td align="center" class="tablagrilla" style="width: 7%">
							<c:out value="${lista.nroReq}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.valor1}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.valor2}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.nomServicio}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 7%">
							<c:out value="${lista.fechaEntrega}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 5%">
							<c:out value="${lista.nroProveedores}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.dscDetalle}" />
						</td>
						
					</tr>
					</c:forEach>
					<c:if test='${control.banListaServicios=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="8" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
				</table>
		      </div>
			</td>
			
	</tr>
	
<!-- 	<tr><td>&nbsp;</td></tr>  -->
	<tr id="activo" name="activo" style="display:none" >
	   <div id="activo1" name="activo1" style="display:none">
		<table id="mad" name="mad" cellpadding="2" cellspacing="2" border="0" bordercolor="blue" style="width:97%;margin-top:6px;margin-left:9px">
			<tr valign="top">
				<td width="50%" align="left">
					<table cellpadding="0" cellspacing="0" >
					 	<tr>
					 		<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 		<td background="${ctx}/images/Evaluaciones/centro.jpg" width="255px" class="opc_combo">Descripci�n</td>
					 		<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
					 	</tr>
				 	</table>
					<table cellpadding="0" cellspacing="0" class="tabla" style="width:97%; margin-top: 3px">
						<tr>
							<td>
								<form:textarea path="txtDescripcion" id="txtDescripcion" cssClass="cajatexto"
									cssStyle="width:95%;height:75px;" onblur="fc_AsignarDescripcion();"/>
							</td>
						</tr>
					</table>
				</td>
				<td width="50%">
					<table cellpadding="0" cellspacing="0" >
					 	<tr>
					 		<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
					 		<td background="${ctx}/images/Evaluaciones/centro.jpg" width="210px" class="opc_combo">Documentos Relacionados</td>
					 		<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
						 </tr>
					 </table>
					<table cellpadding="0" cellspacing="0" style="width:97%; margin-top: 3px" >
						<tr>
							<td>
								<iframe id="iFrameGrilla" name="iFrameGrilla" frameborder="0" height="80px" width="100%"></iframe>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</div>
	</tr>
	
	<table cellspacing="0" cellpadding="0" border="0" align="center" style="margin-top: 15px;" bordercolor="red">
		<tr>
			<td align="center" id="TablaBotonesBienConsumible" name="TablaBotonesBienConsumible" style="display: none;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/agregarcot2.jpg',1)">
				<img alt="Agregar A Cotizaci�n" src="${ctx}/images/botones/agregarcot1.jpg" id="imggrabar" style="cursor:pointer;" onclick="fc_AgregarCotizacion('1');">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerCot1','','${ctx}/images/botones/vercotactual2.jpg',1)">
				<img alt="Ver Cotizaci�n Actual" src="${ctx}/images/botones/vercotactual1.jpg" id="imgVerCot1" style="cursor:pointer;" onclick="javascript:fc_VerCotizacionActual('1');">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAddCotExsoste1','','${ctx}/images/botones/agregarcotexistente2.jpg',1)">
				<img alt="Agregar A Cotizaci�n Existente" src="${ctx}/images/botones/agregarcotexistente1.jpg" id="imgAddCotExsoste1" style="cursor:pointer;" onclick="javascript:fc_AgregarCotizacionExistente('1');">
				</a>
				
				<!-- QUITAR ITEM DE LA BANDEJA DE SOLICITUD -->
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgElimBandeja1','','${ctx}/images/botones/borraritembandeja2.jpg',1)">
				<img alt="Eliminar de la Bandeja" src="${ctx}/images/botones/borraritembandeja1.jpg" id="imgElimBandeja1" style="cursor:pointer;" onclick="javascript:fc_EliminarDeBandeja();">
				</a>
				
			</td>
			<td align="center" id="TablaBotonesBienActivo" name="TablaBotonesBienActivo" style="display: none;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAddCoti','','${ctx}/images/botones/agregarcot2.jpg',1)">
				<img alt="Agregar A Cotizaci�n" src="${ctx}/images/botones/agregarcot1.jpg" id="imgAddCoti" style="cursor:pointer;" onclick="fc_AgregarCotizacion('2');">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerCot2','','${ctx}/images/botones/vercotactual2.jpg',1)">
				<img alt="Ver Cotizaci�n Actual" src="${ctx}/images/botones/vercotactual1.jpg" id="imgVerCot2" style="cursor:pointer;" onclick="javascript:fc_VerCotizacionActual('2');">
				</a>
				
			</td>
			<td align="center" id="TablaBotonesServicio" name="TablaBotonesServicio" style="display: none;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAddCoti3','','${ctx}/images/botones/agregarcot2.jpg',1)">
				<img alt="Agregar A Cotizaci�n" src="${ctx}/images/botones/agregarcot1.jpg" id="imgAddCoti3" style="cursor:pointer;" onclick="fc_AgregarCotizacion('3');">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerCot3','','${ctx}/images/botones/vercotactual2.jpg',1)">
				<img alt="Ver Cotizaci�n Actual" src="${ctx}/images/botones/vercotactual1.jpg" id="imgVerCot3" style="cursor:pointer;" onclick="javascript:fc_VerCotizacionActual('3');">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAddCotExsoste3','','${ctx}/images/botones/agregarcotexistente2.jpg',1)">
				<img alt="Agregar A Cotizaci�n Existente" src="${ctx}/images/botones/agregarcotexistente1.jpg" id="imgAddCotExsoste3" style="cursor:pointer;" onclick="javascript:fc_AgregarCotizacionExistente('3');">
			    </a>
			</td>
		</tr>
	</table>
	
</table>
</form:form>
</body>
</html>