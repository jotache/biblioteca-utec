<%@ include file="/taglibs.jsp"%>

<head>
<script language="javascript">

	function fc_Cerrar(){
		window.close();
	}

	function onLoad(){ 
		
		var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
		
		if(mensaje!="")
			alert(mensaje);
				
	}

	function fc_Regresar(){
			
			codSede=document.getElementById("txhCodSede").value;
			window.location.href="${ctx}/logistica/bandejaSolSolicitudCotizacion.html?txhCodSede="+codSede;
			
	}
	
	function fc_verProveedor(codCoti,cod,nroDoc,razon){
		Fc_Popup("/SGA/logistica/detalleProveedor.html?prmCodProveedor="+cod+"&prmCodCotizacion="+codCoti+"&prmCodRuc="+nroDoc+"&prmCodDescripcion="+razon,600,400);	
	}

	
</script>
</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/logistica/verAutorizaciones.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodCotizacion" id="txhCodCotizacion" />
	<form:hidden path="txhCodSede" id="txhCodSede" />
	<form:hidden path="txhCodEstCotizacion" id="txhCodEstCotizacion" />

	<!-- 1: INDICA SI EL COMPRADOR TIENE ACCESO A LA PARTE PROVEEDOR -->
	<form:hidden path="flgComprador" id="flgComprador" />
	<form:hidden path="codTipoReq" id="txhCodTipoReq" />


	<input type="hidden" id="txhCodDetCotizacion" name="txhCodDetCotizacion" />
	<input type="hidden" id="txhCantidad" name="txhCantidad" />
	<input type="hidden" id="txhCadCodDet" name="txhCadCodDet" />
	<input type="hidden" id="txhCadCodProv" name="txhCadCodProv" />
	
	<form:hidden path="cadPrecios" id="txhCadPrecios" />

	<div style="overflow: auto; height: 500px;width:100%">	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="margin-left: 9px; margin-top: 0px;width: 80%;">
		<tr>
			<td align="left"><img
				src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px"
				class="opc_combo"><font valign=bottom><c:if test="${control.codTipoReq=='0001'}">Aprobaciones de
			Bienes</c:if><c:if test="${control.codTipoReq=='0002'}">Aprobaciones de Servicios</c:if></font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		height="34px" style="margin-left: 9px; margin-top: 7px">
		<tr>
			<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td width="20px">&nbsp;</td>
					<td><img src="${ctx}/images/Logistica/izq2.jpg" height="34px"
						id="tdProducto01"></td>
					<td bgcolor=#00A2E4 class="opc_combo" height="34px"
						style="cursor: pointer; font-size: 12px;" id="tdProducto02">Productos</td>
					<td><img src="${ctx}/images/Logistica/der2.jpg" height="34px"
						id="tdProducto03"></td>

				</tr>				
			</table>
			</td>
			<td>
			<table cellpadding="0" cellspacing="0" style="display: none;">
				<tr>
					<td><img src="${ctx}/images/Logistica/izq1.jpg" height="34px"
						id="tdCompra01"></td>
					<td bgcolor=#048BBA class="opc_combo"
						style="cursor: pointer; font-size: 12px;" id="tdCompra02">Condiciones
					Compra</td>
					<td><img src="${ctx}/images/Logistica/der1.jpg" height="34px"
						id="tdCompra03"></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" style="width:450px;margin-top:6px;margin-left:9px;" align="center">
		<tr>
			<td>
			<!-- BANDEJA -->
			<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandejaProductos">
				<tr>
					<td class="grilla" width="65%">Usuario Aprobador</td>					
					<td class="grilla" width="35%">Fecha Aprobaci&oacute;n</td>					
				</tr>
				
				<c:forEach var="objAprob" items="${control.lstResultado}" varStatus="loop">
					<tr class="tablagrilla">
						<td><c:out value="${objAprob.dscAprobador}" /></td>
						<td><c:out value="${objAprob.fecAprobacion}" /></td>
					</tr>
					<tr class="tablagrilla">
						<td><c:out value="${objAprob.obsEstado}" /></td>
					</tr>
				</c:forEach>
			</table>
			</td>			
							
		</tr>
	</table>
	
	
	<table align="center">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('iCerrarCot','','${ctx}/images/botones/cerrar2.jpg',1)">
					<img alt="Cerrar" src="${ctx}/images/botones/cerrar1.jpg" id="iCerrarCot" style="cursor: pointer;" onclick="fc_Cerrar();">
				</a>
			</td>
		</tr>
	</table>
	</div>


	<!-- ****************VARIABLES GLOBALES UTILIZADAS********************* -->
	<input type="hidden" name="txhNumGrupos" id="txhNumGrupos"
		value="<c:out value="${nroGrupos}" />" />
	<input type="hidden" name="txhNumItems" id="txhNumItems"
		value="<c:out value="${nroItems}" />" />
	
</form:form>
</body>


