<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		document.getElementById("txhMsg").value = "";
		alert(mstrProblemaGrabar);
		}
		}
		function fc_Grabar()
		{
			var i = 0;
			var numSel = 0;
			cadIndicador = "";
			cadDesripcion = "";
			cadComentario = "";
			codCotizacion = "";
			codCondicion = "";
		
			while (true)
			{
				objIndicador = document.getElementById("in" + i);
				objDescripcion = document.getElementById("des" + i);
				objcomentario = document.getElementById("com" + i);
				objCondicion = document.getElementById("cod" + i);
				objCodCotizacion = document.getElementById("cot" + i);
				if ( objcomentario == null ){ break; }
				if(objIndicador.checked){
					numSel++;
					if(objcomentario.value==""){
					alert(mstrIngrese);
					return;
					}
					if(objCodCotizacion.innerHTML=="")
						codCotizacion = codCotizacion + "-1" + "|";
					else
						codCotizacion = codCotizacion + objCodCotizacion.innerHTML + "|";
						cadIndicador = cadIndicador + objIndicador.value + "|";
						cadDesripcion = cadDesripcion + objDescripcion.innerHTML + "|";
						cadComentario = cadComentario + objcomentario.value+ "|";
						codCondicion = codCondicion + objCondicion.innerHTML + "|";
					}
				i++;
				}
			if ( numSel == 0 )
				{
					alert(mstrSelCalCompetencia);
					return false;
				}
			document.getElementById("txhIndicador").value = cadIndicador;
			document.getElementById("txhDesTipo").value = cadComentario;
			document.getElementById("txhDescripcion").value = cadDesripcion;
			document.getElementById("txhCodCotizacion").value = codCotizacion;
			document.getElementById("txhCodDescripcion").value = codCondicion;
			
			if (confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value = "GRABAR";
			document.getElementById("frmMain").submit();
			}
		}
		
		function fc_Flag(param1){
	    val = document.getElementById("in" + param1).value;
	 	if(val == 0){
			document.getElementById("in" + param1).value = "1";
		}
		else{
		document.getElementById("in" + param1).value = "0";
		}
		}
		
		function fc_Leena(param1){
			if(fc_Trim(document.getElementById("com" + param1).value)!= ""){
				document.getElementById("in"+ param1).checked = "true";
				document.getElementById("in" + param1).value = "1";
			}
			else{
				document.getElementById("in" + param1).value = "0";
			}
		}
		
</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/agregarCondicionCompra.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="descripcion" id="txhDescripcion"/>
	<form:hidden path="indicador" id="txhIndicador"/>
	<form:hidden path="desTipo" id="txhDesTipo"/>
	<form:hidden path="codUsuario" id="txhCodUsuario"/> 
	<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="tot" id="txhTot"/> 
	<form:hidden path="codId" id="txhCodId"/>
	<form:hidden path="codDescripcion" id="txhCodDescripcion"/>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" class="opc_combo" style="width: 290px">
		 		<font style="">Condiciones de Compra</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table style="width:97%;margin-top:6px;margin-left:9px"
		cellspacing="2" cellpadding="0" >
		<tr>
			<td>
				<div style="overflow: auto; height: 195px;width:100%">
				<table cellpadding="0" cellspacing="0" class="tablaflotante" border="0" bordercolor="red" style="width:95%">
					<c:forEach var="lobjCast" items="${control.listcondiciones}" varStatus="loop">
						<tr>
							<td style="width: 5%">
								<input <c:if test="${lobjCast.indSeleccion=='1'}"><c:out value=" checked=checked " /></c:if>
								type="checkbox" id="in${loop.index}" onclick="javascript:fc_Flag('<c:out value="${loop.index}" />');" value="${lobjCast.indSeleccion}"/>
							</td>
							<td style="width: 45%">
								<label id="des${loop.index}" style="HEIGHT:13px;width:175px;" >${lobjCast.desTipoCondicion}</label>
								<label id="cod${loop.index}" style="HEIGHT:13px;width:175px;display:none" >${lobjCast.codTipoCondicion}</label>
								<label id="cot${loop.index}" style="HEIGHT:13px;width:175px;display:none" >${lobjCast.codCondicion}</label>
							</td>
							<td style="width: 50%">
							<textarea maxlength="255" lang="255" id="com${loop.index}" class="texto"  style="HEIGHT:40px;width:200px" onblur="fc_Leena('<c:out value="${loop.index}" />');">${lobjCast.detalleInicial}</textarea>
							</td>
						</tr>
					</c:forEach>
				</table>
				</div>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="botonGrabar" style="cursor:pointer" onclick="javascript:fc_Grabar();"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="botonCancelar" style="cursor:pointer" onclick="javascript:window.close();"></a>
			</td>
		</tr>
	</table>
</form:form>
