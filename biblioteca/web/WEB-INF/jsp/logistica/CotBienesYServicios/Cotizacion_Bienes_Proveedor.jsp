<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<style>
.tablagrilla02
{
    /*BACKGROUND-COLOR: #CEE3F6;*/
    BACKGROUND-COLOR: #C8E8F0;
	FONT-SIZE: 8pt;
    /*TEXT-TRANSFORM: capitalize;*/
    WIDTH: 100%;
    FONT-FAMILY: Arial;
    color: #606060;
    
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
	var seleccionTextArea="";
	var indiceGeneral = "-1";

	function fc_permiteNumerosValEnter(posObjEvt){
		if(window.event.keyCode ==13 ){
		var nom = "txtPrecioUnitario"+posObjEvt;
		
			if(Number(posObjEvt)<Number(document.getElementById("txhNumListaBienes").value))
				document.getElementById(""+nom).focus();
			return true;
		}
		fc_ValidaNumeroDecimal();
	}
	
	function onLoad(){
		
		fc_CambiaBandeja('');
		fc_Calcular();
		
		objMsg=document.getElementById("txhMsg");
		if ( objMsg.value == "OKGRABARPRODUCTOS" ){
			alert(mstrGrabar);
		}
		else
		if ( objMsg.value == "OKGRABARCONDICIONES" ){
			fc_CambiaBandeja('2');
			alert(mstrGrabar);
		}
		else
		if( objMsg.value == "OKENVIAR" ){
			alert(mstrExitoEnvioCot);
			
			if( document.getElementById("flgComprador").value == "1" )
				fc_sel('1');
			else
				fc_Regresar();
		}
		else
		if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
		}
		else
		if ( objMsg.value == "ERRORDETALLES" ){
			alert(mstrIngValAntesEnviDet);
		}
		document.getElementById("txhMsg").value="";
	}
	function fc_ResaltarRegistro(srtIndice){
		num=parseInt(srtIndice)+1;
		srtIndice=num+"";
		srtIndice=fc_Trim(srtIndice);
		trHijo=document.getElementById("tablaBandejaProductos").rows[srtIndice];
		if(indiceGeneral!=srtIndice){
			if(indiceGeneral!="-1")
				trHijoAnt=document.getElementById("tablaBandejaProductos").rows[indiceGeneral];
			for(var j=0;j<5;j++){
				trHijo.cells[j].className="tablagrilla02";
				if(indiceGeneral!="-1")
					trHijoAnt.cells[j].className="tablagrilla";
			}
			indiceGeneral=srtIndice;
		}
	}
	function fc_CambiaProveedor(){
	
		if( document.getElementById("cboProveedor").value != "" )
		{
			document.getElementById("txhOperacion").value="CONSULTAR";
			document.getElementById("frmMain").submit();
		}
		else{
			alert(mstrSelProveedor);
		}
	}
	
	function fc_sel(op){
		
		if(op=="1")
		{
			srtCodCotizacion=document.getElementById("txhCodCotizacion").value;
			srtCodUsuario=document.getElementById("txhCodUsuario").value;
			srtCodEstado = document.getElementById("txhCodEstCotizacion").value;
			srtCodSede = document.getElementById("txhCodSede").value;
			//ALQD,04/02/09.NUEVO PARAMETRO ENVIADO
			srtNumCotizacion =  document.getElementById("txhNumCotizacion").value;
			
			window.location.href="${ctx}/logistica/verCotizaion.html"
					+ "?prmUsuario=" + srtCodUsuario
					+ "&prmCodSede=" + srtCodSede
					+ "&prmCodCotizacion="+srtCodCotizacion
					+ "&txhCodProveedor="+srtCodUsuario
					+ "&prmCodEstCotizacion="+srtCodEstado
					+ "&txhCodTipoReq="+document.getElementById("txhCodTipoReq").value
					+ "&txhNumCotizacion="+srtNumCotizacion;

		}
	}
	
	function fc_CambiaBandeja(strParametro)
	{
		switch (strParametro)
		{
		case "1":
			tablaProductos.style.display="";
			tablaCondicionesCompra.style.display='none';
			tdBotonGrabarProductos.style.display="";
			tdBotonGrabarCondiciones.style.display="none";
			tdEnviar.style.display="";
			document.getElementById("tdProducto01").src="${ctx}/images/Logistica/izq2.jpg";
			document.getElementById("tdProducto02").bgColor="#00A2E4";
			document.getElementById("tdProducto03").src="${ctx}/images/Logistica/der2.jpg";
			document.getElementById("tdCompra01").src="${ctx}/images/Logistica/izq1.jpg";
			document.getElementById("tdCompra02").bgColor="#048BBA";
			document.getElementById("tdCompra03").src="${ctx}/images/Logistica/der1.jpg";
			fc_Calcular();
			break;
		case "2":
			tablaProductos.style.display='none';
			tablaCondicionesCompra.style.display='';
			tdBotonGrabarProductos.style.display="none";
			tdBotonGrabarCondiciones.style.display="";
			tdEnviar.style.display="none";
			document.getElementById("tdProducto01").src="${ctx}/images/Logistica/izq1.jpg";
			document.getElementById("tdProducto02").bgColor="#048BBA";
			document.getElementById("tdProducto03").src="${ctx}/images/Logistica/der1.jpg";
			document.getElementById("tdCompra01").src="${ctx}/images/Logistica/izq2.jpg";
			document.getElementById("tdCompra02").bgColor="#00A2E4";
			document.getElementById("tdCompra03").src="${ctx}/images/Logistica/der2.jpg";
			tablaDetalle.style.display="none";
			break;
		default:
			tablaProductos.style.display='';
			tablaCondicionesCompra.style.display='none';
			tdBotonGrabarProductos.style.display="";
			tdBotonGrabarCondiciones.style.display="none";
			tdEnviar.style.display="";
			fc_Calcular();
			break;
		}
	}
	function fc_Calcular(){
		numeroHijoTabla=document.getElementById("txhNumListaBienes").value;
		for(var i=0; i<numeroHijoTabla;i++){
			fc_AsignarSubTotal(i);
		}
		fc_AsignarTotal();
	}
	function fc_AsignarSubTotal(seleccion){
		cantidad=parseFloat(document.getElementById("txhCantidad"+seleccion).value);
		precio=parseFloat(document.getElementById("txtPrecioUnitario"+seleccion).value);
		subTotal=cantidad*precio;
		document.getElementById('txtSubTotal'+seleccion).value=redondea(subTotal,3);
	}
	function fc_AsignarTotal(){
		//ALQD,16/06/09.SE CALCULA IGV CUANDO ESTA AFECTO Y NO ES PROVEEDOR EXTRANJERO 
		subTotal=0;
		//ALQD,02/06/09.INICIALIZANDO A 0 EL NUEVO CAMPO
		if(document.getElementById("txtOtroCostos").value==null ||
			document.getElementById("txtOtroCostos").value=="")
			 document.getElementById("txtOtroCostos").value="0";
		if(document.getElementById("txhTipoProveedor").value!="0002"){
			igvBD=document.getElementById("txhIgvBD").value;
		}
		else{
			igvBD="0";
		}
		numeroHijoTabla=document.getElementById("txhNumListaBienes").value;
		igv=0.0;
		for(var i=0; i<numeroHijoTabla;i++){
//ALQD,12/11/08. REDONDEANDO A 3 DEC Y 2 DEC. EL TOTAL. ANTES ERA 5
			subTotal=redondea(parseFloat(subTotal)+parseFloat(document.getElementById("txtSubTotal"+i).value),3);
			if(document.getElementById("txhAfecto"+i).value=="S"){
				igv=parseFloat(igv)
					+parseFloat(document.getElementById("txtSubTotal"+i).value)
					*parseFloat(igvBD);
			}
			else{
				igv=parseFloat(igv)+0.0;
			}
		}
		subTotal=parseFloat(subTotal) + parseFloat(document.getElementById("txtOtroCostos").value);
		document.getElementById("txtSubTotal").value=redondea(subTotal,3);
//		igv=parseFloat(subTotal)*parseFloat(igvBD);
		document.getElementById("txtIgv").value=redondea(igv,3);
		total=parseFloat(subTotal)+parseFloat(igv);
		document.getElementById("txtTotal").value=redondea(total,2);
	}
	//redondea el sVal con nDec decimales
	function redondea(sVal, nDec){ 
	    var n = parseFloat(sVal); 
	    var s = "0.00000"; 
	    if (!isNaN(n)){ 
			n = Math.round(n * Math.pow(10, nDec)) / Math.pow(10, nDec); 
			s = String(n); 
			s += (s.indexOf(".") == -1? ".": "") + String(Math.pow(10, nDec)).substr(1);
			s = s.substr(0, s.indexOf(".") + nDec + 1);
	    }
	    return s;
	}
   	function fc_MostrarDetalleRegistro(seleccion, codDetalle){
		seleccionTextArea=seleccion;
		fc_ResaltarRegistro(seleccion);
		fc_MostrarDetallePagina();
		document.getElementById("txtObservaciones").value=document.getElementById("txhDescripcionCot"+seleccion).value;
		document.getElementById("txtObservacionesProveedor").value=document.getElementById("txhDescripcionPro"+seleccion).value;
		document.getElementById("txtObservacionesProveedor").focus();
		//*******************************************************************************
		codCotizacion=document.getElementById("txhCodCotizacion").value;
		condicion="0";
		codUsuario="";
		if(document.getElementById("cboProveedor").value!="")
		   codUsuario=document.getElementById("cboProveedor").value;
		else codUsuario=document.getElementById("txhCodUsuario").value;
		//*******************************************************************************
		document.getElementById("iFrameAdjunto").src="${ctx}/logistica/CotDetBandejaDocumentosByItem.html?txhCodCotizacion="+codCotizacion+
			"&txhCodDetCotizacion="+codDetalle+
			"&txhCondicion=1"+ //condicion+
			"&txhCodUsuario="+codUsuario+
			"&txhTipoAdjunto=0&txhTipoProcendencia=1";//antes:-->txhTipoProcendencia=1
		//*******************************************************************************
		document.getElementById("iFrameAdjuntoProveedor").src="${ctx}/logistica/CotDetBandejaDocumentosByItem.html?txhCodCotizacion="+codCotizacion+
			"&txhCodDetCotizacion="+codDetalle+
			"&txhCondicion="+condicion+
			"&txhCodUsuario="+codUsuario+
			"&txhTipoAdjunto=0&txhTipoProcendencia=2";
		//***********************************************
	}
	function fc_MostrarDetallePagina(){
		tablaDetalle.style.display="";
	}
	function fc_AsignarDescripcion(){
		document.getElementById("txhDescripcionPro"+seleccionTextArea).value=document.getElementById("txtObservacionesProveedor").value;	
	}
	
	function fc_GrabarProductos(){
		if ( document.getElementById("cboMoneda").value == "")
		{
			alert(mstrSeleccioneMoneda);
			return false;
		}
		
		if(document.getElementById("txhNumListaBienes").value > 0){
			fc_DatosBandejaProductos();
			
			if(confirm(mstrSeguroGrabar)){
						document.getElementById("txhOperacion").value="GRABARPRODUCTOS";
						document.getElementById("frmMain").submit();
			}
		 }
		else{
			alert(mstrNoExisteReg);
		}
	 }
   
	function fc_DatosBandejaProductos(){
		
		var cadenaCodDetalle="";
		var cadenaDescripcion="";
		var cadenaPrecios="";
		numeroHijoTabla=document.getElementById("txhNumListaBienes").value;
		for(var i=0; i<numeroHijoTabla;i++){
			if(i==0){
				cadenaCodDetalle=document.getElementById("txhCodigoBien"+i).value+"|";
				cadenaDescripcion=document.getElementById("txhDescripcionPro"+i).value+"|";
				cadenaPrecios=document.getElementById("txtPrecioUnitario"+i).value+"|";
			}
			else{
				cadenaCodDetalle=cadenaCodDetalle+document.getElementById("txhCodigoBien"+i).value+"|";
				cadenaDescripcion=cadenaDescripcion+document.getElementById("txhDescripcionPro"+i).value+"|";
				cadenaPrecios=cadenaPrecios+document.getElementById("txtPrecioUnitario"+i).value+"|";
			}
		}
		document.getElementById("txhCadenaCodDetalle").value=cadenaCodDetalle;
		document.getElementById("txhCadenaDescripcion").value=cadenaDescripcion;
		document.getElementById("txhCadenaPrecios").value=cadenaPrecios;
		document.getElementById("txhCantidad").value=numeroHijoTabla;
		document.getElementById("txhOtroCostos").value=document.getElementById("txtOtroCostos").value;
	
	}
	function fc_GrabarCondiciones(){
		
		if(document.getElementById("txhNumListaCondiciones").value > 0){
			fc_DatosBandejaCondiciones();
			if(confirm(mstrSeguroGrabar)){
				document.getElementById("txhOperacion").value="GRABARCONDICIONES";
				document.getElementById("frmMain").submit();
			}
		}
		else{
			alert(mstrNoExisteReg);
		}
	}
	function fc_DatosBandejaCondiciones(){
		
		var cadenaCodDetalle="";
		var cadenaDescripcion="";
		
		numeroHijoTabla=document.getElementById("txhNumListaCondiciones").value;
		
		for(var i=0; i<numeroHijoTabla;i++){
			if(i==0){
				cadenaCodDetalle=document.getElementById("txhCodigoCondicion"+i).value+"|";
				dsc=document.getElementById("txtDscObservacion"+i);
				if(dsc.value=="" || fc_Trim(dsc.value)==""){
					cadenaDescripcion="$|";
				}
				else{
					cadenaDescripcion=document.getElementById("txtDscObservacion"+i).value+"|";
				}
			}
			else{
				cadenaCodDetalle=cadenaCodDetalle+document.getElementById("txhCodigoCondicion"+i).value+"|";
				dsc=document.getElementById("txtDscObservacion"+i);
				if(dsc.value=="" || fc_Trim(dsc.value)==""){
					cadenaDescripcion=cadenaDescripcion+"$|";
				}
				else{
					cadenaDescripcion=cadenaDescripcion+document.getElementById("txtDscObservacion"+i).value+"|";
				}
			}
		}
		document.getElementById("txhCadenaCodDetalle").value=cadenaCodDetalle;
		document.getElementById("txhCadenaDescripcion").value=cadenaDescripcion;
	}
	function fc_Enviar(){
		if( document.getElementById("cboMoneda").value == ""){
			alert(mstrSeleccioneMoneda);
			return false;
		}
		
		if(confirm(mstrConfEnviarCoti)){
		
			if(document.getElementById("txhNumListaBienes").value > 0){
				fc_DatosBandejaProductos();
				var indice= Number(document.getElementById("txhNumListaBienes").value);
				var estado=true;
				for(j=0;j<indice;j++){
			     if(parseFloat(document.getElementById("txtPrecioUnitario"+j).value)==0)
			       {
			          j=indice;
			          estado=false;
			       }
				}
			  if(!estado)
			  {	if(confirm(mstrConfEnviarPedientesCero))
			    {	document.getElementById("txhOperacion").value="ENVIAR";
			        document.getElementById("frmMain").submit();
			    }
			  }
			  else{  document.getElementById("txhOperacion").value="ENVIAR";
			         document.getElementById("frmMain").submit();
			  }
			}
			else{
				alert(mstrNoExisteReg);
			}
		}
	}
	function fc_Regresar(){
		codSede=document.getElementById("txhCodSede").value;
		window.location.href="${ctx}/logistica/bandejaSolSolicitudCotizacion.html?txhCodSede="+codSede;
		
	}
	</script>
</head>
<body>
<!-- ALQD,16/06/09. LEYENDO EL TIPO DE PROVEEDOR-->
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/CotizacionBienesProveedor.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codCotizacion" id="txhCodCotizacion" />
	<form:hidden path="codProveedor" id="txhCodProveedor" />
	<form:hidden path="numCotizacion" id="txhNumCotizacion" />
	
	<form:hidden path="cadenaCodDetalle" id="txhCadenaCodDetalle" />
	<form:hidden path="cadenaDescripcion" id="txhCadenaDescripcion" />
	<form:hidden path="cadenaPrecios" id="txhCadenaPrecios" />
	<form:hidden path="cantidad" id="txhCantidad" />
	<form:hidden path="codEstCotizacion" id="txhCodEstCotizacion" />
	
	<form:hidden path="numListaBienes" id="txhNumListaBienes" />
	<form:hidden path="numListaCondiciones" id="txhNumListaCondiciones" />
	<!-- para condiciones -->
	<form:hidden path="codDetalle" id="txhCodDetalle" />
	<form:hidden path="flgComprador" id="flgComprador" />
	<form:hidden path="codTipoReq" id="txhCodTipoReq" />
	
	<form:hidden path="igvBD" id="txhIgvBD" />
	<form:hidden path="otroCostos" id="txhOtroCostos"/>
	<form:hidden path="tipoProveedor" id="txhTipoProveedor"/>
	
	<input type="hidden" name="hid_Aprobacion" id="hid_Aprobacion" value="1">
	<c:if test="${control.flgComprador=='1'}">
		<table class="Opciones" width="97%" border="0" bordercolor="black" cellpadding="0" cellspacing="0"
			style="margin-left: 9px;">
			<tr>
				<td align="right">
					<input type="radio" id="Radio1" name="rdoBandeja" onclick="fc_sel('1')" VALUE="Radio1">Comprador
					&nbsp;&nbsp;&nbsp;
					<input checked type="radio" id="rdoBandeja" name="rdoBandeja" onclick="fc_sel('2')" VALUE="rdoBandeja">Proveedor
				</td>
			</tr>
		</table>
	</c:if>	
	<!-- **********************************COMPRADOR**********************************-->
	<div style="overflow: auto; height: 510px;width:100%">
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="margin-left: 9px; margin-top: 0px;width: 80%;">
		<tr>
			<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
				<c:if test="${control.codTipoReq=='0001'}"><font valign=bottom>Cotización de Bienes</font></c:if>
			    <c:if test="${control.codTipoReq=='0002'}"><font valign=bottom>Cotización de Servicios</font></c:if>
				<c:out value="::::> No. ${control.numCotizacion}"></c:out>
			</td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		<tr class="texto">
			<!-- *********************PREGUNTAMOS POR EL PREFIL*********************** -->
			<c:if test="${control.flgComprador=='1'}">
			<td width="85px">Proveedor :</td>
			<td width="300px">
				<form:select path="cboProveedor" id="cboProveedor"
						cssClass="combo_o" cssStyle="width:290px" onchange="fc_CambiaProveedor()" >
						
						<c:if test="${control.listaProveedores!=null}">
							<form:options itemValue="codUnico" itemLabel="razSoc"
								items="${control.listaProveedores}" />
						</c:if>
				</form:select>
			</td>
			</c:if>
			<!-- *********************PREGUNTAMOS POR EL PREFIL*********************** -->
			<td width="85px" align="right">Moneda</td>
			<td width="150px">:
					<form:select path="codMoneda" id="cboMoneda" cssClass="combo_o" cssStyle="width:100px">
						<form:option value="">--Seleccione--</form:option>
						<c:if test="${control.listMonedas!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion"
							items="${control.listMonedas}" />
						</c:if>
					</form:select>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" height="34px" style="margin-left:9px;margin-top:7px">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq2.jpg" height="34px" id="tdProducto01"></td>
						<td bgcolor=#00A2E4 class="opc_combo" onclick="javascript:fc_CambiaBandeja('1');" height="34px" style="cursor: pointer;font-size: 12px;" id="tdProducto02">Productos</td>
						<td><img src="${ctx}/images/Logistica\der2.jpg" height="34px" id="tdProducto03"></td>
					</tr>
				</table>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="${ctx}/images/Logistica\izq1.jpg" height="34px" id="tdCompra01"></td>
						<td bgcolor=#048BBA class="opc_combo" onclick="javascript:fc_CambiaBandeja('2');" style="cursor: pointer;font-size: 12px;" id="tdCompra02">Condiciones Compra</td>
						<td><img src="${ctx}/images/Logistica\der1.jpg" height="34px" id="tdCompra03"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- bandeja de datos Productos -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:97%;margin-top:6px;margin-left:9px;" id="tablaProductos">
		<tr>
			<td>
				<div style="overflow: auto; height: 180px">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandejaProductos">
					<tr>
						<td class="grilla" style="width: 40%">Producto</td>
						<td class="grilla" style="width: 15%">Cantidad</td>
						<td class="grilla" style="width: 15%">Unidad<br>Medida</td>
						<td class="grilla" style="width: 15%"><b>Precio Uni.<br>(Sin IGV)</b></td>
						<td class="grilla" style="width: 15%">Subtotal</td>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaBienesProveedor}"  >
					<tr class="tablagrilla" style="cursor: pointer;" ondblclick="javascript:fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.codBien}" />');">
						<td align="left" style="width: 40%">
							&nbsp;<c:out value="${lista.producto}" />
							<c:if test="${lista.descripcionCot!=''}">
								<strong>
								<c:out value=" (ver detalles)"></c:out>
								</strong>
							</c:if>
							<input type="hidden"
								id='txhAfecto<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.afectoIGV}"/>'>
							<input type="hidden" 
								id='txhCodigoBien<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codBien}" />'>
							<input type="hidden"
								id='txhDescripcionCot<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.descripcionCot}" />'>
							<input type="hidden"
								id='txhDescripcionPro<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.descripcionPro}" />'>
						</td>
						<td align="center" style="width: 15%">
							<c:out value="${lista.cantidad}" />
							<input type="hidden"
								id='txhCantidad<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.cantidad}" />'>
						</td>
						<td align="center" style="width: 15%">
							<c:out value="${lista.dscUnidad}" />
						</td>
						<td align="center" style="width: 15%">
							<input type="text" value='<c:out value="${lista.precioUnitario}" />'
								id='txtPrecioUnitario<c:out value="${loop.index}"/>'
								maxlength="15"
								onkeypress="fc_permiteNumerosValEnter('<c:out value="${loop.index+1}" />');" 
								onblur="fc_ValidaDecimalOnBlur(this.id,'10','5');fc_AsignarSubTotal('<c:out value="${loop.index}"/>');fc_AsignarTotal('<c:out value="${loop.index}"/>');"
								class="cajatexto" style="text-align: right;width: 80%">
						</td>
						<td align="center" style="width: 15%">
							<input type="text" class="cajatexto_1" readonly
								id='txtSubTotal<c:out value="${loop.index}"/>'
								value="" style="text-align:right; width: 80%">
						</td>
					</tr>
					</c:forEach>
					<c:if test='${control.numListaBienes=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="5" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
				</table>
				</div>
			</td>
		</tr>
		<tr class="texto" align="right">
			<td><c:if test='${control.tipoProveedor=="0002"}'>
				Otros gastos de importación :&nbsp;
				<input type="text" id="txtOtroCostos"
				class="cajatexto" style="text-align: right;"
				onblur="fc_ValidaDecimalOnBlur(this.id,'10','3');fc_AsignarTotal();"
				value='<c:out value="${control.otroCostos}"/>'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</c:if>
				<c:if test='${control.tipoProveedor!="0002"}'>
				<input type="hidden" id="txtOtroCostos" value="0" />
				</c:if>
			Sub Total :&nbsp;<input type="text" id="txtSubTotal" class="Texto_moneda1" readonly value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr class="texto" align="right">
			<td>Igv :&nbsp;<input type="text" id="txtIgv" class="Texto_moneda1" readonly value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr class="texto" align="right">
			<td>Total :&nbsp;<input type="text" id="txtTotal" class="Texto_moneda1" readonly value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	</table>
	<!-- bandeja de datos condiciones de compra -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:97%;margin-top:6px;margin-left:9px;" id="tablaCondicionesCompra">
		<tr>
			<td>
				<div style="overflow: auto; height: 250px">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandejaCondiciones">
					<tr>
						<td class="grilla" width="50%">Condición</td>
						<td class="grilla" width="25%">Descripción</td>
						<td class="grilla" width="25%">Observación</td>
					</tr>
					<c:forEach varStatus="loop" var="listaCondiciones" items="${control.listaCondicionesProveedor}" >
					<tr class="tablagrilla">
						<td>
							<c:out value="${listaCondiciones.descripcion}" />
							<input type="hidden"
								id='txhCodigoCondicion<c:out value="${loop.index}"/>'
								value='<c:out value="${listaCondiciones.codBien}" />'>
						</td>
						<td>
							<textarea class="cajatexto_1" readonly cols="40" rows="3"><c:out value="${listaCondiciones.detalleCotizacion}" /></textarea>
						</td>
						<td>
							<textarea class="cajatexto" cols="40" rows="3" id='txtDscObservacion<c:out value="${loop.index}"/>'><c:out value="${listaCondiciones.detalleProveedor}" /></textarea>
						</td>
					</tr>
					</c:forEach>
					<c:if test='${control.numListaCondiciones=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="3" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
				</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="4" id="tablaDetalle" width="97%" border="0" bordercolor="red" class="tabla2" style="display: none;margin-left:6px;">
		<tr>
			<td width="50%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td class="">Observaciones</td>
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<textarea id="txtObservaciones" class="cajatexto_1" readonly style="width:400px;height:65px;"></textarea>
						</td>
					</tr>
				</table>
			</td>
			<td width="50%">
				<table cellpadding="0" cellspacing="0" style="width:100%; margin-top: 4px" >
					<tr>
						<td>
						     <br>
							<iframe id="iFrameAdjunto" name="iFrameAdjunto" frameborder="0" height="120px" width="100%"></iframe>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="50%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td class="">Observaciones Proveedor</td>
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<textarea  class="cajatexto_o" style="width:400px;height:65px;" id="txtObservacionesProveedor" onblur="fc_AsignarDescripcion();"></textarea>
						</td>
					</tr>
				</table>			
			</td>
			<td>
			    <br>
				<iframe id="iFrameAdjuntoProveedor" name="iFrameAdjuntoProveedor" frameborder="0" height="120px" width="100%">
				</iframe>
			</td>
		</tr>
	</table>
	<!-- botoneria -->
	<table><tr height="20px"><td></td></tr></table>
	<table align="center" border="0" bordercolor="red">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar" style="cursor:pointer;" onclick="javascript:fc_Regresar();"></a></td>
				
			<td id="tdBotonGrabarProductos">
				<c:if test="${control.codEstCotizacion!='0003'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar" style="cursor:pointer;" onclick="javascript:fc_GrabarProductos();"></a>
				</c:if>
			</td>

			<td id="tdBotonGrabarCondiciones" style="display: none;">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar1','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar1" style="cursor:pointer;" onclick="javascript:fc_GrabarCondiciones();"></a></td>
				
			<td id="tdEnviar">
				<c:if test="${control.codEstCotizacion!='0003'}">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviar','','${ctx}/images/botones/enviar2.jpg',1)">
				<img alt="Enviar" src="${ctx}/images/botones/enviar1.jpg" id="imgEnviar" style="cursor:pointer;" onclick="javascript:fc_Enviar();"></a>
				</c:if>
			</td>
		</tr>
	</table>
	</div>

</form:form>
</body>