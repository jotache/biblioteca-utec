<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){

			if ( document.getElementById("txhCondicion").value == "1" )
			{
				document.getElementById("icoEliminar1").style.display = "none";
			}
			
			strMsg = fc_Trim(document.getElementById("txhMsg").value);
			if ( strMsg == "ERRORELI" ) alert(mstrNoElimino);
			else if ( strMsg == "OKELI" ) alert(mstrElimino);
			
			document.getElementById("txhMsg").value = "";
		}
		
		function fc_seleccionarRegistro( codReq, codReqDet )
		{
			document.getElementById("txhCodReq").value = codReq;
			document.getElementById("txhCodReqDet").value = codReqDet;
		}
		
		function fc_Eliminar()
		{
			if ( document.getElementById("txhCodReq").value == "" ||
				document.getElementById("txhCodReqDet").value == "")
			{
				alert(mstrSeleccione);
				return false;
			}
			
			if ( !confirm(mstrSeguroEliminar1)) return false;
			
			document.getElementById("txhAccion").value = "ELIMINAR";
			document.getElementById("frmMain").submit();
		}
	</script>
</head>

<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/CotDetBandejaProdByItem.html" >
<form:hidden path="operacion" id="txhAccion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codCotizacionDet" id="txhCodDetCotizacion"/>
<form:hidden path="condicion" id="txhCondicion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codReq" id="txhCodReq"/>
<form:hidden path="codReqDet" id="txhCodReqDet"/>
<form:hidden path="codTipoCotizacion" id="txhCodTipoCotizacion"/>
<form:hidden path="codSubTipoCotizacion" id="txhCodSubTipoCotizacion"/>
<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
	<tr>
		<td class="">Detalle de Producto</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0"	class="tabla" width="95%">
	<tr>
		<td width="95%">
			<div style="overflow: auto; height: 80px; width: 100%">
			<% if ( request.getSession().getAttribute("subTipoCot") != null ){  %>
				<% if ( request.getSession().getAttribute("subTipoCot").equals("0002") ){ %>
				<display:table name="sessionScope.listaProductos" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.ProductoByItemDecorator" 
				style="border: 1px solid #048BBA;width:95%;">
					<display:column property="rbtSelProducto" title="Sel." headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
					<display:column property="cantidad" title="Cant." headerClass="grilla" class="tablagrilla" style="text-align:right;width:10%"/>
					<display:column property="fechaEntrega" title="Fec. Entrega<br>Deseada" headerClass="grilla" class="tablagrilla" style="text-align:center;width:30%"/>
					<display:column property="usuSolicitante" title="Solicitante" headerClass="grilla" class="tablagrilla" style="text-align:center;width:55%"/>

					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
				<%}
				else{
				%>
				<display:table name="sessionScope.listaProductos" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.ProductoByItemDecorator" 
				style="border: 1px solid #048BBA;width:95%;">
					<display:column property="rbtSelProducto" title="Sel." 
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:10%"/>
					<display:column property="nroReq" title="Nro.Req." headerClass="grilla" 
						class="tablagrilla" style="text-align:center;width:20%"/>
					<display:column property="usuSolicitante" title="U.Solicitante" headerClass="grilla" 
						class="tablagrilla" style="text-align:left;width:60%"/>
					<display:column property="cantidad" title="Cant." headerClass="grilla" 
						class="tablagrilla" style="text-right;width:10%"/>
						
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
				<%}%>
			<%} %>
			<%request.getSession().removeAttribute("listaProductos"); %>
			</div>
		</td>
		<td valign="middle" width="5%" align="center">
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoEliminar1','','${ctx}/images/iconos/kitar2.jpg',1)">
			<img src="${ctx}\images\iconos\kitar1.jpg" onclick="javascript:fc_Eliminar();" alt="Eliminar" style="cursor:hand" id="icoEliminar1"></a>
		</td>
	</tr>
</table>
</form:form>
