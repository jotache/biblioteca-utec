<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){

			if ( document.getElementById("txhCondicion").value == "1" )
			{
				document.getElementById("icoAdjuntar").style.display = "none";
				document.getElementById("icoEliminar1").style.display = "none";
				<% if (request.getSession().getAttribute("tipoAdjunto") != null){
					if (request.getSession().getAttribute("tipoAdjunto").equals("1")){ %>
				document.getElementById("icoCopiar1").style.display = "none";
				<% }}%>
			}
			
			strMsg = fc_Trim(document.getElementById("txhMsg").value);
			if ( strMsg == "ERROR" ) alert(mstrNoElimino);
			else if ( strMsg == "OK" ) alert(mstrElimino);
			
			if ( strMsg == "FALLO" ) alert(mstrErrorCopiarDoc);
			else if ( strMsg == "EXITO" ) alert(mstrExitoCopiarDoc);
			
			document.getElementById("txhMsg").value = "";
		}
		
		function fc_seleccionarRegistro(codDetalle)
		{
			document.getElementById("txhCodAdjunto").value = codDetalle;
		}
		
		function fc_Eliminar()
		{

			if (document.getElementById("txhCodAdjunto").value == "" )
			{
				alert(mstrSeleccione);
				return;
			}
			if (!confirm(mstrSeguroEliminar1)) return;
			
			document.getElementById("txhAccion").value ="ELIMINAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_VerDocumento(strNomDoc)
		{
			if ( strNomDoc != "")
			{
<%-- 				strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>"; --%>
				//if ( strRuta != "")
				//{
					//window.open(strRuta + strNomDoc);
					document.getElementById("txhAccion").value = "OPEN_DOCUMENTO";
					document.getElementById("txhDocumentDownload").value = strNomDoc;
					document.getElementById("frmMain").submit();
				//}
			}
			else
			{
				alert(mstrNoExiteDocAdjunto);
			}
		}
		function fc_Adjuntar()
		{
			
			var url = "${ctx}/logistica/CotDetAdjuntarDocumento.html";
			url = url + "?txhCodCotizacion=" + document.getElementById("txhCodCotizacion").value;
			url = url + "&txhCodDetCotizacion=" + document.getElementById("txhCodDetCotizacion").value;
			url = url + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
			url = url + "&txhTipoProcedencia=" + document.getElementById("txhTipoProcedencia").value;
			
			Fc_Popup(url,500,150,"");
		}
		
		function fc_Refrescar()
		{
			document.getElementById("txhAccion").value ="";
			document.getElementById("frmMain").submit();
		}
		
		function fc_Copiar(){
		   document.getElementById("txhAccion").value ="COPIAR";
		   document.getElementById("frmMain").submit();
		}
	</script>
</head>

<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/CotDetBandejaDocumentosByItem.html" >
<form:hidden path="operacion" id="txhAccion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codCotizacionDet" id="txhCodDetCotizacion"/>
<form:hidden path="codAdjunto" id="txhCodAdjunto"/>
<form:hidden path="condicion" id="txhCondicion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="tipoAdjunto" id="txhTipoAdjunto"/>
<form:hidden path="tipoProcedencia" id="txhTipoProcedencia"/>
<form:hidden path="dscTipoProcedencia" id="txhDscTipoProcedencia"/>
<form:hidden path="dscTipoProcedencia2" id="txhDscTipoProcedencia2"/>
<form:hidden path="documentDownload" id="txhDocumentDownload"/>

<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<% if (request.getSession().getAttribute("tipoAdjunto") != null){
			if (request.getSession().getAttribute("tipoAdjunto").equals("1")){ %>
			<td width="50%" valign="top">
		<%	}
			else { %>
			<td width="100%" valign="top">
		<%	}
		   }
		   else{%>
			<td width="100%" valign="top">
		<% } %>
		
			<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
				<tr>
					<td class=""><c:out value="${control.dscTipoProcedencia}" /></td>	
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0"	class="tabla" width="100%">
				<tr>
					<td width="95%">
						<div style="overflow: auto; height: 80px; width: 100%">
							<display:table name="sessionScope.listaDocumentosComp" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.CotizacionDocumentosDecorator"
							style="border: 1px solid #048BBA;width:96%;">
								<display:column property="rbtSelArchivo" title="Sel."
									headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
								<display:column property="dscTipoAdjunto" title="Tipo Adjunto." headerClass="grilla" 
									class="tablagrilla" style="text-align:left;width:90%"/>
								<display:column property="imgVerArchivo" title="Ver" headerClass="grilla" 
									class="tablagrilla" style="text-align:center;width:5%"/>
									
								<display:setProperty name="basic.empty.showtable" value="true"  />
								<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
								<display:setProperty name="paging.banner.placement" value="bottom"/>
								<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
								<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
								<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
								<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
								<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
								<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
								<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
								<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
								<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
								<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
							</display:table>
							<%request.getSession().removeAttribute("listaDocumentosComp"); %>
						</div>
					</td>
					<td valign="middle" width="5%" align="left" id="botones">
						<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoAdjuntar','','${ctx}/images/iconos/adjuntar2.jpg',1)">
						<img src="${ctx}\images\iconos\adjuntar1.jpg" onclick="javascript:fc_Adjuntar();" alt="Adjuntar" style="cursor:hand;" id="icoAdjuntar"></a>
						<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoEliminar1','','${ctx}/images/iconos/kitar2.jpg',1)">
						<img src="${ctx}\images\iconos\kitar1.jpg" onclick="javascript:fc_Eliminar();" alt="Eliminar" style="cursor:hand" id="icoEliminar1"></a>
					</td>
				</tr>
			</table>
		</td>
		<% if (request.getSession().getAttribute("tipoAdjunto") != null){
			if (request.getSession().getAttribute("tipoAdjunto").equals("1")){ %>
		<td width="50%" valign="top" >
			<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
				<tr>
					<td class=""><c:out value="${control.dscTipoProcedencia2}" /></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0"	class="tabla" width="100%">
				<tr>
					<td width="95%">
						<div style="overflow: auto; height: 80px; width: 100%">
							<display:table name="sessionScope.listaDocumentos" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.DocumentosDetalleDecorator"
							style="border: 1px solid #048BBA;width:96%;">
								<display:column property="rbtSelArchivo" title="Sel" 
									headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
								<display:column property="dscTipoAdjunto" title="Tipo Adjunto" headerClass="grilla" 
									class="tablagrilla" style="text-align:left;width:90%"/>
								<display:column property="imgVerArchivo" title="Ver" headerClass="grilla" 
									class="tablagrilla" style="text-align:center;width:5%"/>
									
								<display:setProperty name="basic.empty.showtable" value="true"  />
								<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
								<display:setProperty name="paging.banner.placement" value="bottom"/>
								<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
								<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
								<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
								<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
								<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
								<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
								<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
								<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
								<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
								<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
							</display:table>
							<%request.getSession().removeAttribute("listaDocumentos"); %>
						</div>
					</td>
					<td valign="middle" width="5%" align="right">
						<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoCopiar1','','${ctx}/images/iconos/copiar2.jpg',1)">
						<img src="${ctx}\images\iconos\copiar1.jpg" alt="Copiar" style="cursor:hand" id="icoCopiar1" onclick="fc_Copiar();"></a>
					</td>
				</tr>
			</table>
		</td>
		<%
		 }
		}
		%>
	</tr>
</table>
</form:form>
