<%@page import="com.tecsup.SGA.common.CommonConstants"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">

	var strExtensionDOC = "<%=CommonConstants.GSTR_EXTENSION_DOC%>";   
    var strExtensionXLS = "<%=CommonConstants.GSTR_EXTENSION_XLS%>";
    var strExtensionDOCX = "<%=CommonConstants.GSTR_EXTENSION_DOCX%>";
    var strExtensionXLSX = "<%=CommonConstants.GSTR_EXTENSION_XLSX%>";
	var strExtensionPDF = "<%=CommonConstants.GSTR_EXTENSION_PDF%>";
	strExtensionDOC=strExtensionDOC.toLowerCase();
	strExtensionXLS=strExtensionXLS.toLowerCase();
	strExtensionPDF=strExtensionPDF.toLowerCase();
	function onLoad(){
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" ){
			window.opener.fc_Refrescar();
			alert(mstrGrabar);
			window.close();
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
		else if ( objMsg.value == "ERROR1" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
		document.getElementById("txhMsg").value="";
	}
	
	function fc_Valida(){
		var strExtension;
		var lstrRutaArchivo;
		if(fc_Trim(document.getElementById("txtArchivo").value)==""){
			alert(mstrNoArchivo);
			return false;
		}
		else{
			if(fc_Trim(document.getElementById("txtArchivo").value) != ""){
				lstrRutaArchivo = document.getElementById("txtArchivo").value;
				strExtension = "";
				strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
				
				//**************************
				//obtiene el nombre del archivo
				nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();
				document.getElementById("txhNomArchivo").value=nombre.substring("1");
				//**************************
			
				if ((strExtension != strExtensionDOC)&& (strExtension != strExtensionDOCX) &&(strExtension != strExtensionXLS) && (strExtension != strExtensionXLSX) && (strExtension != strExtensionPDF)){
					alert(mstrExtensionArchivo);
					return false;
				}
				else{ if(strExtension == strExtensionDOC)
				      	document.getElementById("txhExtArchivo").value = strExtension;
				      else{ if(strExtension == strExtensionXLS)
				       	    document.getElementById("txhExtArchivo").value = strExtension;
				       	    else{ if(strExtension == strExtensionPDF)
				       	          document.getElementById("txhExtArchivo").value = strExtension;
				       	        }
				      }
				   }
			}
		
		}
		
		return true;
	}
	
	function fc_Guardar(){
		if (fc_Valida()){
			if (confirm(mstrSeguroGrabar)){
				document.getElementById("txhAccion").value="GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
	}
</script>
</head>

<form:form id="frmMain" commandName="control" action="${ctx}/logistica/CotDetAdjuntarDocumento.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhAccion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codCotizacionDet" id="txhCodDetCotizacion"/>
<form:hidden path="extArchivo" id="txhExtArchivo"/>
<form:hidden path="nomArchivo" id="txhNomArchivo"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="tipoProcedencia" id="txhTipoProcedencia"/>

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px;margin-top:10px">
 <tr>
 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width=300px" class="opc_combo"><font style="">Adjuntar Archivo</font></td>
 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
 </tr>
</table>
<table class="tabla" background="${ctx}/images/Evaluaciones/back-sup.jpg"
	style="width:96%;margin-top:10px; margin-left:10px; height:100%" cellspacing="2" cellpadding="0">
	<tr>
		<td nowrap>&nbsp;&nbsp;Tipo Adjunto :</td>
		<td>
			<form:select path="codTipoAdjunto" id="codTipoAdjunto" cssClass="combo_o"
							cssStyle="width:200px">
			<form:option value="-1">-- Seleccione --</form:option>
			<c:if test="${control.listaTipoAdjunto!=null}">
				<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
				items="${control.listaTipoAdjunto}" />
				</c:if>
			</form:select>
		</td>
	</tr>
	<tr>
		<td nowrap>&nbsp;&nbsp;Archivo :</td>
		<td>
			<input type=file id="txtArchivo" name="txtArchivo" class="cajatexto" style="width:350px;">
		</td>
	</tr>
</table>

<table align="center" style="margin-top: 10px;">
	<tr>
		<td>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_Guardar();" style="cursor:hand" id="imgAgregar" alt="Grabar">&nbsp;</a>
		</td>
		<td>
			<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
			<img ID="Button1" NAME="Button1" src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();"></a>
		</td>
	</tr>
</table>
</form:form>