<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){ 
	
	  var objMadFrog = "<%=((request.getAttribute("WilDATilA")==null)?"":(String)request.getAttribute("WilDATilA"))%>";
	  
	  if(objMadFrog=="OK")
	   { TablaInferior.style.display='inline-block';
	     TablaProveedor.style.display='inline-block';
	     
	   }
	  
	  if(document.getElementById("txhMsg").value=="OK")
	  {  alert(mstrSeGraboConExito);
	     document.getElementById("txhMsg").value="";
	     document.getElementById("txhCodProveedor").value=""; 
	     fc_Buscar();
	  }
	  else{ if(document.getElementById("txhMsg").value=="ERROR")
	          { alert(mstrProblemaGrabar);
	            document.getElementById("txhCodProveedor").value=""; }
	 	 }
	 document.getElementById("txhMsg").value="";
	}

	function fc_Regresar(){
	srtCodSede=document.getElementById("txhCodSede").value;
	location.href="${ctx}/logistica/bandejaSolSolicitudCotizacion.html?txhCodSede="+srtCodSede ;
	
	
	}
    function fc_Buscar(){
	/*srtCodSede=document.getElementById("txhCodSede").value;
	location.href="${ctx}/logistica/bandejaSolSolicitudCotizacion.html?txhCodSede="+srtCodSede;*/
	
	document.getElementById("txhOperacion").value = "BUSCAR";
    document.getElementById("frmMain").submit();
	}

	function fc_VerCondCompra(){
	//TablaInferior.style.display='inline-block';
	
	 if(document.getElementById("txhCodProveedor").value!="")
	 	{ /*if(document.getElementById("txhDscNroOrden").value!="Sin Orden")
	 	   {*/
	 	     //if(confirm(mstrGenerarOrden)){ 
	 	       document.getElementById("txhOperacion").value = "GENERAR";
		   	   document.getElementById("frmMain").submit();
		   	 //}
		   /*}
		   else alert("Es Sin Orden");*/
	 	}
	 else alert(mstrSeleccione);	
	}

	function fc_SeleccionarRegistro(srtCodProv, srtDscProveedor, srtDscNroOrden){
	 document.getElementById("txhCodProveedor").value=srtCodProv;
	 document.getElementById("txhDscProveedor").value=srtDscProveedor;
	 document.getElementById("txhDscNroOrden").value=srtDscNroOrden;
	}
	
	function fc_GuardarOrdenCompra(){
	 
	 
	 if(confirm(mstrGuardarOrden)){
	 fc_Encadenar();
	 document.getElementById("txhOperacion").value = "GUARDAR";
     document.getElementById("frmMain").submit();
     }
	}
	
	function fc_Encadenar(){
	 srtLong=parseInt(document.getElementById("txhLongitud").value);
	 
	 var cadCodDetalle="";
	 var cadCodDscDetalle="";
	  for ( a = 0; a< srtLong; a++ ) {
	        
	        if(a==0)
	         {	
	            cadCodDetalle=document.getElementById("codDetalle"+a).value ;
	            cadCodDscDetalle=document.getElementById("TextArea3"+a).value ;
	         }
	       	else 
	        {
	         cadCodDetalle=cadCodDetalle + "|" + document.getElementById("codDetalle"+a).value;
	         cadCodDscDetalle=cadCodDscDetalle + "|" +
	                                          document.getElementById("TextArea3"+a).value;
	        }
	          
	  }
	  
	  cadCodDetalle=fc_Trim(cadCodDetalle);
	  cadCodDscDetalle=fc_Trim(cadCodDscDetalle);
		
	 document.getElementById("txhCadCodigosDetalle").value=cadCodDetalle;
	 document.getElementById("txhCadDescripcionDetalle").value=cadCodDscDetalle;
	// alert(document.getElementById("txhCadCodigosDetalle").value);
	 //alert( document.getElementById("txhCadDescripcionDetalle").value);
	 //Descripción Final --> <c:out value="${lista.dscDetalleCotiFin}"/>banListaBandeja
	}
	
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/CotSolGenerarOrden.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codTipoReq" id="txhCodTipoReq"/>
<form:hidden path="codProveedor" id="txhCodProveedor"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="dscProveedor" id="txhDscProveedor"/>
<form:hidden path="dscNroOrden" id="txhDscNroOrden"/>
<form:hidden path="cantidad" id="txhCantidad"/>
<form:hidden path="cadCodigosDetalle" id="txhCadCodigosDetalle"/>
<form:hidden path="cadDescripcionDetalle" id="txhCadDescripcionDetalle"/>
<form:hidden path="longitud" id="txhLongitud"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codTipoPago2" id="txhCodTipoPago2"/>
<form:hidden path="banListaBandeja" id="txhBanListaBandeja"/>
<form:hidden path="estado" id="txhEstado"/>
<form:hidden path="banListaBandejaInferior" id="txhBanListaBandejaInferior"/>

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" ID="Table8"
 style="margin-top: 7px; margin-left: 9px;">
			<tr>
				<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 		<td background="${ctx}/images/Evaluaciones/centro.jpg" width="770px" class="opc_combo">
		 			<font style="">Generar Orden</font></td>
		 		<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
			</tr>
			
</table>
		
<table cellpadding="2" cellspacing="2" class="tabla" border="0" bordercolor="green" 
			style="width:96%;margin-top:6px;margin-left:9px"
		     background="${ctx}/images/Evaluaciones/back.jpg" height="30px"> 
			<tr>
				<td class="" width="12%">&nbsp;&nbsp;Nro. Cotización:</td>
				<td width="38%"><form:input path="nroCotizacion" id="nroCotizacion" cssClass="cajatexto"
								onkeypress="fc_ValidaNumerosGuion();" readonly="true"
								onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Cotizacion');"
								cssStyle="width:80px; text-align: right;" maxlength="10" />
				</td>
				<td width="10%">Tipo Pago :</td>
				<td width="40%"><form:select path="codTipoPago" id="codTipoPago" cssClass="combo_o" disabled="true"
								cssStyle="width:180px" onchange="javascript:fc_TipoReq();">
								<c:if test="${control.listaCodTipoPago!=null}">
								<form:options itemValue="codSecuencial" itemLabel="descripcion" 
									items="${control.listaCodTipoPago}" />
								</c:if>
							</form:select>
				</td>
			</tr>			
			
</table>
<!-- ALQD,11/02/09.AUMENTADO UNA COLUMNA PARA MOSTRAR EL TIPO DE ORDEN CONSUMIBLE -->
<!-- ALQD,25/05/09.AGREGANDO COLUMNA DE AFECTO IGV -->
<table cellpadding="0" cellspacing="0"  ID="Table1" style="width:96%;margin-top:7px;margin-left:9px">
	<tr>
		<td>
			<div style="overflow: auto; height: 150px">
			<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" class="tablagrilla" 
			        style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA">
				<tr>
					<td class="grilla" width="5%">Sel.</td>
					<td class="grilla" width="22%">Proveedor</td>
					<td class="grilla" width="8%">Tipo</td>
					<td class="grilla" width="10%">Moneda</td>
					<td class="grilla" width="15%">Monto Asignado</td>
					<td class="grilla" width="8%">IGV</td>
					<td class="grilla" width="17%">Nro. Ordenes</td>
					<td class="grilla" width="15%">Estado Ordenes</td>
				<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}"  >
					<tr class="texto">
					   <td align="center" class="tablagrilla" style="width: 5%">
						  <input type="radio" name="producto"
						    <c:if test="${lista.dscNroOrden != 'Sin Orden'}">disabled ="true" </c:if>
							id='valBa<c:out value="${loop.index}"/>'
							onclick="fc_SeleccionarRegistro('<c:out value="${lista.codProv}" />',
							'<c:out value="${lista.razonSocial}" />','<c:out value="${lista.dscNroOrden}" />');">
						</td>
						<td align="left" class="tablagrilla" style="width: 22%">							
							<c:out value="${lista.razonSocial}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 8%">							
							<c:out value="${lista.tipGasOrden}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.dscMoneda}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.dscMonto}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.afectoIGV}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 17%">
							<c:out value="${lista.dscNroOrden}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.dscEstadoOrden}" />
						</td>												
					</tr>
			</c:forEach>
			<c:if test='${control.banListaBandeja=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="8" height="20px">No se encontraron registros
						</td>
					</tr>					 		
			</c:if>
						</tr>
					</table>
					</div>
				</td>
				<!--td>&nbsp;<img src=..\Images\iconos\agregar.gif onclick="javascript:Fc_Registro();"><br>&nbsp;<img src=..\Images\iconos\quitar.gif><br>&nbsp;<img src=..\Images\iconos\ico_Buscar.gif></td-->
				<!--td>&nbsp;<img src=..\Images\iconos\agregar.gif onclick="javascript:location.href='creacion_cotizacion.htm'" style="cursor:hand"><br>&nbsp;<img src=..\Images\iconos\quitar.gif style="cursor:hand"><br>&nbsp;<img src=..\Images\iconos\ico_Buscar.gif style="cursor:hand"></td-->
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td align="center">
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnRegresar','','${ctx}/images/botones/regresar2.jpg',1)" >
					<img src="${ctx}/images/botones/regresar1.jpg" alt="Regresar" style="cursor:hand" onclick="javascript:fc_Regresar();" id="btnRegresar">
				</a>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGenerar','','../images/botones/generar2.jpg',1)" >	
					<img src="${ctx}/images/botones/generar1.jpg" alt="Generar" style="cursor:hand" onclick="javascript:fc_VerCondCompra();" id="btnGenerar">
				</a>&nbsp;
				</td>
			</tr>	
		</table>
		<br>
		<table style="display:none;" id="TablaProveedor" name="TablaProveedor"
		 cellpadding="0" cellspacing="1" class="tabla" border="0" bordercolor="green" 
		     background="${ctx}/images/Evaluaciones/back.jpg" height="30px" 
		     style="width:96%;margin-top:7px;margin-left:9px">
		      <tr>
		      	<td width="8%" align="left" class="">&nbsp;&nbsp;Proveedor:&nbsp;&nbsp;</td>
			    <td width="92%" align="left" class=""><c:out value="${control.dscProveedor}" /></td>
			  </tr>  
		</table>
		<table cellpadding="0" cellspacing="0"  id="TablaInferior" name="TablaInferior" 
		       style="width:96%;margin-top:7px;margin-bottom:5px; margin-left:9px; border: 0px solid #048BBA; display: none;">
	    	<tr>
				<td>
					<table cellpadding="0" cellspacing="1" border="0" bordercolor="white" class="tablagrilla" 
					 style="width:98%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" ID="Table3">
						<tr>
							<td class="grilla">Condición</td>
							<td class="grilla">Descripción Inicial</td>
							<td class="grilla">Observación Proveedor</td>
							<td class="grilla">Descripción Final</td>
						
					<c:forEach varStatus="loop" var="lista" items="${control.listaBandejaInferior}"  >
					<tr class="texto">
					 	<td><c:out value="${lista.descripcion}" />
						    <input type="hidden" id="codDetalle<c:out value="${loop.index}"/>" name="codDetalle<c:out value="${loop.index}"/>"
							 size="5" class="texto_moneda1" value="<c:out value="${lista.codCotiDet}" />" />
						</td>
						<td align="center">
							<textarea class="cajatexto_1" id="TextArea1<c:out value="${loop.index}"/>" 
							 name="TextArea1<c:out value="${loop.index}"/>" cols="40" readonly="readonly" ><c:out value="${lista.dscDetalleCoti}" /></textarea>
						</td>
						<td align="center">
							<textarea class="cajatexto_1" cols="40" id="TextArea2<c:out value="${loop.index}"/>"
							 readonly="readonly" name="TextArea2<c:out value="${loop.index}"/>"><c:out value="${lista.dscDetalleProv}" /></textarea>
						</td>
						<td align="center">
							<textarea class="cajatexto" cols="40" id="TextArea3<c:out value="${loop.index}"/>"
							name="TextArea3<c:out value="${loop.index}"/>"><c:out value="${lista.dscDetalleCoti}" /></textarea>
						</td>
					</tr>
					</c:forEach>
					<c:if test='${control.banListaBandejaInferior=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="8" height="20px">No se encontraron registros
						</td>
					</tr>					 		
					</c:if>	
					</tr>
					</table>
				</td>
				<!--td>&nbsp;<img src=..\Images\iconos\agregar.gif onclick="javascript:Fc_Registro();"><br>&nbsp;<img src=..\Images\iconos\quitar.gif><br>&nbsp;<img src=..\Images\iconos\ico_Buscar.gif></td-->
				<!--td>&nbsp;<img src=..\Images\iconos\agregar.gif onclick="javascript:location.href='creacion_cotizacion.htm'" style="cursor:hand"><br>&nbsp;<img src=..\Images\iconos\quitar.gif style="cursor:hand"><br>&nbsp;<img src=..\Images\iconos\ico_Buscar.gif style="cursor:hand"></td-->
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td align="center">
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)" >	
					<img src="${ctx}/images/botones/grabar1.jpg" alt="Grabar" style="cursor:hand" id="btnGrabar"
					onclick="javascript:fc_GuardarOrdenCompra();">
				</a>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)" >	
					<img src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="javascript:fc_Regresar();" ID="btnCancelar">
				</a>&nbsp;
				</td>
			</tr>	
		</table>
		
</form:form>
</body>
</html>