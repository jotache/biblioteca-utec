<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
		
		function onLoad(){
		  if(document.getElementById("txhTxtDescripcion").value=="CONSUMIBLE")
		  {  TablaBienConsumible.style.display = 'inline-block';
		     TablaBienActivo.style.display = 'none';
		     TablaServicio.style.display = 'none';
		  }
		  else{ if(document.getElementById("txhTxtDescripcion").value=="ACTIVO")
		  		 { TablaBienConsumible.style.display = 'none';
		    		 TablaBienActivo.style.display = 'inline-block';
		    		 TablaServicio.style.display = 'none';
		 		 }
		 		 else{ if(document.getElementById("txhTxtDescripcion").value=="SERVICIO")
		  		    { TablaBienConsumible.style.display = 'none';
		   			  TablaBienActivo.style.display = 'none';
		    		  TablaServicio.style.display = 'inline-block';
		 		    }
		 		 }
		  }
		}
		
		function fc_Regresar(){
		}
		
		function fc_Grabar(){}
		
		function fc_EnviarCotizacion(){}
		
		function fc_CondicionCompra(){}
		
</script>
</head>
<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/bandejaPenVerCotizacionActual.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="txtDescripcion" id="txhTxtDescripcion"/>
<form:hidden path="codTipoReq" id="txhCodTipoReq"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="txtDescripcion" id="txhTxtDescripcion"/>

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px">
	 <tr>
	 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
	 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="765px" class="opc_combo">
	 		<font style="">Condici�n Actual(<c:out value="${control.txtDescripcion}"></c:out>) </font></td>
	 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
	 </tr>
</table>
<table class="tabla" background="${ctx}/images/Evaluaciones/back-sup.jpg" border="2" bordercolor="red"
			style="width:100%;margin-top:2px; height:65px" cellspacing="2" cellpadding="0">
	<tr>
		<td width="10%" class="texto_bold">Tipo Pago:</td>
		<td><form:select path="codTipoPago" id="codTipoPago" cssClass="combo_o" 
						cssStyle="width:180px" onchange="javascript:fc_Familia();">
						<c:if test="${control.listaCodTipoBien!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaCodTipoBien}" />
						</c:if>
			</form:select>
		</td>
		<td class="texto_bold">Vigencia Cotizaci�n:</td>
		<td>
			<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto"
						onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
						onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');" 
						cssStyle="width:70px" maxlength="10"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
						align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
			<form:input path="txtHoraInicio" id="txtHoraInicio" cssClass="cajatexto"
						cssStyle="width:80px" maxlength="10" />&nbsp;hrs.
		</td>
		<td>
			<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
						onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
						onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');" 
						cssStyle="width:70px" maxlength="10"/>&nbsp;
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
						align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
			<form:input path="txtHoraFin" id="txtHoraFin" cssClass="cajatexto"
						cssStyle="width:80px" maxlength="10" />&nbsp;hrs.
		</td>
	</tr>
</table>
<br>
<table id="TablaBienConsumible" name="TablaBienConsumible" cellpadding="0" cellspacing="0" width="98%">
	<tr>
		<td width="98%">
			<div style="overflow: auto; height:150px;width:99%">
				<display:table name="sessionScope.listaBienConsumible" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.LogisticaDecorator"  requestURI="" 
				style="border: 1px solid #048BBA;width:98%;margin-left:3px">
					<display:column property="checkSelRequerimientoBien" title="Sel" 
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
					<display:column property="nroRequerimiento" title="C�digo" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="fechaEmision" title="Producto" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					<display:column property="dscCecoSolicitante" title="Unidad Medida" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="dscSubTipoRequerimiento" title="Cant." headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					<display:column property="totalRequerimiento" title="Nro. Prov." headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</div>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center" >
			<input type=button class="boton" value="Regresar" onclick="javascript:fc_Regresar();">
			<input type=button class="boton" value="Condiciones de Compra" onclick="javascript:fc_CondicionCompra();">
			<input type=button class="boton" value="Regresar" onclick="javascript:fc_Grabar();">
			<input type="button" class="boton" value="Enviar Cotizaci�n" onclick="javascript:fc_EnviarCotizacion();">
		</td>
	</tr>
</table>

<table id="TablaBienActivo" name="TablaBienActivo" cellpadding="0" cellspacing="0" width="98%">
	<tr>
		<td width="98%">
			<div style="overflow: auto; height:150px;width:99%">
				<display:table name="sessionScope.listaBienActivo" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.LogisticaDecorator"  requestURI="" 
				style="border: 1px solid #048BBA;width:98%;margin-left:3px">
					<display:column property="checkSelRequerimientoBien" title="Sel" 
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
					<display:column property="nroRequerimiento" title="Codig�" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="fechaEmision" title="Producto" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					<display:column property="dscCecoSolicitante" title="Unidad Medida" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="dscSubTipoRequerimiento" title="Cant." headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					<display:column property="totalRequerimiento" title="Nro. Prov." headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</div>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center" >
			<input type=button class="boton" value="Regresar" onclick="javascript:fc_Regresar();">
			<input type=button class="boton" value="Condiciones de Compra" onclick="javascript:fc_CondicionCompra();">
			<input type="button" class="boton" value="Enviar Cotizaci�n" onclick="javascript:fc_EnviarCotizacion();">
		</td>
	</tr>
</table>

<table id="TablaServicio" name="TablaServicio" cellpadding="0" cellspacing="0" width="98%">
	<tr>
		<td width="98%">
			<div style="overflow: auto; height:150px;width:99%">
				<display:table name="sessionScope.listaServicio" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.LogisticaDecorator"  requestURI="" 
				style="border: 1px solid #048BBA;width:98%;margin-left:3px">
					<display:column property="checkSelRequerimientoBien" title="Sel" 
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
					<display:column property="nroRequerimiento" title="Nro. Req." headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="fechaEmision" title="U. Solicitante" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					<display:column property="dscCecoSolicitante" title="Grupo Servicio" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					<display:column property="dscSubTipoRequerimiento" title="Tipo Servicio" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					<display:column property="totalRequerimiento" title="Nombre Servicio" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="totalRequerimiento" title="Fec. Entrega Deseada" headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:25%"/>
					<display:column property="totalRequerimiento" title="Nro. prov." headerClass="grilla" 
					class="tablagrilla" style="text-align:left;width:10%"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
			</div>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center" >
			<input type=button class="boton" value="Regresar" onclick="javascript:fc_Regresar();">
			<input type=button class="boton" value="Condiciones de Compra" onclick="javascript:fc_CondicionCompra();">
			<input type="button" class="boton" value="Enviar Cotizaci�n" onclick="javascript:fc_EnviarCotizacion();">
		</td>
	</tr>
</table>

</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>