<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){

			strMsg = fc_Trim(document.getElementById("txhMsg").value);
			if ( strMsg == "ERROR" ) alert(mstrProblemaGrabar);
			if ( strMsg == "ERROR1" ) alert(mstrProvRepetido);
			else if ( strMsg == "OK" )
			{
				window.opener.fc_Refrescar();
				alert(mstrSeGraboConExito);
				window.close();
			}
			
			document.getElementById("txhMsg").value = "";
		}
		
		function fc_seleccionarRegistro( codProv )
		{
			document.getElementById("txhProveedor").value = codProv;
		}
		
		function fc_Grabar()
		{
			if ( document.getElementById("txhProveedor").value == "")
			{
				alert(mstrSeleccione);
				return false;
			}
			
			if ( !confirm(mstrSeguroGrabar)) return false;

			document.getElementById("txhAccion").value = "GRABAR";
			document.getElementById("frmMain").submit();
		}

		function fc_Limpiar()
		{
			document.getElementById("txtRuc").value = "";
			document.getElementById("txtRazonSocial").value = "";
		}
		
		function fc_Buscar()
		{
			if(	fc_Trim(document.getElementById("txtRuc").value) == "" &&
				fc_Trim(document.getElementById("txtRazonSocial").value) == "" ){
				alert(mstrIngreseIngreseValorBuscar);
				return false;
			}
			document.getElementById("txhAccion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
		}
	</script>
</head>

<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/CotDetAgregarProveedores.html" >
<form:hidden path="operacion" id="txhAccion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codCotizacionDet" id="txhCodDetCotizacion"/>
<form:hidden path="codProveedor" id="txhProveedor"/>

<table cellpadding="0" cellspacing="0" background="${ctx}\images\popup\popuplog.jpg" width="96%" height="38px"
		style="margin-left:9px; margin-top: 10px;">
	<tr height="25px">
	     <td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 <td background="${ctx}/images/Evaluaciones/centro.jpg" width="330px" class="opc_combo"><font style="">B�squeda de Proveedores</font></td>
		 <td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
	</tr>
</table>

<table cellpadding="1" cellspacing="1" class="tabla" border="0" bordercolor="green" style="width:96%;margin-top:10px; margin-left:9px;height: 30px;" 
		 background="${ctx}/images/Evaluaciones/back.jpg">
	<tr>
		<td width="70px" class="">&nbsp;&nbsp;RUC:</td>
		<td>
			<form:input path="ruc" id="txtRuc" cssClass="cajatexto" cssStyle="width:95px" maxlength="11"
			  onkeypress="fc_PermiteNumeros();" onblur="fc_ValidaNumeroOnBlur(this.id);"/>
		</td>
	<tr>
	<tr>
		<td width="70px" class="">&nbsp;&nbsp;Proveedor:</td>
		<td width="300px">
			<form:input path="razonSocial" id="txtRazonSocial" cssClass="cajatexto" cssStyle="width:250px" maxlength="50"
			 onkeypress="fc_ValidaTextoEspecial();" onblur="fc_ValidaSoloLetrasFinal1(this,'Proveedor');"/>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
			<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" style="cursor:hand;" id="icoLimpiar" onclick="javascript:fc_Limpiar();" />
			</a>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
			<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" style="cursor:hand;" id="icoBuscar" onclick="javascript:fc_Buscar();" />
			</a>
		</td>
	<tr>
	
</table>
<table cellpadding="0" cellspacing="0"	class="tabla" width="95%" style="margin-left:9px; margin-top: 10px;">
	<tr>
		<td width="95%">
			<div style="overflow: auto; height: 230px; width: 96%">
				<display:table name="sessionScope.listaProveedores" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.ProveedorCotizacionDecorator" 
				style="border: 1px solid #048BBA;width:95%;">
					<display:column property="rbtSelProveedor" title="Sel." 
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
					<display:column property="razonSocial" title="Proveedor" headerClass="grilla" 
						class="tablagrilla" style="text-align:left;width:65%"/>
					<display:column property="dscTipoPersona" title="Tipo<br>Persona" headerClass="grilla" 
						class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="dscCalificacion" title="Calificaci�n" headerClass="grilla" 
						class="tablagrilla" style="text-align:left;width:15%"/>
						
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
				<%request.getSession().removeAttribute("listaProveedores"); %>
			</div>
		</td>
	</tr>
</table>

<table style="margin-top: 10px;" align="center">
	<tr>
		<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','${ctx}/images/botones/aceptar2.jpg',1)" >
				<img src="${ctx}\images\botones\aceptar1.jpg" alt="Aceptar" style="cursor:hand" id="btnAceptar" onclick="javascript:fc_Grabar();">
			</a>&nbsp;
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)" >
				<img src="${ctx}\images\botones\cancelar1.jpg" onclick="window.close();" alt="Cancelar" style="cursor:hand" id="btnCancelar">
			</a>&nbsp;&nbsp;
		</td>
	</tr>
</table>
</form:form>
