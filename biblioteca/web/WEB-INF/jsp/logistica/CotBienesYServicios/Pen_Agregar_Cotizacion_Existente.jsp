<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
		
		function onLoad(){
		
			if(document.getElementById("txhBandera").value=="1")
			{
				document.getElementById("txhValRadio").value=document.getElementById("txhConsteBienConsumible").value;
				TablaBien.style.display = 'inline-block';
				TablaServicio.style.display = 'none';
			}
			else if(document.getElementById("txhBandera").value=="2")
			{
				document.getElementById("txhValRadio").value=document.getElementById("txhConsteBienActivo").value;
				TablaBien.style.display = 'inline-block';
				TablaServicio.style.display = 'none';
			}
			else if(document.getElementById("txhBandera").value=="3")
			{
				TablaBien.style.display = 'none';
				TablaServicio.style.display = 'inline-block';
			}
			 
			//Validando el mensaje
			msg = document.getElementById("txhMsg").value;
			if ( msg != "")
			{
				if ( msg == "-1" ){alert(mstrProblemaGrabar); return false;}
				else if ( msg == "-3" ){alert("La cotización seleccionada no cuenta con una subfamilia similar al del bien seleccionado."); return false;}
				else if ( msg == "-2" ){alert("La cotización seleccionada no cuenta con un tipo de servicio similar al del requerimiento seleccionado."); return false;}
				else if ( msg == "0" ){
					window.opener.fc_Buscar();
					alert(mstrSeGraboConExito);
					window.close();
				}
			}
		
			var objLong = "<%=((request.getAttribute("BUSCAR")==null)?"":(String)request.getAttribute("BUSCAR"))%>";
			
			if(objLong=="OK")
			   Table1.style.display = 'inline-block';
		}
		
		function fc_Etapa(param){
		
			switch(param){
				case '1':
				   	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value ;
				    break;
				case '2':
				    document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienActivo").value ;
				   	break;
				   	
				}
		}
		
		function fc_Limpiar(){
		document.getElementById("txtNroCotizacion").value="";
		document.getElementById("txtFecInicio").value="";
		document.getElementById("txtFecFinal").value="";
		document.getElementById("codTipoServicio").value="-1";
		}
		
		function fc_Buscar(){
		     srtFechaInicio=document.getElementById("txtFecInicio").value;
		     srtFechaFin=document.getElementById("txtFecFinal").value;
		     var num=0;
		     var srtFecha="0";
		    
		     if( srtFechaInicio!="" && srtFechaFin!="")
		       {  	num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		          	if(num==1) 	srtFecha="0";
		            else srtFecha="1";
		       }
		   if(srtFecha=="0") 
		   {
		     document.getElementById("txhOperacion").value = "BUSCAR";
		     document.getElementById("frmMain").submit();
		   }
		   else alert(mstrFecha);
		   
		}
		
		function fc_Guardar(){
		
			if ( document.getElementById("txhCodCotizacion").value == "")
			{
				alert(mstrSeleccione);
				return false;
			}
			
			if (confirm(mstrSeguroGrabar))
			{
				document.getElementById("txhOperacion").value = "GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
		
		function fc_SeleccionarRegistro(param){
			document.getElementById("txhCodCotizacion").value = param;
		}
</script>
</head>
<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/bandejaPenAgregarCotizacionExistente.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="consteBienConsumible" id="txhConsteBienConsumible"/>
<form:hidden path="consteBienActivo" id="txhConsteBienActivo"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="valRadio" id="txhValRadio"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="codTipoReq" id="txhCodTipoReq"/>
<form:hidden path="codSubTipoReq" id="txhCodSubTipoReq"/>
<form:hidden path="bandera" id="txhBandera"/>
<form:hidden path="dscTipoReq" id="txhDscTipoReq"/>
<form:hidden path="banListaBandeja" id="txhBanListaBandeja"/>
<form:hidden path="codSeleccionados" id="txhCodSeleccionados"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="indInversion" id="txhIndInversion"/><!-- ALQD,07/08/09 -->

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px; margin-top: 6px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="535px" class="opc_combo">
		 		<font style="">Consulta Cotizaciones de <c:out value="${control.dscTipoReq}"/> </font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
</table>
		<table class="tabla" background="${ctx}/images/Evaluaciones/back.jpg" border="0" bordercolor="red"
					style="width:96%;margin-top:7px;margin-left:9px; height:65px" cellspacing="2" cellpadding="0">
			<tr>
				<td width="15%">&nbsp;&nbsp;Nro. Cotización:</td>
				<td width="20%"><form:input path="txtNroCotizacion" id="txtNroCotizacion" cssClass="cajatexto"
									cssStyle="width:100px; text-align: left;" maxlength="15" 
									onkeypress="fc_ValidaNumerosGuion();"
							        onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Cotización');"/>
				</td>
				<td colspan="2" width="65%">&nbsp;&nbsp;Fec. Cotización: <form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');" 
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
							<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
								onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
								onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');" 
								cssStyle="width:70px" maxlength="10"/>&nbsp;
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
							<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
								align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
				</td>
			</tr>
			<tr>
				<td width="15%">&nbsp;&nbsp;Tipo Pago :</td>
				<td width="20%">
					<form:select path="codTipoPago" id="codTipoPago" cssClass="combo_o" 
								cssStyle="width:140px" onchange="javascript:fc_Familia();">
								<form:option  value="">--Todos--</form:option>
								<c:if test="${control.listaCodTipoBien!=null}">
								<form:options itemValue="codSecuencial" itemLabel="descripcion" 
									items="${control.listaCodTipoBien}" />
								</c:if>
							</form:select>
				</td>
				<td width="55%">
				 <table border="0" width="100%">
				  <tr>
					<td>
						  <table id="TablaBien" nombre="TablaBien" style="display:none" border="0" width="100%">
						    <tr>
								<td width="30%">Tipo Bien:</td>
								<td align="left" width="70%">
									<input Type="radio" checked name="rdoEtapa"
									    <c:if test="${control.valRadio==control.consteBienConsumible}"><c:out value=" checked=checked " /></c:if>
										id="gbradio1" onclick="javascript:fc_Etapa('1');" disabled="true">&nbsp;Consumible&nbsp;
									<input Type="radio" name="rdoEtapa"
										<c:if test="${control.valRadio==control.consteBienActivo}"><c:out value=" checked=checked " /></c:if>
										id="gbradio2" onclick="javascript:fc_Etapa('2');" disabled="true">&nbsp;Activo&nbsp;
									<input type="checkbox" id="chkIndInversion"
										<c:if test="${control.indInversion=='1'}"><c:out value=" checked=checked "/></c:if>
										onclick="fc_Etapa('1');" disabled="true"/>&nbsp;<label id="lblIndInversion">Inversión</label>
								</td>
							</tr>
						  </table>
						  
						  <table id="TablaServicio" name="TablaServicio" style="display:none" border="0" width="100%">
						    <tr>
						       <td>Tipo Servicio :</td>
							   <td>&nbsp;&nbsp;
									<form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto" 
										cssStyle="width:180px" onchange="javascript:fc_Familia();">
										<form:option  value="">--Todos--</form:option>
										<c:if test="${control.listaTipoServicio!=null}">
										<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
											items="${control.listaTipoServicio}" />
										</c:if>
									</form:select>
							   </td>
						       
						    </tr>
						  </table>
						</td>
				   </tr>
				  </table>
				</td >
						<td align=left colspan="1" width="10%">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
							 style="cursor: pointer; 60px" 
							 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
						&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
							 style="cursor: pointer; margin-right: " onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
						</td>
				
			</tr>
		</table>
		
		<table cellpadding="0" cellspacing="0"  id="Table1" name="Table1" width="98%" 
		      style="margin-left: 9px; margin-top: 7px;">
			<tr>
				<td width="98%">
					<div style="overflow: auto; height:200px;width:99%">
					<table cellpadding="0" cellspacing="1" id="tablaBandeja" class=""
					    style="width:97%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" >
					<tr>
						<td class="grilla" width="4%">Sel.</td>
						<td class="grilla">Nro. Cotización</td>
						<td class="grilla">Fecha Cotización</td>
						<td class="grilla">Fecha Inicio Vigencia</td>
						<td class="grilla">Fecha Fin Vigencia</td>
						<td class="grilla">Tipo de Pago</td>
							
					</tr>
					
					<c:forEach varStatus="loop" var="lista" items="${control.listaConsulta}"  >
					<tr class="cajatexto" ondblclick="fc_MostrarDetalleRegistroSE('<c:out value="${loop.index}"/>');">
						<td align="center" class="tablagrilla" style="width: 4%">
							<input type="radio" name="producto"
								id='valSe<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro('<c:out value="${lista.codigo}"/>');">
							<input type="hidden" 
								id='indiceSe<c:out value="${loop.index}"/>'
								value='<c:out value="${loop.index}"/>'>
						</td>
						<td align="center" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.nroCotizacion}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.fechaInicio}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.fechaFin}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.dscTipoGasto}" />
						</td>
						
					</tr>
					</c:forEach>
					<c:if test='${control.banListaBandeja=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="8" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
				</table>
			  </div>
			</td>
		</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td align="center" >
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGuardar','','${ctx}/images/botones/grabar2.jpg',1)">
						<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imgGuardar" style="cursor:pointer;" onclick="javascript:fc_Guardar();">
					 </a>
					 <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">
						<img alt="Cancelar" src="${ctx}/images/botones/cancelar1.jpg" id="imgCancelar" style="cursor:pointer;" onclick="javascript:window.close();">
					 </a>
					
				</td>
		</tr>
			
		</table>
		
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>