<%@ include file="/taglibs.jsp"%>
<%@page import='com.tecsup.SGA.bean.UsuarioSeguridad'%>

<html>
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
		
	function onLoad(){
		
		var objLong = "<%=((request.getAttribute("Bandera")==null)?"":(String)request.getAttribute("Bandera"))%>";
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
		{	TablaTipoBien.style.display = 'inline-block';
			TablaTipoServicio.style.display = 'none';
		}
		else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
				  {
				     TablaTipoBien.style.display = 'none';
			         TablaTipoServicio.style.display = 'inline-block';
				   
				  }
			
		}
		
	}
	
	function fc_TipoReq(){
		document.getElementById("txhOperacion").value = "TIPOREQ";
	    document.getElementById("frmMain").submit();
	}
	
	function fc_Etapa(param){
		
		switch(param){
			case '1':	
			  	
			   	document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienConsumible").value ; 
			    document.getElementById("txhOperacion").value = "BUSCAR";
		        document.getElementById("frmMain").submit();
			    break;
			case '2':
			   
			    document.getElementById("txhConsteRadio").value=document.getElementById("txhConsteBienActivo").value ;
			  	document.getElementById("txhOperacion").value = "BUSCAR";
		        document.getElementById("frmMain").submit();
			   	break;
			   	
			}
	}
	function fc_Buscar(){
		 
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteBien").value)
			{ 	 srtFechaInicio=document.getElementById("txtFecInicio").value;
		    	 srtFechaFin=document.getElementById("txtFecFinal").value;
		    	 var num=0;
		   		  var srtFecha="0";
		    	 if( srtFechaInicio!="" && srtFechaFin!="")
		      	 {
		           num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		          if(num==1) 	srtFecha="0";
		           else srtFecha="1";
		      	 }
		    
		    if(srtFecha=="0")   
		    	{  document.getElementById("txhOperacion").value = "BUSCAR";
		           document.getElementById("frmMain").submit();
		         }
			 else alert(mstrFecha);
		    }
		else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteServicio").value)
		          { if(document.getElementById("codGrupoServicio").value!="-1")
		             { srtFechaInicio=document.getElementById("txtFecInicio").value;
		    			 srtFechaFin=document.getElementById("txtFecFinal").value;
		    			 var num=0;
		   				  var srtFecha="0";
		    			 if( srtFechaInicio!="" && srtFechaFin!="")
		      			 {
		        		   num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		        		   if(num==1) 	srtFecha="0";
		        		   else srtFecha="1";
		      			 }
		     
		   			 if(srtFecha=="0")
		    		   { document.getElementById("txhOperacion").value = "BUSCAR";
		    		   document.getElementById("frmMain").submit();}
					 else alert(mstrFecha);
		             }
		            else alert(mstrSelGrupoServicio);
		          }
		}
	}
	function fc_SeleccionarRegistro(param1, param2, param3, param4, param5){
		
		document.getElementById("txhCodCotizacion").value=param1;
		document.getElementById("txhCodOrdenCompra").value=param2;
		document.getElementById("txhCodEstadoBD").value=param3;
		document.getElementById("txhNroCotizacionBD").value=param4;
		document.getElementById("txhCodTipoPagoBD").value=param5;
		
	}
		
	function fc_Regresar()
	{
		document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
	}
	
	function fc_VerCotizaciones(){
		
		if(document.getElementById("txhCodCotizacion").value!="")
			{
				srtCodUsuario = document.getElementById("txhCodUsuario").value;
				srtCodSede = document.getElementById("txhCodSede").value;
				srtCodCotizacion = document.getElementById("txhCodCotizacion").value;
				srtCodEstado = document.getElementById("txhCodEstadoBD").value;
				srtCodTipoReq = document.getElementById("codTipoRequerimiento").value;
				srtNumCotizacion = document.getElementById("txhNroCotizacionBD").value;
				/*MODIFICADO CARLOS CORDOVA - 12/04/2008*/
				/*Redirecciona a distintas páginas dependiendo del perfil*/
				<%
					UsuarioSeguridad usuarioSeguridad = (UsuarioSeguridad)request.getSession().getAttribute("usuarioSeguridad");
					if(usuarioSeguridad!=null){
					String strPerfiles = usuarioSeguridad.getPerfiles();
					if ( strPerfiles.indexOf("LRCECO") > -1 ||
							strPerfiles.indexOf("LDCECO") > -1 ||
							strPerfiles.indexOf("LDADM") > -1 ||
							strPerfiles.indexOf("LGEGE") > -1 ||
							strPerfiles.indexOf("LCOMP") > -1 ||
							strPerfiles.indexOf("LJLOG") > -1 ||
							strPerfiles.indexOf("LALMC") > -1 ) {
				%>
				
					location.href="${ctx}/logistica/verCotizaion.html"
					+ "?prmUsuario=" + srtCodUsuario
					+ "&prmCodSede=" + srtCodSede
					+ "&prmCodCotizacion="+srtCodCotizacion
					+ "&txhCodProveedor="+srtCodUsuario
					+ "&prmCodEstCotizacion="+srtCodEstado
					+ "&txhCodTipoReq="+srtCodTipoReq
					+ "&txhNumCotizacion="+srtNumCotizacion;
					
				<% } else { %>
					
					location.href="${ctx}/logistica/CotizacionBienesProveedor.html"
					+ "?txhCodUsuario=" + srtCodUsuario
					+ "&txhCodSede="+srtCodSede
					+ "&txhCodCotizacion="+srtCodCotizacion
					+ "&txhCodProveedor="+srtCodUsuario
					+ "&txhCodTipoReq="+srtCodTipoReq;
				<% }
				}%>
			 }
			 else alert(mstrSeleccione);
	}
		
	function fc_GenerarOrden(){
		
		if(document.getElementById("txhCodCotizacion").value!="")
		 {  if(document.getElementById("txhCodEstadoBD").value==document.getElementById("txhCodEstadoCerrado").value) 
		    { srtCodUsuario=document.getElementById("txhCodUsuario").value;
				srtCodSede=document.getElementById("txhCodSede").value;
				srtCodTipoReq=document.getElementById("codTipoRequerimiento").value;
				srtCodCotizacion=document.getElementById("txhCodCotizacion").value;
				srtCodTipoPago=document.getElementById("codTipoPago").value;
				srtNroCotizacionBD=document.getElementById("txhNroCotizacionBD").value;
			
			   location.href="${ctx}/logistica/CotSolGenerarOrden.html?txhCodUsuario="
			   +srtCodUsuario+ "&txhCodSede="+srtCodSede+ "&txhCodProveedor="+srtCodUsuario+
			   "&txhCodCotizacion="+srtCodCotizacion + "&txhNroCotizacion="+srtNroCotizacionBD +
			   "&txhCodTipoPago="+document.getElementById("txhCodTipoPagoBD").value;
			  
			}
			else alert(mstrNoGenerarOrden);
		 }
		 else alert(mstrSeleccione);
	}
	
	function fc_GrupoServicio(){
		document.getElementById("txhOperacion").value = "GRUPOSERVICIO";
	        document.getElementById("frmMain").submit();
	}
	
	function fc_Familia(){
	}
	function fc_Estado(){
	}
	
	function fc_Excel(){
		if(document.getElementById("codTipoRequerimiento").value=="0001"){
		tipo= "B";
		if(document.getElementById("txhConsteRadio").value==document.getElementById("txhConsteBienConsumible").value){
		des = "C";
			}
			else{
			des = "A";
			}
		}
		else{
			tipo="S";
			des = frmMain.codTipoServicio.options[frmMain.codTipoServicio.selectedIndex].text.replace("--","");
			des=des.replace("--","");
			if(document.getElementById("codGrupoServicio").value=="-1")
			 { alert(mstrSelGrupoServicio);
			   return false;
			 }
			}
		dscEstado=frmMain.codEstado.options[frmMain.codEstado.selectedIndex].text.replace("--","");
		dscEstado=dscEstado.replace("--","");
		dscTipoPago=frmMain.codTipoPago.options[frmMain.codTipoPago.selectedIndex].text.replace("--","");
		dscTipoPago=dscTipoPago.replace("--","");
		dscGruoServ=frmMain.codGrupoServicio.options[frmMain.codGrupoServicio.selectedIndex].text.replace("--","");
		dscGruoServ=dscGruoServ.replace("--","");
		u = "0014";
		url="?tCodRep=" + u +
		"&tCodSede="+document.getElementById("txhCodSede").value +
		"&tCP=" + document.getElementById("txhCodPerfil").value +
		"&tCE=" + document.getElementById("codEstado").value +
		"&tFI=" + document.getElementById("txtFecInicio").value +
		"&tFF=" + document.getElementById("txtFecFinal").value +
		"&tNR=" + document.getElementById("txtNroRequerimiento").value +
		"&tCTR=" + document.getElementById("codTipoRequerimiento").value +
		"&tCTP=" + document.getElementById("codTipoPago").value +
		"&tP1=" + document.getElementById("txtProveedor1").value +
		"&tP2=" + document.getElementById("txtProveedor2").value +
		"&tCR=" + document.getElementById("txhConsteRadio").value +
		"&tCTS=" + document.getElementById("codTipoServicio").value +
		"&tDTRB=" + des +
		"&dscUser=" + document.getElementById("txhDscUser").value +
		"&tDTP=" + dscTipoPago +
		"&estado=" + dscEstado+
		"&tCGS="+ document.getElementById("codGrupoServicio").value+
		"&tDGS=" + dscGruoServ+
		"&tVal1=" + document.getElementById("txhValSelec1").value;
		
  		window.open("${ctx}/logistica/reportesLogistica.html"+url,"","resizable=yes, menubar=yes");

	}
	function fc_SeleccSinOc(valor){
		//ALQD,30/01/09. NUEVA FUNCION
		if(valor=="1") 
		  {  if(document.getElementById("txhValSelec1").value=="1")
		    	 document.getElementById("txhValSelec1").value="0";
		     else
		     document.getElementById("txhValSelec1").value=valor;
	      }
	}
</script>
</head>
<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/bandejaSolSolicitudCotizacion.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="consteRadio" id="txhConsteRadio"/>
<form:hidden path="consteBien" id="txhConsteBien"/>
<form:hidden path="consteServicio" id="txhConsteServicio"/>
<form:hidden path="consteBienConsumible" id="txhConsteBienConsumible"/>
<form:hidden path="consteBienActivo" id="txhConsteBienActivo"/>
<form:hidden path="valRadio" id="txhValRadio"/>
<form:hidden path="indice" id="txhIndice"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codPerfil" id="txhCodPerfil"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codOrdenCompra" id="txhCodOrdenCompra"/>
<form:hidden path="codEstadoBD" id="txhCodEstadoBD"/>
<form:hidden path="codEstadoCerrado" id="txhCodEstadoCerrado"/>
<form:hidden path="banListaBandeja" id="txhBanListaBandeja"/>
<form:hidden path="nroCotizacionBD" id="txhNroCotizacionBD"/>
<form:hidden path="dscUser" id="txhDscUser"/>
<form:hidden path="dscSede" id="txhDscSede"/>
<form:hidden path="dscTipoPago" id="txhDscTipoPago"/>
<form:hidden path="codTipoPagoBD" id="txhCodTipoPagoBD"/>
<form:hidden path="valSelec1" id="txhValSelec1"/>

	<!-- BOTON REGRESAR -->
	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<!-- TITULO -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo"><font style="">Bandeja de Cotizaciones</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- CABECERA DE BUSQUEDA -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
		cellspacing="2" cellpadding="2" border="0" bordercolor="red">	 
		<tr>
			<td class="" width="12%">Nro. Cotización:</td>
			<td width="30%"><form:input path="nroCotizacion" id="txtNroRequerimiento" cssClass="cajatexto"
							onkeypress="fc_ValidaNumerosGuion();"
							onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Cotizacion');"
							cssStyle="width:90px" maxlength="15" />
			</td>
			<td class="" width="11%">&nbsp;Fec. Cotización:</td>
			<td width="22%">
				<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto" 
							onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
							onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');" 
							cssStyle="width:70px" maxlength="10"/>&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
							align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
				<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
							onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
							onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');" 
							cssStyle="width:70px" maxlength="10"/>&nbsp;
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
							align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
			</td>	
						
			<td align="right">Tipo Pago :</td>
		    <td align="center">
				<form:select path="codTipoPago" id="codTipoPago" cssClass="cajatexto" 
							cssStyle="width:150px" onchange="javascript:fc_Familia();">
							<form:option value="-1">-- Todos --</form:option>
							<c:if test="${control.listaCodTipoBien!=null}">
							<form:options itemValue="codSecuencial" itemLabel="descripcion" 
								items="${control.listaCodTipoBien}" />
							</c:if>
				</form:select>
		   </td>
		</tr>
		<tr>
			<td class="">Tipo Req.:</td>
			<td>
				<form:select path="codTipoRequerimiento" id="codTipoRequerimiento" cssClass="combo_o" 
							cssStyle="width:130px" onchange="javascript:fc_TipoReq();">
							<c:if test="${control.listCodTipoRequerimiento!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listCodTipoRequerimiento}" />
							</c:if>
				</form:select>
			</td>
			<td colspan="4">
				<table id="TablaTipoBien" name="TablaTipoBien" style="display: none;" border="0" bordercolor="blue"
					cellpadding="0" cellspacing="0">
				    <tr>
				    <td width="30%" align="left">Tipo Bien:</td>
					<td align="left" width="70%"><input Type="radio" checked name="rdoEtapa"
						    <c:if test="${control.consteRadio==control.consteBienConsumible}"><c:out value=" checked=checked " /></c:if>
							id="gbradio1" onclick="javascript:fc_Etapa('1');">&nbsp;Consumible&nbsp;
						<input Type="radio" name="rdoEtapa"
							<c:if test="${control.consteRadio==control.consteBienActivo}"><c:out value=" checked=checked " /></c:if>
							id="gbradio2" onclick="javascript:fc_Etapa('2');">&nbsp;Activo&nbsp;
					</td>
					</tr>
				</table>
			<!-- ALQD,30/01/09. PARA MOSTRAR LAS COTIZACIONES CERRADAS SIN OC -->
				<table id="TablaTipoServicio" name="TablaTipoServicio" style="display: none;" border="0" bordercolor="blue"
					cellpadding="0" cellspacing="0" >
				    <tr>
				      <td width="18%">Grupo Servicio:</td>
					  <td colspan="1" width="35%">&nbsp;&nbsp;
						<form:select path="codGrupoServicio" id="codGrupoServicio" cssClass="combo_o" 
							cssStyle="width:180px" onchange="javascript:fc_GrupoServicio();">
							<form:option  value="-1">--Seleccione--</form:option>
							<c:if test="${control.listaCodGrupoServicio!=null}">
							<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
								items="${control.listaCodGrupoServicio}" />
							</c:if>
						</form:select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  </td>
				      <td width="18%" align="right">Tipo Servicio:&nbsp;&nbsp;&nbsp;&nbsp;</td>
					  <td width="29%" align="left">	<form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto" 
									cssStyle="width:150px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<form:option value="-1">-- Todos --</form:option>
									<c:if test="${control.listaCodTipoServicio!=null}">
									<form:options itemValue="codSecuencial" itemLabel="descripcion" 
										items="${control.listaCodTipoServicio}" />
									</c:if>
							</form:select>
				     </td>
				   </tr>
			   </table>
			
		   </td>
		   
		</tr>
		<tr>
			<td>Proveedor :</td>
			<td>
				<form:input path="txtProveedor1" id="txtProveedor1" cssClass="cajatexto"
							onkeypress="fc_PermiteNumeros();"
							onblur="fc_ValidaNumeroOnBlur(this.id);"
							cssStyle="width:65px" maxlength="11" />&nbsp;
				<form:input path="txtProveedor2" id="txtProveedor2" cssClass="cajatexto"
							onkeypress="fc_ValidaTextoEspecial();"
							onblur="fc_ValidaSoloLetrasFinal1(this,'Proveedor');"
							cssStyle="width:200px" maxlength="50" />&nbsp;
			</td>
			<td width="10%" align="left">&nbsp;Estado :</td>
			<td width="20%">
				<form:select path="codEstado" id="codEstado" cssClass="cajatexto" cssStyle="width:150px" onchange="javascript:fc_Estado();">
					<form:option  value="-1">--Todos--</form:option>
						<c:if test="${control.listaCodEstado!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaCodEstado}"/>
						</c:if>
				</form:select>
			</td>
			<td align="right" colspan="2">
				<input Type="checkbox" name="checkbox1"
				    <c:if test="${control.valSelec1==control.consteCotSinOc}"><c:out value=" checked=checked " /></c:if>
					id="checkbox1" onclick="javascript:fc_SeleccSinOc('1');">Cots. sin OC generada&nbsp;
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
				<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle" 
					 style="cursor: pointer; 60px" 
					 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
				
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle" 
					 style="cursor: pointer; margin-bottom: " onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExcel','','${ctx}/images/iconos/exportarex2.jpg',1)">
				<img src="${ctx}/images/iconos/exportarex1.jpg" alt="Limpiar" align="middle" 
					 style="cursor: pointer; margin-right" 
					 onclick="javascript:fc_Excel();" id="imgExcel"></a>
			</td>
		</tr>
	</table>

	<!-- BANDEJA -->
	<table cellpadding="0" cellspacing="0"  style="margin-top: 6px" id="TablaBandejaBien" 
		        name="TablaBandejaBien" width="100%" border="0" bordercolor="red">
		<tr>
			<td>
				<div style="overflow: auto; height:320px;width:100%">
					<table cellpadding="0" cellspacing="1" class="" bordercolor="red" border="0"
					 style="border: 1px solid #048BBA;width:95%;margin-left:9px">
						<tr>
							<td class="grilla" width="4%">Sel.</td>
							<td class="grilla" width="16%">Nro. Cotización</td>
							<td class="grilla" width="10%">Fecha Cotización</td>
							<td class="grilla" width="10%">Fecha Inicio Vigencia</td>
							<td class="grilla" width="10%">Fecha Fin Vigencia</td>
							<td class="grilla" width="20%">Tipo de Pago</td>
							<td class="grilla" width="20%">Estado</td>
							<td class="grilla" width="10%">Nro. Orden</td>
						</tr>	
						<c:forEach varStatus="loop" var="lista" items="${control.listaBandeja}"  >
						<tr class="texto">
					   		<td align="center" class="tablagrilla" style="width: 4%">
								<input type="radio" name="producto"
									id='valBa<c:out value="${loop.index}"/>'
									onclick="fc_SeleccionarRegistro('<c:out value="${lista.codigo}" />',
									'<c:out value="${lista.codOrdenCompra}"/>', '<c:out value="${lista.codEstado}"/>',
									'<c:out value="${lista.nroCotizacion}"/>','<c:out value="${lista.codTipoPago}"/>');">
							
							</td>
							<td align="center" class="tablagrilla" style="width: 16%">
								<c:out value="${lista.nroCotizacion}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.fechaEmision}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.fechaInicio}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.fechaFin}" />
							</td>
							<td align="left" class="tablagrilla" style="width: 20%">
								<c:out value="${lista.dscTipoPago}" />
							</td>
							<td align="left" class="tablagrilla" style="width: 20%">
								<c:out value="${lista.dscEstado}" />
							</td>
							<td align="center" class="tablagrilla" style="width: 10%">
								<c:out value="${lista.nroOrdenCompra}" />
							</td>
						</tr>
						</c:forEach>
					    <c:if test='${control.banListaBandeja=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="8" height="20px">No se encontraron registros
							</td>
						</tr>
						</c:if>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<table style="margin-top: 10px;" align="center">
		<tr>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgVerCot','','${ctx}/images/botones/vercot2.jpg',1)">
					<img alt="Ver Cotizaciones" src="${ctx}/images/botones/vercot1.jpg" id="imgVerCot" style="cursor:pointer;" onclick="javascript:fc_VerCotizaciones();">
			    </a>
			    <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGenOrd','','${ctx}/images/botones/generarorden2.jpg',1)">
					<img alt="Generar Orden" src="${ctx}/images/botones/generarorden1.jpg" id="imgGenOrd" style="cursor:pointer;" onclick="javascript:fc_GenerarOrden();">
			    </a>
			</td>
		</tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>
</html>