<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style>
.tablagrilla02
{
    /*BACKGROUND-COLOR: #CEE3F6;*/
    BACKGROUND-COLOR: #C8E8F0;
	FONT-SIZE: 8pt;
    /*TEXT-TRANSFORM: capitalize;*/
    WIDTH: 100%;
    FONT-FAMILY: Arial;
    color: #606060;
}
</style>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	var seleccionTextArea="";
	var indiceGeneral = "-1";
	function onLoad(){		
		objMsg=document.getElementById("txhMsg");
		//alert(objMsg.value);
		if ( objMsg.value == "OK_GRABAR" ){		
			alert(mstrGrabar);
		}
		else if ( objMsg.value == "OK_ENVIAR" ){		
			alert(mstrExitoEnvioCot);
			fc_Regresar();
		}
		else if ( objMsg.value == "ERROR_GRABAR" ){
			alert(mstrProblemaGrabar);
		}
		else if( objMsg.value == "ERROR_QUITAR"){
			alert(mstrErrorQuitarCot);
		}
		else if( objMsg.value == "ERROR_ENVIAR"){
			alert(mstrErrorEnvioCot);
		}
		else if( objMsg.value == "ERROR_PROV"){
			alert(mstrAsignarProvebyDetalle);
		}
		else if( objMsg.value == "ERROR_DATOS"){
			alert(mstrLlenarCamposObli);
		}
		//para saber que no existe un acotizacion existente
		else if(document.getElementById("txhCodCotizacion").value==""){
			alert(mstrNoEncontradoCotPendiente);
			fc_Regresar();
		}	
		document.getElementById("txhMsg").value="";
		document.getElementById("txhOperacion").value="";		
	}	
	function fc_MostrarDetalleRegistro(seleccion,codigo){
		//ALQD,06/04/09. COLOREANDO EL REGISTRO SELECCIONADO
		fc_ResaltarRegistro(seleccion);
		seleccionTextArea=seleccion;
		tablaDetalle.style.display="";
		fc_MostrarDetalleRegistroServicios(seleccion);
	}	
	function fc_MostrarDetalleRegistroServicios(seleccion){
		//***********************************************
		codCotizacion=document.getElementById("txhCodCotizacion").value;
		codDetCotizacion=document.getElementById("txhCodigo"+seleccion).value;
		condicion="0";
		codUsuario=document.getElementById("txhCodUsuario").value;
		document.getElementById("iFrameProveedoresSugeridos").src="${ctx}/logistica/CotDetBandejaProveedores.html?txhCodCotizacion="+codCotizacion+
			"&txhCodDetCotizacion="+codDetCotizacion+
			"&txhCondicion="+condicion+
			"&txhCodUsuario="+codUsuario;
		//***********************************************		
		//documentos relacionados		
		document.getElementById("iFrameDocumentosRelacionadosActivo").src="${ctx}/logistica/CotDetBandejaDocumentosByItem.html?txhCodCotizacion="+codCotizacion+
			"&txhCodDetCotizacion="+codDetCotizacion+
			"&txhCondicion="+condicion+
			"&txhCodUsuario="+codUsuario+
			"&txhTipoAdjunto=1&txhTipoProcendencia=0";
			
		document.getElementById("txtDescripcionActivo01").value=document.getElementById("txhDescripcionCot"+seleccion).value;
		document.getElementById("txtDescripcionActivo02").value=document.getElementById("txhDescripcion"+seleccion).value;
	}	
	function fc_SeleccionarRegistro(codigoDetalle,seleccion){		
		document.getElementById("txhSeleccion").value=codigoDetalle;
		document.getElementById("txhCodDetalle").value=codigoDetalle;
	}
	function fc_Grabar(){
		if(document.getElementById("cboPago").value==""){
			alert(mstrSelTipoPago);
			document.getElementById("cboPago").focus();
			return;
		}
		if(document.getElementById("txtFechaVigenciaIni").value==""){
			alert(mstrIngFechaIniVige);
			document.getElementById("txtFechaVigenciaIni").focus();
			return;
		}
		if(document.getElementById("txtHoraVigenciaIni").value==""){
			alert(mstrIngHoraIniVige);
			document.getElementById("txtHoraVigenciaIni").focus();
			return;		
		}
		if(document.getElementById("txtFechaVigenciaFin").value==""){
			alert(mstrIngFechaFinVige);
			document.getElementById("txtFechaVigenciaFin").focus();
			return;
		}
		if(document.getElementById("txtHoraVigenciaFin").value==""){
			alert(mstrIngHoraFinVige);
			document.getElementById("txtHoraVigenciaFin").focus();
			return;
		}
		fc_DatosBandejaSolReqBien();
		if(confirm(mstrSeguroGrabar)){
			document.getElementById("txhOperacion").value="GRABAR";
			document.getElementById("frmMain").submit();
		}
	}
	function fc_AsignarDescripcionActivo(){		
		document.getElementById("txhDescripcionCot"+seleccionTextArea).value=document.getElementById("txtDescripcionActivo01").value;		
	}
	function fc_DatosBandejaSolReqBien(){		
		tabla=document.getElementById("tablaBandeja");
		hijosTr=tabla.getElementsByTagName('tr');		
		numeroHijoTabla = hijosTr.length-1;
		
		var cadenaCodDetalle="";
		var cadenaDescripcion="";
		
		for(var i=0; i<numeroHijoTabla;i++){
			if(i==0){
				cadenaCodDetalle=document.getElementById("txhCodigo"+i).value+"|";				
				cadenaDescripcion=document.getElementById("txhDescripcionCot"+i).value+"|";												
			}
			else{
				cadenaCodDetalle=cadenaCodDetalle+document.getElementById("txhCodigo"+i).value+"|";				
				cadenaDescripcion=cadenaDescripcion+document.getElementById("txhDescripcionCot"+i).value+"|";				
			}
		}		
		document.getElementById("txhCadenaCodDetalle").value=cadenaCodDetalle;
		document.getElementById("txhCadenaDescripcion").value=cadenaDescripcion;
		document.getElementById("txhCantidad").value=numeroHijoTabla;
		
	}
	function fc_Regresar(){
		codSede=document.getElementById("txhCodSede").value;
		window.location.href="${ctx}/logistica/bandejaPenAtencionCotizacion.html?txhCodSede="+codSede ;
				
	}
	function fc_Quitar(){
		if (document.getElementById("txhSeleccion").value != ""){
			if( confirm(mstrEliminar) ){
				document.getElementById("txhSeleccion").value="";				
				document.getElementById("txhOperacion").value = "QUITAR";
				document.getElementById("frmMain").submit();
			}			
		}
		else{
			alert(mstrSeleccion);
			return false;
		}
	}
	function fc_Focus(id){		
		document.getElementById(id).focus();		
	}
	function fc_CopiarDescripcion(){
		document.getElementById("txtDescripcionActivo01").focus();
		texto=document.getElementById("txtDescripcionActivo01").value;
		texto=texto+document.getElementById("txtDescripcionActivo02").value;
		document.getElementById("txtDescripcionActivo01").value=texto;
	}
	function fc_ValidaFechaIngresada(idFecha,idHora){		
		objFecha = document.getElementById(idFecha);
		if (objFecha.value== "")
		{
			window.event.returnValue=0;
			alert(mstrIngFechaAntesHora);		
			document.getElementById(idHora).focus();
		}
	}
	function fc_FechaMayor(opcion){
		 srtFechaInicio=document.getElementById("txtFechaVigenciaIni").value;
	     srtFechaFin=document.getElementById("txtFechaVigenciaFin").value;
	     var num=0;
	     var srtFecha="0";
		if((opcion=="1" && srtFechaFin!="") || (opcion=="2"/* && srtFechaInicio!=""*/))    
		{  if(opcion=="2" && srtFechaInicio==""){
		       alert("Debe ingresar una fecha inicial de vigencia de cotizaci�n.");
		       return false;
		   }   
			if(srtFechaInicio!="" && srtFechaFin!="")
		   {  	num=fc_ValidaFechaIniFechaFin(srtFechaInicio,srtFechaFin);
		        //alert(num);
		          	if(num==1) 	srtFecha="0";
		           else srtFecha="1";	    
		   }
			if(srtFecha=="0"){ alert(mstrValFecIniFecFinVige);
			//document.getElementById("txtFechaVigenciaFin").focus();
						   return false;
	    	}
		}
		
		      
		/*fechaIni=document.getElementById("txtFechaVigenciaIni").value;
		fechaFin=document.getElementById("txtFechaVigenciaFin").value;
		if(fechaIni!="" && fechaFin!=""){
			if(fechaIni > fechaFin){
				alert(mstrValFecIniFecFinVige);
				if(opcion=='1'){
					document.getElementById("txtFechaVigenciaIni").value="";
					document.getElementById("txtFechaVigenciaIni").focus();
				}
				else{
					document.getElementById("txtFechaVigenciaFin").value="";
					document.getElementById("txtFechaVigenciaFin").focus();
				}
				return false;
			}
		}*/
	}
	function fc_CondicionesCompra(){
		codUsuario=document.getElementById("txhCodUsuario").value;
		codCotizacion=document.getElementById("txhCodCotizacion").value;		
		Fc_Popup("${ctx}/logistica/agregarCondicionCompra.html?txhCodUsuario="+codUsuario+
				"&txhCodCotizacion="+codCotizacion,500,300);		
	}
	function fc_Enviar(){		
		if(confirm(mstrConfEnviarCoti)){
			document.getElementById("txhOperacion").value="ENVIAR";
			document.getElementById("frmMain").submit();
		}
	}
	function ismaxlength(obj,tamanio){		
		var mlength=parseInt(tamanio);
		if (obj.value.length>mlength){
			obj.value=obj.value.substring(0,mlength)
		}
	}	
	function fc_Exportar(){
	u = "0020";
	url="?tCodRep=" + u + 
			"&tPago=" + frmMain.cboPago.options[frmMain.cboPago.selectedIndex].text + 
			"&Vc=" + document.getElementById("txtFechaVigenciaIni").value +
			"&Vc1=" + document.getElementById("txtFechaVigenciaFin").value +
			"&Hi=" + document.getElementById("txtHoraVigenciaIni").value +
			"&Hi1=" + document.getElementById("txtHoraVigenciaFin").value +
			"&Ccot=" + document.getElementById("txhCodCotizacion").value + 
			"&txhCodSede=" + document.getElementById("txhCodSede").value +
			"&Ctco=" + document.getElementById("txhCodTipoCotizacion").value;
		window.open("/SGA/logistica/reportesLogistica.html"+url,"Reportes","resizable=yes, menubar=yes");
	}
	
	function fc_TipoPago(){ 
	   if(document.getElementById("cboPago").value!=""){
	      if(document.getElementById("cboPago").value==document.getElementById("txhConsteCajaChica").value){
			if(document.getElementById("txhFlag").value=="1")
				{ document.getElementById("txtFechaVigenciaIni").value=document.getElementById("txhFechaVigenciaIniBD").value;
				  document.getElementById("txtHoraVigenciaIni").value=document.getElementById("txhHoraVigenciaIniBD").value;
				  document.getElementById("txtFechaVigenciaFin").value=document.getElementById("txhFechaVigenciaFinBD").value;
				  document.getElementById("txtHoraVigenciaFin").value=document.getElementById("txhHoraVigenciaFinBD").value;
			   }
			  
		   }
		  else if(document.getElementById("cboPago").value==document.getElementById("txhConsteOrdenCompra").value){
				if(document.getElementById("txhFlag").value=="1")
						{ document.getElementById("txtFechaVigenciaIni").value=document.getElementById("txhFechaVigenciaIniBD2").value;
						  document.getElementById("txtHoraVigenciaIni").value=document.getElementById("txhHoraVigenciaIniBD2").value;
						  document.getElementById("txtFechaVigenciaFin").value="";
				          document.getElementById("txtHoraVigenciaFin").value="";
					   }
					  
				   }
		  
		}
		else{ if(document.getElementById("txhFlag").value=="1") 
				{ document.getElementById("txtFechaVigenciaIni").value="";
				  document.getElementById("txtHoraVigenciaIni").value="";
				  document.getElementById("txtFechaVigenciaFin").value="";
				  document.getElementById("txtHoraVigenciaFin").value="";
			   }
		}
	 
	}
//ALQD,06/04/09. PARA RESALTAR EL ITEM SELECCIONADO
	function fc_ResaltarRegistro(srtIndice){
		num=parseInt(srtIndice)+1;
		srtIndice=num+"";
		srtIndice=fc_Trim(srtIndice);
		trHijo=document.getElementById("tablaBandeja").rows[srtIndice];
		if(indiceGeneral!=srtIndice){
			if(indiceGeneral!="-1")
				trHijoAnt=document.getElementById("tablaBandeja").rows[indiceGeneral];
			for(var j=0;j<6;j++){
				trHijo.cells[j].className="tablagrilla02";
				if(indiceGeneral!="-1")
					trHijoAnt.cells[j].className="tablagrilla";
			}
			indiceGeneral=srtIndice;
		}
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/CotizacionActualServicios.html">
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="seleccion" id="txhSeleccion" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codCotizacion" id="txhCodCotizacion" />
	<form:hidden path="codDetalle" id="txhCodDetalle" />	
	<form:hidden path="indInversion" id="txhIndInversion" />	
	<form:hidden path="codTipoCotizacion" id="txhCodTipoCotizacion" />
	<form:hidden path="codSubTipoCotizacion" id="txhCodSubTipoCotizacion" />
	<!-- usados para grabar -->	
	<form:hidden path="cadenaDescripcion" id="txhCadenaDescripcion" />
	<form:hidden path="cadenaCodDetalle" id="txhCadenaCodDetalle" />
	<form:hidden path="cantidad" id="txhCantidad" />
	
	<form:hidden path="consteCajaChica" id="txhConsteCajaChica" />
	<form:hidden path="consteOrdenCompra" id="txhConsteOrdenCompra" />
	
	<form:hidden path="fechaVigenciaIniBD" id="txhFechaVigenciaIniBD" />
	<form:hidden path="horaVigenciaIniBD" id="txhHoraVigenciaIniBD" />
	<form:hidden path="fechaVigenciaFinBD" id="txhFechaVigenciaFinBD" />
	<form:hidden path="horaVigenciaFinBD" id="txhHoraVigenciaFinBD" />
	<form:hidden path="fechaVigenciaIniBD2" id="txhFechaVigenciaIniBD2" />
	<form:hidden path="horaVigenciaIniBD2" id="txhHoraVigenciaIniBD2" />
	<form:hidden path="flag" id="txhFlag" />
	<!-- /usados para grabar -->
	
	<!--T�tulo de la P�gina  -->	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
		 		<font style="">		 			
				 	Cotizaci�n Actual (Servicios)
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0" >
		<tr>
			<td width="10%">&nbsp;&nbsp;Tipo Pago:</td>
			<td width="30%">
				<form:select path="cboTipoPago" id="cboPago" cssStyle="width:150px" cssClass="cajatexto_o" 
				             onchange="javascript:fc_TipoPago();">
					<form:option value="">--Seleccione--</form:option>
					<c:if test="${control.listaPago!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion" 
							items="${control.listaPago}" />
					</c:if>
				</form:select>				
			</td>
			<td width="15%" align="right">Vigencia Cotizaci�n: &nbsp;&nbsp;&nbsp;</td>
			<td width="25%">
				<form:input path="fechaVigenciaIni" id="txtFechaVigenciaIni"
					cssClass="cajatexto_o" cssStyle="width: 40%" maxlength="10"				
					onkeypress="fc_ValidaFecha('txtFechaVigenciaIni');"
					onblur="fc_FechaOnblur('txtFechaVigenciaIni');fc_FechaMayor('1');"/>
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaVigenciaIni','','${ctx}/images/iconos/calendario2.jpg',1)" onclick="fc_Focus('txtFechaVigenciaIni');">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaVigenciaIni" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>				
				<form:input path="horaVigenciaIni" id="txtHoraVigenciaIni"
					maxlength="5"
					onkeypress="fc_ValidaHora('txtHoraVigenciaIni');fc_ValidaFechaIngresada('txtFechaVigenciaIni','txtHoraVigenciaIni');"
					onblur="FP_ValidaHoraOnblur(this.id);fc_ValidaRangoHoraFechaOnBlur('','txtFechaVigenciaIni','txtFechaVigenciaFin','txtHoraVigenciaIni','txtHoraVigenciaFin','1');"
					cssClass="cajatexto_o" cssStyle="width: 20%" />hrs.
			</td>
			<td width="20%">
				<form:input path="fechaVigenciaFin" id="txtFechaVigenciaFin"
					cssClass="cajatexto_o" cssStyle="width: 40%" maxlength="10"				
					onkeypress="fc_ValidaFecha('txtFechaVigenciaFin');"
					onblur="fc_FechaOnblur('txtFechaVigenciaFin');fc_FechaMayor('2');"/>
					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFechaVigenciaFin','','${ctx}/images/iconos/calendario2.jpg',1)" onclick="fc_Focus('txtFechaVigenciaFin');">
					<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFechaVigenciaFin" alt="Calendario" style='cursor:hand;' align="absmiddle">
				</a>				
				<form:input path="horaVigenciaFin" id="txtHoraVigenciaFin"
					maxlength="5"
					onkeypress="fc_ValidaHora('txtHoraVigenciaFin');fc_ValidaFechaIngresada('txtFechaVigenciaFin','txtHoraVigenciaFin');"
					onblur="FP_ValidaHoraOnblur(this.id);fc_ValidaRangoHoraFechaOnBlur('','txtFechaVigenciaIni','txtFechaVigenciaFin','txtHoraVigenciaIni','txtHoraVigenciaFin','2');"
					cssClass="cajatexto_o" cssStyle="width: 20%" />hrs.				
			</td>
		</tr>
	</table>	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:97%;margin-top:6px;margin-left:9px;">
		<tr>
			<td>
				<div style="overflow: auto; height: 140px">
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>						
						<td class="grilla" width="4%">Sel.</td>
						<td class="grilla" width="10%">Nro. Req.</td>
						<td class="grilla" width="20%">U.Solicitante</td>
						<td class="grilla" width="15%">Grupo Servicio</td>
						<td class="grilla" width="15%">Tipo Servicio</td>
						<td class="grilla" width="20%">Nombre Servicio</td>
						<td class="grilla" width="10%">Fec. Entrega Deseada</td>
						<td class="grilla" width="6%">Nro. Prov.</td> 													
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaCotizacion}"  >
					<tr class="texto" ondblclick="fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.codigo}" />');" style="cursor: hand;">						
						<td align="center" class="tablagrilla" style="width: 4%">
							<input type="radio" name="producto"
								value='<c:out value="${lista.codDetalle}" />'
								id='val<c:out value="${loop.index}"/>'
								onclick="fc_SeleccionarRegistro(this.value,'<c:out value="${loop.index}"/>');">
							<input type="hidden" 
								id='txhCodigo<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codDetalle}" />'>
							<input type="hidden" 
								id='txhDescripcion<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.descripcion}" />'>
							<input type="hidden" 
								id='txhDescripcionCot<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.descripcionCot}" />'>
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.nroRequerimiento}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.producto}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 15%">
							<c:out value="${lista.unidadMedida}" />														
						</td>
						<td align="left" class="tablagrilla" style="width: 20%">
							<c:out value="${lista.cantidad}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.fechaEntrega}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 6%">						
							<c:out value="${lista.nroProveedores}" />							
						</td>	
					</tr>
					</c:forEach>
				</table>
				</div>				
			</td>			
			<td width="30px" valign="middle" align="right">
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoEliminar','','${ctx}/images/iconos/kitar2.jpg',1)">
					<img src="${ctx}/images\iconos\kitar1.jpg" alt="Eliminar" style="cursor:hand"id="icoEliminar" onclick="fc_Quitar();">
				</a>
				<br/>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="4" id="tablaDetalle" width="98%" border="0" bordercolor="blue" class="tabla2" style="display: none;margin-left:4px;">
		<tr>
			<td width="50%">
				<iframe id="iFrameProveedoresSugeridos" name="iFrameProveedoresSugeridos" frameborder="0" height="120px" width="100%">
				</iframe>									
			</td>			
		</tr>		
		<tr>			
			<td width="50%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td>Descripci�n - Comprador</td>	
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<textarea id="txtDescripcionActivo01" class="cajatexto_o" style="width:400px;height:65px;" onblur="fc_AsignarDescripcionActivo();" onkeyup="return ismaxlength(this,'4000');">
							</textarea>
						</td>	
					</tr>
				</table>							
			</td>
			<td width="50%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td>Descripci�n</td>	
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<textarea id="txtDescripcionActivo02" class="cajatexto_1" readonly style="width:400px;height:65px;"></textarea>
						</td>
						<td valign="middle" width="27px">
							<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoCopiar','','${ctx}/images/iconos/copiar2.jpg',1)">
								<img src="${ctx}/images\iconos\copiar1.jpg" alt="Copiar" style="cursor:hand" id="icoCopiar" onclick="fc_CopiarDescripcion();">
							</a>
						</td>
					</tr>
				</table>				
			</td>				
		</tr>
		<tr>
			<td colspan="2">
				<iframe id="iFrameDocumentosRelacionadosActivo" name="iFrameDocumentosRelacionadosActivo" frameborder="0" height="120px" width="100%">
				</iframe>
			</td>
		</tr>		
	</table>
	<!-- juego de botones -->
	<table align="center">
		<tr>
			<td align=center valign="top">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar" style="cursor:pointer;" onclick="fc_Regresar();"></a><td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgCondicionesCompra','','${ctx}/images/botones/condicionescompra2.jpg',1)">
				<img alt="Condiciones de Compra" src="${ctx}/images/botones/condicionescompra1.jpg" id="imgCondicionesCompra" style="cursor:pointer;" onclick="fc_CondicionesCompra();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imgGrabar" style="cursor:pointer;" onclick="fc_Grabar();"></a></td>
			<td>
					<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExportar','','${ctx}/images/botones/exportar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/exportar1.jpg" id="imgExportar" style="cursor:pointer;" onclick="fc_Exportar();"></a></td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviarCotizacion','','${ctx}/images/botones/enviarcot2.jpg',1)">
				<img alt="Enviar" src="${ctx}/images/botones/enviarcot1.jpg" id="imgEnviarCotizacion" style="cursor:pointer;" onclick="fc_Enviar();"></a></td>
		</tr>
	</table>
		
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFechaVigenciaIni",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaVigenciaIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFechaVigenciaFin",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFechaVigenciaFin",
		singleClick    :    true
	});		
</script>
</body>