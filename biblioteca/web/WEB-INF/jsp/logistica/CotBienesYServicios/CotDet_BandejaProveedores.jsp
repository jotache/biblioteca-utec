<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){

			if ( document.getElementById("txhCondicion").value == "1" )
			{
				document.getElementById("icoAgregar").style.display = "none";
				document.getElementById("icoEliminar1").style.display = "none";
				document.getElementById("icoGrabar1").style.display = "none";
			}
			
			strMsg = fc_Trim(document.getElementById("txhMsg").value);
			if ( strMsg == "ERRORELI" ) alert(mstrNoElimino);
			else{ if ( strMsg == "OKELI" ) alert(mstrElimino);
			      else{ if( strMsg == "EXITO" ) alert(mstrSeGraboConExito);
			            else if(strMsg == "FALLO" ) alert(mstrProblemaGrabar);
			      }
			}
			
			document.getElementById("txhMsg").value = "";
			document.getElementById("txhCodIndicesSeleccionados").value = "";
		}
		
		function fc_seleccionarRegistro( codProv )
		{
			document.getElementById("txhProveedor").value = codProv;
		}
		
		function fc_Eliminar()
		{
			if ( document.getElementById("txhProveedor").value == "")
			{
				alert(mstrSeleccione);
				return false;
			}
			
			if ( !confirm(mstrSeguroEliminar1)) return false;
			
			document.getElementById("txhAccion").value = "ELIMINAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_Agregar()
		{
			var url = "";
			url = "${ctx}/logistica/CotDetAgregarProveedores.html";
			url = url + "?txhCodCotizacion=" + document.getElementById("txhCodCotizacion").value;
			url = url + "&txhCodDetCotizacion=" + document.getElementById("txhCodDetCotizacion").value;
			url = url + "&txhCodUsuario=" + document.getElementById("txhCodUsuario").value;
			Fc_Popup(url,500,400,"");
		}
		
		function fc_Refrescar(){
			document.getElementById("txhAccion").value = "BUSCAR";
			document.getElementById("frmMain").submit();
		}
		
		function fc_Grabar(){
		    fc_ValidarCorreos();
		    if ( confirm(mstrSeguroGrabar))
		    {
			    document.getElementById("txhAccion").value = "GRABAR";
				document.getElementById("frmMain").submit();
			}
		}
		
		function fc_CambiaIndicaEnvCorreo(){
		
	}
	
	function fc_ValidarCorreos(){
	  madfrog=document.getElementById("txhLongFlagCorreo").value;
	
	  var num=parseInt(madfrog); 
	  
	   for(a=0; a<num;a++)
	   { if(a==(num-1))
	     {if(document.getElementById("chkIndEnvCorreo"+a).checked)
	         document.getElementById("txhCodIndicesSeleccionados").value=document.getElementById("txhCodIndicesSeleccionados").value+"1";
	      else document.getElementById("txhCodIndicesSeleccionados").value=document.getElementById("txhCodIndicesSeleccionados").value+"0";
	      
	     }
	     else
	     {
	       if(document.getElementById("chkIndEnvCorreo"+a).checked)
	        document.getElementById("txhCodIndicesSeleccionados").value=document.getElementById("txhCodIndicesSeleccionados").value+"1|";
	       else document.getElementById("txhCodIndicesSeleccionados").value=document.getElementById("txhCodIndicesSeleccionados").value+"0|";
	     
	     }
	   }
	  
	}
	
</script>
</head>

<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/CotDetBandejaProveedores.html" >
<form:hidden path="operacion" id="txhAccion"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codCotizacion" id="txhCodCotizacion"/>
<form:hidden path="codCotizacionDet" id="txhCodDetCotizacion"/>
<form:hidden path="codProveedor" id="txhProveedor"/>
<form:hidden path="condicion" id="txhCondicion"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="codIndicesSeleccionados" id="txhCodIndicesSeleccionados"/>
<form:hidden path="longFlagCorreo" id="txhLongFlagCorreo"/>

<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
	<tr>
		<td class="">Proveedores Sugeridos</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0"	class="tabla" width="95%">
	<tr>
		<td width="95%">
			<div style="overflow: auto; height: 80px; width: 100%">
				<display:table name="sessionScope.listaProveedores" cellpadding="0" cellspacing="1"
				decorator="com.tecsup.SGA.bean.ProveedorCotizacionDecorator" 
				style="border: 1px solid #048BBA;width:96%;">
					<display:column property="rbtSelProveedor" title="Sel." 
						headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
					<display:column property="razonSocial" title="Proveedor" headerClass="grilla" 
						class="tablagrilla" style="text-align:left;width:60%"/>
					<display:column property="dscTipoPersona" title="Tipo<br>Persona" headerClass="grilla" 
						class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="dscCalificacion" title="Calificación" headerClass="grilla" 
						class="tablagrilla" style="text-align:left;width:15%"/>
					<display:column property="checkSelBandeja" title="Enviar<br>Correo" headerClass="grilla" 
						class="tablagrilla" style="text-align:center;width:5%"/>
						
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
					<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
					<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
					<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
					<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
					<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
				</display:table>
				<%request.getSession().removeAttribute("listaProveedores"); %>
			</div>
		</td>
		<td valign="middle" width="5%" align="center">
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoAgregar','','${ctx}/images/iconos/agregar2.jpg',1)">
			<img src="${ctx}\images\iconos\agregar1.jpg" onclick="javascript:fc_Agregar();" alt="Agregar" style="cursor:hand;" id="icoAgregar"></a>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoEliminar1','','${ctx}/images/iconos/kitar2.jpg',1)">
			<img src="${ctx}\images\iconos\kitar1.jpg" onclick="javascript:fc_Eliminar();" alt="Eliminar" style="cursor:hand"id="icoEliminar1"></a>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('icoGrabar1','','${ctx}/images/iconos/grabar2.jpg',1)">
			<img src="${ctx}\images\iconos\grabar1.jpg" onclick="javascript:fc_Grabar();" alt="Grabar" style="cursor:hand"id="icoGrabar1"></a>
		</td>
	</tr>
</table>
</form:form>
