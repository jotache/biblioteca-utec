<%@ include file="/taglibs.jsp"%>

<head>
<style type="text/css">
	.resaltado { 
		font-family: Arial, Helvetica, sans-serif; 
		font-size: 8pt; 
		color: #000000; 
		font-style: normal; 
		border: #000000 solid; 
		font-weight: none; 
		background-color: #FFFF00; 
		cursor: hand; 
		border-width: 0px 0px 0px 0px 
	}
	.tablagrillared
	{
	    background-color: red;
		font-size: 8pt; 
	    font-family: Arial;
	    color: #000000;    
	}	
</style>

<script language="javascript">

	function fc_CambiarFechas(){

		if (document.getElementById("txtFecIni").value=='' || document.getElementById("txtFecIni").length==0){
			alert('Ingrese la Fecha de Inicio');
			document.getElementById("txtFecIni").focus();
			return false;			
		}
		
		if (document.getElementById("txtHoraIni").value=='' || document.getElementById("txtHoraIni").length==0){
			alert('Ingrese la Hora de Inicio');
			document.getElementById("txtHoraIni").focus();
			return false;			
		}

		if (document.getElementById("txtFecFin").value=='' || document.getElementById("txtFecFin").length==0){
			alert('Ingrese la Fecha de fin');
			document.getElementById("txtFecFin").focus();
			return false;			
		}
		
		if (document.getElementById("txtHoraFin").value=='' || document.getElementById("txtHoraFin").length==0){
			alert('Ingrese la Hora de Fin');
			document.getElementById("txtHoraFin").focus();
			return false;			
		}

		if (confirm('�Seguro de actualizar las fechas?')){		
			document.getElementById("operacion").value ="CAMBIAR_FECHAS";
			document.getElementById("frmMain").submit();	
		}
	}

	function onLoad(){ 
		
		var mensaje = "<%=(( request.getAttribute("mensaje")==null)?"": request.getAttribute("mensaje"))%>";	
		
		if(mensaje!="")
			alert(mensaje);
				
		fc_ini();
		fc_iniSelUsuario();
		fc_iniResta();
		//DESHABILITA LA TODOS LOS INPUT DE LA BANDEJA SI SE ENCUENTRA EN ESTADO CERRADA
		fc_DesabilitaBandeja()			
	}

	function fc_radio(op){	
		if( op == "2" ){
			srtCodUsuario = document.getElementById("usuario").value;
			srtCodSede = document.getElementById("txhCodSede").value;
			srtCodCotizacion = document.getElementById("txhCodCotizacion").value;
			srtCodEstCotizacion =  document.getElementById("txhCodEstCotizacion").value;
			srtNumCotizacion =  document.getElementById("txhNumCotizacion").value;
									
			location.href="${ctx}/logistica/CotizacionBienesProveedor.html?"+
				"txhCodUsuario="+ srtCodUsuario+
				"&txhCodSede="+srtCodSede+ 
				"&txhCodCotizacion="+srtCodCotizacion+
				"&txhCodProveedor="+srtCodUsuario+
				"&txhCodEstCotizacion="+srtCodEstCotizacion+
				"&prmFlgComprador=true" +
				"&txhCodTipoReq="+document.getElementById("txhCodTipoReq").value+
				"&txhNumCotizacion="+srtNumCotizacion;
		}
	}

	function fc_Regresar(){
			codSede=document.getElementById("txhCodSede").value;
			window.location.href="${ctx}/logistica/bandejaSolSolicitudCotizacion.html?txhCodSede="+codSede;
			
	}
	
	function fc_Recalcula(){
		fc_iniSelUsuario();
		fc_iniResta();
	}

	function fc_OcultarDetalleProveedor(){
		document.getElementById("iFrameDetalleProv").style.display = "none";
	}

	function fc_VerDetalleProveedor(codDetalle,codUsuario,obs,obsProv){
	
	document.getElementById("iFrameDetalleProv").style.display = "";
	document.getElementById("txaObservaciones").value = document.getElementById("txhObs"+obs+"_"+obsProv).value;
	document.getElementById("txaObservacionesProveedor").value = document.getElementById("txhObsProv"+obs+"_"+obsProv).value;
	
	codCotizacion = document.getElementById("txhCodCotizacion").value;	
	//*******************************************************************************
	document.getElementById("iFrameAdjunto").src="${ctx}/logistica/CotDetBandejaDocumentosByItem.html?txhCodCotizacion="+codCotizacion+
				"&txhCodDetCotizacion="+codDetalle+
				"&txhCondicion=1"+
				"&txhCodUsuario="+ //codUsuario+
				"&txhTipoAdjunto=0&txhTipoProcendencia=1";//antes:--> txhTipoProcendencia=1
	//*******************************************************************************
	document.getElementById("iFrameAdjuntoProveedor").src="${ctx}/logistica/CotDetBandejaDocumentosByItem.html?txhCodCotizacion="+codCotizacion+
				"&txhCodDetCotizacion="+codDetalle+
				"&txhCondicion=1"+
				"&txhCodUsuario="+ codUsuario+
				"&txhTipoAdjunto=0&txhTipoProcendencia=2";
	//***********************************************
	
	}

	function fc_verProveedor(codCoti,cod,nroDoc,razon){
		Fc_Popup("/SGA/logistica/detalleProveedor.html?prmCodProveedor="+cod+"&prmCodCotizacion="+codCoti+"&prmCodRuc="+nroDoc+"&prmCodDescripcion="+razon,600,400);	
	}

	function fc_CerrarCotizacion(){
		var numItems = document.getElementById("txhNumItems").value;
		var estCot = document.getElementById("txhCodEstCotizacion").value;
				 	
	 	if(Number(numItems)>0 && estCot!="0003" ){
	 		if(confirm(mstrSeguroRealizarAccion)){
				document.getElementById("operacion").value ="irCerrarCotizacion" ;
				document.getElementById("frmMain").submit();	
			}
	 	}else{
	 		alert(mstrNoSePuedeRealizarLaAccion);
	 	}
		
	}

	function fc_Grabar(){	
		//ALQD,09/06/09. DESCONTANDO OTRO_COSTOS INCLUIDOS
		//ALQD,15/04/09. EN LA CADCODDET INCLUIRE LA CANTIDAD Y LA UNIDAD INGRESADA EN SERV. DE TIPO MANTTO
		/*INICIO DE PARAMETROS*/
		var numGrupos = document.getElementById("txhNumGrupos").value;
		var numItems = document.getElementById("txhNumItems").value;
	 	cadCodProv = "";cadCotDet = "";	cantidad = 0;
	 	cadPrecios = "";
	 	
	 	var estCot = document.getElementById("txhCodEstCotizacion").value;
		 
	 	//SI ESTADO ES DIFERENTE DE CONST_COT_EST_CERRADA CHAR(4) := '0003';
	 	//PERMITE GRABAR
	 	if(fc_Trim(estCot)!="0003"){
	 	
		 	if(Number(numItems)>0){
		 	
				for (var k = 0; k < Number(numItems); k++)
				{
					for (var j = 0; j < 3; j++)
					{
						pos = "rdo"+k+"_"+j;
						txtPrecio = "txt"+k+"_"+j;
						txtAux = "aux"+k+"_"+j;
						if(document.getElementById(pos)!=null)
						{
							if(document.getElementById(pos).checked)
							{
								val = document.getElementById(pos).value;
								cadCodProv = cadCodProv+val+"|";
								codItem = document.getElementById("hidItem"+k).value;
								//ALQD,15/04/09.AGREGANDO CANT. Y UNIDAD AL DETALLE
								if(document.getElementById("txhCodTipoReq").value=="0002" &&
								   document.getElementById("txhCodGruServicio").value=="0002"){
								elemento="hid"+k;
								codItem = codItem +"$"+ document.getElementById(elemento).value;
								combo="cboUnidad"+k;
								if(document.getElementById(combo).value=="0000"){
									codItem = codItem +"$";
								}else{
									codItem = codItem +"$"+ document.getElementById(combo).value;
								}
								}
								cadCotDet = cadCotDet+codItem+"|";
								valor = parseFloat(document.getElementById(txtPrecio).value)
									- parseFloat(document.getElementById(txtAux).value);
								valor = redondea(valor,5);
								cadPrecios = cadPrecios + valor+"|";
								cantidad = cantidad + 1;
							}
						}
					}
				}
				
				vPrecios = cadPrecios.split('|');
				var precio0=false;
				
				for (var i = 0; i < vPrecios.length-1 ; i++){
					if (parseFloat(vPrecios[i])==0){
						precio0=true;
						break;
					}
				}
				if (precio0==true){
					alert('Operaci�n Cancelada:\nHa seleccionado un producto cuyo precio de Cotizaci�n es CERO');
					return false;
				}
				
				document.getElementById("txhCantidad").value  =cantidad;
				document.getElementById("txhCadCodProv").value=cadCodProv;
				document.getElementById("txhCadCodDet").value =cadCotDet;
				document.getElementById("txhCadPrecios").value = cadPrecios;
				
				if( Number(document.getElementById("txhCantidad").value)>0 ){
//ALQD,12/11/08. ELIMINADO EL MENSAJE
//					if(confirm(mstrSeguroGrabar)){
						document.getElementById("operacion").value ="irRegistrar" ;
						document.getElementById("frmMain").submit();
//					}
				}else{
					alert(mstrSeleccione);
				}
			}
			else{
				alert(mstrNoSePuedeRealizarLaAccion);
			}
		}
		else{
			alert(mstrNoSePuedeRealizarLaAccion);
		}
	}

	function fc_Eliminar(){	
		var numItems = document.getElementById("txhNumItems").value;
		var estCot = document.getElementById("txhCodEstCotizacion").value;
		 	
	 	if( Number(numItems)>0 && estCot!="0003" ){
			if(document.getElementById("txhCodDetCotizacion").value!="")
			{	
				if(confirm(mstrSeguroEliminar1))
				{
					document.getElementById("operacion").value ="irEliminar" ;
					document.getElementById("frmMain").submit();
				}
			}else
			{
				alert(mstrSeleccione);
			}	
	
	 	}else{
	 		alert(mstrNoSePuedeRealizarLaAccion);
	 	}
	 	
	}


	function fc_ini(){
	
		var numGrupos = document.getElementById("txhNumGrupos").value;
		var numItems = document.getElementById("txhNumItems").value;
		var SumTotal = 0;
		var valor = 0;
		var nuevoTotal = 0;
		var valaux = 9999999;

		var prec_prov = 0;
		var prec_altoprov=0;
		var max="";
		
	 	//ITERA FILAS
		for (var k = 0; k < Number(numItems); k++)
		{
			min = "";						
			document.getElementById("tdx"+k).className="";			
			
			prec_prov = 0;
			prec_altoprov = 0;
			for (var j = 0; j < 3; j++)
			{
				if (document.getElementById("rdo"+k+"_"+j)!=null)
				{
					if(document.getElementById("txt"+k+"_"+j)!=null)
					{
						posActual = k+"_"+j;
						valor = parseFloat(document.getElementById("txt"+posActual).value);
						prec_prov = prec_prov + valor;

						if (valor > prec_altoprov) {
							prec_altoprov = valor;
							max = posActual;
						}
						
						if (valor>0 && valor<parseFloat(valaux)){
							valaux = valor;
						}
						
						if(min=="")
						{	
							var x = document.getElementById("txt"+posActual).value;
							
							if( parseFloat(x) > parseFloat("0") )
								min = posActual;
						}
						else
						{						
							val1 = document.getElementById("txt"+posActual).value;
							val2 = document.getElementById("txt"+min).value;
							
							if( parseFloat(val1) > parseFloat("0") )
							{
								if( parseFloat(val1) < parseFloat(val2) )
								{
									min = posActual;
								}
							}
						}						
					}
				}
				
			} //here

			if (prec_prov==0)
				document.getElementById("tdx"+k).className="resaltado";
			
			if (valaux==9999999)
				valaux=0;
			//ALQD,15/04/09. AGREGANDO CONDICIONAL PARA IDENTIFICAR SERV. DE TIPO MANTTO
			if(document.getElementById("txhCodTipoReq").value=="0002" &&
			   document.getElementById("txhCodGruServicio").value=="0002"){
				nuevoTotal = nuevoTotal + valaux;
			}else{
				nuevoTotal = nuevoTotal + valaux * parseFloat(document.getElementById("hid"+k).value);
			}
			valaux = 9999999;
			
			if(min!=""){
				posFila= min.substring(0,1)
				if(document.getElementById("txt"+min).value!="0"){
					document.getElementById("td"+min).className="tablagrillaglo2"
					//ALQD,15/04/09. AGREGANDO CONDICIONAL PARA IDENTIFICAR SERV. DE TIPO MANTTO
					if(document.getElementById("txhCodTipoReq").value=="0002" &&
					   document.getElementById("txhCodGruServicio").value=="0002"){
						SumTotal =  parseFloat(SumTotal)+ parseFloat(document.getElementById("txt"+min).value);
					}else{
						SumTotal =  parseFloat(SumTotal)+ parseFloat(document.getElementById("hid"+posFila).value)*parseFloat(document.getElementById("txt"+min).value);
					}

					var porc_m
					if (prec_altoprov>0){
						porc_m = parseFloat((prec_altoprov) * 100/130);
						if (porc_m>= parseFloat(document.getElementById("txt"+min).value))
							document.getElementById("td"+max).className="tablagrillared";
					}
					
				}
			}

		}

		document.getElementById("txtSumTotal").value=nuevoTotal;	
	}

	function fc_iniResta(){
	
		var SumSistema  = document.getElementById("txtSumTotal").value;
		var SumUsuario  = document.getElementById("txtSumTotalUsuario").value;
		
		if( SumUsuario!="" && SumSistema!="" )	
			document.getElementById("txtDiferencia").value = parseFloat(SumSistema)- parseFloat(SumUsuario);
		else
			document.getElementById("txtDiferencia").value=parseFloat("0");	
	    document.getElementById("txtDiferencia").value=redondea(document.getElementById("txtDiferencia").value,2);
	    document.getElementById("txtSumTotal").value=redondea(document.getElementById("txtSumTotal").value,2);
	    document.getElementById("txtSumTotalUsuario").value=redondea(document.getElementById("txtSumTotalUsuario").value,2);
	}

	function redondea(sVal, nDec){ 
		    var n = parseFloat(sVal); 
		    var s = "0.00"; 
		    if (!isNaN(n)){ 
				n = Math.round(n * Math.pow(10, nDec)) / Math.pow(10, nDec); 
				s = String(n); 
				s += (s.indexOf(".") == -1? ".": "") + String(Math.pow(10, nDec)).substr(1); 
				s = s.substr(0, s.indexOf(".") + nDec + 1); 
		    } 
		    return s; 
		}

	function fc_iniSelUsuario(){
	
		var numGrupos = document.getElementById("txhNumGrupos").value;
		var numItems  = document.getElementById("txhNumItems").value;
		var SumTotalUsuario = 0;
	 
		for (var k = 0; k < Number(numItems); k++)
		{
			for (var j = 0; j < 3; j++)
			{		
				pos = k+"_"+j;//POSICION ACTUAL
				if(document.getElementById("rdo"+pos)!=null)
				{
					if(document.getElementById("rdo"+pos).checked)
					{	
						//ALQD,15/04/09. AGREGANDO CONDICIONAL PARA IDENTIFICAR SERV. DE TIPO MANTTO
						if(document.getElementById("txhCodTipoReq").value=="0002" &&
						   document.getElementById("txhCodGruServicio").value=="0002"){
							SumTotalUsuario =  parseFloat(SumTotalUsuario)+ parseFloat(document.getElementById("txt"+pos).value);
						}else{
							SumTotalUsuario =  parseFloat(SumTotalUsuario)+ parseFloat(document.getElementById("hid"+k).value)*parseFloat(document.getElementById("txt"+pos).value);
						}
						//MODIFICADO RNAPA 21/08/2008
						document.getElementById("txt"+pos).readOnly = false;
						document.getElementById("txt"+pos).className = "cajatexto";
						document.getElementById("txt"+pos).style.textAlign = "right";
					}
					else{
						document.getElementById("txt"+pos).readOnly = true;
						document.getElementById("txt"+pos).className = "Texto_moneda1";
					}
				}
			}
		}
		
		document.getElementById("txtSumTotalUsuario").value=SumTotalUsuario;
	}
	
	function fc_sel(codCotizacion ,codDetalle){			
		document.getElementById("txhCodDetCotizacion").value=codDetalle;		
	}
	
	
	function fc_Exportar(){
	u = "0021";
	url="?tCodRep=" + u + 
			"&CtRe=" + document.getElementById("txhCodTipoReq").value +
			"&txhCodSede=" + document.getElementById("txhCodSede").value +
			"&Cco=" + document.getElementById("txhCodCotizacion").value;
		window.open("/SGA/logistica/reportesLogistica.html"+url,"Reportes","resizable=yes, menubar=yes");
	}
	function fc_DesabilitaBandeja(){
		var estCot = document.getElementById("txhCodEstCotizacion").value;
		
	 	if(fc_Trim(estCot) == "0003"){
			var numGrupos = document.getElementById("txhNumGrupos").value;
			var numItems  = document.getElementById("txhNumItems").value;		
		 
			for (var k = 0; k < Number(numItems); k++)
			{			
				for (var j = 0; j < 3; j++){		
					pos = k+"_"+j;//POSICION ACTUAL
					
					if(document.getElementById("rdo"+pos)!=null){
						document.getElementById("txt"+pos).readOnly = true;
						document.getElementById("txt"+pos).className = "Texto_moneda1";
						document.getElementById("rdo"+pos).disabled = true;					
					}				
				}	
			}
		}
	}
	
</script>

	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


</head>


<body topmargin="0" leftmargin="0" rightmargin="0">

<form:form commandName="control" id="frmMain"
	action="${ctx}/logistica/verCotizaion.html">

	<form:hidden path="operacion" id="operacion" />
	<form:hidden path="usuario" id="usuario" />
	<form:hidden path="txhCodCotizacion" id="txhCodCotizacion" />
	<form:hidden path="txhCodSede" id="txhCodSede" />
	<form:hidden path="txhCodEstCotizacion" id="txhCodEstCotizacion" />
	<form:hidden path="numCotizacion" id="txhNumCotizacion" />

	<!-- 1: INDICA SI EL COMPRADOR TIENE ACCESO A LA PARTE PROVEEDOR -->
	<form:hidden path="flgComprador" id="flgComprador" />
	<form:hidden path="codTipoReq" id="txhCodTipoReq" />

	<input type="hidden" id="txhCodDetCotizacion" name="txhCodDetCotizacion" />
	<input type="hidden" id="txhCantidad" name="txhCantidad" />
	<input type="hidden" id="txhCadCodDet" name="txhCadCodDet" />
	<input type="hidden" id="txhCadCodProv" name="txhCadCodProv" />
	
	<form:hidden path="cadPrecios" id="txhCadPrecios" />
	<!-- ALQD,15/04/09.NUEVA PROPIEDAD A LEER -->
	<form:hidden path="codGruServicio" id="txhCodGruServicio" />

	<c:if test="${control.flgComprador=='1'}">
		<table class="Opciones" width="97%" border="0" bordercolor="black" cellpadding="0" cellspacing="0"
			style="margin-left: 9px;">
			<tr>
				<td align="right">
					<input type="radio" id="Radio1" name="rdoBandeja"
						checked="checked" onclick="fc_radio('1')" VALUE="Radio1">Comprador
						&nbsp;&nbsp;&nbsp;
					<input type="radio" id="rdoBandeja"
						name="rdoBandeja" onclick="fc_radio('2')" VALUE="rdoBandeja">Proveedor
				</td>
			</tr>
		</table>
	</c:if>
	<div style="overflow: auto; height: 500px;width:100%">	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		style="margin-left: 9px; margin-top: 0px;width: 80%;">
		<tr>
			<td align="left"><img
				src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
			<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px"
				class="opc_combo"><font valign=bottom><c:if test="${control.codTipoReq=='0001'}">Cotizaci�n de
			Bienes</c:if><c:if test="${control.codTipoReq=='0002'}">Cotizaci�n de Servicios</c:if>
			<c:out value="::::> No. ${control.numCotizacion}"></c:out>
			</font></td>
			<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red"
		height="34px" style="margin-left: 9px; margin-top: 7px">
		<tr>
			<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td><img src="${ctx}/images/Logistica/izq2.jpg" height="34px"
						id="tdProducto01"></td>
					<td bgcolor=#00A2E4 class="opc_combo" height="34px"
						style="cursor: pointer; font-size: 12px;" id="tdProducto02">Productos</td>
					<td><img src="${ctx}/images/Logistica/der2.jpg" height="34px"
						id="tdProducto03"></td>
					<td class="texto_bold" align="right">Fec. Inicio :</td>
					<td><form:input maxlength="10" path="fecInicio" id="txtFecIni"
						onkeypress="javascript:fc_Slash('frmMain','txtFecIni','/');"
						onblur="javascript:fc_ValidaFechaOnblur('txtFecIni');"  
						cssClass="cajatexto_1" cssStyle="width:70px"/>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgfecini','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" id="imgfecini" alt="Seleccionar Fecha Inicial" 
							style="cursor:pointer; vertical-align: middle"></a>
					</td>
			
					<td class="texto_bold" align="right">
						&nbsp;&nbsp;Hora Inicio:
					</td>
					<td>
						<form:input maxlength="5" path="horaInicio" id="txtHoraIni" cssClass="cajatexto_1" 
							cssStyle="width:30px"
							onkeypress="fc_ValidaHora('txtHoraIni');"
							onblur="fc_ValidaHoraOnBlur('frmMain','txtHoraIni','hora inicial');"/>
					</td>


					<td class="texto_bold" align="right">&nbsp;&nbsp;Fec. Fin :</td>
					<td><form:input maxlength="10" path="fecFin" id="txtFecFin"
						onkeypress="javascript:fc_Slash('frmMain','txtFecFin','/');"
						onblur="javascript:fc_ValidaFechaOnblur('txtFecFin');" 
						cssClass="cajatexto_1" cssStyle="width:70px"/>
						<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgfecfin','','${ctx}/images/iconos/calendario2.jpg',1)">
						<img src="${ctx}/images/iconos/calendario1.jpg" alt="Seleccionar Fecha Final" id="imgfecfin" 
							style="cursor:pointer; vertical-align: middle"></a>&nbsp;
					</td>
					<td class="texto_bold" align="right">&nbsp;&nbsp;Hora Fin:
					</td>
					<td>
						<form:input maxlength="5" path="horaFin" id="txtHoraFin" cssClass="cajatexto_1" 
						cssStyle="width:30px" onkeypress="fc_ValidaHora('txtHoraFin');"
						onblur="fc_ValidaHoraOnBlur('frmMain','txtHoraFin','hora final');"/>
					</td>

			
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!-- 0003 //cerrada no mostrar grabar fecha -->
						<c:if test="${control.txhCodEstCotizacion!='0003'}">
							<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgGrabarFec','','${ctx}/images/iconos/grabar2.jpg',1)">
							<img src="${ctx}/images/iconos/grabar1.jpg" alt="Actualizar Fechas" id="imgGrabarFec" onclick="fc_CambiarFechas();"
								style="cursor:pointer; vertical-align: middle"></a>
						</c:if>
					</td>
				</tr>				
			</table>
			</td>
			<td>
			<table cellpadding="0" cellspacing="0" style="display: none;">
				<tr>
					<td><img src="${ctx}/images/Logistica/izq1.jpg" height="34px"
						id="tdCompra01"></td>
					<td bgcolor=#048BBA class="opc_combo"
						style="cursor: pointer; font-size: 12px;" id="tdCompra02">Condiciones
					Compra</td>
					<td><img src="${ctx}/images/Logistica/der1.jpg" height="34px"
						id="tdCompra03"></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<!-- ALQD,15/04/09.SE A�ADIRA DOS CONTROLES PARA SOLICITAR LA CANTIDAD Y UNIDAD DE MEDIDA REFERENCIA -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:97%;margin-top:6px;margin-left:9px;">
		<tr>
			<td>
			<!-- BANDEJA -->
			<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandejaProductos">
				<tr>
					<td class="grilla" width="3%">Sel.</td>
					<td class="grilla" colspan="2" width="31%">Productos Agrupados Familia/SubFamilia</td>
					<td class="grilla" width="7%">Ult.<br>Cantidad</td>
					<td class="grilla" width="7%">Ult.<br>Precio</td>
					<td class="grilla" width="7%">Unidad</td>
					<td class="grilla" width="7%">Cantidad</td>
					<td class="grilla" width="15%">Proveedor 1</td>
					<td class="grilla" width="15%">Proveedor 2</td>
					<td class="grilla" width="15%">Proveedor 3</td>
				</tr>
				<c:set var="nroGrupos" value="0" />
				<c:set var="nroItems" value="0" />
				<c:forEach var="objGrupo" items="${control.lstResultado}" varStatus="loop">
				<c:set var="nroGrupos" value="${loop.index}" />
				<!-- *****************PINTADO DE LOS PROVEEDORES ****************-->
				<tr class="tablagrilla">					
					<td colspan="7">&nbsp;</td>
					<c:forEach var="objCabecera" items="${objGrupo.cabeceras}" varStatus="loop2">
						<td onclick="fc_verProveedor('<c:out value="${control.txhCodCotizacion}" />'
							,'<c:out value="${objCabecera.codProv}" />','<c:out value="${objCabecera.nroDoc}" />'
							,'<c:out value="${objCabecera.razonSocial}" />')" class="tablagrillaglo" >
							<u style="cursor:hand;">
								<c:out value="${objCabecera.razonSocial}" />
							</u>
						</td>
					</c:forEach>					
				</tr>
				<!-- ***************** FIN PINTADO DE LOS PROVEEDORES ****************-->

				<!-- ***************** PINTADO DE ITEMS O DETALLES ****************-->
					<c:forEach var="objItem" items="${objGrupo.items}" varStatus="loop2">
						<tr class="tablagrilla">
							<td>
<%-- 							<c:out value="${objItem.unidad}"></c:out> --%>
							<!-- GUARDA EL CODIGO DEL DETALLE O ITEM --> <input
								type="hidden" id="hidItem<c:out value="${nroItems}" />"
								value="<c:out value="${objItem.codCotiDet}" />" /> <input
								type="radio" id="CheckboItem" name="CheckboItem"
								onclick="fc_sel('<c:out value="${objItem.codCoti}" />','<c:out value="${objItem.codCotiDet}" />')">
							</td>
							<td ><c:out value="${objItem.dscFam}"/> 							
							</td>
							<td colspan="">
								<label id="tdx<c:out value="${nroItems}"/>" class="">
									<c:out value="${objItem.dscBien}" />
									<c:if test="${objItem.tieneComentario=='1'}">
										<strong>
										<c:out value=" (ver detalles)"></c:out>
										</strong>
									</c:if>
								</label>
							</td>
							
							<td align="right"><c:out value="${objItem.canUltiOrden}" /></td>
							<td align="right"><c:out value="${objItem.preUltiOrden}" /></td>
							<%-- ALQD,15/04/09.PARA SERVICIOS MANTTO HABRA COMBO DE UNIDAD DE MEDIDA --%>
							<%-- JHPR 2008-09-29 --%>
							<td align="right">
							<c:if test="${control.codTipoReq=='0002' && control.codGruServicio=='0002'}">
							<select id='cboUnidad<c:out value="${nroItems}" />' style='width:100%' class='cajatexto_o'>								
								<option value="0000">--Seleccione--</option>
								<c:if test="${control.lstUniMedida!=null}">
																									
									<c:forEach var="unidad" items="${control.lstUniMedida}"  >									
										<option value="${unidad.codSecuencial}"
											<c:if test="${fn:trim(objItem.unidad)==fn:trim(unidad.codSecuencial)}">
												<c:out value="selected" />
											</c:if> 
										>										
											<c:out value="${unidad.valor1}" />
										</option>										
									</c:forEach>
								</c:if>
							</select>							
							</c:if>
							<c:if test="${!(control.codTipoReq=='0002' && control.codGruServicio=='0002')}">
							<c:out value="${objItem.unidad}" />
							</c:if>
							</td>
<!-- ALQD,04/06/09.DEFINIENDO VARIABLE QUE CONTENDRA LA SUMA DE PRECIO_UNITARIO Y OTRO_COSTO -->
							<c:set var="cantidad" value="${objItem.cantidad}" scope="session" />
							<td align="right">
							<c:if test="${control.codTipoReq=='0002' && control.codGruServicio=='0002'}">
								<input type="text" size="8" style="text-align: right;"
								id="hid<c:out value="${nroItems}" />"
								value="<c:out value="${objItem.cantidad}" />" />
							</c:if>
							<c:if test="${!(control.codTipoReq=='0002' && control.codGruServicio=='0002')}">
								<input type="hidden"
								id="hid<c:out value="${nroItems}" />"
								value="<c:out value="${objItem.cantidad}" />" /> <c:out
								value="${objItem.cantidad}" />
							</c:if>
							</td>

							<!--*********************** PRECIOS **********************************-->
							<c:forEach var="objNota" items="${objItem.precios}"
								varStatus="loop3">
								<td align="center" valign="middle"
									id="td<c:out value="${nroItems}" />_<c:out value="${loop3.index}" />">

								<!-- OBJ RADIO --> 
								<c:if test="${ objNota.precioUnitario==null || objNota.precioUnitario == '.00000' }">
								<input type="radio" onclick="fc_Recalcula()"
									disabled="true" NAME="rdo<c:out value="${nroItems}" />"
									id="rdo<c:out value="${nroItems}" />_<c:out value="${loop3.index}" />"
									value="<c:out value="${objNota.codProv}" />"
									<c:if test="${objNota.flgChecked =='1'}">  checked="checked" </c:if>>
								</c:if> 
								
								<c:if test="${ objNota.precioUnitario!=null && objNota.precioUnitario != '.00000' }">
								<input type="radio" onclick="fc_Recalcula()"
									name="rdo<c:out value="${nroItems}" />"
									id="rdo<c:out value="${nroItems}" />_<c:out value="${loop3.index}" />"
									value="<c:out value="${objNota.codProv}" />"
									<c:if test="${objNota.flgChecked =='1'}">  checked="checked" </c:if>>
								</c:if> <!--FIN OBJ RADIO-->
								<!-- LA OPERACION ES PARA CORTAR A 5 DECIMALES EL RESULTADO -->
								<input type=text class="Texto_moneda1"
									size="12" value="<c:out value="${objNota.precioUnitario+(objNota.otroCosto/cantidad-((100000*objNota.otroCosto)%cantidad)/cantidad/100000)}" />"
									id="txt<c:out value="${nroItems}" />_<c:out value="${loop3.index}" />"
									name="txt<c:out value="${loop2.index}" /><c:out value="${loop3.index}" />"
									onkeypress="fc_ValidaNumeroDecimal();"
									onblur="fc_ValidaDecimalOnBlur2(this.id,'10','5');"
									readonly="true">
								<input type="hidden"
									value="<c:out value="${objNota.otroCosto/cantidad}" />"
									id="aux<c:out value="${nroItems}" />_<c:out value="${loop3.index}" />"
								/>

								<!-- IMAGEN DETALLE PROVEEDOR -->
								
								<input type="hidden" value="<c:out value="${objNota.obs}" />"
									id="txhObs<c:out value="${nroItems}" />_<c:out value="${loop3.index}" />"
									name="txhObs<c:out value="${nroItems}" /><c:out value="${loop3.index}" />" />
									
								<input type="hidden"
									value="<c:out value="${objNota.obsProv}" />"
									id="txhObsProv<c:out value="${nroItems}" />_<c:out value="${loop3.index}" />"
									name="txhObsProv<c:out value="${nroItems}" /><c:out value="${loop3.index}" />" />
									
								<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgbajar','','${ctx}/images/iconos/bajar2.jpg',1)">
									<img  src="${ctx}/images/iconos/bajar1.jpg" style="cursor:hand" id="imgbajar"
										alt="Muestra Detalle Proveedor por Producto"
										ondblclick="fc_OcultarDetalleProveedor();"
										onclick="fc_VerDetalleProveedor('<c:out value="${objItem.codCotiDet}" />','<c:out value="${objNota.codProv}" />','<c:out value="${nroItems}" />','<c:out value="${loop3.index}" />')">
								</a>
								</td>
							</c:forEach>
							<c:remove var="cantidad" scope="session"/>
							<!--*********************** FIN PRECIOS ****************************-->

							<c:set var="nroItems" value="${nroItems+1}" />

						</tr>
					</c:forEach>
					<!-- ***************** FIN PINTADO DE ITEMS O DETALLES ****************-->
					<!--  /table-->					
				</c:forEach>
			</table>
			</td>			
				
			<td width="30px" valign="middle" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
					<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_Eliminar();"
					alt="Eliminar">
				</a>
			</td>
		</tr>
	</table>
		
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" width="94.5%">
		<tr class="texto" align="right">
			<td colspan="7">Total Asignado por el Sistema :&nbsp;S/.<input
				type=text class="Texto_moneda1" readonly value="" ID="txtSumTotal"
				NAME="Text14"></td>
		</tr>
		<tr class="texto" align="right">
			<td colspan="7">Total Asignado por el Usuario :&nbsp;S/.<input
				type=text class="Texto_moneda1" readonly value=""
				ID="txtSumTotalUsuario" NAME="Text14"></td>
		</tr>
		<tr class="texto" align="right">
			<td colspan="7">Diferencia :&nbsp;S/.<input type=text
				class="Texto_moneda1" readonly value="1.00" ID="txtDiferencia"
				NAME=""></td>
		</tr>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
	</table>

	<div id="iFrameDetalleProv" style="display: none">
	
	<table ID="DetalleProducto" name="DetalleProducto" style="margin-left: 9px;"
		cellpadding="2" cellspacing="2" class="tabla2" border="0"
		bordercolor="red" width="97%">
		<tr>
			<td width="50%">
			<table cellpadding="3" cellspacing="0" class="tabla2"
				style="width: 100%" ID="Table3">
				<tr>
					<td>Observaciones</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="0" class="tabla2"
				style="width: 100%" ID="Table4">
				<tr>
					<td><textarea readonly class="cajatexto_1"
						style="width: 430px; height: 65px;" ID="txaObservaciones"
						NAME="txaObservaciones"></textarea></td>
				</tr>
			</table>
			</td>
			<td width="50%">
			<table cellpadding="3" cellspacing="0" class="tabla2"
				style="width: 100%" ID="Table5">
				<tr>
					<!-- td>Adjuntos</td-->
					<td>&nbsp;&nbsp;</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="0" class="tabla"
				style="width: 100%" ID="Table6">
				<tr>
					<td><iframe id="iFrameAdjunto" name="iFrameAdjunto"
						frameborder="0" height="120px" width="100%"></iframe></td>

				</tr>
			</table>
			</td>
		</tr>
	</table>
	<table ID="DetalleProducto1" name="DetalleProducto1" style="margin-left: 9px;"
		cellpadding="2" cellspacing="2" class="tabla2" border="0"
		bordercolor="red" width="97%">
		<tr>
			<td width="50%">
			<table cellpadding="3" cellspacing="0" class="tabla2"
				style="width: 100%" ID="Table3">
				<tr>
					<td>Observaciones Proveedor</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="0" class="tabla2"
				style="width: 100%" ID="Table4">
				<tr>
					<td><textarea class="cajatexto_1" readonly
						style="width: 430px; height: 65px;" ID="txaObservacionesProveedor"
						NAME="txaObservacionesProveedor"></textarea></td>
				</tr>
			</table>
			</td>
			<td width="50%">
			<table cellpadding="3" cellspacing="0" class="tabla2"
				style="width: 100%" ID="Table5">
				<tr>
					<!-- td>Adjuntos Proveedor</td-->
					<td>&nbsp;&nbsp;</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="0" class="tabla"
				style="width: 100%" ID="Table6">
				<tr>
					<td><iframe id="iFrameAdjuntoProveedor"
						name="iFrameAdjuntoProveedor" frameborder="0" height="120px"
						width="100%"> </iframe></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	
	</div>
	
	<table align="center">
		<tr>
			<td align="center">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar" style="cursor: pointer;"
						onclick="javascript:fc_Regresar();">
				</a>							
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgExportar','','${ctx}/images/botones/exportar2.jpg',1)">
					<img alt="Exportar" src="${ctx}/images/botones/exportar1.jpg" id="imgExportar" style="cursor:pointer;" onclick="fc_Exportar();">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imggrabar1','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="imggrabar1" style="cursor: pointer;" onclick="javascript:fc_Grabar();">
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('iCerrarCot','','${ctx}/images/botones/cerrarcot2.jpg',1)">
					<img alt="Cerrar Cotizaci�n" src="${ctx}/images/botones/cerrarcot1.jpg" id="iCerrarCot" style="cursor: pointer;" onclick="fc_CerrarCotizacion();">
				</a>
			</td>
		</tr>
	</table>
	</div>


	<!-- ****************VARIABLES GLOBALES UTILIZADAS********************* -->
	<input type="hidden" name="txhNumGrupos" id="txhNumGrupos"
		value="<c:out value="${nroGrupos}" />" />
	<input type="hidden" name="txhNumItems" id="txhNumItems"
		value="<c:out value="${nroItems}" />" />
	<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txtFecIni",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgfecini",
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "txtFecFin",
			ifFormat       :    "%d/%m/%Y",
			daFormat       :    "%d/%m/%Y",
			button         :    "imgfecfin",
			singleClick    :    true
		});
	</script>
</form:form>
</body>


