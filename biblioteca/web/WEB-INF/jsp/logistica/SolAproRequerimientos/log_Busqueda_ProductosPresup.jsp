<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var tot = "";	
		function onLoad()
		{
		document.getElementById("txhDscTec").value = document.getElementById("txhDes").value;
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			 document.getElementById("txhMsg").value = "";
			window.opener.fc_Refrescar();
			alert(mstrSeGraboConExito);
		//	window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		document.getElementById("txhMsg").value = "";
		alert(mstrProblemaGrabar);
		}
		else if(objMsg.value == "DOBLE"){
		objMsg.value = "";
		document.getElementById("txhMsg").value = "";
		window.opener.fc_Refrescar();
		alert(mstrUnoOmas);
		}
		}
	
		function fc_Busca(){
		document.getElementById("txhDes").value="";
		if(document.getElementById("txtCodigo").value == "" && document.getElementById("txtDescripcion").value == ""
		   && document.getElementById("codFamilia").value == "" && document.getElementById("codSubFamilia").value == ""){
		alert(mstrIngreseFiltros);
		return false;
		}
			document.getElementById("txhOperacion").value="BUSCAR";
			document.getElementById("frmMain").submit();
		}
		function fc_Muestra(strCod){
	
			document.getElementById("txhCodProducto1").value = strCod;
			document.getElementById("txhOperacion").value="MUESTRA";
			document.getElementById("frmMain").submit();
		}
		function fc_Imagen(strcod){

		strRuta = "<%=(( request.getAttribute("msgRutaServer")==null)?"": request.getAttribute("msgRutaServer"))%>";
		if ( strRuta != "" )
		{
			if ( fc_Trim(strcod) != "") window.open(strRuta + strcod);
			else alert(mstrNoTiene2);
		}
		}
		function fc_Adjunto(strCod){
		strRuta = "<%=(( request.getAttribute("msgRutaServer2")==null)?"": request.getAttribute("msgRutaServer2"))%>";
		if ( strRuta != "" )
		{
			if ( fc_Trim(strCod) != "") window.open(strRuta + strCod);
			else alert(mstrNoTiene);
		}
		}
		function fc_Limpia(){
			document.getElementById("txtCodigo").value = "";
			document.getElementById("txtDescripcion").value = "";
			document.getElementById("codFamilia").value = "";
			document.getElementById("codSubFamilia").value = "";
			document.getElementById("txhDes").value="";
			document.getElementById("txhDscTec").value = "";
					document.getElementById("txhOperacion").value="LIMPIA";
					document.getElementById("frmMain").submit();
		}
		
		function fc_Grabar(){
			/*if(tot>1){
				alert(mstrSeleccioneUno);
				return false;
			}*/
			//if( tot>0){
			if ( document.getElementById("txhCodProducto").value != "" ){
				if (confirm(mstrSeguroGrabar)){
					document.getElementById("txhOperacion").value="GRABAR";
					document.getElementById("frmMain").submit();
					}
			}
			else{
				alert(mstrSeleccione);
			}
		}
		function fc_seleccionarRegistro(strCod){
			
				strCodSel = document.getElementById("txhCodProducto").value;
				flag=false;
		if (strCodSel!='')
		{	
			ArrCodSel = strCodSel.split("|");
			strCodSel = "";
			for (i=0;i<=ArrCodSel.length-2;i++)
			{
				if (ArrCodSel[i] == strCod){ 
				flag = true; 
				}
				else{
					strCodSel = strCodSel + ArrCodSel[i]+'|';
				}
			}
		}
		if (!flag)
		{   	
			strCodSel = strCodSel + strCod + '|';
		}
		document.getElementById("txhCodProducto").value = strCodSel;
		
		fc_GetNumeroSeleccionados();
	}
	function Fc_MuestraSubFamilia(){
	document.getElementById("txhDes").value="";
		document.getElementById("txhOperacion").value="MUESTRASUBFAMILIA";
		document.getElementById("frmMain").submit();
	}
	function fc_GetNumeroSeleccionados()
	{
		strCodSel = document.getElementById("txhCodProducto").value;
		if ( strCodSel != '' )
		{
			ArrCodSel = strCodSel.split("|");
			tot = ArrCodSel.length - 1;
		}
		else{ 
		tot = 0;
	
		}
	}

	function fc_TipoBien(){
		document.getElementById("txhCodTipoBien").value="0002"; //Consumibles
		document.getElementById("txhOperacion").value="LISTAR_FAMILIA";
		document.getElementById("frmMain").submit();
	}

	function fc_TipoActivo(){
		document.getElementById("txhCodTipoBien").value="0001"; //Activos
		document.getElementById("txhOperacion").value="LISTAR_FAMILIA";
		document.getElementById("frmMain").submit();
	}
	
	</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Busqueda_ProductosPresup.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="sede" id="txhSede"/>
	<form:hidden path="codProducto" id="txhCodProducto"/>
	<form:hidden path="codProducto1" id="txhCodProducto1"/> 
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="des" id="txhDes"/>  
	<form:hidden path="codAlumno" id="txhCodAlumno"/> 
	<form:hidden path="codReq" id="txhCodReq"/>
	<form:hidden path="codTipoBien" id="txhCodTipoBien"/>
<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" class="opc_combo" style="width: 650px">
		 		<font style="">Busqueda Catalogo de Productos..</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<table class="tabla" style="width:98%;margin-top:6px;margin-left:6px" background="${ctx}/images/Evaluaciones/back.jpg" 
		cellspacing="2" cellpadding="2" > 
		<tr>
			<td nowrap>C�digo</td>
			<td>
				<form:input path="codigo" id="txtCodigo" onkeypress="fc_ValidaNumerico2();"
					onblur="fc_ValidaNumeroOnBlur('txtCodigo');" cssClass="cajatexto"  maxlength="10" cssStyle="width:90px"/>
			</td>
			<td nowrap>Descriptor</td>
			<td>
				<form:input path="descripcion" id="txtDescripcion" 
				cssClass="cajatexto" size="20" maxlength="60" cssStyle="width:250px"/>
			</td>
		</tr>

		<tr>
			<td>
			</td>
			<td>
				<input type="radio" id="rbtConsumible" name="radioTipoBien" checked onclick="fc_TipoBien();">Consumible&nbsp;
				<input type="radio" id="rbtActivo" name="radioTipoBien" onclick="fc_TipoActivo();">Activo&nbsp;&nbsp;&nbsp;
			</td>
			<td></td>
			<td></td>
		</tr>

		<tr>
			<td nowrap>Familia</td>
			<td>
				<form:select path="codFamilia" id="codFamilia" cssClass="cajatexto"
					cssStyle="width:200px" onchange="javascript:Fc_MuestraSubFamilia();">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listFamilia}" />
					</c:if>
					</form:select>
			</td>
			<td nowrap>SubFamilia</td>
			<td>
				<form:select path="codSubFamilia" id="codSubFamilia" cssClass="cajatexto"
					cssStyle="width:200px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listSubFamilia!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listSubFamilia}" />
					</c:if>
					</form:select>
			</td>
			<td align="right">
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
						<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:fc_Busca();" alt="Buscar" style="cursor:hand" id="icoBuscar"></a>&nbsp;
					<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
						<img src="${ctx}/images/iconos/limpiar1.jpg" onclick="javascript:fc_Limpia();" alt="Limpiar" style="cursor:hand" id="icoLimpiar"></a>&nbsp;
			</td>
		</tr>
	</table>
	<table style="width:98%;margin-top:6px;margin-left:6px" cellspacing="0" cellpadding="0" border="0" bordercolor="blue">
		<tr>
			<td style="vertical-align: top; width:100%">
				<div style="overflow: auto; height: 185px;width:100%">									
					<display:table name="sessionScope.listProducto" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
					requestURI="" style="border: 1px solid #048BBA;width:98%">
					
					<display:column property="rbtSelCatalogo" title="Sel" headerClass="grilla" class="tablagrilla" style="text-align:center;width:5%"/>
					<display:column property="codProducto" title="C�digo Producto" headerClass="grilla" class="tablagrilla" style="text-align:center;width:10%"/>
					<display:column property="lblproducto" title="Producto" headerClass="grilla" class="tablagrilla" style="text-align:left;width:30%" />
					<display:column property="lblproducto1" title="Unidad<br>Medida" headerClass="grilla" class="tablagrilla" style="text-align:center;width:10%"/>
					<display:column property="lblproducto2" title="Precio<br>Unitario" headerClass="grilla" class="tablagrilla" style="text-align:right;width:10%"/>
					<!-- display:column property="lblproducto4" title="Stock<br>Disp." headerClass="grilla" class="tablagrilla" style="text-align:right;width:8%"/-->
					<display:column property="imagen" title="Ver<br>Imagen" headerClass="grilla" class="tablagrilla" style="text-align:center;width:6%"/>
					
					<display:setProperty name="basic.empty.showtable" value="true"  />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='10' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<% request.getSession().removeAttribute("listProducto"); %>
				</div> 
			</td>
		</tr>
	</table>
	<table style="width:98%;margin-top:6px;margin-left:6px;" cellspacing="0" cellpadding="0" >		
		<tr>
			<td width="50%" style="vertical-align: top">
				<table class="tabla" style="width:96%; height: 25px" background="${ctx}/images/Evaluaciones/back.jpg" 
					cellspacing="0" cellpadding="0" > 
					 <tr>
					 	<td >&nbsp;Descripci�n</td>
					 </tr>
				 </table>
				<table cellpadding="0" cellspacing="0" class="tabla" style="width:96%; margin-top: 3px">
					<tr>
						<td>							
							<form:textarea path="descTecnica" id="txhDscTec" cssClass="cajatexto_1" cssStyle="width:100%;height:95px;" disabled="true"></form:textarea>
						</td>	
					</tr>
				</table>
			</td>				
			<td width="50%" style="vertical-align: top">
				<table class="tabla" style="width:96%; height: 25px" background="${ctx}/images/Evaluaciones/back.jpg" 
					cellspacing="0" cellpadding="0" > 
					 <tr>
					 	<td >&nbsp;Documentos Relacionados</td>
					 </tr>
				 </table>
				 <table cellpadding="0" border="0" cellspacing="0" style="width:100%; margin-top: 3px" align="left">
				 	<tr>
				 		<td>
						 <div style="overflow: auto;margin-left; height: 100px;width:100%" align="left" >									
							<display:table name="sessionScope.listTipoAdjunto" cellpadding="0" cellspacing="1" 
								decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
								requestURI="" style="border: 1px solid #048BBA;margin-left;width:96%">
								
								<display:column property="dscTipAdj" title="Tipo Adjunto" headerClass="grilla" class="tablagrilla" style="text-align:left;width:90%"/>
								<display:column property="adjunto" title="Ver" headerClass="grilla" class="tablagrilla" style="width:10%; text-align: center"/>
								
								<display:setProperty name="basic.empty.showtable" value="true"  />
								<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
								<display:setProperty name="paging.banner.placement" value="bottom"/>
								<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
								<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
								<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
							</display:table>
							<% request.getSession().removeAttribute("listTipoAdjunto"); %>
						</div>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
	</table>
	<table><tr height="5px"><td></td></tr></table>
	<table align="center">
		<tr>
			<td colspan="3" width="100%" align="center">
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:fc_Grabar();" alt="Graba y Envia a Mantenimiento" id="btnGrabar"></a>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar"></a>
			</td>		
		</tr>			
	</table>
</form:form>
