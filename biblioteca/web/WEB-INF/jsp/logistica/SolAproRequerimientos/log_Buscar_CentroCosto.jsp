<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	
	function onLoad()
	{
		
	}
	
	function fc_Grabar(){
		
		if ( document.getElementById("txhCodCecoSeleccionado").value == "" )
		{
			alert("Debe seleccionar un centro de costo.");
			return false;
		}
		
		nomObjeto = document.getElementById("txhNomObjeto").value;
		window.opener.document.getElementById(nomObjeto).value =  document.getElementById("txhCodCecoSeleccionado").value;
		window.close();
	}
	
	function Fc_Buscar(){
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	
	function fc_SelCentroCosto(strCod){
		document.getElementById("txhCodCecoSeleccionado").value = strCod; 
	}
	</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Buscar_CentroCosto.html">
	<form:hidden path="operacion" id="txhOperacion"/> 
	<form:hidden path="sede" id="txhSede"/>
	<form:hidden path="nomObjeto" id="txhNomObjeto"/>
	<form:hidden path="codCecoSeleccionado" id="txhCodCecoSeleccionado"/>
	
	
	<table cellpadding="0" cellspacing="0" width="96%" style="margin-left:6px;margin-top:6px">
		<tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="300px" class="opc_combo">
		 		<font style="">Buscar Centro de Costo</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	<table cellpadding="2" cellspacing="0"  class="tabla"
		background="${ctx}/images/Logistica/back.jpg" 
		style="margin-left:6px;margin-top:6px; width: 96%">
		<tr>
			<td width="25%">Centro Costo :</td>
			<td width="70%">
				<form:input path="descripcion" id="txtDescripcion" cssClass="cajatexto" cssStyle="width:80%;"/>
			</td>
			<td align="right" width="5%">
				<a OnMouseOut="MM_swapImgRestore()" OnMouseOver="MM_swapImage('icoBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" onclick="javascript:Fc_Buscar();" alt="Buscar" style="cursor:hand" id="icoBuscar"></a>&nbsp;
			</td>		
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="96%" style="margin-left:6px;margin-top:6px;">
	  	<tr>
	  		<td>
	 			<div style="overflow: auto; height: 285px;width:100%">									
					<display:table name="sessionScope.listCentros" cellpadding="0" cellspacing="1"
					decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
					requestURI="" style="border: 1px solid #048BBA;width:96%">
					<display:column property="rbtSelCentroCosto" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
					<display:column property="codSeco" title="C�digo" headerClass="grilla" class="tablagrilla" style="align:left;width:10%"/>
					<display:column property="descripcion" title="Nombre" headerClass="grilla" class="tablagrilla" style="align:left;width:25%"/>
					
					<display:setProperty name="basic.empty.showtable" value="true" />
					<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
					<display:setProperty name="paging.banner.placement" value="bottom"/>
					<display:setProperty name="paging.banner.item_name" value="<span>Registro</span>" />
					<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
					<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<% request.getSession().removeAttribute("listCentros"); %>
				</div>  
			</td>
		</tr>
	</table> 
	<table align="center">
		<tr>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:fc_Grabar();" alt="Graba y Envia a Mantenimiento" id="btnGrabar"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="Cancelar" id="btnCancelar"></a>
			</td>
		</tr>
	</table>
</form:form>