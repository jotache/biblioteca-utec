<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var cod = "";
	var codUni = "";
	function onLoad()
		{
		document.getElementById("txtProducto").value = document.getElementById("txhProducto").value;
		objMsg = document.getElementById("txhMsg");
		
		document.getElementById("txhOperacion").value="";
		}
	function Fc_Grabar(){
	id = cod;
	codSerie = codUni;
	pos = document.getElementById("txhPosicion").value;
		window.opener.fc_setSerie(id,codSerie,pos);
		window.close();
	}
	function fc_buscarDescriptor(strCod,strCodPro){
	cod = strCod;
	codUni = strCodPro;
	}
	</script>
	</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_BusquedaActivos.html">
	<form:hidden path="operacion" id="txhOperacion"/> 
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="codProducto" id="txhCodProducto"/>
	<form:hidden path="sede" id="txhSede"/>
	<form:hidden path="posicion" id="txhPosicion"/> 
	<form:hidden path="producto" id="txhProducto"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		<tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="400px" class="opc_combo">
		 		<font style="">Busqueda de Activos
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>	
	
	<table class="tabla" style="width:98%;margin-top:6px;margin-left:6px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
					cellspacing="2" cellpadding="0" border="0" bordercolor="red">	
		<tr>
			<td width="20%">Familia :</td>
			<td>
				<form:input path="familia" id="txtFamilia" 
					cssClass="cajatexto_1" cssStyle="width:150px;" readonly="true"/>
			</td>
			<td width="20%">Sub Familia :</td>
			<td>
				<form:input path="subFamilia" id="txtSubFamilia" 
			  		cssClass="cajatexto_1" cssStyle="width:150px;" readonly="true"/>
			</td>
		</tr>
		<tr>
			<td width="20%">Producto :</td>
			<td colspan="3">
			<textarea id="txtProducto" class="cajatexto_1" style="width:98%;" readonly></textarea>	
			</td>	
		</tr>			
	</table>
	<table cellpadding="0" cellspacing="0" style="width:98%;margin-top:6px;margin-left:6px;" border="0" bordercolor="red">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" width="100%">
					<tr><td>
					<div style="overflow: auto; height: 200px;width:100%">		
													
						<display:table name="sessionScope.listProductos" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:100%">
							
							<display:column property="rbtSelProductosActivos" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
							<display:column property="nroSerie" title="Nro. Serie" headerClass="grilla" class="tablagrilla" style="align:left;width:150px"/>
							<display:column property="nroOrden" title="O/C. Asoc." headerClass="grilla" class="tablagrilla" style="align:left;width:180px"/>
							<display:column property="valorCompra" title="Valor Compra" headerClass="grilla" class="tablagrilla" style="align:left;width:300px"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
						<% request.getSession().removeAttribute("listProductos"); %>
					</div>
					</td></tr>
				</table>	
			</td>
		</tr>
	</table>
		
	<table align=center style="margin-top:7px">
		<tr>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
			</td>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar"></a>
			</td>
		</tr>
	</table>	
</form:form>
