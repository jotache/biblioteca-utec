<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{
			objMsg.value = "";
			document.getElementById("txhMsg").vlaue = "";
			alert(mstrSeGraboConExito);
	//		window.opener.document.getElementById("frmMain").submit();
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		document.getElementById("txhMsg").vlaue = "";
		alert(mstrProblemaGrabar);
		}
		document.getElementById("txhOperacion").value="";
		}
		 function Fc_Grabar(){
		 if ( fc_Trim(document.getElementById("txhCodRes").value) !== "" && document.getElementById("codComplejidad").value !==""){
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();
		}
		else{
			alert(mstrSeleccion);
			}
		}
		function fc_seleccionaRes(strCod){
		document.getElementById("txhCodRes").value = strCod;
		}
</script>
	</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_ConsultarAsignarTrabajo.html">
	<form:hidden path="operacion" id="txhOperacion"/>
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="sede" id="txhSede"/>
	<form:hidden path="codRes" id="txhCodRes"/>
	<form:hidden path="codReq" id="txhCodReq"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<!-- ALQD,21/04/09. CAMBIE EL ANCHO DE UN TABLE DE 100 A 97% -->
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		<tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="400px" class="opc_combo">
		 		<font style="">Consultar / Asignar Trabajo
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table class="tabla" style="width:98%;margin-top:6px;margin-left:6px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
		cellspacing="2" cellpadding="0" border="0" bordercolor="red">
		<tr>
			<td class="">Nro. Req.</td>
			<td>&nbsp;<form:input path="nroReq" id="txtNroReq"
				onkeypress="fc_ValidaTextoNumeroEspecial();"
				onblur="fc_ValidaNumeroLetrasGuionOnblur(this,'nro. req');"
				cssClass="cajatexto_1" size="20" readonly="true" cssStyle="width:150px;" />
			</td>
			<td class="">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	

	<table cellpadding="0" cellspacing="0" style="width:98%;margin-top:6px;margin-left:6px;" border="0" bordercolor="red">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" width="100%">
					<tr><td>
					<div style="overflow: auto; height: 150px;width:100%">
					
					<display:table name="sessionScope.listTrabajo" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator"
						requestURI="" style="border: 1px solid #048BBA;width:97%">
						
			 			<display:column property="rbtSelResponsale" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
						<display:column property="nombreUsu" title="Responsable" headerClass="grilla" class="tablagrilla" style="align:left;width:250px"/>
						<display:column property="valor1" title="Total Trab. Asignado" headerClass="grilla" class="tablagrilla" style="align:left;width:100px"/>
						<display:column property="valor2" title="Prom. Total Esfuerzo" headerClass="grilla" class="tablagrilla" style="align:left;width:100px"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
					</display:table>
					<% request.getSession().removeAttribute("listTrabajo"); %>
				</div>
				</td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<table cellpadding="1" cellspacing="2" class="texto_bold">
		<tr>
			<td class="">Definir Complejidad&nbsp;:&nbsp;&nbsp;
				<form:select path="codComplejidad" id="codComplejidad" cssClass="cajatexto" cssStyle="width:100px">
					<form:option value="">--Todos--</form:option>
					<c:if test="${control.listComplejidad!=null}">
						<form:options itemValue="codSecuencial" itemLabel="descripcion"
						items="${control.listComplejidad}" />
					</c:if>
				</form:select>
			</td>
		</tr>
	</table>
	
	<br>
	
	<table align=center style="margin-top:7px">
		<tr>
		<td>
		<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
		<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
			</td>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
			<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar"></a>
		</tr>
	</table>
</form:form>
