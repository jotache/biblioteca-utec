<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>
	var cod = "";
	var nombre = "";
	function onLoad()
		{
		
		document.getElementById("txhOperacion").value="";
		}
		function Fc_Grabar(){
		codResponsable =cod;
		nomResponsable = nombre;
		pos = document.getElementById("txhPosicion").value;
		window.opener.fc_setResponsable(codResponsable,nomResponsable,pos)
		window.close();
	}
	function Fc_Busca(){
		document.getElementById("txhOperacion").value="BUSCAR";
		document.getElementById("frmMain").submit();
	}
	function Fc_Limpia(){
	document.getElementById("codTipoPersonal").value = "";
	document.getElementById("txtNombre").value = "";
	document.getElementById("txtApePaterno").value = "";
	document.getElementById("txtApeMaterno").value = "";
	}
	function fc_seleccionarRegistro(strCod,strTipo,strNombre){
	cod = strCod;
	nombre = strNombre;
	}
			</script>
	</head>
	<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_BusquedaEmpleados.html">
	<form:hidden path="operacion" id="txhOperacion"/> 
	<form:hidden path="msg" id="txhMsg"/>
	<form:hidden path="posicion" id="txhPosicion"/>
	<form:hidden path="codAlumno" id="txhCodAlumno"/>
	<form:hidden path="sede" id="txhSede"/>
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		<tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="400px" class="opc_combo">
		 		<font style="">Busqueda de Empleados
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>
	
	<table class="tabla" style="width:98%;margin-top:6px;margin-left:6px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
		cellspacing="2" cellpadding="0" border="0" bordercolor="red">	
		<tr>
			<td width="20%">Tipo Personal :</td>
			<td>
				<form:select path="codTipoPersonal" id="codTipoPersonal" cssClass="cajatexto"
					cssStyle="width:150px">
					<form:option value="">--Todos--</form:option>
						<c:if test="${control.listTipoPersonal!=null}">
							<form:options itemValue="codSecuencial" itemLabel="descripcion"
							items="${control.listTipoPersonal}" />
						</c:if>
				</form:select>
			</td>
			<td align="right" colspan="2">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:Fc_Limpia();" ></a>&nbsp;					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:Fc_Busca();"></a>&nbsp;
			</td>
		</tr>
		<tr>
			<td>Nombres :</td>
			<td colspan="3"><form:input path="nombre" id="txtNombre" 
				onkeypress="fc_ValidaTextoEspecial();"  
				onblur="fc_ValidaTextoEspOnBlur('txtNombre','nombre');"
				cssClass="cajatexto" size="50" cssStyle="width:180px;" />
			</td>
		</tr>
		<tr>
			<td>Apellido Paterno:</td>
			<td><form:input path="apePaterno" id="txtApePaterno" 
				onkeypress="fc_ValidaTextoEspecial();"  
				onblur="fc_ValidaTextoEspOnBlur('txtApePaterno','apellido paterno');"
				cssClass="cajatexto" size="20" cssStyle="width:150px;" /></td>
			<td>Apellido Materno:</td>
			<td><form:input path="apeMaterno" id="txtApeMaterno" 
				onkeypress="fc_ValidaTextoEspecial();"  
				onblur="fc_ValidaTextoEspOnBlur('txtApeMaterno','apellido materno');"
				cssClass="cajatexto" size="20" cssStyle="width:150px;" /></td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" style="width:98%;margin-top:6px;margin-left:6px;" border="0" bordercolor="red">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" width="100%">
					<tr><td>
					<div style="overflow: auto; height: 200px;width:100%">			
																
						<display:table name="sessionScope.listEmpleados" cellpadding="0" cellspacing="1"
							decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
							requestURI="" style="border: 1px solid #048BBA;width:100%">
							
							<display:column property="rbtSelEmpleados" title="Sel" headerClass="grilla" class="tablagrilla" style="width:5%;text-align:center"/>
							<display:column property="nombre" title="Nombres" headerClass="grilla" class="tablagrilla" style="align:left;width:150px"/>
							<display:column property="tipoPersonal" title="Tipo Personal" headerClass="grilla" class="tablagrilla" style="align:left;width:180px"/>
							<display:column property="departamento" title="Departamento" headerClass="grilla" class="tablagrilla" style="align:left;width:300px"/>
							
							<display:setProperty name="basic.empty.showtable" value="true"  />
							<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
							<display:setProperty name="paging.banner.placement" value="bottom"/>
							<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
							<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
							<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
						<% request.getSession().removeAttribute("listEmpleados"); %>
					</div>
					</td></tr>
					</table>	
				</td>
			</tr>
		</table>
		
		<table align=center style="margin-top:7px">
			<tr>
			<td>
			<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
			<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:Fc_Grabar();" alt="Grabar" id="btnGrabar"></a>
				</td>
				<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="cancelar" id="btnCancelar"></a>
			</tr>
		</table>
</form:form>

