<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language=javascript>	
		function onLoad()
		{
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" )
		{	
			objMsg.value = "";
			window.opener.document.getElementById("frmMain").submit();
			alert(mstrSeGraboConExito);
			window.close();
		}
		else if( objMsg.value == "ERROR" )
		{
		objMsg.value = "";
		alert(mstrProblemaGrabar);
		}
		fc_cambio();
		if(document.getElementById("txhHaber").value=="OK"){
		fc_Haber();
		document.getElementById("txhHaber").value="";
		}
		}
		
		function fc_cambio(){
		
			document.getElementById("divBusEmple").style.display = "none";
			
			if(document.getElementById("txhFlag").value ==""){
				document.getElementById("txhFlag").value = "1";
				tabla.style.display="none";
				tabla2.style.display='inline-block';
				tabla22.style.display='inline-block';
				tabla3.style.display="none";
				document.getElementById("divBusEmple").style.display = "inline-block";
			}	
			else{
				document.getElementById("txhFlag").value = "";
				tabla.style.display="";
				tabla2.style.display='none';
				tabla22.style.display='none';
				tabla3.style.display="none";
			
			}
		}
		function Fc_Limpia(){			
		document.getElementById("codCentroCostos").value = "";
		document.getElementById("txtNombre").value = "";
		document.getElementById("txtApPaterno").value = "";
		document.getElementById("txtApMaterno").value = "";
		}
		function Fc_Buscar(){		
		document.getElementById("txhOperacion").value="BUSCAR";
					document.getElementById("frmMain").submit();			
		}
		function fc_Grabar(){
		if ( document.getElementById("txhCodigo1").value != "" ){
		if(confirm(mstrSeguroGrabar)){
		document.getElementById("txhOperacion").value="GRABAR";
		document.getElementById("frmMain").submit();			
		}
		}
		else{
			alert(mstrSeleccione);
			}
		}
		function fc_Imagen(strCodigo){
		//tabla3.style.display="";
	//	fc_Haber();
		document.getElementById("txhCodigo").value=strCodigo;
		document.getElementById("txhOperacion").value="LLENAR";
		document.getElementById("frmMain").submit();
		}
		function fc_Haber(){
		tabla3.style.display="";
		}
		function fc_seleccionaRes(strCodigo,strNombre){
			document.getElementById("txhCodigo1").value = strCodigo;
		}
		
function fc_Exportar(){
// alert(document.getElementById("chek").checked);
		var srtFlag="";
		if(document.getElementById("txhFlag").value=="1")
		 srtFlag="1";
		else if(document.getElementById("txhFlag").value=="")
			  srtFlag="0";
	   al=document.getElementById("codCentroCostos").options[document.getElementById("codCentroCostos").selectedIndex].text ;
	   al=al.replace("--","");
	   al=al.replace("--","");
	   //alert(al+">>D<<"+document.getElementById("txhDscUsuario").value);
	   usu=document.getElementById("txhDscUsuario").value;
	   url="?tCodRep=" + document.getElementById("txhCodTipoReporte").value +
					"&tCodSede=" + document.getElementById("txhSede").value +
					"&tCodUsu=" + document.getElementById("txhCodUsuario").value + 
					"&tFlagEstado=" + srtFlag +  
					"&tCodCeco=" + document.getElementById("codCentroCostos").value +
					"&tNombre=" + document.getElementById("txtNombre").value + 
					"&tApPat=" + document.getElementById("txtApPaterno").value  + 
					"&tApMat=" + document.getElementById("txtApMaterno").value + 
					"&tNroReq=" + document.getElementById("txtNroRequerimiento").value +
					"&tDscCeco=" + al ;
					
	//alert(srtFlag);	/encuestas/reporteEncuestas.html	/logistica/reportesLogistica.html	
	//alert(url.length);
	if(url!="")	
	window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
	
		}
		
</script>
</head>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/log_Aprobacion_Responsable.html">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="flag" id="txhFlag"/>
<form:hidden path="msg" id="txhMsg"/> 
<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
<form:hidden path="sede" id="txhSede"/>
<form:hidden path="haber" id="txhHaber"/>
<form:hidden path="codigo" id="txhCodigo"/>
<form:hidden path="codigo1" id="txhCodigo1"/>
<form:hidden path="codAlumno" id="txhCodAlumno"/>
<form:hidden path="codTipoReporte" id="txhCodTipoReporte"/> 
<form:hidden path="dscUsuario" id="txhDscUsuario"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="nomSede" id="txhNomSede"/>
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:6px;margin-top:6px">
		<tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="650px" class="opc_combo">
		 		<font style="">Consultar / Asignar Responsable
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		</tr>
	</table>	
	<!-- CABECERA --> 
	<table class="tabla" style="width:98%;margin-top:6px;margin-left:6px;height: 30px;" background="${ctx}/images/Evaluaciones/back.jpg" 
					cellspacing="2" cellpadding="0" border="0" bordercolor="red">
		<tr>
			<td width="15%">Nro. Req.:</td>
			<td width="30%">
				<form:input path="nroRequerimiento" id="txtNroRequerimiento" 
					 readonly="true"
					cssClass="cajatexto_1" size="20" cssStyle="width:60%;" />
			</td>			
			<td width="15%" align="right">
				<input type="checkbox" name="chek" id="chek" <%if("".equalsIgnoreCase((String)request.getAttribute("flag"))){ %> checked<%} %> onclick="fc_cambio()" /> 
			</td>
			<td width="30%">
				Definir Otro Responsable
			</td>
			<td width="10%">
			</td>
		</tr>
	</table>	
	<div id="tabla" style="overflow: auto; height: 200px;width:100%">
	<table cellpadding="0" cellspacing="0" style="width:99%;margin-top:6px;margin-left:6px;" border="0" bordercolor="red">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" width="100%">
					<tr><td>
					<div style="overflow: auto; height: 190px;width:99%">
														
						<display:table name="sessionScope.listResponsable1" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:98%">
						
						<display:column property="rbtSelResponsale" title="Sel." headerClass="grilla" class="tablagrilla" style="text-align:center;width:5%"/>
						<display:column property="nombreUsu" title="Responsable" headerClass="grilla" class="tablagrilla" style="align:left;width:50%"/>
						<display:column property="valor1" title="Total Trab. Asignado" headerClass="grilla" class="tablagrilla" style="text-align:right;width:20%"/>
						<display:column property="valor2" title="Prom. Total Esfuerzo" headerClass="grilla" class="tablagrilla" style="text-align:right;width:20%"/>
						<display:column property="img" title="Ver Detalle" headerClass="grilla" class="tablagrilla" style="text-align:center;width:5%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
					</div>
					</td></tr>
				</table>
			</td>
		</tr>
	</table>
	</div>	
	<div id="tabla3" style="display:none; height: 80px;width:100%;margin-top: 0px">
	<table cellpadding="0" cellspacing="0" style="width:98%;margin-top:6px;margin-left:6px;" border="0" bordercolor="red">
		<tr>
			<td>
				<table cellpadding="0" align="center" cellspacing="0" border="0" bordercolor="white" width="70%">
					<tr><td>
					<div style="overflow: auto; height: 80px;width:100%">									
						<display:table name="sessionScope.listDetalle" cellpadding="0" cellspacing="1"
						decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
						requestURI="" style="border: 1px solid #048BBA;width:96%">
						<display:column property="codReq" title="Numero Req." headerClass="grilla" class="tablagrilla" style="text-align:center;width:20%"/>
						<display:column property="fecha" title="Fecha" headerClass="grilla" class="tablagrilla" style="text-align:center;width:20%"/>
						<display:column property="descripcion" title="Descripción" headerClass="grilla" class="tablagrilla" style="text-align:left;width:60%"/>
						
						<display:setProperty name="basic.empty.showtable" value="true"  />
						<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
						<display:setProperty name="paging.banner.placement" value="bottom"/>
						<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
						<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
						<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
						</display:table>
					</div>		
					</td></tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<div  id="divBusEmple" style="width:100%;display:none;" >		
	<table id="tabla2" class="tabla" style="display:none; width:98%;margin-top:6px;margin-left:6px;height: 30px;" background="${ctx}\images\biblioteca\fondoinf.jpg" 
				cellspacing="2" cellpadding="0" border="0" bordercolor="red">		
		<tr>
			<td width="15%">Centro de Costo:</td>
			<td width="30%">
				<form:select path="codCentroCostos" id="codCentroCostos" cssClass="cajatexto_o"
							cssStyle="width:150px">
							<form:option value="">--Seleccione--</form:option>
							<c:if test="${control.centroCostos!=null}">
							<form:options itemValue="codTecsup" itemLabel="descripcion"
							items="${control.centroCostos}" />
							</c:if>
							</form:select>
			</td>					
			<td width="15%">Nombres:</td>
			<td width="40%" colspan="2">
				<form:input path="nombre" id="txtNombre" 
					onkeypress="fc_ValidaTextoEspecial();" onblur="fc_ValidaTextoEspOnBlur('txtNombre','nombre');"
					cssClass="cajatexto" size="40" maxlength="60" cssStyle="width:250;"/>
			</td>			
		</tr>
		<tr>
			<td width="15%">Ap. Paterno:</td>
			<td width="30%">
				<form:input path="apPaterno" id="txtApPaterno" onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaTextoEspOnBlur('txtApPaterno','apellido paterno');"
					cssClass="cajatexto" size="40" maxlength="60" cssStyle="width:150px;"/>
			</td>
			<td width="15%">Ap. Materno:</td>
			<td width="30%">
				<form:input path="apMaterno" id="txtApMaterno" onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaTextoEspOnBlur('txtApMaterno','apellido materno');"
					cssClass="cajatexto" size="40" maxlength="60" cssStyle="width:150px;"/>
			</td>
			<td width="10%" align="right">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imglimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
					<img src="${ctx}/images/iconos/limpiar1.jpg" align="absmiddle" alt="Limpiar" style="CURSOR: pointer" id="imglimpiar" onclick="javascript:Fc_Limpia();" ></a>&nbsp;					
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
					<img src="${ctx}/images/iconos/buscar1.jpg" align="absmiddle" alt="Buscar" id="imgBuscar" style="CURSOR: pointer" onclick="javascript:Fc_Buscar();"></a>&nbsp;
			</td>
		</tr>
	</table>
	<table id="tabla22" cellpadding="0" cellspacing="0" style="display:none;width:98%;margin-left:6px;height: 100px;margin-top:6px;"
		 border="0" bordercolor="red">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" bordercolor="white" width="100%">
					<tr>
						<td>
							<div style="overflow: auto; height: 200px;width:100%">									
								<display:table name="sessionScope.listResponsable2" cellpadding="0" cellspacing="1"
									decorator="com.tecsup.SGA.bean.LogisticaDecorator" 
									requestURI="" style="border: 1px solid #048BBA;width:98%">
								
									<display:column property="rbtSelResponsale" title="Sel" headerClass="grilla" class="tablagrilla" style="text-align:center;width:5%"/>
									<display:column property="nombreUsu" title="Nombres" headerClass="grilla" class="tablagrilla" style="align:left;width:40%"/>
									<display:column property="valor1" title="Centro De Costos" headerClass="grilla" class="tablagrilla" style="align:left;width:30%"/>
									<display:column property="valor2" title="Cargo" headerClass="grilla" class="tablagrilla" style="align:left;width:25%"/>
								
									<display:setProperty name="basic.empty.showtable" value="true"  />
									<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='7' align='center'>No se encontraron registros</td></tr>"  />
									<display:setProperty name="paging.banner.placement" value="bottom"/>
									<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
									<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
									<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
								</display:table>
							</div>		
						</td>
					</tr>
				</table>
			</td>
		</tr>			
	</table>
	</div>
	<table class="tablagrilla" cellpadding="0" cellspacing="0" 
		style="width:98%;margin-left:6px; border: 1px solid #048BBA;margin-top:6px;">
		<tr height="20px">
			<td width="15%">&nbsp;&nbsp;&nbsp;Definir Complejidad :</td>
			<td width="85%" align="left">
				<form:select path="codComplejidad" id="codComplejidad" cssClass="cajatexto"
					cssStyle="width:265px">
					<c:if test="${control.listComplejidad!=null}">
					<form:options itemValue="codSecuencial" itemLabel="descripcion"
					items="${control.listComplejidad}" />
					</c:if>
					</form:select>
			</td>
		</tr>
	</table>
	<table align="center">
		<tr>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src="${ctx}/images/botones/grabar1.jpg" style="cursor:hand;" onclick="javascript:fc_Grabar();" alt="Graba y Envia a Mantenimiento" id="btnGrabar"></a>
			</td>
			<td>
			   <a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnExportar','','${ctx}/images/botones/exportar2.jpg',1)">		
					<img src="${ctx}/images/botones/exportar1.jpg" style='cursor:hand;' onclick="javascript:fc_Exportar();" alt="Exportar" id="btnExportar">
				</a>
			<td>
				<a onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','${ctx}/images/botones/cancelar2.jpg',1)">		
				<img src="${ctx}/images/botones/cancelar1.jpg" style='cursor:hand;' onclick="javascript:window.close();" alt="Cancelar" id="btnCancelar"></a>
			</td>
		</tr>
	</table>	
</form:form>