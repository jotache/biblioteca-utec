<%@ include file="/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">
	function onLoad(){

       if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodBien").value)
		        botondevolver.style.display='inline-block';
	   else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodServicio").value)
		         {    botondevolver.style.display='none';
		         }
		     }
	
	   if(document.getElementById("txhBanderaTipoReq").value=="BIEN"){
		    
			document.getElementById("TablaBien").style.display = 'inline-block';
			
		}
		else{ if(document.getElementById("txhBanderaTipoReq").value=="SERVICIO"){
		       
				document.getElementById("TablaServicio").style.display = 'inline-block';
				
			  }
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodBien").value)
		  {
		     document.getElementById("TablaBien").style.display = 'inline-block';
		  }
		 else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodServicio").value)
		  		{ document.getElementById("TablaServicio").style.display = 'inline-block';
		  		}
		 }
		}
		
		if ( document.getElementById("txhMsg").value=="OK")
		{   document.getElementById("txhMsg").value="";
			alert(mstrElimino);
			
		}
		else{ if ( document.getElementById("txhMsg").value== "ERROR" )
				{   document.getElementById("txhMsg").value="";
					alert(mstrNoElimino);
				}
			  else{ if ( document.getElementById("txhMsg").value== "NO_SE_ELIMINA" )
					{   document.getElementById("txhMsg").value="";
						alert(mstrNoElim);
					}//ALMACEN_CERRADO
					else if ( document.getElementById("txhMsg").value== "ALMACEN_CERRADO" )
						{   document.getElementById("txhMsg").value="";
						    alert(mstrEstadoLogistica);
						}
			   }
		}
		 document.getElementById("txhCodRequerimiento").value="";
		 document.getElementById("txhCodEstadoBD").value="";
		 document.getElementById("txhIndiceDevolucion").value="";
		 document.getElementById("txhCodTipoBien").value="";
	}
		
		function fc_Limpiar()
		{
			document.getElementById("txtNroRequerimiento").value = "";
			document.getElementById("txtFecInicio").value = "";
			document.getElementById("txtFecFinal").value = "";
			document.getElementById("cboCeco").value = "-1";
			document.getElementById("cboEstado").value = "-1";
		}
		
		function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
		}
		
		function fc_Nueva(){
		
		  if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
		  {
				opcion=document.getElementById("codTipoRequerimiento").value;
				var indTitulo="";//0 BIEN - 1 SERVICIO
				if(opcion==document.getElementById("txhConsteCodBien").value){
					indTitulo=0;
				}
				else
				if(opcion==document.getElementById("txhConsteCodServicio").value){
					indTitulo=1;
				}
				codRequerimiento=document.getElementById("txhCodRequerimiento").value;
				codSede=document.getElementById("txhCodSede").value;
				codUsuario=document.getElementById("txhCodUsuario").value;
				window.location.href="${ctx}/logistica/SreqBienesPresup.html"+
									"?txhCodRequerimiento="+ codRequerimiento+
									"&txhCodSede="+codSede+
									"&txhCodUsuario="+codUsuario+
									"&txhIndTitulo="+indTitulo+
									"&txhCodTipoRequerimiento="+document.getElementById("codTipoRequerimiento").value+
									"&txhCodRequerimientoOri=" +
									"&txhCodTipoBien="+document.getElementById("txhCodTipoBien").value +
									"&txhTipoProcedencia=0";
		 }
		 else alert(mstrEstadoLogistica);
		}
		
		function fc_Edita(){
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
		  {
			if(document.getElementById("txhCodRequerimiento").value!="")
			{  if(confirm(mstrSeguroModificar1)){
					opcion=document.getElementById("codTipoRequerimiento").value;
					var indTitulo="";//0 BIEN - 1 SERVICIO
					if(opcion==document.getElementById("txhConsteCodBien").value){
						indTitulo=0;
					}
					else
					if(opcion==document.getElementById("txhConsteCodServicio").value){
						indTitulo=1;
					}
					
				    codRequerimiento=document.getElementById("txhCodRequerimiento").value;
					codSede=document.getElementById("txhCodSede").value;
					codUsuario=document.getElementById("txhCodUsuario").value;
					window.location.href="${ctx}/logistica/SreqBienesPresup.html"+
						"?txhCodRequerimiento="+codRequerimiento+
						"&txhCodSede="+codSede+
						"&txhCodUsuario="+codUsuario+
						"&txhIndTitulo="+indTitulo+
						"&txhCodTipoRequerimiento="+document.getElementById("codTipoRequerimiento").value+
						"&txhCodRequerimientoOri=" +
						"&txhCodTipoBien="+document.getElementById("txhCodTipoBien").value +
						"&txhTipoProcedencia=0";
			   }
			}
			else alert(mstrSeleccion);
			
		}
		else alert(mstrEstadoLogistica);
		
		}
		
		function fc_Elimina(){
		
		if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
		  {
			if(document.getElementById("txhCodRequerimiento").value!="")
			  {
			    if(confirm(mstrSeguroEliminar1)){
			      document.getElementById("txhOperacion").value = "ELIMINAR";
			     
			      document.getElementById("frmMain").submit();
			    }
			  }
			else alert(mstrSeleccion);
		}
		else alert(mstrEstadoLogistica);
		
		}
		
		function fc_TipoReq(){
		     if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodBien").value)
		        botondevolver.style.display='inline-block';
		     else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodServicio").value)
		             { botondevolver.style.display='none';
		              
		             }
		     }
		     
		     fc_Buscar();
		
		}

		function fc_Buscar()
		{    srtFechaInicio=document.getElementById("txtFecInicio").value;
		     srtFechaFin=document.getElementById("txtFecFinal").value;
		     var num=0;
		     var srtFecha="0";
		     if( srtFechaInicio!="" && srtFechaFin!="")
		       {
		           num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		           if(num==1) 	srtFecha="0";
		           else srtFecha="1";
		       }
		    
		    if(srtFecha=="0")
		    	{	document.getElementById("txhBandera").value ="1";
		    	    document.getElementById("txhOperacion").value = "BUSCAR";
					document.getElementById("frmMain").submit();
		    	}
			else alert(mstrFecha);
			
		}
		function fc_seleccionarRegistro(srtCodigo, srtCodEstado, srtIndiceDevolucion, srtSubTipoReq){
		 document.getElementById("txhCodRequerimiento").value=srtCodigo;
		 document.getElementById("txhCodEstadoBD").value=srtCodEstado;
		 document.getElementById("txhIndiceDevolucion").value=srtIndiceDevolucion;
		 document.getElementById("txhCodTipoBien").value=srtSubTipoReq;
		}
		
		function fc_Devolver(){
		
		 if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)
		  {	if(document.getElementById("txhCodRequerimiento").value!="")
				{
					  if(document.getElementById("txhIndiceDevolucion").value=="1")
						{ if(document.getElementById("txhCodEstadoBD").value!=document.getElementById("txhConsteCodEstadoPendiente").value &&
							document.getElementById("txhCodEstadoBD").value!=document.getElementById("txhConsteCodEstadoRechazado").value &&
							document.getElementById("txhCodEstadoBD").value!=document.getElementById("txhConsteCodEstadoAprobacion").value &&
							document.getElementById("txhCodEstadoBD").value!=document.getElementById("txhConsteCodEstadoCerrado").value )
							{  if(confirm(mstrSeguroDevolverBien)){
							    codRequerimiento=document.getElementById("txhCodRequerimiento").value;
								codSede=document.getElementById("txhCodSede").value;
								codUsuario=document.getElementById("txhCodUsuario").value;
								srtOpcion="0";
								window.location.href="${ctx}/logistica/SreqDevolverBienes.html"+
									"?txhCodRequerimiento="+codRequerimiento+
									"&txhCodSede="+codSede+
									"&txhCodUsuario="+codUsuario+"&txhOpcion="+srtOpcion+
									"&txhCodTipoBien="+document.getElementById("txhCodTipoBien").value;
							   }
						   }
						  else{ if(document.getElementById("txhCodEstadoBD").value==document.getElementById("txhConsteCodEstadoPendiente").value)
						         alert(mstrNoDevolverPendiente);
						        else{ if(document.getElementById("txhCodEstadoBD").value==document.getElementById("txhConsteCodEstadoRechazado").value)
						              alert(mstrNoDevolverRechazado);
						              else{ if(document.getElementById("txhCodEstadoBD").value==document.getElementById("txhConsteCodEstadoAprobacion").value)
						                     alert(mstrNoDevolverAprobacion);
						                    else alert(mstrNoDevolverCerrado);
						              }
						         }
						  }
					   }
					   else alert(mstrDevolverArticulos);
				
				}
				else alert(mstrSeleccion);
		  }
		  else alert(mstrEstadoLogistica);
		
		}
	function fc_repSolxEntregar(){

		codRep = "0026";
		url="?tCodRep=" + codRep +
			"&codUsuario=" + fc_Trim(document.getElementById("txhCodUsuario").value);
		window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
		
	}

</script>
</head>

<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/bandejaSreqPresupuestado.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="banderaTipoReq" id="txhBanderaTipoReq"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="consteCodBien" id="txhConsteCodBien"/>
<form:hidden path="consteCodServicio" id="txhConsteCodServicio"/>
<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
<form:hidden path="codOpciones" id="txhCodOpciones"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="codEstadoBD" id="txhCodEstadoBD"/>
<form:hidden path="consteCodEstadoPendiente" id="txhConsteCodEstadoPendiente"/>
<form:hidden path="consteCodEstadoRechazado" id="txhConsteCodEstadoRechazado"/>
<form:hidden path="consteCodEstadoAprobacion" id="txhConsteCodEstadoAprobacion"/>
<form:hidden path="consteCodEstadoCerrado" id="txhConsteCodEstadoCerrado"/>
<form:hidden path="indiceDevolucion" id="txhIndiceDevolucion"/>
<form:hidden path="codTipoBien" id="txhCodTipoBien"/>
<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
<form:hidden path="bandera" id="txhBandera"/>

	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo"><font style="">Bandeja de Requerimientos..</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	</table>
	<!-- Titulo de la Pagina -->
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg"
				cellspacing="2" cellpadding="2" border="0">
		<tr>
			<td width="10%">&nbsp;Tipo Req. :</td>
			<td align="left" width="30%">
				<form:select path="codTipoRequerimiento" id="codTipoRequerimiento" cssClass="combo_o"
					cssStyle="width:110px" onchange="javascript:fc_TipoReq();">
					<c:if test="${control.listTipoRequerimiento!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
						items="${control.listTipoRequerimiento}" />
					</c:if>
				</form:select>
			</td>
			<td width="10%">&nbsp;Nro. Req. :</td>
			<td width="10%">
				<form:input path="nroRequerimiento" id="txtNroRequerimiento" cssClass="cajatexto"
					onkeypress="fc_ValidaNumerosGuion();"
					onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Req.');"
					cssStyle="width:90px" maxlength="15" />
			</td>
			<td width="10%" align="right">&nbsp;Fec. Req. :</td>
			<td width="20%">
				<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"
					cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
					align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
				<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"
					cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
					align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
			</td>
		</tr>
		<tr>
			<td >&nbsp;Centro Costo :</td>
			<td align="left">
				<form:select path="codCeco" id="cboCeco" cssClass="cajatexto"
					cssStyle="width:320px">
					<form:option value="-1">-- Todos --</form:option>
					<c:if test="${control.listCeco!=null}">
					<form:options itemValue="codTecsup" itemLabel="descripcion"
						items="${control.listCeco}" />
					</c:if>
				</form:select>
			</td>
			<td >&nbsp;Estado :</td>
			<td align="left">
				<form:select path="codEstado" id="cboEstado" cssClass="cajatexto"
					cssStyle="width:100px">
					<form:option value="-1">-- Todos --</form:option>
					<c:if test="${control.listEstado!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
						items="${control.listEstado}" />
					</c:if>
				</form:select>
			</td>
			<!-- ALQD,26/01/09. A�ADIENDO REPORTE VER PENDIENTES DE ENTREGA -->
			<td colspan="2" align="right">
				<a href="javascript:fc_repSolxEntregar();" >
					Reporte Pendientes de Entrega
				</a>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
				<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle"
					 style="cursor: pointer; 60px"
					 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle"
					 style="cursor: pointer; margin-right: " onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
			
			</td>
		</tr>
	</table>
	<!-- BANDEJA -->
	<table cellpadding="0" cellspacing="0" id="tablaBandejaSolicitud" border="0" bordercolor="red"
		style="width:97%;margin-top:6px;margin-left:9px" >
		<!-- Aki era el Problema al cerrar los tr -->
		<tr>
			<td id="TablaBien" style="display: none;">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							<div style="overflow: auto; height:350px;width:100%">
								<display:table name="sessionScope.listaBien" cellpadding="0" cellspacing="1"
								decorator="com.tecsup.SGA.bean.LogisticaDecorator"  requestURI=""
								style="border: 1px solid #048BBA;width:98%">
									<display:column property="checkSelRequerimientoBien" title="Sel"
										headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
									<display:column property="nroRequerimiento" title="Nro. Req." headerClass="grilla"
									class="tablagrilla" style="text-align:center;width:15%"/>
									<display:column property="fechaEmision" title="Fec. Req." headerClass="grilla"
									class="tablagrilla" style="text-align:center;width:10%"/>
									<display:column property="dscCecoSolicitante" title="Centro Costo" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:15%"/>
									
									<display:column property="dscSubTipoRequerimiento" title="Tipo Bien" headerClass="grilla"							 
										class="tablagrilla" style="text-align:left;width:10%"/>
									

									<display:column property="totalRequerimiento" title="Precio Referencial" headerClass="grilla"
									class="tablagrilla" style="text-align:right;width:15%"/>
									<display:column property="dscEstado" title="Estado" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:15%"/>
									<display:column property="dscDetalleEstado" title="Detalle Estado" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:15%"/>
									
									<display:setProperty name="basic.empty.showtable" value="true"  />
									<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='8' align='center'>No se encontraron registros</td></tr>"  />
									<display:setProperty name="paging.banner.placement" value="bottom"/>
									<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
									<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
									<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
									<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
									<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
									<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
									<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
									<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
									<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
									<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
								</display:table>
							</div>
						</td>
						<% request.getSession().removeAttribute("listaBien"); %>
					</tr>
				</table>
			</td>
			<td id="TablaServicio" style="display: none;">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td >
							<div style="overflow: auto; height:350px;width:100%">
								<display:table name="sessionScope.listaServicios" cellpadding="0" cellspacing="1"
								decorator="com.tecsup.SGA.bean.LogisticaDecorator"  requestURI=""
								style="border: 1px solid #048BBA;width:98%;">
									
									<display:column property="checkSelRequerimientoServicio" title="Sel"
										headerClass="grilla" class="tablagrilla" style="text-align:center; width:5%"/>
									<display:column property="nroRequerimiento" title="Nro. Req." headerClass="grilla"
									class="tablagrilla" style="text-align:center;width:10%"/>
									<display:column property="dscCecoSolicitante" title="Centro Costo" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:15%"/>
									<display:column property="dscGrupoServicio" title="Grupo Servicio" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:10%"/>
									<display:column property="dscSubTipoRequerimiento" title="Tipo Servicio" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:14%"/>
									<display:column property="totalRequerimiento" title="Precio Referencial" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:10%"/>
									<display:column property="fechaEmision" title="Fec. Req." headerClass="grilla"
									class="tablagrilla" style="text-align:center;width:10%"/>
									<display:column property="dscEstado" title="Estado" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:10%"/>
									<display:column property="dscDetalleEstado" title="Detalle Estado" headerClass="grilla"
									class="tablagrilla" style="text-align:left;width:16%"/>
									
									<display:setProperty name="basic.empty.showtable" value="true"  />
									<display:setProperty name="basic.msg.empty_list_row" value="<tr class='texto'><td colspan='9' align='center'>No se encontraron registros</td></tr>"  />
									<display:setProperty name="paging.banner.placement" value="bottom"/>
									<display:setProperty name="paging.banner.item_name" value="<span >Registro</span>" />
									<display:setProperty name="paging.banner.items_name" value="<span>Registros</span>" />
									<display:setProperty name="paging.banner.no_items_found" value="<span class='texto'>No se encontraron registros</span>" />
									<display:setProperty name="paging.banner.one_item_found" value="<span class='texto'>Un registro encontrado</span>" />
									<display:setProperty name="paging.banner.all_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando todo {2}.</span>" />
									<display:setProperty name="paging.banner.some_items_found" value="<span class='texto'>{0} {1} encontrados, mostrando {2} a {3}.</span>" />
									<display:setProperty name="paging.banner.full" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
									<display:setProperty name="paging.banner.first" value="<span class='texto'>[Primero/Anterior] {0} [<a href={3}>Pr&oacute;ximo</a>/<a href={4}>Ultimo</a>]</span>" />
									<display:setProperty name="paging.banner.last" value="<span class='texto'>[<a href={1}>Primero</a>/<a href={2}>Anterior</a>] {0} [Pr&oacute;ximo/Ultimo]</span>" />
									<display:setProperty name="paging.banner.onepage" value="<span class='texto'>{0}</span>" />
								</display:table>
							</div>
						</td>
						<% request.getSession().removeAttribute("listaServicios"); %>
					</tr>
				</table>
			</td>
			<td id="TablaBotones" name="TablaBotones" align="right" width="3%">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgagregar','','${ctx}/images/iconos/agregar2.jpg',1)">
				<img src="${ctx}/images/iconos/agregar1.jpg" style="cursor:pointer;" id="imgagregar" onclick="fc_Nueva();"
				alt="Agregar Solicitud"></a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgmodificar','','${ctx}/images/iconos/modificar2.jpg',1)">
				<img src="${ctx}/images/iconos/modificar1.jpg" style="cursor:pointer;" id="imgmodificar" onclick="fc_Edita();"
				alt="Modificar Solicitud"></a>
				<br/>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgquitar','','${ctx}/images/iconos/quitar2.jpg',1)">
				<img src="${ctx}/images/iconos/quitar1.jpg" style="cursor:pointer;" id="imgquitar" onclick="fc_Elimina();"
				alt="Eliminar Solicitud"></a>
           </td>
		</tr>
	</table>
	<table id="botondevolver" align="center" style="display: none; margin-top: 10px" border="0" bordercolor="blue">
		<tr>
		    <td align=center>
		       <a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgDevolver','','${ctx}/images/botones/devolver2.jpg',1)">
		       <img src="${ctx}/images/botones/devolver1.jpg" style="cursor:pointer;" id="imgDevolver" onclick="javascript:fc_Devolver();"
						alt="Devolver">
		       </a>
		    </td>
	   </tr>
	</table>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>