<%@page import="com.tecsup.SGA.common.CommonConstants"%>
<%@ include file="/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" type="text/javascript">

	var strExtensionDOC = "<%=CommonConstants.GSTR_EXTENSION_DOC%>";
    var strExtensionXLS = "<%=CommonConstants.GSTR_EXTENSION_XLS%>";
    var strExtensionDOCX = "<%=CommonConstants.GSTR_EXTENSION_DOCX%>";
    var strExtensionXLSX = "<%=CommonConstants.GSTR_EXTENSION_XLSX%>";
	var strExtensionPDF = "<%=CommonConstants.GSTR_EXTENSION_PDF%>";
	
	function onLoad(){
		objMsg = document.getElementById("txhMsg");
		if ( objMsg.value == "OK" ){			
			//window.opener.document.getElementById("txhCodAlumno").value="";
			window.opener.fc_Refrescar();
			alert(mstrGrabar);
			window.close();
		}
		else if ( objMsg.value == "ERROR" ){
			alert(mstrProblemaGrabar);
			window.close();
		}
	}
	
	function fc_Valida(){
		var strExtension;
		var lstrRutaArchivo;
		if(fc_Trim(document.getElementById("txtArchivo").value)==""){
			alert(mstrNoArchivo);
			return false;
		}
		else{
			if(fc_Trim(document.getElementById("txtArchivo").value) != ""){
				lstrRutaArchivo = document.getElementById("txtArchivo").value;
				strExtension = "";
				strExtension = lstrRutaArchivo.toLowerCase().substring(lstrRutaArchivo.length - 3);
				
				//**************************
				//obtiene el nombre del archivo
				nombre = (lstrRutaArchivo.substring(lstrRutaArchivo.lastIndexOf("\\"))).toLowerCase();				 
				document.getElementById("txhNomArchivo").value=nombre.substring("1");
				//**************************
			
				//if (strExtension != strExtensionDOC && strExtension != strExtensionXLS){
				/*if ((strExtension != strExtensionDOC)&&(strExtension != strExtensionXLS)&&(strExtension != strExtensionPDF)){
					alert(mstrExtensionArchivo);
					return false;
				}
				else{ if(strExtension == strExtensionDOC)
				      	document.getElementById("txhExtArchivo").value = strExtension;
				      else{ if(strExtension == strExtensionXLS)
				       	    document.getElementById("txhExtArchivo").value = strExtension;
				       	    else{ if(strExtension == strExtensionPDF) 
				       	          document.getElementById("txhExtArchivo").value = strExtension; 
				       	        }
				      } 
				   }*/
				document.getElementById("txhExtArchivo").value = strExtension;
			}
		
		}
		
		return true;
	}
	
	function fc_Guardar(){
		if (fc_Valida())
		{
		  if(document.getElementById("codTipoAdjunto").value!="-1")  
			{if (confirm(mstrSeguroGrabar))
			  {				
				document.getElementById("txhOperacion").value="GRABAR";
				document.getElementById("frmMain").submit();
			  }
			}
		  else alert(mstrSelTipoAdjunto);
		} 
	}
</script>
</head>
<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/bandejaSreqUsuarioAdjuntarDocumentos.html" enctype="multipart/form-data" method="post">

<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="codRequerimiento" id="txhCodRequerimiento"/>
<form:hidden path="codDetRequerimiento" id="txhCodDetRequerimiento"/>
<form:hidden path="msg" id="txhMsg"/>
<form:hidden path="extArchivo" id="txhExtArchivo"/>
<form:hidden path="nomArchivo" id="txhNomArchivo"/>	

<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:10px;margin-top:5px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/Flotante/icono.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/Flotante/repeticion.jpg" width=220px" class="opc_combo"><font style="">Adjuntar Archivo</font></td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/Flotante/curvatit.jpg"></td>
		 </tr>
	 </table>	
	 
	<table class="tabla" background="${ctx}/images/Evaluaciones/back-sup.jpg" 
			style="width:98%;margin-top:2px; height:100%" cellspacing="2" cellpadding="0"> 
		<tr>
			<td nowrap>Tipo Adjunto :</td>			
			 <td>
				<form:select path="codTipoAdjunto" id="codTipoAdjunto" cssClass="combo_o" 
								cssStyle="width:200px">
				<form:option value="-1">-- Seleccione --</form:option>
				<c:if test="${control.listaTipoAdjunto!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
					items="${control.listaTipoAdjunto}" />
					</c:if>
				</form:select>
				</td>
				</tr>
				<tr>
				<td nowrap>Archivo :</td>			
				<td>
				 <input type=file name="txtArchivo" id="txtArchivo" class="cajatexto" style="width:350px;">
					</td>
				</tr>
			</table>
			
		<br>
		<table align=center>
			<tr>
				<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgAgregar','','${ctx}/images/botones/grabar2.jpg',1)">
				<img src= "${ctx}/images/botones/grabar1.jpg" onclick="javascript:fc_Guardar();" style="cursor:hand" id="imgAgregar" alt="Grabar">&nbsp;</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Button1','','${ctx}/images/botones/cancelar2.jpg',1)">
				<img ID="Button1" NAME="Button1" src="${ctx}/images/botones/cancelar1.jpg" alt="Cancelar" style="cursor:hand" onclick="window.close();"></a>
			</td>
			</tr>
		</table>
</form:form>
</body>
</html>