<%@ include file="/taglibs.jsp"%>

<head>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/lang/calendar-en.js"></script>
	<script type="text/javascript" src="${ctx}/scripts/jscalendar/calendar-setup.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">
		function onLoad(){
		
		document.getElementById("TablaIndGrupo").style.display = 'none';
		document.getElementById("uno").style.display = 'inline-block';
		document.getElementById("TablaTipoServicio").style.display = 'none';
		
		var objLong = "<%=((request.getAttribute("TIPO")==null)?"":(String)request.getAttribute("TIPO"))%>";
		
		if(document.getElementById("txhBanderaTipoReq").value=="BIEN"){
		    
			TablaBien.style.display = 'inline-block';
			TablaIndGrupo.style.display = 'none';
			TablaTipoServicio.style.display = 'none';
			
		}
		else{ if(document.getElementById("txhBanderaTipoReq").value=="SERVICIO"){
		        if(objLong=="RADIO")
			    TablaServicio.style.display = 'inline-block';
			    
				TablaIndGrupo.style.display = 'inline-block';
				TablaTipoServicio.style.display = 'inline-block';
				document.getElementById("uno").style.display = 'none';
				}
		}
		
		if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodBien").value)
		  {
		     document.getElementById("TablaBien").style.display = 'inline-block';
		  }
		 else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodServicio").value)
		  		{ document.getElementById("TablaServicio").style.display = 'inline-block';
		  		}
		 }
		
		var load = "<%=((request.getAttribute("load")==null)?"":(String)request.getAttribute("load"))%>";
		if(load=='load')
			fc_Buscar()
		
		}
		function fc_Limpiar()
		{
			document.getElementById("txtNroRequerimiento").value = "";
			document.getElementById("txtFecInicio").value = "";
			document.getElementById("txtFecFinal").value = "";
			document.getElementById("cboCeco").value = "-1";
			
			document.getElementById("txtUsuSolicitante").value = "";
			document.getElementById("codTipoServicio").value = "-1";
		}
		
		function fc_Regresar()
		{
			document.location.href = "${ctx}/menuLogistica.html?txhCodSede="+document.getElementById("txhCodSede").value;
		}
		
		function fc_Nueva(){
		}
		
		function fc_Edita(){
		}
		
		function fc_Elimina(){
		}
		
		function fc_TipoReq(){
		  if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodBien").value)
		  { TablaTipoServicio.style.display = 'none';
			document.getElementById("TablaIndGrupo").style.display = 'none';
		    document.getElementById("uno").style.display = 'inline-block';
		   
		  }
		  else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodServicio").value)
		 	 { TablaTipoServicio.style.display = 'inline-block';
			   document.getElementById("TablaIndGrupo").style.display = 'inline-block';
		       document.getElementById("uno").style.display = 'none';
		       
		  	 }
		  }
		   fc_Buscar();
		}

		function fc_Buscar()
		{   srtFechaInicio=document.getElementById("txtFecInicio").value;
		    srtFechaFin=document.getElementById("txtFecFinal").value;
		    var num=0;
		    var srtFecha="0";
		    
		    if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodServicio").value)
			  {  if(document.getElementById("txhIndGrupo").value=="")
		            document.getElementById("txhIndGrupo").value="0";
			  }
			else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodBien").value)
		              document.getElementById("txhIndGrupo").value="";
			 }
		     
		    if( srtFechaInicio!="" && srtFechaFin!="")
		       {  	num=fc_ValidaFechaIniFechaFin(srtFechaFin,srtFechaInicio);
		          	if(num==1) 	srtFecha="0";
		           else srtFecha="1";
		       }
		    
		    if(srtFecha=="0")   
		    	{
		    	      document.getElementById("txhOperacion").value = "BUSCAR";
					  document.getElementById("frmMain").submit();
		    	}
			else alert(mstrFecha);
			
		}
		function fc_seleccionarRegistro(srtCodigo){
		}
		function fc_IndiceGrupo(param){
		document.getElementById("txhIndGrupo").value=param;
		document.getElementById("txhOperacion").value = "RADIO";
		document.getElementById("frmMain").submit();
		}
		
		function fc_MostrarDetalleRegistro(param1, srtIdReq, srtIndiceEnvio){
		 document.getElementById("txhCodIndRequerimiento").value=srtIdReq;
		 srtCodTipoReq=document.getElementById("codTipoRequerimiento").value;
		 srtCodSede=document.getElementById("txhCodSede").value;
		 srtIndGrupo=document.getElementById("txhIndGrupo").value;
		 
		 if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodServicio").value)
		  {  if(document.getElementById("txhIndGrupo").value=="")
		         document.getElementById("txhIndGrupo").value="0";
		  }
		 else{ if(document.getElementById("codTipoRequerimiento").value==document.getElementById("txhConsteCodBien").value)
		         document.getElementById("txhIndGrupo").value="";
		 }
		 srtIndGrupo=document.getElementById("txhIndGrupo").value;
		 url="${ctx}/logistica/AreqBienesServicios.html?"+
	     "txhCodUsuario="+document.getElementById("txhCodUsuario").value+"&txhCodSede="+srtCodSede+
	     "&txhCodRequerimiento="+srtIdReq+"&txhIndiceEnvio="+srtIndiceEnvio+
	     "&txhIndGrupo="+srtIndGrupo+
	     "&txhCodTipoRequerimiento="+srtCodTipoReq ;
	     
		 window.location.href=url;
		}
	function fc_repSolAprobados(){
		codRep = "0028";
		url="?tCodRep=" + codRep +
			"&codUsuario=" + fc_Trim(document.getElementById("txhCodUsuario").value);
		window.open("${ctx}/logistica/reportesLogistica.html"+url,"ReportesLogistica","resizable=yes, menubar=yes");
	}
		
</script>
</head>

<body>
<form:form id="frmMain" name="frmMain" commandName="control" action="${ctx}/logistica/bandejaAreqUsuario.html" enctype="multipart/form-data" method="post">
<form:hidden path="operacion" id="txhOperacion"/>
<form:hidden path="banderaTipoReq" id="txhBanderaTipoReq"/>
<form:hidden path="codSede" id="txhCodSede"/>
<form:hidden path="consteCodBien" id="txhConsteCodBien"/>
<form:hidden path="consteCodServicio" id="txhConsteCodServicio"/>
<form:hidden path="codPerfil" id="txhCodOPerfil"/>
<form:hidden path="indGrupo" id="txhIndGrupo"/>
<form:hidden path="valorIndGrupo" id="txhValorIndGrupo"/>
<form:hidden path="codIndRequerimiento" id="txhCodIndRequerimiento"/>
<form:hidden path="codUsuario" id="txhCodUsuario"/>
<form:hidden path="consteGrupoGenerales" id="txhConsteGrupoGenerales"/>
<form:hidden path="consteGrupoServicios" id="txhConsteGrupoServicios"/>
<form:hidden path="consteGrupoMantenimiento" id="txhConsteGrupoMantenimiento"/>
<form:hidden path="banListaBien" id="txhBanListaBien"/>
<form:hidden path="banListaServicio" id="txhBanListaServicio"/>
<form:hidden path="bandera" id="txhBandera"/>

	<table cellpadding="0" cellspacing="0" border="0" width="99%">
		<tr>
			<td class="opc_combo">&nbsp;&nbsp;&nbsp;</td>
			<td align=right>&nbsp;<img src="${ctx}/images/iconos/Regresar.gif" onclick="javascript:fc_Regresar();" style="cursor:hand" alt="Regresar"></td>
		</tr>
	</table>
	<!-- Titulo de la Pagina -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="width:97%; margin-top: 5px; margin-left: 10px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td align="left" background="${ctx}/images/Evaluaciones/centro.jpg" width="432px" class="opc_combo"><font style="">Bandeja de Aprobación de Requerimientos</font></td>
		 	<td align="left" valign="middle" class="opc_combo" id="TablaIndGrupo" name="TablaIndGrupo" width="365px" background="${ctx}/images/Evaluaciones/centro.jpg">
			
			</td>
			<td align="left" id="uno" name="uno" background="${ctx}/images/Evaluaciones/centro.jpg" width="362px" class="opc_combo"></td>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 	
		 </tr>
</table>
	
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg"
				cellspacing="2" cellpadding="2" border="0">
		<tr>
			<td width="13%">&nbsp;Tipo Req. :</td>
			<td align="left" width="24%">
				<form:select path="codTipoRequerimiento" id="codTipoRequerimiento" cssClass="combo_o"
					cssStyle="width:110px" onchange="javascript:fc_TipoReq();">
					<c:if test="${control.listTipoRequerimiento!=null}">
					<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion"
						items="${control.listTipoRequerimiento}" />
					</c:if>
				</form:select>
			</td>
			<td width="13%">&nbsp;Nro. Req. :</td>
			<td width="15%">
				<form:input path="nroRequerimiento" id="txtNroRequerimiento" cssClass="cajatexto"
					onkeypress="fc_ValidaNumerosGuion();"
					onblur="fc_ValidaNumeroGuionOnblur(this, 'Nro. Req.');"
					cssStyle="width:90px" maxlength="15" />
			</td>
			<td width="10%" align="right">&nbsp;Fec. Req. :</td>
			<td width="25%">
				<form:input path="fecInicio" id="txtFecInicio" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecInicio','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecInicio');"
					cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecIni','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecIni"
					align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>&nbsp;&nbsp;
				<form:input path="fecFinal" id="txtFecFinal" cssClass="cajatexto"
					onkeypress="javascript:fc_Slash('frmMain','txtFecFinal','/');"
					onblur="javascript:fc_ValidaFechaOnblur('txtFecFinal');"
					cssStyle="width:70px" maxlength="10"/>&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgFecFin','','${ctx}/images/iconos/calendario2.jpg',1)">
				<img src="${ctx}/images/iconos/calendario1.jpg" id="imgFecFin"
					align="absmiddle" alt="Calendario" style="cursor:pointer;"></a>
			</td>
		</tr>
		<tr>
			<td >&nbsp;C. Costo :</td>
			<td align="left">
				<form:select path="codCeco" id="cboCeco" cssClass="cajatexto"
					cssStyle="width:340px">
					<form:option value="-1">-- Todos --</form:option>
					<c:if test="${control.listCeco!=null}">
					<form:options itemValue="codTecsup" itemLabel="descripcion"
						items="${control.listCeco}" />
					</c:if>
				</form:select>
			</td>
			<td colspan="1">&nbsp;Solicitante :</td>
			<td align="left" colspan="2">
				<form:input path="usuSolicitante" id="txtUsuSolicitante" maxlength="80"
					onkeypress="fc_ValidaTextoEspecial();"
					onblur="fc_ValidaSoloLetrasFinal1(this,'U. Solicitante');"
					cssClass="cajatexto" size="40" />
			</td>
			<td colspan="1" align="right">
				<a href="javascript:fc_repSolAprobados();" >
					Reporte Req. Aprobados
				</a>&nbsp;&nbsp;&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgLimpiar','','${ctx}/images/iconos/limpiar2.jpg',1)">
				<img src="${ctx}/images/iconos/limpiar1.jpg" alt="Limpiar" align="middle"
					 style="cursor: pointer; 60px"
					 onclick="javascript:fc_Limpiar();" id="imgLimpiar"></a>
				&nbsp;
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgBuscar','','${ctx}/images/iconos/buscar2.jpg',1)">
				<img src="${ctx}/images/iconos/buscar1.jpg" alt="Buscar" align="middle"
					 style="cursor: pointer; margin-right: " onclick="javascript:fc_Buscar();" id="imgBuscar"></a>
			</td>
		</tr>
		<tr id="TablaTipoServicio" name="TablaTipoServicio"><td>&nbsp;</td>
			<td align="left">

			<input type="radio" id="gbradio1" name="rdoBandeja" align="left"
				<c:if test="${control.valorIndGrupo=='0'}"><c:out value=" checked=checked " /></c:if> onclick="javascript:fc_IndiceGrupo('0');">
				Grupo Generales / Otros
				<br>
				<input type="radio" id="gbradio2" name="rdoBandeja"
				<c:if test="${control.valorIndGrupo=='1'}"><c:out value=" checked=checked " /></c:if> onclick="javascript:fc_IndiceGrupo('1');">
				 Grupo Mantenimiento
				&nbsp;
				
			</td>
			<td>Tipo Servicio :</td>
			<td colspan="3">
				<form:select path="codTipoServicio" id="codTipoServicio" cssClass="cajatexto"
						cssStyle="width:200px">
						<form:option value="-1">-- Todos --</form:option>
						<c:if test="${control.listaCodTipoServicio!=null}">
						<form:options itemValue="codTipoTablaDetalle" itemLabel="descripcion" 
							items="${control.listaCodTipoServicio}" />
						</c:if>
				</form:select>
			</td>
		</tr>
	</table>

	<div style="overflow: auto; height:250px;width:100%">
	<table cellpadding="0" cellspacing="0" id="tablaBandejaSolicitud1"
		class="tabla1" style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td id="TablaBien" name="TablaBien" style="display: none;">
				
				<c:if test='${control.banListaBien!="0"}'>
					<acronym title="Haga Doble clic para ver los detalles"></acronym>
				</c:if>
				
				<table cellpadding="0" cellspacing="1" class=""
		 			style="border: 1px solid #048BBA;width:100%;" >
				
					<tr >
						<td class="grilla" width="10%">Nro. Req.</td>
						<td class="grilla" width="8%">Fec. Req.</td>
						<td class="grilla" width="28%">Centro Costo</td>
						<td class="grilla" width="12%">Tipo Bien</td>
						<td class="grilla" width="10%">Precio<br/>Referencial</td>
						<td class="grilla" width="16%">U. Solicitante</td>
						<td class="grilla" width="16%">Detalle Estado</td>
					</tr>
					<c:forEach varStatus="loop" var="lista" items="${control.listaBien}"  >
					<tr class="texto" 
					style="cursor: pointer;"  
					ondblclick="fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.idRequerimiento}" />','<c:out value="${lista.indiceEnvio}" />');"
					>
					
						<td align="center" class="tablagrilla" style="width: 10%">
							<a href="#" onclick="fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.idRequerimiento}" />','<c:out value="${lista.indiceEnvio}" />');">
							<c:out value="${lista.nroRequerimiento}" />
							</a>
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 28%">
							<c:out value="${lista.dscCecoSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscSubTipoRequerimiento}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.totalRequerimiento}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 16%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 16%">
							<c:out value="${lista.dscDetalleEstado}" />
						</td>
												
					</tr>
					</c:forEach>
					
					<c:if test='${control.banListaBien=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="7" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
					
				</table>

				<c:if test='${control.banListaBien!="0"}'>
					</acronym>
				</c:if>
				
			</td>
			
			<td id="TablaServicio" name="TablaServicio" style="display: none;">
				
				<table cellpadding="0" cellspacing="1" class=""
		 			style="border: 1px solid #048BBA;width:100%;">
					<tr>
						<td class="grilla" width="10%">Nro. Req.</td>
						<td class="grilla" width="8%">Fec. Req.</td>
						<td class="grilla" width="12%">Centro Costo</td>
						<td class="grilla" width="10%">U. Solicitante</td>
						<td class="grilla" width="12%">Tipo Servicio</td>
						<td class="grilla" width="12%">Nombre Servicio</td>
						<td class="grilla" width="12%">Asignado A</td>
						<td class="grilla" width="14%">Detalle Estado</td>
						
					</tr>
					
					<c:forEach varStatus="loop" var="lista" items="${control.listaServicio}"  >
					<tr class="cajatexto" style="cursor: pointer;" ondblclick="fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.idRequerimiento}" />'
					,'<c:out value="${lista.indiceEnvio}" />');">
						
						<td align="center" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.nroRequerimiento}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.fechaEmision}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscCecoSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.usuSolicitante}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscSubTipoRequerimiento}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.dscTipoServicio}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 12%">
							<c:out value="${lista.usuAsignado}" />
						</td>
						<td align="left" class="tablagrilla" style="width: 14%">
							<c:out value="${lista.dscDetalleEstado}" />
						</td>
					</tr>
					</c:forEach>
					
					<c:if test='${control.banListaServicio=="0"}'>
					<tr class="tablagrilla">
						<td align="center" colspan="8" height="20px">No se encontraron registros
						</td>
					</tr>
					</c:if>
					
				</table>
				
			</td>
			
		</tr>
	</table>
</div>
</form:form>
<script type="text/javascript">
	Calendar.setup({
		inputField     :    "txtFecInicio",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecIni",
		singleClick    :    true
	});
	Calendar.setup({
		inputField     :    "txtFecFinal",
		ifFormat       :    "%d/%m/%Y",
		daFormat       :    "%d/%m/%Y",
		button         :    "imgFecFin",
		singleClick    :    true
	});
</script>
</body>