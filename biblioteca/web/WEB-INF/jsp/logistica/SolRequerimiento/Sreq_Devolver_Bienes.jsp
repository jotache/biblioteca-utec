<%@ include file="/taglibs.jsp"%>
<head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" type="text/javascript">	
	function onLoad(){
		opcion = document.getElementById("txhOpcion").value;
		switch(opcion){
			case "0":				
				tablaMotivoDevolucion.style.display="";
				tablaObservaciones.style.display="none";
				tablaBotonesSolReq.style.display="";
				tablaBotonesAlmacen.style.display="none";
				break;
			case "1":				
				tablaMotivoDevolucion.style.display="";
				tablaObservaciones.style.display="";
				tablaBotonesAlmacen.style.display="";
				tablaBotonesSolReq.style.display="none";
				document.getElementById("txtMotivoDevolucion").className="cajatexto_1";
				document.getElementById("txtMotivoDevolucion").readOnly="true";
				break;
		}
		//alert(document.getElementById("txhCodSubTipoRequerimiento").value);
		//fc_AsignaTipoBien();
		objMsg=document.getElementById("txhMsg");
		
		if ( objMsg.value == "OK_GRABAR" ){		
			alert(mstrGrabar);			
		}		
		else{ if(objMsg.value == "ERROR_GRABAR"){
				   alert(mstrProblemaGrabar);
				}
				else{  if( objMsg.value == "OK_ENVIAR_ALMACEN" ){
					    alert(mstrExitoEnvioAlmacen);
					    fc_Regresar();
						}
						else{  if( objMsg.value == "ERROR_ENVIAR_ALMACEN" ){
							    alert(mstrErrorEnvioAlmacen);
								}
								else if ( objMsg.value == "ALMACEN_CERRADO" ){
									  alert(mstrEstadoLogistica);
									 }
									 else if ( objMsg.value == "DETALLES_NO_SELECCIONADOS" ){
										alert(mstrExisDetallesSinActivos);
									 }
							}
				}
			}	
		document.getElementById("txhMsg").value="";
	}
	/*function fc_AsignaTipoBien(){
		if(document.getElementById("txhCodSubTipoRequerimiento").value==document.getElementById("txhCodTipoBienActivo").value){
			document.getElementById("rbtActivo").checked=true;					
		}
		if(document.getElementById("txhCodSubTipoRequerimiento").value==document.getElementById("txhCodTipoBienConsumible").value){
			document.getElementById("rbtConsumible").checked=true;
		}
	}*/
	function fc_Regresar(){
		codSede=document.getElementById("txhCodSede").value;
		window.location.href="${ctx}/logistica/bandejaSreqUsuario.html?txhCodSede="+codSede+
		"&txhCodUsuario="+document.getElementById("txhCodUsuario").value; 
	}
	function fc_Grabar(){
	if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
	  {					
		fc_DatosBandeja();
		if(fc_Trim(document.getElementById("txtMotivoDevolucion").value)!=""){
		  if(confirm(mstrConfiGrabarDatos)){
			/*alert("codRequerimiento="+document.getElementById("txhCodRequerimiento").value+
				"\ncodDevolucion="+document.getElementById("txhCodDevolucion").value+				
				"\ncadena codigo="+document.getElementById("txhCadenaCodigo").value+
				"\ncadena cantidad="+document.getElementById("txhCadenaCantidad").value+
				"\ncantidad="+document.getElementById("txhTamListaBienes").value+
				"\nmotivo devolucion="+document.getElementById("txtMotivoDevolucion").value+
				"\ncodUsuario="+document.getElementById("txhCodUsuario").value);*/
			document.getElementById("txhOperacion").value="GRABAR";		
			document.getElementById("frmMain").submit();
		 }
	 	 }
	  	 else alert(mstrMotivoDevolucion);	
	  }
	  else alert(mstrEstadoLogistica);			
	}
	function fc_DatosBandeja(){		
		
		var cadenaCodigo="";		
		var cadenaCantidad="";
		
		tamListaBienes=document.getElementById("txhTamListaBienes").value;
		
		for(var i=0; i<tamListaBienes;i++){			
			if(i==0){
				cadenaCodigo=document.getElementById("txhCodigo"+i).value;								
				cadenaCantidad=document.getElementById("txtCantidadDevuelta"+i).value;								
			}
			else{
				cadenaCodigo=cadenaCodigo+"|"+document.getElementById("txhCodigo"+i).value;
				cadenaCantidad=cadenaCantidad+"|"+document.getElementById("txtCantidadDevuelta"+i).value;
			}
		}
		
		document.getElementById("txhCadenaCodigo").value=cadenaCodigo;		
		document.getElementById("txhCadenaCantidad").value=cadenaCantidad;
	}
	
	function fc_EnviarAlmacen(){
	  if(document.getElementById("txhEstadoAlmacen").value==document.getElementById("txhConsteEstadoAlmacen").value)		
	   {
		  if(confirm(mstrSeguroEnviarDevolucionAlmacen)){
			 document.getElementById("txhOperacion").value="ENVIAR_ALMACEN";		
			 document.getElementById("frmMain").submit();
		  }
	    }
		else alert(mstrEstadoLogistica);
	}
	
	function fc_CantidadDevolver(srtIndice){
	  var max=0;
	  var cant=0;
	  max=parseFloat(fc_Trim(document.getElementById("txhMaxDevolver"+srtIndice).value));
	  cant=parseFloat(fc_Trim(document.getElementById("txtCantidadDevuelta"+srtIndice).value));
	  if(max<cant)
	  { 
	    document.getElementById("txtCantidadDevuelta"+srtIndice).focus; 
	    alert(mstrNoDevolverCantidad);
	    document.getElementById("txtCantidadDevuelta"+srtIndice).value="";
	  }
	  
	}
	
	function fc_NumeroSerie(srtIndice){
	
	 document.getElementById("codDevolucionDet").value=document.getElementById("txhCodDevDet"+srtIndice).value;
	 document.getElementById("codRequerimientoDet").value=document.getElementById("txhCodigo"+srtIndice).value;
	 
	 Fc_Popup("${ctx}/logistica/log_Numeros_Series_Devolucion.html?txhCodUsuario=" +
		document.getElementById("txhCodUsuario").value + "&txhCodBien=" +
		document.getElementById("txhCodBien"+srtIndice).value + "&txhCodReq=" + document.getElementById("txhCodRequerimiento").value +
		"&txhCodReqDet=" + document.getElementById("codRequerimientoDet").value + 
		"&txhCodDev=" + document.getElementById("txhCodDevolucion").value +
		"&txhCodDevDet=" + document.getElementById("codDevolucionDet").value +
		"&txhTipoProcedencia=1",500,200);
	}
	
	function fc_MostrarDetalleRegistro(srtIndice, srtCodigo)
	{
	}
	
	function fc_Rechazar()
	{
	}		
	</script>
</head>

<body>
<form:form id="frmMain" commandName="control" action="${ctx}/logistica/SreqDevolverBienes.html" >
	<form:hidden path="operacion" id="txhOperacion" />
	<form:hidden path="msg" id="txhMsg" />
	<form:hidden path="codUsuario" id="txhCodUsuario" />
	<form:hidden path="estado" id="txhEstado" />
	<form:hidden path="codSede" id="txhCodSede" />
	<form:hidden path="codRequerimiento" id="txhCodRequerimiento" />
	<form:hidden path="codDevolucion" id="txhCodDevolucion" />
	<form:hidden path="opcion" id="txhOpcion" />
	<form:hidden path="tipoBien" id="txhTipoBien" />
		
	<form:hidden path="codSubTipoRequerimiento" id="txhCodSubTipoRequerimiento" />
	
	<form:hidden path="codTipoBienActivo" id="txhCodTipoBienActivo" />
	<form:hidden path="codTipoBienConsumible" id="txhCodTipoBienConsumible" />
	<form:hidden path="tamListaBienes" id="txhTamListaBienes" />
	
	<form:hidden path="cadenaCodigo" id="txhCadenaCodigo" />
	<form:hidden path="cadenaCantidad" id="txhCadenaCantidad" />
	<form:hidden path="codTipoBien" id="txhCodTipoBien" />
	<form:hidden path="codDevolucionDet" id="txhCodDevolucionDet" />
	<form:hidden path="codRequerimientoDet" id="txhCodRequerimientoDet" />
	<form:hidden path="estadoAlmacen" id="txhEstadoAlmacen"/>
	<form:hidden path="consteEstadoAlmacen" id="txhConsteEstadoAlmacen"/>
	<!-- title -->
	<table cellpadding="0" cellspacing="0" border="0" bordercolor="red" style="margin-left:9px;margin-top:7px">
		 <tr>
		 	<td align="left"><img src="${ctx}/images/Evaluaciones/izquierda.jpg"></td>
		 	<td background="${ctx}/images/Evaluaciones/centro.jpg" width="780px" class="opc_combo">
		 		<font style="">
		 			Devoluci�n de Bienes
		 		</font>
		 	</td>
		 	<td align="right"><img src="${ctx}/images/Evaluaciones/fin.jpg"></td>
		 </tr>
	 </table>
	<!-- cabecera -->	
	<table class="tabla" style="width:97%;margin-top:6px;margin-left:9px" background="${ctx}/images/Evaluaciones/back.jpg" 
				cellspacing="2" cellpadding="0" >
		<tr>
			<td width="20%">Nro. Devolucion:</td>
			<td width="30%">
				<form:input path="nroDevolucion" id="txtNroDevolucion"													 
					cssClass="cajatexto_1" cssStyle="width: 40%" readonly="true"/>
					
			</td>
			<td width="20%">Fec. Devolucion:</td>
			<td width="30%">
				<form:input path="fechaDevoluci�n" id="txtFechaDevoluci�n"					
					cssClass="cajatexto_1" cssStyle="width: 40%" readonly="true"/>
			</td>
		</tr>
		<tr>
			<td>Centro de Costo:</td>
			<td>
				<form:input path="centroCosto" id="txtCentroCosto"					
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>
			</td>				
			<td>Usuario Solicitante:</td>
			<td>				
				<form:input path="usuSolicitante" id="txtUsuSolicitante"
					cssClass="cajatexto_1" cssStyle="width: 80%" readonly="true"/>				
			</td>
		</tr>
		<tr>
			<td>Tipo de Requerimiento:</td>
			<td>
				<form:input path="tipoRequerimiento" id="txtTipoRequerimiento"
					cssClass="cajatexto_1" cssStyle="width: 50%" readonly="true"/>				
			</td>
			<td>Tipo Bien:</td>			
			<td>
				<input type="radio" id="rbtConsumible" name="radioTipoBien" checked
				 <c:if test="${control.codTipoBien==control.codTipoBienConsumible}">
				  <c:out value=" checked=checked "/></c:if> disabled="disabled">Consumible&nbsp;
				<input type="radio" id="rbtActivo" name="radioTipoBien" 
				<c:if test="${control.codTipoBien==control.codTipoBienActivo}">
				 <c:out value=" checked=checked " /></c:if> disabled="disabled">Activo
			</td>
		</tr>		
		<tr>
			<td>Nro. Req. Asociado:</td>
			<td>
				<form:input path="nroReqAsociado" id="txtNroReqAsociado"
					cssClass="cajatexto_1" cssStyle="width: 50%"/>				
			</td>
		</tr>						
	</table>
	<!-- bandeja de la pagina -->
	<!-- ALQD,06/04/09. MOSTRANDO CANT. RECIBIDA EN VEZ DE CANT. APROBADA -->
	<table cellpadding="0" cellspacing="0" id="tablaBandejaSolicitud" 
		style="width:97%;margin-top:6px;margin-left:9px" >
		<tr>
			<td>
				<div style="overflow: auto; height: 150px">				
				<table cellpadding="0" cellspacing="1" style="width:100%;margin-top:0px;margin-bottom:5px;border: 1px solid #048BBA" id="tablaBandeja">
					<tr>						
						<td class="grilla" width="10%">Codigo</td>
						<td class="grilla" width="25%">Producto</td>
						<td class="grilla" width="10%">Uni. Med.</td>
						<td class="grilla" width="15%">Cant. Recibida</td>
						<td class="grilla" width="15%">Devolver</td>
						<td class="grilla" width="10%">Total Devueltos</td>	
						<td class="grilla" width="15%">Numero Serie</td>												
					</tr>									
					<c:forEach varStatus="loop" var="lista" items="${control.listaBienes}"  >
					<tr class="texto" ondblclick="fc_MostrarDetalleRegistro('<c:out value="${loop.index}"/>','<c:out value="${lista.codigo}" />');">						
						<td align="center" class="tablagrilla" style="width: 8%">							
							<c:out value="${lista.codigoBien}" />
							<input type="hidden" 
								id='txhCodigo<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codigo}" />'>
							<input type="hidden" 
								id='txhMaxDevolver<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.maxDevolucion}" />'>
							<input type="hidden" 
								id='txhCodDevDet<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codDevolucionDet}" />'>
							<input type="hidden" 
								id='txhCodBien<c:out value="${loop.index}"/>'
								value='<c:out value="${lista.codigoBien}" />'>	
						</td>
						<td align="left" class="tablagrilla" style="width: 32%">
							<c:out value="${lista.producto}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 8%">
							<c:out value="${lista.unidadMedida}" />
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">
							<c:out value="${lista.totCantRecibida}" />
						</td>
						<td align="center" class="tablagrilla" style="width: 5%">
							<input type=text value='<c:out value="${lista.cantidadDevuelta}" />'
								id='txtCantidadDevuelta<c:out value="${loop.index}"/>'
								maxlength="10"
								onkeypress="fc_ValidaNumeroDecimal();"
								onblur="fc_ValidaDecimalOnBlur(this.id,'10','2');fc_CantidadDevolver('<c:out value="${loop.index}"/>');"
								class="cajatexto" size="12" style="text-align: right;">			
						</td>
						<td align="right" class="tablagrilla" style="width: 10%">						
							<c:out value="${lista.totalDevuelto}" />							
						</td>
						<td align="center" class="tablagrilla" style="width: 10%">							
							<c:if test="${lista.indiceAsignarSerie=='1'}">
							<img src="${ctx}/images/iconos/agregar_on.gif" onclick="javascript:fc_NumeroSerie('<c:out value="${loop.index}"/>');"
							     style="cursor:hand" alt="N�mero Serie">
							</c:if>
						</td>							
					</tr>
					</c:forEach>
					<c:if test='${control.tamListaBienes=="0"}'>
						<tr class="tablagrilla">
							<td align="center" colspan="6" height="20px">No se encontraron registros
							</td>
						</tr>					 		
					</c:if>
				</table>
				</div>
			</td>			
		</tr>
	</table>
	<table cellpadding="0" cellspacing="4" id="tablaMotivoDevolucion" width="98%" border="0" bordercolor="blue" class="tabla2" style="margin-left:6px;display: none;">
		<tr>
			<td width="100%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td class="">Motivo de Devoluci�n</td>	
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<form:textarea path="motivoDevolucion" id="txtMotivoDevolucion" cssClass="cajatexto_o" cssStyle="width:99.5%;height:40px;"/>
						</td>
					</tr>
				</table>							
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="4" id="tablaObservaciones" width="98%" border="0" bordercolor="blue" class="tabla2" style="margin-left:6px;display: none;">
		<tr>
			<td width="100%">
				<table cellpadding="3" cellspacing="0" class="tabla2" style="width:100%">
					<tr>
						<td class="">Observaciones</td>	
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" class="tablagrilla" style="width:100%;border: 1px solid #048BBA">
					<tr>
						<td>
							<textarea id="txtObservaciones" class="cajatexto" style="width:99.5%;height:40px;"></textarea>
						</td>
					</tr>
				</table>							
			</td>
		</tr>
	</table>	
	
	<br>
		
	<table align="center" id="tablaBotonesSolReq" style="display: none;">
		<tr>
			
			 <td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('botonGrabar','','${ctx}/images/botones/grabar2.jpg',1)">
					<img alt="Grabar" src="${ctx}/images/botones/grabar1.jpg" id="botonGrabar" style="cursor:pointer" onclick="javascript:fc_Grabar();">
				</a>
			</td>
			<td align=center valign="top">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgEnviarAlmacen','','${ctx}/images/botones/enviaralmacen2.jpg',1)">
					<img alt="Enviar Almacen" src="${ctx}/images/botones/enviaralmacen1.jpg" id="imgEnviarAlmacen" style="cursor:pointer;" onclick="fc_EnviarAlmacen();">
				</a>
			</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar01','','${ctx}/images/botones/regresar2.jpg',1)">
					<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar01" style="cursor:pointer;" onclick="fc_Regresar();">
				</a>
			</td>
		</tr>
	</table>
	
	<table align="center" id="tablaBotonesAlmacen" style="display: none;">
		<tr>
			<td align=center valign="top">
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRegresar02','','${ctx}/images/botones/regresar2.jpg',1)">
				<img alt="Regresar" src="${ctx}/images/botones/regresar1.jpg" id="imgRegresar02" style="cursor:pointer;" onclick="fc_Regresar();"></a><td>
			<td>
				<input type="button" class="boton" value="Confirmar" onclick="fc_Confirmar();">&nbsp;</td>
			<td>
				<a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('imgRechazar','','${ctx}/images/botones/rechazar2.jpg',1)">
					<img alt="Rechazar" src="${ctx}/images/botones/rechazar1.jpg" id="imgRechazar" style="cursor:pointer;" onclick="fc_Rechazar();">
				</a>
			<td>
		</tr>
	</table>
		
</form:form>
</body>